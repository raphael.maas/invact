# -*- coding: utf-8 -*-

import os
from pathlib import Path

ROOT_DIR = Path(os.path.dirname(os.path.abspath(__file__))) # This is your Project Root

EDGAR_DB_BASE_URL = "https://www.sec.gov/Archives/edgar/"

DATA_INTERNAL = ROOT_DIR / "internal_Data"

DATA_DIR = ROOT_DIR / "input_Data"
LTS_DIR = DATA_DIR / "lts"
ORG_PSY_CAP_DIR = DATA_DIR / "org_psy_cap"


EDGAR_RAW_DOWNLOADS = DATA_INTERNAL / "edgar/raw_downloads"
EDGAR_FIRST_STAGE = DATA_INTERNAL / "edgar/first_stage"
EDGAR_FIRST_STAGE_PROBLEM_FILES = DATA_INTERNAL / "edgar/first_stage_problem_files"
EDGAR_FIRST_STAGE_FIXED_PROBLEM_FILES = DATA_INTERNAL / "edgar/first_stage_fixed_problem_files"
EDGAR_HTML_FILES = DATA_INTERNAL / "edgar/html_files"

EDGAR_RAW_DOWNLOADS_13G = DATA_INTERNAL / "edgar/raw_downloads_13G"
EDGAR_FIRST_STAGE_13G = DATA_INTERNAL / "edgar/first_stage_13G"
EDGAR_FIRST_STAGE_PROBLEM_FILES_13G = DATA_INTERNAL / "edgar/first_stage_problem_files_13G"
EDGAR_FIRST_STAGE_FIXED_PROBLEM_FILES_13G = DATA_INTERNAL / "edgar/first_stage_fixed_problem_files_13G"
EDGAR_HTML_FILES_13G = DATA_INTERNAL / "edgar/html_files_13G"



DUMPS_DIR = DATA_INTERNAL  / "pogram_data_dumps"
LTS_IMPROVED = DATA_INTERNAL / "lts_improved_data_set"

STATISTICS_DIR = ROOT_DIR / "statistics"
EXCEL_FILES_DIR = ROOT_DIR / "output_ExcelFiles"


# Labels
NAME_OF_SUBJECT_COMPANY = "NameOfSubjectCompany"
CIK_SUBJECT_COMPANY = "CIKSubjectCompany"
COMPANY_IN_SNP500 = "CompanyInSNP500"
NAME_OF_FILING_COMPANY = "NameOfFilingCompany"
CIK_FILING_COMPANY = "CIKFilingCompany"
FILING_DATE = "FilingDate"
FILENAME = "Filename"
NR_OF_OCCURENCES_ITEM2_DOT = "NrOfOccItem2Dot"
NR_OF_OCCURENCES_ITEM3_DOT = "NrOfOccItem3Dot"
NR_OF_OCCURENCES_ITEM4_DOT = "NrOfOccItem4Dot"
NR_OF_OCCURENCES_ITEM5_DOT = "NrOfOccItem5Dot"
INDICES_OF_ITEM2_DOT_HITS = "i_item2"
INDICES_OF_ITEM3_DOT_HITS = "i_item3"
INDICES_OF_ITEM4_DOT_HITS = "i_item4"
INDICES_OF_ITEM5_DOT_HITS = "i_item5"
TEXT_ITEM2 = "TextItem2" 
TEXT_ITEM4 = "TestItem4"
BD = "BrokerDealer" 
BK = "Bank"
IC = "InsuranceCompany"
IV = "InvestmentCompany"
IA = "InvestmentAdviser"
EP = "EmployeeBenefitPlanOREndowFund"
HC = "ParentHoldingCompany"
SA = "SavingsAssociation"
CP = "ChurchPlan"
CO = "Corporation"
PN = "Partnership"
IN = "Individual"
OO = "Other"

NR_OF_OCCURENCES_IDENTANDBACKGR = "NrOfOccIdentAndBackgr"
NR_OF_OCCURENCES_PURPOFTRANSACT = "NrOfOccPurpOfTransact"
AMENDMENT_TEXT = "AmendmentText"
AMENDMENT_POSITION = "AmendmentPosition"
WARNINGS = "Warnings"
