# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 20:10:32 2020

@author: Raphael
"""
import pandas as pd
import pickle
from Definitions import EXCEL_FILES_DIR, DUMPS_DIR
from math import isnan
import datetime

def getDataFrameFromDict(data_dict):
    return pd.DataFrame.from_dict(data_dict)


def dumpObject(object_to_be_dumped, filename):
    with open(DUMPS_DIR / (filename + ".pickle"), "wb") as file:
        pickle.dump(object_to_be_dumped, file)


def loadObject(filename):
    with open(DUMPS_DIR / (filename + ".pickle"), "rb") as file:
        loaded_object = pickle.load(file)
    return loaded_object

def getCurrentTimeAsString():
    return datetime.datetime.today().strftime("%H:%M:%S")

def writeDataFrameToExcel(pDataFrame, name):
    print(f"Start writing data from dataframe to file {name}.xlsx.")
    print(f"Starting time: {getCurrentTimeAsString()}")
    pDataFrame.to_excel(EXCEL_FILES_DIR / (name + ".xlsx"))
    print(f"Finish time: {getCurrentTimeAsString()}")


def loadExcelIntoDataFrame(filename):
    return pd.read_excel(filename)

def loadEdgarFileIntoDataFrame(filename):
    return pd.read_csv(filename, sep = "\t")
    

#https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()
    
