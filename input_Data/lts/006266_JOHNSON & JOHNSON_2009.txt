CHAIRMANS LETTER
To Our Shareholders

As I reflect on the year that just passed,
and the decades since the founding of
Johnson & Johnson in 1886, I can say without
hesitation that the year 2009 was one of the
most challenging in our history. Faced with
significant patent expirations worth nearly
$3 billion in sales, the most severe global
economic downturn many of us have ever
experienced and increased competition
across our markets, our people delivered
results that were as impressive as any we
have ever achieved. Fundamental beliefs
embedded in Our Credo guided their
decisions and actions.

We delivered on our financial commitments,
continued to pursue long-term growth
opportunities, and emerged stronger and
well-positioned for sustainable growth.
Through all this, the people of
Johnson & Johnson remained inspired and united by a common
purpose: caring for others. More than ever, we know that caring
for the health and well-being of people is not only an outstanding
business but a mission that truly touches lives.

2009 Results When 2009 began, we set expectations for financial
results anticipating the business and economic challenges,
including a forecast of our first reported sales decline in 76 years.
Thanks to the diligence of our people and disciplined management
focus, 2009 results were at or above most expectations.
Worldwide sales were $61.9 billion, a decrease of 2.9 percent
from 2008. Operational results declined 0.3 percent, and the

negative impact of currency was 2.6 percent.
Adjusted earnings were $12.9 billion1, and
adjusted earnings per share increased
1.8 percent1. We also generated free cash flow
of approximately $14.2 billion2. Achieving
these results in a year when operational sales
were essentially flat reflects outstanding
efforts by our leadership teams to manage
their businesses and contain costs.
During 2009, Johnson & Johnson
delivered a total shareholder return of
11.3 percent. This was a strong performance,
although a lower rate of return for one
year than some of our comparative indices.
Over two and three years, we outperformed
the Dow Jones Industrial Average, the
Standard & Poors 500 and other drug and
health care indices. This reflects solid performance
during the recent market downturn,
when we retained more relative value than these indices. Over the
longer term, Johnson & Johnson continued to outperform most
stock indices in total shareholder returns.
2 0 0 9 H i ghl i ghts While managing short-term challenges,
we took important steps for sustainable growth and an expanded
leadership position in health care.
 We strengthened core businesses and invested in the launches
of a number of recently approved innovative products. R&D
investments from the last several years are coming to fruition
in exciting and meaningful ways, and in 2009 we invested
another $7 billion in R&D.

 In addition to advancing pipelines through internal development,
we continued to acquire, invest in and collaborate
with other companies to generate new platforms for growth.
Since the beginning of 2008, we have made eight major acquisitions
and invested in several strategic transactions. Recent
highlights include the acquisitions of Cougar Biotechnology, Inc.
for oncology, and a key compound from Elan Corporation, plc
for Alzheimers disease. We formed strategic alliances with
Crucell NV for vaccines and Gilead Sciences, Inc. for HIV
therapies. In 2010, we acquired Acclarent, Inc. for minimally
invasive sinus surgery.
 We also continued to expand our global presence, including
building operations and expanding our reach in the BRIC
countriesBrazil, Russia, India and Chinaand other
fast-growing developing markets.
 And we continued to play a role in helping to shape health
care policy around the world, given our broad perspective on
the sector.
The future of health care is promising and exciting. It is not,
however, without ongoing challenges that must be addressed.
Development costs are increasing. Changes in patient and consumer
behaviors, as well as constrained health care budgets, are
lingering effects from the economic downturn.
As we assessed this evolving global environment in 2009, we
concluded that restructuring our organization was needed to
ensure sustainable growth. This included the necessary plan to
eliminate approximately 7,500 positions, by far the most difficult
decision of the past year. However, our actions will increase
efficiency and make additional resources available for investment
in long-term growth platforms and new product launches. We
began implementing these restructuring plans shortly after our
announcement in November 2009 and are continuing in accordance
with the required consultation procedures in each market.

Moving Ahead Every difficult period brings with it a corresponding
opportunity for growth. Despite a challenging year, we are
stronger today than we were a year ago. We have outstanding new
products, robust pipelines and talented people working in a
streamlined organization with more resources for growth.
In addition to our strengths, we see favorable trends in the
health care market:
 The global health care market is expected to grow almost
5 percent per year over the next five years. With our broad base
of businesses, we participate in about one-third of this overall
market. And we are focused on some of the fastest growing segments
in health care. In fact, many segments where we compete
are growing as fast as, or faster than, the overall market.
 Many nations, such as China, India and Brazil, are increasing
access to care for their citizens. While progress is still under
way, there are sizable growth opportunities for companies like
Johnson & Johnson that are deeply immersed in these markets
and the health care needs of their people.
 And finally, our expertise and business strategies are aligning
with many evolving trends in health careincluding
personalized medicine, comparative effectiveness, wellness
and prevention, companion diagnostics and biomarkers
many of which you will read about in the following pages.
Of course, we must meet additional challenges ahead. The
effects of the economic downturn, such as high unemployment
rates and decreased access to health care, shrinking hospital
budgets and avoidance of out-of-pocket costs for discretionary
health care purchases, present hurdles for our industry. These,
coupled with the rising costs of regulatory requirements
such as larger and more costly clinical trialscreate a dynamic
health care environment that requires disciplined action
for success.
I believe that the brightest and most innovative health care
companieswith dedicated, focused people who care about the
business of caringwill thrive in this evolving and still-changing
environment. Johnson & Johnson is one of these companies.

Growth Priorities Johnson & Johnson has tremendous assets
for growth: our people, products, pipeline and global presence.
At the foundation is Our Credo, a common set of values unifying
our approximately 115,500 people around the world, and an
operating model that has served us well for decades. Our unwavering
operating model includes a commitment to being broadly
based in health care, a decentralized management approach that
keeps our people close to customers, managing for the long term
and a focus on people and values. Our businesses rely on Our
Credo and our operating model to provide a consistent framework
for decision-making while leaving specific strategies to local
business or franchise leaders, who are closest to the customer.
Within our strategic framework we galvanize our organization
around high-level business priorities that reflect the changing
global environment. These provide leaders with a common set of
growth priorities. For 2010, these include:
 Innovative Products: Our growth has always been based on
scientific innovations that serve unmet patient and customer
needs in a meaningful way. This has created market leadership
positions in many of our businesses. We will stay focused on
bringing forth innovative, accessible and effective products
and entirely new business modelsthat address the most
prevalent health care needs.
 Robust Pipelines: Johnson & Johnson has one of the most
robust product pipelines in our history. The multitude of
opportunities necessitate that we target, invest in and manage
its development. A mix of internal and external sources will
sustain a flow of new products that provide a competitive
advantage. We fully expect the new products coming from
todays pipeline to accelerate the proportion of our sales driven
by newer products.
 Global Presence: As a global health care leader, we must
continue to expand our presence and execute strategies in an
appropriate way for diverse markets and customers. Our
approach will be strategic, effective and cost-efficient to
address local needs. This may mean relating to customers in
new ways, tailoring product innovation to market needs or
building health care capacity. For Johnson & Johnson, this
also means a special focus on high-growth emerging markets
such as the BRIC countries.
 Talented People: The hallmark of Johnson & Johnson is our
talented people. They are passionate about winning in the
marketplace and making a difference in peoples lives. I believe
we have some of the best talent in the health care industry.

Our ability to develop, challenge, motivate and reward a diverse
workforce is our cornerstone for sustained growth.
The people in our Medical Devices and Diagnostics, Pharmaceuticals
and Consumer segments have consistently delivered
against plans for growth. These segments are each market leaders,
with No. 1 or No. 2 positions in many of their businesses. In fact,
70 percent of sales are from products with leading market share
positions, with approximately one-fourth of sales last year coming
from new products introduced in the past five years.

Medical Devices and Diagnostics The Medical Devices and
Diagnostics (MD&D) franchises comprise the worlds largest
medical technology business, with 2009 sales of $23.6 billion,
an increase of 4.2 percent operationally. Four of the seven
franchises had solid sales gains during the past year. Tougher
competition for drug-eluting stents and tighter out-of-pocket
spending on products like contact lenses and diabetes test strips
pressured sales in our Cordis Corporation, Diabetes and Vision
Care franchises.
Growth products spanned a range of treatment categories,
including wound care products and biosurgicals from Ethicon,
Inc.; energy technology and the REALIZE Adjustable Gastric
Band-C from Ethicon Endo-Surgery, Inc.; artificial joints, spine
and sports medicine products from DePuy, Inc.; and new products
from Ortho-Clinical Diagnostics, Inc.
Several products introduced new standards of care for the
medical devices industry. CARTO 3, from Biosense Webster, Inc.,
gives physicians a detailed three-dimensional view of the heart so
they can treat cardiac arrhythmias, including atrial fibrillation.
The SURGIFLO Hemostatic Matrix Kit, our advanced flowable
hemostat for use in a broad range of surgical procedures, is the
first product launch from the acquisition of Omrix Biopharmaceuticals,
Inc. and an example of technology resulting from the
combination of our medical device and biologics expertise.
Our Vision Care franchise continued the global rollout
of 1Day ACUVUE TruEye, the worlds first daily disposable
silicone hydrogel contact lens and an exciting breakthrough in
contact lens technology. We anticipate introduction in the
U.S. in 2010.
MD&D also strengthened its portfolio through several recent
strategic acquisitions. These included Acclarent, Inc. in the ear,
nose and throat surgical space; Finsbury Orthopaedics, Ltd. in
hip implants; and Gloster Europe, a developer of innovative
area-decontamination technologies to help prevent health careacquired
infections, a growing global concern.
The pipeline is strong with promising new products such as
SEDASYS System, the first computer-assisted personalized
sedation system, and the PINNACLE CoMplete Acetabular Hip
System, the first ceramic-on-metal hip replacement. Both products
received favorable recommendations from U.S. Food and Drug
Administration (FDA) Advisory Committees in 2009.
In addition to new product introductions and robust pipelines,
MD&D continually expanded its global reach, particularly in
emerging markets, with research and development centers,
professional training centers and manufacturing facilities.

Pharmaceuticals Our Pharmaceuticals segment, with sales
of $22.5 billion, represents the worlds seventh largest pharmaceutical
business and fourth largest biotech business. The segment
experienced an operational sales decline of 6.1 percent in 2009,
reflecting the loss of nearly $3 billion in sales due to losing market
exclusivity for RISPERDAL (risperidone) and TOPAMAX
(topiramate). Excluding the impact of generic competition,
pharmaceutical sales increased by approximately 7 percent
operationally.
This growth was driven by larger products, including
REMICADE (infliximab), for the treatment of a number of
immune-mediated inflammatory diseases; CONCERTA
(methylphenidate HCl) Extended-release Tablets in attention
deficit hyperactivity disorder (ADHD); and RISPERDAL
CONSTA (risperidone) Long-Acting Injection, an atypical
antipsychotic administered every two weeks for the treatment
of schizophrenia or the maintenance of bipolar 1 disorder.
Promising newer products continued their positive growth
trajectory, such as PREZISTA (darunavir) in HIV; VELCADE
(bortezomib), for multiple myeloma, developed in partnership
with Millennium: The Takeda Oncology Company (we have rights
outside the U.S.); INVEGA (paliperidone), a once-daily atypical
antipsychotic for the treatment of schizophrenia or acute
schizoaffective disorder; and INTELENCE (etravirine), for
HIV combination therapy.
Our pharmaceutical pipeline is one of the most robust in our
history. We launched five newly approved drugs in 2009:
SIMPONI (golimumab) and STELARA (ustekinumab) in
immunology; NUCYNTA (tapentadol) Immediate Release
Tablets for pain relief and INVEGA SUSTENNA (paliperidone
palmitate) for the treatment of schizophrenia; and PRILIGY
(dapoxetine) in select countries across the world in sexual health.
In addition, we continue to expand our core products with new
indications, a practice we have done well historically. For example,
REMICADE now has 15 FDA-approved indications across a broad
spectrum of immune system disorders.
Our future pipeline is promising. An important product in
registration is rivaroxaban, which we are co-developing with Bayer
HealthCare AG. Rivaroxaban is a novel oral anticoagulant that
may prevent a host of thrombotic conditions, including venous
thromboembolism and stroke in atrial fibrillation. It is being
evaluated in five different indications. And we have important
compounds in Phase III clinical trials, including treatments for
diabetes, prostate cancer and Alzheimers disease.
Building on our already strong pipeline, we engaged in
acquisitions and innovative agreements and collaborations with
companies that offer potentially significant advances in patient
care. These include a potential first-in-class treatment for slowing
the progression of Alzheimers disease (Elan Corporation, plc);
a potential universal monoclonal antibody product for the
treatment and prevention of influenza (Crucell NV); an HIV
therapy with a single combination pill (Gilead Sciences, Inc.);
and a potential breakthrough in prostate cancer (Cougar
Biotechnology, Inc.).
While strengthening its pipeline, our Pharmaceuticals business
expanded geographically, with a focus on emerging markets. We
have been expanding our sales reach in China; maintaining a
strong manufacturing footprint in China, Mexico and Brazil; and
developing our R&D presence in emerging markets like India and
China by establishing an R&D operation in Mumbai, an R&D headquarters
in Shanghai and a collaboration with Tianjin Medical
University Cancer Hospital on biomarker research.


Consumer Our Consumer business
continues to distinguish itself with sciencebased
innovation, proprietary technologies
and recommendations by health care professionals.
We are the premier consumer health
care business. Over a billion people around
the world count on our consumer products
for themselves and their families.
In 2009, Consumer sales were $15.8
billion, growing 2 percent operationally
despite a soft economy. Operational sales
increased in the Skin Care, Womens Health,
Oral Care and Wound Care franchises, with
growth in several product lines, including
NEUTROGENA, AVEENO, LISTERINE and
SPLENDA. Innovation in iconic brands is a
cornerstone of our Consumer business. In
2009 we launched LISTERINE TOTAL CARE
in the U.S. and AVEENO NOURISH+ Hair
Care, among others.
We also developed new ways of doing
business. Noteworthy developments include the opening of
the first NEUTROGENA store, in Mumbai, India; the expansion
of skin iD, an online personalized acne solution sold directly to
consumers; and the selling of products on home shopping channels.
More than half of Consumer sales come from markets outside
the United States. Deep consumer insights into local markets and
relevant product introductions drive global expansion, particularly
in developing markets. Recent acquisitions continue to fuel
global growth, such as the thriving DABAO brand in China, Vania
Expansion SNC in Europe and LE PETIT MARSEILLai S, expanding
beyond its French heritage into new markets.

The Changing U.S. Health Care Landscape The health care
landscape has changed at an unprecedented pace over the past
few years and will continue to evolve.
As the health care debate unfolds in the United States and other
markets, Johnson & Johnson supports reform that expands access
to care, improves the long-term sustainability of the U.S. health
care system and builds on the best aspects, including incentives for
medical progress. We believe that appropriate reforms can both
improve patient care and create growth opportunities for health
care companies.
Johnson & Johnson is well-positioned for a changing landscape.
We remain focused on adding value to the health care system,
developing programs that promote wellness and creating better
solutions for chronic care. We remain true to who we are,
motivated by the goal of providing meaningful innovations that
benefit society.

A Citizen of the World We care in ways that can change the
world. Beyond our medical breakthroughs, we are committed to
the people and causes that need our support. We work with hundreds
of partners worldwide to make life-changing and sustainable
differences in health care. When natural disasters strike, such as
the earthquake in Haiti, we work with our partners to provide cash
as well as products from across our businesses.
Our philanthropic efforts globally focus on saving and improving
the lives of women and children, building health care capacity and
preventing diseases. At Johnson & Johnson
companies worldwide, our people make a
difference in their local communities.
Putting others first reminds us that
Johnson & Johnson people can transcend
whatever challenges come our way.
As world citizens, we continually
make strides to enhance the sustainability
of our facilities and packaging. This year,
our annual report is more environmentally
friendly. We have adopted 100 percent
post-consumer recycled paper for the inside
pages, reduced the number of printed pages
and developed an online version, accessible
at www.investor.jnj.com/2009annualreport.
We invite you to join our sustainability
efforts by electing to receive next years
annual report and shareholder materials
electronically. A tear-off card at the end
of this report provides easy-to-follow
instructions.

Our Commitment To You To you, our shareholders, I commit
that Johnson & Johnson will be a leader in health care. We have led
many firsts for the health care industry: the first antiseptic
bandages, the first soft disposable contact lenses and the first
coronary stent, among others. And we will continue this leadership
tradition well into the future. We are a company that is deeply
committed to what we do and to the people we serve.
Our enduring business model, the strength of our current
products, pipelines that have never been more robust, a strong
balance sheet and a growing global presence provide the critical
ingredients for sustained leadership. Most important, the people
of Johnson & Johnson carry on the Companys legacy with an
inspiration of caring that passes from generation to generation
of employees.
For all these reasons, I remain confident that Johnson & Johnson
will lead in health care, drive important innovations for customers
and patients, and achieve long-term, superior rates of return for
you, our loyal shareholders.
William C. Weldon
Chairman, Board of Directors, and Chief Executive Officer
March 17, 2010
