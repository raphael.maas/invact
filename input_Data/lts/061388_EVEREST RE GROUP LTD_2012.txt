A Letter to our Shareholders
We are pleased to report that, in 2012, Everest increased book value
per share, adjusted for dividends, by 18%. This result was achieved
despite the impact of Superstorm Sandy, record crop losses, and declining interest rates. And it demonstrates the strength and diversity of
our organization.
As a top tier reinsurer with global access and multi-line capabilities,
Everest commands significant market presence and is able to garner
opportunities not readily available to smaller, less diversified competitors. Having more than forty years of experience in the risk business,
Everest has developed strong underwriting prowess and a reputation
of providing responsive and innovative solutions to its clients. Building
on these strengths, we have increased our investment in both people
and technology to develop �best in class� data-driven risk management
systems to improve portfolio optimization and risk accumulations
across all business activities.

The results of 2012 are the product of a clear strategic vision
that continues to focus all activities on building long-term value
for our shareholders. The ever changing dynamics of our operating environment require responsive, informed strategies and
there are various levers at our disposal�underwriting, investments, and capital management�to achieve this goal. The creation of shareholder value, which represents growth in book
value per share plus dividends paid to shareholders over time,
is how we measure the ultimate success of our strategies. And
since our entry into the public market in 1995, Everest has
increased shareholder value at a compound annual rate of 13%,
which we believe is a testament to the strength and effectiveness of our operating strategies.
We remain committed to achieving double digit returns, despite
a challenging macroeconomic environment. Our strategy is to
optimally position the underwriting portfolio to achieve
improved profitability, which can mitigate any slowdown in
investment income. Our earnings power was evident with 2012
results, as Everest absorbed one of the Top 5 all-time industry
losses, with Superstorm Sandy, and yet still had one of its best
years on record.
Underwriting�A Differentiator
The property-casualty market is considered a cyclical business,
and while that may be true, what really matters is a company�s
ability to measure the relative return on the risk it is assuming,
irrespective of the point in the cycle. Risk selection and pricing
are the underwriting differentiators in our market, which are
borne out by the underwriting results a company reports.
During 2012, Everest posted a combined ratio of 93.8% and
generated underwriting income of $259 million despite catastrophe losses of $361 million, net of reinstatement premiums.
Stripping away catastrophe losses and the small amount of
favorable prior year loss reserve development, our attritional
combined ratio improved 3 points, year-over-year, to 85%.
Said another way, that is 15 points of potential profit being
thrown off by the $4.2 billion of earned premium on our underwriting portfolio. That is powerful and provides a significant
cushion to absorb potential catastrophe losses in any given
year. The basic tenet of our underwriting philosophy has not
changed. We seek to achieve an adequate return on the risk
capital we deploy. Towards that end, we continually refine and
improve upon the techniques we use to price, measure, and
monitor risk. We have added underwriting and actuarial
resources to our already strong bench, developed extensive
data-driven analytical tools, and reached out to new markets
and relationships.
Driving Returns
During 2012, we rebalanced the reinsurance portfolio towards
excess of loss property business, where we were seeing the
greatest improvement in risk-adjusted returns. Today, 48%
of our reinsurance property portfolio is written on an excess of
loss basis as compared to 36% a year ago, without any marked
reduction in gross written premium. We were able to write more
business and spread our catastrophe aggregate accumulations 

outside of peak zones, thereby improving the diversification of
the portfolio and the overall return on this business. Focused
marketing efforts have also strengthened our broker relationships and broadened our customer base, which has resulted in
the development of new products tailored to meet the underserved needs of specific markets, both on the property and
casualty side. As a result of these initiatives, the current year
attritional combined ratio for the reinsurance segments
improved 2.6 points to 80.9% for 2012.
Moving on to our primary insurance business, we have been
working towards reshaping this book over the last several years
to move it to underwriting profitability. Historically a casualty
book of business, largely derived from the MGA (managing general agent) distribution network, we have repositioned this
book towards shorter-tail, specialty focused products with an
increased emphasis on direct distribution through our own network of offices. To jumpstart our strategic repositioning of this
book, in 2011 we acquired Heartland, a crop insurance agency,
which added $275 million of gross written premium. We also
began writing A&H primary business, which we had previously
written on a reinsurance basis, and we expanded product offerings in other short-tail lines. Combined, short-tail business now
represents 45% of the insurance book from 22% in 2010. At the
same time, we terminated underperforming books of MGA
business and began pushing for rate increases on our California
workers� compensation book. While many have since joined us
in the fight for rate in this market, we were one of the first to
start this effort and, since 2009 we have achieved compounded
annual rate increases of 55%, well in excess of loss cost trends.
The fruits of our labor are finally starting to show through.
While we did see some prior year loss development in 2012 on
run-off construction liability and workers compensation business, we continue to note improvement in the underlying
current year attritional ratios. Setting aside the underwriting
loss on the Heartland crop book, due to the record drought
conditions in the U.S., the current year attritional combined
ratio for the insurance segment was 97.2% for 2012, an 8.1 point
improvement when viewing 2011 on a comparable basis.
For 2013, we have a number of new initiatives underway on
both the reinsurance and insurance side to further push the
momentum we are seeing from the strategic actions taken thus
far. During the January renewals, we wrote an additional $150
million of property reinsurance premium, aided by the introduction of a new innovative product, while maintaining our
net peak zone 1-in-100 PML at approximately 10% of shareholders� equity, after reinstatement premiums and taxes. Also, as
recently announced, we have established a capital markets platform, Mt. Logan Re, which will enable us to generate fee
income and provide additional catastrophe capacity to the marketplace without increasing the risk exposure to Everest. Given
our well-balanced portfolio, Everest should continue to generate
strong, and improving, underwriting results for 2013, assuming
a normalized level of catastrophes, which should continue to
outpace any contemporaneous decline in investment yields
that could otherwise reduce our overall returns.
Investment Headwinds
During 2012, Everest had $600 million of investment income,
which represented 77% of our pre-tax operating income. As
this suggests, investment returns are an important lever in
driving the overall returns of the organization. With $16.6 billion of cash and investments and persistently low reinvestment
rates, we spend quite a bit of time thinking about our asset
allocations.
Beginning in 2009, we began to dynamically alter the allocation of our assets in response to declining yields and sovereign

credit quality concerns. Our focus, similar to our underwriting
strategies, has been to maximize returns while minimizing
risk. Since that time, we have allocated a position to alternative
investments, including high-yield bonds and short-duration
bank loans, as well as a position to equities; the latter focused
on dividend paying stocks of high-quality corporates. We continue to maintain a high quality portfolio, with an average credit
rating of AA-, but we have shortened the duration to 3 years.
These actions reduced pre-tax interest rate risk by $394 million
since 2009, assuming a 200 basis point increase in interest
rates, and provided for improved overall investment returns.
The total return on our investment portfolio was 5.8%, benefiting from strong investment returns on alternative investments
of 10.7% and equities of 13.8%.
Yield spreads continued to contract for investment grade and
high-yield fixed maturity issues during 2012, which will continue to pressure investment income going forward. At some
point though, interest rates will rise and we have prudently
positioned our portfolio to take advantage of this event, when
it occurs.
Maintaining Strength
With its high-quality investment portfolio and solid reserve
position, Everest�s balance sheet and the financial security it
offers its clients remains its hallmark. Efficient capital management is key to balancing the needs of our various constituents
and we work diligently to keep these in check while preserving
our financial strength. Each of the rating agencies�A.M. Best,
Standard and Poor�s, and Moody�s�have done their independent assessment of the sustainability of the Everest franchise
and have consistently awarded us high ratings.
We are prudent managers of capital. With $6.7 billion of shareholders� equity, we do maintain an excess capital position,
which provides us one of the strongest balance sheets in the
industry. This is an important advantage as it allows us to
respond aggressively into the market should the unexpected
occur. Given the complex and evolving nature of risk, capital
management is crucial to protecting the franchise.
Nevertheless we recognize our fiduciary responsibility to our
shareholders to return underutilized capital, particularly when
our stock is trading at its current levels. Since 2006, we have
repurchased 23% of our outstanding shares, returning $1.4 billion of capital to shareholders. During 2012, we returned $390
million of capital to shareholders through dividends and share
repurchases. While it had been our intent to repurchase a great
deal more, Superstorm Sandy kept us out of the market for
much of the last quarter of the year. For 2013, we will continue
to take advantage of this buying opportunity as we believe our
stock remains undervalued, despite the run-up our shareholders enjoyed in 2012.
Indeed, 2012 was a good year for Everest�s shareholders as ownership of the stock provided a total return to shareholders, a
measure that reflects the combination of stock price appreciation and dividends, of 33.3%, which was more than twice that
provided by the S&P 500. Over the last five years, a difficult
period for the financial services industry, Everest�s shareholders
benefited from a 22.6% return on its shares, outperforming the
S&P 500 return of 8.6% by a wide margin, over this same period.
While the share price is a reflection of the relative change in
book value, it is also a reflection of the investor�s confidence of
the earnings power and balance sheet strength of the company.
Over the last several years, we have been actively communicating our strategy for improved returns and have increased the 


transparency of our reporting, in particular for loss reserves,
which is an important element of the balance sheet. This year,
we issued the second publication of our annual loss reserve triangles. Our investors and analysts welcomed this information as
they are able to gain insights into our reserving and form their
own opinion on the strength of our reserve position.
We were encouraged by the increased confidence the market
had shown Everest in 2012. While book value per share rose
16% during the year, Everest�s share price climbed more than
30%, indicating the increasing investor confidence in the market
and Everest.
In Closing�
Shareholder value is created when a Company continually
strives to build on its strengths. Everest�s strengths are defined
by its outstanding people, broad product capabilities, global
franchise, strong client relationships and robust balance sheet.
Putting this all together, we are excited about our prospects for
2013. We are also confident in the longer term future of Everest,
due to its dedicated team of people focused on building the
financial value of this Company.
Our success can also be attributed to our Board, whose experience has been invaluable in leading this Company. During
2012, we had two of our long-standing directors retire�Ken
Duffy and Marty Abrahams�and we thank them for their many
contributions over the years. Joining the Board was John Amore,
who brings a wealth of industry knowledge to augment the
diversified talents of our remaining Board members.
We thank our Board, our employees, and our shareholders for
their continued support.
Sincerely,
Joseph V. Taranto
Chairman and Chief Executive Officer
Dominic J. Addesso
President 