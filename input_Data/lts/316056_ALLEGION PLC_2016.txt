Dear Shareholders,
The Allegion team delivered a strong year in 2016.
Our people drove industry-leading organic growth, and expanded adjusted operating margins in all
business regions. We significantly increased our cash flow while providing excellent shareholder
returns since our spin-off. In fact, 2016 was our most profitable year as a stand-alone company. Im
proud of our accomplishments, and even more excited about the future. Our industry is on the cusp
of major technical changes including new levels of connectivity and convenience. Allegion is at the
forefront of delivering forward-thinking solutions that link function, safety and design, in order to
keep people, places, and their property secure.
Allegion is a leader in electro-mechanical convergence and innovation, which we believe will drive
growth globally for the security industry in the coming years. We complement mechanical security
with electronic solutions, creating an optimized balance that helps customers address their unique
security needs, codes and cost constraints, while also gaining the benefits of technology. One
measure of the success of this strategy is our vitality index, which is an indicator of Allegions
capability to generate revenue from new product innovations. Since 2013, Allegions vitality index
has tripled, and we have a goal to hit more than 25 percent by 2019. Electronics and connected
solutions are a growing part of our portfolio that are demanded by a connected world.
Allegions key strategies
Allegions board of directors and management team refreshed our overall business strategy in
2016. Key observations reflect continued industry consolidation, the electro-mechanical
convergence, the importance of enterprise excellence, and the critical requirement of disciplined
capital deployment. Allegions performance since its creation has been noteworthy; we
believe our vision, purpose, values and strategic growth pillars are sound. Disciplined capital
deployment is demanded by our shareholders and top of mind for our team.
Keys to the future
Looking ahead, I am optimistic.
Fundamentally, overall market conditions remain favorable, and provide opportunities for Allegion
solutions around the globe. We see new infrastructure investments in core markets. Institutional
demand is strong, both for new construction and retrofit. Our renowned product portfolio and new
products, guided by specifying teams, position us for growth. Residential housing construction
continues to expand and consumer confidence is positive.
A disciplined and focused approach to key vertical market segments allows us to make targeted
investments that drive long-term organic growth and profitability. Allegion has gained market share,
and we expect this trend to continue. And with more than a billion secured door openings in the
Allegions key strategies
Keys to the future
world, Allegion is positioned to leverage the electro-mechanical convergence for decades to come.
Our expertise and focus in the complex security market give us a significant competitive
advantage.
A track record of success, positive end markets, technological trends and a great team give us
confidence in our future.
Team of experts
Our high levels of execution and performance are driven by our most important assets  our people,
coupled with a management system that drives continuous improvement. We have a strong team
of experts who are committed to our vision to make the world safer, securing the places where
people thrive. Our operating system helps us plan for results and holds our team accountable for
delivery of our long-term strategy, investments, quality, customer satisfaction, and employee
engagement. Our people are a highly focused team driving a great business.
Employee engagement and employee safety are high priorities. We measure employee
engagement globally and report our results to the Allegion board of directors. For the third year,
Allegion increased the engagement of our workforce. We also continue to have an exceptional
occupational safety record and are a safety leader in our industry. Our teams engagement,
commitment to safety, and sense of responsibility to our global communities are the foundations of
our culture.
Whats next? The future is bright at Allegion!
We live in an uncertain world. The safety and security of people and property are top concerns of
our customers, and leading economies are demanding investmentall of which provide Allegion
with opportunities. Our 2016 results delivered growth in organic revenue, adjusted operating
margins, shareholder value and the vitality index, as well as significant cash flow; these results
show our potential in the years ahead. The ability to perform above the market, while making
significant investments in our business, demonstrates that Allegions disciplined approach to longterm growth and shareholder value is working.
I love the business and believe in our people. Our culture is developing and strong. Our strategy is
clear; we are focused and committed to profitable growth and shareholder returns. Our future is
bright, and I firmly believe our best is yet to come. Thank you to our board and the global Allegion
team for their commitment to excellence.
My best,
Dave Petratis, Chairman, President and CEO
Allegion plc
Team of experts
Whats next? The future is bright at Allegion!
world, Allegion is positioned to leverage the electro-mechanical convergence for decades to come.
Our expertise and focus in the complex security market give us a significant competitive
advantage.
A track record of success, positive end markets, technological trends and a great team give us
confidence in our future.
Team of experts
Our high levels of execution and performance are driven by our most important assets  our people,
coupled with a management system that drives continuous improvement. We have a strong team
of experts who are committed to our vision to make the world safer, securing the places where
people thrive. Our operating system helps us plan for results and holds our team accountable for
delivery of our long-term strategy, investments, quality, customer satisfaction, and employee
engagement. Our people are a highly focused team driving a great business.
Employee engagement and employee safety are high priorities. We measure employee
engagement globally and report our results to the Allegion board of directors. For the third year,
Allegion increased the engagement of our workforce. We also continue to have an exceptional
occupational safety record and are a safety leader in our industry. Our teams engagement,
commitment to safety, and sense of responsibility to our global communities are the foundations of
our culture.
Whats next? The future is bright at Allegion!
We live in an uncertain world. The safety and security of people and property are top concerns of
our customers, and leading economies are demanding investmentall of which provide Allegion
with opportunities. Our 2016 results delivered growth in organic revenue, adjusted operating
margins, shareholder value and the vitality index, as well as significant cash flow; these results
show our potential in the years ahead. The ability to perform above the market, while making
significant investments in our business, demonstrates that Allegions disciplined approach to longterm growth and shareholder value is working.
I love the business and believe in our people. Our culture is developing and strong. Our strategy is
clear; we are focused and committed to profitable growth and shareholder returns. Our future is
bright, and I firmly believe our best is yet to come. Thank you to our board and the global Allegion
team for their commitment to excellence.
My best,
Dave Petratis, Chairman, President and CEO
Allegion plc
world, Allegion is positioned to leverage the electro-mechanical convergence for decades to come.
Our expertise and focus in the complex security market give us a significant competitive
advantage.
A track record of success, positive end markets, technological trends and a great team give us
confidence in our future.
Team of experts
Our high levels of execution and performance are driven by our most important assets  our people,
coupled with a management system that drives continuous improvement. We have a strong team
of experts who are committed to our vision to make the world safer, securing the places where
people thrive. Our operating system helps us plan for results and holds our team accountable for
delivery of our long-term strategy, investments, quality, customer satisfaction, and employee
engagement. Our people are a highly focused team driving a great business.
Employee engagement and employee safety are high priorities. We measure employee
engagement globally and report our results to the Allegion board of directors. For the third year,
Allegion increased the engagement of our workforce. We also continue to have an exceptional
occupational safety record and are a safety leader in our industry. Our teams engagement,
commitment to safety, and sense of responsibility to our global communities are the foundations of
our culture.
Whats next? The future is bright at Allegion!
We live in an uncertain world. The safety and security of people and property are top concerns of
our customers, and leading economies are demanding investmentall of which provide Allegion
with opportunities. Our 2016 results delivered growth in organic revenue, adjusted operating
margins, shareholder value and the vitality index, as well as significant cash flow; these results
show our potential in the years ahead. The ability to perform above the market, while making
significant investments in our business, demonstrates that Allegions disciplined approach to longterm growth and shareholder value is working.
I love the business and believe in our people. Our culture is developing and strong. Our strategy is
clear; we are focused and committed to profitable growth and shareholder returns. Our future is
bright, and I firmly believe our best is yet to come. Thank you to our board and the global Allegion
team for their commitment to excellence.
My best,
Dave Petratis, Chairman, President and CEO
Allegion plc