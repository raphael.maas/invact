BY YEAR-END 2003, CABLEVISION HAD CHARTED A COURSE
for the companys future. After years of dedicated development, we completed
the rebuild of our extensive broadband network, creating one of the best
and most advanced telecommunications systems in the world. This accomplishment
makes Cablevision one of the only companies able to offer all its
innovative services to every customer in its footprint.
The completion of our rebuild also brings to an end an era of significant capital
investment. Now, we are able to turn our full attention and resources to
producing steady returns by making incremental investments in revenuegenerating
services. Today, Cablevisions customer bundle includes the
companys new Optimum Voice digital voice-over-cable service, as well as
our iO: Interactive Optimum digital video and Optimum Online high-speed
Internet services, both of which grew at impressive rates in 2003.
Solidifying Cablevisions role as a leading provider of high-definition (HD)
programming this past year was Rainbow DBSs launch of VOOM, a television
satellite service to meet the demands of todays rapidly growing, but
underserved, HDTV audience.
We also began laying the groundwork for new investment opportunities for
our shareholders. In late October, we unveiled a plan to create two distinct
companies, each with defined business plans and clear investment characteristics.
Remaining with Cablevision will be the telecommunications and
Lightpath businesses, as well as our New York assets, including Madison
Square Garden. Comprising the new entity will be: Rainbow DBS; three of
Rainbow Medias national entertainment services  AMC, IFC (The
Independent Film Channel) and WE: Womens Entertainment; and certain
other Rainbow businesses. With the spin-off expected to be completed later
this year, we look forward to offering two compelling investment choices that
reflect the sound strategic position of Cablevision and this newest combination
of Rainbow assets.
Cablevision is committed to delivering value to consumers and shareholders
alike as we continue to shape the industry and secure our future with digital
offerings, while capitalizing on the many opportunities that lay ahead.
Theres only one New York, and
Cablevision is serving it better than
ever. Now, 4.4 million homes in the
New York metropolitan area have
complete access to Cablevisions
portfolio of services over our new
40,000-mile fiber-rich network.
These advanced, integrated offerings
simplify life for busy New
Yorkers as they communicate in
the nations most demanding and
attractive business and entertainment
marketplace.
In 2003, our telecommunications
services division, which includes
the companys analog and digital
video, high-speed data, voice and
Lightpath commercial telecommunications
businesses, capitalized
on the success of our superior
network, recording a 12 percent
increase in net revenue.
Meanwhile, the total combined
number of video, data and voice
customers, or revenue-generating
units (RGUs), was 4.9 million, up
25 percent from 2002.
Today, more than 30 percent of
our cable customers receive digital
services, compared with only 7
percent a year ago. This has
helped to make Cablevisions
iO: Interactive Optimum digital
video offering the fastest-growing
service in the companys history.
Ending 2003 with 905,000 customers,
four times as many as at
the end of 2002, iO now offers
customers access to a wide array
of networks and powerful new
technologies, including: 700 titles
of video-on-demand (VOD) content
available at all times; 14 HD
programming services, featuring
the industrys first and only HD
VOD offering; groundbreaking
interactive applications; and international
programming options,
such as iO en Espaol, the industry's
most comprehensive slate of
Spanish-language networks.
Another Cablevision success story,
our Optimum Online (OOL) highspeed
Internet offering continues to
be one of the fastest and most reliable
broadband services in the
nation. OOL ended 2003 with 1.1
million customers, up 37 percent
from year-end 2002. The service also
saw its industry-leading penetration
rate of homes-passed increase to 24
percent by year-end 2003.

Three out of four people in Cablevisions footprint
who subscribe to a high-speed data service are
Optimum Online customers.

Completing the bundle  and capitalizing
on the strong consumer
demand for a competitive and
reliable voice service  is
Cablevisions newest product,
Optimum Voice. Introduced in the
fall and now available across our
entire service area, Optimum
Voice attracted 29,000 customers
by year-end 2003 as we
The Academy of
Television Arts & Sciences
awarded iO a 2003
technical Emmy for
Outstanding Achievement
in Interactive Television.
became the first cable operator
to offer a competitive VoIP service
throughout our footprint.
Given Optimum Voices ease of
use and low cost, we expect customer
numbers to climb significantly
in 2004 with the introduction
of new calling features.
Also holding strong, especially in
the face of lingering softness in
the telecommunications sector,
was Lightpath, which reported a
13 percent increase in net revenue
in 2003. Cablevisions business
telephony and data division also
recorded a 10 percent rise in the
number of buildings on net this
past year.
5
By year-end 2004,
Cablevision's nationally
recognized educational
initiative, Power to
Learn, will bring the
Internet to more than
2,100 schools and
libraries in the companys
service area while
strengthening the homeschool
connection with
powertolearn.com, which
provides educational
content and tools to
teachers, students and
parents throughout the
New York area.
Lightpaths network
demonstrated incredible
resilience on August 14,
maintaining full system
availability for all customers
for the duration of the East
Coast blackout.
Now available to more than 4 million homes-passed in the
New York metropolitan area, Optimum Voice offers unlimited
calling across the United States and Canada, and five popular
calling features for the low, flat monthly rate of $34.95.
Continuing to play an important
role in New Yorkers lives are
Cablevisions entertainment venues,
sports teams, and local and
regional programming networks.
Billboards 2003 "Venue of the
Year," Madison Square Garden,
attracted more than 4 million visitors
to hundreds of events this past
year, including the 45th annual
Grammy Awards, the
Rolling Stones HBO concert
special and The
Z-100 Jingle Ball. In
August 2004, The
Worlds Most Famous
Arena will host the first
Republican National
Convention ever
held in New
York City.
6
Madison Square Garden
remained the worlds premier
entertainment venue in
2003, hosting hundreds of
memorable events, including
The Simon & Garfunkel
Reunion and Chris Rocks
Black Ambition Tour.
VJs Marianela Pereyra and
Dennis Da Menace with
Outkasts Andre 3000 and Big
Boi at Fuses new street-front
studio in midtown Manhattan.
Also at the Garden, and
leading the NBA in ticket
revenue for the 12th straight
year, the new New York
Knicks welcomed two-time
all-star Stephon Marbury,
basketball luminary Isiah
Thomas and the NBAs alltime
winningest coach
Lenny Wilkens, while the
Rangers added top scorer Jaromir
Jagr for the 2003-2004 season.
Meanwhile, the Liberty remained
No. 1 in ticket revenue, with attendance
among the top in the WNBA.
Radio City Music Hall also had a
banner 2003, as more than 1.4 million
visitors passed through its turnstiles
to attend an array of shows,
including the 20th Anniversary of
the MTV Video Music Awards, the
Tony Awards and a run of sold-out
performances of Sinatra: His Voice.
His World. His Way, the first official
live theatrical stage production
featuring the crooners image,
voice and work. Meanwhile, the
Radio City Christmas Spectacular
and world-famous Rockettes continued
to attract audiences across
the country, and A Christmas Carol
celebrated a successful 10th and
final season.
Also making news in 2003 was
Fuse. Formerly muchmusic usa,
the successfully re-branded Fuse
grabbed national attention this
past year for its meteoric rise to
become the nations fastest-growing
cable network in the coveted
12- to 24-year-old demographic,
with more than 29 million viewing
subscribers.
At the same time, Clearview
Cinemas provided added
convenience for New Yorkers
in 2003 with a multi-year,
multimillion-dollar deal with
remote ticketing provider
AOL MovieFone.
Complementing its New York properties
this past year was
Cablevisions local and regional
programming. The companys fivechannel
News 12 Networks received
a host of accolades in 2003, including
10 New York Emmys and 13
National Telly Awards, while providing
even more local news and continuous
updates at a faster pace to
meet the needs of its busy viewers.
With more than 7 million viewing
subscribers, the companys
MetroChannels also marked a
memorable 2003. Awarded six
New York Emmys, MetroTV made
7
Regional Programming significant strides, debuting
two new, original series, while
welcoming back 52 days of Full
Frontal Fashion to MetroStories.
Metro Traffic & Weather also
celebrated a successful year,
launching an all-new traffic mapping
system to remain New Yorks
most reliable source of local traffic
information, with a cumulative
average of 750,000 homes tuning
in each day.
In 2003, MSG Networks cablecast
hundreds of live events in HD and
garnered five Emmys. And the
companys Rainbow Sports
Networks extended carriage of the
NHLs San Jose Sharks and
Columbus Blue Jackets, the NBAs
Cleveland Cavaliers and MLBs
Oakland As. These networks
include five regional sports
channels reaching 19 million
viewing subscribers in San
Francisco, New England,
Florida, Ohio and Chicago,
and a 50 percent interest
in the national Fox
Sports Net.
Rainbow Media Holdings LLC broke
new ground in 2003, expanding its
content offerings as it continued to
develop its popular programming.
Spurred by increases in both advertising
revenue and subscriber numbers,
Rainbows core networks
recorded revenue growth of 24 percent
to $609 million.
Currently in 74 million viewing
homes, AMCs prime-time household
ratings were up 13 percent this
past year. Meanwhile, the networks
new weekly series, Sunday Morning
ShootOut, hosted by entertainment
Mix It Up, a Courteney Cox-David
Arquette creation. In addition, the
network published a book with
Simon & Schuster based on WEs
celebrated series When I Was a Girl,
which features
accomplished
women relating
personal childhood
stories.
Rainbow also expanded
its leadership in the on-demand
arena in 2003. Mag Rack, the companys
suite of on-demand video
magazines, extended distribution to
2.1 million homes and launched
exciting new titles such as Personal
Trainer. In addition, the company
introduced World Picks On
Demand, an SVOD service offering
National Programming
8
New 2003 Mag Rack
titles include
Personal Trainer,
which offers home
workouts in aerobics,
kickboxing, Pilates,
belly dancing and more
for all fitness levels,
24/7.
industry veterans Peter Guber and
Peter Bart, launched in October
2003 to rave reviews. Also noteworthy
for AMC were American
Cinematheque Tributes honoring
Denzel Washington and Nicole
Kidman.
In 2003, IFC (The Independent Film
Channel) marked another successful
year of its signature series
Dinner for Five, announcing a third
season for 2004. In addition, the
network, which now has 30 million
viewing subscribers  up from 26
million in 2002  launched IFC
Films On Demand and Uncensored
On Demand, two popular, new subscription
VOD services. IFC also
unveiled its most ambitious original
programming slate ever for 2004
with eight original productions,
while its documentary release on
1970s American cinema, A Decade
Under the Influence, opened to
critical acclaim.
Reaching nearly 47 million U.S.
households in 2003, WE: Womens
Entertainment premiered its first
original film, Between Strangers
starring Sophia Loren, and
launched a new home-design show,

Hindi, Latino, Mandarin and Russian
programming, and sportskool, the
nations first VOD cable network
dedicated to expert sports and fitness
instruction.
Perhaps the most significant new
endeavor by Rainbow this past year
was the creation of an integrated
suite of 21 brand-new HD channels.
Leveraging Rainbows national and
niche programming expertise, this
content package represents the
largest single source of programming
created exclusively for HDTV.
This suite of 21 channels forms the
core of Cablevisions new HD-centric
programming satellite service,
VOOM. Introduced in October 2003,
this service, managed through
Cablevisions Rainbow DBS satellite
division, delivers more HD programming
than any other satellite or cable
service in the nation. In addition to
more than 30 HD channels, VOOM
features the latest in HD technology.
In 2004, this will include a groundbreaking
digital video recorder capable
of recording and playing back HD
programming.

dented amount of HD programming
is made possible by Rainbow 1.
With its ahead-of-the-curve technology,
this single satellite provides
unparalleled capacity that will double
with the arrival of MPEG-4.
Rainbow DBS is in a unique
position to take advantage
of this compression technology
to maximize the delivery
of its advanced programming
across the continental
United States. This
ability to deliver content
to consumers on
a much wider scale
than ever before is due
to the power of
Rainbow 1 and is reminiscent
of the incredible
opportunities cable
provided in 1973 when
Cablevision began distributing
its content to
outlying neighborhoods
across the nations first
suburban cable system.
As we move forward,
we see a promising
future, with VOOM
as the centerpiece of a
new company entering
an exciting new era.
CONSUMERS DEPEND ON CABLEVISIONS DIGITAL SERVICES
and technologies to improve their daily lives.
Today, Cablevision stands alone in delivering these integral products
across the worlds most advanced network to millions of loyal customers
in the nations most attractive market.
Going forward, as we focus on our core assets and strengthened
financial position, we will continue to provide cutting-edge services
and extend the boundaries of communications. We will also unveil a
new company that honors Cablevisions 30-year heritage as a content
leader, setting the stage for new opportunities.
Thank you to our co-workers, customers, financial partners and
shareholders. It is because of you that consumers will look to
Cablevision for decades to come as we deliver products and services
that continue to entertain, excite and enable.