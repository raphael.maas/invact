Dear Fellow Stockholders:
I would like to welcome you as you review our annual
stockholders report.
Overview
The year 2012 was a ground breaking year for Macerich 
literally. It provides a solid foundation to build on in 2013. Last
year, we broke ground on our 526,000 square foot enclosed,
two-level Fashion Outlets of Chicago project. This center is
expected to open on August 1, 2013, offering one of the most
outstanding fashion line-ups of any new outlet center to open
in the United States in many years. Our anchors, Last Call by
Neiman Marcus, Bloomingdales The Outlet Store, Saks Fifth
Avenue Off 5th, and Forever 21, will be joined by such stellar
fashion retailers as Longchamp, Brunello Cucinelli, Prada,
Gucci, Giorgio Armani, Halston, Michael Kors, Coach, Coach
Mens, Tory Burch and many others. We view Fashion Outlets
of Chicago as a terrific addition to our portfolio, increasing
the Companys presence in one of the largest markets in the
country.
We also broke ground during 2012 on the mixed-use
densification of Tysons Corner Center, which will add 1.4
million square feet to one of the countrys premier retail
centers. The Tysons expansion includes a 524,000 square
foot, 22-story office tower; a 500,000 square foot, 30-story,
430 unit luxury residential tower; and a 17-story, 300 room
Hyatt Regency hotel. We made it clear during the year that
we would not proceed with the office component without
signing a lead anchor tenant. In November of 2012, we were
successful in signing Intelsat, the worlds leading provider
of satellite services, as our lead anchor tenant for the office
tower. Leasing of the remainder of the Tysons office tower is
progressing extremely well and we expect additional anchor
announcements in the near future. It is important to note that
Tysons Corner Center is already one of the top retail centers in
the United States, generating almost $1 billion in annual sales,
and is the dominant center in the Washington D.C. market.
This center is anchored by Nordstrom, Bloomingdales, Lord &
Taylor, Macys and one of the highest volume AMC Theatres in
the United States.
The densification of Tysons Corner Center has been planned
for over ten years and is intended to coincide with the grand
opening of the Metrorail extension that will link Tysons
Corner to downtown Washington D.C. later this year; and
in the near future to Dulles Airport. Northern Virginia and
Fairfax County officials desired a transit-oriented development
surrounding this new important transportation hub and we
were successful in obtaining the entitlements to create exactly
that type of development at Tysons Corner. We believe we
will not only create significant net asset value in each of the
three new towers but, equally important, we will enhance the
productivity of the retail center and demand from retailers
for flagship locations at Tysons. We are confident that upon
completion next year, this development will be one of the
premier projects, possibly the premier project, of its kind in
the United States.
Balance Sheet, Portfolio Management and
Acquisitions
In 2012, we made significant progress in enhancing and
strengthening our balance sheet while also rebalancing our
portfolio through strategic acquisitions and dispositions
of non-core assets. Notably, we successfully extended
the average debt maturities on our balance sheet from
approximately three years to over five years while reducing our
average borrowing costs. The chart on page 4 illustrates this
significant lengthening of our debt maturities. We financed
over $2.4 billion of debt during the year, reducing our average
effective interest rate from 5.0% as of December 2011 to 4.3%
as of December 2012. These transactions produced excess
proceeds of over $1.3 billion, which were used for acquisitions,
developments, redevelopments, reduction of corporate debt
and for general corporate purposes.
On the portfolio management side, we were successful
in disposing of $468 million of non-core assets and this
disposition of non-core assets will continue in 2013. More
importantly, we acquired $1.7 billion of new assets and
additional partnership interests in core assets. In Fall 2012, we
increased our ownership in FlatIron Crossing in Broomfield
($548 sales per square foot) to 100% from our previous 25%
level, and we likewise acquired 100% ownership of Arrowhead
Towne Center ($635 sales per square foot) in Northwest
Phoenix, with the purchase of our partners 33% interest.
We are also very excited about the $1.25 billion acquisition
of Kings Plaza Shopping Center in Brooklyn, New York, completed in November 2012 and the January 2013 acquisition
of Green Acres Mall in Valley Stream, New York. The
acquisitions of Kings Plaza and Green Acres dramatically
increase our investments in the greater New York Boroughs
and regional market. More than 10% of our portfolio net
operating income (NOI) is now generated from the Boroughs
in New York. We have already demonstrated tremendous
success in this market through Queens Center and are
extremely confident about the future success of Kings Plaza as
well as Green Acres in the years to come. These acquisitions
highlight our strong interest in densely populated trade areas
where we can create value through our market position and
organizational resources.
While I am pleased that our average sales reached $517 per
square foot in 2012, I have felt for some time that this twodimensional average does not reveal the true power of our
portfolio. Therefore, we recently started disclosing our sales
per square foot by property and providing specific guidance
illustrating which properties generate our Companys NOI
(see pages 9-10). This supplemental disclosure highlights the
fact that our top 40 centers account for approximately 78%
of our NOI and have average sales per square foot of almost
$600. After our planned dispositions of non-core assets are
completed, we anticipate our top 40 centers will produce sales
of over $600 per square foot and about 85% of our NOI.
Operations
On the operating side, we had a very good year in terms of
funds from operations growth, same center NOI growth,
increased occupancies and leasing spreads. We believe
the fundamentals in the regional mall business are strong.
Our year-end portfolio average sales per square foot were
approximately $517, representing a $50 a foot increase from
the sales per square foot that we enjoyed in 2007 - the first
complete year prior to the severe recession of 2008/2009.
We had strong leasing spreads of 15% in 2012. In addition,
our mall portfolio occupancy increased to 93.8% at December 31, 2012 compared to 92.7% at December 31, 2011.
Although we saw a positive increase over 2011 same center
NOI, our management team is focused on improving last
years 2.9% same center NOI growth. We are specifically
focusing on increasing occupancy levels, converting temporary
leases to permanent leases, and bringing expiring rents up to
market in our improving environment where retail margins
have widened and retailers appetite for growth has returned.
Both U.S. and global retailers are primarily targeting the
United States for their new store strategies because of the
safety and security that they find in our economy compared to
the rest of the world.
Dispositions and Capital Recycling
Our guidance in February of this year assumed property sales
in the range of $500 million to $1 billion of assets. Assuming
the midpoint of that range, we believe upon completion
of these dispositions, we will generate equity proceeds of
approximately $700 million which will be utilized to primarily
fund our remaining development pipeline over the next three
to four years. This is an important component of our strategy
to rebalance our portfolio by selling non-core assets and
recycling the capital into highly productive core assets in our
key markets.
The properties we plan to sell are generally in smaller cities,
generate sales between $300 to $400 per square foot, and
are unencumbered. As a result of these characteristics, these
malls are attractive to an emerging market of private equity
investors. We expect to sell seven to ten properties and look
forward to refining our focus on our remaining portfolio,
redevelopments, and new developments.
Developments and Redevelopments
Looking at our development and redevelopment pipeline:
 We look forward to the grand opening of Fashion Outlets of
Chicago in August of this year and anticipate it will be one of
the strongest new outlet centers opening in many years.
 We are working diligently at Tysons Corner and expect our
expansion project will be delivered on time and on budget,
opening in phases throughout 2014.
 We will break ground soon on the 150,000 square foot
expansion of Fashion Outlets of Niagara Falls USA, and our
pre-leasing of this expansion space is proceeding very well.
 We will further enhance The Shops at North Bridge with
the highly anticipated 2013 opening of Eataly, the largest
artisanal Italian food and wine emporium in the world.
This will be Eatalys second U.S. location, its largest one, at
60,000 square feet, and a signature attraction for visitors.
 We have made great progress on our development plans
for Broadway Plaza in Walnut Creek and are optimistic
we can commence demolition of approximately 60% of
that center in January 2014, with the re-opening of the
small shop area occurring in phases in 2016. Our terrific
department store line-up of Neiman Marcus, Nordstrom
and Macys will remain open during construction as will
much of the retail center. Upon completion, we anticipate
adding 200,000 square feet to the center and investing
approximately $250 million together with our 50% partner,
Northwestern Mutual. We expect attractive returns on our
investment and believe that this is an opportunity to create
a highly desirable, fashion center in the Bay area. Similar to
our transformation of Santa Monica Place, we believe we will
transform Broadway Plaza from an already wonderful retail
location into one of the most dynamic centers on the West
Coast.
 We are already formulating plans for the redevelopment
of our newly acquired Kings Plaza and Green Acres. At
Kings Plaza, we anticipate commencing the first phase in
2014 with the construction of a new food court, an interior
remodel and a complete remerchandising of the center.
There are over 50 tenants that have highly productive stores
at our Queens Center but do not have a presence at Kings
Plaza. We are working with unique retailers such as Michael
Kors and anticipate Kings Plaza will be one of the great value
enhancement opportunities in our future. The reception
from the retailer community has been extremely positive
given our success at Queens Center. At Green Acres, we
are under contract to acquire approximately 20 acres of
contiguous land and there is strong interest for big box and
other uses that will further solidify this center that already
generates over $800 million in annual sales.General Outlook
As I mentioned earlier, the retail climate in the United States
has steadily improved over the last three years and is now
overall very good. Fundamentals are strong in terms of supply
and demand in the regional mall industry and we believe
that our focus on major metropolitan markets and dominant
fortress assets will serve us well for many years to come.
The Arizona economy has bottomed out, diversified and is
experiencing a robust economic recovery which will benefit
our portfolio in this market. Phoenix has generally led the
nation in economic and/or population growth most years of
every decade over the past 60 years and is expected to grow
at a rate of twice the U.S. average over the next five years.
We believe the current economic expansion that is underway
in Arizona will provide us an opportunity to build another
regional mall in West Phoenix in the Goodyear marketplace as
well as potentially a fashion outlet mall in the North Scottsdale
market over the next three to five years. Given our land
holdings and control of the key growth corridors in the market,
it is simply a question of when, not if, in Arizona.
The regional economies in Southern California as well as
Northern California are likewise strong and our markets in
the Boroughs of New York and Northern Virginia are some
of the best in the United States. We have numerous future
growth opportunities and believe by concentrating on fewer,
but higher quality assets, we can deliver the best possible
stockholder return on a risk-adjusted basis.
Summary
We have an outstanding portfolio with nearly 80% of our NOI
generated from high sales properties located in some of the
most populated and fastest-growing markets in the United
States. We are committed to further enhancing our portfolio
and fueling our external growth by not only successfully
executing our current development pipeline but also by
unlocking our embedded redevelopment opportunities.
Consistent with our investment strategy of recycling capital,
we plan to fund most of our development and redevelopment
pipeline through the sale of our non-core assets. While this
may be a familiar theme in the real estate markets today, it
is very difficult to achieve in the regional mall sector. The
number of dominant regional mall opportunities to build or
acquire is extremely scarce. Also, the universe of buyers to
purchase our non-core assets is limited. We are confident
we will be successful and will continue to make great
progress in our evolution of rebalancing our portfolio and
recycling our capital into our most productive properties and
projects. We believe our redevelopment opportunities and
key developments coupled with an improving environment
for releasing and remerchandising make the future of our
Company very bright.
I want to thank our stakeholders and our Board of Directors
for their guidance during the year and welcome our new Board
member, Andrea Stephen. I also want to thank Jim Cownie,
who retired as a director in 2012, for his over 18 years of
service to our Company. Jim made invaluable contributions to
our Board and to the formation and growth of our Company.
I look forward to reporting to you during the balance of this
year.
Very truly yours,
Arthur M. Coppola
Chairman and Chief Executive Officer