A Message to Our Shareholders:

Right now, one in five people suffer from cardiovascular disease  a disease that will claim more lives than the next six
leading diseases combined. As we work each day to make innovative, lifesaving therapies available for people facing this
complex and life-threatening disease, we are mindful of the immediacy of the threat and the urgency of our mission.
The 5,000 employees of Edwards Lifesciences are committed to making a positive difference in the lives of the patients
we serve, and this past year is an excellent example of the continued progress we are making toward this goal.
It has been an exciting fourth year for Edwards as we introduced more new products in 2003 than in any other year in our
history. We also laid some important groundwork that positions us favorably in exciting, and potentially large, new markets.
This year was not without its share of challenges, either, as we made some difficult but necessary resource reallocation
decisions consistent with our sharpened growth strategy. And, even though we fell somewhat short of our targeted
underlying sales growth rate, we exceeded our other 2003 financial goals: growing adjusted net income 16.5 percent
and generating $103 million of free cash flow, while recording a nearly 12 percent increase in R&D investment. Importantly,
we remain well on our way to achieving our aspiration of becoming a faster growing company.
2003: Building On Our Successes When we first became an independent, publicly traded company in 2000, one of our
goals was to build a more robust product pipeline through a combination of internal research and development efforts,
selected acquisitions and investments in exciting new technologies. To achieve this goal, we committed to consistently
increase the percentage of sales that is directed to R&D investment. Not only were the new products we introduced in
2003 a direct result of that increased investment, they represent enhancements to our strong platform of global franchises.
Building on our market-leading PERIMOUNT tissue valves, the newly approved Carpentier-Edwards PERIMOUNT Magna
aortic heart valve is the first and only device of its kind to combine more than 20 years of clinical experience with the most
advanced tissue engineering technologies. The Magna valves unique, patented design is intended to optimize blood flow
for patients and further enhance overall performance and durability. We believe Magna will ultimately become the leading
tissue heart valve in the world.

To promote broader mitral pericardial tissue valve use, we launched the Tricentrix holder system. This unique device helps
hold and position our PERIMOUNT mitral replacement valve during surgery, creating better visualization and operating
space for the surgeon. Complementing the industrys broadest offering of valve repair products, the new IMR ETlogix ring
was developed in partnership with several leading surgeons to meet the specific needs of a group of patients whose
coronary artery disease has distorted the shape of their hearts.
During the year, we also launched the EMBOL-X system, the only technology currently available in the U.S. that can help
surgeons to capture emboli, such as blood clots or tissue fragments, to prevent them from traveling through a patients
bloodstream following cardiac surgery. Our PreSep central venous oximetry catheter, also launched in 2003, is an important
tool in the effective diagnosis of sepsis, one of the leading causes of death in intensive care units.
The introduction of Edwards pre-mounted, balloon-expandable LifeStent SDS biliary system and LifeStent SDS peripheral
system mark the companys entry into the large and rapidly growing non-coronary stent market. LifeStents patented helical
design provides clinicians with unique deliverability and conformability in their treatment of biliary and peripheral vascular
conditions. We expect to introduce LifeStent NT, a complementary line of self-expanding stent products, later in 2004.
Because of the timing of their market approvals, our new products contributed only slightly to sales in 2003, but they
are expected to contribute meaningfully to our growth in 2004 and beyond.
A Successful Year Despite Challenges As part of a rigorous, long-range strategic planning process, we took a number of
difficult but necessary actions in 2003 designed to focus our resources and position the company for continued success.
In August, we reduced our employee headcount by two and a half percent, and in December, we announced our plan
to seek strategic alternatives for our Lifepath AAA program. These actions, along with our exit from several non-strategic
businesses, enabled us to redirect resources to both our market-leading franchises and the most promising areas of future
growth for our company.

During the year, our leadership position in the U.S. heart valve market came under attack from increased competitive
forces, and although our reported Cardiac Surgery sales growth rate was 16.6 percent, our underlying growth rate was
slightly below our expectation. We are responding in a deliberate and systematic fashion by increasing the size of our sales
force and launching a sharpened message that clearly demonstrates to our physician customers the superior attributes of
our products. Edwards PERIMOUNT valves remain the most widely implanted tissue heart valves in the world, and we are
confident that our competitive responses, together with the appeal of our technologically advanced new products, will
enable us to gain market share in 2004.
Two leadership changes occurred early in the year. In February, Robert Ingram, Vice Chairman, Pharmaceuticals for
GlaxoSmithKline, joined Edwards already strong, independent Board of Directors. In March, we welcomed Corinne Lyle
as our new chief financial officer. With her knowledge of the health care industry and previous service on our Board and
its audit and public policy committee, Corinnes transition was quick and seamless. Under her leadership, we continued to
strengthen our balance sheet and took advantage of the favorable interest rate environment, adding a layer of long-term,
fixed-rate capital by completing a $150 million convertible debt offering with attractive terms. Additionally, we used a
portion of our strong cash flows to repurchase nearly 1.8 million shares of stock during the year.
Laying a Foundation for the Future We made significant investments and acquisitions in innovative technologies during
the past year  several of which represent new, market-expanding opportunities. In February, we acquired a percutaneous
(or catheter-based) mitral valve repair program from Jomed N.V. In December, we announced the acquisition of Percutaneous
Valve Technologies (PVT) and its percutaneous aortic valve replacement program. These transactions add valuable clinical
experience and strong intellectual property to our own percutaneous valve repair and replacement programs.
Percutaneous technologies offer the promise of less-invasive treatments, shorter hospital stays and faster recovery times,
and clinicians are very enthusiastic about the potential to treat a new group of patients who may not be candidates
for conventional surgical procedures. We feel the addition of Jomed and PVT to Edwards portfolio gives us the most
comprehensive program of catheter-based valve therapies, uniquely positioning us for success in this potentially large
and exciting new market.

Another area that were investing in is minimally invasive cardiac output (MICO), which represents an unmet clinical
need in critical care and is a natural extension of our market-leading line of Swan-Ganz hemodynamic monitoring catheters.
We anticipate first clinical use of MICO technology in the U.S. as early as 2005. And, in early 2004, Edwards will also be
commencing human clinical trials with our partner, Sangamo Biosciences, for a novel biotech therapy designed to treat
peripheral artery disease.
Overall, the future has never looked brighter for Edwards and we will continue to balance our investments in future
technologies between those that defend and extend our core franchises, those that form the basis for new growth
platforms, and those that represent exciting, transformational opportunities in cardiovascular care.
What to Expect in 2004 For 2004, we have set financial goals that keep us squarely on the path to becoming a
faster growing, more profitable company. These include: driving sales in the range of $915-$940 million; growing our
rate of R&D investment at or above our underlying sales growth rate; delivering net income growth in the range of
13 to 15 percent (excluding the impact of the PVT acquisition); and generating free cash flow of $90-$95 million.
While we are committed to achieving our financial goals, our primary focus will always be the needs of patients suffering
from advanced cardiovascular disease. In spite of all the therapies available today to treat cardiovascular disease, the need
for innovative new products will only continue to grow as the population ages and life expectancies increase. As we have
done over the years, we will continue partnering with clinicians to develop life-saving technologies, and we are committed
to continue bringing more new products to market just as we did this year. Were proud of what we accomplished for
patients in 2003, and remain very enthusiastic about the opportunity to help drive future advancements in cardiovascular
disease treatment. We thank you for your support as we continue to help improve peoples lives, right now.
Michael A. Mussallem
Chairman and Chief Executive Officer