Dear Fellow Shareholders
As 2008 unfolded, we experienced the value of the business model
that has seen Omnicom through good times and bad. Our strategy
of building a leading portfolio of global advertising and marketing
brands, diversified by discipline and geography, enabled us to post
new records for revenues, net income and earnings per share.
Those results were underpinned by an exceptional body of creative
work by our agencies that garnered a major share of the worlds
most prestigious awards for creative excellence.
Yet it is clear to us that the
business model which has been
the foundation of Omnicoms
success is being tested by new
economic realities and
fundamental industry changes.
After three relatively strong
quarters, the final three months of
2008 were the most challenging
we have experienced since the
recession of the early 1990s. The
near-simultaneous contraction of
global economies, rising
unemployment and declining
consumer and business confidence
led to decreased spending by
clients, especially on year-end
project work. As clients adjusted
their budgets to the downturn, our
revenues declined across all
disciplines in the fourth quarter.
The financial impact of lower
revenues was intensified late in the year by the dramatic weakening
of all major currencies against the U.S. dollar. Should exchange rates
remain at approximately the same levels as those at the end of the
first quarter of 2009, we expect that Omnicoms full-year
consolidated revenues will decline from the record levels of 2008.
Our agency management teams did a great job of helping us
manage through these initial challenges. They maintained the
creative excellence for which they are renowned while amassing a
substantial record of new business wins. They continued to play critical roles in advising clients on how to navigate the emerging
economic downturn as well as the rapidly changing media
landscape. And, as it became
clearer that the global economy
was quickly sliding into recession,
they made difficult decisions to
selectively reduce staff levels and
incentive compensation pools to
better align costs with expected
revenues. Maintaining this creative
excellence, client service and
business focus will be essential to
achieving our targets for
profitability during this downturn.
This is a challenging mandate in
the current business environment,
but it is a task to which we bring
substantial expertise. Omnicom is
a learning organization  one that
disseminates the most important
lessons from our successes and
shortcomings. We have invested
continually since 1995 in
formalizing our collective business
knowledge through a variety of
advanced education programs,
including what we learned in
successfully navigating past
recessions. Over the past 13 years,
thousands of our senior managers
have attended Omnicom
Universitys Senior Management
Program (SMP), Postgraduate SMP
Program or the Advanced
Management Program in the
United States, Europe or Asia.
During 2008, a key topic of study
was managing through an
economic downturn, a theme that certainly will receive even more
attention in 2009.
Omnicom Finance also holds Financial Leadership Conferences in
the U.S., Europe and Asia that disseminate our accumulated
expertise on key business management issues such as accounting,
taxation, real estate, financial systems and business planning. Putting
the power of this intellectual capital to work across our 1,500
agencies remains a high priority for us for the most compelling
reason  it circulates specific business expertise across our
agencies that can return real value in any economic environment.
Creating value by doing the right thing
The socially responsible policies and practices that we support also
create meaningful value for Omnicom. As weve said before,
investments in broadening the cultural diversity of our agencies,
reducing our environmental impact and participating in worthy social
causes are smart business, and we intend to continue our support of
these practices in whatever business environment we confront.
Diversity In 2007, we established the Omnicom Diversity
Development Advisory Committee (DDAC), giving it the broad
mandate to advance the cultural diversity of the advertising industry.
The DDAC held numerous meetings during 2008 intended to orient
its eight members to Omnicom, to existing diversity efforts at our
agencies and to best practices of other major organizations that are
recognized for their leadership on this important issue. In early
2009, Omnicom hired its first Chief Diversity Officer, Tiffany R.
Warren, to manage our policies and practices. Ms. Warren has
extensive experience in the advertising industry as an executive in
charge of multicultural programs and community outreach.
Environment We realize that, as a global enterprise, Omnicom
and its agencies have an impact on the environment, and we have a
responsibility to be as efficient as possible in our use of resources
of all kinds. Of all the ways that we can reduce our collective
environment impact none is more important than implementing
ongoing improvements in how we manage energy usage across our
agencies. During 2008, in just one of many examples, we initiated a
strategy to reduce energy consumption across our properties.
Carbon footprint data will continue to be collected and analyzed to
determine where we can further reduce our energy usage without
compromising services to our clients.
Letter from the PresidentPhilanthropy Each year, Omnicom agencies volunteer thousands
of hours of communications support and direct involvement for
worthy social causes. While there are hundreds of examples of
giving back to our communities, three were especially notable last
year. When parts of Sichuan Province in western China were
devasted by earthquakes, the Omnicom family of companies
responded quickly to help with relief efforts. Our employees
provided substantial financial donations and worked with clients to
provide medicine, water and other supplies and services. In Prague,
Czech Republic, 140 volunteers attending our European Financial
Leadership Conference participated in a much-needed refurbishment
of the state-run Detske Centrum Orphanage. Additionally, our
volunteers organized a very successful auction that enabled us to
raise funds for an extensive wish list of items needed by the
orphanage. And more than 200 attendees at the 2008 Omnicom
Group Americas Financial Leadership Conference volunteered to
help out in the 19
th
-annual Serve-a-thon event in South Boston. Our
people focused their energies on helping to restore Gavin Middle
School and the Old Colony Housing Projects Teen Center, also
providing the center with equipment for its new computer lab.
Meeting the challenges ahead
A core component of Omnicoms business model is broad
diversification of clients, services and geographies. With no single
client representing more than 2.8 percent of 2008 revenues, and a
growing share of our business coming from emerging markets, we
have some insulation against the severity of the global economic
contraction. Until a broadly based recovery is clearly established, we
will continue to work with our agencies to help them align their
cost structures with sustainable revenues. And while we must run
our businesses with a sharp focus on efficiency, our lifeblood is
helping clients deliver their messages as creatively and economically
as possible. Those skills will be tested during 2009. Our creative
mandate remains the same  to invent engaging conversations
between brands and consumers that capture and hold their
attention. But our economic mandate to do more with less demands
that we challenge the status quo.
Across all disciplines, the ongoing integration  and interactivity 
of traditional and digital media to achieve memorable
communications with measurable results requires our agencies to
collaborate in new and efficient ways. In that regard, Omnicom has
Letter from the Presidentsome fundamental competitive advantages: a leading portfolio of
global advertising and marketing brands that are the best in the
business, unmatched creativity in delivering value to our clients, and
a proven ability to bring resources together across disciplines and
geographies to collaborate on any marketing or communications
strategies our clients may have. These capabilities have delivered
excellent returns to shareholders in the past, and we are confident
they will continue to do so when economic growth resumes.
In the meantime, we will remain very disciplined in aligning costs
and maintaining cash. The balance-sheet strength we have carefully
cultivated will help us deal proactively with changes brought about
by media fragmentation and emerging consumer habits of
interacting with those media. It is very likely that the recession will
provide opportunities to acquire agencies at reasonable prices that
will enable us to fill out the capabilities we can offer to clients in
select disciplines or markets. As always, we will be disciplined in
any acquisition  with fit, price and culture being our primary
criteria  to ensure that financial flexibility continues to be a
defining quality of Omnicom.
While there is little certainty at this writing about the depth and
duration of the current recession, one thing is certain in our minds:
Our highest priority is helping clients grow their business. To that
end, Omnicom offers an unparalleled suite of capabilities, a proven
ability to provide innovative, cost-effective solutions wherever we
operate and a global team of management, creative and account
professionals that is simply the best our industry has to offer. That is
a value proposition which we believe is very compelling in any
environment, not only for clients but for shareholders as well.
Sincerely,
John D. Wren
President & Chief Executive Officer