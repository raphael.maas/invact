For Varian Medical Systems, fiscal year 2009 was both a
challenging and a promising period that brought out the
best in our company. In North America, we encountered a
deep recession, proposals for drastic cuts in reimbursement
rates for radiotherapy at freestanding clinics, and a healthcare
reform movement that put the entire U.S. market
under a cloud of uncertainty. But we showed that we could
weather this storm while achieving many major successes
that point to continued long-term growth for our company.
Considering the economic environment, our financial performance
was remarkable, although not at the levels we are used to seeing
from this company. Compared to continuing operations from
the previous fiscal year, fiscal 2009 net orders grew by 3 percent
to $2.4 billion, revenues rose 7 percent to $2.2 billion, operating
earnings increased 13 percent to $474 million, net earnings
climbed 12 percent to $331 million, and our annual net earnings
per diluted share improved 15 percent to $2.65. By year end, we
had grown net order backlog by 9 percent to $2.1 billion, and
built our cash position to a solid $554 million. We entered fiscal
year 2010 in a very strong financial position.
We managed to leverage our revenue growth into even higher
earnings growth during the year with the help of successful costcutting
initiatives, including a salary freeze, tighter limits on hiring,
cutbacks on contract work, travel restrictions, and several
other measures that should continue to help margins in fiscal
2010. Subsequent to the end of the year, we reduced our workforce
by about 2 percent as an additional cost-control measure for
fiscal 2010 as the economy continues to work toward recovery.
Oncology Systems
All parts of our company were affected by the recession, but it
was particularly tough in North America for our Oncology Systems
business. This business reported annual net order growth
of 1 percent and revenue growth of 8 percent. In North America,
net orders were down 7 percent primarily because of the recession,
which caused hospitals to reduce their capital equipment
budgets and slow their purchasing activity. A government proposal
for an up to 40 percent cut in reimbursement rates at freestanding
clinics across the United States brought that portion of
the market to a virtual standstill in the latter part of the fiscal year.
Meanwhile, tougher access to capital as well as ongoing debate,
confusion, and fear over U.S. healthcare reforms added to the
malaise. Fortunately, efforts by many patients, doctors, and clinical
and industry groups, including our new Government Affairs
team in Washington DC, succeeded in persuading the government
to cut reimbursements for freestanding clinics by just 1 percent
in 2010, thereby enabling a return to a more normal business
environment for this portion of the market.
The troubles in North America during fiscal 2009 overshadowed
many successes for our Oncology Systems business. Annual orders
in the international market rose by 11 percent, or 16 percent
on a constant currency basis, versus the previous year. Europe,
Asia, and Latin America all contributed to this growth, demonstrating
that there is clear demand for our cancer-fighting products
in underequipped regions where we have significant
potential for expansion.
New technology and products also factored in the growth of the
Oncology Systems business. We finished fiscal 2009 with more
than 270 installations of our RapidArc product for faster, more
cost-effective radiotherapy and radiosurgery, and the rapid rollout
of this product helped to drive strong margin improvements
in this business for the year. To meet demand within international
markets for a cost-competitive system with advanced treatment
capabilities and higher patient throughput, we launched the
Unique low-energy accelerator, which can deliver fast, imageguided
RapidArc treatments. We have already booked our first
order for this new machine, which we expect will drive even faster
adoption of our RapidArc technology around the world. With
its competitive pricing, we believe Unique will change the value
proposition for many customers by enabling them to deliver
world-class care at a lower, more affordable cost per patient.
Our technologies for faster dose delivery, higher-quality imaging,
and better motion management are making it possible for more
and more radiation oncology centers to expand their practices
with radiosurgery, where ultraprecise, high doses are delivered in
five or fewer treatments. Orders for our Novalis Tx radiosurgery
platform hit record highs in fiscal 2009. Several pioneering centers
have already reported very promising results from radiosurgery for
lung cancer. Dr. Robert Timmerman of the University of Texas
Southwestern Medical Center in Dallas has used radiosurgery to
stop the growth of early-stage nonsmall cell lung cancer at its
original site among selected frail, inoperable patients. Nearly 50
percent of these patients were alive and cancer free three years
after their diagnosis. These results suggest that Varian is in a position
to provide a game-changing, noninvasive treatment alternative
to surgery for lung cancer, which is among the deadliest and
most prevalent cancers in the world.
Service also contributed to the annual revenue growth in our
Oncology Systems business with the help of software service
agreements and a higher contract capture rate on new installations
with more sophisticated capabilities. Annual service revenues
are of increasing importance to our business.
X-Ray Products
In our X-Ray Products business, annual net orders rose 1 percent
versus the previous year, overcoming significant order declines in
the second and third quarter when X-ray equipment manufacturers
reduced inventories of X-ray tubes and flat-panel detectors in
response to the recession. However, annual revenues for this business
rose 9 percent, driven in part by demand for our new flatpanel
detectors for digital radiography. An ongoing broad-based
conversion to more cost-effective, filmless X-ray imaging should
help to support continuing growth in our X-Ray Products business.
Our flat-panel products represented more than 40 percent
of X-Ray Products net orders for the year, and they are on track
to become the largest part of this business. We intend to continue
developing new panel products with better capabilities, including
higher resolution and lower X-ray dose requirements. Filmless
imaging and higher patient throughput have also created the need
for a new breed of X-ray tubes that cool more quickly and enable
higher throughput with more images per hour on X-ray imaging
equipment. We made good progress on the development of this
next-generation tube during the year. We also intend to expand
our X-Ray Products business through improved distribution in
developing markets.
Emerging Businesses
The economic downturn and customer inventory reduction
efforts also appeared to impact our emerging Security and Inspection
Products business during fiscal year 2009 as governments and
cargo screening system integrators postponed purchasing decisions.
Annual orders and revenues were down for this business
versus the previous fiscal year. However, we remain optimistic
about the long-term prospects for this business, largely because
of our technological advancements, which are improving image
quality, speeding throughput, and automating the screening
process through materials discrimination.
The Varian Particle Therapy business had a good year. We received
our CE mark, an important European certification, for
our proton therapy system, and we are marketing the system for
several projects around the world. Patient treatments have commenced
at a center that we are commissioning in Germany. In the
second quarter of fiscal 2009, we divested and discontinued the
operations of the Research Instruments portion of the ACCEL
business, which we acquired in fiscal 2007, in order to focus the
unit exclusively on the continued development of our particle
therapy business.
The Challenge Ahead
The mission of Varian Medical Systems is to use the power of
focused energy to save lives and prevent harm. We believe that
our technology can make the world a much better and safer place.
Given our achievements of fiscal year 2009, we have shown that
we can perform well in difficult circumstances. We have continued
to innovate, and we have supplied customers in all of our
markets with many new lifesaving capabilities and products that
offer the potential of better clinical outcomes, greater utility,
higher throughput, and lower cost per procedure. We have enhanced
our customer service and maintained our high standard
for reliability. We have paid particular attention this year to smart
cost controls so that we can deliver the best value to both our customers
and our investors. All of these efforts are contributing to
our success, and they will continue to do so for the long term.
The driving force behind these efforts is the great talent of the
people of Varian and their commitment to our mission to save
more lives. This mission will carry us through 2010 and beyond.
We thank you for being a part of it.
Sincerely,

Tim Guertin
President and CEO