Fellow Shareholders:
P&G touches the lives of consumers around the world every day. Thirty million times a day, in 
more than 160 countries, P&G brands face their first moment of truth, when consumers stand 
in front of a store shelf  at a Wal-Mart store in West Virginia, a Carrefour store in France, a 
bodega in Mexico, a corner market in Egypt or a tiny sari sari store in the Philippines  and 
decide whether to buy a P&G brand, or a competing product. 
A lot happens in that moment, as consumers assess the performance, quality and value P&G 
brands offer relative to other products on the shelf. When we strike the right balance between 
brand promise and store price, we win that first moment of truth. 
Winning the first moment of truth is only half the battle. Soon after, P&G brands face a second 
moment of truth. Nearly two billion times a day, P&G products are put to the test when 
consumers use Tide to clean their clothes, Pampers to care for their babies, Pantene to care for 
their hair, Olay to improve the condition of their skin, Crest to brighten their smile. Every one of 
P&Gs brands is put to this test.
In every one of those nearly two billion experiences, consumers decide whether P&G brands live 
up to their promises. When we get this right  when we deliver the benefits promised, when 
we provide a delightful and memorable usage experience, when we make everyday life a little 
bit better, a little more convenient, a little bit healthier and more beautiful  then we begin 
to earn the trust on which great brands are built.
At P&G, weve been competing for and winning these moments of truth for 165 years. As a 
result, weve built one of the largest and strongest portfolios of leading brands in the world.
We have made substantial progress over the past two years in strengthening P&Gs business. 
Were back in the lead. Were committed to continuing this progress and to ensuring we 
squarely face changing marketplace realities  to get out in front and to stay in front of change.
Back on Track
Getting back in the lead has certainly been Job One over the past two years. With clear 
strategic choices, operational excellence and financial discipline, we are now returning to the 
consistent, reliable earnings and cash growth that shareholders expect from P&G.
We are delivering long-term growth objectives ahead of plan. Our long-term goals are 
to consistently deliver double-digit core earnings-per-share growth, and 4% to 6% sales 
growth, excluding the impacts of foreign exchange. Core net earnings increased 10% to $5.1 
billion. Core diluted net earnings-per-share, which excludes the impact of restructuring charges 
and the prior-year amortization of goodwill and intangibles (which is no longer required under 
new accounting rules), increased 10% to $3.59. Core operating income increased 12% to 
$7.6 billion. Volume for the year grew 7%. Sales were up 4%, excluding foreign exchange 
impact, despite corrective pricing actions taken over the last 18 months. Consumers have 
reacted positively to the improved value our brands now offer, with volume and sales growth 
accelerating in the second half of the year.
This growth is broad-scale. Every business unit delivered profitable growth at rates above the 
corporate objective. Every geographic region delivered volume growth. We have work yet to do, 
but we have achieved P&Gs long-term growth objectives a year earlier than we had projected.
We are increasing free cash flow. Cash generation is a key indicator of a companys underlying 
health, and P&Gs management is focused on creating leadership, sustained cash flow growth in 
each of our business units. Free cash flow  operating cash after capital spending  was $6.1 billion, 
up 83% from last year and more than triple the free cash flow generated two years ago. Capital 
spending improvement has been a key driver. Our objective was to reduce capital spending to 6% 
of sales by fiscal 2004, and weve exceeded that goal two years ahead of schedule. We are now 
resetting our capital spending target to below 5% of sales, and continue to look for opportunities 
to improve asset efficiencies. 
We are delivering superior total shareholder return (TSR). TSR is the key business unit metric 
upon which P&Gs business planning and management compensation are based. The combination 
of strong earnings growth and focus on cash has resulted in superior business returns over the past 
year. P&Gs share price has responded accordingly, outpacing the major indices of Dow Jones and 
the S&P 500.
These are substantial achievements, in tough economic times, in a very competitive global marketplace. These are the kinds of results P&G has delivered consistently and reliably for decades. This 
is the level of performance that P&G shareholders expect and also that we  the men and women 
of P&G  expect of ourselves. I want to commend the dedication, discipline and creativity of P&G 
people who delivered these results.
Success Drivers
There are six drivers of P&Gs success: consumer focus, strategic choices, operational 
excellence, financial discipline, organizational structure and brand building capability.
Consumer focus. The consumer is boss and we put the consumer at the heart of all we do. 
First and foremost, this means getting the consumer value equation right on all our brands. 
It also means delivering superior, consumer-meaningful innovation. Competitive pressures 
make this a continuing challenge, but we are working hard in every part of our business to 
deliver a continual stream of innovation that meets consumer needs in ways that reset 
performance expectations and represent superior value.
We are also making sure that P&G business leaders  including me  get out of the office 
and into stores and homes to talk directly with people who buy and use our products. No 
other real-world experience clarifies the choices we must make as much as these 
conversations with consumers. 
Strategic choices. Weve focused on P&Gs core categories, leading brands, and the biggest 
retail customers and country markets. And, were investing in faster-growing, higher-margin, 
more asset-efficient businesses. A good example is the acquisition of Clairol, which 
complements the core Hair Care business and helps P&G enter the fast-growing hair colorant 
category. Strategic choices like these are paying off. For example, about one-fourth of total 
gross margin improvement in fiscal 2002 came from our emphasis on faster-growing, highermargin businesses such as Beauty Care and Health Care.
Operational excellence. Execution determines success, and weve placed heightened 
emphasis on operational excellence at P&G. Again, Clairol is a good example. We closed this 
deal in less than six months and fully integrated business operations seven months later. 
Most important, we accomplished a smooth integration and were on track to deliver the 
committed synergy savings ahead of schedule.
Financial discipline. We have reinforced P&Gs long-standing commitment to financial 
discipline. Our efforts are seen most clearly in the Companys business results. Even before 
the highly publicized accounting failures at several companies, we took a number of specific steps to ensure strong corporate governance. We reiterated clear expectations for ethical 
behavior. We instituted a broad-based Financial Stewardship program  more than 18 months 
ago  to provide even more focus on fiduciary responsibilities, internal controls and 
accounting processes. We continue to engage openly with P&G's Board of Directors, and 
with the Board's Audit Committee, which is comprised entirely of independent directors. And 
we maintain an independent relationship with Deloitte & Touche, including formal processes 
to approve non-audit services. These practices are consistent with the way P&G has operated 
for decades, and we are continually renewing the organizations commitment to them. 
Organizational structure. P&Gs unique operating structure is creating meaningful 
competitive advantage. In 1999, we reorganized the Company by consolidating categories 
and brands into Global Business Units (GBUs), aggregating country and regional go-to-market 
capabilities into Market Development Organizations (MDOs) and providing single-source 
business services through a Global Business Services organization (GBS). It has taken some 
time to implement, but were now reaping real advantage from consumer and market 
responsiveness and the substantial synergies made possible by the new structure. The success 
of Pampers Baby Stages of Development initiative in the United Kingdom is one good 
example. The business unit focused on delivering superior product, packaging and 
advertising; the MDO delivered an outstanding trial plan, superior in-store fundamentals and 
strong External Relations support. By dividing and conquering against a common business 
strategy and plan, we achieved more, faster. Since the launch, Pampers U.K. market share 
is back to 51%, 16 points ahead of the #2 brand. 
Brand building. Branding is more important than ever  and big, leading brands are more 
valuable than ever. In a sea of choices where confusion reigns, consumers value the reliable 
promise of their favorite brands. This plays to our strength: branding is in P&Gs DNA. P&G 
has 12 billion-dollar brands in its portfolio today  and more are expected to come. These 
brands represent more than half of the Companys sales and profits. Equally important, these 
brands account for the majority of P&Gs consumer interactions  the millions of moments 
of truth we face and win every day. 
5
steps to ensure strong corporate governance. We reiterated clear expectations for ethical 
behavior. We instituted a broad-based Financial Stewardship program  more than 18 months 
ago  to provide even more focus on fiduciary responsibilities, internal controls and 
accounting processes. We continue to engage openly with P&Gs Board of Directors, and with 
the Boards Audit Committee, which is comprised entirely of independent directors. And we 
maintain an independent relationship with Deloitte & Touche, including formal processes to 
approve non-audit services. These practices are consistent with the way P&G has operated for 
decades, and we are continually renewing the organizations commitment to them. 
Organizational structure. P&Gs unique operating structure is creating meaningful 
competitive advantage. In 1999, we reorganized the Company by consolidating categories 
and brands into Global Business Units (GBUs), aggregating country and regional go-to-market 
capabilities into Market Development Organizations (MDOs) and providing single-source 
business services through a Global Business Services organization (GBS). It has taken some 
time to implement, but were now reaping real advantage from consumer and market 
responsiveness and the substantial synergies made possible by the new structure. The
success of Pampers Baby Stages of Development initiative in the United Kingdom is one
good example. The business unit focused on delivering superior product, packaging and 
advertising; the MDO delivered an outstanding trial plan, superior in-store fundamentals and 
strong External Relations support. By dividing and conquering against a common business 
strategy and plan, we achieved more, faster. Since the launch, Pampers U.K. market share 
is back to 51%, 16 points ahead of the #2 brand. 
Brand building. Branding is more important than ever  and big, leading brands are more 
valuable than ever. In a sea of choices where confusion reigns, consumers value the reliable 
promise of their favorite brands. This plays to our strength: branding is in P&Gs DNA. P&G 
has 12 billion-dollar brands in its portfolio today  and more are expected to come. These 
brands represent more than half of the Companys sales and earnings. Equally important, 
these brands account for the majority of P&Gs consumer interactions  the millions of 
moments of truth we face and win every day. 
Changing Consumer Expectations
While weve made steady progress, we know were in a fast-moving, global marketplace in
which the magnitude, pace and scope of change are accelerating. To keep P&G in the lead, 
it is essential that we anticipate change and get in front of it. Were doing that with a focus
on three new realities that are shaping the future marketplace.
First is the changing expectations of consumers. Our business has always been about consumer 
value and in todays demanding economic environment, value is more important than ever. 
The key to winning in this environment is to reset expectations for performance and price.
Olay Total Effects is a good illustration. In global research, women identified seven distinct
signs of aging that affect the condition of their skin. Many products addressed one or two 
of these signs, but no single product fought all seven. Further, the products that performed
best in this category were high-end department store brands priced at $60 or $70 a bottle.
With Olay Total Effects, we created a single product for all seven signs of aging based on
technology that rivals or exceeds the best department store skin care brands  and offered it 
at a fraction of the department store price but at the top end of mass skin care pricing. Olay
Total Effects is now the #1 anti-aging moisturizer in the U.S., U.K., China, Canada and
Australia  growing our total Olay Skin Care franchise by nearly 20%.
The difficulty of meeting this performance/value challenge in developing markets  such as 
China or Eastern Europe  is even greater. Consumers in these markets have similar performance 
expectations, but far less purchasing power. To succeed in these markets, its often necessary 
to rethink the fundamentals  everything from manufacturing to product and packaging to 
marketing and distribution. We have experience winning in developing markets with a range 
of creative solutions: single-use Pantene sachets in China and van-based distribution in Poland, 
for example. And we continue to develop other approaches. This will be an area of increasing 
importance for P&G.
Retailers as Partners and Competitors
The second new reality is the changing nature of retailing. Increasingly, we have a dual 
relationship with retail customers: we are strong partners and sometimes competitors.
In concept, the need for tighter relationships between manufacturers and retailers is obvious: 
we both serve the same consumer, we both want to build consumer loyalty to our brands, 
were both trying to grow sales faster and more profitably. But, in practice, retailers and 
manufacturers can work at cross purposes. Energy, resources and time that could be devoted to 
creating a delightful first moment of truth shopping experience can be spent in unproductive 
discussions over shelf space, pricing, discounts and terms. 
Together with retail partners, we are working hard to change this practice. For example, we 
know consumers are often frustrated when buying hair care products. They find it hard to 
locate everything they want and are often left confused and searching for product information. 
Were working with more than 30 retailers to enhance the performance of their hair care 
departments. Weve simplified the shopping experience, provided more consumer education 
and made it easier for consumers to find and ultimately use the products that best meet their 
needs. Shoppers are spending half the time finding products and more time in the aisle 
browsing and discovering products  all of which leads to increased volume, sales and profits 
for P&G and our retail partners. These changes are delivering department growth ranges 
between 10% and 44% for retailers and P&G. 
Retailers are sometimes competitors as well as partners. Their own brands are growing as the 
retailers, themselves, grow. Private labels or store brands strive to match innovation quickly and 
try to present a compelling value alternative in many categories. This is healthy, in my opinion. 
It requires that we continue to lead innovation and to price P&G products competitively. 
Further, the growing strength of store brands underscores the importance of always being the 
#1 or #2 brand in any category. Brands that cant maintain this leadership stature will find it 
difficult to compete effectively with the best store brands. Based on our internal global share 
measures, we have the #1 or #2 brand in 17 of our 19 key global categories  categories that 
account for about 70% of sales and earnings. P&G is in a strong position, and ready to become 
an even better retail partner.
The New Interconnected Organization
The third new reality is the emerging importance of operating as an interconnected 
company. A recent report by industry consultant Booz Allen Hamilton noted Vertical
thinking (i.e., own or control every link in the supply chain) has given way to virtual thinking 
(i.e., create a flexible web of supply relationships and focus exclusively on what one does best).
At P&G, we see this as an enormous opportunity  in part, because thats where the marketplace is headed and, even more so, because P&G has a strong history of developing partnerships that bring out the best in us and our partners. Weve done it for years with advertising 
agencies, customers, joint venture partners and technology suppliers. 
When Dr. Johns, the maker of SpinBrush toothbrushes, approached us about licensing the 
Crest name, we recognized the opportunity. Within five months of that first contact, we 
bought the company and then brought Crests new, powered SpinBrush to market in record 
time. Crest SpinBrush, now in 20 markets, has a 50% volume share of the growing poweredbrush category in the U.S. and has quadrupled the sales of the original Dr. Johns product. 
When we developed the bisphosphonate technology in Actonel, we increased marketing 
capability by partnering with Aventis, whose field sales force had broad access to and 
credibility with doctors. The partnership worked well, and today Actonel is a nearly $400 
million brand and growing. 
Our vision is that P&G will be the best company in the world at spotting, developing and 
leveraging partnerships in every area of the business. In fact, I want P&G to be a magnet 
for best-in-class partners who want to build significant new business together.
The Next Generation of Leadership
The bottom line is this: P&G is getting back on track. We have what it takes and were doing 
what it takes to stay in the lead  now, and in the future. Were making clear strategic choices, 
strengthening operational excellence and operating with rigorous financial discipline. Were 
delivering the earnings growth to which weve committed  ahead of plan. Were generating 
cash from every business unit  at record levels. Were delivering returns that exceed the 
performance of the Dow Jones index and the S&P 500. In short, we are returning to the level 
of performance you, and we, expect from P&G  and we are determined to keep it up.
I want to close this letter with one final point. The reason things work well at P&G is that 
everyone is an owner and a leader. We hire people because theyre leaders. We give them the 
training, development and experience to become even stronger leaders. We promote people 
who deliver superior results, operate with integrity and strengthen those around them. We 
have a culture that values and embraces leadership. Thats true at the top of our organization. 
In the middle. At entry-level. Its true of people who've just joined P&G, as well as those 
whove spent 20- or 30-year careers at P&G. 
The key in such an organization is to provide individual and business unit growth opportunities, 
and then to empower people to lead and execute with excellence. That is precisely what were 
doing at P&G. I have no doubt we have the right organization to keep our Company growing. 
In these past two years, I have witnessed a passionate sense of ownership for our business that 
deepens my confidence in P&Gs future, no matter what challenges we may face. 
As owners of Procter & Gamble, you can be assured that the pioneering spirit, operating 
discipline and dogged commitment to being in the lead that have always characterized this 
Company  for 165 years!  are as alive today as they have ever been. 
A.G. Lafiey
Chairman of the Board, 
President and Chief Executive15
