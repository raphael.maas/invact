Dear Shareholder:
In todays more connected and dynamic global economy
there is a growing need for the benchmarks, research,
analytics and data of McGraw Hill Financial. Our unique
products and services facilitate the flow of capital and
commerce through global markets, promoting growth,
development and higher standards of living.
Harold McGraw III
Chairman of the Board
The evolution of financial markets and
economies all over the world today is
shaping the future. Exciting developments
include a sea change in investment
management, from the rise of private
wealth in Asia Pacific to the ongoing
movement of financial flows, from actively
managed funds to passive investments.
Another area demanding attention is
global trade. Policy makers recognize
that opening new markets is an economic
imperative. According to a report by the
United Nations, almost all countries
with substantial improvement in human
development over the past two decades
have also become more integrated with
the world economy.
Recent policy developments offer good
news. Toward the end of last year the United
States and India, together with the WTO,
salvaged the multilateral deal known as the
Trade Facilitation Agreement (TFA) to cut
red tape at border crossings. Now, the G20
must work to ensure all 160 nations of the
WTO ratify and implement the TFA.
An increase in cross border capital flows
and the liberalization of trade policies are
welcome developments as McGraw Hill
Financial looks to international markets
for growth. For details of what the
company is doing to serve international
markets, I encourage you to read about
the ways the company is creating growth
and driving performance throughout
the pages of this Annual Report.
As announced on February 25, 2015, I
informed the Board of Directors of my decision not to stand for re-election as a Director
at the next Annual Meeting of Shareholders
scheduled to take place on April 29, 2015.
I want to thank our outstanding Board for
their guidance and counsel over the years
and I am deeply touched by their awarding
me the title of Chairman Emeritus.
I want to thank you, our shareholders,
for your support and confidence in our
ability to drive growth and to create superior
shareholder value. In addition, I want to
wish every success to Doug Peterson,
President and CEO, and Ed Haldeman
whom the Board will propose as Chairman
following the Annual Meeting. Both these
individuals are world-class leaders and
highly respected for their achievements.
McGraw Hill Financial is an outstanding
company with a bright future and a proud
legacy of providing essential intelligence to
customers. I am so very grateful to all the
people who have contributed to the success
of our businesses over many generations.
Since our founding by James H. McGraw
in 1888, the McGraw Hill teams have
continually met the transformative needs
of our customers and markets, and will
continue to do so in the future.
Sincerely,
Harold McGraw III
Chairman of the Board
Dear Shareholder:
In todays more connected and dynamic global economy
there is a growing need for the benchmarks, research,
analytics and data of McGraw Hill Financial. Our unique
products and services facilitate the flow of capital and
commerce through global markets, promoting growth,
development and higher standards of living.
Harold McGraw III
Chairman of the Board
Chairman Harold McGraw III

Dear Shareholder:
I am pleased to report McGraw Hill Financial has made great progress serving
global capital, commodity and commercial markets with our compelling
benchmarks, analytics, research and data. We generated 7% revenue growth
and 20% adjusted diluted earnings per share (EPS) from continuing operations.
We also resolved significant legal and regulatory matters, completed our
portfolio rationalization of media assets and built excellent momentum to
create growth and deliver shareholder value.
Douglas L. Peterson
President and Chief Executive Officer