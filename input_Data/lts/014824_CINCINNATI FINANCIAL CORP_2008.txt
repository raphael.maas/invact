To Our Shareholders, Friends and Associates:
Your company broke some records in 2008.
Uncharacteristically, this was not good news.
Policyholders of The Cincinnati Insurance
Companies suffered record storm damages in the first
half of the year. Then, winds from Hurricane Ike came
through the Midwest in September, causing our largest
gross loss ever from a single catastrophe, $129 million,
reduced by reinsurance to a net loss of $58 million.
The property casualty insurance industry  our main
business  endured a fifth year of pricing pressure. As
disciplined underwriters, we experienced a decline of
3.4 percent in net written premiums in 2008 compared
with a decline of 0.8 percent industrywide.
For the first time, earnings on our investment
portfolio were less than the year before. Our
$537 million of pretax investment income was off
11.6 percent from the 2007 total on lower dividends
paid by holdings in our equity portfolio. This shortfall
is one reflection of the forces buffeting the investment
markets over the past months. Because of our low cost
basis, we were able to lock in gains on sales of many
holdings, leading to $686 million of net gains from
investment transactions. Those gains were partially
offset by unprecedented non-cash write-downs of
$510 million, arising from our judgment that securities
we continue to hold in our portfolio may not recover
lost market value within a reasonable period of time.
Our capital remained at high levels despite this
multitude of pressures, but volatility in our equity
portfolio caused ratings agencies to place our ratings on
negative review and then lower them. By year-end 2008,
A.M. Best Co. and Moodys Investors Service had Stable
outlooks on our ratings. Our
property casualty group rating
from A.M. Best remains in
the Superior category, but at
A+ it is a notch down from
the A++ we enjoyed for many
years. This rating of our
financial strength continues
in the top 10 percent of the
more than 1,000 insurer
groups A.M. Best rates. Our
insurance companies are
capitalized at levels far
exceeding regulatory
requirements, increasing our
flexibility through all periods to invest in and expand
our insurance operations.
When all was said and done, net income totaled
$429 million, about half of 2007 earnings. Year-end
book value was $25.75 compared with $35.70 at
year-end 2007. We take little consolation in knowing
that we were not alone in these disappointments and
that broader economic and natural forces were at work.
And we cannot say the worst is over. The first two
months of the year have made it clear that we need to
brace for another difficult year in 2009.
What we will say is that our agent-centered business
model is sound and gives a solid foundation for our next
steps. We were not sufficiently prepared for all that
happened in 2008. We cannot change the past, but we
certainly have learned from it. We took major steps in
response to the current extreme conditions, and we

are confident we are building a more competitive and
agile company.
We have the resources, the plans and the people to
create long-term value for shareholders, policyholders,
agents and associates.
Our efforts in 2008, to
be multiplied in 2009
and beyond, will firm up
our position of strength
and we will emerge a
stronger competitor
than ever over the next
five years.
Strong
Financial
Resources
We are going forward
with strong liquidity
and capital.
Cash on hand was
more than $1 billion at
year-end. Implementing
revised portfolio
guidelines, we sold
selected equity
investments in 2008 and
early 2009, locking in
gains and reducing
volatility by increasing
sector and company
diversification. In
particular, we took our
financial sector holdings
to 12.4 percent of the equity portfolio at year-end
2008 from 56.2 percent at year-end 2007. We ended
the year with no single stock accounting for more
than 14.5 percent of the equity portfolio, which

remained in an overall unrealized gain position at
year-end.
Reducing our financial sector holdings included
taking gains on our position in Fifth Third Bancorp,
reducing it to 12 million shares at year-end 2008.
During January 2009, we sold most of our remaining
financial sector equity holdings, including the last shares
from our Fifth Third holding.
Ample reserves and an effective reinsurance program
have protected our liquidity, allowing fixed income
investments to mature and equity investments to
appreciate over time. Even in a tough year like 2008,
cash flow provided adequate funds to pay claims. We
have never been forced to prematurely sell securities to
pay claims. At its year-end value of $5.8 billion, our
diversified, highly rated fixed income portfolio more
than covers our total insurance reserves.
The parent company has healthy capital and
flexibility, with $1.3 billion of cash and invested assets
at year-end 2008. In the current environment, we intend
to preserve capital and did not repurchase shares in the
second half of 2008. Low debt leverage also increases
our flexibility. We had access at year-end 2008 to
$176 million on our lines of credit, and our three issues
of long-term debt are not due until 2028 and 2032.
Through 2008, your Cincinnati Financial cash
dividends increased annually for 48 consecutive years,
a record that fewer than a dozen U.S. companies can
claim. Generally, our directors declared the increases
during the first quarter. In February 2009, the board
maintained the quarterly dividend at 39 cents,
postponing discussion of a 2009 increase to later
in the year. Many other companies have cut dividends in
recent months, including some that contributed to our
investment income. As we make quarterly 2009
dividend decisions, well carefully weigh our dividend
record, responsibly considering our resources and our
initiatives to preserve capital and grow profitably over
the long term.
With $3.360 billion of
property casualty statutory
surplus and a ratio of written
premiums to surplus of only
0.9 to 1, we are deploying
capital and resources where
they will help us expand
and succeed.
Strategic
Operating Plans
We are working toward
improved efficiencies to
benefit agency and
company profitability.
We will achieve a good
return, over time, on staffing expense for new territories
and states and on new technology to improve ease,
efficiency and profitability for our agencies and our
company. We have shortened timelines for delivery of
our next generation processing systems. We now expect
our new system for commercial packages and
commercial auto to go live in 10 states in 2009 and
most other states in 2010. Our personal lines system
now is scheduled to move to a faster, friendlier format
in early 2010. Other business data initiatives will

improve our underwriting and pricing by assuring
quality data for use in models for rate-making,
forecasting, risk management and other decisions.
Accurate exposure, loss and expense data will help us
address weaker property casualty underwriting
profitability, partially masked in 2008 by reserve
releases. Years of ongoing underwriting efforts, as well as
allocation refinements, have built a trend of lower than
expected losses from previous years, helping current
calendar year profitability. This favorable reserve
development more than offset our unusually high
catastrophe losses in 2008, allowing a statutory
combined ratio near breakeven at 100.4 percent
versus an industrywide ratio of 104.7 percent.
We continue in 2009 to apply the same prudent
reserving philosophies and practices that have
consistently benefited our results.
Our growth initiatives are succeeding.
Prices continue to be low across the property casualty
industry on both new and renewal business. The
independent insurance agents who market our policies
are helping their clients weigh value and service in
addition to price, and they continue to award us a
generous portion of their carefully selected new
business. Agents are using our three-year policy term as
a distinct sales advantage to attract and retain
commercial accounts. Agents find that many businesses
will pay slightly more to get stable costs on selected
coverages, allowing them to budget a known amount.
We wrote $368 million of new business premiums, up
13.1 percent in 2008.
The $43 million increase
is about the same
amount as the sum of
contributions from
agencies appointed since
2004 and from our new
surplus lines operations.
Production generally
ramps up over the first
10 years as agency
relationships mature.
We continue to expand
to new agencies and
states, opening central
Texas in 2008 and
making plans to open
Colorado and Wyoming

in 2009. Over time, our westward movement is
improving our spread of risk and potentially mitigating
catastrophe volatility.
We offer the full range of products our property
casualty agencies need to diversify their revenues and
meet the needs of people in their local communities.
We will see growing contributions from our surplus
lines operation  which wrote $14 million in 2008, its
first year of operation  and from sales of personal lines
by agencies that previously marketed only our
commercial policies. By steadily improving our
processing systems and pricing accuracy, we are helping
agencies see the value of selling our personal lines.
Approximately 80 percent of our property casualty
agencies sell our life insurance products, which feature
simplicity, guarantees and service supported by our
investment in imaging and workflow technology.
Committed People, Clear Priorities
We are setting the stage for continuity of leadership,
values and commitments.
Our own jobs changed in 2008. As Vice Chairman
Jim Benoski was preparing to retire in early 2009 from
active employment, we moved ahead with executive
transitions. Jack and Jim continue to lead our board
as chairman and vice chairman, respectively. As of
July 1, 2008, Jim passed the presidential torch to Ken,
who stepped up from his post as chief financial officer;
and Jack likewise turned over chief executive officer
duties to Ken. Steve Johnston joined the company to be
our new chief financial officer, bringing 25 years of
experience in insurance accounting, finance,
investments, actuarial and technology.
This is the first time your
companys president and
CEO came from outside our
insurance underwriting and
marketing ranks. With his
40+ years with the company,
Ken is the right leader as we
sharpen our focus on our
efficiency, enterprise risk
management, data quality,
modeling and technology
initiatives to support smart
growth and benefit agents,
policyholders and
shareholders. His selection
was timely; together he and
Steve brought their seasoned
financial perspectives and a
measured, analytic approach
to our decisions as events of
the second half of 2008
unfolded.
At the same time,
responsibilities for several
other executive officers
broadened or changed,
allowing all to round out their
experience and qualifications
to advance in our next
generation of leadership. J.F. Scherer, head of
our Sales & Marketing area, now is executive vice
president. Tom Joseph now is president of

The Cincinnati Casualty Company, heading up our
personal lines operation. Bud Stoneburner has taken the
reins in our largest business area, commercial lines.
There also were director transitions in 2008.
Ken joined the board, and Dirk Debbink departed after
he was recalled to active military duty and appointed
Vice Admiral and Chief of Navy Reserve, U.S. Navy.
We thank him, both for his service to your company
and for his service to our country.
All of our directors and members of our executive
team share a firm commitment to perpetuate our
agent-centered values. Agents and policyholders can rely
on our board and executive team to continue
differentiating Cincinnati in the marketplace making
decisions at the local level, providing superior claims
service and managing the company to preserve and
build financial strength.
We made it a priority to honor relationships and act
with integrity as we carried out change.
Some difficult and necessary actions better
positioned your company for the future. Foremost
among those hard decisions was the one relating to
our 2009 dividend, discussed earlier in this letter.
We also moved most of our associates out of
our defined benefit pension plan and into
401(k) accounts with matching contributions. Our
goal was to make our benefits program more attractive
to many current and prospective associates while reducing
potential volatility on our company balance sheets.
Finally, in February 2009, we chose to close CinFin
Capital Management, our asset management subsidiary,
suspending fees and facilitating smooth transitions to
other money managers. Over its 10 years, CinFin
Capital had been profitable, but further growth would
have required resources we believe we can use more
strategically to advance our core insurance business.
Lasting Shareholder Value
We are committed to building long-term rewards for
you, our shareholders.
Near-term indicators for the U.S. economy continue
negative, although our agents and field representatives
are reporting some positive signs that insurance pricing
is flattening and may firm. Your company, like most
American companies, has never before operated through
a period of comparable uncertainty and volatility in the
financial markets. In 2008, our total shareholder return
was a negative 22.5 percent compared with negative
36.9 percent for the S&P 500 Index.
The many unknowns we all face include details and
impacts of government programs affecting everything
from banking, insurance and taxes to energy policy,
public construction and
entitlements. Even healthy
companies like ours that are
not recipients of government
funds cannot predict how
TARP, the stimulus package
or other programs may change
the environment for our
industry or for the businesses
and people we insure.
Any specific 2009
performance targets we could
suggest would be heavily
conditioned by assumptions.
In line with the long-term
perspective we use to set
decisions and initiatives, we believe guidance that looks
beyond 2009 will prove more helpful to those who
study the outlook for our future success.

Our resources, plans and people give us the flexibility
we need to assure resilience through all underwriting
cycles and economic markets. We are confident that for
the five-year period spanning 2010 to 2014, we can
achieve an average of 12 to 15 percent for a measure we
are calling our value creation ratio. This ratio is the sum
of the growth rate of book value per share plus the ratio
of dividends declared per share to beginning book value
per share. It captures the drivers of our progress in
building shareholder value  the contribution of our
insurance operations, the success of our investment
strategy and the high priority we place on paying cash
dividends to you.
We believe this target is achievable as long as there is
some firming of commercial insurance prices this year,
and by the end of 2010, a turn toward recovery in the
economy and financial markets. Will we achieve this
level of value creation in 2009 or in every year of the
five-year span? This is unlikely. Our five-year target
represents an average, recognizing that the inevitable
ups and downs of markets and business cycles,
catastrophes and, yes, looming economic uncertainties
may take a toll in some years. Our focus is not on
timing, but on plans that build success over time.
Short-term pressures cannot alter our focus on
achieving success over time through our flexible,
relationship-based and agent-centered business model.
We are more determined and convinced than ever that
we will recover our stride and step up to higher levels of
performance over the coming years.
Respectfully,

John J. Schiff, Jr., CPCU Kenneth W. Stecher
Chairman of the Board President and Chief Executive Officer
