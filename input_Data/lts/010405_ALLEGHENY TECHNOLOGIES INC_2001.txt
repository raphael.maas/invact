The year 2001 was a very challenging year for our Company. We experienced business
conditions that have not been seen for decades.
When the year began, a market dichotomy existed for Allegheny Technologies products.
Demand for most of our high performance materials was quite strong, while markets for most
of our stainless flat-rolled products were under substantial pressure and markets for our industrial
products were showing signs of weakness. In addition, extraordinarily high energy costs,
particularly at our Wah Chang operation in the Pacific Northwest, were hurting earnings. These
conditions held through the first half of the year. By the start of the third quarter, we began to
see signs that the weak U.S. economy was showing improvement and energy costs had
returned to more normal levels.
Then the tragedy of the terrorist attacks of September 11 struck. Those terrible events not only
gave a severe jolt to our confidence as a free and safe people but also severely impacted our
markets, particularly commercial aerospace. Since then we have been working with intensity
to expand our marketing focus to areas less influenced by that event and less affected by the
continuing recession. Recognizing the seriousness of the impact on many of our key markets,
we also accelerated our focus on major cost reductions and productivity gains.
Reflecting the overall economy, earnings declined throughout 2001. Revenues stayed
relatively consistent through the initial three quarters but slid by 8 percent in the fourth quarter.
We recognized the implications of the recession early in the year and took a number of actions to
reduce costs and increase cash flow. Those actions were quite successful. By year-end, we had
generated $135 million in cash flow by reducing managed working capital and delaying non-critical
capital spending. Additionally, our cost reduction efforts totaled $115 million before the effects of
inflation. As a result, we preserved financial liquidity, maintained our $0.80 per share annual cash
dividend and kept an investment grade rating on our public debt. This became critically important in
December when we issued $300 million in new ten-year notes and entered into a new $325 million
revolving bank credit agreement.
Excess global stainless steel melt capacity remains a serious negative. While stainless steel flatrolled
products are not included in the Section 201 trade case, antidumping and countervailing
duty cases won in 1999 remain in effect for stainless steel sheet and strip as well as coiled plate
products. The industry is carefully monitoring unfairly traded imports and can be expected to take
timely action when appropriate. We continue to urge the Bush Administration to remain vigilant in
the enforcement of our countrys fair trade laws. Meanwhile, we plan to remain
cost competitive through ongoing programs of aggressive cost reduction and
productivity improvement. One of our most significant cost reduction efforts in 2001
was the permanent idling of our less efficient Houston, Pa. stainless steel melt shop.
That action reduced our stainless steel melt capacity by nearly 30 percent.
However, the idling action does not diminish our stainless steel rolling and finishing
capacity, where we add the most value to our products, nor does it diminish our
capability to grow in this market.
Our strong emphasis on safety continued in 2001 as evidenced by the Companys
OSHA Recordable Incident Rate improving 17 percent and the Lost Workday
Case Rate improving 16 percent. Over the last two years, both of these measures
of safe operations have improved by over 40 percent. Safety will continue to be
our number one operating priority as we pursue our goal of zero accidents.
In some respects, its difficult to reflect on 2001 as a year that held anything but disappointments:
 September 11th and all of its negative implications,
 The worldwide recession,
 The weakest market for commodity stainless flat-rolled products in memory,
 Volatile energy costs, particularly during the first half of the year, and
 A strike at our Wah Chang operation in Albany, Oregon.


Our management team responded to these challenges, and we are proud of a number of
significant accomplishments achieved during this very difficult year:
 Record managed working capital reduction,
 Record shipments to the commercial aerospace markets for nickel-based alloys and
superalloys and premium titanium alloys,
 Strategic capital investments to expand and strengthen the capabilities of our High
Performance Metals segment,
 Continued major improvements in workplace safety,
 Operating profitability achieved at STAL, our new Precision Rolled Strip products joint venture
in Shanghai, China,
 Market expansion for Precision Rolled Strip products in Europe and Asia, and
 Improved liquidity through issuance of ten-year notes and finalization of a new revolving
bank credit agreement.
In 2001, we launched Operational Excellence, a comprehensive continuous improvement initiative
based on lean manufacturing principles that will enable us to further improve safety, reduce
costs, shorten lead times, reduce inventories and improve customer satisfaction. Operational
Excellence has rapidly become imbedded in our management philosophy and provided the
operational platform necessary to exceed our cost reduction and managed working capital
improvement targets in 2001.
In 2002, we are prepared for another difficult year. Our top priority is to continue reducing costs
and generating cash. We have identified $100 million of additional cost savings opportunities. Our
capital expenditure plan has been reduced to $50 million, well below our annual depreciation.
We have targeted another $65 million in managed working capital reductions. Success in achieving
these goals should give us the necessary financial capacity to weather this economic downturn.
Despite current uncertain economic conditions, we believe our Company is well positioned to
emerge strongly when the economy recovers. We have leading positions in a number of highvalue
specialty materials products with compelling growth
fundamentals, as depicted in the charts on pages 3 and 4. Our
reputation for quality, service, delivery reliability and technical
leadership remains among the very best within the industries we
serve. The global markets we serve are diversified and growing.
Our capital base is solid, and we have a history of consistently
generating free cash flow.

ANNUAL REPORT 2001
We are convinced that the long-term prospects
for the Company are promising. This is a key
reason why we emphasize our Coordinated
Business Development and other strategic
balanced growth initiatives. Coordinated
Business Development provides the commercial
framework to bring the Companys capabilities
together to identify and develop growth strategies
across our wide array of specialty materials. We also continue to direct our leading-edge
technologies to customer-focused specialty materials solutions utilizing high-value products.
Our dedicated and talented employees and their commitment to excellence are the fuel that
sustains our Company. Because it all starts with an order, we thank our customers for their
continued confidence. We also express our sincere appreciation to our Board of Directors for
providing guidance in strategic development and wisdom in the many decisions that are required
to maintain a successful enterprise. Our Board was strengthened in 2001 with the addition of
James C. Diggs, senior vice president and general counsel of PPG Industries, Inc. Jim brings an
additional global industrial perspective to our Board. We extend a special note of appreciation to
Ray Groves who, due to the demanding requirements of his new position as president and chief
operating officer of Marsh Inc., retired as a member of the Board of Directors in February 2002.
Rays advice and counsel have been consistently valuable.
We also offer our sincere thanks to our shareholders. You have many investment choices. We
are honored that you have chosen Allegheny Technologies and will strive to reward your investment.
Our goal is to create shareholder value. We recognize that this goal goes hand-in-hand with
our commitment to integrity, both in how we conduct our business and how we report our
financial results.
Sincerely,
 
Bob Bozzone, Chairman President and Jim Murdy, Chief Executive Officer
March 2002
