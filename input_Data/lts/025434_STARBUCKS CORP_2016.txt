Dear Shareholders,
With this years letter, we celebrate another record-breaking year of financial
performance, as well as share with you what may be the most important strategic
developments for our company since Starbucks first changed how the world
consumes coffee.
After 45 years, Starbucks has become one of the most respected and admired companies in the world, an enduring
global merchant that today delivers the Starbucks Experience more than 85 million times per week at more than
25,000 stores in 75 countries.
As I reflect on our storied history, I am so proud of the success we have achieved but am even more proud of how
we have achieved it. By staying true to our mission, values, and guiding principles, Starbucks is redefining the role
and responsibility of a for-profit companycreating long-term value for our shareholders while enhancing the lives
of our partners and people in the communities where we live and work.
I am confident that the trust our customers have in the Starbucks brand will provide a strong foundation as we
embark on a new phase of global growth.
As we recently announced, Kevin Johnsonour current president, chief operating officer, a seven-year board
member, and my partner in running every facet of Starbucks business day-to-day over the last two yearswill
assume the duties of chief executive officer on April 3, 2017.
I will remain executive chairman, focusing full-time on our next wave of retail innovation, as well as our ambitious
social impact agenda.
This transition comes at a time when innovation and effectively scaling our business around the world are equally
essential to our future success. That is why I personally asked Kevin to accept the ceo role.
Kevins value to Starbucks is indisputable. As a board member, Kevins insights helped us scale our business, as well as influenced how we seamlessly integrated technology into our operations and the consumer experience. When Kevin became coo in 2015, his global management expertise helped make the past two fiscal years the best performing years in the history of the company.
Kevin also shares our values, and after collaborating for almost a decade, we have formed a mutually respectful partnership, and will continue to work together in the years ahead.
In light of this transition, Kevin and I have co-authored this years shareholders letter. I will begin with a review of our financial performance in fiscal 2016, and introduce the multi-year strategy that Kevin and I also have co-authored. Then Kevin will take you through more highlights of our plan, as well as a review of our most recent social impact initiatives.
Given all that we have to discuss, this years letter is a bit longer than usual, but it is important that you understand our vision and share our confidence in our future.
Personally, I could not be more excited about where Starbucks is headed, or more proud of what we have achievedespecially this past year.
Delivering Record Revenue, Operating Income, and Earnings
In 2016, Starbucks Coffee Company continued to deliver quarter after quarter
of record revenue and profit growth:

For the full-year fiscal 2016, Starbucks grew consolidated revenue to a record $21 billion, an 11 percent increase over last year. Growth was primarily driven by a 5 percent rise in global comparable store sales and
the opening of 2,042 net new stores globally, which are outperforming prior classes of new stores.

Consolidated operating income was a record $4.2 billion, a 16 percent increase over fiscal 2015.

Consolidated operating margin of 19.6 percent was a notable 80 basis points higher than the prior years operating margin.

Non-GAAP earnings per share that excluded the extra week in fiscal 2016*, was a record $1.85, up
17 percent over fiscal 2015 non-GAAP earnings per share*.

Through dividends and share repurchases, Starbucks returned a record $3.2 billion to shareholders.
As always, our strong performance would not have been possible without the extraordinary effort and talent of our more than 300,000 partners who proudly wear the green apron. The customer experiences they create in our stores improved performance and drove business around the world.
In fiscal 2016, our Americas operating margin expanded 110 basis points to 25.3 percent; Europe, the Middle East, and Africa (EMEA) achieved an operating margin of 13.5 percent, and the China Asia-Pacific (CAP) margin grew
60 basis points to 21.5 percent, led by our fastest growing market, China. In the U.S., currently our largest market with more than 13,000 stores, annual revenue grew by 13 percent, with our newest class of U.S. stores delivering record year-one sales. Throughout the year, we continued to capture growth opportunities despite the many economic, geopolitical, and consumer challenges around the world.
Beverage and food innovation also contributed to our global growth as customers around the world traded up to premium beverages and embraced new beverage platforms such as Nitro Cold Brew, Starbucks Reserve coffees, Sweet Cream Cold Brew, and Teavana shaken iced teas. Innovation within our food platform, enabled by our 2012 acquisition of La Boulange, continued to drive increased revenue, particularly in our morning business.
Our achievements in fiscal 2016 reflect the strength of our core business, the relevance of the Starbucks brand around the world, and our commitment to innovationall strengths that will serve us well in the years ahead, especially as we continue to address the seismic shifts in consumer behavior that are reshaping the retail industry.
Innovating in a Changing World
Three years ago, I shared my observations about the game-changing effects of e-commerce and mobile shopping on bricks-and-mortar retailers. In the years since, major retailers continue to close stores, reduce new-store openings, or move their businesses completely online.
Starbucks, in contrast, has increased our store count and grown revenue and earnings to record levels.
While we are not immune to shifts in consumer behavior, our ability to navigate through them is directly linked to the experience that we have long created for our customers. From the U.S. to China, Starbucks stores remain a Third Place destination, a welcoming environment that serves the worlds finest coffees and handcrafted beverages while providing a sense of community and emotional connection.
Going forward, Starbucks will stay true to our core reason for being, but we must continue to innovate.
Todays digital age requires that all retailers reimagine the experiences they create in their physical stores as well
as online. Given this new reality, Starbucks five-year plan is our most aspirational one yet, designed to achieve three goals: Elevate the Brand, Elevate the Customer Experience, and, ultimately, Amplify Starbucks as a Destination around the world.
We will achieve these aspirations by focusing on several key initiatives outlined below.
Creating an Ultra-Premium Retail Experience
In 1971, when the first Starbucks store opened in Seattles Pike Place Market, we began a journey that forever changed how the world experiences coffee. In 2014, we opened the Starbucks Reserve Roastery & Tasting Room
in Seattle and once again introduced to the world a reimagined coffee experience.
The 20,000-sq.-ft. Roastery has become a magical destination for Seattleites and visitors from all over the world, as well as a working lab for breakthrough product innovations that we have rolled out to thousands of Starbucks stores. As the worlds most immersive, coffee-forward retail experience, the Roastery represents our next evolution as a global merchant.
In upcoming years, we plan to open 20 to 30 new Roasteries in key global cities, with the next Roastery opening in Shanghai in 2017 followed by New York and Tokyo in 2018. Each Roastery will surprise and delight customers with stunning interior design, our Starbucks Reserve brand of exclusive micro-lot coffees, on-site roasting facilities, coffee bars that showcase the latest brewing methods, and a delicious array of artisanal food offerings.
The Roasterys ultra-premium experience has cast a halo on the entire Starbucks brand, and with sales that far exceed regular Starbucks stores, the Roastery represents exciting new growth opportunities for the company.
Enhancing the Customer Experience at Scale
As we open more Roasteries, we also will create a new class of retail stores. In the years ahead, we expect to open approximately 1,000 Starbucks Reserve stores around the world, designed to bring elements of the Roastery to life for millions more of our customers.
Like the Roastery, Starbucks Reserve stores will create an ultra-premium experience, offering our exclusive Starbucks Reserve brand coffees and multiple brewing methods paired with artisanal food from our new partner, Italian baker Princi. Development of Starbucks Reserve stores is already under way and, based upon our experience with the Roastery, we expect them to deliver double the financial performance of a traditional Starbucks store.
To extend this coffee-forward, premium retail experience even further, aspects of the Starbucks Reserve stores will be threaded into new and existing Starbucks stores with new Starbucks Reserve bars. We are very pleased that early performance of more than 15 Starbucks Reserve bars in Starbucks stores throughout the U.S. is exceeding expectations.
I will oversee the Starbucks Reserve brand and the opening of Roasteries, Starbucks Reserve stores, and Starbucks Reserve bars in my role as executive chairman, and I truly feel as passionate about this next wave of retail and coffee innovation at Starbucks as I did about our first espresso bar, which I personally opened so many years ago.
At this point in our letter, I am honored to pass the pen to Kevin to share more details of our strategic plan, which we already have begun to execute together with much success.
Extending the Starbucks Experience with Our Digital Capabilities
As Howard wrote, our five-year strategic plan is the most aspirational plan in the history of Starbucks. I am honored, as incoming ceo, to lead a company so deeply committed to innovation, operational excellence, and our mission of inspiring human connection as we scale our business around the world.
Leveraging technology is a key part of our strategy as we further extend the Starbucks in-store experience with a simple, elegant digital experience for our customers.
In response to the seismic changes in consumer behavior that Howard described, Starbucks invested early to
create an industry-leading mobile and digital experience that is based on a powerful combination of
Starbucks Rewards, Payment, Ordering, and Personalization capabilities integrated in ways that result in increased customer engagement and spend.
We continue to evolve and enhance Starbucks mobile and digital platform. In 2016, Starbucks Rewards successfully transitioned from a frequency-based loyalty program to a spend-based program in the U.S., supporting an 18 percent increase in active membership and a 94 percent retention rate, our highest ever.
By the end of fiscal 2016, payments made by mobile devices (Mobile Payment) represented 25 percent of all transactions in our U.S. company-owned stores, up from 20 percent a year earlier. In addition, Mobile Order and Paywhich allows our customers to place and pay for food and beverage orders prior to entering a storecontinues to be embraced at a growing rate, providing significant speed and convenience for customers and partners, especially during our busiest times.
Recently, we launched Personalization, our newest digital innovation. Personalization enhances the customer experience by providing offers and product suggestions that are tailored to individual customers. We are excited
about the potential of Personalization to increase customer engagement and help us grow the business. Already, we are seeing customer response rates at triple the level we saw from non-personalized communications.
Going forward, Starbucks mobile and digital platform will continue to elevate the customer experience and drive business outcomes as we further innovate and extend our capabilities to more customers around the world.
Focusing on China, a Key Growth Market
Perhaps nowhere has the Starbucks Experience been embraced more enthusiastically than in China, a country we first entered 18 years ago. In fiscal 2016, China was our fastest growing market and had another year of record financial performance.
Today, our 2,500 stores across 118 cities in China serve more than five million customer visits per week, and are among our most elegant and profitable Starbucks stores in the world.
Yet, we are just beginning to realize our potential in this region. Chinas emerging middle class and ongoing economic reforms will benefit both our ready-to-drink coffee and retail businesses, and during the next five years we intend to double our store count in China to 5,000 in more than 200 cities. Increasing engagement with our Chinese customers also presents significant opportunity as we expand our mobile and digital platform to reflect the lifestyles of Chinese consumers, and our unique investments in our partners will continue to set our brand apart here. Partner housing allowances, training, and forums that honor our partners families are among our programs that contribute to the deep emotional connection that our Chinese partnersand their parentsfeel for the company, which extends to the connections our people create with their customers in our stores.
Long-term, we are committed to building our business in China methodically and respectfully as the country evolves into our largest market in the world.
Extending the Brand Outside Our Stores
In 2016, we continued to gain market share in the at-home and ready-to-drink coffee markets as annual revenue
in our high-margin Channel Development business grew by 12 percent to $1.9 billion.
Starbucks products are now distributed in more than 50 countries around the worlddown grocery aisles and in hotels, on college campuses and in offices. We finished fiscal 2016 as the number one K-Cup brand in the U.S., where our roast-and-ground coffee also remains the number one premium packaged coffee brand. In Europe, the introduction of our espresso capsules that are compatible with Nespresso machines in the U.K. and France is off to a strong start, and distribution throughout the region will continue in 2017.
We also introduced two significant partnerships in 2016. In the U.S. and Canada, Anheuser-Busch will soon launch our new ready-to-drink Teavana cold tea beverages, with distribution expanding throughout 2017. In China, our regional partner Tingyi Holding Corp. is now producing and distributing Starbucks ready-to-drink Frappuccino bottled beverages throughout the country.
As we look to the future, we intend to continue to build and invest in unique partnerships that leverage the power
of our brand in ways that no other consumer-products company comes close to matching.
Using Our Scale for Good
In 2016, Starbucks remained committed to enhancing the lives of our partners, coffee farmers, the communities where we do business, and people in communities beyond our own.
On behalf of Howard and myself, I am proud to share an overview of the recent impact we have had in the U.S.,
in particular:
To serve those who serve our country, to date Starbucks has hired 8,000 veterans and military spouses; dedicated 30 Starbucks Military Family Stores to honor service members and their families; and sent care packages to thousands of troops.
We are extending job opportunities to Opportunity Youth by hiring 10,000 young men and women who
are not in school and not employed, and helping to connect hundreds of thousands of others to more than 50 companies to secure employment.
We opened stores in low- to medium-income communities in Ferguson, Mo.; Phoenix, Ariz.; Queens, N.Y.; and Chicago, Ill., as part of ongoing efforts to help economic development and revitalization by creating jobs, providing skills training for youth, and investing in minority-owned contractors and suppliers.
During our annual Global Month of Service in the U.S., 17,000 partners joined almost 54,000 volunteers who completed 1,669 projects to benefit young people.
Our first original series, Upstanders, celebrated exceptional civic engagement through 10 emotional profiles in written, video, and podcast formats, which together garnered more than 72 million views.
Since announcing Starbucks FoodShare program in March 2016, 347,000 ready-to-eat meals have been distributed to local communities, and by 2021, 100 percent of food available for donation from participating company-operated U.S. stores should amount to almost 50 million meals shared.
And thanks to incredible customer participation, Starbucks One Tree for Every Bag Commitment program has raised to date enough money to plant 22 million new disease-resistant coffee trees for coffee farmers around the world.
We are also effecting positive change by extending more educational and training opportunities to our partners, supporting local charities, volunteering in communities, helping partners with housing costs, and helping vulnerable youth secure employment.
These are only some of our achievements, and we hope you share our pride in our partners efforts to make a difference.
Going forward, you should know that I share Howards belief that Starbucks mission, values, and guiding principles must remain core to who we are as a company, and as leaders. We have exceptional talent on our Senior Leadership team who also share this belief, and together we are committed to enhancing the lives of those in the many communities we serve.
We are also pleased to announce that three values-based leaders from world-class companies have been nominated to the Starbucks Board of Directors. Please join us in welcoming Rosalind Brewer, the Executive Vice President, President, and Chief Executive Officer of Sams Club**; Satya Nadella, the Chief Executive Officer of Microsoft; and Jrgen Vig Knudstorp, Executive Chairman of the LEGO Brand Group. The diverse experiences of these talented individuals will make all of us at Starbucks better, and we are extremely honored to have them join our journey after their election at our 2017 Annual Meeting of Shareholders.
In closing, Howard and I are incredibly optimistic about the companys future, and you have our shared commitment to ensure that Starbucks financial performance continues at the levels that it has in the past.
For decades, Starbucks has succeededand our shareholders have benefiteddespite macroeconomic challenges, shifts in consumer behavior, and market surprises. Going forward, we know that challenges, shifts, and surprises will continue, but we are poised to navigate them as we always have, by playing the long game as we embrace reinvention, while staying true to our core reason for being.
On behalf of everyone at Starbucks, we thank all of our shareholders for your ongoing support and confidence in our future.
With warm wishes and respect,
Howard Schultz and Kevin Johnson