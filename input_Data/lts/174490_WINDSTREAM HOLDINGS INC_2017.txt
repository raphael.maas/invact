Dear Shareholder:
2017 was a very productive year for Windstream. We positioned the company for the growth
opportunities ahead and improved the financial and operating results for almost all metrics across the
business. Among other accomplishments, we completed the EarthLink and Broadview acquisitions;
developed and launched SD-WAN Concierge, our fully managed network solution to meet the evolving
demands of the cloud; and introduced OfficeSuite, Broadview�s best-in-class Unified Communications as
a Service product, across our entire footprint.
To sharpen our focus, we moved to a two-business unit structure: ILEC Consumer & Small Business
and Enterprise & Wholesale, and refreshed the branding for both units to reflect the transformation
Windstream is undertaking to meet our customers� evolving needs.
During the year, our board of directors revised the company�s capital allocation strategy, eliminating the
quarterly dividend. The board concluded this was in the long-term best interests of our shareholders
because it afforded us the opportunity to pay down additional debt, which we believe is the most
effective way to generate strong returns for all of our stakeholders.
For 2018, we have five key priorities. First, we will continue to advance our industry-leading Enterprise
and Wholesale product and service capabilities, including SD-WAN and OfficeSuite, as well as security
and on-net solutions. Second, we will launch next-generation broadband deployment techniques that
are faster and more cost effective, helping us to improve customer retention and grow market share.
Third, we will further simplify our business and transform customer-facing and internal tools through
our multi-year information technology integration project. Fourth, we will drive revenue improvements
through enhanced sales and improved customer retention in both business units. Lastly, we will
continue to work to optimize our balance sheet.
We are a fundamentally different company today than we were a year ago, and we will continue to
evolve. The management team and I are confident that we are on the right path to improving revenue
trends, driving adjusted OIBDAR growth and creating value for all stakeholders. I am proud of the hard
work by everyone in our company in 2017, and I remain grateful for the faith our shareholders have
placed in us.
Sincerely,
Tony Thomas
President and Chief Executive Officer
March 31, 2018