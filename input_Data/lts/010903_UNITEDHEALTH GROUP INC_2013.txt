To our shareholders:

As we send this annual report to you, 2013 and our many
accomplishments that year seem well in the past. Today,
health care markets in the United States remain active and
continue to evolve as new policies and regulations and
the long-standing pressures on the health care system are
meeting market realities. This is the beginning of an era of
change and adjustment in which we will need to keep the
ultimate goal clearly in all our minds  an improved health
care system where our health care resources are better
deployed and accessed . . . delivered to more Americans
at lower costs, to them and to our society.
We are seeing similar dynamics as we deepen our
engagement in countries outside the United States: the
desire by governments, payers and providers to make
more ef cient and consistent use of resources and respond
more effectively to the growing needs of their people.
The employees of UnitedHealthcare, Optum and
UnitedHealth Group have never been more engaged in
these essential efforts. Working together in 2013, they
meaningfully advanced the performance capabilities of this
enterprise and delivered real value to those we are privileged
to serve. As a consequence, our enterprise continued to
grow and diversify. The consistency of our employees
commitment to performance and service translates into
measurable improvements that have a positive impact
on peoples health and in their lives. We thank them for
their remarkable efforts on behalf of those we serve and
those of you who invest in this enterprise.
The culture and capabilities of our enterprise are powerfully
aligned with our mission  striving for a better health care
environment that better serves at both the individual and
national level. We see more change and further challenges
continuing over the next few years as new social policies and
higher expectations for better quality and more affordable
care settle into the health care economy. We are well
deployed across the health system and participating in
many aspects of these changing market dynamics.
We expect to grow, strengthen and further diversify to
meet the changing needs of consumers, care providers and
bene ts sponsors, and continue to evolve and adapt to
the opportunities we see for the future. As we have done
throughout our history, we will bring to bear the power
of information, clinical insight and enabling technology
to increase the value we are delivering in new and more
modern ways across more vibrant and effective health care
markets, better serving more people.
What will not change will be our dedication to our mission:
to help people live healthier lives and to help make the
health system work better for everyone. We will continue
to embrace a culture of integrity, compassion, innovation,
relationships and accountable performance, all anchored
by fundamental execution disciplines and service to others.
And we remain steadfast in the  scal responsibilities and
disciplines that sustain our enterprise and provide an
appropriate return for the capital you entrust to us.
Thank you.
Sincerely,
Steve Hemsley
President and Chief Executive Officer