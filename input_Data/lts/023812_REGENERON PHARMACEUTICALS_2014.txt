Dear Shareholders,
For over a quarter of a century, Regenerons mission has
been to build an innovative company that consistently
and repeatedly brings new medicines to patients with
serious diseases. With EYLEA (aflibercept) Injection
already making a tremendous impact by returning and/
or sustaining vision in people with a number of different eye diseases, and additional therapies for serious
diseases in late-stage clinical development, Regeneron is
increasingly positioned to deliver on this vision.
2014 was a landmark year of progress across all aspects
of our business. We delivered strong earnings growth, inFUHDVHGVDOHVDQGJDLQHGQHZDSSURYDOVIRURXUWDJVKLS
product EYLEA, made critical advances in our late- and
earlier-stage pipeline, and expanded our workforce and
infrastructure to support future growth. Decades of innovation and hard work have led to a world-class and uniquely
homegrown pipeline of 15 therapeutic candidates with
the potential to help patients with a range of serious
diseases, including heart disease, cancer, life-threatening
infections, asthma and other allergic diseases, as well as
LQWDPPDWRU\FRQGLWLRQV
At the end of 2014, based on strong positive data from
10 Phase 3 trials in people with hypercholesterolemia,
ZHDQGRXUFROODERUDWRU6DQR?VXEPLWWHGDSSOLFDWLRQV
to U.S. and European Union regulatory agencies for
PRALUENT (alirocumab) Injection, our PCSK9 inhibitor for
lowering bad low-density lipoprotein (LDL) cholesterol.
PRALUENT has received priority review status from the
U.S. Food and Drug Administration (FDA), and we expect
DFWLRQLQ-XO\:HDUHZRUNLQJKDUGZLWK6DQR?WR
prepare for a successful commercial launch upon potential regulatory approval.
We also advanced the Phase 3 clinical program for sarilumab,
our IL-6 antibody for rheumatoid arthritis, and expect to
submit our U.S. regulatory application in the second half
RI$QGZHPRYHGGXSLOXPDERXU?UVWLQFODVV,/
IL-13 blocking antibody, into Phase 3 studies. In 2014,
dupilumab was granted Breakthrough Therapy designation by the FDA for atopic dermatitis, based on promising
early clinical results. The FDA grants Breakthrough Therapy
designation to expedite the development and review
of drugs that have shown strong potential for serious
or life-threatening conditions in need of new treatment
options. Dupilumab also posted positive mid-stage results
in asthma and in patients with nasal polyps and associated
chronic sinusitis. In 2015, the FDA agreed that our Phase
2b study in asthma could potentially serve as one of the
two required pivotal studies for this indication.
Our groundbreaking Regeneron Genetics Center
(RGC), a human genetics initiative of unique size, scale
and scope, launched in January 2014 and has already
exceeded our expectations. The RGC is now sequencing
at a rate of about 80,000 individuals per year, and intends
WR?QLVKWKHVHTXHQFLQJRIDERXWLQGLYLGXDOV
by the end of 2015. This genetic information will then
be linked to the individuals electronic health records,
gathered by our partners at Geisinger Health Systems,
allowing us to determine association of genetic variants
with health or disease.
We continue to build a world-class 400,000 square foot
SURGXFWVXSSO\IDFLOLW\LQ,UHODQGZKLFKZLOOVLJQL?FDQWO\
expand our biologic supply capabilities for commercial
products. We also continue expansions of our Rensselaer,
NY production capacity and our Tarrytown, NY R&D laboratories and corporate headquarters. Our workforce is rapidly
expanding as well  in early 2015 we welcomed our 3,000th
Regeneron employee. Throughout our expansion, we have
focused on sustaining the innovative culture that makes us
unique. To that end, we are proud to be named the Top
Employer in the Biopharmaceutical Industry by Science
magazine for the third year in a row. We were also pleased
to be recognized by Forbes, which named us one of the
WRS?YHPRVWLQQRYDWLYHFRPSDQLHVLQWKHZRUOG ? DFURVV
all industries  for the second consecutive year.
Finally, we warmly welcomed Robert A. Ingram, General
Partner of Hatteras Venture Partners and former
Vice Chair, Pharmaceuticals of GlaxoSmithKline, to
our board of directors.
With important late-stage products moving closer
to market, Regeneron has the potential to generate
multiple, significant revenue streams in the coming
years from important medicines that treat new groups
of patients in need.
Thank you for your continued support in this journey
and for your shared commitment to improving the lives
of people touched by serious disease. We invite you to
read more about all we have accomplished this year and
to learn about whats yet to come for Regeneron.
Sincerely, 