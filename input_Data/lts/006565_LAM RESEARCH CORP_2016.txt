LETTER TO OUR STOCKHOLDERS
Lam Research delivered another record year for financial performance in fiscal 2016, achieving nearly
$6 billion in both shipments and revenue and generating over $900 million in GAAP net income and
over $1 billion in non-GAAP net income*. In a flat capital-spending environment, we have grown our
shipments by 14% CAGR over the last two fiscal years and our GAAP and non-GAAP net income by
over 20% CAGR. These results reinforce our strong position, as technology inflections enabling the
next generation of semiconductor innovation continue to intensify around our products and solutions.
At the core of our outperformance are our culture and values, our close partnerships with our
customers, and our ability to deliver innovative products, services, and capabilities at scale that directly
address key semiconductor manufacturing and technology inflections such as 3D NAND, multiple
patterning, FinFET, and advanced packaging. Our focus on customer trust, collaboration, and execution
has allowed us to increase opportunities by addressing an increasing portion of capital spending and
delivering enhanced value to customers, employees, and stockholders.
Our customers seek to capitalize on the acceleration in innovation centered on the next wave
of industry applications that many believe will be integral elements of our lives in the future.
Advancements like virtual reality, machine learning, connected devices and smarter automobiles will
require unprecedented scaling of performance, power and cost reduction for computing, storage and
networking, significantly beyond the traditional definition of Moores Law. For cloud and consumer
applications in particular, this acceleration in innovation is driving significant increases in demand for
evermore capable DRAM, NAND and logic chips.
We believe that Lams key products in deposition, etch, and clean and their on-wafer performance
are increasingly important to the success of our customers. In partnership with customers, we are
helping the industry implement multiple
patterning driven scaling in DRAM
and logic. We are also facilitating cost
and density scaling for 3D NAND and
new memory technologies, as well
as facilitating improved computing
performance and memory bandwidth
with FinFETs and advanced packaging
applications.
Our investments in R&D have placed us in an outstanding position to help drive needed innovation and
to capitalize on the opportunities presented by current and future inflections. As a result of our strategic
and operational execution through the last several years, Lams addressable market as a share of wafer
fabrication equipment (WFE) spend has increased from 26.5% in 2013 to over 30% in 2015 and is once
again on track to exceed the performance of overall WFE in 2016. The 3D NAND inflection in particular has accelerated and is expected to drive a greater than 30%
growth in NAND WFE in calendar 2016. Deposition and etch are key process technology enablers of
3D NAND, which is perhaps best illustrated by the greater than twice the growth in our nonvolatile
memory markets over the last two years and exciting multi-year growth outlook going forward. Our
VECTOR Strata and ALTUS deposition systems, along with our Kiyo conductor and Flex dielectric
etch systems, provide enabling technological capability for critical applications in 3D NAND formation,
while also providing a roadmap for other vertical scaling and productivity improvement opportunities.
Our ALTUS systems with atomic layer deposition capability and Kiyo with Hydra technology provide
enhanced variability control and help our customers extend multiple patterning to sustain scaling
roadmaps in both logic and memory devices.
Beyond systems capabilities, we deliver additional value to customers through Lams Customer
Support Business Group, which serves more than 40,000 process modules in our installed base.
This organization provides world-class support for our new systems during our customers integrated
circuit production ramps and addresses our customers ongoing and critical operational needs for
spares and services over the useful life of our systems. Moreover, this organization is contributing
highly innovative, differentiated technology, productivity and lifecycle solutions that are resulting in
served market and market share growth as well as an increasing revenue stream annuity that supports
our investments in our future and our long-term customer roadmaps.
Lam is executing at a high level today, while taking important steps to build an exciting future
organically and acquisitively. We believe that our strong product and service portfolio, aligned with
multi-generation industry roadmaps, and our unique values and culture position us for sustainable
long-term outperformance. Lams success would not be possible without the rewarding partnerships
we have built with our customers and suppliers, the achievement and commitment of our employees
worldwide, and the valued support of our stockholders. We would like to take this opportunity to thank
all stakeholders, and we look forward to building on these achievements in fiscal year 2017 and beyond.
Sincerely,
Martin B. Anstice Stephen G. Newberry
President and Chief Executive Officer Chairman of the Board