To my fellow owners,
I must admit that it is incredibly bittersweet to be writing
my final letter as Chairman and CEO of SunTrust Banks, Inc.
SunTrust is a truly great Company—the SunTrust brand and
legacy is one that I am incredibly attached to and proud
of, one that is well respected within the community, and
one that many current and former teammates have worked
extremely hard to build and enhance. We all stand on the
shoulders of others, and I thank all past CEOs who built
something special. But, great companies cannot stand still.
We must be focused forward, we must be willing to evolve
and respond to changing dynamics in our industry, and we
must be willing to take risks and disrupt ourselves.
On February 7th, we announced a merger of equals with BB&T. Some view this
announcement as the end of an era. I view this as the beginning of something
epic and profoundly transformational. I am extremely confident in our future,
and I have the utmost conviction that this merger of equals between two great
companies positions us to achieve so much more together than we could
ever do alone.
I do not want you to think of this transaction as the coming together of two
legacy institutions, but rather the combination of two strong foundations to create
a different bank, to create the premier financial institution. Individually, SunTrust
and BB&T are two very strong financial institutions; together, we have
an opportunity to be the best.
A common question I get is, ‘Why BB&T?’ The answer starts with leadership—
Kelly King, Chairman and CEO of BB&T, is a strong, purposeful leader and I have
the utmost respect for him and his team. Kelly and I share a common vision about
the future of banking, the importance of embracing the opportunity for disruption,
and the need to make significant investments in transformational technology to
drive a sustainable competitive advantage. 
The next derivative of leadership is culture—and culture matters.
A merger of equals does not work unless the two companies are
like-minded and have strong cultural alignment. At SunTrust, we
define our purpose as Lighting the Way to Financial Well-Being. At
BB&T, their mission is to Make the World a Better Place to Live. Our
mission and purpose-driven cultures are incredibly complementary,
and they will be the foundation upon which we build the combined
company. We both believe that teammates and associates want
to work for companies that stand for something, and we are
committed to making this the best bank in the country to work for.
Culture and leadership are the two big qualitative factors in any
combination, but equally important are the strategic and financial
merits of the transaction. On this front, you could not draw up
a better partnership than SunTrust and BB&T. The combination
between our two institutions is highly synergistic, financially
compelling, and transformative.
› This merger brings two highly synergistic and complementary
business models together to create one of the most diverse
and comprehensive business mixes in banking. We believe this
is a powerful combination with compelling revenue synergy
opportunities as we each grow our specialized businesses into
an expanded client base and drive further scale into certain
businesses, while attracting new clients along the way. We also
have a great opportunity to recognize top talent and select the
best business processes, technology, and locations from both
SunTrust and BB&T, which will position us for success for years
to come.
› The results of this transaction could not be more financially
compelling. It has the potential to produce industry-leading
financial performance, evidenced by a 51% tangible efficiency
ratio and a return on tangible common equity of 22%1
.
› And finally, this combination is transformational—it provides
us with the scale we need to compete and win in the rapidly
evolving world of financial services. Together, we have the
capacity for accelerated investments in technology. We will lead
with an innovative mindset and embrace the opportunity for
disruption, all with the goal of creating a superior, differentiated
client experience.
Kelly and I have had many conversations about our mutual desire
to focus forward and position our companies for future success.
Our increased capacity for investments, when combined with our
revenue synergy opportunities, creates a compelling growth story
which is one of the many reasons we believe this is the beginning
of something epic. 
Investment Thesis
Another question I have received is, ‘Why did you deviate from
your core investment thesis, which seemed to be more focused
on organic growth?’ I actually would argue the merger with BB&T
doubles down on our core investment thesis. Our owners have
invested in SunTrust for three primary reasons:
(i) we remain focused on investing in growth and technology,
(ii) we have demonstrated success in improving our efficiency
and returns, but still have potential for further improvements,
and (iii) we have a strong capital position and risk profile, which
has enabled us to return increasing amounts of capital to our
owners. Every single one of these opportunities is meaningfully
enhanced by our combination with BB&T.
Importantly, our financial performance directly supports our
investment thesis; 2018 marked the seventh consecutive year of
higher adjusted earnings per share (up 40%), improved efficiency
(down 150 basis points4
), and higher capital returns (up 41%). This
progress, which builds upon a higher base each year, is just one
indicator of the culture of continuous improvement and high
performance we have instilled across the Company. While our
investment thesis has remained consistent, our execution and
performance intensity has improved each year. Another notable
evolution is the increasing relevance of technology. While we
have maintained a consistent focus on investing in growth, today,
more than ever, much of that growth is dependent upon the
investments we have made and continue to make in technology.
Relatedly, technology is a critical driver of our ability to enhance
our efficiency and effectiveness.
Investing in Growth & Technology
As a bank CEO, I cannot help but get excited about the pace of
change we have seen within the banking industry. As an industry, 
we have come a long way in terms of how we interact with our
clients, the services we provide them, and the role technology
plays within. In fairness, it can also be challenging because in many
ways, it is hard to predict what the industry will look like 10 years
from now. In 2018, I asked each of our line of business leaders to
enhance their strategic planning process by assembling a diverse
set of teammates to attend day-long sessions in our innovation lab
to discuss the future of banking and map out what their specific
business could look like in 2028 under a number of different
scenarios. Much of our long-range strategic planning work in 2018
helped shape my thinking that a combination with BB&T would
give us the requisite scale, capacity, and capabilities to significantly
and quickly enhance our competitive position.
At the same time, I have been and continue to be very optimistic
about SunTrust’s standalone medium-term organic growth
opportunities. Importantly, a significant amount of our growth
and investment opportunities today are the result of the consistent,
successful investments we made in prior years.
While this is admittedly over simplified, our ‘go to market strategy’
is to deliver thoughtful advice to our clients and provide them
with a superior client experience. To that end, we feel there are two
primary growth levers for us—technology and talent. These two
levers are not mutually exclusive; in each of our lines of business,
we need both to win.
Big picture, across the Company, our technology strategy is tied
to three primary objectives:
(1) Experience: our goal is to invest in digital platforms that
enable clients to do business when, where, and how they
choose. We want to put more capabilities at our clients’
fingertips and provide a superior, differentiated experience.
(2) Efficiency: we must make the requisite investments to
modernize our architecture and maximize agility. In many
ways, the investments we make in technology are self-funded
by the efficiencies that technology produces.
(3) Security: we must leverage technology as a defense to
provide cyber security, ensure we have adequate stability,
and protect our clients’ information. 
In order for us to successfully execute our technology strategy, we
recognize that we must leverage partnerships and APIs to connect
our digital platforms to the broader financial services ecosystem
and expand our products, solutions, and capabilities beyond the
traditional banking channels. Given this, we feel partnerships are
an essential driver of our ability to execute against the first two
pillars of our strategy.
We are also working to change the way we design and produce new
and upgraded technologies. We recognize that change is constant,
and continuous improvement is a must. Across the Company, we
are focused on creating cross-functional agile delivery teams; this
allows us to create, deploy, and enhance client-centric solutions
better, faster, and more frequently. In 2018 alone, we assembled
75 agile teams to deploy new projects and upgrades.
And finally, we are making significant investments in our backend infrastructure. Our goal is to enhance the architecture of the
Company with a cloud-based, open architecture framework. By
building an open-architecture framework, we maximize our ability
to design the optimal client experience by leveraging cloud-based
platforms and APIs to more easily integrate apps, services, and
digital experiences across the ecosystem. The cloud also enhances
our efficiency and stability as it lowers our cost to serve and
enables continuous deployment of upgrades without interrupting
our service to clients.
Going forward, our merger with BB&T accelerates our capacity to
invest in transformational digital innovation and also gives us the
opportunity to choose the best of both company’s technology
and systems. Individually, SunTrust and BB&T are both coming
from positions of strength—we have both made great progress
in investing in client-friendly technology and modernizing our
infrastructure. Together, we will lead with an innovative mindset,
relentlessly focused on the client experience, in order to create a
truly differentiated and sustainable competitive advantage.
Consumer Segment
The pace of change in the Consumer business is tremendous;
client preferences evolve dramatically each year and we strive to
provide clients with a platform that allows them to do business
with SunTrust at their convenience. For certain clients, that means
ensuring that our branches and call centers are staffed with
talented teammates who can assist with their full range of needs,
from a simple transaction to lending to financial planning. For
clients that are looking for more insightful advice around their
financial goals and priorities, we have made concerted efforts to
elevate our expertise by hiring and developing Premier Bankers and
Private Financial Advisors.
For other clients, that means ensuring that we have digital
platforms which allow them to accomplish all of their financial
planning and transactions via self-service, at any hour of the day.
We have made and continue to make investments in our mobile
and online platforms to ensure we are providing for a frictionless
experience and putting more functionalities at their fingertips. We
have made great strides here; greater than 90% of our consumer
products can now be opened with a mobile device, and our new
payments platform in partnership with Zelle® makes it much easier
for clients to execute P2P transactions. In fact, P2P users increased
by 138% and transaction volumes increased by 300% in 2018.
In addition to the changes we have seen in mobile and online
banking, the world of consumer lending has also evolved
significantly. Fortunately, SunTrust has been at the forefront of this
evolution. In 2012, we acquired LightStream, an online consumer
lending platform, which provides financing to prime and super
prime borrowers for the majority of consumer purchases (cars,
home improvement projects, debt consolidation, etc.). Better
yet, the application takes the average client less than 20 minutes
to complete, and in many cases we are able to provide sameday funding. We strongly believe we have a headstart in this
space, but we are highly focused on ensuring we make consistent
investments to keep up with the pace of innovation and fully
capitalize upon the competitive advantage that LightStream
provides. We have also made significant investments in pointof-sale lending partnerships; today, we have three partners
(two of which were added in 2018) which provide financing for
home improvement-related projects. These investments have
put us at the center of when and where consumers make their
lending decision. LightStream and third-party partnerships
combined delivered approximately $2 billion in loan growth in
2018, improving growth and returns within the Consumer Segment
while also allowing us to reach a new set of clients (roughly 25% of
production is from SunTrust clients).
In March, we introduced SmartGUIDE, our new fully digital
mortgage application which takes an average client 37 minutes to
complete (about half the time of a traditional mortgage application).
Since its introduction, we have already achieved 64% adoption and
should see further progress from here. This not only provides for
a superior client experience, it also streamlines our underwriting
process and reduces loan cycle origination times.
In addition to meeting the depository and lending needs of our
Consumer clients, we are also highly focused on meeting their
wealth management needs. Within Private Wealth Management
(PWM), we are focused on having the right team, coverage model,
and technology to support our clients and our teammates.
One of our primary areas of focus within PWM in 2018 was hiring
new advisors and expanding into new markets. New production
AUM in 2018 was $4.2 billion, 70% higher than 2017, which helped
offset the market pressures we felt in the fourth quarter of 2018.
We also introduced a new Digital Client Portal to our PWM clients
which provides clients with a fully digital, bespoke, holistic view of
all aspects of their financial lives and includes enhanced financial
planning tools. One of the key benefits of the investments we have
made in our Digital Client Portal is that we will be able to leverage
the architecture we built for all clients across the bank.
Bigger picture, I feel very good about the investments in technology
and talent we have made across our Consumer Segment to
enhance the client experience. My own confidence is validated
by third-party recognition. We received a number of impressive
awards in 2018 which are outlined on page 25 of this report; the
most notable of these are the four Javelin leader awards for
our mobile and online platforms, which represent very strong
performance relative to our peers. BB&T has also received a
number of impressive awards that support my earlier comment
that we are both coming from a position of strength.
Overall, 2018 was a very strong year for our Consumer Segment.
Our healthy, broad-based balance sheet growth, the higher-rate
environment, and well-controlled deposit costs drove a 4% increase
in revenues relative to 2017. This strong revenue growth, combined
with our ongoing efficiency initiatives, drove a 200-basis-point
improvement in the Consumer Segment’s efficiency ratio.
Going forward, we are excited about the opportunity we have
to enhance our consumer banking delivery model by bringing
together the best of both companies’ capabilities and strategies,
and increasing our investments in client-friendly technology. We
also benefit from certain revenue synergy opportunities including
the ability to leverage our LightStream platform, SmartGUIDE
application, and point-of-sale lending platform into an expanded
client base.
Wholesale Segment
Within our Wholesale Segment, we have a clear competitive
advantage because of our talent and our focus on the middlemarket. We win because of the quality of our people, the advice
they deliver, and the way we work together. More specifically, we
deliver a comprehensive set of product capabilities and industry
expertise to middle-market clients in a One Team manner.
The competitive advantage we have in Wholesale today is largely
the result of the investments we have made over the last 15 years
in hiring, developing, and retaining talent. By way of background,
during the Financial Crisis, several of our larger competitors were
shrinking; however, we remained committed to investing in
growth. This allowed us to capitalize on an opportunity to recruit
key talent; many of the leaders we brought in at this time are the
foundation for the superior middle-market corporate & investment
banking platform we have built (which goes to market as SunTrust
Robinson Humphrey, or STRH).
To put numbers behind it, over the last 10 years, we have hired
more than 800 bankers at STRH, and we will continue to recruit
key talent into our platform each year. With the exception of a
small energy boutique, Lantana, which we acquired in 2014, this
growth has been organic.
Today, STRH has eight industry practice groups and over 30
sub-industry verticals and a robust equity research and trading
platform with over 700 small- and mid-cap companies under
coverage. And, we deliver our full suite of capabilities from 14
different offices across the country. STRH’s revenues have
increased at a strong 7% CAGR over the past five years.
Technically, STRH is one of our three lines of business within
Wholesale, with the other two being Commercial Banking and
Commercial Real Estate (CRE). However, I don’t think about STRH
as its own line of business—the lines are intentionally blurred
between our three Wholesale lines of business. Our STRH platform
exists to serve the capital markets needs of all Wholesale clients,
not just our corporate and investment banking clients. In fact, 20%
of our capital markets income in 2018 (investment banking and
trading) was from our Commercial Banking, CRE, and Private
Wealth clients, up from just 12% compared to the prior year. Across
the industry, it is not common for commercial bankers, investment
bankers, industry specialists, and product specialists to work
cohesively—but this is exactly what happens at SunTrust every
day—what we refer to as the One Team approach. 
Overall, 2018 was a good year for our Wholesale Segment, with
our success driven by the strategies I described above. Revenue
was up 2% relative to 2017 and the business maintained a strong
efficiency ratio of 45%. Capital markets revenues declined for
the first time in 11 years, which is largely a reflection of market
conditions and our intentional pullback in the leveraged finance
space, which was down 20% compared to 2017.
To frame up the opportunity we have in our capital markets
business, today we have a 2-3% market share (for our addressable
market), and we feel our market share can easily double. So how
do we get there? It comes down to three main levers:
(1) Deepen: we have an opportunity to deepen the relationships
we have with our existing clients. Today, we serve as the
left lead for approximately 15% of our clients, and we would
like to see that continue to grow over time, which will in turn
improve our average fee per transaction.
(2) Heighten: we will continue to elevate our expertise. In
essence, we want to see M&A and equity (the two more
strategic products) comprise a larger portion of our revenues.
Today, these two businesses represent 31% of our capital
markets revenues, but they are growing faster than debt
capital markets, a good indicator of our increasing strategic
relevance with clients.
(3) Expand: we have a significant opportunity to expand our
client base by bringing data and analytics to more effectively
prospect the future clients who will benefit from more expertise.
This is particularly true for our Commercial Banking and
CRE businesses. We are also expanding our geographical reach.
In 2018, we opened our fourth Commercial Banking expansion
market in Houston (a market where we already have Corporate
Banking and Private Wealth teams). And now, we will have a
new client base from BB&T that can benefit from the strategic
advice, product suite, and industry vertical expertise that we
have built within Wholesale over the past 15 years.
We had good underlying momentum across each of these three
levers in 2018—left lead relationships grew by 11%, M&A and equity
originations had record performance and increased 15%, and finally
capital markets revenues from non-CIB clients increased 49%.
We are highly optimistic about the revenue synergy opportunities
that our combined Wholesale banking business has with BB&T.
We will have the opportunity to deliver a leading middle-market
investment banking platform into an expanded and established
corporate and commercial banking client base with a larger
balance sheet. We are also increasing our capacity for continued
growth in capital markets, which had grown to comprise 24% of
SunTrust’s fee income but will represent approximately 15% of the
combined company’s fee income.
It is no coincidence that this is the longest section of my annual
letter to you, our owners. Investing in growth and technology
is key to our success—but in order to create capacity for these
investments, we must improve our efficiency.
Improving Efficiency & Returns
We achieved a key milestone in 2018 by reaching our sub-60%
adjusted tangible efficiency ratio target. We initially declared this
target back in 2011 when our efficiency ratio was 72%. At the time,
this was a highly aspirational target; we did not know when we
would get there, but it was important to be ambitious. For each of
the last seven years, we have been making progress toward this
goal and in 2018, we achieved 150 basis points of progress, ending
the year at 59.6%5
.
The four pillars of our efficiency opportunity are organizational
efficiency, technology enhancements, real estate, and
procurement. Equally important, efficiency is a means to an end—
it allows us to create capacity to invest in growth and technology
(all the things I discussed in the previous section of the letter).
Relatedly, focusing too much on one metric will not maximize
our long-term value, which is why we have a broader focus on
returns. In 2018, our ROA improved by 23 basis points and our
ROTCE improved by 350 basis points6
. In fairness, much of this
improvement is reflective of the benefits of tax reform, but some
of it is due to our internal focus on maximizing risk-adjusted
returns. We want to ensure that as we extend our balance sheet,
we do so in a way that is profitable and is appropriately priced for
the risk we are taking.
Strong Capital & Risk Position
I’ve spent the majority of this letter outlining what we are doing
on the ‘offense’ side of our business (areas of investment, areas
of growth, and drivers of improved profitability), but as a financial
services company, we are equally as focused on ensuring we have
a strong defense. To that end, it is notable that 2018 marked the
tenth anniversary of the Financial Crisis. While I don’t like to live in 
the past, and I certainly don’t want to relive that period, I cannot
help but be incredibly proud of all that we have accomplished over
the last 10 years.
There were many lessons we learned in the Financial Crisis, and
we have ensured that we truly internalized each of them, not the
least of which is the importance of diversity. If you look at our loan
portfolio today, we have significant diversity both by loan type and
by geography, which is a reflection of our diverse business mix.
While our discipline has been consistent over the last ten years,
the sophistication of our risk-monitoring tools has improved
dramatically. As an example, for the past few years, we have been
working on an enhanced Risk Appetite Framework (RAF), which
we formally introduced in 2018. This RAF gives us the ability to
measure our actual risk position, on a granular basis, compared
to our moderate risk appetite, across all risk disciplines (credit
risk, liquidity & market risk, operational risk, technology risk, etc.).
Our risk committees and the Board review our actual risk profile
against our established enterprise risk appetite on a quarterly basis.
In summary, the strength of our risk defense can be attributed to
four main factors: (i) a strong capital and liquidity position which
is appropriate for our risk profile; (ii) a diverse loan portfolio and 
conservative credit risk profile; (iii) a sophisticated and disciplined
risk culture; (iv) and a low-market-risk business model.
Our risk profile is validated by our performance in the Federal
Reserve’s stress tests. While each year’s test is not a perfect
predictor of how our portfolio will perform under stress, our
consistent and strong performance under the different scenarios
for each of the last five years validates the diversity and strength
of our portfolio and processes.
Importantly, the strong cultural alignment we have with BB&T
also encompasses our risk cultures. Both SunTrust and BB&T have
conservative risk cultures and that will not be compromised as
we come together; in fact, together we benefit from increased
diversity. We will also be in a unique position of producing bestin-class returns while having a strong balance sheet and low
levels of credit risk.
Purpose & Culture
The most tangible way to measure our progress each year is
through our financial performance. But the tangible results are the
outcomes from our purpose focus. We believe we, as a Company,
have a tremendous opportunity and responsibility to promote
and enable financial confidence. This is why we exist, this is our
purpose and our passion.
Given this, I measure our success by asking, ‘How did we deliver
on our purpose of Lighting the Way to Financial Well-Being? How
did we enhance our culture as a Company this year? How did we
improve the lives and career trajectories of our teammates?’ As I
reflect on our progress against these intangibles, I cannot help but
be incredibly proud of our Company and the way we are building a
different bank.
Importantly, our orientation as a purpose-driven Company is
the foundation for our culture. Our culture is one where purpose,
performance intensity, teamwork, trust, and a client-first mindset
successfully co-exist. We have worked incredibly hard to cultivate
this culture, and I hope that those of you who interact with us can
feel it. Our purpose-driven culture drives how we go to market,
how we interact with our clients, and what products and advice 
we offer our clients. And finally, our culture is exemplified in our
ability to work together as One Team, agnostic to products and
lines of business, with the ultimate goal of providing clients with
the solution that meets their needs, not ours.
Our culture is also the foundation for our approach to the
development and recruitment of our teammates. Our teammates
are the lifeblood of the Company, and we feel that investing in our
own is a key contributor to our future success. Over the past few
years, we have worked to build out a robust set of development
programs for teammates at all stages in their careers. Each
year, we make significant investments to enhance and refine our
program offerings. Our teammates give us great feedback on
these as well— they recognize the investments we make in their
development and their careers, and they become more engaged
as a result.
Our culture also serves as a source of strength in times of tragedy.
I watched teammates from across the Company take heroic
actions to help the families of those that lost their lives in the
horrible shooting at our Midtown branch in Sebring, Florida, in
January. No words or actions can express the deep mourning we
share with the families and community, but the desire to give,
serve, and stand strong together helped us take the first step
toward healing.
As the leader of this Company, I am incredibly proud of our
purpose-driven culture and how we live it out on a daily basis.
I encourage you to read more about it, including our onUp
Movement and Momentum onUp, on pages 14–19.
Conclusion
I first want to thank our teammates for all that you accomplished
in 2018—it is because of your passion for promoting financial
confidence, helping our clients achieve smart growth, and
providing a superior client experience that we are on our way to
becoming the premier financial services institution. I am incredibly
proud of the team we have at this Company, and we remain
committed to investing in you. I also recognize there will be change
as we move forward and merge with BB&T, but I commit to you
that this change will be thoughtful, supportive, and compassionate. 
To our owners, thank you for your ownership in SunTrust. I hope
that you, as owners, can appreciate all that we have done and will
continue to do in order to continue driving long-term value.
And finally, thank you to our Board of Directors and our executive
leadership team. Your dedication, wisdom, experience, and
expertise have been and will continue to be instrumental in
strengthening our financial performance and long-term potential.
Together, we will also remember and benefit from the outstanding
service of Dr. Phail Wynn, our fellow Director and friend who
passed away unexpectedly last year. It is with the utmost honor
that I want to recognize him here.
To conclude, I want to leave you with a few key
takeaways. First, SunTrust and BB&T are coming
together from positions of strength and we truly
believe that this combination is one where two
plus two equals five. Second, our conservative
risk appetites will not be compromised by this
combination, they will be enhanced. Relatedly,
our mission and purpose-driven cultures will
be amplified when combined, not diluted. And
finally, we truly believe all stakeholders will
benefit from this combination—together, we
can better serve our clients, communities,
teammates, and owners more than ever before.
This merger of equals is the first step towards
creating the premier financial institution and
we are incredibly excited about the future. 

William H. Rogers, Jr.
Chairman and Chief Executive Officer
SunTrust Banks, Inc.
February 22, 2019