I am proud to report that 2002 was a year of many accomplishments
for VF Corporation. By doing what we do best  nurturing and building
some of the strongest apparel brands in the world  our management
team met and successfully overcame economic and industry challenges.
Todays consumers are more value-driven than ever before. But
our ability to reduce costs has allowed us to build even more value
and innovation into our products. Weve kept our brands attuned
to consumer needs, while keeping our company strong and healthy.
Key accomplishments for 2002 include:
> An increase in market share for many of VFs core brands in the U.S.,
including Lee, Vanity Fair and Bestform
> Strong sales performance by many of our brands in Europe, including
Lee, Wrangler, The North Face and Eastpak
> An 11% increase in marketing investment, to heighten the visibility
of our brands among consumers
> Healthy increases in profitability for most of our businesses, resulting
from the successful completion of our Strategic Repositioning Program
> The successful exit of two underperforming businesses, Jantzen
swimwear and Private Label knitwear
We now look forward to the challenges and opportunities of 2003.
We continue to take a cautious stance toward the outlook for retail
sales and consumer spending on apparel. Nevertheless, we expect
to benefit this year from the hard work we did in 2002 to strengthen
our product offerings, intensify our marketing communications
and reduce our cost structure. We expect both higher sales and
earnings in 2003.
2002 FINANCIAL REVIEW
We made excellent progress in 2002 toward meeting our long-term
financial goals:
> Return on capital. Improving our return on capital demonstrates
our ability to manage corporate assets efficiently and is a key driver
of shareholder value. In 2002 we achieved our return on capital
goal of 17% from continuing operations. Further improvement is
planned in 2003.
> Sales growth. Our long-term sales growth goal is 6%. Reflecting weak
retail conditions, sales dipped 3% in 2002. However, most VF brands
maintained or increased market share, with success in products such
as Wrangler Five Star Premium Denim and Lee Dungarees jeans for
men, Riders jeans for women, The North Face outerwear, JanSport
Airlift packs and Healthtex Kidproof stain resistant childrenswear.
Our licensed sports apparel business grew strongly in 2002, driven by
new products for the National Football League. Our licensed Tommy
Hilfiger intimate apparel line also experienced a double-digit sales
increase. Were expecting sales increases in most of our businesses in
2003, and believe that acquisitions will play an important role in
reaching our long-term sales goal.
> Operating margin. Our profitability improved tremendously in 2002,
with operating margins expanding to 12.2% from 8.7% in 2001.
Actions related to our Strategic Repositioning Program, including
plant closures and cost reductions, resulted in cost savings
exceeding $100 million in 2002. We expect to realize additional
savings of more than $30 million from this Program in 2003. Our
long-term goal remains 14%.
> Invested capital. We targeted a $200 million reduction. Over the
past two years, we have reduced invested capital by more than
$300 million, primarily through tight inventory control.
> Debt to capital ratio. At the end of 2002, our debt to total capital
ratio, net of cash, was 9.2%, well below our long-term target of 40%.
Our low debt level and strong cash position provide us with the
flexibility to pursue new avenues of growth.
Results in 2002 reflected restructuring charges related to our
Strategic Repositioning Program and a one-time charge related
to a required change in accounting for goodwill. Including these
charges, VF reported a loss per share of $1.38. Earnings from
continuing operations excluding the effect of the accounting
change was $3.24 per share, up 71% from $1.89 per share in 2001.
Excluding charges taken in both 2002 and 2001, earnings per share
from continuing operations rose 30% to $3.38 from $2.60.
VF continues to generate strong cash flow. In 2002, cash flow from
operations was $646 million. We continue to seek ways to deploy
our cash into high return opportunities. Our cash flow priorities are
acquisitions, share repurchases and dividends. 2002 also marked the
30th consecutive year of higher dividend payments to shareholders.
2003 PRIORITIES
We have a number of priorities for 2003  but our focus is
on capturing new sources of growth. At the forefront of our
strategy is consumerization: knowing our consumers better than
any other apparel company, and giving them and our retail
partners the right products at the right time.
Keep the momentum going in our U.S. jeans businesses.
Challenges we expect to face this year include new competition
for our jeans brands in mass market stores and the closing of a
number of stores by one of our largest customers. Were attacking
these challenges on a number of fronts. At the top of the list is great
product. Well be building on the product successes of 2002 with a
steady stream of new, consumer-right products across all our
brands. Second is marketing: maintaining a healthy level of
investment behind our brands to keep them front and center with
consumers. Third, well be leveraging our low cost supply chain and
state-of-the-art replenishment services to be the gold standard in
responsiveness to retailers. Last but not least, well continue to
search for new brands to add to our portfolio.
Identify new ways to take advantage of changing retail
dynamics. The winners of tomorrow will be those who control
the destiny of their brands on the retail floor, who can supply an
ever-changing array of new products and brands to consumers,
and whose business strategy is aligned with those of the strongest
players in the industry. Our European jeans and intimates units are
taking the lead in this area with the rollout of new in-store shops and
boutique concepts. In the U.S., were putting more resources behind
analyzing our customers business, to identify common areas for
growth and to increase the frequency and hit rate of new ideas.
Continue to lead with product innovation. In 2003, our Intimates
group will be launching the CurvationTM brand, targeted to curvaceous
women seeking beautiful, comfortable bras and panties in mass
stores. The North Face will launch the A5TM line of functional cotton
apparel in sports specialty stores. Lee Jeans will roll out its Lee
Dungarees Series 66202TM line of casual, hip cotton pants for
department stores. And this spring, JanSport is introducing its
Live WireTM pack, with built-in features for CD-carrying music
enthusiasts. These are just a few examples of how VF will continue
to use innovation as a means of meeting the specific needs of
targeted groups of consumers.
Fill the gaps in our current portfolio. Were committed to growth
and see opportunities to expand our top line by adding brands
and products that reach new and different consumer segments,
geographic markets or channels of distribution. Our top priorities are
in our jeans and intimates businesses  particularly in the upper and
mid-tier channels of distribution  both here and abroad. Well also
be looking for new brands to add to our growing Outdoor coalition.
Finally, we remain interested in adding a lifestyle brand that crosses
multiple product categories. As always, we will be very disciplined
in our approach to ensure that any acquisition we make will provide
good growth prospects and good returns to our shareholders.
TAKING THE LONG VIEW
We also know what it will take to be successful in the years to come.
Our brands are strong, but we wont stop there. Well be looking for
ways to extend the equity of our brands, so that they make a true
lifestyle statement to consumers. For example, our Lee brand is
adding an array of casual, comfortable and stylish pants and tops
for both men and women to complement their core denim products.
And were expanding The North Face brand into new categories,
including footwear.
Looking forward, we believe the retail apparel industry will continue
to consolidate. The big will continue to get bigger, holding true for
both retailers and suppliers. We intend to be an active participant in
this process, adding brands in our existing product categories as
well as in new ones. Our approach will be disciplined: conducting
in-depth analyses of consumer trends, tapping into industry growth
segments and using brands to address targeted consumer segments.
Most important, we believe that having the right leaders in place
is the surest way to guarantee our future success. VFs primary
competitive advantage has always been its people: leaders who are
experts in meeting the demands of constant and dramatic changes in
our industry. In 2002 VF launched a comprehensive and sophisticated
leadership development program, to ensure we continue to identify
and develop the future leaders of our Company.
We enter 2003 with world-class brands, a low cost structure and a
responsive, highly flexible supply chain. We look forward to putting
these strengths to work to generate new opportunities for our
associates and value for our shareholders.

Mackey J. McDonald
Chairman, President
and Chief Executive Officer