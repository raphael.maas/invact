LETTER TO SHAREHOLDERS

Dear Customers, Employees
and Shareholders,
Despite an economic environment that steadily
deteriorated throughout 2008, ITT continued
its successful track record and achieved our
best year yet in terms of business performance.
And while our attention is squarely
focused on the challenges that lie ahead in
2009, our company is well-positioned to
sustain long-term success.
There are very clear reasons why we
feel good about the state of ITT in this uncertain
environment. Our foundation is sturdy,
our customer relationships are strong, and our
strategy remains the right one for these times.
While it cant be business as usual, there is
much that we can control to ensure our longterm
success. We are getting closer to customers,
aggressively reducing costs, focusing on
cash generation and continuing to invest in our
long-term strategies.
The core beliefs and vision that have
guided ITT on our journey to become one of the
leading multi-industry companies in the world
todayand a top financial performer in this
category over the past five yearswill not
change. The people who work for us, buy from
us, invest in us and benefit from our products
and services have high expectations of ITT, and
we know we can continue to deliver for
them by staying true to the fundamentals that
have served them and us so well for so long.
ENDURING HUMAN NEEDS
From deep space to beneath the ocean floor,
ITT is addressing essential human needs in partnership
with customers on all seven continents.
In the past year, we developed one of the
worlds most efficient ultraviolet lamps for safe

water disinfection and became the leading provider of systems to protect U.S.
and allied soldiers from roadside bombs. Our employees developed the
ground infrastructure for Americas next-generation air traffic control system
and transmitted and translated the signals from the Phoenix Mars rover that
offered the first conclusive evidence that water once existed on the Red Planet.
These vital advances tackle problems that touch us allfrom overcrowded
skies and shrinking water supplies, to global security and the very
human need to understand the universe around us.
ITT is working closely with customers to take on the daunting issues that
matter most to the well-being of the world. We welcome the challenge,
and our business will continue to grow because we are committed to developing
sustainable solutions that help people live healthier, safer, fuller lives
today, tomorrow and far into the future.
BALANCED PORTFOLIO
In todays volatile environment, we rely on the balance in our portfolio to
provide some relative support and stability. We have built our company around
three key platformsFluid Technology, Defense Electronics & Services and
Motion & Flow Controland our balanced portfolio allows us to deliver consistent
and more predictable results.
In 2008, these businesses combined to deliver 22 percent earnings
growth over the prior year and record revenue of $11.7 billion. This was
achieved through the successful integration of more than $2 billion in acquisitions
and healthy seven percent organic revenue growth. These results
occurred despite taking significant actions to position our company for the
year ahead, and are testament to the strength of our balanced portfolio.
In 2009, we will undoubtedly be challenged in our commercial segments
Fluid Technology and Motion & Flow Controlwhich we expect will
contract this year. In our view, this will be partially offset by our strong
Defense Electronics & Services portfolio, which we anticipate will continue to
grow its top and bottom line. This confidence comes from our belief that
the Defense portfolio is nicely aligned with our customers priorities, evidenced
in part by a funded backlog of more than $5 billion.
Our customers are central to who we are at ITT. The quality and criticality
of our products make us an essential provider and partner to them. In
these volatile market conditions, we are aligning our activities, including our
vital services and maintenance offerings, with our customers current needs.
We are also improving product life cycle costs through energy efficiency and
total cost of ownership reductions.

PROVEN STRATEGIES
At ITT, we are staying true to the strategies that have helped us become a respected,
strong-performing company that customers and investors count on year after year.
Our unwavering direction comes from the ITT Management System. This shared
set of integrated strategic processes is aligned around a common vision and group
of values and represents the way we think, the way we operateand the way we will
continue to create value for all our stakeholders.
As the economic landscape shifts under our feet, the ITT Management System
keeps us grounded and helps us make prudent decisions about where to invest in
our company and where to cut back.
In December we consolidated the oversight of our Fluid Technology and Motion
& Flow Control segments to create a more efficient structure that empowers our
customer-facing businessesthe heart of ITTto respond to customer needs more
quickly and with greater impact. Gretchen McClain, an accomplished ITT leader,
was promoted to run these groups. At the same time, we named David Melcher as
the new president of our Defense business, following a distinguished 32-year career
in the U.S. military, where he rose to the rank of three-star general.
Throughout 2008, and long before the bottom fell out of the global economy, our
sites continued to pursue large-scale Lean Transformation efforts to streamline
their operations and improve lead times, inventory levels, delivery performance and
other metrics that matter most to our customers.
Guided by the ITT Management System, we also have a proven process for identifying
promising new businesses and effective leaders and then integrating them into the
ITT way of doing business.
This system brings value-based discipline to everything we dofrom capital
allocation to supplier managementand is the bedrock our business has stood upon
to reach our goals in any and all market conditions.
LOOKING AHEAD
In this global, interconnected economy, companies still have the power to control
their own destinies.
At ITT, we will continue to make informed decisions based on the world we live
in and to make intelligent, bold moves that are consistent with our values and keep us
on the right path.
That means continuing to invest in our new corporate philanthropy program,
ITT Watermark, which is helping to bring safe water to people in need and enhancing
our global water leadership position. It means investing in a learning organization
to deliver greater leadership development and training opportunities for our employees.
It means keeping our eyes open to new opportunities, while remaining disciplined to
our fiscal strategies, including cash management and liquidity preservation.
And as always, it means relying on our strong leadership and front-line teams to
take an accurate reading of their markets and create solutions that give us a solid
competitive advantage. ITT is a company full of smart people with great ideas, good
instincts and strong values. More than anything, this is the strength of ITT. These may be
uncertain days, but ITT people know what it takes to come out on the winning end.
In 2008, we proved that we could weather a sudden and precipitous downturn in
the global economy and still turn in a record year. And while 2009 will be more challenging,
in the long-term, we are confident that ITT has the right portfolio, strategies
and people to handle whatever comes our way and resume our upward climb as one of
the worlds top-performing and most respected companies.
Steven R. Loranger
Chairman, President and
Chief Executive Officer