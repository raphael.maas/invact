LETTER TO SHAREHOLDERS
We are well-positioned for further growth and expect that well invest close
to $1 billion on exploration and development in 2006. We enter 2006 with our
largest inventory of prospects and acreage ever.
Cimarex Energy enters 2006 with its largest set of
growth opportunities ever, an expanded base of core
assets and a solid balance sheet. While we did not arrive
at this position through last years efforts alone, well
long remember 2005 as the year in which we acquired
Dallas-based Magnum Hunter Resources, Inc. and firmly
established the Permian Basin a new core area. Valued
at $2.1 billion, this stock-for-stock deal tripled our proved
reserves, doubled our production and brought us a large
portfolio of drilling and development opportunities for
2006 and years beyond.
Well also remember 2005 as a year in which oil and
gas prices soared. Boosted by Middle East tensions,
hurricanes in the Gulf of Mexico and resilient energy
demand all around the world, the price of oil rose to over
$60 a barrel and natural gas prices temporarily spiked to
$13-14 per thousand cubic feet. All at a time when the
ink had barely dried on our Magnum Hunter deal.
Benefiting from high prices and greatly expanded production volumes, our earnings and cash flow reached
new highs in 2005. Net income totaled $328 million, or
$4.90 per share. Oil and gas sales increased to $1.1
billion. Our operating activities provided $705 million
of cash flow and sales of non-core properties added
another $149 million of capital available for reinvestment
and debt reduction. We will continue to weed through
the asset base and may have some additional sales
during 2006.
Including what we spent on the Magnum Hunter properties after the June 7 acquisition date, we invested $642
million on exploration and development during 2005. With
that, we found 237 billion cubic feet equivalent of new
proved reserves and replaced 183 percent of production.
Utilizing cash flow in excess of reinvestment, proceeds
from property sales and cash on hand, we paid off all
Magnum Hunters $270 million of bank debt and still
ended 2005 with $62 million in the bank. We also initiated a quarterly dividend beginning in March 2006 and
started to buy back some of our stock.
Even with a 50% increase in our planned 2006 drilling
program, projections for the corresponding 2006 cash
flow available to re-invest indicated the potential for
further cash build up. The purpose behind our stock
buyback is to offset dilution from stock based compensation programs when we have cash in excess of our
capital requirements. The flip side is that we would
borrow money on our $1 billion bank credit facility to
fund our drilling program if prices weaken or if we find
the right acquisitions.
Somewhat overshadowed by the Magnum Hunter
transaction is the fact that we grew our exploration
program and production volumes in each of our core
areas. We now find ourselves well-positioned for further
growth and expect that well invest close to $1 billion
on exploration and development in 2006. Included in
that figure is $6080 million for infill drilling and
recompletion programs in some of Magnum Hunters
older fields.
Yes, 2005 was busy and very challenging. Many of our
employees worked brutal schedules to accomplish the
integration of Magnum Hunter into Cimarex. The fact
that the two companies were about the same size and
different in many ways, increased the difficulty. I want to
specifically recognize the efforts of our storm battered
New Orleans group who in the midst of all this had to
leave their homes, move their families and continue
their work in our Dallas office. They have moved back to new offices in New Orleans and are working hard to get
things back together.
Weve grown a lot since most of the Cimarex management team was assembled at tiny Key Production Co.,
Inc. in 1993. Back then we had just 30 billion cubic feet
equivalent of proved reserves and only a little bit of
an organizational heart beat. Our proved reserves now
stand at 1.4 trillion cubic feet equivalent and are 72
percent gas and 81 percent proved developed. We have
a 6.3 million gross acre leasehold position (3.7 million
net acres) that is about 60 percent undeveloped. We will
add to and drill on this acreage for many years to come.
The current forecast for our 2006 average rate of daily
production is a range of 485505 million cubic feet
equivalent. Thats not a goal, its an estimate, and you
can bet that we will be working hard to move that
range upward. We enter 2006 with our largest inventory
of prospects and acreage ever. We have access to an
adequate number of rigs to execute our program and
have an outstanding exploration organization that has
grown substantially over the past few years.
We are very proud that we are a company that has been
able to attract and retain talented oil and gas professionals because we take that as evidence that we are
the type of company they want to be a part of and the
type of company we want to be.financially stable and
committed to growth through the drill-bit.
As always, we want to thank our Board of Directors
for their guidance and governance. To our shareholders,
we remain committed to our overriding objectives of
achieving consistent profitable growth and building
solid underlying value.
Although we have grown a lot, were still a relatively
small company that is competing effectively in a large
industry. Its fun, its profitable, and its just the beginning
of what we think Cimarex will ultimately become.
F.H. Merelli
February 28, 2006