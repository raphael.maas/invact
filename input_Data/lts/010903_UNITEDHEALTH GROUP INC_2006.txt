The dramatic change taking place in health care today is that, increasingly, individuals choose,
purchase and manage their own health care coverage and are much more personally involved
in making decisions about their preventive behavior, as well as the cost, quality and
appropriateness of health care. Changing individual behavior is driving new technology
applications, reshaping products, rationalizing distribution, defining costs and setting
measurable, higher quality standards. The consumer is melting the historical boundaries of
health care and opening new markets and opportunities.
We are embracing the new dynamics of an evolving consumer-centric market by focusing
on six key areas:
We are making consumers the focal point of everything we do. The end-user will determine
care delivery relationships and judge the service experience. Peoples needs will drive where
our capital is applied, the technological changes we pursue, how models for decision-making
information are shaped, and how health care education and coaching are delivered, enhancing
the consumers clinical relationship and our role in it.
Continuing to diversify is imperative on both micro and macro levels. We are moving into
emerging market segments  ethnic markets and demographic niches, new group
aggregations, customized product types and innovative funding approaches. On the macro
level, UnitedHealth Group sees opportunities to diversify into care provider services, government
and financial services, and global pharmaceutical, device and safety services. We believe our
diversified portfolio positions us to serve in virtually every sector of the health care market.
We are focusing our technological expertise to better serve people with simple, reliable
technology solutions, creating front-end consumer versions of everything we offer. Today,
integration means bringing together all clinical services and interactions, all information,
all operations, all financial capabilities and all customized consumer services into a single,
technology-enabled consumer experience. And that experience must be engaging, intuitive
and personalized.
Industry forces are also driving the growth of unique health care financial services.
UnitedHealth Group has a significant first mover advantage in consumer health financial
services with Exante Financial Services. In commercial services, we are at the very early stages
of establishing a financial transaction clearinghouse to serve a new era of transactional advances
in health care.
Enterprise-wide we are reforming and cultivating our long-standing competencies to serve
expanding market needs. We know we must take these competencies to the next level. Making
our capabilities work for people on their terms will help them, and will also work for
UnitedHealth Group from a growth and financial performance perspective.
Finally, we are embracing a corporate culture that thrives on change, social responsibility
and ethical behavior. This is not something that is new for us, but it is a critical area where we
need to make greater investment and have even greater emphasis. We must be known for
high integrity and high performance.
It is also important to recognize what is not changing at UnitedHealth Group. We remain
true to our pursuit of clinical excellence, to evidence-based care, to facilitate and improve the
health system, to the respect we hold for patient/physician relationships, and to consumer
choice and access. In our business, we will continue to price to our cost; to achieve a fair return
on capital investment; and to pursue excellence in execution and performance, and maintain
rigorous discipline.
Throughout 2006, the people of UnitedHealth Group have focused on our mission-driven
priorities as a health and well-being company. Despite challenging circumstances, they have
remained dedicated to making health care work better and have continued to deliver the
innovation, service and performance our customers, business partners and investors expect
of this Company.
Our work continued to help improve the quality of care Americans received and to make
their health care more cost-effective. We accelerated health care affordability through more
effective cost management and broader availability of products and access to care.
We also continued our long-standing operating business discipline. Our unique clinical
approaches are increasingly sophisticated and effective. The innovative technology and
information capabilities of our enterprise continue to help us understand what is happening
in health care delivery and to creatively put that information to work on behalf of those we serve.
UnitedHealth Groups results in 2006 were characterized by strong organic revenue growth
of 21 percent, supplemented by the late 2005 acquisition of PacifiCare, pricing of risk-based
medical coverages consistent with underlying medical cost trends and disciplined operating
cost management:
> Our 2006 revenue of $71.5 billion represents a 54 percent gain over 2005;
> Earnings from operations of almost $7 billion were up 37 percent year over year;
> Cash flows from operations increased 60 percent to approximately $6.5 billion;
> Net earnings of nearly $4.2 billion were up 35 percent from 2005; and
> Earnings per share increased to $2.97 per share.
The Company provided major medical coverage and related care facilitation services to
about 1.5 million more people and launched Medicare Part D prescription drug plans that
reached a total of 5.7 million seniors by year end  who saved in excess of $14 billion in outof-
pocket expenses in 2006 by utilizing these plans. The Company also brought ancillary or
specialty benefits and services to 2.7 million new people in its specialty businesses, grew its
consumer-directed plan offerings to reach a total of nearly 2 million people at year end, saw
21 percent growth in its health information technology and services businesses and helped
individuals open thousands of new dedicated health banking accounts.
We interact directly with everyone in the health care environment, end-to-end  patients,
doctors, hospitals, employer-payers  and this breadth and scale enables us to manage
efficiency and quality of treatment, physician and hospital costs, and drug and technology
costs. Of particular importance, we have real-time, intimate involvement in the lives of people
when they are in need. As people take more control of their health, we are in a position to
help empower them to make the best choices, to help coordinate care and disease
management and to support their preventive behavior. Our complete and integrated clinical
data can be translated into information for action  getting the right treatment to the right
individual, improving quality, safety and appropriateness of care.
Perhaps one of the best examples of the effectiveness of the combination of consumercentric
principles, innovative technology, analytical rigor and social consciousness is the success
of the United Health Foundations multiyear support for community clinics in medically
underserved communities. In each clinic participating in the program, the emphasis is on
preventive care, coordination of care and the use of nationally recognized standards of
treatment, tailored to the unique needs of each community in collaboration with local care
providers. These clinics are replicable across the country, yet each is responsive to the unique
circumstances in the community it serves.
The four clinics, operating in New York, Washington, D.C., Florida and Louisiana, were
evaluated in 2006 in an independent study conducted by George Washington University. The
study confirms that the quality of care in each of the centers of excellence meets or exceeds
care provided in the private sector. United Health Foundation commissioned the study based
on our posture of careful evaluation of everything we do as a business. We believe social and
philanthropic activities must be pursued with that same accountability and discipline and
should produce measurable, positive results.
UnitedHealth Group also remains committed, as we have always been, to more equitably
and effectively delivering quality health care to uninsured Americans. In 2006 we joined
16 of the nations most influential health care organizations to form the Health Coverage
Coalition for the Uninsured (HCCU). Despite divergent political and ideological views that
exist among the members of this coalition, we have committed to jointly press lawmakers to
act on HCCUs proposal to significantly expand health coverage for our nations uninsured,
starting immediately with expanded coverage for children in 2007. We hope this concerted
effort by a diverse array of business, professional and charitable groups will mark an important
turning point.
In the past year, we have come through a period of momentous change as a company. We
will be addressing for some time the issues that arose in the past year. They have become a
catalyst for a maturing culture that embraces high ethical, social and business standards.
We understand that ongoing evolutionconstant changeis a fundamental and positive
element of the health care industry. UnitedHealth Group is a highly adaptive enterprise that is
changing at or above the speed of the market to remain competitive. Looking ahead, it is clear
that there is increasingly stronger growth and performance potential within our enterprise.We
believe it is also clear that we have the talent, the positioning, the agility and drive to help realize
that great potential for our shareholders, our customers and our employees.

Sincerely,
Stephen J. Hemsley 
President and Chief Executive Officer 

Richard T. Burke
Chairman of the Board of Directors