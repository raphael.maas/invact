                                               TO OUR FELLOW SHAREHOLDERS

                                                             Our financial results were led by another strong year      underlies our business model and the value creation
The term that comes to mind as a key
                                                             of revenue and earnings growth in the financial segment.   resulting from the allocation of those dollars. In 2006,
way to describe the past year at Fiserv
                                                             We made continuing progress in developing our              we generated $447 million of free cash flow, resulting
is Renaissance.
                                                             internal revenue growth capabilities in the financial      in a five-year total of more than $2.2 billion. Going
                                                             segment, which has grown by at least 5% in each of         forward, we believe we can enhance free cash flow
During 2006, we took important steps in a business
                                                             the last seven quarters. This performance is indicative    at an even faster pace.
renewal process and saw a resurgence of our already
                                                             of the strength of our financial businesses.
strong company. We designed strategies to revitalize
                                                                                                                        Historically, we've used the majority of our free
growth, expand operating margins and further
                                                             These strong financial results came in what some           cash to fund acquisitions. Our strong acquisition
differentiate our client services  building on our market
                                                             would call a transition year. In addition to a new CEO,    competency is a well known part of the company.
leadership position and long-term client relationships.
                                                             we had some difficult growth challenges resulting from     We are primarily interested in two types of acquisitions:
                                                             unusually large contract terminations and the sale of      first, those that add to the breadth of our distribution
The hallmark of the year was the unveiling of Fiserv 2.0,
                                                             our investment in a Canadian joint venture in 2005.        system  across any of our primary client segments;
the next generation of our company. That introduction
                                                             Given the headwinds coming into 2006, we are very          and second, those assets that bring specific
was the culmination of months of work by some of the
                                                             pleased with our financial results.                        add-on capabilities that will be highly valued by
best and the brightest people throughout Fiserv. We
                                                                                                                        our client base.
are committed to Value, Opportunity and Growth  for
                                                             Fiserv's combination of solid technology and superb
each of our key constituencies  for the next generation
                                                             people continued to deliver on behalf of our more          However, over the last several years, the acquisition
of our company. We intend to honor that promise.
                                                             than 18,000 clients. Our client satisfaction scores        markets have changed dramatically  and for a
                                                             remained very high, and we continued to retain clients     buyer like Fiserv, that change has been troublesome.
Delivering Value for Shareholders Today
                                                             at superior levels. In addition, more than 86% of our      Significant investment capital from private equity and
Early in 2006 we entered the prestigious Fortune 500
                                                             clients told us they are willing to promote our products   hedge funds, combined with access to relatively
for the first time, and we kept growing. Total revenues
                                                             and services, a clear indicator of the service quality     cheap debt financing, have driven pricing to what we
for the year increased 12% to surpass $4.5 billion.
                                                             we are bringing to the market.                             consider unsustainably high levels. Given our industry
Adjusted earnings per share from continuing operations
                                                                                                                        stature, we believe that we are one of the best suited
were strong as well, increasing 16% to $2.53 per
                                                             One of Fiserv's most important characteristics has         buyers for high quality properties  strategic or
share, which was at the top end of our 2006 guidance
                                                             always been its strong cash flow generation, which         otherwise. As a result, our objective will be to
and in line with our long-term performance outlook.




                                                                                                                                
      MORE 2006
      NUMBERS
                                        monitor acquisition opportunities in a disciplined             seven percentage points. We also outperformed the
                                        way, mindful of current market valuations.                     relevant NASDAQ indices for the year. We believe that
    22.3%                                                                                              with our premium business model, leading market
    consolidated adjusted operating
                                        During 2006, we spent $187 million on acquisitions,            position, and our intense commitment to delivering
    margin, up 40 basis points
                                        adding seven companies with product capabilities in            results  that are on average within our long-term
    year-over-year
                                        the insurance and financial segments. The majority             performance outlook  we should produce attractive
                                        of our available capital in 2006 went to repurchase            returns for shareholders.
                                        shares. We re-acquired 12.7 million shares of
                                        Fiserv stock for $560 million. Given the current               Our strong results this year are due to the efforts of
    86%                                 environment, we anticipate that we will continue to            our more than 23,000 employees around the world
    of clients would recommend
                                        allocate capital to share repurchase and use our               who make Fiserv a great company. For years, a
    Fiserv to other potential clients
                                        debt capacity to fund acquisitions.                            Fiserv credo has been that "people make the
                                                                                                       difference" and that continued to be true in 2006.
                                               Fiserv Stock Price           We recognize that it is    At Fiserv, our people are our key differentiator.
                                                     (as of Dec. 31)
                                                                            our job to deliver value   Thanks to each of you for your efforts and commitment
                                         $60
    $282M                                                                   for shareholders each      to making Fiserv its very best.
    spent on product development
                                                                            year, and over time.
                                                                 $52.42
                                                                            In 2006, Fiserv's total    Providing Opportunities for
                                          50                                shareholder return was     Shareholders Tomorrow
                                                       $43.27
                                                                            21%, which compared        Our efforts to define our strategic direction culminated
    $447M                                      $40.19                       favorably to an overall    in September with the introduction of our long-term
    in free cash flow generated
                                          40                                strong equity market.      plan branded Fiserv 2.0  which included our
                                                                            Our shareholder return     renewed vision and mission. Our mission statement
                                                                            exceeded the 14%           captures the essence of Fiserv. Not because it's on a
                                          30                                performance of the S&P     placard, but rather because we believe it. Serving
    21%                                         04        05           06
                                                                            500 by approximately       clients is deeply rooted in our organizational DNA.
    increase in share price




4
It's the essence of what Fiserv has been about since      client success. The theme line neatly communicates           very early in our evolution, we are pleased with our
its founding 22 years ago. The key to our mission is      the essence of our new direction.                            progress and confident that our success will acceler-
enabling more success for our clients. It's a simple                                                                   ate over time.
                                                                         Your Success.
notion  and therein, elegant. It's an idea that we
believe can serve as a greater purpose for our                                                                         We have also adopted five enterprise themes to unlock
                                                                     That's the Point of 2.0
employees around the world.                                                                                            the significant potential we see in the company  and to
                                                          This translates to serving clients differentially. We are    enhance our revenue, earnings and cash flow growth.
                                                          switching to a client segment delivery approach from
"Our mission is to provide integrated
                                                          our more business-unit-centric model. While effec-              Active Portfolio Management
technology and services solutions                         tive and often successful, the prior model didn't               Enhanced Client Relationship Value
that enable best-in-class results for                     always allow us to deliver the holistic solutions that          Operational Effectiveness
                                                          many of our clients prefer. At the same time, we
our clients."                                                                                                             Capital Allocation
                                                          don't intend to adopt a one-size-fits-all approach.             Innovation Inside
Our mission requires that we focus on high-quality        Clients and situations are different. We will utilize the
solutions and that we measure our performance             strength of our broad product and platform diversity         We have specific strategies and tactics in place to
not just on financial results, but on the success we      to meet the needs of a dynamic market. While it's            execute against each of these important areas  with
enable for our clients. Our clients' success is our
success. Our aspiration is to be the partner that
our clients turn to in helping them be their very best.
While we can't define success for each particular
client, we will develop solutions that help them
achieve their goals. It's bold and we mean it.

On the back cover of this annual report, you will see
an ad from a new Fiserv 2.0 campaign that is
designed to communicate our expanded focus on



    priorities linked to market opportunities. We are            institutions. We are one of only a handful of companies   Internal revenue growth is critical to our future success.
    again pleased with our early progress. We expect             in the S&P 500 with at least 15 consecutive years of      Our internal revenue growth rate is now solidly in the
    only limited impact in 2007, with more noticeable            growth in revenue and earnings. So, what's the big        mid-single-digit range  led by strong performance in
    benefit in our 2008 results and beyond.                      deal? The big deal is that we have the potential to be    our financial segment. We will build on that success
                                                                 even better.                                              and anticipate revenue growth acceleration coming
                                                                                                                           from four primary areas:
    Value Plus Opportunity Equals Growth
                                                                 The key to our future success is to combine what
                                                                 has made Fiserv strong in the past with the strate-          New Sales
    "Our vision is to be a global leader in
                                                                 gies that we have laid out for the future. Together, we      Integrated Value
    transaction-based technology solutions."                     believe this will accelerate growth  profitably  and       Additional Scale
                                                                 in a way that builds long-term shareholder value.            Network Value
    Is it really an aspiration to say you want to be something   That is the notion of Renaissance. It's our hope that
    that you already are? We are a global leader today.          this re-birth leads to a "re-formula-tion" of what has    Let's talk for a moment about Integrated Value.
    For the third year in a row, we were named number            made Fiserv great. By changing the formula just           One of our core strategies is to enhance client
    one on the FINTECH 100 ranking of the top                    enough, we are creating something even more               relationship value by delivering more of our products
    providers of technology services in sales to financial       dynamic for our stakeholders.                             and services to our installed base. As a result of the
                                                                                                                           work we did in 2006, we identified more than
                                                                                                                           $2.4 billon of revenue opportunity in our financial
                                                                                                                           segment client base available today. Although capturing
                                                                                                                           the entire amount would be extremely difficult, we
                                                                                                                           believe we can deliver $360 million of incremental
                                                                                                                           annual revenue over the next six years, which would
                                                                                                                           have a favorable impact on our revenue growth
                                                                                                                           rate. We expect to see the early signs of this
                                                                                                                           revenue lift in 2007, and build gradually over the
                                                                                                                           measurement period.


Accelerating revenue growth is an important element                               to evolving client needs, create an urgency that we
of our overall growth story. We can also boost growth                             appreciate in running your company. We are up to
through better execution, focus on segments of our                                the challenge.
business with higher macro-growth rates and margins,
                                                                                                                                                                                                     Ken Jensen
and proper capital allocation. We have an attractive                              Fiserv has always been a superb company. Our
business model that should produce strong long-term                               renaissance has us focused on an even brighter
results and attractive returns for our shareholders.                              future. We have an attractive market opportunity,
                                                                                                                                                            THANK YOU TO KEN JENSEN
                                                                                  a winning formula, and a committed management
                                                                                  team to execute the plan.
Why Now?
                                                                                                                                                            Fiserv said goodbye to Ken Jensen when he retired
Over the last year, the question people asked of Jeff
                                                                                                                                                            in July 2006. Ken, who had been with the company
most frequently was: "What has been the most surprising                           Fiserv 2.0 is the next generation of your company.
                                                                                                                                                            since its formation in 1984, served as senior executive
thing you have found at Fiserv?" His answer to the
                                                                                                                                                            vice president, chief financial officer, treasurer and
question was simple  the opportunities inside the
                                                                                                                                                            assistant secretary of the company. Ken is also a
company are far greater than he imagined. While most
                                                                                                                                                            member of the Fiserv Board and will retire as a
of us don't usually like to be wrong, this was one time                                                                                                     director in May 2007.
when he didn't mind at all!
                                                                                  Jeffery W. Yabuki
                                                                                                                                                            Ken is known as the chief architect of all things financial
                                                                                  President and Chief Executive Officer
While the opportunities are real, they won't last forever.                                                                                                  in Fiserv and is most famous for his leadership in the
                                                                                                                                                            acquisition arena. The company completed more
There are several catalysts combining to drive change
                                                                                                                                                            than 135 acquisitions during his tenure, creating billions
in our industry. We have seen competitors changing
                                                                                                                                                            of dollars of shareholder value. Ken's intellectual
hands over the last year. Buyers appear to be attracted
                                                                                                                                                            curiosity and challenging queries are legendary
by the allure of our industry structure, strong business
                                                                                                                                                            throughout the company.
model, and consistent cash flows. There are now                                   Donald F. Dillon
                                                                                  Chairman of the Board
several very large payments players, alongside
                                                                                                                                                            Perhaps Ken's greatest strength was his internal
international and domestic competitors, all fighting
                                                                                                                                                            compass. He chose a path and stuck to it. His com-
to get a larger piece of the pie. These factors, added
                                                                                                                                                            mitment to the company  its clients, employees and
                                                                                                                                                            shareholders  produced great results for the
                                                                                                                                                            22 years he called Fiserv home. We are all better for
                                                                                                                                                            the opportunity to have worked with him.


                                                                                                                                                            We thank Ken for his many years of outstanding
                                                                                                                                                            contributions to Fiserv. We wish him and his family all
                                                                                                                                                            the best as they embark on the next phase of their lives.
