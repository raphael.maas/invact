To Our Shareholders:

This is my first annual letter to
you as CEO. Im pleased to report
that our business continues to
perform well despite the tough
economy and that Iron Mountain
has made great progress advancing
our strategy of helping customers
navigate an increasingly complex
information landscape.
Our underlying business fundamentals
remain sound as evidenced
by our performance last year. We
achieved our financial goals with 12%
revenue growth and, excluding asset
gains and losses, 13% OIBDA growth,
while driving increased capital
efficiency. These results reflect the
resiliency of our business, as well as
the focus and discipline of 21,000
Mountaineers around the world.
Financially, Iron Mountain
finished 2008 as a stronger company.
We delivered record cash flow
and improved our balance sheet,
finishing the year with a record low
consolidated leverage ratio1 of 3.8x.
In the absence of significant acquisition
activity, this ratio will naturally
decrease as our business continues
to grow. We will continue to be judicious
when it comes to investment
opportunities, but we remain biased
for growth.
Our long-standing strategy has
been to maximize our core businesses,
export those businesses to
international markets, and drive additional
revenue opportunities with
the introduction of new services that
solve our customers information
management problems. Our North
American Physical Business best
defines our core, consisting of hard
copy Records Management, Offsite
Data Protection, Secure Shredding
and Document Management
Solutions. The North American
Physical Business continues to grow
and strengthen, as demonstrated
by our 2008 results of solid revenue
and contribution growth, 9% and
14% respectively. Under talented
leadership, we advanced our growth
agenda in our government and

healthcare verticals and in our
Document Management Solutions,
while at the same time improving
productivity in transportation, real
estate and operations. This is a
solid business with strong recurring
revenues and great competitive
positioning benefiting from our
continuing investments in securing
our customers information assets.
We continue to expand our
international business. In established
markets like the United
Kingdom, we follow the North
American strategy of driving growth
and returns. In emerging markets
such as Continental Europe, we
invest ahead of revenue to build
sales and marketing capabilities
and facility capacity to service our
customers. This strategy is dilutive
in the near term, but should drive
strong returns over time. For the
most nascent markets, we co-invest
with joint venture partners to build
our presence and initial demand.
So far, weve successfully come full
circle with this approach and have
acquired ten joint ventures, and they
are some of our best performing
international businesses. Today, we
operate joint ventures in Denmark,
Greece, India, Poland, Russia,
Switzerland, Turkey and throughout
Southeast Asia. Taken together, we
expect our international businesses
to drive high growth and improved
returns that approach North
American results over time.
Our largest investment for
bringing new services to market
is within our Digital business. In
2008, Digital represented 7% of
total revenues or $223 million. As
part of our growth agenda, we will
increase the relative size of this
business over time through both
organic growth and targeted acquisitions.
Additionally, we continue to
build out our leading Storage-as-a-
Service platform for data protection,
archiving and eDiscovery. The Digital
team delivered strong financial
performance despite a tough environment
for selling technology. This
performance was supported by our
business model where upwards of
75% of our revenues are recurring;
and customers, when faced with
IT budget constraints, prefer a pay-
as-you-go buying model. This was
also our first full-year with Stratify,
Inc., a leading eDiscovery business
we acquired in late 2007. Stratify
had a great year as the team delivered
world-class solutions to our
customers who are facing pressure
from increasing litigation. Stratify
was able to expand its reach through
Iron Mountains core sales force, as
well as through business relationships
with Navigant Consulting, Inc.
and PricewaterhouseCoopers LLP .
In addition to the solid performance
of our businesses, Im
particularly encouraged on two
important fronts: the development
of our people and the evolution
of the markets they serve. Team
development is an area of strategic
emphasis so that we can capitalize
on large, emerging market opportunities.
It is our teams ability to
execute that has just earned us a
place on the S&P 500 Index and
landed us for the fifth consecutive
year on Fortune magazines
list of the Worlds Most Admired
Companies. Team development at
Iron Mountain is central to fulfilling
our potential as a service provider.
The key point to remember
with respect to our markets is that
information is growing without
restraint. The confluence of that
growth along with increased regulation
and tight customer budgets is
what drives demand for our services.
We help uncover sources of value
for our customers by giving them
practical advice on how to manage
their information in ways that
reduce their overall costs and risks.
To that end, we recently introduced
several important new services. Iron
Mountains Virtual File Store is a
service that allows our customers
to store their inactive digital data
with us at a very low cost. Our new
CloudRecovery service, offered
jointly with Microsoft, provides
hosted offsite storage for customers
using Microsoft System Center
Data Protection Manager. And our
Digital Record Center for Medical
Images, offered in collaboration with
Hewlett-Packard, is a long-term, lowcost
archiving service for hospitals.
Each of these three services allows
our customers to store and manage

their information assets more
securely and cost effectively than
they could themselves.
In addition to building our
digital platform, we continue to
aggressively invest in new services
that complement our core business,
specifically Document Management
Solutions. During 2008, we began
to build significant scale on this
platform thats designed to give
our customers faster access to their
information and, in the process,
allow them to derive greater business
value. When combined with
hardcopy records storage, we
offer a comprehensive solution for
managing information as it lives
across paper and digital formats. We
will continue to innovate and deliver
secure services that create efficiency
while also assuring customers that
their information will be there when
they need it.
As we move through 2009, we
will continue to focus on driving
solid performance and maintaining
the financial flexibility to invest
against large market opportunities.
Our priorities are to expand our
core service offerings, particularly
Document Management Solutions,
and add to our Digital platform. We
are continually improving how and
where we invest, acting with a bias
towards growth and returns over
the long term. We intend to deliver
consistently strong financial results
as we invest against our growth
agenda. A strong liquidity position,
combined with the predictability of
our recurring revenue base, allows
us to invest with confidence in
building value for the future. Iron
Mountains fundamentals are solid,
the growth agenda is compelling,
and we continue to improve execution
within our operations.
Here at Iron Mountain, we fully
appreciate that our employees,
customers and investors have been
significantly hurt by the economic
downturn. We know we are fortunate
to have such a strong, resilient
business with great future potential.
Our stability allows us to serve
as a reliable employer to 21,000
Mountaineers who provide 120,000
customers with peace of mind while
saving them money. Even in this
economy, we remain confident in
our ability to produce solid results
and invest for the long term with all
the predictability that has historically
made Iron Mountain such a
solid investment.
Yours truly,
Bob Brennan