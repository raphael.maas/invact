TO OUR SHAREHOLDERS
In 2012, as Alexion celebrated its 20th anniversary as a
biotechnology innovator, we again strongly advanced our
mission to develop and deliver life-transforming therapies for
patients with severe and life-threatening disorders that are
also ultra-rare. Our achievements during 2012 spanned our
key commercial and clinical growth initiatives:
 In PNH (paroxysmal nocturnal hemoglobinuria)  bringing
Soliris (eculizumab) to more patients with PNH in our
core countries of the United States, Western Europe and
Japan, as well as to growing numbers of new PNH patients
in additional countries
 In aHUS (atypical hemolytic uremic syndrome) introducing
Soliris in the United States as the first and only treatment
for patients with aHUS, and preparing for our Soliris
launches in aHUS in the major European countries
 Beyond PNH and aHUS  advancing our broad development
pipeline, with lead clinical programs now investigating
five highly innovative compounds, including eculizumab,
as treatments for patients with nine additional severe and
life-threatening ultra-rare disorders
 And overall, achieving significant growth across our
business while maintaining strong financial discipline to
achieve increasing operational leverage
2013: Reaching Milestones Across
Our Growth Initiatives
With a global operations platform that now serves patients
in nearly 50 countries and the most robust pipeline of
innovative therapeutic candidates in our historywe are on
track to reach an unprecedented number of key milestones
across our commercial and clinical initiatives in 2013. Our
objectives for the year include:
 In our approved indications for Soliris reaching more
patients in countries where commercial operations are
already in place, and accelerating our market entries
in new countries. In PNH and aHUS alike, our steadfast
objective is that every patient who can benefit from Soliris
will have access to Soliris
 In our development programs for new indications with
eculizumab advancing our clinical programs in lifethreatening, ultra-rare disorders in multiple therapeutic
areas, including nephrology, neurology and transplant
 In our development programs with highly innovative
therapeutics beyond eculizumabaccelerating the clinical
development of asfotase alfa as the first treatment for
patients with hypophosphatasia (HPP), and progressing the
development of our other highly innovative therapeutics
including our cPMP replacement therapy for patients
with molybdenum cofactor deficiency (MoCD) type A;
ALXN1102/1103, our alternative complement pathway
inhibitor; and ALXN1007, a novel anti-inflammatory antibody
Serving More Patients with PNH Worldwide
Even as we drive forward with these wide-ranging growth
initiatives, our focus on serving patients with PNH has never
been greaterespecially since, on a global basis, the majority of
PNH patients have not yet received an accurate diagnosis, let
alone commenced appropriate treatment. PNH is a debilitating
and life-threatening ultra-rare blood disorder characterized by
uncontrolled complement activation leading to complementmediated hemolysis (destruction of red blood cells). Before
Soliris, approximately one-third of patients with PNH did not
survive more than five years from the time of diagnosis. With
the availability of Soliris, the outlook for patients has changed
dramatically: In the initial PNH registration trials, 100 percent of
patients had an objective response to Soliris, and long-term data
published by independent investigators from patients treated
with Soliris for up to eight years showed improved survival rates
similar to normal healthy individuals matched for their age
and gender. In 2012, data presented at the American Society of
Hematology (ASH) annual meeting further demonstrated longterm efficacy and safety in PNH patients treated with Soliris
over the course of 10 years.
The growing body of data regarding both PNH and the clinical
benefits of Soliris is vital for patients. As with many ultra-rare
disorderseven those that are severe and for which there is
an effective, approved therapypatients with PNH often suffer needlessly because there may be little awareness amongst the
medical community of how to recognize or treat their disorder.
In 2013, the seventh year of the global introduction of Soliris,
we will build on the medical evidence as we continue to expand
our PNH disease awareness programs and diagnostic initiatives.
Since 2007, these programs have been vital to educating the
medical community on which patients are at greatest likelihood
of having PNH, the best ways to arrive at an accurate diagnosis,
and the most effective treatment options.
In 2012, as in the past, we served substantial numbers of
new patients with PNH in each quarter across our longest
established territories in the United States, Western Europe
and Japan. Importantly, we also began to serve initial
patients in Turkey, Brazil and Russia as we implemented
disease awareness programs and diagnostic initiatives. In
2013, we will increase our penetration in all countries in
which we are currently active. Additionally, we look forward
to significant opportunities to serve patients in parts of Latin
America, including Argentina, Colombia and Mexico, as well
as additional countries including Korea.
Bringing the Unprecedented Hope of Soliris to
Initial Patients with aHUS
As we continue our mission to transform the lives of patients
with PNH, we have now also begun to transform the lives of
patients with aHUS, a chronic, ultra-rare and life-threatening
disease in which a genetic deficiency in one or more
complement regulatory genes causes chronic uncontrolled
complement activation. The lifelong uncontrolled complement
activation in aHUS progressively damages vital organs, leading
to stroke, heart attack, kidney failure and premature death.
Prior to the availability of Soliris for the treatment of aHUS,
65 percent of patients required dialysis, had permanent kidney
damage or died within the first year of diagnosis despite
receiving plasma exchange or plasma infusion (PE/PI). As in
PNH, 100 percent of patients in the aHUS registration trials
had an objective response to Soliris.
Following strongly positive results in our prospective clinical
trials in aHUS, the FDA and European Commission granted
marketing authorization for Soliris as the first and only
approved treatment for patients with aHUS at the end of
2011. Both approvals resulted in strong and broad labels that
include pediatric and adult patients with aHUS. In 2012, the
first full year in which Soliris was available to patients in the
US, we were able to rapidly meet the urgent medical needs
of patients presenting with aHUS.
We enter 2013 with a foundation in place for a strong global
launch in aHUS. The devastating course of this disorder, together
with the life-transforming benefits of Soliris treatment, provides
a solid underpinning for rapid diagnosis, reimbursement and
treatment of aHUS patients around the world. We are pleased
to have built strong support among key opinion leaders who
recognize the life-transforming impact that Soliris is having
on patients with aHUS. Our efforts are further supported
by a growing body of clinical data demonstrating the longterm benefits of Soliris for patients with aHUS. This includes
two-year data from the prospective Phase 2 trials that were
presented at both the American Society of Nephrology (ASN)
and ASH annual meetings in 2012, which showed continued
improvement in hematologic and renal function with Soliris.
Additionally, an established group of European aHUS clinical
experts published a paper supporting Soliris as first-line therapy
in patients confirmed to have aHUS.
In the US, a broad range of aHUS patients are being diagnosed
and treated with Soliris, including both children and adults,
and those with and without a history of receiving plasma
exchange. Patients with aHUS are also being treated by
multiple specialties and in both hospital and outpatient
practice settings. As is also our experience in PNH, we are
seeing positive results from our diagnostic initiatives and
disease awareness programs for aHUS. For example, an aHUS
diagnostic pathway has been introduced to hematologists
and nephrologists with positive impact, which we believe
will facilitate the more rapid diagnosis of patients with aHUS.
Importantly, patients with aHUS in the United States are
gaining rapid access to Soliris through broad reimbursement. In Europe, we are now serving aHUS patients in Germany
and initial patients in other countries are also receiving
Soliris on a named-patient basis or through local funding
authorities. We are also on track to complete the
reimbursement processes for aHUS in most of the larger
Western European countries and to commence individual
country launches during 2013. While our European teams
are planning for their individual country launches, we have
also recently submitted our supplemental Biologics License
Application (sBLA) for aHUS in Japan, an important step for
subsequent access to Soliris for Japanese patients. We look
forward to serving an increasing number of patients with
aHUS around the world throughout 2013 and beyond.
Serving More Patients with More Severe
and Ultra-Rare Disorders
Beyond our current commercial initiatives in our approved
Soliris indications, we are keenly focused on the need to
develop additional highly innovative uses for the breakthrough
complement-inhibition technology of eculizumab. We are
equally driven by the need to develop new therapeutic agents,
both within and beyond complement-mediated diseases.
The expertise we have gained from our experience with Soliris
informs all of our pipeline development decisions. We are
among the very few companies to have successfully developed
and commercialized a high-value ultra-orphan therapy,
bringing it through clinical trials and regulatory approval, and
then commercializing it successfully in multiple indications
around the world on our own. Through this experience, we
have learned how to work with regulators to develop approval
pathways and trial protocols in disease settings in which
there are no clinical precedents. We have also gained valuable
experience in enrolling trial sites around the world to recruit
patients with the rarest and most severe disorders, as well
as in conducting successful disease education programs to
significantly raise awareness of a devastating and ultra-rare
disorder and the most effective ways to diagnose and treat it.
To best employ these skills on behalf of patients, we
now focus only on the most severe and life-threatening
disorders for which there are no approved or effective
therapies disorders that are simultaneously the rarest, in
which only very few patients are affected. We now have nine
such R&D programs under way, each of which is expected to
reach one or more important milestones in 2013.
Investigating the Potential of Eculizumab (Soliris)
Beyond PNH and aHUS
Our five lead development programs with eculizumab are
focused on severe and ultra-rare complement-mediated
disorders beyond PNH and aHUS.
 Shiga-toxin E. coli-related hemolytic uremic syndrome (STECHUS) is a devastating and life-threatening ultra-rare disease
that develops in a subset of patients who become infected
with enterohemorrhagic E. coli (EHEC). Final 28-week data
from our single-arm eculizumab STEC-HUS trial, which was
initiated in Germany during the historic EHEC outbreak in
mid-2011, were presented at the ASN meeting in November
2012. The data showed that the full clinical study cohort of
198 patients had a rapid and sustained improvement in TMA
and reversal of organ damage with eculizumab treatment.
Importantly, we are also obtaining and analyzing additional
control data from an exploratory matched-control analysis
of patients with severe STEC-HUS receiving eculizumab
versus other patients who received only best supportive
care. The preliminary findings from this analysis showed
that eculizumab treatment was associated with consistently
higher rates of improvement in renal and neurological
function at 8 weeks and 28 weeks, when compared with
supportive care only. We believe the additional control data
will strengthen our anticipated regulatory submission.
 Kidney Transplant is another area in which complement
can play a significant role. We currently have two
development programs under way:
 Acute Humoral Rejection (AHR)we are currently enrolling
patients at elevated risk of AHR, or antibody mediated
rejection, in a multinational living-donor kidney transplant
trial and a separate multinational deceased-donor kidney
transplant trial  Delayed Graft Function (DGF)we recently expanded our
kidney transplant program to include patients at elevated
risk for DGF as we sponsored an investigator-initiated study
 Neuromyelitis Optica (NMO) is a devastating, lifethreatening, ultra-rare neurological disease in which
uncontrolled complement activation causes damage
to the spinal cord and optic nerve, leading to paralysis,
respiratory failure, blindness and premature death. Positive
data from an investigator-initiated Phase 2 single-arm
study of eculizumab in patients with severe relapsing NMO
presented at the 2012 American Neurological Association
annual meeting showed eculizumab significantly reduced
frequency of attacks in patients with severe relapsing NMO.
Following recent meetings with regulators in the US and
Europe, we are accelerating this program and expect to
initiate a single multinational registration trial.
 Myasthenia Gravis (MG) is a debilitating and sometimes
life-threatening neurological disorder in which
uncontrolled complement activation leads to destruction
and inflammation at the neuromuscular junction, resulting
in severe muscle weakness. Following encouraging results
from a Phase 2 study in severe and refractory MG, we have
started regulatory discussions and are planning to initiate
our next study.
Beyond Eculizumab: Highly Innovative
Therapeutics to Treat Ultra-Rare Disorders
Our development programs beyond Soliris are guided
by what we know well and do well, applying our proven
skills exclusively to patients suffering from severe and
life-threatening illnesses in ultra-rare settings. As with
our eculizumab programs, we focus only on therapeutic
candidates with strong potential to transform patients
lives  not just provide an incremental benefit.
 Asfotase alfa is a highly innovative, late-stage biologic being
developed for patients with hypophosphatasia (HPP), an
inherited and life-threatening ultra-rare metabolic disorder
that leads to progressive damage to multiple vital organs,
including destruction and deformity of bones. One-half of
newborn patients with HPP die within one year, and there
are no effective treatment options. Clinical trial results from
a single-arm study published in the New England Journal
of Medicine in 2012 showed a dramatic clinical benefit of
asfotase alfa and an objective response in 100 percent of
study participants. Alexion is currently accelerating the
clinical development program for children with HPP, with
our objective to file for pediatric registration in the US
and Europe. In addition, we are completing the current
clinical development program for adults with HPP. Beyond
the US and Europe, we have started a registration trial
in Japan. During 2012, we also completed an important
phase of the process development work in the asfotase alfa
manufacturing program to support our clinical studies and
eventual commercialization.
 cPMP replacement is an investigational therapy for
the treatment of patients with molybdenum cofactor
deficiency (MoCD) type A, a severe, ultra-rare and genetic
metabolic disorder that is fatal in newborns. Early case
studies with cPMP have shown highly encouraging results
in individual patients, and we are driven by the potential
for cPMP to change the grim outcomes that these infants
currently face. We are now planning to initiate the first
prospective clinical studies with cPMP.
 ALXN1102/ALXN1103 are the intravenous and
subcutaneous versions, respectively, of our novel
alternative pathway complement inhibitor, which we are in
the early stages of investigating as treatments for severe
and ultra-rare inflammatory disorders. We are progressing
through our current Phase 1 clinical trial, with the
objective to develop subsequent clinical studies with this
novel complement inhibitor.
 ALXN1007, a novel anti-inflammatory antibody designed
to target severe and ultra-rare inflammatory disorders,
is a product of our antibody discovery technologies. We
recently completed a single-dose Phase 1 clinical trial in
56 patients and are planning to discuss a multi-dose study
with regulators.Continued Strong
Financial Performance
2012 was another year of sustained
growth in revenues and profitability
for Alexion. For the full year, we
recorded sales of $1.134 billion, an
increase of 45% compared to 2011.
By maintaining fiscal discipline
as we have grown our global
operations to now serve patients
in nearly 50 countries, we achieved
a 60% increase in non-GAAP net
income to $425.2 million in 2012.
In 2012 we reported robust yearon-year revenue growth across all
of our countries. Revenues in the
US increased 52% during the year,
reflecting continued growth in PNH
and the first full year of the aHUS
launch. This strong growth in our
largest market six years after the
initial launch supports the robust
long-term growth potential of the Soliris franchise. Revenues
in Europe increased 27% on a constant currency basis more
than five years following our initial EU approval. Likewise, we
achieved strong growth of 40% in our PNH operations in AsiaPacific, largely related to Japan. The growing momentum of
our global expansion is further indicated by the 141% year-onyear growth we experienced across our newer markets. Taken
together, the diverse operations that we are building around
the world provide us with a foundation for long-term growth
in PNH, aHUS and future indications.
Strengthening Our Human Capital and Resources
Today, Alexion has more than 1,500 employees working in
Company facilities in 35 countries. We are grateful for the
passion and commitment of our talented and dedicated
employees as we work together to serve patients with ultrarare diseases. In 2012, we welcomed two new leaders to
Alexions executive team: John Moriarty
joined Alexion as Senior Vice President and
General Counsel, and Frank Wright joined us
as Senior Vice President and Head of Alexion
Pharma International Srl. Both are seasoned
executives who will play key roles as we grow
our multi-product portfolio in an increasing
number of countries.
To support our continued growth, we have
also announced the relocation of our global
headquarters from Cheshire, CT to New
Haven, CT in 2015. Our new headquarters
will provide a global center for our research
initiatives and business operations around the
world to accelerate our efforts to serve more
patients with severe and life-threatening
ultra-rare disorders.
Looking Ahead in 2013
Our history of innovation since 1992 will
serve as the guide for future growth. We
are inspired by the number of lives we have been able to
transform through the introduction of Soliris for PNH and
aHUS, and we will employ our expanding global presence
and skills to deliver additional breakthrough therapies for
patients with devastating and ultra-rare disorders who
currently suffer without hope. We are thankful for all those
who make our work possible: our employees, Board of
Directors and shareholders, as well as healthcare authorities
around the world. Most of all, we thank the patients,
families and physicians we serve around the world as we
strive to reach new levels of innovation on their behalf.
Leonard Bell, MD
Chief Executive Officer
April 2013