Dear fellow shareholders:
I am pleased to report that your Company achieved nearly
every goal set by your Board of Directors for Fiscal 2006,
while also completing the most significant renovation in
Heinzs history. As a result, we enter Fiscal 2007 with
strong momentum and a favorable outlook.
Importantly, the Companys proven ability to generate
excellent cash flow continued into Fiscal 2006, and we took
the opportunity to share much of it with our shareholders.
From Fiscal 2003 to Fiscal 2006, we have returned $4.2
billion in dividends and share repurchases to you, our
shareholders. In fact, Heinzs total shareholder return
over the past three years from the time of the spin-off of
our non-core U.S. businesses on December 20, 2002,
through February 3, 2006 (prior to the recent run-up in
the share price) was 18.9 percent, compared to 16.0
percent for the Standard & Poors food group.
In Fiscal 2007, we will continue to return cash to you with
a substantial 16.7 percent increase in the dividend. We
also plan to return yet another $1 billion to shareholders
in share repurchases over the next two years.

Heinzs plans for accelerated growth in Fiscal 2007 build
on the significant progress your Company has made over
the past four years, during which we have:
 Greatly simplified and improved the Company;
 Refocused on 10 core brands, which have grown at a
three-year compound average rate of nearly 6 percent;
 Reduced stock keeping units (SKUs) from 35,000
to 16,500;
 Concentrated on 11 key markets;
 Substantially improved the capabilities of our people
and processes; and
 Eliminated layers of bureaucracy across our North
American, European and Pacific headquarters.
Heinz, as a result, is now structurally the Company the
Board and I envisioned four years ago, with a sharp focus
on three value-added categories generating more than
90 percent of the Companys sales and 95 percent of its
economic profit.
Progress can be measured by the following:
 The Heinz North American Consumer Products division,
the first to benefit from our simplification efforts and
portfolio repositioning against core brands, has averaged
nearly double-digit profit growth since Fiscal
2003. Fiscal 2006 was the strongest year yet for the
Consumer Products team, with sales up 13 percent.
 Heinzs businesses in Australia and New Zealand have
performed even better following a similar initiative to
simplify and refocus their businesses. Since Fiscal
2003, these two divisions have delivered a combined
sales growth of approximately 15 percent.
 Our top brands are performing better than ever with
our flagship Heinz brand growing sales at a three-year
compound annual rate of 6 percent across the globe.
 In the U.S., Heinz Ketchup has grown sales at a threeyear
compound rate of more than 7 percent, boosted
by new packaging innovations and line extensions.
We are aiming to build on this success with the launch
of the new Fridge Door Fit bottle this summer.
 In our five emerging markets of Russia, Indonesia,
China, India and Poland, which account for 40 percent
of the worlds population, we have profitable businesses,
with costs among the lowest in the Company. Heinz
expects to generate approximately one-quarter of
its sales growth in Fiscal 2007 from these promising
markets.
 Heinz Europe, which has been the exception to this
growth trend up to now, has firm plans to grow our
iconic Heinz, Plasmon, HP and Lea & Perrins
brands, following the simplification initiatives and
divestitures of non-core businesses completed last
year. Our European teams have been strengthened
with the promotion of proven performers and
experienced leaders from other Heinz units and
top-tier packaged goods companies.

A realistic plan for superior value and growth
On June 1, we announced the Heinz Superior Value and
Growth Plan for Fiscal 2007 and 2008. The Plan, which
builds on our progress of the last four years, was widely
welcomed by investment analysts as both aggressive
and realistic. It has three goals:
1) Reduce costs to improve profit margins;
2) Grow the core portfolio through innovation; and
3) Generate cash to deliver superior shareholder returns.
1) Reduce costs
Heinz is using improved processes to drive out costs
that add no value for consumers or customers. New
information systems are driving savings in purchasing,
inventory management and waste reduction. Our trade
promotion management systems are boosting the
efficiency and effectiveness of our trade spending
programs. For example, the U.S. Consumer Products
team has improved its trade spend efficiency to where it
is now considered best-in-class by Cannondale Associates,
while still growing sales and volume for Heinz and its
customers. We are expanding these systems across the
Company to drive out inefficient spending and optimize
our returns. We successfully executed the rollout of a
new SAP enterprise management system in the U.K.
and Ireland this spring, without a hitch.
Further integration of our $6 billion global supply chain
is underway to focus on leveraging our scale in global
procurement, continuous improvement and plant
rationalizations. We plan a further $60 million savings
in selling, general and administrative costs (SG&A)

in Fiscal 2007. Overall, we expect these productivity
initiatives to combine with pricing and mix to improve
gross margin in Fiscal 2007.
In total, we identified $355 million in savings that we
expect to generate over the next two years. These
numbers, while appearing aggressive, are based on the
continued benefits from the initiatives we successfully
executed in Fiscal 2006, and are being implemented with
the long-term health of the Company in mind.
2) Grow the core portfolio through innovation
The goal of the Heinz Plan is to accelerate growth. This
years Annual Report is dedicated to Growth Through
Innovation, personified by the creative professionals
in the Heinz Global Innovation and Quality Center in
Pittsburgh, and our other global facilities.
Housed in a state-of-the-art complex, the new Innovation
Center has greatly improved our ability to develop
differentiated, great-tasting, convenient and nutritious
foods that enrich the lives of families everywhere.
Heinz has well over 100 new product initiatives planned
for Fiscal 2007, supported by our planned 19 percent
increase in marketing and advertising. These include
Fridge Door Fit Ketchup; Ore-Ida Easy Breakfast
Potatoes; new varieties of Classico sauce, including
new organic varieties; Mean Beanz and international
soups in Australia; and new recipe meals for toddlers
and HP Sauce in the popular upside down bottle in
the U.K., to name just a few.

Heinz is dedicated to helping consumers live healthier
lives, without sacrificing the joy of great-tasting foods.
Heinzs heritage is closely associated with healthy,
lycopene-rich tomatoes, and we offer a wide range of
natural and organic foods, as well as foods containing
reduced sodium and sugar. These include Heinz Organic
Ketchup and Reduced Sugar Ketchup, the Weight Watchers
Smart Ones nutritional meals range, all natural Classico,
Heinz Spaghetti Plus with calcium and Omega 3 in
Australia and healthier multigrain pasta in the U.K.
Confident in our ability to provide delicious foods eagerly
sought by consumers, Heinz plans double-digit increases
in research and development spending in each of the
next two years.
3) Generate cash to deliver superior
shareholder returns
The third pillar of the Plan is to deliver superior
shareholder returns with a keen focus on improved
performance, share repurchases and a planned dividend
payout ratio of approximately 60 percent. As you can
see from the charts below, this dedication to shareholder
value is not new.

Your Board is committed to superior shareholder value
and is ranked by Institutional Shareholder Services, a
preeminent corporate governance rating agency, as the
best in the S&P Food, Beverage and Tobacco group; and
among the top 10 in the entire S&P 500. Good corporate
governance and strict director independence are critical
to protect shareholders against special interest groups.
Your Board is a diverse, independent and intensely
engaged collection of men and women who represent
all shareholders. They set stretching, but realistic, goals
for management, and then hold us accountable for our
performance. We debate many issues, but ultimately we
are united in support of the Companys shareholders and
our other important constituents, including consumers,
customers and employees.
Todays Heinz is moving forward with a growing
confidence in our outstanding people, world-class brands
and our plan for building sustainable growth and superior
value. We appreciate the support received from hundreds
of shareholders over recent months, and want you to
know that we always will put the interests of all
shareholders first and foremost.
Sincerely,
WILLIAM R. JOHNSON
CHAIRMAN, PRESIDENT AND
CHIEF EXECUTIVE OFFICER