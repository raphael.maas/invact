DELIVERING
FISCAL 2017
Accentures very strong financial performance in fiscal
2017 reflects the continued successful execution of our
growth strategyin particular, the rapid rotation of our
business to new, high-growth areas.
We delivered all the objectives in our initial business
outlook for the year, driving broad-based revenue
growth that significantly outpaced the market, as well as
double-digit growth in earnings per share on an adjusted
basis. We also generated excellent free cash flow and
significantly increased our investments in the business,
while again returning substantial cash to shareholders.
Here are some highlights:
 We delivered record net revenues of
$34.9 billion, a 7 percent increase in local
currency, with positive growth across
the vast majority of our industry groups,
geographic markets and businesses.
 We generated very strong new bookings
of $37.4 billion, a 6 percent increase
in local currency.
 Diluted earnings per share were $5.44,
compared with $6.45 in fiscal 2016. After
excluding a $0.47 per share pension
settlement charge in fiscal 2017 and $1.11
per share in gains on the sale of businesses
in fiscal 2016, adjusted EPS of $5.91 in
fiscal 2017 increased 11 percent.
 Operating margin was 13.3 percent,
compared with 14.6 percent in fiscal
2016. Excluding the pension settlement
charge in fiscal 2017, adjusted operating
margin was 14.8 percent, an expansion
of 20 basis points from fiscal 2016.
 We generated free cash flow of
$4.5 billion and returned $4.2 billion
in cash to shareholders through
dividends and share repurchases.
 We announced a 10 percent increase
in our semi-annual dividend, to $1.33
per share, shortly after fiscal year-end.
1
We also reached an important milestone in fiscal 2017:
Net revenues from what we call the Newdigital, cloud
and security servicesgrew about 30 percent to $18 billion,
and accounted for 50 percent of total revenues for the
first time. With our rapid rotation to the New and our highly
diverse portfolio of business, we successfully executed in
an uncertain and increasingly competitive environment and
continued driving strong, profitable growth.
Our very strong performance in fiscal 2017
on top of our outstanding results for the last
two fiscal yearsclearly demonstrates that we
are executing our growth strategy in a durable
and sustained way.
For the last three fiscal years combined, we
delivered compound annual revenue growth
of 9 percent in local currency, as well as
9 percent compound annual growth in
adjusted earnings per share. Over the same
three-year period, Accenture has delivered a
compound annual total return to shareholders
(including dividends) of 20 percenttwice the
total return of the S&P 500 Index.
Leading in the New
We are in the midst of a technology revolution
that is disrupting and transforming businesses
and entire industries around the world. In this
context, we are working with our clients to
apply innovation and intelligence at the heart
of their organizations.
Our combination of end-to-end capabilities
and deep industry expertiseacross
Accenture Strategy, Accenture Consulting,
Accenture Digital, Accenture Technology
and Accenture Operationsis unique in
the marketplace. We are competing at scale
in each of our five businesses and driving
synergies across them to deliver tangible
business outcomes for our clients. We also
continue to leverage our unmatched position
in the technology ecosystem as the largest
independent services provider and the leading
partner of many key players.
This is why Accenture remains the partner
of choice for so many of the worlds leading
companies and largest government agencies.
We serve more than three-quarters of the
FORTUNE Global 500 and 95 of the top 100.
We continue to build strong and enduring
client relationshipsall of our top 100 clients
have been with us for at least five years, and
98 have been clients for 10 years or more.
We are especially pleased with the
leadership positions we have built in digital,
cloud and security services as we rotate
to the New consistently and successfully
around the world.
 DigitalAccenture Interactive has been
recognized as the largest provider of digital
marketing servicesboth globally and
in the United Statesand is working with
many of the worlds leading brands to
transform their customer experiences.
We helped Melia Hotels, the European
2
Our very strong performance in fiscal
2017 clearly demonstrates that we are
executing our growth strategy in a
durable and sustained way.
hospitality company, implement a digital
transformation strategy to increase sales
through data-driven customer segmentation. In just one year, direct sales increased
27 percent and more than 1 million people
joined Melias rewards program.
 CloudAccenture is helping many leading
companies migrate seamlessly to the cloud
to realize the benefits of increased agility
and lower costs. We are working with
Hess, a leading global independent energy
company, to implement a cloud-based,
as-a-service operating model. With our
cloud solutions, analytics and automation,
Hess is increasing efficiency and agility
across its asset base while benefiting from
consumption-based pricing.
 SecurityWe are working with clients
to deliver comprehensive solutions spanning
strategy development, risk management,
cyber defense, digital identity, application
security and managed security services.
For a global retailer, we are modernizing
its digital identity management system,
embedding artificial intelligence, robotics
process automation, chatbots and other
advanced capabilities to enhance security,
increase productivity and get products
into stores faster.
We are always looking ahead to anticipate
the next big waves of growth, in areas
such as artificial intelligence, blockchain,
augmented reality and quantum computing.
And we are already integrating these
technologies into innovative solutions for
many of our clients. Together with 1QBit,
a quantum software firm, we recently collaborated with Biogen, the biotech company,
to develop a new quantum-enabled molecular
comparison application that could dramatically speed up drug discovery for complex
neurological conditions.
Investing for the Future
To make Accenture even more relevant and
competitive, we continue to invest aggressively across the companyboth organically
and through strategic acquisitions. In fiscal
2017, we significantly stepped up our pace
of acquisitions, deploying approximately
$1.7 billion across 37 transactionsnearly
twice our investment in fiscal 2016with
the majority of our acquisitions in the New.
For Accenture, acquisitions are an engine
to drive organic growth by enhancing our
differentiation in the marketplace. In fiscal
2017, key acquisitions in digital included
SinnerSchrader, a digital agency in Germany,
and Paris-based OCTO Technology, a leading
digital consulting firm. In cloud, we acquired
DayNine, one of the largest providers of
Workday solutions, and Phase One, a leader
in Salesforce solutions for US federal clients.
And in security, we acquired Arismore, a
French company specializing in identity and
access management, and iDefense, a cyber
threat intelligence business.
We strengthened our capabilities in New IT,
including Agile development methodologies,
with the acquisitions of SolutionsIQ in the
United States and Concrete Solutions in Brazil.
In addition, we continue to build our expertise
in key industries with acquisitions such as Kurt
Salmon in retail, InvestTech in capital markets,
and Seabury Group in aviation.
3
We also continue to make significant investments in our unique Accenture Innovation
Architecture. Leveraging specialized
capabilities across the companyfrom
research, ventures and labs to studios,
innovation centers and delivery centers
we are pioneering new ways of collaborating
with clients to co-create, scale and deliver
disruptive, cutting-edge solutions.
As part of our innovation-led approach,
we continue to open new facilities around
the world. In fiscal 2017, we opened 16 Liquid
Studioswhere we accelerate software developmentand eight delivery centers as well as
innovation hubs in Bengaluru and Houston,
which bring together multiple elements of our
Innovation Architecture.
We also opened The Dock in Dublin, a multidisciplinary research and incubation hub, where
the entire Innovation Architecture comes to life.
The Dock is a launch pad for more than 200
designers, developers, researchers and experts
in artificial intelligence, advanced analytics,
augmented reality and the Internet of Things
to create and rapidly prototype innovative
solutions for our clients.
Accenture Labs, which celebrated its 30th
anniversary during the year, is applying
technology not just for clients, but also to
solve complex social challenges through its
Tech4Good initiative. Our team in Bengaluru
developed an artificial intelligence smartphone solution that delivers real-time narration
to help the blind navigate the world and live
more productive lives.
Our People and Our Communities
As a professional services company, Accentures
people ultimately make the difference in driving
innovation and delivering high-quality services
to clients. That is why attracting the very best
people and investing to further develop
their skills are among our highest priorities.
In fiscal 2017, we invested $935 million in
learning and professional development for
our employees, including substantial investments in re-skilling to help them stay relevant
in key areas such as cloud, artificial intelligence and robotics. In addition, in just over
18 months, we trained more than 160,000
people in New IT, including automation,
Agile development and intelligent platforms.
At Accenture, we have an unwavering
commitment to inclusion and diversity, and
we offer an inclusive environment regardless
of ethnicity, religion, gender, sexual orientation, age or disability. With our focus on
gender equality, we are pleased that women
now represent more than 40 percent of our
total workforce. Quite simply, our rich diversity makes Accenture stronger, smarter and
more innovative.
We are also committed to achieving our
vision of improving the way the world works
and livesfrom closing employment gaps
to advancing sustainable economic growth.
With our global capabilities, digital experience
and innovation mindset, we develop solutions
that address a wide range of social challenges.
By using technology to scale our impact,
we are well on our way to achieving our
Skills to Succeed goal of equipping more
than 3 million people by 2020 with the skills
to get a job or build a business. And through
our commitment to environmental sustainability, we continue to improve our energy
efficiency and to decrease per-employee
carbon emissions by leveraging the latest
collaborative technologies.
4
We are especially proud that Accenture was
recognized for the second year in a row on
FORTUNEs Change the World list for our use
of data analytics to help health care clients
save patients lives. In addition, we were
pleased to be recognized once again by
FORTUNE as one of the 100 Best Companies
to Work For, and by Ethisphere as one of the
Worlds Most Ethical Companies.
In closing, I want to thank all Accenture
people for their dedication and passion
in delivering value for our clients and our
company. We finished fiscal 2017 with strong
momentum, positioning us well for the new
fiscal year. With our highly differentiated
capabilities and focused investments in the
Newtogether with our disciplined management of the businessI am confident in
our ability to continue driving sustainable,
profitable growth.
5
Pierre Nanterme
Chairman & CEO
October 26, 2017