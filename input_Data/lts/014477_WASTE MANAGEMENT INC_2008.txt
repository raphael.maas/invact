2008 was a year of continuing strength and
progress for our company. More than ever,
we proved ourselves to be a company that
our customers, employees, shareholders, and
communities can count on to serve their needs
and protect the environment.
Its satisfying to be able to say this in light of
the years dramatic economic downturn, which
promises to present continuing challenges.
Like most businesses and individuals throughout
North America, we expect to face hurdles,
make sacrifices, and tighten budgets in the
coming year.
However, we have built a foundation of financial
and operational strength to sustain our company
through the inevitable changes in economic
seasons. We are solidly positioned to make the
adjustments that the times demand and to
continue to pursue our long-range goals.
The majority of our business, which relates to
providing essential services to commercial and
residential customers, is resistant to downturns
in the economy. This held true throughout 2008,
as internal revenue growth from volume in
those lines remained consistent. However,
we saw further volume declines in our more
economically sensitive businesses of industrial
collection, transfer, and recycling. Recycling
commodity revenues were affected by steep
market price declines and reduced volumes as a
result of much lower demand. We expect these
challenges to continue in 2009.
Given the weak economy, it is more important than
ever for us to maintain our disciplined approach to
pricing and cost control. We have learned from our
customers that the demand for higher quality
services is strong, which allows us to maintain our
pricing leadership. Already, we have restructured
our field operations through consolidation, reducing
the number of market area operations from 45 to
25 and eliminating duplicative functions. We have
We pledged by 2020 to have 25,000 acres of
protected land for wildlife habitat and 100 landfills
certified by the Wildlife Habitat Council. One
year later, we are already halfway to that goal,
with 49 of our landfills certified and a total of
21,000 acres around our landfills set aside for
wildlife habitat.
We pledged by 2020 to triple the amount of
recyclable materials we manage. Although recycling
markets have been severely impacted by the
economy, we made good progress on our pledge
during the year, opening four new single-stream
recycling facilities.
We pledged by 2020 to double our waste-based
energy output. In 2008, we commissioned eight
new landfill-gas-to-energy plants and launched a
new business to build these plants for third parties
like municipalities and other waste companies.
We also are pursuing a number of waste-to-energy
projects in the U.S. and overseas.
In 2008, Waste Management was again included
on the list of Most Ethical Companies compiled by
the Ethisphere Institute, an international think tank
dedicated to the research and promotion of
corporate responsibility. Ethisphere reviews more
than 10,000 of the worlds leading companies to
assess codes of ethics, sustainable business
practices, and comments from industry peers and
customers. Waste Management was the only
company named from the environmental services
and equipment category.
As we move forward, we will continue our focus on
effectively managing our resources, maintaining our
pricing and cost discipline, and driving efficiency
throughout the organization. We are mindful of the
focus of investors on cash generation, and we will
manage our capital in a manner that continues to
produce strong free cash flow.
One thing we know: great American companies are
fueled by flexibility and innovation. More than ever,
these qualities will be needed as companies
everywhere must rethink strategies, redefine their
response to changing customer needs, and reshape
their businesses by bringing forth fresh ideas. We
have the best people, the best technology, and the
best assets in our industry. Most importantly, we
build on these strengths with the willingness and
ability to flex when necessity demands itand to
innovate always.
This is what makes Waste Management a company
you can count on. Thank you for helping to make
it so.

Sincerely,
David P. Steiner
Chief Executive Officer
realigned our corporate staff to more efficiently
support the new field operations and expect these
combined actions to provide annualized savings of
more than $100 million.
We also see customers increasingly turning to
us for assistance with their sustainability practices
and with reducing costs. As the leading company
in our industry, we are committed to helping our
companyas well as our customers and our
communitiesthrive and prosper through business
practices that reflect social responsibility and
environmental excellence.
In late 2007, we set out four goals for sustainable
growth that benefit not only the environment, but
also our business as we continue to provide
solutions for critical future needs. Here is a
report on our progress:
We pledged by the year 2020 to lower our
emissions and increase our fuel efficiency
by 15 percent. We are working with engine
manufacturers to achieve this goal. The
21,000 vehicles in our fleet provide a pressing
incentive to lower fuel costs and protect the
environment. In 2008, we began field-testing
the first hybrid waste collection truck and a firstgeneration
hybrid dozer for use at our landfills.