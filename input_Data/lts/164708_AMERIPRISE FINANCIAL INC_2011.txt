Dear fellow shareholders,

Ameriprise Financial is stronger now than ever before. Thats a fact that few
financial services companies can claim. For four years now, the business
environment has presented a steady series of challenges, from the nearcollapse
of the financial system in 2008 to the global economic malaise
of 2011 and with several notable flare-ups in between. Over that period,
the markets have regressed, with the S&P 500 Index down 14 percent.
Through it all, including a distinctly difficult second half of 2011  when the
U.S. and Europe careened toward major debt crises, market volatility soared
and interest rates lingered on the floor  Ameriprise Financial has endured
and thrived.
Today, were a leading diversified financial services company with $631 billion
in assets under management and administration. We take a unique approach
to serving individual investors and institutions financial needs, combining
leadership positions in financial planning, wealth management, retirement
and asset management. And we maintain an exceptionally strong financial
foundation that reinforces our client relationships and ensures we can
continue to invest for growth and return significant capital to shareholders.
Meeting challenges from a position of strength
Certainly, our business is connected to the markets: volatile equity markets
impact asset levels and fees and create investor anxiety, while low interest
rates limit the income we earn in our spread businesses.
In 2011, just as we have for six years as
a public company and since our founding
in 1894, we met these challenges. The
company delivered record operating
revenues and earnings. For the year,
operating net revenues grew by 10
percent to $10.1 billion. Operating
earnings were $1.2 billion, up 4 percent
from a year ago, with operating earnings
per diluted share increasing 10 percent
to $5.00. And our operating return on
equity ended the year at 13.2 percent,
up from 12.9 percent at year-end 2010.

We also continued to strengthen a hallmark of our company: our financial
foundation. We maintained our large excess capital position, managed our
diversified investment portfolio effectively and reinforced our risk management
program. This strength enabled us to return $1.7 billion to shareholders
through share repurchases and dividends. In 2011, we repurchased 27.9
million shares of Ameriprise common stock for $1.5 billion  more than double
what we committed in 2010  and we announced two dividend increases,
which together represented a 56 percent increase in our quarterly dividend
from the prior year. In fact, with the latest increase, we have raised our
quarterly dividend five times since becoming a public company six years ago.
Our growth platforms: wealth management, retirement
and asset management
We have built our company to compete in two primary ways: as a leader in
wealth management and retirement in the U.S. and as an asset manager
with an increasingly global business.

We have invested heavily in our wealth
management and retirement business
over the past several years, and in
2011, we began to capture the benefits
of these initiatives: we generated solid
revenue growth, improved profitability
and increased advisor productivity.

Our advisors, 10,000 strong across the country, are the face of our brand.
They are energized and motivated. Advisor satisfaction and retention rates
for our more productive advisors are near record highs, and we have strong
momentum in our experienced advisor recruiting. Over the past three years,
more than 1,100 advisors moved their practices to Ameriprise, bringing with
them new client relationships and billions of dollars in client assets.
The strength of the Ameriprise brand is a key element of our growth story.
We continue to develop our MORE WITHIN REACH brand platform, and in
2011 we launched new television advertising, an expanded online presence
and enhanced local marketing programs. Our advertising reinforces our
rich heritage, deep client commitment and financial strength. The results
have been very good: client acquisition increased 10 percent over last year,
client satisfaction remains excellent and we continue to improve the overall
awareness of our brand.

Developing deep, long-term client relationships and serving consumers
retirement needs require a wide range of product solutions. We have a
broad suite of compelling products to help clients meet their goals, grow
their assets, generate income and protect their wealth. In fact, we have
earned top 10 positions in each of our main businesses, including mutual
funds, life insurance and annuities.
Our annuity and insurance businesses provide important retirement
capabilities, and we manage these businesses prudently. Our products are
competitive, our pricing is appropriate and our risk management is strong.

Unlike other companies that distribute their products widely, we provide our
variable annuities and life and disability insurance exclusively to Ameriprise
clients. We know them well, which improves our underwriting and supports
strong client persistency.
Our annuity and insurance product lines do more than offer income guarantees
and asset protection; they are important contributors to our asset accumulation
goals. Our new RiverSource RAVA 5SM variable annuity proved to be one of
our strongest product launches in years, and our new indexed universal life
insurance product is being well received by advisors and clients.

Overall, our wealth management and retirement business had a strong year.
Retail client assets were up 2 percent to $310 billion, driven by strong net
inflows in wrap accounts and variable annuities. And we made significant
progress in our ongoing efforts to increase advisor productivity, with operating
net revenue per advisor growing 12 percent in 2011 to a record $384,000.
We also improved the profitability of our advisory business considerably. Advice
& Wealth Management operating margin increased to 10.9 percent from 9.6
percent a year ago. And our insurance and annuity businesses continued to
deliver solid returns and provided important revenue and earnings diversity.
An increasingly global asset manager
While many consumers know Ameriprise as a financial advisory firm, asset
management also plays a critical and expanding role for our company. Our
asset management business ended the year with $436 billion in assets under
management, ranking eighth in long-term mutual fund assets in the U.S., fourth
in retail funds in the U.K. and 27th in global assets under management.

We operate two complementary asset management platforms: Columbia
Management in the U.S. and Threadneedle internationally. Each has a distinct
investment process and unique perspective

Together, we offer a broad selection of strong performing products, including
113 Morningstar four- and five-star rated funds. And we are expanding beyond
our historic strengths in the U.S. and the U.K. to serve investors in Continental
Europe, Asia, the Middle East, Australia and other markets.
Building on our strong asset management foundation
In the U.S., we have essentially completed the integration of RiverSource
Investments and Columbia Management and have an outstanding base
from which to grow. Were delivering strong long-term retail and institutional
investment performance. While the business experienced net outflows
in 2011, we captured market share in a difficult mutual
fund market characterized by equity fund outflows across
the industry. In the institutional business, were winning
important mandates and our new business pipeline
is strong.
At Threadneedle, a good year included significant wins:
the firm won a $14 billion mandate to manage Liverpool
Victorias insurance and pension portfolio  a major
institutional win in the U.K. And when Threadneedles
original contract with Zurich, its former parent, expired,
we retained management of all the existing assets.
In addition, Threadneedle continued to deliver strong
investment performance and is expanding its asset
gathering beyond the U.K. In 2011, approximately two-thirds
of Threadneedle sales were outside the U.K., excluding the
Liverpool Victoria win and former-parent sales.

Together, Columbia Management and Threadneedle represent a compelling
capability. Our asset management business generated $528 million in operating
earnings in 2011, up 29 percent in a challenging market, and our adjusted net
pretax operating margin of 33.2 percent was up from 30.1 percent a year ago.
While our results are strong, I am confident that we can grow even further
from here.

We are ideally situated
Ameriprise is well positioned to benefit from three significant long-term trends
shaping the U.S. retail financial services marketplace: the beginning of the
Baby Boomer retirement era, a growing mass affluent and affluent population
and the critical need for financial advice.
U.S. households hold approximately $25 trillion in financial assets, with
more than half held by retirees and those nearing retirement. Over the next
10 years, assets held by people 55 and older are expected to grow at a
much faster rate than the assets of people younger than 55. Investments
in IRA s, 401(k)s and other retirement products represent the bulk of total
household financial assets, and we offer product solutions across the
retirement spectrum.
Our target market is investors with between $100,000 and $3 million in
investable assets. This group holds a large portion of total U.S. financial
assets, and it is growing at a faster rate than other groups.
These two forces  a large retirement
opportunity and the large and growing mass
affluent and affluent population  drive a
third trend: a significant need for financial
advice. Consumers are concerned about their
ability to achieve their goals, including living
the retirement theyve envisioned, and many
are seeking help from a knowledgeable and
trusted financial advisor. As more consumers
learn about the holistic approach we offer,
I am confident they will choose Ameriprise.
The right time for Ameriprise
As the U.S. and other countries struggle
with significant economic challenges and
the inability to agree on a fiscal path for

the future, Ameriprise Financial continues to deliver for our clients, advisors,
employees and shareholders. Ameriprise is a strong, stable company with
a record of standing behind our products and our clients. Our decisions and
actions are prudent, strategic and for the long term.
I believe no other company can provide personal and comprehensive advice
to as large a client base as Ameriprise can. In fact, in 2012 were introducing
a new framework for our advisors to engage with clients about retirement,
including meeting essential needs, maintaining their lifestyle, preparing for the
unexpected and leaving a legacy.
For many Americans, conversations about retirement can be difficult, so were
making it easier for our advisors to discuss clients current financial positions
and the actions they should take to achieve a confident retirement.
Looking ahead
Few financial companies offer the investment opportunity of Ameriprise.
Were making steady progress as a leading diversified financial services firm
with two primary capabilities: wealth management and retirement, and asset
management. Our fee-generating, less capital-demanding businesses are
leading our growth and providing important diversification.
We will continue to balance investments in the business with ongoing
expense controls and re-engineering initiatives. Re-engineering is a core
discipline, and it provides important flexibility in a fragile economic recovery.
Regardless of the economic and market
environment, Ameriprise has the capital
strength and flexibility to endure.
Weve proven we can seize opportunities in
challenging times, and that we can accelerate
growth when markets are favorable. I feel great
about our capabilities and the opportunities
before us.
Thank you
Ameriprise Financial has been in business for well over a century, a fact
that gives us strength and important perspective. I have the privilege of
leading this great company, the past 11 years as Chairman and CEO, and my
executive leadership team is also long-tenured. Together, we share a deep
understanding of the firms capabilities, client needs and market risk, and we
embrace the responsibility of keeping Ameriprise Financial strong for all our
constituents: our millions of clients, our 10,000 advisors, our employees and
our shareholders.
I would like to thank our clients for entrusting their financial dreams to
Ameriprise. We will continue to work on your behalf and will stand with you
throughout the journey.
To our employees and advisors, thank you for representing our firm every day.
Our mission is to help people feel confident about their financial future, and I
appreciate all that you do to bring the Ameriprise client experience to life.
To my fellow members of the Ameriprise Financial board of directors, thank
you for your support and counsel. All Ameriprise constituents benefit from
your perspective.
And finally, to our shareholders, thank you for your ongoing trust in Ameriprise.
We will continue to do all we can to reward it.
Sincerely,
James M. Cracchiolo
Chairman and Chief Executive Officer


