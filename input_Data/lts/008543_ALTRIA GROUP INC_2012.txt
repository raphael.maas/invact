Dear Shareholder

I am pleased to report that Altria delivered strong results and
returns for our shareholders in 2012.
Altria grew its full-year adjusted diluted earnings per share
(EPS) by 7.8% behind the business performance of our
operating companies, complemented by higher earnings from
our equity investment in SABMiller. Our total shareholder
return of 11.8% for the full year outperformed our U.S. tobacco
peers and the S&P 500s consumer staples sector.

Altrias results were driven by our employees continued focus
on our Mission to own and develop financially disciplined businesses
that are leaders in responsibly providing adult tobacco
and wine consumers with superior branded products. Our
Values of Integrity, Trust and Respect, Executing with Quality,
Passion to Succeed, Driving Creativity into Everything We Do
and Sharing with Others guide our behavior as we pursue this
Mission and our business strategies.
Altria made significant progress in 2012 as we executed our
four core strategies to Invest in Leadership, Align with Society,
Satisfy Adult Consumers and Create Substantial Value for
Shareholders. We highlight some examples of our actions in
the pages that follow.
Our consumer products companies invested in strong premium
brands that provide the foundation for income growth.
Each of our tobacco companies seeks to grow income while
maintaining modest share momentum for its core premium
brands. Despite a continuing, challenging external environment,
our tobacco operating companies premium brands had
an excellent year as our companies continued making investments
for their long-term success. These companies grew
their adjusted OCI in the smokeable and smokeless products
segments and gained retail share in cigarettes, cigars and
smokeless tobacco for the full year of 2012.
In the smokeable products segment, PM USA introduced
Marlboros new brand architecture in 2012 and supported
Marlboros four product families, Red, Gold, Green and Black
with brand-building activities throughout the year. These activities
included the expanded distribution of products and equityenhancing
promotions that engaged millions of adult smokers.
PM USA also engaged adult competitive smokers with trialgenerating
promotions that PM USA moderated as the year
progressed. Middleton continued to enhance Black & Milds
product portfolio with seasonal offerings and the introduction of
Black & Mild Jazz untipped cigarillos into select states.
In the smokeless products segment, Copenhagen and Skoal
delivered solid volume growth and retail share gains on a
combined basis for the full year. These results were
primarily due to the ongoing contributions of Copenhagen
Long Cut Wintergreen and Long Cut Straight and the expansion
of Copenhagen Southern Blend.
In wine, Ste. Michelle delivered volume growth with a
continued emphasis on expanding distribution into offpremise
channels.

