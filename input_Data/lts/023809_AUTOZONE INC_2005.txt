We made AutoZone a stronger company in 2005. We continued our string of record earnings
and earnings per share since becoming a public company in 1991.

We invested more than $280 million to improve our current
stores, open 193 new stores, add a new distribution center,
and improve our strong technology infrastructure.
 We grew operating cash flows and maintained our investment
grade credit rating while using excess cash to repurchase
shares.
 We continued to expand the reach of our proprietary
brands across categories throughout the store. These
key brands, including Valucraft and Duralast, are some of
the strongest brands in the automotive aftermarket.
 We repositioned our Commercial business to not only
become immediately more profitable but to position it for
profitable future growth.
 We opened our 81st store in Mexico, while continuing to
expand our reach by opening our first stores in Puerto Rico.
 We launched 2006 by initiating the theme Live the Pledge.
We know winning in this business means providing exceptional
customer service and by living our Pledge, we will
ensure satisfied customers.
AutoZone has enjoyed a truly amazing history. Our meteoric
rise, since our first store in Forrest City, Arkansas in 1979,
is due to our incredible AutoZoners and their commitment
to customer satisfaction. I often get asked, What makes
AutoZone so different from others? Why have you been so
successful? While we enjoy many structural advantages,
the true answer always comes back to our AutoZoners. We
begin every meeting with the AutoZone Cheer, the AutoZone
Pledge, and we share Extra Miler stories. These stories are
from customers who took the time to share their incredible
experiences. Its this dedication to the customer that is at the
center of our operating plan for 2006. Our Vision is: Relentlessly
create the most exciting Zone for vehicle solutions!
Our three strategic priorities remain U.S. Retail, Commercial,
and Mexico.
Since being named President and CEO in March, our team
has performed a significant assessment of our performance
and our practices. While our industry has remained strong,
we have not been satisfied with our sales performance over
the past several quarters. We conducted extensive research,
and our customers confirmed our convictions. Focus on the
basics. We must exceed our customers expectations by
leading with trustworthy advice. We must have the right
merchandise at the right price, at the right time in order to
deepen our relationship with our customersone transaction
at a time. In an environment where our customers disposable
incomes continue to be challenged by historically high
gas prices, and competition for those customers exists, we
must exceed our customers expectations. Our focus is to
Live the Pledge.

AutoZoners always put customers first!
We know our parts and products.
Our stores look great!
Weve got the best merchandise at the right price.
Our Pledge clearly articulates our commitments to our customer.
As we Live the Pledge, we remain confident AutoZone
will continue to be incredibly successful.
The Environment We See
Our industry continues to experience stable growth in spite
of the near-term macroeconomic challenges. Last year, the
Retail or DIY segment, an estimated $35 billion* industry, grew
at 4%* while Commercial, an estimated $48 billion* industry,
grew at an even faster 4.8%* clip. This marks the 14th consecutive
year of steady growth in both of these segments. With
the average age of a passenger vehicle on the road being
9.5 years and rising, the need for AutoZone is greater than
ever. Additionally, Americans are driving longer distances
and own more vehicles per household than ever before. It
will be our job well into the future to service their needs.
U.S. Retail: Continues to Be Our #1 Priority
With 3,592 stores in the U.S. at the end of our fiscal year,
and an expectation to continue to open new stores at a midsingle
digit growth rate, we are firmly committed to servicing
all our customers needs in this, our core business. With an
approximate 13%* market share in this $35 billion* industry,
we continue to see many years of profitable growth. Our
success in the upcoming year will be driven by several initiatives
to help our AutoZoners succeed. At the store level,
well be providing our AutoZoners with significantly more
training. We will increase the number of AutoZoners with ASE
(Automotive Service Excellence) certifications. Our customers
deserve that. We will continue to focus on helping our
customers complete the job right, the first time, with the right
parts, at the right price! From the store manager to the newest
AutoZoner, were committed to having our AutoZoners
have the tools and knowledge necessary to deliver trustworthy
advice to every customer. These are the basics and
they are what have made us the number one destination for
auto parts.

Our customers have spoken through numerous surveys,
and we have listened.
 We will intensely focus on our stores to improve the
customer shopping experience by making sure they
are properly merchandised and well presented.
 Were also offering an expanded special order network
that allows us to offer, on a next-day basis, more than
double the number of unique items we previously carried.
The initiative, named Project Got It!, more than doubles
the available skus to our customers to over 750,000 on
a same day or special order basis!
 We are expanding the hours of operations in many of
our stores.
 We will make sure our marketing messages are informative
and effectively delivered to our targeted audience.
This relentless pursuit of improving the customer experience
will also be seen through improvements in store appearance.
We have purposely reduced the amount of off-shelf merchandise
placements in our stores to allow a smoother shopping
experience. Were continuing our store refresh program
and increasing our relocation efforts to meet the changing
needs of our customers.
Our goal continues to be to optimize shareholder value. We
started four years ago with a Return on Invested Capital
(ROIC) of 13%, and Im proud to say today we have achieved
almost 24%, one of the highest in all of retail!
Commercial: Continued Significant Growth Opportunities
Commercial, our second strategic priority, is a far newer
business for AutoZone than the DIY segment. Weve grown
the business to roughly $720 million in just 10 years! Sitting
today with only an approximate 1.5%* market share, we see
significant growth opportunities with both the smaller up-anddown-
the-street customers and the larger chain players.
With 2,104 Commercial programs at the end of 2005, we
believe we can add to that count while enhancing the relationships
we have with our current customers.

While growth potential is huge, 2005 was a repositioning
year for the Commercial business at AutoZone, as we focused
on increasing profitability. While this effort had immediate
positive effects to the bottom line, it reduced sales. Our
management team determined that it was imperative to focus
on growing the business profitably, for the long term. Weve got
a superb team in place to lead this renewed effort, and weve
launched new initiatives to jumpstart our sales performance.
At the core of the business for us sits the Commercial Specialist.
This AutoZoner handles all the day-to-day orders from our
professional customers. In the past six months weve provided
the Specialists with new training, technology and marketing
tools to go and win the business. Through the use of our new
PDA handheld devices, our Specialists and Drivers record
and time-stamp every phase in a Commercial sale. This new
knowledge allows us to place the right resources when and
where theyre most in demand. Additionally, we developed
and launched a new marketing initiative that offers the more
you buy, the more you save incentives. We understand this
business is won day in and day out at the customer level.
Our Commercial Specialists must be trained and motivated
appropriately to win the business. While the industry is highly
fragmented, with the top three players having only an approximate
17%* market share, it can be highly competitive. In order
for us to win, we will focus on our strengths: national reach,
extensive parts availability, speedy delivery service, and
industry leading technologyall to provide excellent service.
And, we have ALLDATA. It continues to be the premier provider
of automotive diagnostic and repair information to the
professional technician. With over 60,000 customers today,

and the continued expansion into a full suite of service products,
ALLDATA continues to provide a competitive advantage
in the development of this customer base.
With a low startup requirement for capital investment and
the ability to leverage our pre-existing stores and distribution
system, Commercial will continue to be our second overall
priority as it is highly accretive to our ROIC.
Mexico: Steady Growth, Future Potential
Our third priority continues to be Mexico. At the end of 2005,
we had 81 stores in Mexico being serviced by a distribution
center in Nuevo Laredo and a store support center in Monterrey.
Mexico represents superb growth opportunities. Our stores
in Mexico have customized inventory and are unique to the
Mexican automotive landscape as most competition is poorly
capitalized and dedicated to very specific lines of products
and services.
We will continue to open stores in Mexico, as we expect to
celebrate our 100th store opening in 2006.
The Future
I can promise you we will continue to be efficient stewards
of your capital. We will maintain our financial disciplines and
focus only on those initiatives that exceed our after-tax return
on invested capital hurdle. This past year, while successful
by many companies standards, we certainly were not satisfied.
We will never be satisfied. My first task as the new CEO
was to travel extensively, talking to AutoZoners across the
Company to hear what they said. I also talked to a tremendous
number of customers. We performed broad-scale
customer research and came to the conclusion that our
future growth doesnt reside with any one new initiative, or
any other silver bullets for that matter. Our success has and
AutoZone continues to deliver record EPS growth.
However, we cannot simply sit back and expect
continued successes. Complacency is not an option.
We have to be relentless. Our plan for 2006 is about
focusing on the basics. Get the blocking and tackling
right, and our customers will continue to generously
reward us.

will continue to come from our AutoZoners. What struck me
over and over again was the passion they have for our business.
When AutoZoners go the extra mile to help the customer,
we hear about it. Our customers love to tell us about
their great experiences. Our success has been built on
exceeding customers expectations and we are determined
to do that by focusing on the basics. We have to make sure
were doing the right things every day to win every customers
business.
In summary, AutoZone continues to be the leader in an exciting
and fast growing industry. We have a clear plan for the
future and a strong, experienced team to execute it. We are
focused on operating AutoZone to exceed our customers
expectations and be the desired place of employment across
our industry, all while creating long-term shareholder value.
I would personally like to thank our AutoZoners across the
Company for their amazing dedication and commitment to
customer satisfaction. Our AutoZoners have an enormous
amount of pride in what they do. They are my inspiration!
I would also like to thank our CEO Team, comprised of the
40 officers of our Company, for their commitment to providing
passionate leadership to all our AutoZoners. They are incredible
leaders!
Can we grow this business, and grow it profitably into the
future? Yes. AutoZone is well positioned to profitably grow
sales. We have a clear plan for the future and a strong team
to execute it. I look forward to updating you on our continued
success well into the future.