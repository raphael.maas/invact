
Fellow Shareholders:

On behalf of the 68 thousand Xerox people around the world,
I am pleased  no, thrilled  to report that we have gone a long
way to restoring Xerox to good financial health and positioned
our company for a period of growth and opportunity. If I sound
bullish on Xerox, its because I am. We have weathered the
most serious crisis in our history and emerged a company that
is stronger, different and better  a company prepared and
resolved to take Xerox to new levels of greatness.

Perhaps most importantly, we believe we have
begun to earn your trust. A year ago we made
a series of commitments to you. And we have
already made good on most of them.
We said we would improve liquidity and we
did. Last year we generated operating cash
flow of $1.9 billion. Since the end of 2000 we
have reduced total debt by $4.4 billion. And we
ended the year with $2.9 billion cash on hand.
We said we would begin to drive equipment
sales and we have  by making 2002 our
biggest new product year ever. Equipment
sales declines have moderated substantially
in each of the last five quarters. Thats a very
encouraging trend and a key part of our strategy. Equipment
sales not only add revenue today, they add to our profitable
post sales revenue stream tomorrow.
We said we would improve our gross margins to the high 30s
and we exceeded our commitment. Our gross margins in 2002
were 42.4 percent. Some of that improvement was reinvested
as price decreases for our customers.

We said we would drive selling, administrative and general
(SAG) costs down and we have.We reduced SAG costs by
six percent last year and implemented actions under the
Turnaround Program which will reduce our total cost base
by $1.7 billion.
We said we would return Xerox to full-year profitability and
we did. After absorbing $670 million of pre-tax restructuring
charges, we were able to report a return to profitability in
2002. All of our major operations were profitable for the
full year.
Over the past two years, we have reduced costs, improved
liquidity, stabilized our business, strengthened our business
model, invested in our future and laid out a plan to return to
growth and to provide value for our shareholders. But all of
us at Xerox are keenly aware that last years accomplishments
are yesterdays news. The task before us now is to harness
our momentum and take Xerox to the next level of greatness.
We are well on our way.

Opportunities For Growth

Xerox has three strong opportunities for growth over the next
several years.
There is, of course, the production market which we lead and
which is growing at about two percent a year. It is expected to
reach $39 billion by 2006. Within that market, digital production
color  where Xerox has a commanding lead  is growing
much more rapidly at more than 30 percent per year.
A strong array of systems and solutions  led by the Xerox
DocuColor iGen3 digital production press  promises to
dramatically expand the market opportunity for Xerox. There
is every reason to expect that we can grow faster than the
market as a whole by driving the new business of printing
in areas such as one-to-one marketing and print-on-demand.
Early customer response to our new generation of color
production technology has been very encouraging.
Our second opportunity is in the office  a market that is
relatively flat, but very large at $52 billion a year. Xerox is
entrenched in the office and we are competitively advantaged
in segments of the market that are growing the fastest. Three
examples: color in the office, digital multi-function devices
and value-added office solutions and services.

As this report goes to press, we will be introducing new technology
for the office  products and services that will further
expand our fleet of worldclass offerings. They will include
new solutions, a new family of black and white multi-function
products, and color printers.
And our third opportunity is in the services market which is
growing at around 15 percent a year. This market includes
knowledge, content and document management  areas
where we have considerable expertise and resources.
Our highly successful document outsourcing business gives
us a strong base on which to build. More than 12,000 Xerox
employees currently work on site in hundreds of customer
offices around the world. They operate in-house printing and
copying centers, and manage document workflow across
the enterprise for our customers. More and more, these customers
are turning to us for help in redesigning processes,
improving productivity and strengthening customer relationships
 all starting from our base of knowledge and expertise
around the document.
In aggregate, these three opportunities  production, the office
and services  give us lots of room to run. We do not want for
opportunity. We are engaged in three attractive markets
where we can choose our sweet spots, add value for our customers,
grow profitable revenue for Xerox and reward our
shareholders for their investments. We estimate that over time
these three markets will yield five percent a year revenue
growth  even more if we are highly successful in areas such
as digital production printing, color, high value solutions for
the office and services.

Our Vision For The Future

If you think of Xerox as a world leader in office products like
copiers and printers, you are only half-right. To be sure we
have the broadest and deepest set of products in our industry
 an arsenal of products that were substantially strengthened
in 2002 and will be again in 2003.
But thats only part of the story. I spend a lot of my time 
every opportunity I get really  talking to Chief Executive Officers
and Chief Information Officers. I hear several common
themes emerge over and over. They need to drive significant
improvements in productivity. They need to 
access information quickly anytime, anywhere. They want to
outsource parts of their business that others can do more
effectively. They want to be able to market more effectively
and to serve their customers more efficiently.
Increasingly, these business leaders are turning to Xerox
for help:
 Thats why Bank of America asked Xerox to manage its
fleet of 63 hundred digital multifunction devices and two
thousand light lens copiers  a three-year contract worth
over $50 million that will save money for the bank.
 Thats why UnitedHealth Group called on Xerox to manage
their extensive human resource records as they migrate to a
paperless Internet world. Their twin goals are lower costs
and improved customer satisfaction.
 Thats why Lloyds TSB asked Xerox to manage seven
digital print centers that form the heart of their marketing
communications operations  at an annual expected savings
of $10 million.
In todays economy, businesses and organizations of all sizes
face the challenge of improving their performance or being
overtaken by the tide of change -- whether its more effective
communication with customers, leveraging what a company
knows, exploiting the opportunity of new technology, or simply
reducing costs and boosting productivity, improving
results requires rethinking how work gets done. This is what
Xerox does best. Our vision is both simple and powerful:
Helping people find better ways to do great work.
Since the introduction of the plain paper copier, Xerox has
been about transforming the workplace. Through our worldclass
research and technology; our expertise in printing and
production; our skills and experience in content, document,
and knowledge management; and our experience in process
innovation, we serve as change agents, enabling customers
to improve business processes and bottom line results.
What Shareholders Can Expect
It would not be an exaggeration to say that our performance
last year has gone a long way to restoring both our health,
our credibility and our confidence. Weve also made good
progress in the past year on corporate governance:
 We have adopted strict new guidelines on what constitutes
director independence. Applying this definition, 75% of our
Directors are independent.
 We have proactively integrated Sarbanes-Oxley and
proposed NYSE rules into our governance processes.
 We have revised and strengthened the charters for our
Board of Directors committees.
 We hold regular executive sessions of outside directors
without Xerox management present.
 We launched a massive effort to strengthen internal controls,
train our people and promulgate a clear and strong
Code of Conduct.
 We established an Ethics Helpline for our employees and
have taken other measures all aimed at making Xerox a
role model in ethical behavior.
 And, of course, in June we brought Larry Zimmerman on
board as new Chief Financial Officer.
Going forward, here is what shareholders can expect from
Xerox and from my management team.
First, credibility.We made a lot of commitments a year ago.
Weve delivered and weve executed. My intention is to continue
to earn your confidence and to give every ounce of my
energy to that end. When we say well do something, we will.
You should expect no less. We will deliver no less.
Second, communication.Well be open, honest and accessible
and well build an environment that encourages communication
with the investment community  freely, openly and often.
To enable you to make the wisest investment decisions, you
should have the information you need about the company as
quickly as practicable. My objective is to make sure you have it.
Third, leadership. In two ways. In the market through technological
and market leadership. But also in terms of our people
and our culture. As good as we are today, we will be better
tomorrow. You have no interest in a company that isnt leadership
in every way. Neither do I.
Fourth, decisiveness. Ill set clear direction for our people. Ill
hold people and businesses accountable. There will be no
sacred cows. If a product development program falters, well
end it quickly. If a business isnt profitable and has no credible
plan to become profitable fast, well shut it down. If we see an
opportunity, well grasp it. Decisiveness in planning and speed
in execution will become hallmarks of the Xerox culture.
Fifth, execution.We will continue to introduce competitive
technology, to drive costs down, to streamline our business,
and to execute against our business model. To ensure that
we are best in class in every aspect of our business, we have
introduced Xerox Lean Six Sigma  a powerful set of tools
that will drive improved results.
Our investors put a lot of faith in us. I take that very seriously.
And I insist that all our people do as well. We have too much
opportunity in front of us to fail. Im excited by the challenges
and so are our people. And were eager to give you a good
return on your trust.
All of us at Xerox firmly believe that our best days are still
ahead of us. Thank you for the confidence you have placed in
us. Our collective task at Xerox is to earn that confidence and
to give you a good return on your investment.

Anne M. Mulcahy
Chairman and Chief Executive Officer
