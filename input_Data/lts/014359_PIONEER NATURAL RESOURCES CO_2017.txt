FELLOW SHAREHOLDERS

I am pleased to report that Pioneer had another excellent year in 2017, with strong earnings, solid execution, robust oil production growth, excellent well performance and reduced production costs. The key drivers of this strong performance were the continued success of Pioneer's horizontal drilling program in the Permian Basin of West Texas and the outstanding efforts of our employees. Our Permian Basin asset is located in the top oil shale play
in North America and considered by many to be one of the best oil plays in the world. We are drilling low-cost, highly productive wells that generate high rates of return as a result of a low all-in cost of approximately $19 per barrel oil equivalent (BOE). We expect this cost to be reduced further over time as our operations become more efficient and benefit from technology and innovation improvements.
During 2017, Pioneer's stock performed in line with the average of its 11-company E&P peer group. Over the five-year period from 2013 through 2017 and the three�
year period covering 2015 through 2017, Pioneer's stock significantly outperformed the average of its peers. This strong performance reflected our strong balance sheet, attractive derivatives position, improving capital efficiency and excellent operational performance during a period of highly volatile commodity prices.
 


Total production grew by 16% during 2017 compared to 2016. Oil production increased by 19% over this same period and represented 58% of our total 2017 production. We added proved reserves totaling 314 million barrels oil equivalent (MMBOE) in 2017 from discoveries, extensions and technical revisions (excluding positive price revisions of 52 MMBOE, proved reserves divested of 7 MMBOE and proved reserves acquired of 1 MMBOE). These reserve additions, which replaced 309% of full-year 2017 production, were added at a highly competitive drillbit finding and development cost of $8.46 per BOE.


The Company reduced production costs per BOE by 12% in 2017 compared to 2016 (excluding production taxes). The decrease was driven by cost-reduction initiatives and increasing volumes of horizontal Permian Basin production, which had an average production cost of approximately $2.00 per BOE. Horizontal Permian Basin oil barrels are among the cheapest to produce in the world.
Pioneer is focused on optimizing the development of our extensive Permian Basin resource. We continued to improve our well productivity in this play during 2017 by utilizing higher-intensity fracture stimulations , which combine increased fluid volumes and proppant concentrations with longer laterals, optimized stage
lengths and clusters per stage. Our vertical integration operations contributed to this improved  productivity. We also continued to build out our Permian Basin infrastructure, including the construction of large- scale tank batteries and saltwater disposal facilities for
horizontal wells, the construction of additional field and gas processing facilities and the build-out of a field-wide water distribution system.
With growing oil production in the Permian Basin, we have increased our oil pipeline transportation capacity from West Texas to the Gulf Coast in order to sell our oil to U.S. refineries and into the export market. During the fourth quarter of 2017, we shipped approximately 115 thousand barrels of oil per day (MBOPD) to the Gulf Coast, of which approximately 90 MBOPD was exported to destinations in Asia, Europe and Latin America. Our longer-term target is to transport 70% to 80% of forecasted net oil production using dedicated pipeline transportation to the Gulf Coast.
 
2018 OUTLOOK
The petroleum industry has been operating in a lower oil price environment since late 2014 , when North American oil prices began declining due to a worldwide oversupply of oil. During the fourth quarter of 2016, Organization of Petroleum Exporting Countries (OPEC) members and some nonmembers, led by Russia, pledged to reduce their oil output by roughly 1.8 million barrels a day from October 2016 levels to draw down worldwide inventories and reduce a global oversupply . The agreement
became effective in January 2017 and was originally set to expire in March 2018. In November 2017, OPEC members and some nonmembers agreed to lengthen the output reductions through December 2018 . These
output reductions represented an unprecedented level of cooperation among oil-producing countries and, coupled with healthy oil demand, resulted in an increase in oil prices during 2017.
In 2018 , the worldwide demand for oil is expected to further increase as economic growth around the world is forecasted to be stronger than the last several years. This demand increase is expected to be met by higher supplies of oil from U.S. shale production growth and further oil inventory drawdowns . The Company expects ongoing oil price volatility during 2018 as compliance with the output reduction agreement, changes in oil inventories and actual demand growth are reported.
Earlier this year, we announced plans to divest the Company's South Texas, Raton and West Panhandle assets. I want to personally thank all of our employees who operate and manage these assets for their


commitment and dedication. These employees have created significant value for our shareholders over the many years we have owned these assets. After these divestitures are completed, Pioneer will be a Permian Basin "pure play." At that time, the Company expects reported revenue per BOE to increase and operating expense per BOE to decrease, thereby significantly improving reported cash operating margins.
As a Permian Basin and, further, a Midland Basin "pure play," Pioneer is well positioned to drill high-return wells, grow production and bring forward the inherent net asset value associated with this asset. We are forecasting Permian Basin oil production growth in 2018 ranging
from 19% to 24% compared to 2017. Total Permian Basin production growth, including NGLs and gas, is also forecasted to grow 19% to 24% compared to 2017 on a BOE basis. We expect internal rates of return for the 2018 drilling program to average 65%, assuming an oil price of
$55.00 per barrel and a gas price of $3.00 per thousand cubic feet (MCF).
In addition to maintaining strong returns on invested capital, we also believe in returning cash to shareholders and recently announced an increase in the Company's semiannual dividend to $0.16 per share (equivalent to
$0.32 per share on an annualized basis). Concurrently, we announced a common stock repurchase program for 2018 to offset the impact of dilution associated with employee stock compensation awards.

STRONG FINANCIAL POSITION
Pioneer continues to maintain one of the best balance sheets in the energy industry. We had $2.2 billion of cash on hand (including liquid investments) at the end of 2017,
 
with a ratio of net debt to 2017 operating cash flow of
0.3 times and net debt to book capitalization of 5%. We continued to be rated mid-investment grade by Moody's, S&P and Fitch. In addition, attractive oil derivative positions improved 2017 cash flow and margins.
The 2018 capital program of $2.9 billion, which includes
$2.65 billion for drilling and completion activities and
$260 million for water infrastructure , vertical integration and field facilities , will be funded from forecasted cash flow of $2.8 billion (assuming an oil price of $55 per barrel and a gas price of $3 per MCF), proceeds from asset divestitures and cash on hand. Derivative positions are expected to cover more than 85% of forecasted 2018


Permian Basin oil production and more than 60% of torecasted 2018 Permian Basin gas production. The ratio of net debt to forecasted 2018 operating cash flow is expected to remain below 0.5 times throughout the year.

TEN-YEAR VISION

We are in year two of our 10-year plan and remain committed to achieving oil production greater than
700 MBOPD and total production greater than 1 million barrels oil equivalent per day in 2026. By steadily increasing the pace of drilling our low-cost, high-return Permian Basin horizontal wells through 2026, we expect to deliver robust cash flow growth that will self-fund
our capital program, improve our corporate returns and ultimately generate significant free cash flow.
Looking forward, the breakeven oil price required to fund our planned capital program declines from approximately
$58 per barrel in 2018 to approximately $50 per barrel in 2020 and approximately $40 per barrel in 2026. At an oil price of $55 per barrel and a gas price of $3 per
MCF, our cash flow is expected to grow by approximately 20% annually and to exceed $11 billion in 2026, and our return on capital employed is forecasted to increase from approximately 5% in 2018 to 15% in 2026 or sooner.
 

 

CONTINUED FOCUS ON SAFETY AND THE ENVIRONMENT
During 2017, the Company published its inaugural Sustainability  Report. The report highlights the programs and initiatives that we have implemented over many
years to protect the environment; ensure the health, safety and professional development of our employees; promote high standards of integrity and business conduct; and positively impact the communities in which we live and work.
Pioneer had safety and environmental accomplishments in a number of important areas in 2017. Focusing on creating an incident-free and injury-free workplace, we continued reducing our OSHA recordable incidents and motor vehicle incidents. Our environmental initiatives resulted in a significant reduction in reportable spills
and spill volumes. Building out our successful water management infrastructure in West Texas and securing additional supplies of non-potable water allow us to significantly reduce the use of fresh water in our fracture stimulation operations. Reusing non-potable water from our fracture stimulation operations is also contributing
to this initiative. The Interstate Oil and Gas Compact Commission (IOGCC) recognized Pioneer's efforts in this area by presenting us with their Environmental
Partnership Award for our water conservation projects in the Texas cities of Midland and Odessa.


OUR PEOPLE PROVIDE A COMPETITIVE ADVANTAGE
Having great assets isn't enough. We have to execute day in and day out. This would not happen without
the commitment to excellence displayed by Pioneer employees. We benefit from having a great culture, which we continue to enhance, and a diverse and inclusive workforce with strong technical skills, the inspiration to pursue the application of new technology and innovations in our operations and a focus on achieving success.
In 2017, we were named the fifth-best place to work among large companies in the Dallas/Fort Worth area based on a survey of our employees conducted by The Dallas Morning News. This is the eighth year in a row that Pioneer has ranked in the top five.
I want to thank each and every person at Pioneer for the tremendous contributions they made to our continuing strong performance in 2017 and for giving
 
back in meaningful ways to the communities where they work and live by volunteering their personal time and resources.
In closing, we have successfully managed through three years of lower commodity prices by diligently focusing on reducing costs, improving well productivity and
maintaining a strong balance sheet. Looking forward, we are well positioned to execute at a high level and enhance shareholder value by growing cash flow, returning cash
to shareholders and improving corporate returns.



Sincerely,

 

Timothy L. Dove
President and Chief Executive Officer
