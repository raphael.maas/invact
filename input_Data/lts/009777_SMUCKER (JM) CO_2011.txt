Dear Shareholders and Friends:
Our family  all 4,500 of us at The J. M. Smucker Company 
is pleased to share a successful year of accomplishments
with you. Fiscal 2011 delivered impressive results and
performance despite the challenging economic environment.
These results prove that when a dedicated team is focused
on Our Purpose  bringing families together to share
memorable meals and moments  and implementing a
clear strategy, strong financial results and enhanced
shareholder value will follow:
 Sales grew to $4.8 billion, an increase of five percent
over last year, due to strong performance across
many of the brands in our portfolio.
 The strength of our brands and our ability to manage
successfully through a volatile commodity cost
environment resulted in a seven percent increase
in non-GAAP earnings per share to $4.69.
 We repurchased approximately 5.7 million common
shares that represented over four percent of shares
outstanding. We also increased our dividends paid
to shareholders by 17 percent.
C ontinuous investment in our Company has resulted
in a portfolio of iconic brands, the majority of which hold
category-leading market positions. This leadership reflects
our long-held philosophy of delivering great products at a
fair value. We believe value is as much about quality,
consistency, and trust as it is about price.
This past fiscal year, our brands have delivered this
type of value in numerous ways with impressive results:
 N ew product innovation contributed significantly to
the Companys growth, particularly within our coffee,
peanut butter, and baking brands.
 O ur consumer communications efforts continued to
drive brand equity, and we continued to be the primary
share-of-voice for most of our categories. As a result,
we produced an unprecedented number of television
and broadband video advertisements during the year.
 We dramatically increased our digital marketing and
social media programs, which now comprise over
10 percent of our Companys media spend.
We are particularly proud that our communication
efforts continue to be executed in an effective and familyfriendly
manner. The Parents Television Council has
honored Smucker with the #1 ranking on its Top Ten
Best Advertisers list recognizing companies that advertise
only on responsibly produced entertainment programs.
Long-Term Perspective
As an independent company with a history of leadership
continuity, we are able to manage our business with a
long-term perspective. Our unique culture, combined
with this long-term perspective, has enabled us to deliver
consistently strong financial results. On a 10-year basis,
our total shareholder return has outpaced the average
of the Standard & Poors packaged foods industry, as well
as those of broader market indexes.
We remain committed to preserving our Independence
and ensuring leadership continuity because both have been
significant contributors to our culture, financial performance,
and ability to serve our business and constituents 
consumers, customers, employees, suppliers, communities,
and shareholders.
A ccordingly, throughout our history, we have proactively
planned for leadership succession. As part of this process,
several executive appointments and realignments became
effective on May 1, 2011, the start of our 2012 fiscal year.
 V incent C. Byrd is now President and Chief Operating
Officer with responsibility for the Companys U.S.
Retail businesses. A 34-year veteran of the Company,
Vincent was previously President of U.S. Retail Coffee.
 Mark T. Smucker, who has served in Company
leadership roles for 13 years, most recently as President
of Special Markets, now heads U.S. Retail Coffee
as President.
 P aul Smucker Wagstaff, a 15-year Company veteran,
has assumed the role of President, U.S. Retail Consumer
Foods in a newly consolidated business area combining
the current Consumer business with the Oils and
Baking business.
 Steven Oakland, who was President of Smuckers,
Jif , and Hungry Jack, is now President, International,
Foodservice, and Natural Foods. Steven has been with
Smucker for 28 years.
 Barry C. Dunaway, formerly Senior Vice President,
Corporate and Organization Development, with
24 years experience with the Company, is now Senior
Vice President and Chief Administrative Officer,
overseeing Human Resources, Legal, Corporate
Development, and Information Services.

 Mark r. Belgya, our Senior vice president and chief
Financial officer and a 26-year veteran of the
company, adds internal audit to his responsibilities
that include accounting, investor relations, Financial
planning, Tax, and Treasury.
in addition, effective august 16, 2011, richard Smucker
will serve as sole chief executive officer, while Tim Smucker
will continue to serve as chairman of the Board with a focus
on the Board of Directors, corporate strategy, succession
planning, support for china growth opportunities, and will
be an ambassador of our culture with our constituents.
lookinG ForWarD
While Our Vision is to own and market north american
food brands that hold the #1 market position in their
respective categories, we also believe it is important to
embrace a global perspective for long-term growth.
as we look toward fiscal 2012, we remain confident in
our ability to execute our long-term strategy and remain
committed to growing our business with contributions from
all three of our growth drivers  category and market share
growth, new products, and acquisitions. ongoing investments
in product launches and marketing initiatives will further
strengthen the trust consumers have in our brands.
Managing through the challenging commodity cost
environment will also remain a primary area of focus. We
continue to utilize a combination of price increases and cost
saving initiatives to offset higher costs. With our consistent
approach to pricing transparency, the strength of our brands,
and our teams ability to execute, we expect to continue to
effectively manage through this period of commodity
cost volatility.
We recently acquired the coffee brands and business
operations of rowland coffee roasters, inc., including its
leading hispanic brands Caf Bustelo and Caf Pilon. While
respecting and preserving the rich heritage of these brands,
we believe they will benefit from our increased marketing
support, go-to-market strategy, and strong national presence.
We anticipate these brands will be a great complement to
our existing portfolio. achieving a seamless integration
will be a key priority in fiscal 2012.
china, with its vast consumer population, is a significant
opportunity for us to consider. We have allocated resources
to review these opportunities and to evaluate various means
of entry into the chinese market.
our purpoSe
With an uncertain economic recovery, consumers remain
thoughtful about their choices and are more conscious
than ever of brand value. in such an environment, Smucker
is well positioned to meet their needs with quality products
available through our various distribution channels.
We will continue to work to fulfill Our Purpose of
bringing families together to share memorable meals and
moments. We encourage you to read more about Our
Purpose in this report. ultimately, our ability to fulfill
this Purpose, achieve Our Vision, and consistently
deliver solid financial results is a reflection of the hard
work and dedication of our employees. We were honored
to again have our employees recognized by FORTUNE
magazine, naming Smucker as one of the 100 Best
companies to Work For. We attribute our inclusion
on this list to the quality of our employees. Together,
we share the same Basic Beliefs  Quality, People, Ethics,
Growth, and Independence  and draw upon these Beliefs
to guide us on a daily basis.
it is our family-sense of sharing in a job well done that
has made The J. M. Smucker company what it is today and
positions us well for continued growth. To our employees,
thank you for your continued commitment; and to our
shareholders, thank you for your continued support.
Sincerely,
Tim Smucker richard Smucker
June 22, 2011