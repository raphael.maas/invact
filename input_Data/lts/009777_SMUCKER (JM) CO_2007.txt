Dear Shareholders and Friends:

Fiscal 2007 was another successful year for The J. M.
Smucker Company. The years strong results were achieved
despite commodity-driven cost increases incurred across
the Company. Key financial highlights include:
- Sales, excluding divested businesses, were up
five percent, and net income grew 10 percent.
- Net income per share was $2.76, up from $2.45 last
year, a 13 percent increase.
- Cash flow from operations
exceeded $273 million,
allowing for a dividend
increase for the 27th time
in the last 30 years.
Fiscal 2007 confirmed that
we have the right strategy,
that our business remains
strong during challenging
economic conditions, and that
we are blessed with talented
and dedicated employees.
For the 10th consecutive
year, thanks to our employees
efforts, our Company was
listed among FORTUNE magazines
100 Best Companies
to Work For.
Maintaining Our Vision
We remain committed to our
strategic vision of owning
leading North American food
brands, and  through consistent,
creative marketing
efforts  we continue to strengthen our brands in
consumers hearts and minds. This strategy has yielded
share-of-market growth in almost all of our brands.
In the last several months, we completed three acquisitions
that complement our core businesses: Eagle Brand,
North Americas number-one brand of sweetened condensed
milk; White Lily flour, cornmeal, and frozen biscuits;
and Five Roses flour in Canada. These acquisitions
represent an incremental $250 million in annual sales,
strengthening our leadership position in the baking aisle.
In fiscal 2007, as part of our ongoing efforts to strategically
refine our portfolio, we divested our Canadian nonbranded,
grain-based foodservice and industrial businesses.
We maintained the momentum of our new product
initiatives with this years introduction of more than
40 items. These products were developed to provide consumers
with choices that are good and good for you,

easy for you, and that make you smile. Highlights
include a line of Pillsbury Reduced Sugar cake mixes and
frostings, a milestone reformulation of all Crisco shortening
to contain zero grams trans fat per serving, and new varieties
of Smuckers Uncrustables sandwiches, adding to that
lines double-digit growth trend. These innovations and
many others, along with a number of new organic items
across our product categories, are key to our future growth.
Our Commitment to
Doing the Right Thing
We are a principles-based
company. Our Basic Beliefs 
Quality, People, Ethics, Growth,
and Independence  have been
our foundation for 110 years.
Our employees are committed
to these values and to each
other, as described in our statement
Why We Are  Who We
Are... Our Culture. This ensures
our ability to provide the quality
food products we are proud to
stand behind.
Because of our long-held
belief in doing the right things
and doing things right, environmental
and social responsibility
is nothing new at The
J. M. Smucker Company. We are
dedicated to pursuing renewable
energy, source reduction, and
responsible disposal, and we
support employee efforts to volunteer for activities that
improve their communities.
We believe encouraging families to eat together is also
the right thing to do. As illustrated in Miriam Weinsteins
book, The Surprising Power of Family Meals, research
shows that families who eat together are stronger, smarter,
healthier, and happier. We feel privileged that people
invite our brands into their homes and lives every day,
making us part of that important ritual, the family meal.
In summary, we are committed to all of our constituents,
and we thank you for your confidence in our
ability to grow your Company.

Sincerely,
Tim Smucker
Richard Smucker