Dear Shareholder,

Janus Capital Group has been taking big strides
since New Years Day, when we merged with Stilwell
Financial, our former parent company. With every
step, were focused on building a more competitive
business model  one that produces long-term value for
our shareholders. And though weve set an ambitious
agenda, weve already covered a lot of ground in a
short time.
In announcing the merger last September, we made
commitments to:
 Continue improving fund performance so that at
least two-thirds of our funds are ranked in the top
two quartiles by Lipper.
 Diversify Janus product lineup by offering new
investment disciplines to complement our growth
and core equity funds.
 Brand the products of our new subsidiaries and
leverage them through Janus global distribution
network.
 Streamline operations to create a stronger,
more efficient company thats well positioned to
grow profitably.
DOING WHAT WE SAID WED DO
After only a few months as a public company, Im
proud to report that Janus Capital Group is off to a
fast start in delivering on our commitments.
Continuing to Improve Fund Performance
Nothing is more important to me than restoring
investor confidence. To that end, our investment team
leaders made several changes in 2002 that are starting
to pay off. (Please see page 6 for details.) Although
Im encouraged by the significant improvement in
last years results on a relative basis, I realize that
absolute performance is what counts  and thats
what were focused on delivering.
Broadening our Product Lineup
Weve introduced several products from our new
subsidiaries that complement Janus growth emphasis
and, importantly, establish Janus Capital Group as a
single source for diverse investment specialties.
These new disciplines include mathematical riskmanaged
funds from Enhanced Investment
Technologies (INTECH) and value funds from Perkins,
Wolf, McDonnell and Company and Bay Isle Financial.
Meanwhile, Janus more conservative core equity and
fixed-income funds continue to grow rapidly as investors
seek shelter from the markets volatility.
Leveraging Janus Global Distribution Network
In our business, distribution is everything. With this
in mind, weve quietly built a broad, open distribution
system that helps us meet the needs of investors,
clients and key partners around the world. As we
continue to broaden our product lineup, we can
leverage that powerful network by marketing a wider
range of investment disciplines under the
well-recognized Janus brand. And were delivering
our products in every conceivable way  through the
direct, no-load channel, through advisers and
financial intermediaries, through traditional institutional
relationships and through our fast-growing
offshore efforts.
Increasing Operating Efficiency
Meanwhile, were managing this business as
efficiently as possible. We replaced the holdingcompany
structure with an operating company. Weve
brought Bergers subsidiaries into our fold and are
dissolving Berger Financial Group, which will eliminate
redundant expenses such as salaries, facilities and
back-office operations. We expect these actions to
result in savings of $40 million annually.

FINANCIAL RESULTS
Our 2002 financial performance reflected the challenging
environment in which we  and other growth-oriented
investment managers  operated.Were not happy that
our average assets under management for the year
fell 23% to $164 billion, but the fact is that most of
that decline was due to market depreciation.
Meanwhile, revenues were down 26% last year, driven
by the drop in assets and investors shift to low-fee
products such as money market and bond funds.
Given this difficult climate, were encouraged that
our 2002 operating margin remained above 30%,
excluding some one-time charges associated with the
reorganization.1
CREATING VALUE
Weve also been working hard to create shareholder
value in other parts of our business. We continue to
explore several strategic alternatives for our 33%
interest in DST Systems, which provides back-office
support for mutual fund companies. Given the
importance of our DST investment and the significant
tax ramifications of the various options under consideration,
were being deliberate in our due diligence.
On a separate front, we recently converted Janus
Capital Management LLC shares into Janus Capital
Group common shares. This conversion is important
because it directly aligns employees interests with
those of our shareholders and provides a more
favorable tax structure for Janus Capital Group.
LOOKING AHEAD
Obviously, our biggest challenge in todays environment
is delivering the best-of-breed fund performance
weve all come to expect. I have great confidence in
our investment team leaders and the steps they are
taking to make good on this goal.
In the months ahead, well keep moving forward to
address our other strategic priorities: increasing
operating efficiency, diversifying our range of products
and leveraging our distribution network. Were just
getting started, but I hope you can see that some of
the reorganization benefits we envisioned  more
corporate stability, increased transparency and lower
recurring costs  are already becoming a reality.
We believe our steady progress on all these issues is
positioning Janus Capital Group well for a market
recovery and a bright future.
Mark B. Whiston
President and Chief Executive Officer
Janus Capital Group Inc.