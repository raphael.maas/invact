As we complete Fiscal 2013, we�re reminded of the fact that we are finishing the second year of our three-year
commitment to transform Copart into a more nimble and scrappy company that delivers on the promise to be Fast, Easy,
Transparent, and Comprehensive in every process. We kicked off this transformation in Fiscal 2012 with Project
Overdrive and started out the year with our move from California to Texas.
Texas is definitely culturally different from California, and in some ways the Dallas influence is no doubt affecting
Copart. On so many fronts, Project Overdrive is about our culture. You can never underestimate the value of culture in a
company, and our culture is legendary in so many ways. From our robust history, which spans more than 30 years, to the
growth of locations in the 1990s, we have embraced technology and change that is part and parcel of our founder and
Chairman Willis Johnson�s personality. Our culture is one that guides all of us daily in all we do here at Copart. The
transformation of our company with Project Overdrive is about bending our culture in a positive direction but definitely
not changing it into something unrecognizable from the past.
Our culture allowed us to perform in November of 2012 when we had the role of helping our customers deal with the
aftermath of Hurricane Sandy, which impacted more than 14 locations in the Northeast. Just to quantify what a big deal
Sandy was, the hurricane, which started on October 22 and ended on October 31, 2012, was the second costliest hurricane
in United States history. Estimates as of June 2013 assess damage to have been over $68 billion, a total surpassed only by
Hurricane Katrina.
Even our experience with Hurricane Katrina did not prepare us for the damage done by Hurricane Sandy. The inflow of
flood-damaged cars was far more immediate and compressed. We mobilized the full force of our Catastrophe Action
Team, marshaling more than 325 employees and 575 car transporters. In 90 days, we picked up more than 85,000
vehicles in a market that normally wouldn�t do that many vehicles in a year. Employees from as far away as England and
car transporters from as far away as Seattle were sent in; and at one point, Copart had more than 14 additional yards
created to deal with so many vehicles. At the peak of the effort, we picked up more than 2,300 cars in one day, or almost
one car every 25 seconds. We consumed almost 11 acres a day in storage capacity. The cost to Copart was no small issue
either, as we expended over $36 million to handle the additional volume. I want to publically thank all those at Copart
who worked tirelessly, many working seven days a week, to help in the cleanup of a catastrophe unlike any other. A
special shout out goes to those who worked Thanksgiving Day across the company in an effort to not let one day go by
without making progress on behalf of our insurance customers and the affected communities. In the end, our teams
worked more than 60 days straight.
In May of this year, we closed a deal with Quad Cities Salvage Auctions (QCSA) and brought to Copart new locations across
the U.S. and the resources of an experienced management team, with many individuals having well over a decade of
experience in the industry. In addition to QCSA, we brought Desert View Auto Auctions (DVAA) into the Copart family.
DVAA specializes in the selling and processing of donation vehicles from various national and regional charities across the U.S.
These two additions to Copart will make our management team stronger, our products more robust, and our relationships
greater; and they will give us an even richer network of facilities across the country. We are excited about the opportunities that
lie ahead and will be fully integrating best practices into Copart, QCSA, and DVAA in fiscal 2014.
We are committed to our mission, vision, and values and will continue to find ways to simplify and streamline our
process and to continually innovate and execute on our vision for the company and the much needed next generation of
products and services that are in our future. Speaking of next generation, VB2 turned 10 years old this year, and it was no
coincidence that we replaced this technology with our third-generation auction platform, VB3. Under VB2
, we sold more
than 10 million vehicles, and under VB3, we believe that VB3, which does not have a membership requirement, will
permit us to expand our buyer base and ultimately the number of cars we sell. Our third-generation platform is browser
embedded, allowing an unlimited number of auctions on your screen. It also has the ability to count down auctions
dynamically, adjusting for buyers� different bidding styles and the different types of product we sell: from 20-year-old
Ford Sedans to one-of-a-kind Ferrari F-50s, like the one we were selling in Connecticut as I penned this letter. By the
way, the final bid was $446,000. Finally, with the new look and feel of VB3, we will be able to really allow buyers,
members, and general looky-loos to come to the auction and give feedback during the auction on how they are enjoying
their experience. Because with VB3 you don�t have to be a member to watch, anyone can observe and learn about the
auction process. This was not the case with VB2
, as membership was a necessity. Just as an example of the impact of
VB3, auction attendance in September 2013 was up 48% over the previous September.
This year also marked the release of a brand new website with more than 100 new features that are improvements over
our old site or completely new to Copart and our industry. The ability to search for vehicles has never been easier at
Copart.com. With our new website, we can recommend cars you may want based on your browsing and bidding activity.
Today our marketing department is focused on driving eyes to Copart.com, making it easier to find vehicles once you are
on our site, and streamlining the process so it is more efficient and less time consuming. Occasionally we send out a few
emails to our members as well. Okay, specifically we send more than 100 million emails a year, so the ability to make
sure the right vehicles end up in the right customers� hands has never been better or easier at Copart. This new and
improved Copart.com is our foundation. It is based on modern technology and has a contemporary look and feel. You can
JOBNAME: Copart Inc. PAGE: 5 SESS: 10 OUTPUT: Mon Nov 11 20:12:10 2013 SUM: 3DECFCBA
/Alljobz/dps-nj/10k/30887-Copart/d00-wrapper
look forward to many improvements to our site in the years ahead. We also continue to see growth in new member
volume. Registration volume is already up 15% in the first two months of FY 2014.
For a third round of accomplishments in 2013, let�s talk mobile. I�m happy to say that we launched our app on the
iPhone and iPad and today see more than 10% of our bidding coming in on mobile. This is just another way to make it
easy for our customers to search, view, and bid on vehicles at Copart. Mobile also helps in the five-step process that we
have for attracting new members. We talk about this at Copart very frequently. It is important to take customers through
the steps that start and end with Awareness, Membership, Bidding, Second High Bidding, and finally High Bidder. In just
nine months since its launch, members have submitted more than $1.2 billion worth of bids through our mobile app!
As a global company, Copart is unique in that when we open a site anywhere in the world, it benefits all of our other locations,
regardless of geography. For example, when we opened in England, it benefited the U.S., as it increased our members and
expanded our buyer base. We call this phenomenon cross pollination, and it�s one of the exciting examples of how we expect
global expansion to benefit all of our customers at Copart. Before, we would never have thought that buyers in Poland, who
were active in England, would end up buying in the U.S.; but we now know this to be the case. International expansion has
also allowed our business to benefit from scale, and we have been strategic in permitting that increased scale to fund
investments in systems and products to benefit all our customers worldwide. In that spirit, we expanded into Brazil, Spain,
Dubai, and Germany in 2013, extending our footprint to seven countries. Already Dubai is our second largest international
bidder country in the U.S. market, surpassed only by Mexico. This cross pollination will benefit not only the U.S. but also the
U.K., especially as we expand in Germany, Spain, and the rest of the European Union.
So our year ends with growth in four more countries, a new website at Copart.com, a new mobile platform on iPhone and
iPad, new third-generation auction technology with VB3, and the addition of QCSA and DVAA�and all this was done
while dealing with Hurricane Sandy. We are obviously very proud of all that has been accomplished in a one-year time
frame, but we are still very excited to see what 2014 will bring and what is on the horizon.
On behalf of the Board of Directors and our leadership team, Vinnie, Willis, and I want to thank you for your continued
support.
Sincerely,
A. Jayson Adair
Chief Executive Officer