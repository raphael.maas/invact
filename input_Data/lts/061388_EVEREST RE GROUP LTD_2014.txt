LETTER FROM THE
Chairman
The Everest franchise, represented by its innovative
culture and strong discipline, will continue to
provide differentiated performance making Everest
a clear winner in this evolving market.

Everest enjoyed yet another successful year in 2014 with
$1.2 billion of net income and a 17% return on equity, outpacing most in our industry. In fact, despite a challenging
business environment, Dom and our team delivered
record operating earnings of $24.71 per diluted common
share, providing for a 14% increase in book value per
share and propelling shareholders� equity to new levels at
$7.5 billion.
These achievements reflect the essence of Everest and its
strategies, which are focused on delivering the highest
value to its shareholders through sound underwriting fundamentals and efficient capital management. This means
that we will balance profitable growth with our capital
strategies. In 2014, we returned close to $650 million to
our shareholders, in the form of dividends and share
repurchases, while at the same time, growing the
business.
As Dom will discuss in his letter, alternative third party
capital has significantly added to the industry�s capacity.
As efficient managers of capital, we took advantage of this
phenomenon to lower our own cost of capital thereby
improving the net margins on our growing book, despite
noted rate declines. This is a testament to the strength,
grit and determination of the Everest team, which has a
long track record of successfully identifying and executing
on opportunities.
While Everest is beginning its 20th year as a listed company on the NYSE, its long history actually dates back
more than forty years, having established leading positions around the globe. Indeed, during 2014, we hosted a
celebration in Singapore for our Asia Pacific clients, brokers, and employees to recognize their contribution to
Everest�s success in this region over the last thirty-five
years. Our Board of Directors, in addition to senior executives of the Company, joined in this milestone event.
Also during the year, we welcomed Gerri Losquadro to our
Board. Gerri brings a wealth of industry experience and a
unique perspective that complements the other members 

of our Board. Already she is making a positive impact and
is a welcome addition. We have a high quality Board with
a diverse level of experiences across various disciplines.
Collectively, their counsel is invaluable to Everest�s management team as they navigate through the challenges of
today and tomorrow.
Looking ahead, we are pleased with our position and
believe that the Everest franchise, represented by its innovative culture and strong discipline, will continue to provide differentiated performance making Everest a clear
winner in this evolving market.
Thank you for your continued support,
Joseph V. Taranto
Chairman of the Board