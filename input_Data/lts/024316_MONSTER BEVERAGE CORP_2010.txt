TO OUR STOCKHOLDERS
2010 marked our 18th consecutive year of record revenues. We achieved record gross sales in
2010 of $1.489 billion, which was primarily attributable to increased sales of Monster Energy
drinks both internationally and in the United States. Notably, we achieved continued success in
expanding the distribution of our Monster Energy brand into new international markets.
Monster Energy is now available in 60 countries and territories outside of the United States.
During 2010 we introduced a number of new beverages including:
 Monster Energy Absolutely Zero energy drinks in 16-ounce aluminum cans.
 Monster Energy Import Light and Dub Edition energy drinks, both of which are
packaged in 18.6-ounce resealable end aluminum cans.
 Monster Energy Extra Strength Nitrous Technology Black Ice, which is a zero
calorie drink in 12-ounce sleek aluminum cap-cans.
 Worx Energy, energy shots in 2-ounce PET plastic bottles.
 X-Presso Monster coffee energy drinks in 9.6-ounce aluminum cans.
 Monster Energy M3 Super Concentrate in 5-ounce glass bottles.
 Admiral Iced Teas in 23-ounce aluminum cans.
 Huberts Lemonades in 16-ounce glass bottles.
 Hansens Natural Fruit and Tea Stix, a line of all-natural low-calorie powdered drink
mixes.
 Blue Sky Shots, all natural functional shots, in 2.5-ounce PET plastic bottles.
In 2010, gross sales outside of the United States increased to $240.6 million from $168.0 million
in the prior year. During 2010, we launched Monster Energy drinks into many new countries
including Hungary, Czech Republic, Slovakia, Austria, Switzerland, Bulgaria, Germany,
Norway, Iceland, Malta, United Arab Emirates, Jordan, Lebanon, Reunion and Tahiti. We plan
to expand the distribution of Monster Energy drinks to additional countries in Central and
Eastern Europe as well as to additional countries in South America and Asia with a view to
achieving our goal of making Monster Energy a global brand.
We are currently in the process of launching our new non-carbonated Monster Energy Rehab
energy drink line, which contains electrolytes and additional supplements in the United States
and intend to expand its introduction initially to Canada and then to additional countries.
We intend to continue to innovate and introduce new Monster Energy drinks as well as other
beverages in 2011, with continued emphasis on lower calorie drinks to meet the increasing
demand from consumers for such products.
The growth of the energy drink category in the United States continues to exceed the growth of
the beverage category in general. Energy drinks are now, in dollar share, according to Beverage
Digest, the second largest segment of the liquid refreshment beverage category in convenience
and gas stores, after carbonated soft drinks. Overall, the value of the energy drink segment is
more than twice as large as the bottled water segment, nearly three times larger than the sports
drink segment and the juice/juice drink segment and more than five times larger than the ready-
to-drink tea segment. This growth trend continues to reflect positively for the future of our
Company. Although the energy category continues to show growth, the macro environment
remains challenging, particularly in light of the recent events in Japan and continued unrest in
North Africa and the Middle East. We are nonetheless optimistic about our ability to continue to
grow the Monster Energy brand and achieve increased sales in 2011.
Sales of Peace Tea continue to meet our expectations and we look forward to achieving
continued improvement in sales and market share for our Peace Tea brand.
Once again, I would like to express my gratitude for the support afforded to the Company by Mr.
Hilton Schlosberg, our President and Chief Operating Officer, and Mr. Mark Hall, President of
our Monster Beverage Division. I would also like to express my personal thanks to our
consumers, stockholders, customers, distributors, and suppliers for their continued support. To
all of our management and employees, my sincere thanks and appreciation for all their efforts,
which are evidenced by the continued success of our Company. To our stockholders, thank you
for the trust you have placed in our management team.
Sincerely,
Rodney C. Sacks
Chairman and Chief Executive Officer 