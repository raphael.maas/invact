our fellow shareholders:
Our transformation journey is gathering speed, due in large part to the successful acquisition of Lorillard, Inc. in 2015. This transaction was a pivotal point in Reynolds Americans long-term growth strategy, and were seeing
the benefits already.
Were pleased to report that R.J. Reynolds Tobacco Company has made a strong start on the integration of the Newport brand. In addition, RAI and its operating companies delivered robust growth in 2015. The end result
was an industry-leading total shareholder return of almost 50 percent for the year, which included an increase
in our quarterly cash dividend of 7.5 percent. Details on RAIs financial results are contained in our year-end
earnings release, which can be found at www.reynoldsamerican.com.
Weve entered 2016 with considerable momentum, and continue to deliver against our commitment to enhancing shareholder value. RAIs $5 billion sale of the Natural American Spirit business outside the U.S., completed in
January, is allowing us to further pay down our debt, and we increased our dividend by a further 16.7 percent already this year.
Among the highlights of 2015, RAIs operating companies greatly strengthened their financial and marketplace position. The addition of Newport to R.J. Reynolds cigarette portfolio drove much of this improvement, but our companies also benefited from favorable economic factors, such as lower gas prices, which increased adult
tobacco consumers disposable income.
Brand leadership could be seen across our companies. R.J. Reynolds Newport, Camel and Pall Mall drive brands achieved a combined cigarette retail market share of more than 29 percent in 2015, while Santa Fe Natural
Tobacco Companys distinctive super-premium cigarette brand, Natural American Spirit, ended the year at a
milestone two share points. American Snuff Companys Grizzly moist-snuff brand continued to grow at twice
the rate of the moist-snuff industry, achieving retail market share of more than 33 percent in 2015. These drive
brands offer strong equity, and were confident that R.J. Reynolds superior trade and consumer marketing
employees will capitalize on additional opportunities for growth in the coming year.
We also advanced our transforming tobacco vision and continued to invest in smoke-free growth platforms for
the future.
In the vapor category, R.J. Reynolds Vapor Companys VUSE Digital Vapor Cigarette completed its national
expansion in 2015 and is now available in more than 110,000 retail outlets. VUSE remains the best-selling
e-cigarette in convenience-gas outlets, and the brand is rolling out a lineup of exciting new formats as well.
VUSE Connect with Bluetooth technology and VUSE Fob were recently made available online, and later this year, the brand will offer VUSE Port  a revolutionary, liquid-based, closed vapor tank system  and VUSE Pro 
an innovative closed-cartridge system.

In addition, Niconovum USA remained focused on the national distribution of ZONNIC nicotine gum last year. ZONNIC doubled its retail availability, and is now sold in about 33,000 convenience-gas outlets. Today, ZONNIC represents one in five nicotine gum products sold across all retail channels.
Both Reynolds Vapor and Niconovum now fall under the newly created RAI Innovations Company, which is tasked with driving speed-to-market with new products across a range of platforms. In addition, the vapor
collaboration agreement that was reached last year between R.J. Reynolds and a subsidiary of British American Tobacco will facilitate RAIs companies ability to more effectively and efficiently meet the preferences of adult tobacco consumers in a rapidly evolving marketplace.
Sustainability remains an integral part of our efforts to keep our businesses thriving over the long term.
For the seventh year, RAI was selected to the Dow Jones Sustainability North America Index, and we posted
the most comprehensive summary of our companies sustainability initiatives to Reynolds Americans website
in 2015.
Our companies also continue to meet their compliance obligations in the evolving regulatory environment,
while maintaining their ability to compete effectively in the marketplace. We continue to strongly believe
that tobacco products should be regulated consistently and fairly, based on sound science, and in a manner designed to achieve significant and measurable harm reduction.
All in all, 2015 was a game-changing year for RAI and its companies. Our priorities for the year ahead are to
successfully complete the Newport integration, while delivering strong financial and marketplace performance, and further enhance shareholder value. We believe we have the right products, programs and people in
place to accelerate our transformation journey.
On behalf of RAI, our companies and our employees, we thank you for your support.
Thomas C. Wajnert Susan M. Cameron
Chairman of the Board President and CEO

