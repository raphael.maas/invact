TO OUR FELLOW SHAREHOLDERS:
The year 2006 was pivotal for The TJX Companies. As
both of us came back into the leadership of the business
in the fall of 2005, we established profitable sales
growth as our top priority. We are pleased with how
our organization responded and the very strong
results we delivered in a relatively short period of time.
With a renewed focus on the fundamentals, and better
off-price buying being key, we improved execution
across the board. We regained our entrepreneurial
energy and were better risk-takers. We also reinforced
the meaning of being accountable and resultsdriven.
We believe that all of these efforts led to our
success in achieving profitable sales growth in 2006.
For the year, net sales grew 9% to $17.4 billion
and consolidated comparable store sales increased
4% over the prior year. Income from continuing
operations reached $777 million and diluted earnings
per share from continuing operations were
$1.63, a very strong 26% increase over last years
results, on an adjusted basis, excluding items noted
below.* Overall, we grew square footage by 4%, netting
85 stores to end the year with a total of 2,466 stores.
We undertook many initiatives across the
Company in 2006 that worked well in the short term,
and we believe put us in a stronger position for
continued success in the long term. We reinvigorated
The Marmaxx Group, renewing our emphasis on offprice
buying and better brands. We delivered powerful
performance at our Winners and HomeSense,
T.K. Maxx and HomeGoods divisions, and our smaller
businesses made good progress. Across the Company,
we made cost reduction a major focus area, which
contributed to bottom-line improvement. In addition,
it allowed us to increase our marketing spend, which
we believe succeeded in driving customer traffic.
A Reinvigorated Marmaxx
The Marmaxx Group, our largest division, delivered
excellent results in 2006, increasing segment profit
by 10% to $1.1 billion, with a comparable store sales
increase of 2%. We made better off-price buying a
key priority in 2006, shifting approximately 10 percent
of our buying dollars into more true, off-price, closeout
buys. In so doing, we improved the flow of great
brands at compelling values to our stores, every day,
upping the WOW factor and increasing the excitement
of the treasure hunt, which encourages
customers to shop our stores more frequently. We
also pursued many merchandising initiatives, including
The Runway at T.J. Maxx designer departments
that we tested in over 40 T.J. Maxx stores. In addition
to bringing newness and excitement to the stores,
these initiatives are one of the many ways in which
we open new vendor doors, giving us the ability to
continue to flow in fresh brands. More effective
marketing was another main goal in 2006, which we
believe we achieved through our increased media
presence and harder-hitting messaging.
Its important to note that, although Marmaxx
is the oldest and largest of our divisions, we do not
view it as mature in the traditional business sense.
We are constantly testing new ways to grow and drive
profitable sales. We have many initiatives underway
for 2007, including our plan to add more than 200
expanded footwear departments at Marshalls. Further,
we plan to be even stronger in our marketing presence
and message. Additionally, we continue to fill in existing
markets with new stores, with a plan to grow our
store base by a net of 50 stores to end 2007 with a total
of 1,619 T.J. Maxx and Marshalls stores. Importantly,
we pursue these opportunities with an organization
that is re-energized, more entrepreneurial in spirit,
and results-driven.
Powe r f u l Pe r formance Internationally
Our Canadian businesses delivered outstanding
performance in 2006, above our expectations. With
strong sales growth, Winners achieved its highest
segment profit margin since 2000 and HomeSense
delivered its best bottom-line performance since we
launched this business in 2001. We increased the
depth of our brand offerings and flowed them consistently
to our stores, which creates freshness and
excitement for our customers every time they shop.
We expanded our offerings of jewelry, accessories
and shoes, which, as at Marmaxx, performed very
well. We see more opportunities in these and other
merchandise categories in the year ahead, including
bringing a Winners variation of The Runway at
T.J. Maxx to Canada. We are also pleased that
HomeSense, our Canadian home fashions concept,
has really captured the attention of Canadian shoppers
and made strong profit contributions in
2006 as it continued to grow. In 2007, we expect to
end the year with 188 Winners stores and 71
HomeSense stores.
T.K. Maxx, the off-price leader in the U.K. and
Ireland, had a tremendous year. This division also
exceeded our expectations in 2006, posting very strong
sales increases and a 58% increase in segment profit
in U.S. dollars. This organization fired on all cylinders
in 2006. They maintained liquid inventory positions
throughout the year, which enabled them to buy into
current trends and flow great brands to our stores.
T.K. Maxx also softened the look of its stores and
improved its presentation of merchandise. In order to
ready ourselves for European expansion, we strengthened
our senior management team, promoting
Paul Sweetenham to Senior Executive Vice President
and Group President, Europe, and naming
Stephanie Morgan as the new Managing Director for
the U.K. and Ireland. We expect to open five stores in
Germany in 2007, our first in that country. We are
familiar with the merchandise in this market, as well
as customers tastes and shopping habits, having had
a buying office in Germany for the last five years. We
believe that with a population of 82 million, Germany
holds strong potential for the growth of our business
in Europe. In 2007, we plan to add 10 T.K. Maxx
stores in the U.K. and Ireland, for a total of 225 stores
in Europe by the end of the year.
Pivotal Year at HomeGoods
We are pleased with HomeGoods greatly improved,
above-plan performance in 2006. Comparable store
sales increased 4% for the year and segment profit
more than doubled over the prior year. The
HomeGoods organization significantly improved
its flow of merchandise, getting the right product, at
the right time, to our stores, which served this
business well in every season. As with Marmaxx and
our international divisions, HomeGoods also undertook
merchandise initiatives that we believe helped
drive customer traffic. For example, we tested 70
HG Kids departments, which create shopping destinations
for childrens furniture and accessories, with
very positive early results. We also were very pleased
to name Nan Stutz, who has nearly two decades of
TJX experience and merchandising expertise, as
President of HomeGoods in early 2007. In the
upcoming year, we see opportunities in soft home
categories and other merchandise initiatives, and
believe that our new marketing campaign will
continue to build greater customer awareness of the
HomeGoods brand. In 2007, we plan to net 12 additional
stores at HomeGoods, bringing the chain to a
total of 282 stores by year-end.
Repositioning A.J. Wright
We repositioned our A.J. Wright business in 2006,
which we believe puts this business on a much
better platform to achieve successful growth. We
closed 34 underperforming stores, which we believe
was necessary for the future success of A.J. Wright.
Narrowing our geographic scope allows us to focus on
the demographic profile that we believe works best for
this concept and also gain advertising leverage. We
currently operate A.J. Wright stores in 19 states, and
believe that the strong results of our new stores in 2006
underscores our better understanding of this business
and its moderate-income target customer demographic.
We have strengthened the organization, naming a
new President in Celia Clancy, who brings 25 years of
merchandising experience and a strong understanding
of this customer base, and adding other senior
management with decades of TJX experience in the
operating, merchandising, and marketing arenas. We
begin a new year focused on capitalizing upon what
we have already learned about this moderate-income
customer, and with its very sizable target demographic,
continue to view A.J. Wright as a great potential
growth vehicle for TJX. In 2007, we expect A.J. Wright
to be cash-flow positive again, and we plan to continue
growing this chain at a measured pace, with five new
store openings planned in existing markets.
Progress at Bobs Stores
We took significant steps to improve performance at
Bobs Stores in 2006. We ended the year just short of
our goal of reducing operating losses by half, and
believe we would have met our target were it not for
the unusually warm weather in the fourth quarter
throughout the Northeastern U.S., where Bobs Stores
is concentrated. During the year, we expanded our
womens casual sportswear departments to encourage
our female customers to shop Bobs Stores for herself,
and saw positive results. We also succeeded in increasing
promotional levels, which drove customer traffic,
while increasing merchandise margins. In 2007, our
focus remains on improving performance at our existing
stores, with no new store openings planned until
we see this business produce comparable store sales
increases that meet our expectations. We expect Bobs
Stores to be cash-flow positive in 2007.
Computer Systems Breach
In January 2007, we announced an unauthorized
intrusion of our computer systems that process and
store information related to customer transactions,
detailed in our Form 10-K. We deeply regret any
difficulties our customers may experience as a result
of this breach. As we have always done, we have
made our customers our top priority in dealing with
this issue and we have dedicated significant human
and financial resources to protect personal data, as
well as communicate with customers about this
problem. We have also communicated in a timely
manner to the credit and debit card companies, as
well as governmental and law enforcement agencies.
We are working with leading computer security
firms on this issue and we want our shareholders and
customers to know that we believe that it is safe for
customers to shop our stores.
In the fourth quarter of fiscal 2007, we
recorded a charge of approximately $.01 per share for
costs related to the breach incurred in that quarter,
including costs for investigating and containing the
intrusion, enhancing computer security, communicating
with our customers, as well as technical, legal
and other fees. We expect to continue to incur additional
expenses such as these over the coming
months, but are not in a position to reasonably
estimate the amounts at this time. Also, we
do not yet have enough information to reasonably
estimate losses we may incur arising from the
intrusion(s). We will continue to report on
this matter periodically.
Financial Strength
Our extremely solid financial foundation gives us
confidence in the short- and long-term success of
TJX. Our strong operations generate significant
amounts of excess cash, which gives us the ability
to simultaneously grow our Company and return
value to shareholders. We began 2006 with a substantial
cash balance and generated an additional
$1.2 billion from operations. After reinvesting in
our business, we repurchased $557 million of TJX
stock, retiring 22 million shares, and increased
the per-share dividend by 17%. Underscoring our
continued confidence in our business and our
ability to deliver profitable sales growth, our Board
approved a new $1 billion program earlier in 2007,
which is in addition to $436 million remaining in
our existing program at year-end. Once again, we
started a new year in an excellent financial position
and we plan to repurchase $900 million of
TJX stock in 2007.

Leveraging Differences
Integrity and treating people with respect and
fairness are core values upon which this Company
was founded. We continue to work diligently towards
a more inclusive work environment, leveraging
differences among people, and opening ourselves
to new ideas, varied experiences and fresh perspectives.
During 2006, we created the TJX Advisory
Board on Differences and Diversity, comprised of
external experts, to share best practices and make recommendations
from an outside perspective. We also
formed several Task Forces comprised of our
Associates to link our diversity efforts with our business
goal of driving profitable sales. In addition, our
Associate Affinity Groups continue to provide
networking, support and fresh ideas. Our Associate
Training, Supplier Diversity, and Community
Relations programs also continue to support our
diversity efforts inside and outside the Company.
We move forward with a commitment to continuing
to do a better job throughout our organization of
leveraging the differences among us.
Bright Prospects fo r 2007 and Beyond
We believe that we are a stronger Company today
than we were a year and a half ago, but our expectation
is to be even better in 2007. We are proud of
the accomplishments of 2006, and view them as a
platform upon which to build for the future. We
undertook many initiatives in 2006, some from which
we learned a great deal and some that will serve as
springboards for growth in the years ahead. With a
stronger entrepreneurial spirit of intelligent risktaking,
we offered our customers more great brands,
and intend to continue to do more of this in 2007. We
were more effective in our marketing across the
Company, and we are excited about the opportunities
to make our advertising even stronger, including
better branding of our businesses. Expense management
remains a key focus, which helped lever our top
line in the past year and support our marketing
investment, and we have dedicated a corporate
group to this effort.
Our go-forward management team
represents deep experience at TJX as well as new
talent, and we have strengthened the leadership
of our divisions as well as in our corporate marketing
and human resources areas. In the same
way that we have no walls dividing departments
in our stores, allowing us to be flexible in presenting
merchandise, we also have no walls,
metaphorically speaking, between our divisions.
We believe the flow of ideas and sharing of best
practices across our divisions is happening more
now than ever before and benefiting all of our
businesses. We see these cross-divisional lines
of communication as being a key element of our
success as we proceed in the future.
We would like to gratefully acknowledge
the dedicated service of Gary Crittenden and
Dennis Hightower, each of whom stepped down as
members of the Board of Directors since our last
letter. Gary Crittenden had served as Director since
2000, and Dennis Hightower since 1996. We thank
them for their significant contributions to our
Company and wish both Gary and Dennis the very
best for future success and good health.
We have great momentum heading into
2007 and many opportunities ahead. We remain
focused on our chief goal of driving profitable sales
growth and executing the fundamentals of our
off-price concept that made this Company great. We
sincerely thank our customers, our Associates, who
now number approximately 125,000, our vendors
and other business associates, and our fellow
shareholders, for their ongoing support.

Respectfully,

Carol Meyrowitz
President and
Chief Executive Officer

Bernard Cammarata
Chairman of the Board
