Report from the Chairman,
President and CEO
2011 was another year of growth for FMC Technologies. Total
revenue for the year was a record $5.1 billion and represented a
24 percent increase over 2010. Earnings per share increased for the
tenth consecutive year, up 7 percent to $1.64. By leveraging our
technologies and services to create customer success, each business
unit maintained or improved upon its market position. As a result,
we generated solid financial and operational results while providing
a foundation to support our continued growth. Inbound orders also
reached a new record of $6.0 billion, and backlog at year-end grew
to $4.9 billion. Subsea revenue reached a new annual high of $3.3
billion in 2011.

Highlights
We strengthened our global subsea market leadership position,
receiving more than half of the total subsea tree awards during
the year. Our subsea processing systems for Totals Pazflor field in
Angola, and Petrobras Marlim field offshore Brazil, were delivered
on time and under budget. These were among the industrys
highest-profile offshore projects in 2011, each introducing new
subsea separation technologies in their respective region. The
fluid control and surface wellhead businesses also experienced
another year of solid growth, primarily due to strong demand in
the North American shale market.
Our businesses are constantly looking to improve upon existing
processes in order to meet customer requirements and deliver
the most reliable and dependable equipment in the industry.
Project execution and equipment quality are two specific areas
where we continue to focus to create a distinct competitive
advantage. Looking forward, the oil and gas industry is expected
to grow at a significant rate and we are taking steps to ensure
that our operations are prepared to support this growth. We
must also continue to attract and retain the necessary level
of talented employees we need to create customer success and
achieve our growth objectives.

Ahead of the curve

The oil and gas industry continued to grow in 2011. In anticipation
of this trend, we strengthened our position in emerging geographic
markets as well as developed new technologies to support subsea
processing and surface shale activities. We also added capacity
to support future growth through global investments in people,
facilities and business processes.
The global offshore and land-based drilling fleets grew as operators
announced increased exploration and production spending to
support new and existing projects. Oil prices increased and ended
the year close to $100 per barrel. This made the economics of
offshore production more attractive to operators, while also
encouraging additional land-based activity.
The increase in offshore activity in the worlds major deepwater
basins resulted in FMC receiving a significant number of project
awards during the year, such as Petrobras Congro/Corvina project.
Located offshore Brazil, the project will employ our subsea
processing solutions to perform gas/liquid separation and boosting.
Our technologies for Congro/Corvina will reduce production costs
and allow Petrobras to decommission a topside platform at the field.
This project continues our record of providing the systems for all six
of the industrys commercial subsea separation projects.

In Australia, FMC was selected to provide the subsea systems for
Chevrons Wheatstone, Shells Prelude and Woodsides Greater
Western Flank projects. These awards reflect the success of our
strategic investments in that growing region. In Canada, FMC
was chosen by ExxonMobil to manufacture subsea systems for
its Hibernia Southern Extension project, the countrys largest
offshore development. And in the Gulf of Mexico, where we were
awarded several projects during the year, our Enhanced Vertical
Deepwater Tree (EVDT) set a world deepwater completion
record of 9,627 feet (2,934 meters).
The strength of our customer alliances resulted in many of
our subsea awards with the worlds leading national and
independent oil companies. In 2011, we expanded our existing
Gulf of Mexico alliance with Anadarko Petroleum Corporation
into a global alliance, and we secured a global agreement with BP
for subsea production systems that broadened our existing Gulf
of Mexico and West Africa Block 18 contracts. We also signed
a new agreement with BG Norge. These relationships, and the
many others already in place, recognize the depth and breadth
of our subsea experience. Our alliances position FMC to receive
additional project awards and often present opportunities to
apply our new technologies.
The strength of activity in shale and other unconventional
formations continued to fundamentally change the energy
industry in North America. This led to significant demand within
our fluid control and surface wellhead businesses for well service
pumps, trees and manifolds to support hydraulic fracturing
operations. Demand was also strong for our Weco/Chiksan
flowline products.

Despite natural gas prices dropping to their lowest levels in
nearly two years, demand for our equipment and services to
support horizontal drilling and hydraulic fracturing operations
remained strong. This was due to operators shifting resources
to the production of oil and natural gas liquids. Internationally,
horizontal drilling and hydraulic fracturing activities are
emerging in Argentina, China, Poland and Saudi Arabia, and
FMC is well positioned to support these regions with our
proven equipment and services.

The oil and gas industry is projected to continue its growth
trend in 2012 and beyond. In the United States, a revival in
oil production is occurring after nearly 40 years of declining
production. Global offshore opportunities and investments are
also expected to increase, supported by higher crude oil prices
and new offshore discoveries.
FMC has identified three areas of focus to capitalize on this
future industry growth: subsea infrastructure, subsea services
and shale-related equipment and services. All three areas
are markets in which most of our businesses currently hold
leadership positions.

We can extend our subsea infrastructure leadership by building
upon our subsea processing success. This includes continued
development of equipment and technologies that increase oil
recovery, perform in demanding high-pressure/high-temperature
environments, extend the life of the field and make new reservoir
discoveries economically feasible. Within subsea services, there
is increasing demand for solutions that lower the cost of well
interventions in order to maximize production from the nearly
4,000 subsea wells that are in operation. The growth of shale
developments remains a major opportunity as demand for fracturing
trees and manifolds, well service pumps, and flowline equipment
will remain strong.
Strategic investments prepared our businesses for the increased
exploration and production spending operators have planned for
2012. We opened our Technology Center and expanded capacity
in Brazil. In Asia, we began construction of a new surface wellhead
facility in Singapore and increased manufacturing capabilities at our
subsea operations in Malaysia. In the United States, we prepared
for growth in the shale markets by increasing our capacity to
manufacture well service pumps and other flowline equipment.

On January 3, 2012, we announced our intention to purchase the
remaining 55 percent of Schilling Robotics. FMC has held a noncontrollng
interest in Schilling since 2008, and the relationship has
been beneficial in expanding our subsea offerings and achieving
our growth objectives. Schillings subsea robotics technology will
be used on Petrobras Congro/Corvina project, and their remotely
operated vehicles (ROVs), manipulator arms and control systems
will add considerable value to our portfolio of subsea solutions.

Based on the continued growth of the subsea market, and the
common alignment and overlap that exists between our surface
wellhead and fluid control businesses, we changed our business
reporting segments. Our operations now report through our Subsea
Technologies, Surface Technologies and Energy Infrastructure
segments.
Former Chairman, Peter Kinnear, retired from FMC Technologies
in 2011 following a distinguished 41-year career. Peters legacy in
building FMC to become the worlds leading subsea solutions provider
has left a lasting impact on FMC and our industry. In addition, we
made several executive appointments during the year. Maryann
Seaman was named Senior Vice President and Chief Financial Officer,
succeeding Bill Schumann who will retire in 2012. We also named
Johan Pfeiffer as Vice President of Surface Technologies, and Barry
Glickman as Vice President of Energy Infrastructure.
A clearly defined path exists for our businesses to extend their
market positions in 2012. We will be successful in achieving our
goals by maintaining our leadership position in the growing subsea
market; expanding our subsea scope with processing technologies
and aftermarket services; and capitalizing on the growth in the shale
market. In the process, we will continue to deliver the growth and
returns that our shareholders expect.
Chairman, President and CEO