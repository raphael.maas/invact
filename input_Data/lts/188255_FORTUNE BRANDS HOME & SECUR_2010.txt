                               Dear fellow shareholders:




Fortune Brands achieved our objectives in 2010 . . . and more. Your company delivered strong growth in sales
and earnings. Our businesses outperformed their respective markets and emerged from the economic downturn
in strong competitive positions. And we charted a course designed to maximize long-term shareholder value.

-- net sales increased 7%, reflecting broad-based growth across all three
   businesses.
-- diluted earnings per share increased 98% to $3.16, reflecting strong
   operating performance and a net gain principally due to tax-related items.
-- excluding charges and gains, diluted earnings per share were $2.81, up 16%.
-- we generated free cash flow of $690 million.
-- to tal shareholder return, including dividends, was more than 41%.

The strength of our three businesses  and of our 2010 results  is a result of our strategic approach during
both the global economic downturn and the front end of the recovery. During the downturn, our focus on the
3 Cs  consumers, costs and cash  enhanced the position of our brands in the marketplace, improved our
cost structures and the productivity of our supply chains, and bolstered our financial strength and flexibility.
And our determination to stay on offense at the front end of the recovery  as we outlined here a year ago 
drove our proactive strategic initiatives and targeted investments in 2010 that helped us build profitable
market share.

Strong Momentum in 2010 Across All Three Businesses
spirits. Our Beam Global spirits business built strong momentum as sales increased to a record level, up 8%
including a 3% benefit from excise taxes and favorable foreign exchange. We boosted brand investment by
double digits to promote sustainable long-term growth. As expected, these investments impacted 2010 operating
income before charges, which declined 3%. Even so, our operating margins remained among the best in the
industry. Our investments paid off in a record year of brand-building innovations that included the introductions
of Maker's 46, Cruzan 9 and Courvoisier 21 and the first full year of the red hot Red Stag by Jim Beam.



We further supported our growth with a substantial increase in digital media, successful new brand-building
campaigns, and our strengthened global sales and distribution organizations. As a result, we grew in our three
major geographies  North America, Europe/Middle East/Africa, and Asia-Pacific/South America  and world-
wide case volumes expanded for eight of our 10 biggest brands: Jim Beam, Sauza, Canadian Club, Courvoisier,
Teacher's, Maker's Mark, Larios and Cruzan. Notably, Maker's Mark exceeded one million cases sold for the
first time in its history.

home & security. Our Home & Security business significantly outperformed the home products market
with sales that grew 8%. Sales expanded across all of our product lines. Significant new business wins with
retailers, robust growth with builders and sustained strength with dealers helped our cabinetry business gain
share and become the North American market leader. Moen gained share in the U.S. and grew at a double-
digit rate in international markets. Our advanced energy-efficient window products helped Simonton grow at a
double-digit rate, and new products and styles fueled healthy growth for Therma-Tru. Master Lock continued
to innovate and the "Tough Under Fire" brand led strong growth for our security and storage products.
The work we did during the downturn to substantially reduce costs and create lean and flexible supply chains
delivered major benefits to the bottom line. With the benefit of higher volumes on a lower cost base, operating
income before charges in Home & Security grew 69%.

golf. Our reported golf sales increased 2% and were up 7% excluding the Cobra brand that we sold to
Puma in April. Both Titleist and FootJoy extended their leadership with robust sales increases fueled by
innovative new products such as the new Titleist 910 driver, next-generation NXT and DT SoLo golf balls
and AP1 and AP2 irons, FJ ICON and Sport shoes, and the new FJ outerwear layering system. Our investments
in expanding our position in attractive Asian markets are helping deliver strong gains now and positioning
our brands well for long-term growth. These investments included the opening of a new golf ball plant
in Thailand, market-specific products for Japan, and the further development of our sales and marketing
infrastructure in Korea, China, Japan and Australia. Operating income before charges was up 33%.




Three Strong Businesses Built to Thrive Independently
The strength and success of our three businesses were critical factors in the decision we announced in
December: In order to maximize long-term value for shareholders, we intend to separate our three attractive
and well-positioned businesses. Our intent is to become a pure-play spirits company, to spin off the Home &
Security business to shareholders in a tax-free transaction, and to either sell or spin off our golf business.
While we are still finalizing detailed separation plans, we expect to complete this proposed plan in 2011,
subject to final Board and regulatory approvals.

This proposal is consistent with the approach the company has taken over a long period of time to drive
shareholder value. Throughout the years, Fortune Brands has actively managed the portfolio to outperform
and create value for the long term. These initiatives have come in three primary areas: first and foremost,
developing our businesses organically; second, high-return add-on acquisitions and divestitures; and third,
returning cash to shareholders through high-return share buybacks and dividends. As a result, we've
organically grown brands that have captured or strengthened the leading market position, such as Moen,
Jim Beam, Titleist, FootJoy, Master Lock and our cabinetry brands; and we've significantly enhanced our
spirits and home & security businesses over the years with strategic acquisitions, while also creating value
through the sale of the wine business, the sale of non-strategic brands across categories, and the innovative
repositioning and subsequent spin-off/merge of the ACCO office products unit. These steps have significantly
enhanced each of our businesses as well as our overall portfolio, while also creating substantial value for
shareholders. Since the company began trading as Fortune Brands in 1997, our total shareholder return has
significantly exceeded that of the S&P 500.

Maximizing Long-Term Value for Shareholders
In line with this approach, our Board and management conducted an ongoing strategic review over the past
four years that included regular evaluation of separating our businesses at the right time to serve the best
interests of our shareholders. As we neared the end of 2010, it was very clear to us that our businesses
had navigated the downturn better than we had expected and were strong enough to compete and grow





aggressively on their own as the economy recovers. Accordingly, we made the decision to take the next logical
step in the evolution of Fortune Brands, which we believe will maximize long-term value for our shareholders.
While the breadth and balance of our portfolio have served shareholders very well, we see the potential for
even greater value by separating our businesses into focused companies when they are in such strong competi-
tive positions and have such compelling growth prospects.

We have tremendous confidence in the future of each of our businesses. We expect these businesses will
have the management, infrastructure, capital structures, and growth and returns prospects to flourish apart
from one another and build on their solid foundations of premier brands and leading market positions.
Assuming completion of the proposed separation:

-- We will be one of the world's leading spirits companies with momentum in key markets and an organization
   designed to win. As a pure-play spirits company, we expect to trade at a multiple appropriate to the high-
   return spirits industry and attract new shareholders.
-- Home & Security will be an industry leader with the opportunity to leverage strong brands, innovative
   new products, lean and flexible supply chains, and great customer service to continue outperforming the
   competition and drive substantial upside for shareholders as the housing market recovers.
-- Our Acushnet golf business will be positioned to extend its leadership with the best brands and finest
   people in the industry. It's a valuable company with strong growth prospects that can create significant
   value for shareholders through a sale or as an independent company.
Each of these businesses competes in attractive consumer categories with excellent long-term fundamentals,
and we see substantial headroom for profitable growth in all three. We are confident the course we've set will
enable our shareholders to participate in the exciting future that each business will build. We look forward
to executing this business separation in the months ahead and continuing our track record of value creation.
I hope you'll read more about each of these strong businesses in the following pages.

On behalf of the 25,000 people of Fortune Brands around the world, thank you for the confidence you've
placed in us. We will continue to aim even higher to continue earning your trust.

                                                   sincerely,




                                               bruce carbonari
                                       Chairman and Chief Executive Officer

                                                February 22, 2011
