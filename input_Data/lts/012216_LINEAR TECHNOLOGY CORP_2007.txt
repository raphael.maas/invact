TO OUR STOCKHOLDERS

Analog excellence. Excelling at the highest levels
of design innovation, customer problem solving,
factory execution, quality and reliability, product
timeliness and corporate financial performance.
Excellence is the standard of performance we
expect at Linear Technology. We expect to be the
best: to have the most advanced products, the
highest level of customer satisfaction and the highest
financial results.

Every analog company claims to be high-performance.
Few are excellent. We strive to be that company.
We bring this commitment to the electronics market.
Most electronic products have both analog and
digital circuitry, but there are far fewer analog engineers
than digital engineers. Frankly there are not enough
analog engineers.
We take on the difficult projects. We invent whats
needed next. We help customers break through with
something unique and better. In the pendulum from
feature rich applications to low cost implementations,
we lean toward feature rich. Our primary customers
are at the cutting edge of technology in networking,
cell phones and basestations, computing, automobiles,
navigation systems, medical electronics and many
industrial areas.
We can afford to be selective. Our annual revenue
is $ 1.1 billion. The analog market is $ 37 billion, of
which we serve roughly 25 %. Within the broad market
our sales are very diversified. In fiscal 2007, our bookings
were distributed as follows: 33 % in industrial; 33 %
in communications (8 % handsets, 10 % basestation
infrastructure, 15 % networking); 13 % in computer;
8 % in automotive; 9 % in high-end consumer and 4 %
in space level and military products. Historically these
percentages have evolved in response to feature
demands within the various markets (see chart). Feature
rich opportunities arise both when these markets
are embryonic and again as they mature as technology
breakthroughs occur.

For example, cell phones initially were technology rich
in such areas as battery efficiency and LED backlighting,
then became more commodity focused as cell phone
demand proliferated in emerging world markets. Presently,
with wireless communications, computing, and
entertainment converging on cell phones, new entrants
in the market are fostering a demand for more feature
rich analog solutions. Automotive is another example of
a major end market becoming feature rich intensive.
Overall revenue of $ 1,083,000 for fiscal 2007 was
down slightly from fiscal 2006s $ 1,093,000. Although
this was a disappointment to us, our direct analog
competitors had similar experiences. Some technology
rich emerging applications such as Power over Ethernet
solutions in the networking area, high speed mixed
signal analog to digital converters used in multi-protocol
basestations, and low power application specific
circuits for use in GPS and other consumer applications,
did well for us. Other areas, such as cell phone and
computing, saw more of the drive toward lowering cost
rather than broadening features, which led to some of
the contraction in our growth.
However, fiscal 2007 was a promising year for seeding
our growth for the near future. In the telecom and datacom
areas, the current emphasis on green technology
for improved power efficiency and lower utility bills, as
well as the emergence of digital power system management
to monitor, control and optimize power at the
system and facility level, were fertile areas for our new
mixed signal power management circuits. Modernization
in lighting provided opportunities for our new families
of 20 microamp to 3 amp LED drivers, with applications
in LCD backlighting, signage and automobiles, all
demanding wide dimming ranges and low power consumption.
Broadband access, with the emerging 3G,
3G LTE, WiFi and WiMax networks, has created opportunities
in the basestation and infrastructure area for
our high-speed analog to digital converters, our high
linearity mixers and amplifiers and our modulators/
demodulators and filters.
As cars incorporate more electronics, there are many
power management opportunities for a supplier such as
Linear Technology, with its high technology, quality and
reliability. Advanced automotive electronics demands
unique power integrated circuits for radar, adaptive
cruise control, parking assistance, side and rear cameras,
electronic steering and braking, engine control
electronics and hybrid vehicle battery management,
particularly in the transition from nickel metal hydride
to lithium ion batteries.
In fiscal 2007 Linear had initial sales in its new family
of Module power supplies. Linear has a heritage
and a reputation for analog technology innovations for
products such as multi-phase switchers, Hot Swap
controllers, Burst Mode converters, Power over Ethernet
(POE) circuits, cell phone photoflash chargers, etc.
This new family of Module regulators represents a similar
technology breakthrough as this product is a standalone
power supply in the form of an integrated circuit.
This family, which should add significantly to our sales

in the next few years, has a broad range of
applications in many diverse end markets including
industrial, datacom, telecom, medical, testing,
computing and automotive.
As we enter fiscal 2008 we believe we are positioned
for growth. In addition to the product and market opportunities
referred to above, we have completed factory
infrastructure projects which give us the essential factory
capacity in wafer fabrication, assembly and test to
support $ 2 billion plus in annual revenues. In this age,
when many companies are outsourcing their manufacturing
requirements, we are a contrarian and firmly
believe that in analog it is important to be internally
vertically integrated. We often supply our customers
unique, technology rich products, without an alternate
pin-compatible source. Consequently excellence in
manufacturing, as defined by outstanding quality,
reliability, short lead times and on-time delivery standards,
is essential for our success. In the last two
years we received supplier excellence recognition from
a variety of companies including Northrop Grumman
and Rockwell Collins in the US, Harman/Becker and
Siemens in Europe and Advantest in Japan.

In fiscal 2007 the Company maintained its tradition of
impressive profitability and cash flow generated from
operations. Net income was $ 411.7 million, down from
$ 428.7 million in fiscal 2006. Diluted earnings per share
were $ 1.39 versus $ 1.37 in fiscal 2006 as there were
fewer shares outstanding. Exclusive of the impact of
SFAS 123R Share-Based Payment net income would
have been $ 463.4 million versus $ 466.9 million
in fiscal year 2006 and diluted earnings per share
would have been $ 1.57 versus $ 1.50 last year. Our
margin percentages continue to be excellent, significantly
better than our nearest analog competitors.
Gross margin as a percent of revenues of 77.7 % versus
78.2 % in the prior year was largely unchanged. Operating
margin was 48.4 %, down from 51.6 % largely
due to increased headcount and compensation related
costs in the technical areas, which were absorbed
over lower revenue amounts. Once again the Company
generated positive cash flow from operations of $ 478.0
million versus $ 510.0 million in fiscal 2006. Cash flow
from operations was positive for the 85th consecutive
quarter. During the year the Company increased the
quarterly dividend payment by 20 % from 15 per share
to 18 per share. We initially began paying a dividend in
1992 and have increased it every year since.

The Company has excellent opportunities to grow its
revenues and to maintain its stellar margins and cash
flow generation capabilities. The management and
employees are energized and committed to capitalize
on these opportunities. Consequently, during the flat
sales growth period in fiscal 2007, to benefit our loyal
stockholder base and to emphasize our positive growth
and margin outlook, the Company took a bold step to
improve its capital structure by utilizing $ 1.3 billion of
its cash and $ 1.7 billion in convertible debt to buy back
roughly 25 % of its outstanding common stock.
This transaction should be accretive to earnings per
share and consequently positively impact the Companys
stock price going forward. Already this has enabled
Linear to increase returns on equity and invested capital.
In summary, analog excellence is both an accomplishment
and more importantly a high standard. It is
rewarding to be recognized, it is also an obligation to live
up to expectations. By delivering better products, better
quality, better profits and better earnings per share, we
continue to strive for excellence. To our customers and
our stockholders, our goal is to be an excellent supplier
and an excellent investment. Measure us accordingly.
Sincerely,

ROBERT H. SWANSON, JR.
Executive Chairman

LOTHAR MAIER
Chief Executive Officer

PAUL COGHLAN
Vice President, Finance
and Chief Financial Officer