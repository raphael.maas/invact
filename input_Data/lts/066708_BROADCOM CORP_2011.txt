To Our Shareholders

2011 marked a milestone in Broadcoms
history as we celebrated the companys
20th anniversary. The occasion served as
a touchstone to celebrate the engineering
innovation that has transformed the
communications industry and reflects on
how far weve come as a company, and
as an industry.

Incredibly, we believe 99.98% of all Internet traffic
today passes through at least one Broadcom chip.
Although our impact on the industry is already
immense, our eyes are locked upon what the next
20 years has in store and how we can continue to
serve as a catalyst for industry innovation.
In 2011, Broadcom's innovative and highly
integrated products, coupled with solid execution,
enabled us to outpace competitors and outgrow
the industry. We did so while meeting our profitability
objectives and delivering an increased dividend to
shareholders. We also nearly doubled the amount
of capital returned to shareholders via share
repurchases and dividends  both indicators of
our financial strength and optimism.

With all three business units delivering strong
performance, annual product revenue rose almost
9% in 2011 year-over-year. That is in contrast to
our overall industry, which grew only in the low
single digits. Today, we sell roughly 500 million
devices per quarter. With the help of diligent cost
controls, we achieved non-GAAP product operating
margins of nearly 21%1, in the center of our target
range. Both cash flow from operations as well
as total cash and marketable securities balances
set new annual records, at $1.8 billion and $5.2
billion, respectively.
Broadcoms R&D scale and breadth of complete
system solutions have enabled us to consistently
gain market share, deliver solid profitability,
reinvest in our businesses and return more capital
to shareholders.

Last year, Broadcom spent nearly $2 billion on
R&D and ranked an enviable 17th among American
corporations in total U.S. patent awards. But for us,
being a technology innovator means more than simply
adding new shelves to our patent library (which
incidentally, with the recent acquisition of NetLogic
Microsystems, Inc., is now approaching 16,800
patents and applications). Much more important is
our ability to turn that intellectual property into
commercially successful products. Our engineers
work closely with our manufacturing partners to
design and deliver complete solutions to meet the
unique demands of the communications markets.
Our chips are found in some of the world's best
known devices  including products by Apple,
Cisco, Dell, Huawei and Samsung to name a few.
They count on us to keep them ahead of the
technology curve with complete families of highly
integrated products that deliver compelling price,
performance and power efficiency.
At Broadcom, we like to say we "engineer the
impossible" for our customers. And we do it
every day. The following are 2011 and early 2012
highlights from our three business groups.

Home:
Broadband Communications Group
In 2011, Broadcom introduced significant new
product families that contribute to the next
generation of home networks and provide the
basis for continued industry leadership. Our
breakthrough Full Band Capture technology, in
which a single Broadcom chip can digitize the
contents of an entire TV spectrum for either
cable or satellite, was greeted enthusiastically by
customers, investors and the press following its
introduction. By eliminating the need for separate
tuners for every channel, Full Band Capture
significantly decreases the cost of a set-top box. It
also allows video content providers to more closely
integrate their offerings with those delivered via the
Web. There's an additional advantage for consumers
in that Full Band Capture enables channel-switching
to occur virtually instantaneously, rather than with the
multi-second delay that can frustrate TV viewers.
Fiber optics may be the networking technology
of tomorrow, but most of today's home video is
delivered via already-installed copper coaxial cable.
Here again Broadcom continues to innovate,
bringing to market new SoC products that double

the capacity of existing home coax networks. Our
Multimedia over Coax Alliance (MoCA) 2.0 family
of integrated devices lets video operators bring
higher quality video to more rooms in the home
while drawing much less power.
Were also changing TV in other ways. Our set-top
box chipsets provide advanced graphics and
high-performance processing, enabling the interface
to your TV to be as engaging as the most sophisticated
website or video game. In modems, carriers
are designing the devices to do much more than
provide simple broadband connections. New
modems will act more like "gateway" devices, with
built-in connectivity, wireless networking, video
and even telephony. Those features, among others,
will be fully-supported in Broadcom's silicon.
The global market for broadband is extraordinarily
diverse. Different parts of the world are on a myriad
of technology paths; within each county, there are
often six or eight major competitors vying for early
advantage. Only a company of Broadcom's size
and scale can expect to be fully engaged with that
global market. Our suite of DOCSIS products,
which allow cable TV operators to transmit both
data and video, is making substantial headway in
China, complementing its already strong position
in U.S. and European markets.
Hand:
Mobile and Wireless Group
We continue to fortify our position in mobile and
wireless technologies, including cellular SoCs, Wi-Fi,
Bluetooth, mobile application processors and
graphics, Global Positioning Systems (GPS/GNSS)
and Near Field Communication (NFC) products.
Our products can be found in just about
every consumer electronics category including
smartphones, tablets, PCs, TVs, set-top boxes,
games, routers and network gateways.
With accelerating popularity in all parts of the
world, smartphones continue to be a key strategic
opportunity for Broadcom. Our wireless connectivity
combo chips are a common element in a majority
of smartphones. In classic Broadcom tradition,
we are taking advantage of this scale and market
knowledge, along with our vast IP portfolio, to
deliver increasingly integrated products that give
our customers virtually everything needed to bring
advanced smartphones to market.

Our cellular SoC products integrate  on a single
chip  one or more application processors, graphics,
video, cellular baseband, and other capabilities
that previously required half a dozen or more
separate integrated circuits. When combined with
other key devices like our wireless connectivity
chips, we can provide customers with a complete
smartphone platform. We're proud that Samsung
chose our complete smartphone platform for the
Galaxy Y series, which is attracting attention for stylish
design and functionality. Broadcoms complete
smartphone platforms will change the economics
of the smartphone business by shortening customer
development cycles and reducing costs.
Another technology shift now underway involves
the arrival of fifth generation Wi-Fi products that
Broadcom calls 5G WiFi, based on the IEEE
specification 802.11ac. Providing speeds up to
three times faster than current Wi-Fi devices, this
new standard will be important across Broadcom's
entire product line, but perhaps nowhere more so
than in smartphones. Faced with exploding demand,
mobile carriers are shifting as much of their cellular
traffic as possible to Wi-Fi frequencies, a trend
that 802.11ac will accelerate. History has taught
us how disruptive a new generation of Wi-Fi can
be, and Broadcom is leading the industry in its
5G WiFi rollout.

Infrastructure:
Infrastructure & Networking Group
Among our enterprise data center customers,
trends such as cloud computing and "Big Data" are
forcing the migration to a newer, faster generation
of server and storage products. We also work closely
with mobile carriers, who are anxious to upgrade
their networks to keep pace with rising demand.
Our products targeting those markets did well in
2011, and we plan to build upon their success.
Broadcom's ability to offer complete solutions,
for mobile and elsewhere, is greatly enhanced with
our acquisition of NetLogic Microsystems, Inc., a
leader in high performance intelligent semiconductor
solutions. The transaction was valued at $3.7 billion,
making it by far the largest in Broadcom's history.
The NetLogic multi-core embedded processor
solution, market leading knowledge-based processors,
and unique digital front-end technology
for wireless base stations are key enablers for the
next generation infrastructure build-out. With the
acquisition, Broadcom is now better positioned
to meet growing customer demand for integrated,
end-to-end communications and processing
platforms for network infrastructure.
The corporate data center is in the early stages
of a multi-year upgrade cycle involving the
replacement of Gigabit Ethernet connections

with links that are 10 times faster, with analysts
estimating that six million servers will be upgraded
in the coming years. Broadcom spotted this shift
early on, and the numerous 10 Gigabit product
announcements from server companies during
2011 invariably involved our chips. In addition, our
10GBASE-T PHY technology was adopted by most
major vendors in 2011. The device saw a five-fold
increase in sales last year, and we will be working
hard to make it the kind of silicon connectivity
success for which Broadcom is known.
Service providers continue to represent a significant
opportunity for our Infrastructure & Networking
Group. As one example, wireless carriers are
upgrading their networks to 4G to deliver next
generation mobile services. As data traffic continues
to grow and speeds continue to increase, service
providers are upgrading their backhaul links from
legacy Time-Division Multiplexing (TDM) technologies
to Ethernet. With its leading breadth of IP and
optimized product offerings, Broadcom is uniquely
positioned to continue to lead the migration to
Ethernet and enable the next generation of both
fiber and microwave backhaul.
An entirely new market for Broadcoms infrastructure
solutions is automobiles. Considering the maintenance,
entertainment, navigation and driver
safety features in a modern car, they are beginning
to require communications networks nearly as

sophisticated as those in homes. While it may be
a few years before our efforts bear fruit, automobiles
are a perfect opportunity for a company with our
technical depth and organizational scale.
In Conclusion
Our 2011 results were solid and all of us at Broadcom
are proud of our hard work involved in making
them happen. But we're proud of something else
as well: That our company has spent its first
two decades at the center of a world-changing
communications revolution.
Broadcom products are now found everywhere
on the planet, from urban corporate data centers,
where information moves between computers at
speeds that boggle the mind, to villages in some
of the most isolated parts of the world, where
mobile phone users are being exposed to the great
potential of technology for the very first time.
Remarkably, all of those users are alike in
possessing an almost insatiable appetite for data.
Helping them satisfy that demand is our mission
and our passion at Broadcom. As we start our third
decade, that commitment has never been stronger.
Scott A. McGregor
President and Chief Executive Officer
March 23, 2012