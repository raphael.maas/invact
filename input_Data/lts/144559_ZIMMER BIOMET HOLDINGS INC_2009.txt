To Our Stockholders:

Zimmer has been dedicated to addressing patients painful musculoskeletal
conditions for more than 80 years. Our intense focus and deep experience frame
a solid strategic platform from which we expand the continuum of care for
patients and support the healthcare providers who take care of their needs.
Acting on our patient-focused mission is the foundation for Zimmers leadership.

Zimmer has earned a unique position in the musculoskeletal market by applying what we have
learned from decades of experience with knees and hips to other areas of the human body. Our bias to
act on behalf of patient care was again evident in 2009 as we continued to expand our product portfolio,
extended applications for our proprietary Trabecular MetalTM Technology and introduced additional
opportunities to tailor treatments through better-fitting implants and innovative surgical instruments.
These accomplishments continued in spite of the global recession, which caused a significant number of
patients to delay implant and oral surgeries as long as possible. We anticipate that they will eventually
proceed with treatments. Indeed, volume stabilized in the second half of 2009, particularly in the
Americas and Asia-Pacific regions.
Zimmers sales in 2009 totaled $4.1 billion. Fully diluted adjusted earnings per share of $3.94, while
slightly below 2008 results, were in line with our expectations coming into the year.
Our performance reflects the commitment of Zimmers people and our solid financial foundation,
strategic global infrastructure and well-anchored strategic plan. These strengths, combined with recent
momentum in reconstructive procedures and new-product regulatory clearances, give us confidence
in achieving sales growth and leveraged earnings in 2010.
Our Strategic Growth Platform Zimmers experience in large-joint reconstruction forms the nucleus
of our musculoskeletal market leadership. That body of knowledge supports an impressive long-term
success rate in knee and hip surgeries. Confidence in outcomes, in large part, reflects surgeons ability
to select implants that will address individual patient needs. Recognizing the value of these solutions,
we continue to expand our offerings. In the fourth quarter of 2009, patient-specific instruments were
cleared for use in certain Zimmer knee surgeries. We are one of only a few orthopaedic device providers
able to offer both pre-operative software and patient-specific pin guides.
We also received clearance for two new acetabular cups used in hip replacement. Additional operating
room flexibility provided by these cup systems along with our range of hip stems further expands
patient-specific capabilities. Empowering surgeons and preserving intraoperative flexibility are two
long-time Zimmer hallmarks. We continue to make those considerations a priority in product development.
Continuing the introduction of new products demonstrates that growth of our reconstructive franchise
remains a key strategic initiative. Those core capabilities also represent the catalyst for another
fundamental strategythat of growing our smaller musculoskeletal business lines.

We not only have the advantage of understanding the musculoskeletal system but also the ability
to leverage our technologies, quality-driven manufacturing network and global distribution system.
Our presence in U.S. hospitals and markets in 100 countries worldwide, coupled with exceptional brand
recognition, means that Zimmer has attractive opportunities to expand our musculoskeletal reach and
accelerate growth in business lines beyond Knees and Hips.
Leveraging Capabilities Two prime examples of how we leverage capabilities are Trabecular Metal
Technology and our standard-setting medical education and bioskills training program.
Trabecular Metal Technology has been applied to implants for more than a decade, long enough
to accumulate a body of peer-reviewed journal articles with clinical data supporting positive results
observed by surgeons and patients. Continuing research and development initiatives have led to
identifying Trabecular Metal Technology applications throughout our business units.
The medical education and bioskills training available through the Zimmer Institute exemplifies
world-class instruction. Its curriculum teaches safe and effective uses of our products and offers
healthcare providers learning opportunities that can lead to better patient outcomes. In 2009, the
Zimmer Institute trained more than 15,000 surgeons and clinicians on our products worldwide.
Our focus in 2010 and beyond continues to include innovative applications of our capabilities and
expertise. With a strong infrastructure in place, including significant investments in manufacturing and
distribution in the past two years, our near-term emphasis is on effective execution, which includes
accelerating sales and earnings growth.
Applying Core Values in Day-to-Day Operations Zimmer employees share a deep commitment to improving
the quality of life for patients. It is especially rewarding when our day-to-day activities bring reminders of
the difference our products make in peoples lives.
My most recent experience along those lines occurred when I visited our facility in Winterthur, Switzerland.
Seated next to me on the flight from Zurich was Susy Balsiger-Peter, whose photo appears on the cover
of this report. Our conversation quickly found common ground because of her interest in orthopaedics
as a nurse and patient. She has had both knees and both hips replaced, with Zimmer products, it turns
out. Her first surgery was a hip replacement in 1988. Its an old one, but it still works, she assured
me. Of her extensive implant experience, she says simply, Im so happy. I have no pain anymore.
Hearing her and others testimonies reminds us not only why we do what we do but also why we work
to expand our musculoskeletal capabilities. We are grateful for the support of our customers, employees
and distributor networks in carrying out Zimmers patient-centric purpose and mission.

David C. Dvorak
President and
Chief Executive Officer
