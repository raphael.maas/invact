

                                                                                                down as investors waited to see the outcome of a chal-
                               To Our Shareholders
                                                                                                lenge to our U.S. Zyprexa patent. The trial was concluded
                               Clearly, for the pharmaceutical industry as a whole, the
                                                                                                in February 2004 and, at this writing, we are awaiting a
                               tough sledding of the past few years intensified in 2004.
                                                                                                ruling from the trial court.
                               Many companies are struggling with significant business
                                                                                                     A second disappointment--and another key factor
                               challenges including looming patent expirations, sputter-
                                                                                                affecting our share price--was weaker-than-expected sales
                               ing R&D output, and, in one case, a major product recall.
                                                                                                for Zyprexa in the U.S., showing an 8 percent decline.
                               In addition to these operational troubles, the industry ran
                                                                                                This sales erosion was driven by concerns about potential
                               into further problems in the external policy environment,
                                                                                                weight gain and hyperglycemia and amplified by intense
                               as concerns about product safety and allegations that
                                                                                                advertising by trial lawyers targeting Zyprexa patients.
                               some companies had concealed important clinical data
                               brought new calls for more stringent regulation.
                                                                                                Genuine progress in a difficult year
                                    Almost as troubling to me as the negative events
                                                                                                     Disappointing though they were, these setbacks
                               themselves is the way in which they have come to domi-
                                                                                                should not obscure the many positive highlights for Lilly
                               nate virtually every discussion of the industry, whether in
                                                                                                in 2004.
                               media coverage or policy debates. Neither investors nor
                                                                                                     In our financial performance, we saw total sales grow
                               legislators can make good decisions in an environment
                                                                                                10 percent over the prior year--at the high end for our in-
                               where everything is portrayed in stark black and white.
                                                                                                dustry. Our eight new products contributed to that result,
                               What is needed is a greater sense of reasonable propor-
                                                                                                accounting for 11 percent of Lilly's total sales. And we
                               tion in public dialogue--the ethic newspapers used to call
                                                                                                expect that share to roughly double in 2005. Moreover,
                               "fair balance."
                                                                                                we managed to beat investors' expectations for earnings
                                    Applying this to Lilly's record for the year, it seems to
                                                                                                for the year, delivering adjusted earnings per share of
                               me that, while we had some undeniable setbacks, we also
                                                                                                $2.82. (For a reconciliation of our adjusted EPS per share
                               had some remarkable achievements in both our business
                                                                                                to the reported EPS of $1.66, please see page 1.)
                               results and our public interactions. Taken together, these
                                                                                                     Most significantly, we continued to run counter to
                               accomplishments point to genuine progress not only for
                                                                                                the industry trend by delivering breakthrough innovation
                               our company, but, in some measure, for our industry too.
                                                                                                at a record pace--a total of eight new drugs since late
                                    We had our share of bad news, and I won't gloss over
                                                                                                2001, thereby doubling our portfolio of promoted prod-
                               it in any way. Most disappointing for our shareholders,
                                                                                                ucts. In 2004, Lilly launched five new products plus six
                               certainly, was the fall in our share value--19 percent for
                                                                                                new indications or formulations in several key markets.
                               the 12-month period. In part, this decline was a sector ef-
                                                                                                Three of these new products were first-in-class: SymbyaxTM
                               fect, driven by the events I just noted. The pharmaceutical
                                                                                                for bipolar depression, Alimta for mesothelioma, and
                               industry has traditionally traded at a 20 percent or more
                                                                                                Yentreve for stress urinary incontinence in Europe. The
                               premium to the S&P 500, but by the end of 2004 it trailed
                                                                                                other two--Cialis for erectile dysfunction in the U.S. and
                               that index by 15 to 20 percent. Lilly's stock was also held

                              
                                                                                                                       it is proceeding as planned, last year saw the extensive
                                                                                                                       overhaul and very successful restart of our key injectable
                                                                                                                       medicines facility in Indianapolis--the plant that had
                                                                                   $15,000
                                                                         been at the center of the original regulatory review. This
                                                                                                     may be our most important "non-story" of 2004.
                                                                                   $12,000
   
                                                                                                                       Rebalancing the scales of trust
   
                                                                                                                             Even as we have been working to build our opera-
   
                                                                                    $9,000
  
                                                                                                                       tional capabilities, we also have taken important steps
   or 70 percent of total net sales, compared with
                                                                                                                       to respond to the very hostile external environment that
   $8.3 billion, or 66 percent in 2003. Zyprexa sales
                                                                                                                       currently weighs so heavily on our industry.
   as a percentage of total net sales decreased from                                $6,000

   34 percent in 2003 to 32 percent in 2004.
                                                                                                                             In 2004, we took a leadership role to allay public
                                                                                                                       concerns about drug safety and to help restore trust in the
     Newly Launched Growth Products
      
                                                                                    $3,000
                                                                                                                       accuracy and accessibility of the industry's clinical trial
     Other Established Growth Products
     
                                                                                                                       data. We have defined and disseminated a very strong set
     Zyprexa
     Prozac/Sarafem/Prozac Weekly
                                                                                                                       of principles for the conduct of clinical research, including
     Other                                                                             $0
                                                                                                                       a commitment to disclose all results, whether favorable or
                                                                                            


                                                                                                                       unfavorable to our products. Most recently, we launched the
                                                                                                                       most comprehensive clinical trial registry of any pharma-
                                                                                                                       ceutical company that included online posting of clinical
                                                                                                                       trials at their initiation as well as complete results once the
                                                                                                                       study is concluded and the drug is brought to market.
Cymbalta for depression--represent outstanding new                                                                          We know that the wellspring of public criticism goes
options in two very competitive categories.                                                                            deeper than anger at industry practices. It reflects growing
     For the most part, sales for these newcomers have                                                                 anxiety about access to and affordability of vital medicines.
met or exceeded our expectations. Symbyax has had a                                                                    Here, too, Lilly has taken action. We continue to maintain
slow start, and Yentreve is too new to evaluate. But we                                                                and promote our "LillyAnswers" program, which makes
are pleased with the strong uptake for Cialis, Cymbalta                                                                many Lilly products available to low-income seniors for
and Alimta. Both Cymbalta and Alimta benefited from                                                                    just $12 a month. The total value of the program more
receiving accelerated FDA review and approvals for new                                                                 than doubled in 2004, delivering $140 million worth of
indications. Cymbalta is the first drug to be approved for                                                             medicines to 235,000 seniors. In addition, our "Lilly Cares"
neuropathic pain associated with diabetes, and Alimta has                                                              program offers our medicines free to needy patients,
been approved for second-line non-small-cell lung cancer,                                                              regardless of age, who could not otherwise afford them. In
the most common cause of cancer mortality. To win                                                                      2004, "Lilly Cares" provided assistance to nearly 160,000
multiple indications in the launch year is rare--and adds                                                              patients, a total donation worth about $166 million.
extra luster to Lilly's bright reputation in R&D.                                                                            I firmly believe these kinds of responsible policies
     Indeed, Lilly's outstanding R&D productivity con-                                                                 and practices can, over time, achieve a better balance
tinues to set us apart from the crowd. We have several                                                                 in the public view, especially as they are echoed in the
exciting compounds now in late-stage development,                                                                      actions of our peers. But it would be a mistake to suppose
including two potential breakthroughs in diabetes, one in                                                              that all of the external problems we are facing now can be
cancer, and a drug we believe could become best-in-class                                                               addressed by improving the industry's reputation.
in the treatment of acute coronary syndrome and stroke.                                                                      What we're witnessing is a collision of several mas-
(For details, see question 5 below.) At the other end of the                                                           sive forces--a biomedical revolution, an aging population,
pipeline, our scientists posted a record-breaking year in                                                              and ever-rising health care expenditures. Eventually, every
2004. The output from discovery research has increased                                                                 part and every participant in the health care system will
by more than 50 percent and the number of candidates                                                                   feel this pressure and every component will be remade by
entering human clinical studies has increased by more                                                                  it. I believe the pharmaceutical companies that succeed in
than 40 percent, compared to the average for the four                                                                  this changing world will be those that find a way to deliver
prior years.                                                                                                           greater therapeutic innovation at a lower overall cost.
     One final operational highlight deserves commen-                                                                        We intend to be one of those companies, and to that
dation precisely because it did not make headlines in                                                                  end, we began implementing last year the first phase
2004. Lilly's manufacturing component has made great                                                                   of a long-term campaign to reduce our cost structure
progress in fulfilling our promise to not only address the                                                             and improve our productivity from top to bottom. Our
serious quality problems that confronted us a few years                                                                wide-ranging cost control efforts are not the typical sort of
ago, but to reengineer this vital component into a world-                                                              belt-tightening where spending requests are temporarily
class capability. The job is not finished; but as evidence                                                             delayed or deferred until better times. Rather, many of our

                                                                                                                                                                                    3



                               restructuring initiatives are intended to be permanent and       But we don't feel Cymbalta's long-term potential will be
                               sustainable. Specifically, in every part of our global busi-     constrained in this environment.
                               ness, we streamlined operations, cut infrastructure, and               We've used Cymbalta's distinctive profile to develop
                               reallocated as well as reduced our total headcount. These        strong positioning in the marketplace. This product ad-
                               measures are expected to generate net savings of about           dresses depression's emotional symptoms--and its painful
                               $150 million in 2005, with larger gains going forward.           physical symptoms. Studies have shown that between 40
                                     This year, we will launch a corporate-wide effort to       and 60 percent of people who are depressed also experi-
                               identify and pursue further productivity gains, using the        ence pain. Physicians have been quick to recognize pa-
                               well-established toolkit of the Six Sigma process. We will       tients in their practices who are troubled by these issues.
                               apply these tools across our operations, looking for every             The opportunities for this drug extend well beyond
                               opportunity to cut waste, reduce variability, shorten cycle      depression. In September, the FDA approved Cymbalta
                               times and boost efficiency. The dollars gained can be har-       as the first available treatment for diabetic peripheral
                               vested to deliver more--to fund a clinical trial that supports   neuropathic pain--or DPNP. I've been moved--and
                               a new indication, or a new market research effort to bring       thrilled--to hear some early success stories from patients
                               our solutions to more patients who may be helped by them.        who have long suffered from this very debilitating
                                    In this effort, I believe we will be addressing both        condition, and who are now getting help from Cymbalta.
                               current and future business challenges. Greater productiv-       It's currently under review for this indication in Europe
                               ity is a key to delivering the financial results our share-      as well. In addition, we have seen encouraging results
                               holders expect and deserve. But it will also be essential to     in Phase II studies testing Cymbalta for the treatment
                               enable us to continue to discover, develop and deliver, as       of fibromyalgia--a condition of very painful physical
                               economically as possible, the new medicines patients are         symptoms, often accompanied by chronic fatigue and
                               waiting for all over the world.                                  emotional distress.
                                                                                                      In short, we believe Cymbalta is one of the most
                                                                                                important and exciting new drugs in our portfolio.
                                   Questions and Answers
                                                                                                Q: Given the poor performance of pharmaceutical
                               Q: What is Lilly doing to stem the decline in Zyprexa?           stocks in recent years and intensifying pressures on
                               A: To begin with, we're focusing our efforts where               the industry, how does Lilly expect to reward share-
                               Zyprexa shines--generally speaking, as an answer for             holders in the future?
                               patients with some of the most significant symptoms of           A: It's true, as I noted above, that the industry is go-
                               schizophrenia and bipolar disorder. Zyprexa grew to be a         ing through a very tough period. Investors have been
                               blockbuster drug on the strength of its rapid action and         understandably cautious in the face of such trends as
                               outstanding efficacy for these patients, and that is still       continuing pressure on pricing, patent life, and new drug
                               perceived to be its strong suit.                                 applications. These factors have helped to drive the phar-
                                    On the negative side, it's now seen as having weight-       maceutical sector to a 10-year low against the S&P 500.
                               gain and hyperglycemia risks that have been heightened           Sales and EPS growth for the industry have fallen several
                               by trial attorneys' advertising efforts. We are undertaking      percentage points in recent years, and annual revenue
                               a number of efforts to restore the balanced view of the          growth is now projected to be in the range of 9 percent
                               benefits and management of risks that patients and their         for the next five years.
                               doctors need to make appropriate use of this medicine.                On the other hand, that level of growth would be
                               First of all, we continue to provide patients with basic         considered excellent in many industries. It can support
                               lifestyle information and specific wellness programs that        strong earnings growth if companies can find the busi-
                               help them deal with weight gain. Second, we're helping           ness discipline needed to bring more through to the bot-
                               doctors understand and manage diabetes risks in this             tom line. Looking at the long term, there is still enormous
                               patient population. Our aim is to demystify these issues,        opportunity for our industry. Demand for effective
                               and reinforce that any patient treated with an atypical          answers to unmet medical needs is high and will likely
                               antipsychotic should be carefully monitored--regardless          grow as populations age and technologies continue to
                               of the particular medicine they're on. Finally, we will          improve. The great challenge for our industry is to meet
                               continue to vigorously defend our brand against mislead-         that demand in a more cost-effective way.
                               ing statements from any source.                                       As for Lilly in particular, I believe we are one of the
                                                                                                few that are already doing what we need to do to meet
                                                                                                that demand. In a very short space of time, we have
                               Q: Antidepressants have been getting a lot of nega-
                                                                                                restocked our portfolio with outstanding new products.
                               tive attention. In light of that, what is the outlook for
                                                                                                We have built the capabilities to support those products
                               Cymbalta?
                               A: Indeed, our Cymbalta launch has coincided with                in the marketplace. We expect no patent expirations until
                               challenging times for the U.S. antidepressant market.            the next decade. We have a proven R&D organization

                
working on a pipeline that is considered among the best                 Finally, a bit further back in the pipeline, we are
in the industry. These assets make us very competitive in         working on another PKC inhibitor, enzastaurin, for use
the current business environment. Moreover, as I noted            against cancer. It is an oral agent that has shown very
earlier, we are taking numerous steps to shrink our cost          little toxicity in studies to date. Last year, we saw early
structure and dramatically improve productivity in order          evidence that this compound may have the ability to fight
to ensure that we will keep a competitive edge in the             glioblastoma, an aggressive type of brain tumor that is
environment that we see evolving.                                 a major cause of cancer-related death among younger
     I have to believe all of these factors position Lilly as a   people--those 18 to 54. If successful, this would be an
growth company.                                                   extraordinary breakthrough.

Q: Why has Lilly's gross margin eroded and when will              Q: How likely is it that the U.S. Congress will legalize
it rebound?                                                       importation of drugs? What impact would that have on
A: The decrease in gross margin in 2004 was due to                Lilly's business?
several factors--but primarily to our ongoing invest-             A: At this writing, the outlook is still uncertain and we
ments to upgrade our manufacturing capabilities and               are getting mixed signals from Washington. On one hand,
the impact of foreign exchange rates. Our gross margin            we hear that importation still has many advocates on
also reflects changes in our product mix over time. We            both sides of the aisle, and that several key legislators are
believe we'll have a more favorable product mix as our            determined to bring a new bill to the floor.
new products grow, but this will be offset by increases in             On the other hand, in a report issued in late 2004, the
the cost of labor, growth in depreciation, and the cost of        U.S. Department of Health and Human Services reaf-
new capacity investments. Therefore, we expect our gross          firmed its long-standing opinion that allowing imports
margin to continue to erode modestly through 2005. As             from other countries would also open a channel for
our new products gain additional traction, we expect our          potentially dangerous counterfeit drugs. The HHS task
gross margin to improve somewhat from 2006 forward.               force found that total savings to consumers from legalized
                                                                  importation would be a small percentage relative to total
                                                                  drug spending in the U.S. (about 1 to 2 percent). Further,
Q: Give us an appraisal of Lilly's near-term pipeline.
                                                                  they found legalized importation would likely adversely
What new drug prospects should shareholders be
                                                                  affect incentives for R&D, thereby slowing the flow of
looking for?
A: Let me cover just four interesting examples of what            new drugs. Since annual R&D spending would drop,
we're developing. Currently, the FDA is reviewing our             importation could result in between four to 18 fewer new
application for exenatide, the first of a new class of drugs      drugs being introduced per decade--a substantial cost to
that we are co-developing with Amylin for the treatment           society.
of type 2 diabetes. In our registration trials, a significant          This conclusion was further buttressed by a report
percentage of patients not only improved their blood              from the U.S. Department of Commerce, which estimated
glucose levels, they also lost weight. We believe that            that price controls in OECD countries cost U.S. drug
exenatide, when approved, will create an option for               companies sales in the range of $18 to $27 billion per
patients who are not well controlled with one or more             year. That loss, in turn, translates into reduced R&D,
oral agents--and before insulin therapy.                          and ultimately means a loss to patients of potential new
     Another very exciting new drug in late-stage trials is       medicines, which the Commerce report put in the range
ArxxantTM (ruboxistaurin), our PKC-beta inhibitor, which          of three or four new drugs per year.
is a potential first-in-class treatment for several serious            At Lilly, we concur with both reports. We do believe
complications of diabetes. We expect the first approved           imports would put patients at risk and we are quite
indication to be for the symptoms of diabetic peripheral          certain that importing price controls--which is the real
neuropathy--a type of nerve damage that is a leading              point of such legislation--would seriously erode our
cause of foot ulceration and amputations. We're also              incentives to innovate, to the detriment of patients the
investigating it for treatment of diabetes-related damage         world over.
to the eyes and kidneys.
     In cardiovascular care, we're also working with              For the Board of Directors,
Sankyo to develop a possible new treatment for acute
coronary syndrome and stroke called prasugrel. This com-
pound works like a widely used anti-clotting agent called
clopidogrel, but early animal data suggest prasugrel may          Sidney Taurel
have some advantages. We've initiated a Phase III trial           Chairman of the Board, President, and Chief Executive Officer
to evaluate prasugrel's capacity to prevent heart attack,
stroke, and death in patients undergoing a procedure to
open a blocked artery.