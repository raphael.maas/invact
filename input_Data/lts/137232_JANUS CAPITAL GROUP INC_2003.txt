To our stockholders

At Janus, our mission is To get investors where they want
to go. To all of us, its much more than a phrase on a
plaque that sits on our desks. Its our pledge. Our promise.
Its what drives every decision and always will. Through a
very challenging time for Janus and the industry, weve
pushed hard to keep our singular focus on that commitment.
During the last few months of 2003, the mutual fund industry
came under fire as allegations of trading improprieties were
brought against a long list of firms. Unfortunately, Janus
was among the first companies named. Although we moved
swiftly to make amends to our affected shareholders, the
regulatory issues have impacted our business. As we go to
print with this report, were working hard to reach settlement
with the various regulators.
In spite of the regulatory challenges, it was a productive year,
a year we used to move Janus forward. We made significant
progress on the three strategic priorities that we outlined
at the beginning of the year. They were (1) to improve fund
performance, (2) to create a more competitive business
model by streamlining the organization and (3) to
broaden our product offerings and leverage our global
distribution network.

Strategic Priority #1:
We Improved Janus
Investment Performance

Very simply, fund performance drives our organization. During 2003,
performance improved on many of Janus flagship growth funds. In fact,
more than 71% of Janus retail domestic growth funds ended the year in
Lippers first quartile based on total return.*
Coming off of three years of a down market and poor performance,
we were pleased to see our funds, particularly our growth funds, start
delivering the type of performance that our fund shareholders deserve.
We remain committed to Janus research approach, which has served
our shareholders and clients well for more than 34 years. That said, we
are always improving our research process  trying to find ways to make
our research the best it can be.
Last year was no different. We took a number of proactive steps that
contributed to our improved performance numbers. We broadened our
equity coverage from approximately 700 to 950 stocks, and we applied
new risk-management tools to help with portfolio construction. We also
recruited several experienced research analysts to join our investment team
and promoted six senior analysts to the new position of assistant portfolio
manager, adding an additional dedicated resource to many of our funds.
Our investment team is working hard to deliver strong, consistent results to
our fund shareholders. To that end, were confident that the addition of our
new President and Chief Investment Officer, Gary Black, will make a strong
team even stronger.

Strategic Priority #2:
We Streamlined Our Corporate
Structure and Strengthened
Our Balance Sheet
By eliminating the management structures and facilities that existed at
Stilwell Financial and Berger Financial Group, we started to realize cost
savings of approximately $10 million per quarter, beginning in the third
quarter of 2003. In March, we further aligned the interest of employees and
stockholders by converting employee-held Janus Capital Management LLC
interests into public company shares. Following the LLC conversion, we sold
Nelson Money Managers plc, a UK-based money manager.
Finally, in December, we exchanged 32.3 million common shares of DST
Systems, Inc. (DST) for a DST commercial printing and graphic design
subsidiary containing $999 million in cash. The transaction is intended
to qualify as a tax-free exchange under section 355 of the IRS Code.
The DST transaction increased our book value by $808 million and,
more importantly, provides Janus with the means to significantly reduce debt
levels and bring capital ratios in line with our peers.

Strategic Priority #3:
We Expanded Our Product
Offerings and Leveraged Our
Global Distribution Network
When it comes to meeting our fund shareholders needs, choice matters.
In one year, we broadened our product lineup by introducing 16 value and
risk-managed products from our subsidiaries and partners. These new
funds and portfolios were well-received by our shareholders and clients.
In particular, INTECH, which utilizes a mathematical risk-managed style
of investing, grew substantially: from representing 5.3% of our assets under
management in 2002 to 9% by the end of 2003. Furthermore, Janus
private/subadvised and offshore channels experienced significant growth
over the last year with INTECH driving expansion across our institutional
business.
The result of our expanded product lineup is more choice for every Janus
fund shareholder and better diversification for Janus Capital Group.
Additional Progress
In addition to these accomplishments, other encouraging signs emerged during
our first year as a public company: the resilience of the Janus brand, the
steadfast determination of our colleagues and the support of our long-term
fund shareholders. Although we experienced fund outflows, the majority of
our fund shareholders have stuck with us during this difficult period.
Were also pleased with the progress of our offshore business, Janus
International. During 2003, we continued to see net inflows with assets
increasing 25% to $6.0 billion. Our businesses in Europe, Japan and
Asia Pacific led our international efforts while we expanded into
northern Europe.

2004 Strategic Priorities
We remain focused on our goal of offering our fund
shareholders portfolio management expertise across
a variety of investment disciplines. To that end,
weve outlined three strategic priorities for 2004.
1. We must restore confidence in Janus among our
fund shareholders and stockholders. We must
demonstrate in every action and communication that
we put the needs of our fund shareholders first.
Early in the second quarter of 2004, well resume our
branding and advertising efforts to better position
Janus Capital Group (JCG) in the marketplace.
2. We must continue delivering strong performance,
particularly across our flagship growth funds. In
order for Janus to be successful, we need our
flagship growth funds to deliver strong, consistent
performance. One of the top priorities of our new
President and Chief Investment Officer, Gary Black,
will be to introduce additional risk-management
tools into our investment process with the goal of
consistently beating our benchmarks and competitors.
A second key to our investment success is having
strong three-year performance numbers. During
the bear market, many of our growth funds
underperformed. However, with each passing
month, the poor performance months are rolling
off and replaced with stronger ones. As a result,
our three-year performance numbers are improving
for many of our growth funds.
Finally, and perhaps most importantly, were
committed to maintaining Janus research culture.
Whether its digging into the fundamentals of a
business or testing mathematical theory, an intense
focus on adding value through superior research is
the common theme of all our investment
management teams.
3. We must maintain a strong and liquid balance sheet
and deploy capital to maximize stockholder value.
JCG ended 2003 with cash of $1.2 billion, primarily
as a result of the DST transaction. Our challenge in
2004 is to deploy this capital in a way that increases
stockholder value. To that end, in March 2004, we
announced an offer to exchange and repurchase up
to $465 million of existing debt. Once completed,
this transaction will give Janus a capital structure
more in line with our peers. Well also have the
financial flexibility to consider selective acquisitions
that broaden our product line and/or expand our
distribution, as well as stock buy-back opportunities.
Importantly, we continue assessing opportunities to
deploy our cash consistent with the desire to maintain
appropriate liquidity.


Although a return to positive financial momentum was delayed in 2003 due
to continued fund outflows, were confident that the steps weve taken this
year have put us in a stronger financial position. To deliver the financial
performance that you deserve, we continue monitoring our expenses and
headcount and are pushing forward with our 2004 strategic priorities.
The hard work at Janus is working. One year after becoming a public
company, Janus Capital Group is better positioned to meet the challenges
that lie ahead. Our commitment to you is to do everything within our
power to enhance the value of your investment. And were better prepared
than ever before to get you to your goals, which is the only way well
get to ours.
Sincerely,
Mark Whiston
Chief Executive Officer
Janus Capital Group Inc.

Steve Scheid
Chairman of the Board
Janus Capital Group Inc.
