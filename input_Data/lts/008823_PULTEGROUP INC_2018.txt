LETTER TO THE OWNERS, CUSTOMERS, TEAM MEMBERS AND
BUSINESS PARTNERS OF PULTEGROUP:

2018 was PulteGroup’s best year in well over a decade. 2018 was arguably the most challenging year we
have faced since the current housing recovery began. It is a complex year when you can make these two
statements and have them both be true.
The U.S. housing industry entered 2018 with tremendous momentum and expectations for another year of
double-digit growth, as the fundamental macroeconomic supports of demand remained solidly in place. While
the strong start to 2018 allowed us to deliver outstanding full-year financial results, the operating landscape
changed and became increasingly more competitive as the year progressed.
Review of 2018 Financial Results
Let me begin with 2018 being our best year in over a decade. There is a lot to be proud of in the Company’s
full year operating and financial results, and I am pleased to say that we realized near-term records across a
variety of business metrics. It is important to note that the strong results are a direct reflection of the business
strategies and tactics we have been implementing since the housing recovery began in 2011.
Driven by strong price appreciation and a 10% increase in closings over the prior year, homebuilding
revenues grew by 19% to $10.0 billion, as we generated higher revenues across all product lines: first time,
move up and active adult. We see our ability to serve all buyers as a distinct advantage and believe this
diversification provides the Company unique opportunities over the housing cycle.
Benefitting from a variety of ongoing initiatives targeting margins, overheads and overall business efficiencies,
we successfully leveraged this top-line growth into a 44% increase in pre-tax income. Inclusive of the lower
tax rate provided by the Tax Cuts and Jobs Act of 2017, our net income for 2018 more than doubled over the
prior year to $1.0 billion. Finally, reflecting the impact of our sustained stock repurchase program which
lowered the Company’s year-over-year diluted share count by another 3%, we reported 2018 earnings of
$3.55 per share, a significant increase from 2017 per share earnings of $1.44.
In addition to driving tremendous earnings growth, the gains realized within our homebuilding business
resulted in 2018 cash flow from operations of $1.4 billion. This strong cash flow allowed the Company to end
2018 with $1.1 billion of cash and a net debt-to-total capital of 28.2%, down from 39.4% at the end of 2017.
We also remained disciplined in our deployment of capital in 2018 and allocated available funds consistent
with our stated priorities. For the year, we invested approximately $2.6 billion into the business in the form of
land acquisition and development, including a 13% increase in land acquisition spend to $1.2 billion. We also
returned approximately $400 million to shareholders through share repurchases and dividends and increased
the dividend by 22% beginning in January of 2019.
We believe that the business model we continue to successfully execute is straightforward but powerful. We
seek to invest in high quality projects that we believe can generate appropriate risk-adjusted returns on
invested capital. We then work to efficiently build high quality homes and deliver an outstanding customer
experience. In the end, we use the resulting cash flow to invest back into the business, fund our dividend,
repurchase stock and, when appropriate, pay down debt. Well executed, we believe this disciplined and
balanced approach can provide better returns to our shareholders over the housing cycle.

The Company delivered outstanding financial results in 2018, but we must not allow this strong performance
to obscure the fact that business conditions grew more challenging and competition became more intense as
the year progressed. More specifically, as mortgage rates rose over the course of 2018, home buyers
became less willing to put new homes under contract.
In absolute numbers, mortgage rates remain low by historic standards and supportive of housing, but I believe
the increase in rates exacerbated affordability challenges that had developed after multiple years of
meaningful home-price appreciation. For some homebuyers, higher rates meant they could no longer qualify
for a mortgage, while others elected to delay their purchase until overall market conditions become more
stable.
The industry now looks to 2019 having every reason to expect the pause in buyer demand can be short lived.
Consumer traffic to new-home communities has remained high, suggesting buyer interest is still strong and
that the market is already working to reset itself. Such underlying strength is consistent with a very favorable
macroeconomic backdrop which includes sustained wage and job growth, record low unemployment, high
consumer confidence and powerful demographics. The industry is also benefitting from years of underbuilding
relative to the ongoing growth in population and household formations.
A Strong Business Platform
As demand slowed during the year, we were often asked about how we would respond and what, if any,
changes we needed to make to our operating model. The strategy and tactics under which we operate were
established with a goal of delivering high returns on invested capital over the housing cycle. As such, it is less
about dramatic changes and more about remaining disciplined as market conditions evolve. Let me explain
how this view is reflected in our business practices.
How we manage our land portfolio is critical to the Company’s short and long-term performance. We continue
to develop our land pipeline with very clear objectives: shorten the duration, accelerate inventory turns,
increase the use of options, reduce market risk and, maybe the most important, be patient. Reflective of this
approach, we have shortened our owned lot supply to 3.9 years in total, and 3.1 years if we exclude legacy
lots in older Del Webb communities. At the same time, we have increased lot options to 40% of our controlled
lot position, and to 55% excluding the Del Webb legacy lots.
We are also investing with a focus on creating a better balance across the buyers we serve. In 2018, our
closings by buyer group were 28% first time, 47% move up and 25% active adult. I believe that having the
composition of our business more closely match that of the markets in which we operate can afford us
opportunities to increase market share, grow our business and reduce risk. To that end, of the approximately
150,000 lots the Company had under control at the end of 2018, 33% were targeted toward first-time buyers,
32% were for move-up buyers and 35% were for active-adult buyers. The resulting change in our closing mix
will be gradual, but given the composition of our land pipeline, the direction is clear.

The mix of buyers we serve will change over time, but we continue to emphasize a build-to-order model in
combination with tightly controlled spec production. Build-to-order allows customers to personalize their home
by selecting the options and lot locations that they value, and for which they are willing to pay. We believe that
our build-to-order approach to the market has been a key contributor to our higher gross margins and higher
overall project returns in recent years. That being said, as the mix of first-time buyer communities increases
within our business, spec production is also likely to rise proportionally as this is a common component of that
business.
Finally, we continue to refine our common plan management platform as we work to build homes more
efficiently and profitably. In 2018, 81% of the 23,107 homes we delivered were part of our common-plan
library. In launching our common-plan platform back in 2011, we wanted to reduce the number of floorplans
we offer, optimize the plans for material content and ease of construction, and then increase the throughput
(unit volume) of each plan. In addition to supporting our current production capabilities, common plans can
make it easier to integrate offsite manufacturing as it develops in the future.
These business-oriented strategies and tactics are important, but I would say that the people-centric initiatives
we are advancing inside our organization are equally vital to the Company’s success. In last year’s letter, I
highlighted that PulteGroup’s employee engagement as measured by Gallup was in the top 5% of all
companies in the world. It is extremely rewarding to report that we repeated this ranking in 2018. Not resting
on our laurels, we have enhanced our employee recruiting, onboarding and training programs, and put in
place new guidelines to purposefully build a more diverse and inclusive culture. Operating in an economy
experiencing record low unemployment, we believe our ongoing investment in people development will
continue to pay dividends.
Well Positioned for the Year Ahead
The estimated 622,000 new homes sold in the U.S. in 2018 marks the twelfth year in a row that industry-wide
sales have failed to reach the 50-year average of 665,000 houses. In other words, we believe the industry
continues to under build relative to the growth in population and household formations. Given such limited
production, even with slower sales in the back half of 2018, government data showed less than seven months
of new home inventory being available for sale heading into 2019.
In addition to a healthy supply dynamic, we see the overall operating environment as supportive of U.S.
housing demand. More specifically, after rising through much of 2018, mortgage rates have fallen and
comments from the Federal Reserve suggest a more measured and thoughtful approach to future rate
increases.
While rates are easing, the job market remains strong with an estimated 130 million people now employed
across the country and unemployment hovering near recent lows of 4%. The strong job market is also likely to
result in a sustained period of wage and income growth which will allow more consumers to purchase a home
and ease some of the affordability challenges which have weighed on demand.

In conclusion, macro conditions remain strong and can support higher housing demand, but the industry is
experiencing a “pause” in buyer activity that has created a period of uncertainty heading into 2019.
Fortunately, we have built a strong business platform and are operationally and financially well positioned to
compete at any stage of the housing cycle.
PulteGroup’s strong market position reflects the hard work and the disciplined execution of our business plan
by the most talented team of employees in the industry. Our Board of Directors and I want to thank the
5,000-plus employees of PulteGroup who are committed to delivering the best homes and homeownership
experience in the industry. I also want to recognize the support we receive every day from our suppliers, trade
partners and investors, as this too is vital to PulteGroup’s sustained success.
Sincerely,
Ryan Marshall
President and Chief Executive Officer