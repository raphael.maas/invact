LETTER TO SHAREHOLDERS
Over generations, our Company has built a strong track record 
of growth and performance by pursuing our mission of being 
the dominant supplier of auto parts in our markets. 2001 was 
no exception. With more than 12,500 dedicated team members 
having a strong focus on customer service, work ethic and our
prominent culture, Team OReilly has completed a Model Year.
In 1998, we embarked on a vision for growth called 1-5-U. It
represented our mission to reach $1 billion in sales in five years.
We are proud to announce that once again Team OReilly has 
risen to the challenge, meeting this goal one year early. This
demonstrates the commitment of our team members and their 
ability to excel beyond expectations.
In late 2001, OReilly seized a tremendous opportunity with 
our acquisition of Mid-State Automotive Distributors, Inc. The
acquisition provided strategic and contiguous growth for our
Company throughout seven additional states, added 82 net new
stores, two distribution centers and over 1,800 new experienced
team members. In addition to our acquisition of Mid-State, 
we added 121 new stores, bringing our total store count to 
875 throughout 16 states.
We continue to make improvements in our use of technology.
Our Global Inventory System has increased the availability of 
parts to our customers by giving our stores visibility to inventory 
at all distribution centers and other stores. This system also
reduces inventory levels at both stores and distribution centers.
We continue to find new ways to utilize TeamNet, our intranet
system and reduce the cost associated with printed materials
while improving communications with our stores.
A lot of hard work by Team OReilly produced another year of
strong financial results. Product sales of $1.09 billion,an increase
of 22.7%, a 10.4% operating margin and net income growth of
26.0% highlight this Model Year. Approximately 56% of product
sales were generated from the do-it-yourself or retail trade, 
and approximately 44% of product sales were generated from 
the professional installer market. We continue our focus on this 
dual-market strategy with a goal of 50% from each market. 
OReilly has positioned itself for the opportunities ahead. 
Our plans for 2002 include opening at least 100 new stores and
same store sales objectives in the mid single-digit range. We 
will continue to leverage our technology investment in the area 
of inventory control. Our goal is to achieve inventory turns of 
1.7 times, an operating margin of 11% or greater and top line 
sales growth of approximately 18-20%. 
We look forward to taking the opportunities that lie ahead in
2002 and converting them to shareholder value. Team OReilly has
a successful track record of responding to these opportunities 
as we strive to be the dominant auto part supplier in our markets.
Thank you for taking time to learn more about Team OReilly and
for your continued support and confidence.
