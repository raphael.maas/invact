Right Now
Massey Energy  our people, our coal, our strategy 
is poised to meet increasing global demand.

Dear Fellow Shareholders:
Years of strategic planning and capital spending have
prepared Massey Energy with the equipment and
facilities to take advantage of the exciting opportunities
in todays coal markets. These markets are strong, due
to the combination of ever-increasing demand, tight
supply, and transportation issues that are difficult
to correct.
Market imbalances in the coal industry have historically
rectified themselves, as we believe this one will. However,
a number of factors suggest that the current worldwide
coal supply/demand imbalance may not be as easily
remedied as in the past. The current situation is significantly
driven by worldwide economic expansions, with
dramatic growth in China. However, the thirst for
energy throughout the world is constantly on the rise.
As a result, pricing for Masseys products has escalated
continually since late 2003. This is especially true
for our metallurgical coal, which is not only in high
demand domestically but also overseas. In this environment,
the Companys goal is to achieve measured
capacity growth and generate rapid rates of return
on our investment.
To date, we have achieved our capacity expansion primarily
through opening low-cost surface mines and
adding a variety of new, high productivity equipment.
While underground productivity issues, lack of experienced
labor, and railroad service problems prevented
us from shipping more tonnage during 2004, we were
able to increase our shipments of metallurgical coal.
Massey Energy  our people, our coal, our strategy 
is poised to meet increasing global demand.
Massey also increased 2004 export tonnage, most of
which is metallurgical coal, to 6.7 million tons, as compared
to 5.0 million tons in 2003.
We believe we are well positioned to grow production
and sales in 2005. We expect metallurgical coal sales
to increase 3 to 4 million tons above the 10.4 million
tons of metallurgical coal we sold in 2004. Massey is
one of the very few companies in Central Appalachia
that is able, from a liquidity, permitting and reserve
position, to significantly increase production in
response to market opportunity.
We took several other steps during 2004 that were targeted
toward protecting our existing capital investment
while setting the stage for revenue growth in future years.
We made two strategic purchases of new reserves for
possible future expansion, including an estimated 20 million
tons in Central Appalachia and over 60 million tons
in Pennsylvania. As a result, we ended the year with total
coal reserves of 2.3 billion tons.
In addition, we established a joint venture with Penn
Virginia Resource Partners, L.P., to develop and manage
coal handling facilities like those we operate at
several of our current industrial customers. We continue
to pursue similar business alliances and consider
the acquisition of coal reserves that represent good
values and strategic fits, while still focusing on limiting
our long-term liabilities, which remain significantly
lower than our competitors.

In 2004, we continued to experience significant upward
cost pressures on commodities and services, such as
fuel, explosives, medical care, labor, insurance and
bonding, which we work aggressively to manage.
Access to experienced labor continued to be an impediment
to growth and productivity improvement, and we
devoted significant effort to hiring experienced miners
and training less experienced miners. We enhanced
our member benefits program by working with a primary
care organization in West Virginia that will open
a medical facility for the benefit of Massey members during
early 2005. This facility represents an innovative
opportunity to simultaneously manage our costs and
increase the quality and availability of healthcare to
our members and their families.
A risk we face now relates to the possibility of illconceived
regulatory programs put in place by state
or federal governmental bodies in response to the
concerns of certain activist groups. Valley fill and other
permitting issues in West Virginia continue to delay,
and in some cases actually prevent, mine expansion.
However, we remain hopeful that the flow of permitting
will continue to improve.
We face continued uncertainty about the implementation
of federal legislation regarding emissions
standards. We trust that realistic compromises can
be found to support the countrys dual imperatives:
the need for a continually cleaner environment and
the need for a reliable, abundant, secure and low-cost
source of energy. We believe it is possible to achieve
both these goals and we applaud further governmental
and industrial support of clean coal technologies, such
as coal gasification.
We were pleased to note that the National Commission
on Energy Policy, an advisory group to Congress and
the Administration, issued a report that supported
many of the views that Massey and others in the coal
industry have been articulating. The message is that
coal must continue to play a major role in supplying
the future energy needs of the United States.
We remain more excited than ever about coal and the
opportunities for Massey Energy over the next several
years. We expect to increase our annual shipments,
our average per ton realization and our profit margins.
We expect to ship more high-priced metallurgical coal,
especially overseas, where strong market conditions
are expected to continue. While cost pressures are
unrelenting, we remain focused on controlling our
cash costs by utilizing lower-cost mining methods and
more high productivity equipment. As a result, we
anticipate significant increases in free cash flow over
the next several years, and improved returns for our
stakeholders. Thank you for your continuing support.
Don L. Blankenship
Chairman, Chief Executive Officer and President