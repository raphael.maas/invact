TO FELLOW SHAREOWNERS AND FRIENDS:
This past year was a historic and, in many ways, a transformational year for Huntington. We celebrated the
150th anniversary of The Huntington National Bank proudly serving our local communities, and we completed
the largest acquisition in our Companys history, FirstMerit Corporation. We also eclipsed two meaningful
financial milestones for the first time, as total assets exceeded $100 billion and quarterly revenue exceeded $1
billion. We welcomed to Huntington an extraordinary group of new colleagues from FirstMerit. We also
continued organic growth in our legacy businesses, while adding new products, including our new recreational
vehicle and marine lending business, and the markets of Chicago and Wisconsin. We continued to invest in our
businesses with a keen eye on both the risks and the opportunities in the future.
2016 Core Performance Highlights
Our 2016 financial performance was strong, particularly when considering the sizable costs involved in the
acquisition and integration of FirstMerit. On a reported (GAAP) basis, Huntington recorded net income of $712
million in 2016, which represented a 3% increase compared to the prior year. Earnings per common share were
$0.70, down 14% year-over-year with the decline largely resulting from the issuance of common shares as
consideration in the acquisition. Return on average assets was 0.86%, while return on average common equity
was 8.6%. Return on average tangible common equity was 10.7%. All of these financial metrics were impacted
by the $187 million of after-tax net expenses, or $0.20 per common share, related to the acquisition and ongoing
integration of FirstMerit and a reduction to the litigation reserve totaling $27 million after-tax, or $0.03 per
common share. Excluding the $187 million after-tax ($0.20 per common share) of FirstMerit-related acquisition
expenses and the $27 million after-tax ($0.03 per common share) litigation reserve reduction, adjusted net
income and adjusted EPS were $872 million and $0.87, respectively. Our solid performance allowed us to
increase the annual dividends paid for the sixth consecutive year.
In the fall of 2014, the Board of Directors adopted five long-term financial goals:
(1) Annual revenue growth of 4%6%;
(2) Annual revenue growth in excess of annual expense growth, or annual positive operating leverage in
financial vernacular;
(3) Return on tangible common equity, or ROTCE, of 13%15%;
(4) Efficiency ratio of 55%59%; and
(5) Net charge-offs of 0.35%0.55%.
In 2016, we achieved three of these five financial goals. Revenue growth was 18% year-over-year,
materially aided by the FirstMerit acquisition. We delivered positive operating leverage on an adjusted basis for
the fourth consecutive year. Net charge-offs were 0.19%, remaining better than the long-term range for the third
consecutive year. The other two long-term financial goals, ROTCE and efficiency ratio, remain opportunities for
improved performance. We expect to continue to reap the financial benefits of the FirstMerit acquisition in the
form of both revenue synergies and expense savings. We expect these deal economics, coupled with a continued
focus on driving positive operating leverage, should help us deliver both the efficiency ratio and ROTCE goals.
FirstMerit Acquisition  Accelerating Achievement of our Long-Term Financial Goals
In August 2016, Huntington closed the strategically and financially attractive acquisition of FirstMerit
Corporation, a truly transformational acquisition. Paul Greig, FirstMerits Chairman, President and Chief
Executive Officer, his management team and the FirstMerit Board of Directors did a great job of building a
company with strong fundamentals and obvious opportunities for growth. FirstMerit was founded over a century
and a half ago in Akron, Ohio, with a purpose of helping customers with their financial needs and helping the
communities they served. P.W. Huntington shared that same vision. Today, we move forward as one company
with a common purpose of doing the right thing and looking out for our customers. The integration of the two
companies has gone very smoothly. That is a testament to the dedication of colleagues on both sides.
The cultural synergies were obvious, but there was also a significant amount of physical overlap, with more
than 80% of FirstMerits deposits located in our footprint and approximately two-thirds of their branches located
within two and a half miles of a Huntington branch, providing opportunity for immediate and additional future
branch rationalization. We are proud now to have the No. 2 deposit market share in Ohio and the No. 6 deposit
market share in Michigan. Huntington enjoys No. 1 market share of branches in both states. FirstMerit also
provided entry into the attractive Chicago and Wisconsin markets. Finally, FirstMerit brought a new product
offering to Huntington with their recreational vehicle (RV) and marine lending business, an attractive business
we were looking to build on our own and are now pleased to expand upon their success. Harvesting efficiencies
by eliminating duplications, as well as capitalizing on the increased depth and breadth of our collective reach, are
the core tenets of the underlying rationale for this transaction.
I believe the benefits of the acquisition are apparent today. During the 2016 fourth quarter, the first
reflecting a full quarters impact of FirstMerit, Huntington generated more than $1 billion in revenue for the first
time. More importantly, not only did FirstMerit make Huntington bigger, the acquisition made us better. The
significant overlap between the two companies and our complementary businesses provided sizable expense
savings, as well as significant revenue synergy opportunities. We upgraded and deepened our talent with an
experienced, highly capable team from FirstMerit. This acquisition also allowed us to become more ingrained in
our local communities than ever before. We expect these financial and strategic benefits will manifest themselves
into our achievement of all five of our long-term financial goals, including ROTCE of 13% to 15% and
efficiency ratio of 56% to 59%, several years sooner than we could have achieved on a standalone basis.
Disciplined Execution  Threading the Needle
In last years letter, I shared that Huntingtons disciplined execution has proven to be a distinguishing
feature of Huntington. That fact was clearly evident in 2016. History has shown that very few organizations have
successfully navigated the difficulties of growing the legacy core business while integrating a large,
transformational acquisition at the same time. However, we are doing just that. Further, there is much more to an
integration than just converting computer systems and rebranding buildings. Integration requires a shared focus
and commitment with colleagues. Both legacy Huntington and legacy FirstMerit colleagues have been working
together with a singular mission, producing solid results while presiding over the best integration I have seen in
my career. There is still work to be done, but the execution to date has been superb. We will finish the job. Our
Board joins me in recognizing and thanking our colleagues for their extraordinary efforts to create these results.
Momentum Across Our Businesses
We maintained our focus and execution with respect to legacy Huntington business in addition to the
integration of FirstMerit. As a result, we enter 2017 with momentum and opportunity across our businesses. Our
commercial segment continues to benefit from investment in our teams and complementary products, such as
capital markets and treasury management. Our Commercial Real Estate business remains disciplined, focusing
on a core group of developers with whom we can build substantial, deep relationships. Consumer and Business
Banking introduced Huntington products and services to our newest clients. We also have expanded our Small
Business Administration (SBA) lending capacity with the build-out of a team in Chicago, a very large market.
Home lending introduced a streamlined and improved underwriting process early in 2016 and then built upon
that success by expanding its expertise into the attractive Chicago and Wisconsin markets. Huntington Auto
Finance built on its distinguished, more than 60-year history of meeting the financial needs of automobile dealers
and consumers, and further expanded its footprint into Texas, Missouri, and Kansas. With the addition of
FirstMerits RV and marine lending business, we immediately began the expansion of the business to nine new
states. Finally, our Private Client Group segment made significant strides in transitioning business focus,
building on core competencies and positioning for growth.
Our Foundation of a Customer-First Culture
We cannot lose sight of our opportunity and our responsibility to help meet our customers financial needs.
Banks help families buy their homes, plan and save for the future, grow their businesses, and create better lives in
countless ways. But no bank can achieve these noble pursuits without truly knowing their customers. We know
there are people and families behind each mortgage application, credit file, and savings account. Banks also
appropriately engender a higher level of expectation, given the stewardship of customers financial well-being.
Much of our success with consumer and business banking at Huntington can be attributed to our Optimal
Customer Relationship, or OCR, strategy and our Fair Play philosophy. We positioned our products and services
around simple, easy to understand concepts. Our success is the result of our more than 16,000 colleagues doing
the right things for our customers and delivering superior customer service every day. Culture is the key, and the
heart of Huntingtons culture is our unyielding commitment to Do the Right Thing for our customers. We get
to know our customers, their unique situations, and their distinct wants and needs. Our compensation and
incentive programs are appropriately aligned, and ongoing monitoring of these programs by our risk management
systems provide an additional level of assurance.
The Importance of Strong Risk Management
With a Board-approved aggregate moderate-to-low risk appetite, Huntington has built a sophisticated
enterprise risk management infrastructure to assure our alignment with our stated risk appetite. An effective risk
culture always starts with an appropriate tone at the top, and the Board and our Executive Leadership Team are
acutely focused on risk management. Furthermore, all of our colleagues are empowered to flag inappropriate or
inadequately identified risks. Every Huntington colleague knows our core belief that we all own risk. All too
frequently over the past few years, the importance of strong risk management and an equally strong risk culture
have been highlighted in the financial press. These unfortunate events weigh heavily upon the industry and have
led to further distrust of some financial institutions.
Corporate Environmental, Social, and Governance (ESG) Strategy
Huntingtons Board of Directors provides a deep, diverse body of expertise, experience, and leadership from
which the Executive Leadership Team and I draw. Strong corporate governance is extremely important to our
Board, and our Board is very significantly engaged. I hope you will take the time to read the 2017 Proxy
Statement, which details many aspects of that commitment.
Huntington is an industry leader in compensation programs, including years ago when we implemented
hold-to-retirement requirements for certain equity compensation and strengthened our ownership expectations for
our senior leadership in order to better align our interests with our fellow shareowners. The Board has adopted
similarly forward-thinking policies with respect to areas such as board governance and our commitment to
diversity and inclusion. It is in that vein that I am excited to announce Huntington recently adopted a corporate
ESG strategy. ESG is an emerging focus area for corporate America, but it should not be viewed merely as a fad.
I look forward to sharing more details of this new commitment in the days and weeks ahead, but I am excited
about where this strategy will lead the Company.
Our Best Days Lie Ahead
While there was much to celebrate in 2016, we enter 2017 with momentum. Our local economies continue
to perform well. Were seeing increased consumer and business confidence across our footprint. Manufacturing
is growing and adding jobs. Our states and local municipalities are generally on firm ground. These factors
contribute to a favorable operating climate and will continue to fuel further economic growth. Interest rates have
begun to increase and appear poised to increase further, which will be additive to our earnings. Finally, there is
reason for optimism that the regulatory pendulum could swing back to the other direction, with tailored relief for
banks with simple business models like ours.
We have entered 2017 a stronger Huntington. We are focused on leveraging our strengths and our
differentiated brand. We are investing in growth areas to drive revenue growth. We are investing in and retaining
our talented colleagues. We are constantly striving to operate more efficiently. We remain disciplined in our
credit and risk management. The future is bright for Huntington. I strongly believe our best days truly lie ahead.
Stephen D. Steinour
Chairman, President and Chief Executive Officer
March 6, 2017