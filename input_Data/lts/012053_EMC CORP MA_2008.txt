Dear Fellow Shareholder,

EMC has emerged from the depths of last years global recession a much stronger and more agile company. Few

businesses can say that. We entered 2010 in our best fi nancial and operational shape ever, and we are in an

excellent position to play an increasingly strategic role in the success of a broadening range of organizations. I say

that because we have the vision, technologies, products, services, partnerships, and, above all, the people needed

to lead the newest and potentially largest wave of change in the IT industrys long evolution.

This new wave is known as cloud computing, and it has captured the attention of pragmatic organizations everywhere

that are frustrated with the increasing complexity, infl exibility, and cost of their information technology environments.

They fi nd cloud computing compelling because it holds the promise of making IT much easier and more cost-effective

to install, use, and manage than ever before. In short, cloud computing should enable organizations to achieve faster

and higher returns on their IT investments.

EMC is enabling this new wave. In fact, our industry-leading portfolio of innovative virtualization, storage, security,

and information and content management products is essential for cloud computing to fulfi ll its promise. For the last

few years, weve been hard at work moving our company into position to provide customers with a safe path from the

world of physical IT to fully virtualized IT and ultimately to cloud-based IT infrastructure. Doing so will open the door to

tens of billions of dollars in new market opportunities for EMC.

In this letter, I will focus on how we strengthened EMC in 2009 and executed our strategy to extend our leadership,

and how we are paving the way for customers to realize the benefi ts of a new vision of IT that we established with

some of our strategic partners.

Since we are building our future on what we have accomplished in prior years and especially last year, lets look fi rst

at 2009. For the full year, we achieved total consolidated revenue of $14 billion. This modest decline from our 2008

revenue happened during a steep, worldwide reduction in IT spending. It is worth noting that we gained momentum

as 2009 progressed and achieved record fourth-quarter revenue of $4.1 billion, up 17 percent sequentially. In the

fourth quarter, we also achieved record results in non-GAAP operating margins, non-GAAP EPS, and free cash fl ow.

For the full year, we exceeded our non-GAAP EPS* goal of $0.87 by three cents, and we far exceeded our per-share free

cash fl ow* goal of $0.87 by achieving $1.27 per share. All told, we generated a record $2.6 billion in free cash fl ow*

and ended 2009 with $9.4 billion in cash and investments. These results highlight our sustained fi nancial strength

and fl exibility, which enable us to keep investing in our future during all kinds of economic cycles. Our stock price

refl ected our ability to thrive in 2009, climbing 67 percent during the year and outperforming the Dow, the S&P 500,

and the S&P high-tech indices over the same period.

VMware, the global leader in virtualization solutions and a publicly traded company of which EMC owns 81 percent,

contributed revenue of $2 billion, an increase of 8 percent from 2008.

During 2009 we continued our comprehensive cost transformation program, exceeding our goal of saving EMC

$450 million. We continue to be systematic in analyzing the way our business operates because our aim is to do

more than simply cut costs selectively for the short term. Instead, we want to improve our cost structure permanently

and achieve a higher level of operational excellence.

I offer my thanks and gratitude to the talented people of EMC around the world who performed extremely well. While

they work heroically every year to execute our strategy and serve our customers and partners, in 2009 they did even

more. They were selfl ess, accepting a temporary reduction in their pay, as did the entire senior management team, to

preserve the jobs of their colleagues and help keep our company strong for the long haul. Whats more, EMCs fully

engaged and supportive Board members also signed up for this temporary pay reduction. Im extremely proud of our

global team.

At its core, EMC is a technology company. Our focus is IT infrastructurethe foundation that enables the applications

and information that every organization needs to be effective. Our mission is to help people and organizations bring

the power of their information to lifeinformation that continues to grow rapidly and relentlessly. And our passion is

to innovate to make life better for customers and to create new opportunities for EMC to grow.

To fuel our innovation, weve been investing roughly 11 to 12 percent of our annual sales in R&D, or more than

$7 billion over the past fi ve years. Over the same period, we invested roughly the same amount to acquire approxi-
mately 40 companies. We will continue to invest heavily to extend our product technology advantages.

Last year, our investments enabled us to introduce highly innovative products and services across our business units

with the best quality in our history. For example, we launched our most advanced storage architecture since the origi-
nal EMC Symmetrix system essentially created the high-end storage industry 19 years ago. Built on the EMC Virtual

Matrix ArchitectureTM designed for virtual data environments, Symmetrix V-MaxTM systems enable customers to scale

out their storage resources simply and cost-effectively, with massive levels of performance, capacity, and connectivity

that can be shared across applications.

During the year, we also introduced breakthrough automation capabilities across EMC storage platforms with our

Fully Automated Storage Tiering (FAST) technology. FAST optimizes performance by automating the movement

and placement of data so that the right data is always in the right place at the right time, which lowers costs and

delivers higher service levels. Drawing on the solutions of RSA, The Security Division of EMC, we have augmented

the information security capabilities of products across our portfolio. To help companies implement sound informa-
tion governance, EMC launched EMC SourceOneTM, a new family of products and solutions for archiving, eDiscovery,

and compliance. In addition, we introduced more than 30 EMC services and solutions to accelerate the adoption of

cloud computing.

Building world-class products and services that can meet customers needs today and in the future is what EMC does

best. Many of our offerings are widely recognized as absolute leadership products in the data center, among them our

next-generation data protection and recovery products, VMware virtualization solutions, Symmetrix V-Max, and

EMC Celerra Unifi ed Storage platforms. Our broad portfolio of services is helping customers get even more value

from their investments in EMC technology and solutions. And our MozyTM cloud-based applications for backup and

personal information management are now handling more than 35,000 terabytes (and growing) of customer data.

We used our strong balance sheet to acquire four innovative growth companies in 2009, most notably Data Domain,

whose leading deduplication technology identifi es redundant fi les and data as they are being stored and provides

a storage footprint that is 10 to 30 times smaller, on average, than the original dataset. Data Domain became the

foundation of a new, high-growth information infrastructure product division called Backup Recovery Systems that is

focused on delivering the next generation of backup, recovery, and archiving solutions. Over the years, weve become

skilled at identifying, acquiring, and integrating companies that complement our own capabilities, help us solve

customers emerging IT challenges, and extend our business into related market segments.

Improving our customer satisfaction and loyalty scores is a top priority for us. Weve made signifi cant progress by

maintaining the highest levels of product quality and by helping our customers continually reduce their costs. Weve

also established a rigorous voice-of-the-customer loyalty measurement that we track closely, as well as a disci-
plined process for continuous improvement. And weve recently implemented a Net Promoter scorecard as a further

discipline for using feedback from our customers to measure and increase their loyalty and to fuel profi table growth.

2010 Prospects

Turning to EMCs prospects for this year and beyond, were confi dent in our ability to grow our revenues, gain signifi -

cant market share, and deliver improving profi tability while investing for the future. Here are a few reasons why:

First, while we remain cautious about the global economys pace of recovery, we do expect the recovery to continue

and overall spending on IT to return to modest growth (between 3 and 5 percent) this year. Our outlook also calls for

EMCs addressable market to grow faster than the IT industry overall, probably in the range of 6 to 8 percent.

We fi nd that many of our customers have emerged from IT lock-down mode. To be sure, they remain intensely

focused on increasing the effi ciency and reducing the cost and energy use of their IT operations. However, they also

have renewed their interest in investing in IT to get closer to their customers, become more agile and global, and

sharpen their competitiveness. Whats more, they want a path to a simpler, more cost-effective, agile IT world. That is

precisely what EMC can offer, which leads to another reason were optimistic about the future.

We have a vision to lead customers on a safe journey to the private cloud, which is our shorthand for a dramatically

more effi cient and effective model to deliver information technology as a servicejust as other forms of infrastructure

like electricity or telephony are delivered as services today. Private clouds will provide customers with unprecedented

levels of effi ciency, control, and choice, enabling them to focus far more of their precious time and budgets on IT

activities that really differentiate them as businesses.

For organizations that want to speed their journey to virtualizing their entire infrastructures and then implement

private cloud computing, EMC and Cisco, together with VMware, established a fi rst-of-its-kind collaboration last year

called the Virtual Computing Environment (VCE) coalition. This powerful coalition will help us move our customers

along the path to their preferred future. In essence, we are telling our customers that next starts now, that they can

get to the future they want sooner with our help.

EMC takes the long view and works hard to strengthen its foundation for long-term growth. With that in mind, we are

focused on four rapidly emerging, multi-billion-dollar markets that we believe present opportunities for double-digit

revenue growth. The largest of these opportunities is to help organizations transform their data centers into automat-
ed, fully virtualized data centers that can operate far more dynamically and cost-effectively than traditional

IT environments.

We are also working with our service provider and telecommunications partners to help them develop their

public clouds, which will make the benefi ts of IT as a service available to both businesses and the general public.

A third big growth opportunity involves using virtualization and advanced information infrastructure solutions to

transform traditional desktop computing. Our aim is to give end users maximum fl exibility while providing IT organi-
zations with the centralized management and control they need to lower costs and increase the security of corporate

information assets.

Our fourth large opportunity is to help customers dramatically reduce the amount of disk storage they need to man-
age and protect their ever-growing volumes of enterprise data by applying our next generation of backup, recovery,

and archive technologies.

To pursue these market opportunities aggressively, we expect to increase our R&D investment this year by 20 percent

over 2009. In addition, we will draw on not only the leading technologies Ive discussed in this letter, but also on the

knowledge and solutions of our ecosystem of alliance partnerstop-tier technology, distribution, service, systems

integration, outsourcing, and service provider partners. We have rededicated ourselves to our strategic partnership

with Dell, a global alliance that is one of the longest-running and most successful in the IT industry and that provides

tremendous value to our mutual customers. We are determined to create a partner ecosystem that is second to none

in the IT industry. Of course, the people of EMC areand always will bethe heart and soul of EMC, the engine of our

growth, and the key to creating substantial value for our customers.

I want to take this opportunity on behalf of the 43,000 members of the global EMC family to express our gratitude and

thanks to the man who made this great company possibleDick Egan, our founder and the E in EMC. Dick passed

away last year after a long battle with cancer, the same month EMC reached its 30th birthday, a mark of longevity

rarely imagined by technology entrepreneurs. His spirit is alive and well in EMC, and his passion, enthusiasm, drive,

and love for people, especially the people of EMC, will be with us always. All of us are in his debt for the winning

culture he created and for what he enabled us to become.

Were determined to sustain EMCs relevance and vitality for decades to come. So we strive to make decisions and

operate our business in a sustainable way. We know the world is endlessly interconnected and recognize that our

companys prosperity depends on a healthy environment and a vital, inclusive society. Newsweek magazine named

EMC to its 2009 list of The Greenest Big Companies in America.

While we are proud of our progress, we know were on a journey and have much to learn about operating as a sus-
tainable company. Earlier this year we became a member of Ceres, a highly regarded national network of investors

and environmental organizations that work with companies to address sustainability challenges. We will tap into its

wealth of knowledge. We will draw on its connections to help us deepen our engagement with diverse stakeholders.

And we will strive to advance our own sustainability initiatives while contributing to the worlds movement to a low-
carbon economy.

Now entering my 11th year with EMC, I can say without hesitation that I have never been more excited about EMCs

future. Our vision of taking customers on a safe journey to the private cloud, when combined with our track record of

execution, is prompting more customers to view us as the go-to company with the know-how, innovative technol-
ogy, and partnerships to help them overcome whatever challenges they may encounter. We intend to lead the newest

wave of IT, and we are confi dent in our ability to deliver a record year that will include market share gains, investment

in the future, and improving profi tability.

Joseph M. Tucci

Chairman, President and Chief Executive Offi cer

EMC Corporation
