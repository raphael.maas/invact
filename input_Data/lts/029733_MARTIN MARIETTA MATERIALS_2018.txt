To Our Shareholders:
Writing an annual letter allows me to speak directly to all of you, our owners, and
share Martin Marietta�s story. Searching for the right words to best capture Martin
Marietta�s compelling story presents its own distinct challenge, particularly with regard
to 2018 when our record financial and safety performance was overshadowed by
lower-than-expected shipment volumes and a lower stock price. Hopefully through
the words I have chosen and the examples provided, you will clearly understand the
challenges Martin Marietta faced and met in 2018 and share my abiding optimism
for our business both for now and for the foreseeable future.
Martin Marietta wants shareholders to know not only how our Company performed
in the past year, but also the circumstances that impacted, both positively and
otherwise, our reported financial and other metrics. We continually strive to be
transparent, keeping you informed of the macroeconomic factors that affect our
results and may influence our future performance. Above all, we believe it is vital to
fully understand our Company�s character. Character matters � a lot. Our character
dictates that we confirm to you that we know and appreciate that you have placed
not only your trust, but also your hard-earned money, in our hands. We take that
responsibility seriously. Our people are the foundation of Martin Marietta and we
are proud of what they do each and every day. Indeed, our teams are dedicated
to producing quality products and results that together enhance the value of your
investment. That�s our covenant with you.
Our character also dictates that we remain mindful of how we produce these results.
Our products build the foundation of the communities in which we operate. That simple
fact transcends everything we do. We have a responsibility to operate with a view
toward improving the future. That�s our covenant to all who come in contact with Martin
Marietta, our operations and our products.
Every Martin Marietta employee is committed to two overarching values: safety and ethical
conduct. These values define our Company. To be clear, these are non-negotiables. Violations
of these values provide the most efficient way out of Martin Marietta as we do not tolerate
actions that are incongruous with our character. The strength of our character drives our
performance and helped drive 2018�s record performance. We believe this commitment to
what we stand for has made Martin Marietta our industry�s preferred employer at a time when
attracting and retaining the best talent is more challenging than ever.
We strongly believe that everything we accomplish flows, directly or indirectly, from the
successful design and execution of our strategic plan, known internally as our Strategic
Operating Analysis and Review, or SOAR. Not surprisingly, SOAR is founded upon an enduring
focus on staying safe � in every respect. Specifically, 2018�s record financial results were largely 

achieved due to our proven ability to work safely. For example, our heritage operations benefited from a reduced Total Injury Incident
Rate, or TIIR (a key industry and regulatory measure), that bested our previous record TIIR established in 2017. Inclusive of the
operations and facilities we acquired and integrated during 2018, primarily Bluegrass Materials Company (Bluegrass), we achieved a
company-wide world-class Lost Time Incident Rate, or LTIR, for the second year in a row. I encourage you to remember these metrics
and be proud, as are we, of how our teams achieved the impressive financial results.
Turning to 2018�s financial results, we are pleased to have established new records in a host of key metrics. Among them, total
revenues rose seven percent to $4.2 billion; Adjusted Earnings Before Interest, Taxes, Depreciation and Amortization, or EBITDA,
increased nine percent to $1.10 billion; and adjusted earnings per diluted share of $8.08, increased 11 percent. The 2018 EBITDA
and earnings per diluted share numbers are adjusted to reflect the impacts of acquisition-related items, and asset and portfolio
rationalization. The 2017 results against which we compared our performance exclude the one-time benefit we recorded in
the fourth quarter of 2017 as a result of the Tax Cuts and Jobs Act of 2017 that went into effect at that time. Our strong 2018
performance underscores the importance of SOAR and our execution against these priorities, namely, operating our businesses safely
and benefitting from a timely and highly accretive Bluegrass acquisition.
Our 2018 financial performance can best be
summarized as challenges faced and challenges
met. Our largest business segment, Building Materials,
posted record total revenues of $4.0 billion (compared
with $3.7 billion in 2017). And yet it could have been
considerably better. We entered 2018 with guarded
optimism. The macroeconomic environment supported
a continuation of the slow and steady momentum our
industry experienced in 2016 and 2017. Specifically,
private-sector construction activity, driven largely by
the residential and nonresidential sectors, remained solid. On the public side, Federal infrastructure investment was still not at
the top of the Congressional agenda (although Democrats, Republicans and the President all voiced support for the needed
additional funding to rebuild our nation�s highways, bridges, roads, streets, airports, etc.). However, key individual states
throughout Martin Marietta�s geographic footprint stepped forward, accepting the challenge. In fact, our top ten states,
accounting for 85 percent of the Company�s Building Materials 2018 total revenues, all introduced incremental transportation
funding measures within the last five years � most with overwhelming voter support. That said, in providing our 2018 outlook,
we did not factor significant growth in aggregates shipments to infrastructure, our largest end-use market.
It is fair to say that no one, including our customers or peers, anticipated the extraordinary weather disruptions we experienced
throughout 2018. Erratic weather was the major driver of an approximately 8-million-ton shortfall in our anticipated
aggregates shipments. Being an outdoor business, record-setting rains, attendant flooding and hurricanes impacted not only
our ability to efficiently operate, but also prevented our customers from having constructive access to their job sites. We even
noted that our �full employment� nation, while good for the country and its citizenry, caused labor shortages, especially in the
construction and transportation sectors. Nonetheless, we anticipated that these challenges would be transitory.
Both experience and, more importantly, careful market-by-market analysis informed us that while infrastructure projects might be
temporarily delayed, they would not be cancelled or shelved indefinitely. Our confidence has been underscored not only by the
numerous state initiatives discussed above, but also by improved public construction funding provided by the Fixing America�s Surface
Transportation, or FAST Act, which is in place through fiscal year 2020. These factors coalesce nicely to frame an appealing trend
for public-sector construction over the next several years. As noted above, private construction activity was well-positioned
entering 2018 � particularly residential construction. By year end, though, housing experienced something akin to a �tale of two 

halves.� Housing starts increased nearly eight percent in the first half before decelerating to low-single-digit growth by year end. That
said, mortgage rate increases have now abated while much of the nation is experiencing full employment and growing wages. These
are good combinations for an enduringly healthy private-sector construction environment, particularly in our key regions.
Mother Nature was also the primary driver for reduced volumes from our cement and downstream operations. And while we
can�t control Mother Nature, we can meet the challenges she poses. Our focus on strong product pricing and cost discipline,
combined with our value-enhancing acquisition of Bluegrass, enabled us to exceed our 2017 performance for both total
revenues and EBITDA. That�s good news. Even better news: 2018�s delayed projects and associated product shipments didn�t
go away. They will be there for us as we enter 2019.
As mentioned earlier, the Bluegrass acquisition has contributed positively to the Company. At $1.625 billion, Bluegrass
was the second largest transaction in Martin Marietta�s history and demonstrates the successful execution of SOAR. The
Bluegrass acquisition added over two billion tons of reserves at 22 active quarry locations across Georgia, South Carolina,
Kentucky, and Tennessee and provided a leading market position in Maryland. Since 2010, the initial year of SOAR, we have
grown the number of markets in which we have a leading market position, defined as a #1 or #2 position, from 65 percent
of our markets to 90 percent.
As a reminder, we announced the Bluegrass acquisition in June 2017 � but that was only the start. It took until April
2018 to receive the requisite Department of Justice clearance allowing the acquisition to be finalized, in which we were
required to divest only two quarries. Successful integration of an acquisition this size is a challenge. Our teams spent the
nearly nine-month period from announcement to closing planning and preparing for integration. True to Martin Marietta�s
character, integration began with safety and ethics. Each new employee received in-person safety and ethics training with
a clear message that our Company expects strict adherence to these objectives. The result � we not only met the challenge
of successfully navigating the integration but also exceeded our own internal synergy expectations. Upon the transaction�s
announcement, Martin Marietta expected to realize $15 million of annual synergies within 12 months of the close date.
I am pleased to report that our teams have now identified more than $20 million of annual cost synergies. We are thrilled
with Bluegrass� performance thus far and look forward to the continued value it adds to our Company.
Martin Marietta�s story continues with the impressive performance of our Magnesia Specialties business. For the third year in a
row, this segment delivered record revenues and profitability, with a seven percent increase in total revenues and a ten percent
increase in gross profit.
For more details about our business and the 2018 financial performance, I encourage you to read the financial statements
contained in this report, visit our website and view our various filings with the SEC.
A complete understanding of our Company is accomplished through an appreciation of not only our annual financial
performance but our commitment to the future and sustainability. I encourage you to read our sustainability reports, which
we have issued since 2015. It is in these reports you will read about the scholarships we provide and the land we donate to
communities for parks or wildlife habitat. The 2018 report specifically highlights our award-winning North Marion, Kansas,
quarry reclamation project, an example of the Company�s environmental stewardship focus, and the $150 million contribution
we made to our qualified defined benefit plan, evidence of our commitment to the future well-being of our employees and
their families. At the heart of our character is our commitment to the future � the future of our communities, our employees
and the environment.
We have also embarked on refreshment of our Board of Directors over the last several years. Since 2016, we have added
four new and highly-qualified directors, including John J. Koraleski, former Chairman and CEO of Union Pacific Railroad,
Donald W. Slager, President and CEO of Republic Services, Smith W. Davis, Partner, Akin Gump Strauss Hauer & Feld LLP,
and Dorothy M. Ables, former Chief Administrative Officer of Spectra Energy Corp. We are delighted to have these new
directors, who together bring strong backgrounds and relevant experience with publicly-traded companies, governance and
diversity as they provide their insights into our Board mix. 


We continued to focus on shareholder engagement in 2018. Last year, in connection with our Annual Shareholders Meeting, our
Say On Pay vote was 78.9 percent, the first year it was below 93 percent. We reached out to our top 30 shareholders to discuss with
them the reasons behind their vote and how the Company could best work to address their concerns. The responses we received
indicated strong support for the fundamentals of our executive compensation program and our pay for performance philosophy and
results, but raised specific concerns relating to certain features of our Employment Protection Agreements and equity awards. Paying
particular attention to the items that our shareholders raised in their conversations with us, we responded by eliminating certain
identified features. Talking to you and listening to you is our hallmark � and one that you can count on to continue.
The Martin Marietta story is not complete without a
discussion of our outlook and what it means for our
shareholders, as we endeavor to enhance the value
of your investment over the long term. Among other
things, we seek to build shareholder value through
our commitment to operational excellence and
prudent use of the Company�s cash. To that effect,
our Board voted in August 2018 to increase the cash
dividend by nine percent, one of the largest increases
in our Company�s history, and we repurchased
521,000 shares of Martin Marietta stock for $100.4
million. Importantly, we still have capacity under
the 20 million share repurchase authorization that our Board approved in February 2015. We believe our ability to use our
strong balance sheet and borrowing capacity to pursue attractive opportunities that become available to us places us in a
far better position to be successful in obtaining them, which has served to create value for you.
Stated succinctly, the outlook for our Company is very positive. We see no indications of either a slowdown or markets we
serve being overbuilt. To the contrary, underlying market fundamentals, including employment gains, population growth, and
state fiscal health, most particularly in those geographic regions where we have a significant presence, remain strong, all of
which bodes well for a continuing expansion of construction activity. The recent slowdown in that activity, is, as discussed
above, just a pause not a stop. In fact, what we have seen, and will continue to see, is a further extension of the construction
cycle. Moreover, any progress from Washington regarding funding for infrastructure spending will only be incremental to our
present view. I would be remiss if I did not remind you that our comparable aggregate shipments have not risen significantly
from the 2010 trough of the last cycle. Thus, despite intermittent headwinds, our industry�s economic future is bright and
within that industry, we believe we are the best positioned company.
Is this a false sense of security? We don�t believe so. Our success is anchored on values and traits embedded in our collective
corporate character: a strict adherence to SOAR, a devotion to operating our facilities safely, a passion for operating with a
view toward improving the future and an expectation of continuous improvement in all that we do. This is what defines Martin
Marietta�s character.
I am grateful to my colleagues for their commitment to the character of Martin Marietta, and I thank our Board of Directors for fully
embracing this ethic. That is why when we, as a Company, face challenges, as we always will, we are confident of our ability to
successfully overcome them. On behalf of all of us at Martin Marietta, I thank you for your trust in us and your continuing support.
Respectfully yours,
C. Howard Nye, Chairman, President and Chief Executive Officer
February 25, 2019