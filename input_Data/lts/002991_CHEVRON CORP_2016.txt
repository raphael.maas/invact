Low commodity prices in 2016 reduced earnings across the
industry and reinforced the need for structural reform. With
Brent averaging $44 per barrel, Chevron reported a loss of
$497 million compared with earnings of $4.6 billion in 2015.
It was a year of transition, and Chevron took significant
actions to reduce costs and improve net cash flow to ensure
our competitiveness in any operating environment.
Our 2016 priorities were to finish projects under construction;
reduce capital spending; reduce operating expenses;
complete asset sales; and operate safely and reliably. We
made substantial progress (see graphic, Page 4) on these
priorities as we worked toward our goal of becoming cash
balanced and able to pay the dividend from free cash flow
in 2017.
The actions we took included thousands of initiatives across
the enterprise to prioritize investments, negotiate better rates
from vendors and suppliers, and improve drilling and other
efficiencies. All of these actions enabled us to increase our
annual per-share dividend payout for the 29th consecutive
year. Our total stockholder returns outpaced our major
competitors by a wide margin last year. In fact, were
No. 1 in total stockholder return relative to our peers for
any cumulative holding period going back 25 years.
Our year-end debt ratio was 24 percent.
Operationally, we met a number of milestones, including
startup at our Gorgon liquefied natural gas (LNG) project.
Gorgon is one of the worlds largest LNG projects, and we
expect it to be a significant revenue source for decades.
We also achieved startup at Alder in the U.K. North Sea. We
brought on line all three trains at the Chuandongbei Project
in China. We restarted the Angola LNG project. We achieved
natural gas production from the Bangka Field Development
Project in Indonesia. And finally, we ramped up our
Jack/St. Malo deepwater project in the U.S. Gulf of Mexico.
Our Upstream business, responsible for exploration and
production, reported worldwide net production of 2.6 million
barrels of oil-equivalent per day. Production increases
were offset by asset sales, normal field declines and
maintenance-related downtime. We added approximately
1 billion barrels of oil-equivalent (BBOE) of proved reserves
in 2016 before asset sales. These equate to approximately
108 percent of net oil-equivalent production for the year.
After asset sales, we replaced approximately 95 percent
of production. In our exploration program, we added
approximately 1.4 BBOE to our resource base.
Our Downstream and Chemicals business, which is
responsible for our refining, marketing and chemical
manufacturing, achieved its highest refinery utilization in
more than 10 years. We also progressed a number of strategic
initiatives. For example, we closed the sale of our Hawaii
refining and marketing business. We sanctioned the Salt
Lake refinerys alkylation retrofit project using technology
we developed to improve the efficiency and safety of this
process  technology that we will license to others. We also
progressed work on our $6 billion joint-venture U.S. Gulf
Coast Petrochemicals Project, which includes a world-scale
ethane cracker and polyethylene units, targeted for initial
production in 2017.
Midstream and Development secured additional LNG
sales commitments for delivery from Australia, despite the
challenging market environment. In addition, we made good
progress toward scheduled delivery of the fifth and sixth
LNG carriers in 2017, to complete the largest shipbuilding
program in our history.
Our 2016 health, environment and safety performance set
or matched record lows in many of our core safety metrics.
We continue to focus on eliminating high-consequence
personal and process safety incidents. We also continued
our social investments. In 2016, we invested $186 million
in global partnerships and programs, complementing our
investments in projects and local goods and services that
created jobs and generated revenues for the communities
where we operate. More details are available in the 2016
Corporate Responsibility Report.
Going forward, were committed to continuing to improve
free cash flow and returns by focusing on investments that
are economic with lower prices and by controlling our capital
program. We announced a 2017 capital and exploratory
budget of $19.8 billion  42 percent less than what we spent
in 2015 and approximately 12 percent lower than 2016 capital
and exploratory investments.
In 2017, we expect Wheatstone, our next major Australian
LNG project, to start up and the Mafumeira Sul project in
Angola to achieve first oil. In Kazakhstan, our 50 percentowned
affiliate, Tengizchevroil, will be working on a project
expected to increase its total production to approximately
1 million barrels of oil-equivalent per day. First oil is planned for
2022. In the Permian Basin, we will continue to focus on shale
and tight resource development, capitalizing on efficiencies
weve already demonstrated in our drilling program there.
I am optimistic about the future.
Our products are responsible for
the greatest advancements in living
standards in recorded history. Energy
demand remains strong, and we expect
the need for oil and natural gas to
continue to grow over the next 20-plus
years as the developing world reaches
for a better quality of life.
As we work to provide the energy the world needs, we will
continue to be guided by The Chevron Way (www.chevron.
com/thechevronway), our roadmap for enabling human
progress as the global energy company most admired for its
people, partnership and performance. In 2016, we refined
our business strategies to address the changing operating
environment. We are focused on improving returns on all our
assets. We maintained our workforce development programs
and university hiring to ensure we have the talented workforce
we need in order to deliver these strategies.
I am confident Chevron will remain competitive and at
the forefront of an industry that provides the reliable and
affordable energy necessary for global economic growth.
Thank you for your interest and investment in the company.
John S. Watson
Chairman of the Board and
Chief Executive Officer
February 23, 2017