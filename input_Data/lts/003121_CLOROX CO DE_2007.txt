                                                            Clorox Shareholders and
                                                                   Fellow Employees
When I introduced myself in last year's annual report, I indicated                        These results were achieved through the hard work and dedication of
there were a number of reasons I was particularly excited to join                         Clorox people throughout the business. These results are theirs,
The Clorox Company: namely, the strength of the company's                                 and I'm proud of their accomplishments.
brands, the quality of the people, its values-based culture and a
                                                                                          2008: Investing in Our Future
great board of directors. Today, I would add to that list the com-
                                                                                          As the chart on page 3 shows, over the past five years, Clorox
pany's focus on the consumer and customer, its tremendous cost
                                                                                          stock delivered a 66 percent increase in total shareholder return, in
discipline and impressive process sophistication. Clorox has lived
                                                                                          line with the S&P 500 index and ahead of our peers. As we con-
up to my high expectations and I couldn't be more pleased to be
                                                                                          cluded fiscal 2007, however, our stock performance did not sustain
leading this organization.
                                                                                          that trend, and total shareholder return increased only 4 percent for
                                                                                          the year. While many factors affect stock performance at any given
My first year at Clorox has been an exciting one, as I've immersed
                                                                                          time, the decline was impacted by the stock market's reaction to
myself in getting to know the business and people, understanding
                                                                                          the outlook we communicated for fiscal 2008. The market's reaction
the tremendous opportunities before us, updating our strategic plan
                                                                                          reflected ongoing concerns about such things as raw-material cost
and delivering our fiscal year 2007 financial commitments.
                                                                                          increases, inflationary pressures and investments we're making to
                                                                                          restructure a portion of our manufacturing network. So the year
2007: Another Strong Year
                                                                                          concluded with a question on investors' minds: Can Clorox continue
Clorox had another very good year in fiscal 2007.
                                                                                          to create significant value for shareholders?

> We grew sales by 4 percent to $4.8 billion. This was our sixth
                                                                                          I strongly believe the answer is yes, and we have the track record to
  consecutive year of strong top-line growth at the middle to upper
                                                                                          support this belief. We have successfully weathered unprecedented
  end of our annual target of 3 percent to 5 percent sales growth.
                                                                                          raw-material cost increases over the past two years. In response,
> We delivered our sixth consecutive year of cost savings greater                         we successfully raised prices on a substantial portion of our prod-
  than $100 million, generating $107 million in cost savings for the                      uct portfolio. While, in the short-term, this suppressed the volume
  year, helping offset higher year-over-year costs for energy-related                     of products we shipped, we believe maintaining higher margins on
  and agricultural-based raw materials, manufacturing and logistics.                      our products through our pricing actions will generate meaningful
                                                                                          future profit. Looking ahead, while raw-material costs are still high,
> We increased earnings by 12 percent, delivering diluted earnings
                                                                                          we anticipate that we will see them begin to drop in the latter half of
  per share from continuing operations of $3.23 in fiscal 2007
                                                                                          fiscal 2008. Importantly, we are confident in our prospects to grow
  compared with $2.89 in fiscal 2006.
                                                                                          the value of the company and we're making substantial investments
> Our financial condition remains very strong, with net cash                              to do so, which we believe will accelerate our ability to increase
  provided by operations of $709 million.                                                 value in the future.
> Strong free cash flow* of $562 million allowed us to repurchase
                                                                                          2008-2013: Heading True North
  2.4 million shares of the company's stock and pay down to target
                                                                                          In 2007, we also completed an in-depth review of our business,
  levels the debt issued to finance the fiscal 2005 reacquisition of
                                                                                          defined what winning means for Clorox, identified our highest value
  61 million shares of Clorox stock from a former shareholder.
                                                                                          opportunities and made corporate strategy choices. In May, we
                                                                                          announced our new Centennial Strategy, named for the company's
                                                                                          100-year anniversary in 2013.







Central to the Centennial Strategy is our definition of winning:                                           ronment of diversity and inclusion; building the capabilities of our
double-digit annual percentage growth in economic profit. We call                                          leaders and ensuring we have a strong bench of players on our
this goal our "true north." It's the single direction everyone at                                          winning team.
Clorox is heading. Simply put, economic profit is the profit a com-
pany makes over and above the cost of paying for the assets used                                           Win With Superior Capabilities in Desire, Decide and Delight
to generate those profits.                                                                                 We will strive to be the best at driving demand creation and building
                                                                                                           consumer lifetime loyalty through superior capabilities in what we
We've chosen this definition of winning because we believe it                                              call the 3Ds: desire, decide and delight. These are the three points
strongly correlates with shareholder value creation. Importantly, we                                       when a consumer faces a choice about our product.
can calculate and determine how to maximize economic profit
across retail channels, with individual retail customers and in spe-                                       > Desire is created by using consistent messages everywhere
cific countries. We can even calculate it for a single business unit,                                        and every time we communicate with consumers about why our
brand or product. This capability gives us tremendous insight into                                           products are the best choice to meet their needs. We want to
our greatest opportunities for value creation; accordingly, we will                                          ensure consumers are predisposed to buy our brands before they
                                                                                                             ever set foot in the store.
use economic profit to make portfolio choices and allocate
resources across the business. We believe economic profit growth
is the best way to create value for our shareholders.                                                      > Decide is about winning at the store shelf -- where most pur-
                                                                                                             chase decisions are made -- through superior packaging and
A deep understanding of economic profit also drives the four stra-                                           execution of what we call AMPS: assortment, merchandising,
tegic choices we've made:                                                                                    pricing and shelving. Getting these fundamentals right will drive
                                                                                                             growth for our retail partners and us.
Be a High-Performance Organization of Enthusiastic Owners
Our No. 1 strategy is about Clorox people.                                                                 > Delight is when consumers take our products home. If we delight
                                                                                                             them, and our products exceed expectations and perform better
Clorox has great people. There's nothing we can't do when we set                                             than competitors' products, we're on the path to lifetime loyalty.
our minds -- and hearts -- to it. And this strategy is all about doing
                                                                                                           See pages 6-13 for examples of our capabilities in the 3Ds.
everything we can to ensure people walk through the door each day
with passion, excitement and enthusiasm, feeling the kind of com-
                                                                                                           Accelerate Growth in and Beyond the Core
mitment to Clorox that a business owner has. We've done a good
                                                                                                           When it comes to market share, 63 of our brands are No. 1 or a
job at this over the years to be sure, but we're raising the bar even
                                                                                                           strong No. 2 in their categories and countries (see page 14). We
higher: We want everyone to perform at the top of their game and
                                                                                                           have an advantaged portfolio, participating in profitable categories
love doing it every day. Key to achieving this is making it easier to
                                                                                                           with strong share positions, and our advantaged portfolio allows us
make decisions quickly through greater role clarity; using economic
                                                                                                           to create economic value for our shareholders. The key to achieving
profit to allocate resources; engaging everyone on a rational and an
                                                                                                           growth is pursuing businesses in growing markets with good profit.
emotional level -- in other words, fostering a culture that ensures
                                                                                                           Ideally, these businesses should be in categories that are large enough
we have our heads and our hearts in the game; enhancing our envi-
                                                                                                                        



to be meaningful to Clorox and with competitive dynamics that allow           We Make Everyday Life Better, Every Day
                                                                              In June, we brought the company's worldwide leadership team
our superior capabilities in the 3Ds to give us a clear right to win.
                                                                              together to roll out our Centennial Strategy. At that meeting, we
                                                                              adopted this phrase as the company's mission statement: We make
To achieve our true north goal, we must accelerate growth both in
                                                                              everyday life better, every day.
our core businesses and beyond. And we believe there's tremen-
dous opportunity to do so. Superior execution with the 3Ds can
                                                                              When you think about it, this statement perfectly describes what
help us broaden the footprint of our existing brands. We will focus
                                                                              Clorox people do every day, either directly or indirectly. We make
on extending them into nearby categories where our well-known
                                                                              and market different products, for different uses, for different peo-
brand names carry weight; increasing the channels where we sell
                                                                              ple in different locations. But each one is designed to make
our brands, such as hospitals and health-care facilities; and
                                                                              everyday life better for consumers around the world. This is why
expanding the range of products we offer in the countries where we
                                                                              we're here. It's why we come to work each day.
do business, with an initial focus on the Americas, Australia and
New Zealand.
                                                                              Indeed, 2007 was a very exciting year to take the helm at Clorox.
                                                                              We delivered very good financial results. We defined true north and
Our financial strength and strong free cash flow give us the flexibility
                                                                              charted a course to get there. We adopted a clear and simple
to reshape our existing portfolio over time and invest in economically
                                                                              statement for why we exist. And, from my vantage point, 2008
attractive new businesses. As we expand, we will use economic
                                                                              looks to be even more exciting as we begin to fully implement our
profit to drive our portfolio choices in a disciplined way and ensure
                                                                              Centennial Strategy.
we leverage the capabilities that drive our competitive advantage.
Our focus will be to further capitalize on four "megatrends" that are
                                                                              I'm proud to be at Clorox and I have every confidence in the 7,800
developing around the world: health and wellness, convenience,
                                                                              Clorox people who are moving in unison toward our shared destina-
environmental sustainability and a more multicultural marketplace.
                                                                              tion, giving their best every day for each other, for the business and
                                                                              ultimately for you, our shareholders.
Relentlessly Drive Out Waste
Clorox has tremendous cost discipline and process sophistication.
                                                                              Sincerely,
We've become extremely effective at identifying ways to save
money and increase productivity. A solid pipeline of cost savings
coupled with our strong free cash flow and capital capacity provide
significant resources to invest in profitable growth and drive share-
holder returns. Economic profit has a role here, too, as we use that
                                                                              Donald R. Knauss
"lens" to evaluate our operations and identify areas that offer
                                                                              Chairman & Chief Executive Officer
opportunities for cost savings. Ongoing process discipline and pro-
                                                                              Sept. 1, 2007
cess improvement, particularly in our supply chain, are also key as
we continue to minimize the cost of processes that are not a source
of competitive advantage and deploy our resources to the highest-
value work.
