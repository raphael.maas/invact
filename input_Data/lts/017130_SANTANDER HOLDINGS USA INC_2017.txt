bank. They are also the hallmarks of a digital bank. Digital banking is simple. A digital bank uses data to personalise its service. And a digital bank  like any good business  treats those it serves, and those who work for it, fairly. 
I am confident that we can deliver this aspiration by applying the wisdom and experience we have built up over generations to innovate and reinvent banking, while maintaining the strengths that have made us successful. We can do so because 
 our growth, profitability and strength show our strategy is working 
 our scale, diversification and predictability give us strong foundations to innovate 
 our culture  our relentless focus on being a responsible bank  will strengthen peoples loyalty in our brand. 
Let me take each of those in turn. 
Our strategy is working 
Our success in 2017 shows that our way of doing business, and our focus on building loyalty, are creating a virtuous circle that delivers growth, profitability and strength. The Group again delivered very strong results for the year, ahead of plan on all our targets  growing underlying profit before taxes by 20% and earning the loyalty of a further 2 million customers. We have endeavoured to create value for our customers, by lending more to them (our loans book is up 2% compared to last year excluding Popular), and by improving our value proposition (customer funds are up 8%). 
Heres the detail. 
Growth: Last year, I said we would increaseour number of loyal customers by a further 1.8 million to 17 million, and invest in technologyto raise the number of digital customers to 25 million. We have achieved these targets, with loyal customer growth of 13% compared to last year to 17.3 million, and digital customer growth of 21%to 25.4 million. This had positive impacts on our revenues  in particular the net fees line grew by 14% to 11.6 billion. 
Growth 
Loyal Customers 
Digital Customers 
Customer revenues 
17.3 million (+13%) 
25.4 million (+21%) 
45,892 
million (+11%) 
Annual Report 2017 11 
Message from Ana Botin 
Profitability 
Cost to income ratio 
RoTE 
Strength 
Attributable Profit 
FL CET1 
We have strong foundations. We have scale. We are diversifed. Our businessis predictable 
Profitability: Santander is one of the most profitable banks in the world (10.4% RoTE) and, as I set out last year, we maintained a broadly stable cost to income ratio making Santander one ofthe most efficient banks in the world (47% cost to income ratio). This allows us to lend more to customers; increase dividends (11% cash dividend per share increase) and generate capital through organic growth (53 bps increase) all at the same time. 
Strength: I said that in 2017 our aim was to increase our earnings and dividend per share; and that we would do this while continuing to grow our capital towards our target of reaching more than 11% Fully Loaded Common Equity Tier 1 by 2018. How have we done? We have grown our attributable profit per share by 1% compared to last year, and we have increased our FL CET1 by 29 basis points to 10.84%, on track to achieve our 2018 target. 
As a result of our growth, profitability and strength, we were able to acquire Banco Popular when it ran into difficulties. We executed the transaction without government assistance, acting fairly with respect to Populars teamsand customers and in the best interests of our shareholders. I would like to congratulate our team who worked tirelessly to put in place the7 billion capital increase to support the deal within weeks, and thank our shareholders for their confidence in us. 
It was followed by the largest sale of real estate assets in Spanish history and a responsible approach to dealing with depositors, with the aim of ensuring their business remained within the Santander Group. 
This has obviously been a highlight of the year, but it should not overshadow what has happened elsewhere. We will expand in Poland after reaching an agreement to buy part of Deutsche Banks business. In the United Kingdom, as our business prepares for the challenge of Brexit,we have seen sustained strong performance. In 
Brazil, despite political and economic uncertainty we grew our revenues by 18% compared to last year, the strongest growth among all banks, while narrowing the gap in profitability compared to our peers. In the United States, 2017 was a pivotal year for Santander US with significant progress on regulatory and business issues: we passed Federal Reserves qualitative capital stress test, made the first dividend payment to Group since 2011, and grew underlying profit by 5%. 
Strong foundations to innovate 
This performance matters as we implement our plans for digital growth, as behind these dry statistics lie some key points. 
First, we have scale  and scale gives us insight. We dont simply have 133 million customers:we know those customers  and in the case of our 17 million loyal customers, we know them very well. In my mind, its much better to have 
a deep understanding of many customers in10 core markets, than it is to have a shallow understanding of the same number of customers in dozens of markets. In the digital age, that depth of insight combined with our scale is invaluable, and those relationships are priceless. 
Second, we are diversified. Our business is mostly focused on retail banking, serving a diverse range of customers, and is balanced between developed economies and emerging economies and between Europe and the Americas, where growth is expected to be strong in 2018. The US will see its ninth yearof economic expansion  the longest upward cycle since 1850  which will help propel growth elsewhere. In the Eurozone, we expect to see strong growth of 2.3%. Looking at our major markets, Spain is forecast to see its fourth consecutive year of growth of around 3%; while the UK's economic growth, although expected to be subdued due to uncertainty caused by Brexit, is forecast to be 1.4%. Meanwhile the Brazilian economy, having returned to growth in 2017, is expected to grow by 3.2%, supported 
12 
Annual Report 2017 
47.4% 
(-70 bps) 
10.4% 
(+3 bps) 
6,619 
million (+7%) 
10.84% 
(+29 bps) 
We aim to redefne banking in a way that serves the distinct needs of all our customers, through common, efficient and flexible global platforms 
by historically low interest rates: business confidence is at the highest levels of the last years. And in Mexico, while the economy isstill overcoming the uncertainty regarding the renegotiation of NAFTA and the impact of the September earthquake, it is expected to benefit from structural reforms. 
Our leading position in 10 core markets, with an aggregate population of a billion people, gives us stability and presents us with new opportunities. In these markets, digital technology is making 
it easier to win new customers  especially the 160 million people in Latin America who are unbanked. And thanks to our global network, I see great potential in developing relationships to serve our customers better along natural corridors of economic opportunity  such as between Brazil and Argentina, or the United States and Mexico. 
Third, our business delivers predictable results throughout the economic cycle. This is because we are diversified, and thanks to our leading market shares in nine of our 10 markets, which allows us to sustain consistent and predictable results. Compared with our peers, for the last twenty years Santander has had the lowest volatility in earnings. This enables us to pay dividends, grow our business, invest in new technology, and as in last years to organically generate capital. 
And this brings me to our approach to innovation. Our sector, like all sectors, faces the challenge of the digital revolution. We aim to redefine banking in a way that serves the distinct needs of all our customers, through common, efficient and flexible global platforms, which can support our local businesses. Our customers want services that are frictionless anytime, anyhow and anywhere. To provide that we need platforms that are resilient and flexible. 
To achieve this, we describe our approach to innovation as reliable "supertankers and agile "speedboats. The supertankers are our 
established businesses, carrying the bulk of our revenues and our growth. The speedboats are our new opportunities, with the flexibility to race ahead of our competitors. Each shares their experiences and all perform better as a result. 
In 2017 we showed what this means in practice. In Spain, we relaunched Openbank with its own, new IT structure and new team. It now serves1 million customers, offering a full range of services  from stocks to mortgages, but only has one branch and just 70 fulltime employees, some of whom have never worked for a bank before. In Brazil, SuperDigital  which allows customers to carry out transactions on their phones without the need for a bank account  is growing at around half a million customers a year. 
Meanwhile, weve been investing and innovating in new payment systems. Santander is likelyto be one of the first global banks to roll outin the next few months our distributed ledger technology to retail customers across Europe and the Americas, bringing real benefits by reducing the speed of international payments from 2-4 days to end of day/overnight. We have completed live pilots in the UK and Spain, with over 1,500 payments made for a value of 2 million. Leveraging this technology and other state of the art solutions, Santander Pay aims 
to become the definitive crossborder payment solution for our customers worldwide. 
As the digital world of financial services continues to grow, much of todays regulation remains rooted in the analogue age. Many insurgent market entrants do not always face the same rigorous regulation as banks like Santander. Over time, the regulation of these businesses must and, I believe, will develop, guaranteeinga level playing field promoting innovation for banks as well as for new entrants. I am certain that reliable, responsible businesses with loyal customers, led by Santander, will emerge as the winners. 
Shareholders 
Cash Dividend per share 
Tangible Net Asset Value per share 
Attributable Profit per share 
Total Shareholders return 2017 
0.19/ 
share 
(+11%) 
4.15/ share 
(+6% excluding exchange rate impact) 
0.40/ 
share 
(+1%) 
16.6% 
(vs. 12% European banks) 
Annual Report 2017 13 
Message from Ana Botin 
Responsible bank 
More than 250,000 microentrepreneurs supported 
Financial inclusion programmes to more than 300,000 people 
129 million invested in education in 2017 
Putting our purpose at the heart of our business is critical if we are to be a truly responsible bank. Our actions need to match our words 
A responsible bank 
This brings me to the theme of responsibility. There are many hallmarks of a responsible bank, but here I would like to single out a few: a strong team that lives by clear values and behaviours; good governance; and a strong sense of purpose that drives the business. 
Let me start with the team. To thrive in todays world, we obviously need to attract and retain the best talent. But we also need to attract a diverse team (women now make up 36% of our board), so that we are better able to understand and serve our customers. If we are to build this team, we need offer people great opportunities. But today, people want more than that. As I mentioned above, people want to work for a company that lives by its values, has a strong sense of purpose, and gives them the chance to make transformational change that benefits millions of people. 
Building this team will enable us to implement change at pace  which requires us to work with agility and focus. This year, weve shown how we can work better, together, across all markets where we operate. Example of this active collaboration are the four speedboats launched, which are global businesses with key executives from various countries. 
But there is more to do. A strategic target for us this year is to focus on developing our businesses by fostering greater collaboration. Each of our businesses has local management and is locally responsive, but 
I want to see them extract full value from being part of a group, so we continue to create the best possible products for our customers. And, wherever our businesses operate, we want to ensure that everything they do is Simple, Personal and Fair. 
To turn these three words into reality, we are embedding our common culture in the day to day of all our teams, encouraging them to live by eight key behaviours. Starting in 2017, under our newly created performance management system, MyContribution, 40% of variable remuneration is linked to howwell employees live our behaviours. This applies to our leadership as well. Across all markets, we have undertaken a series of initiatives (such as KISS  Keep It Simple Santander in the UK) to change processes 
so they are Simple, Personal and Fair. Alongside this, we have also made changes to our governance, to ensure that we have easy to understand policies and procedures, and clear lines of accountability 
so everyone knows who is responsible for what; transparent processes, so we, our regulators and, where appropriate, the public can see how decisions are made; and clear metrics, so we can assess our performance. 
I also want to take the opportunity again to thank Matias Rodriguez Inciarte and Isabel Tocino Biscarolasaga for their great contribution in our Board. Matias, in particular, has been a key senior executive, a board member for more than three decades and an important part of Santander's history. And I would like to welcome Ramiro Mato Garcia-Ansorena, who 
I am certain will add much value thanks to his broad financial and international banking experience. 
Above all, we need to be sure that we are managing risks in a prudent, responsible way. This year, for instance, we upgraded all of our credit risk models across the group to reinforce the sustainability of our business, and we have increased our investment in Cyber Security to stay at the forefront of technological advances in the field. We have also dramatically reduced our exposure to real estate in Spain, which will reach nonmaterial levels by the end of 2018. 
More than that, though, I want to ensure that we are fulfilling our purpose  to help people and businesses prosper. Putting that purpose at the heart of our business is critical if we are to be a truly responsible bank. Our actions need to match our words. For example, I was immensely proud of how our teams in Puerto Rico and Mexico responded to the crises caused by both Hurricane Maria and the earthquake, donating time and money to help those in need. Our scale and strength gives us the ability not just to help in these circumstances, but to support inclusive and sustainable growth wherever we operate. In doing so, we need to tackle three major challenges. 
The first challenge: Two billon people still have no access to the financial system. But once they havea mobile phone and can get online, they can gain access to a bank. They dont need to travel to deposit money or take out a loan or get insurance. The bank is there in their hands. This is empowering millions of 
14 
Annual Report 2017 
The challenge we face is nothing less than the reinvention of banking. Our results, our targets, and above all our approach, show that Santander is rising to that challenge  and winning 
people especially women. Between 2011 and 2014, 700 million people became account holders at banks or other financial institutions for the first time, reducing the number of unbanked adults by 20 percent. As 
I mentioned above, our services and products are already bringing the unbanked into the financial system, enabling them to share the benefits of growth. In 2017, we supported more than 250,000 entrepreneurs with microcredits, mainly in Brazil, and our financial inclusion programmes have reached more than 300,000 people. Our aim is to up the pace, and do even more. 
The second challenge: 600 million jobs need to be created over the next 15 years to match the growthin the global workforce. Many of these jobs will be created by small businesses, which are the engine of economic growth. And those jobs will largely go to people with skills needed in the digital age. To help these businesses grow, we will use technology to provide a personalised service, giving them online advice and helping them to export to foreign markets. Weve created a global online network so we can put a small business in Warsaw or Oporto in touch with potential customers in Sao Paulo or Mexico City. 
And these new businesses need to be sustainable businesses  which is the third challenge: the need for sustainable growth. Banks need to help businesses act in a socially responsible way. That means a number of things, such as supporting businesses as they cut carbon emissions and make the transition to the green economy; financing innovation in green technology; encouraging businesses to operate in a way that supports local communities, respect human rights and encourages inclusive growth. Thanks to Santanders footprint and scale, we are in a good position to support businesses that do this. 
By supporting inclusive and sustainable growthin this way, we shall do even more to help people and businesses prosper. And our efforts will be supported by the unique multinational network of 1,300 universities that Santander has created over several decades. Santander is the largest corporate contributor to education in the world, investing 129 million euros in 2017 alone. Our task now is to work with this network  the research, innovation and skill of these universities  to create a formidable partnership to tackle these global challenges. 
Looking ahead 
As I look to the future, there will be many challenges to address: the supervisory and regulatory regime, especially in Europe and in the UK; emerging risks related to the normalization of interest rates, exchange rate headwinds 
from a strong euro; and continuing geopolitical volatility. However, my team and I look ahead with confidence. Strong growth, digital innovation, meeting the global challenges  all this is why I believe Santanders best days lie ahead. As the world changes, so will we. 
We will provide more details on how we are transforming ourselves to be prepared for future challenges at our Investor Day in October 2018. For this year, I would like to remind you of our strategic targets: 
Growth: We aim to have 18.6 million loyal customers (an 8% increase) and 30 million digital customers (an 18% increase). 
Profitability: We are targeting a RoTE of more than 11.5%. 
Strength: We are aiming for FL CET1 above 11%. 
And for our shareholders, we reiterate our targets of reaching double digit growth in earnings per share and growing our cash dividend per share in 2018. 
So let me end where I began. We are living through an economic revolution. The challenge we face is nothing less than the reinvention of banking. Our results,our targets, and above all our approach, show that Santander is rising to that challenge  and winning. 
Your continued support is key, and I would like to thank you for your trust. 
Ana Botin 
Executive Chairman 
