D e a r S t o ck h o lde r s , Ass o ci a t e s , B us i n e ss P a r tn e r s a nd F r i e nds :
Liz Claiborne Inc. has long been known for its many strengthsa unique brand portfolio, superb
talent, a commitment to leadership, top global sourcing capabilities and, of course, a consistent
return to shareholders.
Its been said, however, that every successful company should reassess what drives its results and find
new ways of superchargingthat is, reinventingthose drivers. As a company, the time is right
for us to do so, and in 2006, we began this process.
There was much to be proud of in the past year. Juicy Couture and Lucky Brand Jeans saw tremendous
growth, and our core Liz Claiborne brand gained momentum in the midst of its important turnaround.
Nevertheless, we are not satisfied with our performance. Our earnings per share did not make the
progress both we and our shareholders have come to know and expect.
The dynamics of our industry continued to place external pressures on our business, and we began
the year anticipating the impact of the integration of May Department Stores into the Federated
organization, the largest consolidation in the history of our wholesale business.
In response to this and other trends, we started to reshape and reinvest our cost base. We reduced
approximately four percent of our global workforce, with significant reductions at senior levels of
the organization, rationalized distribution facilities and closed or repurposed underperforming retail
concepts.While difficult, these decisions and others to come will make it possible for us to reinvent
our strengths to drive growth. In 2006 alone, total cost savings equaled approximately $31 million,
which was reinvested back into our brands.
So, as we enter 2007, we are a company in transitionstrong today, but prepared to make the
changes needed to be even stronger. Let me share with you our path forward.
Investing In Our Brands
In 2006 we invested in our brands and our core business operations.
We opened 45 specialty retail stores globally for brands including Juicy Couture, Lucky Brand Jeans
and Mexx. This brings us to a record of 399 specialty stores across numerous formats, and we
expect this pace of store openings to continue for the foreseeable future, with a goal of opening
100125 specialty retail stores around the world in 2007.
International sales grew during the year and represented 28% of our total revenues.
Turning to our namesake brand, we are seeing results from strong design and merchandising, plus
increased marketing, including the Liz Is campaign, which was inspired to reflect the authenticity
of the brand and the people who wear it, while giving Liz Claiborne new energy and a fresh look.
We plan to continue to invest inand executedesign, product and marketing improvement
initiatives for this and other brands in the future. 
At the end of 2006, our acquisition of Kate Spade took us further into the lucrative and growing
accessible luxury category. A lifestyle brand with positive brand equity and high consumer regard,
we will concentrate on growing its signature handbag and accessories collections while planning
further line extensions for the future. The brand will also take significant steps forward into the
specialty retail arena, increasing its store base, while continuing to support the wholesale customers
Kate Spade has always served so well.
The Three Ms
Multi-brand, multi-channel, multi-geographyour company has evolved dramatically over the
past decade. From a strong, single brand, Liz Claiborne Inc. has become a strategic acquirer with
many brands addressing different lifestyles and price points. In the past five years, seeking to identify
new sources of growth and leverage our brand management skills, we have greatly enhanced
our focus on specialty retail and overseas markets. Looking forward, we aim to grow even more
significantly through global expansion, and to control our brands and product through owned-retail
and direct-to-consumer distribution.
While we will continue to pursue a proven multi-brand approach, even the best strategy can be
refined. Currently, we intend to compete in the consumer and product segments that offer the
most attractive and consistent financial prospects with even stronger, enduring brand propositions.
At the heart of this reinvestment and reinvention will be the concept of Power BrandsJuicy
Couture, Kate Spade, Liz Claiborne, Lucky Brand Jeans and Mexx. For each of these multi-channel,
multi-category, multi-geography brands, we will be developing and implementing accelerated
growth plans, ranging from specialty store expansion to enhanced marketing initiatives to more
widely distributed national advertising campaigns.
In addition, we will seek to acquire companies that will deliver capabilities and/or platforms that
we can leverage across multiple businesses. To be certain, acquisitions will remain important, but we
will also rely on organic growthbringing scale to proven concepts within our portfolio, building
our power brands and developing new ones through an incubator approach.
In the meantime, we are continuing the re-examination necessary to accelerate the development of
our revised and refined strategy. The intent is to announce our plans and goals for the next several
years this July.
The First 100 Days: Reinventing Strengths
As mentioned earlier, we have begun reassessing what drives our results to find ways to reinvent
those strengths, and in my first 100 days as the new Chief Executive Officer of Liz Claiborne Inc.,
my team and I have worked closely to do so.
During this time, we traveled, we talked to people and we listened.
When we met with designers, we heard heartfelt concerns about the need to refocus on our products
and encourage creativity. When we met with our manufacturing and sourcing folks around the
world, we learned about new ways to speed designs to market and better coordinate our efforts with
distribution and logistics strategies.
We also took action:
Chief Creative Officer,Tim GunnFormer Chair of the Department of Fashion Design at
Parsons The New School for Design in New York City and a regular on Bravos Project Runway,
Tim will play a key role in restoring a culture of creativity in which product comes first. As CCO,
he will lead our effort to attract and retain the best design and merchandising talent. And while he
will not act as a designer,Tim will focus closely on improving our design processes.
Chief Operating Officer, Mike ScarpaLong-time Chief Financial Officer of our company, Mike
will now expand his role beyond corporate finance, distribution and logistics, adding responsibility
for manufacturing, sourcing and technology, and consolidating all responsibility for product
movement under his experienced eye.
And, ultimately, we set a courseone born from our mission of serving the consumer globally, in
multiple channels, across multiple brands. This management team came out of its first 100 days with
our attention clearly focused on four drivers that have historically built advantage for our company.
Irresistible Product
Our product goal is simple: To tempt and excite consumers with something they dont needbut
must have. Our merchandise must be more than just appealing. It has to stand out in a crowd. And to
do so, our culture, systems, processes and management must nurture the greatness of our creative design
and merchant community who literally reinvent our product lines each and every season. To deliver
product that improves nearly every financial metric and simultaneously makes our brands strong, we
must harness the talent we have across our companythat is a top priority at Liz Claiborne. And we
believe that in his new role, Tim Gunn will make a rapid difference on this front.
Building Buzz and Brand Loyalty
Brand image is more than the quality or desirability of the product itself. Brand image comes from
who is wearing the brand, what people are saying about it, where it is sold and how it is merchandised.
And in todays world of instant communication, this is more true than ever before.
To build the right kind of buzz for our brands, we are revitalizing our marketing function
strengthening traditional resources and developing new approaches to influencing consumers and
thought leaders. There are huge opportunities to breathe new energy into several of the brands in
our fantastic portfolio and we are eager to take advantage of them.
Supply Chain Solutions
At Liz Claiborne, we have long benefited from a superior supply chain, but as in all aspects of our
business, we must challenge ourselves to improve and when possible reinvent strengths.
As we adapt to operating globally, we need flexibility, speed and efficiency to best serve the variety
of business models we operate around the world. To accomplish this, our supply chain must enable,
and even encourage, design innovation regardless of the consumer segment we are addressing. Be it
contemporary, traditional, mid-tier or designer, we must adhere to the mantra of putting the product
back into production.
Our supply chain vision, in sum, is clear and focusedcreate a best-in-class system that will speed
irresistible product to market. Activities and processes that help reduce cycle time or foster innovation
for one brand may be different than what works or is appropriate for another. Accordingly, across
all of our businesses, we will look carefully for opportunities to apply and take advantage of supply
chain enhancements on a global basis, and we believe we can do so without sacrificing scale advantage
or cost efficiencies.
Finally, to ensure we bring both an operational and product perspective to our efforts, Tim Gunn,
Mike Scarpa and Trudy Sullivan, the President of Liz Claiborne Inc., are jointly leading our supply
chain strategy review, and we expect to identify four to five high-impact initiatives to implement in
2007 and beyond.
A Focus on Talent
While we have a long track record as one of the industrys great developers of teams and talent,
today we face an unprecedented need for creativity. Brilliant designers, strong leaders, cross-functional
thinkers, globally-oriented team playersthese individuals and more have critical roles to play as
we reinvent the strengths of Liz Claiborne Inc. As a result, an integral part of our plan to accelerate
growth is to strengthen our recruitment and development programs.
The Year Ahead
Without question, 2007 promises to be an important year for the company, a transition year. We will
continue our strategy of re-examination. We will make decisions to improve our competitiveness.
Above all, we will reinvent our strengths, day-in and day-out, to drive growth in the coming years.
As always, we appreciate your support and, we look forward to talking to you about Liz Claiborne
Inc.s very bright future.
William L. McComb
Chief Executive Officer