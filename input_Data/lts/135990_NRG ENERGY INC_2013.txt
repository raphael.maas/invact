Occasionally I make reference to the Four companies that will
inherit the earth, by which I mean Amazon, Apple, Facebook and
Google. While there is a hint of facetiousness in my designation  who
can predict the future in the ever-changing business world  I do have
infinite respect for them, their innovativeness, the quality of the
service that each provides and, most of all, the comfortable ubiquity
they have achieved in the hearts and minds and everyday lives of
the vast majority of people, both at home and abroad.
What do these four very successful, but very distinct, companies
have in common? They all provide products or services, directly to
the consumer, which are deemed essential to the enrichment of
their life experience, day in and day out. What causes these four
companies to rise well above others that have similar offerings?
The Big Four offer their own product or service in a manner that
is more comprehensive, seamless, intuitive and, in the case of
Apple, visually elegant, than their respective competitors. They
enable, they connect, they relate, they empower.
There is no Amazon, Apple, Facebook or Google in the American
energy industry today.
There is no energy company that relates to the American energy
consumer by offering a comprehensive or seamless solution to
the individuals energy needs.
There is no energy company that connects the consumer with
their own energy generating potential.
There is no energy company that enables the consumer to make
their own energy choices.

There is no energy company that empowers the
individual wherever they are, whatever they are
doing, for however long they are doing it.
And there is no energy company that the
consumer can partner with to combat global
warming without compromising the prosperous
plugged-in modern lifestyle that we all aspire
to  not just for us who are so blessed to live a
prosperous life in the United States, but for the
billions of people who live in the developing world
and aspire to what we already have.
NRG is not that energy company either, but we
are doing everything in our power to head in that
direction...as fast as we can. But we need to pick
up the pace further, and that is what we intend to
do. The good news is that we have been working
hard over the past few years to put the pieces in
place and, therefore, arent starting from scratch.
Preparing to win the short, medium and long term
So far in this letter, I have told you why we want
to transform our company, our industry and our
society. Now I am going to give you a glimpse of
how we intend to do it, before ending with my
vision of the outcome we hope to achieve. This
is a bit of a broader and longer vision than you are
used to, but as the owners of the company, you
deserve to know where we are headed.

Just a few years ago the prevailing wisdom was that the path to a
clean energy economy depended on our collective willingness to
build a nationwide, high voltage transmission system in order to
transport electricity in vast quantities from the relentlessly windy
and brutally sunny parts of the country, where people generally
dont live, to the more moderate places where Americans tend to
congregate. The folly of that idea thankfully was realized before
anyone actually began to build such an expensive and pointless
white elephant. Now we are headed for the same goal BUT in
the opposite direction: down the path towards a distributed
generation-centric, clean energy future featuring individual choice
and the empowerment of the American energy consumer.
The only real question is how quickly will this future occur?
Even if you believe, as we do, that this future world will be upon us
with lightning-like quickness in electricity industry terms, in real
time the distributed future is going to take a while to get here. In
the meantime, the lights need to be kept on, homes heated and
chilled and appliances powered up. So at NRG we are positioning
ourselves to succeed during a prolonged period through which the
traditional centralized grid-based power system co-exists with the
fast-emerging high-growth distributed generation sector  much
like fixed-line telephony has co-existed with the wireless world for
a couple of decades.
So we are in the process of reorganizing ourselves from the
customers perspective. Our conventional generation business will
be not only focused on maintaining our existing 50,000 MW fleet
in top operating condition, but also on repowering select plants
with flexible fast start units located in advantageous positions on
the grid. Furthermore, in response to the increasing realization in
the business community that no serious industry or commercial
enterprise can prudently run their business based on 100%
dependence on the grid, we see a growing B2B opportunity for our
wholesale business in on-site generation for industry and largescale
commercial customers. The cost to our business customer of
maintaining localized generation will be defrayed by our ability to sell
excess capacity and generation, on behalf of that customer, into the
traditional grid.
As lack of confidence in the grid coincides with the introduction of
new technologies, businesses and homeowners will realize that
there is a better way. And, for them, that means generating most
of the electricity they consume on the premises, from their own
resources. In this new reality, our mass retail electricity franchise,
consisting of Reliant, Green Mountain and NRG itself, becomes
ever more important. Our retail focus is on ensuring that we remain
a first mover in bringing technological innovation aimed at the
home energy consumer to our customers, on terms that they find
attractive. Our marketing relationship with Nest, and their awardwinning
learning thermostat, is a case in point. But it is only the
beginning. We expect to be soon-to-market with a robust platform
offering rooftop solar to homes and businesses and other forms of
sustainable and clean generation that will offer our customers the
ability to dramatically reduce their dependence on system power
from the centralized grid.
And for the customer, business or individual, who simply wants
nothing to do with the grid, the centralized control it represents and
the inhibition of individual choice and restriction of personal freedom
that is implicit in being intertied to the grid, there is the post-grid
future  a future that is driven by renewables, incorporating both
energy storage and sophisticated localized automation to balance
production and load. There will be systems that harness thermal and
electric synergies and across not only clean energy, but also fresh
water production, waste disposal and electrified transportation
to create a virtuous circle of civil sustainability. We are just getting
started in this area with, among other projects, our ground-breaking
Necker Island announcement, but we expect to be a leader in the
area of renewables-driven ecosystems. Our goal is to achieve the
quality and ubiquity of energy outcomes on behalf of our customers
that would one day cause us to be mentioned in the same breath
as the four companies who will inherit the earth. And in turn, our
customers would have the same kind of experience with us that
they have with the four of them and we can emulate the shareholder
value creation of four companies which, at present, have an
aggregate market capitalization of over $1 trillion.
They will hold us accountable
As we forge ahead, I am mindful of the fact that the next generation
of Americans  the generation around my soon-to-be adult
children  is different from you and I. Somehow, some way the next
generation of Americans became all in in their commitment to
sustainability, in every sense of the word including clean energy. With
them, it is built into their DNA, not just learned behavior as it is for us.

And make no mistake about our children. They will hold all of us
accountable  true believers and climate deniers alike. The day is
coming when our children sit us down in our dotage, look us straight
in the eye, with an acute sense of betrayal and disappointment in
theirs, and whisper to us, You knew... and you didnt do anything
about it. Why? And for a long time, our string of excuses has run
something like this: We didnt have the technology...it would have
been ruinously expensive...the government didnt make us do it...
But now we have the technology  actually, the suite of technologies
 and they are safe, reliable and affordable as well as sustainable.
They do not represent a compromise to our ability to enjoy a modern
lifestyle. They represent an opportunity for us to do the right thing
while multiplying shareholder value through greater value addedservices.
And these technological solutions are focused on the
individual consumer  both businesses and individuals  so the
shameful passivity and failure to act of government is irrelevant.
The time for action is now; we have run out of time for more excuses.
You should know that I get up each day animated and motivated to
lead NRG into a transformational role in the clean energy economy
by my intense desire to have a better answer to that question when it
comes from our children, whether it comes from your children or mine:
At NRG, we did all that we could, as fast as we could
do it, and what we accomplished with our partners and
customers turned out to be quite a lot. Enough, in fact,
for you and your generation to finish the job...
That would be a much better answer.
So lets make it happen.
David Crane
President and Chief Executive Officer
March 15, 2014