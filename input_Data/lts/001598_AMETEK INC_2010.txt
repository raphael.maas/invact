Letter to Shareholders
an outstanding year
2010 was an outstanding year. We benefitted from
our strong business portfolio, proven operational
capabilities and a continued focus on our Four
Growth Strategies. We set full-year records for
operating income, operating margins, net income
and diluted earnings per share. We invested more
than half a billion dollars in acquisitions and, in
the process, acquired some outstanding new
businesses.
We had 2010 sales of $2.5 billion, up 18% from 2009.
Operating income was $482 million, compared with
$366 million in 2009, up 32%. We had an operating
profit margin of 19.5%, up 210 basis points from
2009. Our net income was $284 million, or $1.76 per
diluted share, compared with $206 million, or $1.27
per diluted share, in 2009. Operating cash flow was
superb at $423 million, up 16% from 2009. All per
share amounts reflect a 3-for-2 stock split paid to
shareholders on December 21, 2010.
Outlook for 2011
Our outlook for 2011 is especially positive. We
enter 2011 with a record backlog of orders. We
expect our businesses to continue to perform well
and show solid growth in 2011 with our longercycle oil and gas, power and aerospace businesses
showing particular strength. We also expect to
benefit from the full-year contribution of our 2010
acquisitions.
Positioned for Growth
We remain committed to achieving our vision of
doubling the size and profitability of AMETEK over
the next five years.
Our vision is firmly set on the foundation of our
Four Growth Strategies: Operational Excellence,
Strategic Acquisitions, Global & Market Expansion,
and New Products. Those strategies are implemented by a proven management team, utilizing a
disciplined business approach and adhering to a
strong set of core corporate values.
Going forward, we will continue to focus on
acquiring differentiated businesses, pursuing
opportunities in new and emerging technologies,
and establishing new growth platforms for the
Company.
Operational Excellence
Operational Excellence is the cornerstone strategy of our Corporate Growth Plan and a significant
contributor to our success. It was an essential
factor in our ability to leverage our business model
and bring our strong sales growth to the bottom
line in 2010. With a focus on cost reduction and
asset management, Operational Excellence was a
key driver in our operating profit margin reaching
a record 19.5% in 2010.
Operational Excellence is key to our achieving
improved operating efficiencies and superior
asset management results. It helped us establish
a lean manufacturing platform for our cost-driven businesses and allowed us to smoothly expand
low-cost manufacturing at our plants in China,
the Czech Republic and Mexico. Sales from those
plants totaled $330 million in 2010, up nearly
22% from 2009.
We have adapted lean manufacturing techniques
and applied them to our back office operations
and new product development efforts. We also
continue to drive lower costs through our Global
Sourcing and Strategic Procurement Initiatives,
which generated $27 million in savings in 2010.
Strategic Acquisitions
We have a focused acquisition strategy, a thorough due diligence process, and a proven ability
to quickly integrate newly acquired businesses. We
had a tremendous year for acquisitions in 2010.
We deployed nearly $540 million in capital on
acquisitions, a record level for us, and acquired
businesses with more than $220 million in annual
revenue. These highly differentiated businesses
expand our market opportunities and technology
base in the areas of precision motion control,
materials testing, electrical interconnects,
programmable power and high-end analytical
instrumentation.
Our 2010 acquisitions include:
Sterling Ultra Precision, a reseller of machine
tools and proprietary software used in the niche
ophthalmic lens market
Imago Scientific Instruments, a pioneer in leadingedge 3D atom probe technology used by leading
research facilities engaged in nanotechnology and
advanced materials science
Technical Services for Electronics, a leader in
highly engineered interconnects for the medical
device industry
Haydon Enterprises, a leader in high-precision
motion control products for the medical, industrial equipment, aerospace, computer peripheral
and semiconductor industries
AMREL Power, a provider of highly differentiated
programmable power products for the automated test equipment market
Atlas Material Testing Technology, the global
leader in weathering test instruments and related
testing and consulting services, with a network of
outdoor and laboratory testing facilities around
the world
Acquisitions will remain a focus for us, as we
see this strategy as a key driver in the creation
of shareholder value. Going into 2011, our
acquisition pipeline is strong. We have both
the financial resources and the managerial capabilities to acquire additional businesses. We
believe the current economy provides us with a
wealth of promising opportunities to further build
our differentiated business portfolio through
acquisition.
Global & Market Expansion
We are a global company. Less than a decade
ago, international sales accounted for only a
third of our total sales. Today, nearly half of our
sales are outside the United States, and most
of our businesses are global. We enjoy a world
of opportunities and intend in the years ahead
to capture an even greater share of sales from
overseas markets.
In 2010, international sales totaled more than
$1.2 billion, up 17% from 2009. Asia led the way
with a 31% sales increase year over year, driven
by strong performance by our precision motion
control, electrical interconnects and high-end
analytical instruments businesses.
Worldwide we operate more than 40 manufacturing plants and 80 sales and service centers in
nearly 40 countries outside the United States, and
have greatly expanded our sales and marketing
presence in Europe, Asia and the Middle East.
We are particularly focused on the BRIC countries (Brazil, Russia, India and China). We have
significantly increased our presence in those
countries and plan to further ramp up our operations in 2011. Sales to the BRIC countries totaled
nearly $250 million in 2010, up 24%, reflecting
the strength of the economies in those countries
as well as the success of our increased sales and
marketing efforts.
New Products
New products are essential to our long-term
growth and as a result, we have consistently maintained our investment in new product development throughout the global economic slowdown.
We spent $112 million on research, development
and engineering activities in 2010, up 11% from
2009.
Sales of products introduced over the past
three years accounted for nearly 20% of our total
revenues in 2010, reflecting the excellent work of
our businesses in developing the right products
for our customers, as well as the pace and quality
of our research and development efforts.
In 2010, we added a wide range of innovative
products to our highly differentiated product
portfolio. Among them are advanced instruments
for ultraprecise manufacturing, multifunctional
instruments for equipment and materials testing,
advanced micro- and surface analysis systems,
high-performance technical motors and motion
control products, and ultrasensitive elemental and
trace metal analyzers.
A Strong Set of Values
Fundamental to our success are certain disciplines and a set of core values that underlie our
corporate culture. Embedded within our Four
Growth Strategies is a disciplined management
approach that allows us to continuously improve
our operating efficiencies. These management disciplines also guide our investment in new products
and businesses.
At the heart of our management approach is a
set of core values. First among them is a commitment to the highest standards of business
behavior and ethical responsibility. All AMETEK
colleagues are required to adhere to a written
Code of Ethics. We also have a separate Financial
Code of Ethics for the Chief Executive Officer and
senior financial managers. We maintain a strong
system of internal financial controls that is actively
monitored by senior management, and have
implemented additional safeguards to ensure the
integrity and compliance of our businesses and
financial systems.
We are committed to a culture that values
diversity and fosters a work environment that
allows our colleagues to develop meaningful and
rewarding careers. We actively challenge senior
managers to recruit, train and develop individuals
with diverse backgrounds and experience.
We are focused on providing our shareholders
with consistent and superior returns. We strive to
provide our customers with world-class products
and services on time, and at a fair price. We are
sensitive to the needs of our local communities,
actively supporting programs that foster good
corporate citizenship. A Positive Outlook
We believe 2011 will be another outstanding
year. We expect continued solid core growth. We
also expect to continue acquiring excellent businesses and creating innovative new products and
technologies.
We are confident that our Four Growth Strategies
will continue creating value for our shareholders.
Our confidence is based on a proven record of
success, the strength and experience of our management team, and the dedication of AMETEK colleagues worldwide. The success of their efforts is
reflected in our strong stock price, which has more
than doubled over the past five years, significantly
outpacing the returns of our benchmark indexes
(the Russell 1000 and Dow Jones U.S. Electrical
Equipment Indexes).
We thank Sheldon Gordon, who retires from
our Board this May after 22 years of service to
AMETEK, and David Steinmann, who leaves our
Board in May following 18 years of service. We are
extremely grateful to both for their many contributions to our Company.
As we enter 2011, our businesses are strong. Our
Corporate Growth Plan is solid. Our Four Growth
Strategies are yielding excellent results. Our colleagues worldwide are focused on the success of
our Company.
On their behalf, I thank you for your continued
support and confidence. Together, we look forward
to reporting to you on the results of our efforts.
Frank S. Hermance
Chairman and Chief Executive Officer
March 17, 2011