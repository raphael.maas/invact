With customer expectations evolving faster than ever, we're focused on delivering
the best possible service and experience. Our strong financial position has enabled
us to invest in and strengthen our foundation and capabilities so that we can better
serve our customers and increase market share.
While our top- line performance fell short of our expectations, our 2016 results
demonstrated our team's ability to quickly respond to changes in business
conditions and make fundamental improvements to our operating model.
Throughout the year, we took aggressive steps to optimize our resources and
improve efficiencies. We made significant progress in improving our productivity,
particularly around our technology, supply chain and marketing capabilities.
As ever, we strive to offer a curated selection of the best brands. While we look for new opportunities through our
vendor partnerships, we'll continue to thoughtfully edit our assortment to provide newness and the opportunity for
discovery to our customers. Our strategies around product differentiation, including our ongoing efforts to grow
limited-distribution brands like Ivy Park,J.Crew and Good American, in addition to our Nordstrom-exclusive
offering, are a reflection of this.
As we evolve with the changing expectations of our customers, our business has shifted to support the variety of
ways customers shop with us- and that's now increasingly on line and wit h mobile devices. We made significant
investments to fuel growth over the past five years, in both ecommerce and new markets. Ecommerce now
represents nearly one-quarter of our business, compared to eight percent in 2010.
Looking ahead, we know that changes in retail and customer expectations will continue at an accelerated pace.
We're continuing to examine ways we can harness technology and the information our customers are willing to
share with us to better execute on speed, convenience and personalization. We believe if we can deliver in these
areas, it will culminate in our goal of providing an exceptional customer experience . We have a high-quality store
portfolio that gives us an opportunity to fully leverage our local assets - our people, products and places- to meet
customers' needs in a digitally connected world. With good momentum in place, we're committed to serving
customers on their terms and improving our business now and into the future.
BLAKE W. NORDSTROM
Co - President, Nordstrom, Inc.
PETER E. NORDSTROM
Co - President, Nordstrom, Inc.
ERIK B. NORDSTROM
Co - President, Nordstrom, Inc.