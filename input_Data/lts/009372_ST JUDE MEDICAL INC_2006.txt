Global reach. Market selection.
Cost-effective innovation. And
a team of more than 11,000
employees who are committed
to the highest standards of quality
and business ethics. These are just
a few of the many competitive
differentiators that make St. Jude
Medical one of the best medical
device companies in the world.
2006 was a year of special challenges
for our company due to a particularly
weak market in the United States for
implantable cardioverter defibrillators
(ICDs). Our response to these market
pressures this past year demonstrates
our character as an organization.
A weak U.S. ICD market? We
responded by expanding our investment
in new products and new
people. We had one of the most
successful new product introduction
periods in the history of the company
and invested among the highest
in our peer group in cost-effective
research and development  at more
than 13 percent of sales. We also
expanded our U.S. field sales
and support organizations at an
unprecedented pace.
We are a company that is realistic
about our challenges and meets
them head on. We also are a company
that stays focused on maximizing
our opportunities. Outside the United
States, we increased international
revenue to 42 percent of overall
sales in 2006, with strong market
share gains across a number of
geographies. We met the challenges
of 2006 and took advantage of the
opportunities in order to position
ourselves for success in 2007 and
beyond.
We know that actions speak louder
than words. So, to underscore
our confidence in the future, we
announced a $1 billion stock repurchase
program in early 2007. This
is just one more demonstration that
St. Jude Medicals Board of Directors
and management team are doing
everything appropriate to advance
the best interests of our shareholders.
WE MADE LIFE BETTER IN 2006
In addition to our financial results,
the lives that are saved and improved
by St. Jude Medicals devices are an
important measure of our success.
In 2006, we estimate that one of our
medical devices helped treat a patient
somewhere around the world about
once every five seconds each hour of
every day. Several of these patients
are featured in this years annual
report.
St. Jude Medical targets large growth
opportunities where there is a clear
need for innovation. Our objective is
to capture these growth opportunities
as the technology and market leader.
We increasingly are succeeding in
reaching our goal.
Cardiac Rhythm Disorders
Sudden cardiac arrest is a leading
cause of death in the United States,
resulting in the death of approximately
800 to 1,000 people every day.
Similar statistics abound throughout
the world. ICDs are devices implanted
in the chest that can shock an erratic,
potentially lethal heartbeat back to
normal rhythm. These highly sophisticated
devices are amazingly effective,
yet sadly, only a small percentage of
the people who need these devices
actually receive them.
We are doing our part at St. Jude
Medical to address this tragedy. We
expanded our educational and clinical
programs to increase the awareness,
acceptance and availability of ICD
therapy. We launched more than
20 ICD and pacemaker products this
past year. Each of these new products
was designed to help make life better
for patients, caregivers or other
stakeholders impacted by ICD or
pacemaker therapy. 2006 marks the
sixth year in a row St. Jude Medical
has gained global ICD market share.
One of these new products, the
Merlin Patient Care System, is a
programmer for ICDs and pacemakers
that helps physicians retrieve and
analyze patient information more
efficiently during routine follow-up
visits. The Merlin Patient Care System
was designed as a platform technology
to work with next-generation
products and will become increasingly
important during 2007 and 2008.
In 2006, we also received U.S. Food
and Drug Administration (FDA)
approval for St. Jude Medicals
QuickOpt Timing Cycle Optimization
technology. Previous studies have
shown that patients with ICDs and
cardiac resynchronization therapy
defibrillators can benefit from timing
cycle optimization. However, few
patients undergo optimization
because the current clinical standard
 echocardiography (echo)  can be
expensive and time-consuming.
QuickOpt optimization procedures
can be completed in less than two
minutes compared to echo optimization,
which typically takes between
30 minutes and two hours and
requires manual interpretation by
a clinician.
Atrial Fibrillation
Atrial fibrillation (AF) is the worlds
most common cardiac arrhythmia,
affecting millions of people worldwide
and sometimes leading to stroke
or other debilitating consequences.
Historically, AF has been ignored or
treated with drug therapy that is
expensive and often has limited value
to AF patients. Now, thanks in part to
pioneering work by St. Jude Medical,
more and more AF patients around
the world are being cured of their
disease.
Revenue for our AF business grew
28 percent in 2006 and was approximately
10 percent of our total
sales. After more than 10 years of
pioneering work and investment,
our AF business is well on its way
to becoming St. Jude Medicals next
major growth driver.
Promising technologies launched in
2006, like St. Jude Medicals EnSite
System Version 6, provide improved
clinical capabilities for cardiac
mapping and navigation during
electrophysiology procedures. Our
Epicor Cardiac Ablation System,
which received European approval in
2006 for use in AF procedures, allows
an ablation device to be placed on
the outside of a beating heart and
delivers a high-intensity-focusedultrasound
energy across the wall
of the heart to ablate cardiac tissue,
without having to place patients
on a heart-lung bypass machine.
These and many other exciting
advancements St. Jude Medical made
in 2006 have the potential to make
life better for millions of people who
suffer from AF or from other cardiac
rhythm disorders.
FUTURE GROWTH DRIVERS
We pay as much attention to sustaining
our success over the long term
as we do to delivering outstanding
results in the near term. Accordingly,
in 2006, your Board of Directors and
management team took important
steps to invest in St. Jude Medicals
operations and develop future
growth drivers across multiple and
diverse platforms.
Neuromodulation is one such highgrowth,
high-impact opportunity for
St. Jude Medical that is now firmly
in place. Our acquisition of Advanced
Neuromodulation Systems (ANS)
in late 2005 gave St. Jude Medical
an immediate presence in a fastgrowing,
$1 billion market and
diversified our product portfolio
beyond cardiovascular devices.
St. Jude Medical now holds a strong
position in spinal cord stimulation,
the largest segment of todays neuromodulation
market. We have clinical
trials underway to evaluate neuromodulation
technology in the areas
of Parkinsons disease, Essential
Tremor, migraine headaches and
other emerging indications. These
new applications hold the promise
of profoundly improving the lives of
many people. We expect the global
neuromodulation market to be a
multi-billion dollar, multi-indication
opportunity by the year 2010.
We are also building new growth
platforms in the areas of cardiology
and cardiac surgery. As with our
other platforms, we target markets
that we think will have an attractive
growth profile and provide an opportunity
for St. Jude Medical to be the
market leader. St. Jude Medical is
focused on technologies for interventional
cardiology procedures, such as
patent foramen ovale (PFO) closure,
catheter-based approaches to valve
disease, and embolic protection. Our
cardiac surgery franchise also continues
to grow, fueled by strong clinician
demand for the SJM Biocor valve,
our first U.S. stented tissue valve, and
a pipeline of new products.
To optimize long-term product
development and overall operating
efficiencies, we combined our Cardiac
Surgery and Cardiology Divisions to
create a new Cardiovascular Division,
effective January 1, 2007. While
our cardiology and cardiac surgery
sales forces will remain specialized,
the savings generated from a more
streamlined infrastructure help us
maximize our cost-effective investment
in the research and development
of new products.
WE MEET THE HIGHEST STANDARDS
OF QUALITY AND BUSINESS ETHICS
St. Jude Medicals 11,000 employees
are united by a passion for making
life better for patients and by an
uncompromising commitment to
meet the highest standards of quality
and business ethics. We are proud
of our values and want you to know
about them too.
First, quality. Our products have to
meet the highest standards of quality
because lives depend on them. To give
you one example of how seriously we
take this, approximately 40 percent
of our product development costs
for new ICDs and pacemakers can be
directly linked to ensuring that our
products meet the highest standards
of quality. We design for quality from
the beginning. We then test, manufacture,
test, redesign and test again
to improve quality in a never-ending
cycle. We know that the lives of our
families, our colleagues, other members
of our communities and even
our own lives may someday depend
on the quality of our products.
Our mission is to make life better
through our activities as members
of St. Jude Medical. Nothing is more
important to the success of our
mission than making sure that our
products and services consistently
meet the highest standards of quality.
Next, business ethics. We recently
challenged ourselves with the question,
What is more important 
working with the highest degree of
ethics and integrity or working with
the highest degree of success? The
answer, of course, was that we cannot
be successful as a company unless we
conduct ourselves with the highest
degree of professional ethics and
integrity. This includes compliance
with the St. Jude Medical Code of
Business Conduct and all applicable
laws and regulations in all markets
we participate in around the world.
The St. Jude Medical culture of
integrity is an important competitive
differentiator and a key ingredient
to our success. We set clear standards
of acceptable behavior and reinforce
these standards with education and
regular review. We audit ourselves
to ensure that our behavior meets
these standards. All of this is done in
an environment of constant vigilance
and continuous improvement.
ST. JUDE MEDICAL IS WELL
POSITIONED FOR THE FUTURE
Your Board of Directors and management
team look forward to the future
with an informed optimism that
St. Jude Medical is well positioned
to sustain its success. We have the
right people with the right products
in the right markets. We value costeffective
innovation, uncompromising
adherence to the highest standards
of quality and business ethics, and
continuous improvement of all of our
capabilities. These characteristics have
served us well in the past and will
continue to serve us well in the future.
In closing, I want to acknowledge
the many constituencies that represent
a vital part of St. Jude Medicals
past and future success  our more
than 11,000 dedicated employees,
the companys seasoned executive
leadership team and outstanding
Board of Directors, and our talented
clinician partners and the patients
they serve. Collectively, these constituencies
support St. Jude Medicals
efforts to make life better for patients,
caregivers and all of our other stakeholders
around the world.
Thank you for your ongoing support
of St. Jude Medical.
Sincerely,

Daniel J. Starks
Chairman, President
and Chief Executive Officer