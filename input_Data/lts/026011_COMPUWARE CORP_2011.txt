Dear Fellow Shareholder,
The world has fundamentally changed. Web sites drive more revenue, customer satisfaction and brand equity
than ever before. A vast majority of IT and business leaders are using or will use cloud computing in the next
three years. Mobile device sales are overtaking PC sales. Cross-company and -industry collaboration is not an
exception, but a competitive demand.
In this connected world, the power is in the hands of the technology user. Web and mobile sites  as well as
internal applications  must work well anytime, anywhere, on any device. Key information must be simply and
securely shared.
Compuware is at the core of this seismic shift in technologies. Our unique capabilities in the booming
Application Performance Management (APM) and SaaS-based collaboration markets position us for rapid
growth in the upcoming fiscal years. And they supported strong results for FY 11.
Compuwares fiscal 2011 featured year-over-year increases in total revenues, software license fees,
maintenance and subscription fees, and professional services fees. With our growth drivers leading the way,
each part of Compuwares business contributed to this years success.
Capitalizing on our competitive differentiation, our APM and Covisint solutions delivered phenomenal growth
in FY 11. Augmenting these dynamic performances, Uniface and Changepoint also showed year-over-year
increases in revenues. We significantly increased our Professional Services margin, and our Mainframe
Solutions business once again produced strong operating cash flow and customer retention.
In FY 12, we established a business unit organizational model, which will empower each Compuware line of
business to quickly respond to customers, market conditions and competitive advantages. Through this agile,
entrepreneurial and focused model, we expect to produce continued rapid growth of our APM and Covisint
revenues, as well as ongoing strong results from Professional Services, Uniface and Changepoint. We also see
a more positive mainframe environment in FY 12.
We believe Compuware will deliver increased revenues and strong operating cash flow in FY 12. Our company
will achieve these goals by extending our capabilities in APM and Covisint  where we are seeing strong
category expansion  complemented by continued operational improvements in the rest of our business units.
We have staked out a differentiated position in the APM market, and customers are responding. FY 11 APM
license and subscription fees reached $145.5 million, and total APM product and subscription revenues were
nearly $210 million.
Were achieving this phenomenal level of success because Compuware offers any organization that depends
on web, mobile or enterprise applications  and today, thats virtually every organization  broad visibility,
deep-dive analytics and the simplicity of one solution to optimize application performance. Only Compuware
can provide a single solution for maximizing the performance of important applications from the data center,
to the cloud, to the edge of the Internet on a users mobile device.

Thats a compelling value proposition, and customers worldwide are absolutely demanding this capability to
manage application performance from the end-user perspective. In this new competitive landscape, Compuware
has both the first-mover advantage and the most complete solution.
Our partner focus, particularly in the APM space, remains critical in the year ahead. Partner and distributorowned
or -influenced transactions contributed materially to our business in FY 11. We intend to double that
contribution this year by expanding our relationships with tier-one partners such as Cisco and Accenture, and
by our strong focus on the Managed Service Provider space.
Joining APM on a rapid growth path, Covisint continues to expand its business. In FY 11, Covisint grew
its year-over-year revenues by nearly 36 percent to $55 million. Based on this growth rate, which we expect
to accelerate in the year ahead, we will continue to explore a potential carve-out IPO to unlock the value
of Covisint.
Covisint continues to show strong results in the manufacturing sector while solidifying its position as the market
leader in healthcare. The Covisint business is well-balanced between manufacturing and healthcare revenues,
with a growing contribution from emerging markets like energy. Through additional investments in the business
this year, we expect to stake out an entry position in at least one more favorable vertical market.
Our Professional Services, Changepoint and Uniface businesses have achieved remarkable reinventions over
the past two years. Building on last years success, we will continue to focus these business units on producing
profitable growth in their categories.
The Compuware team is absolutely unified and energized around our goals this year, and were working hard
already to make them a reality. Were racing to capitalize on our competitive advantages in key markets 
especially with our APM and Covisint solutions.
Were operating under a fast, focused and efficient business model, and it positions us for an outstanding year.
I look forward to sharing the results with you.
Sincerely,
Peter Karmanos, Jr.
Executive Chairman of the Board