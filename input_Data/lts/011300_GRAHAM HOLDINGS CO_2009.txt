To our shareholders:

At the start of last year, the future of The
Washington Post Company seemed somewhat uncertain.
As a symbol of the uncertainty, we had $400 million
in bonds coming due in just six weeks  and no
lender had said they could be renewed at less-thanextortionate
rates.
The wise advice from our lead director was: wait about
three weeks. And indeed, new ten-year bonds were sold
(albeit at a somewhat higher rate than the old ones).
This modest success turned out to be a good sign.
2009 was a much better year than anyone could have
expected for The Washington Post Company. While
GAAP results looked awful, the balance sheet doesnt
lie: cash and securities net of debt were $269 million
more at year-end 2009 than 12 months earlier (and
the money overwhelmingly came from business proceeds,
not from asset sales).
The Nature of the Company
This somewhat complicated report will read better if
you keep in mind the dramatically changed nature of
our Company.
In 1991 (the year present management took over),
82% of The Post Companys revenues came from
newspaper, magazine and local TV broadcasting
businesses. The newspaper division  then, as now,
mostly the Post  was 47% of the Company.
In 2009, these same three divisions accounted for
only 25% of our revenues. Kaplan alone was 58% of
revenues, and Cable ONE was 16%.
In profits, the change was even more dramatic.
Our once-mighty newspaper business lost a lot
of money in 2009; so did Newsweek. That new
money in the corporate cashbox at year-end came
almost entirely from Kaplan, Cable ONE and Post
Newsweek Stations.
This new order of things suggests that shareholders
are looking at a different set of realities at The
Washington Post Company; the Company is more
dependent than ever on a single business: Kaplan.
(The Post was never as much as 58% of Company
revenues since we went public.) Education has been
a terrific business, for us and for many other companies.
It should be in the future, as well. But as 2010
dawns, Kaplans business faces some risks our shareholders
should know about and understand.
I will address each of our large businesses. But our
reported results for 2009 were so confusing that they
should be explained first.
2009 Results: The Meaning of One-
Time Charges
A reasonable shareholder might ask: how did we
accumulate $87 million of additional cash and pay
off $155 million of debt, while reporting only $91
million of net income?
The answer to this question is astonishingly complicated.
We are not a very large company (less than
Fortune 500 size), but for years, our income statement
has offered a reference book of one-time
charges. We have closed businesses; we have sold
businesses, stocks and real estate; we have announced
restructuring plans that required lease terminations,
severance payments to employees and accelerated
depreciation of fixed assets; we have recorded
impairments to goodwill and other assets of businesses
we own and to stocks weve owned; weve
also reduced the book value of hard-to-value investments
in early-stage companies, offered retirement
incentives (repeatedly) to long-service employees,
closed a printing plant, recorded gains in broadcast
division assets, booked losses (and gains) on
foreign exchange, booked charges for a nonsolicitation
agreement, have had a 49% owned subsidiary
record a one-time tax charge, benefited from a state
tax change, recorded non-cash charges when the
accounting treatment of equity awards was changed,
and recovered insurance proceeds.
Those were only in the past four years!
Whats the meaning of all those charges? Are we better
understood at the end of 2009 as a company that
earned only $91.2 million in corporate net income,
down 78% from our best year ever (as the income
statement says), or as a company that generated the
additional hundreds of millions of dollars described
at the beginning of this letter?
At the very least, this set of questions is an advertisement
for reading all of the financial statements of
a company you own shares in (at least the income
statement, balance sheet and cash flow statement).
Major One-Time Adjustments of 2009
n $64.5 million in early retirement program
expense at The Washington Post,
Newsweek and other operations. We have
had so many of these that shareholders might begin
to question their inclusion as a one-time expense.
My summary: we spent (or will spend) real money
here. Previous management of the newspaper
(that would be me) did not see clearly the drastic
changes that were coming to this business. Current
management (see the Post and Newsweek sections
later in this report) has dramatically cut expenses
at the newspaper and magazine. It should be noted
that retirement-incentive payments were made not
from general corporate funds, but from our pension
fund. (Over many years, reductions in cost at
the Post and Newsweek have been greatly aided
by our overfunded pension plan, a rare asset these
days.) Thats one big reason cash results are better
than reported income.
n $33.2 million in Kaplan restructuring
charges. $24.9 million of these were related to
the closing of Score, an after-school tutoring business
for elementary school children we bought
for a few million dollars and hugely expanded
in the 1990s. Score is instructive: it looked like
a wonderful business. Technology made it possible.
(In 1994, a roomful of school kids could have
access to computer-based teaching and wonderful
coaches, recent college graduates.) And
technology killed it. (The company that made
the software was very slow to update it; anyway,
by 2009, most children had plenty of computer
access in school and at home.)
n $33.8 million in accelerated depreciation
primarily for the closing of a printing
plant at the Post. On the one hand, this is a
non-cash charge. On the other hand, we closed a
printing plant that opened only in 1998 and would
have had a useful life for years after 2009. Plant
closing expenses have all been fully booked, so this
charge wont recur.
n $25.4 million charge for impairment
at Kaplan Ventures. When we reorganized
Kaplan (see discussion in the 10-K), our accountants
went through a normal review of whether
the assets on the books were still warranted by
business conditions in 2009. The largest business
in Kaplan Ventures  Kaplan EduNeering  is
profitable but has not lived up to our expectations
when we bought it. Our accountants concluded
 and theyre right  that its worth less than
we paid for it (in other words, I lost some of your
money here).
Three things to note about impairment charges:
first, they are non-cash (on this years books);
second, the money we spent to acquire EduNeering
in 2007 was real, so the charge reflects an overly
optimistic evaluation of an acquisition catching up
with us; third, these charges are one-way only. Its
only good accounting, but if acquisitions dont perform
as we expect, intangible assets are reduced; if
the same acquisitions recover to higher profits (or
other acquisitions do better than expected), the
charge is not reversed (the profits are recorded on
the income statement). In fact, EduNeering management
is working hard and thoughtfully to make
its results better.
In all, these expenditures diminished our profits.
Our ability to use pension fund assets for buyouts
has been useful. Our management tries to make
money, both on earnings reports and in cash. But our
emphasis will be on the cash results and on the longterm
strength of the Company.
Of course, another reason cash increased was that we
had our slowest year in a long time on the acquisition
front. We chased hard and were hopeful on more
than one occasion, but not much happened.
Newspapers
Newspapers generally performed horribly in 2009;
the Post certainly did. We lost $164 million in the
newspaper division. While the majority of this consisted
of retirement-incentive payments that came
from our pension fund or non-cash accelerated
depreciation, cash losses at the division were about
$33 million (cash losses are operating income, excluding
depreciation, amortization and pension expense,
less capital expenditures).
Publisher Katharine Weymouth and editor Marcus
Brauchli responded by cutting costs sharply. By the
fourth quarter, expenses were down significantly
from a year ago. The fourth quarter was profitable for
the newspaper division (that does not mean future
quarters will be  advertising business is seasonally
highest in the fourth quarter).
We also merged the newsrooms and most business
functions of the Post and washingtonpost.com.
Business results showed some similarities too: both
print and online advertising revenue were down in
2009, print by 23% and online by 13%.
The futures of these two businesses are different.
Despite its challenges, the Post has two unusually
strong print advertising categories: retail, because
the ads work so much better than any alternative,
and advocacy or other ads aimed at our unique
Washington audience. Because of these and our
larger-than-normal penetration, we will do better in
print advertising than most newspapers.
But the future for online advertising is much brighter.
Its a clich in the advertising world that the percentage
of ad spending online doesnt match the percentage
of time people are spending there.
But it will. When Pepsi canceled its Super Bowl ad and
put the money online, it was an indication that advertisers
will be looking for ways to use this still-rathernew
medium effectively. The Post  and all other
media  will have to be online with compelling content
and innovative opportunities to offer advertisers.
Though online advertising will grow, it will only flow
to newspaper sites if we create very good ones.
What remains to be said about the future of the newspaper
business? Plenty.
First, I believe in the work of my colleagues, the journalists
and everyone else producing excellent newspapers
and web journalism every day. Their work at its best tells
us a lot of things we didnt know  things that matter.
Second, good as is the work weve traditionally done,
it is not enough. We have to produce work that is
shaped by the needs of digital-media readers  in
the formats they want, in the media they want, at the
times they want it.
Third, innovation in advertising will be every bit
as important as innovation in the presentation and
delivery of our content.
2010 is very late to be writing this  but not too late.
The infusion of digital talent here in the past two
years is extremely heartening; we need innovation
across the board, in advertising (and customer experience)
as much as in news.
And we need cost control.
Kaplan
Kaplan had a remarkable year, growing its profits
despite restructuring charges of $33.2 million.
The Post Company is increasingly centered around
Kaplan Higher Education, which last year accounted
for 58% of Kaplans revenues.
As the recession took hold, many Americans had an
impulse: maybe I should go back to school. With
employers laying off or looking shaky, additional credentials
looked like a wise bet.
Coming into the year, both Kaplan Higher Educations
brick-and-mortar campus business and its online
Kaplan University were already quite large academic
systems. Kaplan University, with 41,000 online students
on January 1, 2009, grew at a very dramatic
pace and had 60,000 online students enrolled at yearend.
Almost everyone in the for-profit higher education
sector grew; KUs year was spectacular.
In other words, we grew in response to a pronounced
student need. And our students are precisely those
who have been most underserved.
Kaplan Higher Education Campuses helps students
get a step up on the first rung of the ladder of economic
opportunity. Many of these students have
stalled in earlier encounters with the education system.
This is the only good decision Ive ever made,
a 35-year-old medical technology student told Beth
Hollenberg, KHECs chief operating officer.
KHEC works because we work with students. If they
need child care or a part-time job, we help them find
it. If they dont show up for class, we call. Some need
help learning to study. Some need different hours (a
Detroit campus runs classes from 6 a.m. through the
night shift).
At Kaplan University, again, our student is not the
traditional college student. The average age is 34.
Almost no one is getting financial help from parents.
Most have tried college before, but did not complete
the coursework.
Many study online because it fits their lifestyle; some
are working full time or raising families.
Kaplan University is a good place for them. To quote
KHE senior vice president Peter Smith: There
is nothing going on but teaching and learning.
Theres no football team, no marching band, no
leafy campus. Students are there to get a better job
and a foothold in a better career. We teach skills that
employers want.
And, we do it with a considerable degree of personalization.
Each student gets an academic advisor who
may stay with her until she finishes; math and reading
labs are heavily used.
President Obama wants to significantly increase the
number of U.S. college graduates in this decade; specifically,
he wants more opportunity for students
from low-income homes, precisely those we teach.
If the U.S. is to graduate more college students, the
for-profit sector will have to play a role; most state
universities cannot expand today in light of state
budgets (the California State system alone is reducing
its student count by 44,000).
We know from our international experience that
other countries are not standing still; theyve put
together meaningful plans to increase their percentage
of college graduates.
Everyone also wants to see the quality of higher education
improve relative to its cost. And by driving the
adoption of online education, the for-profit sector
has led the way during the past decade.
In the decade to come, we can expect significant
advances in:
n technology and, particularly, the personalization
of education to specific students;
n the ability to reach students where they are: at
home, at work  virtually anywhere;
n the tailoring of education to the needs of working
adults; and
n the export of western-style higher education to
international students who are hungry for highquality
programs, but dont want (or cant afford)
to leave home.
As for-profit education has grown, so have its critics.
Shareholders should be aware that at year-end,
the U.S. Department of Education was proposing
an array of regulations, some intelligently aimed at
cracking down on frauds; some appearing to betray
hostility to for-profit education; and some that are
hard to understand at all.
U.S. government grants and loans to students are key
to all higher education in the U.S., both for-profit
sector and traditional colleges and universities. Those
dollars should not be wasted; they are supposed to
create opportunity for students to receive a genuinely
valuable education.
There are for-profit sector educators with high aspirations
for quality and for the success of their students.
Kaplan is one of them. Such educators seem like natural
partners with a government whose announced
purposes include dramatically expanding the number
of adults who get a college education.
Critics point to the rates of default on loans to students
at for-profit universities. But this is the defect
of our virtues: Kaplan is educating working adults
of modest means. In almost all cases, parents arent
paying a large part of our students costs. But we are
offering a highly practical education to the adults
who can benefit most.
It doesnt work for everybody. Not all of our students
graduate, and most leave with some level of debt. But
these things are true for some students at just about
every college in the United States.
Im proud of the way Kaplan Higher Education has
been built  with student outcomes, quality education
and business results in mind. Well keep working
to improve all three.
There is plenty going on at Kaplan outside Kaplan
Higher Education. Further surprises in our reshaped
Company: the part of Kaplan outside KHE is now as
big in revenues as the Post, Newsweek and our television
stations combined.
One big surprise here is negative: profits in our legacy
test prep business shrank meaningfully for a second
straight year. The economic climate and a flood of new
boutique-style competition were responsible. Kaplan is
aggressively taking steps to bolster its leadership position.
You will hear more about our progress next year.
Our revenues also suffered because our professional
training businesses include large units helping students
prepare for finance and real estate careers.
Demand continued to slow in 2009.
International operations were something of a bright
spot. After being jolted by the financial crisis, particularly
during the first six months, both European and
Asian operations improved throughout the year. We
have very good prospects overseas.
Probably the most important thing that happened
at Kaplan was Andy Rosens impressive performance
as CEO. As I expected when he succeeded Jonathan
Grayer, who built Kaplan so successfully, we have
another outstanding leader.
Broadcasting
The television business faces challenges, but so far
PostNewsweek Stations has responded to them very
well. Advertising results in 2009  especially in the
first half of the year  were very bad. Automotive
advertising has traditionally accounted for roughly
25% to 30% of local TV station revenues, and in early
2009 there simply was no automotive advertising.
PostNewsweek CEO Alan Frank and our excellent
team of station managers responded by reinforcing
our traditional priorities: focus on building our audience
and emphasize local news. We have work to do
in all our markets, but our stations in San Antonio,
Jacksonville, Miami and Detroit retained their traditional
news leadership; gratifyingly, the Orlando
station (under Company news veteran Skip Valet)
showed significant ratings improvement; progress
was slower, but real, in Houston.
Every Post Company shareholder should be grateful
to Alan and the PostNewsweek team; it was their
lowest year in profits in 15 years, but one of their best
management jobs.
Mixed News from Cable-Land
2009 was a simply fantastic year for Cable ONE. Tom
Might and his management team set an all-time, honest-
to-God record for operating income (adjusted for
inflation or any way you want it) in a year when our
other media properties were going backward. We
dont expect cable profits to go up in 2010 (we wont
increase cable rates this year), but they will still be
high compared to historic norms.
Why, then, does the heading on this section say
mixed news? Let me tell you the reasons: government,
content owners, competitors and substitution.
The February 2009 Stimulus Bill included $7 billion
in broadband stimulus that may fund potential
Cable ONE competitors with tax dollars. The
FCC will proclaim a National Broadband Plan in
March 2010, which may fund potential Cable ONE
competitors with billions more tax dollars annually.
The FCC also hopes to proclaim new net neutrality
regulations that might limit the profitability of
Cable ONEs increasingly important Internet service
business.
Content owners are demanding higher and higher
prices from cable companies (a dispute between Fox
and Time Warner Cable made headlines at year-end).
All these increases must be passed on to cable customers.
Cable ONE, with lower-demographic customers,
will try to keep the most popular channels, but will
have to drop those that ask more in price than they
deliver in value.
Competition is reaching a whole new level in all three
Cable ONE businesses: video, voice and data. Dealmaking
by satellite and telephone companies became
epidemic during the recession. (Unfortunately, Cable
ONE is not immune.)
Video substitution is an important new fact of life.
Cable TV viewing is going down as Internet viewing
is going up rapidly. Fortunately, Cable ONE is in both
businesses, but stay tuned for the rest of this story.
Whatever happens, I would bet on Tom Might and
his team to figure out the best ways to compete.
Newsweek
Newsweek managementAnn McDaniel, Tom Ascheim
and editors Jon Meacham and Fareed Zakaria 
reshaped the magazine and its audience throughout
2009. Results were terrible early in the year, but gradually
improved. As we planned, the magazines circulation
is less than it was a year ago, but its readers are
paying more for it, and advertisers appear to be pleased
with our audience. The cuts made in Newsweeks
expenses were difficult, but they were essential.
In summary, I felt a lot better about business at the
start of 2010 than I did at the start of 2009. The huge
uncertainties of 12 months ago have receded. With
little evidence to back up my feeling, I feel better
about the Posts future prospects than I did at this
time last year. On the other hand, our two most successful
businesses, Kaplan and Cable ONE, face more
risks today than a year ago. On the whole, I feel pretty
good about the future of our Company.

Donald E. Graham

Chairman of the Board and
Chief Executive Officer
