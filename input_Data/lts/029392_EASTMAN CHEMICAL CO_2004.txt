To Our Stockholders
       Everybody loves a good story, and we at Eastman do, too. For us, 2004 certainly made for
       an important chapter of our journey  marked by real achievement. For the past two and
       a half years, we have been executing a "turnaround" strategy to position ourselves as a
       company that creates value rather than consumes it. We have been decisive in our actions
       to restructure Eastman's businesses and disciplined in our approach. As a result, we are
       seeing improved financial results and positive momentum, which reinforce our confidence
       that we are moving in the right direction.
                               Improved Profitability
                               We started 2004 with an ambitious agenda that was aimed at a single objective  to
                               improve the company's profitability. Our year-end results demonstrate our determi-
                               nation  and our success  in reaching this goal:

                               > Earnings of $2.18 per diluted share were the best since 2000. The results included
                                 asset impairment and restructuring charges, other operating income and a favorable
                                 tax settlement.
                               > Our sales revenue of $6.6 billion was the best in our company's history.
                               > Gross profit increased by $168 million  a solid improvement over 2003. Yet
                                 these results are even more impressive when you consider that we achieved them
                                 despite a staggering increase in raw material and energy costs of approximately
                                 $600 million, leading to a margin compression of around $100 million.
                               > We experienced revenue growth in all regions  North America, Europe, Middle
                                 East and Africa, Latin America and Asia Pacific. Eastman's performance in Asia
                                 Pacific was particularly strong. There, our revenue increased 22 percent over
                                 2003, and is now about 12 percent of our total company revenue. In 2004, we
                                 also doubled our operating earnings in this region compared to 2003, and our
                                 operating margin is now approaching 10 percent. We expect this growth to continue.






> Excluding asset impairment and restructuring charges, four out of           strategic asset for us, and we pursued several alternatives for
  six reporting segments reported an increase in operating profit.            maximizing our interest in it. The sale to Danisco represented
> By the end of 2004, our stock price was trading more than                   the most attractive available option and will deliver fair value
  45 percent higher than at the end of the previous year. In fact,            to Eastman.
  including dividends, your stock had a total return of approxi-
  mately 50 percent in 2004, significantly outperforming both               > Continuing to strengthen both manufacturing and cost structures 
  the Standard & Poor's 500 and Eastman's peers in the                        Many less visible actions that we have taken have generated
  Standard & Poor's 500 Chemicals Index. This performance is                  improvements in nearly every part of our company. For example,
  a clear indication that the confidence we have in our company               in our Specialty Plastics segment, we continued to rationalize
  is shared by others.                                                        capacity and consolidated production to our Kingsport,
                                                                              Tennessee, facility. Also, we restructured our Polymers segment
                                                                              to create a more integrated organizational structure. These
Doing What We Said We'd Do

Improving profitability was our top priority for 2004. As you've              actions, combined with our corporate initiatives, resulted in a
seen from our full year financial performance, the numbers clearly            20 percent reduction of labor force.
show that we rose to the challenge and successfully delivered
on this. While our overall performance is an important part of              Starting a New Chapter

Eastman's story, so too is the progress we made on these                    In looking ahead to what's in store for Eastman, and for you as our
specific  and significant  actions:                                       stockholders, I am very optimistic that 2005 will be even better
                                                                            than 2004. We expect our hard work over the past year and an
> Addressing underperforming businesses and product lines in                upturn in the chemical industry to help drive further bottom line
  our Coatings, Adhesives, Specialty Polymers, and Inks                     growth. We are also taking actions to enhance our marketing and
  (CASPI) business segment  In July, we divested these busi-               commercial skill sets in order to complement Eastman's strong
  nesses and product lines, which had an immediate positive                 manufacturing and chemical expertise.
  impact on our financial performance.
                                                                            2005 holds great opportunity for Eastman. Yet, like our peers, we
> Pursuing options to maximize value from our interest in                   will continue to face challenges brought on by volatile costs for
  Genencor International, Inc.  In January, 2005 we announced              energy and key raw materials, particularly paraxylene, ethylene
  an agreement to sell our interest in Genencor International,              glycol, and propane. Pricing is a major focus in 2005 and will
  Inc. to Danisco A/S. As we have said, Genencor was not a                  remain the key determinant in our ability to successfully manage


these costs and increase margins. Fortunately, we do see signs of            Again we see this theme emerge as we look at our candidates for
supply and demand tightening for many of our products, which                 future growth. For example, within the health market, one oppor-
will help in giving us the pricing power we need.                            tunity we are pursuing is in food diagnostics. We believe we can
                                                                             leverage our strong heritage of analytical chemistry capabilities
A Greener, Smarter, Lower Cost Strategy                                      with our biotechnology skills to provide fast, accurate testing for
The solid financial foundation that we have established,                     the growing food safety market.
coupled with improved business conditions, has enabled us to
shift our focus to long-term plans for growth. After careful con-            In our polymers business, we're using our know-how and
sideration of our corporate strengths and advantages, we have                technology innovation to strengthen the position of PET in the
chosen a technology-driven strategy, targeted on select oppor-               packaging resin industry. Earlier this year, we broke ground on our
tunities in these high-growth markets, and attractive market                 new 350,000-ton PET plant at our site in Columbia, South
segments within them: building and construction, electronics,                Carolina. This new facility will produce PET resin using many
health, and packaging. These are familiar markets where we                   features of our new, proprietary             technology. This break-
                                                                                                              IntegRex

currently participate, and where we have identified an array of              through technology is based on the integration from paraxylene
greener, smarter, lower cost solutions that draw on our unique               to PET and features multiple process innovations which give it
technologies and have the potential to create significant value              characteristics that are protective of the environment. Not only is
for Eastman.                                                                 it less energy intensive, but because it reduces the number of
                                                                             intermediary steps to produce PET, we are able to build a world-
Let me explain what I mean by "greener, smarter, lower cost." To             scale manufacturing plant that occupies half the footprint of
understand this, you only have to look at our current portfolio of           conventional PTA and PET facilities. Capacity is planned to come
products and see where we have consistently created the most                 on-line in the fourth quarter of 2006, just as the demand for PET
value. This theme of "greener, smarter, lower cost" can be found             is expected to outpace the supply.
in our broad range of polyesters; from our recyclable, low-cost
PET polymers to our tough, chemically-resistant specialty plastics.          We further believe that the benefits of this breakthrough
Or, in many cases, our proprietary technologies provide unique,              technology will extend beyond our commodity PET business into
value-adding functionality for our customers. Our cellulose acetate          our specialty plastics business as well. With        we can
                                                                                                                              IntegRex,

butyrate (CAB), for example, gives customers a unique metal flake            make better use of our intermediates capacity and have the
orientation and surface leveling for coating applications. Our port-         potential to redeploy PET assets to make unique copolyester
folio is rich in such examples.                                              products to serve new markets.



New opportunities like these, together with the strength of                      for cash continue to be funding a strong dividend, reducing net
Eastman's legacy businesses (particularly Fibers, CASPI, and                     debt, and investing in targeted growth initiatives.
Specialty Plastics), will contribute to our success and prosperity
beyond the chemical industry cycle. The challenge now is to                      In Conclusion
conduct the due diligence necessary to make smart choices and                    As we close the chapter on 2004, I want to extend my
prudent investments that will fuel our future growth.                            gratitude to Eastman's 12,000 employees around the world for
                                                                                 making this a pivotal year in our history. Thanks to their talents,
                                                                                 know-how, and commitment, we have successfully executed a
Disciplined Approach to Growth
Our number one priority is continued earnings growth. We have                    turnaround strategy that has significantly improved the company's
set for ourselves a financial goal of generating 10 percent operating            profitability, and better positioned us for profitable growth. We
margins and revenue growth rates that are above gross domestic                   know we must continue to build on this progress, and I'm certain
product (GDP). We intend to demonstrate that we can grow                         that we will.
despite industry cycles. This is what is driving our corporate

strategy efforts.                                                                We have another important year ahead of us. I hope you will join
                                                                                 us as we begin this latest chapter of our journey, and
As we pursue this goal, and throughout all of our actions, you                   I look forward to sharing more of the Eastman story with you as
can be assured that we are grounded in the same high level of                    the year unfolds.
discipline that we demonstrated during the turnaround. We will
be judicious in our management of resources so that we are able
to successfully balance our needs for success in 2005 with our                   Sincerely,
needs for success in the future. This means that we will analyze
and prioritize our efforts, and redirect our people and capital to the
growth initiatives that create the most value for the company 
and in turn, for you, our stockholders.                                          J. Brian Ferguson
                                                                                 Chairman and Chief Executive Officer
Furthermore, we will continue to concentrate on strengthening
our balance sheet. We have consistently demonstrated our ability
to generate strong cash flow, and this year the divestiture of our
interest in Genencor will provide additional flexibility. Our priorities

