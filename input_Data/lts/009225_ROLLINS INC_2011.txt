To Our Shareholders


At Rollins, we remain focused on what we do best: providing superior pest control solutions
to homes and businesses. We are continually looking for ways to innovate and improve
our services. As Orkin founder Otto Orkin said, �We must never be satisfied with less than the
best if we are to secure our share of tomorrow�s market.� In short, our culture dictates a
commitment to continuous improvement, which is reflected in our Company�s performance.

FINANCIAL SNAPSHOT
Rollins had another record year in 2011 � our
14th consecutive year of profit improvement and
improving revenue results. Revenues reached
$1.2 billion, representing 6.0% growth over 2010.
Residential pest control revenues grew 7.7%, termite
control revenues grew 2.8%, and our commercial
business posted 6.0% growth. As a result, net income
increased to $0.69 per diluted share, or a 13.1%
increase compared to last year. This is the first time
in our Company�s history that we have topped $100
million in profits.
Gross margin was up slightly at 48.8%, reflecting
higher fuel and administrative costs associated
with several strategic technology projects and the
integration of our 2010 acquisitions. However, we were
able to realize some overall efficiencies and increase
prices to offset some of these costs.
Rollins shareholders received dividends of $0.28
per share, which was a 16.7% increase over 2010.
This made 2011 the tenth consecutive year in which
dividends increased at least 12 percent. Shareholders
benefitted as well from our stock repurchase of 1.5
million shares of stock.
Total assets grew to more than $645 million and our
Company generated significant free cash flow, with
net cash from operations reaching $154.6 million,
an increase of 24.7% over 2010. Overall, Rollins�
performance demonstrates the strength of our
business and its value even in a weak economy.
We believe our persistent focus on exceeding
our customers� expectations will continue to drive
exceptional performance despite the nation�s
ongoing economic malaise.
CONTRIBUTION FROM OUR BRANDS
During 2011, we realized significant advantages from
our 2010 acquisitions. Trutech, one of the nation�s
largest pest and wildlife removal companies,
exceeded our expectations, and Waltham Services
also performed ahead of plan.
Customers, employees and shareholders continue
to benefit as we adapt best practices from each of
our brands. For example, we adapted companywide
customer satisfaction survey programs attained
from HomeTeam to better gauge customer satisfaction
down to individual location levels. The results of
these processes also allow us to address customer
concerns early, which improves our customer
satisfaction and retention.
Building on the HomeTeam model, we are now
capitalizing on the consumer�s growing desire to
provide feedback and share experiences. This data
is quickly shared with our locations, improving our
focus to better satisfy and retain customers.
We are also constantly developing and honing
strategies to help each of our brands improve its
unique niche in the marketplace, including providing
technical and marketing support to better access new
customers online with more effective internet content.
In addition, we continue to benchmark with successful
companies outside of the pest control industry and
improve results by adapting applicable ideas and
programs. We realize that satisfying customers is a
journey, not a destination. Every customer interaction
is critical and should be better than the last.
ADDRESSING CHALLENGES AND OPPORTUNITIES
With rising vehicle purchasing and maintenance
costs, we investigated our brands� and other successful
companies� best practices to improve the management
of Rollins� fleet. As a result, we began to co-source our
fleet management with Wheels, Inc., a well-known
and highly respected fleet management company.
By consolidating vehicle-related vendors, we expect
to save more than $5 million over the next six years
through improved purchasing, financing, maintenance
and management of our more than 8,000 vehicles.
We also launched several technology improvements
to increase productivity, efficiency and customer
service. We expect these investments to further help
us exceed customer expectations and result in future
cost reductions.

On the opportunity side, the U.S. bed bug
market continued to grow as a result of this pest�s
proliferation and increased consumer and media
attention. We experienced good growth and market
penetration in residential as well as commercial bed
bug services. In total, our companywide bed bug
business increased 33.6% for the year.
Building on the growing bed bug awareness, we
provided a grant to the University of Kentucky to
study bed bug biology, behavior and control. Rollins�
support is the first programmatic funding for bed
bug research by any business, government agency
or institution. We believe the results of this research
can provide information for enhanced treatment and
prevention methods. We also published a bed bug
white paper last year to help businesses, consumers
and government agencies better understand the pest
control methods, detection and issues that are involved
with a bed bug infestation. These two initiatives helped
further position Rollins as the industry leader in bed
bug services and pest control in general.
Capitalizing on bed bug opportunities internally
within all Rollins brands sparked the company�s
Bed Bug Initiative, which included launching a
companywide Bed Bug Resource Center while
developing a certified training program for bed
bug treatment. This was Rollins� first all-brand
endeavor with procedures and policies developed
by this multi-brand, multi-functional team.
In other firsts, Orkin established its first franchise in
China. With 25 Chinese cities having a population of
more than 10 million each, this initiative addresses the
significant potential for Orkin. We also launched two
other international franchises in Nigeria, which are the
first for Orkin in Africa. To date, we have 18 international
franchises, and they are financially beneficial while
providing brand enhancement for Orkin worldwide.
Overcoming challenges and taking advantage of
opportunities have resulted in impressive results for
Rollins. In fact, our stock has appreciated more than
140% over the past five years, which greatly benefits
the vast majority of Rollins employees who are
shareholders in the Company�s 401k plan. Even in
a soft economy, they have improved their financial
situation as a result of the stock�s performance.
Because these employees have a stake in the
Company�s performance, they think and act like
owners, which further drives our success.
OUR FOUNDATION FOR THE FUTURE
During 2011, the Rollins Board of Directors named
John Wilson, Gene Iarocci and Bob Wanzer as
corporate officers and vice presidents. These
respected and highly successful company leaders
possess years of diverse experience. We are fortunate
that they are now assuming a greater role in
directing the future of our Company.
Going forward, we will remain focused on better
satisfying our customers, delivering excellent pest
control solutions, and growing our business. We will
continue to pursue strategic acquisitions that are
complementary. We will maintain a relentless drive to
improve and innovate. We realize that if we are not
moving forward, we will fall behind � a realization
that fuels much of what we do.
We express our sincere thanks to our dedicated
employees for their commitment and hard work, for
without them, Rollins� results would not be possible. We
also thank our Board members for their invaluable
guidance, our valued customers for their confidence
in us and our investors for their continued support. 

R. Randall Rollins
Chairman of the Board
Gary W. Rollins
Chief Executive Officer,
President and Chief Operating Officer