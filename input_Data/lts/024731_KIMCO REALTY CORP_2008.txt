Dear Fellow Shareholders, Partners and Associates:
In Kimcos long business history we have endured many retail
bankruptcies, credit crunches, business cycles and recessions,
but none of these moved upon us with the velocity of what
I will refer to as the Economic Tsunami of 2008. Beginning
last fall, consumers significantly reduced spending and
financial institutions, pressured by loan losses and declining
investment values, reduced lending. Housing prices have declined
virtually everywhere in the United States. All businesses
now seem to have two priorities: (1) to monitor and bolster
liquidity; and (2) to reduce costs. These circumstances point
to a poor environment for the retailer and, consequently, challenging
times for the owner of retail real estate.
Despite the ominous clouds that gathered during much of
2008 and the sharp contraction of business activity, we were
able to deliver steady financial results from our core operating
activities. Funds From Operations (FFO), a widely accepted
measure of REIT performance, were $522.9 million, or $2.02
per diluted share. This compares to $669.8 million, or $2.59
per diluted share, in 2007. Over three-quarters of the decline
can be traced to non-cash valuation reserves for a reduction in
the value of certain of our assets, mostly related to our stock
holdings in other public companies. Excluding these charges,
our FFO per diluted share was $2.46 and $2.59 for 2008 and
2007, respectively.
We maintained solid occupancy levels of 93.7% at year
end, despite retailer bankruptcies and a declining economic
environment. The tireless and enthusiastic work of our leasing
organization yielded positive re-leasing spreads of over 10%
and an average quarterly increase in same-site net operating
income of a solid 2.4%.
We kept our balance sheet flexibility intact with over $1
billion of credit availability as of year end. Our finance team
was able to raise over $1.8 billion of debt and equity, including
over $400 million of common equity prior to the rapid
deterioration of the markets that began in September.
In the balance of this letter I would like to share Kimcos
strategy for dealing with the present difficult environment,
beginning with some historical perspective, and address a few
other issues. In the comments that follow mine, Dave Henry,
our President, Mike Pappagallo, our Chief Financial Officer,
and David Lukes, our Chief Operating Officer, will review
our business in more detail, as well as discuss for you our
strategy for further strengthening our financial position.
The Background
In 1991, we concluded that the REIT model was a good
one and that Kimco should become a public REIT. Our initial
public offering (IPO) occurred in November 1991. For a
few years thereafter, real estate prices were such that we could
acquire shopping centers at entry yields and generate total
returns well in excess of our cost of capital. Over time, as
additional REITs became public, capital flowed continuously
into commercial real estate, and cap rates compressed to the
extent that attractive accretive purchase opportunities in the
U.S. were not widely available. As a result, Kimco began to
acquire shopping centers outside of the U.S.  in Canada and
Mexico  and we even diversified into non-shopping center
properties where we believed value could be added, e.g.,
extended stay residential, urban residential and net leased
industrial properties. We also formed joint ventures with
institutional investors with a low cost of capital who were
looking for well-leased retail properties and stable cash flows.
In 2001 the REIT Modernization Act was passed. This law
allowed REITs to create taxable subsidiaries that were permitted
to engage in a wide range of business activities that
are carried on daily by non-REIT organizations, provided
the REIT paid corporate income taxes on profits from these
activities. Kimco leapt at the opportunity to engage in various
real estate-related businesses where we had expertise, such as
the merchant building of shopping centers and investments
to provide liquidity for the real estate assets of various retailers
like Montgomery Ward, Strawbridge and Clothier, and Ames.
Over the years, these business activities produced substantial
additional income for our shareholders. However, competition,
armed with cheap capital and ample liquidity, pursued
the same or related opportunities. Our competitors were
other REITs, hedge funds, investment banks and other capital
providers. In some cases we became partners with other funds
and investment banks (e.g. Albertsons, Konover and others).
However, as competition increased, our income from these
business activities tapered downward.
The Strategy
And then came the Economic Tsunami of 2008 which
reversed everything with lightning speed. Capital became
very tight and expensive, while risk aversion spiked. A major
recession was at hand. This current environment compels
us to refocus our efforts on maintaining and enhancing our
position as the premier owner and manager of neighborhood
and community shopping centers in North America, and to
grow our beneficial ownership of over 900 shopping centers
containing 80 million square feet of leasable space. This
must be our predominant focus. Our core business, defensive
in nature, should provide the best risk-adjusted returns
for our shareholders in these challenging times. In addition,
our strategy will position us to seize, and take advantage of,
opportunities that normally become available in economic
downturns. This strategy has always been part of our DNA.
The Execution
We have moved to reduce our exposure to development risk
substantially. While development has historically been a profitable
activity for Kimco, we anticipated last year that retail
demand for U.S. development projects in new suburbs would
decline as housing starts declined. (A rising tide of rooftops
being built for potential shoppers is the primary driver of
retailers demand for space in new developments.)
Our institutional joint venture program has been quite
successful for us, as it marries our property acquisition and
management expertise with the investment capital of major
institutions. We continue our efforts to expand this business.
Many institutions may find that some of their real estate
holdings will be subject to economic stress in this difficult
environment. Our institutional joint venture platform offers
new investment capital for Kimco and draws on our proven
managerial expertise in turning around properties under stress.
The Preferred Equity business provided good returns when
opportunities to invest capital in Kimcos core business were
limited. These returns have consisted of a fixed return of
8-10% plus a portion of the upside, usually 25-50%. In the
current environments, we will curtail Preferred Equity investments
and focus on transactions that provide Kimco with
100% of the upside. In addition, capital deployed by
Retailer Services will be limited to short-term investments
that generate high risk-adjusted yields. This year Retailer
Services was a participant in a group that liquidated the
inventory of Mervyns, Linens-N-Things and Fortunoff.
As part of the refocusing strategy, we will emphasize monetizing
assets that do not fit within these core businesses.
Our investments in non-core activities totaled $1.2 billion
at the end of 2008, and we plan to monetize these investments.
The net proceeds will augment our liquidity through
debt reduction and will be used to acquire shopping centers
opportunistically.
The Core Portfolio
Kimco owns approximately 80 million square feet of gross
leasable area (GLA), of which 58 million square feet is owned
100% and 22 million square feet represents our percentage
interest in various joint ventures. The portfolio is diverse
geographically and by tenant, and largely consists of neighborhood
shopping centers and retail outlets that sell consumer
necessities.
The schedule below lists all of the retailers in our portfolio
that account for more than 1% of our annualized base rent.
We believe that the average base rents in our portfolio are
below market, which provides defensive characteristics to our
cash flows and enables us to re-lease vacant space, over time,
without significant revenue declines. For instance, Kmart
rent averages $5.48 per square foot, substantially below
market rent. In many cases, Home Depot has leased our land
and used their funds to build improvements. As I have stated
on prior occasions, these are very defensive assets that have
meaningful growth potential when the U.S. economy begins
to expand again. We believe that few new projects will be
built in the near term, and the lack of new competing developments
should allow market rents eventually to rise.
Deflation Concerns
The Economic Tsunami of 2008 may result in deflation.
Historically, a deflationary environment, once started, is
not easily or quickly reversed. While retailers will suffer as a
result, deflation tends to increase the value of safe, long-term
streams of income; this is perhaps one reason that the U.S.
10-year Treasury note is yielding less than 3% despite massive
stimulus from the U.S. government. Thus, the secure
long-term leases charted on page 3 (much of which are below
market), should also increase in value.
There is no doubt that the retail environment will be very
difficult for some time to come. Most retail segments are
experiencing sales declines, particularly in discretionary retail
such as furniture, apparel, department store and luxury items.
On the other hand, warehouse clubs, supercenters, health
and personal care stores, pharmacies, and food and beverage
stores which sell items that the consumer needs, rather than
wants, have enjoyed modest increases in sales. We continue
to believe that there is substantial value inherent in our core
portfolio. To replace 80 million square feet of buildings and
land would cost, on a conservative basis, at least $150 per
square foot, or $12 billion. This is substantially higher than
the amount of our present equity and debt capitalization.
In Memoriam
In April of last year our co-founder, Martin Kimmel, passed
away. It was a great honor and privilege for me to be associated
with Mr. Kimmel for over 50 years. I met Marty for the
first time when I was an associate at a law firm in which his
brother, Ed, was a partner. Clients of the law firm were considering
the purchase of a property. I was asked to review the
financial numbers, and Marty was asked to look at the real estate
and advise on the soundness of the project. The property
was located in Sackets Harbor, New York. Sackets Harbor is
west of Watertown on Lake Ontario. It was a bitter cold and
freezing March day, but Marty inspected the buildings, the
basements and the roofs, and on our return I was absolutely
amazed at his grasp and recall of construction issues, leasing
issues and assessment of the market.
In 1958, I was involved in developing a shopping center in
Miami, Florida. The construction and leasing problems were
a nightmare. I remembered Marty from the Sackets Harbor
trip. At that time, Marty was living in California. I called
him and said, When you get back to New York, I would
like to discuss our developing a shopping center in Florida.
There was a long silence on the phone, and then Marty said,
I will be on a plane tomorrow. We met the day after he
arrived and very quickly shook hands on a partnership - and
that handshake was all that was ever needed between us. The
two of us, Kimmel and Cooper, became Kimco. Marty
took charge of the shopping center construction and rented
an apartment in Florida. He was just a ball of fire. He had
enormous energy and worked so hard. Everyone liked Marty,
including the subcontractors, leasing agents, retailers and
other developers. That shopping center was the genesis of
Kimco. All of the associates at Kimco enjoyed being with
Marty and listening to his wit and wisdom, including his
stories and jokes. Martys knowledge of real estate and his
insights into people were a vital part of Kimcos growth. You
would never hear Marty say, Well, thats business! Fair dealing
and keeping your word were at the spine of his being. In
July of 1980, Marty was diagnosed with metastatic prostate
cancer. He visited four different physicians and all of them
had a limited projection of his longevity. Marty would not
accept the forecasts and survived all four of the physicians by
fifteen years. All of us at Kimco will miss him very much.
A Bit of Perspective
Finally, please allow me to offer a bit of perspective. The U.S.
economy is now contending with several major problems,
including rising unemployment, a falling stock market, declining
home prices, and a credit market thats more troubled
than its been in decades. This, of course, has greatly impacted
retailers and retail real estate.
We are well aware of these issues, and are managing through
them. Our free cash flows, like those of other retail REITs,
will be negatively affected for a period of time, and we know
that our cost of capital has become very expensive. But
Kimcos assets, for the most part, provide consumer necessities,
and our tenants, while suffering declining sales, are generally
healthy financially. We have very strong relationships
within the retail world, in-fill locations that tend to be more
resistant to economic weakness, and a substantial portion of
our leases at below-market rents. We are confident that we can
get past this difficult period, while also looking for external
growth opportunities.
We should keep in mind that the population of the U.S.
grows by three million each year, or 30 million of increased
population over 10 years  more than the population of
Canada or Australia. As a consequence of this population
growth and resulting demand for retail space, our portfolio
should substantially increase in value over time  notwithstanding
the retailer retrenchment thats occurring today. We
have a long-term horizon and believe that, during most periods
in America, the wind will be at our backs. Patience and
confidence are in short supply these days, but Americans are a
resilient people and these attributes will soon be restored.
On a personal note, please permit me to acknowledge one
more debt of gratitude. My friend and my partner for over
40 years at Kimco, Michael Flynn, retired from his day-to-day
responsibilities at Kimco on December 31. His contributions
over the years to the growth of Kimco, and to me personally
are innumerable and I am very pleased that Mike has agreed
to continue to serve as a strategic advisor and a full member
of our Investment Committee. Fortunately, for all of us here
at Kimco, Mikes wisdom will remain embedded in the fabric
of our firm.
We continue to be blessed at Kimco with a wonderful team
of enormous talent that, in times like these, is a particularly
valuable asset. Dave Henry has 37-years of experience in real
estate, has been through many cycles, and is very equipped to
deal with the present perturbation in the marketplace. David
Lukes, our Chief Operating Officer, is creative, energetic and
enthusiastic about our shopping center business, and has
the ability, with our associates, to maximize its value while
minimizing risk. We are so lucky to have Mike Pappagallo as
our Chief Financial Officer and the watchdog of our balance
sheet. Mike will be instrumental in helping us to navigate the
shoals and sandbars of todays roiled credit markets.
We have been through many cycles in our 50-year business
history and each time we have weathered the storms and have
emerged stronger with a team ready to sail forward. We are
passionate and prepared to do everything within our power to
achieve the success to which our shareholders are entitled.
Sincerely,
Milton Cooper
Chairman & Chief Executive Officer