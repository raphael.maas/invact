DEAR FELLOW STOCKHOLDERS:
I am pleased to share Intercontinental Exchange�s financial results
for 2018 � our 13th consecutive year of record revenues and record
adjusted earnings per share2
. Our track record of growth is a testament
to our strategy since inception: to identify marketplace inefficiencies
and to enhance the workflows of our customers through the application
of technology and our expertise.
We are relied on to establish more prices across more asset classes
than any market operator in the world, a critical function underpinned
by a purpose of bringing transparency to global markets. We do this
because it is transparency that drives greater efficiency, encourages
broader participation and levels the playing field. In essence, it is
transparency that ultimately transforms markets.
The pursuit and execution of this vision led us first to the energy markets
where, at the time of our IPO on the NYSE in 2005, we were on track to
generate what would ultimately be $156 million in total net revenues.
Today, we are one of the largest exchange groups globally, with total
revenues of $5 billion. We are also one of the most diverse, providing
global risk management services across energy markets, agricultural
commodities, foreign exchange, interest rates, equities and an array of
fixed income and credit products. And our global footprint is unmatched,
with marketplaces and clearing houses in the U.S., Canada, the U.K.,
Europe and Asia. 2018 marked the 10th anniversary of our launch of ICE
Clear Europe which, at the time, was the first derivatives clearing
house established in the U.K. in more than a century. Today, ICE Clear
Europe is the cornerstone of a global platform that provides customers
unrivalled risk management solutions and choice, helps insulate our
markets from political and regulatory change and enables us to swiftly
capitalize on growth opportunities as they emerge around the world.
CONSOLIDATED RESULTS
Before turning to the many exciting opportunities ahead, I�d like to reflect
on the best year in our Company�s history. Consolidated revenues
totaled $5 billion in 2018, an increase of 7% year-over-year. In our
Trading and Clearing segment, revenues totaled $2.4 billion, up 14%
compared to 2017. This strong performance was complemented by
our subscription-based Data and Listings segment, which grew to
$2.6 billion, and was underpinned by organic growth of 6% versus
the prior year.
While we are proud of this performance in 2018, it is ultimately about
the returns we generate for our stakeholders. On these measures, we
continue to excel. Our adjusted operating margin2 was 58% in 2018,
representing the value of our businesses, disciplined expense control
and our pay for performance culture of rewarding our employees for
their contribution to our record results.
This focus on execution yielded record adjusted earnings-per-share2
 of
$3.59, up 21% year-over-year and record free cash flow of $2.3 billion,
up 32%. Consistent with our capital allocation philosophy, we used
that free cash flow to return a record $1.75 billion to stockholders in the
form of dividends and share repurchases, a 23% increase year-over-year.
And critically, we accomplished this while executing over $1 billion in
strategic investments as we lay the groundwork for continued growth.
A GLOBAL RISK MANAGEMENT ECOSYSTEM
We operate a global ecosystem of market infrastructure that serves
the entire trade life cycle: from execution, to clearing and settlement
and the data and listings services that are critical to facilitating those
functions. It is a virtuous cycle, where data drives trading and trading
drives data. In our global derivatives exchanges and clearing houses,
we handled a record amount of futures and options volume in 2018,
results that were propelled by growth in both our commodity and
financial products. And importantly, open interest, which we view as a
key barometer for the health and participation in our markets, was up
10% at the end of 2018, an encouraging sign as we look to 2019.
In our oil markets, results were driven by strength in our core products
such as our Gasoil contract, which is the global benchmark for middle
distillates such as jet fuel and diesel. Gasoil average daily volume, or
ADV, grew 10% versus 2017 and open interest reached a record one
million lots during the year. In addition, our flagship Brent crude oil
contract, which reflects the benchmark for two-thirds of the internationally
traded crude oil, generated its 22nd consecutive year of record revenues.
Brent and Gasoil serve as the foundation for a broader global oil complex
that now offers over 650 contracts and serves the global risk management
needs of traders, investors and commercial participants alike. And with
open interest ending 2018 up 8% year-over-year, our oil complex
appears poised for continued strength in 2019.
In our natural gas markets, the development of transportation infrastructure,
coupled with a boom in U.S. production, is driving globalization. Several
years ago, we began preparing for the liberalization of natural gas and
its evolution beyond the Henry Hub benchmark. Through close
collaboration with our customers, we created our U.S. and Canadian
basis markets, a suite of precise risk management tools that reflect
the commercially relevant supply and demand dynamics of 58 distinct
North American gas hubs. At the same time, Europe is quickly becoming
a critical balancing market for global natural gas, a trend we think is
best illustrated by the 26% year-over-year increase in the ADV of our
European TTF gas contract. With the globalization of the commodity in its
relative infancy, we are excited by the future growth potential of our platform.
Our financial futures business offers an array of global interest rate,
equity and foreign exchange benchmarks. Uncertainty related to Brexit
negotiations and global central bank policy drove another impressive
year in our interest rates business, with ADV in our benchmark Sterling
and Euribor contracts up 9% and 15%, respectively, versus the prior
year. These themes carried over to our equity index complex, which
also experienced volatility related to trade negotiations between the
United States and China, which helped drive ADV in our MSCI index
business up 33% year-over-year in 2018.
At the NYSE, cash equity and option volumes increased 14% and 43%
year-over-year, respectively. We continuously work to bring efficiencies,
transparency and reliability to our U.S. cash equities markets. This
includes technology enhancements, such as our Pillar platform, which
we expect will improve the customer experience across our seven
distinct trading venues. And when combined with our hybrid DMM,
or designated market maker model, we can provide a differentiated
execution experience that reduces volatility, provides tighter bid/offer
spreads and offers an all-in cost to trade that is significantly lower than
the average dark venue.
This array of unique execution offerings is critical to customer acquisition
and retention in our corporate and Exchange Traded Fund, or ETF,
listings business, which also produced another strong year. In 2018,
the NYSE helped 73 corporations raise $30 billion in IPO, or initial public
offering, proceeds. This included distinguished consumer brands such
as BJ�s Wholesale Club Holdings, Inc. (NYSE:BJ) and YETI Holdings, Inc.
(NYSE:YETI), as well as leading technology companies such as Ceridian
HCM Holding Inc. (NYSE:CDAY), Pivotal Software, Inc. (NYSE:PVTL),
SmartSheet (NYSE:SMAR), SolarWinds Corp. (NYSE:SWI) and Zuora,
Inc. (NYSE:ZUO), among many others. The NYSE is the premier destination
for the largest global corporations, listing the last 25 consecutive U.S.
IPOs that raised $1 billion or more, including ADT Inc. (NYSE:ADT), AXA
Equitable Holdings, Inc. (NYSE: EQH) and VICI Properties Inc. (NYSE:VICI). 

Critical to our Trading and Clearing customers is the content, as well as
the distribution of that content, that they rely on to inform their risk
management decisions. At ICE Data Services, we have a broad array
of unique, proprietary data and, in particular, a wealth of pricing
information. Illiquid or obscure, we can provide independent and
reliable price evaluations for nearly any instrument in a customer�s
portfolio. And through the application of technology, such as advanced
algorithms and machine learning, we have leveraged that core expertise
to develop new analytics, new benchmarks and other products that
distill that critical raw information and enhance our customers� pre-trade
and post-trade workflows. As industry participants search for greater cost
efficiencies, these comprehensive services are becoming increasingly
valuable, and are why our Data Services growth continued to compound
in 2018, increasing 5% year-over-year on an organic, constant currency
basis to a record $2.1 billion. Demand for these services is best illustrated
by the performance in our Pricing and Analytics business, which grew 7%
on an organic, constant currency basis, accelerating from growth of 5%
in 2017 and 4% in 2016. Coupled with continued productivity gains by
our global sales force, trends favoring the shift toward passive investing,
a growing demand for data to fuel workflow automation and the push
for transparency by global regulators, our Data Services business is
positioned to continue its strong growth into 2019 and beyond.
BUILT FOR FUTURE GROWTH
The application of technology, information and expertise to solve for
market structure inefficiencies has defined our strategy since inception.
Achieving the desired outcome requires recognition of the opportunity
when it arises and, importantly, a globally connected business positioned
to execute on those opportunities, often in anticipation of customers�
future needs. And as we look to the future, we see a number of asset
classes such as fixed income, indices, mortgages and digital assets
that are in the early days of evolution and that present an opportunity to
continue building upon our strong track record of growth.

� Fixed Income: Over the past decade, dealer inventory levels, which
used to be the primary source of fixed income liquidity, have plunged.
At the same time, the U.S. corporate debt market has nearly doubled
in size, the number of fixed income ETFs has grown six-fold and
over one trillion dollars have flowed into fixed income funds in the
U.S. alone. This shift in market structure has made it difficult for
customers to source inventory and is driving demand for integrated
solutions that support automation and productivity gains.
 We have been aligning our business for this transition for some time.
In 2015 we purchased IDC, the leader in fixed income pricing data
and the provider of a rich set of content critical to fixed income
market participants. In 2018, we acquired two fixed income execution
venues � BondPoint and TMC Bonds � which we have since
re-branded as ICE Bonds. ICE Bonds provides state-of-the-art
execution technologies and rounds out a broader fixed income
offering that provides innovative solutions across the pre-trade,
trade and post-trade workflow.
 An example of our unique position within the fixed income ecosystem
is our ETF Hub which, in collaboration with BlackRock, is expected
to launch in late 2019. The ETF Hub is an innovation that aims to
address inefficiencies in the ETF create-redeem process, a currently
disconnected workflow that sometimes takes up to a full day to
complete. Utilizing our industry-leading pricing and reference data,
our portfolio analytics and our execution technology, we are uniquely
positioned to improve customer productivity and lay a foundation for
the trillion dollar ETF industry to continue its robust growth.
� Indices: A secular shift toward indexation and passive investing
has driven a demand by active managers for more data and richer
analytics as they seek to better understand their benchmarks and 

 deconstruct performance. At the same time, passive managers are
seeking control and flexibility in how they innovate and enhance their
offerings. At ICE Data Services, we have built a differentiated index
offering, with fixed income assets benchmarked against our indices
totaling nearly $1 trillion. While we offer traditional index licensing
services, we have also emerged as a leader in serving the growing
demand for self-indexing and custom index services. We are uniquely
positioned to pursue this multi-pronged approach because our
business addresses the entire customer workflow, a flexibility that
enables us to meet an array of customer needs. It�s a value proposition
that is building momentum, with fixed income ETF assets benchmarked
to our indices increasing 80% year-over-year as of the end of
January 2019.
� Mortgages: In October of 2018, reflecting our strategy of transitioning
inefficient workflows from analog to digital, we purchased the remaining
minority stake in MERS, or the Mortgage Electronic Registry System.
MERS, which is a part of our ICE Mortgage Services business, is a data
repository that tracks changes in servicing rights and beneficial
ownership interests for U.S. mortgage loans � essentially it is the golden
record for the market. Over the past 17 years, loans registered on MERS
have grown from 1 million to over 100 million, with over 75% of newly
originated residential loans registered on the system. We have also
worked to develop new products, such as our eNote Solutions, which
we believe will bring greater efficiency and additional security to the
mortgage ecosystem. And with MERS boasting five thousand banks,
credit unions, servicers, investors and government agencies as
members, ICE Mortgage Services is uniquely positioned to play a
critical role in the evolution of the U.S. mortgage market.
� Digital Assets: We have long studied the developments in emerging
technologies such as blockchain and digital assets. And after robust
dialogue with our customers, the opportunity to bring an institutional
solution to the evolving digital asset ecosystem was clear. In August
2018, we announced a new entity called Bakkt, which plans to enable
institutions and consumers to buy, sell, store and spend digital assets
in a trusted, regulated ecosystem. In its first phase, Bakkt expects to
leverage ICE�s markets, clearing and cybersecurity expertise to deliver
the regulated, institutional-grade risk management, physical delivery
and custody solutions that do not exist today. Bakkt includes a premier
group of partners and investors, such as the Boston Consulting Group,
Microsoft and Starbucks, among others, and is working to build critical
infrastructure and drive adoption of digital assets.
AN EVOLVING APPROACH TO SUSTAINABILITY
While sustainability has been an important part of our company�s culture
since our founding days, our approach and focus on environmental,
social and governance issues continues to evolve. At the New York
Stock Exchange, we offer our listed companies educational resources
on sustainability best practices. We are also a leading exchange
for environmental products and are actively engaged in industry
efforts to support best practices for ESG initiatives. In addition, we

remain committed to diversity across all levels of our Corporation,
with women leaders across many key businesses such as Stacey
Cunningham, the first female President of the NYSE; Lynn Martin,
ICE Data Services President and COO; Hester Serafini, ICE Clear US
President and COO; and Elizabeth King, Chief Regulatory Officer. For
more information on our ESG efforts, I encourage you to read our annual
Corporate Responsibility Report at www.theice.com/esg.
CONTINUED FOCUS ON EXECUTION & STAKEHOLDER RETURNS
Our focus is on developing products and services that leverage our vast
infrastructure and bring real solutions to the markets we operate. This
focus has enabled us to grow from a small startup, which brought
efficiencies to energy markets, to a global provider of comprehensive
risk management solutions across virtually all asset classes. And with
this expansion, the opportunities for future growth have only increased.
Whether it is in data, commodity and financial markets, fixed income,
mortgages or digital assets, we expect to continue to build on our track
record of execution, growth and value creation for our stakeholders.
Thank you for your continued interest and support, and I look forward
to delivering and reporting to you on our progress in the coming year.

With my best regards,

Jeffrey C. Sprecher
Chairman & CEO, Intercontinental Exchange
Chairman, New York Stock Exchange
March 28, 2019
