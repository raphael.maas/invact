Message from the Chairman, President
and Chief Executive Officer
To assure product quality, the metallography laboratory at ATIs new Titanium and Superalloy
Manufacturing Facility in Bakers, North Carolina, slices billet into several sections.
Microstructural analysis is conducted by Laboratory Associate Brandi Baldwin, left, while
other sections, held by Chalmers Allison, lab supervisor, undergo mechanical, chemical
and metallographic evaluaions.
The final conditioning step for billet production at the new Bakers facility is surface and
dimensional inspection. In the foreground, Manufacturing Associate Tony Culp confirms
material composition prior to shipment. Behind him, fellow Associates Allan Brown,
Lucas McDonald and Tony Pope complete similar tasks on other finishing cells.
Solid Performance in 2008; Strong Balance Sheet
ATI earned $5.67 per share with revenues totaling $5.3 billion in 2008.
It was the second best year for revenues and earnings per share in
the Companys history. This was accomplished with global economic
conditions turning down sharply in the fourth quarter 2008, resulting in
deteriorating demand in some key market segments for ATI.
Although ATI is not immune to the global downturn, our financial
position remains strong. ATI has a solid balance sheet, a strong
cash position, good liquidity, and we expect to continue to generate
significant cash flow, even with reduced earnings in 2009. In 2008,
we strengthened our position in key global markets and launched
new production facilities.
At the end of 2008, we had nearly $2 billion in stockholders equity,
$470 million of cash on hand, no significant debt maturing, no
borrowings under our $400 million domestic credit facility, and net
debt to total capitalization of 2%. ATI has an investment grade rating
from the credit rating agencies with a stable outlook. Return on stockholders
equity at the end of 2008 was a respectable 27% and return
on capital employed was nearly 22%.
Primarily as a result of the historic negative returns in the equity and
fixed income markets in 2008, we expect 2009 pre-tax retirement
benefit expense of approximately $140 million. The 2009 retirement
benefit expense is primarily non-cash.
Seeing Through the Fog of Uncertainty
As I write this letter in March 2009, global economic conditions
resulting from the credit crisis have led to near-term uncertainty and
lack of forward visibility for many of our customers. We intend to use
these challenging times to positively differentiate ATI as a uniquely
positioned, diversified, technology-driven, global specialty metals
producer. It requires our best to see through the fog of uncertainty
and risk, and to deploy and execute our plans to effectively compete,
operate in, and navigate within the global recession for as long as it
lasts. Simultaneously, we are looking to continue to position ATI for
long-term success where opportunities may arise.
We are adjusting our operating schedules to market conditions, and we
have prepared contingency plans that can be implemented if current
conditions either decline or improve.
As a result of the transformation we began in 2004, ATI is better
positioned for this downturn than ever before. We have significantly
improved our competitive position in key products and global markets.
We have long-term agreements for our core markets and products, such
as jet engine, airframe, grain-oriented electrical steel for modernizing
power distribution, and medical, and other requirement contracts for
project work in markets such as oil and gas, chemical process industry,
and nuclear energy.

We have taken over $600 million in real costs, before the effects of
inflation, out of our businesses since 2004 and have a gross cost
reduction plan of at least $150 million for 2009, which means taking
on average $420,000 of costs out each day. We are also focused on
improving inventory turns to generate additional cash from managed
working capital as well as to help reduce our exposure to volatile raw
materials costs. Since 2004, we have enhanced and modernized our
business to have unsurpassed manufacturing capabilities, so we have
better and more modern equipment compared to the last business
downturn. We are entering new markets and market segments through
our recently commissioned Market Sector Teams.
We continue to manage for solid earnings and strong cash flow.
Our goal is to come out of the recession an even stronger and
better company.
Diversified Products and Markets
ATI is a diversified specialty metals company. No other specialty metals
company offers the breadth of products and product forms of ATI. Key
to our recent and future success is bringing all the assets and products
that we produce into a unified global marketing strategy. A change in the
philosophy and the transformation of the Company is emphasizing that
we are a unique global specialty metals company. In 2008, we derived
28% of our sales dollars, or $1.5 billion, from direct international sales.
ATIs performance is being driven by the global aerospace and defense,
oil and gas, chemical process industry, electrical energy, and medical
markets which totaled over 70% of sales in 2008.
We like these markets today. We like these markets for the future.
In 2008, we implemented our Market Sector Team concept for further
growth in these key markets. Market Sector Teams focus ATIs full
range of diversified products, technologies, and manufacturing
capabilities to a targeted market or market segment.
By technologies, we mean using our extensive research and
development assets to apply existing alloys to new applications
or develop new alloys and products to meet customers needs.
Our current Market Sector Teams  ATI Aerospace, ATI Defense, ATI Oil
& Gas, and ATI Nuclear Energy  give us opportunities to penetrate the
airframe segment, develop new business in defense, and seek growth
opportunities in the global oil and gas and nuclear energy markets.
We are in the process of analyzing the results of the many
economic stimulus packages that have been announced by the U.S.
government and by the governments of other countries. How fast
these packages are implemented and how much is focused on our
major markets are the questions we are eager to have answered.
Nearly anything in these stimulus packages involving energy
infrastructure would be good for ATI. We like power generation, both
traditional and renewable energy, such as nuclear, wind, solar, and
geothermal. We like power distribution, including the modernization
of the existing distribution grid or building a new grid. We also like the
weatherization of homes and buildings. Any programs focused on
clean water and mass transit would also be good for the consumption
of ATIs specialty metals.
In our 2006 annual report, I wrote to you, The specialty metals we
make are necessary for the infrastructure projects that must be built in
developing countries as well as for the rebuilding of the infrastructure
in developed countries like the U.S. and throughout Europe. In 2007,
I wrote, The U.S. energy infrastructure is aging and must become
more efficient. It needs to be fixed and upgraded. In my view, ignoring
our nations energy infrastructure could lead to a catastrophic event.
We welcome that the U.S. government and many other governments of
the world are now focused on building or rebuilding infrastructure.

Direction and Vision, Which Remain Intact
 Unsurpassed Manufacturing Capabilities
 The Most Advanced Specialty Metals Technology
 Flexibility
 Target Markets and Market Segments
 Global Reach
We are investing in our U.S. operations because we believe that a U.S.
manufacturer can compete in a global economy. To compete globally,
ATI must have unsurpassed manufacturing capabilities, the most
advanced specialty metals technology, flexibility, and global reach. We
believe our customers value ATI as a technology leader that provides a
geopolitically secure source of diversified specialty metals.
We have self-funded $1.3 billion in capital expenditures and asset
acquisitions from 2005 to 2008. We currently expect self-funded
capital expenditures of $450 million in 2009. This is lower than our
previous expectation for 2009 of up to $600 million as we are
adjusting the timing of the projects in light of the challenging market
environment expected this year. ATI is conservative in our cash
management practices and we plan to continue to self-fund our
strategic investments in line with our cash flow. We expect to
generate high returns on these investments similar to the returns
we are generating today.
Our strategy is to invest in the best equipment in the world. It requires
our specialty metals technology and proprietary knowledge to run this
equipment effectively to produce the high quality products demanded
by our customers. We continue to invest in our titanium and titanium
alloys capabilities and we now are integrated from titanium sponge to
the most advanced melting operations to hot working and finishing.
We have expanded our nickel-based alloy and superalloy production
capabilities. We have the latest technology in our titanium and specialty
plate production facility. We have upgraded our cold-rolling operations.
We are adding zirconium sponge capacity to prepare our integrated
zirconium operation for what we see as the upcoming renaissance in
the nuclear electrical energy market. Our Precision Rolled Strip joint
venture expansion in China is ramping up this year.
In September 2008, we announced a $1.16 billion project for our
Flat-Rolled Products segment to consolidate two of our melt shops into
one and build the worlds most advanced and powerful hot-rolling and
processing facility for specialty metals. The grain-oriented electrical
steel and specialty steel melt shop consolidation is expected to provide
significant cost reductions and improved yields beginning in 2010.
We are currently working on the engineering, permitting, and land
and site preparation for the hot-rolling and processing facility.
The advanced hot-rolling and processing facility is a four-year project
that is expected to redefine our flat-rolled products business and
further transform that business into a great company. We expect to
be able to go wider, thinner, flatter, and faster across the range of our
high-value and standard flat-rolled products.
When the project is completed in late 2012, we will be able to better
use our existing finishing assets to provide faster flow times to improve
customer service, reduce costs, reduce our exposure to volatile raw
materials costs, and reduce the amount of cash needed for managed
working capital.

Each of these capital projects advances ATIs technology and
manufacturing leadership in these difficult-to-produce specialty
metals. They also provide the necessary capabilities to manufacture
specialty metals for advanced components needed for next generation
and future generation aircraft airframes and jet engines, electrical
power plants, alternative energy applications, energy saving products,
and other global infrastructure projects.
Couple this new equipment with our ATI Business System (ATIBS)
business processes and the flexibility and talent of our people and
we are well on our way to becoming a great company. A great
company must have the best assets and the most advanced technology.
Great companies need smart people, the latest equipment, and the
most advanced technology to provide customers the best solutions
and unparalleled service.
To be successful in a rapidly changing world, a great company must
demonstrate flexibility from the shop floor to sales and marketing.
Because of our diversified products and markets, and our flexibility
and global reach, ATI has the capability to move our periscope to target
markets and market segments. We can quickly adapt to changing
market conditions and seek opportunities for our specialty metals from
around the world.
A good example of being flexible is our move into the global industrial
titanium market. As we became integrated in titanium, we had the
titanium sponge, electron beam melt, and finishing capacity to grow in
the markets for industrial titanium. In just two years, ATI along with our
Uniti titanium joint venture has grown from a minor source of industrial
titanium products to one of the leading suppliers in the world.
We are looking for opportunities that better position ATI going forward.
We are keeping our strategies and vision for ATI in place. We are
maintaining our self-funded capital strategies while adjusting for
expected cash flow.
We believe that effective management, unsurpassed manufacturing
capabilities, the best technology, and flexibility will be the winning
combination.
Positioned to Capitalize on What We Do Well
We are positioned to capitalize on what we do well. Our future is
being invented by the people of ATI, and our technology depth and
product diversification, both in terms of alloys and product forms.
ATI has long been a leader in specialty metals technology and our
capital investments are aimed at maintaining and enhancing our
mission-critical role.
Our future is being driven by the demands of the worlds citizens 
for mobility, manufactured goods, clean air and water, and a modern
infrastructure  and by our customers who make the products to meet
these demands. ATI and our customers together are focused on
developing the technology and products that enable social progress
and industrial development.
We have a defined view of the future and we have the financial means
and people to continue to move ATI forward.
Value-Based Leadership
Each year, I remind you how we define the guiding principles of
Building the Worlds Best Specialty Metals Company . These
principles drive our strategy. It starts with Value-Based Leadership.
Value-Based Leaders are the true difference in companies that move
people to new levels of achievement and success. I look for the leaders
within ATI to move this company forward by demonstrating these key
attributes as individuals:
 Integrity as the Cornerstone of Leadershipbeing honest and
forthright in everything. Empowering people to trust, communicate,
and take action within established boundaries.
 Accountability for outcomes that ensure the long-term
success of ATI.
 Safety and Health and Environmental Compliance are the
prerequisites to all operations.
 Product Quality and Excellence is demonstrated in
everything we do.
 Technology, Creativity, Learning, and Freedom of people to
reach their individual potential is the culture of the company.
In Building the Worlds Best Specialty Metals Company, we focus on
markets whose prospects are largely tied to long-cycle industries that
are in what we see as the early stages of long-term growth.
In Building the Worlds Best Specialty Metals Company, we aim to
do more, make our products better, and implement and execute
faster through ATIBS. ATIBS drives our lean manufacturing initiatives,
improves safety, quality and yields, further reduces overhead cost
structures and delivers excellent customer reliability and service.
I want to personally thank our stockholders, our Board of Directors, our
employees, and the communities in which we operate our businesses
for their continued support of ATI.

Pat Hassey
Chairman, President
and Chief Executive Officer
