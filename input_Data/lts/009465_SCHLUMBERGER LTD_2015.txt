Schlumberger revenue of $35.5 billion in 2015 represented a drop of
27% from 2014 due to customer spending falling as commodity prices
weakened during the year. Revenue in North America decreased
39%, driven by a land rig count that ended the year 68% lower than the
peak seen in 2014, as well as by pricing pressure that intensified during
the year. North American offshore revenue fell more modestly as rigs
in the US Gulf of Mexico shifted from exploration to development
work, although the overall market in North America was the weakest
for oilfield services since 1986. Internationally, revenue declined
21% as customers cut budgets and pressured service pricing, with
these effects often exacerbated by activity disruptions, project
delays, and cancellations.
In the oil markets, the negative sentiments that had dominated the year accelerated during the fourth quarter
after some optimism earlier in the summer. The impact of OPEC lifting production targets to produce at maximum
rates, combined with production in North America from unconventional resources declining slower than expected
following the April peak, has led to supply continuing to exceed increasing demand. As a result, commodity prices
fell dramatically, with oil dropping to a 12-year low by the end of the year. These weaker fundamentals drove industry
exploration and production (E&P) capital investment significantly lower.
In the natural gas markets, US production grew to a record of 75 Bcf/d as new fields in the US Gulf of Mexico
were brought into production and supplies from unconventional shale gas and tight oil reservoirs continued to grow.
This trend is expected to continue with newly completed pipeline capacity in the northeast United States bringing
new supplies. A relatively mild start to the winter together with North American gas storage levels well above
the five-year average is keeping natural gas prices low. Internationally, European gas demand growth returned to
positive territory. Despite this increased demand, storage levels are at record highs due to ample supply from the
North Sea and Russia, as well as from liquefied natural gas (LNG). Demand rebounded in Asia but remained in
a downward trend overall. As LNG exports from Australia grow, the region is likely to remain oversupplied with
low natural gas prices persisting.
Our financial performance in 2015 was significantly impacted by the large decrease in land activity, particularly
in the US, where the year-end land rig count numbered less than 700 rigs. This created massive overcapacity
in the land market that impacted pricing levels across a broad range of oilfield services. Internationally,
revenue in the Europe, CIS & Africa Area fell by 26% as a result of the weakening Russian ruble, and due to
a drop in exploration activities in the North Sea and Sub-Saharan Africa. In Latin America, revenue declined
22% due to decreased activity in Mexico, Brazil, and Colombia as a result of sustained budget cuts that led to rig count
reductions. Middle East & Asia Area revenue decreased 17% on lower activity in the Asia Pacific region, particularly in
Australia, although this was partially offset by robust activity in the Gulf Cooperation Council countries, particularly
Saudi Arabia, Kuwait, and Oman.
Among the Groups, Reservoir Characterization performance was impacted by sustained cuts in exploration spending,
currency weaknesses, and operational disruptions from exhausted customer budgets that affected Wireline activities.
For the Drilling Group, the drop in drilling activity coupled with persistent pricing pressure, currency weaknesses,
and operational disruptions lowered Drilling & Measurements and M-I SWACO revenues across all geographies, but
most significantly in the Europe, CIS & Africa Area. Production Group performance was mainly affected by the fall in
North American land activity as exhausted customer budgets led to a continued decline in rig count and increased
pricing pressure.
In spite of falling activity, new technology sales remained robust across all Groups during the year, representing
24% of total sales and proving the value that innovative technology can bring when delivered with increased efficiency
and higher reliability. The third quarter saw the pressure pumping stage count for BroadBand* unconventional reservoir
completion services reach almost 12,000 and pass the milestone of generating more than $1 billion in cumulative
revenue since its introduction in late 2013. This performance is more than three times the success of the earlier HiWAY*
flow-channel fracturing technique, which already represented a step change in new product introduction.
In terms of health and safety, our performance improved
further in 2015. Our continued focus on driving and journey
management led to a decrease in our auto accident rate
of more than 8% compared with 2014. One of the major
contributing factors to this improvement has been our
investment in the Schlumberger Global Journey Management
Center network that continues to monitor trips made in
countries that we consider to exhibit medium and high driving
risk. In environmental matters, we have shown that many of
our technologies are playing increasing roles in lowering
environmental impact while optimizing the production and
recovery of nonrenewable resources efficiently and reliably.
These are documented in our first Global Stewardship Report,
which illustrates that our approach to sustainability is rooted
in our global culture.
Despite todays weak market for oilfield services, we delivered
strong corporate financial results in 2015. At the beginning
of 2015, the Board of Directors approved an increase in the
quarterly dividend to $0.50 cents per share. This confidence
has been justified by our generation of $5 billion in free
cash flow during the year, after taking into account capital
expenditures of $2.4 billion and investment in future revenue
streams of $1.4 billion. We have returned $4.6 billion in cash to our shareholders through a combination of dividend
payments and stock buy-backs. In addition, we have spent about $500 million on technology acquisitions that broadened
our portfolio in a number of key products and services. Yet we increased our net debt by only $160 million due to our
ability to generate cash, which is unmatched in the oilfield services industry. This has given us an unrivalled ability to
capitalize on a variety of significant business opportunities.
Among these opportunities was the August announcement of our agreement to acquire Cameron International
Corporation, the company with which we formed the OneSubseaTM joint venture in June 2013. The rationale for this
acquisition lies in our belief that the industrys next technical breakthrough will be achieved through the integration
of Schlumberger downhole reservoir and well technologies with Cameron surface drilling, processing, and flow control
technologies. Further development of instrumentation, software, and automation abilities will enable us to launch
a new era of complete drilling and production system performance. On November 17, we received unconditional
clearance from the US Department of Justice. We expect to close the transaction in the first quarter of 2016, which
remains subject to other regulatory approvals and conditions in our merger agreement with Cameron.
In this uncertain environment, we continue to focus on what we can control. Throughout the year we took a number
of actions to navigate through the current market downturn, including a reduction in our workforce. There is no easy
way to let go of employees and everyone at Schlumberger has been affected by this difficult process. We will be better
prepared when activity rebounds due to arranging a temporary leave of absence for more than 1,800 employees.
In spite of this, we remain constructive in our view of the market outlook in the medium term and continue to believe
that the underlying balance of supply and demand will tighten. This will be driven by growth in demand, weakening
supply as the massive E&P investment cuts take effect, and the size of the annual supply replacement challenge. In
continuing to accelerate the benefits of our transformation program across both our Technologies and GeoMarket
regions in 2016, we believe that we will emerge as a stronger company once the price of oil and the market conditions
in our industry improve.
On behalf of all the Schlumberger people around the world, I want to thank our customers for their confidence and
support. I would also like to personally thank our employees for their commitment and focus during what has been a
very difficult year in the oil and gas E&P industry.
Paal Kibsgaard
Chairman and Chief Executive Officer