
2009 was a significant and exciting year for 
RadioShack during which we continued to 
implement several long-term strategic initiatives:  
to strengthen our financial position, to improve  
the quality of our operations, and to revitalize  
our brand. Our progress required tremendous 
effort from the entire team. Despite challenging 
economic conditions, we improved our margins, 
built our cash position, and controlled our costs. At 
the same time, we continued to make operational 
improvements that reinforced our strategic themes 
of mobility, innovation, and service. Our success in 
each of these areas strengthened our organization. 
In the third quarter, we leveraged this strength by 
launching our new brand platform  The Shack
 which quickly captured the attention of  
consumers and the marketplace and clearly 
highlighted our strategic areas of focus. 
In 2009, we demonstrated the value of our mobility 
focus, underscored the strength of our management 
team, and made good on our commitment to deliver 
profitable growth. Moreover, with an eye to the 
future, we made RadioShack a stronger company 
with a clearer value proposition: a mobile authority 
that combines innovative personal technology, an 
inviting shopping experience, and smart associates 
who help consumers find products and services 
that are exactly right for their needs.
BUILDING OUR FINANCIAL STRENGTH 
RadioShack has internalized a culture of financial 
discipline that enabled us to deliver solid results 
even in uncertain economic times. In 2009, we 
drew on this discipline to build our financial 
position. We took decisive steps to control costs,  
to meet customer needs profitably, and to fortify 
our balance sheet. Our financial results reflect 
these actions. In 2009, we increased net sales  
and operating revenues to $4.28 billion, an 
improvement of 1.2 percent compared with  
2008. We delivered net income of $205 million,  
or $1.63 per share, up 8.2 percent from 2008.  
We overcame slow customer traffic to increase 
same-store sales by 1.3 percent. In addition, while 
we invested in our business throughout the year, 
by year-end 2009 we had a cash balance of  
$908 million, an increase of $93 million compared 
with the prior year.
CONNECTING CONSUMERS AND TECHNOLOGY
RadioShacks more than 6,500 locations in  
the United States and Mexico give us a unique 
competitive advantage in scale, reach, and 
convenience. We continue to capitalize on this 
advantage by cultivating traits that differentiate 
RadioShack in the marketplace, such as an 
appealing store format, a comfortable store  
size, a carefully tailored product assortment,  
and friendly sales experts. Over the past several  
years, we have leveraged these strengths to 
become a leader in high-demand mobile technology 
and connectivity products and services. We have 
aggressively developed our position in the wireless 
market, making RadioShack one of the leading 
indepen dent retailers of postpaid mobile phones. 
Today, our wireless business offers consumers  
an excellent combination of benefits, including  
a carefully selected assortment of top-selling 
devices, three major carriers with a full array of 
service plans, competitive pricing, and an easy 
shopping experience, all backed by friendly and 
helpful sales experts. In 2009, we continued to 
expand in the space, adding T-Mobile as the third 
national wireless carrier in our stores and entering 
into agreements to offer Apples iPhone. 
We also developed other aspects of our mobility 
business, expanding our selection of devices that 
enable on-the-go Internet connectivity, including 
netbooks, e-readers, and gaming devices. In the 
process, we continued to reinforce RadioShack  
as the ideal place for todays consumers to find the 
innovative technology products and services they 
need to keep them connected in this increasingly 
mobile world.
We complemented our business development 
efforts by investing in our store network. We made 
select improvements to help us deliver a consistent, 
high-quality shopping experience to every customer, 
in every location, every day. Among other initiatives, 
we upgraded our store layout to make our selling 
floors more appealing and easier to navigate.  
We created attractive new displays, redesigned 
product packaging, and expanded and adjusted 
our assortment of private brand products. We also 
advanced our commitment to excellent service  
by instituting new training programs designed to 
make our sales teams more attentive, effective, 
and informed. 
THE SHACK IS WHO WE ARE
One of our most valuable assets is the RadioShack 
brand, which is widely recognized by consumers 
around the world. However, like any well-established 
brand, it is only valuable as long as it is relevant, 
serving to instantly remind customers  current 
and future  who we are and what we stand  
for. Following our rapid business progress over  
the past several years, we found that consumer 
perceptions of the RadioShack brand no longer 
aligned with our business reality. In fact, most 
people had a rearview-mirror impression of our 
company that fell far short of our award-winning 
customer service and our position as a leader in
mobile technology. In 2009, we set out to close 
this perception gap by launching a new brand 
platform  The Shack  that communicates 
both the current reality and future aspirations of  
our brand. Since we introduced The Shack, we 
have sparked consumer interest and signaled to 
the marketplace our status as a mobile authority. 
The Shack is who we are. We will continue to 
communicate its positive attributes in the coming 
year through a comprehensive marketing program 
that carefully balances our brand-building goals 
with our sales objectives.
We are also changing consumer perception 
through our sponsorship of Team RadioShack,  
the ProTour cycling team that includes American 
cycling icon Lance Armstrong. We are excited  
about our philanthropic commitment to 
LIVESTRONG, Lance Armstrongs foundation 
focused on inspiring and empowering people 
affected by cancer worldwide. In 2010, we  
plan to establish a growing presence for 
LIVESTRONG, Lance and Team RadioShack  
in our stores.
THE SHACK SETS ITS OWN STANDARD
RadioShack is firmly focused on the road ahead. 
Despite ongoing economic uncertainty, we believe 
2010 will be another exciting year for our company, 
and we are energized by the possibilities.
This year we will build on our momentum to 
strengthen our authority in mobility. We will also 
intensify our efforts to develop our other merchandise platforms, especially those areas in which, 
candidly, we lost impetus during the past year  
as we rapidly built our mobility platform. We have 
specific plans under way to sharpen the rationale 
behind all of our merchandising categories, 
ensuring that our products reflect customer 
demand in every category, and that we accurately 
express The Shacks themes of innovation, mobility, 
and connectivity across all product platforms. This 
will involve expanding and refining our private 
brands, competing to carry new and exciting 
devices from our partners, and extending the 
presence of LIVESTRONG and Team RadioShack 
in our stores. As we meet these goals, we will 
demonstrate that there is no other retail destination 
like The Shack  RadioShack has a peer group of 
one, and we set a singular standard for excellence. 
We move forward with a deep commitment to  
fulfill our long-term mission of creating strong, 
sustainable value for our shareholders.

Sincerely,
Julian C. Day 
Chairman and Chief Executive Officer