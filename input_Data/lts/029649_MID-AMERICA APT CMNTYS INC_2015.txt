TO MY FELLOW SHAREHOLDERS,


MAA HAD A TERRIFIC YEAR OF PERFORMANCE IN 2015.
SUPPORTED BY RECORD AVERAGE DAILY OCCUPANCY OF
96.1%, SAME STORE NET OPERATING INCOME INCREASED
A STRONG 7.2%.
Over the past several years, we�ve focused on steadily recycling capital from older properties into
newer investments that we believe will help support higher long-term earnings. We saw record volume
in 2015. We took advantage of the robust investor appetite for multifamily real estate and sold 21
apartment properties capturing gains of $190 million and generating a 14.1% internal rate of return on
the capital invested. MAA�s balance sheet continued to gain strength during the year as our financial
leverage was further reduced, fixed-charge coverage improved and our unencumbered asset
base grew to 72% of total gross assets. All of these metrics are stronger than at any point in our
22-year history as a public company.

This strong performance supported a solid year of investment return for MAA shareholders with a sector-leading total
shareholder return of 26.5% for calendar year 2015. Our Board of Directors recently increased MAA�s annual dividend
rate to $3.28, a record-high 6.5% increase. Our earnings growth rate has continued to improve and our dividend
coverage is stronger than at any point in our company history.
The outlook for the apartment business remains favorable. The growing impact of the millennial generation and their
propensity to rent their housing, along with the continued recovery in the economy and employment market, support a
growing demand for apartments. While we continue to believe that our industry will retain its long-established cyclical
tendencies, absent a material slowdown in the economy, we should continue to capture rent growth above long-term
trends over the near term.
Our long-term performance goals for shareholder capital and the strategy we employ remain anchored with an objective
to outperform over the �full cycle.� We believe that strength and proper positioning for the down part of the cycle
affords the best opportunities for meaningful value creation. It also protects against value destruction that can
happen during periods of contraction. Demonstrated by our results over the past few years, we use the up cycles to
build platform strength, to fine-tune our systems and take advantage of the strong leasing conditions to deliver solid
performance and results.
Our full-cycle strategy includes focusing on deploying capital across the high-growth Sunbelt region in a balanced and
well-diversified manner. Through investing in both large and secondary markets, balanced across urban, suburban,
inner loop and satellite city locations, we are able to capture a lower level of earnings volatility across the full economic
cycle. Our ability to drive high-quality recurring earnings supports steady dividend growth and the opportunity to
compound value over the long haul.
As a consequence of our active capital recycling efforts over the past five years, we�ve meaningfully repositioned
the portfolio with a higher-end product that appeals to a broad segment of the rental market. With the large-scale
efficiencies and sophisticated capabilities of our operating platform and investment-grade balance sheet, we capture
numerous competitive advantages. Those advantages support an ability to drive long-term investment value surpassing
the performance generally associated with the pricing for apartment real estate in the markets where we operate. At its
core, MAA�s ability to compound value at attractive rates over time is based on a simple principle of investing capital
in apartment real estate at pricing that is at a discount to the capability of our platform to generate outperformance.
We believe our full cycle performance objectives and unique strategy within the publicly-traded apartment REIT sector
have been critical factors in supporting our ability to generate top-tier, long-term results for shareholders. The catalyst
for these solid results not only in 2015 but over the past 22 years is the hard work and commitment that our associates
bring to their roles each day at MAA in surpassing expectations of those we serve.
In closing, I want to express my sincere appreciation for our Board of Directors who provide wise counsel, oversight
and support to our team. In accordance with our retirement policy, Ralph Horn, our co-lead independent director who
has served on our board for the past 18 years, and John Spiegel who served first the former Colonial shareholders and
then the MAA shareholders for a combined 13 years, will be retiring effective with our upcoming shareholder meeting
in May. All of us at MAA are grateful for their many years of dedicated service to our company and shareholders.
H. Eric Bolton, Jr.
Chairman and Chief Executive Officer