Vornados Funds from Operations for the year ended December 31, 2009 was $583.6 million, $3.36 per diluted
share, compared to $813.1 million, $4.97 per diluted share, for the year ended December 31, 2008.
Net Income applicable to common shares for the year ended December 31, 2009 was $49.1 million, $0.28 per
diluted share, versus $302.2 million, $1.91 per diluted share, for the previous year.
Here are the financial results (presented in EBITDA format) by business segment:

In both 2009 and 2008, Other EBITDA has been a large negative, penalizing our results. In both years, the largest
items in Other have been end of cycle losses and impairments in certain mezzanine loans, real estate developments
and marketable securities.(1) Detail of Other EBITDA is shown in Appendix 1.
Our core business is concentrated in New York and Washington, DC, is office and retail centric, and represents
80% of our EBITDA. Cash flow from the core business has never declined, either in total dollars or on a same
store basis. This was true in the difficult 2009 and we expect it to be true in 2010.



Michael D. Fascitelli, CEO
At the Annual Board meeting in May 2009, I was delighted to nominate my partner, Michael D. Fascitelli, to be
Vornados Chief Executive Officer. Its the right time for Mike, who is now 53 years old and who has been our
President for 13 years. There is no better real estate executive in the country than Mike. He is experienced and
seasoned. He is smart, has proven good judgment and measure, and is well known, respected and admired by real
estate industry and finance industry counterparties. And, of course, Mike has perfect knowledge of Vornado, its
business, its assets, its people and its financial structure. We could have no better choice than our own crown prince.
Growth
As is our custom, we present the chart below that traces our ten-year record of growth, both in absolute dollars and
per share amounts:

Absolute dollar increases are quite handsome here; per share growth not so much. All REITs issue shares to grow,
as do we. In the short term we may struggle to out earn the dilution from additional share count. But remember,
many of our real estate investments are intended to create value over, say, a five-year horizon(3) and if one looks at
our growth in same-store cash flows, either on a dollar or per share basis over time, they do us proud.

Share Performance
Here is a chart showing Vornados total return as compared to the RMS(*) Index for various periods ending
December 31, 2009 and 2010 year-to-date.

Acquisitions/Dispositions
Our external growth has never been programmed, formulaic or linear, i.e. we do not budget acquisition activity.
Each year, we mine our deal flow for opportunities and, as such, our acquisition volume is lumpy. Here is a ten-year
schedule of acquisitions and this year, for the first time, dispositions.
Our acquisition volume has been ten times our dispositions  a numerical mismatch and probably a mistake.(4)
Further, like everyone else, we stopped acquiring in 2008 and 2009. There were really no assets to buy in this
period, but looking back, it was a once in a life time opportunity to acquire at panic prices debt and equity securities
of publicly traded real estate companies, including our own.


Lease, Lease, Lease
The mission of our business is to create value for shareholders by growing our asset base through the addition of
carefully selected properties and by adding value through intensive and efficient management. As in past years, we are
pleased to present leasing and occupancy statistics for our businesses.

Capital Markets/Liquidity
In 2009 we executed the following financial transactions:
In April 2009 we sold 17 million common shares at $43 per share, realizing net proceeds of $710 million.
In September 2009 we realized $460 million from the sale of 30-year 7.88% unsecured notes. These notes are
callable by us at par without penalty any time after five years.
During the year we completed five secured financings aggregating $277 million at an average interest rate of 5.83%.
In December we repaid a $393 million cross-collateralized loan secured by 42 retail strip shopping centers. This
loan bore interest at 7.86% and was repaid out of cash earning nil. We plan to refinance a similar retail portfolio this
year. Interestingly, market quotes on this potential financing have been steadily improving both as to rate, sub 6%
expected, and as to proceeds, over $600 million expected. Securitization markets are open again for good product
from first class sponsors, albeit at risk averse LTVs.
During the year, we purchased $2.265 billion (aggregate face amount) of our convertible senior debentures and
senior unsecured notes through tender offers and open market purchases.
At year end we had $1.250 billion of cash, restricted cash and marketable securities, and $1.708 billion undrawn
under our $2.560 billion revolving credit facilities. Our firepower today is similar to that at year-end.
After the deleveraging activity of 2009, our debt was just under 40% of our market value balance sheet.
Days ago, we issued $500 million of five-year senior unsecured notes at an effective interest rate of 4.29%.

Dividends
In the context of the financial crisis and frozen capital markets, we acted to protect our balance sheet. Among other
things, in First and Second Quarter 2009, we paid 60% of the dividend in stock and then in Third and Fourth Quarter
2009, we reduced the dividend while continuing to pay a portion in stock. By First Quarter 2010, recognizing the
fortress nature of our balance sheet and relieved that the eye of the storm had passed, we determined to return to an
all cash dividend in the amount of $0.65 per share. Now this is a lot of dividend activity in a very short period of
time for a company that has, since it became a REIT in 1993, a history of dividend continuity and annual increases
(mirroring our FFO growth), but it was a crisis.
With the world upside down, analysts and large shareholders advised us to reduce our dividend to the lowest amount
possible (i.e., to taxable income)  a heretofore sacrilegious notion. Some analysts and shareholders hated receiving
any portion of the dividend in stock. This confused me. We were careful to pay a sufficient portion of the dividend
in cash for shareholders to pay taxes; therefore, the remaining after-tax stock portion could either be retained for
investment or sold to be the equivalent of an all cash dividend. I, for one, retained my stock portion and realized a
50% appreciation, sort of like receiving a dividend and a half.
We recognize that the statistics show that over long holding periods REIT dividends represent a significant portion
of total return. We, at Vornado, understand that our job is to produce increases in per share results, from our income
producing property portfolio, which will result in rising dividends.
Alexanders
Our 32.4% owned affiliate Alexanders will use up its remaining NOL during 2010. Accordingly, to comply with
IRS regulations that a REIT must distribute its taxable income, Alexanders expects to begin paying a dividend in
2010. Shareholders should note that Alexanders earnings are penalized by holding $544 million in cash, currently
earning nil.
Toys R Us
Toys R Us reported 2009 results earlier this month. Their Form 10-K is available at their website at
www.toysrusinc.com. In 2009, Toys R Us successfully refinanced and extended five different loans and facilities
aggregating $3.6 billion. We, Bain Capital and Kohlberg Kravis Roberts & Co. have owned Toys R Us for five
years. Jerry Storch, CEO, and his team have run Toys R Us since 2006, achieving extraordinary results.
Toys R Us is a seasonal business where the Christmas fourth quarter is all important - and now that the books are
closed on 2009 fourth quarter and the 2009 year, the team outperformed again, producing record results for the
Christmas quarter and the year, all this in a weak retail environment. Toys R Us is indeed very well positioned for
the future.

Vornado Goes Green
Years ago when the first green shoots of sustainability started to sprout (pardon the pun), I was skeptical. To say the
first wave of solar panels, etc., were uneconomic would be an understatement. Time marched on, and the green
movement progressed. Our Companys thinking also progressed. It became clear to us that the societal good was
enormous and, as if that werent enough, I came to believe that in the not too distant future our customers, the
largest tenants in the nation, would favor green buildings. The economics are now obvious, especially for long-term
owners like us, that sustainable practices save money for us and for our tenants.
The real estate industry quickly adopted standards for new green, ground-up development. Today, virtually all new
major construction is built to LEED gold or platinum standards.
But the real challenge and opportunity is to make the hundreds and hundreds of millions of square feet of existing
buildings sustainable in the new world. We are doing just that in our portfolio. For example, we are:
 Producing cleaner energy at our buildings through the installation of commercial cogeneration facilities and
the installation of solar panels;
 Installing sophisticated energy management and submetering systems which enable tenants to track their
energy usage, i.e. the Energy Information Portal recently launched in New York;
 Implementing new technologies that help us smartly predict and manage energy usage, a powerful tool in a
deregulated environment. Our Tenant Service Center in Washington, together with the energy curtailment
program that we are implementing, allows us to shed and shift loads during peak periods;
 Retrofitting existing systems with higher efficiency technologies, including low energy lighting and low
flow water fixtures; and
 Operating our buildings day in and day out to conserve resources and reduce waste through recycling and
green cleaning.
Today, our office buildings consume 35% less energy than the national average.
This year alone, our industry leading work has been rewarded through the following awards and designations:
 Over 20 million square feet of EPA-designated Energy Star buildings;
 6 million square feet of LEED certified space, with an additional 8 million square feet expected to be
certified in 2010;
 Leader in the Light Silver Award from NAREIT;
 #1 listed U.S. real estate company in Maastrict Universitys Global Environmental Real Estate Index and;
 Chicagos BOMA 2009 Green Office Building of the Year awarded to 350 West Mart Center.
We are proud that the U.S. Green Building Council selected our building in Washington, DC at 2101 L Street as
their headquarters.
Please see Sustainability on Vornados website at www.vno.com for more information on our initiatives and
programs to expand and develop environmental best practices across all of our businesses.

State of the Businesses
A detailed review of each of our real estate segments for 2009 is in Item 2 of Form 10-K, pages 21-55, and can be
accessed on our website at www.vno.com.
In the recession plagued 2009, Company-wide leasing volumes were a record. Year in and year out, our leasing
stars out-perform the market and keep our buildings full.(5)
Of the 1,417,000 square feet leased in New York this year, 294,000 square feet was leased to existing tenants
expanding their space in our portfolio (in the entertainment, pharmaceuticals, financial services and legal industries)
and 64,000 square feet was leased to tenants who were new to Manhattan.
With little fanfare, new rents at Crystal City have risen from $31.36 in 2005 to $40.57 in 2009. Thats a wow.
Last year, in an impossible market, we sold 1999 K Street a just completed Class A+ 250,000 square foot office
building for $830 per square foot, the silver medal for highest price per square foot ever in the Washington market.
Century 21 and Kohls are now open in Rego Park, Queens, a 600,000 square foot, four-level urban shopping center.
Costco, Toys R Us, and TJ Maxx will be opening shortly. Add Rego Park to Bergen Town Center, Manhattan
Mall and several other retail developments which have recently opened.
The Mart business and the Hotel Pennsylvania are hurting. Even talented managers cannot overcome tough
markets.
End of Cycle Issues
We do not normally invest in land other than for timely use in development. We are now carrying approximately
$266 million in land - $82 million in several development projects, which had to be postponed for economic reasons
and $184 million in Washington, about half of which was unintended, resulting from a default by a buyer.
We are carrying $180 million in three for-sale residential condominium projects, which will sell out over the next
couple of years.
Like all other large real estate companies, public and private, we have a few situations involving under-water
property level non-recourse debt. In several of these situations, we have entered into negotiations with the lender.
Over the last ten years we have made 24 mezzanine loans, aggregating $1.478 billion. During that period, we have
earned $104 million of net interest income after loan loss charges, producing an IRR of 3.8%. Obviously, these are
unacceptable results. At year end, we had on balance sheet $97 million, net of reserves, in a handful of mezzanine
loans.

Distressed Sellers/Distressed Buyers
Analyst Michael Bilerman recently noted that the anticipated avalanche of distressed sellers has yet to materialize,
thereby creating a class of distressed buyers. Legendary trader Ace Greenberg famously noted that there are no
illiquid markets, just incorrect prices. Here are my observations:
 Today, lenders are not selling at panic distressed prices. This is very different than the 1990s - who can
forget the RTC, etc.
 Sellers, and sellers who were lenders (and not natural holders) will flood to market as prices rise. Assets
will soon trade a plenty (just look at the volumes flooding into special servicing) but in controlled processes
at clearing, but not distressed prices.(6)
The Great Recession and the debacle in residential real estate notwithstanding, commercial real estate is nowhere
near as distressed today as in the 1990s. Then, commercial real estate from coast-to-coast was grossly over built;
year after year see-through office buildings stayed see-through. That is not the case today  over leveraged yes, over
built no.
Office demand is a function of jobs. Experts had predicted that more than 350,000 jobs would be lost in New York
in this cycle. In fact, actual job loss was 186,000 versus 341,000 in the early 2000s recession and 443,000 in the
early 1990s recession. Further, and I believe a precursor to an aggressive real estate recovery in New York, the
financial services industry is enjoying record activity and profits and is now adding jobs.
The comments above relate to CBD office and New York, in particular. Our Washington Office and Retail
businesses have been doing just fine.
This Cycle is (Not) Different
I believe the eye of the storm has clearly passed, recovery is now almost certain, its just a question of how strong
and how long.
There is a lot to learn from the last cycle. It lasted 18 years from peak to peak, 1988  2006. In 1991, the door to
the capital markets creaked open and Kimco went public, followed by a flood of real estate IPOs which continued
for the next decade. The parallel to the current cycle is almost exact - in 2009 and 2010, a scant two years after the
bust, REITS were re-equitized by the capital markets. Using the past cycle as a guide, I believe we have a few years
of fragile recovery and then many years of sustained growth ahead of us.
I predict the previous peak in commercial real estate values will be exceeded.(7) In a deleveraging, slower growth
world, income from hard assets becomes more valuable.
* * *
Park Avenue, the major corporate corridor of New York, comprises about 40 million square feet from Grand Central
to 59th Street and buildings there are on average about 45 years old (which is about the average age of the entire
New York office stock). So heres an idea for the powers that be. To keep regenerating New York, why not upzone
Park Avenue as an economic incentive to tear down old buildings and replace them with new-builds which may be,
say, half again the size. They do this in London, quite successfully. (Park Avenue is only one example.)

In Tribute to Robert H. Smith
Robert H. Smith, age 81, passed away suddenly on December 29, 2009. Bob was a brilliant businessman, a
Vornado Trustee, valued advisor and a dear friend. He was a builder who became a giant, and a legend in
Washington. He was a major philanthropist. Taking the business founded by his father, and together with Bob
Kogod, his brother-in-law and life-long business partner (himself a sitting Vornado Trustee), they created the largest
real estate company in Washington, by far. Vornado acquired their commercial business in 2002. This acquisition
of 12.4 million square feet, including Crystal City with its 6.6 million square feet, formed the nucleus of our
Washington business. This business now contains 18.6 million square feet. My eulogy to Bob is posted on
Vornados website www.vno.com. Mike and I, and the Vornado family, extend our deepest condolences to Clarice
and Bobs family.
***
We wish Wilbur a rapid return to robust health. We salute first-time parents Shannon, Mario, James and Wilbur.
We welcome Bob Minutoli.
Thank You
In 2009, the talented men and women of Vornado, 4,600 strong:
 managed, maintained, cleaned and provided security for over 100 million square feet of commercial space.
 leased 7,155,000 square feet (a record, and that in a recession year) in 892 separate transactions.
 were responsible for the investment of $637 million in development and maintenance capital.
 executed over $4 billion of capital markets transactions (not including assisting in those of Toys R Us
and other affiliates).
 sold assets for $263 million of proceeds with profits of $43 million in 16 transactions.
 produced over 1,500 pages of financial reports and SEC filings; and collected $2.7 billion of revenue from
5,600 tenants.
Mike and I thank each of these talented men and women and especially the hundreds whom we are lucky enough to
interact with during each year. We acknowledge and thank our senior partners  Michelle Felman, David
Greenbaum, Chris Kennedy, Joseph Macnow, Sandeep Mathrani, Mitchell Schear and Wendy Silverstein  there are
none better.

Steven Roth
Chairman