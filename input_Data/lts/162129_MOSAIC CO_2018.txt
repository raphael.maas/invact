Management�s Discussion and Analysis of Financial Condition and Results of Operations
Introduction
The Mosaic Company (before or after the Cargill Transaction, as defined below, �Mosaic�, and with its consolidated
subsidiaries, �we�, �us�, �our�, or the �Company�) is the parent company of the business that was formed through the
business combination (�Combination�) of IMC Global Inc. and the Cargill Crop Nutrition fertilizer businesses of Cargill,
Incorporated and its subsidiaries (collectively, �Cargill�) on October 22, 2004. In May 2011, Cargill divested its
approximately 64% equity interest in us in a split-off to its stockholders and a debt exchange with certain Cargill debt
holders.
We produce and market concentrated phosphate and potash crop nutrients. We conduct our business through wholly and
majority owned subsidiaries as well as businesses in which we own less than a majority or a non-controlling interest,
including consolidated variable interest entities and investments accounted for by the equity method.
On January 8, 2018, we completed our acquisition (the �Acquisition�) of Vale Fertilizantes S.A. (now known as Mosaic
Fertilizantes P&K S.A. or the �Acquired Business�). Upon completion of the Acquisition, we became the leading fertilizer
producer and distributor in Brazil. To reflect the fact that our Brazilian business is no longer strictly a distribution business as
well as the significance of our investment in Brazil, we realigned our business segments (the �Realignment�). Beginning in the
first quarter of 2018, we reported the results of the Mosaic Fertilizantes business as a segment, along with our other reportable
segments of Phosphates and Potash.
After the Realignment, we are organized into the following business segments:
� Our Phosphates business segment owns and operates mines and production facilities in Florida which produce
concentrated phosphate crop nutrients and phosphate-based animal feed ingredients, and processing plants in Louisiana
which produce concentrated phosphate crop nutrients for sale domestically and internationally. As part of the
Acquisition, we acquired an additional 40% economic interest in the Miski Mayo Phosphate Mine in Peru, which
increased our aggregate interest to 75%. These results are now consolidated in the Phosphates segment. The Phosphates
segment also includes our 25% interest in the Ma'aden Wa'ad Al Shamal Phosphate Company (the �MWSPC�), a joint
venture to develop, own and operate integrated phosphate production facilities in the Kingdom of Saudi Arabia. We
market approximately 25% of the MWSPC phosphate production. We recognize our equity in the net earnings or losses
relating to MWSPC on a one-quarter reporting lag in our Condensed Consolidated Statements of Earnings.
� Our Potash business segment owns and operates potash mines and production facilities in Canada and the U.S. which
produce potash-based crop nutrients, animal feed ingredients and industrial products. Potash sales include domestic and
international sales. We are a member of Canpotex, Limited (�Canpotex�), an export association of Canadian potash
producers through which we sell our Canadian potash outside the U.S. and Canada.
� Our Mosaic Fertilizantes business segment consists of the assets in Brazil that we acquired in the Acquisition, which
include five Brazilian phosphate rock mines; four phosphate chemical plants and a potash mine in Brazil. The segment
also includes our legacy distribution business in South America which, consists of sales offices, crop nutrient blending
and bagging facilities, port terminals and warehouses in Brazil and Paraguay. We also have a majority interest in Fospar
S.A., which owns and operates a single superphosphate granulation plant and a deep-water crop nutrition port and
throughput warehouse terminal facility in Brazil.
Intersegment eliminations, unrealized mark-to-market gains/losses on derivatives, debt expenses, Streamsong Resort� results
of operations and the results of the China and India distribution businesses are included within Corporate, Eliminations and
Other. See Note 25 of the Consolidated Financial Statements in this report for segment results.
Key Factors that can Affect Results of Operations and Financial Condition
Our primary products, phosphate and potash crop nutrients, are, to a large extent, global commodities that are also available
from a number of domestic and international competitors, and are sold by negotiated contracts or by reference to published
market prices. The markets for our products are highly competitive, and the most important competitive factor for our
products is delivered price. Business and economic conditions and governmental policies affecting the agricultural industry
and customer sentiment are the most significant factors affecting worldwide demand for crop nutrients. The profitability of
our businesses is heavily influenced by worldwide supply and demand for our products, which affects our sales prices and
volumes. Our costs per tonne to produce our products are also heavily influenced by fixed costs associated with owning and 
3
operating our major facilities, significant raw material costs in our Phosphates and Mosaic Fertilizantes businesses, and
fluctuations in currency exchange rates.
Our products are generally sold based on the market prices prevailing at the time the sales contract is signed or through
contracts which are priced at the time of shipment based on a formula. Additionally, in certain circumstances the final price of
our products is determined after shipment based on the current market at the time the price is agreed to with the customer.
Forward sales programs at fixed prices increase the lag between prevailing market prices and our average realized selling
prices. The mix and parameters of these sales programs vary over time based on our marketing strategy, which considers
factors that include, among others, optimizing our production and operating efficiency within warehouse limitations, as well
as customer requirements. The use of forward sales programs and the level of customer prepayments may vary from period to
period due to changing supply and demand environments, seasonality, and market sentiments.
World prices for the key raw material inputs for concentrated phosphate products, including ammonia, sulfur and phosphate
rock, have an effect on industry-wide phosphate prices and production costs. The primary feedstock for producing ammonia
is natural gas, and costs for ammonia are generally highly dependent on the supply and demand balance for ammonia. In
North America, we purchase approximately one-third of our ammonia from various suppliers in the spot market with the
remaining two-thirds either purchased through a long-term ammonia supply agreement (the �CF Ammonia Supply
Agreement�) with an affiliate of CF Industries, Inc. (�CF�) or produced internally at our Faustina, Louisiana location. The
CF Ammonia Supply Agreement provides for U.S. natural gas-based pricing that is intended to lessen pricing volatility. We
entered into the agreement in late 2013, and we began purchasing under it in the second half of 2017. If the price of natural
gas rises or the market price for ammonia falls outside of the range anticipated at execution of the agreement, we may not
realize a cost benefit from the natural gas-based pricing over the term of the agreement, or the cost of our ammonia under the
agreement could be a competitive disadvantage. Based on the prevailing market prices of natural gas and ammonia as of the
date of this report, the difference between what we would pay under the agreement versus what we would pay for ammonia
on the spot market is not material. However, we continue to expect that the agreement will provide us a competitive
advantage over its term, including by providing a reliable long-term ammonia supply. In Brazil, we purchase all of our
ammonia under a long-term supply agreement with a single supplier.
Sulfur is a global commodity that is primarily produced as a by-product of oil refining. The market price is based primarily
on the supply and demand balance for sulfur. We believe our current and future investments in sulfur transformation and
transportation assets will enhance our competitive advantage. We produce and procure most of our phosphate rock
requirements through either wholly or partly owned mines. In addition to producing phosphate rock, Mosaic Fertilizantes
purchases phosphates, potash and nitrogen products which are either used to produce blended crop nutrients (�Blends�) or for
resale.
Our per tonne selling prices for potash are affected by shifts in the product mix, geography and customer mix. Our Potash
business is significantly affected by Canadian resource taxes and royalties that we pay to the Province of Saskatchewan in
order for us to mine and sell our potash products. In addition, cost of goods sold is affected by a number of factors, including:
fluctuations in the Canadian dollar; the level of periodic inflationary pressures on resources in western Canada, where we
produce most of our potash; natural gas costs for operating our potash solution mine at Belle Plaine, Saskatchewan; and the
operating costs we incur to manage salt saturated brine inflows at our potash mine at Esterhazy, Saskatchewan, which are
affected by changes in the amount and pattern of the inflows. We also incur capital costs to manage the brine inflows at
Esterhazy.
We manage brine inflows at Esterhazy through a number of methods, primarily by reducing or preventing particular sources
of brine inflow by locating the point of entry through the use of various technologies, including 3D seismic surveys, micro
seismic monitoring, injecting calcium chloride into the targeted areas from surface, and grouting targeted areas from
underground. We also pump brine out of the mine, which we impound in surface storage areas and dispose of by injecting it
below the surface through the use of injection wells. Excess brine is also stored in mined-out areas of the mine, and the level
of this stored brine fluctuates, from time to time, depending on the net inflow or net outflow rate. To date, our brine inflow
and remediation efforts have not had a material impact on our production processes or volumes. In recent years, we have
been investing in additional capacity and technology to manage the brine inflows. For example, we have significantly
expanded our pumping capacity at Esterhazy in the last several years, introduced horizontal drilling capabilities, and have
added brine injection capacity at a site that is remote from our current mine workings. These efforts allow us to be more
disciplined and efficient in our approach to managing the brine inflow and to reduce our costs. We are currently developing 
4
the K3 shaft at our Esterhazy mine. Once completed, this will provide us the opportunity to eliminate future brine inflow
management costs and risk.
Our results of operations are also affected by changes in currency exchange rates due to our international footprint. The most
significant currency impacts are generally from the Canadian dollar and the Brazilian real.
A discussion of these and other factors that affected our results of operations and financial condition for the periods covered
by this Management�s Discussion and Analysis of Financial Condition and Results of Operations is set forth in further detail
below. This Management�s Discussion and Analysis of Financial Condition and Results of Operations should also be read in
conjunction with the narrative description of our business in Item 1, and the risk factors described in Item 1A, of Part I of this
annual report on Form 10-K, and our Consolidated Financial Statements, accompanying notes and other information listed in
the accompanying Financial Table of Contents.
Throughout the discussion below, we measure units of production, sales and raw materials in metric tonnes which are the
equivalent of 2,205 pounds, unless we specifically state that we mean short or long ton(s), which are the equivalent of 2,000
pounds and 2,240 pounds, respectively. In addition, we measure natural gas, a raw material used in the production of our
products, in MMBTU, which stands for one million British Thermal Units (BTU). One BTU is equivalent to 1.06 Joules.
In the following table, there are certain percentages that are not considered to be meaningful and are represented by �NM�.