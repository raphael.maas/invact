Dear Shareholder,
2016 was a pivotal year for PMI, reflecting exciting progress in our transf ormation from a cigarette company to one that is focused on Reduced-Risk Products. While our cigarette portf olio continued to drive our income growth, we began to see clear signs of the enormous potential for our RRP portf olio.


Higher-than-anticipated cigarette industry volume declines in select markets had an adverse
impact on our cigarette shipment volume and net revenues. However, the combination of favorable pricing and judicious cost management drove strong currency-neutral financial results and an increase in full-year adjusted diluted earnings per share (EPS) for the first time since 2013, despite continued currency headwinds.

2016 Results
Our cigarette volume of 812.9 billion units in 2016 declined by 4.1% versus the prior year, pri� marily reflecting lower cigarette industry volume, notably in the Asia Region. Some 40% of the de�
cline in our cigarette volume was due to Pakistan and the Philippines, where the volume erosion was concentrated in low unit margin brands that had a limited impact on our bottom line.
A portion of our cigarette volume decline was also due to in-switching to our own HeatSticks from our cigarette brands. HeatSticks volume reached 7.4 billion units, which reflected our maximum manufacturing capacity for 2016.
HeatSticks volume would have been much higher absent this capacity restriction, which has obliged us to limit /QOS device sales in Japan since June.
Marlboro cigarette volume declined by 1.4%, due mainly to Algeria, reflecting significant adult smoker rejection of the 2.0 Architecture for Marlboro Round Taste. However, the brand's cigarette volume performance was positive across many geographies, including growth in the European Union (EU) and Asia Regions.
Excluding Algeria, Marlboro's total cigarette volume increased.
Our international cigarette market share, i.e., excluding China and the U.S., declined by 0.6 points to 27.9%, due primarily to low price Fortune and super-low price Jackpot in
the Philippines. Market share for our remaining brand portfolio was essentially flat. We recorded growing or stable market share in 17 of our top 30 operating companies income (OCl)(1> markets. Aggressive price competition in the low price segment put pressure on our share in some markets, such as Italy and the Philippines.
Marlboro continued its widespread cigarette market share growth last year, with increases in the EU, Asia and Latin America & Canada
Regions. Importantly, the brand's cigarette share growth in Asia was achieved in spite of the impact of HeatSticks growth in Japan.
 
Our other key international brands also grew cigarette market share. L&M, the third-largest international cigarette brand, excluding China, increased by 0.1 point to 3.4%, while Parliament and Chesterfield each grew by 0.1 point to 1.6%.
Net revenues, excluding excise taxes, of
$26.7 billion declined by 0.4% versus 2015, mainly due to an unfavorable currency impact of $1.3 billion. On a currency-neutral basis, net
revenues, excluding excise taxes, grew by 4.4%, driven by favorable pricing of approximately 6% of the prior year net revenues, excluding excise taxes, and the strong performance of RRPs.
Adjusted OCI of $11.1 billion increased by 0.9% versus 2015. Excluding currency, adjusted OCI grew by a strong 10.3%, driven by higher currency-neutral net revenues, excluding excise taxes, and a favorable cost comparison versus 2015, notwithstanding continued investment behind RRPs. Our financial results were strong across all four Regions, with currency-neutral adjusted OCI growth ranging from 8.7% in the EU to 12.6% in Eastern Europe, Middle East & Africa (EEMA). Adjusted OCI margin increased
by 0.6 points to 41.8%, or by 2.4 points to 43.6%, excluding currency, again with gains across all four Regions.
Adjusted diluted EPS of $4.48 increased by 1.4% versus 2015, despite a currency headwind of $0.46 per share. Excluding currency, adjusted diluted EPS increased by a robust 11.8%.
Free cash flow'2l was stable at $6.9 billion, despite unfavorable currency of $340 million. Excluding currency, free cash flow increased by 4.9%, driven by higher net earnings, partly offset by higher working capital requirements.
In September, the Board of Directors ap� proved an increase in our quarterly dividend to an annualized rate of $4.16 per share, reflect� ing its continued confidence in our business fundamentals and future prospects. This marked the ninth consecutive dividend increase since
the company's spin-off in 2008 and, since then, represents a total increase of 126.1%, or a compound annual growth rate of 10.7%.
We continued to access the capital markets at very favorable rates in 2016, raising $3.6 billion over the course of the year and reducing the weighted-average all-in financing cost of our total debt by 0.2 percentage points to 2.8%. The weighted-average time to maturity of our total long-term debt stood at 10.6 years at the end
of 2016, essentially stable compared to the prior year.


Fiscal, Regulatory and Illicit Trade Environment
Our continued pricing strength in 2016 was supported by a fiscal environment for
combustible tobacco products that remained largely rational with either no, or moderate, excise tax increases in most of our major markets.
There were, however, some excep� tions, most notably in Argentina. The new government, facing severe macroeconomic and budget challenges, increased the total
tax incidence for cigarettes to approximately 80%, effectively doubling retail prices on
 
have been raised, or are actively being considered, in other countries.
In May, the English High Court issued a judgment rejecting our claim for judicial
review of the U.K.'s plain packaging legisla� tion. Despite the important principles in this case, we decided not to appeal and instead maintain our focus on developing and commercializing scientifically substantiated RRPs that we firmly believe will ultimately benefit U.K. adult smokers and public health far more than plain packaging.
Despite continued progress, notably in the EU Region and Turkey, illicit trade remains a significant challenge for the industry, particularly in markets such as
Brazil and Pakistan. We are committed to playing our part in combating illicit trade and in 2016 launched a new initiative, "PMI
IMPACT," to help confront tobacco smuggling and related crimes. The centerpiece of the initiative is a council of external independent experts whose members have impeccable credentials in the fields of law, anti-corruption and law enforcement. These experts will oversee grants to enable innovation in three key areas in the fight against smuggling and related crimes - research, education and awareness, and action.

Reduced-Risk Products and Research & Development
2016 was a significant year in the company's development of Reduced-Risk Products.
On all fronts - product development, commercialization, scientific substantiation, third-party engagement and the regulatory and fiscal environment - we reached important milestones.
We were particularly pleased by our ongoing progress in commercializing /QOS. This progress was most visible in Japan, where /QOS was rolled out nationally in April. HeatSticks' national market share reached
 
capacity of HeatSticks to increase from seven billion units in 2016 to more than 32 billion units in 2017, with installed annual capacity of approximately 50 billion units by the end of this year. We look forward to
unlocking the true potential of IQOS once the pressure on HeatSticks capacity eases as
the year unfolds.
Our scientific substantiation program for IQOS advanced at a rapid pace in 2016, during which most of the preclinical, clinical, as well as Perception and
Behavioral Assessment (PBA) studies, using innovative and validated measurement instruments applied for the first time to RRPs, were completed. The totality of the evidence collected to date on IQOS is very encouraging, both in terms of the individual risk-reduction potential and the pre-market assessment of population harm effects
through our PBA program.
Last December, we submitted our Modified Risk Tobacco Product Application to the U.S. Food and Drug Administration (FDA), which, if accepted, will undergo a substantive scientific review. Separately, we remain on track to submit our Premarket Tobacco Product Application to the FDA in the first quarter of this year.
In addition, last year we submitted dossiers to a number of EU Member
States as part of the pre-market notification requirements under the TPD 2. Summaries of the regulatory submissions were also provided to key external stakeholders, which significantly enhanced our scientific engagement. Importantly, we continued to receive external validation of our scientific data, as evidenced by over 50 articles that were published by leading scientific peer� reviewed journals and in the press.
We also made significant progress on the product design, marketing and scientific substantiation of our other RRP platforms.


The promise of a next generation e-vapor product took a meaningful step forward last year with the city test of our Platform 4 MESH vaporization technology in
Birmingham, U.K. We also plan city launches of our Platform 2 and 3 products this year.
With regard to the RRP fiscal environ� ment, HeatSticks are either taxed in all launch markets under a dedicated new excise category or as other tobacco products, depending on national legislation. Long-term, we believe that RRPs should constitute a new, separate fiscal category, taxed in line with their risk-reduction profile.
There has also been notable progress in many markets on RRP regulatory frameworks that address marketing, labeling, public use and the ability to communicate benefits. The process, which is very complex, as RRPs are uncharted
territory for the vast majority of regulators, is exacerbated by the fact that the public health community itself is divided on the subject of harm reduction.
The majority of EU Member States have now transposed the provisions of the
TPD 2 on Novel Tobacco Products (NTPs). Importantly, they have done so without any impediments to RRP commercialization. In addition, rules for the review of scientific data on NTPs are in the process of being adopted by key governments.
Lastly, we continued to build our RRP� related intellectual property portfolio in 2016, with over 130 new patent applications filed, and we expect, once again, to have been among the top 100 filers at the European Patent Office. We currently have more
than 1,500 RRP-related patents granted worldwide and over 3,500 such patent applications pending.

Business Development and Manufacturing Footprint Optimization
During 2016, we made progress on a number of ongoing business development initiatives, notably in Sub-Saharan Africa through our Pan-Africa Entrepreneurs investment vehicle, which combines
the strength of our brands with the local knowledge of our business partners.
We also further optimized our manufactur� ing footprint, with the rightsizing of a number of production facilities and the closure of select ancillary facilities. Critically, we began the process of identifying combustible tobac� co factories that are suitable for conversion
to RRP production, which will further support our footprint optimization efforts.
 
Sustainability
We continued to demonstrate our commit� ment to the environment in 2016 and were awarded "Climate 'N. List" status in the CDP Global Climate Leaders Report for the third consecutive year. This award indicates that we were in the top 9% of the world's largest 2,268 companies that were assessed on their carbon footprint.
Our Agricultural Labor Practices program roll-out remains on track, including the implementation of systematic monitoring
of potential labor issues. In 2016, Control Union, an independent audit firm, assessed the status of the program in Brazil, Greece, Indonesia and Malawi and confirmed our strong progress. Importantly, the disclosure of these reports has been acknowledged positively by Human Rights Watch in the context of their research on child labor in the tobacco sector in Indonesia.
Last year, we published our first commu� nication on progress to the United Nations Global Compact, reporting comprehensively on our sustainability practices across human rights, labor rights, environment and anti� corruption. The cornerstone of our report
is our determination to address the health impact of combustible tobacco products through our RRPs and related business transformation.

The Organization
Enhancing organizational effectiveness and employee engagement remains a top
priority - even more so as we embark on our business-transformation journey to becoming an RRP-focused company.
In 2016, the Top Employer Institute recog� nized PMI for its excellence in professional development programs, workplace environ� ment and opportunities for career advance� ment in a total of 24 countries, compared to 15 in 2015.
While we still have more to do, our ambition to improve gender diversity is yielding positive results, with the percentage of women hired and promoted by PMI increasing in 2016. Nearly half of PMl's
new hires at junior levels of the organization were women.
We would like to express our gratitude to the Board for its invaluable contributions
throughout the year. We welcome its newest member, Massimo Ferragamo, who will add to an already formidable group through his entrepreneurial spirit and deep experience
in the global luxury consumer products business, which includes senior manage� ment and board positions at Ferragamo
 
USA Inc. and its parent company, Salvatore Ferragamo S.p.A.
Last year, Jim Mortensen, Senior Vice President, Human Resources, held firm to his intention to retire after a nearly 30-year
career with Philip Morris that was as rich as it was illustrious. On your behalf, and the many employees whose careers were so positively touched by his leadership, we express our gratitude for all of his extraordinary accom� plishments and wish him and his family a wonderful and well-deserved retirement.

The Year Ahead
Our business fundamentals are robust, supported by our leading brand portfolio and a broadly rational excise tax environment.
The strategic initiatives that we have in place should serve to further extend our leadership position in the combustible tobacco category internationally and continue to drive strong currency-neutral financial performance.
Our RRP portfolio provides us with the single-largest opportunity to significantly accelerate the growth of our business and generously reward our shareholders. More than ever, we are committed to accelerating the success of RRPs and continuing to be the undisputed leader of this new category. Our highly ambitious objective is that RRPs ultimately replace cigarettes to the benefit of
all our stakeholders, and we are determined to deploy the necessary resources - both incre� mentally and through a judicious reallocation from the combustible tobacco category - to make this happen.
This complex undertaking entails a major organizational transformation. We are
approaching it as one company with focus, a high degree of motivation and a new mindset, while preserving the core qualities that have made PMI successful. The task is enormous, but the size of the opportunity is worth every effort. We are confident that our most valuable asset - the employees of PMI - will be up to the challenge, as they have always been.

 

Andre Calantzopoulos, Chief Executive Officer

 
Louis C. Camilleri, Chairman of the Board March 3, 2017
 
