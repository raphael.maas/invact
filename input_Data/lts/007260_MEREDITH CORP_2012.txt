On behalf of Meredith and its more than 3,000 employees, we want to thank you for your
investment in Meredith Corporation. As a shareholder, youve trusted us with your financial
resources, and thats a responsibility we take very seriously.
To those who have been our shareholders for a long time, you are cognizant of our proven ability to
leverage our very strong brands to generate significant and sustainable cash flow over time. To those who
have become shareholders in the past year, we hope this annual report will provide more detail on our
operations, and a better understanding of our key strategic initiatives.
In Fiscal 2012, Meredith executed a series of well-defined strategic initiatives designed to generate growth
in revenues, operating profit and cash flow, and increase shareholder value over time. We also faced some
challenges, including magazine advertising and performance at Meredith Xcelerated Marketing.
Fortunately, our Local Media Group delivered its third consecutive year of growth in non-political
advertising revenues, increasing 6 percent. EBITDA margins grew nearly a full point to 36 percent  the
best since Fiscal 2007. And we kept tight control on total Company expenses, which declined 3 percent
excluding recent acquisitions.
As a result, Fiscal 2012 earnings per share from continuing operations before special items were $2.50,
in line with our previously stated expectations. That compares to Fiscal 2011 earnings per share from
continuing operations of $2.81, before special items, which included $0.38 per share, or $28 million, of
incremental political advertising revenues at our television stations. We will experience the benefit of
political advertising again in Fiscal 2013.
Our Strategic Initiatives
Most importantly, in Fiscal 2012 we took important steps
to position Meredith for continued long-term growth and
success.
Our Fiscal 2012 key strategic accomplishments included:
 Implementation of our Total Shareholder Return (TSR)
strategy. Key elements of the strategy include:
(1) A 50 percent dividend increase resulting in a current
annual dividend of $1.53 per share; (2) A $100 million
share repurchase program; and (3) Ongoing strategic
investments to scale the business and increase
shareholder value over time. Since the launch of our
TSR strategy on October 25, 2011 through June 30,
2012, Merediths stock price increased 28 percent, and
the dividend yielded approximately 5 percent. That
equated to a total shareholder return of 33 percent.
 Acquisition of Allrecipes.com, which doubled our
digital audience and revenues. Allrecipes.com
has strengthened our digital reach and scale with
consumers and advertisers alike. We are currently
No. 1 in audience in the digital food space, and in the
Top 3 in the womens lifestyle category online. We
are actively executing a strategic growth plan for
Allrecipes.com. It includes enhancing the brands video
and mobile content; broadening its social media and
international presence; and generating additional
consumer revenue. Purchase of the EveryDay with Rachael Ray and FamilyFun brands. This gives us greater scale in
the important food and parenthood categories. Both brands are performing ahead of our initial
expectations in terms of revenues and operating profit.
 Launch of additional tablet editions and mobile platforms. We now have 20 brands available in
tablet forms across all major platforms, including the iPad, NOOK, Kindle Fire, Samsung Galaxy and
Google Play platforms. We also have more than 50 mobile apps.
 Extension of our successful brand licensing arrangement with Walmart for the Better Homes and
Gardens line through 2016. Today there are more than 3,000 SKUs of Better Homes and Gardens
branded products available at Walmart stores across the U.S.
 Expansion and monetization of our Local Media Group content through an increase in local news
programming, along with more video content creation such as the syndicated daily The Better Show
 which today reaches 80 percent of U.S. households  and the Digs channel on YouTube.
In Fiscal 2012, we increased the size of our audience across all of our media and marketing platforms to
100 million American women  20 percent greater than the prior year and the largest in our 110-year
history. This provides us with unparalleled reach to American women through our portfolio of strong and
vibrant brands led by Better Homes and Gardens, one of the nations leading media brands.
In Better Homes and Gardens we possess a powerful megabrand that reaches approximately 50 million
consumers across diverse print, digital, video and social media platforms. In Fiscal 2012, we were keenly
focused on expanding its electronic tablet editions and mobile applications, as well as social media and
experiential marketing initiatives.
As we continue to build on the large consumer audiences for Better Homes and Gardens and our other
attractive brands, were confident that advertisers and marketers will increasingly use Merediths properties to reach them.
Finally, the key to our brand strength continues to be our talented and creative employees. This includes
our inventive content creators, innovative sales and marketing professionals, dedicated support groups,
and committed management team. We think Meredith possesses the best workforce in the media
industry, and our 110-year track record of success underscores this belief.
What you will see from us in Fiscal 2013
In our National Media Group, we are focused on executing a multi-pronged strategy that includes delivering comprehensive programs for clients across print, video,
digital and mobile platforms. At the same time, we are also
focusing on broadening the types of advertisers we attract,
including those in the beauty, retail, auto and financial
categories.
Were also expanding our innovative Meredith Sales
Guarantee program, which uses The Nielsen Companys
highly regarded Homescan data and our own 100-million
name consumer database to prove increased product sales at
retail as a result of print advertising in Meredith brands. So
far, we have 13 consumer product brands in the program.
To expand this program, weve formed a new partnership
with IMS Health to add the prescription drugs category to
the Meredith Sales Guarantee, and are working on a similar
data-driven solution for the automotive industry.
We are accelerating development of new strategies to
improve revenue and operating profit growth from our
consumer relationships as well. This includes continuing
to transition more subscription acquisition, renewal andcustomer service activities online. These are
important initiatives for several reasons. First, they
enhance customer service. Second, they create
better opportunities for us to upsell at digital
checkout. And finally, they lower our operating
expenses. Taken together, these factors help us
realize an incremental $5 in operating profit per
digital order over the average life of a subscription.
Our Fiscal 2013 goal is to generate more than
4 million, or about 30 percent, of our total annual
subscription orders digitally.
Were also making several enhancements to
Meredith Xcelerated Marketing, including putting
increased emphasis on delivery of branded
content, mobile and social media marketing
strategies, and strategic analytics. These are our
chief competitive strengths. We lowered our cost
structure and bolstered the senior leadership team
in the creative, sales and marketing, mobile and
social, and operational areas. We are aggressively
expanding new business generation initiatives,
have won several new pieces of business, and are
seeing a strong pipeline for new opportunities.
In our Local Media Group, we will continue to
pursue growth in audience ratings  which helps
strengthen advertising rates. This includes adding
more hours of locally produced programming and
making further enhancements to our digital and
mobile activities. We anticipate a strong political
advertising season, and expect to exceed the
amount generated during the last Presidential
election year.
In closing, we want to reiterate the significant
investments made in Fiscal 2012, as well as our
prospects for strong cash flow generation over
time. We continue to be highly confident in the
strength and resilience of Merediths diversified
business model.
Once again, on behalf of our management team
and all Meredith employees, we want to thank you
for your ongoing support. We have a long history
of prudent capital stewardship, as well as an
ongoing commitment to Total Shareholder Return.
It is our mission and pledge to protect and grow the
value of your investment in Meredith Corporation
over time.
Stephen M. Lacy
Chairman and
Chief Executive Officer
Mell Meredith Frazier
Vice Chairman
