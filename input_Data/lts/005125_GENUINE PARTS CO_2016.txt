Total revenues for Genuine Parts Company were $15.3 billion in 2016, a slight increase compared to 2015, and our 84th year of increased revenues in the 89 year
history of the Company. Net earnings were $687 million and diluted earnings per share were $4.59 compared to $4.63 in the prior year.

During 2016, our team worked hard in every aspect of the business to
overcome the challenging sales environment that persisted in our U.S.
markets. We also enhanced our measures to control rising costs and
manage our assets to further strengthen the balance sheet and generate
ongoing and strong cash flows. Due to these efforts as well as the global
growth initiatives across our operations and geographies, the Company is
well positioned for sustainable growth well into the future. Likewise, our
progress in these key areas support our plans for meaningful investments
in future growth via capital expenditures and acquisitions, as well as the
return of capital to our shareholders in the form of the dividend and share
repurchases.
We are pleased to report a substantial increase in the total value of the
Company in 2016, as we provided our shareholders with a 14.3% total annual
return. Over the past 10 years, our total compounded annual return to
shareholders stands at 10.8%.

FINANCIAL STRENGTH
Genuine Parts Company further improved its financial strength in 2016 with a continued
emphasis on sales and earnings growth initiatives, comprehensive cost controls and effective
management of the balance sheet. Our focus in these key areas produced strong cash flows,
with cash from operations reaching $946 million and, after dividends paid of $387 million and
capital expenditures of $161 million, free cash flow was approximately $400 million. At
December 31, 2016, our total cash on hand was $243 million and total debt of $875 million
was a modest 21.4% of total capitalization.
OPERATIONS
The Companys slight increase in 2016 revenues was driven by acquisitions, while core sales
growth remained under pressure across our four business segments and currency translation
was a slight sales headwind.
The Automotive Group, our largest segment at 53% of 2016 revenues, reported a 1% sales increase
for the year. Each of our four geographic regions in which we operate, the U. S., Canada, Mexico
and Australasia, generated positive total sales increases in their local currencies. Entering 2017, we
are optimistic for stronger automotive sales growth in the quarters ahead, driven by the positive
impact of key sales initiatives and favorable industry fundamentals.
Motion Industries, our industrial distribution business, represents 30% of our 2016 revenues and
total sales for this business were basically unchanged from 2015. With that said, sales trends
for Motion improved as the year progressed and we were pleased to have positive organic
sales growth in the fourth quarter. This improvement correlates to the growing strength in
the industrial economy, as evidenced by economic indicators such as Manufacturing Industrial
Production and the Purchasing Managers Index. In addition, the energy sector, which had
contracted throughout 2015 and the first half of 2016, began to stabilize over the last half of the
year, which is encouraging. EIS, our electrical/electronic distribution segment, represents 4% of
our 2016 revenues and, as with Motion, this business is dependent on the manufacturing segment
of the economy. EIS experienced a 5% decrease in sales for 2016, with their fourth quarter proving
to be the strongest of the year. As we move ahead, both Motion and EIS are well positioned for
profitable growth.
S. P. Richards, our office products distribution business, represents 13% of our 2016 revenues and
had sales growth of 2% for the year. This follows sales increases of 7.5% and 10% in 2015 and 2014,
respectively, and was driven by acquisitions. Our acquisition strategy supports our initiatives
to further diversify the SPR business in the large and growing Facilities, Breakroom and Safety
Supplies (FBS) category. We expect this diversification strategy and the execution of our core
growth initiatives to drive SPRs growth in 2017 and beyond.
ACQUISITIONS
Acquisitions are an important element of our growth strategy. In 2016, we added 19 new
businesses to our operations, which are expected to generate approximately $600 million in
annual revenues. Specifically, we expanded our U.S. automotive network with the purchase of
three automotive store groups, one import parts company and one heavy vehicle parts business.
We also added one import parts company in Canada and five new automotive businesses in
Australasia. At Motion, we expanded our U.S. distribution footprint as well as our extensive
product and service offering with five acquisitions. Our electrical distribution business
expanded their wire and cable footprint with one acquisition, and the office segment
completed two acquisitions in the FBS category, further diversifying their product offering
and customer channels.
We are pleased to have added these high quality businesses to our operations in 2016 and are
encouraged by their future growth prospects. We will continue to pursue additional strategic
acquisition targets throughout 2017.
SHARE REPURCHASES
We returned $568 million to our shareholders through the combination of share repurchases
and dividends in 2016. For the year, we repurchased approximately 2.0 million shares of our
Company stock and as of December 31, 2016, we were authorized to repurchase up to an
additional 4.3 million shares. We expect to continue making opportunistic share repurchases
during 2017 as we view this as a good use of cash.
GPC DIRECTORS
In April of 2017, Dr. Mary B. Bullock and Mr. Gary W. Rollins will retire from our Board of
Directors. Dr. Bullock is the President Emerita of Agnes Scott College and the former Executive
Vice Chancellor of Duke Kunshan University and has served as a Director of our Company since
2002. Mr. Rollins is the Vice Chairman and Chief Executive Officer of Rollins, Inc. and has served on
our Board since 2005, including his service as lead independent director since 2013. We want to
thank both Mary and Gary for their many years of dedicated service, and we extend to them our
sincerest gratitude and appreciation. We will miss their excellent counsel.

MANAGEMENT
In accordance with the Companys long-term succession planning, there were a number of key
management changes and promotions over the past year.
Effective May 1, 2016, our Board of Directors elected Paul Donahue to the position of President
and Chief Executive Offi cer, making him only the fi fth CEO in the 89-year history of our Company.
The Board believes that Paul is uniquely qualifi ed to lead Genuine Parts Company into the future,
having served as President of the Company since 2012, Executive Vice President from 2007 to 2011,
President of the U.S. Automotive Parts Group from 2009 to 2015 and Chief Operating Offi cer of
S. P. Richards Company from 2003 to 2007. Prior to joining the Company in 2003, Paul had a
successful 25 year career in the offi ce products industry. Also on May 1st, the Board appointed
Tom Gallagher, our CEO for the past 12 years, to the role of Executive Chairman. As Executive
Chairman, Mr. Gallagher remains instrumental in shaping the Companys key strategic objectives
in addition to his responsibilities as Chairman of the Board.
Richard T. Toppin was promoted to President and CEO of S. P. Richards Company on January 1, 2017.
A veteran in the offi ce products industry, Mr. Toppin joined S. P. Richards in 2008 as Executive Vice
President Sales and Marketing, and in 2010 was named President and COO. Also effective January
1st, Rob Cameron was appointed as Managing Director and CEO of our GPC Asia Pacifi c business.
Mr. Cameron joined the Asia Pacifi c team 13 years ago and has been a key leader in several of our
Asia Pac businesses over the years. Lastly, Larry L. Griffi n, President of EIS since 2015, was elected
as President and CEO of EIS, effective March 1, 2017. Since joining EIS in 1998, Mr. Griffi n has held
a variety of senior leadership roles and, overall, has 33 years of experience in the supplier and
distributor business environment. Rick, Rob and Larry are talented executives and well deserving
of their expanded roles. We look forward to their future contributions to the success of
these companies.
Three key retirements prompted these promotions. C. Wayne Beacham, S. P. Richards CEO
since 2004, retired from the Company on December 31, 2016, following a long and distinguished
career with the Company that dates back to 1974. Effective January 1, 2017, John L. Moller, the
Managing Director and CEO of our GPC Asia Pacifi c business since he joined the team in 2007,
retired from the Company and assumed the role of Non-Executive Chairman for GPC Asia Pacifi c.
Robert W. Thomas, who joined EIS in 1999 and served as CEO of that business since 2001, will retire,
effective February 28, 2017. We want to thank Wayne, John and Bob for their years of service and
outstanding leadership. We wish them the very best in the years ahead.
Lastly, the Board approved J. Scott Mosteller as Vice President Supply Chain, effective February 15,
2017. Mr. Mosteller was previously the Executive General Manager for Logistics and Technology for
the GPC Asia Pacifi c business, and, before that, was President of our Rayloc division.
Scotts promotion to this position serves to strengthen our corporate supply chain team and
we look forward to his future contributions.
CONCLUSION
While our 2016 results were not as strong as we would have preferred, we enter 2017 a stronger,
more diversifi ed global distributor well positioned for long-term, sustainable growth. Our
balance sheet is in excellent condition, our cash fl ows are strong and our 16% return on invested
capital is well in excess of our cost of capital.
At GPC, we are intensely focused on our plans to drive sales and reduce costs. The four building
blocks of our growth strategy include the execution of fundamental initiatives to drive a
greater share of wallet with our existing customers, an aggressive and disciplined acquisition
strategy focused on geographical as well as product line expansion, the building out of our
digital capabilities in each of our four business segments and the further expansion of our U.S.
and international store footprint. We are confi dent that our focus in these four key areas will
positively impact our sales and drive the steady and consistent growth we strive to achieve
across our businesses.
We want to take this opportunity to express our appreciation to our employees,
customers, vendors and shareholders for their commitment to, and ongoing support of,
Genuine Parts Company.
Respectfully submitted,
Thomas C. Gallagher
Executive Chairman
Paul D. Donahue
President and
Chief Executive Offi cer
Carol B. Yancey
Executive Vice President
and Chief Financial Offi cer
February 27, 2017