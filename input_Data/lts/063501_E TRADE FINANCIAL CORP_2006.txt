                                                 
                                                      Dear Fellow Shareholders,

                                                      At E*TRADE FINANCIAL, we are cautious of companies that tout vision
                                                      in the absence of execution. Our management philosophy concludes that
                                                      success is a product of focus, discipline, and execution. We prove it with
                                                      continued multidimensional growth.

                                                      Growth is the measurement of our success, and in 2006 we achieved
                                                      significant growth and shareholder value by all key metrics. In fact, our

                                                      results eclipsed the performance of the overall financial services indus-
   
                                                      try. Last year was a record-setting year for E*TRADE -- we set records
 
                                                      for the fourth consecutive year, earning record total net revenue of $2.4
                                                      billion with record net income of $629 million. We delivered record full-
                                                      year earnings of $1.44 per share, including acquisition-related integration
                                                      expenses. We achieved record growth in total retail client assets of $195
                                                      billion, with organic growth in customer deposits representing $5.4 bil-
                                                      lion -- exiting the year with a 45 percent operating margin.

                                                      While our results are impressive in their own right, they tell a larger,
                                                      more dynamic story. The financial services market is undergoing funda-
                                                      mental change, characterized by extraordinary opportunities that can
                                                      only be realized by companies whose perspectives truly differ from those
                                                      of traditional firms. E*TRADE FINANCIAL is poised to capitalize on these
                                                      changes because our business model gives us the flexibility to take ad-
                                                      vantage of new opportunities as they emerge in dynamic markets. With a
                                                      disciplined approach to cost reduction and efficient execution, we plan to
                                                      turn new global financial opportunities into good business and solid value
                                                      for our shareholders. This approach drove our success in 2006, and we
                                                      believe it will continue to serve us well.

                                                      In 2006, we put the finishing touches on our integration strategy by maxi-
                                                      mizing existing platforms and resources to grow the business efficiently
                                                      and broadly while minimizing capital outlay. Every milestone achieved
                                                      in 2006 is a testament to our ability to maximize existing synergies. The
                                                      power of E*TRADE lies in integrating operational capabilities with prod-
                                                      ucts and services to deliver an extraordinary customer experience.

                                                      The acquisitions of Harrisdirect and BrownCo further completed our
                                                      transformation into a diversified financial services company, and we
                                                      once again demonstrated our ability to quickly integrate employees and
                                                      processes by completing these tasks and welcoming our new custom-
                                                      ers ahead of an extremely aggressive schedule. We retained a higher
                                                      percentage of customers than expected, and those customers brought
                                                      additional assets to E*TRADE. We also acquired Retirement Advisors
                                                      of America, a registered investment advisory firm that manages over $1
                                                      billion in assets and nearly 1,300 client relationships. This acquisition will
                                                      help us build a nationwide network of wealth managers, which will ef-
                                                      ficiently deliver localized advice to clients nationwide.



                                                     Our $40 million investment in customer service in 2006 resulted in an
                                                     average 69 percent improvement in retail customer satisfaction, as
                                                     well as recognition as a finalist in The American Business Awards for
                                                     Best Customer Service Organization. Whether customers prefer to invest
                                                     on their own, seek advice when necessary, or look to E*TRADE for
                                                     full portfolio management, we are proud to say that they can find an-
                                                     swers and assistance faster and more effectively than ever before --
                                                     at www.etrade.com, over the phone, through email, or by visiting an
                                                     E*TRADE Financial Center.

                                                     An essential element of extraordinary customer satisfaction is a cus-
                                                     tomer's experience with E*TRADE services and products. Last year, we
                                                     delivered major enhancements to our flagship product, E*TRADE Com-
                                                     pleteTM. We introduced our E*TRADE Complete Protection Guarantee
                                                     -- the industry's first solution that provides retail customers with com-
                                                     plete fraud coverage, as well as bill payment and privacy protection. We
                                                     delivered new risk analysis and money management tools for optimizing
                                                     customers' holdings, cash, and credit through their E*TRADE Complete
                                                     Investment Accounts. Retail trading customers gained new automated
                                                     trading tools. Enhanced conditional order capabilities make it easier for
                                                     them to pre-program strategies and trade with discipline. An innovative
                                                     retail futures offering now provides access to more than 200 products
                                                     through the top five worldwide futures exchanges, with low commissions
                                                     and assistance from a dedicated derivatives service team.

                                                     Futures were not our only worldwide interest in 2006. E*TRADE opera-
                                                     tions grew globally as well, expanding the E*TRADE web site and trading
                                                     features to France, Italy, and United Arab Emirates. In 2006, we began
                                                     working with IL&FS Investsmart Limited, exploring opportunities for pro-
                                                     viding India's investors with access to international markets and paving
                                                     the way for our global customers to gain new investment opportunities in
                                                     India. Our new office in Shanghai is helping us deepen our strategic busi-
                                                     ness relationships in China. We continue to identify opportunities around
                                                     the world where we can serve growing streams of new customers.

                                                     As we move into 2007, we will continue executing our growth strategy
                                                     by focusing on winning and retaining key target customer segments.
                                                     We expect to take advantage of the unique opportunity that we have
                                                     created on the global front, as well as to tap new areas of opportunity,
                                                     such as our corporate client base. Approximately 2,100 E*TRADE corpo-
                                                     rate clients represent close to $50 billion in un-invested assets -- most
                                                     commonly in stock options. Last year we retained 23 percent of clients'
                                                     exercise proceeds, up from 17 percent in 2005, and we intend to grow
                                                     this business.

                                                     Two high-value corporate changes also occurred last year. We are
                                                     pleased to say that in 2006 E*TRADE joined the NASDAQ Global Select
                                                     Market under the symbol ETFC. We believe that NASDAQ is a true in-


                                                     novator whose value proposition parallels our own. We are proud to be
                                                     listed with NASDAQ and look forward to a long, mutually rewarding as-
                                                     sociation. Also last year, the Federal Reserve Board approved E*TRADE
                                                     Clearing to operate as a subsidiary of E*TRADE Bank, enabling us to
                                                     complete the integration of our balance sheet. This new structure pro-
                                                     vides us with more efficient capital allocation and lower cost of funding.

                                                     It's wonderful to be recognized by the industry for excellent perfor-
                                                     mance, and in 2006 E*TRADE's senior debt rating was upgraded by the
                                                     Dominion Bond Rating Service, while Moody's upgraded E*TRADE's
                                                     Bank and Corporate debt rating. E*TRADE also received numerous
                                                     financial and technology industry awards in 2006. We're pleased by the
                                                     accolades and view them as validation of our disciplined approach to
                                                     capitalizing on opportunities.

                                                     When we began, we committed ourselves to product and service innova-
                                                     tion, disciplined management, and maintaining the highest levels of integ-
                                                     rity with shareholders. We believe that these commitments are the core
                                                     components of enduring value. I feel confident that this formula for value
                                                     continues to yield the growth our shareholders expect. We thank you for
                                                     your support and are honored to have the opportunity to continue to
                                                     earn it.




                                                     Mitchell H. Caplan
                                                     Chief Executive Officer and Director