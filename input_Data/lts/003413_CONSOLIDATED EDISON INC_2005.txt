To Our Shareholders

                                               and corporate expansions fueled the robust
Strength
                                               economy -- in New York City, in West-
In 2005, our focus on the core business
                                               chester County, and in the seven counties
of delivering energy allowed Con Edison
                                               west of the Hudson River in O&R's
to meet the power needs of the world's
                                               service territory.
most demanding marketplace -- and to
                                                 In addition, life in the 21st century is
achieve solid results for our shareholders.
                                               becoming more energy intensive. Whether it's
  As the New York region's economy
                                               the increased deployment of computers and
continued to expand at a healthy rate,
                                               servers or the greater use of air conditioners,
our program of infrastructure investments
                                               high-definition televisions, and cell phones,
-- more than $1.6 billion in 2005 alone --
                                               people are consuming more electricity as
provided the operational strength necessary
                                               they go about their daily routines. While the
to meet the rising demand for energy.
                                               population in our service area grew by 6.5
The company's long-standing financial
                                               percent between 1994 and 2004, electric
strategy of making careful investments and                                                       
                                               consumption grew by 20 percent.                  
maintaining a strong balance sheet and
stable credit ratings enabled us to increase
the dividend we pay to shareholders for
                                                  Leadership Transition
the 32nd consecutive year.
                                                  Con Edison places a high value on stability and continuity in all its endeavors, and
Focused on Delivering Energy                      particularly in the realm of management succession. In February 2006, the board of
The centerpieces of Con Edison are                directors of Consolidated Edison, Inc. elected Kevin Burke, president and chief execu-
our two regulated utilities: Con Edison           tive officer, as chairman of the board. Mr. Burke succeeds Eugene R. McGrath, who
Company of New York, and Orange and               retired effective February 28, 2006. Mr. McGrath, who has served as Con Edison's
Rockland Utilities (O&R). Con Edison of
                                                  chairman since 1990, will remain on the company's board of directors.
New York, which serves the five boroughs
                                                    Mr. McGrath, a 43-year veteran of Con Edison, guided the company through major
of New York City and parts of Westchester
                                                  changes in the energy industry, most notably deregulation and the company's
County, has more than three million
                                                  entrance into competitive energy markets through the establishment of several
electric customers, more than one million
                                                  competitive businesses. He also oversaw the company's largest investment program
natural gas customers, and approximately
                                                  to meet the energy needs of New Yorkers. During his tenure, Con Edison maintained
1,800 steam customers. O&R serves almost
                                                  its position as the most reliable electric utility in the United States.
300,000 electric customers and 125,000 gas
                                                    On September 1, 2005, Mr. Burke became president and chief executive officer
customers in Orange, Rockland, and Sulli-
van counties in southeastern New York State       of Consolidated Edison, Inc. Since joining the company in 1973, he has held senior
and adjacent sections of New Jersey and           operating positions at Con Edison of New York and Orange and Rockland Utilities
Pennsylvania. Together, they employ more          (O&R), and, beginning in September 2000, served as president and chief operating
than 14,000 people.                               officer of Con Edison of New York. As part of an orderly transition, veteran executive
   The growth in both electricity and gas
                                                  Louis Rana succeeded Mr. Burke as president and chief operating officer of Con
deliveries -- our core businesses -- is
                                                  Edison of New York. Robert Hoglund, our former senior vice president of finance,
connected to the economic growth in our
                                                  was appointed chief financial officer. Joan Freilich, formerly chief financial officer,
service area. The economy of our region
                                                  was named vice chairman.
is remarkably diverse and vibrant. And last
year, factors such as the growth in housing


Our operating performance and a healthy local economy
produced solid results in 2005 for our shareholders.



                                   As a result of these trends, demand for           for the S&P electric utilities index, and 2.8
                                   both electricity and gas continues to rise.       percent for the S&P 500. Con Edison is the
                                   Last summer, which was warmer than                only utility in the S&P 500 with 25 or more
                                   usual, both Con Edison of New York and            consecutive years of dividend increases. In
                                   O&R experienced a number of record                January 2006, for the 32nd straight year, we
                                   peaks in customer usage of electricity.           increased our dividend. Raising our divi-
                                   In addition, both utilities set new cold-         dend to an annualized $2.30 per share is
                                   weather electric peaks during the winter          a tangible measure of our commitment to
                                   of 2004-2005, despite the fact that the           shareholder value.
                                   season as a whole was relatively mild.              We work constructively with regulatory
                                   Over the next five years, we expect that          agencies and other constituents to com-
                                   Con Edison of New York's electricity peak         municate the importance of Con Edison
                                   usage will grow about 1.5 percent per year        having the necessary resources to continue
                                   and that O&R's peak customer usage will           investing in the region's energy infrastruc-
                                   grow about 2.7 percent annually.                  ture. Last year, Con Edison of New York's
                                     In Con Edison's vision, the delivery of         new electric rate plan went into effect.
                                   energy demands an unwavering commitment
                                                                                     Competitive Businesses
                                   to reliability, and to excellent environmental,
                                   health, and safety performance. Last year, we     In addition to our core energy delivery
                                   saw continued improvement in our efforts          business, Con Edison also operates several
                                   to conserve natural resources; provide a safe,    competitive energy businesses: Con Edison
                                   healthy workplace; and minimize the               Development, Con Edison Energy, and Con
                                   environmental impact of our operations.           Edison Solutions. These companies generate
                                                                                     and deliver energy, sell energy in wholesale
                                   Financial Results                                 and retail markets, and provide energy-
                                   Our operating performance and a healthy           related services consistent with our support
                                   local economy produced solid results in           for competitive energy markets access and
                                   2005 for our shareholders. Net income in          consumer choice.
                                   2005 was $719 million, or $2.95 per share,           Following a comprehensive review, we
                                   compared with $537 million, or $2.28 per          concluded that the fiber optic network
                                   share, in 2004. Earnings from continuing          constructed by our telecommunications
                                   operations were $732 million, or $3.00            subsidiary, Consolidated Edison Communica-
                                   per share, compared with $549 million,            tions, would have greater value and serve its
                                   or $2.33 per share, in 2004.                      customers more effectively within a broader
                                      Including reinvested dividends, total          telecommunications business environment.
                                   return to Con Edison shareholders in 2005         Consequently, in December 2005, an
                                   was 11.4 percent, compared with 17.7 per-         agreement was reached to sell the company
                                   cent for the S&P electric utilities index, and    to RCN Corporation for $32 million,
                                   4.9 percent for the S&P 500. Over the past        subject to certain adjustments at closing.
                                   five years, Con Edison's stock has returned
                                                                                     The Next Generation
                                   57.4 percent, compared with 30.6 percent
                                                                                     Con Edison's energy infrastructure has
                                  For more than 180 years,                           always played a crucial role in supporting
                                                                                     the economic prosperity of metropolitan
                                 Con Edison has remained                             New York and sustaining the quality of life
                                                                                     that people living and working in the
                                 dedicated to continuity --                          region have come to expect. To carry out
                              in service to our customers,                           this vital mission, we expect to invest more
                                                                                     than $5.3 billion in our infrastructure over
                         in fiscal strength, in leadership,                          the next three years.
                              and in partnerships with our                             Planning for the future requires us to do
                                                                                     more than simply adhere to existing plans.
                                     vibrant communities.                            The energy industry, by its very nature, is




                                                          one of evolving challenges. At present, for       to our core mission -- delivering energy to
                                                          example, the combination of tight supplies        a demanding, dynamic marketplace. Every
                                                          and increased demand for natural gas in the       day, our team arrived at work focused on
                                                          United States has increased the cost of           providing excellent service. But our plans
                                                          electricity, gas, and steam dramatically.         and efforts also revolve around how Con
                                                            As a result, national attention is once again   Edison can build and serve the next
                                                          focused on energy. In advance of winter           generation of New Yorkers. We're confident
                                                          2005-2006, Con Edison took steps to secure        that by intently preparing for the future, by
                                                          adequate supplies of natural gas and intro-       continually improving operations, and by
                                                          duced an education campaign that provided         managing our finances soundly, we will
                                                          customers with information on energy              position our company to meet the energy
                                                          conservation, budget programs, and fuel-          needs of generations of New Yorkers to
                                                          assistance programs.                              come -- and continue to deliver solid
                                                            We're also working to develop the tools,        returns to you, our shareholders. We appre-
                                                          technologies, and processes that will             ciate the support you have shown for Con
                                                          enable us to maintain reliability and improve     Edison and we pledge to work hard in the
                                                          service in the future. A broad range of           coming years to continue to earn your trust.
                                                          initiatives that is currently under way will
                                                          train the next generation of energy-industry
                                                          professionals. And because our future as a
                                                          business is directly linked to the future of
                                                          the communities we serve, Con Edison
                                                          invests in strategic partnerships with not-       Kevin Burke
                                                          for-profit and other groups aimed at im-          Chairman, President, and
                                                          proving the quality of life for our customers.    Chief Executive Officer

                                                          Remaining Focused,
                                                          Remaining Strong
                                                          For more than 180 years, Con Edison has
                                                          remained dedicated to continuity -- in

                                                          service to our customers, in fiscal strength,

      in leadership, and in partnerships with our

                                                          vibrant communities. At Con Edison, 2005
               was in some ways a year of transition, with
       a number of changes in top leadership. But

                                                          it was also a year in which we remained true

