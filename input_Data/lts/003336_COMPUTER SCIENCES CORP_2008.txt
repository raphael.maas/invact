Fiscal 2008 was a year of solid performance by CSC. We achieved revenues of
$16.5 billion, up 11%, and delivered free cash flow, consistent with our guidance.
We set a new record for total announced business awards within our North American
Public Sector line of business of $11.3 billion. Overall, our balance sheet is solid, with
approximately $700 million of cash and cash equivalents on hand at year-end.
With an enviable set of world-class customers, 89,000 dedicated employees
and operations in 55 countries, CSC is a formidable global competitor  a
remarkable company with significant long term potential. Our 2,500 clients rely
on CSC to deliver, and they tell us that we are second to none in the delivery
of mission critical services and solutions.
Building upon this foundation of delivery
excellence, we inaugurated Project Accelerate,
our global growth initiative designed to
invigorate and position our business for a
new level of success. Project Accelerate is
focused on long-term, sustainable profitable
growth for CSC.
CSC is a global technology and business
services company and going forward, will
be operating in three lines of business: North
American Public Sector, Global Outsourcing
Services, and Business Solutions and Services.
North American Public Sector (NPS) ended
fiscal 2008 as a $5.8 billion business, comprising
35% of CSCs revenue. We are fully
dedicated to sustaining and growing our core
U.S. federal government business, investing
in key market segments to achieve higher
growth, and expanding our business in U.S.
state and local governments and Canada.
NPS, with its strong market position and
extensive client footprint across the U.S. federal
government, provided a record $11.3 billion
in announced new-business in fiscal 2008.
Record new business announcements within
our North American Public Sector this year
demonstrated the wide array of capabilities
and services employed by CSC to support
the U.S. public sector, including awards
with the U.S. Environmental Protection
Agency; U.S. General Services Administration;
Internal Revenue Service; and National
TO OUR
SHAREHOLDERS
Fiscal 2008 was a year of solid performance by CSC. We achieved revenues of
$16.5 billion, up 11%, and delivered free cash flow, consistent with our guidance.
We set a new record for total announced business awards within our North American
Public Sector line of business of $11.3 billion. Overall, our balance sheet is solid, with
approximately $700 million of cash and cash equivalents on hand at year-end.
With an enviable set of world-class customers, 89,000 dedicated employees
and operations in 55 countries, CSC is a formidable global competitor  a
remarkable company with significant long term potential. Our 2,500 clients rely
on CSC to deliver, and they tell us that we are second to none in the delivery
of mission critical services and solutions.
Security Agency. Other awards included Net-
Centric Enterprise Services for the Defense
Information Systems Agencys Service-
Oriented Architecture foundation program;
the USAF Eastern Range Technical Services
contract; facility support services at the NASA
Johnson Space Center; IT support services
for the U.S. Department of Homeland Security
Citizenship and Immigration Services; and
an agreement to provide IT services for the
U.S. Department of Health and Human
Services Centers for Medicare and Medicaid
Services.
Global Outsourcing Services (GOS) is a $6.7
billion business responsible for the delivery of
outsourcing services to our clients, globally.
Overall, GOS comprises 41% of the CSC
portfolio. Applications outsourcing comprises
about 30% of its business base, the remainder
being infrastructure outsourcing.
We created Global Outsourcing Services
to enable CSC to manage and deliver our
large IT outsourcing engagements through
a single, consistent, globally leveraged
model. Additionally, we expanded our
market focus to include midsize deals
(transactions ranging from $50 to $350
million in total contract value). The midsize
initiative within GOS added significant new
business in its first year, contributing both
revenue and margin growth, while we also
continue to extend our traditional strength in
the large-scale global outsourcing market.
In fiscal 2008, we extended our valued relationship
with Sun Microsystems Inc., reaching
a global IT applications management services
agreement under which we will provide
all applications development and support
services for Suns business applications
portfolios. Other signings include a five-year,
multimillion-dollar IT services contract with
Eaton Corporation, and an IT outsourcing
contract extension with General Dynamics
Armament and Technical Products, a business
unit of General Dynamics.
In Europe, we agreed to a five-year IT outsourcing
contract renewal with TrygVesta,
the second-largest insurance company in the
Nordic region; and a seven-year IT services
contract with CACEIS, one of Europes leading
financial services providers.
New business awards within the midsize
market include an agreement with the
University of Pennsylvania Health System,
and, in Europe, the UK Network Rail system
and an outsourcing agreement with Det
Berlingske Officin, a major newspaper
publisher in Denmark.
Business Solutions and Services (BS&S)
comprises 24% of CSC, delivering $4 billion of
revenue. It is our fastest growing and most
profitable sector. We address our markets
through six industry verticals, providing
industry specific solutions and services,
including industry specific business process
outsourcing as well as intellectual property
(IP)-based software solutions. BS&S offers
customers a full range of consulting and
systems integration services as well as a
broad portfolio of India based solutions
and services.
New services and solutions business awards
included Western & Southern Financial Group
and a 10-year business process outsourcing
(BPO) contract with Wilton Re. We also
continued our important relationship with
Swiss Re, the worlds leading reinsurer,
with a seven-year contract extension. In
addition, we signed a three-year IT services
contract with international oil and gas group
Woodside, Australias largest publicly traded
oil and gas exploration and production
company.
A key component of our business solutions and
services strategy is to bolster our vertical
industry growth potential through acquisition,
and we made two important acquisitions
during the year: Covansys Corporation and
First Consulting Group (FCG).
Approximately 8,000 employees from our
Covansys acquisition augment our strategic
outsourcing and technology solutions in
the healthcare, financial services, retail
and distribution, manufacturing, telecommunications
and high-tech industries. This
acquisition accomplishes the important
goal of bolstering our offshore resources
in India, while complementing our global
vertical industry offerings and enhancing
our global footprint.
The acquisition of First Consulting Group
deepens our expertise and intellectual
property associated with the key healthcare
vertical market and expands our global
delivery capabilities, including important new
capability based in Vietnam. The addition of
FCG to CSC significantly strengthens our
healthcare offerings while expanding our
healthcare presence in the United States,
Europe and Asia.
With its three lines of business, CSC is one
of the very few companies to successfully
offer a balanced portfolio of solutions and
services across both the public and commercial
sectors. We believe this portfolio
positions CSC for sustained long-term growth
and provides CSC and its investors substantial
mitigation of cyclical economic risk. The
performance of our public sector business
is largely independent of global economic
cycles. Our outsourcing business provides
profitable, long term annuity revenue and,
generally, operates well in economic downturns
as potential clients then look to lower
their internal operating costs. Our BS&S
sector enables growth and enhanced
profitability whenever businesses seek
to improve their competitive advantage.
The combination of these three lines of
business provides CSC growth, long-term
stability and competitive advantage.
CSC is a global company  we deliver to the
world from around the world. As such, we
are especially proud of the progress made in
fiscal 2008 to achieve a balanced, globally
diverse workforce. Exiting fiscal 2008,
29% of CSC employees worked for the
North American Public Sector, 25% worked
in the Americas, 22% of our staff worked
in EMEA, and 24% of our population worked
in Asia/Pacific.
We have chosen to address our markets and
our customers through six industry verticals.
Four of these verticals were launched in
fiscal 2008 and the remaining two will be
launched this fiscal year (2009). Four of
these six verticals have revenues in excess
of $2.5 billion. The remaining two are billion
dollar businesses, providing sufficient scale
to ensure that CSC will be a formidable
competitor in each of our markets. By facing
our customers vertically, we hope to improve
the breadth as well as the depth of the
CSCs value proposition, improving organic
growth.
We created the Global Office of Sales and
Marketing to drive our sales and marketing
culture. Accordingly, fiscal 2009 will see
the launch of a new branding campaign to
reposition and improve CSCs brand equity
across all of our stakeholder communities.
During fiscal 2008 we evaluated and
redesigned the incentive programs of the
company to align with our strategic objectives.
Cash flow performance has been more
heavily weighted in our annual incentive
programs. Sales incentives have been revised
to reward the cross-selling collaboration seen
as critical for enhanced revenue growth. The
Senior Executive compensation program
has been revised to include a three-year
program for long term improvements in
revenue growth and return on invested
capital. These changes in our performance
based incentive programs align executive
and shareholder interests over both shortand
long-term horizons.
Additionally, we announced the relocation
of our corporate headquarters from California
to Falls Church, Virginia which for some years
has been our operational headquarters. This
will improve communications and strengthen
both corporate and operational efficiencies.
During fiscal 2008 we completed the
share repurchase program announced on
June 29, 2006, involving an overall investment
of approximately $2 billion and the
repurchase of 40.7 million shares.
CSC also strengthened its financial position,
in a very difficult credit market, through
the successful placement of $1.7 billion in
long-term debt.
I am honored to represent CSC  a company
with a market reputation for delivery second
to none; a company with a long term strategy
and a well defined set of objectives; and a
company focused on achieving a new level
of performance and a market valuation
commensurate with its long-term potential.
Sincerely,
Michael W. Laphen
Chairman, President and Chief Executive Officer
June 13, 2008
