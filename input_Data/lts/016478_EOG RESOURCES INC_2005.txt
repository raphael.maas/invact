Letter to Shareholders

We Have the Resources for the Future

The robust commodity prices of
2005 drove a clear demarcation
in the stock market between
energy companies such as EOG Resources that
have the resources for the future and those that
lack valuable long-term reinvestment
opportunities. It isnt by chance or luck that
EOG Resources has developed into an even
stronger resource company. The effectiveness
of our long-term approach to the exploration
and production business, which produced
outstanding financial and operational results in
2005, has been realized.
Over the six-year period since EOG became
an independent company, the strategy
articulated annually in the letter to shareholders
has remained remarkably consistent. We have
reiterated the companys relentless pursuit of
capturing high rate of return assets  weighted
in favor of natural gas reserves  in politically
secure areas.
At EOG, we have accomplished a
great deal:
 By perfecting new techniques in both
exploration and development, we have
identified significant North American
resource plays that competitors have
overlooked or underestimated,
 EOG has aggressively strengthened its rich
reserve portfolio throughout the United
States and Canada, in addition to realizing
meaningful opportunities in Trinidad and the
United Kingdom North Sea, and
 With a focus on returns, EOG has pursued
organic production growth rather than
seeking lower return large-scale
acquisitions and mergers to bolster
reserves and production

Delivering On Our Promises
The marketplace rewarded EOGs tenacity based on
both its robust 2005 results and the recognition of its
skill in identifying and developing resource plays that
should be powerful growth engines for years to come.
EOG reported one of the very highest financial
returns of the large cap peer group with 35.5
percent return on equity (2) and 30 percent return
on capital employed (2) in 2005. Compared to the
same benchmark group, EOG delivered the

highest return to shareholders (2)  106 percent,
ranking number four in the S&P 500 Index.
EOG reported net income available to
common of $1,252 million in 2005, compared to
$614 million for 2004. At year-end 2005, the
company reported a debt-to-total capitalization
ratio (2) of 19 percent, compared to 27 percent at
December 31, 2004. The net debt-to-total
capitalization ratio (2) was only 7 percent at yearend
2005.
Total company daily production increased
16.2 percent in 2005 over 2004, all organic
growth achieved through the drillbit. This included
a 12 percent increase in natural gas production
on a daily basis from the United States and
Canada. Total daily production increased 40
percent last year from the United Kingdom North
Sea and Trinidad, where EOG is supplying natural
gas that is used as feedstock for the M5000
Methanol Plant and the Atlantic LNG Train 4, both
of which started up during 2005.
Building on its firmly established North
American asset base, during 2005 EOG
underscored that the Barnett Shale Play in the
Fort Worth Basin is a substantial company
resource. By extending the play well beyond its
conventional limits using horizontal drilling and
enhanced completion technology, EOG has
become an industry leader in the Fort Worth
Basin Barnett Shale with over 500,000 net acres
under lease across multiple counties. Applying
the same parameters as it did to the Barnett
Shale, EOG has accreted positions in new
potential North American resource plays that will
be tested over the course of 2006.
Our Employees Make the Difference
At EOG, it is not a clich  our employees do
make the difference. They represent an invaluable

resource for the future, as important as the
reserves that we seek to develop. EOG has
created a fast-paced, exciting environment
across the company in which the science of
exploration and production thrives. Our creative,
hardworking prospectors and those who support
them not only utilize the latest technology
available in the market, they adapt and modify it
to meet EOGs needs, thereby producing
outstanding results.
We are proud of our EOG workforce. Our
men and women not only care about
performing their jobs extraordinarily well, they
are concerned about the safety and welfare of
those around them and the communities in
which they live and work. It was an employeedriven
effort that galvanized EOGs effort to
raise $1 million to help those whose lives were
torn apart by Hurricane Katrina and Hurricane
Rita. The management team and board of
directors recognize and encourage the unique
individual contributions of our top-notch
people to EOGs success.
Our Goals for 2006 and Beyond
EOG will never be satisfied with the status quo.
To be the best  not the biggest 
independent exploration and production
company in North America remains our goal.
EOG defines best in simple terms 
generating the highest total shareholder returns.
In addition, we strive to be the most profitable
independent exploration and production
company in terms of return on equity and return
on capital employed.
Because we see no significant long-term
change in the global natural gas and oil supply

fundamentals, EOG will continue its consistent
strategy of building the company for the future by
focusing on our strong organic growth platform
rather than making decisions based on short-term
fluctuations in the financial and energy markets.
Based on our deep prospect inventory, EOG is
targeting an increase in total company production
of 10.5 percent for 2006 compared to 2005, while
we expect to achieve a 7 to 11 percent annual
production increase between 2007 and 2010. In

North America, contributions from both the Fort
Worth Basin Barnett Shale and our other extensive
captured assets will fuel this increase.
We will further expand EOGs position in
multiple North American resource plays through
the application of horizontal drilling. We will also
continue to seek additional new prospects on this
continent. We expect EOGs Trinidad and United
Kingdom assets, which already are well on the
way to emulating the companys success in the


United States and Canada, to gather
additional momentum.
EOG will continue to place a high
emphasis on superior reinvestment rates of
return from our capital program and wisely
utilize any free cash flow. We will operate with
a low level of debt, providing our company
with the flexibility to consider different
reinvestment opportunities that include adding
acreage, further developing existing drilling
programs and identifying possible big-target
resource plays. We will work hard to maintain
the companys reputation as a low-cost
operator and a financially conservative
organization with high ethical standards.
Looking Forward
Industry-wide, we expect the valuation chasm
to widen between companies like EOG
Resources that can grow organically through
the drillbit and those that are short of attractive
reinvestment opportunities. EOG has the
resources to maintain long-term performance
and sustainability that will translate into
superior shareholder value for the future. While
consistency will remain our companys
hallmark, the word resources in our name is
EOGs pledge of performance yet to come.
Mark G. Papa
Chairman and Chief Executive Officer
February 22, 2006