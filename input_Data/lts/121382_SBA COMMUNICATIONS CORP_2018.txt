To Our
Shareholders

I believe there is no better business than the tower model
and our reported financials support that statement. 


2018 was another exciting and productive year for SBA. Through strong
leasing activity, low customer churn and strict cost controls, we were able
to show yet again why the tower business is so highly regarded. I believe
there is no better business than the tower model and our reported financials
support that statement. We produced record-breaking margins, solid organic
growth and raised our full year 2018 guidance each and every quarter. We
provided essential support to our customers as they worked towards providing
enhanced wireless services and the latest wireless technologies across their
markets. We drove value to our shareholders by deploying capital towards 

high quality assets, including by repurchasing our own stock. We worked with
local communities and municipalities to help build better wireless. And most
importantly, we positioned ourselves well for a solid 2019.
This year, 2019, we will celebrate our 30th year in business and our 20th year
as a public company. Over the past thirty years, we�ve seen an explosion
in wireless use cases. From simple voice calls and picture messaging in
the early �90s on the 2G network, to web browsing and email on the 3G
network, to FaceTiming and high definition video streaming on the 4G network. 

Each network evolution more impressive than the
last. As we look ahead from 4G to 5G in the coming
years, we�ll be more connected than ever. According
to Cisco�s 2019 VNI Mobile report, 4G connection
generates about three times more traffic than a 3G
connection. By 2022, the average 5G connection
will generate nearly three times more traffic than the
average 4G connection. That�s more data consumption
than I believe anyone could have imagined when we
acquired our first tower in June 1997.
While a lot of the current industry attention is spent
on 5G, 4G deployments have been the primary
contributor to SBA�s growth for nearly a decade and
we believe there is much more to come. Looking
ahead, 4G is still forecasted to maintain the majority of
the market share for the foreseeable future. This will
certainly be the case in our Latin American markets,
perhaps for quite some time. While 5G will certainly
be a key contributor to our growth in the future, we are
most certainly living in a 4G world today. As such, our
customers have remained extremely active deploying
spectrum, both newly acquired as well as refarming
existing spectrum. Our customers are busy across the
board. In the United States, T-Mobile has continued
to deploy low band 600MHz and 700MHz spectrum.
AT&T has been busy deploying Band 14 spectrum as
FirstNet continues to ramp as well as fallow AWS and
WCS frequencies. Sprint has been active rolling out
800MHz, PCS and 2.5GHz as they boost their LTE
footprint. Verizon spent much of 2018 refarming its
spectrum from legacy services to free up frequencies
for future LTE uses. We even saw an emergence from
Dish as they take the steps necessary to roll out their
IoT network in 2020. In addition to our customers�
existing spectrum holdings, of which a large portion
has yet to be deployed, new spectrum bands are on
the horizon including low band, CBRS, C-Band and
mmWave. As new bands start to enter the wireless
ecosystem, we expect that our assets and services
resources will remain center stage for many years
to come. Similar types of current customer activity
and future spectrum opportunities are present in our
international markets.
Looking into the future, we�re just at the beginning
of 5G, with field trials underway and standards set
to be finalized in 2020. Like most generational shifts,
we would expect our customers to focus on the
urban centers first, then more suburban and rural.
But what does 5G really mean? Basically, it means
faster speeds, much greater reliability and ultra-low
latency. With that extra boost of speed and reliability,
new transformative use cases will likely emerge
changing the way we learn and interact. Cutting edge
technologies are already underway, technologies
such as augmented virtual reality, data intensive
mobile apps, wearable devices, wireless sensors and
monitoring, factory automation, driverless cars, smart
cities and machine learning to name a few � and the
numerous use cases not yet imagined. It�s truly an
exciting time to be in the wireless industry. However,
as networks change, one thing stays the same � the
need for towers. We believe towers will always be a
critical part of the overall wireless infrastructure and
over the past three decades, history has borne that
out. Networks have changed, but SBA has stayed
focused on our core business, erecting towers in
locations where consumers and wireless providers
want to be. As the end users demand more from their
wireless devices, we�ll be there to help support our
customers meet those network demands.
As the fundamentals of the wireless network
ecosystem continue to evolve, we will look to find
ways to adapt and prosper from the ever changing
landscape. In 2018, we developed a number of
exclusive wireless infrastructure assets outside of
our traditional macro-site tower focus, two of which
I�ll highlight here. First, we successfully deployed the
first-ever mobile edge computing (MEC) module at
a tower site in the United States. The site is located
near Gillette Stadium, home of the New England
Patriots and a venue with a capacity of over 60,000
people. This system was designed to house compute
and storage capabilities near our cell site to reduce
latency by eliminating portions of the data transport
process. Second, in the landmark Macy�s Herald
Square location in New York City, we constructed,
and now own and operate, a very sophisticated
indoor distributed antenna system. Macy�s Herald
Square is the flagship of the Macy�s department store
chain covering over two million square feet and an
estimated twenty-three million shoppers per year. 

We successfully designed and built a neutral
host network system which allows our carrier
customers to provide reliable mobile wireless
services even in the most populated and
complex radio-frequency design locations.
Both Herald Square and the MEC module at
Gillette Stadium are examples of exciting first
steps for us to dig deeper to find new ways
to participate in the evolving world of wireless
infrastructure. As we move forward we will
focus on finding those exclusive assets which
offer many of the same attributes as we have
observed in our tower portfolio � high barriers
to entry, high operating leverage, low ongoing
capital intensity and high customer renewal
rates. While we expect that towers will always
be our primary focus, we do see opportunities
for tangential expansion into areas where we
can add value for our customers and create
value for our shareholders.
Turning to our operating
results for 2018, I am
pleased with our overall
performance.
We grew the portfolio by 6%, within our annual
goal of 5 to 10% growth. Internationally, we
acquired over 1,000 sites and built almost
400, the largest portion of growth coming
from El Salvador and Brazil. We also grew
in the United States, albeit at a lower rate,
growing the portfolio by approximately 300
sites, mostly through acquisitions. Our gross
same tower year-over-year growth rates,
which represent newly added revenue over
the prior twelve months, increased every
quarter, ending the year at 7.4% and 11.3%
in the U.S. and internationally, respectively.
These levels are the highest since 2016 and
are the result of a very robust level of leasing
activity we saw over the last twelve months. In
Brazil, our largest market outside of the U.S., 

we posted 12.7% gross organic growth, on a constant
currency basis, reflective of strong leasing activity
we enjoyed throughout the year from each of the Big
4 carriers; Claro, Vivo, Oi and TIM. The currency in
Brazil weakened slightly in 2018 compared to 2017,
which impacted our reported results relative to our
strong operating results. Our underlying business
in Brazil remains very strong and we continue to
outperform our original underwriting assumptions.
We�re optimistic the Brazilian economy is headed
in the right direction and we are excited about the
opportunities that should provide us.
On the back of solid revenue growth and strong
cost controls, we posted industry leading operating
margins, ending 2018 with our highest domestic
tower cash flow margin ever, hitting an all-time high of
82.9% in the fourth quarter. We spent just over $400
million on acquisitions and approximately $65 million
on new builds, levels similar to that of 2017. As we
deploy capital, we continuously monitor our return on
invested capital (ROIC), an important metric by which
we gauge the success of our capital deployment
strategy. I am pleased to see we ended the year with
an ROIC of 10.3%, the highest percentage going
back several years. A line item not included in the
ROIC calculation, but equally as important to our
growth in AFFO per share, is share repurchases. We
repurchased approximately $796 million of stock in
2018, a level similar to that of 2017. We retired 5.0
million shares over the course of the year ending
2018 with 112.4 million shares outstanding, down
3.4% from the prior year. We always prefer to reduce
share count, not expand share count, and expect
share repurchases to be a meaningful part of our
capital allocation as we move further into 2019. We
ended the year at 7.3x net debt to annualized adjusted
EBITDA, in the middle of our targeted 7.0x to 7.5x
range. We successfully raised over $3 billion in 2018
in the debt capital markets at extremely attractive
pricing, a testament to our preferred borrower status
and the predictability and steadiness of the underlying
cash flow. I have always believed there is no better
business than towers to take advantage of attractively
priced capital and a strong balance sheet.
In summary, I was pleased with our execution
and results in 2018. We focused on maximizing
growth, controlling costs, strategically growing our
footprint, deploying capital and creating shareholder
value. Moving into 2019, we expect to build on the
great leasing year we had in 2018 and maintain a
hyper-focused approach on maximizing our core
tower business. I believe we have the greatest
telecommunications assets in the world and demand
for wireless services has never been higher. We are
extremely excited about the years to come and look
forward to the new wave of wireless.
In closing, I must recognize the contributions of our
employees and customers to our success. I also want
to thank you, our shareholders, for your continued
confidence in and support of SBA. I look forward to
communicating with you again soon.
Sincerely,
Jeffrey A. Stoops
President and Chief Executive Officer