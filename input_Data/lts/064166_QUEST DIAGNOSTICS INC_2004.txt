To Our Shareholders, Patients, Customers and Employees:

2004 was a very exciting year. I took on a new role as CEO, with a clear mission to earn your trust, take our company to the next level of performance and, in so doing, write the next chapter in the story of Quest Diagnostics. I focused on three immediate goals:

Drive strong financial results while completing the transition to new leadership;
Demonstrate our ability to accelerate profitable organic revenue growth; and
Define a new strategy to uniquely position our company for continued expansion in the face of a changing market environment.
We have made significant progress in each of these areas, and I am extremely pleased with the overall direction in which we are heading.

Another Year of Strong Performance
We once again delivered strong financial results. Our focus on accelerating profitable organic revenue growth drove steady progress through the year. In 2004:

Revenues increased 8.2% to $5.1 billion;
Diluted earnings per share, excluding special charges, grew 18% to $4.77; and
We generated $800 million in cash from operations and we returned a similar amount to our shareholders, in the form of stock repurchases and dividends, truly enhancing shareholder value.

I am very proud of the broad external recognition we received in 2004. For example, Quest Diagnostics was added to the Dow Jones Sustainability World Index of some 300 global leaders ranked by objective benchmarks linked to financial, environmental and social criteria. We were named one of the "Top 100 Innovators" by Information Week for our use of business technology. Also, Business Week cited our website, www.questdiagnostics.com, as one of "The Best Medical Web Sites" because of its comprehensive patient health library. In addition, many of our business units have been recognized for being among the best places to work in various regions, including Buffalo, Dallas/Fort Worth, New Jersey, Northeastern Ohio, Oregon, Phoenix, Pittsburgh and the United Kingdom.

Beginning a New Chapter
We are writing the next chapter in the story of Quest Diagnostics. The healthcare market is changing rapidly and we have positioned ourselves to take advantage of the opportunities that change brings.

For example, consumerism is increasingly influencing healthcare delivery. Patients as consumers are taking more control of managing their own health and have more choice in selecting their healthcare providers. The expected growth of Health Savings Accounts will further spur consumerism. We give patients and doctors new and compelling reasons to select Quest Diagnostics for their laboratory services. Medical quality is an imperative throughout healthcare, as providers, payers and regulators are determined to reduce medical errors and improve overall health. We were among the first healthcare providers to commit to achieving Six Sigma quality and we continue to pursue new ways to improve our operations and processes.

Medical science is changing rapidly, creating the opportunity to develop new tests, and also to move testing closer to patients than ever before. We are committed to leading the way in using new technologies in the laboratory, including genomics and proteomics, and are pioneering the development of microarrays. We also collaborate with leading institutions and companies to bring advanced technologies to market more quickly.

Additionally, information technology is beginning to change the practice of medicine for the better. We recognized this trend early and developed a suite of healthcare information technology solutions that have given us a strong competitive advantage. Doctors value the ability we provide them to order laboratory tests, receive results and prescribe medications electronically, share relevant clinical information with referring physicians in a HIPAA-compliant way, and participate in pay-for-performance programs. This enables doctors to spend more time with their patients and enhance the quality of care.

Patients, Growth and People
In response to these emerging trends, we established our business strategy on three simple but profound words: Patients, Growth and People. These words make up the theme of this year's Annual Report as well as the basis of our blueprint for a successful future.

As a healthcare company, patients have always been at the center of everything we do. Putting patients first is easy to say, but difficult to do. Hence we are giving everyone at Quest Diagnostics the training and tools they need to truly put the patient first.

Patients deserve the highest quality caring service, from drawing blood in our patient service centers to analyzing specimens and speeding the delivery of test results to reduce what we call a patient's "anxiety time." We devote significant resources to continuously improve the entire patient experience using Six Sigma and Lean methods.

Our growth will always be a combination of acquired and organic growth. To drive internally generated growth, we have rededicated ourselves to improving service levels. We have expanded the sales force and enhanced sales training. Additionally, we have strengthened and streamlined our innovation process. A robust innovation pipeline of new ideas, new products and new services helps us create our competitive advantage. Since we serve only 12% of the U.S. market, we have plenty of room to grow in the diagnostic testing business through new product introductions, geographic expansion and acquisitions. However, we also see opportunity to use our competencies and deploy our cash from operations to expand beyond our core laboratory testing business, into adjacent markets, such as diagnostic imaging, healthcare information technology or international markets.

We are unleashing the power of our 38,000 dedicated people. They embrace their responsibility to serve more than half a million patients every night with a great deal of pride. We invest in our people to make sure that they have the latest knowledge and skills to deliver the best in quality care, and to align them with our company objectives. I see an increased level of energy, excitement and commitment from our employees- and in the response from our patients and doctors.

We are fortunate to have extraordinary people and leaders. I would like to express my sincere gratitude to Ken Freeman, who stepped down as Chairman at the end of 2004, for his monumental contributions-to our company and our industry. Ken built the company with a clear mission and a focus on continuous quality and performance improvement. We all miss him and his wisdom, and wish him well in the future.

I am also thankful to two founding Board members who retired in 2004, Ken Brody and Mary Cirillo. Their broad experience and challenging views helped us to grow and improve. We welcomed two new Directors: John Baldwin, M.D. and Gary Pfeiffer, whose vast experience and valuable perspectives complement an already strong, diverse and independent Board.

Excited About the Future
This is an exciting time for all of us. I feel privileged to lead our company at such a pivotal point in its development. We have come a long way. Our new mission captures our aspiration to become the undisputed world leader when it comes to diagnostic testing and medical quality. And our new vision statement recognizes the critical importance of innovation.

We have raised the bar for ourselves by setting our sights high:

We want to be known for the quality of our patient carethe gold standard in healthcare;
We want to become the worlds most advanced and trusted source of diagnostic testing and information for patients and doctors;
We want to create an exciting place for our employees to work and growa place where they can give shape to their dreams and reach their full potential; and
We are committed to deliver superior returns to our shareholders.

Demographics are extremely favorable, with a growing and aging population which will drive increased demand for our services. Diagnostic testing plays an increasingly important role in helping doctors manage their patients' health. The rapid development of new technology provides us with a myriad of new opportunities to fashion new and useful tests for early diagnosis, which will reduce morbidity and mortality. And soon, information technology will enable doctors to diagnose and treat disease by aggregating a patient's genetic predisposition, diagnostic test results and diagnostic images into a single patient-centric electronic medical record. Having so much relevant clinical data in one easily accessed place will drive better decision-making and will provide medical insights to improve healthcare and reduce cost.

We are the leader in a growing, vital and highly competitive industry. We have a tremendous opportunity and the will to win. Our new journey has begun and we are rapidly building momentum. We will write the greatest chapter yet in the history of Quest Diagnostics. I am so glad that you are part of this story.

Thank you for your confidence and continued support.

Take care,

SURYA N. MOHAPATRA, Ph.D.
Chairman, President and Chief Executive Officer
