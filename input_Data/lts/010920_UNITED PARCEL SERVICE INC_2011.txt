UPS delivered another year of
stellar performance in 2011.
In more than 220 countries and territories around the
world, our employees provided logistics solutions and
served customers with reliability that is unmatched in
our industry.
UPS achieved record earnings per share (ePS) in 2011,
well surpassing our previous peak in 2007. our operating
margin and free cash flow are the envy of our industry.
the financial results of our business segments
demonstrated our adaptability.
 Our U.S. Domestic segment produced strong
revenue growth, the highest operating profit since
2007, and significant margin expansion in a slow
growth environment.
 UPS International operations achieved record
revenues on strong volume growth that continued
to outpace the market. this performance was quite
impressive given tumultuous economic conditions
around the world in 2011.
 In the Supply Chain and Freight segment, we
upgraded our capabilities, and continued to
achieve new highs in operating profit with
balanced contributions from all business units.
At our Investor Conference in 2011, we showed that
UPS is the market leader in cutting-edge technologies
and unique customer solutions. We also announced
aggressive long-term targets. Some of these targets are:
EPS growth of 10 to 15 percent, ROIC in excess of
25 percent, and Free Cash Flow conversion greater than
100 percent of net income. our performance and our
focus on shareholder returns makes UPS a blue-chip
stock that belongs in any investment portfolio.
UPS Strategy
as I mentioned in my letter last year, the UPS strategy has
three key tenets: create value for our customers, transform
our business, and invest for growth. In 2011, we executed
this strategy and the results were rewarding.
Creating Value
During the past year, UPS introduced a variety of
innovative solutions.
to support the needs of the fast growing Businessto-
Consumer (B-to-C) market, we launched
UPS My ChoiceSM in the United States. this groundbreaking
offering allows the receivers of UPS shipments
to control aspects of the delivery. We are pleased with
how this product has been received. after unveiling
UPS My Choice in October 2011, the number of new
subscribers approached 750,000 by the end of the year.
to assist our customers with reverse logistics, we
introduced UPS Returns Exchange and UPS Returns
Pack and Collect. these two unique solutions provide
retailers in north america and europe with new and
cost-efficient ways to process returns.

Both of these solutions leverage our extensive network,
which includes UPSs retail presence of almost 5,000
locations of The UPS Store.
Transforming
We continue to refine and
develop the UPS business model
to capitalize on new opportunities
for growth. Great companies adapt
to the changing business world,
and our tradition of transformation
dates back more than a century.
In 2011, UPS made major
investments in our healthcare
capabilities. We added 1.6 million
square feet of healthcare compliant
warehouse space. UPS at
year-end had more than 5 million
square feet for healthcare in 33
facilities around the globe. We plan
to add four new facilities in 2012.
Also in 2011, UPS acquired Pieffe,
a health care distribution company
in Italyfurther adding to our
global capabilities.
Technology powers logistics and makes our business
more productive and efficient. UPS will save more than
$200 million annually just from the following three
technology initiatives:
 New keyless entry systems automatically
enable the ignition, unlock the doors, and
open the bulkhead door.
 Telematics sensor technology gathers
and analyzes data from our vehicles to
improve safety, increase efficiency, and
enhance customer service. Also we use
much less fuel and reduce emissions.
 Next Generation Small Sort (NGSS)
improves efficiency of our small sort
operations by eliminating the memorization
of ZIP codes.
Additionally, with the Network Planning
Tool (NPT) and the On Road Integrated
Optimization and Navigation (ORION),
UPS will link its operations technology with
advanced analytics.
Last, but not least, theres our industry-leading
sustainability program. Our efforts to
improve the fuel efficiency of our air and
ground fleet continues to provide benefits
to our operations while also reducing the
impact on the environment.

Investing to Grow
We believe in the power of global trade to drive
economic growth and improve living standards, and
were heartened in 2011 that the United States passed
three free trade agreements with South Korea, Colombia,
and Panama. UPS continues to invest in its global
portfolio of customer solutions to support the rapid
growth of trade and cross-border commerce.
Latin America is experiencing robust economic growth,
and in 2011 we announced a strategic partnership
with Deprisa in Colombia. In Brazil, we expanded our
healthcare facilities. We also opened UPS Capital
financial services offices in Peru and Colombia.
UPS also is expanding capacity in the important
pan-European market, announcing a $200 million
expansion of the Cologne Air Hub that will result in a
70 percent increase in package sorting capabilities.
The UPS International Air network added new direct
flights from Hong Kong to Cologne. This upgrade
allows UPS to make overnight small package deliveries
to Europe. UPS offers the widest guaranteed next-day
delivery coverage for both packages and heavy freight
in the Europe to Asia trade lane. We also added new
flights to Chengdu, China; South Korea; and Guam.
Helping the Community
While we were busy executing our strategy during 2011,
UPS was also actively involved in communities around

the globe. After the terrible tsunami and earthquake in
Japan, UPS emergency logistics response teams rushed
into action and provided local authorities with our
logistics expertise. The same was done in response to
severe weather events in the United States. Along with
financial support, we helped provide emergency supplies
and necessary equipment.
Our involvement in the communities we serve and
the dedication of UPS people worldwide have earned
widespread recognition. FORTUNE magazine again recognized
UPS as the worlds most admired transportation
company, and The Reputation Institute ranked us
No. 1 in the industry. Were also quite proud of the
University of Michigans American Customer Satisfaction
Index (ACSI) ranking of UPS as No. 1 in the express
delivery industry.
Looking Ahead
While 2011 was a great year for UPS, we are looking
forward to 2012. UPS will celebrate 105 years of
excellence and achievement in August this year.
UPS will be responsible for virtually all the distribution
and logistics services for the London 2012 Olympic and
Paralympic Games. Weve put together an integrated
supply chain solution that includes venue logistics
services, warehousing services and a distribution network
that will collect and deliver everything from documents
to heavy freight. UPS also will be responsible for all
customs clearance, freight forwarding and courier
services before, during, and after the London Games.

As we begin 2012, growth
in the Euro zone and
Asia is slowing, while the
United States economy is
improving at a modest rate.
Our expectation is for
mixed economic growth
around the world in 2012.
Overall we expect global
economic expansion to be
slightly weaker than 2011.
In spite of the macro outlook, UPS expects another
record performance, with earnings per share in a
range of $4.75 to $5.00. Thats an increase of 9 to 15
percent over 2011 adjusted* earnings of $4.35.
New Opportunities
I remain confident that our competitive differentiators
are sustainable for the long term. Our brand is respected
around the world, and our global investments have
positioned us to benefit from economic growth and
expanding global commerce. Our superior worldwide
capabilities are unique in our industry. In our long
history, our heritage and legacy of intellectual property
is difficult to match. Our track record of strong cash
generation will continue to provide us with opportunities
to reinvest and reward shareowners. Finally, our strong
culture of reinventing our business model will ensure
we meet the ever-changing needs of supply chains
around the world.
We have put another great year in the books and now
look with excitement at new opportunities in 2012.
UPSers are ready to deploy our assets and know-how to
provide competitive advantages for our customers. At
the same time, our investors should enjoy another year
of record earnings per share. UPS is uniquely positioned
to capitalize on economic expansion, demographic
changes, and market development around the world.
D. Scott Davis
Chairman and Chief Executive Officer