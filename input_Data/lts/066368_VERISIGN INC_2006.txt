Letter to Our Shareholders
VeriSign has a simple yet compelling mission: to enable and protect the worlds networked interactions.
Our Naming Services and SSL (secure socket layer) businesses continued to demonstrate strong growth during 2006, and over
the last 18 months we have launched new services around global messaging, identity services, and broadband content. This
ability to leverage our core competencies gives us confidence in our growth strategy, particularly for enabling and protecting
the worlds networked interactions.
Financial Highlights
In 2006, VeriSign recognized revenue of nearly $1.58 billion as compared to $1.61 billion in 2005. The Company recorded GAAP
net income of $378 million in 2006, with GAAP earnings per share of $1.53. Revenue was down slightly from last year, as
revenue from our Jamba business was $298 million as compared to $530 million in 2005; excluding Jamba, revenue was up 18
percent from last year.
Our businesses generated positive cash flow from operations of $475 million, allowing us to make strategic investments for
future growth opportunities while also repurchasing more than 6 million shares of VeriSign common stock. We also continued to
strengthen our balance sheet, ending 2006 with approximately $750 million in cash and equivalents.
Business Highlights
Internet Services Group (ISG)
The Internet Services Group includes both VeriSign Information Services (VIS) and VeriSign Security Services (VSS). The group
reported strong financial results in 2006, with increases in all key business metrics, including revenue of $759 million, which was
up 20 percent over 2005.
Our two core franchises, Naming Services and SSL, are healthy, growing, and relied upon every day by hundreds of millions of
Internet users. For almost a decade, people everywhere have searched the Web, sent email, and conducted online commerce
with confidence and reliability based on VeriSigns Internet Services.
VeriSign Information Services, which consists primarily of our Naming Services business, ended the year with 65 million .com
and .net domain names in the adjusted zone, up 30 percent from 2005. Registration renewal rates for the year were 76 percent,
up from 75 percent in 2005. Exiting 2006, our infrastructure handled an average of 24 billion DNS queries per day, an increase
of more than 60 percent from last year.
In November, the Department of Commerce formally approved our .com contract with ICANN, which was announced earlier
in the year. The new contract with ICANN extends through 2012 and provides for clearer processes for new registry service
introductions and provides for renewal, unless it has been determined that VeriSign has been in fundamental and material
breach of certain provisions of the .com Registry Agreement and has failed to cure such breach. It also provides for certain fee
adjustments over time.
VeriSign Security Services, which includes our SSL, Authentication, and Managed Security Services businesses, saw a number
of exciting developments in 2006. In February 2006, we launched the VeriSign Identity Protection (VIP) program to address
the rapidly emerging need for consumer authentication services on the Internet. The VIP program was launched with premiere
participants including PayPal, eBay, Yahoo, and Charles Schwab, and we are encouraged by the consumer adoption results to
date. The VIP program is an excellent example of our investment in infrastructure platforms in support of emerging trends in
the marketplace.
In September, we completed the acquisition of GeoTrust, further extending our leadership position in the SSL certificate
business. Our SSL business saw continued growth throughout the year, ending with an installed base of more than 807,000
certificates, including VeriSign, Thawte, and GeoTrust-branded certificates. We were also the first company to introduce and
issue extended validation (EV) certificates that support new security features in Microsofts Vista and IE7 (Internet Explorer 7)
products. The mantra for 2007 is Green Means Go on the Internet, as EV certificates significantly increase end-user awareness
of e-commerce security.
Communications Services Group (CSG)
VeriSigns Communications Services Group includes a number of businesses providing intelligent communication, commerce,
and content services to fixed line, mobile, and other telecommunication service providers. The businesses are grouped into two
business units: Digital Content & Messaging Services (DCMS) and Communications & Commerce (C&C).
CSG recognized revenue of approximately $816 million, which was down 17 percent from 2005. The decline in revenue was
largely the result of our Jamba business-to-consumer content business being sharply lower in 2006; excluding Jamba, CSG
revenue was up 16 percent from 2005.
DCMS saw the launch of two key initiatives in 2006. In September, we announced the formation of a joint venture, based largely
on our Jamba business, with News Corporation. The joint venture was finalized in January, with VeriSign receiving $188 million in
exchange for News Corporation receiving a controlling interest (51 percent) in the joint venture. The joint venture allows VeriSign
to focus on its core competency of providing digital infrastructure services that are inherently scalable, while gaining the benefit
of News Corporations world-class content creation and distribution assets.
In December, we introduced our intelligent Content Delivery Network, or iCDN. The iCDN is designed to support the delivery of
all forms of content, including long-form high-definition video and audio. The service combines our proprietary enhanced peerto-
peer technology with advanced video-streaming capability running on our global secure infrastructure to provide for highly
efficient and economical delivery of rich content media.
Messaging Services saw strong transactional growth in 2006, as we delivered nearly 40 billion SMS messages, up 139 percent
from 2005. Premium messaging and MMS volumes grew to 88 million messages in the fourth quarter of 2006. However, the
pricing environment in messaging services continues to be challenging, largely offsetting transactional growth.
As part of Messaging Services, VeriSign also provided interactive mobile voting infrastructure for the majority of U.S. interactive
TV messaging, including shows like NBCs Deal or No Deal and Foxs American Idol.
The Communications and Commerce business unit benefited from modest revenue growth in billing and payment services.
However, this growth was offset by a decline in signaling and connectivity revenues, due to ongoing carrier consolidation,
coupled with the pricing pressure inherent in this business, which remains as a source of downward pressure on revenue. We
continue to seek opportunities for improvement in this business through increased international penetration.
Overall, volatility in our B2C content business, along with continued pressure in signaling and connectivity, resulted in CSG
performance that was below expectations for 2006. Looking forward to 2007, we are focused on growth opportunities that
exist within CSG, including messaging and broadband content services, as these services are more aligned with VeriSigns core
competencies.
In Closing
In May 2007, Edward A. Mueller was appointed Chairman of the Board of Directors and I was elected President and CEO of
the company, replacing Stratton Sclavos, who had served as CEO since 1995. We thank Stratton for his many contributions to
VeriSign and wish him well in his future endeavors.
Ed and I have already rolled up our sleeves and, along with our outstanding executive leadership team, are working hard to
shape the future of your company with the strongest possible emphasis on managerial discipline and execution of our longterm
strategy. We believe that our talented team of technical professionals, combined with our unique offering of products and
services, position VeriSign very well for long-term growth, while maintaining our leadership positions in our core businesses.
I would like to thank our shareholders, employees, customers, and business partners for the trust you place in VeriSign and for
your support of our missionto enable and protect the worlds networked interactions.

William A. Roper, Jr.
President and CEO