To Our Shareholders

September 11 was a shattering day for everyone in the United States. Like all Americans, our hearts went out to the victims of the
terrorist attacks and their relatives. Thankfully, the Kaplan associates who were in the World Trade Center that morning escaped,
but many Post Company people lost family members, and many more lost friends and close business associates. The Post and
Newsweek immediately set about months of diligent reporting on these events, which Ben Bradlee describes beginning on page
12.You also would be proud of the quality news and public affairs programming Post-Newsweek Stations devoted to the September
11 tragedy, particularly in the days immediately afterward.
On July 17, my mother Katharine Graham, the publisher of The Post and chairman of this company for decades, died in Boise, Idaho.
The Post, Newsweek, and many other publications and television programs offered tributes to her at the time, focusing on her contributions
to journalism and to the community. A short essay by Warren Buffett describing her business career and what it meant
to the shareholders of this company begins on page 9. The company will carry on with the dedication to excellence in business,
journalism, and education that she inspired.
It seems mundane to be reporting about the companys financial affairs after those two introductory paragraphs. But thats the purpose
of this letter. Although chief financial officer Jay Morse keeps the books here in a fashion somewhere between conservative
and very conservative, it has often seemed necessary to start off this report by explaining something about accounting. This year
is no exception. In 2001 we booked a gain of $321 million on a cable transaction that was, in fact, one portion of a trade:We traded
Cable ONEs systems in California and Indiana for AT&Ts systems in Idaho. This gain is a bookkeeping fiction.We never received
$321 million in cash (we did receive some cash in the trade, but it was less than 20 percent of the sum that was booked). The gain
resulted from accounting rules that require writing the assets we traded up to fair market value, rather than recording them at their
historical cost.
In evaluating this gain, shareholders should do what I do: ignore it. If you ignore it, The Washington Post Company had a
very poor year in reported results. Much of this was attributable to the recession, which had a highly negative effect on advertising
in all media and particularly on help wanted advertising at newspapers, including The Post. Advertising was bad before September
11 and worse after. Other major components of our down year included a large write-off at our BrassRing affiliate and the first-year
effects of the Idaho trade on Cable ONE (the longer-term effects should be excellent, beginning this year).
Fortunately, The Post Company continued making substantial progress on several fronts.We arent planning for an advertising
recovery in 2002 (dont be alarmed by this; we have no record of successfully forecasting changes in the ad market). But
unless advertising gets much worse, the company should have a pretty good year in 2002, both in reported earnings and in substance.
Two divisions in particular look set for good times: Cable ONE and Kaplan.
2002 should be an encouraging year for Cable ONE, partly because of the companys willingness to absorb lower earnings in 2001.
Tom Might has put together a series of management actions that should enable this amazing organization and its associates to bring
in large increases in operating income and cash flow for the next two years.
Operating results in 2001 were pounded by the aforementioned trade of high-performing cable systems in northern California
and Indiana for all of AT&Ts systems in Idaho. The systems we traded had higher cash flow than the Idaho systems we acquired, and
we also absorbed large depreciation and amortization expenses  all of which took their toll on net income in 2001. But Cable ONE
management had an immediate impact on the performance of the Idaho systems. Their cash flow in 2002 should equal that of the systems
we traded, and customer satisfaction, which was much lower in Idaho than in Cable ONE systems, has risen rapidly.We gave up
third-fastest-growing state in the country.We like our prospects in Idaho going forward.
Idaho was only the beginning of an outstanding year at Cable ONE. By year-end we were number one in the industry in
digital video penetration from a standing start just 16 months earlier. To demonstrate the appeal of our service, we offered it free
for one year to any subscriber who was willing to self-install it. The initial returns are in, and they demonstrate a gratifying number
of subscribers who are willing to convert to paying customers. Were right in line (though still near the beginning) with our optimistic
plan to have 250,000 paying subscribers at the end of 2002.
We also had cable modem or (in a few cases) dial-up Internet capabilities deployed in more of our rural MSO than any other
cable operator in the United States. Theorists have written about a digital divide between urban and rural America. That absolutely
isnt the case in Cable ONEs small-town/smaller-city markets, and we hope to end up with very high cable modem penetration. At
year-end 6 percent of Cable ONEs customers subscribed to CableONE.net. This was somewhat behind industry averages because
of our decision to delay the launch of the service until we could do it our way  self-installed by our customers, of course, and without
a third-party Internet access provider. But with the rapid rollout of our modem capability, we hope to catch up rapidly with everyone
else in the industry.
Cable ONEs management has done almost nothing according to standard cable industry practice. Thats because our markets
are different and because we have focused on the twin goals of providing maximum customer satisfaction and good returns to
shareholders. Cable ONE is ready to come through. Tom Might and the Cable ONE team have not let shareholders down.
Kaplan had a very good year in 2001 and set the table for outstanding performances this year and for years to come. Jonathan
Grayer, Andy Rosen, and dozens of smart, energetic, customer-focused Kaplan managers have the company delivering great results
in businesses that address many areas of education. Kaplans oldest business, Test Prep, is performing outstandingly. Kaplans newest
acquisitions, particularly the Quest colleges (now part of Kaplans newly-formed higher education division), have been extremely
successful.We enter 2002 with very high hopes indeed.
Kaplan fell just short of $500 million in revenue in 2001 and became the companys second-largest business in revenues after
The Washington Post. In 2002 revenues will grow dramatically, in part from acquisitions but much more from internal growth.
Kaplan Test Prep, the business Stanley Kaplan invented in the late 1930s and sold to The Washington Post Company in 1984,
continued to experience fast growth in revenues and profits. (Stanleys autobiography, Test Pilot, was published to excellent reviews
last year.) When The Princeton Review went public during the course of the year, we learned that our revenue growth has consistently
outpaced that of our largest, albeit smaller, competitor. Competition is keen, but John Polstein and Kaplans Test Prep team intend to
keep up that record.
Throughout the year we noted that the securities and financial offerings of Kaplan Professional were certain to be affected
by the recession. They were, but were happy to report that Eric Cantor and his team turned in an increase of more than 20 percent
in their contribution to Kaplans profits. The growth opportunities here remain great. Kaplan Professionals one acquisition of 2001
added a truly unusual and excellent business to the unit: ProSource, under Joe Coyne, has dominated Minnesota in financial and real
estate training.
Our higher education division had a banner year in 2001, its first full year as a Kaplan unit. Gary Kerber and his team are
bringing wonderful resources to thousands of students, helping those students get better jobs and earn more money. In January
2002 three acquisitions brought more colleges into Kaplans network, in Texas, Philadelphia, and Columbus, Ohio. There is opportunity
for substantial growth in this business, and Gary has shown  both in 2001 and in the years before  that he can manage a
growing company.
In past years weve written about the amounts the company has invested in various developing businesses. Kaplans postsecondary
online efforts consumed another $10.5 million in 2001 and will lose money again in 2002. But what started as an experiment
is now a business with hundreds of students, serious courses, and a huge growth opportunity. Robert Greenberg, one of
the ablest among Kaplans capable group of managers, took charge of Kaplans online higher education business in 2001. The
goals he and Jonathan have set for this unit are very high indeed.
Finally, Score cut its losses in 2001 and will cut them again in 2002. Score probably will never be a very big business, but
it should be a profitable one. Its providing a vitally needed service if we can judge by the number of kids who run from the family
minivan to Score centers every day, both because Score helps with schoolwork and because they enjoy it so much.Kaplan has been our fastest-growing business for years and should be again in 2002. It is changing the face of our company.
Before long, its likely to become our largest division in revenues and should be growing fast in profitability.
As Kaplan becomes more successful, it becomes more necessary that shareholders understand its results. These include
annual accruals for a compensation plan for Kaplans top management. Back in 1994 we thought Kaplans strong management team
had the potential to take our money-losing education business and turn it into a highly profitable division  if we could keep the managers
together. Most of the team stayed, and the results have been terrific.
Accruals related to the stock compensation plan  mostly non-cash for now, but leading to a cash payout later  make
it difficult to understand Kaplans results. Under the plan, a value for Kaplan, based on its hypothetical value if it were a public
company, is set by our Boards compensation committee. An outside firm participates in the valuation. As Kaplans earnings
have started to increase dramatically, so have the valuations  and the accruals. We accrued $6 million under the plan
in 2000 and $25 million last year. We expect Kaplans profits and its value to increase significantly this year; so, too, should
our accruals.
So much for the news of improving results. If you look at the segment data at the end of this report, youll find that Post-Newsweek
Stations had a big down year in profits; they were off 26 percent. But Alan Frank and the managers of our six television stations 
Joe Berwanger (Detroit), Sherry Burns (Jacksonville), John Garwood (Miami), Jim Joslyn (San Antonio), Henry Maldonado (Orlando),
and Steve Wasserman (Houston)  deserve thanks from every Post Company shareholder for their excellent performance under
difficult circumstances.
2001 was certain to be a down year in television station profits because 2000 included such a large amount of political and Olympics
advertising. The TV business has evolved into one where even-numbered years experience growth, while odd-numbered years struggle
to retain it. Nonetheless, because of the advertising recession, revenues were way below what we had expected. (For four days after
September 11, the stations ran no advertising whatsoever, but revenues were off before and after September 11 as well.)
Post-Newsweek once again recorded the highest operating margins of any publicly reporting group broadcaster.Alan and his team
of station managers started cutting expenses as soon as they saw the year was going to be a poor one, and they never stopped.Post-Newsweek
Stations has made an art form of cutting expenses while making sure news and public affairs programming stays as strong as ever.
Several of our stations set ratings records, and all performed well within their markets. Our poorest-performing station, WKMG
in Orlando, had a management change at year-end with PNS veteran Henry Maldonado taking over, along with news director Skip
Valet. The station quickly became competitive in the 11 p.m. news period, but WKMG still has plenty of work to do to get to the place
where other Post-Newsweek stations have been for years.
At the beginning of 2002, we reached a deal with NBC that should work out very well for both companies. Back in 1994
Bill Ryan, then president of Post-Newsweek Stations, reached a network compensation agreement with NBC that has proven very
beneficial to our company and also has provided to the NBC network the audiences of both Detroits top-rated station and our fastgrowing
station in Houston.
At the end of 2001, NBC made a proposal for a revised arrangement that will run through 2011. Alan Frank recognized the
proposal had highly advantageous qualities for both companies, and an agreement was quickly reached. This was gratifying; relations
between networks and affiliates have been difficult for years, and our company may have contributed to the rhetorical noise
level at times. (We certainly werent alone; it has long seemed to us that the networks have been super-critical of local affiliates and
not at all appreciative of higher-performing groups like Post-Newsweek, even though we bring higher-than-average audiences to
network programming.) We would prefer that all our stations remain affiliated with the major networks, as we traditionally have been;
well do so if we can reach agreements that are financially acceptable for our shareholders.
News is the key building block at every PNS station. Strong news programming produces strong business results. Of particular
note, WJXT won a 2001 Peabody Award for its documentary on domestic violence. Peabodys are usually won by the networks,
so this was a remarkable accomplishment for our smallest station and the team that created the program: anchor Deborah
Gianoulis, producer Sharon Siegel-Cohen, photographer Bobby Thomas, and news director Skip Valet.
In the future, viewers will have more and more high-quality channels to turn to, and there are those who have doubts about
the future importance of local television stations. It has always seemed to us that the station with the best local news in each market
has a huge advantage in getting ready for the future. Thats what we intend to be.
Financially, Newsweek had an extremely tough year. When Rick Smith took over the magazine in 1991, Newsweek was making
no money at all, and Rick described the next ten years as rolling the rock up the hill. In 2001 the rock rolled most of the way back
down again. Advertising pages were off almost 25 percent. Few news or business magazines did better; many did worse.
However, poor business results had no effect whatsoever on Newsweeks journalistic quality.Week after week, the magazine
broke important stories, printed insightful columns, and crackled with life. Circulation revenue was up by a simply unbelievable (and
highly necessary) $21 million. The success of Newsweeks special post-September 11 issues  described in Ben Bradlees essay 
led to thousands of subscriptions that will hold down circulation-building expenses in 2002 and provide advertisers with a meaningful
bonus over the rate base.
Newsweeks management  Rick Smith, president and COO Harold Shain, publisher Greg Osberg, and editor Mark Whitaker 
is simply the best in the business and experienced at rolling rocks up hills.
Again, Newsweek and all magazines face huge increases from the U.S. Postal Service. The Postal Service has its problems,
but its persistent rate posture  raise rates aggressively on the mail customers want (letters and magazines) while slashing rates on
the mail no one wants (third-class junk mail)  seems like a recipe for disaster.
In reporting on The Washington Post newspaper over the years, Ive occasionally called attention to the increasing importance of help
wanted advertising to Post results and to the cyclical character of that business. But the dramatic decline in help wanted in 2001 made
the point far more eloquently than I.
This question often comes up: Is help wanted advertising moving from newspapers to the Internet? The enormous numbers of
resumes that flood in from Post help wanted ads remain ample tribute to the necessity of newspapers for recruitment campaigns. If advertisers
want an online solution, our WashingtonJobs.com is simply the best resource for employers in the greater Washington area.Were
ready to serve advertisers in whatever medium works best for them. But we think newspapers will remain the key ingredient in successful
recruitment advertising.
For Post advertising outside recruitment, it wasnt as bad a year. Retail revenue was down almost 15 percent, but automotive
and real estate advertising held up well.
At the end of the year, The Post raised its newsstand price for the daily paper to 35 cents.We remain one of the lowest-priced
newspapers in the country, a fact that has something to do with the exceptionally high penetration The Post enjoys in the District and the
11 counties that make up the Washington area. But with advertising falling, we, like all other newspapers, have had to ask readers to
bear somewhat more of the costs of producing their morning paper.
Expense performance was a highlight. All departments focused on it, and costs at the newspaper actually declined for the year
despite higher average newsprint prices than in 2000.
The Posts journalistic performance both before and after 9/11 was simply sensational. For years executive editor Len Downie
has been patiently building an exceptional reporting staff. When the events of 2001 hit, The Post was prepared with great reporters
to follow them across the board.
Id also like to single out one particularly important piece of pre-9/11 reporting. Everyone in Washington was horrified by
the death in early 2000 of a little girl named Brianna Blackmond. She had been removed from loving care in an outstanding infants
home and returned for a visit to the custody of a mother previously found unfit. Her death inspired reporters Sari Horwitz, Scott
Higham, and Sarah Cohen to ask how many other children turned over to the citys custody had died. The answer was appalling.
From 1993 through 2000, 229 boys and girls perished after their families had come to the attention of the Districts child protection
system. According to the series, Some children died in accidents or shootings on the streets. Others succumbed to disease. But
one in five  40 boys and girls, most of them infants and toddlers  lost their lives after government workers failed to take key
preventive action or placed children in unsafe homes or institutions. When the series ran, city officials dedicated themselves to
making necessary changes in the child custody system.
Late in 2001 Herbert Block (Herblock) died, ending a 55-year career as The Posts cartoonist. Herb was the greatest editorial
cartoonist in history. No other cartoonist was ever at the center of affairs for such a long period, nor the regular author of so
much memorable work. To paraphrase one of his favorite Americans, someone will succeed Herb, but no one will replace him.
The Gazette newspapers in Marylands Montgomery, Frederick, Carroll, and Prince Georges counties also saw their advertising volume
fall as a result of the recession. Profits fell somewhat, but The Gazette delivered another strong year.
In early 2001 we acquired Southern Maryland Newspapers from Chesapeake Publishing Corp. They, too, were affected by
the recessions impact on advertising. However, under Chuck Lyons able leadership, we look for our now-large group of community
newspapers to be an important contributor to the companys profits in the future.
One of the largest business issues facing the company is the future of Washingtonpost.Newsweek Interactive (WPNI). In important
respects it had a simply great year in 2001. Traffic to washingtonpost.com boomed from January on, and as readers sought news
about September 11 and its aftermath, chief executive officer Chris Schroeder and editor Doug Feaver responded.
Up until 2001, the biggest day in washingtonpost.coms history had been the day after the November 2000 presidential election,
when the uncertainty in Florida led to a record 10.7 million page views, 35 percent more than the biggest day wed ever seen.
September 11 and 12 each brought 28 million page views, and the site recorded several more 10 million-plus page-view days. As I
write this in January, washingtonpost.com seems to have settled down to a traffic level almost 50 percent higher than the level of 2001.
Jupiter Media Metrics, the principal web ratings service, continued to report that washingtonpost.com was the most highly
penetrated local site in the United States. Its 39 percent local-market penetration in October was a runaway number one in the country.
In addition, washingtonpost.com attained a record year for revenues in jobs, cars, and real estate advertising.
Frustratingly, while employers, car dealers, and real estate agencies increasingly used washingtonpost.com to drive business,
other advertisers held back. The sites losses remained high (though no higher than we budgeted, thanks to nice work on the cost side).
Chris Schroeder, chief operating officer Caroline Little, and their impressive team enter 2002 with a renewed commitment
to push revenues higher, drive expenses down, and look for every possible way to expand the sites advertising base.
Flatteringly, Cox Newspapers bought our WashingtonJobs.com software, feeling it was the best available product on the
market, and began using it on its own recruitment web sites.
In our three previous annual reports, Ive proudly highlighted how much money we were spending on new businesses, many
taking advantage of the Internet. These included WPNI, Score, eScore.com, and Kaplans online higher education initiatives,
BrassRing, and a newspaper industry classified advertising initiative, Classified Ventures.
The scorecard on these is mixed. Ive reported on WPNI. Score, after about $50 million of investment, is close to breakeven.
Kaplans online higher education effort (once called The Kaplan Colleges) looks like a winner, possibly a big winner.
eScore.com, an ambitious attempt to create an online equivalent to the Score classroom experience, failed. Much of the
able programming talent gathered to support it is now developing the Kaplan Learning Platform (KLP), a set of learning software
that will undergird Kaplans initiatives to deliver superior educational services across the board, from kids at Score to adult learners
at Kaplans higher education division. KLP will give Kaplan a leg up in all of its online endeavors.
BrassRing, a business-to-business hiring management and recruitment company in which The Post Company has a
49 percent interest, reported a large loss in 2001. Much of this resulted from the write-down of the value of its career fair assets
and therefore was non-cash. Our share of these losses resulted in a pre-tax charge of $75 million, although we invested only
$21 million in cash in BrassRing in 2001.
BrassRing consists primarily of two businesses: a large career fair business and a smaller but rapidly growing software
business that offers customer companies web-based systems that streamline and manage the corporate recruiting process. The
career fair business had a terrible year; almost no large companies were hiring. The systems business had an excellent year.
BrassRings prospects going forward are uncertain, but seem very good. Certainly the company couldnt have a better CEO than
Deb Besemer, and Deb has attracted a very strong management team.
Classified Ventures had a much-improved year under new CEO Dan Jauernig. The company expects to operate without
requiring additional capital from investors in 2002, but it will never return to Post Company shareholders anything like the amount
of money weve put into it. (We own 19 percent of Classified Ventures.)
Whether all these investments turn out to have brought much to Post Company shareholders will be determined by the
futures of WPNI, Kaplans post-secondary businesses, and BrassRing.
No Washington Post Company annual report would be complete without a few words on pension accounting. Ive reported before
that a large portion of our reported profits comes from our pension credit. The number for both 2001 and 2000 was approximately
$4.00 per share (excluding costs for early retirement programs). The table on page 52 breaks down the pension credit by segment.
extraordinary. With Bill Ruane and his firm, Ruane, Cunniff, investing the lions share of the fund, our returns have been among
the best of any U.S. corporation. This gives the people who work here unusual certainty about their pension funding.
The reason we have always singled out the pension credit for your attention is that its a non-cash item and therefore of lesser
quality than the rest of our earnings.
Complex accounting rules decide how much pension credit is reported as part of our earnings, but a key determinant is the
expected rate of return on assets in a pension fund (the higher the expected return, the larger the pension credit and the greater a
companys reported earnings). For 2002 and the future, we have reduced The Washington Post Companys investment return assumption
from 9 percent to 7.5 percent.Weve also lowered a second assumption, called the discount rate, by one-half percentage point.
As we announced in January, the combined effect of these changes and assumptions would be to reduce 2002 earnings by $20-to-
$25 million, although cash receipts dont go down by a dollar. After our January announcement, our actuaries informed us that our
investment returns were much higher than expected, so that the pension credit in 2002 will be off only $10-to-$15 million.
Our decision to reduce the return assumption is unconventional and will penalize our earnings compared to those of other
companies. Warren Buffetts article in the December 10, 2001, issue of Fortune seems to me and to our Board to provide overwhelming
evidence that its not prudent to expect 9 percent returns out into the future.
One other significant difference for 2002 is that because of a change in accounting rules, we will no longer amortize goodwill.
This will have the effect of increasing our reported operating income by approximately $75 million (about $5.00 per share), but its a
non-cash item. A similar adjustment will hit the books of all other companies.
John Dotson joined the companys Board of Directors at mid-year. John had spent 18 years at Newsweek early in his career, rising to become
chief of correspondents. After leaving us, he became a Knight Ridder publisher, first at the Boulder Camera and then at the Akron Beacon-
Journal, where the newspaper won the Pulitzer Prize Gold Medal for Public Service for a groundbreaking series on race relations in the
community. John is a most-welcome addition to the Board.
This year Don Keough retires, an enormous loss to our Board. Dons range of experience, brains, and unfailing good humor
were among the most important supports to Kay Graham and later to me. To say hell be missed is a huge understatement.
Bev Keil, the companys longtime vice president of human resources, retired at the end of the year. Bevs contributions were
very important. She greatly improved and simplified our benefits plans. In addition, she oversaw the growth of The Gazette newspapers
and PostNewsweek Tech Media. She found and hired Chuck Lyons and put him in charge of those businesses. Ann McDaniel moved
from the managing editorship of Newsweek to become Bevs successor.
With Cable ONE and Kaplan leading the way, the company continues to change dramatically from year to year, making it difficult for
me to communicate to you all I should about the nature of your investment. In the fall of 2000, we held a highly successful Shareholders
Day, giving shareholders a chance to hear from our division heads about the most rapidly changing parts of the company.Well probably
stage a second Shareholders Day later this year. Ill communicate with you about it when a time and place are set.
Readers of this report over the years may have noted Im not particularly comfortable with optimistic statements, and I try this one out
somewhat awkwardly. Despite the biggest advertising recession in recent memory, we could have a pretty good year in 2002. In fact, the
next two or three years should be good ones for the company, even assuming no advertising recovery (as long as it doesnt get still worse).
When advertising does start to recover  we have no idea when that will be  well be ready to make progress across the company.

Sincerely,
Donald E. Graham
Chairman of the Board
