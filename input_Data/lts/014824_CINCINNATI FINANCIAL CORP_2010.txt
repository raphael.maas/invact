To Our
Shareholders,
Friends and
Associates:
Financial Highlights
Your companys operating income* rose 27 percent
in 2010, covering shareholder dividends that have
increased each and every one of the past 50 years.
Only 10 other publicly traded U.S. companies can
claim this long record of cash dividend growth; its
preservation reflects the boards recognition of our
progress, its confidence in our plans and its vision
of the opportunities before us.
While 2010 pretax investment income rose
3 percent and underwriting results improved, the
contribution to net income from net realized
investment gains fell $114 million. This resulted in
net income of $377 million, $55 million lower
than the $432 million we earned in 2009. We
chose to retain $755 million of pretax unrealized
investment gains in our $3 billion equity portfolio
at year-end.
The equity portfolio accounted for just over a
fourth of our year-end invested assets. While that
level is roughly half of the pre-2008 level, it is
higher than industry norms. Equity investing
remains a key strategy for your company. Income
from our dividend-paying stocks helps offset
low bond yields. The long-term growth potential
of stocks also serves as a hedge against rising
interest rates and inflation.
Appreciation of common stocks contributes to your
shareholders equity. Fair value of the equity portfolio
rose 13 percent in 2010 on appreciation and
higher invested assets. Book value per share rose
6 percent over 2009, an increase of $1.66 after
dividends to shareholders of $1.59 per share.

Our value creation ratio shows how we are doing
for shareholders, summing the growth rate of book
value per share and the ratio of dividends declared
per share to beginning book value per share. Our
long-term target is an annual average of 12 percent
to 15 percent for the period 2010 through 2014,
and our 11.1 percent ratio for 2010 came within a
percentage point of that range. We expect several
initiatives to further improve our insurance
underwriting profitability and premium growth,
creating stronger value.
Stepping Up to the Starting LinE
Having reached major milestones for several
initiatives over the past two years, we stand ready
and eager to continue executing our plans. In
January, we marked the 60th anniversary of the first
policy issued by The Cincinnati Insurance
Company, a residential fire policy with total
premium of $17.50 for three years. From that
beginning, four independent agents with a good
idea and a lot of determination started a company
that now writes almost $3 billion of annual

premium and has more than $15 billion of
consolidated assets.
Sixty years later, we are stepping up to be a stronger
competitor than ever, just as determined as our
founders to be the best insurance company serving
independent agents in the United States. The
business model and marketplace advantages they
nurtured remain strong, and our efforts are adding
new advantages as we take our position on the
starting line ready to take that model into a new
era. Our initiatives are already having an impact 
regardless of economic uncertainty and the
stubborn commercial insurance market  and any
positive change in these respects would be wind at
our backs.
We see opportunity ahead. Over the past three
years, we have taken decisive actions that stabilized
our investment portfolio and our coastal
catastrophe exposures; brought our technology
systems up to date; improved our data management
and embraced predictive modeling and analytics;
developed robust enterprise risk management and
business planning frameworks; expanded our
operations and agency force; and invested in
training and expertise of our associates.
These ongoing efforts are making us nimble and fit
to provide our agents with more ease and efficiency
in doing business with us. That ease has historically
risen from the availability of our field
representatives in their offices and communities,
our respect for local knowledge and efforts to push
decision making to the local level, and our
commitments to provide outstanding claims
service and a stable market for their business
while controlling expenses. We continue this

agency-centered approach, bringing new tools,
capabilities and services to bear as we seek profitable
growth for our agencies and your company.
Focus on Underwriting
Our 2 percent growth of written premiums in
2010 exceeded industry average growth estimated
at 0.5 percent by A.M. Best, primarily due to our
expansion activities. Our combined ratio improved
almost 3 percentage points to 101.7 percent while
the estimated industry average ratio deteriorated to
103.0 percent. We see limited prospects in the
coming year for increased investment income;
rather, we expect to achieve our performance
objectives for earnings and book value growth
primarily by continuing to execute on strategic
initiatives to improve property casualty
underwriting results.
Each percentage point improvement on our overall
property casualty combined ratio can add
approximately $29 million to underwriting gains.
This is where we will apply our main efforts to
improve performance in 2011.
In 2010, we maintained a healthy level of
profitability in most lines of business. For all lines
except workers compensation, homeowners, and
excess and surplus lines, the combined ratio
averaged 95 percent. That profitable core of
business represented almost 80 percent of our
property casualty premium volume. We will
keep those lines healthy by staying disciplined
and avoiding underpriced business. Broader
use of predictive analytics will help us price all
risks with sufficient profit margin. Most
importantly, we will continue supporting our
strong agency relationships with knowledgeable,
local field staff, profit sharing and superior
claims service.
For each of the three challenging lines, specific
plans and initiatives already under way make us
confident we can achieve profitability as those
initiatives mature:

Workers Compensation: The 2010 loss and loss
expense ratio improved 29.6 percentage points,
reflecting favorable development this year after a
reserve charge for this line in 2009. We expect that
use of predictive modeling will be the primary
driver of further improvement, as analytics assist
our decisions both on individual risks and on the
overall book.
A multidisciplinary task force of claims,
underwriting, loss control and sales associates has
implemented additional initiatives such as our
program to systematically act on policies with the
highest loss ratios, increasing rates or nonrenewing
this business as indicated by a detailed, case-by-case
review. Use of a new supplemental application
informs underwriters about aspects of the
applicants risk management including return-towork
programs, safety practices, pre-employment
physicals and drug screenings. Claims initiatives
involve medical repricing; increased use of workers
compensation claims specialists; and use of new
claims kits to educate employers about safety,
claims reporting and cost control. Our new
workers compensation claims reporting center,
launched in January 2010, has significantly
decreased the lag between an injury and its first
report to us, leading to a faster response that benefits
the employee, the employer and your company.
We are expanding our loss control staff to allow
for more onsite inspections, both to gather
underwriting information and to assist employers
with loss prevention. Our representatives now
provide detailed quantitative and qualitative
assessments of employers workers compensation
practices to accounts with workers compensation
premiums over $50,000.
Homeowners: The 2010 homeowner loss and loss
expense ratio improved 18.8 percentage points,
benefiting from lower catastrophe losses and
improved pricing. We expect incremental
benefits from use of our tiered rating structure
and annual rate changes. Increases averaged
5 percent for policies renewing beginning
October 2009, with a slightly higher average
increase effective for renewals beginning in late
2010. These changes target rates more precisely to
correspond to the quality of each risk and improve
the quality of our homeowner book as confirmed
by insurance scores.
Catastrophe risk management actions also have
improved our book. Over the past three years, we

reduced our homeowner coastal exposures in
Alabama, Florida, Georgia and North Carolina
while introducing hurricane deductibles, tighter
underwriting guidelines and rate increases.
We targeted states with less volatility for personal
lines growth in order to diversify our book. We
made progress toward achieving scale, increasing
personal lines new business 20 percent in 2010.
Technology upgrades are now available in all
29 states where we market personal lines products,
increasing our overall ease of doing business
through improved workflows. These changes are a
competitive advantage, fully supporting our desire
to grow the business and improve profitability.
Excess and Surplus Lines: In its third year of
operations, start-up expenses have leveled off. The
combined ratio improved nearly 40 percentage
points to 115.4 percent, meeting our expectations
for 2010. Our initiatives to move the ratio under
100 percent include claims reviews that add to our
ability to react quickly to adjust coverages in


response to observed trends and to amend our
training, pricing or terms and conditions.
Additionally, we conduct reinsurer reviews of this
book, confirming that our underwriting appetite,
risk profile and reserving are conservative, our use
of deductibles is aggressive and our pricing is
adequate. We are shortening our exposure on risks
with traditionally long liability tails by actively
managing prior acts coverage and retroactive dates
on claims-made policies. And we are staffing to
enhance new business as agents become more
familiar with our new products and expansion of
eligible classes. Excess and surplus lines contributed
$49 million of our nearly $3 billion of property
casualty premium revenues. Growth in 2011 will
be disciplined, as pricing remains soft.
Maximizing Effectiveness
These challenging lines of business give us plenty
of room to improve our results. As we aggressively
pursue improvements, we will also work to amplify
some proven advantages:
 Almost 90 percent of our commercial policies
have premiums of $10,000 or less. Together with
our independent agents, we have served the small
business segment very well. In 2011, we will
develop a 360-degree plan to optimize our small
business product, technology, marketing, pricing
and services.
 Our success depends on the success of our
independent agents. We are working in 2011 to
support their businesses by providing more
planning support, more access to capital and an
updated contingent commission agreement
designed to help stabilize their agency revenues
and reward longer-term profitability.

 Our associates know and support our mission.
We can direct their talent and energy in the most
productive directions, advancing company and
personal goals, by creating performance metrics
that show how components of our value creation
ratio cascade to the team and individual level.
 Our independent agents and field representatives
excel in their consumer interactions, establishing
our reputation as a preferred carrier. As
technology makes it practical and desirable for us
to provide more direct services online and from
our headquarters, we are studying ways to best
align these interactions with consumer and
agent expectations.
Getting the Basics Right
We pay close attention to insurance basics and
work to get them right, year after year.
Insurance Reserves: Industry observers are voicing
concerns that carriers have cut corners by releasing
prior year reserves too soon, in effect subsidizing
current earnings with amounts that may be needed
to fund future liabilities. Our reserving philosophy
and practices remain unchanged. Our consistent
approach has resulted in 22 consecutive years of
favorable development on prior accident years
reserves. We continue to target total reserves in the
upper half of the actuarial range, and we believe
our carried reserves are in the same solid position
at the end of 2010 as they were a year ago.
Investments: Our practice is to cover insurance
reserve liabilities by investing in a diversified,
high-quality bond portfolio that far exceeds
those liabilities. These fixed income securities
assure our ability to fulfill our obligations to
policyholders, while we set the stage for long-term
growth by holding stocks that steadily increase
their dividends.
Reinsurance: Catastrophe reinsurance is a
particularly important tool to control the
variability inherent in our business. After
reinsurance, our maximum exposure to a 2011
catastrophic event that caused $500 million in
covered losses would be $88 million. $500 million
is more than three times the amount of damage
caused to our policyholders by any single event in
our history. In negotiating agreements with our
highly rated reinsurers, we applied savings to the
catastrophe program, prudently reducing our
maximum retention.
These practices protect your companys highquality
balance sheet, positioning us with
formidable financial strength. As 2010 came to a
close in December, A.M. Best affirmed our ratings,
including the A+ Superior awarded to our standard
market property casualty companies. Over the past
two years, we have stepped up our initiatives to
build capital, improve profitability and drive
premium growth. We believe these efforts position
us to create long-term value for you, our
shareholders, and for agents, policyholders and
associates. Thank you for your support.
Kenneth W. Stecher
President and Chief Executive Officer

John J. Schiff, Jr., CPCU
Chairman of the Board
Respectfully,