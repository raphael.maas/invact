To My Fellow Stockholders:

Last year, Schwab added almost 1million new client accounts
while maintaining a healthy balance sheet and delivering significant improvements to our client services and products - a solid performance in the best of times, and an outstanding one given the economic
reality of 2009.

Through Clients' Eyes
As we stuck to our strategy of focus ing on our clients - on seeing our business and the services we provide from their perspective - it became clear that everything we've done in our 35-year history led us to be the firm people needed during this very tumultuous time. Through it all,
Schwab remained a reliable source of financial services, a trusted provider of help and advice, and a stable firm committed to delivering great value - all when it was needed the most.

Let me put a little context around that.

A Tough Environment for Investors and Savers
Despite the market advances since March 2009, investors and savers continue to suffer. An estimated $12.6 trillion in household wealth has simply disappea red between mid-2007 and the third quarter of last year. Millions of Americans have lost their jobs, lost their homes, and lost faith in business and the economy.

In addition to a challenging investing environment, the prevailing ultra-low interest rate policies have made it difficult to earn an attractive return
on savings, hitting particularly hard those who depend on their savings for income. After spending a lifetime making responsible decisions about spending and debt accumulation, many of these savers have
suffered as their yields on cash investments declined to negligible levels. Understandably, some individuals began to focus less on investing and saving and more on shoring up the foundation of their financial picture - for example, paying down debt.

While these factors impacted our revenues in 2009, Schwab came through it all as a strong and solid company.
 


Financial Results
In 2009, clients continued to entrust their hard-earned money to Schwab, bringing in net new assets of $87.3 billion, far ahead of the pace reported by any competitor. Total client assets reached $1.42 trillion at year-end, compared to $1.14 trillion at year-end 2008.

Our asset-gathering strength led to a solid financial performance for the 12 months ending December 31, 2009.

�	Net revenues of $4.2 billion
�	Net income of $787 million
�	Diluted earnings per share of $0.68, compared to $1.05 the prior year, and pre-tax profit margin
of 30.4 percent, down from a record 39.4 percent a year ago. Both reflected the impact of the same low interest rate environment that affected our nation's savers.

To generate these  results in such  a challenging environment, we knew we had to be a very disciplined company. Overall, we reduced expenses by 7 percent by focusing on things we could control when so many
factors - such as market valuations and interest rates - were clearly not in our control. (See Letter from the CFO, page 20.)

Pursuing Our Purpose
One of the most important things we could control was to stay the course with our purpose and our client� focused strategy. When so much was changing all around us, what is unchanging about Schwab kept us strong - and moving forward.

It all starts with our purpose: to help everyone be financially fit. That is the lens through which we view our business decisions and our actions.

To fulfill our purpose, we pursue a clear and transparent strategy. Each quarter, when I report about the state
of our business, Italk about our progress against four strategic pillars: client loyalty; innovation; operating discipline; and One Schwab. These four pillars not only stake out what we intend to do, but they also signal the very specific behaviors that tell how we will carry out each pillar of our strategy:

L Building client loyalty by being service oriented,
2.	Innovating in ways that benefit consumers,
3.	Operating our company in a disciplined manner, and
4.	Leveraging capabilities across our company.

Helping everyone be financially fit compels us to think and act in different ways, and it's that alignment of our
 
purpose with our day-to-day behavior that is central to our firm's success. In looking back on the major accomplishments of 2009, it's clear that our actions delivered very specific benefits for our clients and for our business.

1.	Building Client Loyalty by Being Service Oriented In 2009, Schwab helped more people than ever, serving nearly 10 million accounts across Institutional Services and Investor Services, our two operating segments.

On the institutional, or business-to-business side, we served thousands of employers and third-parfy 401(k) recordkeepers, representing 1.5 million retirement plan participants. We also helped more than 6,000 independent investment advisors take care of their
clients' needs. These independent professionals, located around the country, typically serve as a fiduciary to
their clients - a role that requires their clients' needs to come first. This "client-first" philosophy aligns perfectly with Schwab's commitment to the individual investor, and we are proud of the leading role we play in that growing part of the industry.

Last year, as more commissioned brokers decided to leave their brokerage firm and join the growing number of independent investment advisors, Schwab helped a record 172 new advisory teams as they either started or joined an independent firm. We also introduced
an initiative to help independent advisors grow their business. Our "Make the Move" program saved clients of independent advisors millions of dollars in fees and, in the process, reduced the operating expenses of advisors we serve.

Among individual investors, we served 7.7 million client brokerage accounts and 722,000 banking accounts, which rose 62 percent with the success of our Schwab Bank High Yield Investor Checking� and the introduction of Schwab Bank High Yield Investor Savings�.

At our local Schwab branches, we conducted more than
a half-million client meetings. In our automated channels, we recorded more than 250 million client log-ons to schwab.com and routed 15.4 million calls through our automated systems. In addition, Schwab employees also answered 13.7 million phone calls with an average
speed-to-answer of 22 seconds.

During the most challenging days of early 2009 - when people needed someone to talk to - we didn't wait
for clients to call us. We introduced an outbound calling program and proactively contacted clients to offer
our best suggestions, shaped by our clients' interests. We also increased the number of branch workshops, online webinars, and other forms of client outreach.


In many branches, we assigned a dedicated professional to work with people who drop in without an appointment, making sure both clients and prospects get the help
they need.

As a result of our efforts, we continued to post extraordinary client loyalty results.  As profiled in a number of publications, the Schwab story of client loyalty is a true business success story. In 2009 , more retail clients said they would recommend Schwab than ever before as our internal Client Promoter Score for individual investors reached record levels. Schwab also ranked "Highest in Investor Satisfaction with Self-Directed Services" by J.D. Power and Associates, and remarkably, given our history as a discount brokerage firm, we ranked third in the U.S. Full-Service Investor Satisfaction Study for traditional  brokerage firms.


2.	Innovating in Ways That Benefit Consumers
From day one, Schwab has been an advocate for the individual investor - from pioneering discount brokerage, to launching no-load, no-transaction-fee Mutual Fund OneSource�, to leading the way in Web-based investment and banking services.

Throughout 2009, we took a series of bold, innovative steps to help investors and savers make the most of their money.

In the spring, we introduced a high yield savings account that, in the Schwab tradition, offered a great rate on every client's first dollar - no catches or fine� print "gotchas."
 

Also in early 2009 , we waived advisory fees for new client enrollments in managed portfolio programs, thereby giving clients the help they needed with investment advisory services even at the depth of the stock market decline. We also introduced new versions of our Web sites for individual investors, independent investment advisors, and retirement plan participants - all to very positive reviews.

Last November, we broke new ground in the fast-growing world of exchange-traded funds (ETFs). Eight new Schwab ETFs have some of the lowest operating ex,pense ratios on the market and can be bought and sold without commissions  in Schwab accounts if purchased online, regardless of the number of shares traded. These
ETFs can be purchased in blocks as small as one share per trade.

When we innovate at Schwab, our focus is on delivering client benefits. And in 2010, we will continue our legacy of innovating on behalf of consumers while challenging traditional approaches to financial services where we believe that clients' interests are not optimally served. In fact, we began this year by cutting our online equity commission to $8.95 for all clients, regardless of the size of their trade - or the size of their portfolio. We
also added to our suite of Schwab Managed Portfolios with new portfolios of ETFs, making it easier and more affordable for individual investors to obtain broad diversification in a single, professionally managed account with the low costs of ETFs.

3.	Operating Our Company in a Disciplined Manner
In order to deliver effective products and services at a great value for all clients - especially when
the environment put such tremendous pressure on revenues - we had to be disciplined and efficient in our internal operations.

Early last year, we faced very difficult choices as we worked to address this challenge. We made the painful decision to cut some jobs , but we made those decisions carefully and thoughtfully - and we simply refused to make cuts that would negatively impact client service. As a result of our operating discipline, we were in a position to deliver even greater value to clients while generating healthy financial performance for our stockholders.

In March, we lowered the expense ratio of the Schwab Hedged Equity Fund�. A month later, we enhanced our Schwab Target Funds by reducing expenses charged to investors, adjusting the asset allocation  "glide  path" to be more conservative as investors near retirement, and expanding the underlying funds that we invest in. Only two weeks passed before we cut fund fees again - this time for all Schwab equity index funds - to some of


the lowest levels in the mutual fund industry. We also simplified share classes so that aU Schwab equity index funds offer the same low fund expenses to investors regardless of how much they invest, starting with a low
$100 minimum.

This is our purpose put into action. Now, anyone can open a Schwab One� brokerage account and invest just $100 in a fund like Schwab S&P 500 Index Mutual Fund*, which has a net expense ratio (after waivers) of
.09 percent. In that case, investors can have access to all that Schwab has to offer for just 9 cents a year - less than a single dime.

4.	Leveraging Capablllties Across Our Company Our senior management team recognizes the power of teamwork and collaboration in driving long-term,
sustainable growth. In 2009, we diligently worked across business lines and departments to align on certain
core capabilities, including our people, our processes, and our technology. While each member of our Operating Council is responsible for running an individual business unit, our leaders understand that our mindset and our decisions must be made in the best interest of the entire Schwab organization.

For example, in 2009 we continued to reinforce sustainable,  company-wide  management  bench strength. Over the past several years, executives have been thoughtfully  rotated throughout  different  parts of the business, building knowledge, awareness, and skills they will need to lead Schwab for decades to come.

But the true power of a "One Schwab" philosophy is the ability to learn something once and apply it multiple times across internal organizational lines. From a technology standpoint, we leveraged what we learned from the redesign of schwab.com to benefit other client Web sites across the firm. We applied our Web expertise to our new site for 401(k) participants, which quickly became the top-rated participant site in the industry.

Leading Into the Future
As I continue in my second year as President and Chief Executive Officer of Charles Schwab, I have been thinking quite a bit about leadership - about what's expected
of my leadership of this firm, and what's expected of Schwab's leadership within our industry.

It is my deeply held belief that leadership is not a position you declare, but a right that you earn as others choose
to follow. At Schwab, our entire leadership team is driven by the desire to serve and benefit others. As a result, we are both pleased and humbled at the number of people who've chosen our firm to serve their financial needs.
 
Our faith in the individual investor has been rewarded with their trust in Schwab. Since 1993, Schwab has posted positive net new assets and new brokerage accounts for each and every quarter. That's a track record we're proud of, and one we intend to keep going.

As we look toward 2010 and beyond,- we will continue to focus on what is unchanging about Schwab - our purpose, our strategy, and our day-to-day behaviors.

�	We will continue to focus on the needs of both individual and institutional clients, delivering superior service and a great value.
�	We will be innovative on behalf of consumers, introducing targeted  new products and services and finding new ways to be a "game-changer"  in a financial services industry where many competitors would  prefer the status quo.
�	We will be disciplined in the way we operate and manage our capital so that we have resources to reinvest in an ever-improving client experience and the continued growth of our business.
�	We will manage the company from a holistic perspective, putting our investments and scale to work on behalf of the widest range of clients - even as we work together to build financial literacy for others who may not be our clients. As a result, we will continue to share our expertise through our Schwab
MoneyWise� program as well as our work with nonprofit organizations like SingleStop and Boys & Girls Clubs of America .

At Schwab, our advocacy for investors and savers isn't just words on paper - it is the way we do business every day. You can be assured that we will continue to walk the talk, because when we lead the way to financial fitness, we want ever more people to follow. After all, our success is built one client at a time - as we see the world through clients' eyes.







Walt Bettinger
March 10, 2010
