To our shareholders
2002 was a year of many challenges. Our financial results clearly were
disappointing, and we have taken actions to improve our short-term
performance. Through these actions and the fundamental strengths of
JPMorgan Chase, we begin 2003 with a renewed confidence that our
business model and strategy will produce superior returns over time.
Let me give you my perspective on what we are doing to realize the full
potential of the firm and to create enduring shareholder value.
We are working through our performance issues. The transition from 1990s boom to
post-2000 bust in equity markets has created significant challenges for all participants in the investment
banking and investment management businesses. As stock prices fell for the third consecutive year in 2002,
business volumes  in activities ranging from underwriting securities to advising on mergers and acquisitions
to managing investment portfolios  remained in a depressed state or declined still further.
No one can predict when a recovery may occur in global capital markets, and we are not holding our
breath until that happens. On several fronts, we have taken steps to raise our level of profitability, regardless
of external conditions.
Providing credit remains a key strategic weapon, but we will moderate our concentrations, reduce our
overall exposure and use credit more effectively.
Weve also moved to align our investment banking cost structure with the current weak revenue environment,
committing to staff reductions, resizing our business and geographic presence consistent with
near-term opportunities, and taking various belt-tightening measures, all of which are intended to generate
approximately $700 million in gross savings. Exclusive of severance and related costs, we aim to maintain
the overhead ratio of our investment bank at no more than 60% of revenues. We have taken similar steps
in our investment management and private banking businesses to improve the operating margin.

While we believe in the strategic importance of JPMorgan Partners (JPMP) to the firm, we are acting to
reduce the impact of its earnings on the overall firms results. JPMP is exiting selected businesses and investments
that are not central to its operations. JPMP also is increasing the percentage of third-party capital
invested in its core direct investment portfolio, as well as diversifying its portfolio to reduce concentrations
in specific industries, such as telecommunications and technology. Over time, we will reduce total investments
in JPMP to approximately 10% of the firms common stockholders equity, compared with the 2002
level of approximately 20% and the 2001 level of approximately 23%.
These actions address the short-term challenges facing us. We are well-prepared for a continuing weak
market environment. When the revenue picture brightens and credit stabilizes, we will be in an excellent
position to take full advantage of the upswing.
We have the right long-term strategy to compete and succeed.
Simply put, our strategy is twofold:
 To be a diversified financial services firm with a leadership position in each of our businesses.
 To provide our clients integrated solutions drawing on a wide variety of products and services.
Diversity is important because financial services are cyclical, with each type of business presenting different
risks at different times. Leadership positions are important because they tend to have the highest return and
attract the best people. And integrated delivery across a broad product set is what our clients want because
of its superior convenience and price efficiency.
There are some who would say that our poor performance in 2002 is proof that this strategy is not effective.
My view is that the strategy is right but that the execution of our strategy was not as good as it should
have been. Judgments based on short-term results often can be inaccurate, and we look to the next several
years to vindicate our view.
Diversification certainly was helpful in 2002. Our consumer banking business, Chase Financial Services,
delivered another year of strong performance, with a 24% increase in operating revenues and a 62%
increase in operating earnings, leading to a return on equity of 24%.
Consumer spending remained strong in the United States in 2002. We were able to capitalize fully on this
opportunity through our leadership franchise in all three national consumer credit businesses  credit cards,
home and auto finance  and we grew faster than our competitors did in 2002 in both net income and
revenue. Besides our strength in consumer credit, Chase is the number one primary relationship bank
(based on deposits) for individuals, as well as the number one full-service bank for small businesses, in the
New York tri-state area. Our leading middle market services business provides opportunities across both our
retail and wholesale product array.
Our Treasury & Securities Services business is another source of diversification and earnings stability. With
a strong return on equity of 23% in 2002, this is a highly attractive part of our wholesale franchise. Strong
expense discipline helped produce an increase of earnings of 7% on a revenue increase of just 2%. This
low revenue growth reflects the challenging conditions in wholesale financial markets.

Regarding investment banking, we know that this is a high-return business over the long term despite
todays poor conditions, and we are well-positioned for a rebound. We take four powerful advantages
 a huge client franchise, broad product capabilities, global presence and intellectual capital  and bring
them together for our clients. We have demonstrated that integrated delivery works, holding on to  and,
in many cases, gaining  market share in spite of the weak economic environment.
We believe that the winners in investment banking increasingly will be the firms best able to provide
innovative solutions that draw upon leadership positions in a broad array of products, services and markets.
Through the success of our merger integration efforts, we have become adept in turning this concept of
delivering the whole firm to our corporate and institutional clients into a functioning reality. That is
reflected in a global brand survey on our wholesale businesses released in late 2002, in which we moved up
to second place in the overall category of favorable brand recognition for investment banking.
Our investment management and private banking businesses offer considerable potential as financial
markets stabilize. In the face of three years of falling markets, we have reduced annual expenses in this area
by more than $500 million while preserving the quality and breadth of our investment advice and wealth
management to institutional, high net worth, and retail clients. Our pre-eminence in private banking,
international mutual funds, and institutional investing in core and alternative assets positions us among the
world leaders in active asset management, with $636 billion of assets under supervision at year-end.
To realize the full power of our business model, we have devoted considerable resources to improving our
efficiency. We have under way a broad range of re-engineering efforts, many using the rigorous methodology
of Six Sigma. In 2002, Six Sigma projects yielded more than $400 million of net benefits, and we nearly
doubled the number of employees with expert Six Sigma certification. We also are leveraging one of our
most important competitive advantages  technology. In late 2002, we finalized a groundbreaking, sevenyear
outsourcing agreement with IBM Global Services for our technology infrastructure that will create
significant new value for our clients, shareholders and employees.
We are providing greater transparency and accountability. Any time there is the
spectacle of fraud and corruption, it highlights the need for transparency, accountability and strong corporate
governance. The vast majority of the thousands of public companies in the United States do business in
a fair and honorable way. They recognize that our free market system is highly dependent on trust and that
integrity is a prerequisite for continued survival and growth. As this firms chief executive officer, I welcome
the new requirement that the Chief Financial Officer and I personally certify the firms financial statements.
JPMorgan Chase always has had a strong commitment to high standards, including a strong, independent
Board, good governance practices and auditor independence. We have been  and continue to be  a
leader in going well beyond required disclosure, as this 121-page shareholder report demonstrates. We also
have taken steps to strengthen procedural safeguards in a number of areas in 2002.
Included among the changes we have initiated are: (1) the creation of policy reviews and a senior-level
Policy Review Office to examine complex transactions; (2) the adoption of the Investment Protection
Principles relating to investment research; (3) the decision to expense options in our own financial statements;
(4) the creation of stock ownership guidelines requiring each Executive Committee member to retain
75% of the net shares of stock received from stock grants and options; (5) a prohibition against any new
engagements of the firms external auditors, other than for audit and audit-related services and for tax

advice; and (6) the implementation of proposed New York Stock Exchange listing standards for corporate
governance. Collectively, these changes provide greater transparency for all investors and stricter accountability
in the maintenance of high ethical standards inside this firm. These and other steps we have taken are
discussed in a special section on governance and business practices later in this report.
We are developing a culture focused on performance and leadership.
Our firms principal output  the way we add value to the marketplace  is delivering financial solutions to
help our clients realize their aspirations and their potential. Our main resource is the depth and breadth of
our intellectual capital in the form of the people who work at JPMorgan Chase.
In the long term, therefore, our success rests squarely on their ability to execute  making full and effective
use of the tremendous capabilities and talents within the firm.
For our people to maximize these capabilities, great communication, teamwork and leadership are required.
A key tool in this effort is our Leadership Center  called LeadershipMorganChase (LMC)  which is helping
to foster all three ingredients. Completing its first full year of operation in 2002, LMC engaged nearly 1,000
of the top officers in the firm who went through an extensive program, and an additional 40,000 employees
took part in shorter sessions. The results were impressive. Our 2002 climate survey, which encompasses all
of our employees worldwide, shows significantly higher scores in understanding the strategy, commitment
to the firm and satisfaction for those who have experienced this program.
Everyone who participates in LMC is encouraged to move out of his or her own comfort zone; to challenge
me and other senior leaders; to grapple with real issues; to be innovative and decisive  in short, to act as a
leader who takes responsibility for fixing problems and driving the firms vision and strategy.
Looking ahead, my confidence in the firms potential rests solidly on the specific actions that we have taken
and the inherent strengths of our franchise. In 2003, there are good reasons for optimism regarding the
global economic outlook. It appears that inflation has been effectively muted in much of the world. In the
United States, the absence of inflation has been combined with rising productivity, or increasing output per
work hour  the best single measure of economic health. Productivity growth in the United States reached
an impressive 4% in 2002. That suggests that transformation of the workplace, fueled by innovation,
technology and globalization, has not ended or even slowed. By the end of 2003, our economists expect
global growth to return to the 3% annual pace that was the average during 1995 to 2000.
While the stage is being set for sustained, long-term growth, we in this firm are not counting on better
economic or market conditions to trigger much-needed improvement in our own performance. In 2003,
there will be an even greater emphasis on performing in each of our businesses; taking action toward
specific, measurable goals; and holding people accountable for results.
We have the right model, the right strategy, and the right people. We will stay the course, with confidence
in our long-term potential. What we need now is better performance and improved execution. That will be
the unrelenting focus of JPMorgan Chase in 2003.
William B. Harrison, Jr.
Chairman and Chief Executive Officer
March 3, 2003