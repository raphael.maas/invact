TO OUR STOCKHOLDERS,

A decade ago, I had the privilege of working in the energy industry
in England.
The company I was responsible for leading was based in Bristol  a
city much like Milwaukee with a long history in engineering, design,
and manufacturing.
During those years abroad, I often heard the phrase Ship shape and
Bristol fashion  a phrase that celebrated a job well done, a pride in
setting a new standard of excellence.
Fast forward to today. A different time. Different challenges. But
with apologies to that old English phrase, Wisconsin Energy finished
2008ship shape and Milwaukee fashion. From customer satisfaction
to network reliability to progress on our Power the Future plan, the
company made great strides during the year. And in a tough economy,
we posted solid financial results.
Here are some of the highlights of 2008.
HIGHER Earning s
Wisconsin Energys earnings from continuing operations grew to $3.03
a share in 2008, an increase of 6.7 percent over 2007. Cash from
operations rose over the previous year as well to a new record high.
In one of the most volatile years in history for the financial markets,
Im pleased to report that Wisconsin Energy stock performed in the
top 20 percent of all stocks listed in the United States and in the top
15 percent of major American utilities during 2008. And Wisconsin
Energys total shareholder return over the past three years exceeded
the return you would have earned in the S&P 500, the Dow Jones
Industrials, the NASDAQ, or our peer group of regulated utilities.
Last December, our board of directors approved a new policy that
targets stronger growth in future dividends. The new policy paved
the way for a 25 percent increase in our dividend effective in the first
quarter of 2009. The new dividend is equivalent to an annual rate of
$1.35 per share. This marks the sixth consecutive year that weve been
able to raise your dividend.
Our new target calls for us to pay out between 40 and 45 percent of our
earnings in dividends each year from 2009 through 2011, and a slightly
higher payout ratio of 45 to 50 percent starting in 2012.

Power the Future Update
If youve followed the company over the past several
years, you know that our major strategic initiative
continues to be our Power the Future plan  a
massive and much needed upgrade to strengthen
the energy infrastructure of the region. The plan calls
for completing four new generating units between the
years 2005 and 2010, plus the building of Wisconsins
largest wind farm to date. When all is said and done,
our investment in these infrastructure projects will total
more than $2.9 billion.
As the theme line on the cover of this report points out,
were literally seeing the future in the making. During
2008, with the completion of two key components of
our Power the Future plan, we added more megawatts

of capacity to our generating fleet than in any other
year in our history.
In May, the second 545-megawatt unit at our Port
Washington Generating Station began commercial
service. And later that same month, the Blue Sky Green
Field wind farm began producing 145 megawatts of
carbon-free electricity for our customers.
Our Port Washington station consists of two identical
natural gas-fired units. Both units were completed
on time and on budget and both were recognized by
Power Engineering magazine as the Best Gas-fired
Project of the Year.
Blue Sky Green Field was completed under budget
and ahead of schedule. It consists of 88 wind turbines
arrayed across 10,600 acres of farmland in eastern
Wisconsin. This project is one of several targeted
efforts were undertaking to deliver more energy from
renewable resources and to comply with Wisconsins
renewable energy law.
To meet the state standards, we must continue to
add to our portfolio of renewable generation. We
took the next step on this path in October when we
filed with the Wisconsin Public Service Commission
for approval to build the Glacier Hills Wind Park,
approximately 45 miles northeast of Madison. Our
plans call for the placement of up to 90 wind turbines
that could produce approximately 200 megawatts of
electricity. Cost estimates range from $340 million to
$530 million, depending on the turbine size and other
factors. Commercial operation is expected by 2012.
Oak Creek Expan sion
Certainly no discussion of our progress would be
complete without a review of our largest Power the
Future project in Oak Creek, south of Milwaukee, where
we are building two 615-megawatt coal-fired units.
Im pleased to report that we reached an agreement
last summer with the two environmental groups that
had been opposing the water intake permit for the
plant. U nder the settlement, the groups withdrew
their opposition. The deadline for appealing the water
intake permit subsequently passed, so we now have
all the permits needed to complete the Oak Creek
expansion, and all litigation surrounding these permits
has been resolved.
On the construction front, Unit 1 and the common
facilities at Oak Creek were 85 percent complete at
the end of 2008. Unit 2 was approximately 37 percent
complete. Bechtel Power Corporation, our general
contractor, continues to target the completion of Unit 1
by the end of this year and Unit 2 eight months later at
the end of August 2010.
As you may know, in late December, Bechtel
submitted formal claims requesting that we and our
two partners in the Oak Creek project pay $485
million above the amount that is called for in our lump
sum, turnkey contract. Of that total, $413 million is
for cost and schedule relief that Bechtel relates to
severe weather, changes in local labor conditions,
and other factors.
The remaining $72 million is for the alleged effects of
changes and delays prior to our issuing the full notice
to proceed with construction in July 2005. We believe
Bechtel was fully compensated for any and all impacts
of the delayed start.
Finally, Bechtel has asked for six months of relief from
liquidated damages beyond the September 29, 2009,
guaranteed completion date for Unit 1, and three
months of relief beyond the September 29, 2010,
guaranteed date for Unit 2.
Were reviewing the claims and have asked Bechtel
for additional information. We expect the claims to be
resolved through the formal dispute resolution process
outlined in the contract. If were not able to reach
agreement in non-binding mediation, we will move
to binding arbitration. The process could take up to
18 months to complete.
The Cha llenge s Ahead
Theres no question that 2009 will be a difficult year for
the Wisconsin and U.S. economies. Our service area
has a diverse commercial and industrial base that often
mitigates the impact of economic downturns. But we are
not immune to the challenges facing our customers. In
the fourth quarter alone, consumption of electricity by
our retail customers declined by 4.9 percent.
To prepare for the economic turmoil that lies ahead,
weve taken several steps to lower our costs for 2009 
tightening operating and maintenance budgets, deferring
certain capital projects, restricting hiring, and freezing
officer salaries. We will continue to monitor closely the
trends in our revenues and our spending. And well take
additional steps, if necessary, to stayship shape.
Like every business buffeted by the economic times,
we face near-term challenges. Yet today, as I close this
letter, Im more optimistic than ever about our future 
a future in which our company is positioned well for
exceptional performance and solid growth. We have the
tools. We have the focus. We have the energy.
On behalf of our entire management team, thank you
for your confidence and support.

Sincerely,
Gale E. Klappa

Chairman, President and Chief Executive Officer


