TO OUR SHAREHOLDERS, FRIENDS AND ASSOCIATES:
FINANCIAL REVIEW
Your company reported $432 million
of net income for 2009, up less
than a percent from the 2008
result. Book value per share at December 31
reached $29.25, 14 percent above the
year-end 2008 level. Property casualty
surplus rose to $3.648 billion compared
with $3.360 billion at year-end 2008.
Shortly after our 2009 earnings
announcements, A.M. Best affirmed its
stable outlook and our A+ Superior
insurer financial strength ratings, awarded
to fewer than 11 percent of property
casualty insurers.
How we got to this point says much
about your company and our cautious,
fairly positive outlook on 2010 and beyond.
At the beginning of 2009, the crisis in
the financial markets had taken its toll on
our investment portfolio, reducing our
income from stock dividends and our
realized and unrealized investment gains.
Broad economic weakness, together with
a prolonged period of soft pricing for
commercial insurance, pressured our
premium revenues even as loss costs
continued to rise. While our capital,
liquidity, financial flexibility and capacity
for future growth remained exceptionally
strong, the declining profit trends
were unsatisfactory.
We had put our enterprise risk
management program into high gear in
mid-2008, working to identify specific
metrics that define our risk tolerance and
specific plans to stay inside their
boundaries. In early 2009, many initiatives
already were under way to stabilize and
conserve our capital, drive growth of our
insurance business and improve
profitability. Our sense of urgency was
strong. Nevertheless, by the end of the
first quarter, high catastrophe losses led to
a large underwriting loss for property
casualty insurance operations. The
declining trends continued for investment
income, for the investment portfolio and
for our property casualty surplus.

The second quarter brought little relief.
Book value and surplus rose on better
securities valuations. However, higher
pretax investment interest income only
partially offset lower dividend income.
High second-quarter catastrophe losses
piled on top of the first-quarter losses, and
we added to our reserves for prior period
workers compensation loss estimates.
Fitch Ratings cited the unfavorable
underwriting performance as it lowered
our insurer ratings to A+(Strong), albeit
raising the outlook to stable. We kept
working on our initiatives.
Solid earnings and favorable balance
sheet trends emerged in the second half.
By year-end, we were able to report three
consecutive quarters of increasing assets,
book value and statutory surplus, as well
as two consecutive quarters of property
casualty underwriting profit. Mild
weather prevailed, partially offsetting the
effects of continued price competition
and lower payrolls and sales for businesses
that pay premiums based on those
measures. Securities valuations rose, and
in the fourth quarter, pretax investment
income resumed a growth trend. Property
casualty operations, life operations,
investment income and investment gains

all contributed during the second half to
the rise in book value. While we are
not yet satisfied, improving trends have
returned to your company.
FOUNDATION FOR THE FUTURE
The insurance business is not for the
faint-hearted who are distracted or
discouraged by near-term events and
results. Pushing negativity of the first half
into the background, our leaders and
associates kept in the foreground the
initiatives that would position your
company to grow profitably in years to
come. Our 2009 progress was significant.
CAPITAL:
 Completed the rebalancing of our
$10.562 billion investment portfolio,
including the first-quarter sale of our
remaining Fifth Third Bancorp shares
and ongoing transactions to manage
issue and sector concentrations within
guidelines. At year-end, our equity
holdings were 25.4 percent of invested
assets. Our largest equity sector is
healthcare, at 18 percent and our largest
equity holding is Procter & Gamble, at
5.8 percent of our publicly traded
common stock portfolio or 1.4 percent
of the investment portfolio.
 Continued to build our bond portfolio
with laddered maturities to protect
against increasing interest rates and the
corresponding decline of fair value.
Our $7.855 billion bond portfolio had
an average rating of A2/A at year-end
and a 35 percent increase in fair value
during 2009.
 Maintained parent company cash and
invested assets of $1.040 billion at
year-end to support financial flexibility
for the insurance companies, liquidity
to support dividend consistency and
low debt leverage to support strong
credit ratings.
AGENCY RELATIONSHIPS AND GROWTH:
 Demonstrated our commitment to
agents by accelerating our major
technology projects to increase their
operating efficiency and by providing
them with field, headquarters and
online training options on the new
systems. (See Serving Agents with
Improved Technology on Page 4.)
 Expanded the size of our agency force,
the product lines we offer those
agencies and the geographical diversity
of our operations. We established
87 new agency relationships in 2009,
including our first agencies in Colorado
and Wyoming. We staffed additional
territories in Texas, which we just
entered in late 2008.
 Increased local expertise available to
agents and policyholders by adding to
our staff of workers compensation
claims specialists and managers as well
as loss control specialists.
 Responded promptly and fairly to
more than 15,000 first-half catastrophe
claims in Cincinnati style, earning
policyholder loyalty to our company
and agents.
PROFITABILITY:
 Developed predictive analytics and
underwriter training to improve risk
selection and pricing accuracy and
adjusting rates and rate/credit structure
 all actions designed to begin restoring
profitability to our currently
unprofitable homeowner and workers
compensation lines.

 Trimmed headquarters expenses by
focusing on department level spending.
 Identified our risk tolerance for
catastrophe exposures and acted where
necessary to begin moving within the
boundaries. Modeled projections
demonstrated the effectiveness of
these actions and plans, helping us
to negotiate better terms on our
2010 catastrophe reinsurance agreements.
 Protected our balance sheets by
accurately setting case reserves and by
increasing total reserves for workers
compensation estimated losses when
new information caused assumptions in
our calculations to change. Our overall
reserve development for prior periods
was again favorable in 2009, reflecting
the consistency of our conservative
reserving practices. Overall reserves
remain well into the upper half of the
actuarial range.
We were buoyed by a strong sense of
accomplishment even before the bad
weather subsided in the third quarter.
Most of the impacts from this work in
2009 will accumulate over time, increasing
the stability of our investment and
underwriting results and cementing the
agent relationships that distinguish your
company and help build long-term
shareholder value.
TRANSITIONS, CONTINUITY
AND STRATEGY
Your companys longstanding record
of annual dividend increases is a key
contributor to that shareholder value.
As many public companies decreased or
suspended their dividends in 2008 and
early 2009, our board continued ours then

acted in the third quarter of 2009 to
increase it. Your new indicated annual
dividend was $1.58 per share, up
2 cents, signaling the boards confidence
in our financial strength and flexibility
and managements ability to position the
company for future performance.
This action also signaled our values of
consistency, predictability and a long-term
perspective  values that create steadiness
in our management of market cycles, trust
in our insurance relationships with agents
and policyholders and transparency in our
communications with investors. Those are
values that have characterized your
company since its founding in 1950 by
four independent agents. We regretfully
note the passing in early 2010 of our last
living founder, Bob Schiff. (See our
tribute on the inside front cover.)
The composition of our board was
unchanged in 2009, with a new
independent director joining us early in
2010. Linda W. Clement-Holmes is a
talented and high-achieving Procter &
Gamble executive with extensive
leadership experience in technology
strategy, management and
implementation. She brings expertise that
complements that of other directors,
rounding out our board. Linda is serving
on a new, 14th board seat and on the
boards independent audit committee.
Our vice chairman and retired
president, Jim Benoski is not standing for
re-election at this years annual meeting of
shareholders, as previously announced.
With his departure, the number of board

seats will return to 13. (See our tribute on
Page 13.)
The board has approved our enterprise
strategic plan for 2010. They will measure
our success executing the plan in several
ways. The value creation ratio is our
primary measure of progress. We believe it
captures the contribution of our insurance
operations, the success of our investment
strategy and the importance we place on
paying cash dividends to shareholders.
It has two components: 1) our rate of
growth in book value per share plus
2) the ratio of dividends declared per
share to beginning book value per share.
For the period 2010 through 2014, we
continue to target an annual value
creation ratio averaging 12 percent to
15 percent.
Several goals are the keys to increasing
our book value and achieving that target:
1) year-over-year property casualty
premium growth exceeding the industry
average of our insurance business;
2) a combined ratio consistently under
100 percent; 3) total return on the equity
portfolio exceeding total return on the
S&P 500 Index; and 4) year-over-year
growth of investment income.
Those are ambitious goals, and well
stretch to meet them. We will act in 2010
to manage capital, to make it easier for
agents to do business with us and to
enhance our ability to improve and
sustain profitability. While many of the
same initiatives described above are
ongoing, some new initiatives are notable.
MANAGING CAPITAL
We will again work to maintain a
diversified investment portfolio, applying

our risk management guidelines and
balancing our needs for current income
and long-term appreciation. Well also
further develop our comprehensive,
enterprise-level catastrophe management
program, including regional guidelines
that work with our underwriting and
reinsurance efforts.
EXCEEDING AGENT EXPECTATIONS
Our most important point of
differentiation and competitive advantage
is our agent relationships. Our 2010
emphasis is squarely on service, and on
our commitment to make it easier for
agents to do business with us. In 2010, we
will develop short- and long-term
technology plans, also gathering and
acting on data that measures agent
satisfaction with our systems and service.
We will develop department level service
improvement plans and customer service
training programs for associates. Well
provide direct policyholder services that
our agents say they want for their clients.
By taking service to the next level, we aim
to be the carrier of choice for each
agencys best business. (See Serving
Agents with Improved Technology on
Page 4 and Serving Policyholders on

DRIVING GROWTH
To grow our insurance business, we will
focus our resources on markets where our
penetration is low and opportunities are
high. We expect to appoint 65 new
professional agencies in 2010, in a range
of sizes and with aggregate annual
premiums of about $1 billion with all
carriers they represent. We will give our
product portfolio attention, further
broadening and diversifying the types of
commercial products offered (See Serving
Our Agents with a Full Range of Products
on Page 7).
IMPROVING PROFITABILITY
To sustain underwriting profitability
through all cycles, we are continuing in
2010 to develop pricing capabilities for
each line of business and remediation
plans for each underperforming line of
business. We also expect to develop more
expertise for larger, complex risks. Well
reduce and manage expenses, moving
toward operational budgets at the
department level to help managers
maximize use of resources. For each of
these efforts, well provide improved
management and associate training and
establish metrics that ensure accountability.
ACCOUNTABILITY
Our 2010 plans and metrics fully
support accountability of executives to the
board and shareholders, of managers to
department heads and of associates to
supervisors. Additionally, shareholders
approved the Annual Incentive
Compensation Plan of 2009 at last years
Annual Meeting of Shareholders. The
board of directors recently adjusted the
balance of compensation components for
executive officers, implementing this plan
that makes the vesting of awards
contingent on attainment of specific
company performance metrics. In turn,
except in unusual circumstances, the
executive officers no longer will receive
nonperformance-based, discretionary
bonuses that other associates continue
to receive.
Regardless of any economic upturn or
market cycle changes that may or may not
occur in 2010, we are confident in our
ability to build on the improving trends
and significant achievements of 2009.
We thank our loyal shareholders for the
opportunities to accomplish more for you
in 2010.

Respectfully,

John J. Schiff, Jr., CPCU Kenneth W. Stecher
Chairman of the Board President and Chief Executive Officer