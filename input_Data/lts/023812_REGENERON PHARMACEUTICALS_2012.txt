Dear Shareholders,
2012 was an extraordinary year for Regeneron.
We managed one of the most successful product
launches in the history of our industry, driving
revenues to $1.4 billion and the Company to
full-year profitability for the first time. Our first
anti-cancer drug was approved. The readers of
Science magazine voted Regeneron the best
biopharmaceutical company in the world to
work for. Our stock market value tripled. We
made progress with our pipeline, with three
programs now in Phase 3 testing. We are
realizing a 25-year dream: to apply science to
the successful pursuit of new medicines and,
in the process, to build a vibrant enterprise. Lets now dig a bit deeper into the major events and accomplishments of 2012:
The EYLEA (aflibercept) Injection launch: EYLEA, approved in 2011 for the treatment of wet age-related macular degeneration (wet AMD), achieved product sales of
$838 million during its first full year on the U.S. market. This secured EYLEA a place in
pharmaceutical history as one of the five most successful new product launches ever
in the biopharmaceutical industry.
EYLEA regulatory approvals: Bayer HealthCare, our collaborator outside the United
States, obtained approvals for EYLEA in wet AMD in the European Union, Japan, and
several other countries, and EYLEA was granted approval in a second indication in the
United States: macular edema following central retinal vein occlusion (CRVO). This eye disease is characterized by leakage of fluid behind the retina that in the absence of successful
treatment often causes severe vision loss. EYLEA reduces this fluid buildup by blocking a
blood vessel growth factor called vascular endothelial growth factor, or VEGF.
ZALTRAP (ziv-aflibercept): Our anti-cancer agent was approved in the United States
and launched by our collaborator, Sanofi, for patients with metastatic colorectal cancer
(mCRC) that is resistant to or has progressed following an oxaliplatin-containing treatment regimen. Sales from launch in August through December were $32 million. Launches in Europe are under way following approval in the European Union in February 2013.
Progress with our pipeline: We advanced several drug candidates in clinical
development. See page 7 for details.
Financial performance: We had our first full year of profitability in 2012. For the year,
the Company reported non-GAAP net income of $530 million and year-end cash,
marketable securities and trade receivables of almost $1.2 billion.#1 biopharmaceutical employer: We are gratified that the prestigious journal
Science, in its annual reader poll, named Regeneron the best place to work in the
global biopharmaceutical industry. In 2011, we placed second. Placing at the top
of the Science rankings, as well as receiving a Best Places to Work award from
The Scientist magazine for the fifth consecutive year, is validation that we are
maintaining a science-driven culture, one that enables us to attract and retain top
scientific talent. We are also pleased to have been recognized as Biotech Company of
2012 by Scrip Intelligence, a leading pharmaceutical industry publication.
Emergence as one of the largest biotechnology
companies in the world: We are now a major player
in biotechnology, whether the metric is market capitalization (approximately $20 billion at the time this
annual report went to press), revenue ($1.4 billion
in 2012), spending on research and development
($626 million in 2012), biologics manufacturing
capacity (where we rank among the top 15), or
number of employees (more than 2,000).
Building for the future: We hired our 2,000th
employee in March 2013; we broke ground on new
buildings at our Industrial Operations and Product
Supply Group (IOPS) site in Rensselaer, New York to increase production capacity;
in early 2013, we signed a lease to occupy two more buildings that will be erected on
our main campus in Tarrytown, New York, to provide more laboratory and office space,
and we opened our first office abroad, in Dublin, Ireland, to facilitate coordination
with Sanofi and Bayer HealthCare and with our global product supply chain.EYLEA (aflibercept) Injection: Growth Story in a Single Product
We have consistently gained market share for EYLEA in wet AMD in the United States
since launch, and based on informal physician surveys we believe the EYLEA share was
approximately 22 percent at the end of 2012. According to our market research, most of
our new EYLEA starts have been with newly diagnosed patients, while about one third
of patients have been switched to EYLEA after treatment with ranibizumab (Lucentis)
or off-label bevacizumab (Avastin).
We are optimistic about the outlook for EYLEA sales by
Bayer HealthCare in Europe, Japan, Australia, and other countries where EYLEA recently has been launched.
The branded wet AMD market outside the United
States is roughly equal in size to the U.S. market.
Macular edema from CRVO, as well as new indications,
may be other sources of growth for EYLEA. Initial
results from the first of two Phase 3 studies in Diabetic
Macular Edema (DME), a common complication of
diabetes, should be available in 2013. We and Bayer
HealthCare are also conducting Phase 3 studies in another retinal condition known as Branch Retinal Vein
Occlusion (BRVO).
ZALTRAP (ziv-aflibercept): Our First Approval in Cancer
The U.S. and European approvals of ZALTRAP give Regeneron a foothold in oncology. ZALTRAP was approved based upon its use in combination with a chemotherapy
regimen known as FOLFIRI versus FOLFIRI alone in a Phase 3 trial. In this study, patients presented with metastatic colorectal cancer that was resistant to or progressed
following treatment with a chemotherapy regimen that contained oxaliplatin.
Sanofi will commercialize ZALTRAP (ziv-aflibercept) worldwide as part of our 50/50
collaboration. As we believe the future of anti-cancer therapy is to combine biological
agents in the way that chemotherapy drugs often are used together, we have begun a
Phase 1 trial testing the combination of ZALTRAP with a Regeneron-developed antibody to a target called angiopoietin-2 (Ang 2), another blood vessel growth factor.
ARCALYST (rilonacept): For a Rare Inflammatory Condition
Sales of our third marketed product, ARCALYST, for cryopyrin-associated periodic
syndrome (CAPS), were $20 million in 2012, similar to the prior year. We were
disappointed that in 2012 the FDA was not
satisfied with the Phase 3 clinical trial data
we presented for the prevention of gout flares
in patients initiating uric-acid lowering
therapies and asked for a new clinical trial. We
will continue to sell ARCALYST in the United
States but do not currently have plans to conduct further trials in gout.
A Deep Pipeline
We currently have three drug candidates in large randomized, placebo-controlled Phase 3 trials. These include Phase 3 programs with EYLEA for
DME and BRVO; with alirocumab, our antibody to the novel cholesterollowering target PCSK9; and with sarilumab, our antibody to the interleukin 6 receptor
(IL-6R) for rheumatoid arthritis. A fourth drug candidate, dupilumab, our antibody to the interleukin 4 receptor (IL-4R), will shortly move into Phase 2b studies in atopic
dermatitis and asthma. Except for EYLEA, all of these product candidates are being
co-developed with Sanofi.
The Phase 3 ODYSSEY program of alirocumab is the largest clinical program in our
history: Ten trials in which the primary endpoint is LDL cholesterol reduction are
recruiting approximately 4,000 patients, and a trial to measure the impact of alirocumab
on cardiovascular death and disease is expected to enroll 18,000 patients. All but two of
these studies are evaluating the use of alirocumab with a statin drug compared to the
statin or other lipid-lowering agent alone. The Phase 3 program builds on the results of
three positive Phase 2 trials reported and published in prestigious medical journals last
year. We anticipate reporting the first Phase 3
results later this year and the balance, except
for the outcomes study, in 2014.
Our collaborations with Sanofi and Bayer
HealthCare remain central to our financial and
clinical strategies. We earned total collaboration revenues of $494 million from Sanofi and
Bayer in 2012. The majority of these revenues
are direct offsets to the research and development expenses we incurred on collaboration
projects. With product development expenses reimbursed to a substantial degree by
our collaborators, we are in the favorable position of being able to invest aggressively in
our pipeline and at the same time deliver earnings growth to our shareholders.
Founded in 1988, Regeneron turned 25 early this year. We can now begin to see over
the horizon to Regeneron at 30 and beyond. What we see is a vibrant, diversified biopharmaceutical company, one with
multiple on-market products, a sciencedriven productive R&D engine, and products
and product candidates that address serious
medical problems of the 21st century such
as eye diseases, hypercholesterolemia, rheumatoid arthritis and other inflammatory
diseases, and cancer.
While Regeneron is changing rapidly, our
values are constant. As a company founded and run by scientist-physicians, we take
a science-based approach to everything we do; we build game-changing and foundational technology platforms as well as individual product candidates; we invest in
and reinforce our culture, one where our people are challenged to be inventive, to give
their best, and to work collaboratively. Above all, we are driven by a belief in rigorous
science, the pursuit of excellence, and, especially, the awareness that we are here to serve
patients and their unmet medical needs.
Thank you for your trust in us over the years. It is extremely satisfying to be delivering
results for our patients and shareholders.