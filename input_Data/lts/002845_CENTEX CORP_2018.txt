Dear Distinguished Shareholders,
The Board of Commissioners has reviewed the business
strategic and policies formulated and implemented by the
management of the Company under the supervision and
guidance of the Board of Directors in 2018, and received
the report on PT Centex Tbk (the �Company�)�s activities and
management reports submitted by the Board of Directors, and
we have studied the !nancial position of the Company period
1 April 2018 - 31 March 2019, and the results of its operations
and its cash "ows for period 1 April 2018 � 31 March 2019,
which have been audited by the public accounting !rm of
�Siddharta Widjaja & Rekan� No. 00470/2.1005/AU.I/04/1546-
I/I/VI/2019 dated 26 June 2019, we provide approval on the
!nancial statements.
PERFORMANCE OF THE BOARD OF DIRECTORS
The Board of Commissioners considers that amidst the
challenges of economic conditions, both nationality and
globally and also the increasing competition in the Textile and
Textile Product industry. The Board of Directors has sought
strategic measures to keep the Company growing in the
competition of the textile business.
The !nancial performance of the company during the year
re"ected the relentless, astute and calculated e#orts of the
Board of Directors. The Company successfully recorded a
gross sales increase US$.8.136 thousand or 24.55% and Net
Loss of the year decrease US$.1.428 thousand or 96.11% with
liabilities to total assets ratio are 1.01%. Those !gure was same
from previous period. Total assets of the company was decline
by 1.33 % in 2018.
The Board of Commissioners has worked in such way to
ensure supervision of the managements of the Company is
in accordance with the established plans. This is in line with
the Board of Commissioners function to ensure that the
implementation of the Company�s strategic in conducting its
business activities is always oriented towards the upgrading
of the Company�s values in accordance with standard
and procedures and taking into account compliance with
applicable regulations.
The Board of Commissioners considers that the strategy
of the Company its both formulation and implementation
that has been done in 2018 is optimal. Furthermore, the
Board of Commissioners has advised the Board of Directors
to strengthen to strategy that includes optimization of
income. The Board of Commissioners believes that with
good communication and routine the Board of Directors can
perform its functions optimally so that the Company�s growth
can be improved.


BUSINESS PROSPECT
The Board of Commissioners believes that the business and
strategy prospects developed by the Board of Directors of
Centex have been able to meet the challenges and see the
business opportunity for achieving the Company�s target,
both short and long term. The Development value added
items, develop area customers and innovation of product,
reduce of costs, realization CSR, human resource development
and increasing market price will help the Company grow
sustainably.
The Board of Commissioners believes that the strategy set by
the Board of Directors should still be periodically evaluated
while maintaining risk that are likely to arise and evaluate
nosiness processes and also pay attention on health, safety
and environment.
IMPLEMENTATION OF GOOD CORPORATE GOVERNANCE
(GCG)
In carrying out its supervisory function, the Board of
Commissioners shall not only be responsible to the results
or objectives achieve but also continuously monitor the
process to achieve the expected results. Implementation
of good governance remains the focus of the duties and
responsibilities of the Board of Commissioners together
with the Board of Directors. This is because the Board
of Commissioners together with the Board of Directors
aims to make realize corporate governance that is
expected to maintain the sustainability of the Company.
Along with the development of the Company�s business
activities, adequate risk management practices are also
required to be able to identity potential risk that my arise. On
this matter the Board of Commissioners monitor and advises
on risk managed by the Company.
Overall, the Board of Commissioners considers that the
Company has built a risk monitoring system and implemented
good corporate governance, risk management and internal
control guidelines. The Board of Commissioners together
with the Board of Directors is committed to continuously
improve the implementation of GCG which is consistently
implemented.
In regard to GCG implementation, the Board of Commissioners
and Audit Committee have supervised the Company�s
!nancial reporting process, internal control system, audit
process and compliance with the laws, regulations and code
of conduct of the Company.
METHOD AND ADVISING THE DIRECTORS
The joint meeting mechanism between the Board of
Commissioners and the Board of Directors is used by the
Board of Commissioners to provide direction or advice to the
Board of Directors in performing its duties as a manager of
the Company.

Within this meeting, we discuss together with the Board of
Directors the e#ectiveness and realization of the Company�s
strategies in accordance with our duties and responsibilities
as mandated by the Shareholders, all for the sake of
materializing the work plans and achieving the targets of the
Company.
CHANGES IN THE COMPOSITION OF THE BOARD OF
COMMISSIONERS
In the year 2018, there was no change of the Board
of Commissioners of the company. We hope that the
current composition will be able to perform the Board of
Commissioners duties more optimally of the Company.
APPRECIATION
The Board of Commissioners is optimistic that Centex will
be able to continuously grow its business amidst various
economic challenges and the impact of global and domestic
economy volatility. Centex shall continuously refresh its
commitment to implementing all GCG principles for the sake
of sustainable Company�s management.
The Board of Commissioners have advised the entire
management lines and the employees of the Company to
cooperate in a synergistic manner with all stakeholders.
Through such synergy, we believe that the Company shall be
able to provide the best services, well beyond the customers�
expectation.
Concluding this reports, The Board of Commissioners wish
to thank to the Board of Directors, All Shareholders, business
partners, Audit Committee and all employees for their trust
and support given to us for the dedication and hard work to
achieve better development of the Company.

Dewan Komisaris Perseroan | Board of Commissioners of the Company
SUHARDI BUDIMAN
Presiden Komisaris | President Commissioner
 HIDEO UMEKI KATSUTOSHI INA
 Komisaris Komisaris Independen
 Commissioner Independent Commissioner