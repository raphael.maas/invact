Dear Fellow Shareholders:
2016 was the year our new company was created as a result of Air Products and Chemicals, Inc. spin-off
of its excellent Electronic Materials Division as Versum Materials, Inc. Trading under the symbol, VSM,
Versum Materials began trading on the NYSE on October 3.
In setting up Versum, Air Products wanted to create the opportunity for it to be successful as an
independent company for years to come, taking care not to burden the Company with high debt levels. As
a result, Versum is well capitalized and able to fully pursue its goal of being the premier specialty materials
provider to the semiconductor industry. Versum Materials has a great future ahead of it.
Management
To lead Versum as President and Chief Executive Officer, we chose Guillermo Novo. Guillermo brings more
than 30 years of experience in leadership positions across the broader materials industry, and he is well
qualified to take the Company forward. To extend this leadership strength, Versum Materials has built a
strong team that includes:
 George Bitto, who is senior vice president and chief financial officer;
 Pat Loughlin, who is senior vice president, operations and supply chain; and
 Michael Valente, who is senior vice president, law and human resources, general counsel
and secretary.
I know this management team well and have observed first hand their focus on customers, employees and
shareholders, and I am confident they will work diligently to move Versum Materials forward and create value
for shareholders.
Strategy
Versum Materials has a strong business with leading market positions and a cutting edge portfolio. As
Guillermo explains in his letter, there are significant organic growth opportunities for the business. The
Company has solid growth, high margins, low capital intensity, high free cash flow and it is well capitalized,
which will also enable Versum to deliver profitable growth through strategic acquisitions and partnerships.
Governance
I am pleased to report that the Versum Materials Board is well balanced, with each independent board
member bringing distinct experiences, backgrounds and skills to the table. The common themes among
them is their industry experience, successful track record of achievement and focus on strong governance.
As Chairman of the Board and a fellow shareholder, I am honored to work with this high caliber group of
experienced individuals:
Jacques Croisetire, who is the former chief financial officer of Rohm & Haas Company and
Bacardi Limited;
 Dr. Yi Hyon Paik, who has more than 26 years of technology and business experience in
the electronic materials industry and has led the Electronic Materials business of Rohm &
Haas Company;
 Thomas J. Riordan, who has more than 30 years of experience in the specialty
chemicals industry, including 15 years at Rockwood Holdings, Inc. as its chief legal and
administrative officer;
 Susan C. Schnabel, who has extensive experience in private equity investment, including DLJ
Merchant Partners, and is co-founder of aPriori Capital Partners; and
 Ambassador Alejandro Wolff, who has more than 30 years of government service in the U.S.
Department of State, including serving as the U.S. Ambassador to Chile.
These individuals bring extensive experience in specialty materials companies, as well as technical, finance,
private equity and international government affairs expertise. Most importantly, they share another common
focus: creating shareholder value.
Acknowledgements
I want to close by thanking those who have supported us in launching Versum Materials and who are key to
the Companys continued success:
Customers  Thank you for your business. Versum Materials exists for one purpose: to collaborate with
you and enable the next generation of semiconductors for the digital world we live in. Your success is
our success. Providing excellent service to you remains the foundation of all Versum Materials is aiming
to achieve.
Employees  I want to commend each of the talented and hard-working Versum Materials employees who
are focused on helping customers innovate and grow. The collective success of Versum Materials depends
on each person coming to work, acting as if he or she is the CEO of the company, and doing his or her best
to improve performance. I know you are committed to building on a successful past and taking Versum
Materials forward to an excellent future.
Shareholders  Thank you for your confidence and investment in the Company. We are focused on creating
shareholder value; that is our top priority.
Seifi Ghasemi
Chairman of the Board, Versum Materials