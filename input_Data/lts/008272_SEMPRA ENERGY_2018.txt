Fellow Shareholders:
At Sempra Energy, we are always looking for new and better ways to serve our
customers. It drives our employees� sense of purpose every day. We know that
improving the lives of those we serve also helps make our company even more
valuable for all our stakeholders. Sempra�s vision reflects this commitment:
delivering energy with purpose.
NORTH AMERICA�S PREMIER ENERGY INFRASTRUCTURE COMPANY
I am very proud of the progress we made in 2018 to advance our strategic mission
to become North America�s premier energy infrastructure company. Through our
focus on People | Priorities | Culture, we charted an exciting new path to grow
stronger, more profitable and more purpose driven.
To support our mission, we made a number of strategic decisions:
? We honed our geographic focus to the most attractive markets in North America,
strengthening our leadership positions in three of the top 15 economies in the
world: California, Texas and Mexico.
? We narrowed our focus within the energy value chain to electric and natural
gas infrastructure where we believe there is an optimal risk/reward profile for
our owners.
? Finally, we positioned our business to better serve important markets. By
investing in North America�s liquefied natural gas (LNG) infrastructure, we
are poised to unlock cleaner and more reliable energy for tens of millions of
consumers in Europe, Asia and the Middle East.
This is just the beginning. Today, we are more focused, more disciplined and wellpositioned for growth and improved profitability.
PATH TO PREMIER
California
Our California utilities are performing well. They continue to be recognized for
excellence in customer service, safety, reliability and clean energy. Last year, San
Diego Gas & Electric (SDG&E) won an award for best electric reliability in America,
and received the coveted Edison Award for its investments in grid resiliency and
the reduction of climate-related vulnerabilities, like wildfires. Southern California
Gas Company (SoCalGas) also continues to be a national leader in customer service
and is delivering on its promise of operational excellence � modernizing more than
400 miles of the largest gas distribution and transmission network in the U.S. in 2018.
Texas
Last year, we completed the most ambitious transaction in our history, acquiring
a majority stake in the largest investor-owned utility in Texas, Oncor Electric
Delivery Company LLC (Oncor). This transaction significantly expanded our
electric infrastructure business in a growing market � Texas is the second-largest
economy in the U.S. We also believe there is a remarkable opportunity for Oncor
to continue to invest in critical infrastructure to meet the growing electricity needs
of its customers.

Mexico
Our subsidiary, IEnova � one of the largest private energy companies in Mexico �
has a strong set of investment opportunities. IEnova continues to make significant
progress in the midstream and refined fuels markets to help Mexico diversify its
energy supply.
LNG
We also have made significant progress in becoming a leader in LNG export
infrastructure in North America. We began the commissioning process on our
Cameron LNG joint-venture project in Louisiana and expect to ship the first LNG
cargos in mid-2019. In addition to Cameron LNG, we are executing on four other
development projects with a goal of building up to 45 million tonnes per annum of
LNG export capacity to serve global markets � this is more than the average daily
consumption of California, the fifth-largest economy in the world. These projects
aim to improve the energy security and carbon footprint of our allies, while
supporting U.S. producers of natural gas.
Portfolio Optimization
We also executed several strategic asset sales to focus our investment strategy on
the most attractive markets in North America and strengthen our balance sheet.
The sales of our U.S. non-utility solar, wind and natural gas storage assets are
expected to generate approximately $2.5 billion in proceeds. In early 2019, we also
announced our decision to sell our South American businesses, including Luz del
Sur S.A.A. in Peru and Chilquinta Energ�a S.A. in Chile.
PEOPLE | PRIORITIES | CULTURE
Building a high-performing culture begins and ends with unlocking the full potential
of our more than 20,000 diverse and talented employees. That is why on my first
day as CEO last spring, I announced my focus on People | Priorities | Culture. The
goal of this initiative is to reinvest in our employees, lay out a clear vision for the
future and align our companies to be even stronger and more focused. To support
this initiative, our senior leadership team embarked on a Listening Tour, visiting
all four corners of Sempra�s family of companies to engage our employees in a
conversation about how we can make our company better and more purpose driven.
Our employees� commitment to our shared values is critical. Doing the right thing,
championing people and shaping the future represent our core values.
The growth opportunities ahead of us are exciting. So is our ability to make a
positive impact. We are charting our own path as we aspire to become North
America�s premier energy infrastructure company � delivering energy with purpose.
 Ever forward,
Jeffrey W. Martin
Chairman and Chief Executive Officer