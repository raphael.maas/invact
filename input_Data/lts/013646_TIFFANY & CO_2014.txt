Dear Shareholder:
You are cordially invited to attend the Annual Meeting of Shareholders of Tiffany & Co. on
Thursday, May 28, 2015 at 9:30 a.m. in the Great Ballroom of the W New York  Union Square
hotel, 201 Park Avenue South (at 17th Street), New York, New York.
To attend the meeting, you will need to register online. To do so, please follow the instructions in the
Proxy Statement on page PS-9. When you arrive at the meeting, you will be asked to provide your
registration confirmation and photo identification. We appreciate your cooperation.
Your participation in the affairs of Tiffany & Co. is important. Therefore, please vote your shares whether
or not you plan to attend the meeting. You can vote by accessing the Internet site to vote electronically,
by completing and returning the proxy card by mail or by calling the number listed on the card and
following the prompts.
_______________________
I am pleased to report to you that fiscal 2014 was another year of solid performance for Tiffany & Co.
While strong growth in worldwide sales and net earnings during most of the year was partly offset by
softer results in the fourth quarter, Tiffanys net sales and earnings for the full year reached record levels.
Tiffanys broad-based success reflected our consistent focus on long-term strategies: adding exciting
new designs to complement our core product offerings, expanding the reach of our marketing
communications, opening stores in key markets, and further enhancing the in-store experience for
our customers. We remain committed to that long-term approach.
In the year ended January 31, 2015, worldwide net sales increased 5% to $4.25 billion. On a constantexchange-
rate basis that excludes the effect of translating foreign-currency-denominated sales into
U.S. dollars, worldwide net sales rose 7% and comparable store sales rose 4%. Performance across our
regions was healthy for the full year, ranging from total sales growth in local currencies of 10% in Asia-
Pacific, to 6% in both the Americas and Europe, to 4% in Japan.
Complementing the sales growth, our operating margin, excluding certain charges recorded in 2013,
rose 1.3 points to 21.0% as gross margin benefited from favorable product input costs, selective price
increases and a favorable shift in sales mix.
During the year, we took advantage of market conditions and redeemed $400 million of long-term debt
using proceeds from the issuance of $550 million of debt at considerably lower interest rates, which
also extended maturities. This resulted in our recording a debt-extinguishment charge, after tax, of $61
million, or $0.47 per diluted share.
Net earnings were $484 million, or $3.73 per diluted share. Excluding the charge in 2014 and charges
recorded in 2013, net earnings rose 13%, which exceeded our original expectation for the year.
Tiffany has a solid balance sheet to support our current business initiatives and anticipated expansion.
At year-end, total long-term and short-term debt represented 39% of stockholders equity. We also
continued to return capital to stockholders by increasing the quarterly dividend 12% in 2014,
representing the 13th increase in the past 12 years, and by spending $27 million to repurchase 301,000
shares of our Common Stock.
From a product perspective, we saw growth in all jewelry categories in 2014. We were especially
pleased with customer reaction to the launch of the TIFFANY T jewelry collection in the third quarter,
representing a modern design targeted to the self-purchaser. And our new interpretation of the ATLAS
collection, introduced in late 2013, continued to broaden its appeal among our global customer base.
As you know, effective April 1, I have retired from my position as Chief Executive Officer. In my more
than 30 years at Tiffany, and as CEO for the past 16 years, I have seen our Company make extraordinary
strides to become a truly global luxury brand. I take great pride in that, as we successfully grew our
sales, store base and earnings, we also maintained an uncompromising focus on the integrity of the
TIFFANY & CO. brand and, most importantly, demonstrated through our actions that a corporation
must earn its social license to operate by acting responsibly toward the communities it serves and the
global environment.
I have great confidence in the leadership of our Company, led by Frederic Cumenal, and look forward to
continuing to serve Tiffany in my new role as non-executive Chairman of the Board. We appreciate your
interest and support.
Sincerely,