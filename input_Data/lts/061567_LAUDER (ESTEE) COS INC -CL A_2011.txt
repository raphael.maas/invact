Dear Fellow Stockholders,
I am pleased to report that fiscal 2011 was another outstanding year for The Este Lauder Companies.
Driven by fantastic momentum across our brands, regions and product categories, we made significant
progress against our strategic goals, outperformed the global prestige beauty industry and set many
financial
records for our Company. Today, we are more than two years into our strategy and by virtually all
measures have made tremendous strides. As a result of our success, we are extending our strategy for
another year, through fiscal 2014, with even more ambitious goals.
We experienced many outstanding achievements during the past year. First, we grew sales by 13 percent
three times the growth of global prestige beauty. Additionally, we saw double-digit sales increases
across our largest product categories of skin care and makeup, as well as in our geographic regions. We
had our best year in North America in a decade, achieved prestige beauty leadership in China and became
the leader in skin care in travel retail, the fastest-growing category in one of the fastest-growing prestige
beauty channels.
It is clear our strategy is working, we are executing with excellence and we remain focused
on our visionto be the global leader in prestige beauty: a brand-building powerhouse of unrivaled creativity
and innovation.
Fiscal 2011 milestones* include:
 Record net sales of $8.8 billion
 Record gross margin of 78.1%
 Operating margin of 13.0%
 Record net earnings of $743 million
 Record earnings per share of $3.69
 Record cash flow from operations of more than $1 billion

Our strategic journey began in 2009. Since that time, we have stepped forward with confidence and
better aligned our organization and infrastructure to support our new corporate strategy. In the process,
we reduced non-value-added costs and imposed new financial discipline across the Company. At the
same time, we developed the necessary capabilities to improve our competiveness and, today, we are
better positioned
to manage our business more effectively and allocate our resources to the most promising
opportunities.

In fiscal 2011, we updated our strategy to prioritize those opportunities with the biggest potential and, as
a result, increased our share in several key product categories and geographic regions. For example, we
saw the luxury sector recover significantly and prestige beauty gain against mass in many important
countries
and categories. Our continued commitment to providing prestige product and service innovations
has been paying off.
We recognize that in order to be a truly global company we need to be even more locally relevant. This
year, we grew our capabilities to innovate closer to where our consumers
live and shop, especially in emerging
markets. We opened a larger research and development facility in Shanghai to help us develop breakthrough
technologies and products based on the preferences of Chinese consumers. We also expanded

our distribution in the most locally relevant channels within each of our geographies. In Brazil, for instance,
M.A.C expanded its distribution by increasing the number of its freestanding stores.
We enhanced our capabilities within the digital space to take advantage of this increasingly important
communications area. One of the ways we did this was by leveraging game-changing technologies, such as
mobile applications and social media platforms, to allow our brands to connect with consumers in new and
interactive ways. Since the digital universe is fundamentally changing how consumers research and buy
beauty products, we will continue investing in ways to evolve our digital capabilities.
Another exciting opportunity is the travel retail channel, where in fiscal 2011 we were one of the
fastest-growing beauty companies. As airports continue to become more vibrant shopping destinations,
we are creating innovative new products and services that will help us fully leverage this important
channel.
However,
we recognize the potential volatility and risks associated with the travel retail
business,
which can change in an instant due to events beyond our control, so we will manage our
expectations accordingly.

The qualities that have been central to The Este Lauder Companies since our beginning shape who we
are and inspire us to deliver uncompromising quality and unparalleled service to our consumers.
One such strength is our innate creativity. Today, we are focusing on fewer, but more impactful product

launches. We are supporting these launches with an effective mix of pull-push marketing efforts, combining
television,
print and digital activities to attract consumers to our counters, stores and websites and then
treating them with our High-Touch services. This year, we utilized our improved marketing techniques
to support several of our breakthrough product launches Este Lauders Advanced Night Repair
Synchronized Recovery Eye Complex, Cliniques Repairwear Laser Focus, Origins Plantscription and
La Mers The Radiant Serumall of which benefited and contributed significantly to our growth.
Our outstanding results this year reinforce that our strategy is working. I want to express my sincerest
thanks to our Board of Directors for its sage guidance and ongoing support. I also extend my appreciation to
our retailers, consumers and you, our stockholders, for your partnership and passion for our products and
unique High-Touch service approach.
Naturally, strong results are only possible when outstanding people unite and deliver their besttogether.
I want to express my sincerest thanks to William Lauder and Leonard Lauder for their partnership and
guidance,
and for building a Company of such great people. I am also honored to have at the helm of our
Company, alongside the Lauder family, a talented management team whose strategic focus and discipline
has been crucial to our success. Their efforts on a day-to-day basis have kept our global workforce
motivated
and on course. I extend my deep appreciation to our employees around the globe, as they are
truly what make our Company extraordinary.
As we start fiscal 2012, there is a significant amount of economic uncertainty in many parts of the world.
Recent volatility may make it harder to predict both the full recovery of prestige beauty growth, as well as
how global consumer confidence, demand and spending will be impacted. However, our fundamentals
are
solid, our business strategies are designed to strengthen the Company over the long-
term and we believe

we are well positioned to manage our business more effectively and efficiently. We are energized by our
momentum and are committed to refining our strategy as needed to continue to capture the biggest
opportunities that will sustain us for the years to come. I am confident that by staying focused and
disciplined,
we will continue moving forward on the path toward a very promising future of sustained
profitable growth.
Sincerely,
Fabrizio Freda
President and Chief Executive Officer