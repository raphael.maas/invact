Letter to shareholders
When we innovate, we win. This simple
statement epitomized our approach in 2012
and will only be amplified as we set the bar
higher in 2013 and beyond. It is the philosophy
and attitude that put points on the scoreboard
for 2012, with 25% net revenues growth, 31%
earnings per share growth, and significant improvement to our inventory positioning, which
drove a nearly doubling of our cash position to
$342 million. It has helped deliver eleven consecutive quarters of 20%+ net revenue growth
for the Under Armour Brand. It has enabled us
to add nearly $1
billion in net revenues over the
past three years,
putting the Company in position to reach our goal established at
our June 2011 Investor Day: 2X our net revenues
from 2010 to 2013.
Innovation has helped successfully transition our Brand from a tight t-shirt company
to a fully integrated athletic brand capable of
servicing the full needs of athletes. As a statement to this progression, compression apparel
represented 63% of our apparel mix during
our IPO year of 2005. Today, it is down to just
14%.
Innovation can drive platforms for the Brand,
including Charged Cotton and UA Storm, which
both posted substantial growth in 2012 following their 2011 introductions. It can also produce
technologies like coldblack, a fabrication that reflects the suns heat and light, keeping you cooler
and more comfortable on a hot summer day.
Innovation for us also means making our
core UA Tech t-shirts better than ever before
in 2012, adding a softer touch and anti-odor
attributes. Its the notion of newness that
our customer demands and will be a heightened focus as we flow more product to the
market while reducing our reliance on legacy
programs. Its broadening our appeal to more
consumers while also reducing our dependency on weather extremes by focusing on areas
like Fleece, which grew nearly 50% in 2012.
And its understanding that the needs of our
athletes are changing, and that versatility is a
winning proposition.
Another specific area where innovation
is driving results is in Womens, where key
investments made over the past few years in
areas such as fit and design are helping introduce Under Armour to a whole new consumer.
We debuted platforms like Armour Bra and
UA Studio, which are redefining her expectations for the Brand. These product messages
were amplified by our first targeted Womens
campaign, Whats Beautiful, which provided
a unique community to challenge and shatter expectations on what she can accomplish.
While we are pleased with the nearly $400
million Womens business we have built to
date, we are positioned to amplify her voice
in 2013. We recently added our new Executive
Creative Director for Womens, and we are focused on assortment, fit, color, and the right
distribution.
Much like Womens, the potential of our
Brand is seemingly limitless with kids. Youth
product grew at a faster rate than both Mens
and Womens in 2012 and should again lead
the way in 2013 as we focus on product expansions in areas like graphic t-shirts and fill distribution gaps in locations like department stores.
The notion of NEXT is a guiding principle
in our Youth business
the UA consumers of
tomorrowas well as
how we are communicating with these future
athletes. We remain focused on capturing the
NEXT generation of
professional athletes,
including 2012 National League MVP and
World Series Champion
Buster Posey, 2012 National League Rookie of
the Year Bryce Harper,
up-and-coming tennis phenom Sloane
Stephens, WBC superwelterweight champion Canelo Alvarez and
former world #1 amateur golfer, 19-year-old
Jordan Spieth.
Part of the promise of the Brand and our
ability to be next with the upcoming generation of athletes will be driven by our efforts in
Footwear. Still just in our seventh year, the category grew 32 percent to reach nearly a quarter of a billion dollars. However, to date, this
success has really only manifested into meaningful market share in one category: cleats. We
are building on-field credibility with our athletes through truly game-changing products
such as our $130 UA Highlight football cleat.
This type of pinnacle product helped UA approach nearly 30 percent market share in both
baseball and football in 2012, and we will continue to build upon that momentum in 2013.
Some of that momentum will be evident in
Running footwear, where we built upon some
of the early success of our $120 UA Charge RC
product and Micro G cushioning technology
to unveil UA Spine last July. UA Spine is our
unique stance on melding lightweight and stability, and the platform will move well beyond
running in 2013.
Great product requires great distribution,
and we continue to work with our partners like
Dicks Sporting Goods, The Sports Authority,
and Academy to elevate our Brand. Ongoing
investments such as our All-American and
Blue Chip shop-in-shops with Dicks Sporting
Goods are providing a more comprehensive assortment to our consumers. At the same time,
we continue to look for new ways to reach consumers, especially in channels that are more
relevant to certain product categories like
Womens and Youth. To that end, we entered
more than 500 new department store doors
in 2012, including Macys, Dillards, Belk and
Lord & Taylor, and are positioned to broaden
this reach going forward.
Our conversation around distribution
would not be complete without our Direct-toConsumer channel. Direct-to-Consumer net
revenues grew 34 percent in 2012 to over a half
of a billion dollars, or nearly the size of our entire business in 2007. The channel represented
29 percent of total net revenues for the year, up
from 27 percent in 2011 and 23 percent in 2010.
The bulk of this business is driven by our Factory House outlet stores, which continue to help
us better manage our excess inventory while
reaching out and providing value to our consumers. We expanded this store base in the U.S.
from 80 stores in 2011 to 101 in 2012.
Beyond outlets, we opened the next generation of UA Specialty with our Harbor East
store in downtown Baltimore in February
2013. Delivering an unrivaled store experience through specialization, localization, and
innovation, the store provides an important
learning lab that we will continue to refine
and test with additional stores in the future in
strategic locations.
Rounding out Direct-to-Consumer, Ecommerce will continue to be a growth vehicle for
the company as we drive enhanced merchandising and storytelling, including a clearer
connection with some of our larger branding
initiatives planned in 2013.
While all these initiatives continue to drive
our business at home, we are mindful that the
opportunities abroad are bigger. Our international business represented only 6% of net revenues in 2012, and we continue to believe that
the strength in our core U.S. business affords
us the opportunity and patience to make the
right decisions in Europe, Latin America, and
Asia. We are able to take a different, broader
approach to how we enter these markets. Our
grassroots efforts help us build the Brand by
being intensely focused on sport authenticity
in local markets. We are able to balance this
approach with larger brand-building initiatives such as the July 2012 introduction of
our kit for Tottenham Hotspur of the English
Premier League, reaching over 20 million fans
globally. Finally, in the age where we are all
connected like never before through technology, we have the ability to change the traditional approach to reaching consumers in new
markets through digital means like Ecommerce
and social media.
With all of our focus on building great product for athletes, no
element of our business is more critical to our success
than the continued
development of our
team. As we build
the organizational
structure in Baltimore and around
the world, we will
consistently supplement this great
leadership to help ensure our progress as a leading global athletic brand. In 2012, we added
new leadership within International, Supply
Chain, Womens, and Human Resources. Our
success in 2012 is a direct result of the team
we have and continue to build in Baltimore and
around the world.
Expect to see more of us in 2013 and beyond. Expect a louder, more focused, more
disruptive voice. Expect more innovation and
product that redefines how an athlete performs.
As we prepare for 2013 and beyond, we will remain humble in the success we have achieved
and hungry to press on to new opportunities.
Kevin A. Plank
Chairman of the Board of Directors &
Chief Executive Officer