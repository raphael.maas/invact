DEAR STOCKHOLDERS,
2016 was a solid year for Alliance Data. Once again, we grew
across all three key metrics: revenue by 11 percent, adjusted
EBITDA, net,1
 by 9 percent, and core EPS by 12 percent. In fact, our
performance outpaced not only our peers, but also the S&P 500
and U.S. GDP, proving again that our model is viable and strong.
As a growth company, each of our businesses is expected to
contribute to our goal of balanced growth every year. We hit
some choppy waters in 2016, with each business facing a
distinct challenge. The good news is we have a solid plan in
place to return to the consistent growth that characterizes our
performance history, and we expect solid growth again in 2017.
Periodically I am asked how our three businesses fit together.
Our businesses are more similar than they appear on the
surface: Each has a platform that reliably collects SKU-level
data linked to customer purchases, which is the gold standard
for understanding consumer behavior. Our businesses make
responsible use of data to derive insights about the consumer to
drive additional sales for our clients, such as at a grocer (through
BrandLoyalty), a specialty retailer (through Card Services), or
an automotive manufacturer (through Epsilon).
2016 IN REVIEW
Card Services had another stellar year in 2016, with 24%
growth in revenue and 14% growth in adjusted EBITDA,
net, despite an increase in our allowance for loan losses of
$200 million. Credit sales increased 18% for the year, and
average receivables grew a robust 24%. It was a huge year in
terms of high-quality new signings whose portfolios should spool
up to another $2 billion vintage over the next several years.
Clearly the headwinds were credit losses.
Epsilons performance in 2016 disappointed financially, but there
were some bright spots. While adjusted EBITDA decreased 6%
on a 1% revenue increase, the core offering grew 4%, which
included a 4% drag from our Technology platform business. In
addition, Agencys growth rate turned positive in the second half
of the year.
In 2016, we intensified our focus on cost improvements. We
ended the year with our Bengaluru, India, office at full scale.
While the opening and ramp-up entailed costs, we are already
leveraging the financial benefits of the move, particularly on the
labor side. Our LEED-certified facility speaks to our companywide commitment to associate well-being and resource efficiency,
as well as responsibly investing in local communities.
Overall, we move into 2017 with plenty of optimism along with a
laser-like focus on areas of further improvement.
LoyaltyOne, which includes our Canadian coalition loyalty
business and Netherlands-based BrandLoyalty, ended the year
with a decrease in revenue of 1% and adjusted EBITDA, net,
increasing 4%. Excluding the expiry reset, revenue increased
17%. BrandLoyalty had a terrific year with high single-digit
growth in revenue and adjusted EBITDA. Expansion efforts
in Canada continued to exceed expectations, while the pilot
program success in the U.S. indicates an emerging growth driver
in 2017.
In Canada, the AIR MILES Reward Program experienced a
challenging year related to a planned year-end expiration of
AIR MILES reward miles, originally announced in 2011. Before
LoyaltyOne cancelled the expiration policy in the face of a
then-pending change in Ontario law banning time-based expiry,
collectors rushed to redeem their AIR MILES reward miles,
leading to revenue pulled forward into 2016. AIR MILES issuance
increased 1% for the year, as sponsors scaled back fourth-quarter
promotions during the expiration issue.
At the corporate level, liquidity remained a comfortable
$1.1 billion at year-end. We took advantage of a negative
market reaction to the headwinds we faced, repurchasing
approximately $800 million of our stock over the course of
2016. In total, we have repurchased more than $4.3 billion of
our stock since the beginning of 2008. Further, we introduced a
1% quarterly dividend, funded the capital required for growth in
our Card Services business and purchased the final 30% of our
BrandLoyalty business, 20% on an accelerated basis.
LOOKING AHEAD
As we progress through 2017, we anticipate putting 2016s
challenges largely behind us, and we expect all segments to
contribute solid growth. For LoyaltyOne, we anticipate another year of growth for the
BrandLoyalty business. The runway in this business is long, and
we expect to achieve this growth by increasing our geographic
footprint as well as by growing relationships within our existing
footprint. With the AIR MILES Reward Program, we have already
seen issuance and redemptions return to normalized levels, and
we expect issuance growth in the 3% range in 2017. Committed as
always to client satisfaction and delivering consumer value  and
working within the new provincial law that has nullified loyalty
program expiry policies  we are retooling the program to offset
the lost economics while ensuring we maintain solid appeal for
program collectors and sponsors. We anticipate momentum for
the program overall to accelerate as we enter the back half of 2017.
At Epsilon, the bulk of the core offering is expected to deliver
solid growth in revenue and earnings, and the other 25% of
our offerings  primarily the Platform technology group  has a
clear game plan to stabilize and return to growth in 2018. Weve
lowered the expense base through our Bengaluru office, and our
next step is to drive revenue growth by standardizing the product
and speeding up our time-to-market. Our strategy is multifaceted.
First, we intend to offer secure cloud-based database platforms
for mid-market companies that typically cant afford our
custom-built, hardware-based databases. Second, we will link
the ad-tech platforms to CRMs digital data and unique reach.
Third, we will revitalize sales and client services to aggressively
sell technology platforms. And finally, we plan to leverage the
advantage of our service-rich offering. Add this all up, and we
should return to mid-single-digit revenue and adjusted EBITDA
growth in 2017.
At Card Services, expect nice growth again in 2017. The
pipeline is robust, and we anticipate another $2 billion vintage
year. Card receivables growth of about $2.4 billion  or 15% 
represents a slight deceleration in growth rate due to the start-up
nature of new signings, as those new clients start with zero
tender share and then ramp up to about 30% tender share over
approximately three years. The net loss rate continues to track in
line with our delinquency expectations, and we expect the credit
normalization process will be completed by year end. After the
challenging 2016 year-end holidays, were seeing strong interest
and investment by retailers to enhance programs to get at the
data, which suggests that the tender share growth rate will
continue. We see much of our credit sales now coming through
online channels compared to in-store, and we stand ready to be
the one-stop solution with the best suite of services and tools
for our retailers digital channels.
At Corporate, we will focus on maintaining an engaged and
diverse workforce, robust liquidity and modest leverage, which,
along with expected healthy cash flow, will provide us with the
flexibility and resources to grow the business. Additionally, we
expect to continue with shareholder-friendly programs, including
our $500 million authorized share repurchase plan and 1%
quarterly dividend.
THOUGHTS IN CLOSING
As we look at 2017, I firmly believe in the flexibility of our
business model, which has enabled Alliance Data to grow in
just two decades from a start-up to a Fortune 500 and S&P 500
company. We will put the challenges of 2016 behind us by midyear, and I look forward to the re-acceleration in our businesses
in the back half of 2017. This will set us up for what I call the
slingshot effect  propelling the company into 2018.
As a company, we are committed to our 17,000+ enormously
talented, engaged and diverse associates worldwide, and I am
confident that they remain fully aligned and on board for the ride
ahead. Their dedication to delivering a premium level of service
to our clients  among the worlds most reputable and successful
brands  as well as giving generously of their time, talents and
resources within our local communities gives me great pride.
And I want to reiterate my commitment to our stockholders that
we remain first and foremost a growth company. Our long-term
goals are to deliver double-digit growth in revenue and earnings,
along with robust free cash flow generation, modest leverage
and strong visibility. We have vastly outperformed the market
over the last 15 years since our $12 per share IPO. While we
faced choppy waters in 2016, we believe were headed for
calmer sailing by the back half of 2017, with strong acceleration
into 2018 and beyond. We appreciate your support and patience
as we maintain a steady and purposeful course  one that
ensures we remain an attractive, sustainable growth company.
Edward J. Heffernan
President and Chief Executive Officer