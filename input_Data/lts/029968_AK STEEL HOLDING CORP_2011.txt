Thanks to a great team effort,
AK Steel finished the year 2011
well-positioned for the future. Despite
extraordinarily high raw material costs
and weaker-than-expected economic
conditions, our full-year 2011
financial performance represented
meaningful progress.
By remaining focused on our Three
Cs approach to serving Customers
better than any other steelmaker,
reducing Costs and conserving Cash,
AK Steel, once again, was able to
rise to the challenge and emerge as
a stronger company.
For the full-year 2011, AK Steels
revenues reached approximately
$6.5 billion, an increase of about
8 percent over 2010. In addition,
shipments for the year 2011 totaled
5.7 million tons, a slight increase
compared to 2010. And, our
average selling price for the year
2011 was $1,131 per ton, a 7
percent improvement over 2010.
Indeed, AK Steel continued to
move in the right direction in 2011,
starting with an unwavering focus on
our key values of safety, quality and
productivity.
Safety First, First In Safety
Workplace safety is our top
priority at AK Steel, and our 2011
safety performance continued to
lead the steel industry average. As
a result of our commitment to safety,
the company earned significant
recognition in 2011.
Our Coshocton Works and
Zanesville Works each received
several awards for safety from
the Ohio Bureau of Workers
Compensation Division of Safety
and Hygiene in 2011, and both
of our AK Tube plants were
recognized by the Fabricators
& Manufacturers Association,
International, for their exceptional
safety performance. Of special
note, the employees at our
Zanesville plant worked the entire
year of 2011 without any OSHA
recordable injuries
a fantastic job
by everyone at that operation.
Quality Performance
As a testament to the hard
work and dedication of the men
and women of AK Steel, the
company recorded its best-ever
results for quality across several
key performance indicators during
2011. In fact, according to
independent surveys conducted
by Jacobson and Associates,
our carbon and specialty steel
customers rated us number one
in product quality, service and
overall satisfaction for the full-year
2011. We were also delighted
to receive Supplier of the Year
awards from three of our valued
customers: Kenwal, Flack and
Mapes & Sprowl. And, we were
recognized by Toyota for Quality
Performance and Supplier Diversity
Achievement. These are marvelous
accomplishments, and they are the
result of tremendous work by our
entire organization.
Productivity Achievements
In terms of productivity, our units
ran at slightly more than 80 percent
of capacity for the full-year 2011.
During that time, we not only
improved our yield performance,
but also set new yield records at
several operating units and plant
locations throughout the company.
A real standout performer was our
Coshocton Works, where new
productivity marks were set at the
Z-mills, temper mills, slitters and
other units.
Operational Improvements
At Butler Works, AK Steels new
Number 5 electric arc furnace (EAF)
became operational in 2011. The
new furnace will help us lower
production costs, enhance product
quality and create greater flexibility
to produce carbon, stainless and
electrical steels for our customers.
In recognition of this investment,
AK Steel received the 2011 award
for Best Operational Improvements
from American Metal Market, a
leading metal industry publication.
In addition, SunCoke Energys
Middletown coke plant became
operational in 2011. The new
SunCoke plant will provide our
Middletown blast furnace with a
long-term supply of coke, as well as
produce electricity from heat-recovery
generators. This initiative, along
with our Number 5 EAF, will help
enhance the long-term profitability of
the company in a meaningful way.
Vertical Integration
In October of 2011, AK Steel
announced that it had acquired
interests in iron ore and metallurgical
coal, two essential steelmaking raw
materials. These investments represent
a major step forward in becoming
more vertically integrated, one of the
companys top strategic initiatives.
The acquisitions will gradually
provide AK Steel with a low-cost
supply of high-quality iron ore and
metallurgical coal for our own
steelmaking operations, as well as
a financial hedge against global
market price increases for those
raw materials.
Magnetation LLC
On the iron ore front, AK Steel
formed Magnetation LLC, a joint
venture with a private company,
to produce iron ore feedstock and
pellets from legacy reserves in
Minnesota. The joint venture utilizes
magnetic separation technology
to recover iron ore from existing
stockpiles of previously mined
material. Utilizing these iron ore
tailings eliminates the need for
traditional drilling, blasting and
excavating, and can result in the
creation of new wetlands in an
environmentally responsible manner.
By the year 2016, AK Steel expects
to procure approximately 50 percent
of its annual iron ore requirements
directly from Magnetation LLC.
AK Coal Resources, Inc.
AK Steel also acquired metallurgical
coal interests in Pennsylvania
and formed AK Coal Resources, Inc.,
a wholly-owned subsidiary. As a
result, AK Steel now controls, through
ownership and lease, more than 20
million tons of low volatile met coal
reserves in Somerset County, PA.
The company expects to be mining
significant amounts of coal beginning
in 2014 and anticipates reaching
its goal of about 50 percent selfsufficiency
in met coal by the year
2016.
Sound Financial Management
To enhance liquidity and produce
greater financial and strategic
flexibility, AK Steel increased its
revolving credit facility from $850
million to $1.1 billion in 2011. In
addition, the company contributed
$170 million to its pension trust fund
in 2011. From 2005 through the
end of 2011, we contributed more
than $1.3 billion to the pension fund.
We also made our final $65 million
contribution to the Middletown Works
Retiree VEBA in 2011, and made an
initial payment of $32 million related
to the Butler Works retiree settlement
as well. The company ended the year
2011 in solid financial condition with
liquidity of $559 million.
TEAMwork in 2012
Great teams find a way to win,
and our team remains focused on
creating and delivering value for our
shareholders. Looking to the future,
AK Steel finds itself on the front-end of
an economic recoverywith vertical
integration initiatives set to unlock
significant value. I am fortunate
to lead an excellent management
team with a track record of success,
and that experience will continue to
serve the company well as we take
advantage of the opportunities in
2012 and beyond.
In addition, AK Steel is well-served
by its Board of Directors, including
Robert H. Jenkins (Lead Director),
Richard A. Abdoo, John S. Brinzo,
Dennis C. Cuneo, William K.
Gerber, Dr. Bonnie G. Hill, Ralph
S. (Mike) Michael, III, Shirley D.
Peterson and Dr. James A. Thomson.
I would like to take this opportunity
to thank each of them for their strong
support and leadership in 2011.
AK Steel was on the ROAD to
Recovery in 2011, and we will
continue to work as a TEAM to
achieve peak performance in 2012
by striving to:
Tap our potential;
Enhance our product margins;
Accelerate vertical integration, and
Manage working capital while
serving customers better than any
other steelmaker.