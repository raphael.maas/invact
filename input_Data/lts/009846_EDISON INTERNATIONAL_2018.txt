In the 12 months since my last letter, our company has been operating
in a challenging environment. In 2018, we again saw the devastation of
wildfires in California. Almost 2 million acres burned last year, and close to
100 lives were tragically lost due to these catastrophic events catalyzed by
climate change. At the start of 2018, historic winter rains brought damaging
mudslides. Our thoughts are with all those across the state who were
affected by these events. Our company’s number one priority continues to
be the safety of customers, first responders, workers and communities

At Edison, our 12,500 employees are meeting the
challenge daily of protecting our communities. They
make us proud to be part of this 133-year-old company
that is such a vital part of our region. Our crews,
working alongside first responders, restored power in
fire and mudslide-impacted areas as quickly as possible
and without serious injury to any of our employees or
contractors. Our external outreach teams worked with
affected communities on recovery, and together, our
company and employees raised more than $230,000
in charitable contributions for recovery efforts.
In February of this year, our board approved an
additional contribution of $3 million to the Edison
International Wildfire Assistance Fund to enhance
community resiliency and wildfire prevention
and mitigation.
Despite the adverse conditions, Edison International
reported core earnings for 2018 of $4.15 per share
compared to $4.50 a year ago. Comparison of yearover-year results is not particularly meaningful
because Southern California Edison (SCE) has not
yet received a decision in its 2018 General Rate Case.

Our GAAP earnings, however, were impacted by a
$1.8 billion after-tax charge related to existing and
expected claims arising from the wildfire and mudslide
events of 2017 and 2018, resulting in a net loss of
$1.30 per share (see opposite page for a reconciliation
of core and GAAP earnings). These results compared
to 2017 GAAP earnings of $1.73 per share.
In December, we increased our dividend for the 15th
consecutive year to an annual rate of $2.45 per share.
This reflected our continuing commitment, even in a
challenging operating environment, to target a dividend
payout ratio of 45 to 55 percent of SCE’s core earnings. 

Our stock price, which reached an all-time high in
2017 before the recent fires and mudslides, continues
to trade at a deep discount, reflecting investor
uncertainty over the state’s legislative and regulatory
responses to these unprecedented conditions. Our
shares likely will not recover their full value until these
issues are resolved. I will devote much of this letter
to discussing our strategy for addressing this.
Long term, we remain firmly committed to our
corporate strategy of leading the transformative
change underway in the electric power industry by
pursuing opportunities in clean energy, efficient
electrification, grid modernization and customer
choice, thereby delivering value to shareholders.
We will execute this vision with a relentless focus
on operational and service excellence, starting
always with safety as our first priority.
Our primary focus for 2019 is to put the full weight
of our company’s talent and resources into helping
the state prevent wildfires and limit their impact,
while protecting the financial health of our utility to
continue to safely deliver reliable, affordable and clean
electric power. The widespread destruction of wildfires
is a major threat to the safety of our communities, the
health of our economy and California’s ambitious goals
for reducing greenhouse gas emissions. Achieving our
company’s and the state’s long-term goals requires
addressing this immediate crisis first.
Multiple factors contribute to wildfires across SCE’s
service area and throughout California. These include
the buildup of dry vegetation in areas severely
impacted by years of historic drought, the failure of
multiple responsible parties to clear this buildup of
hazardous wildfire fuel, increasing temperatures, lower
humidity and strong Santa Ana winds. Such factors can
trigger wildfires and strain or damage utility facilities, no
matter how well designed, constructed and maintained.
Wildfire risk is increasing at the same time that more
and more residential and commercial development
is occurring in some of the highest-risk areas — with
about 35 percent of SCE’s service area in high fire
risk areas.
From a business perspective, we recognize that this all
has implications for the financial health of our industry.
In January, Pacific Gas & Electric (PG&E) and its parent
company filed for bankruptcy protection due to billions
of dollars in potential liabilities associated with wildfires.
We are in a far different place than PG&E today. The
potential liabilities they face are an order of magnitude
larger than those SCE may face. We believe that we
have the financial capacity to manage our current
potential wildfire liabilities. However, going forward
we must focus on both operations and policymaking
activity that will protect against and mitigate wildfire risk
and bring more certainty to the questions of liability
and cost recovery. We will continuously evolve and
enhance our operational strategies and practices to
reflect this new reality. The year-round wildfire season
is here to stay, and we must adapt accordingly.
Our immediate focus is on wildfire prevention,
mitigation and policy advocacy. To that end, in
February 2019 we filed at the California Public Utilities 

Commission (CPUC) our annual Wildfire Mitigation
Plan to address the increasing climate change-driven
“new normal” threat of wildfires. The plan includes
specific metrics that provide transparency to the public
and other stakeholders and will enable the CPUC to
evaluate SCE’s performance.
As part of our plan, we are enhancing and accelerating
inspections on all the company’s overhead power lines
in high fire risk areas. These inspections go beyond
compliance checks, accomplishing both needed
repairs and preventive maintenance to reduce risks
of ignition. Our vegetation management program
inspects and prunes as needed approximately 900,000
trees annually, with more than 400,000 of these trees
located in high fire risk areas. This year, SCE proposes
to remove 7,500 additional hazard trees. Our Wildfire
Mitigation Plan is built largely on our Grid Safety and
Resiliency Program, announced in September,
to harden grid infrastructure and mitigate fire risk
through 2020.
In addition to the enhanced and accelerated
inspections, other key elements of the Wildfire Mitigation
Plan include reducing the risk of sparks that can ignite
fires by replacing at least 96 miles of overhead power
lines in high fire risk areas with insulated wire by the
end of 2019. SCE is installing thousands of “current
limiting” fuses that should reduce fire risk, plus
high-definition cameras and weather stations to
help monitor high fire risk weather conditions across
our service area. We continue to refine our operating
practices related to vegetation management and
Public Safety Power Shutoffs. We are also using new
digital tools, such as applying new machine learning
techniques to continuously monitor our 5 million smart
meters to detect signs of wires down and other trouble
on our system.
As for our policy advocacy, we will continue to act as
a constructive voice to identify durable, long-term
solutions, working with the new administration and
Legislature in Sacramento, local elected officials and
the CPUC. We were encouraged by Gov. Newsom’s
proposal to significantly increase the state’s resources
dedicated to wildfire prevention and response in his
budget. We also look forward to working with the
new Commission on Catastrophic Wildfire Cost and
Recovery created by Senate Bill 901 (SB 901), signed
into law last year.
SB 901 was a first step toward addressing wildfire
issues, but additional action is needed to adapt
to the changing climate, such as clear standards
for critical infrastructure, smarter policies around
building structures in high fire risk areas and a statecoordinated funding strategy to effectively address
fire prevention and suppression.

Recent events that have occurred since SB 901
was enacted, including the PG&E bankruptcy and
downgrades of Edison International, Southern
California Edison and San Diego Gas & Electric
(SDG&E) credit ratings, demonstrate that there is
more work to be done to avoid further harm to
California and its economic vitality. As regulators
exercise their responsibility to implement SB 901,
they must restore investor and ratings agency
confidence in the state’s support of financially
strong, investment-grade regulated electric utilities.
If these issues are not promptly and adequately
addressed, the cost of capital for utilities will increase,
which, in turn, will increase customer costs.
We are in our current situation due to what we
believe is a breakdown of the regulatory compact
in our state. In the application of a principle largely
unique to California, courts have held investor-owned
utilities strictly liable for wildfire damages where utility
infrastructure is a substantial cause of the wildfire, even
if the utility is not at fault. This is based on a legal theory
called “inverse condemnation,” whereby courts have
concluded that the utility has taken (or “condemned”) 

the damaged property and must pay for the damages,
much like a municipality would pay a property owner if
it condemned property to build a road or other public
improvement. Importantly, in applying this theory,
courts have generally assumed that investor-owned
utilities can spread the costs to their customers, much
like a municipality can spread the costs of the property
condemned to build that road to its taxpayers. 

Passing costs to customers is straightforward for
municipal utilities, but investor-owned utilities need
the CPUC to determine our degree of prudency in
order to approve fair cost recovery. However, in 2017
the CPUC denied SDG&E the ability to recover all
their uninsured costs from 2007 fires. The CPUC said
SDG&E had not been prudent, but we believe the
CPUC essentially applied a perfection standard, not
a prudency standard. By denying any cost recovery,
it also disregarded the fact that multiple factors
contributed to the fires and their damages.
In a time of increasing wildfires exacerbated by climate
change, a system in which utilities can be held liable for
100 percent of wildfire damages regardless of fault and
barred from recovering their costs is not sustainable
for an industry so vital to our customers’ and the state’s
economic well-being.
We have proposed to the CPUC — and believe this
should be strengthened by legislative direction — the
establishment of a clear, durable and repeatable
process for assessing the prudency of utility wildfire
operations and enabling timely recovery of prudently
incurred, wildfire-related expenses.
The wildfire mitigation plans that SCE and the state’s
other investor-owned utilities were required to file at
the CPUC under SB 901 should be the basis for this
process. These plans identify specific and measurable
wildfire mitigation activities to be undertaken, and
SB 901 includes a process for the CPUC to review
compliance with these plans. We believe the CPUC
should deem that a utility has acted prudently for
purposes of cost recovery if it is found to have
substantially complied with its plan. Where plan
non-compliance is a direct cause of a wildfire, costs
denied should be proportionate to the utility’s
non-compliance and take into consideration other
factors contributing to the wildfire and its damages.
Our view is that the CPUC has the authority to take this
important first step. The CPUC should act quickly and
credibly, but legislation would give investors stronger
confidence in a more explicit linkage between the
utilities’ substantial compliance with SB 901 wildfire
mitigation plans and cost recovery. Quick action by the
CPUC will stabilize the current deteriorating situation
while the Wildfire Commission and the Legislature
consider additional long-term solutions.
An additional complementary measure would be
the creation of a catastrophic wildfire fund to address
the costs of property damage. We are working
proactively with stakeholders on a solution in the
hopes that we could provide a joint solution to the
governor, Legislature and Wildfire Commission.
Looking beyond our immediate focus on wildfire
issues, SCE continues to make investments in areas
such as transportation and building electrification that
will help support California’s ambitious environmental
policies. Electric utilities are uniquely positioned to
facilitate the transformation to a clean energy economy.
We have the size, scope and infrastructure assets to
deliver clean energy and support electrification for
all customers.
Today, more than 150,000 SCE customers drive
electric vehicles, up from 20,000 in 2013. We are
actively pursuing policies and programs to accelerate
the adoption of electric vehicles statewide, from almost
500,000 passenger cars and medium- and heavy-duty
vehicles today to more than 7 million by 2030. 

Over the last year, through our successful Charge
Ready pilot, we installed more than 1,000 electric
passenger vehicle charging stations. In 2018, we also
received approval of our $356 million medium- and
heavy-duty vehicle electrification program, Charge
Ready Transport. Through this program, we will install
infrastructure at a minimum of 870 sites supporting
electrification of nearly 8,500 medium- and heavy-duty
fleet vehicles.
In 2018, we filed our Charge Ready 2 program, a
$760 million program that includes $561 million in
proposed capital investment for infrastructure to
support 48,000 new electric vehicle charging ports and
increased spending on transportation electrificationrelated marketing, education and outreach.
While transportation is the major component of
our electrification strategy, we are also focused on
converting one-third of the building sector by 2030
from gas to electric-powered water and space heating.
Operational and service excellence continues to be
a major priority across our company. Safety is at the
top of our core values — the safety of our workers and
our communities. Our 2018 performance on safety
did not meet our expectations, as our rate of injuries
leading to days away, on restricted duty, or transferred
— known as the “DART” rate — fell short of our target.
However, we have made progress in our efforts to
transform our safety culture and see opportunities
for continued improvement.
SCE improved its ranking in customer satisfaction
with residential customers such that we believe a firstquartile ranking with this metric is achievable. In fact,
we now rank in the first quartile for business customer
satisfaction. SCE significantly improved system
reliability in 2018 and exceeded our goals for the
year. We implemented actions to reduce the number
and duration of repair outages. We are aiming to
achieve top-quartile performance in this area. SCE also
improved its cost performance in 2018 and exceeded
its goal of controlling costs in support of affordable
customer rates. Based on available information, our
cost efficiency metric, measured as total operations
and maintenance cost per customer, ranked in the
top quartile of our industry peers.
We will remain focused on improving our safety culture
and broader operational excellence while we continue
to actively engage with state leaders on policy reforms
to address statewide climate adaptation challenges,
including wildfire mitigation and liability. At the same
time, Edison will continue to invest in grid hardening
and resiliency as well as the other capital programs
that support the state’s ambitious clean energy goals.
Our challenges are serious, but so are we. I’m excited
about the team we have going forward. I want to thank
the board of directors for its guidance and support,
and welcome new directors Keith Trent and Jeanne
Beliveau-Dunn, who bring deep utility operations and
digital transformation skills, respectively.
I am grateful for the opportunity to be part of our
company and our mission. All of us at Edison look
forward to serving our customers, protecting our
communities, leading the way to a clean energy
future and delivering value to our shareholders

Pedro J. Pizarro
President and
Chief Executive Officer
March 4, 2019