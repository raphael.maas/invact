March 1, 2019
Dear Shareholders,
At Celanese, we strive to create sustained value for our shareholders, customers and colleagues. Sustained value
requires us to focus on the things that we can collectively control and to never simply rely on external factors to
dictate our performance. We believe in our corporation that we can produce differentiated results in any type of
market environment and display that through the business models we installed several years ago and continue to
enhance.
In 2018, we reported adjusted earnings of $11.00 per share,(1) a 47% increase over the prior year and the fifth straight
year of record performance. We also generated $1.2 billion of free cash flow(1) which we deployed in a balanced way
to further grow shareholder value. In particular, strong fourth quarter results despite significant uncertainty and
volatility in the marketplace demonstrated the effectiveness of focusing on the things we can influence.
Over the last several years, our communication regarding our performance has centered around our business
models and commercial strategies. When we implemented these models years ago, we also embarked on a parallel
pursuit to shape an organizational culture centered on a shared sense of purpose and values. Within our plants and
offices, we have referred to this effort as OneCelanese. As I look back at how far we have come, I remain convinced
that this unity of purpose and values we are building has contributed as much to our performance as anything else
we have done. Today, our employees are more empowered and engaged than ever before. The aligned and
engaged efforts of our people multiplies the effectiveness of our business models.
Further to that, I would like to take this opportunity to highlight how we are supporting each other and building a
culture of shared purpose.
Safety & Wellness
The safety of our employees and contractors has always been a prerequisite for everything that we do at
Celanese. In 2018, an OSHA recordable injury rate of 0.28 for all employees and contractors globally continued
to demonstrate a safety record far better than the industry average. We take pride in a long history of industry
leadership in occupational safety.
Building on this, we have dramatically increased our focus on individual employee wellness as we believe that
is a prerequisite for a truly engaged organization. To enable this, we have implemented multiple programs to
improve the physical, mental, and financial wellbeing of our workforce. We know that when employees feel at
ease in these areas, they can perform at their full potential and find greater satisfaction in their work. We are
supporting and incentivizing preventative care in a number of ways including on-site physical examinations
and generous contributions to employees� Health Savings Accounts. Through sponsored activities and
platforms for sharing personal success stories, we are also inspiring one another to practice more healthy and
active lifestyles. We have also expanded the resources available to our employees through various assistance
programs, providing them access to qualified professionals to navigate personal and family challenges and help
them set plans for the future.
Career Development
Our success as a corporation is the product of the development and growth of each individual. Every person is
different and every career path must be unique - a one-size-fits-all approach to employee development is
simply inadequate.
In response, we have implemented a framework that guides how we look at career development in an
individualized manner while providing a consistent structure across the company. We call this development
tool our Global Career Framework. By organizing the hundreds of different work opportunities at Celanese into
logically structured job functions and families, we have made it much easier for employees to chart their own
career paths, and this facilitates customized career discussions. Frequent discussions are orchestrated to
identify future options for growth using this framework and to learn from the career paths others in the
company have taken. As a result, our employees have a clearer vision of the opportunities available to them at
Celanese to grow and be challenged in new ways. We are increasingly able to turn those opportunities into
reality. Through all levels of our organization, we see an increasing number of our people who have held 
?
positions in multiple businesses or functions at Celanese, which invariably builds tremendous depth and
capability within the company.
Inclusion & Diversity
It is critical that each of our employees feel fully comfortable bringing his or her �full self� to work each day. This
starts with a culture of inclusivity where differing perspectives are encouraged, respected and utilized.
Inclusivity must always precede diversity and its accompanying benefits.
Today, we are supporting nearly a dozen employee resource groups that foster inclusivity across race, gender,
age, sexual orientation, and disability to name a few. Each resource group is sponsored by a senior leader at
Celanese and is directly guided through locally led initiatives. Through various activities and forums for
discussion, these groups drive greater understanding and celebrate the diversity that exists at Celanese. We are
thrilled to see new branches of these groups springing up across our sites globally through the grassroot efforts
of employees. As an example, in late 2018 a new branch of our Young Professionals resource group was created
and is influencing the career development of our young employees at our offices in Shanghai, China. Today, I
see a Celanese that is more diverse and inclusive than ever before, and we are continuing on this pursuit. With
a culture of inclusivity, we can empower our employees to offer up their individual perspectives and contribute
directly to our success every day.
Shared Ownership
The more individuals that take part in the value we create through shared ownership, the deeper the unity and
alignment in delivering results. At Celanese and across many companies, this principle has long been an
effective means of building alignment between leadership�s efforts and the company�s long-term strategic
objectives.
In 2018, we extended that alignment to our entire organization via a comprehensive Global Equity Grant,
spanning 32 countries and nearly 7,000 employees. With this, essentially every Celanese employee across the
world is now also a Celanese shareholder. In addition, we continue to incentivize our people to personally invest
in ownership of Celanese through an employee share purchase program. As a result of these efforts, we have
better harmonized the daily efforts and decisions of each employee with the value it ultimately creates for
shareholders.
Giving Back
The impact of our giving at Celanese continues to connect us as an organization with our local communities
and those in need. The passion that I see in our employees as they engage in this worthwhile effort continues
to amaze me. In 2018, we hosted approximately 15,000 volunteer events across the globe, in which roughly
9,000 individuals donated over 200,000 hours of service. Each of these events represented a distinct unifying
experience our employees had with each other and their local communities. Over 60% of our people
participated in volunteerism in 2018, more than double the industry standard, and our employees are
increasingly sharing these opportunities with their family and friends.
Our employees and partners continue to generously give of their financial resources through the Celanese
Foundation, which donated over $3 million last year. They are personally engaged in identifying local needs
and helping us to decide how to deploy resources in increasingly impactful ways. As an organization, we are
committed to continued partnership with our local communities to provide critical assistance where needed.
These highlights, among many others, have shaped our progress in creating a culture centered on our shared
purpose. We continue to focus on developing this culture in step with our business models as a differentiated source
of value enhancement.
In everyday interactions, I am inspired by the growing sense of engagement, purpose and strength our teams
demonstrate. Their commitment to our shared goals positions us to continue growing as individuals and as an
organization. I would like to thank each for their contributions to our success in 2018 and for the efforts they
continue to put forth every day. 
?
And on behalf of all of us at Celanese, I thank you for the continued trust you place in us through your investment.
We take very seriously our role as careful stewards of your capital. We remain committed as an organization to our
shared purpose of creating sustained value for all Celanese stakeholders over the years to come.
Sincerely,
Mark C. Rohr
Chairman and Chief Executive Officer 