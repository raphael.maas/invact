To Our Shareholders:

2007 was a challenging year for Morgan Stanley and the entire financial services industry, as the unprecedented disruptions and illiquidity in the credit markets severely impacted some parts of our business. While we suffered significant losses in our mortgage-related business, we still achieved our second highest revenues in Firm history and delivered record financial performance across most of our businesses. We also made significant progress in executing our long-term strategic plan and further strengthened
our client franchise in key areas. And, thanks to the hard work and dedication of our talented people, we have begun 2008 with renewed momentum, increased discipline and an intense focus on performance.

Delivered Record Performance across Our Business despite Mortgage-Related Writedown

The writedown that Morgan Stanley announced at year end in our mortgage-related business was the result of an error in judgment made by a small team in one area of fixed income and a failure to manage that risk appropriately. It was deeply disappointing to meas I know it was to all of our shareholders. We moved aggressively to address these issues and to make necessary changes: holding people accountable; putting a new senior management team in place led by Co-Presidents Walid Chammah and James Gorman; enhancing our risk management function; and being as transparent as possible with investors about our mortgage-related exposure.

These writedowns should not overshadow the strong performance and positive momentum we saw across 
the rest of Morgan Stanleys businesses in 2007. our investment banking, equity ales and trading, and
asset management businesses each delivered record results. our revitalized Global Wealth Management
business more than doubled pre-tax income. And our international business increased revenues by 44%. Even with our mortgage-related writedown, we delivered net revenues of $28.0 billion. The fact is: Our core business remains strong. And we also unlocked significant shareholder value in non-core businesses through the spin-off of Discover in June 2007 and the successful initial public offering of MSCI last fall.

The strength of our businessand our strategyis clear in the long-term investment we received from China Investment Corporation in December 2007. This investment grew out of our deep and long-standing ties in China. The Chinese market is a critically important strategic priority for us, and we see dramatic growth opportunities ahead. This investment helps strengthen our relationship with a key player in the region while also giving us additional capital to pursue the growth opportunities we see globally across our franchise in 2008 and beyond.

Continued Strengthening of Our Institutional Securities Franchise
Morgan Stanley continued to deliver strong results across many areas of Institutional Securities this pat
year while maintaining leadership positions in key client businesses and executing against our key
growth initiatives.

our investment banking revenues were a record $5.5 billion in 2007, up 31% from the previous year.
This included record advisory revenues, up 45% from 2006, and record nderwriting revenues, up 21%.
We finished the year ranked #1 in Global Completed M&A, #2 in Global Announced M&A and #3 in Global IPOs. (Source: Thompson Financial from January 1, 2007 through November 30, 2007)

our equity ales and trading business also delivered its best full-year results ever in 2007with net
revenues increasing 38%. This reflected record results in both derivatives and prime brokerage, driven in
part by our continued investments in these businesses. In fixed income ales and trading, our interest
rate and currency products achieved record results, up 6% from the previous year, and we had our second best year ever in commodities, which continues to be a market leader.

Delivered Significantly Improved Performance in Global Wealth Management
Over the past few years, we dramatically reinvigorated our Global Wealth Management business
thanks in large part to the leadership of James Gorman and his team. We delivered profit before tax of $1.2 billion in 2007, up 127% from the previous year, and a pre-tax margin of 21% in the fourth quarter, a major improvement from the 2% margins this business delivered in early 2006. Our financial advisors are among the most productive in the industry, with record annualized financial advisor productivity of $853,000, and we attracted $40 billion in net new money last year.

Weve now delivered seven consecutive quarters of improved financial performance in Global Wealth Management while continuing to make progress on key initiatives to further strengthen this business.
I couldnt be more pleased with the significant progress weve made in just two years time. And Im confident that Ellyn McColgan, an experienced executive who will be joining us in April after a distinguished career with Fidelity Investments, is the ideal person to build on that tremendous momentum.

Made Dramatic Progress Executing Growth Strategy in Asset Management
Our strong commitment to investing in and growing Asset Management also has paid off with our best year ever in 2007. We had record net revenues of $5.5 billion, up 59% from the previous year, and record profit before tax of $1.5 billion, up 72%. Weve achieved five consecutive quarters of net customer inflowswith record net inflows of $35 billion last year compared with net outflows of $9.3 billion

a year ago. We also had record assets under management of $597 billion at the end of November, up nearly 35% from two years ago.
Under Owen Thomas leadership, weve continued to make important progress implementing the changes necessary to improve growth in the Asset Management business. The business launched and incubated 74 new products in 2007. It continued expanding in alternativestaking minority stakes in Abax Global
Capital limited and Traxis Partners. and we established our merchant banking practice by bringing
together Morgan Stanleys industry-leading real estate business and our newly established private equity
business. We also continued the expansion of our international footprint in Asset Management last year
by hiring local sales teams, opening local offices and enhancing our licenses.

Continued Building Out Our Global Franchise
Over the past few years, we have been making important investments in Morgan Stanleys global
franchise to seize the significant growth opportunities in Europe, Asia and the emerging markets. As a result, we achieved record international revenues last year of almost $16 billion, up 44% from 2006. The Firm also continued expanding its global footprint and local presence, particularly in key emerging markets, including:

 China: We more than tripled our revenues in China over the previous year and now are obtaining
various domestic licenseswhich will help us continue strengthening our business in the region.

 Middle East: In 2007, we formed a joint venture with the Saudi Arabian securities firm Capital Group, secured a license in Qatar and established a research team in Dubai. These important investments have helped increase our Middle East revenues about 90% since 2005.

 India: We ended our joint venture with JM Financial last year in order to expand Morgan Stanleys wholesale businesses in India and develop a platform that is fully integrated with our global footprint. Weve been making investments in talent and capital since then and received a merchant banking license in October 2007.

Taking Action ... and Moving Forward
As 2007 came to an end and our new fiscal year began, we moved aggressively to address our risks associated with disruption in the mortgage securities market and continued building on the strong performance across most of our other businesses.

We put in place a new senior management team, including our new Co-Presidents as well as
Mitch Petrick, the new Global Head of Sales and Trading. We reorganized and enhanced our risk management function by moving it to report directly to Chief Financial Officer Colm Kelleher, bringing
in more talent and creating an additional risk-monitoring function within the trading business.
We also consolidated all of the Firms proprietary trading activities under Mitch and made a series of changes throughout Sales and Trading that will create more discipline, partnership and transparency
across the division.

The Path Ahead
While the past year has been difficult, we are aggressively driving forward. Our actions will help us
adjust to the changing market conditions and position Morgan Stanley for continued growth and improved profitability.

In the coming year, we will continue to invest in our international and emerging markets businesses,
as well as key areas of Institutional Securities like commodities, equity derivatives and prime brokerage.
We will expand our Global Wealth Management franchise internationally by growing our private banking
business. And we will build on the momentum in Asset Management, as we look to further strengthen
our private equity and alternatives businesses and grow our stable of experienced portfolio managers.
As we take these steps, we will reallocate our talent and our resources to those areas where we see the best potential for growthand we will remain flexible in pursuing the opportunities we see to enhance value.

The near-term outlook is challengingfor Morgan Stanley and for the financial services industry generally. But we have a strong, broad-based business and a truly global footprint. We remain very positive about Morgan Stanleys business mix, and we continue to believe diversification is a great strength for us. As we move forward in 2008, all of us will be intensely focused on fully realizing the incredible power and tremendous potential of Morgan Stanleys global franchise.

Sincerely,
John J. Mack
Chairman and Chief Executive Officer
February 5, 2008