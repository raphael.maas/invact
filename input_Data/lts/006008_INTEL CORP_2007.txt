In 2007, we continued to focus on extending
our product leadership; leveraging our world-class
process technology and manufacturing capabilities;
and creating a more ef cient, customer-oriented
Intel. Our  scal year results re ect the signi cant
progress we have made in all of these areas.
Revenue in 2007 was $38.3 billion, up 8% over 2006. Our operating
income was $8.2 billion, up 45% over 2006. Net income for 2007 was
$7 billion, up 38% over 2006. Our cash dividend payout reached a record
$2.6 billion, and we announced a 13% increase in our cash dividend
beginning in the  rst quarter of 2008.
Renewed focus on core strengths
We have renewed our focus on leveraging two core strengths that
distinguish Intel from the rest of our industry: the Intel architecture
and our unmatched ability to bring cutting-edge technologies to
market year after year. As part of renewing our focus, we have divested
several smaller operationsincluding those related to application
processing, optical, and certain telecom productsand are contributing
the assets of our NOR  ash memory business to a newly formed
independent company, Numonyx. At the same time, we are investing
in new areas where we believe the application of highly integrated
Intel architecture affords large growth opportunities, such as:
 Energy-ef cient, low-cost mobile Internet devices and ultra-mobile
PCs that enable people to communicate, enjoy digital media, and
access the Internet wirelessly.
 New types of consumer electronics devices that combine entertainment
functions with Internet connectivity.
 Scalable, high-performance visual computing solutions that integrate
vivid graphics and supercomputing performance for scienti c,
 nancial services, and other compute-intensive applications.
 Low-cost PCs designed to meet the needs of  rst-time computer
users, particularly in emerging markets.
Advancing corporate responsibility
More and more, the global need for energy ef ciency is affecting
everything we dofrom how we build and operate our facilities to
how we design our products. We topped Corporate Responsibility
Of cer magazines 10 Best Corporate Citizens by Industry 2007 list
for technology hardware companies, and were the Technology Market
Supersector Leader of the Dow Jones Sustainability Index for the
seventh consecutive year. Such recognition acknowledges our global
health and safety, community, and education programs, as well as
our efforts to reduce our impact on the environment.
Ground-breaking products and processes
We have established a roadmap for sustained technology leadership
through our tick-tock strategy of introducing a new silicon process
technology approximately every two years and ramping the next
generation of microarchitecture in the intervening years.
Our 45-nanometer (nm) processors, launched in November 2007,
were designed from the ground up with energy ef ciency in mind.
As of the end of February 2008, we offered more than 30 of these
processors, which are built using Intel 45nm Hi-k metal gate silicon
technology, an entirely new transistor composition that minimizes
electrical leakage and enables us to continue the pace of innovation.
They boast nearly twice the transistor densityup to 820 million
transistors for quad-core processorscompared to previous chips built
on our 65nm technology, and have set a number of records on key
industry performance benchmarks, while consuming less power. These
eco-friendly processors are also manufactured using a lead-free process.
We are on track to ship our new, highly innovative microarchitecture
code-named Nehalemin 2008, extending our lead in both
performance and power. We have also already demonstrated our 32nm
process technology, scheduled for introduction in 2009.
Strong demand for our products
Throughout 2007, we saw robust demand for our products across
multiple business segments and geographies. In November 2006, we
launched the industrys  rst quad-core processors, and by the end of
2007, we had shipped more than 6 million quad-core units. In 2007,
we completed our transition to the Intel Core microarchitecture,
delivering its energy-ef cient performance bene ts across our entire
desktop, mobile, and server processor lines.
Our integrated platformswhich combine Intel processors and other
technologies to address speci c user needscontinue to provide value
that customers cant get elsewhere. Platform products such as those
built with Intel Centrino processor technologies have enabled us to
take advantage of the worldwide shift from desktop to mobility products,
contributing to revenue growth in that segment of 19% year over year.
Building on 40 years of innovation
We have also made signi cant progress on improving ef ciency across
all of our business operations. For example, during 2007 we achieved
our goal to reduce the amount of time it takes to process wafers in
our factories by 50%. We recognized savings of about two and a
half billion dollars in 2007 and expect additional savings in 2008 as a
result of our ongoing ef ciency efforts. In addition, our 2007 customer
survey results indicate signi cant improvements in our customer
service and responsiveness.
I am extremely proud of our employees, and as we approach our 40th
anniversary in July 2008, I have no doubt that they will continue to
carry on Intels unwavering commitment to moving technology forward
and creating products that change peoples lives.
Letter From Your CEO
Paul S. Otellini, President and Chief Executive Of cer
Technology Impact Energy Ef ciency
Core Strengths Platform Advantage
We are investing in new product
areas, such as mobile Internet devices
and ultra-mobile PCs, that leverage
two of our core strengths: Intel
architecture and our cutting-edge
manufacturing technology.
By combining Intel processors and
other technologies in platform products,
we provide added value that customers
cant get elsewhere, allowing us to
take advantage of trends such as the
worldwide shift to mobility.
The global need to reduce power usage
contributed to strong demand throughout
the year for our products based
on the energy-ef cient Intel Core
microarchitecture, including those for
compute-intensive server applications.
Through the Intel World Ahead Program,
we continue to demonstrate the
impact that our technology can have
on improving education, healthcare,
economic development, and governance
around the world.
2007 Highlights
Intel 45nm Hi-k metal gate silicon technology:
One of the Best
inventions of the year.
TIME Magazine
Our competitive position is the strongest it
has been in years. Demand for our industryleading
products and manufacturing technologies
helped drive strong revenue and earnings
growth in 2007, and our efforts to increase
operational ef ciencies resulted in substantial
savings. We repurchased more than 111 million shares of stock,
reported a record cash dividend payout, and announced a 13%
increase in our cash dividend.
Despite these positive results and an approximate 32% increase
in our stock price during 2007, we saw our stock price fall subsequent
to year-end, due in part to market concerns about a slowdown in the
U.S. economy and a potential slowing computer marketplace. We have
an increasingly international business pro lewith about 70% of our
products going to non-U.S. marketsbut our stock price continues to
be closely coupled to the U.S. economic outlook.
Our strategy has been, and continues to be, to invest in new
products, technologies, and business ef ciencies regardless of the ups
and downs of economic cycles. Currently, we are investing to further
expand our business in emerging markets and to develop products for
market segments that we believe offer growth opportunities, such
as mobile Internet devices, consumer electronics, advanced graphics,
and low-cost PCs.
Looking back on 2007, I am reminded of the profound effects
that our industry has on people across the globe. Through the Intel
World Ahead Program, we are seeing examples of these effects.
This program integrates and extends our efforts to provide people in
emerging markets with technology training and access to affordable,
connected PCs to improve education, healthcare, and economic
development. On a recent trip to Baramati, a village in western India,
I saw how technology is changing communities. The schools have
few computers, but the villagers desire for technology is strong.
Local authorities have turned school buses into traveling computer
labs, bringing technology to different schools on various days. I asked
a 10-year-old girl what her favorite subject was, and she responded,
Tuesday. She said Tuesday was the day the PC bus came to her
school, and it was the highlight of her week.
We also collaborate with business and government organizations,
such as the World Economic Forum and the United Nations, to help
expand the positive impact that technology can have globally.
Together, we are not only working to improve lives, but also sowing
the seeds for our future businessa true win-win proposition.
I believe we are well-positioned to continue to lead our industry
in both business and corporate responsibility. We entered 2008 with
what I believe is the best combination of products, silicon technology,
and manufacturing leadership in our history. Our investments should
allow us to take advantage of new growth opportunities worldwide.
Letter From Your Chairman
Craig R. Barrett, Chairman of the Board