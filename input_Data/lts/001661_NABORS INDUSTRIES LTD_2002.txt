they all matter {heres why}

Growth in our International operations fueled a solid year
despite protracted weakness in our North American businesses,
once again demonstrating the beneficial impact that Nabors
breadth and diversity can have on our performance. The year
also provided strategic opportunities to expand our business
in Canada and further enhance our financial structure, actions
which had a slightly negative short-term impact, but which are
already significantly enhancing the long-term outlook. While 2002
likely represented a cyclical low point, we still managed to post
the fifth best year in the Companys history at $0.81 per share.

Our strong international performance reflected the
initial contribution of multiple long-term, higher-margin
offshore contracts, partially offset by temporarily diminished
land activity in Latin America. Nine similar contracts were
secured throughout 2002, with their full impact materializing
in 2003. Nabors high success rate in securing such contracts
stems largely from the operating and capital cost advantages
associated with our existing global infrastructure and asset
inventory. Last years offshore reorganization was another
contributing factor in that it effectively equalized Nabors
tax burden with that of our foreign competitors.
Our expansion in Canada is proving to be particularly
well timed and will be increasingly germane to earnings
throughout the emerging up cycle. The late 2001 acquisition
of Command Drilling was followed by the 2002 acquisitions
of Enserco in April and Ryan Energy Technologies in
October. These actions substantially increased our presence
in this important market, an essential component in the
North American gas picture. Even though Canadian drilling
was lackluster in 2002, the upswing this operation enjoyed
in the fourth quarter validates the investments we made and,
along with early 2003 results, reinforces our confidence in
the long-term nature of this market.

Nabors took advantage of favorable credit markets to
lengthen the average term of our debt at very attractive rates.
This consisted of two new debt issues, one with a seven-year
term at an interest rate of 47/8 percent and a ten-year issue at
51/8 percent. Even though we experienced weaker year-over-year
results, we were still able to internally fund more than 90 percent
of the cash portion of our expenditures for acquisitions and
new capital equipment, resulting in a financial position that
is stronger and more flexible than ever.
Commodity prices recovered throughout 2002, but did
not reach the levels necessary for our customers to substantially
accelerate their drilling programs until the fourth quarter.
This perceived disconnect is due to the fact that the threshold
at which our customers drilling prospects become viable has risen
substantially since the robust drilling environment we witnessed
in 2001. This rising threshold can be attributed to the higher
costs and risks associated with finding and developing incremental
gas supplies to offset a depletion rate that has nearly doubled
over the last decade. Confidence in the long-term sustainability
of these prices was building as the year ended, but awareness of
historical gas price volatility was a factor in the deferral of higher
activity into the new year. As 2002 came to a close, higher than
anticipated commodity prices spurred a significant up tick in

Canadian activity, which has been a precursor to higher U.S.
activity, concrete signs of which are rapidly emerging.
Going forward, we firmly believe that significantly higher
levels of land drilling are necessary if the U.S. is to develop a
sufficient supply of natural gas, which is the fuel of choice to
drive economic growth because of its environmental desirability.
While there are a number of potential fuel alternatives for natural
gas and petroleum, the regulatory, economic and technical
challenges associated with their development remain formidable.
Indeed, economic competition from these energy sources on a
scale large enough to challenge the preeminence of natural gas
and dampen the need for continued and expanding worldwide
exploration and production efforts is at least a decade away.
In the interim, we expect to see higher average gas prices
than we have witnessed in recent years. These will serve to
reduce the components of gas demand that are most flexible
and sensitive to price, while stimulating supply by improving the
economic incentives to drill. While North America is generally
thought to be a mature basin, a sustained higher price environment
makes drilling in more remote areas and the pursuit of
deeper reservoirs more viable, and justifies the increased use of
rig-intensive technologies to improve the exploitation of existing

fields. These trends will increase the demand for rigs, particularly
those higher specification units that can drill deeper, more
complex wells and those that are uniquely suited for specialized
applications like the Canadian Arctic. Nabors fleet is particularly
well suited to meet these challenges.
We expect Nabors to post progressively improving results
over the next few years, even if the U.S. Lower 48 component
of the North American gas market does not materialize to the
extent we believe it will. Our confidence is based in large part
on our increased presence in the strategic Canadian market,
the probability of continued robust growth in our International
operations and anticipated improvements in our multitude of
other businesses. It appears increasingly likely that we may be
entering a period wherein all of our businesses will experience
simultaneous growth for the first time in the Companys history.
This bodes well from an investor perspective, providing
great promise that we will continue to deliver the kind of
profitability and return on capital that merits the trust you
have shown by investing in this Company.
Sincerely,
Eugene M. Isenberg
Chairman and Chief Executive Officer