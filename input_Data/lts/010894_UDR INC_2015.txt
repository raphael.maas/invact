To Our Fellow Shareholders:

2015 was an impressive year for UDR as we continued making significant progress in the execution of our
Long-Term Strategic Plan. We achieved strong same-store operating results, delivered highly accretive
development projects, further strengthened our balance sheet and participated in two creative value-enhancing
transactions  all of which demonstrated the continuing long-term value creation we are providing to our
shareholders.
Specifically, we:
? Increased FFO as Adjusted per share by 10% to $1.67 from $1.52 in the prior year;
? Generated 2015 same-store net operating income (NOI) growth of 6.7% over 2014;
? Acquired $901 million of apartment communities in the recovering Washington, D.C. market;
? Invested $271 million in the $559 million West Coast Development Joint Venture, earning a preferred
return and delivering projects with an accelerated timeline into high-growth markets in Southern
California and Seattle;
? Sold twelve wholly-owned communities for $409 million for a total gain of $252 million, or 61%;
? Sold our 20% interest in the Texas Joint Venture, for a total price of $400 million, of which UDR
received $44 million in proceeds;
? Completed over $300 million of development projects and commenced construction on ~$400 million
of new developments with a combined potential value creation of 35-40%, or $200 to $300 million of
incremental net asset value;
? Strengthened our balance sheet metrics through increased NOI and accretive transactions, resulting in
30% debt to enterprise value and net debt-to-EBITDA of 5.7x; and
? Declared dividends of $1.11 per share, representing a 7% increase over 2014 declared dividends.
These accomplishments were rewarded through strong total shareholder return (TSR) of 26% in 2015 versus
the FTSE NAREIT Equity REITs Total Return Index and Standard & Poors (S&P) 500 Index returns of
3.2% and 1.4%, respectively. 

Strategy
We remain committed to the
execution of UDRs multi-year
strategic plan, first outlined in
2013, which has focused our
efforts. Our three main
objectives are:
1) Drive organic growth,
expand margins and
improve resident and
associate satisfaction
through our best-in-class
operating platform;
2) Allocate capital to
accretive opportunities,
including development,
that strengthen our market
mix and portfolio quality;
and
3) Accretively fund growth
and maintain a strong and
flexible balance sheet.
Successful execution of these
three objectives generate high
quality cash flow growth which
increases the dividend and net
asset value per share, and
ultimately drives total
shareholder return. 

As I write this, I am
crossing a very
personal milestone. February 12,
2001 was my first day as CEO of
UDR, which was as great an
honor then, as it is now. I am very
grateful for the opportunity and
support the Board of Directors
has given me, and for my fellow
associates who have helped create
a great Company and have
instilled a culture of success.
In the 15 years Ive been at the
helm of UDR, our management
team has overseen significant
transformation of our asset mix
and market concentration through
$16B in real estate transaction
activity. Today, our market mix
of quality communities in Urban
and Suburban locations represents
the balanced portfolio we have
worked hard to build. Our
talented team of associates
manages, buys, sells, develops
and redevelops UDRs
communities, and weve built a
strong and deep bench to ensure
continued success of the
Company for many years to
come.
While we can measure success in
many ways, ranging from our
associate engagement, resident
satisfaction, operational prowess,
asset quality, and balance sheet
strength, we remain focused on
the results that are critical to our
shareholders share price
appreciation and continued
growth of our dividends.
During the past 15 years, we have
delivered compounded annual
total shareholder return (TSR)
of 14%, an excellent result
compared to the S&P 500 Index
returns of compounded annual
TSR of 4%, during the same time
period. The Company has, and
continues, to employ a long-term
strategy resulting in deliberate
and steady improvement that
ultimately led to UDRs inclusion
to the S&P 500 Index on March
7, 2016, an accomplishment with
which we take great pride.
15 Years of Progress:
Shareholders
1. 573% in total
shareholder return
2. 224% in share price
appreciation
3. $2.8B in dividends paid
4. 30% leverage on
enterprise value from 60%
Enterprise
1. $14.5B enterprise value
from $3B
2. $10.9B equity market
cap from $1.2B
Quality
1. $1,956 revenue per
occupied home from $674
2. $286K value per home
from $50K

Economic and Housing Environment
2015 was another good year for the U.S. economy, with GDP growth of 2.2% and an average
unemployment rate of 5.3%. The combination of GDP growth and low unemployment rates translated to
robust job growth of 1.9% or 2.1 million new jobs.
Oil was a significant driver of the global economy in 2015, as the global crude oil glut continued to weigh
on the market. Although U.S. oil prices fell 30% in 2015 to $37 a barrel, the effect of the declining oil
prices translated to a net positive for U.S. consumers. Federal Reserve Chair, Janet Yellen, said, The
decline we have seen in oil prices is likely to be on net a positive. Its something thats certainly good for
families, for households. Its putting more money in their pockets, having to spend less on gas and
energy.
As the economy completes its 7th year of growth, we are confident that the U.S. economic engine is still
the world leader and this growth will continue.
Multi-family demand is primarily driven by three fundamentals: 1) population changes in renter cohorts;
2) rental demand from continuing job growth; and 3) continued decline in homeownership.
1) Population Changes in Renter Cohorts
Two significant renter pools consist of 20-34 year olds (Millennials) and 50-64 year olds (Empty-Nester
Baby Boomers), both of which have a high propensity to rent, and represent the two largest population
cohorts in the U.S., as demonstrated in the figure below.

Millennials, which number 66 million, have delayed the age at which they get married or have children,
actions which typically are associated with the purchase of a single family home. Out of necessity, many
Millennials have been confined to the rental market, as large, looming levels of student debt make it
extremely difficult to afford down payments. Additionally, Empty-Nesters, which number 63 million, have
become free to enjoy the same amenities, activities and cultural events in a live/work/play environment as
those sought out by the Millennials.
2) Rental Demand from Continuing Job Growth
As previously mentioned, 2015 exhibited strong job growth of 1.9% or 2.1 million jobs. This is especially
positive for apartment owners, as job growth and resultant household formation has historically been the
primary driver of incremental apartment demand. Additionally, over the past few years, the strongest job
growth has been reflected in our primary renter cohorts.
When evaluating demand, it is imperative that one also consider new supply. Due to the Great Recession,
from 2009 through 2014 multi-family supply was severely depressed resulting in a deficit in available
apartment homes. While new supply has increased in 2015 and 2016, annual deficits in apartment homes
remain based on current demand.
We believe there is a strong runway with regard to demand/supply in our markets, and our portfolio is well
positioned to take advantage of this opportunity.
3) Continued Decline in Homeownership
The third primary driver of multi-family demand is the declining homeownership rate. U.S.
homeownership reached a peak of 69% in 2004 and has continued to decline to 64% today. It is important
to note that every 1% drop in the homeownership rate results in approximately 1.1 million new renters, or
5.5 million new renters in total a number far exceeding the new supply of rental homes.
In short, economic and housing forces were tailwinds in 2015, and appear to remain
strongly in our favor for the foreseeable future.
Operations
The core business remains our operating platform led by Jerry Davis, a 25 year veteran of UDR. We
entered 2015 after achieving stand-out marks in 2013 and 2014. 2015 was even more impressive. Our
team produced full year revenue and NOI growth of 5.6% and 6.7%, respectively, with revenue growth
well above the national average of 5.2%. Our mix of quality communities and price points in Urban and
Suburban locations meets the needs of a wide range of targeted renters.
Our strategy in 2015 was to push new lease rate growth across our portfolio. This strategy proved
successful, as we increased revenue per occupied home by 5.5% and grew our total portfolio revenue per
occupied home to $1,956 per month. We achieved this growth with just a slight increase in our annual
turnover, and we continue to operate at a very healthy 96% plus occupancy through the work of our very
capable leasing team, combined with our state of the art website and supported by our corporate outbound
call center. All of these factors have helped maintain occupancy and provides us continued pricing power. 

We also maintained a keen focus on expense control. We have implemented several initiatives across our
platform to maintain our operational advantage, including reducing the days our apartments are vacant,
increasing maintenance staff efficiency and internalizing our bad debt collections. These three initiatives
alone led to over $10 million of incremental NOI to the bottom line in 2015.
At the same time, we continue to innovate for the future with a broad set of initiatives that will generate
incremental NOI to our operating platform, and continue to improve our impressive NOI margin that now
stands at 71.5%, an extraordinary level for any business.
Our operating platform consists of our outstanding people, processes, systems,
continuous innovation and execution. It is the crux of our business and we will continue
to drive operational excellence in the future.
Transactions
2015 was an active year of successful transactions for UDR, led by the very experienced team of Harry
Alcock, Andrew Cantor and Warren Troupe. Early in the year, we disposed of our 20% interest in the $400
million Texas Joint Venture which resulted in net proceeds of $44 million and an IRR of approximately
14%. This disposition enabled us to reduce our exposure to the Texas markets, and eliminate our exposure to
Houston, which is highly dependent on the oil market.
In May, we invested $271 million in the $559 million West Coast Development Joint Venture. This joint
venture is comprised of five under-construction communities in highly desirable locations on the West
Coast. With this transaction we were able to accelerate the delivery of 1,533 new apartment homes into highrent growth markets while earning a 6.5% preferred return on our investment through stabilization of each
community, while our partner is responsible for all construction risks. We have fixed price purchase options
to acquire the remaining interests in these communities one year following completion of construction. This
transaction is expected to provide strong returns to our investors.
In October, we closed the $901 million acquisition of six Washington, D.C. assets in conjunction with the
privatization of another multi-family REIT, Home Properties, Inc. The transaction was comprised of three
newly constructed assets, one recently redeveloped asset and two assets with redevelopment potential. This
acquisition was funded largely with the issuance of $565 million Operating Partnership Units (OP Units)
at a price that made the transaction accretive in year one, improved our balance sheet metrics and increased
our exposure to the recovering D.C. market.
Throughout the year we disposed of $409 million wholly-owned non-core assets. We sold each asset at
attractive cap rates and were able to exit the Norfolk, VA and minor Mid-Atlantic markets, and realized
gains of $252 million or 61%.
These transactions highlight our ability to accretively allocate capital, improve our
market and community mix, and grow cash flow while improving our balance sheet
metrics.

Development
We view development as a core competency and a large value creator for our business while continually
refreshing our portfolio. Success of our development program has proven itself over the long-run with
spreads between investment yields and respective capitalization rates on average of 150-200 bps. Our
development projects are concentrated in markets that we believe will enjoy strong household formation
and demand over the coming years. The key to development is being the in right place at the right time and
executing at an exemplary level.
No one gets it right all the time, but we take numerous actions to mitigate risk throughout the development
process. First, the target size of our development pipeline is designed to be in a range that is appropriate for
both our balance sheet and the team in place. We recognize that having an experienced and focused
development team is essential and ours is led by Bob McCullough under the watchful eye of Harry Alcock.
That brings me to our second risk mitigation approach, disciplined underwriting. When we underwrite any
new development, we understand that market rents are subject to change. To mitigate this risk, we utilize
todays rents with modest growth assumptions when computing our targeted returns. If our targeted returns
are not met at the time of underwriting, we will not start a development.
Third, when looking at new development sites, we generally seek entitled land to minimize the time lag and
risk from land acquisition to initial construction. Additionally, we will not start construction until
construction drawings are completed and we have a contract in place guaranteeing our maximum costs.
Most importantly, we have removed our dependency on the capital markets through
our self-funding approach. We size our development pipeline such that we can fully
fund our annual development spend through our free cash flow and non-core asset
sales.
By selling older, non-core assets to develop new core assets in primary markets, we are
able to maintain the quality of our portfolio and match-fund our capital spending. 

In 2015, we completed two communities totaling $311 million in cost. The first of these projects is our 369-
home $218 million 100 Pier 4 community, located in Bostons Seaport District, which is the current and
future home to many large corporations, including General Electric and State Street, relocating either within
or to the Boston area. This project was developed on-time and leased-up well ahead of expectations, at rents
higher than we underwrote. Additionally, we completed Steele Creek, a 218-home, $93 million
participating loan investment, located in Denvers Cherry Creek neighborhood, home to several high-end
retail stores, restaurants and amenities. This high-end product was the first of its kind in the neighborhood
and achieved rents exceeding initial underwriting.
During 2015 we commenced two projects in California for total cost of $392 million. The first is our $342
million, 516-home Pacific City, located in Huntington Beach, CA. The property sits adjacent to the Pacific
Coast Highway and the Huntington Beach Pier, and many amenities including shopping, restaurants and a
hotel are just steps from future residents homes. We started pre-leasing this community in late 2015 and
expect initial move-ins in 2017. Also, we started the $99 million (our share of which is $49.6 million)
Domain Mountain View project in Mountain View, CA. Domain Mountain View will contain 155 homes
and the expected completion date is mid-2017. This community will appeal to the many high-income
employees located in Silicon Valley, including Google, Hewlett Packard and Tesla.
Today, our development pipeline target is $900 million to $1.4 billion, which translates to an annual spend
of $300 to $500 million. At the end of 2015, we had a pro-rata share of $670 million of assets under
construction, as well as $364 million of additional preferred equity and participating loan investments. We
expect to start 2-3 new projects in 2016 while maintaining our same development principles.
Our development pipeline produces average values that are 35-40% accretive to NAV,
or for every $1 spent our shareholders get $1.35 to $1.40 in value.
Balance Sheet
In 2015, as promised, we continued to strengthen our balance sheet metrics. At year end, our leverage ratio
based on an undepreciated cost of assets and Net Debt-to-EBITDA stood at 34.6% and 5.7x, respectively,
an improvement of 400 basis points and 0.8x, respectively over 2014. Since the initiation of our Three-Year
Plan in 2013, we have exceeded our expectations for leverage, Net Debt-to-EBITDA and Fixed Charge
Coverage by large margins.
We strengthened our balance sheet through a number of different actions in 2015, executed by our very
capable team led by Tom Herzog and Warren Troupe, including:
? Issuance of $211 million of common equity and $565 million of UDR OP units, all at a premium to
NAV;
? Amendment of our unsecured credit facility and term loans outstanding, leading to greater capacity,
longer maturity dates and improved pricing; and
? Issuance of $300 million of 4.0% Senior Unsecured Notes due in 2025.
These actions helped lead to a recent rating increase by S&P. We are now rated by Moodys and S&P at
Baa1 and BBB+, respectively an achievement we are quite proud of.
A strong and flexible balance sheet is essential to securing the predictability of our
future cash flows, leading to a growing, safe and consistent dividend.

Board Changes
We have a Board of Directors with a wide-range of skills and expertise to help guide our management
team. We are grateful for their wisdom and counsel. Since the start of 2015, we added two new Board
Members. Mary Ann King, elected in July, brings a high level of expertise in transactions and West Coast
market knowledge (40% of our NOI), through her experience as President with Moran & Company. The
addition of Clint McDonough in February, 2016, added a high level of audit and assurance proficiency,
given his experience as a Managing Partner for the Big-4 accounting firm, Ernst & Young, servicing a
wide range of Real Estate clients for more than 35 years. Our Board of Directors now stands at 10
members, 9 of which are independent.
The knowledge and skill of our Board of Directors is of high value to our continued
success.
Future
As we look ahead to 2016, our outlook appears very similar to 2015. Fundamentals remain very strong due
to demographics, housing and lifestyle choices. We expect another 1.5 million households to be formed in
2016, due in large part to the Millennial cohort, which is forecast to grow through 2030. We expect 2.7
million jobs to be created and 380K multi-family homes to be delivered in 2016. This equates to an
environment largely in our favor. The homeownership rate is expected to continue to decline, with many
analysts forecasting rates as low as 61%. Lifestyle choices are largely leading to a preference for renting
versus ownership. These trends indicate a strong rising tide of opportunity for many years to come.
In early February, we released our updated 2-Year Strategic Plan which provides shareholders a detailed
roadmap of our goals and expectations for the next two years and outlines how we plan to create value.
This level of transparency allows shareholders to measure our performance against expectations and holds
management accountable. We hope you find the information useful and we appreciate your continued
support in UDR.
In closing, I would like to thank all of my associates and Board Members for their hard work in making
2015 another great year for UDR.

Thanks,
Thomas W. Toomey
President and Chief Executive Officer
