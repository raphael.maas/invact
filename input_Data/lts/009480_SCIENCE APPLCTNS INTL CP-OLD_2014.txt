DEAR FELLOW STOCKHOLDERS,
As we look back on our first fiscal year as a transformed SAIC, we are proud of our
successful transition and revitalization of a 45-year heritage into a new, re-energized
public company with $4 billion in annual revenue. On September 27, 2013, we
launched the next generation of SAIC  13,000 employees focused and dedicated
to delivering on both our customer commitments and shareholder expectations.

We Are a More Effective and Nimble Company
For the first eight months of fiscal year 2014 (FY14),
we were part of our former parent company, now
doing business as Leidos. We used that transition
period to do two things: manage and deliver on the
existing business during a challenging time in the
market; and design a new SAIC to better serve our
customers and be more competitive in our industry.
During the redesign, every aspect of new SAIC
underwent rigorous review as we sought to create
a new company better positioned for the future. We
embraced the SAIC brand because of the market value
and our employees pride and sense of ownership in
SAIC. Four parameters guided the enterprise design
that led us to a new innovative company structure:
 Improve service to our customers by leveraging
the full power of SAIC
 Retain and attract the best and brightest people
who share our values and pride of mission
 Improve competitiveness through cost efficiencies
 Emphasize business results that return value to
our shareholders
In February 2013, we put an experienced management
team in place that was engaged with our employees
and customers. The eight-month transition allowed
us to assemble a team that is experienced, understands
success and accountability not just for their respective
businesses, but also to each other. The transition
period also enabled us to implement a major new
operational model for the company  one focused on
performance enabled by true enterprise collaboration.
We evaluated our technical competencies and
organized those into eight well-defined service lines
and placed 85 percent of our skilled professionals in
those service lines. We now have complete visibility
of our technical resources that allows us to invest in
our capabilities and deploy the best-qualified talent
across the enterprise versus the talent that was
traditionally isolated within a business unit. In
other words, we are able to leverage the total
strength of SAIC like never before. We believe this
is a game-changer for our people and our delivery
approach, providing better support for our customers
and driving much broader visibility and access
to enterprise resources and capabilities.
We consolidated our customer-facing professionals,
including business development and program
managers, into five groups. Each customer group
is responsible for a clearly defined client base.
For the first time in our history, we have clear
accountability and responsibility within the company
for supporting each customers tactical and strategic
mission. This focus and alignment allows us to
prioritize our business opportunities to maximize
the return on our investments and improve our
competitive contract win rates.
In order to drive our competitive business model,
we also redesigned our corporate functions with
a clear focus on cost efficiencies to match SAICs
business portfolio and further leverage our lower cost
shared services center in Tennessee. These aggressive
measures resulted in the consolidation of numerous
facilities and centralization of our corporate structure,
delivering more than $100 million in savings from
the prior year.
At the time of the company separation, SAIC added
three new directors who were not previously an
officer or director of our former parent, all with deep
experience in both the government and commercial
sectors. We have a combined board of directors that
brings the experience and governance necessary
to support the companys success.
These immediate changes have provided new
opportunities for our employees and provided greater
value to our customers. Additionally, investors and
analysts have responded positively to the realignment
of our operations, which optimizes effectiveness and
cost in todays competitive government services market.
We Delivered Solid Fiscal 2014
Financial Performance In a Challenging Market
We performed to the guidance we provided to our
investors in September while undergoing major
change due to the separation from our former parent
and implementing a newly designed company.
The market was challenging for SAIC in FY14,
similar to our competitors. However, we were
able to meet our commitment of delivering
revenue of $4.1 billion, reflecting an expected
contraction of 14 percent versus our fiscal 2013
results. This contraction was concentrated in our
defense portfolio largely driven by the loss of
the DISN Global Solutions program, decreased
activity on logistics programs impacted by the
Afghanistan drawdown, the successful completion
of a technical support program to the U.S. Army,
and delays in U.S. government contract awards due
to budget pressures.
Our FY14 margin performance improved during the
year as the impact of the separation costs from our
former parent declined, and our cost improvements
took effect. As a result, our operating margin
increased to 6.0 percent in the last quarter of FY14
and we delivered diluted earnings per share of
$2.27 for the full year in line with our previously
announced estimates.
SAIC ended the year with a strong balance sheet that
included $254 million of cash and FY14 cash flow
from operations of $183 million. This significantly
exceeded our expectations as a result of our focused
efforts on cash collections. This enabled us to
continue to return capital to shareholders through
our quarterly dividend and a recently initiated share
repurchase program.
As reflected in these financial results, SAIC
overcame a number of federal market challenges,
such as the government shutdown in October, and
delays in contract award decisions throughout
the year. Based on the breadth of our services and
solutions we have been able to sustain the volume of
proposal submittals from across the business areas
to address the ongoing needs of our customers. And
we will continue our focus on business performance
as we assist our customers in navigating this new
budget environment.
Our Goals for the Future
As we build upon the successful transition of the
last year, there is still work to be done to optimize
our business. Our focus in FY15 and the next several
years is on four priorities:
1. Low single digit organic revenue growth. In this
challenging federal market, SAIC is focused on
sustaining revenues by protecting our incumbent
contract positions through excellent contract
execution and winning a large share of our recompete
contracts. We are expanding sales to
current customers by drawing on our proven
technical expertise in the eight service lines. We
are growing business with new customers by
leveraging our proven capabilities and SAICs
success with current customers.
2. Annual operation margin expansion of 10 to 20
basis points, on average and over time. Operating
margin optimization is critical to SAICs value
proposition and is a top priority for the enterprise.
Increasing our competitiveness through continued
optimization of our cost structure will position us

to win more business. Similarly, increasing the
SAIC labor content and reducing dependencies on
subcontractors is expected to result in higher bid
fees and subsequent contract fees.
3. Optimize our operational structure and business
model. We continue to optimize our operating
structure through the collaboration of the
customer groups and the service lines to drive
business performance and drive profitable growth
by strategically investing in sales pursuits and
solutions. SAIC is also investing in our key asset 
our talented people  with dedicated development
programs for program management, engineering,
and technical certifications.
4. We remain committed to deploy excess capital
to shareholders. SAIC continues to distribute
a regular quarterly dividend and is returning
capital through our ongoing share repurchase
program that we began in December 2013. In
addition, SAIC has a modest debt level and over
time, we intend to increase our debt level to
a leverage ratio that is more appropriate for a
business with SAICs steady and substantial cash
flow. We believe our cash generation and balance
sheet flexibility allows us to simultaneously invest
for growth, return capital to shareholders, and
pursue selective acquisitions when compelling
opportunities present themselves.
The Future Looks Bright
The reenergized spirit within the SAIC team is
palpable -- collaboration grows stronger every day
and already has been instrumental to improving
the quality of our expanded business development
pipeline to include post-FY14 wins like the U.S.
Armys Aviation and Missile Command (AMCOM)
professional and engineering support services
contract and the Naval Air Systems Command prime
contract to support its Anti-Submarine Warfare
(ASW) sensor systems.
Our extraordinary SAIC employees are defined
by their sense of mission, integrity and serving
our customers worldwide. They give their best,
day in and day out, in support of the mission. Our
employees apply that same level of commitment and
engagement by volunteering in targeted corporate
giving programs to support our customers and
communities in which we live.
Looking out across SAIC today, we are focused,
energized and motivated to support our customers,
our employees and our stockholders going forward.
We are proud of the progress we have made thus far
in the launch of our new company, and the spirit of
collaboration that is prevalent across the company.
The future holds great promise and we are just
getting started.
EDWARD J. SANDERSON, JR. ANTHONY J. MORACO
