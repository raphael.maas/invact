DEAR FELLOW SHAREHOLDERS,

Throughout 2018, we implemented our
blueprint for delivering long-term shareholder
value, and I am excited about 2019 and the
years ahead. I will highlight some of our
most impactful 2018 accomplishments and
then set out our strategy moving forward.
Early in the year, we outlined guiding principles
including a focus on capital efficiency, cash flow
and balance sheet management to be competitive across all investment sectors. While we
saw commodity price volatility and felt the
impact of uncertainty around global factors
such as trade relationships, sanctions, OPEC
compliance and geopolitics, we also returned
SHAREHOLDERS
DEAR FELLOW
significant value to debt and equity investors,
including almost $300 million in share repurchases, approximately $200 million in dividends
and repaid $609 million of outstanding debt.
We ended the year and enter 2019 focused on
aligning costs with current commodity prices
while delivering operational excellence and
continued strong performance in the areas of
health, safety and the environment. Long term,
we plan to sustain moderate growth while
consistently generating free cash flow to deliver
shareholder returns. The startup of Leviathan
later this year is a milestone for the company
and an important step for our future success. 

Active Portfolio Management
and Operational Excellence
In addition to returning value
to shareholders through
dividends, share buybacks and
debt reduction, we successfully
completed several strategic
portfolio transactions. We
exited the Gulf of Mexico and
CONE Midstream Partners
and completed our required 11
percent sell down of our Tamar
interests. With these efforts
and the hard work of Noble
Energy�s talented employees
around the world, we have
positioned our unique and
Delivering a new level of safe and responsible development though the Colorado Mustang
Comprehensive Drilling Plan.
diverse asset base to excel
for many years to come.
We also achieved significant
operational milestones.
� Across our asset portfolio,
we delivered 11 percent
production growth,
adjusted for divestments.
� In the Colorado DJ Basin,
we delivered volumes and
cash flows above plan as
we progressed our Mustang
development. We received
approval for the unique
100-square-mile Mustang
Comprehensive Drilling Plan,
the first development plan 

of its size approved by the
State, and we have already
secured more than 400
associated well permits. We
are grateful that Colorado
voters appreciate the
importance of oil and gas
and its positive impact on
the state�s residents, schools
and economy and rejected
a ballot initiative that would
have significantly restricted
energy development.
� In the Permian Basin, after
a tough first half of the
year, we shifted to row
development and recognized 

improved well performance
in the second half of the
year. After expanded
appraisal drilling in the
first half, we ramped
production and doubled
volumes year-over-year.
We also added critical
operational infrastructure,
and, by securing rights on
the new EPIC long-haul
pipeline to Corpus Christi,
we enhanced transportation reliability and
positioned the company
to access export pricing.
� In the Eagle Ford
Basin, we strategically
reduced activity in
response to prices and
focused near-term
capital on completing
wells already drilled.
� In Equatorial Guinea (EG),
production outperformed
expectations, and we
progressed the development
of gas monetization. Our
Alen platform in EG will be
a hub for developing the
3 TCF gross gas reserves
that we have already
discovered in the region.
� In Israel, we outperformed
expectations. Tamar
continued to produce reliably
with almost a 100 percent
run time. In our world-class
Leviathan project, we
competed the Leviathan
drilling and completion phase
and are on schedule to come
on line by the end of 2019.
Well testing confirmed the
capability to deliver 1.2 BCFD
combined from four wells.
Further, the initial execution
of agreements to deliver our
gas into Egypt provides a
material step in accessing
that growing regional market. 

� Company-wide, we
materially increased our
exploration inventory,
adding more than
100,000 acres in the
onshore U.S. and entering
a new country offshore
with a position of over
2 gross million acres.
Operating Effectively
Means Operating Safely
and Responsibly
Demonstrating our
commitment to energizing
the world and bettering
people�s lives, we again
delivered excellent safety
performance and continued
our focus on environmental,
social and governance
(ESG) matters. In 2018, we
expanded the purview of
the former Environmental,
Health and Safety Board
Committee to oversee and
guide the company�s Safety,
Sustainability and Corporate
Responsibility (SSCR) strategy
and management, including
initiatives in methane emission
reduction, water management
and climate assessment. We
also developed an executive-level Sustainability and
Corporate Responsibility
(SCR) Committee that works
with a cross-functional group
of experts to think deeply
about climate, health, safety,
social and public policy
issues and other corporate
responsibility matters
relevant to our business.
We continue to have an
engaged Board of Directors.
Each of our directors is
actively focused on responsibly growing shareholder
value. As Edward Cox leaves
our board and Lee Robison
retires from our executive team
in 2019, I�d like to thank both
of them and acknowledge
their contributions to our
success throughout their
tenure. In 2018, we welcomed
Barbara Duganier to our
Board of Directors, Rachel
Clingman as General Counsel
and Brent Smolik as President
and Chief Operating Officer.
I am excited about the
refreshment of our leadership
and the broad and diverse
perspectives and experiences
each of these individuals
brings to Noble Energy.
In September of 2018, we
held our second Global
Day of Caring, a day that
our executive team, Board
members and nearly 1,000
employees in seven regions
around the world took a
workday to volunteer in the
communities where we live and
operate. Our work, more than
4,000 hours globally, ranged
from housing repair to food
collection to care kit assembly 

for troops serving abroad.
These efforts benefitted 19
non-profit organizations
and were amplified by
our global matching gifts
program to further deliver
critical services and support
in our communities.
Looking Forward
This year is a transformational
year for Noble Energy. The
start-up of Leviathan by year
end positions us to generate
long-term, sustainable
free cash flow that can be
returned to the shareholders,
while continuing to build
the company. Our inventory
of high-return U.S. onshore
drilling locations combined
with our unique, world-class
offshore assets provide a
competitive advantage. The
organization will continuously
improve and drive returns
through relentless capital
efficiency. To underscore this
commitment, we have modified our 2019 compensation
program to include an annual
capital efficiency metric.
Earlier in this letter I highlighted significant milestones
in our journey together, and
I appreciate your continued
trust and confidence as we
begin an exciting new chapter
for Noble Energy.
Sincerely,
David L. Stover
Chairman and CEO