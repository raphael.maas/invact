To our investors
Fast, secure and mobile connectivity to
everything on the Internet  everywhere, at
every moment and on every device  is what
drives us at AT&T. It is why we build wireless
and wired networks engineered to handle
massive volumes. And the more capacity and
speed we build into our networks, the more
intensively and creatively our customers
use them. Today, our customers are taking
advantage of these networks, streaming
video to all of their devices at unprecedented
levels. In fact, today more than 50 percent of
our network traffi c is video.
The byproduct of our investments in these
advanced networks has been rapid innovation
across the entire technology ecosystem 
software services, cloud computing, mobile
apps and connected devices. All of this is
changing how people live, how businesses
run and how economies grow. 
But these investments are also changing
AT&T into a vastly different company with
unique capabilities serving new markets
that will further differentiate us from our
competitors. Over the past several years,
we have taken several steps to position
AT&T for the future, while rewarding you,
our shareholders, with solid returns. 
Expanding our video business
Earlier, I pointed out that video is driving
huge increases in network traffi c. Thats no
surprise when you consider that customers
want their video on every screen, whether its
traditional pay TV service, video streamed
over an Internet connection or video to a
mobile device. So, in addition to our network
investments, weve been assembling the
other pieces needed to lead in delivering
video when, where and how customers
want it.
Our acquisition of DIRECTV, which we expect
to complete in the fi rst half of 2015, will make
us the worlds largest pay TV provider, giving
us nationwide reach in the United States,
plus 11 Latin American countries. DIRECTVs
premier TV service signifi cantly improves
the economics and expands the geographic
reach of our current TV business. The
acquisition places us in the best position
to provide customers with integrated
packages of TV, mobile and high-speed
Internet services.
DIRECTV will also provide us with the best
content-owner relationships in the industry.
These relationships are critical to being able
to deliver premium content across multiple
screens, particularly mobile devices, to
meet consumers future video viewing and
programming preferences. Getting more
content to more screens is also the aim of 
our Otter Media joint venture with The
Chernin Group, which is building new digital
video brands and online services.
DIRECTVs superior quality and profi table
nationwide TV service make it economically
attractive to signifi cantly expand our highspeed
Internet service to a total of 70 million
customer locations, because people like to
buy TV and Internet service together.
Not only does DIRECTV perfectly
complement our existing business, it
signifi cantly diversifi es our revenue mix
and is expected to be accretive within
a year after close on a free cash fl ow per
share and adjusted EPS basis.
Expanding our mobile network to Mexico
We believe that the model in the United States,
with exploding demand for mobile Internet
service and all the associated economic
benefi ts, will be repeated around the world 
as companies invest in high-speed mobile
networks. That belief has us searching for
opportunities to invest in wireless outside
the U.S. where the regulatory, investment
and economic climate is right.
We believe that environment now exists in
Mexico. Recent changes in Mexicos legal
and regulatory framework have made it very
attractive for a new entrant. We closed our
Iusacell acquisition in early 2015 and expect
to complete our Nextel Mexico deal by midyear.
Once we do, well move quickly to build a
world-class mobile business in a country with
a strong economic outlook, a growing middle
class and close trade, cultural and geographic
ties to the United States.
Well be able to offer customers the first-ever
North American Mobile Service area  one
seamless network that will cover more than
400 million people and businesses in the
United States and Mexico. For families and
businesses that span the border, AT&T will
be the only company with an advanced
mobile network  built for video  across
both countries. 
A new kind of company
By the end of 2015, once we close our
DIRECTV and Nextel Mexico acquisitions,
we will be a very different company. Our
revenue mix will be much more diversified,
and our ability to integrate products and
services will set us apart from competitors.
Our revenues will come from four areas:
 Business services (wireless and wired)
 will be our largest revenue stream
 U.S. consumer TV and Internet service
 will be second
 U.S. consumer mobility will be third,
 comprising about 20 percent of
 total revenues
 Mexico and Latin American TV and
 mobility will be our smallest but
fastest-growing area
We like that mix a lot. Not only will a more
diversified mix of services, customers and
geographies make us much less dependent
on the U.S. consumer wireless market, well
be a new breed of company  one that will
redefine what a communications company
is and what it does.
We will be a company with the ability to
deliver video to any device. We will be able
to deliver integrated capabilities across a
diversified base of services, customers,
geographies and technology platforms that
are mobile, fast and highly secure. We will
have a path to profitable TV growth. And we
will have a nice set of growing Latin American
businesses positioned well in video and the
mobile Internet.
Regulation that looks forward, not back
The historic investments that catapulted
America to global leadership in the mobile
Internet and high-tech innovation were
the direct result of deliberate and wise
government policy. Since the Clinton
administration, policymakers have agreed,
on a bipartisan basis, that light-touch
regulation was the proper approach for
the Internet. This decision helped unleash
nearly two decades of amazing innovation
rooted in equally amazing levels of investment.
Now, in a major reversal, administration
officials are proposing a radically different
formula that includes end-to-end regulation
of the Internet.
For several years now, weve endorsed the
principles of net neutrality. We agreed to
them when the FCC first established net
neutrality rules in 2010. We also agreed with
the set of net neutrality principles laid out
by President Obama last fall. We still do. But
key policymakers in the administration and
at the FCC are now going well beyond any
previous concept of net neutrality. They are
attempting to regulate the entire Internet
under an arcane law called Title II that was
written in 1934 to regulate the rotary dial
telephone. We feel this antiquated approach
will damage investment and damage the
Internet itself. I wont belabor the point here,
but you will see us continue to aggressively
make the case to policymakers  and, if
necessary, to the courts  that the FCCs
proposed Title II regulation of the entire
Internet is at best a solution in search
of a problem and at worst a threat to the
United States continued global leadership
in technology and innovation. 
Financial results built on strength
Its taken a tremendous amount of your
capital to strengthen our premier network
and lay the groundwork for our future 
nearly $140 billion over the past six years.2
Weve been able to invest
at this level because we
have one of the strongest
balance sheets of any major
U.S. telecom company.
And its paid off with our having what I would
argue is the premier network  not just in
the U.S., but in the world. Now that weve
met our signifi cant network transformation
goals ahead of schedule, going forward you
can expect us to return to normal capital
expenditure levels. 
Last year, we grew revenues to $132.4 billion,
grew adjusted earnings per share to $2.51,
generated $31.3 billion in cash from
operations and realized $8.1 billion more
from selling non-strategic assets. In addition
to investments in future growth, we also
increased our quarterly dividend for the 31st
consecutive year, and returned more than
$11 billion directly to shareowners through
dividends and share repurchases.
This kind of fi nancial strength that allows us
to invest in the future while returning value to
you will continue to be a hallmark of AT&T.
Looking ahead, Im more confi dent than
ever that we have the right team and the
right strategy to defi ne the future, just as we
have shaped the past. As we execute that
vision, I want to thank you, as always, for your
continued confi dence in AT&T.
Sincerely,
Randall Stephenson
Chairman, Chief Executive Offi cer
and President
February 10, 2015