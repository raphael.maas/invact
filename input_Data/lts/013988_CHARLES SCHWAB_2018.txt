To my fellow stockholders,
This is my 11th opportunity to have the honor of crafting our
Annual Report letter. As always, my goal is to keep this letter
clear, direct, and free of jargon, corporate speak, and trendy
buzzwords. The litmus test for my letter is whether it reads
as if I were corresponding with a business partner who has
been out of touch for the past year. As always, please let me
know if I�ve achieved this goal.
The year 2018 created a dichotomy, both for your company�
The Charles Schwab Corporation�and for the markets and
economy. Merriam-Webster�s dictionary defines dichotomy
as �a division into two�contradictory groups or entities.�
In 2018, we saw just such a contradiction�between our
business performance and our stockholders� returns. From a
client growth and metric standpoint, 2018 was our strongest
year in history. By virtually every measure, our clients� trust
and confidence in us helped lead to record growth:
� Record new-to-retail households, up 20%;
� Record daily average trades, up 26%;
� Record new brokerage accounts of 1,576,000; and
� Record core net new assets of $227.8 billion, with total
client assets at $3.25 trillion

From a financial standpoint, we achieved similar record
results. In 2018, we achieved our sixth consecutive year of
record revenue and our fifth consecutive year of record net
income. Revenue grew 18% from 2017, while net income
rose 49% from the prior year, aided by tax law changes.
We achieved this while continuing to invest consistently
in capabilities designed to ensure long-term growth and
operating efficiency. 

And yet, from a stockholder return standpoint, it was a very
disappointing year. After a five-year period in which our total
returns to stockholders averaged over 30% per year, we saw
our stock price fall in 2018 from a starting point of $51.37
to $41.53, despite our ongoing record-breaking business
performance. Truly a dichotomy.
We were not alone. The performances of the broader
economy and equity markets were similarly contradictory.
Although the year was relatively strong from an economic
standpoint, with record low unemployment and strong
GDP growth, the stock market was highly volatile with the
S&P 500� peaking at 2,931 on September 20th and ending
the year down 6% at 2,507. Truly a dichotomy.
Of course, we recognize that individual stock prices and
the overall equity market respond to a variety of factors.
Nevertheless, I am disappointed that our strong business
and financial results did not lead to stronger 2018 returns
for owners of our stock. It is my responsibility to see that
we make optimal long-term decisions for the benefit of our
stockholders, and I believe we are on the correct long-term
path strategically. But, while I remain convinced that our
long-term stockholders will be rewarded as a result of our
efforts, these decisions did not lead to growth in the value of
our stock price in 2018.
Despite this, I have never been more confident about our
future. Our position in the marketplace has never been
stronger. Our client metrics have never been stronger. And
our financials, both revenue and earnings, have never been
stronger. 

The Future for Schwab�A Winning
Approach
Our business model is relatively simple. It revolves around
the Golden Rule: If we treat prospects and clients the way
we would want to be treated, they will choose to start a
relationship with Schwab and do more business with us. This
approach comes to life through our corporate-wide �Through
Clients� Eyes� strategy and its execution, which find life in
our �Virtuous Cycle.�
�Through Clients� Eyes� simply means that we will evaluate
decisions we are faced with by examining: (1) whether they
are likely to attract more prospects to choose Schwab and
encourage clients to do more business with us; and (2) whether
the execution of our decisions through the �Virtuous Cycle� will
reward our clients, employees, stockholders, and communities.
In a world where consumers have more choices than ever, where
transparency is greater than ever, and where the old-fashioned
idea of �price versus quality trade-off� has been proven largely
archaic, we are more confident than ever that our strategy and
execution are ideally positioned for long-term growth.
At Schwab, we have embraced�and even championed�a
�no trade-offs� approach, offering our clients what we
believe is an unparalleled combination of low costs, full
service, and guaranteed satisfaction. And this approach has
contributed to our record-breaking results.
Our strategy is designed around our core beliefs about
investor needs and the future of investing. And as a company
committed to transparency, we share these beliefs publicly
for everyone, even our competitors, to see and evaluate. In
my Annual Report letter last year, I discussed three of the
beliefs we have about the next decade of investing:
� The concept of �beating the market� has given way to
a client focus on financial planning, asset allocation,
tax efficiency, and low-cost investing;
� Fiduciary-standard advice, fee transparency, and low
costs are a fundamental expectation for most clients; and

� Scale is playing an increasingly large role in determining
the �winners� as costs related to cybersecurity,
compliance, and regulatory oversight challenge subscale firms� ability to compete effectively.
These three beliefs remain valid and central to our view of the
landscape. This year, I�d like to discuss three additional factors
we believe are shaping the investment services industry today
and how we are taking advantage of these changes to better
serve our clients and reward our stockholders:
� Independent registered investment advisors (RIAs) will
continue to grow faster than the industry overall due
to an acceleration in the number of brokers turning
independent and affluent consumers� expectations for
transparency and a fiduciary-standard experience;
� Clients view robo-advice as a credible investment
product, but investors of all ages will place their longterm trust in firms that offer a combination of people
and digital experiences; and
� Although brand matters, brand loyalty won�t ensure
client retention as consumers are more willing than
ever to change providers in search of lower cost,
greater transparency, and more objectivity.
Rapid Growth for Independent RIAs
Over 25 years ago, we noticed an unusual phenomenon
at Schwab. Some of the most affluent clients we served
were asking us to allow another individual to access their
investment account. When we dug into the details, we
realized that these investors were hiring independent
professionals to help them manage their money.

These professionals were unique in that they did not work on
commissions as most brokers in the industry at that time did.
Rather, they took a �fiduciary� role�they placed their clients�
best interests ahead of their own, and offered advice and
guidance for a clear and understandable fee.
We set to work building an infrastructure to support these
independent investment advisors and make it easier for
them to serve affluent investors. That was the genesis of our
custodial business.
Today, this area is growing rapidly relative to the rest of the
industry, and it is poised for ongoing success. Investors see 
enormous value in the relationship-based, fiduciary RIA
model. Prospective RIAs see great potential in exiting the
traditional wirehouse model and working independently to
build their own privately owned firms.
We proudly serve more than 7,500 of these RIA firms,
and they entrust us with over $1.55 trillion of their clients�
money. When appropriate, we even refer Schwab clients to
independent investment advisors.
Our position as the largest provider of custodial services for
independent investment advisors has been an important
driver of our overall growth. The revenue and earnings
derived from serving independent advisors make up about
30% of our overall results. And our advertising efforts are
educating more and more investors about the benefits of
working with an independent investment advisor.

Robo-Advice Vs. Professional and
Personal Expertise
Several years ago when the concept of robo-advice�or
advice derived from algorithms and delivered digitally via the
internet or a mobile device�was a relatively new concept,
many proponents suggested that robo-advice would
eliminate the need for investors to work with humans.
We did not fall into the trap of thinking that robo-advice was
an all-or-nothing approach to investing. Rather, we saw it as
an important capability that would supplement the human
side of investment advice, not replace it. Interestingly, many
of the early robo-only proponents now agree with us.
Digitally delivered advice makes sense for many investors.
It can be done at a very low price. It can be delivered
efficiently across time and geographic distance. And it
helps investors consistently stay rebalanced as the market
rises and falls, ensuring that their investments adhere to
their level of risk tolerance.
Robo-advice works so well that our program, Schwab
Intelligent Portfolios�, is the largest truly digital advisory
program in the world in terms of client assets, with well over
$30 billion under management. And it has included access
to live investment professionals from day one.
Combining digital advice with trained and registered
investment professionals ensures clients can access a
calming voice during times of market volatility. At Schwab,
we believe in the power of combining both technology AND
people. In 2018, clients interacted with us digitally 4 billion
unique times, but they also interacted with us over 25 million
times in person or over the phone. Technology is a wonderful
enabler, but when it comes to investing, people matter.
Brand Matters�But Actions That Benefit
Clients Matter More
In recent years, we have been honored to be recognized by
many prestigious third parties such as Fortune magazine,
J.D. Power, Investor�s Business Daily, Barron�s, and many
others. Why does this matter?
Because in the world we live in today�with a 24-hour news
cycle, with overwhelming volumes of social media, with opinion
blogs masquerading as news stories, and with increasingly
sophisticated consumers�the value of what a company says
about itself via advertising has never been lower.
While advertising retains a role in building a company, what
really matters is whether what a company says about itself
publicly is reflected in what it actually does.
For example, every company claims it is �client focused,� but
how many companies have proactively reduced prices by
more than half a billion dollars over the past two years as we
did, compelling our competitors to take similar actions?
Every company says it is striving to delight clients, but why
has no other investment services firm instituted a satisfaction
guarantee as we did several years ago?
Every brokerage firm talks about putting their clients� interests
at the forefront, but why are so many firms reluctant to offer
those clients fee-based, fiduciary investing advice that puts the
clients� best interests ahead of their own?
At Schwab, we believe that we earn client loyalty through
our actions each and every day. We earn it by making
courageous decisions that others criticize for their own
purposes but that we believe are right in the long run.
Let�s look at how we operate at Schwab in all three areas
touched on by these questions.
Pricing. We have lowered our pricing to clients by more than
a half billion dollars over the past two years. In each primary
case, we did so proactively, motivating competitors to follow
suit. Why? Because one of our core beliefs is that we should
share the benefits of our growth with those making that
growth possible�our clients. It is important to remember that
every dollar an investor doesn�t pay in fees or commissions is
an extra dollar in their pocket�where it belongs.
Satisfaction. A number of years ago we introduced a
satisfaction guarantee for our clients. Why? If we do not
delight our clients, or if they are dissatisfied with any aspect of
our service, don�t they deserve a refund? What surprises me
is that competitors often copy innovative ideas we introduce�
but not this time. At Schwab, we have the courage to stand
behind what we do for clients. Shouldn�t everyone?
Fiduciary Standards. Much has been written in the past
few years about fiduciary investment advice. The concept is
simple. Does the investment provider place client interests
above their own when offering investment advice? When a
client invests in one of our fee-based investment advisory
programs, they can be confident in this: As a fiduciary, we
always act in the best interests of our clients. 

Our Commitment
I opened this letter to you, our valued stockholders, by
discussing the dichotomy of this past year. While your
company broke all historical records for client growth in
new-to-firm households, new accounts, and core net new
assets, our stock price ended the year down almost 20%.
And while the economy demonstrated exceptional strength,
with low inflation, record low unemployment, and healthy
GDP growth, the stock market tumbled in the fourth quarter.
A short-term investor might logically ask, �What are you
going to do differently to grow the stock price in 2019?� It�s
a fair question. And my answer is simple: We will not change
our core strategy!
Our �Through Clients� Eyes� strategy is working�arguably
better than at any time in our 40+ year history. Existing
clients are responding and entrusting us with more of their
hard-earned dollars than ever before. New clients are
trusting Schwab with their money at record levels�younger
investors, older investors, new investors, and highly affluent
investors alike.
Markets go up and down. Interest rates go up and down.
Consumer sentiment ebbs and flows. But a strategy based on
serving others the way we would want to be served is timeless.
Of course, we will continue to change and adapt in
ways that help us better serve clients through digital
platforms, mobile capabilities, ever-improving services,
and professional guidance. But who we are and what
we believe in will not change.
When I crafted my first Annual Report letter at the end of
2008, at the height of the financial crisis, I ended with the
following sentences:
I�d like to emphasize the things that remain true
to Schwab, no matter how stormy the external
environment may be.
Our Commitment to Clients: We pledge to deliver
premier service, build trusting client relationships, offer
competitive pricing, and continue the great Schwab
legacy of innovation�both in products and in technology.
Our Commitment to Stockholders: We pledge to
be vigilant in terms of risk management and expense
controls�treating every dollar of expense as if it came from
our own wallet�as we work to enhance stockholder value.
Our Commitment to Employees: We pledge to invest
in our people and in the resources needed to grow this
company for years to come.
As I close my 11th opportunity to share with you, I would
like to reiterate these commitments. We have adhered to
a deeply held set of principles for the last decade, and as
owners you can be confident that these principles remain
at the very core of who we are and what we do every day at
your company.
Thank you for your ongoing trust and confidence!
Warmly,
Walt Bettinger
February 27, 2019

