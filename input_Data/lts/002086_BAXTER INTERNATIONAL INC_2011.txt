Dear Shareholders

Baxter International delivered another strong year through our focus
on life-saving, life-sustaining products and our ongoing commitment
to innovation. Over the course of 2011, we touched millions of lives
with our core product portfolio, increased investment in research and
development (R&D) to accelerate our new product pipeline, intensified
business development to augment organic growth, and gave back
to the communities we serve through industry-leading sustainability
programs. By staying focused on our core mission, Baxter achieved
a high level of financial performance and drove enhanced value for
its shareholders.

Acceleration through Innovation
For 80 years, Baxters success has been built on a simple
mission: serving patients at their times of greatest need
with a diverse portfolio of medically necessary products
and therapies. The company is also constantly evolving,
advancing patient care with cost-effective offerings that
address important clinical needs.
The global macroeconomic environment continued to pose
serious challenges in 2011, creating barriers and complexities
throughout the healthcare industry. Nevertheless, Baxter
was able to accelerate sales growth and profitability, while
expanding its portfolio with R&D pipeline developments that
will deliver benefits for patients and healthcare providers. The
emphasis on innovation is not limited to scientific frontiers; it
also encompasses how we do business as we introduce new
approaches and pursue opportunities that create additional
value for Baxter, our shareholders and patients worldwide.
2011 Financial Performance
In 2011, Baxters worldwide sales increased 8 percent,
totaling $13.9 billion. The company reported net income
of $2.2 billion, or $3.88 per diluted share, compared to net
income of $1.4 billion, or $2.39 per diluted share, in 2010.
On an adjusted basis, excluding special items in both years,
Baxters net income in 2011 was $2.5 billion, representing an
increase of 4 percent from $2.4 billion in 2010, while earnings
per diluted share of $4.31 increased 8 percent from $3.98
in 2010. Cash flow from operations, including pension
contributions, totaled $2.8 billion in 2011.
Platforms for Growth
Baxter is delivering solid performance for customers and
shareholders by focusing on four growth platforms  areas
of opportunity that will move the business forward in the face
of the changing economic landscape.
The first and most fundamental of these platforms is our
existing, broad portfolio of core products. We are constantly
pursuing opportunities to optimize performance through
enhancing our position in current markets and expanding into
new geographies.
Building on this important foundation, Baxter is also driving
growth through a robust new product pipeline, complementary
business development opportunities and new government
partnerships that can expand access to care while strengthening
Baxters market presence worldwide.
Advancing our R&D Pipeline
Baxters enduring growth depends on a consistently innovative
pipeline of new offerings that broaden our portfolio of medically
necessary products and therapies while reinforcing our
leadership in core therapeutic areas. Last years R&D
expenditures reached a record level for the company.
Throughout 2011, we introduced a number of new products,
expanded indications of existing products and advanced
approximately 20 key R&D programs in late-stage clinical
development, many of which have the potential to profoundly
improve the treatment and delivery of care for chronic
diseases like hemophilia, immune deficiencies, Alzheimers
disease and end-stage kidney disease. Among these
accomplishments, Baxter:
 Received United States Food and Drug Administration
(FDA) approval for the subcutaneous administration of
GAMMAGARD LIQUID 10% [Immune Globulin Infusion
(Human)] (marketed as KIOVIG Human Normal
Immunoglobulin (IVIg) outside the United States and
Canada) for patients with primary immunodeficiencies.
 Expanded the indication for KIOVIG in Europe to include
treatment for multifocal motor neuropathy (MMN), and
submitted a supplemental biologics license application to
the FDA for approval of GAMMAGARD LIQUID 10% to treat
MMN in the United States.
 Submitted to United States, European and Canadian
regulatory bodies for approval of HyQvia, Baxters
investigational immunoglobulin therapy administered
subcutaneously and facilitated by recombinant human
hyaluronidase for patients with primary immunodeficiencies.
 Completed enrollment in the first pivotal Phase III clinical
trial evaluating the use of GAMMAGARD LIQUID 10% for
the treatment of mild to moderate Alzheimers disease; the
company completed a futility analysis of this trial and will
initiate a second, confirmatory Phase III trial in 2012.
 Introduced NUMETA (emulsion for infusion) across Europe
as the first triple-chamber nutrition system designed
specifically for pediatric patients.
 Received FDA clearance for Baxters new, next-generation
automated peritoneal dialysis (APD) cycler and new
HomeChoice SmartCare software for use in current
HomeChoice APD machines.

 Began a clinical trial in the United States on a new
home hemodialysis system designed to deliver high-dose
hemodialysis in the home setting.
 Initiated a global Phase III clinical trial to evaluate the safety
and effectiveness of BAX 111, Baxters investigational
recombinant von Willebrand factor, for the treatment and
prevention of bleeding episodes in patients with severe
von Willebrand disease.
 Received FDA approval for a new prophylaxis indication
for ADVATE [Antihemophilic Factor (Recombinant)
Plasma/Albumin-Free Method] recombinant factor VIII
for hemophilia A patients.
 Administered therapy to the first patients in the clinical trial
of BAX 855, a longer-acting (PEGylated) factor VIII therapy
based on the companys full-length ADVATE molecule, as
announced in early 2012.
 Completed Phase I/II trials for BAX 817, a recombinant
factor VIIa therapy for patients with inhibitors to factor VIII
or factor IX; and completed enrollment in the Phase I/III
clinical trial for BAX 326, a recombinant factor IX therapy
for patients with hemophilia B.
 Published Phase II data demonstrating that injections of a
patients own CD34+ adult stem cells into targeted sites in
the heart have therapeutic benefits for patients with chronic
myocardial ischemia, leading to the initiation of a Phase III
clinical trial in early 2012.
Pursuing Aligned Business
Development Opportunities
Throughout 2011, we accelerated the pace of business
development initiatives that complement our current businesses,
enhance our product portfolio and leverage our core scientific
strengths. Baxter most recently:
 Acquired Baxa Corporation and its portfolio of pharmacy
technology products, which increase the efficiency and
safety of oral and intravenous (IV) dose preparation and
delivery. This acquisition broadens Baxters market leadership
in nutrition, expands our presence in the pharmacy and
leverages our global footprint.
 Acquired Prism Pharmaceuticals, Inc., and launched its
lead product, NEXTERONE (amiodarone HCl) in the United
States. NEXTERONE is the only ready-to-use premixed
IV container version of amiodarone, an antiarrhythmic
agent used for ventricular tachyarrhythmias (fast forms
of irregular heartbeat).
 Acquired Synovis Life Technologies, Inc., a leading provider
of biological and mechanical products for soft tissue repair.
This transaction, announced in 2011 and closed in early
2012, complements and expands the companys portfolio
of regenerative medicine and biosurgery products.
 Entered into a collaboration with Momenta Pharmaceuticals,
Inc., to develop and commercialize up to six follow-on
biologic products, also known as biosimilars. The
arrangement leverages Baxters expertise in biologics
manufacturing, augments our early-stage pipeline and
positions us to capitalize on this significant market opportunity.
 Established Baxter Ventures as a means to invest in
promising new companies developing therapies and
technologies in line with Baxters strategic direction.
This initiative is expected to fur ther enhance Baxters
early-stage pipeline and ultimately transform high-potential
concepts into commercial reality.
Baxter is accelerating its focus on acquisitions, partnerships
and related opportunities to advance the company. Our
diverse healthcare portfolio provides for a broad range of
aligned expansion prospects in current and complementary
markets; and our strong financial position affords us the
strength and flexibility to execute the right opportunities at
the right time.
Driving Access through Government Partnerships
With healthcare representing such a significant portion of
GDP spending worldwide, governments are faced with
an intensifying need to control costs, even as they pursue
parallel efforts to increase patient access to care. This
certainly poses challenges for the healthcare industry, but
it can also represent an opportunity for government and
industry to partner in new ways to achieve common goals of
better serving patients.
Baxter is increasingly focused on collaborating with government
partners to expand access to quality car e. For example,
Baxter entered into a licensing agreement with Takeda
Pharmaceutical Company Limited, executed in partnership
with the government of Japan, for exclusive rights to use
Baxters proprietary Vero cell culture and manufacturing
technology in Japan. The arrangement meets Japans
objective of protecting public health through pandemic
influenza preparedness, accessing Baxters highly efficient
technology while representing a new avenue for Baxter to
reach more patients.
Looking ahead, we see significant potential within a number of
our businesses to work directly with governments to address
critical healthcare concerns through unique arrangements
that improve health outcomes while controlling costs.

Sustainability Performance
Baxters definition of success goes beyond sound financial
performance; it reflects a commitment to responsible corporate
citizenship and a will to make a difference in communities
around the world.
Last year, Baxter responded swiftly to natural disasters in
Thailand, Japan and the United States with emergency
shipments of medical supplies, employee donations of time
and money, and further monetary support through The Baxter
International Foundation.
Our ability to mobilize in times of crisis is matched by our
focus on the long term. Through our Science@Work program,
Baxter supports math and science education in local schools
to prepare students for scientific careers. We pursue
initiatives across our locations to decrease net greenhouse
gas emissions, conserve water and save energy. We aid
worthy causes around the world through volunteerism,
product donations and financial support.
Through these efforts, Baxter is frequently recognized
for its sustainability leadership. Baxter has recently been:
 Named 2011 Medical Products Industry Leader on the
Dow Jones Sustainability Index, our 13th consecutive year
on the index and our 10th recognition as the industry leader.
 Named among the Global 100 Most Sustainable
Corporations in the World for the eighth consecutive
year by Corporate Knights.
 Ranked fourth overall in Newsweek magazines Green
Rankings of the largest publicly traded United States
companies, and first globally in the healthcare category
for the third consecutive year.
Creating Shareholder Value
In 2011, Baxter returned approximately $2.3 billion to
shareholders during the year, a 7 percent increase from
2010, through dividends totaling $709 million and share
repurchases of approximately $1.6 billion (or approximately
30 million shares). Also, given our consistently strong cash
generation, Baxter announced in November 2011 a dividend
rate increase of approximately 8 percent, to $1.34 per share,
on an annualized basis. Baxters annual dividend has more
than doubled since 2006. Over the last five years, the company
has maintained a disciplined capital allocation strategy resulting
in approximately $11.4 billion returned to shareholders in the
form of dividends and share repurchases.
Looking Forward
Baxter enters 2012 with strong momentum and confidence
in our sustained success. The company is responding to
todays considerable macroeconomic challenges with clearly
defined opportunities for growth that will serve us well as the
external landscape evolves. Our focus on medically necessary
products will continue to drive increasing demand for our core
portfolio on a global basis; and our robust R&D pipeline
holds the promise of new products and therapies that will
allow us to serve even more patients more effectively.
Innovation will remain the key to Baxters future. To this end,
we are well supported by the companys sound financial
position. In 2012, Baxters R&D investment is again expected to
reach record levels. Our significant cash generation will allow
Baxter to pursue select business development opportunities
that enhance and complement our existing businesses,
strengthen our market leadership, and further our mission of
saving and sustaining lives.
The companys 48,500 employees share my confidence and
commitment to a strong future. Together we will continue
working diligently toward expanding patient access to care,
achieving the highest standards of quality and safety, and
meeting the needs of customers in the most cost-effective
manner possible. I look forward to relating next years chapter
in this continuing story of progress and success.
Robert L. Parkinson, Jr.
Chairman and Chief Executive Officer
March 2, 2012