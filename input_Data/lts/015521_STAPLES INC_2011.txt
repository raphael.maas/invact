Fellow Shareholders,
At Staples, we take great pride in our consistent execution and our
ability to evolve and meet the changing needs of our customers.
Throughout our 25 year history weve succeeded by setting
aggressive goals, acquiring and retaining customers, and being
accountable for our performance. We took the same approach
in 2011 and made progress on many of our key initiatives, despite
the challenging economic environment.
In 2011, we grew sales to $25 billion, drove eight percent earnings
growth, and generated $1.2 billion in free cash flow. We returned
nearly $900 million to shareholders through dividends and share
ust under $400 million in capita expenditures to further differentiate our
ng financial results were solid, we continue to work hard and press our
any for long term growth.
In North American Delivery, we did a good job with customer acquisition and retention and were once
again recognized for our industry leading customer service. We made big investments to improve the
functionality, performance and usability of Staples.com. These have helped to provide a better customer
experience and build top-line momentum in this business. Over the past few years, weve invested in
marketing, pricing, associate training, and an expanded assortment in adjacent categories like facilities
and breakroom supplies. These are starting to pay off. During 2011, we achieved double-digit sales
growth in the facilities and breakroom category and ended the year with an $800 million business in
North American Delivery. Were in the fina innings of the Corporate Express integration. During 2011, we
completed our warehouse systems integration in North America and began the process of transitioning
al of our Contract customers over to our new and improved ordering platform, StaplesAdvantage.com.
In North American Retail, we pulled back on new store openings and increased our focus on improving
the productivity of our existing stores. We continued to make investments to become a leader in copy and
print, business technology and technology services. In copy and print, we broadened our assortment and
improved the quality of our offering. We also expanded our copy and print sales force to build on our low
single-digit market share in this category. Over the past two years weve remodeled the technology area
in more than half of our stores to improve how we sell and service technology products. This helped to
drive strong sales growth in categories like tablets and e-readers, as wel as our EasyTech business during
2011. We also made a big push into the mobile phone business to address an essential need of small
business customers, and we now have a mobile department in 500 stores across the United States.
In International Operations, we had a difficult year on both the top and bottom-line, and we did not make
progress against our profit improvement plans. While the Internationa team did a good job controlling
expenses, sales trends were much weaker than we anticipated. We were impacted by weak performance
in both our European Retai and Australian businesses, as well as headwinds from the European debt
crisis. Despite the difficult trends, we continued to drive solid top-line performance in the European
Contract business. Over the past two years, weve had great success with the launch of our mid-market
Contract offering in the United Kingdom, and during 2011 we introduced this concept in Germany. We
also continued to reduce losses in emerging markets like China, and remain on a path to profitability.
We expect the demand environment in Internationa Operations to remain soft throughout 2012. Well
continue to carefully manage expenses and focus on re-establishing our value proposition with small
business customers, as we work hard to gain share in these highly fragmented markets.

During 2011, we also celebrated our 25th anniversary. We took time to reflect on the history of our
company, but most importantly we spent time thinking about our future and how wel continue
to evolve with the changing needs of our customers. Over the past year weve offset weakness
in some of our core categories with robust growth in categories beyond office supplies, and were
focused on accelerating this trend going forward. Wel enhance our offering of technology products
and services as we strive to become the only company small businesses turn to for expert
technology advice and solutions. Wel provide customers who shop in our stores and on our
web sites with a more seamless Staples experience. Wel leverage our successes in North America
to drive improved performance across our International Operations. Wel simplify our operating
model and streamline our cost structure and ensure that Staples culture is one that continues
to encourage innovation, tenacity and frugality.
Well also remain committed to our sustainability strategy. Were proud of our ability to have a
beneficial impact on the environment while meeting the needs of our customers. Our priorities
include selling more sustainable products and services, offering easy recycling solutions,
eliminating operational waste, maximizing energy efficiency, and becoming a sustainability leader
in the global community.
We know that were strengthening our competitive position, gaining market share in our core
business, and building momentum in categories beyond office supplies. Over time, were confident
that well continue to win by staying true to our proven formula for success: take care of customers,
strive for consistent execution, and invest wisely to drive long-term growth.
In closing, I would like to thank our customers, suppliers, and stakeholders for their continued trust.
Id like to thank our Board of Directors for their strong leadership and support. In particular I want
to recognize Betsy Burton, who will retire from our Board in June 2012. Betsy was a strong leader
and set a great example for the rest of our Board. Her knowledge, expertise, and commitment to
our company made Betsy an invaluable resource during her 19 years of outstanding service. Im
also pleased that we are nominating Drew Faust to join our Board of Directors. Drew, president of
Harvard University, brings a unique perspective and experience leading a large and complex global
organization. Id also like to thank John Mahoney, who after 16 years as Staples chief financial
officer transitioned out of this role at the end of 2011. John has been an integral member of
my team, and I truly appreciate his commitment and support. Finally, Id like to recognize our
associates around the world for bringing to life our vision to be the worlds most trusted source
for office solutions.
Ron Sargent
Chairman of the Board and Chief Executive Officer
April 2012