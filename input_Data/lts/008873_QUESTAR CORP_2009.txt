Questar shareholders realized a total return of
29.1% in 2009, compared to a 26.5% return
for the S&P 500 index. If youve owned our shares
and reinvested the dividends for the past decade
youve realized a 584% total return on your investment  Questar ranked #30 in the S&P 500 index,
beating 94% of Americas top companies. By comparison, the S&P 500 index was down 9% over the
past decade.
We had to adapt to tough conditions in the U.S.
natural gas market in 2009. Questar consolidated
net income declined 42% from $683.8 million, or
$3.88 per diluted share in 2008, to $393.3 million, or $2.23 per diluted share in 2009. Excluding unrealized mark-to-market losses on natural
gas basis-only swaps and gains on non-core asset
sales, Questar net income was $495.2 million or
$2.81 per diluted share in 2009. But tough market
conditions demonstrated that strategy, disciplined
capital allocation, and a strong balance sheet matter in the energy business. In 2009, the average
sales price of natural gas in the regions where we
operate plunged 56% compared to 2008. But our
long-standing hedging policy paid off  the natural gas-price hedges we put in place in prior years
protected cash margins and returns in our exploration and production (E&P) business. Our average
realized natural gas price (including hedges) was
down only 13%. Thus, Questar consolidated 2009
EBITDA1
 was $1,644.4 million, down just 7%
from 2008, but still up 51% from three years ago. Questar E&P EBITDA declined 11% to $988 million while the rest of Questar  Wexpro, Gas
Management, Energy Trading, Questar Pipeline and
Questar Gas  generated EBITDA of $656 million
in 2009. These businesses generate net income and
cash flow that are much less sensitive to commodity prices. They support our solid investment-grade
credit ratings and pay our dividend.
sQuestar Market Resources, our natural
gas-focused E&P and midstream field-services
subsidiary, generated net income of $293.5 million
in 2009, down 50% from $585.5 million in 2008,
as unrealized mark-to-market losses on basis-only
swaps and lower prices resulted in a 67% decline
in Questar E&P net income. Market Resources
2009 EBITDA of $1,349 million was down only 8%
from record 2008 EBITDA. All Market Resources
business units  Questar E&P, Wexpro, and Gas
Management  executed well in 2009.
sQuestar E&P grew natural gas and oil-equivalent
production 11% to 189.5 billion cubic feet equivalent (Bcfe) in 2009. We also grew proved natural
gas and oil-equivalent reserves 24% to 2.75 trillion
cubic feet equivalent (Tcfe) at year-end  while reducing drill-bit capital expenditures by about $290
million compared to 2008. Questar E&P replaced
379% of its production in 2009, despite 271 Bcfe
of negative reserve revisions due to lower commodity prices. Excluding price-related revisions, Questar E&P replaced 537% of its production. Whats
more, the strategy we put in place a few years ago
to diversify from our traditional Rockies E&P focus
is paying off. Questar E&P grew Midcontinent
production 29% in 2009, driven by growth from
our Haynesville Shale, Anadarko Woodford Cana
Shale, and Granite Wash plays. In 2009, 46% of
Questar E&P production came from the Midcontinent  and that share could grow over the next
few years. Were allocating capital to our highermargin plays, in particular the Haynesville Shale
and the Pinedale Anticline. Were also increasing
our exposure to a world-class oil play  the North
Dakota Bakken  and to promising new tight gas
and shale plays in the Midcontinent.
Haynesville Shale  The Haynesville Shale in
northwest Louisiana is one of the most economic
natural gas resource plays in the U.S. today  and
Questar is right in the thick of the action. We now
have 46,000 net acres in the core of this play  up
nearly 50% from a year ago. At year-end Questar
E&P reported proved Haynesville Shale reserves of
592 Bcfe. Haynesville will remain a major growth
driver over the next five years and beyond  we
have about 1,200 locations yet-to-drill assuming
development on 80-acre density.
Pinedale Anticline  The Pinedale Anticline in
southwest Wyoming is the largest natural gas field
in the Rocky Mountain region  and Questars
most valuable asset. At year-end 2009 Questar E&P
reported proved reserves of 1.3 Tcfe at Pinedale, up
12% from year-end 2008. As with our Haynesville
Shale play, we have good visibility on future growth
at Pinedale  Questar E&P and Wexpro together
have up to 1,400 locations yet-to-drill on a mix of 5
and 10-acre density.
Woodford Shale  Through year-end 2009 Questar
E&P had a working interest in 53 producing wells
on our 26,000 net-acre leasehold in the core of the
Woodford Cana Shale play in the Anadarko Basin
in western Oklahoma. This emerging shale play
also offers visible growth for Questar E&P  we
have an average working interest of about 18.4%
in over 1,600 potential new locations. Importantly,
the development economics are enhanced by the
potential for significant condensate and natural gas
liquids (NGL) production.
Granite Wash / Atoka  The Granite Wash / Atoka
play in the Texas Panhandle and western Oklahoma
emerged as one of the hottest new plays in the U.S.
in 2009  and again, Questar E&P is well positioned with over 25,000 net acres and more than a
decade of operating experience in the region. Play
economics are also enhanced by potentially significant NGL production.
Bakken oil  In 2009 Questar E&P confirmed with
the drill bit that the Bakken oil fairway extends onto
a significant portion of our 80,000 net-acre leasehold. In December we turned our third Questar
E&P-operated horizontal well to sales with a peak
24-hour rate of just over 1,400 barrels of oil equivalent per day. This key delineation well recovered
over 27,000 barrels of oil equivalent in the first 30
days on production. We now have a working interest in 27 producing wells in the prolific Bakken oil
playHaynesville Shale, Pinedale Anticline, Woodford
Shale, Granite Wash, and Bakken  plus over 1
million net acres of Rockies leasehold  provide
a solid platform for profitable growth in the years
ahead.
sWexpro  Wexpro, the second E&P business in
our Market Resources group, grew net income 9%
to a record $80.7 million in 2009 while generating
EBITDA of $183.7 million. Under the 1981 Wexpro
Agreement, Wexpro earns a contract-based 19-20%
after-tax return on its net capital employed in the
development of Rockies natural gas reserves on
behalf of our utility, Questar Gas. Wexpro produced
a record 48.2 Bcf of cost-of-service natural gas for
Questar Gas in 2009. Wexpro also replaced its
production and reported record year-end cost-ofservice reserves of 676.3 Bcfe  89% higher than a
decade ago.
sQuestar Gas Management  Our midstream
gathering and processing business reported 2009
net income of $69.4 million, down 15% from 2008
as processing margins declined from record levels
in 2008. Gas Management generated EBITDA of
$162.6 million in 2009.
sQuestar Pipeline posted record net income of
$58.2 million and EBITDA of $162.8 million in
2009. Our FERC-regulated interstate pipeline business has more than doubled earnings from five
years ago. We added compression to expand our
Overthrust Pipeline, which runs through the heart
of the Green River Basin, the largest natural gasproducing basin in the Rockies. Our pipeline team
also secured long-term contracts to support another
major expansion of Overthrust, which we expect to
complete in early 2011.
sQuestar Gas also reported record 2009 net income and EBITDA of $41.6 million and $130.7 million, respectively. Our natural gas utility earned its
allowed return on equity for the fifth year in a row.
Our retail natural gas rates are the lowest in the
continental U.S. and our utility team remains one of
the most productive in the country  Questar Gas
consistently ranks among the top U.S. gas utilities
in lowest O&M cost per customer. In addition, our
customer satisfaction index reached an all-time
high last year. We also partnered with the state
of Utah to expand the distribution of compressed
natural gas (CNG) for cars, trucks and buses. You
can fill a car or truck at one of Questar Gass 26
CNG stations for less than $1 per gallon of gasoline-equivalent today. Greater use of natural gas in
cars, trucks and buses is not just an economically
attractive alternative to gasoline and diesel fuel. It
can significantly improve air quality in urban areas.
With support from
forward-thinking
state officials in
Utah, our utility has created
the blueprint
for greater use of
American-made
natural gas in vehicles throughout
this country.
Overall, Questar
Corporations consolidated return
on assets (ROA)
 earnings before
interest and income taxes (EBIT)
divided by average total assets 
was 8.5% in 2009
 down sharply
from 16.2% in 2008 as unrealized mark-to-market
losses on basis-only swaps and lower natural gas
and oil prices took a toll on Questar E&P returns.
Taking it to the next level  Questars
evolution continues
Weve got terrific people in each of our businesses  and they continue delivering for
shareholders. As noted above, if youve owned
our shares over the past decade youve realized
a 584% total return on your investment  beating the S&P 500 index by 593%. A decade ago
we vowed to put the pedal to the metal in our
E&P business. Our talented Questar E&P team
has delivered  natural gas and oil-equivalent
production in 2009 was nearly 2 times what it
was a decade ago. Questar E&P proved reserves at
year-end 2009 were more than 4 times proved
reserves at the start of the decade. Wexpro grew g
of 6.20 (scale 1-7).
investment base  and therefore, net income 
nearly fourfold over the decade. Over the past ten
years, Wexpro has produced 408 Bcf of natural gas
for Questar Gas and yet cost-of-service reserves at
the end of the decade were still 89% higher than
at the start of the decade. Wexpro cost-of-service
gas has saved Questar Gas customers $871 million
compared to market prices for natural gas over the
past ten years. Questar Gas Management grew net
income eighteen-fold as gathering volumes tripled
and processing volumes grew from virtually zero
to 210 million MMBtu over the decade. Our two
regulated businesses also made key contributions.
Questar Pipeline net income grew from a loss a
decade ago to a record $58.2 million in 2009. Our
Pipeline team today transports three times as much
natural gas as it did ten years ago. Questar Gas
more than doubled its net income over the decade
 our utility team connected over 212,000 new
homes and businesses over the past ten years. Our
customer satisfaction scores reached an all-time
high in 2009.
Looking ahead
So what lies ahead for Questar? The U.S. natural
gas market  by far the largest in the world
 is set to resume growth over the next decade,
underpinned by robust U.S. natural gas supplies
and environmental policies that favor greater use of
natural gas for electricity generation, energy-intensive industry, petrochemical feedstock, and direct
use in homes and businesses. Were confident that
all Questar business units are well positioned to
capitalize on these strong fundamentals. In late
2009 the Questar Board endorsed our five-year
plan. Here are the highlights:
sOur mission has not changed: we find, produce
and deliver natural gas  clean energy that makes
modern life possible.
sNotwithstanding near-term weakness in U.S. natural gas markets, we believe that our best risk-adjusted returns on capital will result from investment in
our E&P businesses  Questar E&P and Wexpro.
Well allocate capital accordingly, mindful that we
must manage commodity-price risk and our E&P
cost structure to ensure acceptable margins, cash
flow and returns in
sWe forecast that Questar E&P production will
grow without acquisitions at a compound annual
rate of 12-15% per year over the next five years.
Weve got confidence in this forecast  its based
on identified drilling inventory in our core Haynesville Shale, Pinedale Anticline, Granite Wash,
Woodford Cana Shale, and Bakken plays. Whats
more, we can accelerate Questar E&P growth by
increasing capital and adding rigs, if market conditions  and prospective returns on capital  support doing so.
sWe also have good visibility on Wexpro growth.
Over the next five years we intend to invest $650-
700 million in Wexpro  about 35-40% of that at
Pinedale. Wexpro net income and EBITDA could
grow by nearly 50% over the next five years.
sWe anticipate significant growth in our midstream
field-services business. Gas Management is building a new cryogenic processing plant to expand our
existing Blacks Fork hub in southwest Wyoming.
This project could generate $60 million per year
EBITDA beginning in 2012. Were also expanding
our Uinta Basin hub  in late 2010 well commission a new cryogenic processing plant adjacent
to our existing Stagecoach plant. In addition, Gas
Management recently entered NW Louisiana to
support Questar E&Ps Haynesville growth. As
long-term investors have witnessed, weve built a
substantial midstream franchise in the Rockies on a
phased entry strategy supported by dedicated production from Questar E&P. Were taking a similar
approach in NW Louisiana.
sWere allocating capital to Questar Pipeline to
expand Overthrust Pipeline, which transports gas
through the heart of the Green River basin, by far
the largest producing basin in the Rockies. We
believe U.S. natural gas markets will demand more
gas from this resource-rich region  and Questar Pipeline is well positioned for future Rockies
growth.
sFinally, were forecasting solid growth from our
utility, Questar Gas. Well allocate capital to connect new customers, replace and expand major
feeder lines, and grow the regional market for CNG
in cars, trucks and buses. If we execute, Questar
Gas has the potential to deliver high single-digit net
income growth over the next five years.
The stage is set
So the stage is set. We believe were in the right
business  U.S. natural gas. We believe we
have the right assets. I know that we have the right
people. Its time to take Questar to the next level.
We owe it to our shareholders, we owe it to our
customers, and we owe it to the dedicated, hardworking employees at all levels of our company.
On behalf of the people of Questar and our board
of directors, thank you for your investment in our
company. Please know that we come to work every
day mindful of the trust you implicitly place in us
when you buy and hold our shares.
Sincerely,
Keith O. Rattie
Chairman, President and CEO