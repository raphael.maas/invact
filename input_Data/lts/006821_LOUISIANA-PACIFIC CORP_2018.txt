TO OUR SHAREHOLDERS:
2018 was a year of continued transformation for LP, marked by significant progress on our strategic shift to a
specialty products and solutions company. We are transforming to create a stronger LP, one that is able to produce
more consistent and sustainable results, greater growth opportunities, a stronger margin profile and increased
shareholder value.
This transformation is already delivering results for shareholders: Since the second quarter of 2018, LP has
committed to return over $1 billion to our shareholders. We�re proud of that record, and we hope to continue that
success in the years ahead.
To drive this transformation internally, in 2018, we instituted changes in our corporate organization structure and
culture that are already driving results. We shifted the organization from a matrix management structure to a line
management structure to ensure each business segment has complete accountability for all facets of the segment
financial results and strategic execution. Culturally, from our manufacturing operations to our sales organization
and corporate offices, we�re clearly defining accountability and our employees are thinking and acting like owners,
demonstrating an ownership mentality and focus on value creation.
This change in mindset has proven to be significant, spurring our people and our company to reach new levels of
accountability and results-oriented performance.
Operationally, we�ve begun major initiatives in three key areas to remove around $75 million of costs over the next
several years:
� Through our OEE efforts, we�re increasing the efficiency of our mills by improving productivity, run-time and quality
� Applying best practices to our supply chain, including procurement, logistics and working capital across $1.4
billion of addressable spend
� Optimizing our infrastructure costs by shifting from a matrix to a line management organization and beginning the
consolidation of our human resources, IT, payroll and accounting satellite offices into the corporate headquarters
Our organizational realignment was aggressive and our people took the challenge seriously, thanks to strong
leadership in all areas of the company. We expect to continue to see the benefits of our realignment throughout
2019 and beyond.
While we operate in a cyclical industry, we are continuously seeking ways to best position LP for growth and
success across market cycles. By seeking bold solutions, LP is leading that charge with an innovative portfolio of
LP� SmartSide� siding and Structual Solutions OSB as well as a range of innovative, high-performance products.
This past year, we also invested $45 million in Entekra, a design, engineering and manufacturing company that is
unique to the U.S. market, providing a fully integrated off-site solution (FIOSS�) for both residential and commercial
construction. This investment aligns perfectly with where we�re heading as a company and demonstrates that we
are looking for building solutions not only through innovative products and adjacent acquisitions, but also through
new processes and methods of building.
We�re proud of the financial and operational results we delivered in 2018. As we look ahead to 2019, we are focused on
building on our strong momentum and continuing to deliver increasing value for our customers and our shareholders.
As always, we sincerely thank our shareholders for their confidence and support.
BRAD SOUTHERN
Chief Executive Officer
E. GARY COOK
Chairman of the Board