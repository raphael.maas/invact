APRIL 10, 2014




                                                                                          LIAM E. McGEE
                                                                                          CHAIRMAN, PRESIDENT
                                                                                          AND CHIEF EXECUTIVE OFFICER



Dear Shareholders:

2013 marked a milestone in The Hartford's turnaround and transformation. Thanks
to the hard work of nearly 19,000 employees and their dedication to serving customers
and partners, The Hartford's core earnings grew 26 percent to $1.7 billion, or
                                                               1




$3.55 per diluted share. Net income was $176 million, or $0.34 per diluted share.

The Hartford's strategy for shareholder value creation remains consistent: 1) profitably grow the company's
focused portfolio of businesses, 2) further reduce the size and risk of the legacy variable annuity liabilities, and
3) transform the company to deliver more customer value while increasing operating effectiveness and efficiency.
Executing these elements will continue to build financial strength and flexibility, with the goal of generating
greater value for shareholders.


OUR STRATEGY IS CONSISTENT


                                                                     CONTINUE                  GREATER FINANCIAL
   DRIVE PROFITABLE               FURTHER REDUCE
   BUSINESS GROWTH         +     ANNUITY EXPOSURE          +       TO TRANSFORM           =      STRENGTH AND
                                                                   THE HARTFORD                    FLEXIBILITY




                                      SHAREHOLDER VALUE CREATION



The Hartford's strategic execution drove 2013 core earnings growth of 41 percent in Property & Casualty, Group
Benefits, and Mutual Funds. The increases in Property & Casualty and Group Benefits resulted from disciplined
pricing and underwriting, expanding margins, and lighter catastrophe-related losses. In Mutual Funds, earnings
growth was driven by higher assets under management.

As a result of our actions and the benefit of global market improvements, we significantly reduced the size and risk
of the legacy variable annuity (VA) blocks in the runoff business, known as Talcott Resolution. The number of
VA contracts declined 26 percent in Japan and 14 percent in the U.S., reflecting a permanent reduction in risk. In
addition, we sold our small U.K. VA book for approximately $285 million to a subsidiary of Berkshire Hathaway.
LETTER TO SHAREHOLDERS | PAGE 2




During 2013, The Hartford transformed into a more effective and efficient company--one that works better for
customers, partners, and employees. The company has been simplifying and improving processes and investing
in capabilities so that we can deliver exceptional customer value and get innovative and profitable products to
market faster. These improvements are also enabling employees to respond more quickly to the needs of customers
and partners.

Through the disciplined execution of its strategy, The Hartford has achieved the financial strength and flexibility to
support and invest in its businesses for profitable growth as well as pay down debt and buy back equity. The company
reduced its debt by $820 million and returned a total of $856 million to shareholders in 2013, $633 million in equity
repurchases and $223 million in dividends. Our total shareholder return for the year was 64 percent, compared with
32 percent for the S&P Index and 47 percent for the S&P Insurance Composite Index.

Further affirming its strong financial position and improved capital-generation outlook, The Hartford announced
in February 2014 a balanced $2.656 billion capital management plan for 2014 and 2015, of which $2 billion is for
equity repurchases and $656 million for maturing debt repayment.

The company's $79 billion investment portfolio, which backs The Hartford's insurance liabilities and surplus, is
healthy and well-positioned to deal with economic uncertainty. As of December 31, 2013, the portfolio has an "A"
weighted average credit quality rating, and our asset/liability management is defensively positioned for resumption
of a modest rise in interest rates over 2014.

I am proud of The Hartford's employees, and thank them for their collective work in turning around the company
and delivering an outstanding year. They remain focused on executing the strategy while helping customers prepare
for the unexpected, protect what is uniquely important, and prevail when the unexpected happens. We are energized
by our accomplishments, but recognize that there is much work ahead to become a company that consistently
sustains high levels of performance.

To make 2014 another year of significant achievement, we are maintaining our focus on execution, profitable growth,
and efficiency, and coming to work each day determined to win in the marketplace. The rest of this letter will describe
how The Hartford plans to compete more effectively and seize new opportunities for profitable growth.


The Hartford In 2014 and Beyond
More than a year ago, we developed a long-term vision for The Hartford that was informed by the views of hundreds
of employees from all levels and parts of the business: To be an exceptional company celebrated for financial
performance, character, and customer value. This vision is our north star, guiding decisions and actions and
helping us act with decisiveness and a sense of urgency.
                                                                                           PAGE 3 | LETTER TO SHAREHOLDERS




                                                  THE HARTFORD'S VISION




In pursuit of this vision, the company's businesses are well positioned to improve their profitability. In 2014, we
expect earnings growth in Property & Casualty, Group Benefits, and Mutual Funds to offset the anticipated decline
in Talcott Resolution's earnings as the size and risk of the variable annuities business declines. We are confident that
The Hartford can continue to generate shareholder value by executing its strategy and improving the operating
effectiveness of its businesses.


MEASURING SUCCESS: ENTERPRISE OBJECTIVES



           FINANCIAL                                 CUSTOMER                                 EMPLOYEE
         PERFORMANCE                                   VALUE                                 ENGAGEMENT




                                            TOP-QUARTILE TOTAL
                                           SHAREHOLDER RETURNS



To measure success in executing the strategy, The Hartford has three enterprise objectives:

    Achieving top-quartile financial performance by growing book value per share and increasing return on equity;
                                                                            
    Creating exceptional customer value, as measured by Net Promoter Score and related metrics; and
    Maintaining top-quartile employee engagement, as measured by Kenexa, a global leader in conducting
     surveys at more than half of the Fortune 500.
LETTER TO SHAREHOLDERS | PAGE 4




We selected these objectives because they will help us measure the effectiveness of our strategic execution. These
measures will confirm how well the company is driving profitable growth, investing shareholders' capital, meeting
customers' needs, and maintaining a highly engaged workforce.


Investing in Our Future
To be a top-quartile performer, we recognize the importance of building for the future, so we are investing in all
aspects of the business, including people, solutions, infrastructure, and distribution. In 2013, The Hartford launched
a plan to invest more than $1.5 billion through 2016 in new systems, applications, and business platforms to enable
the company to bring products to market faster at lower costs, sharpen pricing and underwriting skills, and modernize
foundational technology such as digital capabilities. Our aim is to position The Hartford to anticipate and respond to
market trends and customer needs. We manage these investments with rigor, including regular reviews by members
of the Executive Leadership Team and the Board of Directors.

The Hartford invests in creating a high-performance culture. The company has built a strong and deep leadership
bench as well as programs for developing future leaders, woven the importance of diversity and inclusiveness into
its culture, and achieved top-quartile levels of employee engagement. We have made the company's vision, strategy,
enterprise objectives, and valued behaviors clear to all employees. They know what success means for our company,
along with the best ways to achieve it, by delivering outcomes, operating as a team player, building strong partnerships,
and striving for excellence.

Complementing our bias for developing and promoting from within, we have attracted leading talent with deep
knowledge to fill key roles in product development, underwriting, sales, marketing, research, and pricing analytics.
Our new colleagues have blended with their teams to build market momentum with customers and partners.

The Hartford has competitive advantages in insurance underwriting, claims management, and distribution--core
competencies developed over decades. The company's underwriting discipline manages exposure to loss through
favorable risk selection and diversification.

The mission of The Hartford's 5,500 dedicated claims professionals is to care for customers and earn their trust and
confidence with every interaction, especially at time of loss. Handling more than seven million in-bound calls a year,
they focus on delivering exceptional, compassionate customer value and ensuring quality outcomes that support the
company's financial performance. The Claims organization developed, patented, and implemented industry-leading
methods for tracking and improving customer loyalty.
                                                                                          PAGE 5 | LETTER TO SHAREHOLDERS




Our leading national distribution network of independent agents and brokers is a competitive advantage. We work
closely with distribution partners, learning their business needs, engaging in consultative conversations, and adapting
our business model to help them succeed. Doing so enables the company to profitably grow its partner relationships.
Our agents and brokers tell us they are excited about The Hartford's marketplace momentum, and we are grateful for
their business and value their partnership.


Property & Casualty Commercial
Over the years, The Hartford has built a competitive advantage in providing insurance and income protection for
small- and mid-sized businesses as well as mature individual consumers. Roughly half of the company's Property &
Casualty Commercial written premium is from Small Commercial, and about one-third from Middle Market. While
markets are competitive, we excel in these attractive segments by offering deep knowledge, a complete and balanced
product portfolio, strong agency relationships, and a claims organization renowned for its level of customer care.

Several initiatives are in the works to position our business portfolio for greater success. In Property & Casualty
Commercial, which consists of three businesses (Small Commercial, Middle Market, and Specialty Casualty), we
are building more efficient underwriting platforms and creating easy-to-use technology applications for distributors,
customers, and employees.

This year, we will introduce a new claims management platform for the Property & Casualty businesses, which
will improve claims handling and the customer experience, as well as lower internal operating expenses.

In Small Commercial, which serves the protection needs of more than one million small businesses nationally,
The Hartford has superior products and pricing capabilities. This year, we will roll out a new product for
commercial auto and complete our new business quoting and issuance platform, ICON. One of the industry's
most advanced new business submission tools, ICON enables agents to deliver a bindable quote in less than
five minutes, increasing their productivity. Concurrently, we are working to advance our online service capabilities,
expand distribution, and deliver new value to agency partners.

The Hartford has emerged as one of the leading and most trusted insurance brands for small businesses. They know
we are passionate about helping them succeed. Each year the company conducts one of the most comprehensive
studies of small-business owners nationwide to deepen our understanding of how to help them overcome obstacles
and thrive, and to focus national attention on the need to create an environment that better supports their growth.
Today's entrepreneurs and small-business owners are a powerful force in the country, economically and behaviorally.
They carry on the tradition of the driven, confident men and women who built America. The Hartford celebrates
these optimists who are strengthening the economy by creating jobs, knowledge, products, and services.
LETTER TO SHAREHOLDERS | PAGE 6




The company's Middle Market business provides comprehensive, multiline commercial insurance coverage for
midsize and larger businesses. We balance our historical strength in workers' compensation with growing capabilities
in property and general liability and a broadened portfolio. The company is strengthening Middle Market's line-of-
business capabilities in segments such as marine, construction, technology, and life sciences, and continues to enhance
underwriting fundamentals and drive growth with our most valued distribution partners. We see new market
opportunities in industries such as leisure/recreation and social services, and have significant headroom for growth
with many of the country's largest agents now that our product breadth and underwriting appetite have expanded.

In Group Benefits, where The Hartford is a leader in the employee benefits market, we expect the rebound in
profitability during 2013 to continue, enabled by disciplined pricing across the entire book of business as well as
improving disability loss ratios. Healthcare reform and other forces are causing a shift to voluntary and defined
contribution funding, creating more employee-initiated buying. In response, The Hartford is developing new
products to increase its market penetration, and redesigning its platform to meet new demands, including greater
digital engagement with consumers. Group Benefits also sees opportunities to leverage its product and service
expertise, distribution strength, and trusted brand to deepen penetration in the mid- to small-employer segments,
where The Hartford excels.


Consumer Markets
Consumer Markets provides personal auto, property, and umbrella insurance. Our strategy in Consumer Markets
is to deliver a distinct, integrated customer experience, while achieving a lower-cost operating structure, improving
pricing accuracy, and broadening the target market to adjacent segments.

The core of Consumer Markets is The Hartford's exclusive partnership with AARP, one of the country's largest affinity
groups with approximately 38 million members. We provide personal property and casualty insurance that serves and
protects members on the road and in their homes. The partnership has produced strong results for nearly three decades,
and we recently extended it through January 1, 2023. This agreement provides The Hartford with a competitive
advantage given the number of baby boomers over age 50, many of whom are or will become AARP members.

We are building momentum in our principal distribution channels: AARP Direct, AARP Agency, and Non-member
Agency. In these channels our focus is to balance profit and growth by pricing ahead of expected loss trends, without
compromising retention. AARP Agency complements direct sales to consumers, and we are excited by the growing
momentum of this channel since the majority of AARP members say they prefer to buy insurance from an agent.

The Hartford has a new auto class plan designed to more accurately match price to risk, while increasing the company's
competitiveness among 50- to 59-year-old AARP-age segments. TrueLane, the company's vehicle telematics program,
uses technology to measure driver behavior and fuel efficiency, and is helping to attract and retain better auto risks.
                                                                                         PAGE 7 | LETTER TO SHAREHOLDERS




In 2014, we are investing in digital capabilities across all Consumer Markets channels as well as taking steps to enhance
our other agency channel offering. These investments will extend customer and agent self-service capabilities, with a
growing focus on mobile technology.


Hartford Funds
Hartford Funds offers more than 50 mutual funds in a variety of styles and asset classes sub-advised by Wellington
Management, one of the world's premier asset managers. This partnership has delivered strong fund performance
across multiple time periods, as evidenced by 42 percent, 65 percent, and 74 percent of our funds beating their
Morningstar peers on a 1-, 3-, and 5-year basis, respectively. In addition, we were one of only four fund complexes to
be recognized for two consecutive years by Barron's/Lipper "Best Fund Families of 2013."

This strong performance, coupled with a focused distribution team, drove a 28 percent year-over-year increase in sales.
Although net flows in 2013 were negative largely because of two significant midyear redemptions, they improved as we
finished the year. Fourth quarter net flows were nearly breakeven and led to the best quarter in the past three years.

While Hartford Funds has nearly $100 billion of assets under management, its primary focus is to grow the retail
funds, which at the end of 2013 had assets under management of approximately $71 billion2.

With our attention to product oversight and development, consistent investment performance, and distribution
excellence, we believe Hartford Funds has significant growth opportunities in 2014 and beyond.


Talcott Resolution
Further reducing risk in Talcott Resolution is a key element of our strategy to create shareholder value. In early 2013,
we expanded hedging of our Japan VA book of business to significantly reduce exposure to equity and currency risk.
This enabled Talcott Resolution to remain capital self-sufficient and not require additional capital support from other
parts of the company.

During 2014, we expect our VA policy count to decline 31 percent in Japan and 12 percent in the U.S. Talcott
Resolution's 2014 core earnings are expected to decline from 2013 levels as a result of lower fees from higher
surrenders. However, we expect this decline to be offset by core earnings growth from the rest of the company's
businesses. The Hartford continues to consider other alternatives for reducing the size and risk of the legacy book
of business within Talcott Resolution, such as transactions with third parties and additional policyholder initiatives.
We will evaluate any potential transaction by balancing the sales price and underlying economics with the capital that
would be released.
LETTER TO SHAREHOLDERS | PAGE 8




Increasing Effectiveness and Efficiency
Having sharpened the company's focus and aligned operations with customers' needs, we have opportunities to
run the company more effectively and efficiently. During 2013, The Hartford achieved more than 90 percent of its
$850 million expense reduction target, which included costs related to the sales of the life businesses, and will
eliminate the remaining expenses in 2014. Adjusted for the sale of Catalyst 360, our member contact center for
health insurance products offered through AARP, we ended 2013 with annual run rate expenses of $3.62 billion.
Our efforts to simplify and improve processes should enable us to further reduce the run rate of The Hartford's
controllable expense base by approximately $200 million by the end of 2016.


Economic & Industry Outlook
We see modest growth overall for the global economy in 2014. In the U.S., our 2014 outlook calls for GDP growth
of 2.7 percent and inflation to remain at 2.0 percent. Rising consumption and more predictable government spending
and policy initiatives in 2014 are expected to provide a solid foundation for growth. Yet we believe growth could be
moderated by the effects of higher interest rates and slower investment spending.

The Hartford has the strategic clarity and execution capabilities to deal with a more competitive market in 2014.
We are focused on pricing and underwriting discipline through improved analytics, becoming more efficient, and
generating profitable growth in our businesses.


Regulatory Trends
We pay close attention to regulatory trends, and support reauthorization of the federal terrorism risk insurance
backstop, created by the Terrorism Risk Insurance Act (TRIA), before it expires at the end of 2014. Since its
enactment in 2002, TRIA has been a successful and important economic tool. Terrorism insurance is available and
affordable throughout the U.S., eliminating economic uncertainty and keeping the economy moving as the long
recovery gathers momentum. TRIA works because it is an effective partnership between the private sector and the
federal government.

Working with the American Insurance Association (AIA), where I began a one-year term as Chairman of the Board
in March, The Hartford is educating Congress on the need to swiftly reauthorize the program. AIA is the leading
property-casualty insurance trade association, which represents more than 300 insurers across the country.


Our Vision of an Exceptional Company
Through the collective work of employees, The Hartford is advancing toward its vision: To be an exceptional company
celebrated for financial performance, character, and customer value.
                                                                                         PAGE 9 | LETTER TO SHAREHOLDERS




Financial Performance
We strive to become celebrated for financial performance, and define our goal as top-quartile total shareholder return.
The Hartford recognizes the substantial work required to achieve this level of performance. But having managed
through adversity and turned around the company, we have the passion to win coupled with proven execution skills,
and are determined to reach our destination.

Character
We also want to be celebrated for our character. For The Hartford, character encompasses several attributes, among
them: sustaining our strong commitment to ethical conduct and unwavering integrity; fostering a diverse and inclusive
culture; caring for customers, partners, and one another; engaging with and serving the communities in which we
work and live; and protecting the environment.

The Hartford is proud to be named one of the "World's Most Ethical Companies,"
                                                                                                                           TM
for the sixth time, by the Ethisphere Institute. This recognition, in March 2014,
acknowledges the company's dedication to conducting business with high ethical                                             

standards that empower employees to do the right thing. We are one of only two
property and casualty insurance companies to receive the recognition this year.

To attract and retain the best talent, we are determined to be a community that welcomes and engages all people.
The leadership team and I want The Hartford to be a place where teammates can succeed because of their differences,
not in spite of them. We are honored to be recognized as a "Best Place to Work for Lesbian, Gay, Bisexual and
Transgender (LGBT) Equality" by the Human Rights Campaign (HRC), an accolade received for scoring
100 percent on the HRC's 2014 Corporate Equality Index.

In late 2010, The Hartford announced a $7 million, five-year investment in Asylum Hill, a Hartford, Conn.,
neighborhood that has been home to the company's headquarters for more than 90 years. By the end of 2014, we
expect to have invested a total of $6 million in this initiative. Exemplifying the company's culture of service and
community engagement, Hartford teammates across the country once again gave generously of their time, talent, and
money. In 2013, they volunteered 31,000 hours to serve local community partners in revitalizing neighborhoods,
educating children, and developing outreach programs. In addition, teammates donated, along with The Hartford's
corporate match, $2 million to benefit agencies supported by the United Way.

In recognition of The Hartford's commitment to environmental stewardship, CDP (formerly Carbon Disclosure
Project) placed the company on the CDP S&P 500 Carbon Performance Leadership Index in 2013, the sixth
consecutive year the company has received CDP leadership. Our company is also listed on the Dow Jones
Sustainability Index, which tracks the stock performance of the world's leading companies based on economic,
environmental, and social criteria.
LETTER TO SHAREHOLDERS | PAGE 10




Customer Value
Our vision also specifies being celebrated for customer value. For The Hartford, that means keeping our promises
to customers, partners, and colleagues, as we have strived to do for more than 200 years. It means anticipating their
needs, developing innovative solutions that make a difference in their lives and businesses, and building great
experiences that inspire customers to promote the company to friends and colleagues. The entire enterprise is
working toward the common goal of creating and delivering value to customers. Every day we ask: what can we do
to make their lives better? We recognize that staying ahead of rising customer expectations requires unrelenting
attention to achieving ever higher levels of performance.


Board of Directors
Over the past 12 months, The Hartford has elected two new directors to its Board: Virginia P. Ruesterholz, who
retired from Verizon Communications, where she served as executive vice president, responsible for overseeing key
strategic initiatives; and Julie G. Richardson, who serves as a senior advisor at Providence Equity Partners, LLC.

I want to express my gratitude to the company's active and fully engaged directors, who work closely with me
and the management team in advancing The Hartford's creation of shareholder value. Their wisdom, expertise,
and diverse perspectives are invaluable as The Hartford continues to transform and move closer to the destination
of top-quartile performance.

I offer a special thank you to Senator Paul G. Kirk, Jr., who is retiring from The Hartford's Board in May, after 19 years
of outstanding service to the company. Paul's tenure spanned a formative and exciting time in the company's history,
from our reemergence as an independent entity in 1995 through The Hartford's successful execution of its strategic
plan and repositioning for future growth and long-term shareholder value creation. The Board has the highest regard
for Paul and will miss his wise counsel and passion for The Hartford.


The Next Level of Achievement
In October 2009, when I was appointed The Hartford's Chairman and CEO, the company's future was unclear.
Since then, our top priority has been to restore strength and stability, improve risk management, clarify the company's
strategic direction, and deliver increased shareholder value.

Today, as a result of the hard work and dedication of teammates, The Hartford has refocused on its core competencies
in underwriting, claims, and distribution, built one of the strongest and most collaborative management teams in
financial services, and strengthened such foundational capabilities as risk management, talent and culture, technology
and operations.
                                                                                        PAGE 11 | LETTER TO SHAREHOLDERS




2013 was a year in which we delivered profitable business growth, improved the company's risk profile, and delivered
superior results for shareholders. We have more work to do, and everyone at The Hartford is determined to accelerate
our transformation.

The consistent progress of the past few years has made us more confident in our ability to execute and determined to
pick up the pace. We move into 2014 with fast-improving businesses, financial strength and flexibility, and a commitment
to invest to generate profitable business growth and top-quartile total shareholder returns.

I thank my Hartford teammates for the work they have done to turn around and transform the company, and am
grateful to shareholders for their patience and support. We are optimistic about the future, and determined to continue
successfully executing our strategy and fulfilling The Hartford's vision.

Sincerely,




Liam E. McGee
Chairman, President
and Chief Executive Officer
