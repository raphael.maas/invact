                                    F R A N K L I N R E S O U R C E S , I N C . A N N UA L R E P O R T 2 0 1 4




                                                                      Gregory E. Johnson
                                                                      Chairman of the Board
                                                                      Chief Executive Officer
                                                                      President
                                                                      Franklin Resources, Inc.




Dear Fellow Stockholders,                        with clients, managing our diverse o ering           Operating revenue of $8.5 billion increased
                                                 of investment products, or providing                 6% from $8.0 billion in the prior year--an
We are pleased to share Franklin Templeton's
                                                 world-class client service and support.              all-time record. Operating income reached
2014 annual report with you. During this
                                                                                                      $3.2 billion, and diluted earnings per share
 scal year, ended September 30, 2014, the        As a result of the hard work and dedication
                                                                                                      rose 12% to $3.79 from $3.37 in scal year
company achieved signi cant results and          of our employees, scal year 2014 saw a
                                                                                                      2013--both new records.
made progress on the strategic initiatives       number of positive developments. Most
that continue to enhance the rm's                importantly, long-term relative investment              e company's mix of AUM--diversi ed
position as a premier global investment          performance continued to show strength               by investment objective, sales region and
management company.                              across equity, xed income, hybrid and                client type--is well balanced and has been a
                                                 alternative strategies.                              source of stability. As of September 30, 2014,
For nearly seven decades, individuals and
                                                                                                      equity and hybrid assets comprised 59%
institutions around the world have viewed        During the year, we reached new all-time
                                                                                                      and xed income comprised 40% of total
our organization as a trusted partner.           highs for assets under management
                                                                                                      assets (1% in cash management). We
We remain committed to the ideals of             (AUM), and we ended the year just shy of
                                                                                                      believe Franklin Templeton's diversi ed
prudent nancial advice and active                record levels with $898 billion in AUM--
                                                                                                      business positions us well to capture our
portfolio management. Our organization           a healthy 6% increase over the $845 billion
                                                                                                      industry's tremendous growth opportunities
continuously strives to deliver high-quality     in AUM at the end of scal year 2013.
                                                                                                      and also weather market volatility.
results through investment strategies that       Long term, Franklin Templeton's AUM
serve individual investors, and institutional    has continued to climb, with a compound              Although long-term sales exceeded
and high net-worth clients.                      annual growth rate of almost 10% over                $192 billion, redemptions weighed on net
                                                 the last decade through September 2014.              new ows as investors remained cautious.
   e 2014 annual report highlights the
                                                                                                      Fiscal year 2014 saw net out ows of
many successes we have achieved through          Bene ting from sustained, long-term
                                                                                                      $4.6 billion compared to positive net ows
the collaboration of our global workforce.       appreciation in the global nancial
                                                                                                      of $24 billion in the previous scal year.
   roughout the year, our more than 9,000        markets, solid investment performance
                                                                                                      In line with the industry, we saw out ows
employees around the world worked side           and success attracting new business, the
                                                                                                      in xed income, particularly in Europe,
by side to deliver strong, long-term             company produced another outstanding
                                                                                                      where we had experienced strong in ows
investment results and provide exceptional       year in terms of revenue, income and
                                                                                                      over the past few years. Interest in our
service--whether working face-to-face            earnings per share.



4
                                      F R A N K L I N R E S O U R C E S , I N C . A N N UA L R E P O R T 2 0 1 4




hybrid and balanced strategies, however,         Assets Under Management (AUM) in USD Billions
resulted in positive ows in that category        As of September 30, 2014
for the year.
                                                        41%                                40%                             18%          1%
                                                 2014                                                                                        $898
Focus on Investment Excellence
                                                        40%                              43%                              16%      1%
Franklin Templeton's primary goal is to          2013                                                                                   $845
deliver strong long-term results to our
                                                        39%                         45%                            15%     1%
investors. We believe in the bene ts of          2012                                                                           $750
active portfolio management and strive
                                                        39%                       45%                       15%    1%
for excellence and consistency in our                                                                                    $660
                                                 2011
investment processes.
                                                        43%                        39%                     17%     1%
Globally, as of September 30, 2014, we           2010                                                                   $645
are pleased to report that more than                    ASSET MIX   
two-thirds of the company's long-term                   Q  EQUITY      Q  FIXED   INCOME       Q  HYBRID     Q  CASH    MANAGEMENT
ranked assets placed in the top two quartiles
of their respective peer groups in total
return for the three-year period, and more
than 70% for the ve- and 10-year periods.3

To capitalize on this strong investment
performance, the company has been
positioning itself to take advantage of two
unfolding industry-wide rotations--the
rotation back into equity investing and the
rotation within xed income, as investors
seek nontraditional xed income assets.
Increasing equity market share has been a
multiyear e ort, and our teams continue
to promote educational campaigns that
highlight the importance of diversi cation,
long-term growth opportunities and our
equity capabilities.

Our Time to Take Stock equity campaign
has built momentum around the world,
and is now active in 23 countries--tailored
to each local market. is campaign,                            "Global Product Strategy partners across
combined with strong investment
performance in key strategies, helped fuel                     the organization to deliver and support
equity sales. Notably, equities made up
39% of long-term sales this year, compared
                                                               competitive investment solutions that are
to 29% in scal year 2013.
                                                               designed to meet our clients' investment
To help investors diversify their xed
income holdings, we have also been
                                                               needs around the world."
                                                                                                                                STAN HONG
actively calling attention to the merits
of balanced funds and our outstanding
o erings in this asset class. ese hybrid


                                                                                                                                                    5
                                   F R A N K L I N R E S O U R C E S , I N C . A N N UA L R E P O R T 2 0 1 4




funds, led by Franklin Income Fund,             Our Income for What's Next campaign,                building out its solutions team, focusing
have an impressive track record. e              which focuses on retirement income                   on packaging and delivering the robust set
US-registered version of Franklin Income        strategies, has been our most popular                of capabilities we have developed over
Fund was our best-selling strategy in 2014,     campaign. Featuring strategies like                  decades. We have won several key
and its Class A shares were awarded             Templeton Global Total Return Fund,                  institutional mandates, and liquid
 ve stars overall by Morningstar as of          Franklin Income Fund and Franklin                    alternatives have gathered interest from
September 30, 2014.4 Additionally,              Rising Dividends Fund, the campaign has              retail and institutional investors alike.
we saw an increase in demand for the            generated signi cant interest and favorable
                                                                                                     In October 2013, we reached an important
cross-border version of the fund. In total,     reviews among nancial advisors.
                                                                                                     milestone when we launched the Franklin
our hybrid strategies attracted nearly
                                                   e company's ongoing initiative to                 K2 Alternative Strategies Fund for US
$10 billion of net new ows this scal year.
                                                further foster and formalize collaboration           investors. Broker/dealers are focused on
Fixed income continues to be an important       among its investment teams remains an                increasing their allocations to the liquid
component of our clients' asset mix,            area of focus. is year, we established               alternative space and are turning to fund
especially as investors near or enter           global research teams across our invest-             partners like us. e fund is already
retirement. We have maintained our focus        ment management groups for select                    available for sale to clients at 65 distribution
on educating investors about the strength       industry sectors, including materials,               partners--a testament to the outstanding
of our xed income franchise, which is           technology, nancials, energy, health care            reputation and performance of K2 Advisors,
particularly well positioned to manage          and industrials. Our analysts meet                   the strength of our relationships and our
through a rising interest rate environment      regularly to share their insights and to             partners' appetite for this type of product.
with our line-up of global, oating-rate,        provide our investment teams with an                 Subsequent to year-end, we launched a
high-yield and short-duration strategies.       additional point of view to consider                 cross-border version of the fund.
   e rm launched a series of proactive          when constructing portfolios.
e orts encompassing a broad range of                                                                 Global Perspective Shaped by
                                                Building out our alternatives platform and
educational material that nancial                                                                    Local Expertise
                                                positioning ourselves to meet the increasing
advisors can use in their conversations                                                              Franklin Templeton was one of the rst
                                                demand for solutions and liquid alternatives
with investors to highlight nontraditional                                                           asset managers to strategically invest
                                                have been strategic priorities for the
 xed income strategies and their bene ts.                                                            internationally, developing local research
                                                 rm--and areas where we have delivered
                                                                                                     capabilities and distribution o ces
                                                value to our clients. e rm has been



6
                                     F R A N K L I N R E S O U R C E S , I N C . A N N UA L R E P O R T 2 0 1 4




                                                                  " Within the Franklin Equity Group,
                                                                    our collaborative environment
                                                                    balances autonomy with rigorous
                                                                    debate and accountability."
                                                                                                   SERENA PERIN VINTON




around the world. We have continued to          A key focus in 2014 has been to grow our             AUM by Region
extend our relationships and capabilities       institutional asset base, which currently            As of September 30, 2014
in these local markets for over 25 years.       represents approximately a quarter of                                           66%
                                                AUM. In scal year 2014, we experienced                                  3%      UNITED STATES
We now have o ces in 35 countries and                                                                     LATIN AMERICA
                                                institutional gross sales and net sales
serve clients in over 150. Our dedicated                                                                           4%
                                                growth in many regions. Our US
global equity and global xed income                                                                         CANADA
                                                institutional business, for instance, saw
research teams are located in 44 o ces,
                                                signi cant growth in new business and                         10%
allowing us to build on our strength of                                                               ASIA-PACIFIC
                                                positive net ows in scal year 2014,
uncovering opportunities by visiting
                                                a reversal from the previous year's net                           17%
companies, local suppliers and customers,
                                                out ows. We have bene ted from our                        EUROPE
and understanding the local language                                                                  MIDDLE EAST
                                                focus on de ned contribution retirement                    AFRICA
and culture.
                                                plans and alternative strategies--segments
Our company partners with nearly                of the market that are experiencing higher
250,000 nancial professionals worldwide.        growth rates than the traditional de ned
Not surprisingly, technology is playing a       bene t market. e trend toward more
                                                                                                     AUM by Client Type
more critical role in how we serve the          non-US exposure has put us in a strong
                                                                                                     As of September 30, 2014
needs of our clients. e use of data             position to compete for a greater share
analytics has allowed us to better under-       of this business.                                                               74%
                                                                                                                                RETAIL
stand our customers' needs and, in turn,                                                                                 2%
                                                While the United States is a more mature                  HIGH NET WORTH
create more customized, timely and
                                                market with underlying areas of growth,
relevant communications. We have also
                                                many international markets o er greater
been able to identify more quali ed leads
                                                promise and upside potential. Notably, we
for our advisor consultants to call or visit.
                                                now have 18 countries and regions with                            24%
We want to optimize our return on the                                                                INSTITUTIONAL
                                                over $5 billion in AUM.
investments we are making in sales,
marketing and technology by delivering          In these markets, we are committed to our
valuable insights and recommendations           belief that active portfolio management
for our nancial advisors, gatekeepers and       delivers tangible value. Our new States of
consultants--and their end investors.           Emergence campaign, which makes a
                                                             TM




                                                                                                                                            7
                    F R A N K L I N R E S O U R C E S , I N C . A N N UA L R E P O R T 2 0 1 4




                                                                                      Franklin Templeton the highest long-term
                                                                                      credit rating by Standard & Poor's of any
                                                                                      investment manager.

                                                                                      To achieve quality, long-term investment
                                                                                      results, we must continually attract and
                                                                                      retain talented investment professionals.
                                                                                      Our focus, combined with our strong
                                                                                      reputation, has enabled us to augment our
                                                                                      investment personnel, regardless of
                                                                                      market cycle. In fact, since 2000, we have
                                                                                      increased the number of investment
                                                                                      professionals from 243 to 648 today--
                                                                                      an increase of 167%.

                                                                                         roughout our history, the company's
                                                                                      leaders have managed the rm with a
                                                                                      long-term perspective and a steady, scally
                                                                                      responsible hand. e company maintains
                                 case for active management for emerging
" In International               market equity and xed income strategies,
                                                                                      a exible capital management strategy that
                                                                                      attempts to balance the varied preferences
  Advisory Services,             has been rolled out across 19 markets
                                                                                      of our fellow stockholders. We believe
                                 with 86,000 views on our websites to date.
                                                                                      there is value in maintaining a strong
  we think globally                 is campaign is built on the premise that
                                                                                      balance sheet, comfortably exceeding
                                 not all emerging markets are evolving at
  and act locally to             the same pace, so lumping them all in one
                                                                                      regulatory capital requirements, and
                                                                                      assuring adequate liquidity and nancial
  meet customer                  basket may not yield the best results--a
                                                                                        exibility. We also bene t from having the
                                 powerful argument for active management.
                                                                                      capacity to invest in new products and
  needs and demands,                                                                  capabilities.
                                 Strength and Experience
  leveraging our                 Financial strength and stability continue            We continue to return a large percentage
                                 to be important to investors who are                 of US capital to investors through cash
  global resources and           increasingly choosing asset managers                 dividends and stock repurchases. Since
  in-depth expertise."           based on their ability to serve as long-term         1981, the Board of Directors has increased
                                 partners. Investors want to know that the            the regular dividend every year. In
      TATSUYA OGUCHI             company is going to be there through up,             December 2013, the quarterly dividend
                                 down and o en volatile sideways markets.             rose once again from $0.10 per share to
                                    ey also seek reassurance that their               $0.12, a 20% increase. During the scal
                                 go-to partners will be in a strong nancial           year, the company repurchased approxi-
                                 position to provide the investment                   mately 12 million shares and returned
                                 excellence and superior client service               approximately $1 billion to shareholders
                                 they deserve and have come to expect.                through share repurchases and dividends.

                                 Fiscal responsibility is a deeply ingrained
                                                                                      Global Industry Trends Point to a
                                 value for our rm. e events and ensuing
                                                                                      Bright Future
                                 market volatility over the past decade have
                                                                                      When I look across the landscape of the
                                 helped investors understand the value that
                                                                                      investment management industry and the
                                 a company like Franklin Templeton o ers.
                                                                                      key trends shaping it, I see many reasons
                                 Our strong balance sheet has earned


 8
                                               F R A N K L I N R E S O U R C E S , I N C . A N N UA L R E P O R T 2 0 1 4




for continued optimism. It is clearly a                     business models, service requirements and                           Direct Reports to the
                                                                                                                                Chief Executive Officer (left to right):
dynamic time in our industry as underly-                    client expectations. In this ever-changing
ing forces, like the importance of saving                   environment, our extensive global                                   Donald F. Reed
                                                                                                                                Chief Executive Officer
for retirement and providing income                         expertise and reach allow us to continue to                         President
                                                                                                                                Franklin Templeton Investments Corp.
during retirement, take hold. We continue                   look for opportunities to enhance our                               (Canada)
to bene t from the strong, multiyear                        capabilities and product o erings to better
                                                                                                                                John M. Lusk
trends fueling international business                       service the needs of our customers.                                 Executive Vice President
growth and the evolving investment needs                                                                                        Investment Management
                                                            Our past and future success rests rmly
of a rapidly growing middle class in                                                                                            Kenneth A. Lewis
                                                            on the shoulders of our global employees.                           Chief Financial Officer
emerging economies.                                                                                                             Executive Vice President
                                                            I am proud of our talented workforce,
With this growth, we are also evolving to                   whom I consider to be among the very                                Gregory E. Johnson
                                                                                                                                Chairman of the Board
capitalize on developing trends, like the                   best in the industry. We believe we are well                        Chief Executive Officer
shi in demand for customized solutions                      positioned to confront the challenges and                           President

and alternative investing, as well as how                   capitalize on the opportunities ahead.                              William Y. Yun
                                                                                                                                Executive Vice President
active strategies like those o ered by                                                                                          Alternative Strategies
                                                            Sincerely,
Franklin Templeton t together with the
                                                                                                                                Jennifer M. Johnson
increasing industry-wide popularity of                                                                                          Chief Operating Officer
passive products to create broadly                                                                                              Executive Vice President

diversi ed portfolios.                                                                                                          Craig S. Tyle
                                                                                                                                General Counsel
                                                            Gregory E. Johnson                                                  Executive Vice President
Of course, challenges and complexities are
                                                            Chairman of the Board
also evident. e ongoing wave of new                         Chief Executive O cer and President                                 Vijay C. Advani
                                                                                                                                Executive Vice President
regulations will further impact economies,                  Franklin Resources, Inc.                                            Global Advisory Services



3. Sources: AUM measured in the peer group rankings represents 85% of our total AUM as of 9/30/14. The peer group rankings are sourced from Lipper, Morningstar or
eVestment in each fund's market and were based on an absolute ranking of returns as of 9/30/14. Private equity, certain privately offered emerging market and real estate funds,
cash management, funds acquired in fiscal year 2014, and certain funds and products with limited peer group information were not included.
4. As of 9/30/14, Franklin Income Fund's Class A shares received a traditional five-star overall Morningstar RatingTM, measuring risk-adjusted returns against 573, 479 and 266
US-domiciled Conservative Allocation funds over the 3-, 5- and 10- year periods, respectively. A fund's overall rating is derived from a weighted average of the performance
figures associated with its 3-, 5- and 10-year (if applicable) rating metrics.  2014 Morningstar, Inc. All Rights Reserved. The information contained herein: (1) is proprietary
to Morningstar and/or its content providers; (2) may not be copied or distributed; and (3) is not warranted to be accurate, complete or timely. Neither Morningstar nor its
content providers are responsible for any damages or losses arising from any use of this information. Past performance is no guarantee of future results.


                                                                                                                                                                               9
