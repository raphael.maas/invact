Fiscal 2008 was a record year in the companys
138-year history. We achieved many new milestones, including surpassing $3 billion in sales,
generating more than half of our revenues outside the U.S., and breaking the 35 million case
barrier in annual depletion volumes for our entire portfolio. In an environment marked by
increasing consumer and competitive challenges, we produced strong results underscored by
healthy underlying growth, a strong balance sheet, and excellent cash flows.
Financial Performance. For the year ended April 30, 2008, diluted earnings per share from
continuing operations increased to $3.55, an increase of 10% over last years earnings. Operating
income grew 14% to $685 million on a reported basis, or 8% on an underlying basis. Our
net sales for the year increased 17% to a record $3.3 billion, while gross profit grew 14% to
$1.7 billion, both benefiting from the addition of the Casa Herradura brands acquired in fiscal
2007 and a weaker U.S. dollar. Our healthy cash flows enabled us to pay dividends totaling
$158 million to shareholders last year, a 10% increase over dividends paid out in the prior year
and the 62nd consecutive year of paying regular quarterly cash dividends.
 Since the beginning of fiscal 1999, we have returned approximately $1.3 billion to
shareholders via dividends and cash distributions, including a $204 million cash distribution
in early fiscal 2008 associated with the proceeds received from the sale of Lenox, Inc. Our total
shareholder return, assuming reinvestment of dividends, over that 10-year period has significantly
outpaced the return delivered by the S&P 500. In fact, a $100 investment in our Class B stock
at the end of April 1998 would be worth nearly $300 at the end of fiscal 2008. This compares
quite favorably to a similar investment in the S&P 500, which today would be worth about $150.
Our total shareholder return for this fiscal year was a healthy 8%, higher than nearly all
of our direct competitors and well ahead of the total return for the S&P 500 which declined 5%.
Reflecting our commitment to creating value for shareholders and our confidence in the
future, we completed the repurchase of $223 million of our common stock at an average price
of $68 per share. We believe this was an excellent investment at a time when both interest
rates and our stock price were attractive.
Brand Performance. Because of the continuing excellent performance of our flagship Jack
Daniels brand over the years, some observers of Brown-Forman mistakenly view us as a
one-brand company. But with 10 brands boasting depletions of more than 1 million cases,
we are much more than a one-brand company.
Jack Daniels. Last year, Jack Daniels fortified its position as the worlds best-selling
American whiskey brand with global depletions approaching 9.5 million cases. Depletions
and net sales in the U.S. grew in the low single digits, while outside the U.S., depletions were
up 8%, and net sales grew nearly 17%. Total nine-liter depletions for the Jack Daniels family

of brands, including Gentleman Jack, Jack Daniels Single Barrel, and Jack Daniels ready-todrink
products, reached more than 10 million cases on a drinks-equivalent basis, representing
volume growth of more than 6% for the fiscal year.
Finlandia. Ninety percent of Finlandias 2.8 million cases were generated outside the U.S.,
including over 800,000 cases in Poland, the brands largest market. The brand continued to be a
major growth driver with depletions advancing 16% and net sales gaining 33% this past fiscal year.
Southern Comfort. Our second-largest profit contributor, Southern Comfort continued to
deliver solid gross profit growth in fiscal 2008, driven by volume and price increases in
Australia, South Africa, and some established European markets. Global volumes held steady
at nearly 2.5 million cases, despite a decline in the U.S., the brands largest market. The brand
continues to exemplify our companys brand renovation capabilities and now has posted nine
consecutive years of gross profit growth.
Casa Herradura. In the first full year since we acquired Casa Herradura, we carefully evaluated
the brands positioning, pricing, and packaging; implemented new trade practices to
reduce excess wholesale and retail inventory in Mexico; and successfully integrated the
Herradura and el Jimador brands into our U.S. distribution network. We believe these brands,
along with the New Mix el Jimador tequila-based ready-to-drink product, will be key contributors
to the companys long-term growth. Renowned for crafting tequilas of superior
quality and authenticity, Casa Herradura was recognized as the 2007 Distiller of the Year by
Wine Enthusiast magazine  the first tequila producer to earn this prestigious title.
Balanced Thinking. As the theme for this report highlights, we believe theres no substitute
for balanced thinking. We believe it is critical to our long-term success and ongoing independence.
A direct reflection of our commitment to balanced thinking is the collaborative,
consensus-oriented manner in which our company is run. We have long believed that the
collective wisdom of all of us is far superior to the individual intellect of any one of us. The
practice of this belief helps us immensely in producing consistently thoughtful and accurate
judgments about our business.
We achieve balanced thinking with a skilled and experienced Board and leadership team
along with active involvement by Brown family members in management, on our Board, and
as supportive shareholders. In the last year, we formed the Brown-Forman/Brown Family
Shareholders Committee to further encourage open, constructive, ongoing dialogue between
the company and its family shareholders. Designed for broad family participation and including
several non-family Brown-Forman executives, the committee has formed working groups
to study areas of particular interest to family shareholders, including governance, philanthropy, 
and family members education and employment at the company. A list of the leaders and
Brown family members who collectively provide the balanced thinking behind our strategies
and success can be found on pages 52 and 53.
Senior Management Changes. In October 2007, we named Matthew Hamel as our General
Counsel. Matt brings over 21 years of international legal and corporate management experience
with Dow Jones & Company and Colgate-Palmolive Company. He succeeds Michael
Crutcher, who retired after 18 years with us, during which he built and led our team of legal
and government relations professionals and served as a valued and trusted advisor.
 In December 2007, Ralph de Chabert was appointed as the companys first Chief
Diversity Officer with the responsibility for developing and implementing a global diversity
and inclusion strategy, with an initial focus on the U.S. He brings to this critical role a proven
track record of significant achievements managing diversity initiatives in the public, private,
for-profit, and not-for-profit sectors, including positions with McKesson Corporation,
Safeway, and American President Companies, Ltd.
 In March 2008, Donald Berg was named Chief Financial Officer effective May 1. A
19-year company veteran and member of our Executive Committee since 1999, Don most
recently served in the key role of Senior Vice President and Director of Corporate Finance,
with responsibilities encompassing investor relations, corporate development, treasury, and tax.
He succeeds Phoebe Wood, who retired at fiscal year-end after bringing thoughtful and
professional oversight to our financial and technology functions and helping lead us during a
period of strong financial performance over the last seven years.
In Conclusion. I believe strongly in this company and in its prospects for long-term growth,
and I thank you on behalf of our management for your long-standing support. We see tremendous
opportunity for the development of our brands in both our U.S. market and the exciting
world outside the U.S., which now accounts for the majority of our sales.
As has been our practice for generations, we will strive for an optimal balance of current
growth and long-term value creation that will provide our shareholders with superior returns
while serving the best interest of employees, business partners, and the communities in which
we work and live.
Sincerely yours
Paul C. Varga
chairman and Chief Executive Officer
June 27, 2008

