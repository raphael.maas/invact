A letter to our shareholders

We are pleased to present our first report of Stilwell Financial Inc., and its
focused efforts in 2000 to continue its evolution as a diversified, global financial
services organization.
This report is important to understanding and appreciating the core strengths and diverse
prospects of Stilwell and its affiliates. First, we mean to exploit Stilwells stage of growth and
development in the dynamic financial services industry. The emergence of Stilwell as an
independent, stand-alone company upon completion of the spin-off transaction in July 2000
provides Stilwell and its shareholders opportunities for growth through thoughtful and targeted
internal and external development. Second, we intend to build a disciplined and dynamic business
model that can be implemented in rapidly shifting markets for financial services. And third,
we are committed to ensuring that Stilwell continues to expand its presence both domestically
and internationally, and broaden its range of businesses and products. Stilwell believes it has
established a strong platform to support future growth in revenues, operating income and net
income, deriving its strength from the experience and capabilities of Stilwells subsidiaries 
Janus Capital Corporation, Berger LLC and Nelson Money Managers plc  and its equity
investment in DST Systems, Inc.
The remarkable success of Janus in the last few years dominated our results for 2000  Janus
assets under management have grown from $51 billion in 1996 to nearly $250 billion at
December 31, 2000. Its success has fueled the increase in Stilwells earnings. Through strong
management, unique culture, efficient operations and diversified distribution structure, Janus
has managed rapid growth through a structured cost approach that strives to maximize
efficiencies and minimize fixed overhead.
Berger has also done well  it has doubled its assets under management and operating margins
compared to 1998. And since our initial investment in Nelson, we have seen its assets under
management increase by more than 50%. Finally, we attach significant importance to our 33%
ownership interest in DST and continue to view this investment as a strategic partner that
provides unusual strength to our balance sheet and knowledge of the technology essential to the
financial services industry. The sharp downturn in the market in the third and fourth quarter of
2000 does not detract from the accomplishments of this Stilwell group of companies.
Through the efforts of Janus, Berger, Nelson and DST, we reported record earnings in 2000,
driven by an 84% increase in average assets under management from $164.2 billion to $301.9
billion. Net income of $663.7 million, a 112% increase over 1999, included non-recurring gains
that contributed approximately $52.9 million to the 2000 results. Consolidated revenues in
2000 surpassed $2 billion, growing by approximately 85% compared to prior year. Perhaps more
impressively, operating income nearly doubled year to year, reaching $1 billion. This greater rate
of growth in operating income versus revenues reflects the improved operating margins in 2000
and consistent attention to cost management.

The value of assets under management remains the key driver to our financial
results. The 84% increase in average assets under management reflects a record $68
billion in net cash inflows during the year and solid performance through the first
half of 2000. This unprecedented growth was partially offset by a lag in performance
(reflecting the wide variation of the various market indexes) during the last six
months of the year, resulting in market depreciation of $68 billion for the year.
As a result of the market decline in the second half of the year, Stilwells assets
under management were essentially unchanged at December 31, 2000 compared
to December 31, 1999.
Again, Janus led in this result with
aggregate net cash inflows of $66.9
billion, which topped the mutual
fund industry and exceeded 1999s
then-record $56.4 billion of inflows
by 19%. The number of shareowner
accounts of Janus also increased by nearly two million from year-end 1999.
While Januss reputation in recent years has been deservedly celebrated for the
success of its funds that concentrate in the growth equity market, Janus is also
continually taking steps to satisfy investor needs worldwide. In addition to launching
several new domestic funds, including value style products, Janus has continued to
leverage its strong brand name across various distribution channels, including the
Janus Adviser Series to increase financial intermediary access to its portfolios and
broadening distribution of the Janus World Funds into Asia and Canada.
Berger followed its strong performance in 1999 with another 15% increase in its
assets under management during 2000, reflecting net inflows of more than $1.3 billion.
Berger also has strengthened its investment management team with its growth and
value products and opened its first interactive investor center to further enhance its
customer service capabilites. Nelson, our United Kingdom-based subsidiary,
experienced an 8% growth in assets under management during 2000, and is seeing
its aggressive marketing and technology investment produce top-line results.
Both Janus and Berger continue to offer top quality long-term performance in their
funds and portfolios, evidenced by the number that achieved Morningstars fourand
five-star ratings. Approximately 85% of Janus and Berger funds fall in these
categories as of year-end.
Equity earnings from DST totaled $69.8 million for 2000 versus $44.4 million
in 1999. Exclusive of approximately $11.4 million non-recurring gains, Stilwells
portion of DSTs income improved 32% over 1999, largely due to higher revenues
and operating margins. DST has consistently reported strong earnings, and its
prospects in 2001 and beyond are very positive.
We are very proud of these successes and resulting financial growth, but Stilwells
future possibilities and challenges are our priority. Our most important duty is to
ensure that our subsidiaries have the resources to manage and expand their business

franchises and to strengthen client relationships with a high level of quality and
versatile service. A key component of growth for our subsidiaries centers on ongoing
sales and marketing efforts, with emphasis on competitive investment returns,
uperior customer service and diverse product offerings.
Stilwell is fortunate to partner with four companies that have credible brand name
platforms. The Janus name is widely known and respected, and the Janus management
group has cultivated this brand skillfully. Berger and Nelson continue efforts to enhance
their respective brands by utilizing targeted marketing and appropriate media exposure.
With respect to our strategic equity investment, DST is considered to be the premier
provider of back-office support for the mutual fund and other financial services industries.
Stilwell supports each of our subsidiaries management through respect for their
autonomy and provision of meaningful equity incentives for each subsidiarys
management. Management and employees of Janus, Berger and Nelson are stockholders
of their respective companys, providing strong incentives for growth and success to
each individual owner.
Equally crucial to our success are two components of our strategy that are familiar to
our shareholders. Specifically, we strongly believe in the benefits of repurchasing our
common stock. Stilwells Board of Directors authorized a $1 billion program in July
2000. To date, we have repurchased over seven million Stilwell shares. We also welcome
the opportunity to acquire the common stock of our subsidiaries as it becomes available
based on our firm belief in the core business models and franchises of our subsidiaries.
The recently announced transactions with various Janus minority stockholders
demonstrates this fundamental belief. Such repurchases also permit the recycling
of this equity and its broader distribution among our subsidiaries.
We are also pursuing strategic acquisitions and partnership opportunities that will
provide long-term value to you. We are not constrained as to the opportunities we
evaluate, but we believe it is important to focus on financial services companies with
global perspectives to add diversity to the Stilwell portfolio of investments.
Stilwell and its predecessor have been in the mutual fund industry since 1962. Over
this period, we have experienced many ups and downs in the securities markets.
However, we have believed in the underlying strength of demographic forces which
have driven the expansion of financial services here and around the world. We remain
committed to this industry and bringing to our shareholders the long-term value of
our investments. We will continue to build on the accomplishments of Janus, Berger,
Nelson and DST, and we will critically evaluate all avenues available for value improvement.
We have tremendous expectations and enthusiasm for our company, and we hope that
you share both.

Landon H. Rowland
Chairman of the Board, President
and Chief Executive Officer
March 16, 2001