Fellow Shareholders,
Leggett & Platt accomplished much during 2006. We attained record sales and earnings, transitioned to a
new CEO and a new COO, and completed the restructuring we began in 2005. We also updated our growth
and margin targets, filled several newly-created business development and product development positions,
increased the dividend for the 35th consecutive year, and bought back 3% of our stock.
Record Sales and EPS. Full year sales increased 4% to a record of $5.5 billion. Acquisitions added 5% to
our revenues, but divestitures associated with our restructuring activity reduced sales by 2% (as anticipated).
Internal sales grew less than 1%, with gains in sales of foam and carpet underlay offset by weak market
demand for bedding, wire, automotive, and certain other components.
Full year EPS increased from $1.30 in 2005 to a record $1.61 in 2006. The earnings increase primarily
reflects reduced restructuring-related expenses, lower workers compensation costs, operational benefits
from restructuring activity, reimbursement of Canadian lumber duty, a full year of earnings from recent
acquisitions, and reduced share count.
Our cash flow and balance sheet remain strong. During the year we
generated $479 million in cash from operations, a 7% increase over
2005. We continue to produce significantly more cash flow than is
required to fund internal growth and dividends; extra cash is being
used for acquisitions and stock repurchases.
Promotions, Restructuring, and New Targets. For only the fourth time
in the last 70 years, Leggett named a new CEO. In a seamless and long-planned transition, Felix Wright
handed the CEO baton to Dave Haffner. Felix joined the company in 1959, has long been one of Leggetts
key executives, and served as CEO from 1999 to 2006. He continues to provide counsel as an active
employee Chairman of the Board and internal advisor to Leggetts senior management. As also anticipated,
Karl Glassman succeeded Dave Haffner as Chief Operating Officer. Dave and Karl have been with Leggett for
23 and 25 years, respectively. They have co-authored these annual Shareholder Letters for the last five years,
and should be familiar to investors.
The restructuring we announced in the fall of 2005 is now complete. Through that process we closed, consolidated
or divested 36 operations. Collectively, these operations had revenue of approximately $400 million;
most of that volume was moved to remaining facilities. Net expenses associated with restructuring-related
activities in 2005 and 2006 totaled $67 million. The ongoing annual benefit from this activity is expected to
be about $30-35 million in EBIT, or about 10-12 cents of per share earnings.
In September we updated Leggetts growth and margin targets. We aim for 8%  10% annual sales growth,
from a combination of internal expansion and external acquisitions. By 2009 we expect to achieve 7% net

margin and 11% EBIT margin. We expect higher sales, improved margins and possible reductions in share
count to result in average EPS growth of 10% per year.
Dividend Growth Continues. In May, the Board of Directors increased the quarterly dividend from $.16 to
$.17 per share. At an indicated annual rate of $.68 per share, 2007 marks 36 consecutive years of dividend
increase, each year since 1971. Compound dividend growth has averaged nearly 14% since 1971 (and 11%
for the last decade). Only one other S&P 500 company can match that 36-year growth record.
In 2007, Leggett was again named to Standard & Poors list of Dividend Aristocrats. Mergents Dividend
Achievers lists Leggett among the top 110 companies for 10-year growth in dividends. And Fortune magazine
continues to rank Leggett among the top third of the companies it deems as Americas Most Admired.
We continue to target dividend payout (over the long run) at approximately one-third of the average earnings of
the preceding three years. Leggett is proud of its dividend growth record, and we expect to extend that record
far into the future.
Significant Stock Repurchase. In 2006, we used $150 million to purchase 6.2 million shares of Leggett
stock. We paid, on average, about $24 per share. Over the last two years the number of outstanding shares
declined by 7%, to 178 million shares on December 31.
The Board of Directors has authorized the purchase of up to 10 million shares of Leggett stock in 2007;
however, we have established no specific repurchase schedule. We look at many factors when deciding how
much stock to buy, including the current stock price and the amount of cash we expect to have available.
Stock Performance. Though Leggett accomplished much during 2006, the stock price improved only
modestly, well below our expectations. Long-term stock price appreciation is of acute interest to us, in part

because most of our personal wealth is tied to Leggett stock. We are keenly aware of the responsibility (for
investment growth) with which many small shareholders, employee-partners, and institutional investors have
entrusted us.
Operating performance over the last few years has been strongly influenced by external factors, including
reduced market demand for certain products, increased foreign competition, significantly higher costs for
steel, energy, and oil-based materials, and adverse changes in currency rates. If we want the stock price to
increase, we must overcome these external challenges and achieve consistent, profitable growth over the
long term. This is our intent.
Sales Growth. Leggetts recent sales growth, averaging 5%  6% per year, has come primarily from our acquisition
program. Our internal (or organic) sales growth has been modest. As mentioned above, we are aiming for future
annual sales growth of 8%  10%. Acquisitions should continue to add about 5% to yearly sales growth, and
we believe we can improve internal growth to the 3%  5% range. To enhance internal growth, we determined
which factors have been constraining us, and then started making changes to alleviate those constraints.
In our discussions, we noted two things: (i) the majority of the markets in which we participate have been
growing slower than GDP, and (ii) urgent, daily, tactical duties can be so consuming that operating executives
have little time to address strategic objectives (such as growth). As a result, we havent been generating as
many growth ideas as we would like. In response, we created new positions, enabling a group of executives to
dedicate 100% of their time toward profitable growth, and holding them accountable to achieve it.
More Resources Dedicated to Growth. In June we created a new, senior-level position, reporting directly to
the COO, with responsibility for company-wide engineering and innovation efforts. We wanted to augment and
coordinate the companys many technology efforts by providing our innovators a single focal point and senior
representative on the executive team. We staffed that position with a highly talented engineer and business
leader who accumulated R&D and engineering experience while at GM, Lear, Visteon, and Maytag.
In September we established five new business development positions, each reporting directly to one of our
five segment presidents. By year end we staffed those positions with an impressive group of individuals, all
of whom possess significant M&A or business development experience. They bring transactional knowledge
from previous roles as investment bankers and legal advisors, as well as experience within Leggetts and other
corporations M&A departments. Collectively they hold 8 advanced degrees in business and law. All five were
promoted from within, enabling us to reduce risk by staffing these new positions with known, highly successful
candidates who are already familiar with our executives, objectives, practices and culture.
We are also establishing a new corporate function with responsibility to uncover new growth platforms. This
unit, reporting directly to the CEO, will uncover opportunities in growing markets that Leggett does not yet
serve (or that do not yet exist), investigate current and potential customers unmet needs, and provide insight
and trend analysis regarding markets, customers and consumers. As a result of these efforts, we anticipate
eventually moving into some new, faster-growth markets. We are in the process of finding the right executive
to lead this effort.

These appointments help us reenergize the companys focus on innovation, product development, and growth.
We expect to see greater idea generation, improved accountability for growth, movement into faster-growing
markets, and a renewed corporate-wide understanding of the urgency and importance of innovation and growth.
Margin Improvement. In addition to our growth efforts, we are also committed
to improving margins over the next few years. For the full year 2009, we
expect EBIT margin to be about 11% (equivalent to a net margin of about
7%), or about 220 basis points (bp) higher than in 2006. EBIT margin
improvement should come from four areas, as follows:
 50  100 bp from higher volume
 50  100 bp from new products
 about 60 bp from our restructuring efforts
 about 40 bp from continuous improvement
Strong Cash Flow. We have a strong balance sheet, and plenty of cash to
fund the growth we contemplate. Over the past 5 years, we generated over $2 billion of cash from operating
activity. After paying the dividend and funding maintenance capital, over $1.1 billion was available for growth
projects (i.e. expansion or acquisition). Though we used roughly $600 million of that cash to purchase our
stock, it was available to fund growth, had we found attractive opportunities. We also possess significant debt
capacity, should we need it. For the near future, we expect to have $350  450 million of cash available
annually for investment in growth initiatives.
We are convinced that our plans to grow sales and improve margins will help us attain our EPS growth goal
of 10% per year, on average. As that occurs, we expect return on equity (ROE) to increase to the 16%  17%
range. We take very seriously our responsibility to deliver on these plans, and to keep you informed of our
progress.
Sincerely,

Felix E. Wright David S. Haffner Karl G. Glassman
Chairman of the Board President and Executive Vice President and
Chief Executive Officer Chief Operating Officer
February 27, 2007