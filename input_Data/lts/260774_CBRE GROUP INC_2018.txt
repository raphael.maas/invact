Dear Fellow Shareholders:
2018 was another excellent year for CBRE. Adjusted earnings(1) rose 20% to $3.28 per share on 15%
growth in revenue. This marked our 9th consecutive year of double-digit increases in adjusted
earnings-per-share(1)
, and both revenue and adjusted EBITDA(1) reached new all-time highs of
$21.3 billion and $1.9 billion, respectively.
Our continued strong growth reflects our people�s intense focus on our strategy of delivering client
outcomes that are differentiated from those they can receive elsewhere. These outcomes are being
enabled by the many operational gains and impactful investments we have made across our business,
particularly in strategic acquisitions and Digital and Technology capabilities.
In the midst of one of our best years on record, we also reorganized our business, putting in place a new
corporate structure (effective January 1, 2019) that better aligns with how we manage our global
business. Beginning this year, we will operate � and report our results on � three global business
segments: Advisory Services, Global Workplace Solutions and Real Estate Investments.
This new structure will provide greater transparency into our performance and positions CBRE to be a
better company in significant ways:
� Our management team now has clearer lines of authority with greater accountability for the results
they produce.
� We have escalated our focus on excellence across our business lines, including embedding
increasingly robust Digital and Technology capabilities in them.
� Our Client Care program has been re-structured to provide deeper insights into ways that we can
improve client outcomes.
� We have taken steps to minimize cost and bureaucracy to help sustain both our financial
performance and engagement with employees.
Importantly, we are also making moves to operate our business in a socially responsible manner,
particularly in the areas of environmental stewardship, governance, ethical conduct, diversity & inclusion
and employee well-being. Over the past year, the gains we are making have been recognized by
prominent third-parties such as Forbes, Fortune, Barron�s, Dow Jones, the Ethisphere Institute and others,
and we are determined to continue our progress going forward.
We enter 2019 with excellent momentum across our global business. Our people are energized, our
financial position is exceptionally strong, and we are poised to take maximum advantage of CBRE�s
scale, resources and growing suite of technology tools.
Globally, the economy is expected to grow at a healthy, but moderately slower pace than in 2018.
Cross-border capital flows are solid, notwithstanding the ongoing trade and geopolitical tensions. While
we remain mindful of potential macro challenges and the length of the current economic expansion, this
continues to be a supportive environment for our business.
In particular, we expect solid revenue growth in our transaction businesses in 2019, supported by
market share gains. We also see continued momentum in real estate outsourcing as our competitive
advantages continue to grow, driven by large investments in strategic acquisitions and operational and
technology gains. This portends another year of strong growth for our outsourcing business in 2019.

The strong performance you have come to expect from CBRE � year-in and year-out � would not be
possible without our more than 90,000 global professionals. Their dedication to our clients is the
linchpin in our strategy and the reason for our ongoing success.
In closing, we thank you for your continued support and the confidence you place in CBRE. We work
hard every day to earn your trust.
Sincerely,
Robert E. Sulentic
President & Chief Executive Officer
CBRE Group, Inc.