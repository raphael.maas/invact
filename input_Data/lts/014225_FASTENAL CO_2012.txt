Letter to shareholders

As a shareholder, its important to move beyond the numbers and
gain at least a basic understanding of the company youre investing
in. So, before we delve into our results for 2012, wed like to present
Fastenals core principles and business strategy.
Our organization is guided by a motto of Growth Through Customer
Service, and we embrace a decentralized structure that encourages
everyone in Fastenal to make decisions in pursuit of that goal. We
start by hiring people who share our values of Ambition, Innovation,
Integrity, and Teamwork. Then, after providing them with intensive
training, we place these individuals in a role alongside veteran team
members and give them an opportunity to make smart decisions for
their customers and the company. By empowering every employee to
make timely, local decisions, weve created an environment in which
we respond with urgency and allow good ideas to emerge.
Our business has been built with local stores as
the focal point. Some may feel this isnt the most
efficient model in todays world, but we strongly
believe the closer we are to our customer, the better
we can understand their needs and help them
improve their business. We also believe in vertical
integration throughout our organization. Our
teams work hard to develop solutions and systems
in-house whenever possible  from designing
our own computer systems, to operating our own
trucking fleet, to utilizing our own engineers to
design our warehouse systems. We believe this approach not only
makes us more profitable but also more responsive and flexible.
Now that you have a better sense of how we operate, lets focus on our
results for 2012. The year started strong with 20% sales growth in the
first quarter, but in the late-April/May timeframe we saw a slowdown in
our growth. This slower environment occurred over a broad geographic
area and in most customer segments, and continued throughout the
remainder of the year. For the year, our sales were $3.13 billion, an
increase of 13.3% over our sales of $2.77 billion in 2011.
Our net earnings in 2012 were $421 million, an increase of 17.5%
compared to our earnings of $358 million in 2011. This increase
allowed us to achieve pre-tax earnings as a percentage of sales of
21.5%, an increase of 70 basis points over the 20.8% we reported in
2011 and the highest reported pre-tax earnings in company history.
This improvement was made possible entirely by good expense
control  in fact, our gross margin decreased slightly, coming in at
51.5% for 2012 versus 51.8% in 2011. We worked hard on improving
our gross margin throughout the year but found it difficult due to a
lower percentage of sales coming from fasteners (our highest-margin
product category) and due to better than average growth from larger
customers (which typically receive volume-leveraged pricing terms).
Expense control has always been a part of our culture, and its an
area where we continually focus. This year was no exception as our
operating and administrative expenses grew only 9.5% compared to
the 13.3% sales growth number cited above. Employee and related
expenses, our largest operating expense category,
grew at 10.1%. This expense was pushed higher
by: (1) a 44.0% increase in our employee profit
sharing program (this is calculated based on
our pre-tax earnings); (2) the introduction of our
vending incentive (more on this below); and (3)
our decision to implement a pay guarantee for
store managers. The non-payroll category, which
includes all general expenses, the largest being
occupancy, grew at just 8.3% versus 13.3% sales
growth.
We continue to make progress on our pathway to profit strategic
growth initiative. Introduced in 2007, this plan simply calls for us to
slow down our store openings while adding sales people. Through
this balanced approach, we originally projected we could increase our
average monthly store sales from $72,000 to $125,000 over a five-year
period from 2007 to 2012, in turn increasing our pre-tax earnings one
percentage point a year, from 18 to 23% of sales. Because of the impact
of the 2008-2009 recession on our revenue, we anticipated the timeline
would be extended by 18 to 24 months. However, during the recession
we became more efficient  lean, if you will  and we now believe our
23% pre-tax earnings goal can be achieved with average monthly store

sales of just $100,000 to $110,000. Based on this recalculation, and
assuming normal economic conditions, we remain confident we can
achieve our 23% pre-tax earnings goal in 2013.
One of the true highlights of the year was the strong growth of our
FAST SolutionsSM? (industrial vending) program. Why are we working
so hard to grow this business? Its simply a better way to both buy and
sell product. The benefits to our customers are numerous: reduced
consumption, reduced inventory investment, reduced purchase orders,
reduced product handling, and 24-hour product availability. The benefit
to Fastenal is a much stronger relationship, which translates into more
opportunities. We also save money on the service side of the business
because, with our Internet-enabled machines, the
orders automatically flow into our system. This
saves labor, eliminates errors, and streamlines the
supply chain.
Late in 2011 we introduced an employee incentive
program designed to increase the number of
vending agreements signed with new and existing
customers. By all accounts, this incentive has
been a tremendous success. We far exceeded our
internal goal of signing 15,000 vending machines
and ended the year with 20,162 signings  an increase of 157%
compared to our signings in 2011.
We will continue to work hard in all areas of industrial vending, with
the goal of providing better and more cost-effective solutions for our
customers. To that end, our team will work with our equipment supplier
to continually improve the machines we have today, and to develop the
systems we will need in the future.
This year proved to be a challenging one for our international business.
The global economic slowdown affected nearly all of our international
markets, but we were hit particularly hard in Asia and Europe. The
majority of the decline can be attributed to our existing customers
simply purchasing less in 2012 versus 2011. Our Canadian business
was also impacted by factors out of our control, yet we were able to
grow sales by 10.1% (in local currency, this growth was 11.6%). Our
sales growth of 32.9% in Mexico represented a bright spot for our
international operations  this was the one international market that
remained strong throughout the year. As reported in last years letter,
we opened our first location in Brazil in 2011, and although its still a
relatively small operation, were happy to report that its growing and
adding customers. In 2012, we opened in three additional countries 
Colombia, Romania, and Thailand. Overall, our international business
grew 14.9% in 2012 compared to 44.4% in 2011.
We continued to make investments in our sales specialists in 2012.
These individuals cover larger geographical areas, and their job is
to provide a high level of expertise when making end-user calls with
our store personnel. Each person is trained in a specific area. The
largest group is dedicated to national account
business, but we also have teams focused on
metalworking/manufacturing, government, safety
products, industrial vending, and large fastener
opportunities. In addition, there are smaller teams
specializing in various other product and market
opportunities. Overall, the results have been very
positive, and we believe our specialists represent a
good investment to help drive continued growth in
our existing stores and markets.
More than 30 years ago, our founder Bob Kierlin recognized that in
order to provide the best possible service to our customers, we
needed to be able to manufacture hard-to-find parts. Back then it
was one machinist, a couple of used pieces of equipment, and a lot
of enthusiasm. This business has steadily grown over the years, and
today it is a large producer of fasteners and specialty machined parts
with approximately 569 employees and locations on four continents.
As weve grown and expanded our manufacturing business, weve
also increased our capabilities. Today we can produce anything from a
standard screw or bolt to a highly machined replacement part. These
capabilities have been implemented with one purpose in mind, and
that is customer service. Many of our customers run their facilities
24 hours a day, seven days per week, so when something breaks they
need their local store to respond immediately. With our large inventory
of raw materials, our broad capabilities, and our locations around the
globe, we can deliver.

Our distribution team worked very hard in 2012 on developing new
systems and ideas to make our systems not only more cost-effective,
but also more accurate and efficient. The largest project was the
construction of our automated warehouse in Winona, MN, which came
online in the third quarter. In addition to giving us the storage capacity
we need to support future growth, this system is allowing us to reduce
labor costs, decrease shipping errors, and accelerate picking time so
our trucks can get on the road earlier in the evening. Based on the
success were seeing with this system and similar
systems previously implemented in our Dallas,
TX and Indianapolis, IN facilities, weve made a
decision to accelerate our investment in warehouse
automation and have started projects in both our
Atlanta, GA and Akron, OH facilities. In last years
letter, we reported that our distribution team was
focused on providing better service at a lower
price, and thats exactly what they accomplished
 yet again  in 2012. Not only did our distribution
costs grow far slower than sales, our accuracy also improved. This is
a direct result of the talented distribution team we have at Fastenal.
In other distribution-related news, our transportation team purchased
six compressed natural gas (CNG) semi trucks. These vehicles
arrived in the fourth quarter, and to date weve seen a nice cost savings
compared to our diesel-powered fleet. Our team will continue to monitor
the results of this program to determine how we expand it in the future.
The introduction of CNG technology is part of the teams larger goal of
increasing our transportation efficiency, and they made great progress
during 2012. Although fuel costs were at record highs for the year, we
moved our product at the lowest relative cost in company history.
Our information technology group not only keeps the computer
systems running, they also continue to develop new and better ways
to save time and improve our customer service. During the year they
developed a new software system that allows us to automate the
ordering and billing process for our vending machines. Weve spoken
with many store managers about this software, and everyone says it
is saving them a tremendous amount of time while improving their
accuracy. The IT group also developed a new inventory management
application for our larger volume stores. This system is very similar
to what we use in our distribution centers, and it allows the store to
pick orders faster and more accurately. These are just two examples of
the solutions this group is working on to improve our business. Most
people probably dont realize how much time employees spend on the
computer. Every time we develop ideas like these, we can potentially
free up thousands of hours per month that can be better spent serving
our customers and growing our business.
During the past year, our product development
team focused much of their time on two areas. The
first was our metalworking product expansion. In
2011 we made a strategic decision to increase
our investment in this product line in order to
create more customer opportunity. We chose this
area for several reasons, but the major reason is
that we have well-established relationships with
customers in nearly every market that are large
users of this product. Another reason is that
weve been selling these products for years, albeit with a much less
comprehensive offering than we have today. The product development
team has done a very good job of first determining which parts were
needed to fill in our offering, and then finding the best suppliers with
which to partner. Our goal for 2012 was for metalworking product sales
to exceed overall company growth by a minimum of ten percentage
points, and were pleased to announce that weve exceeded that goal.
The teams second area of focus has been the continual development of
Fastenals lineup of private label brands. This is an important initiative
because it allows us to provide our customers with high-quality
products at a competitive price. Although we still have a tremendous
amount of work ahead of us, were making very good progress with
this project.
To keep pace with the nonstop demand for new skills and knowledge
in the field, the Fastenal School of Business, our corporate university,
provided more training, to more employees, in more creative ways,
than ever before. Overall, Fastenal employees completed more than
8,600 face-to-face courses. Online training isnt new at Fastenal, but
2012 saw a major increase in participation. During the year, employees
completed more than 247,000 online courses, nearly doubling the
number of online courses completed in 2011.

With over 11,000 sales personnel working in locations throughout
North America and the world, it has become harder to communicate
effectively with everyone, so weve been working on several ideas to
improve communications. Our biggest effort in 2012 was to start a
twice-monthly conference where we bring 40 to 45 store managers
to our headquarters in Winona, MN. During this
two-and-a-half-day conference, they get to spend
one to two hours with each of our senior leaders
to learn about what were doing as a company,
with an opportunity to provide feedback and voice
concerns. On the second night we have a small
banquet which is attended by several of our senior
people, including Bob Kierlin, our founder  this is
really just a chance to visit and enjoy the evening.
During the year we were able to bring in nearly
700 managers for this program. Although this is a
significant time commitment for our store leaders,
it has proven to be time well spent, and we are
going to increase the schedule for next year.
In closing, here are four key reasons why were confident that we have
a great opportunity to continue to grow our business in the future. First,
we operate in a very large and fragmented market, and we believe there
is plenty of market opportunity for us to capture. Second, because we
have built a very profitable business, weve put ourselves in a position
where we can generate the cash necessary to finance future growth.
Third, weve invested in infrastructure and systems to support a much
larger volume in all areas of the business: IT, distribution, sourcing,
store locations, and many other areas. Fourth  and most important
of all  is our great employees. As mentioned at the beginning of this
letter, weve worked very hard to foster a decentralized culture, one
that allows people at all levels of the organization to make decisions.
We also strive to develop a sense of ownership by encouraging people
to be creative and provide solutions. This is coupled with a reward
system that benefits those who demonstrate they can perform at a high
level. The end result: rather than limiting important decisions to a small
group of managers, we have thousands of well-trained entrepreneurs
contributing ideas, providing solutions, and making smart decisions to
grow their business.
These are the people who developed the systems and achieved the
success weve been discussing in this letter. And they are the ones
who go the extra mile to service our customers.
Today, as always, customer service is what sets us
apart from our competitors, and for that we thank
everyone on the Fastenal Blue Team for all youve
done in 2012 to make our company better.
Thank you, also, to all of our loyal shareholders
for the years of support. Although 2012 was not a
great year by our standards, we certainly werent
held back by a lack of effort or desire  and were
already working hard on another year of Growth
Through Customer Service.
Willard D. Oberton, CEO Leland J. Hein, President