To Our Shareholders
April 22, 2016
Our mission at American Tower is to be the leading
provider of communications real estate in the U.S.
and select markets around the world while
delivering great results for our shareholders.
Our focus on this mission has positioned American Tower to increase the speed and effi ciency of
mobile voice and broadband deployments in the Americas, Europe, Africa and Asia�something
that every person in this Company is very proud of. Meanwhile, our disciplined, data-driven
capital allocation process has resulted in outsized growth and strong, consistent performance.
In 2007, after completing our merger integration with SpectraSite, which established
American Tower as a clear industry leader in the U.S., we accelerated our international
expansion efforts while continuing to pursue additional investments domestically. As a result,
we believe American Tower is the communications real estate provider best positioned of any
player in the industry to pursue the widest set of capital allocation opportunities. Given this
unique strategic position, we have delivered very strong growth in property revenue, Adjusted
EBITDA and Adjusted Funds From Operations (AFFO) since 2007, while simultaneously
increasing our Return On Invested Capital (ROIC) by approximately 40 basis points. 

These results are in large part driven by the broad
set of capital allocation opportunities available to
the Company, which primarily center on building
further scale while improving profi tability and
increasing operational effi ciency. While our fi rst
capital allocation priority remains our common
stock dividend, which has grown by over 26% on a
compounded annual basis since we introduced it in
2012, we have also achieved signifi cant growth by
pursuing accretive investment opportunities
through both our discretionary capital investment
program and global merger and acquisition
platform. Finally, when opportunities for investment
in assets did not meet our return thresholds and
our leverage was within our target range, we
deployed $3.6 billion through our share repurchase
program since the beginning of 2007.
Our executive management team carefully
allocates capital among these alternatives based
upon many factors, including demand trends for
communications real estate in countries that meet
our investment criteria, capital market conditions,
the availability and pricing of assets and the
Company�s fi nancial position.
In 2015, our major capital allocation initiatives
were to fund our growing common stock
dividend, devote over $700 million to capital
investment projects and complete three major
strategic transactions in our U.S., Latin America
and EMEA regions. Simultaneously, we deployed
nearly $600 million to repay existing indebtedness
as part of our commitment to our investment
grade rating.

Dividends
American Tower�s common shareholders received
approximately $770 million in dividends in 20151
,
or $1.81 per share. This distribution grew nearly
30% over the prior year, and it puts us in the top
decile of listed REIT dividend growth. Our 2015
distribution also continued our multiyear track
record of over 20% dividend growth per annum.
Capital Expenditures
Over 75% of our 2015 capital expenditures were directed toward
tower or small cell construction projects, the redevelopment of our
existing assets to support additional tenant revenue opportunities or
ground lease purchases or long-term easements that secure the
underlying property rights for our sites. Each of these categories of
capital deployment supports revenue growth and the profi tability of
our business through either additional revenue generation or
operating cost reduction.
Our skill and experience in new tower development and construction,
deep customer relationships and sales and marketing abilities have
historically driven strong tenancy growth and have enabled us to
generate high returns on investment. Each and every one of the new
build projects completed in 2015, for example, was based on an
anchor tenant contract commitment on day one and, on average,
generated an initial NOI yield of more than 9%. 

U.S. Transactions
Our assessment, using internal and external analyses, is that the
demand for mobile broadband service in the U.S. will continue to
grow at a rapid pace for years to come. Given the technologies
available, the limitations of Radio Frequency (RF) physics and the
population distribution and geography of the U.S., we also fully
anticipate signifi cant additional demand for tower space now and in
the future. It was on this foundation that we made the investment
decision to secure the rights to lease and operate approximately
11,500 sites from Verizon. This milestone transaction solidifi ed
American Tower�s leadership position in the U.S. communications
real estate industry.
Our investment decision regarding the Verizon sites was underpinned
by a number of important factors: excellent locations that are
attractive to additional mobile operators, favorable structural capacity
and ground space and a low initial third-party tenancy ratio. All of
these factors should contribute to strong, steady, long-term growth
for the Verizon sites, and we are already seeing growth rates that
exceed those of our legacy domestic portfolio.
Moreover, the Verizon transaction complemented our 2013
acquisition of Global Tower Partners (GTP) by further diversifying our
asset base and, as a result, is expected to elevate and extend the
trajectory of growth in our U.S. property segment over time.
International Transactions
As attractive as we believe the domestic investment environment to be,
we expect carefully evaluated international acquisition opportunities to
have even higher returns over time. Our view is grounded in the
expectation that in less mature mobile markets still reliant on third
generation (3G) or even second generation (2G) technologies, property
revenue can grow even faster for longer than in the U.S. or other
developed markets given the signifi cant incremental investment
required to build advanced mobile data networks. 

This expectation has been confi rmed in recent years, as our Organic
Core Growth1
 rates in our international property segments have
exceeded that of our U.S. property segment by an average of nearly
500 basis points per annum since 2012.
Consequently, we completed two signifi cant acquisitions during
2015 in countries representing the largest free market democracies
on their respective continents: Brazil in South America through our
acquisition of approximately 5,500 tower sites from Telecom Italia
(TIM), and Nigeria in Africa through our acquisition of over
4,700 tower sites from Bharti Airtel.
In addition, we reached an agreement in late 2015 to dramatically
increase our strategic position in India, the largest free market
democracy in Asia. This transaction will result in American Tower
securing a controlling majority stake in Viom and is expected to
subsequently lead to the combination of the Viom portfolio with our
existing ATC India operations. As a result, we will be the largest truly
independent communications real estate provider in India, operating
nearly 60,000 sites in that country.

A Harmonious Balance: High Growth,
Strong Returns and Financial Strength
American Tower deployed a total of $8.5 billion in capital in 2015:
9% to return cash to shareholders through our common and preferred
stock distributions; 9% for capital expenditures, including to support
the construction of over 3,200 new sites; 82% for the acquisition of
real property and sublease rights to over 22,000 additional sites.
At the same time, our effi cient and effective operation of existing
assets around the world resulted in driving double-digit growth in
property revenue, Adjusted EBITDA and AFFO per Share while
simultaneously maintaining a solid 9.4% ROIC as of the fourth
quarter1
. The strong performance of our business as well as our
historically prudent balance sheet management also led to a reduction
in our net leverage ratio to 5.2x net debt to Adjusted EBITDA on a last
quarter annualized basis2
. We therefore ended the year with both the
largest scale business and the lowest fi nancial leverage among our
U.S.-listed tower company peer group.
With the closing of the Viom transaction and the recently
announced transaction in Tanzania, American Tower will operate
over 140,000 communications real estate properties in 14 countries
on fi ve continents. Our Company has extensive and highly integrated
business relationships and long-term contracts with many of the
world�s leading Mobile Network Operators (MNOs), which we
expect to drive sustained cash fl ow growth on these assets. Our
expanding cash fl ow in turn supports our dividend that has grown
at a 26% CAGR since its inception.

We are now strategically positioned to ride the mobile internet
investment wave from 2G to 3G to 4G and beyond in many of the
world�s most populous free market democracies. We feel that all of
these attributes make American Tower the clear leader in our
industry and will drive ongoing growth and performance to benefi t
our shareholders far into the future.
James D. Taiclet, Jr.
Chairman, President & Chief Executive Officer

