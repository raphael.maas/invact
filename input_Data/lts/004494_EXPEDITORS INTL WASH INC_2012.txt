To Our Shareholders
2012  Another year, another anomaly. Apart
from coming through a difficult year, as usual,
we did pretty well. Tough times dont last,
tough people do, and we have an abundance of
those such people.

The bright, albeit bitter sweet, note was a
closure letter from the D.O.J.; one paragraph
indicating that we were no longer a subject
of their industry-wide investigation into anticompetitive
behavior. Five years and nearly
twenty million dollars later, were very pleased
to have emerged from this D.O.J. ordeal as we
thought we would, i.e. no adverse findings and
no fines or penalties. The one company that
got immunity got off free from their admitted
misdeeds and cost the entire industry millions.
Its a shame, but the saga is over.
Because of the global economy, 2013
should be only slightly better. As we enter
our 34th year, there is one word that lights
our company  ENDURANCE. When asked
whats in store for the future, the answer
is always the same, just more of the same.

Its not politically correct, and seldom are
we, to continue our hiring freeze. But,
first and foremost our commitment to
shareholder value must begin with our
commitment to protect the people who are
here. They are the ones who create that
value and they and their families come first.
I refrain from any political statements as
this letter would be ten pages in length. We
continue our improvement in I.T. solutions
with a great team and also have a very
robust compliance system globally.
This year we opened new offices in Santo
Domingo in the Dominican Republic Montevideo,
Uruguay, Copenhagen, Denmark and
Luxembourg. We also had an open house for
our office in Beijing which is state of the art.
Our carrier relations, both Ocean and
Air, are solid and we have great thirdpar
ty par tners. We continue to take
profitable market share without losing
any thing significant. Shipment count is up
despite smaller volume and less weight in
shipments. More market share in declining
market s is dif ficult to measure. As weve
shown in the past, the true measurement
of those down-market gains become most
acutely measurable when the market
turns and it will turn.
We will endure next year and as always we
thank our shareholders, our customers, our
carriers, and, most importantly, the best
people in the industry  ours.
Peter J. Rose
Chairman of the Board & CEO