TO OUR SHAREHOLDERS


The Company�s
performance in 2016
continued to reflect the
business momentum
generated through our
consistent, solutionsoriented approach to
banking. This approach,
in combination with
our extensive suite of
products and services,
differentiates differentiates Peoples�
United in the market
and enables usto
strengthen existing
relationships and
forge new ones. Loan
balances ended the year at $29.7 billion, marking the
seventh consecutive year of growth, and asset quality
remained industry leading as evidenced by net loan
charge-offs as a percentage of average loans of only
six basis points. Over this same seven-year period, we
have also nearly doubled both our deposit base to
$29.9 billion and discretionary wealth management
assets to $8 billion. These results are a testament to
the quality of clients we do business with and the
remarkable efforts of our employees.
Enhancing Profitability
Full year 2016 net income increased eight percent
from the prior year to $281 million, the highest
level in the Company�s history. On a per common
share basis, we reported $0.92, an increase of seven
percent from a year ago. The year benefited from
higher net interest income driven by ongoing loan
growth as well as our ability to effectively control
costs. As a result, the efficiency ratio was 60.5 percent,
a year over year improvement of 100 basis points.
Performance during the year generated a return on
average tangible common equity of 10.2 percent, an
increase of 20 basis points from 2015. Despite the
prolonged low interest rate environment, we have
achieved compound annual growth in net income
and earnings per common share of eight percent and
11 percent, respectively, over the last five years.
Expanding Presence in New York
The pending acquisition of Suffolk Bancorp and
recent addition of Gerstein Fisher align prominently
with our strategic priority of growing and
strengthening People�s United in the New York
Metro area.Suffolk is an excellent cultural fit and
complements our previous acquisitions and organic
growth in the market. Their impressive Long Island
footprint, particularly on the island�s eastern
end, where we do not have a presence, strong
client relationships and outstanding deposit base
will further bolster our franchise in this attractive
banking market. Gerstein Fisher, a New York Citybased investment management firm, with its
respected quantitative investment approach and
scalable technology-enabled platform enables us to
provide an even greater set of investment solutions
to clients. We are excited about the benefits of each
of these transactions, particularly the strong talent
joining our team.
Creating Long-Term Shareholder Value
Our commitment to building a strong banking
franchise for the long term is unwavering and
reflected in the Company�s strategic plan, which is
developed in partnership with the Board of Directors
each year. During the strategic planning process
we strive to balance and utilize six leversto create
shareholder value and move the Company forward.
These levers are:
1. Enhance client-focused capabilities: While we have
always successfully partnered with clients to deliver
thoughtful, straightforward solutions that address
their financial needs, we continually look for ways
to expand our capabilities. As such, an investment
was recently made in a leading edge customer
relationship management system to enrich client
experiences when interacting with our bankers and
product specialists. The platform will enable our
sales force to more efficiently meet client needs,
drive referrals, increase retention and strengthen
relationships. This investment will further improve
relationship profitability over time.
2. Grow the balance sheet: Our successful geographic
expansion, diversified business mix, investment
in talent and new business initiatives have led
to average annual loan growth of eight percent
over the last five years. Importantly, this growth
has outweighed significant net interest margin
compression and enabled net interest income,
excluding accretion, to grow at an annual rate of
seven percent over this same time period. Looking
ahead, we expect further loan growth as we continue
to leverage investments in the New York Metro and
Greater Boston areas, while also deepening our
presence in heritage markets such as Connecticut
and Vermont. 

3. Maintain excellent asset quality: Exceptional
risk management is a hallmark of People�s United.
Non-performing assets as a percentage of loans and
other real estate owned as well as net loan chargeoffs as a percentage of average loans continue to be
lower than the averages of our defined peer gro than the averages of our defined peer group
and the 50 largest banks in the country. We will not
sacrifice asset quality to achieve growth and remain
committed to the conservative and well-defined
underwriting philosophy that has served us well.
4. Diversify revenues: Because fee income is not
interest rate sensitive or subject to the dynamics
of the credit cycle, we have worked diligently to
strengthen this revenue source organically and via
acquisitions. This has been accomplished by investing
in banking infrastructure including foreign exchange,
trade finance and loan syndication platforms, while
recent acquisitions of Gerstein Fisher and two
insurance brokerage firms have bolstered our wealth
management and insurance businesses. We continue
to look for opportunities to enhance fee income and
aspire for 30 percent of our revenues to be derived
from such sources.
5. Control costs: The mindset of proactively
managing expenses has become truly ingrained
in the culture of the Company and has limited the
increase in non-interest expenses to an average
annual rate of less than one percent over the
last five years. We are especially proud of these
results, considering we have made investments in
revenue producing initiatives as well as covered
heightened regulatory compliance costs. While costs
are anticipated to move modestly higher in the
coming year, further improvement in the efficiency
ratio is expected, reflecting our continued focus on
enhancing operating leverage.
6. Deploy capital efficiently: Our prudent
management of capital has enabled us to grow
business organically while also consistently
returning capital to shareholders as evidenced
by 23 consecutive years of increases in common
stock dividends. We understand the importance of
the dividend to shareholders and valuation of the
Company�s shares. As such, we remain committed
to our current strategy of annually increasing the
common dividend.
Supporting Our Communities
At People�s United, helping communities where we
live and work to grow and thrive has always been
deeply embedded in our mindset. Through our two
foundations, People�s United Community Foundation
and People�s United Community Foundation of
Eastern Massachusetts, we award grants principally
within three areas of focus: community development,
youth development and affordable housing. In 2016,
our foundations together awarded over $3.3 million
to more than 500 nonprofit organizations througho more than 500 nonprofit organizationsthroughout
our footprint. People�s United Bank Community
Relations also gave more than $2.9 million in support
of local events and charitable initiatives.
In addition, through our Financial Education
Community Outreach Program, we delivered over 600
workshops to nearly 20,000 individuals to enhance
financial literacy and foster financial stability in our
communities. We are also excited about our new
unique educational collaboration with the AARP
Fraud Watch Network that aims to better protect
consumers in the northeast, particularly older adults,
against financial exploitation. Finally, we are once
again very proud of our employees� commitment to
their communities as demonstrated by the more than
30,000 volunteer hours they logged over the course
of the year.
Moving Forward
In 2017, People�s United celebrates 175 years of being
a trusted advisor, and while economic, competitive
and regulatory landscapes change over time, the
Company�s goal of helping clients achieve financial
success remains unchanged. The capability to offer
a full range of products and services, while also
providing exceptional �know-how�, continues to
make our franchise unique in the northeast corridor.
This differentiation enables us to build deep, multiproduct relationships that not only satisfy client
needs, but also enhances the Company�s profitability.
As such, we are confident in our ability to further
deliver exceptional service to clients and profitable
growth to you, our shareholders, in 2017 and beyond.
We appreciate your continued support.
Sincerely,
President and Chief ExecutiveOfficer
People�s United Financial, Inc.
March 1, 2017