Fellow Shareholders,
On behalf of the Perrigo management
team and the over 13,000 Perrigo
employees worldwide, I am extremely
proud to report our tenth consecutive
year of record earnings. By effectively
executing our Base Plus Plus Plus
strategy, we continued our consistent
track record of delivering superior
shareholder returns, with a total
shareholder return of over
970% since 2007.

Over the past eight years, we have transformed Perrigo
into a top five global OTC pharmaceutical company.
In fiscal 2015, we took significant steps to build on this
platform through three highly strategic acquisitions that
further strengthened our global scale and distribution
footprint while expanding our European OTC branded
product portfolio.
While I am proud of our track record of results, I am even
more excited to share with you the strategies we have
developed that will continue to drive Perrigos performance
as an independent company in the years to come, as well
as the mega-trends that will support that strategy.
I also want to address the recent attempts by Mylan N.V.
(Mylan) to acquire Perrigo. As we have consistently
maintained since April, we believe Mylans offer
substantially undervalues Perrigo and is not in the best
interests of our shareholders. This transaction would
impede Perrigos ability to realize the opportunities that
its current management team has developed. As a result,
we believe that we can best fulfill our mission to provide
Quality Affordable Healthcare Products on a global scale
to the benefit of all of our stakeholders as a standalone
company.
Key Fiscal 2015 New Product Launches
Drive Organic Growth
Perrigos unique value proposition is embodied in our commitment
to consistently deliver Quality Affordable Healthcare Products.
We do so through our world-class R&D capabilities, as well as
through strategic partnerships and acquisitions. We are proud to
have launched more than 150 new products during fiscal 2015,
led by outstanding execution from our R&D, supply chain and
sales teams. Each of our business segments contributed to these
product introductions, which made significant contributions to
fiscal 2015 performance.
In our Rx business, we launched generic versions of
clobetasol spray and our AB-rated testosterone gel 1%.
In our consumer-facing businesses, both XLS Max Strength,
a line extension to the brand XLS Medical for weight loss,
and the launch of Frontline Plus equivalent flea and tick
control for cats and dogs made significant contributions.
We also received approvals for our store brand equivalent
to Mucinex D and generic testosterone gel 1.62%.
These approvals demonstrate our ongoing commitment
to developing high quality products that deliver superior
value to retailers and consumers worldwide.

Confident in Growth of Our
Durable Base Business
Over the past several months, I have spoken frequently
about Perrigos Base Plus Plus Plus business model,
which focuses on effectively leveraging Perrigos strengths
as a standalone company, and which will enable us to
drive our short-term and long-term success. At the base
of that model are our unparalleled consumer-facing
businesses that, thanks to the addition of Omega Pharma N.V.
(Omega), now represent more than 75% of our overall
portfolio on an annualized, pro forma basis.
In addition to our consumer businesses, this base includes
our highly profitable Rx business and a Specialty Science
business that generates consistent royalty revenues
and strong cash flows.
Layered on top of our durable global base is a new
product pipeline that is expected to deliver $1 billion
of new product revenue over the next three years,
even before the inclusion of Omega. These elements
form the foundation of our future business and
are the basis of our three-year 5-10% organic
net sales compounded annual growth rate
(CAGR) goal.

Plus Plus Plus Growth Opportunities
In addition to base growth, we have identified $29 billion
of potential Rx-to-OTC switches over the next five years.
These opportunities were not included in our organic growth
goal and represent new product revenues that would be in
addition to the organic product launches planned in the
base business.
Plus, we fully expect to continue our efforts to expand our
product portfolio, geographic reach and operating leverage
through successful inorganic growth, acquisition opportunities.
Our focus is on those acquisitions that will add value.
Each opportunity is subject to stringent internal hurdles
for return on invested capital (ROIC) and debt capacities.
The Perrigo team has an outstanding track record in this
area, which has contributed to our historically balanced organic
and inorganic growth rates. Inorganic growth opportunities were
also excluded from our net sales growth goal.
Finally, we have potential upside from new indications for
Tysabri, as a treatment for Secondary Progressive Multiple
Sclerosis (SPMS) and stroke. These potential new indications
are also incremental to our base plan.
We have great confidence that our base business model,
when combined with our Plus Plus Plus growth opportunities,
will continue to drive Perrigo forward for many years to come.

Significant M&A Activities Drive Global Platform Growth
Omega Acquisition Transforms Perrigo
into a Top Five OTC Company
As you may recall, in December 2013, we established a
strategic foothold for expansion into continental Europe
by acquiring Elan Corporation, plc. That foothold then
served as a vital launching point for our acquisition of
Omega, a Belgium-based, branded OTC drug maker.
The Perrigo-Omega combination created a top five
global OTC pharmaceutical company with the operational
structure and cash flow generation to further accelerate
our international growth. Omega immediately enhanced
our global scale and broadened our footprint, providing
us with access to an established European commercial
network of 211,000 pharmacists, 105,000 retail stores
and 3,900 para-pharmacies.
This acquisition demonstrated our ability to execute on
our global strategy, diversified our revenue and cash
flow geographically, and provided access to the durable
European OTC cash-pay market. We are now able to
leverage the commercial infrastructure, supply chain
capabilities and financial strength of the combined
companies to pursue and execute highly synergistic
bolt-on transactions.
In particular, I am pleased to welcome the entire Omega
team to the Perrigo family, and look forward to drawing
on their considerable expertise to support our growth
initiatives throughout the European market.
Expanded European OTC Branded Products
and Mexican Capacity and Capabilities
In June 2015, within just three months of closing the
Omega transaction, we announced the acquisition of
several leading European OTC products in the areas of
nicotine replacement therapy, cold and flu, and cold sore
management from GlaxoSmithKline (GSK). These brands
had combined 2014 sales of approximately $110 million.
This acquisition was a direct result of the Perrigo-Omega
combination. Neither Perrigo nor Omega could have
executed the transaction as standalone companies.
Perrigos supply chain expertise, combined with Omegas
comprehensive European commercial infrastructure
and proven brand-building capabilities, allowed us
to be a perfect buyer for these assets and uniquely
position us to execute value-accretive deals such as the
GSK products acquisition.
In May 2015, we also acquired Patheons Mexican
operations, which further strengthened our Mexican
platform and supply chain capabilities with the addition
of softgel manufacturing technology. This acquisition
also served to broaden our leadership position,
product portfolio and customer network in Mexico.
Continued Leverage of Key Mega-trends
On a macro level, the favorable market position we hold
and the significant global scale we have achieved position
us well to capitalize on several key mega-trends.
As the global population ages and life expectancy
lengthens, the use of pharmaceuticals increases; in fact,
people over the age of 60 consume pharmaceuticals at
more than twice the rate of people under age 60. At the
same time, there will be fewer people in the 20-to-64
age group to help fund healthcare, which will require
increased efficiency in the healthcare system to make
it as cost-efficient as possible for both providers and
patients. Perrigo will play a significant role in addressing
the need for cost-efficient healthcare.
In addition, an increasing number of product categories
are poised to switch from Rx-to-OTC status. With OTC
medicines providing $102 billion of value to the U.S.
healthcare system annually, and store brand OTC products
delivering 8-9 times the savings over branded Rx products,
consumers will continue to benefit from switching to
OTC products and staying with them over the long term.
Again, we expect Perrigo to play a significant role in
these Rx-to-OTC switches.
Key Strengths in Diverse Markets
Create a Unique Value Proposition
Perrigos unique value proposition is supported by our
commitment to product development. Our new product
pipeline currently includes 136 filings awaiting regulatory
approval. In addition, our superior global supply chain,
manufacturing, distribution and vertical integration
capabilities all contribute to our cost efficiency and
provide a significant barrier to competitive entry.
Perrigos global scale is unmatched in the pharmaceutical
industry. Today, we produce more than 11,000 unique
formulas, and our ability to mass customize labels and
packages results in more than 26,000 stock keeping
units (SKUs) that deliver more than 50 billion medication
dosages a year. This mass customization capability is
unique in the healthcare industry and is a significant
contributor to our competitive advantage.
These competitive advantages reinforce our durable
business base and help drive our growth as store
brand market share continues to expand and additional
products switch from prescription to OTC status.
Before closing, I would like to briefly
comment on important events that
reflect our commitment to our durable
business model and our ability to
deliver superior value while being
responsive to our stakeholders.
Customer Driven
Organizational Optimization
In conjunction with the Omega acquisition, we
refined our organizational structure. Our new
reporting segments are: Consumer Healthcare
(store brand OTC, animal health, infant nutrition,
contract manufacturing and vitamins, minerals and
supplements, as well as our Israel pharmaceuticals
and diagnostic products business), Branded Consumer
Healthcare (Omega), Rx Pharmaceuticals, Specialty
Sciences (Tysabri royalties) and Other (active
pharmaceutical ingredients).
By defining reporting segments to reflect a more
comprehensive product portfolio and by streamlining
our sales organization, we are better able to serve
our diverse customer base.
Calendar-Year Realignment
As part of our ongoing commitment to be responsive
to our fellow shareholders and to align ourselves with
industry peers, the Perrigo board approved changing our
fiscal year-end to align with the calendar year, beginning
with the 2016 fiscal year. This change gives investors
the ability to more easily compare Perrigo with their
other investments, the majority of which report results
on a calendar-year basis. It also gives us the ability
to provide full-year guidance on a calendar-year
basis, which will streamline the integration process of
acquisitions already reporting on a calendar-year basis,
and is more consistent with the statutory and tax
year-end requirements of non-U.S. countries where
we do business.
Unsolicited Proposal and
Offers from Mylan
As I mentioned in my opening, in April 2015, Perrigo
received an unsolicited proposal and two subsequent
unsolicited offers from Mylan to acquire all outstanding
shares of Perrigo Company plc. Perrigos Board of
Directors unanimously believes that Mylans offer grossly
undervalues the Companys current cash flows, business
and financial platforms and future growth opportunities.
It presents clear risks and would result in value
destruction for shareholders, including by forcing them
into a corporate governance prison with extreme antishareholder
provisions. This was true in April and is still the
unanimous view of our Board of Directors today, now that
Mylan has launched its September 14, 2015 unsolicited
exchange offer, or tender offer, to acquire all of the issued
and to be issued ordinary shares of Perrigo. As we have
noted, Mylan has offered a value-destructive and dilutive
transaction that is a bad deal for all shareholders and we
strongly urge shareholders to take NO ACTION with
regard to Mylans offer.
In closing, let me reiterate that Perrigo
is in a unique position to continue to
deliver Quality Affordable Healthcare
Products to families worldwide.
Moreover, Perrigos experienced and
accomplished management team has
accelerated its proven growth strategy
through a consistently strong track
record for execution. We have a global
team of over 13,000 talented and
dedicated employees committed to
our ongoing success.
I believe our team is in the best position to realize the
full potential and power of Perrigos durable Base Plus
Plus Plus business model. Coupled with our diversified
businesses and global reach, we have unparalleled
ability to continue providing superior returns for
consumers, patients, retailers and shareholders alike.
As we look forward to the coming year of opportunities,
on behalf of the entire management team and all the
Perrigo employees, I thank you for your continued
confidence and support.
Sincerely,
Joseph C. Papa
Chairman, President and Chief Executive Officer
September 24, 2015