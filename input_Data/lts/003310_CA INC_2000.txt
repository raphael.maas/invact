                                                                                                                        With the acquisition of PLATINUM technology International, inc. and Sterling Software, Inc. this past year, two
                                                                                                                        of the largest acquisitions in the history of the software industry, we gained key technologies in the areas of
                                                                                                                        portals, business intelligence, application development, and infrastructure management that will blend with
  DEAR                                                                                                                  our products and services to enhance our ability to deliver complete eBusiness solutions.
SHAREHOLDER,                                                                                                            We also strategically combined our own experience and technologies with the expertise of other leading compa-
If you read the headlines this past year, it should be obvious that right now                                           nies to give us an edge in expanding into growing eBusiness market segments, such as the Application Service
we are in the midst of a tremendous business-transforming revolution made                                               Provider (ASP) and Business-to-Business (B2B) markets.
possible by the Internet. This eBusiness revolution is moving at lightning
speed, driving some businesses to rapid and unprecedented success.                                                      Both these areas promise significant opportunities as businesses increase their dependence on Web
                                                                                                                        applications to extend their reach outside their enterprise to customers, suppliers, and partners alike.
I' m pleased to report that we ended this fiscal year in our strongest financial                                        Our joint ventures with companies such as The Acer Group, Korea Telecom Hitel, Shanghai Telecom, Satyam
position ever. We experienced net revenue growth of 31% and grew our net income                                         Computer Services, Cable and Wireless HKT, and Nissho Iwai Corporation (NIC) leverage our collective
(excluding special charges) to over $1.5 billion, an increase of 17% versus last year.                                  expertise and establish a strong presence in these expanding areas.
We continue to run a very profitable business. EBITDA (earnings before interest, taxes,
depreciation, amortization, and special charges) was over $3.3 billion, and we generated                                From my perspective, however, success in the marketplace is only one way to measure CA. An equally important
cash from operations of over $1.5 billion.                                                                              measurement -- especially in terms of our strength for the future -- is our ability to attract and keep the best IT
                                                                                                                        professionals in the industry. And we are succeeding in this area as well. Not only do our employees demon-
This year we hope to further build on our past success and capitalize on the new opportunities presented by             strate their expertise and commitment to excellence on a regular basis, but we also receive numerous accolades
eBusiness and the Internet.                                                                                             for our progressive work environment, competitive salaries, employee benefits, and training programs.


Around the world, companies are frantically trying to understand how to put eBusiness to work for them -- immedi-       I'm particularly proud of CA's first-place rating for social responsibility in FORTUNE magazine's survey, because
ately. Because to survive during this revolution, let alone to succeed during it, there are two things organizations    it recognizes our long-standing commitment to humanitarian ventures. This commitment goes beyond the 200%
absolutely must do: they have to move fast, and they have to get it right. It's critical that they do both, because     matching contribution we make for any employee's charitable contribution. As a company, CA contributes to,
doing it wrong quickly is just as bad as taking forever to get it right.                                                and provides technological support for specific nonprofits such as the National Center for Missing & Exploited
                                                                                                                        Children, The Smile Train, the Make-A-Wish Foundation,  and Habitat for Humanity. We do this because we
We all understand what it means to be fast, but what does getting it right really mean? It means using visualization    believe that along with our success comes a responsibility to our global community. And we take this commit-
and dynamic intelligence to provide customers with a unique experience. It means integrating all business systems       ment just as seriously as we take the commitment to our customers and to our employees.
          to make the entire process seamless from the customer's perspective. And it means having a 100%
          reliable and secure eBusiness environment, even over the Internet. At CA, we have the technologies,           As we speed ahead delivering the promise of eBusiness to our customers, these commitments won't change.
          products, and services that ensure our customers can achieve these goals -- fast. CA is uniquely able         We'll continue to innovate and to form alliances with other companies to bring the best technology at the best
          to provide our clients with a comprehensive solution to their eBusiness needs.                                value to our customers. We'll continue to provide our exceptionally talented personnel with the best work
                                                                                                                        environment and benefits. And, we'll continue to share our success with worthy humanitarian groups.
          Jasmine  ii, our eBusiness platform, provides a sound basis for the seamlessly integrated eBusiness
          solutions our customers need. Combined with the dynamic intelligence of Neugents, TM our patented neural      I want to thank you, as always, for your past support and hope that the following pages will help you under-
          network technology, Jasmine ii gives us a strong presence in the dynamic eBusiness market.                    stand the vision we have for eBusiness and the technologies we deliver that will help guarantee our customers
                                                                                                                        are strong, successful participants in the eBusiness revolution. With their success will come CA's success
          And thanks to interBiz, TM the CA eBusiness application division designed to assist clients, suppliers,       as we work to enhance your shareholder value.
          and partners in capitalizing on eCommerce business opportunities, these products are deployed by
          BizWorks, TM the eBusiness Intelligence Suite, to provide integrated applications to this demanding market.   Very truly yours,


          The Unicenter TNG  family continues to be the industry-leading enterprise management solution for man-
          aging Web infrastructures. Its Neugents technology provides intelligent analysis and predictive abilities
          that give our customers the edge when it comes to critical areas, such as resource management, reliability,   Charles B. Wang
          and performance. And, eTrustTM addresses the critical need for end-to-end Internet security. All of these     Chairman and Chief Executive Officer
          systems management areas are, of course, all that much more important in this new eBusiness world.            Computer Associates International, Inc.
