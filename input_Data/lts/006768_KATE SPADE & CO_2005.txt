DEAR STOCKHOLDERS, ASSOCIATES, BUSINESS PARTNERS AND FRIENDS:
Our Company ended 2005 with its 40th consecutive quarter of sales growth, a sales increase of 4.6 percent and
a healthy balance sheet.
For the full year, diluted earnings per share increased 3.2 percent which included a $0.10 reduction in earnings per
share associated with the adoption of FASB 123R and the shift in the composition of our equity-based compensation
from options toward restricted stock.
While these results were positive, they were not as strong as we had anticipated earlier in the year.
A Challenging Environment
These are challenging times.The apparel market was relatively flat in terms of overall unit sales as shoppers spent
more of their disposable income on electronics and their homes. Relentless retail consolidation continued, led by the
Federated-May and Sears-Kmart mergers and the sales of Neiman Marcus, Proffitts, McRaes and Carson Pirie Scott.
But even given these adverse conditions, we could have used to greater advantage the strengths that make us uniquely
suited to prosper in an inhospitable environment.We realize that we must do more to leverage our considerable
competitive advantages.You should know that we are moving quickly to do so throughout our Company.
Competitive Advantages
The diversity and quality of our brand portfolio, supported by our core competencies in design, marketing,
manufacturing, sourcing and distribution, are what set us apart and ahead of our competitors.
These resources work to make each brand far stronger as part of our Company than it would be on its own.
Whether developed organically or acquired, big or small, start-up or established, each brand in the portfolio
benefits from the full support of a well-honed infrastructure and a management team that excels at building and
extending well-defined brands into true lifestyle offerings, available to consumers worldwide via multiple channels.
Our portfolio continues to grow to address evolving consumer preferences and underdeveloped market niches.
In November 2005, we acquired Prana, one of the strongest-trending brands in the high-growth active and yoga
industries.Through retailers specializing in these product categories, Prana takes us into a new channel of
distribution that reaches younger consumers with active, outdoor lifestyles. In January 2006, we added Mac & Jac,
Kenzie and Kenziegirl to our portfolio.These three brands, sold in specialty and department stores, offer fashionforward yet classic styling in premium apparel for work, nights out, weekends and special occasions.
The portfolio approach guides our accessories and licensing initiatives, as well.The Accessories Division offers
handbags, jewelry and fashion accessories under 24 brand names. And from shoes and swimwear to eyewear and
home products, we license our brands in more than 70 categories to expand their lifestyle appeal and relevance.
Diversification is also key to our distribution. Our brands are sold in over 30,000 points of sale around the world,
including upscale and mainstream department stores, mid-tier chains, specialty, mass merchandiser and outlet stores,
catalogs and onlinein short, wherever consumers elect to shop.
Our size, resources and advanced systems add further competitive advantage in a world where fewer but larger
and more sophisticated retailers demand seamless collaboration. In 2005, we reaped the benefits of past
investments in our technology and logistics infrastructure and launched new initiatives that will enhance our
operations in the years to come.
Lessons Learned
As a result of our less than stellar performance in 2005, we learned a number of important lessons that are leading
to change in key areas of our business and which I will discuss in greater detail later in this letter.
We are intensifying our effortsalready well under wayto focus on opportunities beyond U.S. department
stores, with a particular emphasis on international markets and direct-to-consumer retailing.
We are restructuring to improve performance, reduce costs and create a flatter, faster, more responsive
organization that is more closely aligned with customer and consumer needs.
We are fundamentally changing our supply chain to respond more quickly to market and fashion trends.
International Strength
Our global presence continues to grow and now accounts for 26 percent of our sales. Thirty-eight of our brands
are sold in various retail formats in 80 countries.
In 2005 our international performance was strong, with Mexx leading the way. Offering distinctive styling
and exceptional value, this brand turned in superior results in its core Western European markets and substantial
growth in Eastern Europe.
We continue to press forward with transforming the Mexx organization into a multi-brand platform to support
businesses that can benefit from Mexxs regional market expertise.While this is moving more slowly than we
would have liked, in the years ahead we believe it will enable brands like Juicy Couture, Lucky Brand Jeans, Ellen
Tracy, Liz Claiborne and Enyce to develop much stronger presences throughout Europe.
Also via our Mexx platform, we are pursuing growth through a partnership with a leading Middle Eastern developer
who is among the first to have successfully brought mega-malls to the region.This will allow us to enhance our
presence in a number of under-penetrated but high-potential markets across the Middle East, including Saudi Arabia
and the United Arab Emirates. Longer term, this alliance will help us develop in other major markets with favorable
demographic and economic potential, such as Pakistan and Egypt.
Closer to home, our business has grown 25 percent in Canada and 30 percent in Mexico, with both Mexx and
Liz Claiborne contributing substantially to these gains. Our strong operations in these markets are good examples
of how a multi-brand portfolio provides the flexibility needed to cater to national or regional preferences.
Mac & Jac, our newest acquisition, will further strengthen our presence in the Canadian market, while its retail
stores in Shanghai and Beijing will provide experience and guidance for other brands in our portfolio as we seek
to reach Chinese consumers in the future.
Direct-to-Consumer
We are committed to the continued development of our direct-to-consumer businesses because we believe this is
critical to better controlling our own destiny and addressing changes in consumer shopping habits. Increasingly,
consumers are opting to shop at specialty stores that present products in the most engaging and relevant ways.
Simply put, we frequently have great product, but department stores dont always buy or present it to maximize
its appeal to the ultimate consumer. Our ongoing investments in direct-to-consumer businesses, while capitalintensive, will enable us to position and support our brands to best advantage.
In 2005, direct-to-consumer sales accounted for 25 percent of total revenues, up from just 18 percent in 2001.
Here again, the portfolio strategy works to our advantage, with 11 brands in multiple formats allowing us to suit
every taste and pocketbook.To name a few: Lucky Brand Jeans stores combine fashion with a contemporary spin
on rocknroll; European styling sets the theme for our Mexx stores; Sigrid Olsen stores blend art and fashion;
Juicy Couture offers sexy, luxurious basics; and Elisabeth stores provide the plus-size woman with a shopping
experience and an array of products for her special needs.
The flexibility of our retail portfolio also helps us use real estate to best effect. If a particular location can perform
better for one brand than for another, we can switch. Instead of forcing growth by putting one brand into more
malls than really suit it, we can be selective, offering the most appropriate brands in fewer locations and leveraging
our influence with developers who seek fresh new store concepts. But for all the differences in our stores,
they benefit equally from a common support structure: sourcing, systems, leasing, store design and the like.
Turning to specific results, our Lucky Brand Jeans stores again performed very well, now with 14 consecutive
quarters of double-digit comp store sales.We have opened 102 stores since acquiring this brand in 1999 and plan
to open approximately 35 more in 2006. Our Sigrid Olsen specialty business turned in encouraging results, too,
and we intend to expand this concept aggressively, adding 25 to 30 new stores this year.
In the coming months, we will also expand our Juicy Couture retail stores to build on their phenomenal success,
carefully selecting locations in affluent venues that reinforce the cachet essential to the brands appeal. From a base
of three Juicy Couture stores today, we plan to open eight to ten new locations in international and domestic
markets in 2006.
In Europe, Mexx is our most fully developed direct-to-consumer brand, with 141 Company-owned stores in 13
countries and 291 concession stores throughout Europe. And another European success story is in progress with
Monet and Co. retail stores offering among other brands, Liz Claiborne, Monet and Ellen Tracy jewelry, handbags
and fashion accessories to consumers in France and the Benelux countries.
In the rapidly evolving world of e-commerce, we are pleased to report that our business was up 34 percent over
the previous yeardriven by the online offerings of Liz Claiborne, Lucky Brand Jeans, Elisabeth, C&C California,
Claiborne, Mexx and Prana.We are excited about the e-commerce opportunities for these and other brands in 2006.
A Hybrid Approach
While direct-to-consumer is an important growth engine, this does not mean that our wholesale businesses are
receiving less attention. Indeed, our approach is most aptly described as a best of both worlds hybrid, with
wholesale and retail models each making major contributions.
On the wholesale front, while we have less control, we generate the substantial returns on capital that have long
distinguished our Company.That is why our portfolio will continue to include multiple brands that resonate in
the department store environment.
This hybrid approach enables us to offer our products (some 300 million units each year) wherever consumers
choose to shop, be it department, specialty, outlet, concession or franchise stores, through the mail or online.
Within this multi-channel complex, an increasing number of our brands, including Mexx, Lucky Brand Jeans,
Liz Claiborne and Juicy Couture, successfully bridge the wholesale and direct-to-consumer formats.
Wholesale Brand Performance
Because Liz Claiborne is the Companys namesake and largest wholesale brand, it warrants special mention.We have
had our challenges here in recent years, but in spite of retail consolidation and increased emphasis on private labels,
Liz Claiborne continues as a pillar of the Better zone in mainstream U.S. department stores. In fact, with strong
cross-category performance in accessories and licensed products, Liz Claiborne is one of the best-selling fashion
brands in department stores.There is also no doubt that the brand continues to have a uniquely broad appeal
ranked sixth in consumer awareness in the Womens Wear Daily 100, a statistically valid survey of women. Up two
spots from last year, Lizs ranking is higher than that of any other fashion brand. In sum, this remains a truly great
brand, and we are moving to improve performance with enhanced, updated product and added value.
Beyond Liz Claiborne apparel, there are good results to report for most of the brands we sell to our wholesale
customers. In particular, some of our strongest results have come in upmarket venues such as Bloomingdales,
Neiman Marcus and Saks, where consumers are willing to pay for luxury and status brands.
In the better denim zone, we were again very successful as both Lucky Brand Jeans and DKNY Jeans finished nicely
ahead of 2004.These gains are notable in light of the great number of new entries competing for denim dollars.
Juicy Couture remains hot, posting excellent sales in womens and encouraging growth in the newer mens and
kids businesses.Among the many things that set Juicy Couture apart is the brands ability to consistently generate
buzz through cost-effective non-traditional marketing. Since acquiring this brand, we have built upon Juicy
Coutures aspirational image with extensions into accessories, sunglasses, swimwear and shoes.We are also
launching a higher-priced Juicy Couture designer line called Couture Couture, to be sold in select specialty and
department stores; and in the Fall of 2006 we will introduce the first-ever Juicy Couture fragrance.
In our mens lines, Enyce scored double-digit increases in department stores, while Axcess/Mens at Kohls and
Crazy Horse/Mens at JC Penney both had strong sales. For 2006, our mens focus remains on increasing our
footprint and penetration across multiple channels while adding product extensions that create seven-day-a-week
lifestyle brands.
Lastly, in a year when the category suffered, our popular-priced womens apparel thrived. Emma James and
J.H. Collectibles scored impressive gains in mainstream department stores, First Issue at Sears was best in its class,
and Axcess at Kohls was a true standout, with expanded career offerings and extensions into non-apparel lines.
Moreover, as mid-tier retailers like Kohls, Sears and JC Penney increasingly focus on lifestyle brands, we are
expanding our lines accordingly to include a range of products from refined sportswear to denim and a full
complement of accessories and licenses.
Accessories and Licenses
Our accessories and licensing portfolios continued to experience excellent growth as they extended the lifestyle
appeal of our apparel names.This strong performance was driven by our ability to preserve the identity of each
brand while applying it to non-apparel classifications.
If our Accessories Division were a stand-alone entity, it would be among the largest non-apparel fashion companies
in the world. Standout performers include Lucky Brand and Juicy Couture handbags and jewelry, as well as
Kenneth Cole jewelry, with Liz Claiborne accessories also remaining strong.We are especially excited about the
launch of the Liz Claiborne leather handbag collection, which has gained a great following since its debut in
September 2005. On the mid-tier front, from Crazy Horse to Villager to Axcess, we had an exceptional year,
with double-digit increases across handbags, jewelry and fashion accessories.
In 2005 we continued to expand our licensed brand portfolio, working with best-in-class partners to extend the
reach of our brands in categories where we do not have a core competency. Sigrid Olsen Home and Enyce Kids
both recently launched, and early results promise great potential. Also, Laundry licenses in shoes, coats and bridal
fared well in 2005. Juicy Couture licenses took the brand into completely new categories with products like
a custom-designed Juicy Couture T-Mobile Sidekick.
Restructuring for Growth
In the first quarter of 2006, we undertook a rare but necessary restructuring of our Company to address the
changes taking place in our industry. By streamlining decision-making and redeploying resources, we will,
of course, reduce costs. But importantly, we have set the stage for growth and more efficient management of our
complex multi-brand, multi-channel and global businesses.
In the new structure, many brands and operations will be grouped according to consumer offerings and channels
an approach that retains key brand-specific functions, such as design, merchandising and marketing, while
benefiting from shared planning, sales, sourcing and consumer research.These significant changes build competitive
advantage by making us more nimble in a rapidly changing marketplace, and thus better able to capitalize on
growth opportunities. Of course, we will continue to pay particular attention to power brands like Juicy
Couture, Lucky Brand Jeans, Mexx, Sigrid Olsen and Liz Claiborne, all of which operate in a global, multichannel, multi-category environment.
It should be noted that as part of our restructuring, we are fundamentally changing the way we manage our
supply chain. By moving key aspects of the product development process closer to manufacturing sites and using
sophisticated global communications technology, we will increase the speed with which our products progress
from the design concept to the final consumer, thereby enhancing our ability to react quickly to shifts in
demand and distribution patterns.
Role of Technology
At the heart of this supply chain evolution is proprietary technology built on a centralized platform known as
the Product Lifestyle System. Consistent with our long-standing and industry-leading commitment to superior
technological infrastructure, our new systemcalled VelociT will facilitate faster, boundary-less collaborative
product development around the globe, further strengthening our competitive position by significantly cutting
time and cost.
This technology infrastructure also plays an important role in our growing retail businesses via Customer
Relationship Management (CRM). This register-linked system for gathering data gives us heretofore unavailable
insight into consumers attitudes and behaviors. It tells us who is buying whatsize, color and styleand when.
The result is an enhanced ability to market directly to consumers and to test our promotional offers for maximum
efficiency and effectiveness. Coupled with VelociT, our CRM-enhanced understanding of tastes and habits enables
us to increase the productivity of our database marketing programs.
We Are Brand Managers
At this writing, we offer consumers 46 well-defined brands that meet fashion needs from classic to contemporary,
relaxed to active and denim to streetwear. But more than the number of brands is their quality. Each is a leader in
its category, sought after by consumers for its cachet, unique styling and value.
We take a consumer-products approach towards managing this fashion company. Our brand management system is
driven by consumer understanding and emulates the methods and disciplines used by leading packaged goods
companies. For each brand, this holistic approach, rooted in a clear positioning, goes far beyond product design to
connect those activities that add value for which consumers are willing to pay. These encompass everything from
concept development to in-store presentation, including manufacturing, product flow, sales and marketing,
customer service, licensing and line extensions. The resulting brand-centricity ensures that each brand under
our roof remains distinctive and relevant to its consumers.
Our People
In the end, our success depends upon the ability of our people to create and execute strategies that respond to an
ever more complex and demanding marketplace.This is why we focus considerable resources on developing our
associates and on cultivating and rewarding talent wherever it is found in the organization. For example, through
the Leader Studio, a unique program designed to enhance the leadership skills and competencies of high-potential
members of our team, we develop the bench strength and depth of management that allow us to fill needs
around the globe with the right executives or associates.
We also seek to build for the future by recruiting top talent as early as possible, in part through a highly competitive
college intern program. In past years, we have converted nearly one-third of the participants into full-time
associates, an extraordinarily high success rate. These and other programs recently landed us on the national search
firm Hewitt Associates list of the top 20 best companies for leaders. Lastly, enhancing ethnic diversity is a high
priority of ours, and while we have made real strides, more remains to be done.
The Year Ahead
Because not all consumers are alike in their style and shopping preferences, we firmly believe that our multi-brand,
multi-channel strategy is the right one. Execution, however, needs to improve, and we now have in place a more
agile organization that is better prepared to address the rapidly changing marketplace.
Our brands, with more to come, will lead the way across categories, channels and geographies, and we will
relentlessly focus on offering superior quality, high value and consumer-relevant products wherever and however
our consumers prefer to shop.Within these parameters, we will plan conservatively as the coming months reveal
a clearer picture of the meaning and impact of retail consolidation.
As in the past, we will seek to acquire brands that make financial sensenot just any brands, but those with
distinct positioning and untapped potential. Just look at Juicy Couture, originally sweatsuits, Sigrid Olsen, once
tops, and Lucky Brand Jeans, previously denim bottoms. All are now true lifestyle brands.
Ultimately, all these initiatives will enable our Company to excel in managing the complexity inherent in the
fashion business. In this way, we will prosper.
Finally, I announced in January that I have decided to retire at the end of 2006. Over the past 12 years, I have had
the privilege of working with a talented team of associates to help transform our organization from a Company
dependent upon a namesake brand sold almost exclusively through U.S. department stores into a powerful
multi-brand, multi-channel, multi-geography business. It is the quality of this group and the foundation we have
built together that gives me such confidence in our future. As you may be aware, our Board is conducting
a thorough process to identify my successor and ensure a seamless transition. However, be assured that until I leave,
I remain as fully engaged and committed to leading this great Company as I have been in the past.
Thank you for your continued support.
Sincerely,
Paul R. Charron
Chairman of the Board and Chief Executive Officer