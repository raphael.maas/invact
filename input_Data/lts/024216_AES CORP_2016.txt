Chairman and CEO letter
to AES Shareholders

We are at a time of immense change and excitement
in our industry. The power grid of the 21st century will
look very different from what we have seen in the past,
offering cleaner and more affordable energy. Renewables
are increasingly cost competitive and have become the
dominant type of new build generation in most of our
markets. Existing thermal generation and energy storage
will become more integrated with newer renewable
sources to meet challenges posed by the intermittent
nature of wind and solar generation. We also see a
growing demand for natural gas, with the potential for
gas to replace oil-fired generation in several markets.
Our actions over the past several years have put AES in a great position to
be a premier provider of energy solutions across our markets during this
time of immense change. As we seek to meet the evolving needs of our
energy customers, we benefit from a strong balance sheet, a unique and
diverse global platform, proven success in deploying new technologies and a
talented global workforce.
In 2016 we expanded our renewables portfolio and grew our world-leading
battery-based energy storage business. We began selling our proprietary
Advancion storage platform to utilities and other customers. We have
now deployed Advancion for projects we own and operate across seven
countries, with 436 MW in operation, under construction or in advanced
stage development. We also broke ground on a new gas-fired plant and
LNG regasification facility in Panama, enabling us to play a leading role in
delivering cleaner and lower-cost natural gas throughout Central America.
We have deployed new technologies such as drones and robotics to improve
safety and lower costs for our businesses, and we are continually seeking
innovative applications of technologies to improve our performance.
We continued to make progress on our strategic objectives throughout
2016. Our asset sales program has now achieved $3.6 billion in total sale
proceeds, allowing us to refocus our activities on select markets in which
we have the greatest competitive advantage. In the process, we achieved
our incremental $50 million cost savings and revenue enhancement target
and we are on track to reach $350 million in annual savings by 2018. We
brought on-line 2,976 MW of construction projects in 2016 to drive future
growth. We also achieved our financial guidance, reduced our corporate
debt and increased our dividend.
Despite 2016 being a successful year overall, we did experience some
headwinds. Several of our construction projects were delayed, most notably
the Alto Maipo hydroelectric project in Chile, which required a restructuring
of its financing and Power Purchase Agreement (PPA). We also had large
impairments at DPL in Ohio and our Buffalo Gap wind projects in Texas,
which reflect unsuccessful past investments.
The progress we did make in 2016 on our strategic objectives was reflected
in our stock performance. We had a very positive year and generated total
shareholder return of 26.3%, outperforming the S&P 500 (up 12.0%), the
S&P Utilities Index (up 16.3%) and U.S. listed independent power producers
(down 17.2%).

2016 RESULTS AND ACCOMPLISHMENTS
Turning to our specific results and accomplishments in 2016, we delivered
on our financial guidance, continued to meet our commitments to our
shareholders and extended our progress on our strategy. In terms of future
growth in free cash flow and earnings, we expect 8%-10% growth in our
financial metrics through 2020.
As a result of our actions in 2016, we generated Proportional Free Cash Flow
of $1,417 million and Parent Free Cash Flow of $579 million. As expected,
our Adjusted Earnings Per Share was below our 2015 results, primarily due
to headwinds from foreign currency and commodity prices, as well as a
continued decline in demand in Brazil. We were able to offset much of the
impact, and our 2016 Adjusted EPS came in at $0.98. During the year, we
also achieved many of our strategic objectives. In addition to the cost savings
and completion of construction projects previously mentioned, we also:
 Announced or closed $500 million in proceeds from the sales or selldowns
of eight businesses;
 Increased our quarterly dividend by 9.1%, to $0.12 per share, beginning in
the first quarter of 2017; and
 Advanced 3,389 MW of construction projects which are expected to
come on-line through 2019.

SUCCESSFUL EXECUTION OF OUR STRATEGY
Since late 2011, we have successfully delivered on our strategic goals and during
2016 we extended our progress. The actions we have taken since 2011, such
as cutting our costs, de-levering and reducing our complexity through asset sales
have put us in a strong position to execute on our strategic growth opportunities.
Reducing Complexity
We have been rebalancing our business mix by exiting certain businesses
to reduce risk and re-deploying our excess cash into debt repayment, share
repurchases, dividends and growth projects with long-term, U.S. Dollardenominated
contracts. Since 2011, we have raised $3.6 billion in equity
proceeds from asset sales, decreasing the total number of countries where
we have operations from 28 to 17.
Leveraging Our Platforms for Long-Term Growth
We are focusing our growth on platform expansions in markets where we
already operate and have a competitive advantage to realize attractive
risk-adjusted returns. Since 2011, we have brought on-line 7,932 MW of new
projects, including 2,976 MW of platform expansion projects in 2016 alone.
We currently have 3,389 MW under construction, representing $6.4 billion
in total capital expenditures, with the majority of AES $1.1 billion in equity
already funded and expected to contribute a targeted return on our equity
of approximately 14%. These projects under construction are the most
significant driver of our near-term growth.
Allocating Capital in a Disciplined Manner
Our top priority is to maximize risk-adjusted returns to our shareholders, by
taking a long-term perspective on investing our free cash flow.
Since 2011, we have generated substantial cash by executing on our
strategy, which we allocated in line with our capital allocation framework:
 We used $2,263 million to prepay and refinance Parent debt;
 We returned $2,402 million to shareholders through share repurchases
and a quarterly dividend; and
 We invested $1,336 million in our subsidiaries, largely for projects that
are currently under construction.
We remain committed to strengthening our credit, which is largely driven
by the successful completion of our construction program, cost reductions
and de-levering. Accordingly, we continue to be confident that we can
achieve investment grade stats by 2020.

CREATING LONG-TERM SHAREHOLDER VALUE
AES strives to create long-term shareholder value by providing safe and
reliable electricity-related services in the markets we serve. We have a
unique portfolio of businesses with significant presence in both mature
and rapidly growing developing markets. This makes AES well positioned to
deliver attractive long-term growth in cash flow, dividends and earnings.
Underlying Strengths of Our Businesses
As a result of our proactive contracting and portfolio rebalancing initiatives,
today, about 80% of our business is U.S. Dollar-based and about 83% is
either contracted generation or regulated utilities. The average remaining life
of the PPAs at our contracted businesses is six years, and when we complete
our projects currently under construction, this will be extended to ten years.
Long-term PPAs provide stability and predictability to our earnings and cash flow.
The majority of our businesses are low-cost, flexible and reliable electricity
providers with strong locational advantages. Our knowledge of these
markets and critical mass also puts us in a position to take advantage of
growth opportunities or quickly respond to changing conditions.
Future Growth
Future growth across our markets will be heavily weighted towards less
carbon-intensive natural gas, wind and solar generation. We will seek
opportunities in these growth areas for projects with long-term, U.S. Dollardenominated
contracts. The targeted projects will improve the quality of
our cash flow and help us achieve our credit objectives, while reducing the
carbon intensity of our portfolio.
With respect to renewables, we see an attractive business opportunity in
light of several trends, such as:
 The dramatic drop in the cost of renewables that have made their energy
production competitive;
 Growing demand for renewables  renewables have become the dominant
type of new generation build across almost all of our markets; and
 The availability of long-term PPAs for renewables, providing predictable,
stable cash flows.
The intermittent nature of renewables does pose challenges for the grid.
We believe AES ability to leverage renewables by integrating them with
conventional generation and energy storage can meet these challenges and
give us a strong competitive advantage.
To that end, we recently agreed to acquire sPower, the largest independent
solar developer in the United States. sPower brings 1.3 GW of installed
capacity with an average remaining contract life of more than 20 years with
very credit-worthy offtakers and a first class management and development
team with a pipeline of more than 10 GW. We are very excited by the scale
and capabilities from this acquisition as we look to expand our renewables
portfolio across all our markets and integrate renewables with our extensive
conventional generation fleet and world-leading energy storage platform.

DELIVERING SUSTAINABLE RESULTS
As a leading sustainable power company, our diverse mix of generation
sources provides us the strength and flexibility to adapt to local and
regional market needs, maximize plant efficiency and deliver reliable,
affordable electricity. Our businesses do much more than just provide
power. Improving lives and making a lasting difference in the communities
in which our businesses operate has always been part of our values and
mission. It is our responsibility to provide infrastructure solutions that
support a sustainable social, economic and environmental future. Our
sustainability activities focus on specific areas, or material aspects, within
the context of five broad strategic initiatives:
 Financial Excellence
 Operational Excellence
 Environmental Performance
 Stakeholder Engagement
 AES People
Our values are at the heart of our operations and these values set us apart
from others in our industry. Every day, our people and businesses around
the world are guided by the following core values:
 Put Safety First
 Act With Integrity
 Honor Our Commitments
 Strive for Excellence
 Have Fun Through Work
Putting safety first for our people, contractors and communities, is our
number one value. Unfortunately, 2016 was a very disappointing year for us
as we experienced a major setback in our safety performance. Eight people
from our generation and distribution businesses, as well as construction
projects, died as a result of workplace injuries. We have a plan in place to
take the information we have from each of these events and use it to help
us deliver on our goal to create a workplace free of incidents.
In 2016, AES was named to the Dow Jones Sustainability Index (DJSI) for
North America for the third year in a row by RobecoSAM. We were also
named by Ethisphere as one of the Worlds Most Ethical Companies for the
third year in a row.
CONCLUSION
Our global platform, diverse portfolio mix by generation sources and
technologies, and leading presence in many growing markets, make us well
positioned to meet evolving customer needs and maximize shareholder value.
We will continue to execute on our strategic objectives and seek to be the
low-cost operator of assets in attractive markets, while exercising disciplined
capital allocation that strengthens our credit and reduces overall volatility.
Thank you for your continued support.

Charles O. Rossotti 
Chairman of the Board, March 1, 2017 

Andrs Gluski
President and Chief Executive Officer
March 1, 2017 

