Dear Shareholder,
In 2013, ADI delivered solid profitability and shareholder
returns despite the uncertain macroeconomic environment
that transpired. While fiscal year 2013 revenue of
$2.6 billion was 2% below the prior year, net income
increased by 3% and sales in the second half were 6%
higher than the first half, which we believe are positive
signs for the year ahead.
Net income for fiscal year 2013 totaled $673 million.
Diluted earnings per share were $2.14, rising slightly
from 2012. Cash flow from operations was $912 million,
or 35% of revenue; and over $460 million was returned
to shareholders, primarily through cash dividends. ADI
shareholders earned $1.32 per share in dividends, a 15%
increase over the prior year, and the total shareholder
return was 28%.
As always, we were guided in 2013 by our long-term strategy to drive superior results through superior innovation. During the
year, we sharpened our focus on developing the signal processing systems most critical to our customers, with the highest
potential to sustain the returns we expect of our investments. As such, we increased investments in industrial, communications
infrastructure, and automotive applications, which together accounted for 85% of our revenue in fiscal year 2013. We also
increased investments in the advanced technologies that we believe will secure ADIs leadership position far into the future.
These increases were primarily achieved by redirecting resources, enabling us to keep expenses under tight control in the
uncertain economic climate.

ADIs enduring stability stems from a
diverse business and a strong technology
foundation grounded by exceptionally
talented employees who thrive on creating
innovative solutions to our customers
toughest challenges. Technical innovation
is deeply rooted in our culture and is our
primary strategy for not only maintaining
leading market share in converters and
high performance amplifiers, which
together represented 71% of revenue in
fiscal year 2013, but also advancing our
portfolio of complementary technologies.
Importantly, we are also increasingly
extending our culture of innovation beyond
circuit design and analog manufacturing
process technology into architecting signal processing systems to meet our customers need for more complete solutions.
These newest areas of innovation are creating higher barriers to entry for competitors and strengthening the ADI brand among
customers, partners, and industry experts.
During 2013, ADI was recognized by many of our customers for providing the industry-leading technology, quality and
reliability that make high performance, mission critical applications possible. In one notable example, a world leading
provider of telecommunications equipment singled out ADI as its Supplier of the Year, citing ADIs cutting edge
technology and consistent and excellent support at every level, and describing ADI as the very definition of what it
means to be a collaborative supplier.
In 2013, ADIs EngineerZone technical support community received top honors from Forrester Research, Inc., a global
research and advisory firm, for outstanding use of social media to engage customers in technical design support, thereby
increasing customer loyalty and value. In the EngineerZone community, our technical support expertise is accessible
to engineers all over the world, all the time, and in an interactive way. And Thomson-Reuters included ADI for the third
consecutive year in its prestigious list of the Top 100 Global Innovators.
In every aspect of our business, by focusing
on driving superior results through superior
innovation, we are uncovering new sources
of growth and value creation, and proving
that the cycle of innovation that has fueled
the ADI franchise for decades will continue
to fuel our prosperity in the future. The cycle
begins by attracting and nurturing the best
engineering minds who create superior
innovations with high value for customers
and high profits for ADI. Strong profitability
and financial performance then affords us
the flexibility and stability, regardless of the
cyclicality of our industry, to continue the
cycle by attracting and nurturing top talent
and building superior innovations.

Few worked harder than the late Jerry Fishman to nurture the culture of innovation that Ray Stata founded at ADI. Jerry led
ADI as CEO from 1996 to 2013, was an employee since 1974, and was my mentor and friend for many years. I know that
long into the future I will continue to draw deeply on the learning that he imparted to me as I challenge our people to reach
ever increasing levels of performance.
This year we made steady progress executing against our long-term plan to grow sales, earn extraordinary profits, and
use our cash generation to increase shareholder returns. Tremendous opportunities lie ahead for our great company.
ADI provides an essential technology portfolio that enables pervasive sensing, automation, and communication across
myriad applications that can improve how businesses operate and people live. Our brand is stronger than ever and our
capabilities more complete and more focused, providing us a great platform on which to keep building. I firmly believe
that our best is yet to come.
Sincerely,
Vincent Roche
President and CEO
Analog Devices, Inc.