Letter to our Sharehoders

In early 2003 the proverbial stars were all aligned according to some
industry pundits to set the stage for a rebound in offshore drilling
activity, most particularly in the Gulf of Mexico. Commodity pricing
remained strong both for natural gas and oil throughout the year, and
operators were generating substantial amounts of free cash flow. But
guess what? Those facts did not translate into a meaningful recovery
and the drilling services sector in general did not perform well. 
An anonymous author wrote: things turn out best for people who
make the best of the way things turn out. Nobles management
team, reacting to the conditions that it was dealt, achieved the third highest operating results in its history. Operating revenues for the year were $987 million, compared to $990 million for the year 2002.
Net income per share was $1.25 for the year, as compared to the previous year of $1.57. The net cash
provided by operating activities was $365 million compared to $445 million for 2002. Further af?rmation as to the strength of our results was provided by a recent analysis by a Goldman & Sachs analyst
which showed that the Company was the only driller in the sector to again achieve positive returns
over their weighted cost of capital. The following outlines the af?rmative steps that were undertaken
during the period to achieve these performance levels. 
1. The most important objective for me is to assure that we have safe working conditions for the men
and women in our operations worldwide. This focus has again resulted in a decline in both lost time incidents and recordable incidents in 2003 over 2002 results by 29 percent and 28 percent, respectively. Further,
the Company was honored as the recipient of The Shell Safety Case Award for the semisubmersible, Noble
Jim Thompson. Forty-eight percent of our active rigs achieved three years of operations without a lost time
incident, 18 rigs achieved four years and 13 rigs achieved ?ve years without a lost time incident.
2. The Company expanded its fleet with the acquisition of two jackup units and options to purchase
two additional jackup units in 2004. Assuming
we exercise those options, our fleet would
increase to 59 units. As a reminder, when the
Company went public in 1985 we started with
two 300 independent leg cantilever jackup units.
These acquisitions, once consummated, will be
additive to earnings immediately. 
3. Rig projects either completed or underway
during the year involved the  Noble Tommy
Craighead,  Noble Dick Favor, and  Noble Carl Norberg
jackup units. These capital investments were
critical in assuring that our fleet remains among
the most competitive in the industry. 
4. We completed re-payment of the Noble Paul Romano EVA-4000
project financing, resulting in our
net debt to total capitalization ratio falling to 14 percent which was an improvement over the 17 percent at December 31, 2002. 
5. Management continues to tweak its operational structure to assure that our after tax profit margins are the highest in the sector. However, we will witness some increase in cost as a result of the addition of new rigs in 2004. 
As to what lies ahead for 2004, we believe the West African operation which has suffered over the last
eighteen months from low utilization will markedly improve, both for jackup units as well as semisubmersibles. While we had some unanticipated operating difficulties in Brazil in the fourth quarter, I am
confident our management team will achieve much better results for this year. In anticipation of a continued strong market in the Middle East, we have mobilized the Noble Dick Favor from Brazil to the Persian
Gulf where we have already experienced significant interest in contracting the unit by several clients.
Further, given the reduction in supply, the U.S. Gulf of Mexico should also witness modest improvement. 
We were again recognized by Institutional Investor magazine which surveyed portfolio managers and
sell-side equity analysts to choose the top performing Chief Executive Of?cers in 26 different industry
sectors. Our team was honored as The Best in the oil service sector. I might point out that this is the
second year in a row that the Company was recognized by this magazine. 
The Noble management team continues to follow the mantra of building true shareholder value over
time. The fourth quarter of 2003 represented our thirty-fourth consecutive quarter of positive earnings
and again, for the fourth straight year, we led the peer group in terms of ?nancial results. This team has
and will rise to challenges and will continue to perform. Publilius Syrus said no one knows what he
is able to do until he tries. While we cannot accurately predict the future, we can respond and indeed
keep trying to make things happen. 
On behalf of the Board of Directors and the Noble team, I want to thank the shareholders who support this very effective and simple objective of creating value over time and to my fellow Nobleites,
thank you for your sacrifices and contributions.

James C. Day  
Chairman of the Board and Chief Executive Of?cer