In 2016, UPS experienced
a significant increase in
packages delivered and this
trend is expected to continue
in coming years.
Looking into the future, there are many
great opportunities for UPS. Demand for
our comprehensive portfolio of solutions
is increasing around the world and we are
positioning the company to capitalize on
strategic growth opportunities.
We are embarking on the most sweeping
transformation of our network in decades.
UPS is undertaking a global transformation
to further its Smart Logistics Network.
This enables us to take advantage of new,
profitable growth markets and create greater
efficiency in our global business model.
Over the next several years we will dedicate
additional capital to the global UPS network.
Well invest aggressively in new sorting
capacity, new technology, new automation
and new capabilities. Were also investing in
new flexibility and efficiency that will allow
us to bend the cost curve even lower.
Indeed, if there are three words that serve
as the touchstones that together drive every
decision we make, they are:
Invest. Grow. Deliver.
The pace of change is creating new
opportunities for product innovation,
geographic expansion and use of technology
to disrupt traditional business models.
Companies today must constantly assess
everything they do to ensure theyre keeping
pace. UPS has been doing just that and we will
continue to optimize every element of our
business  including operations, investment
levels, our product mix and pricing.
The pace of change is also creating new
opportunities for businesses. In response,
weve continued to transform to take
advantage of market opportunities that
create profitable growth.
In 2016 we:
 Announced an acquisition and a
strategic partnership with game-changing
companies that enable UPS to extend its
global capabilities.
 Invested $3 billion in capital expenditures
to expand, automate and optimize our
integrated global network.
 Completed the first phase of ORION, our
proprietary route-optimization initiative,
which is now generating more than
$400 million in annual cost savings and
avoidance. We are working on further
enhancements to ORION that will deliver
additional benefits in the future.
 Invested heavily in our International
operations  investments that already are
paying off. Since 2014, UPS has recorded eight
consecutive quarters of double-digit growth
in International adjusted operating profits.
These achievements, along with every new
investment, are a step forward in building the
UPS Smart Logistics Network, an integrated
global equipment and facility network that is
connected by a proprietary digital technology
ecosystem. Were confident this approach
will enable us to achieve even greater levels of
growth and efficiency over time.
A Year in Review
Our 2016 results demonstrate that UPS is
adapting well to the many changes in the
global economy, including double-digit
growth in ecommerce. As we look back at
the year:
 UPS delivered, on average, about 19 million
packages per day. This higher volume drove a
4.4 percent increase in consolidated revenue,
to nearly $61 billion.
 Even as we invest, we continue to generate
strong results that position UPS for profitable
growth. For the full year, adjusted diluted
earnings per share increased almost 6 percent,
to $5.75*, which also marked an all-time high.
 These strong results were driven in part by
our International business segment, which
generated a more than 13 percent* increase
in adjusted operating profit growth on
shipment growth of 4.4 percent.
 Cash from operations remained robust, at
$6.5 billion, which enabled us to raise the
dividend by about 7 percent in 2016. Our
cash flow also enabled UPS to fund nearly
$3.0 billion in capital expenditures. We
returned over 100 percent of Net Income
to our Shareowners, paying $2.8 billion in
dividends, and repurchasing about $2.7
billion in shares.
 UPS was voted No. 1 in the delivery industry
in Fortune magazines 2017 rankings of the
Worlds Most Admired Companies. UPS
also ranked sixth in the Harris Polls annual
corporate reputation survey. The rankings
highlight the strength of our portfolio of
solutions and the UPS brand.
U.S. Domestic
The U.S. Domestic segment produced solid
revenue growth of 4.2 percent over the prior
year. The revenue gains were driven by
balanced growth across all products. The
Next Day Air product category led the way,
up 4.8 percent on a per-day basis. Ground
products were up 4.2 percent and Deferred
Air products increased 2.9 percent per-day.
Adjusted operating profit increased to
$4.9 billion* and adjusted operating margin
was 12.9 percent*.
International
The International business segment delivered
a strong performance again in 2016. Revenue
increased to $12.4 billion. Robust Export
shipment growth of 5.1 percent was the
primary contributor to total International
daily shipments rising 4.4 percent.
Total adjusted operating profit grew to $2.5
billion*, an increase of 13.2 percent*, marking
the second consecutive year of double-digit
earnings growth.
Supply Chain and Freight
The Supply Chain and Freight segment
experienced strong revenue growth of 8.3
percent. This years revenue growth was
boosted by the August 2015 purchase of
Coyote Logistics. We also saw increased
revenue in the Distribution unit led by
growth in the Healthcare, Retail and
Aerospace sectors.
The Global Freight Forwarding and the U.S.
LTL businesses eliminated structural costs
and focused on profitable customers and
lanes, as these units managed through weaker
market conditions. These efforts, along with
the new capabilities acquired in freight
brokerage and healthcare, provide a good
foundation on which to build.
Forward Fast
The change and disruption in the modern
global economy is creating extraordinary
opportunities for companies who can move
forward fast. At UPS, we are moving faster to
capture opportunity. For all the change that
weve seen in the modern global economy, I
believe we are still in the early stages of this
evolution. One of the biggest catalysts is the
desire customers have for more flexibility
and customization.
For instance, technology is allowing mass
production to give way to a new era of mass
customization with production located much
closer to consumption. Our customers need
supply chains that enable them to rapidly
respond to changes in demand, exchange
rates, trade laws and technology. Weve
developed the expertise and the custom
solutions that enable customers to create
highly flexible supply chains.
The continued rise of ecommerce has shifted
the patterns for speed and personalization.
As a result, UPS embarked on a multi-year
program to create differentiated product
solutions that position the company to
lean-in to the future growth of ecommerce
 serving retailers and manufacturers from
around the globe who have their own direct
distribution channel.
The shifting trade patterns and disruption
create a world of new possibilities, but
only for those companies, like UPS, who
can transform, invest wisely and who have
the scale, the flexibility, and the integrated
network to execute on these opportunities.
So be assured that today, there is no part of
our business where we arent asking, How
can we do better?
Weve been rethinking and re-imagining our
operations, our service, our product mix and
our pricing. The company began making the
changes and the investments to improve our
service and to grow profitably.
UPS is investing in automation in our global
network and in fast-growing segments like
healthcare, aerospace and ecommerce.
Were adding more flexible capacity and more
technology to improve our productivity and
to capitalize on market trends and enable our
customers to take advantage of these trends
today and tomorrow.
Today, we have great opportunities before
us to create new legs of growth for UPS
in international, in healthcare and in
ecommerce. Our network is the most efficient,
and were making it even more efficient than
ever, accelerating our ability to grow.
The time to invest more aggressively is now.
Investing for Growth
To support the growth were experiencing
from international and ecommerce, were
accelerating automation in our facilities.
We are updating and replacing some older
facilities with new highly automated hubs
that will provide greater speed, flexibility
and productivity.
Were a company that takes great pride in
how weve transported technology from one
part of our integrated network to another.
Looking forward, the new, highly automated
hubs will be integrated with our networkplanning
technology. This will enable UPS to
extend, connect, and optimize package flow
throughout all parts of the network in the
same way that ORION enabled us to optimize
our driver routes.
Imagine being able to shift and re-direct
package volume between our hubs in ways
that lower our costs, boosts our productivity,
responds to unanticipated surges, all the
while improving our service levels, our asset
utilization and our profitability.
Having the worlds largest, most-efficient and
most-integrated network has enabled us to
maintain industry-leading margins, even as
our industry has been buffeted by volatility
in oil prices, swings in exchange rates,
softness in manufacturing, and the political
uncertainties that have left many businesses
reluctant to expand.
While increasing our efficiency helps us
bend the cost curve, we are also committed
to achieve top-line growth. Over the next
few years, we must raise the percentage
of capital invested back in the business to
our historic norm, to deliver on our major
growth opportunities.
At UPS, we will do this by maximizing the
power of our balance sheet while maintaining
the flexibility to take advantage of any
strategic acquisitions that come available.
Ecommerce Opportunity
Theres no ignoring the fact that ecommerce
is having an effect not just on retail, but on
all of business as more companies implement
direct distribution channels.
The shift toward online commerce is
permanent. In fact, the trend is accelerating
and as a result, UPS has embarked on a
multi-year journey to create profitable
solutions that allow us to lean in to the future
growth of ecommerce, serving retailers,
manufacturers and the many other businesses
looking to build direct distribution channels.
The good news is that the investments
were making in capacity and new solutions
will benefit all of our customers, regardless
of industry. Thats important, because
just as ecommerce has changed consumer
expectations, the internet is changing
procurement and customer expectations in
the industrial world.
The addition of more efficiency and
technology to the UPS network will create
more density on driver routes, lower operating
costs and bolster shareowner returns.
Healthcare Opportunity
Each industry has specific transportation
and logistics requirements, driven by
customer needs, package characteristics
and government regulations. For these
reasons, healthcare logistics is a significant
opportunity for UPS.
The ongoing advances in medicine, coupled
with the aging of the worlds population,
creates the need for logistics partners who
can provide the specialized temperature,
chain-of-custody and speed-in-transit
that todays cutting-edge healthcare
companies require.
Over the past five years, UPS made
acquisitions to further build out a suite of
specialized solutions. Our acquisitions of
Polar Speed, CEMELOG, Marken and others,
give us unique new capabilities in clinical
trials storage and distribution that our
competitors around the world cant match.
International Opportunities
While ecommerce makes the headlines today,
we remain resolute that the international
markets remain our best long-term
opportunity. As a result, were continuing
to build out our global network, deepening
and widening the services we provide in the
worlds fast-growing markets.
In fact, in 2016 our four fastest-growing
markets were China, Vietnam, Pakistan and
the United Arab Emirates, each of which
recorded double-digit growth. More broadly,
the 15 developed and emerging markets weve
prioritized offer between $80 billion and
$90 billion in opportunity across all of our
business units.
We are committed to helping customers of
all sizes navigate global trade complexity
and take advantage of the opportunity that
comes with opening up markets. The UPS
International operating model has evolved
to serve not only multinationals, but also the
small and mid-sized businesses that want to
ship regionally and locally.
The International Segment has strong
momentum and we will continue to prioritize
investment to expand our international
footprint through organic growth,
partnerships and acquisitions. Youll hear
more in the coming months.
Summary
UPS is making the investments to increase
efficiency, capacity and profitability of its
global integrated network.
Think of it as the next generation of the
UPS Network.
A smart, efficient and integrated network.
The alignment were creating across our
business and the transformational changes
were making in our operations represent
great opportunities for our customers and our
investors. In short, UPS is Investing so it will
continue to Grow and Deliver for shareowners.
David Abney
UPS Chairman and
Chief Executive Officer