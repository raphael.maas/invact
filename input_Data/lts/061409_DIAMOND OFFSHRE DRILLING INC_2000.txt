    LETTER TO SHAREHOLDERS                                 intermediate rigs more attractive. In addition, there
    In 2000, Diamond Offshore's revenues and net has been a dramatic increase in the number of
    income declined significantly as a result of the mid-water depth drilling plans filed with the
    sharp fall in oil prices in 1999. Still, by the second U.S. Minerals Management Service in recent
    half of 2000, a recovery began to take hold, driven months. Filings doubled between the first and
    by oil prices averaging just above $30 a barrel and fourth quarter 2000, signaling a growing demand
    natural gas prices soaring to historic highs. By year- for mid-water rigs in the months ahead.
    end, all of our key rig markets were showing signs       Diamond Offshore participated in the vibrant
    of strength, pointing to better times for our com- demand for jack-ups in the Gulf of Mexico, spurred
    pany and industry in                                                             by high natural gas prices,
                                      In the fourth quarter of 2000, accelerating depletion
    2001.
      Our 2000 results reflect-       major and independent oil and rates, and increased
    ed soft demand in our                                                            demand for gas-fired
                                      gas companies announced electrical generation. By
    core market, intermediate
                                      budget increases for exploration the end of the second
    water-depth semisub-
    mersible rigs, which              and production of as much as 20 quarter, all of our jack-ups
    accounted for almost 50%                                                         were fully utilized.
                                      percent over 2000 levels, the Dayrates more than dou-
    of revenues. Dayrates
                                      largest increase in two decades. bled through the year.
    declined in this market by
    26% over those in 1999 as         The positive developments in all Dayrates for the Ocean
    contracts negotiated in                                                          King, one of our jack-ups
                                      our markets, combined with high
    more favorable periods                                                           in the Gulf of Mexico,
                                      energy prices, give us optimism soared from $17,500 in
    expired. Our intermediate
    water-depth business,             for improved results for Diamond early 2000 to $54,000 in
    largely confined to the                                                          the fourth quarter.
                                      Offshore in 2001.
    Gulf of Mexico, was                                                                We also saw strong
    impacted by two trends in the offshore market-- demand and dayrate increases for our
    major oil companies focusing on more lucrative deepwater and ultra-deepwater semisubmersibles.
    ultra-deepwater projects, and independent oil Contract commitments for the Ocean America, a
    companies working shallower waters with jack-up deepwater rig, climbed from $67,000 per day at mid-
    rigs in the search for natural gas.                    year to $117,500 per day by year-end. All of our deep-
      Fortunately, as we approached the end of 2000, water contracts signed for first and second quarter
    the prospects for our intermediate water-depth fleet 2001 reflect similar rate increases. We're delighted to
    improved. Our backlog of work began to build and see substantially higher dayrates in the North Sea and
    dayrates climbed to the $40,000 range, versus a recovery in the Southeast Asia market as well.
    $25,000 during the trough of activity last summer.        Diamond Offshore continually strives to expand
    Rising dayrates for premium jack-ups also make our its fleet capabilities. In December 2000, we

2
completed the conversion of a former accommoda-           worked to maintain a strong balance sheet. Cash
tion vessel into one of the most advanced deepwa-         and marketable securities held by the company
ter semisubmersible rigs ever built, the                  amounted to $862.1 million, while its total long-
                                                Ocean


                 Despite cost overruns, the rig is        term debt was $856.6 million. Prevailing market
Confidence.


expected to make significant profits during its initial   conditions made it attractive for Diamond Offshore
contract--a five-year drilling program in the Gulf of     to raise $402 million of capital through the issuance
Mexico that commenced January 2001.                       of zero coupon convertible debentures. In addition,
   In May 2000, we announced plans to invest $180         we spent $93 million in 2000 repurchasing our
million to convert the semisubmersible,                   shares when prices were low.
                                                Ocean


             into a special-                                                           In the fourth quarter
Baroness,


ized rig for an emerging                                                             of 2000, major and
market--developing oil                                                               independent oil and gas
fields in ultra-deep water                                                           companies announced
depths. The engineering                                                              budget increases for
for the rig has been                                                                 exploration and produc-
completed, and we expect                                                             tion of as much as
the project to be finished                                                           20 percent over 2000
on time and on budget,                                                               levels, the largest
just like our other success-                                                         increase in two decades.
ful Victory Class upgrades,                                                          The positive develop-
the                                                                                  ments in all our markets,
    Ocean     Quest,    Ocean


       and                                                                           combined with high
Star,       Ocean      Victory.


Slated for delivery from                                                             energy prices, give us
Singapore in the first quar-                                                         optimism for improved
ter of 2002, the                                                                     results for Diamond
                        Ocean


            is designed to                                                           Offshore in 2001.
Baroness


drill ultra-deep wells in ultra-deep water.                 We thank our shareholders, our customers, and
   To better withstand--and prosper from--the             dedicated employees worldwide who continue to
industry's cyclical nature, Diamond Offshore has          help this remarkable company grow and prosper.




James S. Tisch                             Lawrence R. Dickerson
Chairman of the Board and                  President and Chief
Chief Executive Officer                    Operating Officer
