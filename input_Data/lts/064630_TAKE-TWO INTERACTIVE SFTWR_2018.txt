DEAR
SHAREHOLDERS,

Fiscal 2018 was another outstanding year for Take-Two, underscored by operating results that greatly
surpassed the initial outlook that we provided at the start of the year. We delivered $1.8 billion in net
revenue, Net Bookings grew 5% to nearly $2.0 billion, and we generated earnings growth and margin
expansion. Our strong business performance converted into substantial net cash provided by operating
activities, which grew 19% to $393 million and, as of March 31, 2018, we had over $1.4 billion in cash and
short-term investments. It bears noting that we achieved these results despite an unusually light release
schedule, reflecting the strength of our robust catalog led by Grand Theft Auto and the substantial
contribution that we now receive from recurrent consumer spending. Today, our enterprise is a global
leader in the interactive entertainment industry and a significantly higher-margin business than at any
time in our history.
OUR KEY ACHIEVEMENTS
Q We delivered record digitally-delivered net revenue and Net Bookings. Digitally-delivered net revenue grew
23% to $1.13 billion and digitally-delivered Net Bookings grew 25% to $1.35 billion.
Q We generated record net revenue and Net Bookings from recurrent consumer spending. Recurrent
consumer spending accounted for 42% of total net revenue and 48% of total Net Bookings.
Q Grand Theft Auto V and Grand Theft Auto Online continued to exceed our expectations in fiscal 2018 �
as they have in every year since their release nearly five years ago � with combined Net Bookings from
the titles growing year-over-year. Grand Theft Auto V has sold-in over 95 million units, reflecting its status
as the highest-rated title of the current console generation and the �must have� game for purchasers of
PlayStation 4 and Xbox One. Moreover, Grand Theft Auto Online broke monthly audience records in June,
July and December, added more new users than in fiscal 2017 and delivered its biggest year yet for virtual
currency sales. Rockstar Games achieved these results through the ongoing release of a wide array of free
additional content.
Q We released NBA 2K18, which is now our highest-selling sports title ever with sell-in to date of over 9
million units � up 17% over last year�s release. In addition, our NBA 2K series continues to benefit from
growing engagement and recurrent consumer spending. During fiscal 2018, average revenue per user,
revenue per hour and unique multiplayer users all increased double-digits, and recurrent consumer
spending on NBA 2K grew 34% to a new record.
Q We successfully launched WWE 2K18, which has been enhanced by an array of downloadable add-on
content, including a Season Pass. The WWE brand is as popular as ever, and we believe there is a substantial
long-term opportunity to grow our WWE 2K series by leveraging further the development and marketing
expertise of 2K and Visual Concepts.
Q We formed Private Division, our new publishing label that focuses on bringing titles from top independent
developers to market. Private Division will publish several titles based on new IP from renowned industry
creative talent, including Panache Digital Games, The Outsiders, Obsidian Entertainment and V1 Interactive.
Private Division is also the publisher of Kerbal Space Program, which Take-Two acquired in May 2017.
 Q During fiscal year 2018, we repurchased 1.51 million shares of Take-Two common stock for $154.8 million.
Q On March 19, 2018, Take-Two was added to the S&P 500� index.
DEAR
SHAREHOLDERS,
2
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2018 ANNUAL REPORT
SUCCESSFUL GROWTH STRATEGY
Take-Two�s strategy is to develop the highest-quality, most compelling interactive entertainment franchises in the
business, and deliver them on every platform around the world that is relevant to our audience. Complementing our
core business with offerings that drive ongoing engagement with and recurrent consumer spending (including virtual
currency, in-game purchases and add-on content) on our titles after their initial purchase is an important, highmargin growth opportunity and, therefore, a key strategic priority of our organization. We now support nearly all
of our new releases with innovative offerings designed to achieve this objective. Recurrent consumer spending also
helps to strengthen our results between front-line releases while providing additional entertainment to consumers.
World-class creative teams: Creativity and innovation remain the core tenets of our organization, and are the
lifeblood for our ongoing success. We have over 3,250 employees working in game development in 18 studios around
the world � including some of the most talented visionaries in the business. The creative teams at Rockstar Games
and 2K are renowned throughout the world for their consistent ability to deliver games that set new benchmarks for
excellence. In addition, our Social Point studio further enhances our organization�s development capabilities with one
of the few creative teams in the world possessing a track record of producing multiple hits in the free-to-play mobile
sector. Whether expanding beloved franchises, launching new intellectual property or providing innovative ways for
audiences to remain captivated and engaged, we have an unwavering commitment to producing the highest quality
entertainment experiences.
Diverse portfolio of industry-leading intellectual property: Take-Two is a financially strong, global interactive
entertainment enterprise with numerous successful franchises encompassing a variety of genres. Our diverse
portfolio of intellectual property includes 11 series with at least one five-million unit selling release, and 60 individual,
multi-million unit selling titles. Since 2007, we have added 9 new brands to our lineup, including hits such as
BioShock, Borderlands, Red Dead Redemption and WWE 2K.
Capitalizing on growth of digital distribution: During fiscal 2018, we continued to capitalize on our industry�s
ongoing transition towards digital distribution, and delivered both record digitally-delivered net revenue and
Net Bookings, including our highest-ever from recurrent consumer spending. In addition to virtual currency for
Grand Theft Auto Online and NBA 2K, recurrent consumer spending was enhanced by a variety of offerings, including
free-to-play games led by WWE SuperCard, which grew over 20% year-over-year and has now been downloaded
nearly 17 million times, Social Point�s mobile titles Dragon City and Monster Legends, and NBA 2K Online � which
remains the #1 PC online sports game in China with over 37 million registered users. Net Bookings from add-on
content grew more than 40%, led by offerings for Sid Meier�s Civilization, particularly the Rise and Fall expansion
pack; XCOM 2, particularly War of the Chosen; WWE 2K; and Mafia III. Our results also benefited from ongoing
growth in full-game downloads, with more than 35% of units for current-generation consoles and over 95% of units
for PC delivered digitally. Moreover, nearly half of our catalog sales for old-generation consoles are being delivered
through digital download. Digital distribution is disproportionately benefitting our catalog, as it gives consumers the
opportunity to buy older titles that no longer receive physical shelf space. Over the long-term, we expect the trend
towards digital distribution to continue, with the percentage of sales of console games through full-game download
continuing to grow.
 Interactive entertainment is one of today�s most dynamic and popular
art forms. We are actively investing in a variety of emerging platforms, business models and markets that we believe
have the potential to further diversify our business, expand our audience and enhance our financial performance.
Q Our January 2017 acquisition of Social Point expands our presence in the free-to-play mobile market, which
is the largest and fastest-growing segment within our industry. We believe that Social Point is a perfect
complement to our business because it produces high-quality entertainment that can attract and retain a
loyal, highly-engaged player base and deliver sustainable results. Social Point focuses on �mid-core� games
that feature greater gameplay depth than casual games, and their titles typically monetize and retain players 
3
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2018 ANNUAL REPORT
at higher rates than its competitors in this segment. They have a number of exciting titles planned for launch
in the coming years, and we continue to view Social Point as an important long-term growth opportunity
for Take-Two.
Q eSports is an exciting growth trend in our industry. To that end, we are very pleased with the progress of the
NBA 2K League � our joint-venture with the NBA that marked the first official competitive gaming league
based on a U.S. professional sports league � which kicked off its inaugural season in May 2018. Following the
qualifying rounds during which more than 76,000 people participated, in April 2018, 102 of the best NBA 2K
players in the world were drafted by 17 NBA teams to compete in a 15-week season that will conclude with
the NBA 2K League Playoffs and Finals in August 2018. The NBA 2K League has been steadily building its
portfolio of high-profile partnerships and sponsorships, including Dell, Intel and Twitch, which is the League�s
official streaming partner. We look forward to watching the continued growth of the NBA 2K League, which
has the long-term potential to enhance engagement, and to be a meaningful driver of profits for
our Company.
Q Asia represents the largest online video game market in the world and an important long-term growth
opportunity for our organization. Building on the popularity of NBA 2K Online in China, 2K and Tencent are
teaming up again to co-develop and release the title�s highly-anticipated successor, NBA 2K Online 2. This
new game is based on the console edition of NBA 2K and features 2K�s legendary gameplay, 27 customizable
position types, new player trading systems, eSports-optimized features, localized commentary and more.
NBA 2K Online 2 is currently in closed Beta testing and is planned for commercial release this fall. In addition,
we are pleased to expand our successful partnership with Tencent through the planned release of Kerbal
Space Program on Tencent�s WeGame distribution platform as a premium PC game. We are excited about
Tencent�s WeGame platform and the opportunity to grow our business in China.
Innovative marketing and global distribution: Creating groundbreaking entertainment experiences is only part
of our formula for success. Our marketing teams execute well-coordinated global campaigns that leverage nearly
every form of media � from traditional to social � to turn our product launches into tent pole events. We also work in
lockstep with our key retail partners, both brick-and-mortar and online, to create promotions that drive consumers
to the point of sale. Our global distribution network ensures that our products are available to consumers throughout
the world, both physically and digitally � wherever and whenever they desire.
Sound financial foundation: With more than $1.4 billion in cash and short-term investments as of March 31, 2018,
Take-Two has ample capital to pursue a variety of investment opportunities. We are very excited about our growth
potential over the long-term, and our strong cash position and outlook give us the flexibility both to invest in organic
growth opportunities, as well as potential studio or IP acquisitions, and strategic partnerships. We also have the
ability to return capital to shareholders, including through opportunistic share repurchase.
EXCITING LINEUP OF NEW RELEASES
We expect fiscal 2019 to be another year of profitable growth for Take-Two, including both record Net Bookings and
record net cash provided by operating activities.
Q On September 11, the next annual installment of NBA 2K will return to the hardwood court with the series�
signature style and deep authenticity, and will feature Milwaukee Bucks standout and two-time NBA All-Star
Giannis Antetokounmpo on the cover. This year marks the 20th anniversary of our industry-leading basketball
simulation, and we are confident that 2K and Visual Concepts will once again take the series to exciting
new heights with the release of NBA 2K19. 2K will feature three-time NBA Champion, four-time NBA MVP
and avid NBA 2K gamer LeBron James on the cover of the NBA 2K19 20th Anniversary Edition. The iconic
cover features Akron�s favorite son alongside an artistic composition of words personally chosen by LeBron,
including �Strive for Greatness,� �Driven,� and �Equality.� In addition to the exclusive James themed content
and items, fans who purchase the NBA 2K19 20th Anniversary Edition will receive early access to the game.
4
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2018 ANNUAL REPORT
Q On October 9, 2K�s WWE series will be back with WWE 2K19, bringing gamers into the virtual squared circle
with their favorite WWE Superstars, gameplay modes and variety of hard-hitting, in-ring action. A.J. Styles,
�The Phenomenal One�, will be the game�s cover Superstar. This year, for the first time, fans will have the
opportunity to compete in the WWE 2K19 Million Dollar Challenge, whereby the winner of the tournament will
face-off one-on-one against Styles in WWE 2K19 to vie for the $1 million grand prize.
Q On October 26, Rockstar Games will launch Red Dead Redemption 2, the eagerly awaited sequel to one
of the label�s most critically acclaimed and beloved titles. Created by the team behind Grand Theft Auto
V and Red Dead Redemption, Red Dead Redemption 2 is Rockstar Games� first title developed from the
ground up for the current console generation. An epic tale of life in America�s unforgiving heartland, Red
Dead Redemption 2�s vast and atmospheric world will also provide the foundation for an entirely new online
multiplayer experience. We could not be more excited about the upcoming launch of Red Dead Redemption
2, which is poised to be another massive, entertainment event.
Q Throughout fiscal 2019, we will continue to deliver an array of digitally-delivered offerings designed to drive
engagement with, and recurrent consumer spending on, our recent releases and upcoming titles, including
updates for Grand Theft Auto Online, WWE SuperCard and more. In addition, Social Point and 2K will
continue to broaden our offerings for mobile devices.
Looking ahead, we have a robust long-term development pipeline across our labels, featuring offerings from our
renowned franchises along with new intellectual properties that promise to diversify further our industry-leading
portfolio. In fiscal 2020, we plan to launch a highly anticipated title from one of 2K�s biggest franchises that we
expect will enhance our results during that year.
OUR FUTURE
This year marks the 25th anniversary of Take-Two. Over that time, we have built our Company into a diversified and
profitable enterprise. In particular, we are extremely proud that Take-Two is home to our industry�s best talent, whose
passion and creative vision consistently captivate and engage audiences around the world. Take-Two is exceedingly
well-positioned to capitalize on the vast opportunities in our industry, including advances in hardware, the ability to
drive ongoing engagement through connected experiences and additional content, and the continued proliferation of
mobile platforms and emerging business models. As a result, Take-Two is poised to deliver growth and returns for our
shareholders over the long-term.
We would like to thank our colleagues for delivering another strong year for our Company. To our shareholders, we
want to express our appreciation for your support.
Sincerely,
Karl Slatoff
President
Strauss Zelnick
Chairman and Chief Executive Officer