To our stockholders

It is a rare opportunity to be part of something truly transformational.
Bristol-Myers Squibb is no longer becoming BioPharma. We stand
today as a fully focused and energized BioPharma company, confident
in our strategy, confident in our people and confident in our culture.
In 2009, we took three significant steps to complete our transformation:
 We extended our partnership with Otsuka Pharmaceutical Co., Ltd.,
thus adding 29 additional months of Abilify sales in the U.S.;
 We acquired Medarex, Inc., significantly expanding our oncology
pipeline and biologics capabilities; and
 We split off our holdings in Mead Johnson Nutrition, creating
shareholder value and focusing Bristol-Myers Squibb solely
on biopharmaceuticals.
And in 2009, we delivered on our commitments. I am pleased to report
that for the third consecutive year our company has performed well and
achieved strong financial results with growth in sales and earnings.
For the year, we posted a 6 percent sales growth, led by our key products such as Plavix, Abilify, Baraclude,
Orencia, the Sustiva franchise, Sprycel and Reyataz.
In 2009, net sales from continuing operations were $18.8 billion, compared with $17.7 billion in 2008.
Diluted net earnings per share from continuing operations were $1.63 in 2009 compared with $1.35 in
2008, and non-GAAP earnings per share from continuing operations were $1.85 and $1.49, respectively.
And we ended the year with nearly $10 billion in cash, cash equivalents and marketable securities  funds
that will enable us to further invest in our future.
DELIVERING ON OUR COMMITMENTS
As recently as two years ago, Bristol-Myers Squibb was a traditional diversified midsized pharmaceutical
and health care products company. As others have merged into megacompanies, we are becoming more
streamlined and focused. As others have broadened their portfolios, we are focusing on select areas of
medical need. As others have widened their geographic footprints, we are concentrating on key major
and emerging markets.
Today we are a BioPharma leader with a vision for the future. We are leaner, more agile and better able
to execute our strategy and adapt quickly to change. Our entire organization is focused on a single mission:
to discover, develop and deliver innovative medicines that help patients prevail over serious diseases.
Our transformation is working. For the past two years, our total return for shareholders, including
dividends, has been among the best in the industry. We promised to deliver a 15 percent compound
annual growth rate in our non-GAAP earnings per share from continuing operations from 2007 to 2010,
and  if we meet our expected 2010 non-GAAP guidance  we are on track to exceed that. We outperformed
most mega pharma companies, diversified companies and pure biotech companies, as well as the
industry average. In December, we announced an increase in the dividend per share of common stock to
$0.32 per quarter. Reflecting our long-standing commitment to deliver shareholder value,I am proud to
say that our company has paid dividends to stockholders every single quarter since December 1, 1933.
We have driven solid, global top-line growth and are doing a good job in managing expenses. We are now
in a period of growth, which I believe will continue through 2011.

PRODUCT AND PIPELINE ADVANCES
In 2009, we gained regulatory approval in the U.S. and Europe for Onglyza, an important new medicine
for the treatment of type 2 diabetes in adults. Onglyza was discovered by Bristol-Myers Squibb and is
being developed and marketed in partnership with AstraZeneca. This approval marks the reentry of our
company into metabolics, an area in which we have a strong legacy and in which we intend to return to
a leadership position. Following Onglyza  and also with AstraZeneca  we are developing dapagliflozin,
a potentially first-in-class compound in Phase III clinical development for the treatment of type 2 diabetes.
Since 2002, we have delivered nine important
new products, including two biologic medicines. This output
compares favorably to many of our competitors, even those
whose R&D spend is significantly greater than ours.
Our pipeline continues to expand. During the past year,
we increased the number of compounds in Exploratory and
Full Development by nearly 40 percent. About one-third of
our compounds in development are biologics.
We have restructured our collaboration agreement with
Eli Lilly and Company  originally signed in 2001 with ImClone
 to include necitumumab, a novel biologic in Phase III
development for non-small cell lung cancer. We will share
development and potential commercialization in the U.S.,
Canada and Japan.
In September, the U.S. Food and Drug Administration
accepted the companys submission of a Biologic License
Application for belatacept, a promising investigational
immunosuppressive agent for use in kidney transplantation.
And following belatacept, we anticipate regulatory submissions
for up to four new drugs through 2012.
A FOCUSED STRATEGY
As we have pursued our BioPharma strategy, we have
taken steps to reduce the number of our manufacturing
facilities by approximately 50 percent since 2006, monetized
our nonpharma businesses and focused on our biopharmaceutical
business.
In February, we executed the highly successful partial IPO
(initial public offering) of Mead Johnson Nutrition, and we
split off our remaining holdings of Mead Johnson in December.
In April, we extended our commercialization agreement
with Otsuka for Abilify. Our collaboration with Otsuka,
which had been scheduled to terminate in the U.S. in 2012,
will now continue until 2015.
In addition, as a global but focused BioPharma company,
we are expanding into select emerging markets  those
that represent significant long-term-growth opportunities.
Our approach is to successfully develop and commercialize
select, innovative products in a specific set of key high-growth
markets  namely, Brazil, Russia, India, China and Turkey.

PRODUCTIVITY INITIATIVES
Bristol-Myers Squibb is becoming more agile, entrepreneurial and accountable, and our productivity initiatives
are helping fuel those cultural changes. While growing top-line sales and building our pipeline, we are
reducing our cost base by simplifying our organization, making it more cohesive and efficient. Our productivity
initiatives are on track to meet or exceed our $2.5 billion cost improvement goal by the end of 2011.
Today, our productivity improvement is continuous and sustainable. As an example, with funding derived
from productivity savings in Exploratory Development, the R&D organization is advancing a number of
early development compounds into Phase I and Phase II development, thus creating more output at the
same level of spend.
While overall strategic objectives are still established from a corporate perspective, the business units
have greater responsibility for determining how to achieve those objectives, and employees have greater
accountability for their success.
However, our competitive landscape is constantly changing. World economies are still struggling, generic
competitors are eroding our Plavix sales in Europe, payer and customer demands are increasing, and it is
likely that some form of health care reform in the U.S. will impact our company. Therefore, we are taking
additional steps to identify increased savings in order to deliver our 2010 commitments.
OUR STRING OF PEARLS
Productivity isnt just about saving money. To succeed, we need to take our savings and reinvest in our
future, such as product launches, key product development and our String of Pearls.
In 2007, with the purchase of Adnexus Pharmaceuticals, we launched our String of Pearls initiative
of innovative alliances, partnerships and acquisitions. Each transaction is highly strategic, designed to
enhance our own internal capabilities and targeted to fit within our overall strategy.
For instance, we have made a strategic decision to return to
a leadership position in oncology based on a strong legacy in
cancer medicines dating back 40 years. Our current portfolio
includes three key products  Erbitux, Sprycel and Ixempra 
and we have a robust pipeline of innovative medicines in
development. To further enhance our position, in August
we acquired Medarex, Inc., for $2.3 billion.
In 2009, we also entered into global collaborations with
ZymoGenetics, Nissan Chemical Industries and Teijin
Pharma, and Alder Biopharmaceuticals. In early 2010,
we entered into a global collaboration with Allergan. Our
full array of 10 pearls encompasses many of our key disease
areas, including cancer, cardiovascular disease, immunology,
neuroscience and virology.
BUILDING ThE FUTURE
Weve accomplished a lot, but challenges remain.
Our chief concerns are patent expirations beginning in
2012 in the U.S. with Plavix and Avapro. By reining in costs,
rationalizing our manufacturing network and advancing our
new products, we are now fundamentally better off. Thanks
to all of our BioPharma initiatives  including the extension
of our collaboration with Otsuka  we have transformed

what was previously thought of as a cliff
in 2012 and 2013 to a more manageable
plateau on which we can create a platform
for future growth.
For whatever lies ahead, we are in a much
stronger position  and better prepared to
build our next period of growth projected for
2014 and beyond.
OUR MISSION AND COMMITMENT
Bristol-Myers Squibb has transformed. But
we have never swayed from our commitment
to our patients.
Curtis Dunaway is one such patient. In 2007, at
age 39, Curtis was suffering from kidney failure.
I was on the verge of knocking at deaths door,
he says. He received a transplant and was given
immunosuppressant drugs, but the side effects
left him increasingly shaky, restless and sleep
deprived. Then researchers at the University
of California, San Francisco, Medical Center
asked him if hed like to enter a clinical trial for
belatacept. He did, and Curtis has responded
well. With the grace of God, my life has literally
been given back to me, he says. No words
can express my appreciation and gratitude.
It is for patients like Curtis that Bristol-Myers
Squibb employees come to work every day.
To better reflect Bristol-Myers Squibb as we are
today  and with input from employees worldwide and the approval of the Management Council
and the Board of Directors  we have redefined our companys mission statement and created a new
Mission and Commitment (featured in a special section beginning on the facing page).
Now, our Mission is a succinct 14 words: To discover, develop and deliver innovative medicines that
help patients prevail over serious diseases. This Mission clearly sets us apart as a company dedicated
to providing medicines that address unmet medical needs.
We have transformed our company, delivered on our commitments and set a course for the future.
This would not have been possible without unflagging dedication and innovation by our employees
and counsel and support by our Board of Directors. To be sure, there are challenges ahead. But I have
every confidence that we are now prepared to face them head-on and to emerge stronger than ever.
James M. Cornelius
Chairman and Chief Executive Officer
March 9, 2010