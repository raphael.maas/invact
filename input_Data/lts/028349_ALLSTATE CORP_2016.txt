Thomas J. Wilson 
Chairman and Chief Executive Officer 
Thomas J. Wilson 
Chairman and Chief Executive Officer 

Dear Fellow Shareholders 

Allstate exists to create more prosperity 
for customers, shareholders, employees, 
agency owners and communities. We do this 
by protecting people from lifes uncertainties 
and preparing them for the future with a 
talented team of over 79,000 Allstaters 
making a difference in the world. 

Allstates consumer-focused strategy is working to create 
a more sustainable business despite dramatic changes 
in the external environment. The rapid response initiated 
in 2015 to increased costs associated with auto accidents 
successfully improved auto profitability in 2016 but did 
have a negative impact on the number of cars that we 
insure under the Allstate, Esurance and Encompass 
brands. Homeowners insurance had good returns despite 
a significant increase in catastrophes from the prior year. 
Investment returns on our nearly $82 billion portfolio 
were good at 4.4% and the portfolio is positioned for 
economic growth. Net Income and Operating Income* 
declined from the prior year to $1.8 billion ($4.67 per share) 
and $1.8 billion ($4.87 per share), respectively, as pretax 
catastrophe losses increased by $853 million from 2015. 
Operating Income Return on Equity* was 10.4% in 2016. 

Shareholders received $486 million of dividends ($1.32 
per share) and $1.3 billion of shares were repurchased, 
which represents 5.3% of the outstanding shares on 
January 1. The result of this financial performance is a

total shareholder return of 21.5% in 2016, which continued 
our long-term record of outperforming peers and the 
S&P 500 index. The board also built on an exceptionally 
strong governance record by formalizing and expanding 
lead director responsibilities as described in the Report 
from Independent Directors in the Proxy Statement. 

We also made progress on 2016s five Operating Priorities. 

BALANCING OPERATING PRIORITIES 

Operationally, the challenge this year was to balance 
our first three Operating Priorities: Better Serve Our 
Customers, Achieve Target Economic Returns on Capital 
and Grow Policies in Force. All of these are important 
to long-term value creation but in 2016 we had to make 
trade-offs among them due to the rapid deterioration in 
auto insurance profitability beginning in 2015. We were 
able to raise Allstate Brand auto insurance prices an 
average of 7.2% in 2016 but this had a negative impact on 
customer satisfaction, and the number of auto insurance 
policies declined by 2.9%. We made progress in better 

serving customers across a broad set of activities from 
policy service to claims but we experienced a decline in 
our Net Promoter Scorethe ratio of customers who 
would recommend us to otherslargely reflecting auto 
premium increases. Allstate Benefits continued to have 
excellent growth with policies in force increasing by 13.4%; 
they exceeded 3.7 million at the end of the year. 

The fourth Operating Priority was to Proactively Manage 
Investments and we achieved a 4.4% total return on the 
portfolio despite uneven results throughout the year 
and implementation of several initiatives to optimize 
risk and return. The portfolio remains 71% fixed income 
with 85% of that rated investment grade. The portfolio is 
positioned for continued economic growth in the U.S. by 
being overweighted to corporate bonds but defensively 
positioned for rising interest rates. A shorter-term bond 
portfolio is a good risk-and-return trade-off even though 
it results in lower current income. We also made progress 
increasing performance-based investments, which 

include private equity and real estate, and increasing the 
level of equity investments backing long- term immediate 
annuity liabilities. 
BUILDING STRATEGIC PLATFORMS 
The fifth Operating Priority  Build and Acquire Longterm 
Growth Platforms  is focused on improving our 
existing businesses and expanding into new areas. 
The role of business in society is changing and Allstate is 
adapting and building a 22nd Century Corporation that: 
 Is defined by its strategic platforms and its products 
and services. Our definition of a strategic platform is 
shown in the diagram on this page and is more fully 
explained in last years Letter to Shareholders. 
 Has more integrated relationships with customers, 
employees, vendors and communities. 
 Is a Force For Good creating positive change in society. 
The 10,360 Allstate exclusive agencies, which are in 
virtually every community in America, are a platform 
that is continually being strengthened. Our Trusted 
Advisor initiatives to utilize technology to personalize 
interactions, initiate relationships and reduce the human 
modem aspect of service has yielded tangible results: 
Over 2 million new personalized insurance proposals 
were delivered in 2016. 
We are building a strategic platform in the connected 
car space by leveraging our customer relationships, 
analytical expertise and market presence. Improving 
the personal transportation system represents one of 
the biggest economic opportunities in America today. 
A 25% improvement in efficiency is achievable with 
current technologies and could increase household 
income by as much as 5% or $3,000 per household per 
year. Allstate is actively working to capture these benefits 
for customers. We also need to expand our customer 
relationships because ultimately there may be fewer car 
accidents and reduced demand for auto insurance. Arity, 
a newly formed connected car entity, provides expertise 
and services to our insurance entities to support the 
1 million- plus Drivewise and DriveSense customers. 

These telematics products provide insurance customers 
an accurate price and enhance their driving experience. 
Aritys plan is to also provide connected car services to 
third parties, creating additional revenue for Allstate. 
The Integrated Digital Enterprise strategy utilizes data, 
algorithms, advanced technology and process redesign 
to improve effectiveness and efficiency. Our initial focus 
is on claim settlements for property damage. 
The acquisition of SquareTrade for $1.4 billion closed 
January 3, 2017, and expands the ways we protect 
customers by providing product protection plans sold 
through major retailers. These plans cover a wide range 
of consumer goods including personal computers, 
cell phones, televisions and household appliances. 
Achieving an acceptable return for shareholders on the 
capital deployed in the acquisition will require continued 
expansion of this innovative business model. 
BUILDING INTEGRATED RELATIONSHIPS 
Integrated relationships with customers improve growth 
and returns, and create a more sustainable business 
model. Trusted advice from Allstate agency personnel 
reduces the probability that customers will switch 
insurance for a small price difference. Incorporating 
connected car services into insurance offerings 
integrates us into customers daily driving experience. 
The acquisition of SquareTrade increases our policies in 
force to over 68 million and provides consumers broad 
protection options. 
A sustainable business also needs integrated 
relationships with the people who make it a success. 
We assume employees are customers to whom we sell 
an opportunity to make a difference in peoples lives, 
be rewarded financially and grow professionally. This 
gives us a competitive advantage in attracting, retaining 
and motivating people who are the key to our success. 
The Employee Value Proposition includes competitive 
compensation, open talent- management practices, 
inclusive diversity as a core company value, employee 
resource groups that bring together those with likeminded 
interests and the opportunity to build a plan to 
achieve their personal purpose. All in, it is about creating 
dignity through work. Our 2016 annual employee survey 
showed employee satisfaction exceeded the industry on 
28 of 29 measures. 
Similarly, our goal is to make owning an Allstate agency 
one of the most rewarding small business opportunities 
in America. The Agency Value Proposition includes great 
branding and advertising, a broad range of products and 
services, training and support, advanced technology 
and loans to agency owners to support their business 
growth. Agency satisfaction did decline slightly in 2016 as 
a result of the challenges associated with increasing auto 
insurance premiums.
 
BRINGING OUT THE GOOD 
Being a great company goes beyond making a profit  it 
means making a difference in the world. I am particularly 
proud that the company was honored for a third 
consecutive year as one of the worlds most ethical 
corporations. Allstate also is making that difference by 
supporting youth empowerment, survivors of domestic 
violence and over 4,800 nonprofit organizations 
throughout the country. Thousands of Allstate agency 
owners and employees committed 230,000 hours of 
volunteer time last year, improving their communities and 
reinforcing respect for Allstates brand. 
I am privileged and proud to lead Allstate because it 
is a purpose- driven organization with talented team 
members who are shaping a better future. In the process, 
we also are building a more valuable company for you. 
Thomas J. Wilson 
Chairman and Chief Executive Officer 
April 12, 2017 


