Dear Shareholders,

In 2010, Gap Inc. took a big step onto the global retailing stage.

With significant advancements in our online and franchise
businesses, as well as entry into two more of the world's top 10
retail markets, 2010 will stand out as the year we positioned the
company to compete and win globally.

We continued to expand share of sales generated from our
International and online businesses. In 2006, 14 percent of our
sales came from online and International. Now, we are on track
to nearly double that percentage within three years.

I'm proud we were able to execute on the next phase of our
strategy while continuing to deliver attractive financial results
for our investors. We achieved double-digit earnings per share
growth for the fourth consecutive year, and we're well-positioned
to deliver further shareholder value in the years ahead.

The hard work of many people across Gap Inc. made these
accomplishments possible. For that reason, on the following
pages, I've asked a few of our leaders to help answer some ques-
tions that have come up in our conversations with shareholders.

We have a compelling company strategy, and now it's up
to us to execute and deliver for our shareholders, employees
and customers.




Glenn K. Murphy
Chairman of the Board and Chief Executive Officer
                                           How should we think about Gap Inc.'s global opportunity?
                                           Glenn: In 2006, we operated in eight countries and only sold products online in the United States.
                                           Now, we're in 31 countries with online sales available to customers in over 90 countries.
                                           Managed by local teams in London, Tokyo and Shanghai, we have the infrastructure to
                                           increase our share of the $1.4 trillion global retail market. Customers increasingly demand
                                           access to our brands through a variety of channels. In response, we've aggressively added
                                           Outlet stores to existing markets and opened in new markets with huge potential. We also
                                           worked with our franchise partners to expand more rapidly around the world. We realize
                                           this is still early days for us and we continue to put the foundation in place to support the
                                           business. However, we are tremendously excited about the global runway in front of us.

                                           Stephen Sunnucks, president, Europe and International Strategic Alliances: Over the past
                                           five years, our European and franchise store base has more than doubled from 165 to 367.
                                           Without a doubt, one of the most thrilling moments during my 30-plus years in apparel re-
                                           tailing was when Gap and Banana Republic opened in Milan this year. In an atmosphere of
                                           excitement and energy, droves of shoppers filled our stores in a city known as the center of

                                           world fashion. Simultaneously, half-way around the world, our Shanghai-based colleagues

                                           introduced Gap to China and made a lasting first impression. Our approach to managing
                                           the business is matching the "global" resources of the company with the "local" experience
                                           of our teams, and I believe it's one of the secrets to our success.

                                           Grace Wong, marketing leader for Gap China: Before launching in China, we invested
                                           thousands of hours to really understand our new customers. Here, consumers shop as a
                                           national pastime, using social media for style inspiration. These insights showed us the
                                           huge potential for Gap's American style. For me, this has been an unparalleled opportunity.
                                           Moving my family from San Francisco to Shanghai was an easy decision. I think our first
                                           marketing campaign, "Let's Gap Together," was captivating because we didn't just export

                                           an American brand  we integrated Gap into the Chinese culture.



                                           Is growing in North America still important to your strategy?
                                           Glenn: Absolutely. We have three very well established brands in Gap, Banana Republic and
                                           Old Navy, along with two up-and-coming brands with huge potential, Piperlime and Athleta.
                                           We understand the strengths of these brands, and I was encouraged that we grew our top
                                           line sales in 2010. To execute with more speed and consistency, we integrated the Outlet divi-
                                           sion with Gap and Banana Republic, and named Art Peck president of Gap North America.
                                           Priorities for 2011: continue to modernize our fleet of stores; focus on attracting new
                                           customers; maximize the fast product pipeline; and introduce more product categories.

                                           Tom Wyatt, president, Old Navy: At Old Navy, our singular focus on our target customer,
                                           Jennie, helped drive our solid performance, increasing comp for the second straight year.
                                           Value, fashion and fun are critical for her. We brought our refreshed store experience to
                                           about 250 stores by the end of 2010. I'm pushing the product teams, every day, to develop
                                           more unique offerings, such as our successful licensed T-shirts for the family. Our new
                                           marketing direction incorporates social media and music to attract more customers to
                                           the Old Navy brand. We're aiming to win and beat the competition, and it starts with

                                           how we serve and delight Jennie.

Simon Kneen, head of design, Banana Republic: Our products are designed in New York
and provide customers with the versatile, modern styles they expect. This pool of products
has become more exciting as our American brand meets our global business needs.
At times, the adjustments for different markets often help to strengthen North America.
For example, Japan has a big appetite for our leather outerwear. To meet that need, we cre-
ated eight or nine different leather styles  from a trench to a bomber. This global approach
allows us the latitude to push creative boundaries. It produces innovative, brand-right
styles that bring additional dimension and character to our North America line.


How does the company differentiate itself?
Glenn: My responsibility is to challenge our teams to be creative and to stand out in
the eyes of our customers, whether they are shopping in a store or online. Leaders must
also balance the drive for innovation with discipline, especially around decision making.
We have many examples of how we've achieved this, such as our online "universality"
platform that allows customers to shop all of our brands at once. In today's competitive
environment, success comes when hundreds and hundreds of fresh ideas  both large
and small  are executed well each month.

Andi Owen, head of Gap Outlet: The Outlet team is constantly brainstorming new ways
of working, and I'm proud of our ability to be creative, nimble and quick on our feet. Working
closely with vendors, our small product team has the freedom to move quickly when we see
                                                                                                 
something resonate with customers. In fact, we can go from concept to design to order
                                                                                                 
within a matter of days. While I was leading Banana Republic Outlet last year, one of our
many brainstorms led to an idea called the "Real Deal." The product and value proposition
was compelling and clear, and the campaign became a huge hit. Customers shopped
much more than the year before, driving our strong performance.

Michelle Chan, senior director at Gap brand: I was given some white space to create new
concept stores to showcase our 1969 Premium Jeans, which led to the 1969 "pop up"
store in Los Angeles. This became our laboratory for countless ideas  covering everything
from visual merchandising to store design to the fixtures that worked best. What we
learned was incorporated into other Gap stores around the world. Most stores have a 1969
presence that has some of the ideas from the LA stand-alone store, and we're excited to         
continue pushing for more brand-enhancing, unique ideas.                                        



Can you share what led to such a strong financial position?
                                                                                                                                              




Glenn: Gap Inc. has a solid financial framework. Maintaining cost discipline allowed us
to invest in growth vehicles while still delivering on the bottom line. Going forward, we're
determined to achieve our goal of growing top line revenue. It's a top priority for the
management team and paramount to our success.

Sabrina Simmons, CFO: Our economic model remains strong, and we were disciplined in
meeting our financial commitments. Earnings per share for 2010 improved by 19 percent,
and we grew net sales by 3 percent. This year, while we invested more in the infrastructure     
for growth, we still achieved the highest operating margin in a decade: 13.4 percent.           
                                             And we're also pleased with our commitment to return cash to shareholders through
                                             share repurchases and dividends. In total, we distributed $2.2 billion in 2010. The strength
                                             and diversity of our portfolio of brands makes these results possible, and also allows us to
                                             successfully navigate changes in the marketplace.

                                             Toby Lenk, president of Gap Inc. Direct: Our division has benefited from some of the new
                                             investments mentioned above, and we understand the central role online plays in the
                                             company's business model. We continued to gain market share this year, through our large
  global online expansion and the encouraging results for Athleta and Piperlime. We're given
                                             the freedom to make investments for the long term, and it's paying off. We're on track to
                                             double our revenue  from about $1 billion in 2009 to $2 billion in 2014.


                                             How confident are you that you've got the right talent to
                                             support your initiatives?
                                             Glenn: Gap Inc. is becoming a truly global company, and we recognize it's a paradigm shift
                                             for our employees and leaders. It requires a new mindset, along with the right processes
                                             and structure. From store managers to senior executives, this rapid progress has created

                                             tremendous opportunities for many people. In Town Hall meetings around the world over
                                             the last year, employees were excited and inquisitive about how we seize this moment in
                                             our history.

                                             Eva Sage-Gavin, head of Global Human Resources and Corporate Affairs: I'm excited that
                                             a number of our leaders are taking on unique, pivotal roles to support the company's
                                             growth strategy. The Gap Global Creative Center in New York is a prime example, with
                                             executives now responsible for global product design, merchandising and marketing.
                                             This was enabled by our leadership development over the last few years, and our continued
                                             emphasis on succession planning. Across the company, our annual survey shows employ-
                                             ees are engaged and energized about our direction, with a continued, strong commitment
                                             to doing what's right. We're proud that Gap helped launch the White House Skills for
                                             America's Future initiative, a partnership of corporations, the U.S. government and
                                             community colleges working to strengthen job skills.

                                             Ira Puspadewi, senior director, Global Responsibility: I support our work to improve the lives
                                             of female garment workers in Southeast Asia who make our products. In 2010, we expanded
                                             our program  called Personal Advancement and Career Enhancement (P.A.C.E.)  that
                                             provides life and workplace skills education. About 3,500 women have participated in the
                                             award-winning training thus far. At one factory, women who completed the training were
                                             promoted at nearly five times the rate of other factory workers. There's a global connectedness

                                             to how this company works  and that makes me proud to come to work every day.
