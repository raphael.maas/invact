A letter to our shareholders
Customer first  these two words have 
created positive change at IGT. By making 
every decision through the lens of whats 
best for our customers, we continue to set 
a benchmark that remains unchallenged. 
The results weve delivered in 2011  driving 
higher revenue, growing gaming operations, 
improving profitability, leveraging our strong 
cash flows, and increasing our Interactive 
presence  are further testament to this fact.
In fiscal 2011, we generated more than $530 
million of revenue in one quarter for the first 
time since 2009 and increased consolidated 
revenues to $1.96 billion. We grew our 
domestic, gaming operations installed base for 
the first time since fiscal 2007 and improved 
our full year consolidated gross profit margin 
150 basis points to 58%. Our total cash 
position grew to over $500 million and we 
decreased IGTs borrowing costs. With the 
addition of Entraction, we have added poker, 
bingo, and sports betting to our robust online 
and mobile portfolio.

While we are pleased with the results weve 
delivered, our journey remains unfinished. We 
are globalizing the international organization 
and introducing our first wave of localized 
content. We are building material efficiencies 
into our Research and Development labs, 
which provide expanded capacity to drive 
breakthrough innovations across our entire 
portfolio. IGTs creative studios are producing 
some of the best games and products our 
company has ever seen.
These impressive financial and operational 
results are a tribute to the unmatched effort of 
the global workforce of IGT. Our employees 
are working hard to drive meaningful change 
and they are improving the organization 
in a way that further differentiates us from 
the competition. We share a collective 
responsibility for the future success of IGT and 
it is our employees who ensure we continue to 
blaze exciting, new trails.
In fiscal 2012, our purpose remains 
unchanged  to lead and transform the gaming 
world by creating and providing the most 
innovative, ubiquitous, disruptive gaming 
experiences and solutions anywhere, for the 
demands of today and the opportunities of 
tomorrow. We are doing so because weve made 
a promise to all IGT stakeholders  customers, 
shareholders, and employees  to generate 
unrivaled growth and value across every aspect 
of our business. With our purpose and promise 
as our foundation, we will be focused on the 
following objectives in 2012:
We plan to increase the velocity of revenue 
growth by capitalizing on our international 
improvements and by penetrating new markets. 
We intend to improve our North American 
replacement share by producing games that 
continue to set the industry standard. We 
aim to build on the momentum in our 

gaming operations business to drive increased 
placements all over the world and we plan to 
find new ways to monetize our vast Intellectual 
Property portfolio.
We intend to further improve our 
profitability by increasing the profit per unit in 
gaming operations and expanding our global 
process efficiencies. Every single day is an 
opportunity to make improvements in our 
processes and in our profitability.
We plan to further energize our Interactive 
business by enhancing our poker platforms 
capabilities, securing significant online poker 
customers and expanding our world-class 
virtual casino content offerings.
Finally, we intend to continue to leverage 
the unsurpassed cash flows we generate to invest 
in future growth opportunities, strengthen 
the balance sheet and augment returns to our 
shareholders.
As we look to the future, we will relentlessly 
push the limits of what is believed to be 
impossible. Throughout the course of all 
industries, there are seminal moments that 
fundamentally disrupt the status quo and 
challenge everyone to think differently. It is 
this never ending challenge  the challenge of 
realizing the impossible  that inspires us to 
innovate in ways that will positively change the 
world forever. 
At IGT, we will continue to seize our full 
potential by championing this spirit, taking 
bold, unconventional steps, and delivering 
experiences that will shape the future of 
entertainment beyond our imagination. 
Patti S. Hart
CHIEF EXECUTIVE OFFICER