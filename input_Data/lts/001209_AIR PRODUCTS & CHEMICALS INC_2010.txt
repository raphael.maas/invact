Executing on innovation, integration and improvement
Growth is but one of our 2015 goals. To reach our return targets,
we must continue to develop a culture of improvement that delivers
productivity and margin expansion. To drive cost leadership and
increase market competitiveness, we will focus on actions that drive
innovation, continuous improvement and the integration of our
businesses.
Innovation has been a cornerstone of Air Products for over seventy
years. We continue to find new ways to reduce the cost of air separation and hydrogen plants. We are also introducing new applications
that make our customers more productive and successful, as well
as developing new products in our Electronics and Performance
Materials businesses.
Integration creates efficiency and is key to success in the industrial
gas business. Our new projects in LaPorte, Texas; Chengdu, China;
and Gent, Belgium are just a few examples of fully integrated on-site
and merchant plants. Connecting the nearly 600 miles of the Texas
and Louisiana hydrogen pipeline systems will improve supply reliability
for our customers and improve returns for Air Products. And we run
our business on a globally integrated model, allowing us to rapidly
transfer best practices while positioning ourselves to meet our customers needs.
We have been using Continuous Improvement (CI) tools to enable
productivity for many years, and CI has contributed to our return
and margin improvements. It is how we operate our business. CI is
a management accountability and an individual responsibility of
all employees. It is how we will deliver higher growth, improve our
pricing and deliver greater cost productivity. In short, it is an important enabler to achieving our 2015 goals.
Delivering revenue, margin, and returns
While we currently see some economic uncertainty, we believe we
have the momentum necessary to meet our 2015 goals. This belief
is based on our strong project backlog, our continued actions to
improve our business operations and market positions, and our cost
management and productivity focus.
For 2012, we expect our capital spending will be in the range of
$1.9 to $2.2 billion, an increase of 20 to 40 percent over 2011. The
majority of this investment will be in projects underpinned by our
on-site business model. We will continue to look for opportunities
that complement our existing positions and build toward leadership
in high growth and emerging markets. Our most recent equity affiliate
investment in Saudi Arabia is one such example.
As always, I am able to look forward with confidence by looking back
at the efforts of our 18,900 employees worldwide. They really are a
differentiator for Air Products. They are the Air Products Advantage.
My thanks to all of them for their dedication and passion in serving our
customers while delivering on the expectations of our shareholders.
Executing on innovation, integration and improvement
Growth is but one of our 2015 goals. To reach our return targets,
we must continue to develop a culture of improvement that delivers
productivity and margin expansion. To drive cost leadership and
increase market competitiveness, we will focus on actions that drive
innovation, continuous improvement and the integration of our
businesses.
Innovation has been a cornerstone of Air Products for over seventy
years. We continue to find new ways to reduce the cost of air separation and hydrogen plants. We are also introducing new applications
that make our customers more productive and successful, as well
as developing new products in our Electronics and Performance
Materials businesses.
Integration creates efficiency and is key to success in the industrial
gas business. Our new projects in LaPorte, Texas; Chengdu, China;
and Gent, Belgium are just a few examples of fully integrated on-site
and merchant plants. Connecting the nearly 600 miles of the Texas
and Louisiana hydrogen pipeline systems will improve supply reliability
for our customers and improve returns for Air Products. And we run
our business on a globally integrated model, allowing us to rapidly
transfer best practices while positioning ourselves to meet our customers needs.
We have been using Continuous Improvement (CI) tools to enable
productivity for many years, and CI has contributed to our return
and margin improvements. It is how we operate our business. CI is
a management accountability and an individual responsibility of
all employees. It is how we will deliver higher growth, improve our
pricing and deliver greater cost productivity. In short, it is an important enabler to achieving our 2015 goals.
Delivering revenue, margin, and returns
While we currently see some economic uncertainty, we believe we
have the momentum necessary to meet our 2015 goals. This belief
is based on our strong project backlog, our continued actions to
improve our business operations and market positions, and our cost
management and productivity focus.
For 2012, we expect our capital spending will be in the range of
$1.9 to $2.2 billion, an increase of 20 to 40 percent over 2011. The
majority of this investment will be in projects underpinned by our
on-site business model. We will continue to look for opportunities
that complement our existing positions and build toward leadership
in high growth and emerging markets. Our most recent equity affiliate
investment in Saudi Arabia is one such example.
As always, I am able to look forward with confidence by looking back
at the efforts of our 18,900 employees worldwide. They really are a
differentiator for Air Products. They are the Air Products Advantage.
My thanks to all of them for their dedication and passion in serving our
customers while delivering on the expectations of our shareholders.
Executing on innovation, integration and improvement
Growth is but one of our 2015 goals. To reach our return targets,
we must continue to develop a culture of improvement that delivers
productivity and margin expansion. To drive cost leadership and
increase market competitiveness, we will focus on actions that drive
innovation, continuous improvement and the integration of our
businesses.
Innovation has been a cornerstone of Air Products for over seventy
years. We continue to find new ways to reduce the cost of air separation and hydrogen plants. We are also introducing new applications
that make our customers more productive and successful, as well
as developing new products in our Electronics and Performance
Materials businesses.
Integration creates efficiency and is key to success in the industrial
gas business. Our new projects in LaPorte, Texas; Chengdu, China;
and Gent, Belgium are just a few examples of fully integrated on-site
and merchant plants. Connecting the nearly 600 miles of the Texas
and Louisiana hydrogen pipeline systems will improve supply reliability
for our customers and improve returns for Air Products. And we run
our business on a globally integrated model, allowing us to rapidly
transfer best practices while positioning ourselves to meet our customers needs.
We have been using Continuous Improvement (CI) tools to enable
productivity for many years, and CI has contributed to our return
and margin improvements. It is how we operate our business. CI is
a management accountability and an individual responsibility of
all employees. It is how we will deliver higher growth, improve our
pricing and deliver greater cost productivity. In short, it is an important enabler to achieving our 2015 goals.
Delivering revenue, margin, and returns
While we currently see some economic uncertainty, we believe we
have the momentum necessary to meet our 2015 goals. This belief
is based on our strong project backlog, our continued actions to
improve our business operations and market positions, and our cost
management and productivity focus.
For 2012, we expect our capital spending will be in the range of
$1.9 to $2.2 billion, an increase of 20 to 40 percent over 2011. The
majority of this investment will be in projects underpinned by our
on-site business model. We will continue to look for opportunities
that complement our existing positions and build toward leadership
in high growth and emerging markets. Our most recent equity affiliate
investment in Saudi Arabia is one such example.
As always, I am able to look forward with confidence by looking back
at the efforts of our 18,900 employees worldwide. They really are a
differentiator for Air Products. They are the Air Products Advantage.
My thanks to all of them for their dedication and passion in serving our
customers while delivering on the expectations of our shareholders.Executing on innovation, integration and improvement
Growth is but one of our 2015 goals. To reach our return targets,
we must continue to develop a culture of improvement that delivers
productivity and margin expansion. To drive cost leadership and
increase market competitiveness, we will focus on actions that drive
innovation, continuous improvement and the integration of our
businesses.
Innovation has been a cornerstone of Air Products for over seventy
years. We continue to find new ways to reduce the cost of air separation and hydrogen plants. We are also introducing new applications
that make our customers more productive and successful, as well
as developing new products in our Electronics and Performance
Materials businesses.
Integration creates efficiency and is key to success in the industrial
gas business. Our new projects in LaPorte, Texas; Chengdu, China;
and Gent, Belgium are just a few examples of fully integrated on-site
and merchant plants. Connecting the nearly 600 miles of the Texas
and Louisiana hydrogen pipeline systems will improve supply reliability
for our customers and improve returns for Air Products. And we run
our business on a globally integrated model, allowing us to rapidly
transfer best practices while positioning ourselves to meet our customers needs.
We have been using Continuous Improvement (CI) tools to enable
productivity for many years, and CI has contributed to our return
and margin improvements. It is how we operate our business. CI is
a management accountability and an individual responsibility of
all employees. It is how we will deliver higher growth, improve our
pricing and deliver greater cost productivity. In short, it is an important enabler to achieving our 2015 goals.
Delivering revenue, margin, and returns
While we currently see some economic uncertainty, we believe we
have the momentum necessary to meet our 2015 goals. This belief
is based on our strong project backlog, our continued actions to
improve our business operations and market positions, and our cost
management and productivity focus.
For 2012, we expect our capital spending will be in the range of
$1.9 to $2.2 billion, an increase of 20 to 40 percent over 2011. The
majority of this investment will be in projects underpinned by our
on-site business model. We will continue to look for opportunities
that complement our existing positions and build toward leadership
in high growth and emerging markets. Our most recent equity affiliate
investment in Saudi Arabia is one such example.
As always, I am able to look forward with confidence by looking back
at the efforts of our 18,900 employees worldwide. They really are a
differentiator for Air Products. They are the Air Products Advantage.
My thanks to all of them for their dedication and passion in serving our
customers while delivering on the expectations of our shareholders.Executing on innovation, integration and improvement
Growth is but one of our 2015 goals. To reach our return targets,
we must continue to develop a culture of improvement that delivers
productivity and margin expansion. To drive cost leadership and
increase market competitiveness, we will focus on actions that drive
innovation, continuous improvement and the integration of our
businesses.
Innovation has been a cornerstone of Air Products for over seventy
years. We continue to find new ways to reduce the cost of air separation and hydrogen plants. We are also introducing new applications
that make our customers more productive and successful, as well
as developing new products in our Electronics and Performance
Materials businesses.
Integration creates efficiency and is key to success in the industrial
gas business. Our new projects in LaPorte, Texas; Chengdu, China;
and Gent, Belgium are just a few examples of fully integrated on-site
and merchant plants. Connecting the nearly 600 miles of the Texas
and Louisiana hydrogen pipeline systems will improve supply reliability
for our customers and improve returns for Air Products. And we run
our business on a globally integrated model, allowing us to rapidly
transfer best practices while positioning ourselves to meet our customers needs.
We have been using Continuous Improvement (CI) tools to enable
productivity for many years, and CI has contributed to our return
and margin improvements. It is how we operate our business. CI is
a management accountability and an individual responsibility of
all employees. It is how we will deliver higher growth, improve our
pricing and deliver greater cost productivity. In short, it is an important enabler to achieving our 2015 goals.
Delivering revenue, margin, and returns
While we currently see some economic uncertainty, we believe we
have the momentum necessary to meet our 2015 goals. This belief
is based on our strong project backlog, our continued actions to
improve our business operations and market positions, and our cost
management and productivity focus.
For 2012, we expect our capital spending will be in the range of
$1.9 to $2.2 billion, an increase of 20 to 40 percent over 2011. The
majority of this investment will be in projects underpinned by our
on-site business model. We will continue to look for opportunities
that complement our existing positions and build toward leadership
in high growth and emerging markets. Our most recent equity affiliate
investment in Saudi Arabia is one such example.
As always, I am able to look forward with confidence by looking back
at the efforts of our 18,900 employees worldwide. They really are a
differentiator for Air Products. They are the Air Products Advantage.
My thanks to all of them for their dedication and passion in serving our
customers while delivering on the expectations of our shareholders.Executing on innovation, integration and improvement
Growth is but one of our 2015 goals. To reach our return targets,
we must continue to develop a culture of improvement that delivers
productivity and margin expansion. To drive cost leadership and
increase market competitiveness, we will focus on actions that drive
innovation, continuous improvement and the integration of our
businesses.
Innovation has been a cornerstone of Air Products for over seventy
years. We continue to find new ways to reduce the cost of air separation and hydrogen plants. We are also introducing new applications
that make our customers more productive and successful, as well
as developing new products in our Electronics and Performance
Materials businesses.
Integration creates efficiency and is key to success in the industrial
gas business. Our new projects in LaPorte, Texas; Chengdu, China;
and Gent, Belgium are just a few examples of fully integrated on-site
and merchant plants. Connecting the nearly 600 miles of the Texas
and Louisiana hydrogen pipeline systems will improve supply reliability
for our customers and improve returns for Air Products. And we run
our business on a globally integrated model, allowing us to rapidly
transfer best practices while positioning ourselves to meet our customers needs.
We have been using Continuous Improvement (CI) tools to enable
productivity for many years, and CI has contributed to our return
and margin improvements. It is how we operate our business. CI is
a management accountability and an individual responsibility of
all employees. It is how we will deliver higher growth, improve our
pricing and deliver greater cost productivity. In short, it is an important enabler to achieving our 2015 goals.
Delivering revenue, margin, and returns
While we currently see some economic uncertainty, we believe we
have the momentum necessary to meet our 2015 goals. This belief
is based on our strong project backlog, our continued actions to
improve our business operations and market positions, and our cost
management and productivity focus.
For 2012, we expect our capital spending will be in the range of
$1.9 to $2.2 billion, an increase of 20 to 40 percent over 2011. The
majority of this investment will be in projects underpinned by our
on-site business model. We will continue to look for opportunities
that complement our existing positions and build toward leadership
in high growth and emerging markets. Our most recent equity affiliate
investment in Saudi Arabia is one such example.
As always, I am able to look forward with confidence by looking back
at the efforts of our 18,900 employees worldwide. They really are a
differentiator for Air Products. They are the Air Products Advantage.
My thanks to all of them for their dedication and passion in serving our
customers while delivering on the expectations of our shareholders.