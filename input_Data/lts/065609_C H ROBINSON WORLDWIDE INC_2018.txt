TO OUR
SHAREHOLDERS

In today�s complex trade environment, businesses are looking increasingly for a logistics
partner that can provide transportation, logistics, and supply chain solutions that
accelerate commerce. At C.H. Robinson, we leverage our talented team of supply chain
professionals; our best in class processes; and our powerful, global technology platform,
Navisphere�, to provide logistics expertise to a growing ecosystem of over 124,000
customers and over 76,000 active contract carriers.
With deep knowledge in eight different service lines and multiple customer segments�
and over 18 million annual shipments and $20 billion in freight under management�
we have unmatched experience and scale that give an information advantage to the
over 200,000 companies that conduct business on our global platform, creating better
outcomes for our customers, contract carriers, and employees.
Strong financial results
Our ability to offer a broad portfolio of services�delivered to a growing ecosystem
backed by our outstanding people, proven processes, and leading technology�translated
into strong financial performance in 2018. We delivered $16.6 billion in total revenues,
$2.7 billion in net revenues, and over $900 million in operating income, each representing
all-time highs for C.H. Robinson.
In our North American Surface Transportation (NAST) business, we honored our customer
commitments in a tight freight environment while pricing to reflect marketplace conditions,
leading to a 17 percent increase
in net revenues. In our Global
Forwarding business, we delivered
another year of market share growth
and maintained our position as the
#1 NVOCC from China to the United
States. We also completed the
integration of Milgram & Company,
Ltd., bolstering our footprint
in Canada and increasing our ability to cross-sell service lines to our customers. Our
Robinson Fresh� business navigated a challenging fresh produce environment to deliver
growth in net revenues and operating income as well as an operating margin improvement
of 190 basis points. Our Managed Services business surpassed $4 billion in freight under
management, with customers increasingly valuing the ability to manage their carrier
selection and complex supply chains without fixed investment in people or technology.
And our European Surface Transportation business continued to grow as well, as we
expanded our customer base.
Altogether, strong operating performance, combined with improved working capital and
a lower tax rate, allowed us to deliver record levels of net income, earnings per share, and
cash flow from operations. This drove nearly $600 million in cash returns to shareholders
through dividends and share repurchases.

Our technology provides an information advantage
C.H. Robinson has been a platform company since its inception; we have always matched
supply and demand to clear transactions across our ecosystem of customers and contract
carriers. In an increasingly competitive freight environment, our ability to turn data into
actionable insights via artificial intelligence and machine learning is an incredible asset for
C.H. Robinson, providing an information advantage to our customers, contract carriers,
and employees.
Customer expectations are changing rapidly across the transportation landscape. When
we talk to our customers, they seek our expertise in three key areas: cost, visibility, and
speed. Our information advantage
enables us to meet the needs of our
customers in each of these areas.
With our ability to provide reliable,
on demand capacity and market
insights that optimize the movement
of customer freight, we help lower
the cost of our customers� supply
chains. Through our power to provide real time visibility to global shipments across our
network, we provide our customers with greater inventory awareness that allows them
to plan more effectively. And with a number of options for fully automated interactions
through our Navisphere platform, we are able to accelerate the pace of commerce across
our customer base.
Our contract carriers are looking increasingly for technology-based solutions to improve
their asset utilization and provide other benefits that enable them to be successful
business owners. As a result, we leverage our information advantage to provide them with
unique and tailored solutions, including planning and route optimization tools that increase
operational asset rates as well as fully automated freight booking and payment processes.
Our investments in technology also drive greater efficiency and better outcomes for our
employees. Our technology currently provides our employees with real time information
that predicts price, cost, profitability, and available capacity. We continue to increase
automated shipment acceptance based upon customer-specific rules to further promote
employee productivity; we also continue to leverage machine learning and artificial
intelligence to refine recommendations and hone our algorithms to become even more
accurate. This allows our customer representatives to accept more freight and provides
more opportunities for our carrier representatives to auto-match capacity, ultimately
leading to more freight under management.
Moving forward, to extend and strengthen our competitive technological advantage, we
will continue to invest in technologies that leverage our experience and scale to provide
increased capabilities and insights to our network of customers and contract carriers. We 

will continue to invest in process improvement and automation, driving operating cost
efficiency across the company and higher levels of service execution for our employees.
And we will continue to invest in our people, ensuring they have robust and rewarding
career opportunities as their jobs evolve and we deploy increased machine learning,
artificial intelligence, and data science capabilities. We believe it is the combination of our
people, our processes, and our technology that enables C.H. Robinson to increase the
logistics expertise we provide across our network of customers and contract carriers and
increase the financial returns we provide to our shareholders; we will continue to invest in
each of these critically important areas.
In closing
In February 2019, I announced my intention to retire as chief executive officer of
C.H. Robinson. As part of a long-planned succession process, I also announced that Bob
Biesterfeld will become the next chief executive officer of our company. Both moves will
become effective May 9, 2019. During his almost two decades at C.H. Robinson, Bob has
consistently demonstrated deep industry knowledge, strategic vision, and a passion for
delivering results. He has been the driving force behind our digital transformation efforts,
accelerating the pace of innovation and technology deployment across our organization.
Bob is an exceptional leader who is committed to C.H. Robinson�s culture and core values,
and I am highly confident in his ability to extend C.H. Robinson�s long track record of success.
As I reflect on my 17-year tenure as chief executive officer of C.H. Robinson, I am very
proud of what we have accomplished. We have aggressively expanded our service
offerings and network to become a comprehensive, global third party logistics (3PL)
company. Over this period, we have increased our total revenues fivefold and returned
$6.1 billion to shareholders through share buybacks and dividends. But overall, I am
most proud of the winning culture we have created and the over 15,000 employees who
continue to drive our success today.
To all our customers, contract carriers, and suppliers, thank you for your business and for
trusting us to help accelerate your global commerce. To our shareholders, thank you for
your investment in C.H. Robinson. And to our employees around the world, thank you for
your hard work this year, and congratulations on your achievements. I take immense pride
in our 2018 results, and I am excited to see the next chapter of growth at C.H. Robinson.
I remain as confident as ever that we have the right people, the right processes, and the
right technology to continue to win in the marketplace.
John Wiehoff
Chief Executive Officer, President, and Chairman of the Board