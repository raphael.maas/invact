A Letter To                   Cardiovascular
Our Stockholders
                              disease is a
                              global burden

                              encountered by


                              millions of people.


                              Each year, an
                              estimated 500,000
                              people undergo
                              open-heart surgery
                              to replace or repair
                              their malfunctioning
                              or diseased
                              heart valves and
                              virtually all high-
                              risk patients in the
                              operating room or
                              intensive care unit
                              are candidates for
                              having their cardiac
                              function monitored.

The fight to make a positive difference            product introductions in the repair and
in the lives of these patients and their           replacement segments driving global
families requires meaningful innovation to         growth. In particular, the introduction
enhance today's technologies. Edwards              of a new aortic valve and repair ring
Lifesciences' 50-year commitment to                helped lift the U.S. growth rate. In 2009,
discovery has led to many of the most              on an underlying basis, we gained share
advanced heart valve therapies and                 and grew our global surgical heart valve
critical care monitoring technologies in           therapy product line by 8 percent, faster
the industry. In 2009, we continued to             than the market growth rate.
build upon this legacy by introducing
several new products and furthering our            In our transcatheter heart valve product
progress in developing new technologies            line, the number of valve procedures
designed to transform patient care.                performed commercially in Europe and
                                                   abroad more than doubled compared to the
                                                   prior year. With surgical and transcatheter
Another Successful Year of Growth in 2009
In a year of slow economic growth,                 valves combined, our Heart Valve Therapy
Edwards not only continued to build on our         franchise grew almost 18 percent on an
distinguished track record, but also met or        underlying basis.
exceeded all of our 2009 financial goals. We
achieved total reported sales of $1,321 million,   Since receiving CE Mark in 2007, thousands
which represents an 11 percent underlying          of patients have benefited from the
growth rate. We stayed very focused on             revolutionary Edwards SAPIEN transcatheter
delivering strong growth while improving           heart valve. We are pleased to be able to
profitability, and in 2009 our gross profit        offer this life-saving technology to patients
margin reached 69.8 percent. In addition,          in over 30 countries whose severe aortic
we achieved full year non-GAAP diluted             stenosis largely goes untreated.
earnings per share growth of 19.6 percent
and generated non-GAAP free cash flow of           In the U.S., we completed enrollment
$178 million, beating our $160 to $170 million     of our landmark PARTNER Trial--the
goal. Our shareholders were rewarded with a        world's only randomized, pivotal clinical
58 percent increase in share value for 2009.       trial of a transcatheter aortic valve--
                                                   which assures our substantial lead in
During 2009, we planned to increase the            providing this therapy to a large patient
growth of our market leading surgical              population. We applaud the trial's
Heart Valve Therapy products, extend our           investigators for their significant efforts
leadership in Transcatheter Heart Valves           in meeting this important milestone,
and raise our Critical Care growth rate            and for their dedication to the follow-
through innovation.                                up that continues into 2010 and beyond.
                                                   Assuming a favorable outcome and a one
I am proud of the accomplishments we               year approval process, we would expect
made in surgical heart valves, with major          U.S. approval of our Edwards SAPIEN



transcatheter heart valve in 2011. In Japan,     We are fortunate that our strong
we completed our first compassionate             operating performance and cash flow
use cases and look forward to starting a         enabled us to continue helping worthy
clinical trial in 2010.                          organizations make a difference,
                                                 and in 2009 our Board of Directors
Building upon this momentum, we made             approved a $15 million contribution to
excellent progress on our next-generation        The Edwards Lifesciences Fund, which
Edwards SAPIEN XT transcatheter heart            will allow us to have a positive impact
valve, which enables access to smaller           on our global communities for years to
arteries, thereby expanding the treatable        come. During the year, the Fund made
patient population. In the U.S., we submitted    grants of $4.6 million to more than 100
our study design for the Edwards SAPIEN          nonprofit organizations.
XT valve to the FDA and expect to begin
enrolling patients in 2010. This trial, called   Ensuring Innovative Minds Help Provide
PARTNER II, includes both our new 18 French      Patients with Stronger and Healthier Hearts
NovaFlex transfemoral and the Ascendra           Key to our company's success is
2 transapical delivery systems. And, our         developing innovative and enduring
progress continues with the recent CE Mark       products that not only fuel top line
for the Edwards SAPIEN XT next generation        growth, but also enable clinicians to make
transcatheter valve and its delivery systems,    a positive difference in the lives of the
leading to a European rollout in 2010.           patients they treat. In 2009, we made
                                                 significant investments in our future by
In our Critical Care franchise, sales of         committing more than $175 million to
newer products--particularly the FloTrac         develop new technologies, an increase
system--continued to achieve strong              of 26 percent over the prior year. We
results. As part of our strategy to pursue       are excited about the new products we
innovative technologies, we rolled out           introduced during 2009 and look forward
an upgrade to our FloTrac system to              to continued momentum in 2010 driven
extend its applicability to additional           by our robust product pipeline.
clinical settings. We also made significant
progress in improving the accessibility of       In 2009, we introduced several new
our parameters to more physicians and            products to help address the prevalence
strengthening the clinical evidence that         of cardiovascular disease. In Heart Valve
demonstrates the value of this technology.       Therapy, we launched the Carpentier-
                                                 Edwards PERIMOUNT Magna Ease aortic
Our Critical Care product line grew by           valve, a new best-in-class surgical heart
approximately 5% on an underlying basis,         valve. The next-generation design of
which was lower than our original projections.   this valve sets a new standard for tissue
Sales growth improved in the second half of      valves by combining enhanced ease-of-
the year as constraints on hospital capital      implant with unsurpassed hemodynamics
spending lessened.                               and proven long-term durability.



Nearly 100,000 patients undergo mitral        Committed to the Future
valve repair surgery each year. To better     As we look to the future, Edwards remains
address the needs of this patients, we        focused on delivering on our commitments
introduced the Carpentier-Edwards             to shareholders, while we continue to strive
Physio II mitral valve repair ring in the     to live our Credo. Our financial goals for
U.S. and Europe. This advanced repair         2010 build on our momentum and we expect
product for degenerative mitral valve         to deliver another year of strong growth
disease provides the security of a proven     and profitability. Excluding special items,
design based on 40 years of experience        we expect to generate sales for the year in
in mitral valve reconstruction.               the range of $1,430 to $1,500 million and
                                              increase our gross profit margin by 50 to
Many patients in need of heart valve          100 basis points. In addition, at the same
therapies are seeking less invasive           time we are investing aggressively in R&D,
approaches that provide benefits such         we intend to grow net income 17 to
as smaller incisions, quicker recoveries      19 percent for the year and generate annual
and minimal scarring. Continuing our          free cash flow of $190 to $200 million.
strategy of developing more minimally
invasive products, we introduced the          Edwards Lifesciences is committed to
PORT ACCESS EndoDirect System in the          developing technologies that advance care
U.S. This system's combination of tools       so that patients do not simply survive, they
enables surgeons to perform intricate         thrive. We will continue to act boldly and
valve procedures while avoiding the larger,   decisively to ensure that the innovative
more invasive incisions associated with       minds of today can provide the patients of
traditional surgery.                          tomorrow with stronger and healthier hearts.

In the critical care community, there is      We are grateful that our shareholders
growing evidence that closely monitoring      support our dedication to patients and we
glucose levels in critically ill patients     are truly appreciative of your trust.
can positively impact their care and
outcomes. However, existing intermittent      Sincerely,
monitoring methods make this challenging
for hospitals to employ on a widespread
basis. To help address this problem, we are
developing a real-time glucose monitoring     Michael A. Mussallem
                                              Chairman and Chief Executive Officer
system to allow clinicians to better manage
the glycemic levels of patients in an
                                              
operating room or intensive care setting.
                                              
During the year we developed our first-       
generation product and have now begun        
a clinical evaluation of this product with    
researchers in European hospitals.          