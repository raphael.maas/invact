SHAREHOLDER
MESSAGE:
A HISTORIC
TRANSACTION
Dear Fellow Shareholders: The results of Fiscal
2002 have been somewhat overtaken by the
historic June 13 agreement between Heinz and
Del Monte Foods, a transaction designed to
unlock shareholder value. Although that very
positive agreement is the focus of this letter, I would like to first set the
stage with a few highlights from the past year. Fiscal 2002 was
characterized by both achievements and challenges. We achieved a
nearly 7% increase in sales for the year, due primarily to several exciting
acquisitions and the continued success of our innovation strategy. Key
acquisitions included Classico premium pasta sauces, Delimex frozen
Mexican foods, Poppers frozen snacks, and licensing rights for the T.G.I.
Fridays brand of frozen appetizers.
Our increasing focus on innovation and marketing also strengthened
Heinzs brands, with consumer and trade marketing spending increasing
a combined 11.6%. Innovations included purple and mystery varieties of
EZ Squirt kids condiments, Jack Daniels grilling sauces, StarKist Lunch to
Go, Heinz Ketchup Kickrs, Boston Market HomeStyle pot pies, Ore-Ida
Funky Fries, Pup-Peroni Nawsomes dog snacks, new organic varieties of
ketchup in the U.S. and Heinz Simply baby food in the U.K.
The strength of our brand is reflected by Heinz ketchup, where market
shares reached a record 60% in the U.S. and 66% in the U.K., driven by
continued innovations. Smart Ones and Boston Market HomeStyle Meals
scored double-digit sales growth behind new campaigns featuring Kristi
Yamaguchi and Larry Bird.
Cash flow from operations improved substantially to $891 million,
from $506 million in the prior year, and we made great progress in
setting the stage for future improvement in working capital.
Heinz recorded diluted earnings per share of $2.39 in Fiscal 2002
(excluding special items), in line with the companys earnings outlook
of November 8, 2001 and the subsequent market consensus. This was
6.3% below the prior year, reflecting the challenges in pet food and
Australia/New Zealand; the strength of the U.S. dollar; and the impact
of the economic slowdown on foodservice.
We are building on our achievements and addressing our
challenges aggressively. We look for currency exchange rates to be
more favorable in the coming year. We have new, highly motivated
management in Australia/New Zealand. Most importantly, foodservice
appears to be recovering.

Finally, we are preparing for a stronger future and strengthening our
businesses by addressing the portfolio issues that have obscured our
great progress in frozen food and ketchup, condiments and sauces.
This will be accomplished through the transformative Heinz/Del Monte
transaction whereby Heinz will spin off its U.S. and Canadian pet food
and pet snacks business and its U.S. tuna, infant feeding, retail private
label soup and gravy and College Inn broth businesses and merge them
with Del Monte Foods. This has the potential to create a win-win for not
only Heinz shareholders, but also our employees and the communities in
which we operate.
The strategy behind this transaction is to make Heinz a more focused,
more predictable and faster-growing business. It will enable us to
leverage our greatest strengths in a more effective manner. First, we will
have a stronger portfolio, with a U.S. business that will rank among the
best in the food industry. Our global business, post transaction, has grown
sales at a healthy 5% annual rate over the past two years, compared to
2.6% for the pre-transaction business. This performance includes growth
rates of 7% for North American grocery, 16% for Heinz Frozen Food and
7% for Europe.
Second, this transaction will greatly simplify Heinz and refocus our U.S.
business against two highly profitable categories (Ketchup, Condiments
& Sauces and Frozen Foods) and two strong channels (retail grocery
and foodservice) where we have a proven track record and a pipeline of

exciting innovations. Globally, we plan to increase marketing by more
than $100 million in Fiscal 2003 to support this innovation. We will be
able to spend more on fewer businesses and drive faster growth.
Third, we anticipate Heinz will be more transparent, with a sharper
focus on two strategic food platforms: Meal Enhancers  like ketchup,
condiments and sauces around the world  and Meals & Snacks, both
frozen and ambient, for all ages, from infants to teens to adults.
Fourth, this transaction should unlock significant shareholder value.
It removes businesses that have experienced negative trends and whose
margins are lower than our post-transaction core. Additionally, the
increased focus, better growth prospects and more predictable
performance should support an expanded price/earnings multiple.
We will, as part of this transaction, adjust our dividend beginning April
2003 to recognize the companys smaller size and free up substantial
cash flow to help underwrite additional marketing and/or pay down
debt. We expect to pay an indicated annual dividend of $1.08 per share
starting in April 2003, a 33% reduction from the present rate of $1.62
per share. We recognize the importance of the dividend to many of our
shareholders and, for this reason, have provided time to adjust to the
new payout. The dividend remains an important part of Heinzs
commitment to shareholder value. Following the adjustment, we
anticipate a dividend payout ratio slightly better than our peer group
average of 48% and in the top quartile of the S&P 500 (whose average
is 29%). Our current dividend payout represents almost 70% of
earnings and greatly limits the companys ability to grow. We believe
that investing some of these resources in faster growth will provide Heinz
shareholders greater, more tax-efficient value in the long run.
The transaction should immediately improve Heinzs debt position,
reducing our debt by approximately $1.1 billion on the date of closing.
We aim for a further $1 billion reduction in debt by the end of
Fiscal 2005.
This is a powerful initiative, which Heinz has contemplated for some
time but would not transact without a suitable partner. We have found
that partner in Del Monte, which should become a center-of-store
powerhouse by adding StarKist, 9-Lives, Pup-Peroni and Kibbles n Bits.
Del Montes already-proven management team will benefit from an
infusion of outstanding new talent from Heinz. The companys new
businesses will bring superior margins and returns versus Del Montes
present portfolio and therefore should prosper through increased

investment and focus. Importantly, the businesses will remain in
Pittsburgh, a key consideration, as we wanted to minimize disruption to
our employees and communities. These new businesses will give Del
Monte increased scale, greater retail leverage, significant opportunities
for operating synergies and a much-improved stable of brands.
Fiscal 2003 will be a transition year, as this transaction is not likely
to close until almost three-quarters through the fiscal year. Accordingly,
our Fiscal 2003 outlook seeks to establish a solid new performance
benchmark that reflects the significant increase in our marketing
investment and related transaction costs. Our estimated EPS target for
Fiscal 2003 from continuing operations is around $2.00 per share,
excluding deal-related charges.
Looking ahead, Heinz will be well positioned with a superior margin
and growth profile. From our new base, our goal is to deliver consistent
performance across a wide range of indicators, such as sales and
earnings growth, improved cash flow and return on invested capital.
On a personal note, I want to acknowledge the contributions of Heinz
officers concluding their tenures with the company. Dave Williams is
retiring from Heinz and its board in September after 35 years. I want to
pay special tribute to his outstanding leadership in Europe. Heinz will
nominate Dave to the post-transaction Del Monte board. Paul Renne,
a 28-year veteran, retired from the board and Heinz in December. He
was a highly regarded steward of our finances. Dick Wamhoff, retiring in
September after 34 years, deserves special thanks for his achievements
in many senior roles, most recently in Asia/Pacific. I also wish to
recognize Bill Goode, who will leave in December after 39 years of
stellar service across many Heinz businesses. In July, Karyll Davis winds
down her Heinz career. During 26 years, she has made valuable
contributions, particularly as corporate secretary.
In conclusion, the Heinz/Del Monte transaction furthers Heinzs
vision of being the worlds premier food company. This is an exciting time
for Heinz, and we look forward to delivering on the great promise and
potential that we see before us in the coming years.
William R. Johnson
Chairman, President & Chief Executive Officer