Dear Shareholders,
PNC had another successful year in 2014. We reported net income of
$4.2 billion, or $7.30 per diluted common share. Our return on average
assets was 1.28 percent. We added customers, grew loans by $9.2 billion
and increased deposits by $11.3 billion. Additionally, we reduced expenses by
nearly $200 million, strengthened our capital position and achieved a more
liquid balance sheet.
The fourth quarter of 2014 was PNCs seventh consecutive quarter of $1 billion or more in net income.
While past performance does not guarantee future results, that consistency through what continues to
be a very challenging revenue environment is a testament to the soundness of our strategic direction
and the commitment of our employees to creating long-term value for our shareholders.
In 2014, we grew tangible book value, an important estimate of shareholder value, by 10 percent over
the prior year. Also, PNCs stock reached an all-time high share
price in December and fi nished the year 18 percent higher than
at the close of 2013. That performance ranked second among the
fi rms in our identifi ed peer group, which averaged a 9 percent
increase, and compared favorably with 11 percent for the S&P 500.
We also returned signifi cant capital to shareholders in 2014.
We announced plans to repurchase up to $1.5 billion worth of
common shares over the four quarters beginning in April 2014.
Under that plan, as of December 31, we had repurchased 12.9 million common shares for $1.1 billion.
We also raised the quarterly dividend on our common stock to 48 cents per share in the second quarter.
Given our strong capital position, we remain committed to returning additional capital to shareholders,
subject to the regulatory Comprehensive Capital Analysis and Review (CCAR) process.

And, we advanced to No. 2 among super-regional banks on Fortunes annual list
of most-admired companies.
We have achieved all of this in the current environment in much the same way
we established ourselves as one of the strongest-performing fi nancial services
companies and grew through the crisis  by focusing our efforts on helping our
customers realize their fi nancial goals without expanding our risk appetite and by helping
our communities prosper. Thats what it means to us to be a Main Street bank rather than
a Wall Street bank.
Calling ourselves a Main Street bank is not a declaration of our capabilities or a comment on the
sophistication of our people; it is a statement about the philosophy that governs how we relate to and
serve our customers and communities. Those relationships are at the heart of our business model.
By and large, we live and work where our customers live and work. We understand their fi nancial goals.
Whether they are saving for retirement, looking to buy a new home, building a small business
or growing a large company, we are there to help. When they thrive, we thrive.
We have the experience, expertise and innovative products to provide
our customers with the insight they need to chart their fi nancial future
and to match up against our Wall Street competitors, but we dont take the
same risks, and we dont trade long-term value for short-term opportunity.
At the heart of our corporate culture is the fundamental belief that if
we always strive to do right by the people we serve and if we treat our
customers well, we will make money and be profi table through time.
It is a simple model, the purity of which is well suited for the environment in
which we are operating.
This is a very challenging time for our industry. The regulatory environment
continues to evolve, and the cost of compliance continues to rise.
Competition grows more intense, even appearing at times to be irrational
as other institutions seem to compromise long-term profi tability for the
sake of winning new business. And the perpetuation of historically low
interest rates has made it extremely diffi cult for banks to grow net interest income.
At PNC, we expect that interest rates will begin to rise later in the year as the domestic economy
continues to expand. However, global macroeconomic factors are likely to persist and could affect the
timing of otherwise anticipated rate increases.

Controlling What We Can Control  Executing on
our Strategic Priorities
PNC is positioned to perform well in a rising rate environment, but we recognize that net interest
income will remain under pressure until rates do rise. As a result, in recent years we have
focused on strategic priorities aligned to drive an increase in fee income across our diversifi ed
lines of business. In essence, we are working to control what is in our power to control and to
mitigate the impact of market headwinds.
Our strategic priorities work together to position us to achieve targeted outcomes  expanded
market share, deeper customer relationships, increased fee income and improved operating
effi ciency  that form a solid foundation for long-term value creation. In 2014 we made important
progress against each of our priorities.
Driving Growth in New and Underpenetrated Markets
Capturing More Investable Assets
Three years after our acquisition of RBC Bank
(USA), we are growing in the Southeast across
all of our lines of business faster than in our
legacy markets. Prior to 2012, we had only a small
presence in this region, but today it represents
an increasingly important source of revenue and
opportunity for organic growth of the company.
Brand awareness is up in the Southeast to about
65 percent from 50 percent a year ago. We have
fully built out our teams across the Southeast
markets. And with our start-up costs behind us,
we are competing to develop relationships, win
business and gain share.
It will take time for us to build a leading banking
franchise in these intensely competitive
markets, but we are exceeding our expectations
to this point and are very excited about our
potential in the region. Meanwhile, in addition
to our success in the Southeast, we are making
signifi cant strides in other underpenetrated
markets such as Chicago and St. Louis, as we
deliver value-added solutions to our customers.
We are experiencing signifi cant revenue growth
in these markets, and we continue to strengthen
our brand.
Every customer can use help planning their
fi nancial future. That is as true for the college
student who opens a Virtual Wallet account
through one of our University Banking
branches as it is for baby boomers approaching
retirement or, for that matter, business leaders
thinking about how to better manage their
companies pension funds.
PNC has been in the asset management
business for a long time, and we have
established our company as one of the largest
bank asset managers in the United States. In
recent years, we have worked to make investing
and retirement a part of every client
conversation, and our employees 
regardless of the line of business they
primarily serve  have taken up the
charge to help our customers ask and
answer critical questions about these
issues.
Our Asset Management Group grew
client assets under administration in 2014 to
$263 billion as of December 31, up $16 billion
from December 31, 2013, driven by stronger
equity markets, new sales production and
cross-sell referrals from other lines of business.

Redefi ning the Retail Banking Experience
Building a Stronger Mortgage Business
Our transformation of the retail banking
experience continues to gain momentum as
customer preferences concerning how, where
and when they bank continue to evolve. Today,
nearly 50 percent of our retail customers prefer
non-branch channels for their routine banking
transactions, opting instead for the convenience
of digital, ATM and telephone banking services.
We now process more than 35,000 mobile
deposits every day  more than double the
number we processed just two years ago.
When customers visit a branch, they often are
looking for a fi nancial consultant who can help
them open a new account, make a decision
about their fi nancial goals, or solve a problem.
With this in mind, we have been working to
convert many of our traditional branches to
a new, more effi cient model that offers more
technology and more opportunities for our
employees to have a meaningful sales and
service conversation with our customers.
To date, we have completed nearly 300 of these
conversions. We also are piloting different
branch styles as well as new services such as
instant card issuance, which enables us to print
a customers new debit card in the branch in a
matter of minutes.
At the same time, recent transformation efforts
and deposit product changes have resulted
in fee growth across several areas including
merchant services, deposit service charges,
brokerage and credit card as our conversations
with customers create opportunities to increase
our share of wallet.
Since PNC re-entered the residential mortgage
banking business with the acquisition of
National City Corporation and its mortgage
business at the end of 2008, our goals for
mortgage have been twofold. First, we have
been cleaning up past issues that were systemic
at National City and with many mortgage
lenders across the industry. We have made a lot
of progress on that front, and the majority of this
work is now behind us. Second, we have been
building PNCs residential mortgage product
into one that enhances our brand, complements
our other home lending products, and cements
our relationship with customers for whom
buying a home will likely be the most important
fi nancial transaction of their lives.
In 2014, we introduced PNC HomeHQ and PNC
Home InsightSM Tracker, online tools that enable
home buyers to easily research and apply for
home lending products, track the status of their
applications throughout the origination process,
and manage their accounts across digital
platforms. These new tools are part of a longterm
plan to improve the ease and convenience
of home buying.
PNCs mortgage origination volume was down
in 2014, but we outperformed most bank
peers even as the industry overall continued to
experience origination declines. As a result, we
grew relative market share. Also, late in 2014
we announced that PNC Senior Vice Chairman
Joseph Guyaux would assume leadership
of PNC Mortgage as its president and chief
executive offi cer, effective January 31, 2015.
Succeeding him as PNCs chief risk offi cer is
Executive Vice President Joseph Rockey.

Bolstering Critical Infrastructure and Streamlining Core Processes

Critical to our success across all of our priorities
are the reliability, functionality, scalability and
security of our information systems and the
effectiveness and effi ciency of our internal
processes. As weve seen in high-profi le cases
within our industry and others in recent years,
few things can damage a companys reputation
and its relationships with its customers faster
than failed technology and data breaches. There
was a time, not that long ago, when a bank
executives chief security concerns had to do
with the quality of the vaults in the branches.
Today, we are constantly working to ensure that
our online applications are always accessible by
our customers and that our customers data and
money are secure from threats posed not just by
men in masks but by cyber criminals attempting
to break in from halfway around the world.
A little over a year ago we announced a multiyear
effort aimed at turning technology into a
true competitive advantage by not only continuing
to offer new and innovative products and
services but by bolstering our infrastructure to
dramatically improve the reliability and security
of our systems. In 2014, we made signifi cant
progress toward those goals, initiating a new
data center strategy that will allow for greater
standardization and improved software support
while helping to prevent system failures or to
recover much more rapidly in the rare event that
an outage does occur. We also have upgraded
cyber defenses that detect and repel thousands
of threats each day, and we work closely with
industry and intelligence community partners to
investigate and take action against attackers.
Internally in 2014 we launched the Better Way
program, an employee-driven, customerfocused
approach to improving how work gets
done across the company. Better Way empowers
employees to suggest process improvements
to positively impact our customers, resulting
in more satisfi ed customers, greater effi ciency
and improved productivity. The program was
piloted in about 10 business units in 2014 and
will continue to roll out across the company in
the months ahead.

Seeing Results
We are making signifi cant investments in our
businesses and technology even as we have
reduced expenses. At the same time, our
execution against our strategic priorities is
resulting in growth in fee income as designed.
In 2014, we grew full year fee income by
$189 million, or 4 percent. And, excluding
residential mortgage, which continues to be
subject to headwinds that are impacting the
entire industry, fee income increased in 2014
by $442 million, or 10 percent.

Delivering a Superior Banking Experience for Every Customer
In an industry built around products that, at their most basic level, are largely the same from
one provider to the next, we aim to differentiate ourselves from the competition by continuing
to improve the way we engage with our customers. In spite of industry consolidation in recent
years, customers have nearly unlimited options when it comes to choosing a bank, so it is more
important than ever that when a customer chooses PNC we deliver an unparalleled experience
with exceptional service every time, across every channel.
To that end, in 2014 we named PNC Executive Vice
President and Chief Marketing Offi cer Karen Larrimer to
serve as the companys fi rst Chief Customer Offi cer and
to lead PNCs new Offi ce of the Customer. Under Karens
leadership, the Offi ce of the Customer is working to
improve policies that unnecessarily burden or negatively
impact our customers.
Building on the values that have long defi ned our culture,
our employees are taking greater accountability for
addressing customers concerns and solving customers
problems. We are looking at and refreshing our approach
to face-to-face, telephone care center and even social
media engagement to have more productive conversations
with the people we serve. And we are giving our employees
more avenues by which to speak up when they have
ideas for improving the customer experience or learn of
customer concerns.
While we recognize the reality that perfection is unattainable, we endeavor to distinguish
ourselves and win in the marketplace through our efforts to deliver a superior banking
experience for every customer.

Delivering for our Communities and Employees
Our success is not just determined by how well we serve our customers or the value we
create for our shareholders; it also is dependent upon the efforts we undertake to help our
communities and our employees prosper.
Throughout 2014 we celebrated the 10-year anniversary of PNC Grow Up Great, PNCs
$350 million initiative to expand access to quality early childhood education, particularly for
underserved children. To date, Grow Up Great has helped more than 2 million children get
a better academic start and a better chance at lifetime success.
We also continued our commitment to green building, topping off construction of our new
headquarters tower in Pittsburgh, which will be the worlds greenest offi ce tower when it opens
in the fall of 2015. And our net-zero-energy branch in southern Florida achieved LEED Platinum
certifi cation. Additionally, as you can read in our 2014 Corporate Responsibility Report, available
at PNC.com, our businesses implemented a number of important changes to make health and
environmental concerns a more prominent factor in PNCs lending.
And for our employees, in 2014 we launched a major new talent development effort designed
to create greater opportunity for professional growth and career advancement. The effort also
makes managers throughout the company more accountable for building diverse teams and
for identifying and promoting employees who distinguish themselves while demonstrating
commitment to PNCs values, risk management and ethical standards.

Great Expectations
Overall, we had a successful year in 2014. In spite of the challenges that persist, we are even
more excited about the opportunities in front of us and how we are positioned as we enter 2015.
We are a year further into the execution of our strategic priorities and continue to make
important progress. We enjoy a strong capital position. Our balance sheet is now compliant with
the liquidity coverage ratio requirements that went into effect in 2015. And we have a terrifi c
team of employees across the franchise committed to delivering a superior banking experience
for every customer and focused on creating long-term value for our shareholders.
I would like to thank our customers for continuing to trust PNC to help them achieve fi nancial
well-being. I also would like to thank our board of directors for their service to our company on
behalf of all of our shareholders, as well as for their ongoing counsel and support. In October,
we welcomed a new director, A&R Development Corp. President and Chief Operating Offi cer
Marjorie Rodgers Cheshire. And at this years annual meeting in April we will say goodbye to
three directors who are retiring. Richard Berndt; George Walls, Jr.; and Helge Wehmeier are
three extraordinary individuals whose leadership and vision helped to shape our strategic
direction and guide our execution throughout their years of service on our board.
Finally, I would like to thank you for your confi dence in our company and for your continued
support as we work to deliver for our shareholders, customers, employees and communities.

Sincerely,
William S. Demchak
Chairman, President and Chief Executive Officer


