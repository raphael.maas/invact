                                            Clorox Shareholders and
                                                   Fellow Employees
                                                                      > We generated $363 million in economic profit (EP)**,
I feel very good about the performance of The Clorox Company
                                                                        including the near-term dilutive effect of acquiring Burt's
in fiscal year 2008, especially given the unprecedented
                                                                        Bees. While EP was down slightly from $379 million in fiscal
commodity cost environment and overall inflationary pressure
                                                                        year 2007, we believe this acquisition will position us for
on the consumer. I am especially proud of our employees, who
                                                                        higher EP growth over the long term.
made significant progress on the Centennial Strategy that I
outlined for you in last year's letter. Below are some highlights
from fiscal 2008.                                                     We Delivered Significant Results on Our Centennial
                                                                      Strategy
                                                                      We successfully launched our Centennial Strategy, including
2008 Was a Solid Year in a Challenging Environment
Clorox had a solid year in fiscal 2008:                               our focus on EP. Double-digit annual EP growth over time is our
                                                                      "true north" goal because we believe it strongly corresponds
> We grew sales 9 percent to $5.3 billion. This was our               with shareholder value creation. Looking at our business
  seventh consecutive year of top-line growth of 4 percent or         through an EP lens gives us insight into the greatest opportunities
  more, at or above our annual target of 3 percent to 5 percent       for value creation and drives the four strategic choices we
  sales growth. It was also our strongest top-line growth in          made as part of the Centennial Strategy. In fiscal 2008, we
  seven years.                                                        made significant progress in each of the following areas:
> We generated cost savings of $93 million, on top of more            Be a high-performance organization of enthusiastic owners
  than $100 million in cost savings in each of the prior six years.   Our first strategy is about our employees. When everyone is
                                                                      engaged and working well together toward a shared vision and
> Despite absorbing commodity cost increases of more than
                                                                      common goals, we can do just about anything. Studies have
  $100 million above our initial estimates, we delivered
                                                                      shown the No. 1 driver of engagement is employees'
  earnings per share solidly within our original outlook for
                                                                      understanding of and connection to the company strategy.
  the fiscal year, excluding certain charges. Diluted earnings
                                                                      Following the initial rollout to leaders in June 2007,
  per share for the year were $3.24, which includes an impact
                                                                      communicating our Centennial Strategy across the organization
  of 26 cents of restructuring-related charges and 9 cents
                                                                      was a top priority. We moved quickly to engage and enroll
  dilution associated with the acquisition of Burt's Bees, Inc.
                                                                      employees through employee meetings, online training and
> Our financial condition remains very strong, with net cash          printed materials. We also increased the frequency of strategy
  provided by operations of $730 million.                             review meetings with business units and functions, contributing
                                                                      to stronger cross-company linkages and alignment while using
> Strong free cash flow* of $560 million enabled us to partially      EP as a means to determine resource allocation.
  pay down debt issued to finance the acquisition of more than
                                                                      
  12 million shares of Clorox stock and to finance the Burt's
                                                                      
  Bees acquisition.                                                   




We are also working hard to build on our strong culture of                                                 our U.S. economic profit. To accelerate growth in this channel,
collaboration and teamwork across the company. At the                                                      we increased staffing in our customer-facing organization and
beginning of fiscal 2008, we established a formal team of highly                                           continued to build strong capabilities our retail partners value.
engaged employees from around the world to represent the                                                   As a result, our grocery customers named us to 17 new
voice of all employees to surface the best ideas for making                                                category advisory positions. In addition, our focus on improved
everyday Clorox life better. In addition, we launched a manager                                            assortment and shelving resulted in more than 90 new Clorox
certification program and a new peer-to-peer recognition                                                   Company items on shelf across a number of grocery retailers.
program, just to name a few accomplishments.                                                               Our emphasis on the grocery channel drove a sales turnaround
                                                                                                           of 5 percentage points from the past four-year trend. I believe
In March 2008, we conducted one of our periodic companywide                                                we are laying a foundation for accelerating growth in the
surveys to measure employee engagement. Survey results                                                     channel, which will remain an area of focus over the longer
indicated a significant increase over the prior year in key areas                                          term. We are optimistic that this effort will generate incremental
that typically drive engagement and contribute to how much                                                 sales and profit.
discretionary effort employees put into their work and how
long they intend to stay with the company. We are pleased with                                             Accelerate growth both in and beyond the core
our progress and remain focused on continual improvement in                                                We realize that, to achieve our true north goal, we must
this area.                                                                                                 accelerate growth in our core businesses as well as beyond in
                                                                                                           the categories, channels and countries where we do business.
Win with superior capabilities in the "3Ds": Desire, Decide                                                One of the ways we're doing this is by capitalizing on the four
and Delight                                                                                                significant consumer "megatrends" we identified: health and
Desire is about ensuring consumers are predisposed to buy                                                  wellness, environmental sustainability, convenience and a
our brands before setting foot in the store. We do this by                                                 more multicultural marketplace.
communicating consistent messages about why our products
are the best choice to meet their needs. Decide is about                                                   Clearly, fiscal 2008 was a year of strong sales growth. We
winning at the store shelf through superior packaging,                                                     launched the Green WorksTM line of natural cleaners, which far
assortment, merchandising, pricing and shelving. Delight is                                                exceeded our initial expectations. The brand received the 2008
                                                           ,
about striving to ensure our products exceed consumers                                                     Innovation and Creativity Award from the Grocery
expectations when they take our products home, which is an                                                 Manufacturers Association and a Sustainability Innovation
                                                                                                                                            
important step toward earning lifetime loyalty.                                                            Award from Wal-Mart. The Brita team did an outstanding job

A key element of our strategy to win in the area of "decide" was
incremental investment in the grocery channel, which
represents about a third of our U.S. sales and more than half of



of positioning our water-filtration brand to capitalize on trends        Building on our long-standing heritage of environmental
in sustainability and convenience, driving substantial category          stewardship, we established a companywide environmental
growth and all-time record sales for the fiscal year. The Burt's         sustainability strategy that we anticipate will help us achieve
Bees acquisition marked our entry into the natural personal              cost savings over time in addition to helping us reduce our
care category. We are extremely pleased with this acquisition,           environmental footprint.
which contributed about 2 percentage points of the company's
                                                                         (See pages 14-17 to learn more about what we're doing in
sales growth for the fiscal year. We continued to drive the
                                                                         the area of environmental sustainability and other aspects of
integration and growth of the bleach businesses we acquired
                                                                         corporate social responsibility.)
last year in Canada and Latin America. Also in Latin America,
                                              
we extended our strong Poett and Mistoln home care
                                                                         Differentiation Is Our Competitive Advantage
brands into the adjacent air fresheners space, and continued
                                                                         We anticipate another challenging year in fiscal year 2009 as
to build our franchise of scented cleaning products.
                                                                         consumers remain under financial pressure and we anticipate
(See pages 5-13 to learn more about how we are capitalizing on           year-over-year raw-material cost increases. At the same time,
the consumer megatrends.)                                                we have a track record for effectively managing our business
                                                                         in a difficult environment, and I believe we're taking the right
Relentlessly drive out waste                                             steps to mitigate these factors, including increasing prices on
The unprecedented commodity cost increases we faced in                   more than half of our portfolio during fiscal 2009. I have
fiscal 2008 underscored the importance of driving cost savings           confidence we can implement these increases because we
and increasing productivity. Our strong cost savings for the             have brands that consumers trust to deliver value and that our
year are testament to the organization's cost and process                retail customers want. Across our portfolio, we're capitalizing
discipline. Key initiatives included making a strategic investment       on the key consumer megatrends of health and wellness,
to restructure and streamline our home care manufacturing                environmental sustainability, convenience and a more
network for improved flexibility and cost structure. Under the           multicultural marketplace. We're bringing innovation to our
restructuring, we are creating a home-care supply-chain hub              categories and leveraging strong customer capabilities that our
in the Atlanta area that will provide improved efficiency and            retail partners value.
cost savings from raw materials and packaging to manufacturing
and transportation.                                                      We Are Focused on Creating Long-Term Shareholder Value
                                                                         As the chart above shows, Clorox stock has delivered a 6.4
                                                                         percent annual increase in total shareholder return over the





past five years. As with many other companies, our stock            One thing I've learned during tough times is that challenges
performance over this period -- and in particular the past fiscal   can bring out the best in people -- and at Clorox we have some
year -- was impacted by the market's reaction to the volatile       of the best people around. Someone will win in this tough
commodity cost environment and overall inflationary pressures.      environment, and I believe it can -- and will -- be Clorox. We
In fiscal 2008, our total shareholder return decreased 14           have the right priorities, organizational focus, talent and passion
percent. That said, Clorox remains focused on making the right      to deliver results that our shareholders expect and deserve. On
decisions for the overall health of the business to create          behalf of everyone at Clorox, thank you for continuing to place
shareholder value over time. Our cash flow remains strong, and      your trust in our company.
we are continuing to pay down our level of debt, while also
supporting dividend growth. We're exercising disciplined capital    Sincerely,
spending and using our economic profit lens to determine
resource allocation.

We Will Continue on Our Path Toward True North
We believe more than ever that we're on the right path to
achieve our true north goal of delivering double-digit annual
percentage growth in economic profit. We remain focused on          Donald R. Knauss
the choices we've made to support our Centennial Strategy.          Chairman & Chief Executive Officer
Looking ahead, we are working to achieve high levels of             Sept. 1, 2008
engagement across the Clorox organization; gain market
share in our product categories by executing the 3Ds of
desire, decide and delight; accelerate growth with innovation
behind the four megatrends and drive out waste to improve
gross margin.
