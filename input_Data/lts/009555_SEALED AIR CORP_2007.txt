2007 was a year of continued steady performance as well as several
milestone achievements. We expanded our reach, grew net sales by 7% to
$4.7 billion in the year and achieved solid earnings growth. We accomplished this
while diligently executing our growth plans, continuing to maximize the effi ciency
of our cost structure and enhancing the strategic position of our international
footprint. We achieved a solid 3% volume growth rate for the full year, with growth
accelerating in the second half of the year despite challenging economic conditions
in North America in that period. We attribute our performance to the strength
of our solutions, our expanding reach into new applications and our growth into
developing regions.
During the year, the ongoing development of emerging economies created growth opportunities for us. In developing
regions1, our net sales grew 17% in the year to represent 15% of our consolidated revenue, a success attributed to the
strength of our brands, our product portfolio and longstanding presence within the regions. Rising local demand and
strong export markets contributed to our double digit sales growth rates in Asia-Pacifi c and Latin America, with the
Asia-Pacifi c region surpassing the $600 million sales milestone and Latin America achieving a $400 million sales milestone.
Additionally, we expanded our footprint in developing regions in the fourth quarter with the opening of our new fl agship
manufacturing facility outside of Shanghai, China. This new facility incorporates new meat packaging technologies that
pioneer packaging efforts in that country.
We are pleased that we delivered a record gross profi t of $1.3 billion in 2007 in spite of rising raw material and energy
prices. Although our raw material costs were relatively comparable to the prior year for the fi rst nine months, we incurred
an incremental $21 million in raw material costs in the fourth quarter due to higher input prices. To offset these costs, we
announced price increases across various product lines worldwide, which we expect will benefi t our business in 2008.
Our operational achievements include the successful launch of an e-commerce portal to improve our procurement
practices. We continued to increase the percentage of raw material scrap recaptured and re-used, which improved yields,
reduced raw material costs and reduced the amount of material we sent to landfi lls. Additionally, we achieved the
conversion of all of our largest manufacturing sites outside of North America onto a standard SAP information system
platform and we centralized our North American customer service operation.
We sustained a strong balance sheet during the year by decreasing working capital and generated substantial cash fl ow
from operations. In turn, we utilized our cash to provide internal funding for a variety of investments and acquisitions.
We also returned $71 million to our stockholders, primarily in the form of quarterly cash dividends, which we increased by
33% in February of 2007 and again by 20% this February, refl ecting our confi dence in the strength of both our business
and our cash fl ow.Business Review
In July, we expanded our reporting structure to better explain to investors our business
strategy and investments, which are focused on accelerating growth and expanding
reach.
Our new Food Packaging segment, focused primarily on bulk packaging of meats and
cheeses worldwide, experienced solid growth during the year with volume growth rates
above slaughter rates. Net sales increased 8.2% during the year, with steady strength
in rollstock materials and vacuum shrink bags and grew at double digit rates in Latin
America and the Asia-Pacifi c region. We launched a number of new products, including
Marinade-on-Demand packaging, our Cryovac Flood Printed bag, which is a metallic
foil laminate that enhances the display of meats at retail, an easy-open Grip & Tear
bag and the HangPak bag, a solution that expands the display formats of laminatebased
product packaging at retail. Most notably, the Food Packaging segment sold the
companys fi rst product made from an annually renewable material with the launch of
our NatureTRAY package.
Our new Food Solutions segment, a high-growth business focused primarily on food
packaging formats used by consumers and food service packaging used in commercial
kitchens, experienced accelerating sales over the course of the year with 12.1% net
sales growth including 5% volume growth, driven primarily in North America and
Europe. During the year, case-ready net sales increased 12%, with the portfolio now
over a $450 million product family. In 2007, case-ready benefi ted from increased sales
penetration and its introduction into China. We also launched two new formats, the
Cryovac Mirabella package and the Darfresh Bloom package, both of which
require less packaging than the original case-ready products. We continued our reach
into packaging for food service, with new applications in Simple Steps packaging as
well as with the new Cryovac FlavourMark retort and aseptic bulk pouch solution
that enables shelf stable food storage for up to 24 months. The ongoing adoption of the
entire vertical pouch packaging (VPP) portfolio generated double digit sales growth and
VPP net sales of over $125 million for the year.
The new Protective Packaging segment experienced a 2.1% net sales increase for the
year with a solid 9% volume growth rate in the Asia Pacifi c region, which was largely
offset by a decrease in unit volume in North America of 1.7%, refl ecting slowing
economic conditions in that region. We expanded the PackTiger paper-based cushioning
system to North America and sold over 300 systems worldwide. The Protective
Packaging segment maintained its operating margin of 13.8% during the year and
continued to focus on system enhancements and consumer-oriented features.
Our new Other category, a growth segment focused on specialty materials, medical
applications and new ventures, experienced a solid 18.1% net sales increase in
2007. This growth was largely attributed to the acquisition of Alga Plastics in our
medical applications business and the purchase of certain assets relating to The Dow
Chemical Companys Ethafoam and related polyethylene foam product lines in the
specialty materials business. Additionally, strong unit volume growth in our medical
applications business in Asia and Europe of 66% and 4%, respectively, contributed
to organic growth during the year. In new ventures, we achieved our goal of
commercializing a rigid packaging solution from a renewable material through our
Biosphere investment, which complements our NatureTray product. Looking ahead,
we will continue to extend our reach into new technologies and applications that allow
us to diversify our material mix to include other products made from renewable materials.
Expanding Our Reach
This years annual report is more than a summary of the past year. It also describes a
company passionate about what we do and what lies ahead.
In 2008, we will continue our journey and expand our reach, both geographically and
within the lives of consumers worldwide. We will continue to invest in the development
of solutions and applications that address these sustainable trends that are relevant
worldwide:
 rising standards of living in the growing middle class in developing regions
 increasing demand for dining convenience and prepared meals
 increasing global trade and e-commerce, which increases the movement of goods
 increasing sensitivity to resource conservation and energy effi ciency
 increasing investment in healthcare delivery to both growing and aging populations
Strategically, we will continue our focus on growth and the global reach of our business.
This includes our ongoing investment in the BRIC2 regions and the completion of our
global manufacturing strategy with the expansion of our new facility in Mexico, the
launch of a new facility in Poland and the completion of our centers of excellence.
Looking externally, we will continue our investments to pioneer material technologies,
broaden applications and accelerate product launches, leveraging internal innovation
and acquisitions. Our priorities will remain on partnering with customers worldwide to
provide solutions that meet their needs, exceed their expectations and increase their
productivity and profi tability.
Operationally, we will complete our global SAP information system rollout, with the fi nal
phase in North America at mid-year. Additionally, we will continue to optimize our
operations, reduce our costs, and expand initiatives focused on inventory reduction and
enhanced procurement practices, which should improve our working capital position.
I am proud to be a part of a company that is bringing innovative solutions to market that
make the world a better place. Today, we are developing technologies and solutions
that are making signifi cant improvements in the way people work and live around the
globe  such as fi nding ways to reduce the amount of energy needed to ship a product
or creating innovative packaging that extends the shelf-life of food.
I would like to thank all of our customers, my Sealed Air colleagues and our partners for
helping us achieve our goals through ongoing collaboration, dedication to excellence
and the drive for innovation. We know we have more work to do, but I am encouraged
by our accomplishments and optimistic about what lies ahead  knowing nothing is
beyond our reach.

William V. Hickey
President and Chief Executive Officer