Juniper Networks enjoyed a record setting 2000. We achieved and surpassed our goals---both as a company and as a community of people. 
Following is a progress report on our commitment to five principle growth objectives. 
Extend our strategic market leadership position in the largest network backbones 
in the world. 
We extended our core leadership position in the largest backbones in the world by furthering the technology lead of the M40 and M20, and intro 
ducing the M160, more interfaces, and new software and features unopposed by competition. The largest backbone operators in the world are using 
Juniper Networks routers to deploy their new network infrastructure. 
Diversify products and services globally. 
Juniper Networks has diversified its product line. We ended 1999 having just introduced our second product, the M20. We ended 2000 with a 
product portfolio of five, ranging from the M5 to the M160. Other milestones include deployment of over 75 interface types, delivering services from 
thousands of bits per second to billions of bits per second, and delivery of our 7th release of JUNOS software. Our feature set now extends from the core 
of the network to include the service rich requirements of the highspeed access marketplace. 
We continued to see worldwide demand from both new and existing customers, with international revenue representing 35% of total revenue for 
the full year of 2000. Our diverse market coverage enables us to do business in over 30 countries around the world with support from 48 resellers 
throughout Europe, Asia and Latin America. In addition, we penetrated a significant number of new accounts in North America through our direct sales 
force and channel partners. 
Increase the number of markets we serve, leveraging both our technology 
leadership and our distribution and support presence worldwide. 
Juniper Networks increased the number of its markets from one to three. In addition to our heritage focus on the core backbone market, Juniper 
Networks added significant products to a second market, the edge, with our access portfolio of the M5 and M10 routers and several new releases of 
JUNOS software. Also, we are establishing our presence in a third market, the emerging Mobile IP market, and look to our joint venture with Ericsson as 
a powerful vehicle in the realization of this objective. 
Deliver on our commitment to financial fundamentals. 
Juniper Networks increased revenue six fold from 1999 and solidified important areas of gross margin, earnings and cash flow. Our commitment 
to financial fundamentals will serve the company well as economic conditions vary over time. 
Expand our workforce dramatically while protecting the quality of our team. 
Finally, Juniper Networks tripled the size of its global organization through the course of 2000, adding extremely talented people to all aspects of 
the business. The accomplishment of these objectives shows our relentless focus and execution in 2000. However, they all combined to accomplish a 
larger and significant purpose, which is to position Juniper for 2001 and beyond.
Looking forward 
Any new company must reach a certain critical mass---a level of resources, products, customers and financial sustainability---that allows 
for the transition from a new to an established franchise. More than any individual measure, we accomplished in 2000 the opportunity to control our 
own destiny. 
This does not signify the removal of risk, or an accomplishment on which we can rest. In fact, it stimulates exactly the opposite reaction in the 
people of Juniper Networks---an excitement over what we have now made possible by our efforts, and a glimpse of the kind of company we can now 
create given our track record. 
Juniper Networks envisioned the IP Infrastructure market as both new and fundamentally different since creating the company in 1996. We can 
now see new steps required to lead this new market to its potential, for the Internet is now becoming a new multiservice public network carrying voice, 
video and data traffic on a common fabric---both enhancing and simultaneously economizing the network opportunity to connect people and places 
around the globe. 
Juniper Networks has two key priorities for 2001---to drive our innovation engine at full speed, furthering our product and technology lead in 
multiple markets; and continue delivering our commitment to financial fundamentals. 
Our investment in product development---a Juniper Networks hallmark since the founding of the company in 1996---will continue as we build 
on the competitive advantage we have worked so hard to establish in our market. This investment is necessary to reach the opportunities our success 
has created, and will be measured both by the continued addition of high quality talent, and the investment in the supporting infrastructure that will 
bring new products to market quickly. 
Our proven fiscal commitment to financial metrics also will be unwavering, for we measure our success through the delivery of growth and profits. 
It is this parallel achievement of investment, profitability and growth that will drive our focus on relentless execution in 2001. 
I would like to thank our employees, our customers, our suppliers and other business partners, and our investors for their continued confidence 
and commitment to Juniper Networks. 