dear fellow shareholders,

2017 capped Morgan Stanley�s journey through a multi-decade period of challenges
and recovery. By transforming our business mix and risk profile, and embracing the
culture and values on which the Firm was founded, we have positioned Morgan Stanley
to perform through market cycles. Today, we have the strategy, stability and scale to grow
the business by supporting our clients as they create economic activity, fuel innovation,
and secure economic futures for people around the world. Going forward, our continued
success in executing our strategy will be driven by ongoing investment in our people,
culture and technolog.

executing our medium-term strategic priorities
On an annual basis, the Board of Directors and executive management team evaluate our
strategy and refine our goals and priorities to ensure we are working for the long-term
benefit of our shareholders. We update and articulate this strategy at the beginning of each
year and report our progress against it so that our shareholders can hold us accountable.
Let me review our priorities that will guide us over the medium term.
Our financial performance in 2017 improved significantly over the previous year,
achieving the strategic objectives outlined over two years ago. With growth in each of our
business segments, Firmwide net revenues increased 10% and pre-tax profit rose 18%,
demonstrating the operating leverage built into our business model.
With a solid strategy in place, our management team will continue to work toward
generating sustained higher returns by investing for growth while maintaining fiscal 

vigilance and capital sufficiency. The Firm
has a mix of businesses that provides earnings
stability, with complementary franchises in
Institutional Securities, Wealth Management
and Investment Management. We are focused
on the execution opportunities ahead of us
in the medium term for organic growth and
further operational improvement.
Wealth Management will continue to benefit
from scale and secular trends such as the
shift to fee-based assets. We are managing
$2.4 trillion in client assets�a demonstration
of the trust placed in us. Our profit margin*
target for that business is 26% to 28% over
the next two years. We intend to deepen our
lending footprint across our Wealth Management and Institutional Securities franchises.
In Institutional Securities, there are opportunities to continue pressing our advantage in
traditional areas of strength like Investment Banking and Equity Sales and Trading, while
maintaining a credible and critically-sized Fixed Income Sales and Trading business. Our
clients continue to turn to us to execute their most complex transactions globally. Growing
Investment Management is a priority, leveraging our attractive business mix, global client
footprint and top-tier performing strategies. Additionally, our partnership with MUFG
continues to differentiate us by combining our global reach and expertise with MUFG�s
strong lending capabilities and client footprint in Japan.
Over the last two years we exceeded our expense efficiency ratio� target, and our goal is to
continue to drive savings across the businesses and maintain the expense efficiency ratio at
or below 73%.
We are sufficiently capitalized for our business mix, size and risk profile, and will continue
to return capital to shareholders, subject to regulatory approval. By combining disciplined
execution with an attractive capital return profile, we feel confident in our ability to deliver 

sustained and increasing shareholder returns. We expect to drive our return on equity �
to the 10% to 13% range and our return on tangible common equity �
 to the 11.5%
to 14.5% range in the medium term.
Beyond the execution of our strategy, our long-term success lies in making the appropriate
investments in our people and culture to serve our clients, and harnessing current and
emerging technologies to our competitive advantage.
people and culture
are key to our success
The driving force behind our future success will
be the depth of talent and leadership across our
Firm, as it is our employees who sustain our culture
and execute our strategy. We have a strong and
experienced management team and our businesses
have a deep bench of talent.
We are committed to investing in our employees�
skills and capabilities so that we can continue to
build strong, cohesive and diverse teams to drive fresh
ideas for our clients and support the strategic goals of
the Firm. It will take all of us executing together and
serving our clients to realize the growth potential of
the Firm over the coming decade.
At Morgan Stanley, our culture guides our employees,
and our values inform everything we do: we have
a commitment to putting clients first, leading with exceptional ideas, doing the right
thing and giving back. These values honor both our history and our aspirations for the
future. By living these values, we engender the trust and confidence of our clients�from

our wealth management clients who trust us to manage their investments and plan for
their financial future to our corporate clients who seek our advice when facing the most
complex financial transactions. The work we do for our clients not only builds long-term
value for our shareholders, but also creates long-term value for society. We play a critical
role in connecting the providers and users of capital to promote economic growth and
creativity. It takes capital to create lasting, positive
change�from breakthrough research to growing
companies to building retirement security. A strong
and cohesive culture that focuses on our clients and
evaluates and manages risk is critical to ensuring
we deliver for our shareholders, our clients and
our communities.
technology will continue
to reshape our business
Looking ahead, one of our biggest challenges
and a source of great opportunity will be the
technological advances that are disrupting, shaping
and transforming industries globally. The financial
industry is not immune to these changes and
technology will reshape it in important ways
over the coming decade.
This is not the first time that our industry has faced technological change. Through
waves of change, we have adapted and harnessed technology to improve productivity
and service levels and increase customer choice. With the growth of the internet in the
1990s, brokerages began offering stock trades over an electronic network and the online
brokerage was born. This enabled individual investors to directly trade securities without
a stockbroker. Electronic trading systems proliferated in the early 2000s using faster
systems and complex algorithms�and are ubiquitous today. 

Throughout this rapid evolution, we have integrated new technologies to strengthen our
businesses. For example, as the equities market evolved over time, we built a platform
that offers global electronic trading tools including a broad suite of algorithms, smart
order routing and direct market access. By combining a traditional high-touch model
involving human interaction with a then evolving low-touch electronic model that is now
market leading, we were able to offer our clients unparalleled execution. And today our
world-class electronic trading platform is an important ingredient in our No.1 position
in Equities globally. Across our businesses, a key factor in our current success is that we
continuously incorporate new technology to improve service and execution, and to protect
our clients.
Today there are rapid changes in areas ranging from cybersecurity to digital and
mobile to artificial intelligence, and from emerging areas like blockchain technologies.
Technological advances will fundamentally alter the cost structure of our industry
if adopted and used effectively.
Cybersecurity is one of the foremost challenges facing the financial services industry. At
Morgan Stanley, we have strengthened and continue to strengthen our systems to protect
our data and our client assets. In addition to investing in our own protections, we are
working with the rest of the industry and government
bodies to continually upgrade defenses. Over the
past few years our investments in cyber technologies
have grown multifold, significantly ramping up our
defenses in this area.
In our Wealth Management business, we are enhancing
the client experience using state-of-the-art digital and
mobile technologies. We are also investing to make
our Financial Advisors more effective and efficient
so they can spend more of their time advising clients
and less time processing documents or client journals. In effect, the traditional branch
system and operations is in the process of being digitized. This will reap huge benefits in
the coming years both through cost and efficiency gains and by arming our Advisors with
tools to meet the demands and expectations of a new generation of investors.

Over the medium term, artificial intelligence and machine learning have the potential
to touch every part of our business, including in ways none of us have yet imagined.
Already, we see it incorporated in a range of applications�from investment algorithms
to virtual assistants, from delivering personalized insights to detecting and preventing
fraud, from organizing and analyzing reams of big data to driving better decision-making.
For Morgan Stanley, artificial intelligence and machine learning are opportunities that
we will continue to leverage across the Firm.
Over the next decade, these and other technological changes will have enormous
implications for our industry. Long-term winners will be those able to adapt and use
technology to their competitive benefit, and we are deeply focused on these efforts.
looking ahead
Morgan Stanley is strong today and positioned to do well going forward. The external
environment is also conducive to growth. The global economy has strengthened over the
last year, and we are witnessing a period of broad synchronized global growth. In the U.S.,
the enactment of significant corporate tax reform will boost economic activity, a key driver
for financial services. Rising interest rates in the U.S., to the extent that they are driven by
a strong economy, provide a positive backdrop for the industry. And while there has not 

yet been significant revision in the financial services regulatory framework, we expect
to see common-sense recalibrations over the next few years. Though geopolitical risks
remain, we are optimistic about growth over the long term.
I have great confidence in our senior leaders, and we are excited about the future.
And I am proud of our nearly 58,000 employees around the globe who deliver for
our clients each and every day.
Thank you for your continued confidence and investment in Morgan Stanley. I am
optimistic about our future and confident in our ability to deliver lasting value to
our shareholders.
james p. gorman
chairman and chief executive officer
april 6, 2018