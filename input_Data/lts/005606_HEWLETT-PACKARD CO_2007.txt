CEO Letter
Dear Fellow Stockholders:
If one word can best describe HPs
performance in fiscal 2007, it is growth.
For the year, we added more than
$12 billion of new revenue, grew
non-GAAP operating profit dollars
30 percent and returned more than
$12 billion to stockholders through
share repurchases and dividends.
We grew revenues across all of our
business segments and in each of our
regions. Overall, it was an impressive
performance by HPs employees and
partners around the world.

Many factors contributed to that growth, but three
stand out because of the major impact we expect them to
have in the years ahead: the explosion of digital informa
tion and content; the growing need for technology that
enables people to create, store, share and print that
content; and the rapidly growing demand for information
technology (IT) in emerging markets around the world.
The volume of data today is growing faster than our
ability to capture and use it. According to Forrester
Research, the worlds data doubles approximately every
three years , which is expected to lead to a more than
sixfold increase in data between 2003 and 2010.
Feeding the worldwide demand for information and
rich digital content is the enormous expansion of search
engines, blogs, social networks, e-mail, text messages
and online video and images.
To complicate matters, this content often lacks authentication
and proper security, and it is increasingly global
and mobile. Potentially hundreds of millions of new
users in emerging markets are coming onlinefrom
small business owners in China to farmers in Brazil to
consumers in Eastern Europe.
At the same time, the expectations of consumers have
changed. People want instantaneous access to content.
They expect this accessibility regardless of what kind
of device they are using or where they happen to be.
And their tolerance for complexity is low.
-
Opportunities
Dealing with all this dataand rising consumer expectations
is a huge and disruptive challenge for our
customers and their IT environments and a major opportu
nity for HP. It puts pressure on businesses to rebuild,
retool and deploy a flexible infrastructure so they can get
the right information to the right people at the right time.
-
Customers will need systems, software and services to
create, store and analyze content; PCs and handheld
devices to access and share it; and monitors, TVs and
printers to view and print it. Many companies can do
some of these things, but we believe that HP is the only
company with the portfolio and partnerships to do
them all.
It adds up to an enormous addressable market for
HPa $1.2 trillion global market .  Today HP has about
9 percent of this market, so we have a lot of room to
grow. We are working hard to expand our market
coverage and to profitably increase our market share.
To capitalize on these opportunities, we continue to
align the company around an operating framework
with three key elements: targeted growth, efficiency
and capital strategy.
Targeted Growth
HPs businesses are targeting the most promising,
high-growth markets and positioning themselves to
take advantage of the explosion of digital content.
In our Technology Solutions Group, we are helping
customers manage and transform their IT environments.
We had a particularly strong performance in blade
servers during fiscal 2007. Our innovative c-Class
blade systems helped drive year-over-year blade
revenue growth of 67 percent, and we increased our
industry-leading market share.
In our Personal Systems Group, we continue to benefit
as demand shifts toward mobility, consumers and
emerging markets. For example, our sales of notebook
computers increased 47 percent during fiscal 2007.
And our PC business in China nearly doubled, making
it our third largest market for personal systems.

In our Imaging and Printing Group, we are extending
our leadership in our core printing business while also
taking advantage of new, high-growth opportunities
in supplies, graphic arts and enterprise printing. We
are expanding our sales coverage to accelerate growth
in the enterprise business, which we expect to be a
$121 billion market by 2010.
In each of these businesses, we are using our scale to
become even more competitive. We have more than
double the scale of each of our printer competitors
and all but one of our PC and server competitors. We
are leveraging this scale to consolidate share and
improve profits while continuing to reinvest to maintain
our technology leadership.
We expect continued growth in developed countries
but much higher growth in what are commonly
known as the BRIC countriesBrazil, Russia, India
and Chinaand in other emerging economies. Our
revenue in the BRIC countries grew 33 percent in
fiscal 2007 and now represents 8 percent of HPs
total revenue.
As we reduce costs, we are shifting our investments to
capitalize on these market opportunities. While it
is important that we grow, its also important that we
grow the right way with the right mix. For example,
HP hired 1,000 sales professionals in fiscal 2007 to
expand our coverage in key accounts and markets,
and the company added more than 1,000 salespeople
through acquisitions.
We also are investing in higher-margin categories such
as software and services. We spent more than $6 billion
across our businesses in fiscal 2007 to acquire 10
software, technology and service companies. We
expect that each one will add significant capabilities
and technology to our portfolio, as well as new opportunities
for growth.
Between acquisitions and organic growth, revenue in
our software business nearly doubled in fiscal 2007
making HP the sixth largest software company in the
world. Software is a critical differentiator for HPnot
only in our standalone software business but in each
of our businesses as well.
Efficiency
We have made great progress in reducing costs,
which allows us to compete more aggressively and win
in the market. But we have an opportunity to further
improve our efficiency.
We look at everything between revenue and non-GAAP
operating profit as a cost. From that perspective, HP
had almost $95 billion in costs in fiscal 2007. That is
too much. Even with all the work we have done, we
must become more efficient, further reduce costs and
invest the savings to create more stockholder value.
We assess our costs in three areas.
The first is corporate overhead, including IT, real estate
and other corporate expenses. We are reducing our IT
costs by consolidating and modernizing our data
centers and operations. We also are becoming more
efficient in our use of real estate, which is a significant
expense for a company of HPs size. During the next
two years, we plan to reduce our number of sites
worldwide by almost 25 percent.
Second are our product costs. HP is the largest customer
for most of our suppliers, and we are continually
working with them to make sure we get the best terms,
including the best price.
Third are the costs owned by each of HPs businesses.
The businesses have detailed plans to reduce their costs,
and we have launched new efforts to identify more savings.

We continue to align the company
around an operating framework with
three key elements: targeted growth,
efficiency and capital strategy.

Capital Strategy
We align our capital strategy with our operations to
maximize stockholder value.
The first priority is to continue to invest in our businesses.
We do that in two ways. One is to invest to grow, which
shows up in research and development, demand
generation and mergers and acquisitions. The other is
to invest to save. We have made significant investments
in our IT and real estate infrastructure so we can reduce
costs in the future.
Our second priority is to return cash to stockholders,
predominantly with share repurchases and to a lesser
extent with dividends. Last year HP returned more than
$12 billion to stockholders. We will continue to balance
repurchase and dividend levels with other capital
allocation priorities.
Global Citizenship
As HP has grown to become one of the worlds largest
IT companies, our commitment to global citizenship
has remained integral to our business strategy. It is an
important part of our heritage and the values engrained
in HP by Bill Hewlett and Dave Packard. Today, our
citizenship efforts are built on a foundation of strong
corporate accountability and governance, a commitment
to environmental responsibility and active investment
and involvement in the communities in which we
do business.
We are focused primarily on three global citizenship
priorities: climate and energy, product reuse and
recycling, and supply chain responsibility.
HP is committed to reducing our own environmental
impact as well as that of our customers, partners and
suppliers. We launched our Design for Environment
program in 1992, and we have been investing in
energy-efficiency programs for more than a decade.
Innovations such as our Dynamic Smart Cooling and
Thermal Logic technologies offer advanced, energyefficient
solutions for customers. We expect these
technologies to contribute to a 60 percent reduction in
energy consumption in our new data centers. We also
are expanding our use of renewable energy, including
solar power for our San Diego facility and wind power
for several of our facilities in Ireland.
We believe we have reached a tipping point where the
price and performance of IT are no longer compromised
by being green; they are enhanced by it. Our goal
is to reduce the combined energy consumption of our
operations and products by 20 percent in 2010
compared to 2005 levels.
During 2007, we met our goal of recycling 1 billion
pounds of electronic productsincluding 500 million
pounds between 2004 and 2007. We plan to recycle
another 1 billion pounds of electronic products and
print cartridges by the end of 2010.
At the same time, we are working to ensure that our
suppliers and vendors meet HPs high standards for
social and environmental responsibility. HP was the first
IT company to formally launch a supplier code of
conduct. We continually work with our suppliers and
with companies inside and outside the technology
industry to raise standards for labor and human rights,
health and safety, environmental responsibility and
ethics. We do extensive onsite auditing to engage our
suppliers, identify performance gaps and help them to
build their capabilities to meet our expectations.
We believe that HP has an unparalleled ability to drive
simplicity, innovate and influence industry actions in a
way that is good for customers, good for business and
good for the planet.

We believe that HP has an unparalleled
ability to drive simplicity, innovate
and influence industry actions in a way
that is good for customers, good for
business and good for the planet.

Conclusion
We made solid progress in 2007demonstrating that
HP can cut costs and increase revenue at the same
time. However, we are not complacent about our
success, nor do we underestimate the strength of our
competitors.
We see significant opportunities to take advantage of
the explosion of digital content and address our $1.2
trillion market opportunity. We believe that we have a
portfolio and a set of capabilities unmatched in the
industry. We are investing in the right people and
infrastructure, and we are continuing to leverage our
scale and reduce our costs to improve our operating
performance.
Our goal is simple: to make HP the partner of choice for
our customers, the investment of choice for our stockholders
and the employer of choice for our employees.
Thank you for your investment in HP.
Sincerely,
Mark V. Hurd
Chairman, Chief Executive Officer and President
