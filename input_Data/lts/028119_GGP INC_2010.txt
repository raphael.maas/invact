DEAR SHAREHOLDERS:
I am honored to be the CEO of General Growth Properties. With hard work,
perseverance and a little luck, we believe we can turn GGP into one of the great
retail landlords in the U.S. It is time to roll up our sleeves to transform and
rework our assets.
GGP's portfolio of 180 malls includes some of the most outstanding in this
country, such as Ala Moana, Fashion Show, Tysons Galleria, Natick Collection,
Park Meadows and Water Tower Place. The mall portfolio consists of 169 million
square feet; 71 million square feet is in-line space. The malls are well located in
their respective markets. We own 25 of the top 100 malls in the country and
125 of the top 600; evidence of the quality of the assets.
In November 2010, GGP was recapitalized and had an IPO-debut. We raised
$6.8 billion of capital from Brookfi eld Asset Management, Pershing Square
Capital Management, The Blackstone Group, Fairholme Capital Management
and Teachers Retirement System of Texas. In addition, $2.3 billion was raised
in a public offering to replace part of the sponsors capital. The offering was
4x oversubscribed, and constituted the largest equity offering in U.S. REIT
history. This is a testament to the inherent value of the companys assets. As a
result, we will steadfastly guard our balance sheet and the franchise to squarely
focus on the task at hand: driving revenue and creating value for our shareholders.The year ended with Core Net Operating Income of $2.2 billion, effectively fl at
year-over-year. The fourth quarter was stronger than last year, so the trend is
positive. Occupancy at year-end was 92.9%, of which 86.0% is permanent.
The rental spread on permanent occupancy was 2.9%. Each 100 basis points
of permanent occupancy drives $25 million of increased operating income; each
100 basis points of margin improvement on our 14.2% of average occupancy
costs adds $160 million.
We are working to create a streamlined organization with best-in-class assets and
a focus on capital management, operational effi ciency, and the most conducive
environment for our tenants.
THE ASSETS
The last number of years have shown us that retail assets are not immune during
turbulent economic times; however, they do admirably well during these down
cycles. GGP owns a disproportionate number of high performing assets; the top
125 in our portfolio account for 87% of Core Net Operating Income.
GGP's portfolio includes 234 properties comprised of 180 malls (including
11 Special Consideration Properties), 28 stand-alone strip shopping centers,
and 26 stand-alone offi ce buildings, as of year-end.
We believe quality is better than quantity. The United States is over-retailed;
however, quality retail real estate is under-represented. This is where we stand to
make our most gains.
In addition, we have a foot-hold in Brazil through our investment in Aliansce
and Shopping Leblon. Investing since 2004, GGPs $126 million of equity was
worth $490 million as of year-end, equating to a 35% IRR.
THE RIGHT SIDE OF THE BALANCE SHEET
As a result of the recapitalization, our debt was reduced by approximately 30%,
from $27.8 billion to $20.6 billion, a start to the deleveraging. Our maturity
ladder is fl atter and more manageable. While virtually all of our loans were due
before 2013, prior to our reorganization, our maturity schedule is now balanced.A substantial amount of the debt is at the asset level. A unique feature in many
of the mortgages, put in place during the reorganization, allows us to refi nance
without payment of make-whole costs. This structure gives us enormous
fi nancial fl exibility.
We have embarked upon a signifi cant mortgage refi nancing plan. An environment
of low interest rates allows us to achieve more favorable terms than we had
imagined before.
The capital markets have returned and favor the characteristics of our portfolio
of assets  strategically located, irreplaceable and institutional quality with strong
cash fl ows. Two years ago, with asset values at a low point, the few lenders in the
market would only offer fi nancing at very conservative leverage levels with 8%
interest rates for the best assets. Today, valuations have risen and we are seeing
much better fi nancing levels with sub 6% interest rates, and a return of a healthy
CMBS market, life companies, banks and bond market.Of our $18.4 billion of mortgages (at share), approximately $9.8 billion are
open for pre-payment at par. We aim to refi nance approximately $5 billion in
2011, and extend our overall maturity schedule. Since November 9, 2010, we
have refi nanced two properties for $315 million, are in the process of closing
a $185 million loan, and are in the market with an additional six properties
totaling approximately $1.5 billion. In addition, we increased our revolver from
$300 million to $720 million, with an accordion feature permitting an increase
up to $1 billion.
Our focus is on execution.
OPERATIONAL EXCELLENCE
We will initially look inward to grow our income. We manage operations asset by
asset. There is no one size fi ts all approach.
As one of the largest landlords in the country we are in a position to understand
the market and take notice of the subtle shifts. Our portfolio comprises 16,500
permanent tenants, 3,500 of them unique retailers. We executed 2,400 leases in
2010, totaling 7.3 million square feet and we will look to continue this pace 
Lease, Lease, Lease!
Development is an important component of internal growth. Now is the time
to mine our existing portfolio. We will remain competitive and will continue to
invest in our properties. This capital investment should yield double digit returns
on equity as it is an incremental investment in a current retail center (add-on to a
mall; re-doing an old anchor) and we estimate that over the next fi ve years, there
is $1.5 - $2.0 billion of discretionary opportunities that may be available to us.
We are focused on the details of our assets in order to make all of them the most
signifi cant and secure in their markets. ACQUISITIONS AND DISPOSITIONS
Since November 9, 2010, we have reduced our asset base by 4.5 million square
feet, comprised of Arizona Center, nine strip/power centers and three Special
Consideration Properties, totaling $298.6 million of gross proceeds and
reducing our debt by $213.2 million. This activity has provided net proceeds
of $224.6 million.
Our goal is to have a portfolio comprised of approximately 150 assets, principally
malls. Completing the disposition plan will enable us to generate proceeds of
approximately $2.0 billion and pay down approximately $1.6 billion of debt with
surplus proceeds used to repay corporate and asset level fi nancings as well as fund
ongoing growth of the company.
Our focus will be selective and discerning on strategic acquisitions with an eye
toward enhancing the quality of our portfolio.
TENANTS
Retail real estate is a consumer-driven asset class. Our job is to create a shopping
experience for our customers to leave their computers and venture out to our malls.
The consumer is at the forefront in the economy and is at the forefront of our
minds as we constantly seek to improve our malls. It is a symbiotic relationship.
We want the most productive and exciting tenants in our malls and so do shoppers.
Tenants create the mall, they are our clients, and we are doing everything to
provide a collection of retailers and a physical aesthetic for them to excel.
MARKET FUNDAMENTALS
Markets are improving and retail sales are up. Comparable tenant sales in
our mall portfolio were $446 per square foot as of year-end 2010, a 6.4%
year-over-year gain. Having witnessed promising sales increases sequentially
every month since hitting a low point in December 2009, we are encouraged
to expect continued increases into 2011. STOCK PERFORMANCE
We will manage the business for long term value creation. Although we are unable
to directly infl uence the stock price, we can certainly strive to produce higher cash
fl ows per share while simultaneously focusing on strengthening our balance sheet.
PEOPLE
I have assured my GGP colleagues that stabilization of the organization is an
important part of my 100 Day Agenda. I have assembled new members of my
senior management team led by professionals in leasing, Alan Barocas; anchor
relationships and development, Richard Pesin; and asset management, Michael
McNaughton. Their wealth of experience unequivocally lends them to being
leaders in their fi elds. They are all highly regarded in the industry.
We are building a culture of meritocracy, with colleagues rewarded for their
attention to establishing each of our shopping centers as the best in the market.
CONCLUSION
The fi nest talent and assets coupled with a deleveraged balance sheet creates a retail
landlord that can sustain a downturn. Furthermore, it enables us to capitalize on
market opportunities. A new start and a renewed focus on being the leading retail
real estate operator in the country can turn this vision into reality.
I would like to thank all my colleagues at GGP for having faith in the company,
performing in times of uncertainty, and you the shareholders for believing in us.
Sandeep Mathrani
Chief Executive Officer