Despite a difficult sales environment in 2002, we made substantial progress during the year. Sales
of our new products improved sharply, confirming our increased competitiveness, and we saw
sales gains in several markets that are emerging as growth drivers for Altera. Most importantly,
enthusiastic market reaction to the three new device families we launched this year demonstrates
the attractiveness of the newest members of our product portfolio.These advanced device families
significantly extend our technology leadership and expand our application range, further adding
to our future growth potential.
Following a sharp decline in revenues during the prior year, quarterly sequential growth resumed
in the first quarter of 2002. However, quarterly sequential growth during 2002 was not sufficient
to offset the steep 2001 declines. Altera revenues for 2002 were $712 million, down 15 percent from
2001. Net income was $91.3 million, $0.23 per diluted share. Gross margin was 63.0 percent of
revenue. Operating income was 13.7 percent of revenue, below our long-term target, but a solid
performance considering the years sales challenges. Our balance sheet remains debt-free with
$943 million in cash and short-term investments.
New product sales increased dramatically during the year. Beginning in early 2001, we saw
increased design win momentum as we introduced our Quartus II development software and
a series of innovative, first-to-market, programmable logic devices (PLDs). Revenue from those
successes plus our earlier design wins drove new product sales growth throughout 2002. In the
fourth quarter of 2002, new product revenues more than doubled compared to the same period
in 2001, and represented 33 percent of sales.We expect to see continued strong new product growth
in 2003, as the products we introduced this year begin to ramp and add to the momentum we
have already created.
Our business mix is evolving as we pursue the full market potential for PLDs.While the communications
market has been a key growth driver, current market conditions will constrain its near-term
potential. At the same time, our other markets offer outstanding penetration and growth opportunities.
These changing trends can be seen in our 2002 results. Our consumer as well as industrial and
automotive markets reported the best sales progress, up 36 percent and 22 percent respectively in
the fourth quarter of 2002 compared to the same period in the prior year, and the computer
and storage market gained 13 percent. In comparison, conditions were more muted in
the communications market with a decline of 1 percent in the fourth quarter versus
the same period in the prior year.
New state-of-the-art programmable logic products are the fundamental drivers
for our growth.We announced three new device families in 2002all
equipped with features and performance that are unique to Altera,
expanding the reach of programmable devices and advancing our
competitiveness as a PLD supplier. Benefiting from a new approach
to product development, these products were developed in less time but with much greater customer
input than any previous Altera family.We were able to identify the crucial performance thresholds
and essential features that customers wanted, resulting in rapid adoption across our customer base.
In February we announced the Stratix device family, a new general-purpose FPGA family.The
first Stratix device shipped in May, ahead of schedule, and the manufacturing roll-out has been
both rapid and smooth.The family has an entirely new and more efficient architecture, providing
industry-leading performance in much more efficient and cost-effective devices, with features not
available in competing FPGAs. By the end of 2002, Altera had shipped six members of the Stratix
family to more than 280 customers around the world.
The Cyclone device family, announced in September, began shipping in December, also ahead
of schedule. The Cyclone family offers the industrys first FPGAs designed from the ground up to
specifically target low-cost, high-volume applications. The Cyclone family was developed with
input from hundreds of customers to deliver capabilities at price points that make the Cyclone
family very attractive for higher volume platforms previously beyond the reach of an FPGA solution.
Because the Cyclone architecture is optimized for these specific customer needs, it delivers typical
Altera gross margins while opening the door to a dramatically wider range of applications across
all of our markets.
The Stratix GX family, announced in November, builds on the expertise Altera gained from the
Mercury device family. Mercury devices were the first PLDs with an embedded high-speed transceiver.
The Stratix GX family offers even faster data rates coupled with the performance advantages
of the Stratix architecture. The data integrity, speed, and flexibility delivered by Alteras proprietary
transceiver has made this device a success with customers, who recognize Alteras proven expertise
gained through the pioneering Mercury family. Customers began taking delivery of the first
Stratix GX devices in January 2003, and we have already recorded our first design wins.
In 2003 and beyond, these products will add to the impressive growth we
are already seeing in our new product category. Our R&D is sharply
focused to be first to market with advanced PLD technology
that will differentiate us from the competition and give our
customers compelling reasons to use more Altera PLDs
in their designs.We are seeing the benefit of this
focus as we were able to leverage our operations
and execution strengths to quickly turn our
development promises into production reality,
delivering a variety of new products on or
ahead of schedule.
Over the next few years, ongoing changes in semiconductor economics offer an excellent growth
environment for Altera.We often compete against application-specific integrated circuit (ASIC)
and application-specific standard product (ASSP) vendors. Sharply increasing up-front development
costs are making ASIC solutions prohibitively expensive for more and more applications and are
forcing ASSP companies to narrow their product offerings. Simultaneously, these same technology
advances are steadily lowering the relative price of the competing PLD alternative. As a result of
these factors, we estimate that an additional $2+ billion market is opening up for us, dramatically
increasing the growth opportunity for PLDs. Programmable logic technology offers the unique
advantage of standard devices suitable for use across a very large range of applications, creating a large
revenue potential and allowing Altera to make further inroads against other semiconductor technologies.
The year was one of great accomplishment. As we entered the year, we had an extremely ambitious
product roll-out schedule.We delivered on or ahead of schedule through all of 2002. Our solid
execution was a credit to the diligence of our worldwide workforce who remained focused during
a challenging and critical year. My thanks go to them.
The success of our new product offerings this year derives largely from the insights gained from
intense dialogue with our customers. Our customers remain a central focus for Altera.We will
grow by understanding their needs and driving our technology to deliver greater benefit to them.
We have been focused on making this interaction even broader and deeper. This years new product
strengths speak to the value of this process. Our research and development pace has remained high
and we will see more Altera innovation throughout 2003 as we continue to raise the bar competitively.
Our aim is to use these strengths to produce technology-driven growth and increasing shareholder
value. Even with the challenges of the past two years, we have aggressively built on nearly two
decades of innovation with important new products and capabilities. The years accomplishments
have both sustained and fortified our growth potential, and give us the confidence that we will
continue to grow in value to our customers and shareholders.