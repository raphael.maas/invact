My fellow shareowners, 
We focused on building a bright future for our customers, shareowners and our company in 2014. 
The transformation of our generation fleet continues toward cleaner, more efficient energy sources. We are helping our communities grow by making natural gas available to new customers, and we are recruiting new businesses to our service territories.
Our dedicated and talented employees achieved our goals for safety, reliability, customer service and financial performance. We are proactively building the expertise and talent needed to address increasing retirements in our workforce.The result was another outstanding year. This wouldnt happen without the men and women who make Alliant Energy shine and the ongoing support of our shareowners.
Delivering on our plan
Alliant Energy achieved numerous key milestones in 2014.
The company celebrated the successful completion of two major environmental retrofit projects, one in Wisconsin and one in Iowa, that reduced sulfur dioxide and mercury emissions by 90%. Construction is underway at our new natural  gas-fired Marshalltown, Iowa, generating station.
We announced our plan to construct a new natural gas-fired generating facility in southern Wisconsin called the Riverside Energy Center Expansion.
In partnership with the Indian Creek Nature Center in Cedar Rapids, Iowa, we are creating a self-sustaining solar project that we will use to study and plan future renewable expansion.
Our company is positioned to meet new and existing state and federal environmental targets. To help maintain competitive costs for our customers in Wisconsin and Iowa, we have retail electric base rate freezes in place through the end of 2016.
Growing our communities
When our communities thrive and grow, so do we. Our gas business expansion includes a 13-mile pipeline to serve several large industrial customers in Ames, Iowa. In Wisconsin, we are expanding natural gas service to the Oakdale area to stimulate economic development. These projects will provide benefits for years to come.
Our community involvement is central to making our customers lives better. In 2014, Alliant Energy and its Foundation invested close to $3 million in creative partnerships.
An example is our partnership with Beloit College in Wisconsin to repurpose Blackhawk Station, a retired generating facility. The building will serve as a campus hub for students for years to come.
The reason we do this is simple. Strong communities mean a vibrant future for the region  and for our company.
Creative problem solving
Our employees seek out solutions for our company and our customers and are applying the best sustainable practices every day.
You can see it in the ways we help customers put energy efficiency to work. You can see it in the new technology that allows us to detect system problems faster and dispatch crews more efficiently.
You will see it in the way we are transforming our customer service with technology to provide additional online options for customers and streamline the customer experience.
Placing a premium on creative thinking is paying off for our customers and our company.
Creating value
You, and the financial community as a whole,  see the excellent value we provide.
The market value of our company grew surpassing $7 billion for the first time  in 2014. Alliant Energy stock was traded at an all-time high of $69.78 during 2014. And our share price ended the year 29% higher than the close of 2013. 
We have increased our common dividend again. Thats 277 consecutive quarters we have paid a dividend.
When combining our strong stock performance and dividend payout over the last five years, we delivered a higher return than you would have earned by investing in the S&P 500 Index or the Edison Electric Institute Stock Index.
We have high expectations for our companys future. One thing you can always count on: Alliant Energy will operate responsibly, embrace environmental stewardship and continue to earn the trust you have placed in us. Everyone at Alliant Energy thanks you for your support and commitment to our company and our customers.
Sincerely,
Patricia Leonard Kampling
Chairman, President and CEO
March 5, 2015

