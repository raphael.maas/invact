Walgreens reported its 34th consecutive year of record
sales and earnings in 2008, a track record matched by only
one other Fortune 500 company. During the year, we made
strategic investments to broaden our health and wellness
offerings, while outperforming the retail drugstore industry in
both pharmacy and front-end sales. We have a strong balance
sheet, solid credit rating and financial flexibility, and have
taken prudent steps to ensure our continued success in the
face of a weakening economy.
Our strategy is to transform Walgreens into a more efficient
and customer-focused company, both for drugstore customers
and for patients and payors seeking quality pharmacy, health
and wellness services that are accessible and affordable. Our
focus is to improve both top and bottom line performance
and value creation. Change is never easybut it is absolutely
imperative, and Walgreens is committed to becoming a more
nimble and profitable company.
Following are the key milestones accomplished in fiscal 2008.
We focused on serving our customers better
In this economy, consumers are looking for low-cost alternatives
and the best overall value. We offer patients a way to stretch
their dollars and maintain their prescriptions in one place
without sacrificing the safety, service or convenience of their
nearby neighborhood drugstore.
One example of this customer-centered philosophy has been
the successful introduction of our Prescription Savings Club,
which now has more than one million members and goes well
beyond the discount generic programs offered at other retailers.
Members receive savings on more than 5,000 name brand and
generic medications, including 400 generics that are available
for less than a dollar a week. They also receive a 10 percent
rebate on all Walgreens branded products that can be used
for future store purchases.
Were also working to improve our customers experience.
Over the past few years, weve seen the number of items carried
in our stores expand from approximately 18,000 to more than
25,000  a breadth that can lead to complexity and confusion
for customers. We intend to streamline our merchandise
selection in fiscal 2009 to improve the overall Walgreens
store experience and increase sales. As the architect Mies van
der Rohe said, Less is more.
Meanwhile, we have expanded our private brand product
offerings, which provide value to customers and higher profit
margins to our stores. We continue to offer competitively
priced consumables, whose sales have been very strong,
particularly for fast, easy, midweek fill-in needs in our
conveniently located retail stores.
We contained costs and are launching
new cost initiatives
During the year, we launched an aggressive  and successful 
cost control effort that delivered solid bottom line results.
Were very proud of Walgreen people across our organization
for their perseverance in this initiative. Our store managers
have proven their ability to quickly adjust to evolving conditions
while maintaining a strong focus on customer service.
Over the course of fiscal 2008, we reduced the rate of growth of
expenses by six percentage points, even with the costs associated
with opening 561 net new drugstores this year, versus 468 last
year. This was an outstanding result. And we see plenty of
opportunity for even greater efficiencies.
We are currently conducting a company-wide initiative to align
our costs, culture and capabilities to our strategy and to the
realities of the current economic environment. As this report
went to press, we announced plans that target approximately
$1 billion in annual cost reductions by 2011 through more
efficient processes, strategic sourcing and plans to reduce
corporate overhead and work throughout our stores.
Our pharmacies are working to fill prescriptions more efficiently.
Were in the early stages of rolling out a new initiative to transform
 and advance  the practice of community pharmacy.
Currently, weve focused on 300 Florida stores where were
moving tasks such as phone calls, data entry and insurance
verification from individual stores to more efficient central
processing facilities.
These centers will also fill approximately one-third of
prescriptions for delivery to the store when patients request
next-day pickup. Were creating a hybrid approach that
blends the best of community pharmacy practices and central
fulfillment. These efforts will not only reduce our cost to fill
prescriptions, but will give our pharmacists more time to offer
expanded counseling services that improve patient compliance
and help people better manage chronic conditions such as
diabetes and hypertension. In many states, the government
already pays for these additional, valuable services.
We adjusted our organic store growth
Walgreens remains among the fastest growing retailers in the
country, even with our planned slowdown of organic growth
from the current 9 percent rate to 5 percent by 2011. Slower
growth offers two big positives: first, more time to develop
our management ranks and focus on improving the customer
experience; and second, greater flexibility to invest in
opportunities that will strengthen our core base and deliver
attractive returns to our shareholders.
While organic growth was once our primary growth strategy,
today it anchors a much broader strategy, which you will
find discussed throughout these pages.
Were growing our health clinics and specialty
pharmacy business to complement our drugstores
We now operate more than 600 health and wellness clinics in
our stores and on employer worksites. Since acquiring two
worksite health center companies last spring, weve integrated
these services into our new Health & Wellness division,
which plans to expand to 800 sites by the end of fiscal 2009.
These facilities will position us as one of the nations foremost
providers of health and wellness services. They are highly
complementary to our retail pharmacies and increase awareness
of our Walgreen brand.
The satisfaction of both patients and nurse practitioners in our
Take Care in-store clinics is very high. The clinics are bringing
new people into the health care system by offering high quality,
accessible and affordable solutions. A study by the nonprofit
Rand Corp. found retail health clinics are attracting patients who
are not routine users of the current health care system, and do
not have a primary care physician. Take Cares nurse practitioners
regularly refer patients who need more in-depth care
to physicians, keeping them out of high-cost emergency rooms.
Our Health & Wellness division is also the largest operator of
worksite health centers and pharmacies in the country. At fiscal
year-end, we had 364 clinics on or near employer campuses,
offering diverse services including primary and acute care;
wellness, pharmacy and disease management services; and
health and fitness programming. Employers recognize the
significant value and cost savings these centers can provide.
Specialty pharmacy is the fastest growing sector of pharmacy,
expanding at about 15 percent a year. It is also very
complementary to our core retail pharmacies. Of the new
products awaiting FDA approval in 2008, 80 percent are
specialty drugs, according to IMS Health. Weve built our
specialty business primarily through acquisitions, the largest
of which was our 2007 purchase of OptionCare, which was
accretive to earnings in its first year of operation.
Our discussions with current and potential clients indicate
that our independence from a major PBM (pharmacy benefits
manager) is a significant factor in winning business, such as
the specialty pharmacy contract we signed earlier this year
with Prime Therapeutics, a PBM owned by 10 Blue Cross
Blue Shield plans covering 20 million lives.
Weve built the foundation of the most patient friendly
pharmacy, health and wellness delivery network in the
country  one that offers an affordable, accessible, quality
solution to managed care, government and large employers.
This includes 6,400-plus retail stores nationwide; our fastexpanding
health and wellness clinics in stores and on
employer campuses; drug infusion centers; and the largest
independent specialty pharmacy business in the country.
Walgreens mission is to provide the most convenient access
to consumer goods and servicesand pharmacy, health and
wellness services  to our customers, patients, employers
and payors  in communities where people live and work
across America. This will build brand loyalty, sales, profits
and shareholder value.
Weve expanded our management team and
investor communications
Weve expanded our leadership team to help drive bold,
creative and innovative responses to a rapidly changing retail
and health care environment. In the past six months, Wade
Miquelon joined our team as CFO, Sona Chawla as senior VP
of E-commerce, and Kim Feil as chief marketing officer. We
welcome the new ideas and challenges that theyre bringing
to our organization.
In 2008, we held regular quarterly earnings conference calls,
providing clarity to our communication with investors. And in
late October, we hosted an Analysts Day to share our vision
of how  and why  Walgreens is changing and growing to
offer Americans a compelling solution to their shopping and
pharmacy, health and wellness needs.
The challenges
Industry-wide, prescriptions are growing at the slowest pace in
47 years, according to IMS Health. Several factors drove this
slowdown in 2008, including the switch of the allergy drug Zyrtec
from prescription to over-the-counter status, fewer new drug
introductions and safety concerns over newer medications.
The good news is that we continue to gain prescription market
share and now fill 17.6 percent of all retail prescriptions in
the country, up from 16.8 percent last fiscal year.
According to a July survey by the National Association of
Insurance Commissioners, 22 percent of Americans are
reducing doctor visits and 11 percent are scaling back on
medication use. While the drugstore business historically has
been recession resistant, no retailer is immune to the impact
of rising unemployment, inflation and wealth erosion on
millions of cash- and credit-strapped consumers.
When it comes to the holidays, we believe were well-positioned.
Few people cancel Christmas and other major holidays, but
they do buy down. Weve performed well in past recessions
as people seek value in our lower priced toys, electronics
and seasonal dcor. We also bought carefully for 2008 as we
anticipated a prudent consumer. Overall, we remain cautious
in our outlook for fiscal 2009.
Looking ahead
Were working within Walgreens and through our industry
groups to mitigate proposed reductions in prescription
reimbursement. Aggressive cuts in Medicaid prescription
payments for those on state aid can reduce access for patients
who need care the most.
Over the longer term, we foresee strong demand for our health
and wellness offerings as baby boomers age. Prescriptions are
projected to represent just 10.3 percent of American health care
spending in 2008, and are one of the most important factors in
preventing far costlier care in hospitals and emergency rooms.
Employers, insurance companies and government have an
enormous stake in keeping people on their medication and
out of high-cost care.
Our Board of Directors has formed a special committee that is
currently conducting a nationwide search for a permanent
CEO. The committee is considering both internal and external
candidates. Meanwhile, we thank retired CEO Jeff Rein for his
outstanding contributions to the company over the past 26
years. From his days as an assistant manager to his years in top
management, Jeff worked tirelessly to make Walgreens better
for shareholders, customers and employees. His respect for
individuals is a lesson  and a gift  which he has left to so
many here at Walgreens.
In closing, thank you to our investors for your commitment and
support. And a special thanks to our 237,000 employees who are
the face of Walgreens to millions of customers every day.
Sincerely,

Alan G. McNally
Chairman and acting Chief Executive Officer

Gregory D. Wasson
President and Chief Operating Officer