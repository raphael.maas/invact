TO OUR STOCKHOLDERS
LETTER TO OUR STOCKHOLDERS
Lam Research delivered a remarkably successful fiscal 2014, securing market growth and delivering financial
performance consistent with our stated targets relative to semiconductor wafer fabrication equipment spending.
We began the fiscal year with our first-ever $1 billion revenue quarter, and we grew revenue in each consecutive
quarter of fiscal 2014. For the fiscal year, shipments and revenue increased by 23% and 28%, respectively.
These significant achievements demonstrate the value of our commitment to optimizing our portfolio of products
and services to solve our customers most critical challenges. In addition, the Company delivered increased
annual profitability with operating income growing more than twice the rate of revenue growth.
This strong financial performance generated cash from operations of $717 million, significantly contributing to
our targeted return of $1 billion to our stockholders by the end of fiscal 2016, while at the same time supporting
continued investment for our future growth. During the fiscal year, we repurchased 4.9 million shares of common
stock at an average share price of $52.10, and we initiated our first quarterly dividend of 18 cents per share.
Outperformance for Lam Research compared with overall spending on wafer fabrication equipment has emerged
as a major theme for our business, not just in fiscal 2014, but more importantly in our future. The devices our
customers are creating are growing increasingly more complex as feature sizes continue to shrink and new
device architectures and materials are introduced. Semiconductor manufacturing processes are correspondingly
more dependent on leading-edge equipment and support, particularly within the market segments served by
our strong portfolio of deposition, etch, and clean products, as well as our spare parts and service offerings.
Increased customer investments in these areas have already grown our served available market, and we expect
expansion in the amount of $2 billion in our annual served market over the next few years.
This market expansion results from our customers need to introduce new technologies to continue their
innovation. The introduction of FinFET structures in logic, advanced patterning schemes in DRAM memory and
logic, and 3D architectures in NAND memory and advanced packaging creates inflection points in the market
which we have targeted for additional market share gains. These critical applications require stringent control
of process variability to maximize customer yield and facilitate the manufacture of devices with lower power
consumption and improved semiconductor performance. Lam is investing heavily in these opportunities, and
as a result, we are innovating at perhaps the highest rate in the history of our company, with 17 new product
configurations introduced in the last 12 months. We are very excited about key product penetration successes
that, with continued execution by the Company, position us well for the upcoming market expansion:
 In deposition, customers are relying on our VECTOR ALD Oxide tool in multiple patterning schemes that
use atomic layer deposition to create highly conformal films to define critical pattern dimensions. In 3D
memory structures, our VECTOR Q Strata delivers ultra-smooth, uniform films with high productivity, and
our ALTUS Max ICEFill provides void-free tungsten-fill for the geometrically complex 3D NAND wordlines.
 Our market-leading performance in etch has been enhanced by the introduction of the 2300 Kiyo family with
Hydra technology. This technology facilitates within-process die-by-die uniformity control and also can correct
for incoming pattern non-uniformities. We also announced the introduction of an atomic layer etch capability
on the 2300 Kiyo F Series, which is the first high-volume-manufacturing-worthy atomic layer etch system
available in the market. For 3D NAND, our newly introduced 2300 Flex F Series etches high aspect ratio
structures with minimal distortion or sidewall damage.  In wet clean, our next-generation tool is being evaluated at several of our largest customers, and we see
emerging acceptance for our broader offering. Damage-free cleans are exceptionally difficult at critical
dimensions, and our new spin clean tool has demonstrated this capability with competitive productivity.
These and other application successes support our confidence in our ability to continue growth in excess of
the pace of our industry. Over the next three years, we are targeting share gains of 4-8% in deposition, 3-5% in
etch, and 5-10% in single-wafer clean from the calendar year 2013 baseline. Overall, we estimate our technology
inflections market share to be in excess of 50% across the deposition, etch, and clean product portfolio, some
10 percentage points higher than our current baseline.
While these anticipated market share gains will drive equipment shipments and revenues over the next several
years, it is also important to recognize our growing spares and service business, which extends the life of
our installed base by offering our customers productivity and technology enhancements, as well as providing
process knowledge solutions. Our
installed base business is projected to
deliver higher revenue growth rates than
the implied number of Lam tools shipped
over the next several years, as we focus
on delivering more comprehensive,
differentiated, and configurable lifecycle
solutions to our customers.
A key differentiator in Lams current success as well as our future outperformance opportunity is our focus on
customer trust and collaboration. We invest in collaborative research programs across our industry with our
customers, consortia, and leading educational institutions to remain broadly engaged with next-generation
process development. We partner extensively with our customers, in their research and development
laboratories as well as their manufacturing facilities, contributing to, or delivering, solutions for their most difficult
challenges as rapidly as possible. This commitment to our customers and the knowledge we develop as a result
of this collaboration serve as the foundation for our fast solution leadership at a time of ongoing consolidation in
our sector, both among equipment providers and customers.
In closing, we extend our sincere thanks to everyone who has made a commitment to our company: to our
supportive stockholders for your interest and investment in us; to our valued customers for their collaboration
and support; to our suppliers for their responsiveness and partnership; and to our global employees for their
tremendous efforts and dedication and whose contributions are fundamental to the success of our business.
Sincerely,
Martin B. Anstice Stephen G. Newberry
President and Chief Executive Officer Chairman of the Board