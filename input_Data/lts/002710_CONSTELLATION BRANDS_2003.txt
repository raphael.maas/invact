


                           DEAR SHAREHOLDERS
                                                           A PROVEN STRATEGY
    More than any previous year in Constellation's
    history, Fiscal 2003 validated our multiple-
                                                             Constellation achieved strong results this past year
    category, brand-focused and consumer-driven
                                                           because of a proven business strategy.
    operating strategy. We achieved strong sales
    that were leveraged into excellent financial             More than a decade ago, we set a course to
    results and the creation of shareholder value.         participate in all the major beverage alcohol

                                                           categories  wine, beer and spirits. Today we have
    In this review, we discuss our strategy and how it
                                                           strong presence and scale in each, and are not
    helps us to keep growing. We also offer perspective
                                                           dependent upon a single category for our success.
    on Constellation's market strength and present
    highlights from each of our businesses that              Understanding consumer trends is equally
    underscore the contributions of our people,
                                                           important. To stay close to our markets, we have
    products and performance to our success.
                                                           empowered our businesses by maintaining individual




                                                                        portfolio, and newly developed brands keep the

                                                                         portfolio contemporary. In many cases our
 FISCAL 2003 PERFORMANCE
                                                                         brands are #1, #2 or #3 in their categories, in
 HIGHLIGHTS
                                                                         others they may be the fastest growing
      Net sales increased five percent to reach
        $2.7 billion
                                                                         participant. Together they enhance our
      Operating profit margins increased                                competitive advantage.
        80 basis points*
                                                                             Building on that business strategy, our
      Net income grew 22 percent*
                                                                         operating strategy is focused on scale, breadth and
      Earnings per share increased 16 percent*
                                                                         growth. Scale improves our distribution and
      Record free cash flow from operations
        of $164 million                                                  routes to market, breadth meets consumer

                                                                         preferences, and participating in a balance of high
      The completion of five consecutive years
        of generating double-digit earnings
                                                                         growth and steady growth businesses drives
        per share growth*
                                                                         consistently superior performance.

sales, marketing and production functions. Our
                                                                         AN IMPORTANT ACQUISITION
talented employees are adept at recognizing and

responding to consumer taste preferences. They                               As the beverage alcohol industry consolidates,
know best what new brands, line extensions,                              there are acquisition opportunities in all areas.
sales techniques and marketing innovations are                           But for Constellation, a prospective acquisition
necessary to keep the business performing well.                          target must survive a disciplined evaluation
They also know how to pinpoint new growth                                process that ensures it is a good fit, maintains our
opportunities. And because of Constellation's                            portfolio balance, and increases shareholder
sound financial position, we can act quickly to                          value. Only a few make the grade  BRL Hardy
invest in our existing business or acquire new                           clearly did.
businesses to capture those opportunities.
                                                                             We began our relationship in August 2001,
   The result is our broad and strong portfolio                          when we established our joint venture  Pacific
of over 200 wine, beer and spirits brands. No                            Wine Partners  to distribute Hardy's Australian
other beverage alcohol company has this breadth.                         wines and a selection of California wines in
Well-established brands are the foundation of our                        the U.S.
business, acquired brands invigorate our




       The joint venture's performance exceeded our          because of our leading position in each

     expectations and was the catalyst for acquisition       key category.

     discussions. The evaluation was made easier
                                                               We have an enviable roster of businesses and
     because we knew and respected Hardy's senior
                                                             each plays a strategic role in making Constellation
     management. We shared similar ways of doing
                                                             greater than the sum of its parts.
     business, and by combining our mutual strengths
                                                               Our dynamic wine business has brands ranging
     we knew we could create the leading wine business
                                                             from the most popular to the highest in luxury. Its
     in the world. Having now come together, the
                                                             tremendous scale and leading positions in the
     addition of Hardy's fast growing business will
                                                             world's key growing wine markets has allowed us
     accelerate Constellation's overall sales and earnings
                                                             to generate steadily increasing profits.
     growth prospects.
                                                               Imported beer is one of the fastest growing

     MARKET STRENGTH                                         beverage alcohol categories in the U.S. and our

                                                             business is a pacesetter. We import a great mix of
       There is no pure measure of a company's
                                                             brands whose sales are growing at a significantly
     market strength within the fragmented beverage
                                                             higher rate than those for domestic beers. And
     alcohol industry. However, Constellation is unique
                                                             there is considerable room for them to keep

                                                             growing. While imports have rapidly grown over

                                                             the past ten years, they command just over an
       LEADING POSITIONS
                                                             11 percent share of the overall U.S. beer market.
          #1 importer of beers in the western U.S.

          #1 producer of wines in Australia                    Our spirits business is stable, generating

                                                             significant profit and strong cash f low. While
          #1 supplier of wines in the U.K.
                                                             premium brands get most of the fanfare, the area
          #1 independent U.K. drinks wholesaler
                                                             we focus on  value to mid-premium spirits 
          #2 wine producer in the U.S.
                                                             commands a 50 percent share of the total U.S.
          #2 wine producer in New Zealand
                                                             spirits market and is strategically important to
          #2 cider producer in the U.K.
                                                             our distributors. As a low cost producer,
          #3 spirits producer in the U.S.
                                                             Constellation leads this category. This allows us to

                                                             be price competitive and innovative.



   Our U.K. business represents a foothold for                   We manage Constellation for ongoing success

growth. With the unique position of being both a              and set clear goals reflecting our high potential. We

beverage alcohol producer and independent drinks              target growing net sales from our existing businesses

wholesaler, we can capitalize on our industry                 by an average of six percent or greater per year over

knowledge and find new market opportunities.                  the long term, and managing our Company

One of those key opportunities is the rapid growth            effectively to generate earnings per share increases

of New World wine consumption in the U.K.                     of 15 percent or greater from that growth. To

Adding Hardy's brands to our already leading                  complement those targets, we look for strategic and

portfolio is also a springboard for market expansion          financially attractive acquisitions to further accelerate

into other countries where New World wines are                sales and earnings.

increasingly popular.
                                                                 We have achieved that level of growth over

                                                              more than ten years  something in which I know
AS WE LOOK FORWARD
                                                              every Constellation employee takes pride. Our
   For nearly 60 years, Constellation has prided itself       consistency is the key to delivering value to you, and
on the way it does business. We have established              I firmly believe it will continue. We move into Fiscal
strong relationships with our distributors, retailers         2004 with our strategies unchanged, and we are
and suppliers; and we offer our consumers                     executing them from a significantly stronger base
consistent, high-quality brands they know and                 than just a year ago.
trust. Our culture is also focused on maintaining the
                                                                 I am excited about our prospects and believe
highest standards of integrity in all that we do.
                                                              the best part of Constellation's history is yet to

                                                              be written.




