WELCOME FROM OUR CEO
This year marks Gileads 30th anniversary. I joined Gilead more than 27 years
ago as a research scientist and have worked in many different roles over the
years, completing my first year as CEO in March. Being part of a company that
has transformed care and made a difference in the lives of so many individuals is
something that I carry with me every day, as do all Gilead employees.
During my past year in this new role, Ive spent time
meeting with people across the organization  researchers,
field representatives, manufacturing team members and
many, many others. My most important takeaway from
these interactions  and the knowledge that gives me great
confidence in Gileads future  is that our nearly 9,000
employees share a drive to accomplish the same goal: to
develop and deliver innovative medicines that truly
change lives.
SCIENTIFIC INNOVATION
Its often difficult to describe what we do to people who
dont see our efforts up close. I spoke recently with an
employee in our research organization who has dedicated
his career to improving treatment for people with HIV. Like
many of us, he could not have imagined the progress that
has been made since HIV, the virus that causes AIDS, was
first identified. Since the approval of Viread in 2001, Gilead
has led the field, introducing multiple single tablet regimens
that improve and simplify the lifelong treatment that is
required to suppress the virus. Yet, we know we can do more.
Our researchers continue to innovate, with an unrelenting
focus on how to further improve care and even, perhaps,
one day cure HIV. I recently watched from the audience as
one of our scientists presented at a medical conference on
groundbreaking work to develop an investigational molecule
that inhibits HIV capsid, a novel target, and is potent enough
to potentially be dosed far less frequently than the daily
therapy required today. This is just one example of the
exciting work taking place in our laboratories every day,
across our therapeutic areas. And it is just one example
of what we do, and how we do it, made clear through the
dedication of our employees.
In chronic hepatitis C (HCV), we have introduced three
unique treatments since 2013, each one expanding the
number of people who can be cured with direct-acting
antiviral combination therapy. Such rapid innovation is
unusual in our industry. And yet we are developing a new
combination regimen, designed specifically for use in a
salvage setting among patients who have not been cured
by existing treatments. This regimen is currently under
regulatory review and we hope to have approval later this
year. Gileads range of treatment options means that nearly
every patient will have a good choice available to potentially
cure them of their HCV infection, regardless of the genotype
of the virus or the level of disease in their liver. With tools
like these in hand, it seems more and more possible that we
could realize the goal of eliminating HCV.
Were making significant strides in nonalcoholic
steatohepatitis, or NASH, which is a chronic liver disease
associated with the accumulation of fat in the liver, that
can lead to inflammation, progressive fibrosis and cirrhosis.
NASH is a growing public health concern as more and more
individuals progress to the later, debilitating stages of fibrosis.
Patients who progress to the latest stages of disease have a
poor prognosis and very few options for treatment. We are
targeting this smaller patient population for studies using
one of our three investigational drugs. The most advanced
is selonsertib, a compound that targets ASK-1, an enzyme
implicated in NASH. Last year we presented data from a
Phase 2 clinical study suggesting that selonsertib can reverse
fibrosis in patients with NASH. Two Phase 3 studies of
selonsertib are now enrolling.
In the area of inflammation, Phase 3 studies of filgotinib, an
investigational JAK1-selective inhibitor for inflammatory
disease indications, are now underway in rheumatoid
arthritis, ulcerative colitis and Crohns disease. Filgotinib is being studied as a once-daily pill and the safety profile seen in Phase 2 studies has encouraged us and our partner Galapagos to look at filgotinib across a range of different inflammatory diseases where JAK-1 activity is implicated.
We have now started or announced our intention to study filgotinib in six Phase 2 clinical trials in additional inflammatory diseases.
While we are encouraged by our internal pipeline programs, our 30-year history has taught us that innovation comes from many sources and we will continue to pursue compounds, programs or companies that complement our science, fit with our culture and have the potential to make a big difference for patients.
ENABLING ACCESS
Over the past year, Ive had the chance to meet with many physicians, payers and public health officials to discuss how we can best enable access to our medicines. We have put in place significant discounts and rebates for our HCV medicines to ensure we reach the populations in highest need; for example, our lowest pricing in the United States has helped expand the number of veterans being linked to care in the Veterans Administration health care system. And weve worked with governments, from Portugal to Mongolia, to broaden access to HCV therapy.
In HIV, through our access program, we have increased the number of people receiving the companys antiretroviral therapies in the developing world from 30,000 in 2006 to more than 10 million in 2016. The countries where our access program operates account for more than 75 percent of the worlds HIV cases. These 10 million treated individuals represent about two thirds of the total number of people receiving treatment worldwide  a significant public health success. We thank our manufacturing partners and important funders of these initiatives such as the Global Fund and the U.S. PEPFAR program. Without their vital help and dedication, we would not have been able to provide access for more than one quarter of the worlds infected population. We look forward to bringing our tenofovir alafenamide (TAF)-based regimens to HIV-infected people in the developing world.
Gilead is also working on neglected and emerging tropical diseases, most of which disproportionately impact resource-limited countries. We have partnered with the World Health Organization for many years to provide AmBisome for use in several developing countries as a treatment for visceral leishmaniasis, a deadly parasitic infection. We are also collaborating with U.S. Department of Defense, Centers for Disease Control and Prevention and National Institutes of Health, as well as multiple academic institutions, to discover and develop novel antivirals for highly pathogenic infections and neglected or emerging viral diseases. As an example,
GS-5734  Gileads most advanced investigational agent 
is currently being studied in Ebola survivors.
COLLABORATIVE APPROACH
In 2016, Gilead partnered with more than 2,000 organizations  providing a total of almost $460 million in funding across the four areas of focus of our corporate giving program: reducing health disparities, providing access, advancing medical education and supporting local communities.
The impact of this funding is critical, and in the following report, you can read about some of the work that our grantees do to reach and advocate for people with serious diseases. Beyond the funding we provide, Gilead employees engage directly, through volunteerism, with organizations in our communities. In 2017, we will mark our first formal week of volunteerism, in honor of the companys 30th anniversary, as employees around the world will offer their time to local groups whose missions are closely aligned with our own.
IN CLOSING
This year, we have combined our Annual Report and Corporate Social Responsibility Report into a single report titled Year in Review 2016 to share essential information about the companys financial, social, environmental and governance performance. The stories we highlight underscore how the companys core values guide us in our mission to advance the care of individuals living with life-threatening diseases worldwide. I encourage you to learn more about what inspires our employees to be good corporate citizens.
I am proud of the efforts we have made and will continue to make, and I look forward to sharing with you the many accomplishments across this organization in 2017.
Thank you,
John F. Milligan
President and Chief Executive Officer
May 2017