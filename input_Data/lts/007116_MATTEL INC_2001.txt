To our shareholders

DESPITE THE SIGNIFICANT CHALLENGES WE FACED DURING 2001, MATTEL HAD A
SUCCESSFUL YEAR AND IS POISED TO CONTINUE IMPROVING ITS PERFORMANCE
IN THE YEARS AHEAD.

2001 presented substantial obstacles for
our company. Global economies softened;
the September 11th terrorist attacks eroded
U.S. consumer confidence; and as a result,
several important U.S. retailers canceled
holiday reorders as they intensified their
focus on inventory management in light of
uncertain consumer spending prospects.
As a result, despite reaching record levels,
Mattels three percent worldwide net revenue
growth for the year was at the low end
of our expectations. A precipitous eight
percent decline in U.S. shipments during
the fourth quarter brought full-year gross
revenues down one percent in the U.S.,
the worlds largest toy market. Offsetting
weak U.S. sales, our international gross
revenues grew 10 percent for the year and
we have now experienced six consecutive
quarters of growth internationally. A turnaround
in our European business, which
grew 13 percent for the year, led the
international performance. Our business
also grew strongly in Latin America
(20 percent), Canada (10 percent) and
Australia/New Zealand (3 percent).
In last years letter to shareholders, I wrote
that one of our key priorities for 2001 was
to strengthen core brand momentum in
the U.S. and abroad. And we did just that.
Consumer demand for our brands grew
around the world as we gained market
share in virtually every country where
we sell our products. In the U.S., despite
weaker-than-expected
shipments to
retailers, all of our
major brands
showed strength
with consumers
and posted sales
increases at retail.
According to NPD industry data for toy
sales at the consumer level, we gained

market share in the U.S. in the dolls, vehicles,
action figures, games and puzzles and
core infant and preschool categories. Our
market share of total traditional toys grew
1.2 percentage points to 23.5 percent.
We also recorded strong market share
gains outside the U.S.,
as our market share
grew in the five major
European markets,
as well as in Canada,
Mexico and Australia.
We began reaping the
benefits of our strategic partnership
with Bandai, Japans largest toy
maker, eliminating chronic operating
losses in that country and building our
brands in the worlds second largest
toy-purchasing nation.
Our brands benefited from strong marketing
programs, which promoted product
lines such as Barbie in the Nutcracker,
the innovative Whats Her Face? doll,
which combines fashion and activity play,
the Fisher-Price
Rescue Heroes line
of positive role models,
and Hot Wheels,
which once again
claimed the title of bestselling
toy of the year.
We further gained market share by improving
the performance of our supply chain.
Our customer service levels strengthened
measurably as we partnered with retailers
to get the right product to the right place
at the right time, all with lower costs.
This improvement allowed retailers to
better plan and execute merchandising
programs, which supported our market
share growth.
In 2001, perhaps
the most dramatic
evidence of our
improved supply
chain performance
was seen in the successful,
worldwide
launch of our Harry Potter toys to coincide
with the global theatrical debut of
Harry Potter and the
Sorcerers Stone,
last years box office
smash. This was
particularly challenging,
as never before
had we launched so
many products on a
global basis in such close proximity to the
holiday season. I am pleased to report
that our supply chain teams executed
tremendously well.
I also wrote in last years annual report
about the importance of executing our
financial realignment plan and delivering
the cost savings we promised. I am
delighted to say that we are on track to
realize the full $200 million in savings that
we projected. In 2001, gross margin
increased by 110 basis points, while
selling, general and administrative costs

decreased by 40 basis points, excluding
the bad debt expense related to Kmart
Corporations bankruptcy filing. (Printing
this report in conjunction with our Form
10-K is just one example among hundreds
of projects to lower overhead costs.) As
a result, operating profit advanced
15 percent and
represented 13.3
percent of sales.
Worldwide income
from continuing operations was $351.6
million or $0.81 per share, excluding
restructuring and non-recurring charges.
This represents 20 percent growth from
$293.3 million or $0.69 per share in 2000.
Including restructuring and non-recurring
charges, full-year earnings per share from
continuing operations were $0.71, compared
to $0.40 in 2000.
Most encouragingly, as a result of our
strong cash flow from operations in
2001, we ended the year with more than
$600 million in cash, an increase of more
than $380 million from the prior year. We
also reduced debt by more than $230 million.
Year-end debt as a percentage of total capital
was 42 percent, down from 52 percent
at year-end 2000. This puts us well on our
way to achieving our stated long-term
objective of having debt represent approximately
one-third of total capital.
While delivering on our near-term financial
objectives, we also made investments
aimed at ensuring a strong future. We
invested in manufacturing capacity in
Mexico to absorb
production from our
Kentucky plant,
which will cease
operations this year
as part of a phased
closure. We improved
efficiencies by moving
more product designers
and engineers closer to production locations.
We developed and have begun executing
a three-year plan to fully integrate and
upgrade our information systems and
technology infrastructure. We have added
warehouse and office space in China to
capitalize on the regions structural cost
advantages.
We also acquired
Pictionary, Inc., the
owner of the
Pictionary board
game, obtained a
license for the
rights to Barney,
and renewed our global alliance with
Sesame Workshop and our license for
the Disney character brands. Licensing
agreements with Vivendi Universal
Publishing and THQ also were finalized to
extend our core brands, including Barbie
and Hot Wheels, to console games and
personal computers.
Most importantly, we have invested in the
people of Mattel, whose talents provide

our most prized
competitive
advantage. While
our workforce was
smaller in 2001,
and has been further reduced in 2002,
we have enhanced employee development
with performance tracking, leadership
classes, global employee
surveys and follow-up
action plans. The result
is a smaller, and we
believe, more
productive workforce.
Finally, our progress
continues in the important areas of
corporate governance and social responsibility.
We strengthened our Board of
Directors with the addition of G. Craig
Sullivan, Chairman and Chief Executive
Officer of The Clorox Company and
Kathy Brittain White, Executive Vice
President and Chief Information Officer
of Cardinal Health, Inc. in 2001. These
well-respected, highly successful business
leaders add invaluable new perspectives
to board discussions.
We continued to
improve the communities
where Mattel
employees live and
work. We provided
the time, talent and
resources needed to
build a childrens hospital
in Tijuana, Mexico, several elementary
schools in Indonesia and a daycare center
in China. Here in the U.S., construction of
the new, world-class Mattel Childrens
Hospital at the University of California,
Los Angeles is underway thanks to the generosity
of the Mattel Childrens Foundation.
Our work with the Mattel Independent
Monitoring Council
(MIMCO) is another
source of great
pride. Since 1997,
MIMCO has developed
standards for
and audited the
working conditions
and welfare of our manufacturing employees
around the world. In 2001, MIMCO
broadened its reach by advising and
assisting Mattels suppliers in an effort to
improve the lives of more people who
are associated with Mattels products.
While we have successfully begun to turn
around Mattels performance, there still is
much work ahead of us. First-rate companies
consistently perform well and that is
what we intend to do.
Last year, the inculcation of our vision
allowed us to refocus on what we do better
than any other company in the world 
design, manufacture and market the most
innovative toys. While our vision of being
the "worlds premier toy brands for today
and tomorrow" remains unchanged, our
mindset for 2002 is progressing from
refocus to optimize. We will accom-

plish this by adhering to the four priorities
I outlined in last years shareholder letter.
I would like to tell you how we intend to
build upon these objectives this year:
1. Strengthen core brand momentum in
the U.S. and abroad. In 2002, we will build
on the strong point-ofsale
momentum that
we experienced in 2001.
As an encore to our highly successful
Barbie in the Nutcracker film and product
line, in 2002, Barbie will star in the story of
Rapunzel, with a video and toy line.
Harry Potter fans will enjoy toys associated
with the video and DVD release of
Harry Potter and the Sorcerers Stone,
as well as the fall movie premiere of Harry
Potter and the Chamber of Secrets.
Matchbox celebrates its 50th anniversary
in 2002 with a year-long marketing program
and a commemorative product line
for car-loving kids of all ages. As the infant
and preschool leader, Fisher-Price will introduce
a new line of educational toys while
also expanding its targeted age group
with a line of Pre-Cool products. New
games also will be launched, including the
Disneys Wonderful World of Music
Game featuring some of the worlds mostlistened-
to scores. New action figures supporting
Yu-Gi-Oh! will debut based on
the successful cartoon and card game from
Japan. Masters of
the Universe, including
He-Man, will be
relaunched to a new
generation of hero
worshippers. And
as part of The
American Girl
Collection, this
year millions of
young girls will
meet Kaya, the first Native American
American Girl doll.
Latin America will be a primary focus for
us. While sales have grown at a significant
rate in the past few years, our business
systems and management skills have not
kept pace, resulting in inconsistent profit
growth. Therefore, we have appointed a
solid new management team and are making
adjustments we believe are necessary
to convert sales growth into increased cash
flow and profitability.
2. Execute the financial realignment plan
and deliver the cost savings announced in
September 2000. In 2002, we will close
our Murray, Kentucky manufacturing plant
and will continue to evaluate opportunities
to eliminate underutilized production

capacity, improve our gross margin and
lower overhead costs. We have reduced
the workforce at our Mattel corporate
locations by seven percent, and our
investments in information systems and
technology will help us better manage the
business, with lower costs in procurement,
finance, distribution
and manufacturing.
3. Improve supply
chain performance
and customer service levels. We have
become one of the industrys leaders in
customer service. And while there always
is work to be done when it comes to customer
service, we also are focusing on
improving supply chain efficiencies.
For example, we will launch fewer new
SKUs this year by taking advantage of
multi-lingual packaging, providing us with
increased distribution options for any given
toy. We have implemented a companywide
initiative to better align shipments
with consumer demand by contracting the
time it takes for us to manufacture and distribute
toys. While our new program will
reduce first-half revenues, there will be tangible
benefits in terms of lower shipping
and promotional costs, as well as strengthening
our partnerships with retailers as
they continue to focus on just-intime
inventory management.
4. Develop our people and
improve our employee development
processes. In 2002, advanced leadership
and global leadership classes will be
added to our training curriculum, and a
new innovation platform will be offered to
product designers to further hone their
creativity. E-learning initiatives will be
expanded, allowing more employees the
power to develop their
skills at their own individual
pace.
Mattels vision is to create
and market the worlds premier toy brands
for today and tomorrow. In the past two
years, we have refocused the company on
its core business  toys, and its core competency
 building brands. We have
reduced costs and developed our people.
We have begun to optimize our product
portfolio and business systems. And most
importantly, we have tasted success  and
we hunger for more.
On behalf of my 27,000 colleagues around
the world, thank you for investing in our
business  and in us.
Sincerely,
the worlds premier toy
brands for today and
tomorrow
Robert A. Eckert
Chairman of the Board and
Chief Executive Officer