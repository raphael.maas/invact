To our stockholders,
Im pleased to report that our company delivered
another year of progress in 2017. On virtually every
meaningful measure  from network reliability to
customer satisfaction to community involvement 
we continued to perform at a high level.
We achieved record financial results. Our largest
utility, We Energies, was named the most reliable
utility in America and the best in the Midwest for the
seventh year running. We made significant progress
upgrading the natural gas infrastructure in Chicago.
And after reviewing our environmental, social and
governance practices, Corporate Responsibility
Magazine named us one of the best corporate
citizens in the nation.
In addition, our track record of reliability and
competitive pricing was a factor in the decision
by Foxconn Technology Group to invest $10 billion
in a massive, high-tech manufacturing campus
in Wisconsin. This is one of the largest economic
development projects in American history. We expect
Foxconn to employee 13,000 people as they create a
brand new industry here in the United States.
Now, for a few details. Our adjusted earnings for 2017
totaled $3.14 a share  the highest earnings per share
in company history. These reported earnings exclude
a one-time, non-cash gain of 65 cents a share from
the tax reform law that was signed last December.
This one-time, non-cash gain reflects the application
of the new tax law to the companys non-utility assets
and to the assets of the parent company.
Over time, the impact of tax reform will also benefit our
utilities. And we are filing plans with regulators in each
of the four states we serve to channel the impact of a
lower tax rate into direct benefits for customers.
In addition, there were a number of other positive
regulatory developments during the year. In August, the
Wisconsin Public Service Commission unanimously
approved our proposed rate settlement. Base rates for
all of our Wisconsin utilities will remain flat for 2018
and 2019. In total, this will keep base rates frozen for
four years and essentially gives our customers price
certainty through 2019.
As part of the agreement, we also expanded and
made permanent certain pricing options for our large
electric customers. These options will continue to help
many of our customers grow their businesses, create
jobs and reduce their energy costs.
In Illinois, we continue to make real progress on the
Peoples Gas System Modernization Plan. On January 10
of this year  after an extensive review  the Illinois
Commerce Commission issued an order that supports
continuing the program at the scope, pace and
investment level that we proposed. This program is
critical to providing our Chicago customers with a
natural gas delivery network that is modern, safe and
reliable. For many years to come, well be replacing
outdated natural gas piping  some of which was
installed more than a century ago  with state-of-theart materials.
Turning to Michigan, we obtained final regulatory
approval last October for the construction of
approximately 180 megawatts of new, natural gas-fired
generation in the Upper Peninsula. Site preparation
began within days of the commission order. 
Our plan is to bring the new facilities into commercial
service by mid-2019, and at that time or soon thereafter,
we expect to retire our coal-fired power plant in
Marquette, Michigan.
The project calls for a $266 million investment in
reciprocating internal combustion engines. We
call these RICE units. These units, which will be
owned by one of our Michigan utilities, will provide
an affordable, long-term power supply for customers
in the Upper Peninsula, including the iron ore mine
owned by Cleveland-Cliffs.
The RICE units are part of our new five-year capital
investment plan. Totaling $11.8 billion, our updated
capital plan is focused on reshaping our generation
fleet for a clean, reliable future.
Our approach calls for greater reliance on natural
gas and solar energy to meet customer demand for
electricity. We plan to add another 50 megawatts
of RICE generation in northern Wisconsin by 2021.
We also have the option to invest up to $200 million
in the Riverside power plant  a natural gas-fired
facility being built by Alliant Energy.
And importantly, we plan to expand our portfolio of
renewable generation. Over the past five years, utilityscale solar has increased in efficiency, and prices
have dropped by approximately 70 percent. These
factors make solar a cost-effective option for our
customers  an option that also fits very well with our
summer demand curve. Were in active discussions
with developers, and we plan to file for approvals
with the Wisconsin commission this spring.
As we look to the future, the company has a solid
financial base and a strong pipeline of investment
opportunities. With confidence in our future prospects,
the board of directors raised the quarterly dividend
at its January meeting to 55.25 cents a share  an
increase of 6.25 percent over the previous rate. This
will mark the 15th consecutive year that our company
will reward our shareholders with higher dividends.
We continue to target a dividend payout ratio of
65 to 70 percent of earnings. Were in the middle
of that range now, so I expect our dividend growth
will continue to be in line with the growth in our
earnings per share.
On a personal note, I know that all of you are
interested in an update on Allen Leveretts recovery
from the stroke he suffered last October. Allen, of
course, succeeded me as chief executive in 2016.
As of the date of this writing, I can report to you that
Allen is in good physical condition, and he continues
to make progress in his recovery and rehabilitation
work. Among other activities, Allen has been engaged
in extensive speech therapy.
No specific time table has been established for his
return to the company. So, as we announced a few
months ago, I have agreed to serve as chief executive
for as long as necessary.
Thank you for your confidence, your support and
your investment in WEC Energy Group.
Sincerely,
Gale E. Klappa
Chairman and Chief Executive Officer
Feb. 28, 2018