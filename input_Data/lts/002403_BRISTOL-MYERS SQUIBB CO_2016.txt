TO OUR SHAREHOLDERS
2016 was an important year for Bristol-Myers Squibb. We delivered strong operational and financial
results. We expanded the indications and market performance of our PD-1 inhibitor Opdivo. We
advanced Eliquis towards leadership in the prevention of stroke for patients with atrial fibrillation.
We advanced a broad and deep portfolio and pipeline of opportunities in Immuno-Oncology and in
core specialty therapeutic areas such as cardiovascular, fibrosis and immunoscience. And we continued
our work in underserved communities to make healthcare accessible to those who need it.

We also focused on the future, continuing to
advance our Biopharma strategy by initiating
an important operating model evolution aimed
at focusing our resources on the most important
priorities for our business.
It was a year in which we further strengthened
our platform for creating value, recognizing our
responsibility to shareholders to build long-term,
sustainable growth through innovation that
makes a difference, today and tomorrow.
And throughout 2016, in every part of our
organization and in every part of the world, our
work continued to be driven by an unwavering
commitment to the people at the center of everything
we do  our patients and their families. They
are the reason for our focus on transformational
medicines. They are the reason we worked harder
and faster to deliver our results.

Delivering for Today
With respect to our overall operating
performance, 2016 was an outstanding year.
We exceeded our budget targets and delivered
$19.4 billion in revenue  17% growth
over the preceding year. This was due to
double-digit growth in a number of key
markets and across much of our portfolio.
As a result of our strong operating performance,
we achieved 2016 GAAP EPS
of $2.65 and Non-GAAP EPS of $2.83,
which represents growth of over 150% and
41% vs. the previous year, respectively.
Much of our success in 2016 was driven
by the strength of our Immuno-Oncology
portfolio with revenues of nearly $5 billion.
Opdivo sales reached $3.8 billion, as a result
of a strong uptake across all tumor types.
Yervoy sales in the U.S. started to grow
again, due to the adoption of the combination
regimen in melanoma.
Tackling the hardest-to-treat cancers is
challenging, and we saw this first-hand
last August with the negative outcome of
Checkmate 026, our Opdivo monotherapy
clinical trial in 1st line lung cancer. We
have integrated important lessons learned
from this trial into our broad development
program in lung cancer, and we believe
our medicines will continue to play an
important role in improving treatment
outcomes for this deadly disease.
More generally, we further broadened the
label for Opdivo, which is now approved
in the U.S. for 11 indications in six tumor
types  an unprecedented accomplishment
in just over two years. This includes a new
indication in head and neck cancer, which
was achieved late in the year, as well as in
bladder cancer, which was achieved early in
2017. We also saw Opdivo grow outside the
U.S., with significant regulatory approvals
in the European Union and Japan.
Beyond Immuno-Oncology, other key
growth drivers for our strong 2016 results
included the anti-coagulant Eliquis, which
delivered $3.3 billion in sales  an 80%
increase over the previous year. Eliquis
has become a leading oral anti-coagulant
globally for stroke prevention in patients
with atrial fibrillation. In the U.S., we
established the leadership of Eliquis in
cardiology in terms of the number of total
prescriptions, surpassing warfarin, the
long-time standard of care. Additionally,
we made good progress in the overall
market, which includes primary care
physicians.
Orencia and Sprycel performed very well
in 2016. Both experienced double-digit
growth with $2.3 billion and $1.8 billion
in sales, respectively.
The company and its partner Ono also
secured an important patent litigation
settlement, signing a global license
agreement to settle all patent-infringement
litigation related to Mercks PD-1
antibody Keytruda* (pembrolizumab).
This settlement was a critical milestone
for Bristol-Myers Squibb in protecting
our intellectual property rights as the early
innovators in the science of PD-1, a key
mechanism in Immuno-Oncology that
has proven to have transformational impact in cancer care.
We will continue to assert our intellectual property rights in
this important field and to defend our scientific discoveries.
Delivering for Tomorrow
Throughout the year, we also focused efforts on building our
future. This was very clear in the work we did to develop and
enhance our promising pipeline of transformational medicines.
Towards the end of the year, we started to see data from our
next wave pipeline in Immuno-Oncology, with three of these
new assets potentially entering registrational studies in 2017.
In addition, we expect over a dozen data readouts spanning
multiple tumor types that could further broaden the label
of Opdivo and Yervoy over the next couple of years.
Lung cancer remains a priority for Bristol-Myers Squibb,
and we made important progress in our studies in combination
therapies, which are at the core of our strategy. In fact,
we have several ongoing Phase III combination programs to
address the urgent need for improved treatment options for
patients with lung cancer.
We also advanced our early pipeline in cardiovascular,
immunoscience and fibrosis, where we expect to see important
data readouts beginning in 2017. These programs address
areas of high unmet medical need, such as heart failure, lupus
and non-alcoholic steatohepatitis (NASH). Many of these are
first-in-class molecules, with the potential to be best-in-class.
We have always recognized the importance of acquiring
innovation externally, and 2016 was an important year for
business development at Bristol-Myers Squibb. Our business
development activity continued to enhance our key disease
areas of focus and spanned partnerships with academia,
biotech and larger companies. We acquired Cormorant
Pharmaceuticals, which broadens the companys oncology
pipeline focus on the tumor microenvironment, and Padlock
Therapeutics, which expands the companys immunoscience
pipeline. We signed license agreements with Nitto Denko
for a targeted siRNA therapy in advanced NASH and with
PsiOxus Therapeutics for an armed oncolytic virus to
address solid tumors. Our collaboration with Enterome
focused on microbiome-derived biomarkers and drug
targets to be developed as therapeutics for cancer.
In order to ensure resources are applied to the most important
opportunities, the company initiated an evolution of our
operating model  one that is strengthening our ability to seize
opportunities in an increasingly challenging environment.
Delivering for Underserved Communities
Throughout the year, we continued to build and execute
programs to help ensure that all patients have access to health
care, including our medicines. Through our Bristol-Myers
Squibb Foundation, we promoted health equity and better
health outcomes for people around the world. Through our
Patient Assistance programs, we provided medicines to
patients with financial challenges.
Taken together, our commitment to access has been focused
on populations in need with a global scope. It has included
HIV and cancer in Africa as well as hepatitis C in Asia.
It has included cancer care for vulnerable populations, as
well as mental health care for veterans in the U.S. And this
commitment has included several other programs, including
those that help patients who need temporary help paying for
their medication.
In 2016, our commitment to good citizenship extended beyond
these initiatives. We continued our work with the United
Nations Global Compact, as well as our Go Green activities
at Bristol-Myers Squibb sites to promote sustainability.
We provided disaster relief to support affected populations in
Haiti, Ecuador and Japan. And we joined the White House
Moonshot Initiative, as we share its goal of accelerating new
treatments and, eventually, finding a cure for cancer.
We also put in place an exciting new initiative to provide
cancer care to children in sub-Saharan Africa. Formally
launched in 2017, Global HOPE is a
first-of-its-kind pediatric hematologyoncology
treatment network  one
designed to develop capacity and
infrastructure to treat and dramatically
improve the prognosis and outcomes
of thousands of children with cancer in
southern and eastern Africa.
Strengthening Our Company
Culture
Throughout the past year, we continued
our focus on the 25,000 people who
make it all possible  our extraordinary
colleagues around the world.
We placed an increased priority on
making sure that the experience of
Bristol-Myers Squibb employees is
engaging, challenging and rewarding.
We increased our focus on diversity and
on creating a powerfully inclusive work
experience. We launched our new global
People & Business Resource Groups
(PBRGs), an evolution of the U.S.-
based Employee Resource Groups,
which leverage employees business
insights and diverse perspectives. And
we advanced our WorkPlace initiative,
making significant physical changes to
buildings and offices to create an even
more energizing, collaborative work
environment.
At Bristol-Myers Squibb, our people
are our competitive advantage, and
throughout 2016, we continued to
celebrate and strengthen that fact.
Looking Forward
As I look ahead, I am excited by the
tremendous opportunities we have
to advance the science and continue
making a difference in the lives of our
patients and their families. It is through
this important work that we will deliver
long-term value for our shareholders.
The appointment of Dr. Thomas Lynch,
Jr. as our Chief Scientific Officer is an
important step in our evolution as our
company transitions to its next phase of
growth. Tom brings a unique perspective
to our business through his deep
experience as a clinical researcher, leader
of large research centers and a practicing
physician. We are fortunate to have him
leading our R&D organization.
Building on our progress in 2016, we
are well positioned to navigate the
increasing complexity and competitiveness
of our industry, including the
very important conversations about
pricing and access. We will continue to
draw on the energy, vision and diversity
of our workforce to discover, develop
and deliver transformational medicines.
And we will continue to do all that
we can to make sure that all patients 
regardless of their circumstances  have
access to quality healthcare.
This is who we are. This is what we do.
Giovanni Caforio, M.D., Chief Executive Officer
March 9, 2017
