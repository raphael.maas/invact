TO OUR SHAREHOLDERS:

My letter to you last year spoke of
our execution against the top
priorities I outlined when taking
over as CEO in 2010: efficiently deploying
capital and optimizing our businesses.
Specically, I detailed our success in
expanding our branch presence in the
key markets of Boston and New York, our
strong nancial results, and 10% share
repurchase authorization announced in
December 2012. In 2013, we continued
to invest in our franchise, the leading
independent full-service commercial bank
operating in New England and southern
New York.
Building on Our Expertise
We entered 2013 in an advantaged
position with a well-recognized and
trusted brand operating in markets which
can only be characterized as exceptional:
highly-educated, entrepreneurial, and
deep in small and mid-sized businesses.
These are the best commercial banking
markets in the country. During the year,
we found a number of high-return,
strategically important investment
opportunities to further strengthen
our franchise.
Commercial Banking
We bolstered our commercial banking
capabilities with additional talent in
Boston and southern New York. In
addition, we committed resources to
strengthen our commercial services
offering, particularly international trade
finance and customer interest rate
swaps, which were driven primarily by
demand from existing customers. We
also strengthened our government
banking team. With our long-standing
commitment to communities throughout
our footprint it makes perfect sense that
we provide banking solutions to more of
the municipalities in our markets. Finally,
as we have grown we recognized an
obvious opportunity to service the larger
corporations within our lending markets,
targeting those with $500 million to
$3 billion in annual revenue. We have
the full set of capabilities necessary to
service these customers.
Retail Banking
We invested in the southern New York
retail markets, adding three in-store
branches at Stop & Shop locations. We
built on our culture of empowering
employees through training that builds
expertise and enables them to better
serve our customers across a broad
range of products and services. Our
branches are not only deposit-gathering
locations, but asset-generating businesses
with employees certied in consumer,
residential and small business lending and
holding Series 6 and 63 licenses for certain
insurance and investment products.
Wealth Management
We invested in additional high-quality
in-market Boston, Connecticut, and
Southern New York-based wealth
management and private banking
relationship managers. In addition, in late
2013, we added a new senior executive
for wealth management who possesses
a track record of success spanning over
25 years. We see great potential for our
wealth management and private banking
unit given the team we have assembled,
our best-in-class product oering, longstanding
commercial and individual
relationships, and outstanding markets.
Regulatory
In addition, we have continued to
make infrastructure investments
to keep pace with the heightened
regulatory environment in the form of

additional compliance staff and upgraded
information technology systems.
All of these initiatives, just like the ones we
made in 2010-2012, allow us to better serve
our customers. In every case, we invested
in people who are seasoned, in-market,
and well-known to our management
committee.
Delivering on Our Capabilities
While our geographic and product line
investments over the past few years
are still in the relatively early stages of
development, they are already helping
to support strong loan growth. Notably,
Peoples United is one of only six banks
within the top 50 by assets that have
grown loans every quarter since the end
of 2010. Our highest return opportunity
now is to further deepen our relationships
with customers, serving their deposit, cash
management, insurance, payroll, merchant
processing, brokerage, private banking,
and wealth management needs.
Financial Results
Though our financial performance in 2013
was solid, it understates our progress as
a company. Average earning assets grew
12%. However, our net interest margin fell
fifty-five basis points from 3.86% in 2012 to
3.31% in 2013. This decline was the result of
strong loan growth, the effects of expected
run-off in our acquired loan portfolio and
continued portfolio re-pricing, all within
this prolonged period of ultra-low interest
rates. Our non-interest income growth in
2013 reected the ongoing improvement
in most of our fee-based businesses. I
am particularly pleased that operating
expenses were relatively at as compared
to 2012, considering the signicant
number of strategic investments we made
and the increasing cost of regulatory
compliance.
The following are some highlights from
2013. We grew loans and deposits per
share by 23% and 14%, respectively, and
operating earnings per share increased
3% from 2012 to $0.77 per share.
Operating return on average tangible
equity improved to 9.2% from 8.5%. Asset
quality remained industry-leading with
meaningful declines in non-performing
loans in both the originated and acquired
portfolios and an improvement in loan
charge-offs of two basis points to 0.19%.
Returns to Shareholders
During 2013, we returned approximately
$665 million  or over 15% of our current
market capitalization  to shareholders in
the form of dividends totaling $205 million
and share repurchases of $460 million.
Looking Ahead
As we begin 2014, there is no question we
have entered a new phase in our evolution.
With normalized capital levels, an industryleading
set of capabilities, greater presence
in key metropolitan areas, and a low-risk
business model, we are well positioned
for future growth. Our commitment
to delivering an exceptional customer
experience has served us well for over
170 years, and is being leveraged to
deepen our relationships with both
new and long-standing customers.
We appreciate your continued support.
Sincerely,
John P. Barnes
President and Chief Executive Officer
Peoples United Financial, Inc.
March 3, 2014