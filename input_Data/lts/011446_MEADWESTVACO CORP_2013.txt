
To Our Shareholders, Customers and Employees:

From the deep insights that fuel our participation in targeted markets to
the solutions that help our customers brands win with global consumers,
MWV is a packaging company that delivers. We create value for our
customers. We return value to our shareholders. And, we value our employees
who deliver results around the world.
In fact, we are committed to maximizing the value we deliver to everyone
who has a stake in our success. We did so in 2013 by growing the companys
revenue, returning to shareholders proceeds from our U.S. forestlands along
with our strong dividend, and investing in our business for the long term.
However, the profits we generated last year were below our expectations,
largely due to several operational challenges. We have put in motion an
intensive program to improve our profitability  which will drive significantly
higher earnings and cash flow in 2014 and beyond.
From this strong position, we are looking ahead to accomplishing our
vision of packaging leadership characterized by consistent and sustainable
profitable growth.
Continuing Commercial Success
Overall, we had two percent growth on the top line in 2013  our fourth
consecutive year of profitable sales growth since the recession in 2009. This
was a very good result given still-difficult global economic conditions and
some negative impact from currency. Our continued commercial success
in the marketplace for packaging and specialty chemicals comes from
the four pillars of our profitable growth strategy  commercial excellence,
innovation, emerging markets and expanded participation  as well as the
investments weve made in capacity, capabilities and market insights.
For instance, last year we introduced innovative solutions such as
Versaplast, a home cleaning trigger sprayer, Optilock technology for our
adherence packaging solutions for healthcare, and the Hero Pack and
fragrance pump collection featured on the previous page. We see tremendous
potential for the insight to in-market approach that leverages all of our
capabilities to drive innovation with our customers. More of our customers
are recognizing the advantage of this system and initiating projects and

partnerships that lead directly to sales lift and supply
chain benefits for their brands and revenue growth for
our business. We expect more of this work in 2014,
with a number of new product launches planned in the
food, beverage, beauty and personal care, home and
garden, and healthcare markets around the world.
Last year, we continued to leverage our investments
in emerging markets, driving 10 percent organic growth
in these key regions (not including additional revenue
from the acquisition of Ruby Macons). Our investment
in Brazil  a new paper machine and related mill
enhancements  was a big part of this growth, as we
had higher paper sales and increased pricing based
on the quality improvements of our new paperboard,
HyPerform. We are expanding our corrugated
packaging business in India in similar ways and expect
steady growth as the market continues to move toward
higher-quality corrugated packaging to support middleclass
consumerism and modern retail. Our focused
participation in Brazil, India and other emerging markets
has grown this share of our revenue to nearly a third of
total sales  and we expect to expand further as our
consumer products customers look to these regions
for the majority of their growth over the next decade.
Growth in specialty chemicals also helped pace the
companys top line performance. A four percent increase
in revenue in this segment was driven by continued
strength in value-added chemicals markets that are
aligned with ongoing global growth trends such as

infrastructure building and rebuilding, energy exploration,
and automobile emission controls. With this wellshaped
market participation strategy, we expect this
business to continue to be a source of substantial value
for our shareholders.
Focusing on Profitability
While we are pleased with the progress we made in the
marketplace and the momentum we have carried into
this year, we also know we could have done a better job
turning our commercial success into bottom line results
in 2013. We reported pre-tax income from continuing
operations of $222 million, a modest increase compared
to last year, but saw a substantial improvement in cash
flow from continuing operations, which increased to
$358 million (compared to $220 million in 2012).
These results would have been much stronger had
we not been hampered by operational challenges,
especially early in the year. We were also completing
the final stages of our major growth and productivity
investments. In the latter part of the year, we operated
much better and began to see the benefits from
productivity gains in Brazil and from initial startup of
the new biomass boiler at our Covington, Va., facility
 as well as from an overhead cost reduction program
initiated at the beginning of 2013 that delivered $43
million in savings for the year.
In addition to the positive operating momentum we
carried through the fourth quarter and into this year, we
announced a comprehensive program to improve our
margins and establish a level of profitability and cash
generation that we and our shareholders expect. We
are taking actions that will reduce our costs, simplify
our organizational structure, intensify our innovation
efforts, prioritize our capital investments, and refine our
market participation  all while extending the progress
we have made with our growth strategy. Looking at
this margin improvement program together with the
overhead savings we generated last year, we expect a
combined impact of $150 million to be realized by 2014
and as much as $200 million by the end of 2015.

Delivering Value from Forestland
In addition to the commercial and operating momentum
of our packaging and specialty chemicals businesses,
the sale of our U.S. forestland in 2013 has enabled us
to return significant value to our shareholders.
In completing a unique transaction with Plum Creek
Timber Company, we accomplished each of the strategic
objectives we set at the beginning of the process.
We generated maximum, tax-efficient value from the
assets and we have returned $700 million directly to
shareholders in the form of stock repurchases and a
special dividend.
Beyond these immediate returns, our shareholders will
continue to participate in the upside potential of our
well-located development properties in South Carolina 
especially the Nexton and East Edisto projects in
the Charleston region that the experienced MWV
management team is now leading as part of our joint
venture with Plum Creek.
Building into the Future
We are moving forward confidently with our growth
strategy and margin improvement actions  in both cases
building on the commercial and operational success
that is already evident in each of our segments. We are
all restless to improve performance, and we will do so
with the commitment to excellence that each of our
employees demonstrates around the world.
We will also benefit from the strategic vision and
execution abilities of two newly-elevated leaders.
MWVs Board of Directors recently elected Bob Beckler

and Bob Feeser each to the position of executive vice
president, with global commercial and operations
responsibilities, respectively. They have led some of
our most successful businesses over the past many
years, and the alignment of our commercial and
operations platforms with these two outstanding leaders
will enable us to move even more swiftly to accomplish
our aggressive goals.
With their ascension, Jim Buzzard has stepped down
as president of MWV after 36 years. Jims contributions
to our success have been meaningful and far-reaching
for three decades  and he has helped inspire a new
generation to lead MWV with his brand of integrity,
expertise, and unquestioned commitment to excellence.
His leadership will be missed. So, too, will be the
guidance from Jim Kilts and Doug Luke, who will each
complete their service on our Board of Directors in April.
MWV has the foundation in place to be the complete
package  a foundation built on insights that lead to
smart choices, innovative solutions that lead to better
performance in the market, and a determination to
execute that will lead to earnings and cash flow
improvement this year and for many more years to
come. That will be our mark of success, and I thank
everyone associated with this meaningful endeavor.
Sincerely,
John A. Luke, Jr.
Chairman and Chief Executive Officer
February 24, 2014