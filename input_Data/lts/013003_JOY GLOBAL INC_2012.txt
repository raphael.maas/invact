Dear Shareholders,

I am pleased to report that fiscal 2012 was another
year of exceptional performance and significant
accomplishments. In fact, 2012 was a record year for
revenues and earnings. These results include LeTourneau
and International Mining Machinery (IMM) acquisitions,
both of which were accretive after acquisition costs and
excess first-year purchase accounting charges. We also
made progress on the implementation of our strategic
imperatives, and we continued to improve our operational
efficiencies during the year.
At the same time that we delivered record results,
we also continued to invest in the future. In fiscal 2012

we invested to take advantage of high growth
opportunities that will come, both directly and via
seaborne supply, from China, India and other emerging
markets. We ended the year with our third major
factory in the Tianjin, China region well underway. This
factory will produce equipment to our global standards,
and will increase the range of machinery delivered
to China and reduce our response time to other key
global markets. In 2012 we also acquired IMM, the
leading Chinese manufacturer of underground mining
equipment, to give us access to the mid-tier domestic
market. IMM serves the largest segment of the high
growth China coal market, and will additionally benefit
from the continued efforts to mechanize and improve
the safety of these mines. Although 2012 created some
headwinds for IMM, the competitive advantage that we
are creating though technology transfers will put IMM
back on track in 2013.
We also continued to invest in new products and
technologies that can create major new revenue
streams. We are developing two new shovels that will
unlock significant value for our customers. At 135 tons
per pass, the Magnum shovel will achieve the optimum

of three-pass loading 400 ton haul trucks, significantly
increasing fleet productivity. At the other end of the
product range, the hybrid diesel-electric shovel will
increase our access to non-electrified mines, and
will give our customers the reliability of an electric
shovel with the mobility of a hydraulic excavator. On
the underground side, we are developing technology
that is proving its ability to dramatically improve the
productivity and safety of metal mines by mechanically
cutting the hardest underground reefs. These long term
development programs will add value by enabling us to
grow faster than the markets we serve.
The year 2012 was one in which we balanced current
year performance with investments that can add
long-term value to Joy Global. But fiscal 2012 was also
a transitional year, as slowing markets took us from
growing to working off backlog.
The current conditions represent a market correction
in the long term growth of commodities, as emerging
markets continue on their path of industrialization.

China and India have 35 percent of the worlds population
and yet their value of Gross Domestic Product per capita
is only a fraction of that in developed economies. In
addition, 20 percent of the worlds population lacks
access to electricity. This is consistent with our view
that we are still in the early stages of a long-term
secular growth cycle in mined commodities, and
we are positioning Joy Global accordingly. Although
our customers dialed back their expansion plans in
2012, they continue to make investments based on
this positive long-term outlook. Their investments
are currently focused on lowering the unit cost of
production. As a result, they continue to invest in new
technologies that increase productivity, improve safety
and reduce manning levels. They are also investing
in new mine expansions that will come on line with
unit costs low on the marginal cost curve. These
investment criteria are well suited to Joy Globals line-up
of machinery that leads the industry in reliability and
productivity, and our range of new mining systems that
reduce manning levels and improve safety.
In the current soft market, there is an imperative to
reduce cost, which we see as an opportunity. This is
an opportunity to further streamline our operations
and permanently lower our cost base so we can deliver
industry leading results over a range of outcomes.
At the same time, this will increase our leverage as
higher growth returns.
We will also use this opportunity to build on our
strengths to ensure that efficiencies deliver sustainable
value. Our core strength is in our direct to the end-user
business model that is most effective in supporting
the mission-critical equipment we manufacture. We
continue to strengthen our aftermarket services by
investing in new and upgraded service centers that
better serve key and emerging mining markets, and in
developing technologies that improve the reliability and
productivity of our machinery. In 2012, we opened
new service centers in Chile and in the Minnesota
iron ore range; we substantially expanded and
upgraded our service facility in Arizona; we started
construction on new service centers in Russia, India,
and the Hunter Valley of Australia; and we approved
new service centers in Peru and British Columbia,
Canada. We also continued to prove the value of
remote condition monitoring and diagnostics via our
Smart Services platform, and added new monitoring
centers to serve customers in China, Eurasia, the
U.S. and Australia.


Another strength is Operational Excellence, which
has been effective in streamlining our manufacturing
processes to reduce cycle times and lower costs.
This has been a major component of our improved
profitability in recent years, and we continue to see
opportunity with every iteration of the program in our
legacy facilities. In 2012, we expanded the applications
of Operational Excellence into the LeTourneau facility in
Longview, Texas, and in 2013 will take these programs
to the IMM facilities in China.

In 2012, we rolled out One Joy Global to bring the
strengths of two great businesses, our P&H surface
mining and Joy underground mining divisions, together
to strengthen our direct business model by having
a singular focus on the customer and by becoming
more efficient. In this effort, we are eliminating
overlapping and duplicate processes by identifying and
standardizing on internally best practices. The result
is greater efficiency and improved consistency to the
customer. We unveiled the Joy Global brand for our

operating units at MINExpo in September of 2012,
with strong positive response from our customers.
We have consistently demonstrated our focus on
cost and efficiency, and that has enabled us to deliver
results that lead the industry. All markets, and not just
commodities, have been volatile and unpredictable over
the past several years, and it is therefore important that
we are also building a business that can quickly adapt
and deliver performance over a range of outcomes.
At the same time, we will continue investing in our most
valuable assetour aftermarket servicesto help our
customers get more productivity from our equipment,

and in new products and technologies that can help us
grow faster than the markets we serve. This balanced
approach has and will continue delivering sustainable
value to our shareholders.
Michael W. Sutherlin
President and Chief Executive Officer