Dear Fellow Investors,
Throughout Marriott International�s long and successful history,
our most important and enduring principle has remained
the same: We put people first, whether it�s our associates,
our guests, our hotel owners, our shareholders, or the people
who live in the communities in which we operate. Our commitment
to this principle helped us deliver what is arguably
the best year in Marriott�s more than 90-year history.
In 2017, we delivered strong financial results, gained market
share, opened a record number of new rooms, increased our
worldwide pipeline of hotels under development, and ended
the year with more than 6,500 properties in 30 leading hotel
brands spanning 127 countries and territories. We made
significant progress on our Starwood Hotels & Resorts
integration to fully leverage our portfolio of global brands and
our combined loyalty programs, while achieving significant
cost savings. At the same time, we continued to innovate at
every level on behalf of our guests.
2017 by the Numbers
Our financial performance in 2017 was exceptional. Adjusted
diluted earnings per share totaled $4.36, a 32 percent
increase over the full-year 2016 combined results. Adjusted
earnings before interest, taxes, depreciation, and amortization
rose 8 percent to $3.2 billion.*
Worldwide systemwide revenue per available room (RevPAR)
increased 3 percent in 2017 and occupancy reached 73 percent.
Our worldwide RevPAR Index, a measure of comparable
hotel market share, increased modestly. Marriott returned
$3.5 billion to shareholders in 2017 through share repurchases
and dividends and Marriott�s average stock price rose
nearly 45 percent.
Together with owners and franchisees, Marriott opened more
than 470 hotels with over 76,000 rooms during 2017 � driving
our worldwide distribution up by more than 5 percent, net, to
nearly 1.3 million rooms.
We ended the year with a record 460,000 rooms in the
development pipeline, with over half located outside of North
America. Industry data shows that Marriott brands represented
7 percent of worldwide room supply, but an impressive
22 percent of hotels under construction.
Enrollment in our combined loyalty programs spiked after the
Starwood acquisition and continued at a rapid pace. By the end
of 2017, our combined loyalty programs had nearly 110 million
members, the largest loyalty programs in hospitality today.
Our integration of Starwood continued to provide benefits
in terms of cost savings, from combined global sales, to
common human resources and financial systems, to streamlined
procurement programs for our owners. We�ve made
great progress on realizing general and administrative cost
savings and expect to reach our goal of $250 million of
savings in 2018.
Innovating for the Future of Travel
While the outstanding results of 2017 are deeply gratifying,
our continued success requires us to innovate at every
level to meet the needs of increasingly demanding and
sophisticated travelers in a rapidly changing market with
new disruptive competitors.

Among our many efforts, Marriott is focused on building partnerships
and developing the technology to deliver immersive,
personalized experiences on a mass scale. Last year, we
invested in PlacePass, which will let our guests choose from
more than 100,000 local experiences in 800 destinations
worldwide. We launched a partnership with China�s Alibaba
aimed to provide a problem-free travel experience � from
planning to booking to activities � for the burgeoning
Chinese travel market.
We continue to innovate with initiatives created to help
elevate and personalize the guest experience. For example,
the Internet of Things (IoT) Guestroom Lab, at our corporate
headquarters, explores giving each guest a unique experience.
By using mobile and voice-enabled technology, the
guest has the ability to set up the room to best meet their
needs � whether that is creating the ultimate relaxation
environment or one that enables productivity for the business
traveler. We also launched the Travel Experience
Incubator Challenge together with Accenture and 1776, a
leading global incubator and network for startups. The Travel
Experience Incubator is designed to discover and foster
startups working on innovative technologies and solutions to
improve travel. Through the Challenge, startups developed
solutions using data and technology, with a goal of increasing
customer connectedness and enhancing travel experiences.
We�re applying this innovative spirit to our combined loyalty
programs as well. This year, in partnership with JPMorgan
Chase and American Express, we expect to offer co-branded
credit cards that will provide new benefits to loyalty members.
We are also working to unify Marriott Rewards and Starwood
Preferred Guest (SPG) into one loyalty program that will
allow our members to earn and redeem points across the
portfolio as well as earn benefits to recognize and reward
their allegiance to our brands.
We are also partnering with the World Economic Forum,
Accenture, and others in a public-private collaboration. This
ambitious project, called Known Traveler Digital Identity,
will employ biometrics and other data to create a verifiable,
unique, secure digital identity for �known� travelers. This
framework should provide a seamless, virtually hassle-free
travel experience around the world as we anticipate 1.8 billion
international trips to be taken annually by 2030.
Doing Good
Putting people first also means putting our communities and
environments first. We saw this core value at work throughout
2017 when our associates sprang into action to help
those stricken by a multitude of natural disasters.
To deepen this commitment, last year we launched our new
sustainability and social impact platform, which we call Serve
360: Doing Good In Every Direction, to guide how we make
a sustainable and positive impact wherever we do business.
Marriott has embarked on ambitious sustainability goals for
2025, including cutting our carbon emissions by 30 percent
and food waste by 50 percent. We are embedding human
rights criteria in recruitment and sourcing, and we�re on
course to be the first in our industry to have 100 percent of
hotel associates trained to spot signs of human trafficking.
In addition, we expect that our associates will contribute
15 million volunteer hours by 2025. For us, how we do business
is as important as the business we do.
Why We Win
Day in and day out, Marriott thrives by staying focused on
associates, guests, owners, and results. Our culture empowers
associates to exceed expectations and build careers. Our
30 brands offer something for everyone. We are delivering
unique experiences, leveraging design, new technologies,
exceptional customer service, and modern amenities that
speak to today�s traveler. For owners, our strong brands, efficient
systems, and best-in-class loyalty programs fuel hotel
performance, enabling our hotels to deliver great economic
returns and outperform competitors. We have longstanding
relationships with our owners, who are even more excited to
come back again and again to develop new hotels or convert
existing hotels to one of our leading brands.
But as our founder, J. Willard Marriott Sr., always said, our
success is ultimately due to the talent and efforts of the
more than 700,000 people who wear a Marriott name badge
worldwide, and who everyday work to delight our guests.
We are excited to welcome you soon.
J.W. �Bill� Marriott Jr.
Executive Chairman and Chairman of the Board
Arne M. Sorenson
President and Chief Executive Officer