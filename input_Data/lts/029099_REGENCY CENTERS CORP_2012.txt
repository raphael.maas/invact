To our fellow shareholders:
Over the past several years, Regency Centers has built upon our rock solid foundation to
sustain meaningful growth in shareholder value. We believe the steps we have taken to
enhance our portfolio, development program, and balance sheet are positioning us to
consistently perform at Regency�s high standards, which will distinguish us as a best-in-class
shopping center company.
In 2012, the hard work and focus of our dedicated team of professionals were clearly evident in
our financial results, which drive Funds from Operations (FFO), net asset value (NAV) and, in
turn, shareholder returns. I am extremely proud of what the team has accomplished. Several of
their more notable achievements include:
� Our leasing and operations team capitalized on robust demand and the improved health
of our retail customers to produce 1,800 new and renewal leases, totaling more than
five million square feet. This outstanding work pushed percent leased in the operating
portfolio to 94.6%�close enough to our goal of 95% that we are now eyeing a more
ambitious goal of 96%.
� Our asset managers are beginning to harvest higher rents from increased pricing power,
as evidenced by rent growth of 5.5%, including almost 20% on new leases. This
favorable trend results from occupancy rapidly approaching our historic levels, the ever
tightening supply of available space, and strong retailer demand to expand into the
highly desirable shopping centers that make up Regency�s portfolio.
� These factors, together with the team�s single-minded determination, drove same
property net operating income (NOI) by 4%, or almost $15 million. At today�s property
valuations, this translates into approximately $250 million of NAV, or more than $2.50
per share.
� Regency�s development group, working closely with operations, completed nine projects
that are now over 97% leased. Another $150 million of new developments were started
and are nearly 90% leased. Together with the transformation of another five centers
through redevelopment, these 17 exceptional shopping centers have resulted in $75
million in additional NAV.
� Regency�s transactions team generated more than $450 million from asset sales. The
majority of these 27 shopping centers no longer met our risk, strategic or NOI growth
profiles. With these dispositions, we reduced the nonstrategic properties currently
targeted for sale to less than 5% of the portfolio and were able to direct nearly $250
million toward the acquisition of extremely high-quality shopping centers. The portfolio
enhancement is compelling. The centers we purchased benefit from three-mile
populations that average 100,000 people and household incomes of more than 
$100,000, grocery sales exceeding $1,000 per square foot, and projected long-term NOI
growth of better than 3%, metrics vastly exceeding those of the properties we sold.
� Regency�s capital markets team used the remainder of the proceeds from the
dispositions to pay down debt and fund development. In addition, they further fortified
the balance sheet and our access to capital by issuing more than $300 million of
perpetual preferred stock at significantly lower rates than the preferred that was
redeemed and by expanding our line of credit to $800 million. With only $70 million
outstanding at the end of the year, Regency had $730 million of capacity on our bank
facilities and manageable amounts of debt maturing during the next several years. We
also mitigated the interest rate risk on 60% of the unsecured debt maturing in 2014 and
2015 by locking in Treasury rates that were approaching historical lows.
These accomplishments would not have been possible without the incredible efforts of
Regency�s exceptional people, working seamlessly across functional lines. From inside the tent,
we have seen the benefits from the dedication of the team building for some time now. It is
gratifying that these improving trends are starting to clearly translate into the two most critical
financial results:
� Core Funds from Operations (CFFO) totaled $2.56 per share, up 6.7%. The CFFO growth
rate is a good proxy for increasing NAV, and would have been even higher had it not
been for the dilutive impact of our asset sales, which exceeded our acquisitions by a
significant amount.
� Most important, total shareholder return increased by more than 30%, meaningfully
above the average of our peers.
A Sharpened Strategy to Build Shareholder Value
To be sure, we recognize that a single year�s positive results do not define Regency�s standard
for success. This is particularly the case given our view that economic growth and consumer
spending, while positive, will be slow for the foreseeable future. We also are keenly mindful of
how powerful competitive and structural forces, as well as the Internet, are reshaping the
grocery industry and key retail categories.
2013 will be Regency Centers� 50th year in business, and over the past half century, our focus
has been�and today remains�on consistent, superior performance over the long term. We
have learned valuable lessons from boom and bust business cycles and a constant evolution of
our tenant base. Our experience over the past five decades has molded and sharpened our
strategy for building on our core competencies and growing shareholder value.
Reliable NOI Growth: The �Holy Grail�
Our strategy starts with an intense focus on producing reliable growth in NOI, Regency�s �Holy
Grail� and the cornerstone of consistent increases in earnings and NAV. Our experience shows
that community and neighborhood shopping centers in infill trade areas with supply constraints
and substantial buying power, and anchored by highly productive grocers, will benefit from
sustainable competitive advantages. This compelling combination attracts the best national,
regional and local retailers and restaurants, and translates into occupancy and pricing power.
Net operating income is further fortified by distinguishing the appearance of Regency�s
shopping centers through timely maintenance and well-conceived renovations. Finally, NOI is
strengthened through superior tenant and shopper experiences, created through diligent asset
management and the effective use of technology.
The reliability of rental revenues will be further enhanced as we continue to add to our portfolio
through new acquisitions and developments that share the attributes discussed above and are
comparable to the exceptional centers in which we invested in 2012. At the same time we are
playing offense, we are also extremely vigilant and proactive about identifying and selling those
ever-diminishing number of centers that are no longer �in the fairway.�
Development: A Core Value-Creating Competency
Our ability to create value through disciplined development and redevelopment of exceptional
shopping centers is one of our core competencies. While we substantially reduced the size of
our development infrastructure in response to the �Great Recession,� we took the view that
retailer demand for prime space would eventually return, and we maintained ample capabilities
for a right-sized development program.
Our successful developments share the same critical ingredients that characterize the high
quality, infill shopping centers found in our operating portfolio. We embrace opportunities that
are difficult to entitle and assemble�sometimes taking years. These are the centers that play
to our strengths and are most resistant to future competition. In essence, we are seeking out
the very best locations that will attract highly productive traditional and specialty grocers and
top-notch side shop retailers and restaurants. Developing special locations with great anchors is
the reason that the $275 million of developments started since the recession are more than
90% leased and are contributing an estimated $125 million to NAV.
A Balance Sheet for All Seasons
Although capital is plentiful and at rates that are incredibly low by any standard, the deep
economic downturn that began in 2008, and lasted well into 2010, was a poignant reminder of
how quickly the financial markets can become real ugly. We continue to manage Regency�s
balance sheet to be prepared to weather a bad financial storm and to profit during normal
conditions. Specifically, this means we will continue to rigorously monitor our commitments,
maintaining substantial uncommitted capacity on our $800 million line of credit. We will also 
opportunistically improve our financial ratios through the astute sale of assets and equity on a
basis accretive to NAV.
Regency Centers� Brand: Our People
Since my parents, Joan Newton and Martin Stein, founded Regency in 1963, our people have
been our most fundamental asset. We believe they�re the best in the business, a team forged
by skill, experience and creative energy, working together to provide exceptional service to our
customers, to connect to our communities, and to create value for our investors. In essence,
our people are Regency�s brand and, together with our special culture, distinguish us from our
peers.
This culture was epitomized by Bruce Johnson, who retired at the end of 2012. During the 33
years Bruce has been my wise partner and friend, including 20 as Regency�s consummate chief
financial officer, he not only played a critical role in all of our important milestones, he also
personified our core values. As Mr. Perspective, he always exemplified the perfect mixture of
family, relationships, doing what is right, community service, industry leadership, conservative
financial management, and performance. Bruce was a true leader in making Regency an
exceptional company, and we will be forever appreciative of his huge contributions.
A Vision and Commitment to Sustain Excellence
As I look back on the past several years, it is clear to me that Regency had the resiliency,
capabilities, and most important of all, the dedication of our people, to emerge from the
downturn as a stronger and more focused company. We know what it takes to be a blue chip
company that year in and year out produces superior results and builds shareholder value. That
is the vision Regency�s talented team is committed to realizing each and every day.
On behalf of our entire management team, particularly Brian Smith, our president, and Lisa
Palmer, who in 2013 succeeded Bruce as chief financial officer, I�d like to thank our
shareholders for putting their faith in our strategy, our team of hardworking professionals for
their extraordinary efforts, our board of directors for their thoughtful and insightful guidance,
and our many partners, particularly our tenants and the communities in which we operate, for
their support.
Sincerely,
Martin E. �Hap� Stein, Jr.
Chairman and Chief Executive Officer