Focus. There's no more important word at Textron these days. We're focused on execution, we're 
focused on alignment and we're focused on results. 
Forward. We're moving forward, thinking forward and building momentum for the future. 
That's because we're dedicated to transforming Textron into the premier multi-industry company, 
recognized for our network of powerful brands, talented people and world-class processes. 
Our journey began three years ago when we set out to transform Textron -- fundamentally and 
comprehensively. While simultaneously weathering one of the toughest economic climates in 
recent history, we've been changing the very DNA of this company. 
These efforts helped offset a $500 million decline in 2003 sales volume, reflecting a downturn in 
the business jet and key industrial markets. Although we were not satisfied with our 2003 financial performance, the benefits of our transformation were apparent as we significantly offset the 
earnings impact from last year's sales volume drop-off through programs like Textron Six Sigma, 
Integrated Supply Chain Management, restructuring and other efforts. 
The pipeline of new products coming to market points to very strong performance in the 
second half of this decade. Our greatest need right now is to accelerate the pace of execution 
in 2004. 
So we're highly focused, yes, and moving fast forward. 

Our Strategy
We will deliver exceptional shareholder value by building world-class competencies in: 
Enterprise management -- managing our businesses to generate high returns on invested capital through a focus on customers, people and process 
Portfolio management -- identifying, selecting, acquiring and integrating the right mix of businesses that will drive higher performance while benefiting from our enterprise management 
capabilities 

The development of this strategy and its fundamental support system has been largely 
accomplished. Our job now is to execute consistently and successfully while continuing to build 
organizational capability. 
Our enterprise management strategy has three facets: 
Making customers successful 
Attracting and developing talented people 
Implementing world-class processes to enhance productivity and innovation 
Indeed, customers, people and process have become the Textron mantra. Ultimately, we know 
that our performance will be only as good as our commitment and ability to help customers succeed. And we will only help customers succeed if we have the right people and the right 
processes in place. 
We are dedicated to delivering a superior customer experience. Each Textron brand has a 
unique position and relationship with its customers, and we are committed to helping those 
brands and relationships flourish with innovative, market leading solutions and a relentless focus 
on meeting current and future customer needs. 
Creating Opportunities through New Products 
Our powerful brands, such as Bell, Cessna, EZGO, Jacobsen, Kautex, Greenlee and others, 
continue to lead their industries with technological innovation, generating new products that 
promise impressive revenue and earnings growth through the end of the decade and beyond. In 
2003 alone, we brought more than 120 new and upgraded products and services to market, creating opportunities for our customers to realize benefits unavailable elsewhere in our industries. 

Our commitment to new product development has been unwavering through the downturn 
that has affected our markets. These investments will deliver significant returns and further 
strengthen our market leadership as our markets rebound. 
Bell's groundbreaking V22 Osprey tilt-rotor program will move into the operational evaluation 
phase late in 2004. Upon program approval, the U.S. Department of Defense plans to purchase 458 aircraft, representing $19 billion in total new revenue for Bell. 
Bell continues to innovate and leverage this remarkable technology, developing a commercial 
version, the BA609, with first deliveries expected in 2008. In addition, the world's first 
unmanned tilt-rotor -- the Bell Eagle Eye -- will bring unique benefits to the U.S. Coast Guard's 
Deep-water program, with a potential value of approximately $1 billion. 
Bell has also been selected for the U.S. Government H1 upgrade program, which calls for the 
remanufacture of 100 Huey and 180 Super-Cobra helicopters. Initial deliveries on this $5.6 billion program are slated for 2006. 
Cessna's three newest jets -- the CJ3, Sovereign and Mustang -- are bringing benefits such as 
greater speed, range and lower price of entry to current and aspiring business jet owners. We 
continue to offer the broadest, most modern and technologically advanced product line in the 
industry. 
We're planning first deliveries of the CJ3 and Sovereign this year and expect to begin delivery 
of the Mustang, our new entry-level jet, in 2006. Based on the strength of our new model 
announcements, Cessna's backlog was $4.4 billion at the end of 2003, and we are well positioned to benefit as demand for business jets rebounds. 
Textron Fastening Systems has introduced the world's first Intelligent Fastening Technology 
that integrates micro processing capability into the fastener. It has the potential to redefine how 
the OEM-world thinks about and deploys fastening systems. 
Kautex has developed the world's first fuel system that meets the stringent new Partial Zero 
Emission Vehicle (PZEV) standards initially being implemented in California. Kautex has 
received the first PZEV production contracts from two major auto manufacturers for this cutting 
edge technology, with first deliveries slated for late 2004. 
Jacobsen's MagKnife turfcare system, which allows for faster changing of commercial mower 
blades, was voted best new product in the turfcare industry in Europe and is helping our customers realize impressive productivity gains. 
EZGO has launched a new off-road sport vehicle. The ST 4x4, designed for rugged landscapes, is the first major new product completely designed and developed under Textron's 
new Design for Six Sigma process. 
Building Internal Capabilities to Get Closer to Customers 
Our customer focus is evident in many ways. One example: Bell Helicopter's ninth consecutive 
year as the leader in customer support according to Pro Pilot magazine's annual reader 
survey. 
Not satisfied to rest on such laurels, however, we are constantly seeking new ways to understand 
and delight our customers. In 2003, we designed a rigorous process for measuring and tracking 
customer satisfaction that goes beyond traditional measures. This best-in-class process will 
improve how our businesses manage the customer experience throughout the relationship life 
cycle, and will help us gauge future recommendation and repurchase intent -- leading indicators 
of customer loyalty and financial performance. And it's only one of a number of efforts underway 
across Textron as we continue to strive to make our customers more successful. 
Another critical element of our enterprise management approach is to develop a workforce of 
exceptionally talented people -- high achieving, passionate and accountable. They underpin our 
ability to achieve industry leading operating performance, nurture strong customer relationships 
and drive growth into the future. We are dramatically improving the capabilities of employees at 
all levels, assessing and aligning our people and charting development plans to help them 
achieve their personal best. 
Today we are guided by newly established leadership competencies that represent the behaviors 
and skills we need to achieve our vision. These competencies and our organizational objectives 
are driven through our new Performance Management and Goal Deployment processes, 
making sure that our people are working on the things that matter most. Through these processes, employees and supervisors are held accountable to drive even higher individual and team 
performance. 
In 2003, we launched several new enterprise wide development programs focused on our middle 
managers and supervisors, expanding access to more than 4,000 employees who can benefit 
from new leadership and managerial programs. We also enhanced our Management Assessment 
Process to help assure we have the right leaders in the right positions, and we continue to support 
their ongoing development with world class custom leadership training programs. 
Textron Six Sigma is also providing a meaningful talent development opportunity -- to date, helping more than 6,700 employees expand their skills through Black Belt, Green Belt or other 
Textron Six Sigma related training. 
Our third focus within enterprise management is to develop world-class processes that will drive 
long-term productivity and profitability. Step by step, we are reengineering every core process 
to achieve industry leading performance. 
Textron Six Sigma 
Textron Six Sigma is a critical enabler of our business process improvement effort. It is a disciplined, data driven approach to eliminating waste, reducing variation and driving growth. 

Just two years into the implementation of Textron Six Sigma, we have: 
Trained 500 Black Belts and more than 1,300 Green Belts 
Implemented Design for Six Sigma and New Product & Service Introduction standards under 
which new products and services are being brought to market 
Generated significant returns on our initial investment 
The benefits of Textron Six Sigma are being realized in many ways. Case in point: Bell Helicopter 
is using a new online system to recoup $1.5 million annually in previously unclaimed warranty 
costs from suppliers. And, Jacobsen and Textron Financial created an integrated, automated 
shipping approval process for Jacobsen products, expediting shipping, financing and collections and saving more than $2 million in the first year. 
Supply Chain Management 
We have also undertaken a comprehensive Integrated Supply Chain initiative to gain competitive 
advantage, beginning with areas such as strategic sourcing. We continue to gain momentum 
and build capability supporting this critical long-term initiative. In 2003, we expanded our global 
sourcing offices in Poland and China, and are planning to open an office in Mexico this year. 
Strategic sourcing not only drives savings, but also provides opportunities for future growth. 
Once dependent on competitors for global windshield washer pump motors, Kautex identified 
high quality Chinese suppliers that are now building its proprietary motor design. The move, 
which enabled Kautex to further expand its product portfolio, is expected to drive additional revenue growth in the coming years. 
Shared Services 
We are creating internal shared service organizations to address business needs for a fraction of 
the cost and with greater speed and efficiency than a traditional decentralized model. In 2003, 
we continued to build our IT shared service organization and introduced similar models in 

Finance and Human Resources. Within our IT initiative, which is furthest along, here's what we 
achieved in 2003: 
Saved $34 million through infrastructure consolidation, IT procurement leverage and head 
count reductions 
Connected all Textron IT network environments, enabling us to share information systems and 
applications to drive business results 
Created several Centers of Excellence to support enterprise wide applications such as 
PeopleSoft , SAP and product data management solutions 
As a multi-industry company, our ability to maintain the right mix of businesses is critical to success. Whether it's acquisitions or divestitures, portfolio management is an essential component 
of multi-industry leadership, and we intend to become the very best at it. 
Over time, we will enhance and reshape our portfolio by divesting noncore assets and investing 
in branded businesses in attractive industries with substantial long-term growth potential. By the 
end of the decade, Textron will have a streamlined portfolio of leading global brands, each 
generating $1 billion or more in annual revenue with returns of at least 400 basis points above 
our cost of capital. 
We are developing rigorous processes to support our portfolio management capability. We have 
also established a comprehensive set of acquisition evaluation criteria and in 2003, applied 
them to our existing portfolio and developed a strategic investment plan for each business. 
In 2003, we divested our Omni-Quip business, noncore portfolios from Textron Financial, our 
interest in an Italian automotive joint venture and a unit of Textron Fastening Systems. Over the 
past three years, we have divested approximately $2.3 billion of revenues in noncore businesses, allowing us to allocate capital to our stronger, faster growing businesses. 
Staying the Course
We have maintained a steady focus on transforming our company by emphasizing successful 
customers, talented people, world-class processes and portfolio management. We are also 
staying the course with respect to our restructuring program and financial goals. 
We've just finished the third year of our restructuring program, again ahead of plan. By the pro 
gram's conclusion at the end of 2004, we expect to realize ongoing savings of $480 million per 
year. 
Return on Invested Capital (ROIC) remains our financial compass, and we also continue to be 
highly focused on cash generation and maintaining a strong balance sheet. In addition, we 
remain dedicated to achieving our key 2006 financial targets: 
Organic revenue growth averaging 3  5 percent per year 
Earnings-per-share growth averaging at least 10 percent per year 
ROIC of at least 400 basis points higher than our weighted average cost of capital 
We still have much work to do, but we remain determined never to lose sight of our vision. In this 
spirit of optimism tempered by realism, we are making progress and moving forward every day. 
I witnessed this momentum as I traveled to Textron facilities around the world in 2003. I met with 
many managers and employees who, like Textron itself, are focused and moving forward. They 
are a source of pride and inspiration for us all, and I want to thank them for their dedication and 
contribution to Textron's success. 
I also want to acknowledge and thank four distinguished Textron directors -- Teresa Beck, Stuart 
Dickson, John Macomber and Sam Segnar -- who are retiring after many years of service to our 
company. In turn, I am very pleased to welcome three new members to our Board: Kathleen 
Bader, Chairman, President and CEO of Cargill Dow LLC; Kerry Clark, Vice Chairman of The 
Procter & Gamble Company; and Ivor Evans, Vice Chairman of Union Pacific Corporation. These 
leaders will provide invaluable perspective and guidance as we continue to move down the path 
to premier status. 
2003 was a year in which our Global Leadership Team locked arms and demonstrated remark 
able resolve to make the tough decisions and hold themselves and their people to even higher 
standards of performance amid a challenging market environment. And our Management Committee has never been stronger, driving teamwork, discipline, accountability and performance at 
all levels of the organization. We have an exceptional leadership team that is inspired and driven 
to lead Textron into the future. 
Three years ago, we said we were going to do many things to revitalize Textron. We're doing 
them and are starting to see results. We crafted a vision for multi-industry leadership and 
aligned our organization to deliver on it. We developed and implemented enterprise management and portfolio management strategies. We're accelerating our execution, emphasizing 
successful customers, world-class processes and talented people. 
Ultimately, we know that this work will lead us to performance that our customers, investors and 
employees deserve. Today, we remain focused on the tasks at hand and resolved to keep 
moving forward. 
2004 will be another challenging year in many of our end markets, but that will neither dampen 
our enthusiasm and confidence in the future, nor will it cloud the clarity of our action plan. Sup 
ported by our transformation efforts, we will continue to focus on improved earnings and cash in 
the near term while investing in new products and services for the longer term. As the markets 
rebound, your company will be exceptionally well positioned for growth and profitability. 
Thank you for your continued support. 



