To our stockholders                                                                                               



    2009 was an important year of transition                                      or $1.36 per diluted share compared to                                            Strategic Direction
for Baker Hughes. In a difficult market, we                                       $1.64 billion or $5.30 per diluted share in                                           For the past several years, Baker Hughes
made several strategic moves to enhance our                                       2008. North America revenues declined 31%                                         has invested significantly in people, infra-
ability to compete on a global scale and fully                                    and revenues outside of North America fell                                        structure and technology. These investments
participate in the most significant opportuni-                                    9% in 2009 compared to 2008.                                                      served as the foundation for the next set of
ties in our industry over the next decade.                                             The year ended with a bit of good news.                                      strategic actions designed to help us increase
    During the year, we reorganized to be                                         Worldwide revenue increased 9% sequen-                                            market share and achieve long-term profit-
more responsive to our customers, relocating                                      tially in the fourth quarter compared to the                                      able growth. We communicated this strategy
operational leadership from our existing                                          third quarter of 2009 as activity increased                                       to our organization with the visual aid of a
offices to 32 region and geomarket offices                                        in all geographic regions.                                                        simple pyramid, as depicted on the inside
around the world. We created enterprise-                                               Net income for 2009 was impacted by                                          cover of this annual report.
wide marketing, technology and supply                                             charges of $250 million before tax ($0.55                                             In brief, increasing market share and
chain organizations to focus on key market                                        per diluted share), including $138 million                                        achieving long-term profitable growth
segments, optimize our product portfolio,                                         associated with reorganization and sever-                                         requires Baker Hughes to pursue two related
accelerate the pace of product introduction,                                      ance, $18 million in acquisition-related costs,                                   enterprise strategies: continue to build global
and improve our operational efficiency. We                                        and an increase of $94 million to our allow-                                      capabilities to serve our customers around
also reached agreement to acquire BJ Services                                     ance for doubtful accounts, as many of our                                        the world, and deploy customized local solu-
Company, a provider of pressure pumping                                           domestic and international customers strug-                                       tions to meet our customers' needs in each
and other oilfield services. BJ Services will                                     gled in the challenging economic conditions                                       geographic market.
add significant capabilities in pressure pump-                                    of 2009. Our operating profit margin for                                              We further identified three imperatives
ing and stimulation, closing a significant gap                                    the year was impacted by the lower activity                                       that are critical to the implementation of
in our technology portfolio.                                                      levels, significant price erosion, and the extra                                  these strategies. First, improve our customer
    In 2009, the world faced the worst global                                     costs we carried to ensure a smooth organi-                                       focus, so we can better understand our
recession since the Great Depression and                                          zational transition. Given the progress we                                        customers and proactively meet their needs.
demand for energy fell in step with the                                           have made on our transformation, these                                            Second, achieve operational effectiveness
decline in economic activity. Capital spend-                                      additional costs should largely be behind                                         and improve our overall cost position. Third,
ing by our customers, as measured by the                                          us as we enter 2010.                                                              optimize our existing technology portfolio
Barclays Capital Spending Survey, declined                                             During 2009, debt decreased $533 mil-                                        and fill strategic gaps, including our reser-
15% in 2009 compared to 2008. The aver-                                           lion to $1.80 billion, and cash and short-                                        voir engineering capabilities and pressure
age U.S. active rotary rig count of 1,090 in                                      term investments decreased $360 million to                                        pumping services. To implement these imper-
2009 was down 42%, from 1,879 rigs in                                             $1.60 billion as compared to 2008. Capital                                        atives, we launched a number of initiatives,
2008, and the average international rig                                           expenditures were $1.09 billion, depreciation                                     including a major reorganization and tar-
count of 997 was down 8% for the year.                                            and amortization expense was $711 million                                         geted acquisitions.
    Baker Hughes revenues of $9.66 billion                                        and dividend payments were $185 million
in 2009 were down 19% from $11.86 bil-                                            in 2009.
lion in 2008. Net income was $421 million




Improve Customer Focus:
Moving to a Geomarket Model
      Our first imperative is to improve cus-
tomer focus, and that means getting closer
to our customers. The reorganization we
                                                                                                                 
                                                                                                
                                                            
implemented in May was based on moving
from a portfolio-managed company with all
                                                                                                  
the leadership in the southern United States,                     

to a geomarket organization with Eastern
                                                                                                                     
and Western Hemispheres, nine presidents in                                                                                       
regions around the world, and 23 geomarket
managing directors on the ground in all the                                  
major markets.
      The geographic organization improves
customer focus in a number of ways. By                                               
locating senior management within the geo-
markets, decision-making authority is closer
to our customers and decisions can be made            This more-focused market approach is                  We formed our Global Supply Chain and
at "customer speed" By organizing the prod-
                       .                          giving us the granular view we need to                    Manufacturing organization in 2009 so that
uct line field operations into geomarket oper-    establish the right expert teams, drive the               we could achieve synergies from combining
ating units, we have improved our ability         right market-based technology needs, and                  our product line manufacturing capabilities.
to deliver multi-product solutions specifically   develop the right infrastructure in the right             The new organization leverages our global
tailored to meet local requirements.              places around the world.                                  footprint of manufacturing facilities, suppliers
      Over the last three years, we made a                                                                  and logistics expertise. Optimizing our supply
determined and successful effort to hire          Achieve Operational Excellence:                           chain will be an important area of focus in
local talent to constitute the largest share      Improving Supply Chain and                                2010 and beyond.
of our regional management, and we are            Administrative Functions                                     As part of our cost reduction efforts,
developing other employees so they can                With operations in more than 90 coun-                 we are also targeting $50 million in annual
fill leadership roles at the geomarket,           tries, we know that managing our supply                   savings through outsourcing specific shared
region and hemisphere levels. In addition,        chain effectively is one of the single largest            services functions in 2010, and we are
we have placed nationals in executive             opportunities for improving execution and                 making improvements in our information
roles within each of the regions and have         reducing costs; in fact, we estimate that                 technology infrastructure.
attracted senior executives from other            we can save as much as $300 million over
companies, including companies outside            several years through implementation of
of the oilfield services industry, to fill        enterprise supply chain strategies.
some of these positions.



                         In a difficult market, we made several strategic moves to enhance our
                         ability to compete on a global scale and fully participate in the most significant

                         opportunities in our industry over the next decade.





Our research and development and technology centers,
including the Celle Technology Center (in Germany)
and Center for Technology and Innovation (in Houston),
play a vital role in developing the market leading
technologies necessary to meet future needs of
our customers.




                                                         Optimize the Product Portfolio:                      We also took bold steps in 2008 and
                                                         Promoting Market-Driven Solutions                2009 to cultivate a strong reservoir capability
                                                             In restructuring the company, we did         within Baker Hughes, acquiring Gaffney,
                                                         not want to lose sight of the many benefits      Cline and Associates; GeoMechanics Interna-
                                                         of focusing on products. We elected to           tional; Helix (now Baker) RDS; and Epic
                                                         maintain product line teams responsible          Consulting to form the Reservoir Technology
                                                         for research and development and for effi-       Consulting (RTC) group. Today, RTC boasts
                                                         ciently delivering products and services to      nearly 400 seasoned industry professionals
                                                         the geomarkets.                                  with backgrounds in petrophysics, geology,
                                                             Our Products and Technology organization     geomechanics and geophysics, and offers
                                                         is comprised of three product centers which      a range of expertise from field evaluation
                                                         are grouped to enable technical synergies        to completion and production. This team's
                                                         and the development of broad, multi-product      global activities connect directly to Baker
                                                         line customer solutions. The Drilling & Evalu-   Hughes' services for oil and gas wells,
                                                         ation product center focuses on drill bits,      and present significant opportunities to
                                                         drilling systems, and logging-while-drilling     increase business.
                                                         and wireline products and services for forma-        The Products and Technology team is also
                                                         tion evaluation. The Completion & Produc-        responsible for our technology and product
                                                         tion product center develops well completion     centers, our global marketing functions, and
                                                         technologies and artificial lift systems, and    our product and service reliability initiatives.
                                                         the Fluids & Chemicals product center deliv-     Our technology and product centers com-
                                                         ers drilling fluids and production chemicals.    prise our global technology network, linking




  Research                                                                                                 Reliability
                                                          Innovation


                              Our technology and product centers comprise our global technology network,
                              linking central research and development with commercial applications.
                                                                                                                                   



                                                                                                   Our new geographical model gives Baker Hughes
                                                                                                   tremendous opportunities to drive standardized
                                                                                                   and rationalized processes and to improve our
                                                                                                   operational efficiencies in supply chain and
                                                                                                   manufacturing activities.




central research and development with com-       on such projects in the past. With the com-       Opportunities and Outlook
mercial applications. In 2010, we will begin     bination of Baker Hughes and BJ Services,             Looking forward, Baker Hughes will
construction of two regional technology cen-     we can compete on an equal footing with           focus on a number of areas which provide
ters located in Brazil and Saudi Arabia. These   companies that already have this capability.      opportunities for long-term growth, includ-
centers will focus on commercialization of       Pressure pumping is also a key service in         ing relationships with national oil compa-
technical solutions which span our portfolio     deepwater projects, which require fracturing,     nies, development of unconventional gas
and address the specific challenges faced        cementing and gravel packing services, and        reservoirs, and deepwater exploration
by our customers in Latin America and the        combining capabilities with BJ Services will      and production.
Middle East. The global marketing team           make Baker Hughes a leader in this area.              National oil companies control more than
bridges the geographic and product line              BJ Services also is a leader in shale frac-   eighty percent of the world's hydrocarbon
organizations to develop and deliver market      turing technology and services in North           reserves and each national oil company has
strategies and the reliability team is respon-   America, and has operating bases in most of       unique requirements. Our geomarket organ-
sible for quality in manufacturing and for       the key shale basins. Significant opportunities   ization allows us to tailor our product and
reliable execution in the field.                 exist for Baker Hughes and BJ Services to         technology capabilities and local content
                                                 integrate their respective technologies.          programs to meet each national oil com-
BJ Services Merger                                                                                 pany's needs.
    In addition to building our reservoir cap-   Compliance                                            Within North America, the unconven-
abilities throughout 2009, we announced a            Over the past several years, our employ-      tional gas fields present the greatest oppor-
proposed merger with BJ Services Company.        ees have undertaken a broad range of com-         tunity for oilfield service companies. With the
This transaction will bring pressure pumping     pliance initiatives that focus on our Core        pending addition of BJ Services, Baker Hughes
and stimulation capabilities to the company,     Values of Integrity, Teamwork, Performance        can compete with a full breadth of products
in addition to a number of other comple-         and Learning. These initiatives are at the        and services for each of these opportunities.
mentary technologies.                            heart of how we work every day, and I                 In 2009, in anticipation of a burgeoning
    BJ Services' capabilities in cementing,      commend and thank all employees for their         deepwater exploration and production mar-
coiled tubing, hydraulic fracturing and off-     efforts in this regard.                           ket, we formed a geomarket organization
shore pressure pumping will advance Baker            While compliance remains every employ-        specifically focused on Gulf of Mexico cus-
Hughes' competitive position in all of our       ee's responsibility, we have established a        tomers. In addition, we increased our pres-
strategic market segments, particularly in       professional Ethics and Compliance group          ence in Brazil by investing in new facilities,
unconventional gas and deepwater projects,       within our legal department to guide and          adding to our local workforce and signing
which have the highest potential for growth      implement our compliance program under            a technology agreement with Petrobras.
in the next decade.                              the direction of the Audit & Ethics commit-       We also expanded our operations in West
    Pressure pumping is an important com-        tee of the Board of Directors, our General        Africa, adding new facilities in Angola and
ponent of many integrated operations proj-       Counsel and our Chief Compliance Officer.         Nigeria. We are well positioned to increase
ects, and we have partnered with BJ Services                                                       our share of products, technology and ser-
                                                                                                   vices for this critical market.




                        By locating senior management within the geomarkets, decision-making
                        authority is closer to our customers and decisions can be made at

                        "customer speed"
                                       .





Baker Hughes is committed to hiring local talent
within our regions, and to developing that talent
to fill leadership roles at the geomarket, region and
hemisphere levels.




                                                                             Looking forward, gas-directed drilling                  India and the Middle East  combined with
                                                                         in North America is gradually increasing,                   modest spare production capacity, are sup-
                                                                         and we believe this trend will likely con-                  porting higher oil prices and laying the
                                                                         tinue through 2010. However, strength of                    foundation for increased spending in 2010.
                                                                         the unconventional gas business is depen-                       In the long term, significantly higher
                                                                         dent on continued improvement in supply                     exploration and development activity will be
                                                                         and demand fundamentals and natural                         needed to offset production declines and
                                                                         gas prices.                                                 ultimately grow energy supplies. As the
                                                                             Internationally, we believe that customer               worldwide economy recovers, demand for
                                                                         spending reached its low point in the third                 our technologies and services will increase.
                                                                         quarter of 2009. Forecasts for increasing                   With our new customer-focused organiza-
                                                                         economic growth  particularly in China,                    tion and our strategic actions, which include
                                                                                                                                     the planned merger with BJ Services, we
                                                                                                                                     believe that we are emerging from this cycle
                                                                                                                                     a stronger global competitor.
                                                                                                                                         In closing, I thank our stockholders and
                                                                                                                                     customers for their confidence in Baker
            Horn River
                                                                                                                                     Hughes as we took bold steps to transform
                                                                                                                                     our company in 2009.
                                                                                                                                         I also recognize the contributions of our
                                                                                                                                     employees, who have worked hard to serve
                                              
                                                                                                                                     our customers while adopting a new organi-
                                                                                                                                     zation and business model that will enable
                                    
                                                                                                                                     long-term growth and profitability.
                                                                                Sincerely,
                                 
                                                                                                                                     Chad C. Deaton
                                                                     
                                   
      
                                                                                                                                     Chairman, President and CEO
                                   




                                   With our new customer-focused organization and our strategic actions, which
                                   include the planned merger with BJ Services, we believe that we are emerging

                                   from this cycle a stronger global competitor.
