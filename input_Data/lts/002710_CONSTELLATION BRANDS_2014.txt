FELLOW SHAREHOLDERS
If ever there was a game changing year for
Constellation Brands, it was fiscal 2014. We completed
the most transformational acquisition in the history
of our company, gaining ownership of the Crown
Imports beer business while achieving solid financial
performance.
The beer deal nearly doubled the size of Constellation,
and positioned the company as the largest multicategory
supplier for beer, wine and spirits in the U.S.
Reinforcing the value of this industry-leading status is
our dollar sales growth. In fiscal 2014, we posted the
highest dollar sales growth among our key competitors
in the total beverage alcohol category.
Our fiscal 2014 financial results demonstrate the
significant contribution our beer business is making to
our overall financial profile. We had an exceptional year
posting sales, profits and free cash flow that exceeded
our expectations for both the commercial beer
business and our newly-purchased Nava brewery. The
beer business dramatically outperformed the import
category and the overall U.S. beer market, generating
remarkable net sales growth of 10%. In fact, in fiscal
2014, our beer business accounted for an astonishing
85% of total import category dollar sales growth and
gained more than 50 basis points of market share of
the U.S. beer industry.

In fiscal 2014, we seamlessly integrated the beer
commercial business and brewery operations into a
unified beer division encompassing production, sales,
marketing and distribution. Operations are running
smoothly at our brewery in Mexico, our supply chain
is operating efficiently and key performance metrics
are being achieved. And were well underway with our
brewery expansion plans, which will enable us to double
production capacity at the site; build a new brew house,
packaging area and warehouse; and complete state-ofthe-
art infrastructure improvements.
Turning to our wine and spirits business, in fiscal 2014,
we focused our efforts and investments on U.S. brandbuilding
activity to ensure the business remained healthy
and well-positioned to generate profit growth going
forward. These activities included targeted promotional
and marketing investments that enabled us to achieve
market share volume gains and above-market depletion
growth of 3.5% across our entire U.S. wine and spirits
portfolio, these efforts also contributed to growth of
nearly 6% for our collection of Focus Brands that
include Kim Crawford, Mark West, Black Box, SVEDKA
Vodka, Ruffino, Rex Goliath and Woodbridge by Robert
Mondavi, among other brands. Additionally, we also
announced our commitment to invest $20 million in
our various California wineries to improve production
efficiencies and increase capacity to meet growing
consumer demand for our California wines.
As a result of these efforts, we continue to garner
numerous awards and recognition from prominent
industry publications, particularly for our new products
and our Focus Brands. We also achieved major wine and
spirits performance milestones for the first time in fiscal
2014 - we sold more than 65 million cases of wine and
spirits; Kim Crawford hit 1 million cases in global sales;
Jackson-Triggs sold more than 1.8 million cases; and
Constellation Brands became the #1 producer and
#1 exporter of New Zealand wine by volume.
In the spirits category, SVEDKA posted double-digit
consumer takeaway trends, in addition to gaining volume
and dollar share of the vodka category. Black Velvet
grew volume 11% driven by the core Black Velvet brand,
as well as popular new flavor introductions of Toasted
Caramel and Cinnamon Rush.
Our international wine and spirits business gained
significant momentum in fiscal 2014 with the signing
of several strategic distribution agreements and
realignment of the business in Mainland Europe.
We finalized a unique and very promising strategic
agreement with VATS Liquor Group in China, a major
producer and distributor of wine and spirits for
the Chinese market. We plan to jointly develop and
exclusively market and promote the iconic Robert
Mondavi brand in China - which is the worlds number
one selling table wine. China is currently the fifth largest
wine consumption market globally, selling more than
200 million cases annually - and its a market that has
doubled in size in five short years.
We also signed a distribution agreement with Interfood
Importacao Ltda., Brazils top imported wine distributor;
as well as distribution agreements for SVEDKA with
Lotte in South Korea and Mohan Brothers in India. And,
we renewed Robert Mondavi Winery brands sponsorship
agreement for an additional two years with Asias
50 Best Restaurants.
Overall, fiscal 2014 was a monumental year for
Constellation Brands as we became a true triple threat
in beer, wine and spirits. Thanks to the talents, dedication
and incredible hard work of our employees, we have
opened the door to a new and exciting era of what I believe
has the potential to generate sustainable growth and
virtually endless opportunities.
The following pages provide a closer look at our fiscal
2014 highlights across our beer, wine and spirits
categories, as well as a review of our latest Corporate
Social Responsibility accomplishments.
We are extremely proud that our fiscal 2014 performance
delivered yet another rewarding year of extraordinary
shareholder value. We look forward to continuing the
momentum in the year ahead.
Thank you for your continued support of our company
and our mission to build brands that people love.
Cheers!
Rob Sands
President & Chief Executive Officer

