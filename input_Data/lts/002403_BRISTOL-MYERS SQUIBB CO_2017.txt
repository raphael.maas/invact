For us, it�s all about our patients.

Our mission is focused on patients. Our science is driven by patients. What we do. How we do it. Everything about Bristol-Myers Squibb is centered on our patients and their families.

In 2017, that focus led to strong performance across the company. It also led us to further evolve our operating model, making us a more focused company that is able to deliver faster and better for our patients.

SERVING OUR PATIENTS
Throughout 2017, we drove superior commercial execution, ending the year with $20.8 billion in revenues � a 7 percent increase over 2016. Our business focused on areas of unmet need where we are making a big difference.

The treatment of cancer remains a critical focus of our work. Our immuno-oncology portfolio has played a key role, establishing Opdivo as a foundational therapy with significant potential to grow even further. Now approved in nine tumor types and 15 indications,* Opdivo sales grew by 31 percent, ending the year with $4.9 billion in global sales. Further, this immuno-oncology medicine remains a leading treatment in second-line non-small cell lung cancer and in second-line renal cancer. Alone and combined with Yervoy, Opdivo is the leading medicine for first-line melanoma in the U.S.

global sales
Our novel oral anticoagulant, Eliquis, also delivered $4.9 billion in sales, representing a 46 percent increase over its 2016 revenues, as it continued to become established as the standard of care for patients with atrial fibrillation (AF) and venous thromboembolism (VTE)-related thrombosis. In fact, Eliquis became the No. 1 prescribed next-generation anticoagulant in the U.S. and in several other countries around the world. And in some markets, Eliquis has even begun to surpass warfarin, as physicians gain experience with Eliquis and the value it brings to patients with AF and VTE.

Continued growth in other key brands, namely Orencia and Sprycel, also contributed to strong performance in 2017.

Throughout 2017, we also made great progress in our pipeline, driven by the need to reach more patients with more treatments.

With respect to immuno-oncology, our increasingly broad program advanced on many fronts, underscoring the importance of our innovation-based strategy and our growing capabilities in translational medicine.

Early in 2018, we announced that our first-line lung cancer study (CheckMate�227) demonstrated that the combination of Opdivo and Yervoy delivered superior progression-free survival vs. chemotherapy in patients with high tumor mutation burden, an emerging biomarker that we have been advancing through our clinical research.

This is the third tumor type to show benefit from this combination therapy and an important validation of our efforts to deliver the right treatment to the right patient at the right time, making progress against the promise of personalized medicine.

Additionally, two Phase 3 Opdivo studies were stopped early for demonstrating superior efficacy vs. the previous standard of care, and four new indications for Opdivo were added in the U.S. alone last year � bladder cancer, colorectal cancer (MSI-H), hepatocellular carcinoma and adjuvant melanoma.

The progress of our next-wave oncology assets underscored the continued opportunity we have to harness the immune system and discover ways to extend the benefits of immuno-oncology to more patients. This includes our program for IDO, which has been investigating this important target in the tumor microenvironment and its role in treating several tumors, as well as our LAG-3 program, which is expanding to include more tumors and more patient types.

Beyond oncology, we advanced important assets in our pipeline. In fibrosis, for example, we saw progress with FGF21 for the potential treatment of non-alcoholic steatohepatitis (NASH) and we will be advancing that medicine further this year. From our immunoscience pipeline, we provided an update on TYK2 in psoriasis, stating that we have achieved proof of concept that allows us to move to the next stage of development in 2018.

The continued evolution of our operating model is enabling us to focus our resources on the most critical priorities in delivering new medicines to patients in need. We continued to increase our R&D investment, investing $4.8 billion* in R&D in 2017, a 9 percent increase over 2016.

We also prioritized business development, understanding that innovation must be sourced internally and externally to support our mission of delivering transformational medicines to patients. To that end, throughout the year, we executed more than 45 business development agreements � including a new partnership with Halozyme as well as the acquisition of IFM Therapeutics and in February of this year, a global partnership with Nektar Therapeutics� all of which added innovative immuno-oncology agents or delivery systems to our pipeline.

We maintained a disciplined approach to capital allocation, reflecting our commitment to delivering value to our shareholders. In addition to a productive year for business development, we repurchased $2.5 billion of our company�s common stock, and, for the ninth consecutive year, we increased our dividend.

We recently appointed two new Board Members � Karen Vousden, Ph.D., Senior Group Leader at the Francis Crick Institute in London and Chief Scientist of Cancer Research UK, and Jos� Baselga, M.D., Ph.D., Physician-in-Chief and Chief Medical Officer at Memorial Sloan Kettering Cancer Center. We are excited to have these two accomplished scientific leaders with deep experience in clinical research on our Board and look forward to their contributions to advancing our strategy and pipeline of transformational medicines.

SUPPORTING OUR COMMUNITIES
Throughout 2017, we continued our work in underserved communities throughout the world � building capacity and working to provide better health outcomes � and in the U.S., through Bristol-Myers Squibb�s patient support programs, we continued to provide assistance to patients who sought access to our innovative medicines.

The Bristol-Myers Squibb Foundation advanced its focus on addressing health disparities through important programs around the world. This included multi-country programs in Africa for cervical and lung cancer, with the initiation of the most comprehensive training and treatment initiatives on the continent focused on pediatric cancers and blood disorders. It included extensive programs in the U.S. for lung cancer, for removing barriers and for increasing access to specialized care for vulnerable populations and supporting the reintegration of our returning veterans and their families. And it included a range of other important initiatives around the globe � from hepatitis awareness, prevention and care in China and India to oncology nursing in Central and Eastern Europe.

Following a series of natural disasters in the Western Hemisphere, Bristol-Myers Squibb and the Bristol-Myers Squibb Foundation worked to provide relief to those affected by the widespread destruction and tragic loss of life. This was particularly true in Puerto Rico, where multiple company sites and more than 1,000 colleagues were directly impacted by Hurricane Maria. Collaborating across many parts of our company and in conjunction with the Foundation, we worked around the clock in the days following the storm and consistently and diligently in the months since to support our employees, bring our business back on line, and help many others affected by the storm. All told, the Foundation provided more than $2 million in cash and the company donated more than $10 million in much needed products in disaster relief in 2017.

And, once again, our corporate citizenship extended to the United Nations Global Compact and a series of �Go Green� sustainability initiatives throughout our company. We also received a host of important recognitions, including for the third consecutive year the U.S. Department of Environmental Protection�s �Energy Star Partner of the Year� award.

STRENGTHENING OUR CULTURE
Throughout 2017, we placed a high priority on developing a highly-engaged and diverse workforce, because our people � the 24,000 colleagues worldwide who call Bristol-Myers Squibb �home� � are our greatest competitive advantage. They drive our growth. They serve our patients. Without their incredible hard work, passion, and integrity, none of our success would be possible.

shareholders
To this end, in 2017, we maintained a constant focus on patients, primarily through our �Who Are You Working For?� initiative. Patients and their families visited our sites. They shared their stories. And many of them participated in our third annual �Global Patient Week� celebration, which occurred last fall with more than 80 employee events around the world. With the theme �Because There is More to Do,� the occasion provided a unique opportunity for our employees to interact with patients and to see firsthand the impact of the work they do each and every day, underscoring the need to keep moving forward.

Alongside this patient-focus, we maintained our energizing, engaging and inclusive culture through the continued development of our talented professionals. In 2017, this involved an increased emphasis on managers� capabilities in developing their people, and building an even more diverse and inclusive workforce � a critically important driver of employee engagement, collaboration, and overall business performance. The importance of Diversity & Inclusion in our company is clear in the growing membership in our People and Business Resource Groups (PBRG) which serve as powerful business platforms organized around different dimensions of diversity within our company. Our PBRGs collectively have 9,650 members spanning 45 countries.

Our employees value the importance of giving back, and in 2017, we launched an innovative program that encourages Bristol-Myers Squibb colleagues to volunteer outside the company to share their professional skills with non-profit organizations. Through this skills-based volunteer initiative, colleagues contribute their time and expertise to organizations of their choosing � something that benefits everyone involved.

And embedded throughout our culture is a deep commitment to integrity and uncompromising ethics. These values are central to our mission and our focus on working for patients. I am proud that all of our employees operate with a collective understanding that strong business results depend on ethical behavior and integrity. It is how we work at Bristol-Myers Squibb.

SETTING OUR SIGHTS
Looking forward, we are excited about our future.

Much of the work we did in 2017 has positioned us well for continued growth. Our financials are solid. Our portfolio and pipeline are increasingly robust and diversified. And our work to evolve our operating model has transformed our company in ways that are helping us to work smarter, faster and better.

Taken together, we are ready for the road ahead. Ready to grow our company. Ready to provide patients with even more treatment options and even more hope.

Again, that�s what we�re all about.