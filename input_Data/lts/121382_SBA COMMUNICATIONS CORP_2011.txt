to our shareholders

The year 2011 was a very exciting one for the wireless tower industry in general and SBA
in particular. During the year we witnessed the announcement of an agreement for AT&T
to acquire T-Mobile, an agreement by Verizon to acquire AWS wireless spectrum owned
by a consortium of cable companies, AT&T acquiring wireless spectrum from Qualcomm,
and numerous other acquisitions and divestitures by and among wireless carriers. Sprint
finalized its plans for Network Vision, 4G LTE service was launched by multiple carriers,
sales of smartphones and wireless tablets soared, and Google agreed to acquire Motorola
Mobility. At SBA we had a record year for revenue, adjusted EBITDA, equity free cash
flow and a number of other important financial metrics, including outperforming our own
internal forecasts. We crossed the 10,000 towers owned milestone and we expanded
into two new countries, ending the year with operations in seven different countries.
Why all the activity? Because wireless is booming, particularly wireless data. The
numbers are staggering and very positive for our future. Here are some findings from
Cisco�s annual Global Mobile Data Traffic Forecast Update, 2011-2016:
 Global mobile data traffic grew an estimated 133 percent in 2011, and is predicted
to double again in 2012.
 Global mobile data traffic in 2011 was eight times the size of the entire global
Internet in 2000.
 Average smartphone usage nearly tripled in 2011.
 Global mobile data traffic is predicted to increase 18-fold between 2011 and 2016, a
compound annual growth rate of 78 percent.
 Mobile network connection speeds are predicted to increase 9-fold by 2016.
 Two-thirds of the world�s mobile data traffic is predicted to be video
by 2016.
 The number of mobile-connected devices will exceed the world�s population
in 2012.
How will all this growth be possible? With more wireless infrastructure, particularly
towers and antenna, which is exactly what we do here at SBA.
We find ourselves in the middle of an extremely powerful surge of wireless growth.
This surge is occurring in the United States, in the countries outside the U.S. in which
we operate and in other countries around the globe. With our position as an essential
distribution point in the chain of wireless delivery and consumption, we are provided
many great opportunities to participate in this tremendous growth in wireless. As
stewards of your company, it is my responsibility and the responsibility of the rest of

the management team to seize upon these
opportunities and execute against them well
in order to maximize long-term value for
our shareholders. We continue to focus on
executing with a high level of achievement,
and we believe that we executed particularly
well in 2011.
Our business in the United States performed
very well in 2011. AT&T and Verizon were
active throughout 2011 primarily with
additions of new equipment to existing site
locations. These equipment additions were
to allow AT&T and Verizon to offer new highspeed wireless data services (3G and 4G for
AT&T, and primarily 4G for Verizon). Because
we have a large base of existing sites used
by AT&T and Verizon, we benefitted greatly
from their additions of new equipment to
existing sites. AT&T and Verizon contributed
over one-half of our incremental new leasing
revenue in 2011. While both AT&T and
Verizon made substantial progress during the
year, they both have a lot of additional work
to do to fully offer their next generation of
services on a nationwide basis. As a result,
we expect to remain very active with both
companies in 2012 in both our leasing and
services segments.
Other U.S. wireless customers were less busy
in 2011, but still busy enough to meaningfully
contribute to a strong leasing year for SBA.
Metro PCS, Leap Wireless, US Cellular and
others all contributed to our 2011 results.
T-Mobile USA started off the year very actively,
building out a number of new antenna sites to
improve coverage. In March of 2011, however,
AT&T announced its agreement to acquire
T-Mobile, and from that point forward new
site activity from T-Mobile predictably slowed
down. T-Mobile activity levels stayed subdued
through the December announcement of the
termination of the proposed acquisition. With
T-Mobile once again on an independent
path, we saw (again predictably) a flurry of
T-Mobile leasing activity late in the year, some
of which was captured in our 2011 financial
results but most of which will be recognized
in 2012. Early in 2012 T-Mobile has stayed
active, and recently announced a major 4G
new technology initiative which will involve
adding new equipment to existing sites in
much the same way as we have seen from
other carriers. We expect T-Mobile will be a
solid contributor to our leasing and services
activity in 2012.
We also spent a great deal of time in 2011 with
one of our major customers, Sprint, planning
and negotiating the terms of Sprint�s Network
Vision initiative as it applies to our U.S.
towers. Network Vision is a comprehensive
project that encompasses an equipment
exchange at every one of Sprint�s CDMA sites
nationwide, a defined period for the shutdown
of Sprint�s iDEN network, services work and
other items. The agreement we reached with
Sprint contemplates a multi-year period of
dramatically increased activity by Sprint on
our sites. We completed the agreement in
the fourth quarter. As a result, we benefitted
to some degree in our reported fourth quarter
leasing revenue, although very little of the
Network Vision project, as it applies to our
sites, was done in the fourth quarter. As
we have moved into 2012, Sprint Network
Vision activity has picked up considerably. 

We expect Sprint to stay active all through
the year, and be a material contributor to
our 2012 results.
Our international business grew very
rapidly and ahead of budget in 2011. We
started the year with 321 towers in four
countries outside the United States and
its territories. We ended the year with
1,330 towers in six countries - Canada,
Costa Rica, El Salvador, Guatemala,
Nicaragua and Panama. International
leasing revenue grew from $5.6 million
in 2010 to $20.9 million in 2011. We
are now the largest independent tower
company in Canada and the five-country
Central American region in which we
operate. The wireless environment in
each of these regions has developed and
we think will continue to develop much
like the United States. We continue to
see growing wireless use, particularly
data, increasing smartphone adoption,
improving 3G service with plans for
4G service and the need for additional
infrastructure to handle the demand
for additional wireless service. Our
international customers are adapting
well to the collocation model, just as U.S.
wireless carriers did fifteen years ago at
the beginning of the independent tower
ownership industry. Our international
customers include America Movile/Claro,
Bell Canada, Cable & Wireless, Digicel,
Wind, Millicom, Rogers, Telefonica/
Movistar and Telus. They are strong,
credit-worthy entities that provide good
diversification to our U.S. business. We
have made significant progress since
we began international operations three
years ago, and are now very capable and
comfortable in evaluating and executing
upon new international opportunities. We
are very excited about our international
business, and we expect another year of
strong international growth in 2012.
In addition to growing our business
organically through our wireless carrier
customers deploying new equipment
or adding additional equipment on our
towers and other sites, we grow our
business by increasing the number of
towers and other sites that we own or
control. Portfolio growth is our preferred
use for capital allocation. We had a big
portfolio growth year in 2011, adding
a net of 1,413 towers and growing our
tower portfolio by 16%. We built 388
new towers and acquired 1,085 towers.
Most of the new towers we built last year
were in Central America, although we did
build 116 in the United States and 40 in
Canada, which was the most we have
built in the U.S. in years and the most we
have built in Canada ever. We continue
to buy towers in a wide range of volumes
per individual transaction. We completed
many single tower transactions in 2011,
while our largest acquisition was 580
towers. We continue to apply rigid
investment criteria to our portfolio growth,
seeking to acquire or build towers in the
U.S. that we expect will produce returns 

at least 200 to 500 basis points above our
weighted average cost of capital and that
will be accretive to equity free cash flow per
share. We seek even higher returns on our
international investments. We continue to
be extremely happy with our portfolio growth
decisions, as past transactions are performing
ahead of plan. As big as 2011 was for SBA
in portfolio growth, we expect 2012 to be
even bigger given our recently announced
acquisition of over 2,300 towers in the United
States and Central America from Mobilitie,
LLC. We expect this transaction to close in
the second quarter of 2012.
Because we are optimistic about future
wireless growth and the prospects for wireless
infrastructure, we seek each year to allocate
a certain amount of capital to invest based
on the level at which we want to maintain
and manage our balance sheet. As I stated
previously, our preference for that capital
allocation is portfolio growth. Sometimes,
however, we find better relative value in our
common stock than contemporaneously
available portfolio growth opportunities. 2011
was a year where we found at times a greater
degree of relative value in repurchasing our
common stock than from portfolio growth
opportunities. We invested $225 million in
2011 to repurchase our common stock at
an average price of $38.01 per share. We
believe we repurchased our common stock at
prices well below its intrinsic value, and we
believe this allocation of capital will produce
material future value for our shareholders.
We achieved record financial results in
2011 by executing well operationally, taking
advantage of strong wireless growth in
our U.S. and international markets, and by
allocating capital wisely. We grew site leasing
revenue 15%, tower cash flow 14%, adjusted
EBITDA 15%, equity free cash flow 17%, and
equity free cash flow per share 21%. Our
balance sheet remained stable, and we ended
the year with a net debt/annualized adjusted
EBITDA ratio of 7.3x. This is approximately
the same ratio at which we ended each of
the prior two years. We ended the year in a
strong financial position, with ample liquidity
and resources to allocate material capital to
growth once again in 2012. Our common
stock appreciated approximately 5%, to end
2011 at $42.96 per share. The creation of
shareholder value, as primarily measured by
increasing equity free cash flow per share,
remains our primary goal.
We had a very good year in 2011. We are
off to an even better start in 2012. We are
expecting another year of material growth
and record financial results. We thank you,
our shareholders, for your support, and we
thank our customers and employees for their
contributions to our success. We look forward
to executing well again this year and reporting
our 2012 results.
Sincerely,
Jeffrey A. Stoops
President and Chief Executive Officer