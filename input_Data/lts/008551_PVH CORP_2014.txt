Dear Fellow Stockholders:
2014 was a year of action for PVH, which further
solidified our position as one of the largest branded
lifestyle apparel companies in the world. We followed
through with the investments that were initiated
in 2013, which centered on our people, products,
infrastructure, supply chain and distribution. Our
teams filled open positions across the organization
and focused on developing our talent, enhancing
the technologies and platforms used across PVH,
elevating our product assortments and upgrading
the in-store experience for consumers, while taking
steps to improve the efficiency of our supply chain.
We remained committed to driving market share
gains, preserving and improving operating margins,
introducing new products and creating innovative
marketing for our brands. We started to see that these
investments were taking hold in late 2014, and we
believe that these actions will be critical to driving
long-term financial growth for PVH.
From a financial perspective, PVHs 2014 results
were characterized by two distinct stories that
share one common thread. The first half of the
year was marked by continued investments in our
Calvin Klein business and an overly promotional
retail environment in North America. The second
half evidenced the initial stages of a recovery in the
Calvin Klein business, as our investments began to
yield improvements in performance, and our business
held up despite continued promotional pressures and
foreign exchange volatility, which had an impact on
our international business results, particularly in
Tommy Hilfiger. Throughout the year, macroeconomic
and geopolitical challenges existed, tempering results
in all regions. For the year, our earnings per share on
a non-GAAP basis grew 4% to $7.30*, including an
approximate $0.15 negative impact related to foreign
currency exchange rates, as our iconic global designer
lifestyle brands, Calvin Klein and Tommy Hilfiger,
further expanded their operations worldwide and
drove our performance. The worldwide consumer
appeal of Calvin Klein and Tommy Hilfiger resulted
in the continued expansion of sales, market share
and the global reach of these two brands, which in
2014 represented over 75% of our business.
While our financial results were below our initial
expectations, our diversified business model and solid
execution enabled us to manage through the volatile
environment and achieve 16%* earnings per share
growth on a non-GAAP basis in the second half of
2014. There continued to be positive momentum in
our businesses  with particular strength in Tommy
Hilfiger, Warners, Calvin Klein Underwear and North
America sportswear. We ended 2014 with our Calvin
Klein and Tommy Hilfiger businesses well positioned to
navigate through the uncertain consumer environment
and foreign exchange rate volatility expected in 2015. 
Positioning our Businesses
for Long-Term Growth
Despite the challenges to our 2014 earnings, we
continued to make strategic investments in our
businesses to strengthen our operations and enhance
the image of each of our brands in the marketplace.
We focused on consumer engagement, with a strong
emphasis on our e-commerce operations and digital
reach. We invested in our Calvin Klein and Tommy
Hilfiger websites to improve the navigation and overall
consumer shopping experience, which resulted in
double digit growth in our e-commerce sales versus
2013. We also furthered our digital marketing and
social media presence across our brands. Our
investments received many acknowledgements,
including Calvin Klein and Tommy Hilfiger both being
ranked in the top ten on L2s Digital IQ Index: Fashion
for the second year in a row.
We also focused on succession planning in 2014 to
position our businesses for their next phase of global
growth. During the second half of 2014, Steven
Shiffman was appointed as Chief Executive Officer,
Calvin Klein, replacing Tom Murry, who retired,
and Daniel Grieder was named Chief Executive
Officer, Tommy Hilfiger, PVH Europe, replacing Fred
Gehring, who remains with us as both Vice Chairman,
PVH Corp., and Executive Chairman, Tommy Hilfiger,
serving as a strategic partner for both Daniel and
me. Daniel and Steven have long tenures within the
businesses they now lead and we believe that they
will continue to drive the global growth of these
businesses and position them for long-term success.
Our February 2013 acquisition of Warnaco
significantly expanded our directly owned operations
for the Calvin Klein brand and, since then, we
have taken steps to unify our brand messaging for
a more consistent consumer experience across
regions and product lines. We upgraded the design
and quality of our products and invested in our
shop environments to better convey Calvin Kleins
premium positioning. We improved distribution,
significantly reducing our off price and club exposure and closed approximately 40 Calvin Klein stores in
Europe to focus on locations that are more brand
enhancing. We also launched 360o
 marketing
campaigns to engage consumers across the world,
particularly for Calvin Klein Underwear, which
continues to deliver strong global performance,
and Calvin Klein Jeans, which is embarking on a
turnaround in North America and Europe.
We continued the integration of the acquired Calvin
Klein businesses, including completing systems
conversions across North America and Europe, and
our Asia systems conversion effort will be completed
in Spring 2015. We also completed our sourcing
integration, which is expected to provide many
benefits, including leveraging best practices and
partners, faster speed to market capabilities and
enhanced quality of our products.
One of our largest endeavors was the initiation of
the Calvin Klein Jeans turnaround in North America
and Europe. During Fall 2014, we introduced our
updated product lines, which featured enhanced
styles, quality, fit and designs. In conjunction with
these changes, we opened approximately 225 shopin-shops in North America and approximately 60 in
Europe. Initial reads on the new product and shop
productivity and profitability were positive, particularly
on the mens side. The Calvin Klein Jeans turnaround
is ongoing and we expect continued improvement
in 2015. Product upgrades were also a highlight for
the Fall 2014 Calvin Klein Underwear collections. We
delivered new basic assortments into North America
stores that featured enhanced fabrics, waistbands
and packaging. To showcase these improvements,
we upgraded many existing shops, including Macys
Herald Square, which is the largest Calvin Klein
Underwear shop in the world, and opened new stores
in key markets in North America, Europe, Asia and
Brazil. We also continued to invest in Tommy Hilfiger, which performed well despite the macroeconomic
environment. We grew our square footage and
gained market share by enhancing our value
proposition within our two largest markets, Europe
and North America. This resulted in gross margin
expansion in Europe, as our product enhancements
were well received and we were able to limit price
promotions compared to prior seasons. The Tommy
Hilfiger lifestyle continues to resonate with consumers
worldwide, and we are capitalizing on this by driving
expansion in our growth categories, which include
mens tailored apparel and underwear, as well as
womenswear and accessories.
Across many of our Heritage Brands wholesale
businesses, including Izod, Van Heusen, Warners
and Speedo, we reinvested profits to upgrade our
shop environments with new fixtures, point of sale
marketing and signage. These investments have
started to pay dividends, as our new presentations
showcase our products more prominently. The
Core Intimates and Speedo businesses, which
were acquired in the Warnaco acquisition, are now
fully integrated into our Heritage Brands platform
and are leveraging PVHs best practices to drive
each business.
Our commitment to driving long-term financial
growth for our stockholders remains strong, and we
believe that the actions taken since the Warnaco
acquisition will pave the way for future revenue and
earnings per share growth. We have paid down over
$925 million of the debt incurred at the time of the
Warnaco acquisition, including $425 million in 2014,
despite the continued investments in our Calvin Klein
business. Accordingly, our net leverage ratio has
improved to 2.7x* and we plan to pay down at least
$425 million of debt in 2015. Calvin Klein
2014 global retail sales of Calvin Klein products
were $8.1 billion, as the brands provocative,
modern design positioning continued to resonate
with consumers around the world. Impactful
marketing campaigns remained at the core of the
Calvin Klein consumer engagement strategy, and
with greater direct control of the brand offerings,
we focused on leveraging Calvin Kleins heritage to
grow the top and bottom lines across regions and
product lines.
During 2014, our Calvin Klein revenues grew 5%* on
a constant currency basis (3% growth on a GAAP
basis). This was achieved even with the global
macroeconomic headwinds and brand-specific
actions we took, including transitioning to new
Calvin Klein Jeans product in Fall 2014, reducing
the brands off-price and unattractive account
presence and closing underperforming stores in
Europe. Sales growth was driven by strength in
sportswear, mens and womens underwear and
accessories, which performed well in all key markets.
Additionally, we enhanced our existing e-commerce
site in the U.S., launched e-commerce platforms in
Europe and Brazil and introduced a Tmall web store
in China, which drove outsized online sales growth.
By region, the North American business was strong,
achieving 5% revenue growth, driven through healthy
comparable store sales growth, market share gains
at wholesale and moderate square footage growth.
Calvin Klein also posted overall sales gains in Asia,
where the brand is positioned well and consumers
appreciate its aspirational lifestyle. This helped offset
weakness in Calvin Kleins European operations,
which underwent a restructuring beginning in 2013,
as management took steps to improve jeanswear
performance by upgrading product design and
quality, improving distribution and enhancing shop
environments. The Calvin Klein repositioning is in its
early stages in Europe, and with 2014 as a year of
investment and rebranding, we believe that Calvin
Kleins performance will begin to improve throughout
Europe in 2015 and beyond.
We are optimistic about Calvin Kleins future.
The brand position is strong, and we believe that
featuring Justin Bieber in the new Calvin Klein Jeans
and Calvin Klein Underwear Spring 2015 marketing
campaigns will engage consumers, energize the
brands and drive business. At the same time, we
believe that our investments in Calvin Klein will
strengthen our worldwide operations and allow us to
grow our global footprint across the retail, wholesale,
e-commerce and licensing channels. As we look
out further, we will look to operate more businesses
directly, which we believe would be additive to the
businesss overall growth profile. As we invest in the
brand, we believe that Calvin Klein will achieve strong
top and bottom line underlying growth in the future.Tommy Hilfiger
Tommy Hilfigers preppy with a twist design aesthetic
continued to appeal globally, resulting in 2014 global
retail sales of $6.7 billion. During the year, our teams
focused on elevating Tommy Hilfigers positioning
through upgraded product and presentations and
enhancing its presence online through digital and
e-commerce investments. We focused on expanding
in the emerging markets of Asia and Latin America,
where the brands largest growth opportunities
are. Turning around Tommy Hilfigers Japanese
operations was another effort, and we took steps to
reposition this business by upgrading the product
to align more closely with Tommy Hilfigers premium
position globally and closing TOMMY, an unprofitable
and brand inconsistent youth-oriented business.
While still early, we are starting to see positive
signs from our investments and we look forward to
seeing further improvement in the underlying business
in 2015 and beyond.
Reported revenues for Tommy Hilfiger increased 6%*
on a constant currency basis, or 4% on a GAAP basis.
Growth was driven by Tommy Hilfigers two largest
markets, Europe and North America, as our teams
delivered on the business strategies, maintained
brand integrity and stayed true to Tommy Hilfigers
premium positioning despite the volatile consumer
environment, difficult multi-year comparisons and
an aggressively promotional retail backdrop in these
regions. In particular, North America had a record
year, with revenues growing 6% and operating
margins on a non-GAAP basis reaching record levels.
International performance, which primarily includes
Europe, remained strong, with revenues growing
5%* on a constant currency basis (3% on a GAAP
basis) and operating margins on a non-GAAP basis
expanding 50 basis points*, as our investments to
enhance our products and the in-store experience
drove results.
We see a healthy path ahead for the Tommy Hilfiger
brand. The keys to driving the brands future global
growth include further elevation of our products,
investing in the in-store experience and communicating
with consumers in new and creative ways. We believe
that bringing on Rafael Nadal as the brand ambassador
for Tommy Hilfiger Tailored and Tommy Hilfiger underwear
will further engage our existing consumer base and
attract new consumers. Additionally, our investments
in digital marketing and e-commerce and our expanded
presence on social media are positioning us to interact
with consumers in a more targeted manner.
Heritage Brands
Our Heritage Brands business experienced mixed
results during 2014, as healthy performance in the
wholesale sportswear, Warners and Speedo divisions
balanced poor performance within the retail and dress
furnishings divisions. Even with these significant
challenges, revenues for the division decreased only
1% compared to 2013.1
The wholesale sportswear division exhibited solid
performance during 2014 and is positioned for further
expansion. We continued to interact with and engage
consumers through our shop environments in the
department store channel and these investments
have significantly improved the shopping experience.
We were pleased that our two largest sportswear
brands, Van Heusen and IZOD, continued to perform
strongly within our key department store partners,
and the healthy launch of IZOD at Kohls in Fall 2014
should bode well for 2015.
Warners and Speedo also posted solid results.
Across these divisions, we invested in our shop
environments, with a particular focus on Speedo within
sporting goods retailers and Warners at Kohls. This
proved to be a successful strategy, as consumers were
able to view our products and understand their features
more easily, resulting in improved conversion rates.
The dress furnishings division underperformed this
year, as we did not move quickly to adapt to the
changing consumer preferences toward fashion dress
shirts compared to our basic-oriented offerings.
This led to markdowns of our excess basic products
and overall poor performance. We did, however,
maintain our leading position in dress shirts in U.S.
department and chain stores and our neckwear and
underwear businesses continued to perform and
gain share. In 2015, we will focus on better inventory
management, as well as innovation and design, as
we seek to differentiate our products, improve our
dress shirt business and drive healthier performance
across the division.

Our Heritage Brands Retail division continued to post
disappointing performance, which led to our January
2015 decision to close all of our IZOD retail stores,
as well as continue to further prune underperforming
Van Heusen locations. This decision allows us to
focus on our healthy wholesale businesses. PVH and Our Corporate
Social Responsibility Commitment
Corporate social responsibility (CSR) underpins how
we operate and engage with all of our stakeholders
 from business partners to factory workers and
consumers. Our steadfast belief in doing the right
thing has been part of PVHs core values, even as
we have experienced rapid growth over the last
decade. As an industry leader, we recognize the
great responsibility and opportunity to make positive
impacts  from source to store  by striving to ensure
safe work environments, preserving the environment,
empowering people and supporting communities in
which we work and live.
CSR at PVH advanced in 2014 as we evolved and
introduced new programs, policies and resources to
enhance partnerships with key stakeholders, such as our
suppliers. We built upon our 20-plus year commitment
to human rights by enhancing our CSR monitoring
program to better address factory fire and structural
safety, starting in Bangladesh. We began to formalize
our remediation and capacity building efforts, with a
focus on developing long-term solutions to endemic
issues. The roll-out of our Chemicals Commitment and
Action Plan marked another advancement to create
a more robust environmental sustainability program.
Additionally, we established a Global Community
Relations department to streamline and leverage global
strategic programming and philanthropic partnerships
through The PVH Foundation, and announced Save
the Children as our worldwide philanthropic partner.
These developments reflect initial steps to address
the significant social and environmental challenges
across our business. We believe the emphasis we
place on CSR today will position PVH for long-term
success. To be transparent and generate dialogue
with our stakeholders about these topics, we
provide updates through our annual CSR reporting
on PVHCSR.com. We believe that our dedication to
CSR across our brands and businesses is and will
continue to be a key competitive advantage as we
strive to deliver future growth and stockholder value.
Establishing a Path of Sustainable Long-Term Growth
With 2014 as a crucial year of investments
made to strengthen and grow our businesses,
we see a sustainable trajectory of long-term
financial growth for 2015 and beyond. Talent
management has been one of PVHs largest focus
areas, as we view our people as a critical asset. In this regard, we invested in talent throughout
the organization, including promoting key leaders and
filling all of the senior positions that were open across
our businesses and regions. We also launched our first
associate-facing campaign, built on a custom social
digital platform designed to connect our associates
around the world. PVH takes great pride in being a
good corporate citizen, and we believe that engaging
our associates across the value chain, along with
engaging with our key partners and investing in their
futures are the keys to driving superior results and
achieving positive stockholder returns.
We believe our businesses are well-positioned to
grow across regions and product lines, driven by our
iconic global designer lifestyle brands, Calvin Klein
and Tommy Hilfiger, although 2015 will be pressured
by the uncertain global macroeconomic environment
and foreign currency headwinds.
Within our Calvin Klein business, we believe that our
largest opportunities for sales and margin expansion
entail further expanding our presence and product
offerings across Asia and Latin America and turning
around our Calvin Klein business in Europe and
our Calvin Klein Jeans business in North America.
Likewise, we believe our Tommy Hilfiger business
is well positioned to grow in its underpenetrated
markets, such as Asia and Latin America, where
we hope to leverage the more developed Calvin
Klein platforms there, and growth opportunities
still exist in the more developed European and North
American markets. Beyond the growth opportunities
stated above, we believe that, over time, we would
benefit from operating more Calvin Klein and Tommy
Hilfiger businesses directly.
As we complete the last phases of the Warnaco
integration in 2015 and leverage the investments we
have made across our businesses in 2013 and 2014,
we expect to see significant underlying growth in
the second half of 2015 and in 2016 and beyond.
Our teams are world class, and the values that
our organization embraces  passion, integrity,
individuality, partnership and accountability  position
us to drive superior results. Despite the uncertain
macroeconomic environment and the significant
underperformance of most relevant foreign currencies
against the U.S. dollar, we continue to see strong
underlying constant currency earnings growth globally
and we look forward to delivering solid financial
returns to our stockholders.
Emanuel Chirico
Chairman and Chief Executive Officer 