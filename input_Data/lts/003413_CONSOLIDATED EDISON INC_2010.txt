To Our
In 2010, the company maintained its leadership position in the industry by delivering energy
safely and reliably in a year marked by turbulent weather. At the same time, we produced solid
returns to our shareholders in an economy still trying to recover.


While many of our customers continued to feel the effects of a weakened economy, our region
fared better than the nation. New York City has created jobs at twice the rate as the rest of the
country, with more than 50,000 people working in new private-sector positions. Commercial
vacancy rates are down, and home values are rebounding. A record-setting 48 million tourists
visited New York City in 2010. We even saw a modest increase in new electric and gas business.


Last year's weather was a study in extremes, from the heaviest snowfalls in February to the
hottest summer on record. A fierce March nor'easter caused widespread flooding. From April
through September, we saw 37 days when temperatures soared to 90 degrees or higher. In
September, tornadoes, rare in our service area, struck communities in Brooklyn and Queens,
bringing down power lines, and a year-ending blizzard dropped more than 20 inches of snow
on the region.


Our ability to endure the challenges of 2010 is a testament to EnergyNY, our three-pronged
plan that allows us to meet our customers' energy needs efficiently. First, we commit to
providing tools and programs to allow for the most efficient use of energy; second, we
invest prudently in our capital plant; and third, we do it all with a steadfast commitment to
the environment.



                               EnergyNY
                                                                                                                  
                               We offer customers a diverse portfolio of energy solutions, each with
                               incentives and benefits, to make their homes and businesses more efficient.
                               Our low- and no-cost energy audits identify ways to enhance heating,
                               cooling, and lighting systems, while cash rebates and subsidies encourage
                               users to invest in upgrades. We pay customers to turn in old, inefficient
                               refrigerators and air conditioners, and then we recycle them for free.


                               Midtown Manhattan's Crowne Plaza Hotel became the first big business to
                               receive cash, for replacing 3,300 bulbs on its Times Square billboard with
                               compact fluorescent bulbs. U-Haul netted $68,500 in rebates for retrofitting
                               all the lighting at its Brooklyn facility. We expect the work to yield U-Haul an
                               annual savings of nearly $120,000 in energy costs. After a free energy audit,
                               an auto and tire shop in Middletown, New York, replaced all of its lighting,
                               paying only 30 percent of the cost -- we paid the remaining 70 percent. A
                               pizzeria in Queens is benefiting from refrigeration and lighting upgrades. For
                               less than a $700 investment, after cash incentives, the business expects to
                               save almost $2,000 a year in energy costs. The programs are working. More
                               than 40,000 homeowners and businesses -- from as far north as Orange
                               County to Staten Island -- have taken advantage of our offerings.


                               A national leader in energy-efficiency services, Con Edison Solutions is
                               expanding its reach to federal, state, and local government markets, with
                               $70 million in projects in 2010. The breadth of Solutions' business continues
                               its steady growth as the company brings energy expertise to public school
                               districts across the country.


                               Another aspect of our efficiency work involves asking customers for help
                               during extreme conditions, such as last summer's heat. Nearly 700 large
                               commercial customers are paid to reduce their energy use on the hottest
                               days. We have given more than 20,000 homeowners and small businesses
                               free programmable thermostats for heating and cooling systems. We can
                               control the thermostats as needed to reduce demand on our grid.



Our energy-efficiency efforts have allowed the company to curb demand for
more energy, a benefit to our customers and to the environment. The
work has also allowed us to defer capital investments by nearly $200 million.




EnergyNY
                                              
                    
EnergyNY guides the investments we make in our vast electric, gas, and steam
infrastructure. The investments are vital to the company's growth and complement
our energy-efficiency work. We spend roughly $2 billion a year to maintain and
upgrade our systems to keep energy flowing efficiently and reliably.


We finished building a 40,000-square-foot substation to make service in
Queens more reliable. Expected to go on-line this year is another new
substation that connects to a new 9.5-mile underground transmission line
from Yonkers to serve customers in the Bronx and upper Manhattan. To
improve service for customers in Rockland and Orange counties, we are
building two substations and a transmission line.


The funds we are committing to develop a smart grid exemplify our strategic
investments. We will be installing state-of-the-art control and monitoring
equipment to make our electric operations more flexible and resilient. New
underground switches will be controlled remotely, and each switch will have a
sensor to transmit information about power levels back to our control centers.
We also are evaluating 1,500 smart meters and 300 personal energy monitors
in customer homes as part of our smart-grid pilot.


We continue to explore ways to bring more natural gas into the region. One
solution involves building 17,000 feet of new transmission pipeline to expand
the number of suppliers we can call on to meet our customers' needs and
help the region meet its clean energy goals.
                                                                                  
2010 HIGHLIGHTS

                                                                                            
                                                                                                           
                                                                                                           
                                                           
    two networks, the first time two networks were established
    simultaneously.
                                                                                                                             
    of overhead cable.
                                                                                   
                                                                                                                                            
                                                         
    smartphone applications to help customers manage bills and
    energy use.


                                                                                                                          
                                                                                                                                   
    expand infrastructure.
                                                                                                                                
                                                                                                                                
                                                        
    dekatherms on January 3, 2010, and the second highest
    monthly delivery record of 34 million dekatherms in
    December 2010.


                                                                                                                                
                                                                                                                                         
                                                                                                                                   
    detection devices and new pumps.
                                                                                                                                                     
    and 400 Fifth Avenue.
                                                                                                                                               
    customers to earn LEED recognition.
                                                                                                                                                  
    so customers can achieve ENERGY STAR certification.


                                                                                      
                                                                                                                    
                                                        
                                                                                                                                   
    pipeline to improve service to Orange County.
                                                                                                                                      
                                                                                                                                      
    growth in service area.
                                                                                               
                                                                                                           
                                                                                                                                            
    gas-equipment rebate program.



COMPETITIVE ENERGY

                                                                 EnergyNY
BUSINESSES

                    
                                                                                                                                  
                                         


                                                                                     
       electricity, 25 percent more than 2009.
                                                     
       Compact in Massachusetts for the
       sixth consecutive year to supply
                                                                 EnergyNY guides our commitment to a more sustainable future.
       electricity to 150,000 customers
                                                                 A cornerstone of our efforts is providing cleaner energy choices for our
       and entered three-year agreement
       to serve Cape Cod municipalities.
                                                                 customers. Our subsidiary, Con Edison Development, broke ground on what
                                                              
                                                                 will be the largest solar farm in the Northeast. Located in southern New
    US Postal Service facilities nationwide.
                                                                 Jersey, the project will produce 15 to 20 megawatts of electric power and is
                                                              
                                                                 scheduled to go into operation in 2011.
       offer with the completion of seven
       solar installations in Massachusetts.
                                                              
                                                                 Con Edison Solutions provides competitive and cost-effective renewable
       resulting in 30,000 new customers.
                                                                 energy to regional and national customers. A major restaurant consortium,
                                                                 which includes 11 of the most popular eating establishments in New York,
                                                     
                                                                 is now buying renewable power through Solutions. In Massachusetts,
                                            
                                                                 Solutions completed seven new solar installations to offer green energy to
       transitioned to a renewable-energy
                                                                 customers on Cape Cod.
       development company with more
       than 20 megawatts of solar projects.
                                                                 We have steadily refined our own business practices to meet sound
       investment by acquiring a gas-storage
                                                                 environmental standards. Last year, the company recycled 90 percent of
       facility in New York State.
                                                                 its waste materials. Our many recycling and recovery programs include
                                                              
       year of a long-term services contract                     one where we sell or donate old electronic equipment to schools and other
       to help the Long Island Power
                                                                 organizations in need.
       Authority manage its supply assets.


                                                                 We are continually greening our offices. A major facility in Spring Valley, New
                                                                 York underwent a comprehensive energy-efficiency overhaul. The work
                                                                 included a new data center and air-conditioning system, new windows and
                                                                 siding, and a new high-efficiency building envelope. By buying energy-saving
                                                                 computers, monitors, and cooling systems, along with new server technology,
                                                                 we are reducing our energy use. We added more hybrids and compressed
                                                                 natural gas vehicles to the company's fleet. Now half of our fleet runs on
                                                                 alternative fuel.



To seal street openings, we are using Green Patch, a new paving product
made from recycled material. Unlike asphalt, the new material contains no
petroleum. In Greenwood Lake, Orange County, we are working with the
village to test energy-saving LED street lamps.


We are the leading sponsor of NYC CoolRoofs, an initiative that coated
a million square feet of rooftops, including nonprofits, city agencies, and
privately-owned buildings. We did our part by cooling roofs at 37 Con Edison
facilities. Through employee volunteers and company funding, two major
                                                                               
YMCA facilities are benefiting from cooler roofs. When the heat rises, the    
                                                                               
new coating reduces the temperatures inside the building and on the roof,
improving air quality, and extending the life of the roof and the building's
air-conditioning equipment.


In Westchester County, we are working with the Westchester Green Business
Challenge, a program that encourages companies to reduce their carbon
footprint by greening internal operations and rethinking the way they use
energy. We support Keep Rockland Beautiful, an organization committed
to creating healthier, safer, and more livable communities.


Con Edison engages customers in energy conservation and efficiency
through its Power of Green website, educational campaigns, e-mail blasts,
free smartphone applications, and a Facebook page. Newly launched energy
calculators are helping customers estimate their energy use and guide new
purchasing decisions. A new Green Team campaign informs customers about
the company's energy-efficiency programs.


All of our hard work on the environmental front has not gone unnoticed.
In Newsweek's annual rankings, Con Edison achieved the second highest
green score among utilities. Results were based on companies' policies,
environmental impact, and reputation. The Carbon Disclosure Project recently
named Con Edison first in its Leadership Index of S&P 500 companies and
first among utilities in the new Carbon Performance Leadership Index. We are
also listed on the Dow Jones Sustainability Index.


THE POWER OF GIVING

                                                  EnergyNY
                                      
Since 2003, we have supported the center's

                                                         
Electronic Waste Recycling Program -- the
longest-running e-waste recycling program
in NYC.

                                                  As the new decade begins, I remain confident in our ability to continue
We provide funding for Solar One's Green
                                                  to provide first-rate service to our customers. Our dedicated workforce
Career Job-Training program and their Green
Design Lab, a curriculum that shows 6th to
                                                  of 15,000 demonstrates the highest level of professionalism every day.
12th graders how to green their schools.
                                                  The men and women of Con Edison consistently garner top industry
                                
                                                  recognition for their innovation and ideas, and for providing the most
For the past 30 years, we have supported
The New York Botanical Garden's Greening
                                                  reliable service in the nation.
the Garden program. The garden serves as a
center for conservation and public education,
and provides 375,000 children, parents, and
                                                  Our industry will continue to change and we will work to adapt to future
teachers with quality educational resources.
                                                  energy needs. We will be prepared to give our customers the energy
                             
                                                  they need for their lifestyles and livelihoods, in the most efficient and
Our contributions to The Public Theater help
the organization make theater accessible and
                                                  cost-effective way possible. Looking forward, EnergyNY will steer our
relevant through productions of new plays and
musicals, and stagings of the classics.           planning, with a comprehensive 20-year look at electric, gas, and steam
                                                  services that will help us make prudent decisions in our operating and
In partnership with NYC, the alliance
                                                  capital spending.
operates Prospect Park, cares for the natural
environment, and offers public programs
with Con Edison's support.
                                                  We remain focused on our mission -- providing safe, reliable, and
                                   
                                                  sustainable power to our customers, while at the same time delivering
For the second year in a row, we sponsored
the Annual Reservoir Challenge -- an event        shareholder value. We have increased our dividend for 37 consecutive
that raises funds for Hackensack Riverkeeper
                                                  years, raising it in January 2011 to a $2.40 per share annual rate.
programs that protect the ecosystem of the
river's watershed.

                                                  The experiences of 2010 strengthened our resolve and helped prepare
                                                  us for the coming years so that we remain in a position to best serve our
                                                  customers and our shareholders. We thank you for your support.




                                                  KEVIN BURKE
                                                  Chairman, President, and Chief Executive Officer
