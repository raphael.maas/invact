In one of the most dynamic business
environments in memory, Verizon
delivered solid results while continuing
to transform for the digital-first
marketplace. Our 2016 performance
reflects the attributes that have
become the hallmarks of Verizon:
leadership in wireless and fiber
networks; a growing base of loyal,
high-quality customers; innovation
in mobile and digital services;
and investment in new sources of
growth in the digital economy. We
remain well-positioned for longterm
profitable growth as we take
advantage of the opportunities
created by technology advances
and growing demand for connected
solutions.

Investing in superior networks
The foundation of Verizons
performance in 2016, as in every
year in our history is our consistently
superior network. This fact was
confirmed by third-party studies such
as the most recent RootMetrics
surveys, which ranked Verizon the
best wireless network in the U.S. in
all six of its categories: overall
performance, network reliability,
network speed, data performance, call
performance and text performance.
This was the seventh consecutive
time Verizon ranked #1 in the U.S.
We have sustained our quality edge
amid a period of extraordinary
growth in 4G LTE wireless network
usage, which was up 47 percent year
over year in 2016, driven by surging
demand for mobile video.
In order to respond to our customers
needs, we continue to evolve our
network architecture to increase
efficiency and provide the highest
quality experience. Our superior
4G LTE Advanced wireless network
will be our mainstay for years to
come. We are constantly adding
to its capacity to handle increased
traffic, and preparing for the future
by deploying and recycling spectrum
while building small cells at a
significant reduction in cost.
We lead the industry in the
development of the next generation
of wireless networks, known as
5G, along with the ecosystem
of products this exciting new
technology will enable. With speeds
more than 1 gigabyte per second
100 times faster than current
wireless technology and latency
that is literally faster than the blink
of an eye, 5G will open new market
opportunities in fixed broadband
and the Internet of Things (IoT). 5G
technology will eventually impact
industry sectors across the global
economy and create an estimated
$12.3 trillion in market opportunity
by 2035.
To prepare for these growth
opportunities, we have created the
largest 5G proving ground in the
U.S. In 2016, we conducted successful
technical trials of 5G infrastructure
and will follow up in 2017 with precommercial
pilots in 11 markets
around the country in preparation for
introducing fixed wireless service.
Were also working with partners
across the technology industry to
develop the ecosystem of devices
and network components to make
5G a reality. We believe these
initiatives leveraged by our strong
spectrum and fiber assets will give
us crucial first-mover advantage in
these new markets.
In keeping with our history of network
innovation, we are also reinventing
our network architecture around
a common fiber platform that will
support all of Verizons businesses.
This new One Fiber architecture
will improve our 4G LTE coverage,
speed the deployment of 5G,
and deliver high-speed broadband
to homes and businesses of all
sizes. We launched One Fiber in
Boston in 2016 and plan to invest
$300 million over six years to deploy
it throughout the city. Going forward,
we will have further opportunities for
expansion through recently acquired
XO Communications, which has
fiber assets in 45 of the 50 largest
markets across the U.S.
Through every generation of
technology, we have used network
innovation to create new opportunities
for growth and deliver superior
service to customers. We remain
dedicated to providing the foundation
for the connected life on which our
customers depend.
Delivering for customers
When J.D. Power asked customers
in 2016 to rate the quality of their
communications services, Verizon
ranked #1 in three out of four regions
for residential internet service, #1 in
six out of six regions surveyed for
wireless, and #1 overall among U.S.
large-business customers. These
ratings speak to both the excellence
of our networks and the dedication of
our employees to delivering superior
customer service.
These attributes also contribute to
Verizons having a high-quality, loyal
customer base. In wireless, we ended
the year with 114.2 million retail
connections, up 1.9 percent year over
year. In 2016 we added 1.3 million
postpaid smartphones and maintained
our industry-leading retail postpaid
phone churn of less than 0.9 percent
for the year, showing that customers
do indeed believe that better matters.
Customers responded strongly to
innovations such as Safety Mode and
carryover data plans that give them
more control over their wireless usage.
In our wireline business, we ended
the year with 5.7 million Fios Internet
and 4.7 million Fios Video customers.
Were growing by showing
customers that our all-fiber Fios
service is indeed wired differently
and delivering a steady stream of
innovations like 750 Mbps Instant
Internet and our popular Custom TV
packages. Fios revenue growth
remains strong, and we see more
opportunity to grow as we penetrate
big urban markets like Boston, New
York and Philadelphia.
Our Enterprise Solutions group
provides global clients with services
such as advanced connectivity,
collaboration and managed services.
Among our customer wins in 2016
were contracts with Target, Oracle,
AECOM, ICICI Bank, Nanyang
Technological University and several
U.S. federal government agencies.
Were changing not only what we
deliver to customers, but also how
we deliver it, as we transform our
processes and customer interactions
around a digital model.
The most obvious driver of the trend
toward digital commerce is the
millennial customer: the Snapchat
generation that will account for
60 percent of U.S. purchasing power
by 2020. But digital disruption
is widespread and crosses all
demographics. Whether its ordering
a ride from Uber, shoes from Zappos
or groceries from Amazon, frictionless
online commerce is rapidly becoming
the norm in the digital economy. In
2016 Verizon made a big shift toward
this digital-first model by introducing
a new My Verizon app that enables
customers to purchase Verizon
services and manage their relationship
with us on their mobile device. After
its first six months, the new app was
already serving 14.5 million customers
and accounting for 13.4 percent of
our digital sales. We will continue to
simplify our products and processes
and improve all our touch points
with customers so that we earn their
loyalty every day.
With so much of daily life and
commerce dependent on our networks,
we take our responsibility to deliver
the promise of the digital world to
customers very seriously. With every
technological advance, the potential
for creating value for shareholders
and for society gets bigger and
more exciting. At the same time, we
have a responsibility for spreading
the opportunities afforded by the
digital world as widely as possible,
which is why weve made access
to technology and tech education
a focus of the Verizon Foundation.
Currently, careers in science,
technology, engineering and math
(STEM) are growing at twice the rate
of other jobs, yet millions of these
openings are going unfilled because
there are too few qualified workers
to fill the demand. In particular,
children in underserved communities
lack exposure to adult role models
in STEM jobs and, as a result, are
underrepresented in technical fields.
Were tackling this issue through
an initiative called Verizon Innovative
Learning (VIL) that provides free
technology, access and immersive,
hands-on learning to students and
teachers, particularly in underserved
communities. Through VIL, we host
a nationwide contest that challenges
students to use technology to
solve real-world problems, provide
free summer technology camps
on university campuses, run free
technology workshops at select
Verizon Wireless stores and much
more. Weve also launched a
campaign called #weneedmore
to call attention to the millions of
students in underserved communities
who lack advanced technology in
schools and exposure to careers
in science and technology. Our
goal is to get others involved in the
mission to expose young people to
the opportunities the digital world
offers and give every student an equal
chance at success. For more details,
visit verizon.com/about/responsibility
and www.weneedmore.com.
Building the growth platform
To drive future growth, we are
developing the platforms, content
and applications that will increase
usage on our network and monetize
our investment in world-class
infrastructure. For several years we
have been building our video assets
with the goal of creating new ways
to reach customers who access their
video content primarily on mobile
devices. According to a Cisco study,
mobile video is growing at a compound
annual rate of 62 percent and will
account for 75 percent of the traffic
on wireless networks by 2020.
The centerpiece of our strategy
in the evolving media marketplace
is AOL, which enjoyed sequential
revenue growth throughout 2016.
Average daily use of our mobilefirst
content engine, go90, is up to
30 minutes per viewer as we add
more unique programming through
partnerships with Awesomeness TV,
Complex Media, professional sports
leagues and other content providers.
In 2016, we announced our intention
to acquire Yahoo. Together, AOL and
Yahoo will give us some 1.3 billion
users, and provide us with added
scale and content to make us a
strong competitive force in digital
advertising, which is projected to be
a $90 billion global market by 2020.
We also strengthened our position
in the IoT marketplace, particularly
in telematics, or connected
transportation solutions. IoT is a
fast-growing area for Verizon; in
the fourth quarter, organic revenue
growth from our IoT businesses
was 21 percent year over year, with
annualized revenues approaching
$1 billion. In fact, with the acquisitions
of Fleetmatics and Telogis in 2016,
Verizon is now the #1 telematics
company in the U.S., and we have
the assets and expertise to address
the global market for connected
fleet services, expected to grow to
$40 billion worldwide by 2020.
More broadly, were building the
platforms and products to seamlessly
connect people and vehicles to
the physical environment. Our
consumer telematics product, hum,
is gaining subscribers, and we will
add to its capabilities this year. Our
IoT platform, ThingSpace, supports
more than 13,000 developers
who are embedding mobility into
a wide range of products and
applications that will connect to
our 4G LTE wireless network. We
also acquired two companies in the
nascent smart cities marketplace,
giving us a robust suite of services
to offer to municipalities, campuses
and other communities to ease traffic
congestion, make neighborhoods
safer, manage energy use and
engage citizens. These kinds of
solutions will also leverage our One
Fiber investment in Boston and
eventually across the country.
The goal of our digital media, IoT and
smart cities initiatives is to create new
growth engines for Verizon by staying
ahead of the trends in a transforming
marketplace. Our focus now is to
integrate these businesses, gain
scale and build the global franchises
that will contribute meaningfully to
Verizons growth.
Focusing on the fundamentals
Achieving growth in a challenging
environment remains our top priority.
Total operating revenues for the year
were nearly $126 billion. Adjusting
for revenues attributable to AOL and
divested wireline properties, total
operating revenues (non-GAAP) were
down 2.4 percent for the year on a
comparable basis despite a solid
operating performance. Adjusted
earnings per share (non-GAAP) were
$3.87, down 3 percent year over
year. In wireless, revenues declined
by 2.7 percent year over year, as
we continued the transition to a
new pricing model and dealt with
the effects of a fluid competitive
environment. Total wireline revenues
decreased by 2.3 percent for the year,
but Fios revenue growth was strong,
at 4.6 percent year over year.
Throughout this rapidly changing
business environment, our commitment
to financial discipline is unwavering.
Total return to shareholders in 2016
was 20.7 percent, which includes our
10th consecutive dividend increase.
Our cash flows funded $17.1 billion
of capital investment to maintain our
network advantage and build new
businesses, and our strong balance
sheet gives us the financial flexibility
to grow the business. In 2016, we
improved our strategic position by
divesting three telephone properties,
continued to drive process efficiencies
and negotiated new labor contracts
that will yield $500 million in cash
savings as well as other benefits
over the life of the contracts. We
also negotiated the sale of our data
center assets, a transaction expected
to close in second-quarter 2017.
Better matters
In a marketplace where some of our
competitors ask customers to
compromise on network quality,
we at Verizon are united in our belief
that better matters and confident
of the unique role we play in delivering
all the benefits of the digital world
to our customers. In a year marked
by many hurricanes, floods, fires and
other natural disasters, our employees
proved once again that the best
network is made up not only of glass
and cell towers, but also of the best
people who go the extra mile for our
customers.
Im grateful to our leadership team for
their dedication and to our Board of
Directors for their strategic guidance.
All of us feel privileged to lead this
great company.
Over our history, we have used periods
of change in technology and industry
structure to grow our company
and move the industry forward. Were
excited by that challenge in 2017
and beyond. We look forward to
developing our capabilities, building
our resources, and making our
networks stronger, faster and better
so that we continue to be the premier
company in delivering the promise
of the digital age.
Lowell McAdam
Chairman and Chief Executive Officer
Verizon Communications Inc.