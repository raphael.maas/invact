LETTER TO STOCKHOLDERS

Dear Stockholders,
We entered 2018 with four major objectives. We�re proud to report that in spite of the many challenges we
faced, we accomplished these objectives and achieved significant success around the globe as follows:
� We improved profitability.
� We spearheaded innovation and design in the watch category.
� We invested in digital to expand our e-commerce efforts.
� We continued to transform our business model under our New World Fossil initiative.
Through these efforts and more, we delivered $2.5 billion in net sales, reduced net debt and generated strong cash flow.
In accordance with our goal to become a leaner, more profitable company, we aggressively responded to the ever-evolving retail
environment, exited unprofitable businesses to sharpen our focus and effectively managed expenses and inventory. While we saw
improvements in 2018, these efforts will continue to provide benefits in 2019 and beyond. We will continue to build on our success
from this past year, while working to further improve our financial performance and create value for you, our investors, in the
coming year.

Our Strategy
Armed with strategic insights from 2018, we plan to
pursue several priority areas to continue our progress
through 2019 and beyond:
Connected
As consumers continued to express their desire for
connected devices, we listened. Thanks to our dedication
to distinguished designs, improved technology and
continuous innovation, we experienced 29 percent growth
in the wearables category in 2018. We launched the next
generation of smartwatches, with the overall smartwatch
category representing 21 percent of our total watch sales
in Q4. Additionally, we signed a strategic technology
partnership with CITIZEN to help further our growth
in the connected space.
By creating products with all the features our customers
want and need, we�re strongly differentiating ourselves
from our competition and positioning Fossil Group for
sustained growth.
FOSSIL Brand
We saw a modest increase in FOSSIL�s global core
watch sales in 2018, as we placed a heavy emphasis on
product innovation and expanding our direct-to-consumer
business. We worked hard to excite the consumer with
new ways to shop, such as Buy Online and Pick Up In
Store, and we experienced notable success for handbags
in the direct channel. Going forward, we�re enhancing
FOSSIL�s consumer targeting and social content
initiatives to further improve engagement while driving
sales through search, affiliates and retargeting efforts.
Digital/E-Commerce
The way our customers shop continues to evolve rapidly
and so must our ability to serve them better and faster.
To achieve this, we must continue to understand our
customers� behaviors and what drives their shopping
channel preference. In 2018, we increased our marketing
spend and invested more in our e-commerce platforms. 

These efforts optimized our ability to reach consumers
online. This approach also helped us more effectively
use the power of social influencers to generate direct
sales from posts which include our products. As a result,
we achieved 21 percent sales growth in our digital and
e-commerce channels in 2018.
Portfolio Brands
Our licensed brands continue to support our company�s
initiatives, as our wide variety of watch styles � from
kate spade new york to Michael Kors to Emporio
Armani � offers options for every customer�s unique
taste and need. We enhanced the footprint of our
licensed brands in the wearables space through
continued innovation and teamwork with our partners,
including launching the first kate spade new york and
Tory Burch touchscreen smartwatches. Additionally,
new licenses with PUMA and BMW will fuel the
growth of our portfolio brands and extend our reach
into currently untapped customer segments.
Cash Flow / Capital Structure
As part of our New World Fossil transformation, we
improved our profitability and strengthened our capital
structure, reducing our net debt by more than $200
million in 2018. With our focus on profitability, we have
exited marginal businesses and closed underperforming
store locations, in addition to implementing significant
working capital improvements. We did an outstanding job
of doing more with less by reducing inventory levels by
$196 million and generating nearly $250 million in cash
from operations.
Sustainability
Over the past year, we strengthened our social and
environmental commitments to our people, planet
and communities through our Make Time for Good
sustainability platform. This year, we plan to announce
our sustainability targets, with a goal of reporting on
progress as early as 2020. While we know we are only
at the beginning of this journey, we look forward to
making the biggest impact that only we can make.

Looking Ahead
In the past year, we have seen how disruption in the
marketplace creates opportunity. We�ve also seen the
critical importance of emphasizing product innovation
and placing the highest priority on strengthening our
connection with consumers. Winning in the marketplace
means engaging our customers where they are and having
the understanding and insight to anticipate where they�re
going next.
We made significant strides in 2018 to change
Fossil Group�s profit profile. And, looking ahead, we
see tremendous opportunity to accelerate our pace
of innovation and creativity. While our competitive
environment remains challenging, I have no doubt in
our ability to capitalize on these retail dynamics and
successfully turn those challenges into new opportunities.
My thanks to our board of directors, our management
team and our approximately 11,000 Fossil Group
employees around the world for all they have done to
position us so strongly for success. And, as always, my
thanks to you for your continued support and confidence
as we embark on this next step in our company�s story.
Sincerely,
Kosta N. Kartsotis
Chairman and Chief Executive Officer 