These accomplishments were achieved
despite entering the year with the lingering
effects of the sharp 2009 economic downturn
and having to carefully navigate the
industry-wide ramifications of the Macondo
tragedy in the Gulf of Mexico.
Our accomplishments in the past year
advanced our most important strategic
objectives: expanding our leading position as
a global, high-specification jack-up company;
growing the earnings power of our fleet; and
enhancing the value of our manufacturing
division while preparing it for an eventual
separation from Rowan. Throughout the
year, we maintained our commitment to a
culture of continuous improvement and to
conducting ourselves in a manner consistent
with our corporate values.
We are Expanding our Leading Position
in High-specification Jack-ups
Through our newbuild program and
successful acquisition of Skeie Drilling &
Production ASA (Skeie), we delivered
six new high-specification jack-up rigs
to our fleet, all on schedule and within
budget, including two of the three Skeie
N-Class jack-up rigs. Our N-Class rigs are
some of the most capable jack-ups in the
world, designed for operation in the harsh
environments of the highly regulated UK
and Norwegian sectors of the North Sea, and
can be equipped to perform simultaneous
drilling and production operations.
We will deliver three more high-specification
jack-ups in 2011 under our newbuild program,
further strengthening our market-leading
share of jack-ups with 2 to 2.5 million
pounds of hook-load capacity. Our remaining
three high-specification rigs under
construction include: the third N-Class rig,
the Rowan Norway; our third 240C rig, the
Joe Douglas; and our fourth EXL class rig.
When those rigs are delivered, we will own
19 of the 37 high-specification jack-up rigs
in operation and will have delivered 11 new
high-specification jack-ups into the Rowan
fleet over the prior three years.
Despite this substantial capital expansion
program, we have maintained a conservative
financial profile and strong balance sheet
with good liquidity and investment grade
credit ratings. In 2010, we obtained $1 billion
of new financing, all at rates of 5% or less,
to facilitate the Skeie transaction. These funds
will ensure that we can complete our newbuild
program while maintaining the financial
flexibility to take advantage of future
opportunities to grow and diversify our fleet.
We are Growing the Earnings Power
of our Fleet
In 2010, we continued to emphasize the
marketing and contracting of our fleet
internationally, including the opening
of an office in Southeast Asia. Through
those efforts, we added almost $1.5 billion
of contract drilling backlog in 2010 and
today have approximately $1.8 billion in
committed backlog.
We also strive to grow the earnings
power of our fleet through a disciplined
approach to capital, a focus on cost
effectiveness and fostering a culture
of continuous improvement. We want to
respect and refine the things we do well,
and to continually question everything we
do to see if there is a better, safer or more
efficient way to operate. Several key initiatives
were implemented in 2010 including efforts
related to supply chain management, cost
effectiveness and downtime reduction.
We strongly believe that proper planning
for all of our activities gives us the perfect
opportunity to evaluate how we perform
and look for ways to improve.
We are Enhancing the Value
of LeTourneau
At our manufacturing division, LeTourneau
Technologies, Inc. (LeTourneau), we
brought new leadership to our talented
management team and continue to improve
efficiencies and overall execution as we
plan for its separation from Rowan. We
now believe that suitable market conditions
exist and expect to begin a process in 2011
to either spin off or sell LeTourneau.
LeTourneaus mining products division
has seen strong demand growth for its
innovative line of front loaders, and that
demand is expected to carry over into 2011
and beyond. LeTourneau shipped 25 mining
loaders in 2010 and currently has another
29 in backlog. We believe that loader
shipments in 2011 could be double what
they were in 2010, reflecting the highest
level of shipments in the Companys history.
Recently, LeTourneau began production
of the Generation II front-end wheel
loaders. This new loader generation utilizes
LeTourneaus patented hybrid Switched
Reluctance (SR) electric drive system that
has been designed to recover 100 percent of
the braking energy that would normally be
lost during a typical loading cycle, resulting
in higher productivity and lower operating
costs for its customers through significantly
better fuel efficiency.
LeTourneaus drilling products division
is also seeing improving market conditions.
As we enter 2011, the current jack-up
newbuilding cycle appears to be gaining
momentum with several offshore drilling
contractors recently placing orders for new
jack-ups with deliveries in 2013 and later.
We believe these new orders are a predictable
response by the industry to the increasing
demands of jack-up customers for newer
and higher-specification equipment, and
reflect the need for many established
contractors to begin to renew their aging
jack-up fleets. Approximately 69% of the
world jack-up fleet is now more than 25 years
old and facing significant competitive
challenges in finding work. That reality
is evident in the 93% utilization for highspecification
jack-ups, even at a time when
the worldwide jack-up fleet utilization is
below 70%.
We believe that this secular, industrywide
jack-up fleet renewal will be a very
favorable trend for LeTourneau. Our
drilling products division provides jack-up
designs (nearly 30% of jack-ups in operation
worldwide were designed and/or built
by LeTourneau), rig kits, and equipment
packages, mobile land rigs and equipment
for land and offshore rigs, including top
drives, mud pumps, drawworks and power
systems. This business is seeing increased
requests for quotations and we expect it to
be successful in winning new kit and
equipment orders.
We are Committed to our
Corporate Values
At Rowan, our corporate values are effectively
a roadmap for how we want to conduct
ourselves individually and collectively as
we pursue our mission and strive to ensure
that Rowan is recognized by our customers
as the most efficient and capable provider of
demanding drilling services. Our corporate
values address core issues such as respect
for the individual, commitment to integrity,
protection of the environment and continuous
improvement. Most important of our corporate
values, however, is our commitment to an
injury-free workplace.
We have worked hard to elevate awareness
of safety issues for all of our employees.
We continually engage everyone in the
office and on the rig in a conversation on
safety, emphasizing each employees
responsibility for the safety of co-workers,
customers and third parties. Every employee
at Rowan has the right and the obligation to
stop any operation that is or could be unsafe.
We continually strive for improvement in the
safety of everything we do and are committed
to sharing safety ideas with the rest of the
drilling industry; effective safety practices
should never be considered proprietary.
Wherever possible, the best standards
should be identified and adopted globally.
Rowan is Designed for Success
Our strategy is and has been to focus on
building our offshore drilling fleet. We
strongly believe that Rowan has true brand
value among our customers that enables
us to achieve industry leading jack-up day
rates and utilization. While we work toward
the separation of LeTourneau and our land
division, we will continue to look for ways
to expand our offshore fleet by adding to
our strong position in high-end jack-ups
and, ultimately, expanding into ultra
deepwater. Our management team is
convinced that a balanced fleet provides
the best opportunity to create value for
our stockholders over time.
With our exceptional team of employees
and strong operational reputation, our market
leading position in high-quality equipment,
and our balance sheet strength and liquidity
to pursue our expansion strategy, we are
designed for success and prepared to face the
challenges and opportunities that lie ahead.
In closing, I want to say that it is truly
my privilege to lead this outstanding
organization. I would like to thank our
Board members for their continued support
and you, our stockholders, for your investment
and confidence in us. Most importantly,
I want to express my appreciation and
respect to our dedicated and hard-working
employees for their continued commitment
and pride in all they do for Rowan.

W. Matt Ralls
President and Chief Executive Officer