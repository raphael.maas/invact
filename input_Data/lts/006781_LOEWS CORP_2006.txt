LETTER TO OUR
SHARE HOLDERS AND EMPLOYEES

Our company had an excellent year in 2006. All of
our operating businesses performed extremely
well, with each of our three largest subsidiaries
 CNA, Diamond Offshore and Lorillard  enjoying
a record year. We reported $2.5 billion in consolidated
earnings, more than double the earnings of the previous
year. Earnings per share of Loews common stock were
$3.75 and earnings per share of Carolina Group stock
were $4.46. We repurchased $510 million of Loews common
stock and paid $308 million in dividends to holders
of Loews common stock and Carolina Group stock. An
excellent year indeed.
Long-Term Value Creation
Reviewing the long-term performance of Loews common
stock helps to frame the year 2006 in a broader context.
As you can see from the chart below, if you purchased
Loews common stock at almost any point during the past
25 years and held it, chances are you have had an attractive,
market-beating return. Loews common stock appreciated
at a 16.1 percent compound annual rate during
those years versus 10.3 percent for the S&P 500 Index.
It is important to think about a longer time frame
because, as good as the Companys results were in 2006,
Loews will not double its earnings every year. No company
can do that. More importantly, we have never devoted
our energies to producing just one outstanding quarter or
year. We believe we can generate the greatest value for our
shareholders by focusing on much longer time periods.
That is what Loews has done for more than four decades.
That is what Loews has done for the last five years. And
that is what we intend to continue doing.
Our subsidiaries follow this same philosophy, taking a
long-term view in setting their coporate strategies. The
credit for Loewss results in 2006 belongs to our subsidiaries,
each of which turned in strong financial results:
- CNA improved its position in the commercial property-
casualty insurance market and posted record earnings
due to disciplined underwriting, stringent expense
controls and solid investment results.
- Lorillard continued to increase volume and market share,
and maintained industry-leading unit profitability.
- Diamond Offshore turned in a year of record results in
a robust market for offshore drillers.
- Boardwalk Pipeline performed well during its first full
year as a publicly traded master limited partnership
and announced several expansion projects that create
opportunities for future growth.
- Loews Hotels benefited from a vibrant lodging market
and continued its expansion.
- Bulova achieved strong results led by the sales performance
of its signature Bulova watch brand.
For further details about our performance in 2006, please
turn to the section of this report entitled Loews: A
Financial Portrait, beginning on page 10.
Our Competitive Advantage
is Patience
Our Company maintains a long-term orientation in guiding
our subsidiaries, seeking acquisitions and managing
the holding companys capital.
We help our subsidiaries develop first class management

teams, and we consult intensely on matters of strategy
and capital management, but ultimately we rely on our
subsidiaries managers to determine and implement their
strategies.
We seek out undervalued opportunities to create value,
whether through cash flow generation or asset appreciation,
and we focus on identifying and managing downside risks.
Value investing in the traditional sense consists of purchasing
businesses or assets at substantial discounts to the
present value of the returns they are expected to generate.
We use this approach in assessing every potential acquisition.
As a result, each company in the Loews portfolio
has at least one thing in common: it was acquired at an
attractive price. Markets are cyclical, so at times there are
many investors willing to pay more than we are. Today is
one of those times. The marketplace is crowded, making
it difficult to buy good businesses at attractive prices.
We feel no pressure to invest at any given moment and
we are comfortable maintaining a large amount of liquidity,
which allows us to move quickly when the time is
right. But, as the saying goes, just because youre on a diet
doesnt mean you cant read the menu! Loews continually
evaluates business opportunities, seeking out potential
value-creating acquisitions.
Loews views each business it acquires as a long-term
holding, a perspective that sets us apart from a private
equity firms conventional five years and out timetable.
Accordingly, Loews still owns most of the businesses it
has bought over the years. We have historically acquired
companies with substantial capital or financial assets that
offer products and services for which there is enduring,
if sometimes cyclical, demand. To date, we have not invested
in cutting edge technologies or emerging consumer
trends, but we keep an open mind and will consider any
kind of acquisition if it is in the interests of our shareholders.
What we refuse to do, however, is overpay. We
believe strongly that if a good business is purchased at a
good price, downside risk will be minimized. The converse
is equally true, which keeps us from straying from
our core principles.
When we examine potential acquisitions, we are drawn
to companies with the ability to generate cash flow to
fund internal growth and the payment of dividends. We
are equally drawn to companies with undervalued assets.
Good businesses come in various shapes and sizes  just
look at our subsidiaries. We focus on adhering to our core
principles while maintaining the flexibility to pursue any
opportunity that will benefit our shareholders.
Some investors attempt to value Loews common stock
using a sum-of-the-parts methodology  that is, they calculate
the per share value of the holding companys net assets
by summing the values of our wholly owned subsidiaries;
our ownership stakes in our publicly traded subsidiaries;
the remaining principal amount of the Carolina Group
notional debt; and the holding companys cash position
net of debt. While we consider this calculation to be a useful
benchmark, we prefer to look at Loewss potential for
value creation. We are optimistic about the growth opportunities
for all of our subsidiaries and about our ability to
find new value-creating uses for our liquid assets.
Cash Flow and Liqui s cash flow from subsidiaries.
While Lorillard continued to be a major provider
of holding company cash, both Boardwalk Pipeline and
Diamond Offshore became important cash flow contributors
in 2006, paying combined dividends to the holding
company of $257 million. As we head into 2007, we are
optimistic that Boardwalk Pipelines cash distributions
will continue to benefit from its strong operations and
multi-year expansion projects. Diamond Offshore will
provide even more cash to Loews in 2007 than in 2006,
given the $280 million Loews will receive on March 1st
from the $4.00 per share special dividend declared by
Diamond Offshore on January 30, 2007.
In addition to healthy cash flow from its subsidiaries,
the holding companys cash balances were greatly
enhanced by several important transactions in 2006.
Loews sold 30 million shares of Carolina Group stock
during 2006, netting $1.6 billion and decreasing the
Loews Groups economic interest in Carolina Group to
38 percent. Also, Loews received $729 million in net cash
proceeds and 7.9 million shares of CNA common stock

upon CNAs redemption of the Series H Preferred Stock
sold to Loews in 2002.
The cash flow from subsidiaries and the transactions cited
above, offset by, among other things, shareholder dividends,
share repurchases and the retirement of $300 million
of holding company debt in December 2006, resulted
in an increase in holding company cash and investments
from $2.9 billion at the end of 2005 to $5.3 billion at yearend
2006. This degree of liquidity provides us the flexibility
to contemplate an extraordinary range of opportunities.
In the meantime, our portfolio managers are investing
the cash and investments conservatively and generating
attractive, low-risk returns.
Focus on Ca p ital All ocation
At Loews we focus on capital allocation at the holding
company and at each of our subsidiaries. We believe that
if we allocate Loewss capital for long-term value creation,
it will ultimately be reflected in the value of Loews
common stock. We pursue this goal not only by acquiring
businesses at attractive prices and managing them for the
long term, but also by repurchasing Loews common stock
in the open market. Share buybacks have become a popular
practice in recent years, in part because many companies
repurchase shares to counter the dilutive effects
of stock options and other employee incentives. Such is
not the case at Loews. We repurchase shares only when
they are selling at prices we regard as favorable and when
repurchases create value for our shareholders.
Repurchasing shares to create value for our shareholders
is a long-standing tradition at Loews. In each decade since
the 1970s, Loews has repurchased more than 25 percent
of its shares of common stock outstanding at the decades
start. As a result, there were 544 million shares of Loews
common stock outstanding at the end of 2006 compared
with 1.3 billion in 1971, adjusted for stock splits. Loews
did not repurchase its shares between 2002 and 2005, but
resumed share repurchases in 2006, spending $510 million
to buy 14 million shares of Loews common stock.
While we are pleased with Loewss results in 2006, we are
determined to maintain financial discipline and the flexibility
to deal with any change in the business climate that
may lie ahead. With our strong balance sheet and focus
on finding undervalued assets, we are equally well positioned
to continue enjoying healthy market conditions or
to use a downturn as an opportunity to make new acquisitions
on favorable terms.
In closing, we want to thank our fellow Loews Corporation
employees and the employees of all our subsidiaries.
Their talent, enthusiasm and hard work continue to fuel
Loewss growth and success. We remain confident that
Loews has the right people, the right philosophy and the
right disciplined approach to continue building value for
shareholders year after year.

Sincerely,
James S. Tisch 
Office of the President
February 23, 2007
Andrew H. Tisch 
Jonathan M. Tisch