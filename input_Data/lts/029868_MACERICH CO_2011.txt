Dear Fellow Stockholders:
I would like to welcome you as you review our annual
stockholders report. In this letter, I will be sharing with
you my views about various aspects of our business
today and going forward.
Operations
I am pleased to report that the operating fundamentals
in the top tier regional mall industry, of which Macerich is
a leader, are strong. Our tenants have generally seen their
retail sales return to 2007 levels and, more importantly,
they are generating the healthiest gross operating margins
we have seen in many years. These strong sales levels
and operating margins as well as the lack of new supply
coming on the market have created a favorable environment for our Company to harvest the embedded rent
growth that exists within our portfolio. In 2011, we
experienced strong positive leasing spreads as new leases
signed exceeded expiring rents by approximately 14%.
We were more selective in terms of our tenant mix and
the rents that we would accept at our centers. In looking
forward, we believe our Company is well-positioned to
continue to improve our tenant mix and thereby our sales
productivity, as well as capture additional rent growth
within our portfolio. This is a dramatic departure from
the leasing environment that we found ourselves in during
2009 and 2010.
The strength of our operations is also seen in our sales
and occupancy numbers for 2011. Annual sales per
square foot in our regional shopping center portfolio
ended the year at $489, representing a 13% increase from
2010. Our top 25 malls averaged sales per square foot of
$626 and generate over 56% of our total Company net
operating income (NOI). We saw sales increase in all
of our regions in 2011. Occupancy levels also remained
strong with year-end occupancy of 92.7%.
To help us achieve further growth, we are pleased to have
recently announced that Robert Perlmutter will join our
executive team as Executive Vice President of Leasing.
Bobby was at Heitman Financial for 15 years where he was
responsible for the ownership and management of over
40 U.S. regional malls. After leaving Heitman, Bobby
formed Davis Street Land Company which developed and
owned a portfolio of high quality retail centers. We are all
thrilled to have Bobby join our Company and bring his
creative and retail leadership to Macerich.
Acquisitions, Developments,
Redevelopments and External Growth
One of the highlights of 2011 was our entry into the outlet
business which we predicted in our last annual report.
We successfully acquired Fashion Outlets of Niagara which is one of the leading outlet centers in the United States
with interior tenant sales of approximately $620 per
square foot. After purchasing this center, we acquired an
interest in some contiguous land and have commenced
the pre-leasing of a 200,000 square foot expansion.
In conjunction with our acquisition of Fashion Outlets
of Niagara, we also acquired a 60% ownership interest
in Fashion Outlets of Chicago, a to-be-built outlet center
in Chicago near the OHare International Airport.
This center is currently under construction, over 50%
pre-leased, and development will be funded primarily by
a new construction loan. The grand opening is projected
to be in August of 2013 and we believe it will be one of
the top fashion outlet centers in the United States.
We have further plans to pursue additional outlet opportunities in our core markets. Our intent is to mirror our
regional center portfolio philosophy and own select high
quality, dominant outlet centers.
Another exciting development is the recent ground breaking of our $600 million mixed-use expansion of Tysons
Corner. This expansion includes a 500,000 square foot
office building, a 450 unit residential tower, and a 300 room
hotel which are all slated to open in 2014. We believe that
this expansion will be accretive to our net asset value and,
more importantly, will enhance the productivity of Tysons
Corner, which is already one of the premier shopping malls
in the United States.
We recently celebrated the grand opening of a new
Neiman Marcus store at Broadway Plaza in Walnut Creek.
In addition, we are currently negotiating with the City of
Walnut Creek to obtain an entitlement that would allow us
to more than double the gross leaseable area of the small
shops at this center. We see this as our next Santa Monica
Place project in terms of the opportunity to take an already
fabulous retail location and turn it into one of the most
dominant centers in the United States.
In addition to acquiring Fashion Outlets of Niagara,
we increased our ownership interest in Kierland Commons,
Desert Sky Mall, Superstition Springs Center and
Arrowhead Towne Center in the Phoenix marketplace.
While the economy of the greater metropolitan area of
Phoenix was significantly affected by the recent recession,
it is now rebounding. In fact, our Arizona malls had very
strong productivity over the past two years, including an
increase in tenant sales per square foot of 9.7% in 2011.
We also recently acquired 500 North Michigan Avenue,
a 324,000 square foot office building contiguous to The
Shops at North Bridge in Chicago. We intend to convert
four levels of this office space into a 45,000 square foot signature retail location fronting Michigan Avenue and
potentially integrate it with The Shops at North Bridge. In
addition, we have plans to reposition The Shops at North
Bridge to take advantage of its prestigious Michigan Avenue
location as well as its anchor -- one of the highest sales
volume Nordstrom in the entire Nordstrom department
store chain.
Balance Sheet Management
We again made great strides on our balance sheet in 2011.
During the year, we entered into a new five year (including
extension options) $1.5 billion line of credit with a consortium of 24 banks. This line of credit will provide a valuable
source of liquidity as well as financial capacity for opportunities going forward. We also recently retired our convertible
debentures resulting in the retirement of over $1.0 billion of
unsecured debt in the past two years. Our balance sheet
ratios are some of the strongest in the mall industry today.
We do not have a fixed target for leverage levels, but it is our
goal to continue to increase our balance sheet flexibility and
capacity. We believe this will be achieved primarily through
the selective disposition of non-core assets in our portfolio.
We also have a stated goal to increase the percentage of our
income from our core assets from the current level of
approximately 83% of our NOI to more than 90% in the next
few years. We believe we are well-positioned to accomplish
this goal and it is our firm intent to do so.
Thank you very much for your support over the years.
We look forward to reporting to you in the future.
As always, we want to thank our board of directors for
their guidance in helping us achieve our strong results.
Very truly yours,
Arthur M. Coppola
Chairman and Chief Executive Officer 