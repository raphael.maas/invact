Dear Shareholders,
At Zoetis, we are inspired to serve the world by ensuring
the health of its animals. People rely on animals for a variety
of reasonscomfort and companionship from pets as well
as protein and nutrition from farm animalsand we see
tremendous opportunity to help address these needs and
grow our business.
I first wrote these words in my 2013 Letter to Shareholders upon concluding
our first year as a public company and setting our sights on shaping the future
of the animal health industry. As we mark our fifth anniversary this February,
the opportunities in animal health are more evident than ever; the role Zoetis
plays in helping ensure the health of animals is well-recognized; and the value
we can create for our shareholders has been proven.
During these years, we have relied on several competitive advantages to
deliver our results: a singular focus on animal health, products sold in more
than 100 countries, and a diverse portfolio of approximately 300 product
lines. We have strengthened our three interconnected capabilitiesdirect
customer relationships, highly productive research and development, and
high-quality manufacturingwhile we have increased profit margins and
improved our financial position. And, we have demonstrated our value
proposition to grow revenue in line with or faster than the animal health
market; grow adjusted net income faster than revenue; target value-added
investment opportunities; and return excess capital to shareholders.
In 2017, we delivered our fifth consecutive year of operational revenue growth
and increased profitability8% and 21%, respectivelyconsistent with our
value proposition. We have placed Zoetis firmly on a course for sustainable
profitable growth by continuing to simplify our operations and increasing
our cash for internal and external investments, while returning excess capital
to shareholders.
The credit for this success goes to our Zoetis colleagues. I am very proud
of their dedication and passion for supporting our customers, and I admire
the many ways they demonstrate their commitment to Our Core Beliefs. 2017FIFTH CONSECUTIVE YEAR
OF SOLID PERFORMANCE
Our performance in 2017
demonstrated our financial strength
in the following ways:
 achieved $5.3 billion in revenue in 2017,
becoming the first global animal health
company to earn more than $5 billion
in annual revenue
 grew operational revenue at 8%, faster
than the historical 5% to 6%1
 compound
annual growth for the animal health
market
 grew adjusted net income faster than
revenue at 21% operationally
 achieved adjusted EBIT margin of 34%,
an improvement of 900 basis points in
the past three years
 realized the full impact of our
efficiency improvements and nearly
doubled operating cash flow.
BUILT A RESILIENT, GROWING,
AND COMPETITIVE COMPANY
We demonstrated the resilience of
our business and ability to successfully
compete, achieving growth in the
face of economic and political
challenges, regulatory changes and
the normal business cycles of our
livestock farmer customers.
As we improved our profitability and
cash flow in 2017, we allocated capital
for investments that will support our
future growth in these ways:
 expanded our field force in fast
growing markets such as China
and Brazil
 implemented direct to consumer
advertising and promotional campaigns
in the United States and Brazil to
support key companion animal
products, Apoquel and Simparica
 invested in research and development
and acquisitions
 enhanced the capacity and capabilities
of our global manufacturing network.
GROWTH STRATEGY FRAMED AROUND
EVOLVING CUSTOMER NEEDS
A growing world population, increasing
middle class and urbanization are all
fueling greater demand for meats, milk,
poultry, eggs and fish. Likewise, they are
driving up pet ownership and spending
on healthcare. These have beenand
will remainthe fundamental drivers
of demand for animal health products
and for predictable growth in the animal
health sector. We also see other forces in the
marketplace that are shaping customer
needs and creating new opportunities
to address and issues to navigate.
Maintaining the Pace of Improvements
in Animal Agriculture
Consumers in markets worldwide are
more interested in how their food is
produced. Together with food retailers
and regulators, they are paying attention
to the responsible use of antibiotics in
livestock and to the use of production
efficiency technologies.
While we anticipate continued demand
for these products, our customers
are looking to Zoetis for innovation
to help them maintain the pace of
improvements in animal agriculture.
We are focusing our research and
development and our marketing
on highly integrated solutions and
technologies across the continuum of
carepredicting disease with genetic
tests; preventing it with vaccines that
are administered in some cases with
robotic automation systems; detecting
illness early using diagnostics and digital
sensor systems; and treating disease
with innovative medicines.
Capturing Opportunities from
the Humanization of Pets
Pets have become part of the family,
and the humanization of pets is a
driving force in increased demand for
specialty therapeutics to manage
acute and chronic diseases in companion
animals such as osteoarthritis, allergies,
diabetes and cardiovascular disease.
It is also fueling demand for improved
anti-parasitic therapies, formulations
and combinations to help keep pets
healthy. Add to this the increasing
importance of the millennial generation
who are especially motivated to spend
on pet health and wellness and give
reason to anticipate demand increasing
for years to come.
We are already seeing the return on
our investment in innovation in specialty
care therapeutics with the success of
our canine dermatology products
Apoquel, an oral Janus kinase inhibitor,
and the monoclonal antibody therapy
Cytopoint. These novel therapeutics
generated $428 million in combined
sales in 2017.
We view our industry-leading innovation
as central to staying competitive and
fully capturing the potential of highly integrated solutions that can generate
revenue growth in line with or faster
than the market.
2017 INVESTMENTS SHAPED
PROFITABLE GROWTH
In 2017, we began to see the benefits
of recent allocations of resources, and
we continued to use our increased
operating cash flow to fund future
growth investments.
Delivered Industry-Leading Innovation
With $382 million in R&D expense in
2017, Zoetis continued to shape and
define innovation in the animal health
industry, with 200+ approvals worldwide
for new products and enhancements
to the current portfolio.
For Companion Animals
 We were proud to be the first company
to receive approval in the European
Union for a veterinary use monoclonal
antibody (mAb) therapy with
marketing authorization for Cytopoint,
a mAb that helps reduce the clinical
signs associated with atopic dermatitis,
such as itching, in dogs.
 We received approval in the EU
of Stronghold Plus (selamectin/
sarolaner) to treat fleas, ticks, ear
mites, lice and gastrointestinal worms
and prevent heartworm disease in cats.
This followed the launch in 2016 of
Simparica, a once-monthly chewable
formulation of sarolaner to treat fleas
and ticks in dogs. These were the first
in a series of products to be developed
from our sarolaner platform.
For Livestock
 Approval of our Alpha Ject micro 1PD
vaccine brought a new effective
solution to control pancreatic disease
in farmed salmon in Norway, UK
and Ireland.
 For pigs, our Suvaxyn PRRS MLV,
approved in the EU, extended our
Suvaxyn swine vaccine franchise to
help protect pigs against the common
and costly porcine reproductive and
respiratory syndrome.
Expanded and Enhanced
Manufacturing Capacity
Our customers also count on Zoetis for
a reliable supply of high quality products
at competitive prices, and we continue
to see this as a critical element of
supporting profitable growth.
 Announced Plans to Expand Vaccine
Development and Manufacturing in
China. To help capture the attractive
growth opportunities in China from
increasing demand for quality vaccines
for pigs, cattle, and farmed fish,
we announced plans to break ground
in the spring of 2018 for a new
manufacturing and R&D facility
in Suzhou. We also acquired the
remaining share and became the sole
owner of our joint venture vaccine
manufacturing site in Jilin where
Zoetis has produced vaccines for swine
tailored to the needs of the Chinese
market since 2011.
 Added Capacity to Produce Active
Pharmaceutical Ingredient. We
enhanced our capacity to produce
active pharmaceutical ingredient for
our most important products within
our owned global manufacturing
and supply network. In September
2017, we completed the purchase of
a manufacturing site in Rathdrum,
Ireland. Start-up work is underway
to prepare the site to take on an
important role in our manufacturing.
 Increased Capabilities and Capacity to
Manufacture Medicines and Vaccines.
To meet increasing customer demand
for oral tablet medications and for a
new generation of vaccines developed
on vector technology for the fastgrowing poultry industry, Zoetis began
expansions of manufacturing facilities
in Kalamazoo, Michigan, and Charles
City, Iowa, respectively.
Deployed Capital to
Strategic Acquisitions
In July 2017, Zoetis completed the
acquisition of Nexvet Biopharma
for approximately $85 million.
The transaction strengthened our
development platform of mAb therapies
and added potential new alternatives
to treat chronic pain associated with
osteoarthritis in companion animals, an
area of high need in animal health.
ACTED ON OUR COMMITMENT
TO CAPITAL EFFICIENCY
We have delivered on our value
proposition every year since our initial
public offering in February 2013.
 Achieved operational revenue growth
in line with or faster than the market at
8% in 2017 and every prior year (2013,
7%; 2014, 7%; 2015, 8%; 2016, 5%)  Improved the growth rate in adjusted
net income from 16% in 2013 to 21%
in 2017, consistently faster than our
revenue growth rates
 Increased cash returned to
shareholders in the form of dividends
as a result of annual increases in the
dividend rate from $98 million in 2013
at a quarterly rate of $0.065 to
$206 million in 2017 at a quarterly
rate of $0.105
 Increased the per share dividend by
10.5% between 2016 and 2017
 In December, declared a first quarter
2018 dividend of $0.126 per share, an
increase of 20% from the quarterly
rate paid in 2017
 Completed $500 million share
repurchase program in 2016. Following
authorization by the Board of Directors
of a new $1.5 billion multi-year share
repurchase program in 2016, Zoetis
completed $500 million in share
repurchases in 2017.
CREATED SHARED VALUE
We are proud to use our resources,
expertise and diverse portfolio to
help build a better business and a
better world.
We help create shared value for society
by acting on Our Six Responsibilities:
Advance Animal Health; Promote the
Veterinary Profession; Ensure a Safe and
Sustainable Food Supply; Support the
Communities Where We Operate; Run
Our Business Ethically and Responsibly;
and Provide a Workplace Where
Colleagues Feel Valued.
Highlights of our achievements in 2017
can be found in the sidebar Acting on
Our Six Responsibilities. We provide
a broad view of our accomplishments
across Our Six Responsibilities at the
Responsibility section of our website
www.zoetis.com/responsibility.
SHAPING FUTURE SUCCESS
As Zoetis marks its fifth anniversary,
we are energized by the growth
opportunities in a $31 billion1
 core animal
health market of vaccines, medicines,
parasiticides, and medicated feed
additives. We are also excited by the
prospect of expanding our presence
in genetics, biodevices, diagnostics
and data analytics, which are part
of the broader $150 billion2 animal
health industry.
We are determined to stay wellpositioned as:
 the clear leader in innovation, focusing
our highly productive research and
development on integrated solutions
that help our customers as they strive
to better predict, prevent, detect and
treat disease in animals
 a growing, resilient, competitive
business with a global presence,
diverse portfolio, singular focus on
animal health, and proven business
model, driving increasing financial
strength and competitiveness
 a customer-obsessed company
whose colleagues are passionate
about solving our customers most
pressing challenges and helping their
businesses succeed
 an admired employer with highly
engaged colleagues who live Our Core
Beliefs and demonstrate the culture
of ownership and accountability
 a business with the strategic vision
and management expertise to chart
a successful future for Zoetis and the
animal health industry.
We stand committed to delivering on
our long-term value proposition for
shareholders: to grow revenue in line with
or faster than the market; grow adjusted
net income faster than revenue; make
targeted, value-added investments; and
return excess capital to shareholders.
Thank you, our shareholders, for your
investment and confidence in Zoetis.
Juan Ramn Alaix
Chief Executive Officer