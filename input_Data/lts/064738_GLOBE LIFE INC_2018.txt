2018 proved to be another successful year for Torchmark. Premiums grew over 4% and net operating income as a return on equity, excluding net
unrealized gains on fixed maturities, was 14.6%. Net operating income per share grew 27% from a year ago. Excluding the impact of tax reform, we
estimate the net operating income per share growth would have been approximately 8%.
We often hear people say that the life insurance industry is a mature industry that has little or no growth potential. However, we believe Torchmark
operates in a market that provides tremendous opportunity. Precise execution of our business model, a model which is quite different than that of
most of our peers, has produced solid earnings growth year after year regardless of general economic conditions. The fundamental features of this
model are discussed below.

MARKET
Our target customers are working class
middle-income families. Most of our sales
are to persons who are either uninsured or
underinsured. This is a vastly underserved
market in which there is a great need for
the products we sell. Also due to limited
competition, we don't have to compete for
market share with other companies.
PRODUCTS
We have been selling essentially the same
products for over fifty years. We focus on
basic protection life and supplemental
health insurance products designed to
meet the primary insurance needs of our
marketplace. These products are simple for
agents and customers to understand and
are not impacted by interest rate or equity
market fluctuations.
EXCLUSIVE DISTRIBUTION
We distribute our insurance products
primarily through exclusive agency and directto-consumer marketing channels. Utilization
of these channels enhances our ability to
effectively manage costs and produce stable
underwriting margins.
MARGINS
Most of Torchmark�s net operating income
comes from underwriting income rather
than investment income due to the strong
underwriting margins in our business and
our ability to control costs. Torchmark is
well known for its administrative efficiency
which, along with the extensive data and
experience we have accumulated over the
last fifty years marketing basic protection
products to the middle-income market,
helps us control costs very effectively.
CASH FLOWS
Torchmark consistently generates significant
excess cash flows from its operations. The key
to the consistency is our highly persistent block
of in force business. Approximately 90% of our
premium revenue comes from policies sold in
prior years, and the stability of our in force block has
never been impacted significantly by macroeconomic conditions.
RETURN OF EXCESS CAPITAL TO
SHAREHOLDERS
We remain committed to building our in force
business and then returning excess capital
to shareholders. Torchmark has returned
approximately 71% of its net income to
shareholders through share repurchases and
dividends since 1986, due to the consistent
generation of excess cash flow. 

While we strongly believe in our business model, we understand
the need to continually seek to enhance our operations. Continued
investments in technology will help ensure that we maximize our
ability to adapt to a rapidly changing environment. The goals of these
investments are to:
� Improve the agent experience and ease of doing business
� Facilitate a digital consumer experience
� Replace back office legacy systems with modern,
commercial solutions
� Expand the use of data analytics
� Protect against cyber threats
We believe these investments will help us maintain a position of
strength and generate profitable future growth.
The charts below demonstrate the consistent growth in earnings per
share and book value per share produced by Torchmark.

Torchmark has consistently generated a substantial return on
equity (ROE). In 2018, the net operating income as an ROE, excluding
net unrealized gains on fixed maturities, was 14.6%. On a GAAP basis,
2018 ROE was 12.3%.

Underwriting income reflects premiums less policy benefits,
acquisition costs, and administrative expenses. As we indicated
earlier, Torchmark�s net operating income is generated primarily
by insurance underwriting margin, rather than investment income.
Underwriting income accounted for approximately 74% of pre-tax
operating income in 2018.

We focus on basic protection life insurance and supplemental health
insurance products that best meet the needs of our market and provide
predictable profitability.

American Income is our largest distribution channel. As can be seen in
the charts, life sales and agent count have grown at a compound annual
growth rate of 7.6% and 8.4%, respectively, over the past ten years.
While American Income has had strong agent and sales growth over
the last fifteen years, there have been many times within that overall
period when sales have been flat or even declined. This past year was
one of those periods.
The lack of sales growth in 2018 was due to a lack of agent growth. There
are many variables that impact agency growth. Agencies go through
cycles and it takes tremendous focus and diligence to responsibly grow
an agency force through those cycles. It is both an art and a science.
Understanding human behavior is critical in driving agency results.
We frequently adjust commission and bonus structures to steer activity
in the direction needed to restart or improve agency growth. The
adjustments generally place an emphasis on agent recruiting, training,
or retention. The commission and bonus adjustments may be directed
at any level in the agency hierarchy, depending on what type of change
is needed.
As previously mentioned, agency growth was flat at American Income
in 2018. While recruiting was up 6% for the year, retention of new
agents was down, due in part to historically low unemployment. The
abundance of alternative work opportunities resulted in higher than
normal turnover of new agents. While we haven't increased agent
compensation, we are confident that the restructuring of commission 

and bonus payments that have been made in 2019 will stabilize retention
rates even if unemployment remains at these low levels. In addition
to the impact of low employment, retention was negatively impacted
by the fact that eight new agency offices were opened. Whenever we
open new offices, growing pains occur as training is impacted when
talented agent managers leave existing offices to go start new offices.
This is a great short-term problem to have, as expansion into new offices
generates sustainable agency growth in the long run.
For as long as we can remember, investors have been asking us if
American Income can grow in the face of short term challenges or overall
declines in union membership. The answer to that question has always
been a resounding yes.
While labor union relationships are still an important asset to this
Company, only about 20% of American Income�s new sales come from
union leads. The Company has diversified over the years by focusing
heavily on other affinity groups, third party internet vendor leads, and
referrals to help ensure sustainable growth.

Liberty National markets both individual and worksite products. Over
the years, Liberty National has been transformed from a fixed-cost
home service model to a variable-cost model. With this transformation,
we believe Liberty National is positioned for long-term success. Life
premiums have grown now for two years in a row, after many years of
flat to declining premiums.
As can be seen below, total sales and agent counts have grown at a
compound annual growth rate of 9.5% and 8.6%, respectively, over the
past five years. This growth is being driven through the efforts of the
agency owners who develop people to recruit, train, and retain new
agents and eventually open their own offices in new territories. We are
encouraged by the continuing turnaround at Liberty National and are
enthusiastic about Liberty National�s long-term growth prospects.
Torchmark acquired Family Heritage in 2012. Since 2013, health sales
and agent count have grown 6.4% and 9.6%, respectively, driven by
strong recruiting and agent count growth. We expect strong growth as
Family Heritage continues to improve recruiting and development of
agency middle management.
Family Heritage�s exclusive agency primarily markets limited-benefit
health products in rural areas and smaller cities. Most of these
products include a return of premium feature that generates better
persistency, margins and investment income than typical health
insurance products.


Direct Response is our second largest distribution channel. It began
operations in the 1960s as a direct mail only distribution channel.
Over the years, we have added insert media and electronic media
distribution. Having this three-pronged approach provides us the
ability to monetize leads and reach consumers more effectively than
competitors in the direct-to-consumer space.
While Direct Response has a long history of sales growth, sales have
declined in the past few years due to operational changes made in
response to higher than originally expected claims which started
to emerge in 2015 in certain blocks of business. These operational
changes were designed to maximize margin dollars, knowing that sales
would be negatively impacted. We have seen response rates stabilize
and expect to begin to increase marketing activity and sales in 2019.
In addition to generating direct-to-consumer sales, Direct Response
also provides critical support to the recruiting, lead generation
and data management efforts of our agencies. We expect
Direct Response to continue to be a key contributor to Torchmark�s
long-term success.

United American markets Medicare Supplement insurance through
independent agents and brokers to individuals and union and employer
groups. While we focus primarily on life insurance at Torchmark, we do
like the Medicare Supplement market. It provides stable profit margins
and we have the expertise and the infrastructure to administer this
business efficiently. That being said, the Medicare Supplement market
is very different than the markets we serve through our exclusive
agencies. It is a highly regulated, competitive market that is relatively
easy for companies to enter and exit. As such, we take an opportunistic
approach to this business.
While short-term sales trends are difficult to predict because of the use
of independent distribution, we have seen significant growth over the
last several years. This unit has a stable in force block that has continued
to grow as shown in the chart below.

Excess investment income is net investment income less the required
interest on net policy liabilities and the interest on our debt. We use
excess investment income as a measure to evaluate the performance
of the investment segment. Excess investment income produced 27%
of our pre-tax operating income in 2018. 

Growth in excess investment income has been hampered for several
years by the low interest rate environment. While we have grown
investment income, it hasn�t grown at the same rate as our invested
assets. As we go forward, we expect to be investing new money at
yields closer to the yields on bonds coming off our books through
maturities and other dispositions. As such, we should soon begin to
see investment income grow at the same rate as our invested assets. 

Due to the underwriting margins produced by our insurance operations,
we don�t have to rely on investment income to generate earnings. While
we seek to maximize risk-adjusted, capital-adjusted returns, the most
important criteria when considering new investments is preservation
of principal.
Our investment philosophy is shaped by the nature of our liabilities.
We invest almost exclusively in long-term fixed maturities as these
fixed-rate investments best match our long term fixed-rate liabilities
and enhance our ability to manage capital as efficiently as possible.
Because we invest long, an important investment criteria is to invest
only in entities that we believe can survive through multiple cycles.
While we have a relatively high portion of our portfolio in BBB securities,
we believe they produce the best risk-adjusted, capital-adjusted return.
Furthermore, we don't own higher risk assets such as derivatives and
equities, and have only minimal ownership of commercial mortgages
and asset-backed securities.


As can be seen in the chart above, invested assets have grown from
about $10 billion at the end of 2008 to just under $17 billion in 2018.
This growth was achieved in spite of the fact that we spent $3.9 billion
to repurchase shares over that period.

This chart demonstrates the negative impact of lower interest rates on
investment yields over the last ten years. We hope for higher interest
rates. We are not concerned about the possibility of unrealized losses
resulting from higher interest rates as we have the intent and more
importantly, the ability, to hold our investments to maturity.

Capital management is an integral component of our business model
and strategic decision-making process. We seek to manage capital in a
manner that best benefits our shareholders.
We currently manage to a Company Action Level Risk-Based Capital
(RBC) ratio target of 300% to 320%. We are able to maintain a
significantly lower RBC ratio than many similarly rated peers due to the
lower risk profile of our business and our strong underwriting profits
and cash flow generation. We simply do not need as much capital due
to the consistency of our earnings and excess cash flow generation and
the fact that we primarily have fixed rate liabilities that are not subject
to fluctuations in interest rates or equity markets.
The next chart illustrates the significant excess cash flow generated
routinely at Torchmark. We define excess cash flow as the cash available
to the parent company from the dividends received from the insurance
subsidiaries after paying dividends to Torchmark shareholders and
interest on debt.

Excess cash flow was somewhat lower at Torchmark in 2016 and 2017
due to slight declines in net statutory income. These declines resulted
in part from strong life sales growth and investments in technology.
While life sales growth and these technology investments are
detrimental to statutory net income in the short run, they will produce
growth in excess cash flow in the long run. We expect cash flow to be
up to around $350 to $370 million in 2019.
This excess cash flow is a component of our liquidity. Much discussion
has taken place lately regarding the possibility of a downturn in
the credit cycle. While we do not expect a major recession, we are
comfortable that we have more than sufficient liquidity to handle
any credit migration and defaults that could occur even in a severe
scenario. 

At our current RBC level of 326%, we are holding $100 - $120 million
more capital than needed for the RBC target set by rating agencies
for our current ratings. In addition, we have a buffer of $50 million of
liquid assets at the Parent Company. This gives us a total of $150 to
$170 million of capital to handle migration or defaults without having
to issue any debt or divert excess cash flow from share buybacks. As
a point of reference, the $150 million to $170 million is greater than
the amount of capital we put into the insurance companies in 2008-
2010 during the global financial crisis.
Even if we were somehow required to infuse more capital than we did
during 2008-2010, we currently have approximately $475 million of
borrowing capacity. In addition, we consistently generate well over
$300 million of excess cash flow, after shareholder dividends, every
year. Even under an extreme stress scenario, we would expect to see
minimal, if any, impact to our share repurchase program. 

We began our share repurchase program in 1986 and have bought
back stock every year since then except for 1995 after we purchased
American Income. Since 1986, we have spent $7.5 billion to
repurchase 80% of the outstanding shares of the Company and
have returned approximately 71% of our net income to shareholders
through dividends and share repurchases. While share repurchases
have been the most efficient use of excess capital over the years, we
continually evaluate alternative uses to help ensure we maximize
shareholder value. Please keep in mind that we repurchase shares
using excess capital only. Our first priority is to fully fund and grow our
insurance operations. 

As we look ahead, we are very optimistic regarding Torchmark�s future. We have a strong
business model and a tremendous group of employees, agency owners, and agents. None of
Torchmark�s success would be possible without their efforts.
Our agents can take pride in the fact that they are demonstrating the need for life insurance
and then providing financial security one customer at a time in a market where that security
is sorely lacking. Torchmark�s products meet a critical need for working families. They are
designed to help families survive the death or critical illness of a breadwinner and protect their
future by helping pay off a mortgage, college tuition or meeting other critical obligations.
With respect to our employee workforce, we are extremely proud of the dedication and
quality of our team. We have placed a high priority on succession planning, not just at the
executive level, but throughout all areas of the organization. As a result, we have a deep bench
of talent that gives us confidence that our business model can be executed effectively well
into the future.
We believe strongly in a diversified workplace with equal opportunity for everybody.
Torchmark is committed to providing an inclusive and welcoming environment for all. It�s the
right thing to do, and it�s good for business too.
When you consider the nature of our market and the low risk profile of our business, we believe
you will agree that Torchmark is in a position to consistently generate significant shareholder
value for years to come. Thank you for your investment in Torchmark.

GARY L. COLEMAN
Co-Chairman and
Chief Executive Officer

LARRY M. HUTCHISON
Co-Chairman and
Chief Executive Officer 

