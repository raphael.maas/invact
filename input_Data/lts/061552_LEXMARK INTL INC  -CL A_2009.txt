To my fellow shareholders,
The past year has been challenging as the current economic downturn impacted our industry, with every
competitor experiencing significant declines in revenue. We believe 2009 showed our strategy is working
because we outperformed the market and gained share in our focus segments of workgroup lasers and
business inkjets. We believe Lexmarks competitive position is stronger today than before the start of the
economic downturn, and weve done this while continuing to maintain a strong financial position.
Making Lexmark a leaner, stronger company
Our objective during this downturn has been to make Lexmark a leaner, but stronger, company with a stronger
competitive position. Accordingly, we took a number of actions to reduce our cost and expense, while continuing
to fund our core strategic initiatives. Lexmarks core strategy is to focus on specific segments of the market,
workgroup lasers and business inkjets, and leverage Lexmarks strengths in vertical technology integration,
solutions and services to differentiate ourselves from competitors and bring value to our customers.
During 2009, we announced three separate restructuring actions to reduce our cost and expense. These
actions, along with our prior restructurings and other expense actions enabled us to reduce our operating
expense more than $200 million in 2009. We also continued to improve our working capital performance
through the year. While becoming leaner in 2009, we continued to fund and pursue our core strategic initiatives
in research and development, marketing and sales, and solutions and services.
Strengthening our competitive position
In 2009, we launched a number of new products improving our competitive position in color lasers, laser MFPs
and business inkjets. According to our internal assessment for the U.S., Lexmark received 38 percent of the
industry laser product awards, which is more than three times the number for the next closest competitor, and
received 28 percent of industry inkjet product awards, up significantly from 2008 and making us No. 2 in inkjet
awards received.
We have also strengthened our path to market through expansions of our shelf space in U.S. office superstores.
In our small and medium business channel, we continued to expand the number of value added resellers who
are offering Lexmark products to their customers.

Performing better than the market
Even though our fi nancial results were impacted by the diffi cult economic conditions in 2009, we believe our
strategy is working as we performed better than the market last year. As a result of the improvements in our
product line and path to market, our revenue decline of 14 percent in 2009 was less than the industry average
decline. We had good growth in 2009 in both our color laser and laser MFP units, signifi cantly outperforming
the market. We also continued to have strong growth in 2009 in our managed print services business as we
continued to help our enterprise customers optimize their output environment and improve their paper based
workfl ow. This success enabled us to gain branded unit share in the workgroup laser segment. In inkjet, the
positive reception of our new products, our expansion in U.S. offi ce superstores and the strong growth in the
retail sell out of our Professional Series products, enabled us to gain branded unit share in the above $100
inkjet segment in our focus geographies of the U.S. and Europe.
Maintaining a strong fi nancial position
Our second half 2009 fi nancial results improved substantially compared to the fi rst half. The improvement was
broad based, and in the fourth quarter we delivered particularly strong growth year over year in net income
and cash fl ow. These fourth quarter results included a return to growth in our laser business and a signifi cant
sequential improvement in the year over year performance of our inkjet business. For the year, net cash from
operations was $402 million, marking the eighth consecutive year of net cash from operations over $400 million,
and the 17th consecutive year of positive cash fl ow for the company. We ended 2009 maintaining our strong
fi nancial position with more than $1.1 billion in cash and current marketable securities.
Our people make the difference
At Lexmark, our greatest strength is our people. I often hear from customers that our people differentiate
us from the competition. Customers tell me that our people listen to them, understand their processes, and
proactively suggest solutions that match their needs. Its the commitment of our employees to our vision of
creating customers for life that enables us to continue to win and renew our business with some of the largest
companies in the world. Their continuing commitment to our customers will enable us to work through the
current challenging economic environment and emerge as a stronger company.
In closing, I want to thank our customers, employees, business partners and investors for your continued support.
Sincerely,
Paul J. Curlander
Chairman and Chief Executive Officer
Lexmark International, Inc.