LETTER TO SHAREOWNERS

GE WORKS
At GE, we look forward with confidence.
That is because we can shape some of the
big growth drivers in any era.

Last fall, we hosted a conference
in Silicon Valley to launch what we
call the Industrial Internet, an open,
global network that connects people,
data and machines. Its about making
infrastructure more intelligent
and advancing the industries critical
to the world we live in. We believe its
about the future of industryenergy,
healthcare, transportation, manufacturing.
Its about making the world
work better.
At the conference, we put a GEnx
engine on the stage. People posed
for pictures with the engine; they
marveled at the technology and its
sheer size. It was a reminder of two
things. First, few companies can do
what GE does; the scale we operate on
and our decades of investment are a
competitive advantage. Second, in an
uncertain economy, long-term growth
and competitiveness require the endless
pursuit of innovative productivity.
Similarly, I recently returned from
Sub-Saharan Africa, a region that was
off the radar when I became CEO.
Today, we are at a $3 billion annual
run rate, and that could double in the
next few years. GE could have $1 billion
Franchises in Nigeria, South
Africa, Mozambique and Angola. We
are investing in capability and people.
There are very few American companies
in the region. But we could sell
more gas turbines in Africa than in the
U.S. in the next few years.
A GE annual report has never fully
featured software and Africa. Today,
we feel they are essential and we
can lead. Our ability to create our
own future is why GE can win in
any environment.
It starts with a culturethe foundation
for any successful enterprisea
culture that inspires our people to
improve every day. Our team is missionbased:
We build, move, power and
cure the world. We believe in a better
way: We constantly learn from our
customers, our competition and each
other. We seek solutions for our customers
and society. And we are a
We Company. We know that strong
teams with great people outperform
individuals. That is why GE Works.
The global economy for 2012 was
within our planning scenario, but short
of our hopes. Maybe the best news
believe it or notwas Europe. It didnt
implode! The U.S. is improving, driven
mainly by housing and the consumer,
but capital investment remains sluggish.
As a result, the U.S. continued
its weakest recovery since the 1930s.
China slowed as it went through a
political transition. Because of a weak
macro environment, we were able to
lower input costs, and that had a positive
impact on our margins.
We expect 2013 to be another typical
year in the Reset Era. We remain
confident in the economic strength of
the emerging markets. We are encouraged
by renewed growth and reform
in China, which has a positive impact
on other big resource-rich regions like
Africa, Latin America and the Middle
East. At the same time, we are in
unprecedented fiscal territory in the

U.S. and Europe, which will keep a limit
on growth in the short term.
The major source of volatility in corporate
planning is the U.S., something I
never thought I would see. We would
all like to believe that the U.S. will continue
at a steady rate of 3%4% GDP
growth, as we saw in the 30 years
before the global financial crisis.
However, the U.S. faces more major
political storms this year: the fiscal
situation, repeated debt-limit controversy
and tax reform. We fear that this
uncertainty will impact capital investment.
And the amount of regulation
tends to grow during periods of fiscal
strain, and we are certainly seeing that
in the U.S. The number of major regulations
regulations with more than
$100 million of impacthas exploded in
the last few years. The result has been
an additional burden on business.
Until we solve for these constraints, it
is hard to see that the U.S. will return
to its full growth potential.
We have demonstrated that GE can
perform in this environment. In 2012,
we grew our segment profits by 11% to
$22.9 billion. We generated $17.8 billion
of cash from operating activities (CFOA),
up 48%, and returned $12.4 billion of
cash to investors through dividends and
stock buybacks. Our total shareholder
return grew by 21%, well ahead of the
16% growth in the S&P 500. Our market
cap grew by about $30 billion, and
we remain the seventh most valuable
company in the world.
We like the way GE is positioned in
this environment: a great portfolio
of world-class, technology-leading
businesses; a strong position in fastgrowth
global markets; leading-edge
service technologies that achieve customer
productivity; high visibility with
a backlog of $210 billion; and a strong
financial position. We want investors
to see GE as a safe, long-term investment.
One with a great dividend that is
delivering long-term growth.
FIVE CHOICES THAT DRIVE
THE FUTURE
Strategy is about making choices,
building competitive advantage and
planning for the future. Strategy is
not set through one act or one deal.
Rather, we build it sequentially through
making decisions and enhancing
capability. As we look forward, it is
important that investors see the
Company through a set of choices we
make for the purpose of creating value
over time.
First, we have remade GE as an
Infrastructure Leader with a
smaller financial services division.
We like infrastructure markets because
they are growing and because they
utilize GE capabilities in technology,
globalization, financing and customer
relationships. About $60 trillion of infrastructure
investment is needed by 2030
to support billions of new consumers
joining the middle class in the emerging
world, and to support developedmarket
productivity. At $100 billion of
revenue with 15% margins, we are the
largest and most profitable infrastructure
company in the world.
Over the last decade, we have grown
our infrastructure platforms by
investing in adjacencies, pursuing
opportunities that are closely related
to our core. About one-third of our
infrastructure revenues comes from
businesses we werent in a decade
ago. These include fast-growth businesses
like Oil & Gas, Life Sciences, and
Distributed Power. This growth has
come through organic investment and
focused acquisitions.

At the same time, we are creating a
smaller, more focused financial services
companyone that has a lower
risk profile and adds value to our
industrial businesses. We will continue
to reduce the size of GE Capital from
the $600 billion of assets it was in 2008
to a goal of $300$400 billion in the
future. GE Capital has a sound fiscal
position, with Tier 1 capital above 10%
and strong liquidity. We can generate
returns above our cost of capital. Over
the next few years, we plan for GE
Capital to return about $20 billion of
dividends back to the parent. We will
purposefully reallocate capital from
financial services to infrastructure and
grow it faster. Our goal is to have infrastructure
earnings reach 70% of our
total over time.
We have dramatically simplified GE
over the past decade. The last major
portfolio move we made was exiting
NBC Universal (NBCU). In the first
phase, we sold 51%, and reallocated
$11 billion from the proceeds to purchase
new platforms in Energy and
Oil & Gas. These businesses already
have generated $1 billion of earnings
and are growing 20% annually.
Recently, we announced an agreement
for the disposition of the remainder of
NBCU, and its real estate, for $18.1 billion.
This creates additional cash
for value creation in the short term,
through increased share repurchase
and investment in growth.
Second, we are committed to allocating
capital in a balanced and
disciplined way, but with a clear
priority for dividend growth. GE will
generate $100 billion for allocation over
the next few years, including cash from
existing operations, dividends from GE
Capital and dispositions.
The top priority remains growing the
dividend. Since 2000, we have paid out
$106 billion in dividends, more than
any company except Shell, and more
than we paid out in the first 125 years
of the Company combined. We like
GE to have a high dividend yield,
which is appealing to the majority of
our investors.
We plan to buy back shares to get
below 10 billion, where we were before
the crisis. We will make significant
progress toward that goal in 2013 by
allocating a significant portion of the
NBCU cash to repurchase our shares.
In total, we plan to return $18 billion to
investors this year through dividend
and buyback.
We will continue to execute on focused
acquisitions, a capital-efficient way to
grow the Company. We will keep our
focus on acquiring specific capabilities
where GE can add substantial value.
We can execute on a few of these
each year.
Third, we have significantly increased
investment in organic growth, focusing
on R&D and global expansion.
In doing so, we have invested ahead
of our competition. We believe that
investing in technology and globalization
is key to gaining market share.
Annually, we invest more than $10 billion
to launch new products and build
global capability. We make these
investments with the full benefit of
GEs scale.
Over the past decade, we have
doubled our annual R&D investment,
increasing $2$3 billion to 5%6% of
revenue. Because of this investment,
we have progressed from a company
that can launch one new commercial
engine each decade to a company
that can launch one each year. We
will launch 10 new gas turbines this
decade, significantly more than in
previous times. We are a broader and
deeper technology leader than at any
time in our history.
We have built a company that has
high share in growth regions. In 2012,
we had $40 billion of orders in growth
regions, a 12% increase over the prior
year and a threefold increase in the
last decade.

Achieving these results has required
substantial investment in capability
and people. Between 2010 and
2014, we are making 30 investments
in manufacturing, research, services,
customer innovation and training in
growth markets. We have developed
and repositioned our leaders to capitalize
on growth-market opportunities.
We have 10,000 commercial resources
focused on the needs of our customers
in growth markets. We have achieved
local relevance with global scale.
We use the entire GE enterprise to
improve the value of our investments
in technology and globalization.
For technology, we have a Global
Research Center Network that builds
strategic capability, spreads technology
around the world and innovates
for local markets. We have a Global
Growth Organization, led by a GE Vice
Chairman, that allows GE to better
compete by using our talent.
Fourth, we have built deep customer
relationships based on an outcomesoriented
model. Our growth is aligned
with customer outcomes, and our
products improve their productivity.
We have grown our service revenue
from $21 billion to $43 billion over
the past decade. Services represent
about 75% of our industrial earnings.
With $157 billion of service backlog,
we have the momentum to grow in
the future.
We believe in a solutions-oriented
selling model, one that can deliver outcomes
for customers. In Healthcare,
we are aligned around the major
accounts so that we can help them
transform ahead of U.S. healthcare
reform. In Oil & Gas, we deliver comprehensive
technical solutions for our
customers. In Aviation, we create value
through the performance of our new
technologies. We only win when our
customers win.
Fifth and finally, we have positioned
GE to lead in the big productivity
drivers of this era. This is important
for growing our margins while keeping
our customers competitive. The
levers of productivity are constantly
changing. For more than a century,
GE has been a leader in productivity
and innovation.
We will lead in the shale
gas revolution. The volume of
and access to shale gas and other
unconventional resources in the U.S.
(and other regions) will change the
competitive balance in energy for a
generation. This gives the U.S. one of
the lowest costs of electricity in the
world and the chance to be an energy
exporter. Big industries
like railcould convert from diesel to
gas. The option of becoming energyindependent
is now possible for North
America. Through our Oil & Gas business
we can provide important content
in extraction, development, and environmental
protection of shale gas. We
are the world leader in gas-powered
generation and transportation.
We are extending GEs lead
in advanced manufacturing.
Manufacturing is a major source of
competitive advantage. After decades
of outsourcing capability, we now see
companies rebuilding their manufacturing
strength. Companies used
to make investment decisions purely
on labor cost. However, there are
new materials that can revolutionize
performance, and precision technologies
and high-power computing are
transforming how we manufacture.
GE will insource more manufacturing
content. We are investing in processing
technologies such as additive
manufacturing. In the future, we aspire
to reduce the cycle times for complex
systems and lower cost.

We are making a major investment in
software and analytics. We know that
industrial companies need to be in the
software business. We want to make
the analytics around our products,
real-time data, and operating solutions
a GE core competency. We have built a
Software and Analytical Center of
Excellence in California, where we are
adding a vast array of human talent to
achieve our goals. We know that our
services in the coming year depend
on building smarter machines with
the ability to extract and analyze data.
We will be a leader in analytics. And
that will make GE more valuable to our
customers. This is the power of the
Industrial Internet.
The reason why analytics are important
in the infrastructure industry
relates to what we call The Power
of 1%. Across our customer base,
improving asset performance by 1%
can add $20 billion of customer profit
annually. In our world, small changes
mean big outcomes.
For investors, we have defined where
we play, how we win, our capital allocation
priorities and investments for
the future. These five choices will set
up our performance and drive our success
over the long term.
WE ARE EXECUTING ON
OUR COMMITMENTS
Last year, we set focused execution
goals for GE: double-digit industrial
earnings growth; margin expansion;
restarting the GE Capital dividend to
the parent; reducing the size of GE
Capital; and balanced capital allocation.
We achieved all of our goals for
the year.
Our businesses are performing.
In 2012, our industrial segment earnings
grew by 10% to $15.5 billion. Our

industrial segment organic revenue
growth was 8% and margins grew by
30 basis points, both metrics comparing
favorably to peers. Growth was
broad-based; all of our reported segments
grew earnings for the first time
since 2006. We finished the year with
$210 billion of backlog, a record for
the Company.
We grow our industrial businesses
by pulling the same levers. We lead
with technology, invest in fast-growth
markets, drive value in the installed
base, invest in adjacencies and
grow margins.
Oil & Gas is our fastest-growing business,
with revenue of $15 billion and
earnings growing 16%. We compete
in high-growth markets. We are
investing to launch new products fully
utilizing our broad technical capability.
For instance, we launched the first
subsea compressor at Statoil, creating
an industry-leading position. Our
orders grew by 16% in the year, and
we are winning new business around
the world.
Our Power & Water business grew
earnings by 8% in 2012, and we expect
to be about flat in 2013. We are wellpositioned
for long-term growth in
natural gas power generation, distributed
power, and services. However,
Wind power generationwhere GE
leadsis more volatile. We had a very
strong year in 2012 but, due to U.S.
regulatory uncertainty, this year will
be difficult. Based on strong global
demand with expanding service,
we expect Power & Water growth to
resume in 2014.
Over the next few years, we see
earnings upside by improving
our performance in markets like

Energy Management. This business
is complementary to our core infrastructure
franchise, yet our share is
less than 10% and our margins are
lower than those of our competitors.
We are seeing outstanding opportunities
for growth in power conversion
and digital energy.
We expect another year of strong
industrial performance in 2013.
Oil & Gas, Aviation, Healthcare
and Transportation should hit
all-time-high earnings in 2013.
Our plan targets 10% industrial
earnings growth.
GE Capital had earnings of $7.4 billion,
up 12%. Our Tier 1 common ratio is
10.2%, well above the regulatory goals.
GE received a $6.4 billion dividend
from Capital. Our team has done a
great job of reducing commercial real
estate exposure, which was $46 billion
at year-end, down 50% from its peak.
GE Capital continues to outperform
regional and money center banks in
important areas like net interest margins
and losses.
The core of GE Capital is being a
leading lender to middle market
customers, building on our deep
experience in, and understanding of,
these markets and assets. In businesses
like sponsor finance, aircraft
leasing and retail services, and middle
market lending and leasing, GE Capital
has deep domain experience and will
continue to grow.
Our initiatives are delivering results.
We drive cross-company initiatives
to generate organic growth and
improve margins. We review our
progress against our long-term goals
and through relative performance of
20 industrial peers and large banks.
Our performance is near the top in
almost every category, but we still
have room for improvement.
We have built broad technical capability
that can deliver big systems
and foster innovation. Each year we
file about 2,000 patents in the U.S.,
putting GE in the top 10 for innovation.
GE engineers and scientists
from around the world collaborate
and demonstrate a real culture
of execution.
GE products deliver vast customer
value. Over the next few years, we will
transform our aviation engine product
line with several new models, including
the launches of GEnx, the CFM LEAP
and the GE9X. Each engine will
improve airline fuel efficiency by 15%,
while reducing emissions. By 2020, we
will have 46,000 GE engines in service,
up from 4,100 in 1990. That is a product
of our technical expertise.
In Healthcare, we are launching highmargin
products at every price point
and across all modalities. This is how
we win around the world. In computed
tomography (CT), we launched the
Discovery CT750 HD FREEdom, a highend
scanner, which can greatly reduce
radiation dose. At the same time, we
launched the CT Brivo, sold at 20% of
the price of a high-end scanner, which
is growing share in global markets. In
fact, in the first six months, we sold
100 Brivos to Chinese customers,
many of whom had never owned a CT.
We leverage technology to launch
new businesses. Our Energy Storage
business is a great example of how we
innovate and bring to scale state-ofthe-
art technologies. Researchers in
our GRC labs invented a new battery
based on technology from more than
30 patents. GE teams also designed an

advanced manufacturing process to
build the battery efficiently. In essence,
we created a start-up within the
Company, and we expect the business
will generate more than $1 billion in
revenue annually in just a few years.
Our growth-market revenue
expanded by 11% to $37 billion. We
have a segmented global strategy.
We aspire to grow in China. We will
lead the industrialization of resourcerich
regions. And we will retain our
operating presence in Europe as
it restructures.
GE has a strong franchise in China that
grew by 19% in 2012. Our advantage
is in localization and partnerships. Last
year, we opened two customer innovation
centers, in Chengdu and Xian.
At the same time, we are partnering
with Chinese state-owned enterprises,
achieving global scale. In 2012, we
announced a joint venture with XD,
a Chinese leader in transmission and
distribution equipment, and digital
energy. This allows us to capture global
growth, in an industry where we have
low share, with a Chinese cost base.
Our other fast-growth global opportunity
is in resource-rich countries,
where we have built a competitive
advantage. From Latin America to
the Middle East to Africa to Russia to
Australia to Canada, their goals are
the same: converting resource wealth
into industrial expertise and jobs. In
these regions, we remain committed
to a Company-Country approach.
Last year we announced a $1 billion
investment in Saudi Arabia across four
GE platforms.
The payback from global investments
is huge. Our recent acquisition in
Power Conversion received $600 million
of orders in Brazil alone by
leveraging GEs presence and relationships
in the country. Similarly,
GE Transportation is building a
$1 billion business in Russia and
Kazakhstan, based on local capabilities.
Excellence in localization is a
GE competitive advantage.
GE has a productive manufacturing
and engineering base in Europe. We
recently entered into an agreement
to acquire Avio, an Italian high-tech
aviation supplier that would add to this
base. While Europe may remain sluggish
for a while, we have an important
installed base and smart and dedicated
teams helping our customers.
We will continue to be a good partner
for Europe, sustaining a robust manufacturing
and engineering base.
Services growth was 4%, fueled
by a growing installed base and
expanding content. The Industrial
Internet is revolutionizing the services
we provide our customers, helping
them to become more productive
operations. GE will leverage our vast
service backlog to develop technologies
that enhance the performance
of our productsand the enterprises
in which they operatewhile growing
our dollars per installed base. The
Industrial Internet is built on intelligent
machines, advanced analytics and
people at work that can save airlines,
railroads, hospitals and utilities billions
of dollars each year.
The impact of these solutions is
being felt by customers across various
industries. Norfolk Southern is a
large North American Class I railroad
and our launch customer for GE Rail
Network Optimization. This solution
uses data and analytics to improve
operating decisions across the entire

transportation network, including
railroads, shippers, intermodal terminals
and repair shops. They estimate
that every 1 mph increase in network
speed saves them $200 million in
annual capital and operating expense.
In Healthcare, weve deployed our
Hospital Operation Management
(HOM) solutions in more than 50 hospitals.
HOM tracks hospital assets to
ensure that quality care is delivered
across a patients stay, from admission
to discharge. The HOM launch customer
was Mt. Sinai Hospital, where
we help them track 15,000 assets and
have shown a 10% improvement in
patient throughput.
We are taking a few bigger swings
where we are improving the enterprises
in which our assets operate. In
Aviation, to address the fleet performance
of global airlines, we launched
Taleris, a joint venture with Accenture.
This analytical tool will allow airlines
to predict maintenance events
before they happen. The goal is Zero
unplanned downtime. Taleris aids airlines
such as Qantas and JetBlue with
their fleet performance, maintenance
and operations, allowing them to save
millions of dollars annually through
more efficient use of their airplanes.
Margins grew by 30 basis points to
15.1%. Our goal is to grow another
70 bps in 2013. We will achieve this by
improving processes while reducing
structural cost.
We are in our third year of GE
Advantage, our process-improvement
program. Our teams are improving
on 40 big processes throughout the
Company. I review each of these frequently,
and we realized $800 million
of margin improvement in 2012.
One good example is our
Transportation: Requisition to
Platform process, which facilitates
our new product launches. Results
so far include: 80% system reuse; sixmonth
reduction in cycle; and a 35%
reduction in sole-source suppliers and
overtime. GE Advantage helps us sustain
a competitive advantage.
We are also simplifying the way we
run GE, with an eye to lowering
our structural cost and improving our
speed. In 2011, our selling, general
and administrative expenses (SG&A)
as a percentage of industrial sales
were 18.5%. We are aiming to reduce
this structure to below 16% by 2014.
This is a total of $2.5 billion of cost out;
by the end of 2012, we achieved
40% of our goal.
Another big contributor to better margins
involves attacking product cost
through better design. In our Aviation
business, where we have a solid backlog
for many years, we are reshaping
our supply chain to increase GE content.
We are investing in advanced
materials, manufacturing technological
global capacity. In Appliances, we
launched four new products in 2012. In
2013, we will launch four more. All are
increasing share and margins.
We are executing on our commitments.
In 2013, we would like to: grow
industrial earnings by 10%; achieve
2%6% industrial organic revenue
growth; return a significant amount
of cash from GE Capital to the parent;
and return $18+ billion of capital to
investors in dividends and buyback.
WE CONTINUE TO IMPROVE
OUR CULTURE
Over the holidays, I was reviewing
the annual Gallup poll that rates
institutions in the U.S. Once again, big
business has a low overall rating, with
21% favorability, roughly a third of the
approval for small business. The mood
reflects the economic environment

We are lucky that government can set
the floor with favorability at 13%.
So is size inherently bad? I dont think
so. But size can breed a perversion of
bureaucracy, a sense of entitlement
and a distance from reality. Size is bad
when it crushes innovation. A good
culture is the only filter that can make
size a strength and not a weakness.
Over the past year or so, I have made
it a priority to personally connect with
entrepreneurs and venture capitalists.
I wanted to understand more
about the start-up culture and the
ways that smart entrepreneurs run
their companies.
Now, I dont want to make GE a startup.
GEs great strength is our scale.
GE has more than 40,000 salespeople,
supported by 50,000 engineers and
scientists; we can sell in more than
160 countries with the worlds sixth
most valuable brand.
The trick is to keep all of that, but without
the bureaucracy and arrogance
that can often accompany size.
The fact is that GE was becoming too
complicated. We were simply working
on too many things that arent important.
We had too many checkers
and not enough doers. Visiting with
entrepreneurs has helped me focus
on complexity, accountability and
purpose. I have found two books
The Lean Startup and The Startup
Playbookto be particularly useful.
Entrepreneurs simplify everything.
They are purpose-driven. They focus
on customers, people and solving
problems. They do fewer things, but
with bigger impact. They dont delegate
important decisions; rather,
they position decision-makers close
to the action. There is no headquarters,
no layer of checkers. They use
judgment, they move fast, and they
are accountable.
The unique leadership movement
inside GE today is Simplification.
Part of it is structural. We want the
Company to be lower-cost, have
shorter cycle times, and match authority
to accountability. And we need to
accomplish this across multiple platforms,
in diverse markets, living in an
era of hyper-regulation.
But the other part of Simplification
is cultural. Big companies fail when
they lose a culture of accountability
accountability for outcomes.
We must compete with purpose. And
we must deliver outcomes for customers,
investors, society and each other.
We are building processes that drive
speed, accountability and compliance.
We are committed to long-term thinking
despite volatility in the current
environment. The decisions we are
making today will shape the Company
for years to come. GE can execute on
a scale few can match. I hope, as an
investor, that makes you proud.
So, what is leadership? It is the harnessing
of culture, the culture of GE
Works. We are mission-based. We
search for a better way. We drive
solutions for our customers and
society. We are a We Company. It is
driving accountability for outcomes.
It is fostering smart risk-taking and
business judgment.
You have invested in GE. You know the
choices we have made for the next
decade. You have seen our execution
and the key metrics we use to manage
the Company. You have a sense for our
culture and leadership team. You will
see this reinforced in the rest of this
report. Let me know what you think.
You can e-mail me at ask.jeff@ge.com.
Jeffrey R. Immelt
Chairman of the Board
and Chief Executive Officer
February 26, 2013