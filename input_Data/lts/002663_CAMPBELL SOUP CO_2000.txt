A Wake-up Call
Fiscal 2000 was a very disappointing 
year. Our sales declined 2 percent to $6.3 billion and earnings per share declined 4 percent to $1.65. All our employees want to forget the year just passed. But they can't. And that's the spur. We all want to fight back to the Winner's Circle. We want to regain our pride as professionals. We want to resume wealth building for our shareowners. We know that these aims are all linked to delivering competitively superior sales, profit, and cash results. And we know that these results do not truly reflect the experience and potential of our people power. That is why fiscal 2000 was indeed a wake-up call.

Evaluation - Lessons Learned
We have spent a lot of time in self-examination, and we've drawn some clear conclusions: 
1. While Campbell Soup Company had a poor year overall, many operating entities set new records. This was the case with, for example, Australia, Belgium, France, Germany, Godiva, Indonesia, Malaysia and South Asia, Mexico, and New Zealand. These operations leveraged brand power and attracted new consumers as they built sales and market share. They linked with trading partners to attract consumers and accelerate profitable sales.
2. Underperformance was centered in the United States and included Campbell's soups, sauces, and Pepperidge Farm. Consumers did not reach for our brands with the same love and loyalty as before. "Why?" we asked.
3. The answer lay, in part, in the fact that the competition was very much alive. But it also rested more heavily on our slow-down of creativity and innovation. We failed to prove our product relevance with consumers in a dynamic marketplace.

This was especially true for icon Campbell's soup brands. We under-delivered on marketing power. The consumer voted at the cash register. Although Campbell remained the wet soup market leader with over 70 percent of the U.S. market, the year saw a slow-down in consumption

Winning With The Consumer
For many years at Campbell Soup, we have talked of People Power driving Brand Power. Our market leadership in many product categories is a tribute to those powers. Campbell's, V8, Pace, Pepperidge Farm, Homepride, Erasco, Liebig, and Arnott's for example - are all #1 brands in their respective markets. However, we must always remind ourselves that market leadership is bestowed by the ultimate power - the consumer. Long-term wealth building is the outcome of having passionate and dedicated consumers. Every month, hundreds of millions of buying decisions drive our brand results. Winning with the consumer is the end game. Winning with the consumer dictates which brand wins and which ones lose. There are no guarantees. Brand leadership must be earned and then reaffirmed every year. That's the challenge we face and we accept it with a new urgency.

Resurgence - People Power 
As fiscal 2001 moves into the fall season in North America, we have marketing programs that are loaded with lessons learned. People power is turned on; you can judge that by the innovations and consumer meaningful points of difference that are highlighted across the range of Campbell brands presented on the pages of this Annual Report. 

Commitment To Wealth Building
We are resolute in our commitment that our prime purpose is building shareowner wealth. To deliver on this commitment we must win where it counts - with the consumer, with repeat purchases. This is achieved by delivering a sustainable competitive advantage in our brands - by beating the competition. Then we will again deliver superior returns for our shareowners. And return to the Winner's Circle. And again be known as long-term wealth builders.

This jingle has been named as one of the top five American advertising jingles of the last 100 years. At Campbell Soup Company, it is our anthem. It links to the promise that goes out on mailed correspondence from our Camden World Headquarters.

That promise says simply, "Everything we make contains our business reputation." It is fitting that, at the dawn of a new millennium, "M'm! M'm! Good!" will once again be in the forefront of consumer advertising, and fitting indeed that it will be used for our first and most famous range of products - Campbell's Red & White condensed soups. What's your favorite? 

I'm looking forward to an "M'm! M'm! Good!" fiscal 2001 year. 

