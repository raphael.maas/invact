Letter to Shareholders
Dear Fellow Shareholders:
This past year has been an extraordinary time for all of us. The stability of our lives has
been shaken, and we began fiscal year 2002 in an atmosphere of uncertainty. At that time, after
careful consideration, we shared our feelings about the future with you. We believed fiscal 2002
would be a good year for IGT despite the pervasive pessimism that was a prominent feature of the
business landscape. After communicating our optimism to you, the employees and management
of the IGT family went out and made it happen. Three financial records were achieved during the
year:
1. Combined, revenues and earnings of unconsolidated affiliates reached $1.9 billion,
up from $1.3 billion in fiscal 2001;
2. Operating income increased 35% over last year; and
3. Adjusted net income per share was $3.36, up from $2.83 in fiscal 2001.
When you remember that at the close of fiscal 2001 we were still reeling from September 11th,
with a weak national economy and our own industry struggling in the aftermath of those horrible
events, our financial records are even more remarkable. We were able to communicate optimism
about IGTs growth potential based on the companys financial strength, market diversity,
unmatched technological and game design capabilities, and our first-rate management team.
IGTs secret weapon was the work ethic, resolve and talent of our employees.
Let me share with you some of our achievements. Our primary accomplishment for fiscal
2002 was the completion of the Anchor Gaming acquisition, which presented us with significant
strategic opportunities.
 It gave IGT ownership of important technology and intellectual property, such as the
wheel patents, and important patented portions of our ticket-in/ticket-out (TITO)
technology.
 It significantly increased our presence in the public gaming sector, which turned out to
be a strong source of replacement demand this year.
 It expanded our activities into several areas of legal gaming where we previously
had no presence, including online lottery systems, further enhancing the diversity of our
revenue sources.
 It brought strong management talent such as T.J. Matthews, our Chief Operating Offi-
cer, to IGT.
 Further, the Anchor acquisition met the requirement for immediate earnings accretion,
which is a prerequisite to IGT merger prospects.
The second most notable achievement for the year was aggressive EZ Pay penetration, which
strongly influenced replacement demand for the year. These numbers and milestones are signifi-
cant:
 IGT went live with 15 additional EZ Pay systems during our fourth quarter, bringing
us to a total of 95 systems installed as of the end of fiscal 2002.
 We completed 54 EZ Pay installations during fiscal 2002, more than doubling the
number of systems online.
 The total number of TITO machines sold to date (by all suppliers) more than doubled
during fiscal 2002 to over 70,000 at fiscal year end.
As the numbers demonstrate, tickets have quickly become a must have feature of the
modern casino. There are three primary reasons for this: (1) players quickly come to expect
tickets once theyve experienced them, because they are neat, clean, quicker than cash and not
as heavy as a bucket of coins; (2) casino operators are discovering that tickets reduce overhead,
labor and other costs by as much as 45%; and (3) tickets reduce downtime for machines by reducing or eliminating hopper fills and speeding up hand pays. For these reasons, player satisfaction,
reduced overhead, and less machine downtime, earnings per machine increase.
In a year of sharply curtailed capital spending by businesses worldwide, IGT increased
product sales revenue to a record $846 million (up from $824 million in fiscal 2001) and brought
total unit sales to a record 124,000 (up from 120,000 in fiscal 2001). We were able to grow
our product sales business and increase our market share in a very challenging environment,
thanks to our ongoing commitment of time, talent and money to research and product development.
This led to our introduction of over 70 new games in fiscal 2002, our best suite of games
ever.
Domestically, strong replacement sales, driven primarily by TITO, accounted for the bulk
of product sales and led us to set a new annual record of 40,700 replacement machines sold.
Great geographic diversification meant that the post-September 11th slowdown in tourism that hit
Nevada particularly hard was offset by higher shipments across all of our other markets. Thats the
good news. The better news was that Nevada shipments were, by far, the strongest in the fourth
quarter, and continued to strengthen in early fiscal 2003.
Our international business had its strongest year ever in terms of operating profit. IGTEurope
reported record operating profit for the year. In fact, its profit contribution was the highest
ever by any international subsidiary. In the UK, Barcrest continued as the market leader, ending
the year in excellent position in terms of game design and marketing capability. We are ready to
take full advantage of UK gaming liberalization as it unfolds over the next few years.
Even in fiscal 2002s challenging business environment, our proprietary gaming operations
grew in both revenues and gross profit. We attribute this growth to our ability to manage
our brand mix and our use of brand extensions that allow us maximum flexibility to maintain high
player interest. We also successfully managed our geographical mix by placing more games in
under saturated, higher win-per-unit jurisdictions.
New titles introduced during the year (HARLEY-DAVIDSON, Diamond Cinema, and
Elvira) joined perennial favorite Wheel of Fortune and 2001s The Price is Right in leading
the way to gains in gross profit, yield per unit and size of installed base. We are optimistic about
the opportunities for continued growth in our proprietary gaming segment in fiscal 2003. The drivers,
much like those for product sales, are better games, gaming expansion and new technology.
Were also very encouraged by the prospects for lottery expansion. The 2002 elections
set the stage for growth and expansion in a number of states.
As fiscal 2003 gets underway, our confidence in continued growth remains intact.
The prospects for public gaming are particularly encouraging as a growth driver. Technology
advances, such as EZ Pay and our new Advanced Video Platform, continue to find favor with
casino operators and their customers alike. Replacement buying by our customers continues to
grow. Our game designs just get better and better, adding value to the casino floor to an extent
that is unmatched in the industry. The people of IGT remain committed to developing and building
the best games and products in our industry and keeping IGT a leader on the slot floor.
To that end, we encourage you to thumb through the pages at the back of this report to
take a look at the products and games that we will debut in fiscal 2003. We think they represent
one of the strongest lineups ever, and that alone is enough to cause us to feel good about IGTs
future. Thank you for your continued interest and support.
Sincerely,
Charles N. Mathewson
Chairman of the Board
G. Thomas Baker
President and Chief Executive Officer
