Energy for You summarizes why our business is centered on our
customers and communities. Our employees give their best each
day to deliver the energy that makes lives more comfortable,
productive, and enjoyable.
We have a commitment to be Always There  providing reliable
electricity and natural gas to our customers. We make significant
investments for safety, reliability, and the growing energy
demands of our service territory. Over the next five years, we
plan to spend nearly $7 billion in capital.
To put customers in control, weve made significant investments
in technology. In 2017, customers will be introduced to our new
online preference center, which will give them even more options
for when and how we contact them regarding their billing
options, program promotions, and usage.
To make life better, we endeavor to ensure the safety and
reliability of our systems. By keeping the lights on and the
natural gas flowing, we help our customers enjoy their lives. For
example, we invested in drive-by leak detection tools, which
enable us to check more miles of pipe than ever before to help
maintain the safety of our natural gas delivery system.
To build stronger communities, we volunteer our time and give
our support to make a positive difference in our communities.
In 2016, our employees donated nearly 5,000 units of blood,
which is enough to impact more than 14,000 lives. Giving back
is core to who we are as a company.
2016: A year of strong growth
We had a strong year in 2016, marked by a dividend increase,
growth in earnings, and acquisitions. Our results were driven
by a number of factors, including solid customer growth
in both our electric and natural gas utilities with more than
90,000 additional meters.
Total shareholder return for the company in 2016 was
40.88 percent, outperforming the S&P 500 Utilities Index
of 16.29 percent and the S&P 500 Index of 11.96 percent.
In early 2017, we raised our dividend for the 12th consecutive
year when our board declared a regular quarterly cash dividend
of 26.75 cents per share. This represents a 4 percent increase
from the previous quarterly dividend and, when annualized,
equates to $1.07 per share.
CenterPoint Energy reported 2016 net income of $432 million,
or $1.00 per diluted share. Our annual adjusted earnings, using
the same basis as our guidance, were $501 million, or $1.16 per
diluted share(1). The majority of earnings, $380 million, were
from utility operations, while $121 million were related to our
investments in Enable Midstream, a publicly traded master
limited partnership that owns, operates, and develops strategically
located natural gas and crude oil infrastructure assets.
We continue to look for additional opportunities to grow
earnings. CenterPoint Energy Services (CES), our unregulated
energy services business, completed the purchase of Continuum
Retail Energy Services last year and closed on the Atmos Energy
Marketing transaction in January 2017. These acquisitions provide
CES with the kind of scale, geographic reach, and expanded
capabilities that will enable it to grow. Accessing more markets
and efficiently increasing our customer base, our retail energy
business now operates in 33 states and serves approximately
100,000 customers.
The strength of our utilities
CenterPoint Energys long-term success is driven by the
disciplined execution of our strategy to Operate, Serve and
Grow. By addressing the needs of our growing service territories
through capital investment, we are increasing our rate base,
which helps drive our financial performance.
The electric transmission & distribution segment had an
excellent year. Earnings growth was driven primarily through
rate relief from investments needed to serve our increasing
customer base, customer growth, and higher equity return.
Building the infrastructure to serve the energy needs of today
and tomorrow remains a priority. Scheduled to be in service
by summer 2018, the Brazos Valley Connection is a 60-mile,
345-kilovolt electric transmission line in Texas that will run from
Harris County to Grimes County, where it connects to the
northern portion of a similar project.
Our continuing investment in intelligent grid technology
increases reliability, reduces average restoration time, saves
consumers money, and drives innovation. In 2016, we improved
electric reliability 34 percent on intelligent grid circuits. Through
the intelligent grid, customers have avoided nearly 200 million
outage minutes since 2011. Smart meters save consumers more
than $20 million per year in eliminated fees through service
automation. This technology has also saved more than 1.6 million
gallons of fuel, preventing nearly 15,000 tons of CO2 emissions.
The natural gas distribution segment also had a strong year.
Earnings growth was driven by rate relief and customer growth.
Last year, we filed with municipal and state regulatory authorities
to change natural gas distribution rates for Houston-area customers.
Our objective in this filing is for customers throughout
Houston and surrounding areas to pay a uniform rate for the cost
of service and the cost of gas. We also implemented new rates in
Arkansas and Minnesota. The purpose of these rate proceedings
is to allow us to earn a reasonable return for the hundreds of
million dollars spent each year in our service territories to
accommodate growth and make our system safe and reliable.
Continued future growth
Through the leadership of our board of directors, we remain
dedicated to delivering long-term value to our shareholders by
growing earnings and providing a competitive dividend.
In 2017, we expect increased earnings from continued utility
customer growth, rate relief, our competitive retail business,
and our investment in Enable Midstream. We also expect lower
interest expense. We expect these items collectively to result
in solid growth year over year.
Together, our electric and natural gas utilities are expected to
invest $1.5 billion in capital in 2017. Our electric business anticipates
capital spending of $922 million to support sustained customer
growth. Our natural gas distribution business plans to invest
$534 million of capital to accommodate ongoing growth and
pipeline replacement needs.
Our dedicated employees
At the heart of CenterPoint Energy are our employees, who
demonstrate our values of safety, integrity, accountability,
initiative and respect.
Safety of our employees, delivery systems and the public is
our priority. CenterPoint Energy was placed in the top quartile
for Edison Electric Institute and American Gas Association
safety rankings in 2016. However, we also had several serious
safety incidents that reinforced our commitment to working
safely and continuing to improve our safety programs and
performance. Our overall approach to safety performance is
focused on behavior-based safety programs and a commitment
to sustaining a strong safety culture.
We have been honored with several prestigious awards thanks
to the efforts of our employees. For example, we received the
Emergency Recovery Award from the Edison Electric Institute
for our power restoration efforts after severe flooding hit
Houston in April 2016. Our crews devoted nearly 16,000 hours
to this recovery, and our intelligent grid saved 26 million outage
minutes during this event.
Additionally, were proud to have been recognized for our
customer service. In 2016, we were named the top Texas Electric
Transmission and Distribution Service Provider (TDSP) in
customer satisfaction by Cogent Energy Reports in the Texas
TDSP Trusted Brand & Customer Engagement Study.
We also received several environmental awards in 2016,
including the Climate Leadership Award and ENERGY STAR
Partners of the Year award, both from the U.S. Environmental
Protection Agency.
Our energy-efficiency efforts span across commercial, residential,
and low-income programs for both electric and natural gas
consumers. In 2016, approximately 170,000 megawatt hours of
energy were saved. Rebates from our conservation improvements
led to customers saving nearly $18 million  the equivalent of the
annual energy usage of about 27,000 homes.
Last year, we conducted an employee survey, which reflected
high levels of pride, commitment, and employee engagement.
Studies have demonstrated that an engaged workforce can have
a significant effect on financial and operational results as well
as higher customer satisfaction.
Our successes and awards reflect the commitment and talent
of our dedicated workforce.
Our Energy for You
Your investment in CenterPoint Energy supports our company,
our employees, our communities and, ultimately, energy for you.
Thank you for your confidence in our company, leadership, and
vision to lead the nation in delivering energy, service, and value.
Sincerely,
MILTON CARROLL SCOTT M. PROCHAZKA
Executive Chairman President & CEO
of the Board