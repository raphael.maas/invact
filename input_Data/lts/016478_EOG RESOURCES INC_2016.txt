LETTER TO SHAREHOLDERS

A PREMIUM YEAR
2016 was a landmark year for
EOG. This is the year our company
established an enduring new standard
for capital allocation: the premiumwell.
The �premium well� standard is a
return hurdle that was initiated as part
of our annual capital planning process.
The goal for the 2016 capital plan was
a simple one driven by EOG�s culture
of capital discipline: earn a return
on every dollar spent and reset the
company to thrive in a low commodity
price environment. We weren�t going
to bank on a recovering commodity
price to drive capital investment
returns in 2016.
We didn�t realize at the time that the
premium well standard would be an
incredible catalyst for creative thinking
and innovation that would change our
company forever.
PREMIUM WELLS
A premium well is one that earns a
minimum 30 percent direct after-tax
rate of return(1) at $40 oil and $2.50
natural gas. If a well earns 30 percent
on a direct basis, that translates to a
healthy fully-loaded or �all-in� return.
We used the commodity price strip at
the time of $40 oil and $2.50 natural
gas as we were cautiously optimistic
we were at or close to the bottom of
the commodity price downcycle.
Since the premium standard was
established at the start of 2016,
we�ve grown our inventory to a
remarkable 6,000 net premium
locations representing 5.1 billion
barrels of oil equivalent.(2) At our 2016
completions pace of 445 wells, that�s
more than 10 years of inventory.
Furthermore, we see no shortage of
opportunities to replace our inventory
through additional efficiencies, well
performance improvements, and new
exploration ideas.
PREMIUM PERFORMANCE
Public industry production data reveals
that EOG�s wells are often the best in
the industry. EOG achieved leadership
in well performance by identifying 
better rock, using proprietary
precision targeting techniques, and
applying industry leading completion
technology. In contrast, the U.S.
shale industry�s improvement over the
last several years is primarily due to
drilling longer laterals.
We believe combining longer laterals
with our superior technology will
allow EOG to maintain its industry
leadership and competitive advantage
in well performance for years to come.
A PREMIUM ACQUISITION
Our transaction with Yates Petroleum
was truly transformative. Combining
Yates Petroleum�s world class acreage
with EOG�s technical leadership is
the perfect example of one plus one
equals three.
EOG is an organic growth and
exploration leader. To compel our
company to look at an acquisition this
size, it must meet a strict set of criteria.
The acreage must be high quality �
as good as, or better than, EOG�s
existing acreage. The deal must come
at a fair price, and it must be funded
in a prudent manner, allowing EOG to
maintain a strong balance sheet. The
Yates transaction checks all of these
boxes. Truly, a premium match.
A PREMIUM FUTURE
Looking forward to 2020, we are
more excited than ever to resume our
leadership in high-return oil growth.
Transforming EOG into a premiumonly
driller means that if the oil price
$50 to $60, we can grow oil
production at a 15 to 25 percent
compound annual growth rate and
pay the dividend, all within cash flow.
As a result, over time, our goal is for
EOG to return to generating return
on capital employed competitive
with our historical average of about
13 percent.(1)
PREMIUM TO THE CORE
Our people and our culture are at the
core of EOG�s success. No matter how
solid our strategy, it takes premium
people to back it up and deliver on our
commitments. When we announced
our permanent shift to premium this
year, every employee embraced the
challenge and took ownership of his or
her contribution to the cause.
The EOG culture is truly one-of-akind.
While we pull in one direction,
we are a decentralized organization
of employees that live and work in
the communities where we operate.
Our employees are committed to
operating safely and being good
stewards of the environment for
ourselves and for our neighbors.
Every area of operation at EOG,
and every employee, has a unique
purpose and they all play a critical
part in the success of our company.
Our employees, like our company, are
Premium to the Core.