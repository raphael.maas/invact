TO OUR STOCKHOLDERS:
At Dr Pepper Snapple Group, we are executing
on the priorities we established more than two
years ago to grow our vibrant business with flavor.
Our strategy is working. In 2009, our first full
year as a stand-alone company, we grew volume
and dollar share in carbonated soft drinks (CSDs)
and juices in the U.S., Canada and Mexico amid
a challenging economic environment. We also
delivered solid top- and bottom-line results while
strengthening our internal capabilities. This
is allowing us to take advantage of the growth
prospects that exist for our flavor portfolio.
Our priorities are simple. 1) Build and enhance
our leading brands. 2) Pursue profitable
channels, packages and categories. 3) Leverage
our integrated business model. 4) Strengthen
our route to market. 5) Improve our operating
efficiency. We made steady progress
against these priorities in 2009
and achieved the following:
 Outperformed the U.S. CSD
category in volume growth
by a margin of nearly
7 percentage points
 Became the only major
beverage company to grow
U.S. CSD dollar share in
each of the last five years
 Declared our first-ever
dividend and announced a
share repurchase program
 Reached an agreement
to expand availability of
Dr Pepper to all McDonalds
restaurants in the U.S.
 Completed the biggest makeover
of Snapple in its 37-year history
 Achieved national distribution
for Crush in the U.S.
 Added more than 200 new routes
in Mexico
 Increased volume and market
share of Dr Pepper in Canada
Our accomplishments in 2009 are just a
taste of those to come, and we are energized
by the knowledge that our journey is just
beginning. The actions we have taken this
year will build a foundation for long-term
growth that will sustain our company well
into the future.
The Flavor of Growth
Macroeconomic conditions provided a
challenging backdrop for the beverage
industry in 2009. Sales of liquid refreshment
beverages (LRBs) declined for the second
consecutive year, while consumer spending
remained weak and shoppers continued
to gravitate toward value. Despite these
challenges, Dr Pepper Snapple Group
focused on finding new ways to win, and
the result was strong business performance,
as we were the only major beverage company
to increase our share of LRBs in 2009.
Net sales increased 2 percent on a currencyneutral basis and excluding the loss of a
licensed brand that we no longer distribute.
Driving the top-line improvement were price
increases and 4 percent sales volume growth,
partially offset by negative mix from higher
sales of CSD concentrates and value juices.
Strong performance across multiple brands
contributed to the volume growth, with
CSDs up 4 percent and our non-carbonated
beverages up 2 percent. Dr Pepper volume
increased 2 percent, largely driven by Diet
Dr Pepper and the launch of Dr Pepper
Cherry. Among our Core 4 brands, Canada
Dry was up mid single digits and 7UP and
A&W grew low single digits, while Sunkist
soda declined high single digits. Crush
volume more than doubled, adding 48
million cases in 2009 through expanded
third-party distribution in the U.S. and the
launch of Crush value offerings in Mexico.
Our leadership in the juice aisle continued
on the strength of Hawaiian Punch and
Motts, with volume gains of 14 percent
and 8 percent, respectively. Our premiumpriced products continued to be negatively
impacted as consumers shifted to value
offerings. Snapple volume was down
11 percent for the year, but sales trends
are strengthening and the brand improved
sequentially for the last three quarters
of 2009.
In Mexico, we grew our share of flavored
CSDs. The restage of Peafiel flavors and
expanded distribution for the brand resulted
in a mid single-digit increase in Peafiel
volume, while Squirt declined high single
digits. We also grew share of Clamato
and flavored CSDs in Canada, particularly
Dr Pepper, where expanded programming
and trial contributed to double-digit
volume growth for the brand.
Segment operating profit on a comparable
basis increased 17 percent on the strength
of the sales gain and lower packaging,
ingredient and transportation costs.
Excluding certain items, we earned $1.97
per diluted share, an increase of more than
6 percent compared to 2008.
Creating Value for Stockholders
In 2009 we generated $865 million of cash
from operating activities. Our strong, stable
cash flow allowed us to repay approximately
$550 million in long-term debt while
continuing to invest in growth opportunities.
We also began deploying excess cash in
shareholder-friendly ways, including declaring
our first-ever dividend of $0.15 per share on
the company's common stock and announcing
plans to repurchase up to $200 million of
our outstanding common stock over the
next three years.
More recently, we completed the licensing
of certain brands to PepsiCo, Inc. following
its acquisitions of The Pepsi Bottling Group,
Inc. and PepsiAmericas, Inc. As part of the
transaction, DPS received a one-time cash
payment of $900 million before taxes and
other related fees and expenses. Having
used a portion of these proceeds to further
reduce our debt obligations, our total
outstanding debt now stands at $2.55
billion, in line with our target capital structure
of approximately 2.25 times total debt to
EBITDA after certain adjustments. Moreover,
our board authorized the repurchase of an
additional $800 million of our outstanding
common stock, bringing our total share
repurchase authorization to $1 billion.
Less than two years after going public, we
have achieved our target capital structure.
Combined with our focus on growing the
business organically, we are now committed
to returning excess cash to shareholders
over time.
Empowering Our People
Over the past two years, our people strategy
has centered on aligning and mobilizing our
19,000 employees around our business
strategy. The value of these efforts is reflected
in our strong 2009 financial performance as
well as third-party survey results that show
our team leaders level of engagement has
improved significantly in the last two years
and is now in a league with other highperforming U.S. companies.
Leadership Transition
As we announced last October, John Stewart,
our chief financial officer, will soon retire.
Without question, Dr Pepper Snapple Group
would not be where it is today without Johns
talent, dedication and exemplary work ethic.
He played a critical role in our successful
separation from Cadbury Schweppes and led
significant improvements in our systems and
financial controls. He also built a talented and
highly effective finance and IT organization.
Were grateful for Johns many contributions
to our business.
Succeeding John as chief financial officer
is Martin Ellen, who will join us from Snap-on
Incorporated on April 1. Marty has a strong
background in finance as well as expertise
in strategy and operations and will play an
important role in taking our business to the
next level of financial and operating success.
Growing with Flavor in 2010
Although the economy and consumer
spending are not expected to pick up until
later this year, we remain confident in our
powerful brand portfolio, our ability to capture
new sales and distribution wins, our continued
focus on cost control and our dedicated
employees. Combined, they provide the
platform to seize opportunities and deliver
another year of solid financial results in 2010.
Sincerely,