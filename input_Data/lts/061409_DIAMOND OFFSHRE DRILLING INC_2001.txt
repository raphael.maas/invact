


    In spite of the more difficult operating environment that pre-     deepwater and ultra-deepwater markets constitute the most

    vailed in the second half of 2001, Diamond Offshore delivered      promising long-term growth areas for companies like Diamond

    solid financial results for the year. Revenues for 2001 rose 34    Offshore. In an effort to position itself to capitalize on this

    percent over 2000 to $885 million, which translated into a 140     progression, Diamond Offshore has made considerable invest-

    percent increase in net income to $174 million.                    ments to enhance its rig fleet. In 1995, the year the Company

                                                                       began its current upgrade program, only three of the 37 rigs
    At this time last year, offshore drilling was in the midst of a
                                                                       that then constituted its fleet were capable of drilling in
    market upturn. Oil prices were trading in a range slightly
                                                                       waters deeper than 3,500 feet. Since that time, Diamond
    below $30 per barrel and natural gas prices were at historical-
                                                                       Offshore has transformed five mid-water rigs into efficient
    ly high levels. In light of these market conditions, major and
                                                                       deepwater rigs. In addition, the Ocean Confidence underwent
    independent oil and gas companies announced significant
                                                                       conversion from an accommodation vessel to a highly
    increases to their exploration and production budgets. Oil
                                                                       advanced deepwater semisubmersible rig. Today  including
    and gas prices remained relatively robust throughout the first
                                                                       two deepwater rigs that the Company purchased  11 of
    half of 2001, leading the way to Diamond Offshore's improved
                                                                       Diamond Offshore's 45 rigs can operate in deepwater environ-
    year-over-year results. But any doubts about the cyclical
                                                                       ments, while eight of those 11 rigs can work in waters deeper
    nature of the offshore drilling industry were put to rest in the
                                                                       than 5,000 feet.
    latter part of 2001, as dayrates again began to soften and

    active rig counts declined as oil and gas prices receded.          The upgrade of the Ocean Confidence was completed in

                                                                       January of 2001, and immediately began operating under a
    The cyclicality of the offshore drilling industry has led
                                                                       contract with BP in the Gulf of Mexico. In its first year of oper-
    Diamond Offshore to employ a strategy to enhance sharehold-
                                                                       ation, the Ocean Confidence drilled two new wells and a re-
    er value that is both opportunistic and conservative. Although
                                                                       entry sidetrack, setting the reliability standard for newly built
    many companies commit to large new-construction programs,
                                                                       rigs in the process: the Ocean Confidence registered less than
    Diamond Offshore prefers to perform upgrades to existing rigs
                                                                       5 percent downtime for the whole year. In 2001, Diamond
    in its fleet selectively, yet on a relatively consistent basis.
                                                                       Offshore also began the conversion of the Ocean Baroness to
    This strategy allows for a faster, more cost-effective means of
                                                                       deepwater capability. The Ocean Baroness was delivered in
    expanding the capabilities of the Company's rigs than does
                                                                       March 2002  on time and $10 million under budget.
    building new rigs.


    The maturation of wells in the Gulf of Mexico continental          In July 2001, Diamond Offshore announced the $200 million

    shelf has driven oil and gas companies to seek exploration         deepwater conversion of the semisubmersible rig Ocean

    opportunities in deeper waters. Because of this dynamic,           Rover. The Ocean Rover project, which should be completed







in the third quarter of 2003, exemplifies the benefits of             notes due 2007 with $460 million in 30-year senior convert-

Diamond Offshore's upgrade strategy  a brand new rig                 ible debentures paying interest at 1.5 percent per annum.

with comparable specifications would have cost the com-               This refinancing transaction reduced Diamond Offshore's

pany $150 million more and would have taken 18 months                 interest cost by approximately $8 million per year and also

longer to complete.                                                   raised the associated conversion price over 20 percent to

                                                                      $49 per share. With its liquidity and low cost of capital,
Diamond Offshore also plans to spend approximately
                                                                      Diamond Offshore will be able to weather cyclical market
$100 million upgrading six of its 14 jackup rigs over the
                                                                      downturns and continue to improve its fleet.
next two years. When natural gas prices dropped precip-

itously in the second half of 2001, approximately 45 per-             Industry authority Oil & Gas Journal, reporting in January

cent of Gulf of Mexico jackup rigs sat idle. During that              2002 on the year ahead, said, "Efficient and capable rigs

same period, Diamond Offshore's jackups were almost                   will get the exploration and development work." Diamond

fully employed  a tremendous testament to the quality of             Offshore is committed to providing the highest quality

the Company's jackup rigs and their crews. The capital                rigs, crews and service to its customers. At the same

improvements that will be made to Diamond Offshore's                  time, Diamond Offshore will continue to effectively man-

jackups will maintain and improve their quality advantage             age costs and financial assets, honoring its commitment

and enhance their leading market position.                            to build shareholder value. Many thanks to the employ-

                                                                      ees, customers, and shareholders of Diamond Offshore
An essential component of Diamond Offshore's strategy to
                                                                      for their continued dedication and support.
maximize shareholder value over the course of the off-

shore drilling cycle is the strength of its balance sheet.            Sincerely,

High levels of liquidity and manageable leverage allow for

flexibility and opportunism. For one, Diamond Offshore's

strategy of selectively but consistently upgrading its rigs

often requires that it make capital investments when its

cash flows are at cyclical ebbs. At year-end 2001,

Diamond Offshore had more than $1.1 billion in cash and
                                                                      James S. Tisch                         Lawrence R. Dickerson
marketable securities and $921 million in long-term debt
                                                                      Chairman of the Board and              President and Chief
                                                                      Chief Executive Officer                Operating Officer
on its balance sheet. During the year, the Company also

refinanced a large portion of its debt, replacing roughly

$400 million of its 3.75 percent convertible subordinated             March 2002



