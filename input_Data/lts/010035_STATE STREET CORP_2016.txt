2016 was a year in which we generated strong shareholder returns,
deepened and expanded our client relationships, and continued to
digitize State Street to position us and our clients for growth in a
new financial landscape. We also intensified our focus on controlling
expenses, managing risk and optimizing our capital position in an
effort to create long-term value for our shareholders.
Im proud of this performance in a
year marked by continued slow global
economic growth, ultra-low interest
rates and geopolitical shocks.
The volatile year began with a sharp
downturn in global equity markets
on concerns over sliding oil prices
and fears that Chinas growth rate
would slow substantially. Stock prices
recovered nicely by spring with the
help of a rebound in oil prices and
accommodative monetary policy from
central banks in the U.S., Europe, Japan
and China, only to suffer another sharp
reversal in June following the U.K.s
surprise vote to leave the European
Union. Stocks quickly regained their
footing following the Brexit vote and
rallied in November following the
conclusion of the U.S. elections, with
most global equity markets ending the
year with modest gains.
Summary Financial Results
Against this backdrop, we achieved
solid financial results. Our 2016 GAAPbasis
diluted earnings per common
share were $4.97, up 11.2% compared
with $4.47 in 2015. GAAP-basis total
revenue decreased 1.5% to $10.2 billion
for the year, while total expenses
were essentially flat. Our 2016 GAAPbasis
return on average common
shareholders equity was 10.5%, an
improvement of 70 basis points
compared with 2015.
On an operating basis1, our 2016 diluted
earnings per common share were $5.27,
up 7.8% from $4.89 in 2015. Revenue
rose 1.2% on an operating basis to $10.8
billion for the year, while expenses rose
4% to $7.8 billion. Without a one-time
adjustment to deferred compensation
in 2016, expense growth would have
been 1%. Our operating-basis return on
average common shareholders equity
was 11.1%, compared with 10.7% in 2015.
We maintained a strong capital position
in 2016, which allowed us to continue
to deliver on a key priority of returning
capital to shareholders. Net capital
distributions, including dividends and
stock repurchases, were $1.9 billion,
resulting in 90% of GAAP net income
being returned to shareholders.
This supported the performance of our
common stock, which generated a total
return of 19.8% for the year.
State Street Beacon
In last years shareholder letter,
I discussed the launch of State Street
Beacon, a multiyear initiative to
transform our company to become
a digital leader. Im pleased that we
made great strides in 2016 improving
operational and systems performance
and providing innovative client solutions,
particularly in the areas of data
management, risk and analytics.
We digitized a significant number of
platforms that enable straight-through
processing of transactions, and many
of our clients are benefiting from faster,
more accurate processing, improved
efficiencies, better data quality and
real-time insights.
We continue to make investments to
further digitize our company through
Beacon and remain on track to generate
$550 million of annual pretax net
run-rate expense savings by the end of
2020, compared with our 2015 expenses,
all else equal, with the full run-rate
benefit expected to be realized in 2021.2
Growing our Core Business
We saw strong demand for our services
across State Street, resulting in $1.4
trillion in new client commitments for
our asset servicing business in 2016, the
strongest results since 2011. Notable
client wins included Allianz Global
Investors, Chubb, DekaBank Deutsche
Girozentrale, Fiera Capital, First State
Super, Pimco, Russell Investments and
the Japanese investment trust business
of Sumitomo Mitsui Asset Management.
Many of these wins were expansions of
existing relationships, which I see as a
strong endorsement of the value were
providing to our clients.
Our acquisition of GE Asset Management
(GEAM) complements the core strengths
of our asset management business,
State Street Global Advisors (SSGA).
GEAM provides substantial benefits to
our clients, including the addition of new
alternative investment capabilities
and the enhancement of our fundamental
equity and active fixed income portfolio
management teams. It also establishes
SSGA as a leading provider of outsourced
chief investment officer (OCIO) services.
As a result of the transaction, SSGA
began managing more than $100 billion
in assets previously managed by GEAM
on behalf of more than 100 institutional
clients, including corporate and public
retirement plan sponsors, foundations,
endowments, sovereign wealth funds
and insurance companies. The integration
has gone smoothly, with the vast
majority of GEAMs employees and
clients choosing to transition to SSGA.
Strengthening our Global Franchise
Over the past five decades, as weve
expanded our presence globally and
developed deep relationships with
many of the worlds top asset managers
and asset owners, weve built regional
scale and strengthened the depth of our
product offerings and expertise in local
markets. Early in 2016, we opened a new
Center of Excellence in Gdansk, Poland,
to build on the success of the Krakw
operations we opened in 2007. In both
of our Polish locations, our employees
deliver specialized services for clients
across Europe and beyond  including
fund accounting, securities valuation,
financial reporting and hedge fund
administration.
We also opened a new office in
Melbourne to enable us to strengthen
our relationship and client service
model in Australia and support our
suite of solutions and capabilities.
We now provide custody and accounting
services to four of the seven largest
superannuation funds in Australia.
A key element of our global growth and
expansion strategy is our international
bank, State Street Bank International
GmbH, headquartered in Germany
with branches in Austria, England,
Italy, Luxembourg, the Netherlands,
Poland and Switzerland, and with a
representative office in Denmark. In
light of Brexit, having this strong
European banking entity in addition
to our significant U.K. presence is a
distinct competitive advantage for State
Street and our clients who look to us
for multijurisdictional capabilities. With
more than 10,000 employees across
Europe and deep expertise in local and
global markets, were well-positioned
to deliver a broad array of solutions and
support our clients need.
Investing in New Products and Solutions
We continue to support the long-term
growth of our business by investing in
new products and solutions to address
our clients needs and differentiate State
Street from our competitors.
SSGA strengthened its position as a
leader in exchange-traded funds (ETFs)
with the launch of 30 ETFs globally in
2016. Net inflows to our ETFs totaled $52
billion in 2016, led by SPDR S&P 500
ETF Trust (SPY), the oldest and largest
ETF, which ended the year with $26 billion
in net inflows and $226 billion in total
assets.
Building on more than 30 years of
experience in environmental, social and
governance (ESG) investing globally,
SSGA expanded its suite of ESG
products in 2016. It launched an ETF to
track its proprietary Gender Diversity
Index, comprising the stock listings of
U.S. large capitalization companies with
the highest levels of gender diversity on
their boards of directors and among
their senior leadership. The SPDR
SSGA Gender Diversity Index ETF (SHE)
is designed to offer investors the
opportunity to invest in companies with
gender-diverse leadership, while
promoting efforts to increase gender
diversity in corporate America. SHE was
the second most successful ETF
launched in the U.S. in 2016 based on
assets under management at year end.
Data management and analytics
continues to evolve as an increasingly
important focus for institutional
investors searching for advanced tools
to support the multiasset solutions
that todays complex investment
climate requires. In 2016, State Street
Global Exchange enhanced its hosted
data-as-a-service platform, DataGX,
a scalable, cloud-based service that
allows clients to aggregate investment
data from multiple service providers or
data vendors for a more holistic
and integrated view of their business
for portfolio management, analytics
and reporting.
We also launched State Street
MediaStats to help clients extract
accurate, valuable and predictive
insights from the massive amounts of
unstructured data found in digital media
and other large consumer datasets.
MediaStats scours more than 45,000
sources of traditional media, social
media and corporate communications,
in addition to other big data sources,
to estimate future price changes of
individual equities.
Pursuing Risk Excellence
As I discussed in last years letter,
weve taken extraordinary steps
in recent years to strengthen our
regulatory compliance and risk
management controls and operating
environment, with the goal of making
risk excellence a competitive strength
for State Street. Over the past five years,
weve devoted significant attention
and resources to risk excellence,
including a large increase in our annual
investment in compliance, audit and
risk management. Weve adopted a
comprehensive compliance framework
across the company, more than doubled
the size of our compliance staff and
made risk management part of each
employees annual performance ratings.
While we have more work to do,
we continued to make considerable
progress in 2016 strengthening our
controls and operating environment
and reinforcing a strong culture of risk
excellence. We enhanced our Standard
of Conduct and underlying training that
all employees must complete annually,
and we implemented a program to
actively encourage employees to speak
up if they see something wrong and to
challenge decisions irrespective of their
role or level at State Street.
Strengthening our Organization
We took a number of steps to strengthen
our leadership team during the year.
SSGA President and CEO Ron OHanley
was named State Street vice chairman
effective January 1, 2017. Rons leadership
at SSGA together with his 30-plus
years of industry experience provide
invaluable contributions to our efforts to
deliver better solutions for our clients.
Eric Aboaf joined us in December 2016
and took the reins as chief financial
officer in March 2017, responsible for
our global financial strategy and finance
functions, including treasury, accounting,
tax and reporting, and investor relations.
Eric has extensive experience working
with a global financial institution
and deep knowledge of the financial
regulatory landscape.
I also promoted Karen Keenan to a
new role as chief administrative officer
and charged her with managing crossorganizational
activities and initiatives
with an initial focus on establishing
an enterprise-wide internal control
framework and ensuring its consistent
implementation across the company.
Since joining State Street nearly 10 years
ago, Karen has led a variety of complex
control groups and major business units.
I believe shes ideally suited to lead this
critical new function, which includes
oversight for our data strategy and
regulatory compliance efforts.
Corporate Responsibility
We believe that being a responsible
corporate citizen is essential to the
long-term success of our business,
as the strength of our business is
directly linked to the well-being of the
communities in which we operate. Our
State Street Foundation provided $19.4
million to nonprofit organizations around
the world in 2016, including matching
employee contributions of $4.1 million
to 2,229 charitable organizations.
Our employees are passionate about
giving back to the communities in
which they live and work. Last year,
more than a fifth of them participated in
company-sponsored volunteer activities
and devoted more than 120,000 hours of
their time to charitable causes.
We focus our charitable efforts on
education and workforce development
because we believe that providing
people with the education and skills they
need to build careers is vitally important.
In June 2015, we launched Boston
Workforce Investment Network
(Boston WINs), a multiyear, $20
million initiative led by our Foundation
in partnership with five nonprofit
organizations working to advance job
readiness and create meaningful career
pathways for young people. Boston WINs
made great strides in 2016 instilling
close collaboration among State Street,
our five nonprofit partners and Boston
Public Schools to increase the number
of students receiving key services that
advance college and career readiness.
We also saw substantial progress toward
our four-year goal of expanding the reach
of the five partner groups by 60 percent,
and our commitment to hire 1,000 Boston
youth who have worked with one of more
of our Boston WINs partners.
Our efforts to be a responsible corporate
citizen received widespread recognition
in 2016. Corporate Responsibility Magazine
named State Street to its list of the 100
Best Corporate Citizens for the 10th
consecutive year. We also received
a perfect 100 rating for the third
consecutive year in the Human Rights
Campaigns 2016 Corporate Equality
Index, a national benchmarking survey
and report on corporate policies and
practices related to LGBT workplace
equality. And we were named to the
North America Dow Jones Sustainability
Index, the gold standard for corporate
sustainability based on analysis of
financially relevant ESG factors.
Looking Ahead
As we celebrate State Streets 225th
anniversary this year, we are keenly
aware that our strong heritage doesnt
ensure our future success. Instead,
it is our ability to innovate on behalf of
our clients that will continue to shape
our destiny.
Technological innovation has sparked
profound change across the banking
and financial services industry in recent
years, but I believe we have just seen
the tip of the iceberg in terms of how
technology will disrupt and transform
our industry. We fully intend to be at the
forefront of that change, supported by
our digital leadership. Were working
diligently across State Street and
collaborating with a broad range of
external partners to assess, pilot and
deploy new technologies with the potential
to unleash a host of benefits  from new
and innovative products to enhanced
operational efficiencies and controls.
Im proud to lead a company that
for more than two centuries has
successfully adapted to meet the
evolving needs of our clients, and
thankful for the outstanding work our
nearly 34,000 employees around the
world are doing to find better ways to
serve our clients and help them achieve
their goals. Im confident that we have
the right people in place and the right
strategy to build on our success as a
trusted and valued partner to many of
the worlds largest asset owners and
asset managers.
As always, Im grateful to you, our
shareholders, for your investment
and confidence in us.
Sincerely,
Joseph L. Hooley
Chairman and Chief Executive Officer
March 17, 2017