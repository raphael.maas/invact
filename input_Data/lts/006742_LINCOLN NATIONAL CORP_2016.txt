Dear Shareholders,
As a leading public life insurance company in the United States, we are committed to creating long-term value for our shareholders. This
is enabled by our over 9,000 talented employees delivering products and services that offer financial security and peace of mind to our 17
million customers.
Our business model has a demonstrated track record of stability and financial success. Since 2009, we have grown income from
operations per share at a 13% annual rate, improved our operating return on equity by 360 basis points, increased book value per share,
excluding accumulated other comprehensive income (AOCI), at a 7% annual rate and returned $5 billion of capital to shareholders
through share buybacks and dividends.1
Included in this solid long-term performance were strong results in 2016 as we again delivered consistent and profitable growth including:
 Income from operations per share of $6.50, a record and up 19%1
 Assets under management totaled $229 billion, a year-end record
 Book value per share, excluding AOCI, exceeded $57, up 9%1
 Operating return on equity, excluding AOCI, was 12%1
 Capital returned to shareholders remained robust at $1.1 billion or 72% of operating earnings
 Statutory capital as of year-end approached $9 billion, up 5%
We are particularly pleased with this years results given the backdrop of global economic and political uncertainty. As a company that
recently celebrated our 110th anniversary, we have a history of responding and successfully adapting to various environments, including
regulatory changes, shifts in consumer preferences and technological advancements. Therefore, we truly believe companies need to stay
attuned to cyclical and structural shifts in the marketplace and the impact those shifts have on all stakeholders.
A recent notable shift for us on the regulatory front relates to the Department of Labor (DOL) Fiduciary Rule. We have supported the
intent of the DOL rule and its overarching goal to further ensure that our clients best interests are served while working to improve parts
of the rule. The DOL acknowledged in the final rule that both commissions and fees can be appropriate forms of compensation, and we
have moved to refresh and introduce new annuities that maintain invaluable guaranteed lifetime income benefits while providing
compensation choice for consumers.
To respond to the rapid pace of technological advancement, we recently hired a Chief Digital Officer and launched another dimension of
our technology strategy focused on digitization. The born digital companies are raising customer service expectations, and we are
moving to meet these expectations. Our primary focus is to improve our customers experiences and increase our efficiency. We have
already seen benefits from leveraging digital and mobile capabilities. For example, in our Retirement Plan Services (RPS) business, we
have enhanced and simplified the web experience for participants with the launch of an easy click to contribute feature. Since
inception, we have seen more than 19,000 participants better prepare for retirement by increasing contribution rates, securing $45 million
in additional retirement savings.
We expect our digital strategy to result in additional successes and further enhance our reputation as a premier provider of products that
meet critical consumer needs that are unmet by other industries. Our Life Insurance business enables legacy planning, and last year we
paid nearly $2 billion in claims that helped secure families futures following the passing of a loved one. Our Annuities business has
nearly $70 billion of account value in products that offer guaranteed lifetime income, which fills a void from most corporations
eliminating defined benefit pensions.
We also recognize the importance of a vibrant workforce. We invest in attracting and developing our workforce to ensure we have a
competitive edge from an internal and external perspective. Talent is a key driver at Lincoln. Since 2009, we have increased our
employee base and have seen salaries for non-executives increase at a faster rate than the average wages for the United States population.
We couple this with a deep commitment to employee development and promotion opportunities, diversity and inclusion, retirement
planning and our role in the broader community.
Notably, all Lincoln employees are deeply connected to the communities within the key cities where we operate. Our employees
contribute thousands of volunteer hours, and our Lincoln Financial Foundation reinforces our commitment through significant charitable
investments to more than 400 nonprofits.
We are investing for long-term growth through approximately $1 billion in annual capital allocated towards writing new business and a
commitment to employee enrichment. However, we generate a significant amount of capital, leaving us with excess capital that we have
efficiently returned to shareholders. Over the past 6 years the majority, or $4 billion, of capital returned to shareholders has been put
towards buybacks, and our share price as of December 31, 2016, was 76% above our average repurchase price over that period.
Importantly, our long-term commitments and alignment with all our key stakeholders are rewarding shareholders. In 2016, Lincolns
share price increased 32%  outperforming our peers, which increased by an average of 18%.2 This marked the fourth consecutive
three-year cycle where our total return to shareholders was among the best in the industry. Over this time, our share price increased
138% compared to a 49% gain for our peer group.2
As we look forward, we remain confident that our key strategic objectives  manufacture only retail products, target the fastest growing
segments of the broader U.S. market, maintain industry leading risk management, utilize digitization to drive a differentiated customer
experience and actively direct capital to the highest and best uses  will drive long-term, sustainable growth in our four business segments:
Life Insurance, Annuities, RPS and Group Protection.
On a personal note, we want to express our gratitude to William (Billy) Porter Payne, who has announced plans to retire and will not be
standing for re-election to our Board of Directors this year. Billy has provided thoughtful counsel and guidance, and we are sincerely
appreciative of his many years of dedication and leadership, which resulted in significant contributions to Lincoln.
In closing, on behalf of Lincolns Board of Directors, management and all our employees, we want to thank you for your continued trust
and investment. We are excited about the opportunities ahead and our ability to continue creating long-term value for our shareholders.
Dennis R. Glass William H. Cunningham
President and CEO Chairman of the Board
March 27, 2017