                                                                                      To our shareholders
It was a good year to invest with AvalonBay. Last year                                                                                          
we said 2010 would be a transition year with both the                                                                 
                                                                                                                                                                                        
economy and apartment markets in recovery. Indeed,                                                                    

  
                                                                                                                                                  
it was. Modest job growth emerged while apartment                                                                     


                                                                                                                                                                   
rental demand surged to the strongest level in five years.                                                              




                                                                                                                                


                                                                                                                                         
Fundamentals other than employment were driving                                                                       

the apartment market recovery and this shift was                                                                      

recognized by investors. For the year, the S&P 500 Index                                                              
                                                                                                                           
was up 13% while the Morgan Stanley REIT Index rose
                                                                                                                            
24%. For AvalonBay, Total Shareholder Return was 42%.


                                                                                               Last year's annual report, titled "Where You Want To Be",
                                                          
                                                                                               describes an organization that is alert and responsive to
                                                             
                                                
                                                                                               challenging market conditions while prepared for the
       



                                                                                               next phase of growth and opportunities. In this year's
                                                                                               report, we will review our accomplishments in 2010
                  




                                                
                                                                                               and share with you our view of fundamentals that favor
                                                                                               rental housing over the next several years. We will also
                                                                                               provide a deeper look into our business. By understanding
                                                
                                                                                               the value-creation opportunities available to us, we
                                                                
                                                                                               believe investors will better appreciate the potential for
                                                                                               outsized returns going forward. "Appreciating Value" is
                                                  
                                                          
                                                                                               the theme of our report this year.
                                                      


                                                                                               Looking Back at 2010
As we move into 2011, apartment fundamentals look
                                                                                               At the beginning of last year, the economic outlook was
even stronger. Accelerating job growth, limited new
                                                                                               uncertain. The economy had lost 270,000 jobs in the last
supply, a declining homeownership rate, favorable de-
                                                                                               quarter of 2009. Rental rates and occupancies were falling.
mographics and expectations for an improving econ-
                                                                                               Our 2010 financial outlook called for the impact of these
omy all support improved operating performance.
                                                                                               soft economic conditions on our portfolio to moderate and
Accordingly, we expect absorption, occupancy and
                                                                                               trend into a period of stronger demand/supply fundamen-
rental rates for our portfolio to rise. Moreover, attitudes
                                                                                               tals for 2011 and beyond.
among consumers, lenders, and lawmakers are shift-
ing from homeownership towards renting. Given the
                                                                                               In fact, the job market stabilized early in 2010, consumer
key structural advantages of our markets, our ability
                                                                                               confidence improved, and renters that had doubled up be-
to invest capital through multiple growth platforms,
                                                                                               gan to establish their own households. This drove renter
and a balance sheet that supports growth, AvalonBay is
                                                                                               demand higher at a time when fewer of our residents were
well-positioned to extend our industry-leading position
                                                                                               choosing to move. As a result, we began to recoup rental
for value creation in new apartment investment.
                                                                                               revenue lost during the recession sooner than expected.




                               Fundamentals accelerated quickly. We updated investors                                              We sold three assets for an aggregate price of $190
                               in April and again in June, increasing our forecast for rev-                                         million. These sales, at a weighted average Initial Year
                               enue growth. At mid-year, we announced a 5% increase                                                 Market Capitalization Rate of 5.8%, provided a total
                               in our full-year 2010 outlook for Funds From Operations                                              Economic Gain to us of $22 million and a 9%
                               per Share (FFO) and another 2% increase in the fall. We                                              unleveraged IRR on our investment over a 12 year
                               ended the year with Net Operating Income (NOI) from                                                  weighted average holding period - harvesting value
                               our Same Store portfolio down 3% for the year com-                                                   that was reinvested into attractive new development.
                               pared to a projected decline of 6% in our original outlook.
                               Earnings per Share (EPS) increased 7%, primarily from                                               Through our second private discretionary real estate
                               an increase in gains from community dispositions and                                                 investment fund, we acquired six communities with
                               impairment charges reported in 2009 not present in 2010.                                             just under 2,800 apartment homes for approximately
                               FFO increased 3% for the full year.                                                                  $400 million.


                               We also increased our investment activity last year:                                                We completed five redevelopments during the year
                                                                                                                                    for a Total Capital Cost of approximately $80 million
                                    We started 11 communities for a total budgeted cost                                            (excluding costs incurred prior to redevelopment) and
                                     of $640 million. This was 60% more than we antici-                                             started seven redevelopments with incremental
                                     pated at the beginning of the year and demonstrates                                            estimated Total Capital Costs of $80 million.
                                     the multiple dimensions of our Company. Organiza-
                                     tional capacity, a willingness to be agile and balance                                     This opportunistic and timely commitment to new in-
                                     street strength allow us to respond quickly to accelera-                                   vestment required new capital from a variety of sources.
                                     ting fundamentals.                                                                         We issued $340 million of common stock under our
                                                                                                                                Continuous Equity Program (CEP) at an average price
                                                                                                                                of $98  20% above the share price at the beginning of
                                                             

                                                                                                                                the year. We sourced $250 million in ten-year unsecured
                      

                                                                                                                                notes at a coupon of 3.95% and lowered our cost of debt
                    

                                                                                                                                by repaying $64 million with a weighted average rate
                     

                                                                                                                                of 6.3%. The pricing on the ten-year note offering was
Total Capital Cost




                                                                                                                        reported to be the lowest coupon in modern REIT 




                      
                                                                                                                                on unsecured debt by any REIT in any sector, and reflects
                                                                                                                      




                      
                                                                                                                                an appreciation by the credit markets of the strong finan-
                                                                        

                                                                               




                                                                                                              




                       
                                                                                                                                cial condition of AvalonBay and our overall credit qual-
                                                                                                




                       
                                                                                                                                ity. This is especially meaningful given the severity of the
                                              
                                    




                                                      




                       
                                                                                                                                recent credit crisis and our ability to preserve our strong
                                                               




                                                                                                        




                       
                                                                                                                                credit profile during the downturn.
                         
                            

      
                                                                                                                                       
                                    We completed and delivered four new communities
           
                                     totaling over 1,500 apartment homes for a Total    
                                     Capital Cost of $570 million. At year-end 2010, we had                                          
                                     14 developments under construction  double the
                                                                                                                                  
                                     number underway at the prior year-end.





                                             We see opportunities to improve growth and investment returns

                                             in our markets through greater focus on location, asset quality

                                             and customer segment.
                                                                                                                                                                                                           segment as potential buyers face tighter mortgage
                                           The Economy and Apartment Fundamentals
                                                                                                                                                                                                            qualifications and lingering concerns regarding the
                                                                                                                                                                                                            risks and burdens of investing in a home. Some fore-
                                           With only limited job growth, the recovery in our sector
                                                                                                                                                                                                            casts suggest that by 2015, the U.S. homeownership
                                           is evidence that the drivers we believe support strong de-
                                                                                                                                                                                                            rate could fall to 64%, potentially creating two million
                                           mand for rental housing for the next several years are
                                                                                                                                                                                                            new renter households.
                                           now aligning:

                                                                                                                                                                                                           New Supply Perhaps most importantly, a sharp
                                                Demographics Younger age groups have a higher
                                                                                                                                                                                                            reduction in new rental supply is now underway. Over
                                                 propensity to rent and today there are more U.S.
                                                                                                                                                                                                            the last ten years, nationwide construction starts for
                                                 residents age 20 to 29 than 30 to 39 years old. Further,
                                                                                                                                                                                                            multifamily rental units averaged 200,000 per year. In
                                                 the pace of hiring among young adults in the 20 to 34
                                                                                                                                                                                                            2010, only 75,000 multifamily rental units were started,
                                                 years old age range is reportedly at its strongest pace
                                                                                                                                                                                                            a 60% drop. While new starts will likely increase this
                                                 since the mid-1980's. Together, these trends help ex-
                                                                                                                                                                                                            year, only 130,000 apartment homes are anticipated
                                                 plain some of the "unbundling" of households which
                                                                                                                                                                                                            for completion in 2012. These are the lowest levels of
                                                 occurred during 2010 that boosted renter demand.
                                                                                                                                                                                                            new rental development in the last 40 years.


                                                                                                                                                                                                           Government Policy Recent proposals to scale back
                                                Homeownership The nation's homeownership rate is
                                                                                                                                                                                                            the government's footprint in the mortgage market
                                                 approximately 67%, down from the 69% peak reached
                                                                                                                                                                                                            send strong signals of a general shift towards a more
                                                 in early 2005. Not surprisingly, the percentage of our
                                                                                                                                                                                                            balanced housing policy. The impact of this shift will
                                                 residents leaving to purchase a home is near historical
                                                                                                                                                                                                            likely be tougher lending hurdles and higher costs
                                                 lows. This reflects continued weakness in the for-sale
                                                                                                                                                                                                            for homeowners, which will benefit rental housing
                                                                                                                                                                                                            demand in the years to come.
   
                                                                                                               The Road Ahead: Appreciating Value
                                                                                          




                                                                                                                        


                                                          Our markets have a number of key structural advantages:
                                                                                                
                                                                                                                                                                             higher household income, lower home affordability
                                                                                                                         and constraints on new supply. This helps explain why
                          
                                                                                                                                                                             many of our markets were among the top-performing in
                                                                     
                                                                                                                                                       
                                                                                                                         
                                                                                                                                                                             effective rent growth over the last ten years. These ad-
                                                                                                                                                                vantages should continue to result in strong performance
                       




     
                              Whether through development, redevelopment or acquisitions,

                              we will invest capital through multiple platforms as a means to

                              grow Net Asset Value and optimize portfolio performance.
     going forward. As a result, we are likely to see other                                                   

     players enter or expand in our markets. An investor                                                  

     should ask: "How will you create value in a potentially
                                                                                                          




                                                                    
     more competitive environment?"                                                                                          
                                                                                                                                                  




                                                                              Development Starts
                                                                                                          
     Opportunity exists within our markets to improve
     growth and investment returns through greater focus                                                  
     on submarket selection, asset quality and customer
                                                                                                          
     segmentation. Portfolio performance can be enhanced by
     shifting AVB's portfolio over time to the top-performing
                                                                                                            
     submarkets within each of our markets. With enhanced                                                               
     market research and local knowledge, a reasonable goal                                                    
     is to shift the portfolio so that approximately 70% is
     located in the top half of each MSA's submarkets vs.
     approximately 50% today. Within individual submarkets,            Redevelopment Depending on the strategy, redevel-
     performance can vary by asset class. With a greater                opment can range from remerchandising (essentially
     focus on segmenting our markets by customer, price                 upgrading finishes and furnishings) to a major
     point and, ultimately, product type, we can identify the           reconstruction of an older asset. The value created
     most attractive pockets of growth at a more granular level         from a redevelopment includes the higher rents we
     to optimize portfolio performance. With only a 1% share            achieve from improving the asset and the incremental
     of the total rental inventory in our markets and only a            NOI growth over time as a result of higher rents and
     9% share of new development, we believe there is room              lower expenses. Over the past decade, we have invested
     to expand our capabilities and capture return from these           close to $300 million in redevelopment capital for 31
     value-added activities.                                            communities. Assuming favorable market conditions,
                                                                        we believe about $100 million per year of redevelop-
     To optimize portfolio positioning and grow NAV,                    ment opportunities exists in our portfolio for the next
     AvalonBay can invest capital through multiple growth               few years.
     platforms: development (to grow the portfolio), rede-
     velopment (to position the portfolio), and transaction            Transaction Activity Acquisitions are also a platform
     activity (acquisitions and dispositions to shape the port-         for growth. Combined with dispositions, they provide
     folio). Let's look at each platform.                               a vehicle to achieve portfolio reshaping goals. Since
                                                                        1998, we realized a 14.2% unleveraged IRR on $1.5
      Development From $6 billion in new development                   billion of acquired assets that were subsequently sold.
       invested since 1994, we have sold $1.1 billion                   We currently invest in and manage two acquisition funds
       and realized a 16.8% unleveraged IRR and an eco-                 through our Investment Management platform. By
       nomic gain of $96,000 per apartment home. With                   executing acquisition activity principally through the
       the ability to compete for any apartment opportu-                funds, we are able to tap into an additional source of
       nity   within    our     diverse     geographic   footprint,     capital through the private equity market while we
       AvalonBay's development capabilities are unmatched               separately allocate capital into development activity.
       in our sector. At year-end 2010, we had $900 million of          These activities provide both the potential for outsized
       new development underway at a projected weighted                 returns to inves-tors and an additional source of rev-
       average yield of 7%. Our future pipeline consists of 26          enue from fees and promotes, all while being good
       sites for an estimated Total Capital Cost cost of                stewards of the capital provided by both our public and
       $2.2 billion.                                                    private investors.



                                       We have the strongest balance sheet in our sector. A largely

                                       unencumbered asset base and access to unsecured markets

                                       preserves financial flexibility and enhances liquidity. 

      As a value-centric organization, we align our perfor-                                       Conclusion
         mance measures with value creation over a period of
                                                                                                     Through the years, creating value has been our focus and
         time that may not necessarily be within a calendar year.
                                                                                                     Appreciating Value has been the outcome. We recognize
         As a result, we manage our capital structure by match-
                                                                                                     that value and earnings growth align over time to gen-
         ing financing and investing activities. This disciplined
                                                                                                     erate outsized returns and cash flow. Looking forward,
         approach helps guide the type and timing of capital
                                                                                                     our strategy of allocating capital to deliver a range of
         markets activity by managing our credit metrics, liquid-
                                                                                                     multifamily offerings tailored to serve the needs of the
         ity and debt maturities. This helps mitigate the impact
                                                                                                     most attractive customer segments in the best perform-
         of recent capital markets volatility by increasing our
                                                                                                     ing US submarkets positions us well to improve growth
         financial flexibility. Through these efforts, we have
                                                                                                     and investment returns. We believe the fundamentals
         expanded our financing options, avoiding unfavorable
                                                                                                     in our markets, our multiple growth platforms and our
         market conditions, reducing our cost of capital and creat-
                                                                                                     disciplined approach to capital management are all
         ing shareholder value.
                                                                                                     aligned to deliver outsized dividend growth, Total Share-
                                                                                                     holder Return and NAV growth.
         We have found that value-centric activities, characterized
         by low leverage, conservative accounting policies and
                                                                                                     As always, thank you to our shareholders for your con-
         a willingness to recycle capital back into our invest-
                                                                                                     tinued support, to our associates for your extraordinary
         ment platform are highly correlated to Total Shareholder
                                                                                                     efforts, and to our residents who have chosen to make an
         Return. Not surprisingly, dividend growth and Total
                                                                                                     AvalonBay community your home.
         Shareholder Return are highly correlated. Our dividend
         has never been reduced and has always been covered by
         recurring cash flow. We can think of no better evidence
         of an organization's long-term value creation capability
         than outsized cash flow growth as measured by dividend
         growth and retained cash.

