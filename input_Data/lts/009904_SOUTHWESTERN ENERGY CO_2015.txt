Dear Fellow Shareholders: 

A Strong Bridge Forward


The dramatic drop in commodity
prices has significantly impacted
our industry and has once again
forced many organizations to
re-think their strategies and plans
for creating value. At Southwestern
Energy, our strategy is built on a solid
foundation of strong core values and is
inspired by our Formula that has stood
the test of many difficult times. �
The Right People doing the Right Things,
wisely investing the cash flow from the
underlying Assets will create Value+
.
For us, Value+
 means an unwavering
discipline to only invest in projects that
deliver $1.30 discounted at 10% for
every dollar we invest. We call this
ratio �Present Value Index� or PVI of
1.3. What remains true today, no matter
the challenges, is that we have premier
assets, are a low cost operator amongst
our peers, have a relentless pursuit for
innovation and have the best people
in the industry working safely on our
behalf. Inspired by an impressive list of
significant accomplishments in 2015, we
are sharply concentrated on 2016 and
beyond. We approach each day with a
bold determination to manage through
volatile times and strengthen our bridge
to the future with an intense focus
on creating significant long-term
value for our shareholders.
I believe that credibility with our investors
and stakeholders is only derived through
delivering on our commitments. Further,
I believe in our people and their ability
to persevere, innovate, find efficiencies
and safely deliver results. I believe
in the opportunities we have in front
of us, and most of all, I believe in
Southwestern Energy.
With our strong foundation firmly in
place, throughout 2016, we are
focused on driving through the current
commodity market and on further
strengthening our bridge to the future.
Specifically, we will shore up our balance
sheet, enhance margins and optimize
our portfolio of premier assets.
Build on Strong Liquidity to Further
Strengthen the Balance Sheet
Our substantial liquidity position
differentiates Southwestern from
others, and when combined with our
high quality portfolio of assets,
Southwestern is positioned well for a
strong future. Due to our low cost
structure, significant production base
and diverse transportation portfolio,
the Company continues to generate
positive cash flow from our operations in
this challenging price environment.
In 2016, we are committed to
strengthening our balance sheet by
actively pursuing opportunities to reduce
our debt and to preserve our liquidity
by executing on an operating plan that
aligns our capital investment program
with cash flows generated from
operations. Our priority is to generate
solid returns over low-value growth.
Reducing our liquidity to invest in wells
that have marginal economics in
today�s price environment does not
make sense to us.
Operating Efficiently and
Enhancing Margins
We will continue to operate efficiently
and strive to find more ways to drive
costs out of our operations to enhance
our margins. We are critically looking at
everything we do, every day. As a result,
we have already taken steps to lower
costs through a number of organizational
and operational activities.
In my experience, challenging times like
the ones we are experiencing, are
often when companies deliver the
brightest ideas that take our industry to
the next level. And I can tell you, we plan
to be a big part of that future and a
big part of unlocking ideas and value
from our curiosity and learning.
Optimize the Portfolio
Our current production base and our
entire resource base sets us up to
benefit in a tremendous way as pricing
improves. Today, we are focused on how
to maximize the production from our
existing wells through innovation, new
technologies and efficiencies.
With the Company�s differentiating
focus on project returns, the Northeast
and Southwest Appalachia areas are
positioned to be the growth drivers for
years to come, with the Fayetteville
Shale E&P and Midstream businesses
providing a base of cash flows that will
fund this growth activity.
We are prepared for a prolonged
low-price environment, but we remain
equipped with the flexibility and
liquidity to ramp activity quickly when
prices improve. We have the core DNA
in each area of our operations that
will allow us to pivot from our current
activity level to value-adding growth
when commodity prices and cash flow
support an increase in activity.
During 2015, we demonstrated our
ability to maneuver through these
challenging market conditions. We
delivered record drilling results, record
well performance, material well cost
savings and terrific results from our base
production enhancement efforts.
We remained flexible throughout the
year, and accordingly, we reduced
activity to align with market realities and
focused on maintaining a strong liquidity
position. We efficiently integrated the
Southwest Appalachia asset into SWN
during 2015 and it has exceeded our
expectations in every operational aspect
as compared to our acquisition model.
I am confident that we will deliver
on our priorities in 2016 and beyond.
Our company will prosper in any
environment when we focus on
optimizing our superior assets and
combine that focus with disciplined
investing and a relentless pursuit
of innovation. Importantly, underlying
all of our efforts, we are grateful for
the continued support and hard work
of our dedicated employees,
who are truly the Right People and
the drivers of our success.
We look forward to a future when the
natural gas market begins to improve
because we have the core assets
and skills to cross the bridge and deliver
rock-solid results and value to you,
our shareholders.
In conclusion, I want to thank you, our
shareholders, for your support of our
strategy. I also want to thank our
devoted and innovative employees
whom I am honored and privileged to
lead and who work diligently to
strengthen the bridge which will bring
value-added growth to you.
Sincerely,
WILLIAM J. WAY
PRESIDENT &
CHIEF EXECUTIVE OFFICER