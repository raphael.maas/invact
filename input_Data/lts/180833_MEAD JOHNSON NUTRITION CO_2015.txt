A Letter from the
Chief Executive Officer
To our shareholders, colleagues, and business partners: 

I am pleased to report that, in 2015, we made
good progress against a number of important
strategic initiatives.
In key markets, we launched new and improved
formulations for our flagship global Enfamil� franchise.
We also began implementation of important
upgrades to our packaging and product formats.
The introduction of plastic packaging and liquid
product formats in markets across Asia and Latin
America has been well received, as the parents
we serve appreciate the increased convenience in
the use and handling of our products. Lower dairy
input costs helped us deliver profit margins above
historic norms, despite downward pressure on
pricing in a number of countries.
We took the first step toward reshaping our
product portfolio in China and accelerating
growth in new channels there. Food safety
concerns continue to fuel Chinese consumers�
preference for imported food products. In order
to meet the changing demand, we invested
significantly in shifting our offerings away from
locally manufactured products. In the second quarter,
we introduced a fully imported range of products
made at our facility in the Netherlands � and
by year end, these products accounted for more
than a third of our sales in mainland China.
Simultaneously, we began strengthening our
presence in the fast-growing baby store channel,
adding salespeople in support of this effort.
In North America and Europe, we sustained
our positive performance and, once again,
achieved strong sales growth. We saw continued
expansion of our Enfagrow� growing-up milk
business in North America. In Europe, we have
increased profits five-fold since 2012, with a
focused strategy that leverages our product
superiority within the growing Allergy segment.
Our continued focus on talent development
and capability building made many of these
achievements possible. Since 2014, we have
significantly strengthened our senior leadership
team � investing in the development of our
existing leaders, while also adding important
new talent to help expand our capabilities.
In July, we announced that we had resolved
our discussions of China-related matters with
the U.S. Securities and Exchange Commission.
We are, today, a stronger company as a
consequence of what we learned in China.
We continued to execute sustainability improvement
projects against our long-term GreenVision 2020
goals that target notable reductions in Greenhouse
Gas Emissions, Energy Usage, Water Consumption
and Solid Waste. As I have often said, �It makes
no sense for a company to have a mission to give
children the best start in life, if we are not fully
committed to protecting the world that they are
ultimately going to inherit from us.� A highlight in
2015 was our ongoing investment in waste reduction
activities. Our manufacturing plants in Thailand and
the Philippines continue to operate at minimal waste
levels, with aspirations to eventually join those in
China, Singapore and the Netherlands as �zero waste
to landfill� operations.
We also continued to deliver on our commitment
to ongoing improvement in the environmental
practices and working conditions related to our
ingredients. After achieving full traceability and
ethical sourcing of cocoa in 2014, we initiated a
three-year program to enhance the certified level
of sustainability and traceability of the palm oil used
in our products. In 2015, we became a member of the
Roundtable for Sustainable Palm Oil, with an aim
to achieve the stringent certification goals we have
targeted for this ingredient by year-end 2016.
Despite progress made and learnings gained, however,
we were less than satisfied with the performance of
the business in 2015. Any discussion of 2015 would
be incomplete without recognizing the challenges
we faced during the year.
Economic growth slowed in almost every country
across what have historically been our two fastestgrowing regions of Latin America and Asia.
Consumer and retailer confidence suffered as a
consequence, resulting in weaker demand across
markets in those regions.
In a number of key markets, we experienced
deflationary pressure on pricing, in part due to
weakening commodity prices. This situation was
brought on by intense competition for volume
in markets with static demand. And, in some
cases, government regulators exacerbated the
situation by deploying anti-inflationary policies
in markets with deflationary pressure. The net
outcome of all this was growth rates lagging
historical norms across Latin America and Asia.
In the second half of the year, we began to
experience the adverse effect on earnings of a
strengthening U.S. dollar. We derive nearly three 

quarters of both our sales and earnings from
outside the United States; hence, the impact was
substantial. Though we view currency trends
as being temporary and accept they cannot
be immediately mitigated with pricing actions,
based on current indicators, we are hopeful for
an improved pricing environment in the mid-term.
In recognition of a more challenging external
environment, we undertook a significant effort in
the second half of the year to sharpen our strategy
and strengthen our performance under these new
circumstances. Going forward, our strategy will
retain a number of key elements, but also reflect
important refinements in our thinking around our
global infrastructure, organizational design and
operational efficiency.
What stays the same:
� Our commitment to long-term value
creation for our shareholders.
� Our commitment to the long-term
welfare of our employees.
� Our focus on pediatric nutrition.
� Our continued investment in brands,
innovation and science.
� Our unwavering commitment to integrity.
� Our GreenVision 2020 goals and
giving back to our communities.
What is new:
� A pursuit of expense and infrastructure
reduction against firm targets.
� An intent to reduce administrative and
supervisory layers and increase agility.
The evolution of our strategy will allow us to create
a faster, more responsive business that is easier
for employees to navigate � and where decisions
are made closer to customers and consumers.
This new approach to the business will support
our financial ambition and protect our ability to
invest in future growth when the commodity cycle
eventually turns and our input costs inevitably
begin to rise again. We believe the subtle changes
we are making across our business � and the
flexibility they will allow � are critical to our
delivery of shareholder value creation over time.
We remain excited about our future prospects
in a world where we see nutritional trends
constantly changing.
Rates of allergy are increasing globally, affecting
up to a third of people at some stage in their lives.
According to one study, food allergies among
children increased approximately 50 percent between
1997 and 2011. Our Nutramigen� brand is backed by
more than 75 studies and is a preferred option for
the dietary management of Cow�s Milk Allergy.
People around the world are changing the way they shop;
and so, we must not only change with them � we must
help lead this change. In both the United States and
China, we have been adding E-Commerce expertise
and forming collaborative partnerships with leading
companies in the digital space. In 2015, our E-Commerce
business grew faster than any other sales channel � a trend
we expect to continue.
Across the world, we continue to see consumers
migrate toward higher-quality, premium offerings.
The motivation among parents to invest in better
nutrition for their infants and children has never
been stronger, despite economic challenges in many
markets. Parents will continue to invest progressively
more in the welfare and nutrition of their children � and
Mead Johnson is very well placed to benefit from their
aspirations for the next generation.
We expect 2016 to be a year of transition for Mead
Johnson. We will continue to evolve our brand
portfolio and drive our channel strategy, while
reducing organizational layers and eliminating
unnecessary cost structure. We plan to exit the
year as a leaner, more resilient and faster-growing
business, with the same unwavering commitment
to create value for our shareholders by helping give
children around the world the best nutritional start
in life.
I thank you for your trust and support, as we continue
this journey together.
Kasper Jakobsen
President and Chief Executive Officer