DEAR FELLOW
SHAREHOLDERS,
Although 2011 was clearly a difficult year given
the slow pace of economic recovery in the
United States and the volatility in the markets,
on a continuing operations basis we continued
to make steady  albeit incremental  progress
on a number of key fronts. Even though we
experienced a net loss available to common
shareholders of $429 million, our core franchise
has strengthened and we achieved sustainable
profitability from our continuing operations,
excluding goodwill impairment, with net income1
of $211 million or $0.17 per share in 2011.
While there is still work ahead, these results
demonstrate that we are successfully executing
our business plan, and I believe that we are well
positioned to capitalize on future opportunities.
Amid the sustained challenging economic environment
and market turmoil, our associates
have done an excellent job staying focused on
what we can control. We recognize the realities
that come with regulatory reform and are making
the necessary adjustments to our evolving
business model. It is important to note that the
Southeast, where our franchise is primarily located,
is projected to outpace the national average
in growth over the next few years. I am
confident our efforts will continue to produce results,
given our brand favorability in the markets
we serve, our relentless focus on our customers
and our commitment to delivering value and
outstanding service quality.
ECONOMIC AND
REGULATORY
ENVIRONMENT
Fragile National Economy. Without a doubt, we
have been operating in a very fragile U.S.
economy, marked by extremely high levels of
unemployment as well as a substantial decline
in home prices. Gross domestic product (GDP)
growth for 2011 slowed to 1.7%. While the unemployment
rate in December 2011 declined
to 8.5%, it has been 9% or higher in 28 of the
past 31 months. There are more than 13 million
Americans still unemployed and approximately
another 9 million working part-time. Almost 43%
of those unemployed have been out of work for
more than six months. If we are to see sustained
economic recovery, the labor market needs to
add about 125,000 jobs per month just to keep
the unemployment rate steady.
Another factor in our fragile national economy is
declining home prices. This decline is in stark
contrast to the yearly increases we saw in home
values following World War II when home ownership
became part of the American dream. Prior
to 2006, many Americans lived that dream and
placed sizeable portions of their wealth in their
homes. In spite of the current decline in home
prices, 65% of Americans own a home, while
just 57% of French and 46% of Germans are
homeowners. That statistic becomes even more
meaningful when you consider that Americas
recession has wiped out more than $6 trillion
in housing wealth since it began in late 2006.
As a result, today one of every four homeowners
owes more on their mortgage than their home
is worth.

Global deleveraging is also impacting the recovery
 driven by the intense debate about how
to cut the federal deficit and the sovereign debt
crisis in Europe. Both issues have created a pervasive
lack of confidence that has resulted in investors
taking fewer risks, causing a major stock
market sell-off and widening credit spreads.
Consumers are also taking fewer risks as they
continue to save more and spend less. To sum
up how much has changed in the U.S. economy,
6.3% of our GDP came from residential
construction in the fourth quarter of 2005, while
only 2.2% came from this component in 2011.
Regional Economic Strengths. Despite the
overall environment, the regional economic
outlook for our 16-state franchise gives us reason
to be cautiously optimistic. Our footprint
includes some of the most attractive markets
in the Southeast, and while these markets still
face the same prolonged challenges of high
unemployment and housing devaluations, we
believe long-term prospects are very good.
Population growth throughout our footprint  a
primary driver of sustainable economic growth 
is projected to be 4.2% between now and 2015,
approximately 10% greater than the national
projection. We believe this anticipated longerterm
growth in our footprint will provide a tailwind
as we expand the number of customers
we serve in our existing markets, which in turn
can ultimately increase profitability for Regions.
One example of the type of opportunity we see is
in Nashville, one of the largest healthcare centers
in America and home to Regions Healthcare
Specialty Banking Group. While the bulk
of the healthcare business is in our Nashville
market, there is tremendous opportunity with
this growing segment throughout most of our
Southeastern footprint. In the last two years
alone, we have closed over $1 billion in new
loan commitments and have grown outstanding
loan balances to more than $750 million.
Overall, spending on healthcare is anticipated to
grow by 5.8% per year between 2010 and 2020
as baby boomers start to retire.
Another example is in Mississippi, where hightech
manufacturing and ship building industries
added 4,000 new jobs last year. Throughout our
Southwest region, which includes Texas, Arkansas,
Mississippi and Louisiana, there are about
30 million acres of agricultural opportunities.
Regions currently banks 5% of that acreage, and
we see tremendous potential to carefully expand
those relationships due to our long history and
expertise in the agricultural market. Commodities
are produced throughout the Southwest,
which gives Regions a prudent way to benefit
from the global demand for commodities.

Increased Regulatory Oversight. Over the past
few years, the financial services sector has seen
a significant increase in regulatory oversight,
which will continue to impact a couple of critical
areas for banks: capital and liquidity requirements,
and the ability to generate revenue.
Regarding the first area, it is clear that financial
institutions will be required to hold more
capital and maintain higher liquidity than they
have in the past. Stress tests and in-depth
capital planning reviews are now a standard part
of the regulatory methodology. In 2010, a new
set of regulatory standards called Basel III was
proposed to increase the quantity and quality
of bank capital. The Basel III directives will require
banks to hold high-quality capital equal to
7% of their risk-weighted assets plus additional
buffers that will vary by size and complexity of
the institution. Moreover, in January 2012, the
Federal Reserve began conducting what will be
an annual assessment called the Comprehensive
Capital Analysis and Review, or CCAR. This
review requires U.S. banks to demonstrate they
have the capital levels to withstand losses and to
meet the Basel III standards. Basel III also introduced
new liquidity requirements for financial
institutions, calling for a liquidity coverage ratio
that will prohibit banks reliance on short-term
funding sources. While these standards are not
final, our estimated Basel III Tier 1 common and
Tier 1 capital ratios1 are above the respective
7% and 8.5% minimums, based on our current
understanding of these guidelines.
On the revenue side, the Durbin Amendment,
which went into effect in late 2011, will reduce
debit interchange revenue by approximately
50%, or $12 billion, for the industry annually.
This revenue loss from the industry will have
to be recovered in some manner as the industry
moves forward, either through revenue
enhancements or expense cuts. We have been
in a free banking environment for almost two
decades, which is unique in a retail business,
and its clear that changes must be made. The
burden is on us to create value for which customers
are willing to pay a fair price.
Regions value proposition includes giving customers
choices in how and where they bank 
whether at a branch, an ATM, over the phone or
online. Value also means operating with integrity
and providing the expertise and personal service
that each customer deserves. Were focused on
delivering a wide range of products and services
that meet customer needs and offering them at
a fair price. We have changed the economics
of how we offer the traditional bank checking
account, and with that change we have moved
to a better business model where customers
can more fully understand the costs of our services
and more easily determine the value they
receive from a relationship with Regions.
CLEARLY DEFINED
STRATEGIC PRIORITIES
Navigating through this environment and the
way it changes our business requires that all
of our 23,707 bank associates understand our
business plan so they can execute it. To that end,
we have clearly identified four strategic priorities:
 Strengthen Financial Performance
 Focus on the Customer
 Enhance Risk Management
 Build the Best Team

Each of these priorities includes a plan to measure
performance  this is an integral part of our
corporate culture and key to our future growth.
We believe teams perform best when all players
understand their roles and have accountability
for what they accomplish both individually
and collectively.
Strengthen Financial Performance
Our 2011 results reflected our priority of strengthening
financial performance. Adjusted pre-tax
pre-provision income2, or PPI, totaled $1.9 billion,
excluding securities transactions, leveraged
lease terminations, branch consolidation costs
and other property charges and goodwill impairment.
This is an increase of 11% compared to
the prior year. Our associates delivered these
results and many more during a year where
headwinds challenged the way we operate and
the banking environment continued to evolve.
Strong Low Cost Deposit Growth. Our strategy
to lower deposit costs while remaining competitive
and improve funding mix produced solid
results for the year, as deposit costs declined
29 basis points relative to our peers, which on
average declined only 21 basis points. Average
low cost deposits grew 6% for the year, reflecting
our ability to significantly re-price maturing
certificates of deposits. As a result, funding
costs were further reduced by a favorable shift
in our deposit mix. Given the significant amount
of certificates of deposit maturing in 2012,
we see additional opportunities next year.
We were pleased to see our deposit strategy was
validated by the FDICs annual market share
report, which showed that even as we reduced
interest rates on deposits, Regions maintained
a No. 1 market share position in Alabama, Tennessee
and Mississippi and also maintained a
position of fourth or better in Florida, Arkansas
and Louisiana.
Improved Credit Quality. Our provision for credit
losses was 47% lower in 2011 than 2010, and
we saw continued improvement in our credit
quality trends. Although our non-performing
assets remain high at $3 billion, they did decline
24% in 2011. Other credit metrics also
showed improvement during the year as our
business services criticized loans, which are
our earliest indicators of problem loans, declined
35%, early and late stage delinquencies
declined 24%, and net loan charge-offs
decreased 29%. Furthermore, our allowance
for loan losses remains strong at $2.7 billion, or
3.54% of total loans. While our improvement in
credit quality will generally track the economic

recovery, which has been slower than we would
have liked, we do expect credit costs to continue
their downward trend into 2012.
Loan Growth. Reflecting both consumers and
business owners lack of confidence in our nations
economic recovery, overall loan demand
remains weak. In the last five years, consumer
home equity loan applications have declined
75%, while loan approvals have remained
steady. Businesses and consumers who are not
confident in the economic outlook simply will
not take on the risk of additional debt if they
can avoid it.
In spite of this environment, Regions extended
$60 billion in new credit in 2011 and $185 billion
since 2009. Our loan production included
$51 billion of business loans, of which $15 billion
were new commitments. Our consumer
loan production totaled $9 billion, a 16% decline
over 2010.
We also made the strategic decision to re-enter
the credit card business for consumers and
small businesses as part of our effort to further
diversify our revenue streams and better serve
our customers. In just six months, we increased
our account production by 5% and generated
$1.1 billion in outstanding loans on the balance
sheet and an additional $6.7 billion in available
credit for our customers. In addition, consistent
with our strategy of expanding our loan product
offerings to meet consumer demand and market
conditions, we also re-entered the indirect
auto lending business, which generated $1 billion
in volume for the year. This was one of the
primary drivers of total consumer loan volume,
which increased 36% in 2011.
Capital and Liquidity Strength. Regions capital
levels  a key measure of a banks financial
strength  are strong, and we are confident they
will remain so even as tougher capital standards
are imposed. Clearly, the de-risking of
our portfolio over the last three years has paid
tremendous dividends in terms of enhancing
our portfolio.
At December 31, 2011, our capital levels also
exceeded Basel III requirements, with a Tier 1
ratio standing at 11.4% and a Tier 1 common1
ratio totaling 7.7%. Since the end of 2010,
our Tier 1 common1 capital increased 66 basis
points to end the year at 8.5%. Liquidity at both
the bank and the holding company remains
solid. We are primarily core funded and have
a loan to deposit ratio of 81%. Cash at the parent
holding company totaled $2.5 billion and
is above our policy minimums of maintaining
a sufficient level of funding to meet projected
cash needs, which include all debt service,
dividends and maturities for the next two years.
As part of our process to evaluate how to best
manage our capital and increase shareholder
value, in January of this year we announced that
we entered into a stock purchase agreement
to sell Morgan Keegan & Company, Inc. and
related affiliates to Raymond James Financial
Inc., for $930 million. As part of the transaction,
Morgan Keegan is expected to pay Regions a
dividend of $250 million before closing, pending
regulatory approval, resulting in total proceeds
of approximately $1.18 billion to Regions.

Importantly, this transaction reduces our overall
risk profile, enhances liquidity and improves
Regions capital ratios while reducing our expense
base and enhancing our focus on our core
banking business. Because we retained our asset
management and trust businesses, we remain
well positioned to continue serving the wealth
management needs of our customers. This transaction
establishes an on-going relationship with
Raymond James, further enhancing our ability
to serve our existing customers. We also expect
that we will maintain important business
relationships with Morgan Keegan associates
that were developed over many years of working
together to serve our customers needs.
This transaction, along with achieving continued,
sustainable profitability and demonstrating a
material improvement in our credit quality, are
all key steps toward the eventual repayment of
the governments investment in Regions. We
believe we have made tremendous progress in
these regards, but we remain prudent and want
to pay back the governments investment from a
place of strength and in as shareholder-friendly
manner as possible.
Margin Improvement. This prolonged environment
of low interest rates that we are currently
operating in has continued to put strains on net
interest income and the resulting net interest
margin. While we have been able to generate
low cost deposit growth and thus reduce deposit
costs to 49 basis points, low loan demand has
put stress on our loan yield. Net interest income
for the year was $3.4 billion, or 0.6% higher
than 2010. However, net interest margin for the
full year was 3.07%, a 16 basis point improvement
from the previous year.
We continue to forecast that interest rates will
remain low through 2012 and barring any unexpected
movement in interest rates, we expect
net interest margin to grow modestly over
the balance of 2012, primarily due to high
yielding CD maturities.
Growing Non-Interest Revenue in a Challenging
Environment. Non-interest revenue, which
is primarily made up of service charges and
interchange fees, was a particular focus in
2011. With the new restrictions recently imposed,
the ability to grow fee-based revenues
has proven to be a critical aspect of our strategy.
New products and services not only allow
us to meet evolving customer needs, but also to
diversify and grow dependable revenue streams.
Our service charges remained steady in 2011,
while most of our industry saw declines. Contributing
to this was our restructuring of checking
accounts to fee-eligible, which was a necessity
to offset lower interchange income. While we
have seen an increase in the volume of transactions
due to the number of new accounts that we
opened, total spending among consumers has
decreased due to the sluggish economy. In addition,
over the last two years we have seen our
second and third best years in mortgage income
and production.

Expense Control. We understand that tightly
controlling expenses during a slow economy is
critical, and Regions remains focused on taking
steps to improve productivity and efficiency. We
are fundamentally changing our cost culture by
continuously seeking opportunities to improve
and control our expense base, while still making
the appropriate investments to enhance
our competitive position and remain a superior
service provider. Non-interest expenses totaled
$3.9 billion in 2011; however, non-interest
expenses from continuing operations3, excluding
the goodwill impairment and regulatory
charge, were $3.6 billion, 5% lower than the
prior year. Regions reduced expenses in 2011
year-over-year faster than almost all of our
peers, and weve accomplished that by containing
credit costs, salaries and expenses. Even as
the economy shows signs of improvement, we
will continue to control expenses and find more
efficient ways to operate.
Focus On The Customer
Keeping customers at the center of every business
decision is what we do each day. We are
focused on building deep and long-term relationships
and are committed to being a trusted
financial advisor to our customers. Our high
loyalty and satisfaction scores come from a
deliberate and disciplined effort to drive a service
culture in the company. As a customercentric
organization, we continuously seek
feedback on the service our customers receive
and, based on that feedback, make adjustments
to enhance service quality and ensure
our associates are providing the very best
banking experience.
A Better Banking Experience. We find it very
encouraging that in 2011  at a time when many
seemed negative toward banks as a result of the
economic downturn  Regions customer satisfaction
and loyalty scores were at an all-time
high. We consistently achieved top rankings in
customer experience and brand loyalty:
 J.D. Power ranked Regions highest in retail
banking satisfaction in Florida
 Regions ranked among the top 12 banks for
retail banking satisfaction in all five of J.D.
Powers geographic regions that correspond
with our footprint
 J.D. Power ranked Regions at or above the
industry average in every component of the
study, which also highlights above-average
improvement in all our delivery channels

in person, online, at ATMs and over the phone
 Greenwich Associates gave Regions its Excellence
Award for Small Business and Middle
Market Banking
 Temkin Group ranked Regions as the top bank
for customer experience and one of the top
companies in America for customer service
across all industries
The key to our success in delivering a better
banking experience is how we focus on meeting
customer needs  not on selling products.
Recognizing that every customer has unique fi-
nancial needs, our bankers develop a thorough
understanding of a customers goals and then
recommend the right products and services for
them that will help them succeed.
New Products and Technology Designed To Deliver
Value. Regulatory reform changed the way
banks generate revenue, which in turn impacted
product design and pricing, with the most
significant change being that the industry would
now charge for products and services that were
once free. With this paradigm shift, Regions took
a deliberate and proactive approach in product
development so customers would see the value
in a banking relationship with Regions. Our Now
Banking suite of products is a good example
of an innovative solution for consumers who
want greater control and take a pay as you go
approach to financial management. With enhanced
check cashing, a reloadable Visa prepaid
card, and money transfer and bill payment
services, consumers have the increased flexibility
to manage their finances the way they prefer.
Further supporting our efforts to improve the
customer experience, Regions invested in new
technology last year designed to improve our
efficiency and effectiveness in serving our customers.
The new system combined five systems
into one, allowing the teller to see a customers
entire relationship with Regions, from a mortgage
to a credit card or a business loan. This
technology gives us the ability to sell products
that customers need, which in turn improves
our bottom line. It also enhances our risk management
practices by allowing a manager to determine
the appropriate level of credit to extend
and the types of products to recommend.
Serving the Unique Needs of Affluent Clients.
In 2011, Regions announced the formation of
the Wealth Management line of business to
serve the unique needs of our affluent client
segment, which is strategically important to our
future growth. Based on market research, we
determined that the size of the affluent markets
in our branch network represents a significant
opportunity to deepen existing relationships
and to obtain more market share throughout the
footprint. To capture this opportunity, we have
brought together Private Banking, Personal
Trust, Institutional Trust and Insurance under
the Wealth Management umbrella, creating a
structure that enables us to take a holistic approach
to serving our clients. Wealth Management
will build on Regions strengths by being a
trusted advisor to our clients, providing exceptional
service quality and delivering value.
Financial Education. We believe there is a
shared responsibility between customers and
banks to avoid another financial crisis  and
at the core is ensuring that customers understand
basic financial management. As part of
our commitment to help customers make sound
decisions, Regions partners with the Financial
Services Roundtable to deliver financial education
to high school students. The program is a
partnership with the EverfiFinancial Literacy

Platform, a web-based curriculum that teaches,
assesses and certifies students in hundreds of
financial topics. One of the best examples of
this partnership came during the 2010  2011
school year, as our associates helped bring to
life a financial education program for 14 high
school classrooms in Atlanta, Georgia. With
our support, these schools adopted the EverFi
Financial Education Program and more than
1,000 graduates obtained a solid foundation of
personal finance and money management skills.
We understand that a lack of knowledge of basic
financial management played a role in the
economic crisis several years ago. With a goal of
supporting financial stabilization throughout the
communities we serve, we all need to be proactive
and we take our role seriously in doing our
part to ensure that history doesnt repeat itself.
Loyalty Drives Performance. Regions loyalty
scores are in the top 10%, which is a marked improvement
to the average score we had in 2008.
Weve invested time and resources to understand
the correlation between loyal customers
and higher loan and deposit balances  and notably,
there is a clear correlation. We learned that
loyal customers have on average $1,000 more
in deposit balances versus non-loyal customers.
For loan balances, loyal customers have $1,900
more versus non-loyal customers. Furthermore,
over an 18 month period, loyal customers increase
their deposit balances by $2,500 versus
non-loyal; and loyal customers increase their
loan balances by $3,000 versus non-loyal customers.
Were not being complacent because of
these results and will continue to find ways to
raise the bar on service excellence.
Relationship managers in our Business Services
Group are committed to delivering clients
a world-class experience by focusing on three
basic tenets:
 Understanding our clients industries, operations,
finances, expectations and needs
 Interacting effectively with our clients on a
consistent basis
 Serving as strategic advisors who are valued
by our clients
According to a survey of our business clients,
this focus has resulted in clients who are committed
to keeping their banking relationship with
Regions. On average, the business clients who
report having a committed relationship with us
hold an average of 30%  50% more in deposits
and loans with us.
The Business Services Group built sustainable
performance in 2011 by leveraging our
core strength in the small business and middle
market segments. Small business, for example,
experienced interest free deposit growth of 13%
in 2011.

Our Commercial and Industrial segment finished
the year with loan balances up $2 billion
for 2011. Throughout the year, we continued
building out our specialty lending groups  like
Healthcare, Technology and Defense, and Energy,
which produced more than $2.6 billion in
loans outstanding.
Diversifying revenue streams and improving
client profitability are critical to strengthening
our financial performance. And by delivering
all Regions has to offer to our business clients,
getting paid an acceptable return for our credit
risk, and exiting unprofitable relationships, we
will expect to deliver improved results.
Fair and Responsible Banking. We see a commitment
to fair and responsible banking as an
important part of our dedication to providing the
very best customer service. Beyond complying
with fair lending laws, our obligation to serve
our customers as a fair and responsible banking
partner is rooted in our values, our trust and
integrity, and our business strategy.
Acting responsibly is not new. Regions associates
have always been committed to doing what
is right, and its about much more than complying
with policies. We have enhanced our traditional
fair lending practices, such as judgmental
underwriting and discretionary pricing, to
encompass the entire loan life cycle, including
servicing and collections. We have implemented
a robust fair and responsible lending program
that will improve transparency in our lending
policies and practices, as well as educating all
associates so that fair lending is an ingrained
part of how we do business every day.
We intend to be an industry leader in this area by
offering quality products at a fair price, by treating
customers consistently at every phase of the
lending process, by keeping customers informed
about their banking options and by helping
economically challenged communities thrive.
Providing consistently good service to all customers
and strengthening our focus on fair and
responsible banking practices is not only good
for business, but also the right thing to do for
customers and communities.
Enhance Risk Management
In 2011, we increased resources and oversight
to ensure we have the right core enterprise risk
management system in place. While there is cost
associated in building this type of comprehensive
infrastructure, the result of our investment
will create a long-term benefit for shareholders.
We consider this initiative to be the responsibility
of all our associates each and every day.
As part of our effort to improve risk and quality
control measures, we moved all underwriting
functions in Business Banking, Community
Banking and Private Banking to the Risk Management
Group. We believe this structure will
create consistency and streamline processes
so that we clearly understand, mitigate and
document the risk associated with our credit
relationships. In addition, realigning the credit
functions within Risk Management will allow our
geography associates to provide increased focus
on client acquisition, expansion and retention
activities.
Customer Assistance  in Good Times and Bad.
Credit quality plays a big role in risk management,
and we believe there should be a shared
responsibility between Regions and our customers
when they face tough times and have
trouble making their loan payment. Thats why
we are proactive about working with customers

who are in distress. We want to do our part so
they can keep their homes, their cars and their
businesses. Shared responsibility means educating
customers, helping them take ownership
of their dreams and helping them find a solution
when times are tough.
Our Customer Assistance Program, which was
started in 2007, is a great example of how we
work with customers to bridge them to better
times. To date, weve helped more than 42,000
people stay in their homes, and our foreclosure
rate is less than half the national average. The
guiding principles of this program are to:
 Focus on long-term solutions
 Balance bank interests and customer needs
 Communicate proactively
 Educate customers and communities
 Provide a quick and simple workout process
Building the Best Team
Standing at the foundation of all we do is our
talented team of associates, and we are committed
to ensuring that we retain our best talent
while also recruiting new talent. Like our other
strategic priorities, we manage performance to
ensure we are maximizing our potential individually
and as a team. We are committed to
fostering an environment where associates can
make a positive difference, grow professionally
and achieve success.
Simply stated, we want to build the best team
in the industry by executing a comprehensive,
integrated approach to organizational development
with a focus on our five corporate values:
 Do What is Right
 Put People First
 Reach Higher
 Focus on Your Customer
 Enjoy Life
This approach includes a thorough review of
everything from recruiting practices to associate
retention, from leadership development to
performance management. We want to ensure
we provide associates the kind of opportunity
that allows them to come to work with a sense
of purpose and go home with a sense of pride
every day.
In our leadership development programs, we
stress the importance of collaboration, integrity
and accountability. Our managers ensure their
teams have clear goals established and that they
provide feedback regularly  not just once a year
in formal reviews. Excellent teams are built when
team members clearly understand their roles
and how their contribution influences success. I
believe our commitment to our values and to our
associates helped guide us through a challenging
economy, and we are confident this same commitment
will be key to our success in the future.
LOOKING AHEAD
The overall consensus for 2012 from economists
is for marginally better but still below-trend
economic growth (GDP growth between 2%
and 3%). The unemployment rate is projected
to remain near 9% for most of the year. Housing
starts are expected to total approximately
700,000, or 100,000 more than in 2011, but
only about one-third of the peak of 2.1 million
units witnessed in 2005. Inflation is projected at
just over 2% for 2012.
As we look ahead to 2012, we recognize that
we still face challenges  both with respect to

the economy and the resulting banking environment.
With this backdrop, we will remain
intensely focused on executing our business
plan and on the areas we can control. We are
confident that we can deliver continued improvement
in our operating results and build
long-term shareholder value  a belief that is
built upon just how far Regions has come in the
past few years in spite of the market turmoil.
Looking at a few key metrics, we:
 Reduced our Investor Real Estate portfolio
50% from the end of 2007
 Doubled our loan loss allowance to $2.7 billion
 Increased our Tier 1 common1 ratio 194 basis
points to 8.5% in four years
 Reduced our loan to deposit ratio from 100%
to 81%  we are core funded
As a result of our de-risking efforts, our strong
core franchise, diverse business mix and a clear
path to building sustainable performance, we
believe Regions is clearly a much stronger franchise
today. Our strategy is working. Recovery
and rebuilding do take time, but with each passing
quarter, we are demonstrating progress and
profitability moving forward.
THANK YOU
In closing, I want to thank our Board of Directors
for their support and guidance over the past
year. I greatly appreciate our chairman, Earnie
Deavenport, for his wise counsel and commitment.
I also want to thank Dr. Condoleezza Rice
for her valuable insight and expertise as an
advisor to our Board of Directors. And I also
want to thank our Operating Committee for its
leadership through an incredibly challenging
environment, and our associates who provide
award-winning customer service every day.
Finally, I thank you, our shareholders, for your
confidence, continuing support and investment.
O.B. Grayson Hall
President and Chief Executive Officer