Fiscal 2016 was an interesting and challenging year in apparel retailing. Some believe there has been a shift in consumer
behavior while others believe the American fashion shopper is simply uninspired by a lack of newness. Our consumer has
an abundance of shopping choices from a number of channels. We recognize the undeniable importance of increasing our
market share during this retail crossroads by giving our customer great reasons to visit Dillards. Particularly, we are
pursuing even more distinctive brands and service experiences that excite and engage our shoppers while elevating our
service level with every visit  both instore and online.
We believe while mediocre retailers are at risk, those of excellence still have a bright future. Accordingly, we were recently
encouraged and honored to be named the number one department store in customer satisfaction by a major, national
consumer satisfaction survey.
During 2016, shareholder return remained a priority at Dillards. We returned almost half our cash flow from operations to
shareholders through share repurchase ($246 million) and dividends ($10 million). We believe this is the best deployment
of excess cash as we continue to balance shareholder return with reinvestment in the business. Over the past seven
years, we have returned $2.7 billion to shareholders, while reducing our outstanding share count from 73.8 million to 31.5
million shares.
We believe our strong balance sheet provides us standout stability relative to our peer group in challenging times.
Notably, we own 90% of our store square footage, and our debt level remains low relative to our peers. While others are
currently responding with store closures, we note that we have strategically closed approximately 50 stores since 2008,
roughly 14% of our fleet. We continue to evaluate store closures where appropriate, but we remain more excited about
what we can accomplish with our stores than without them.
We remain focused on the long-term at Dillards. We are excited about the changes underway in our stores and online with
new brands and new customer experiences. We will continue to pursue newness and inspiration in fashion, in beauty and
for the home. We thank you, our shareholders, for your continued support of Dillards in partnership with approximately
40,000 dedicated associates. We look forward to serving you in the future.
William Dillard, II Alex Dillard
Chairman of the Board & President
Chief Executive Officer