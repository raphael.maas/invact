dear Shareholders

2012 was a year of transition for Nabors. We focused sharply
on things we could control and made great strides toward
achieving the priorities we laid out at the start of the year:
 restoring our financial flexibility,
 driving technology and innovation,
 streamlining our business,
 strengthening customer alignment, and
 enhancing operational excellence.
The progress we made on our priorities reflects the quality
of the entire Nabors team, our extensive assets and the
geographic diversity of our operations, which allows us to
leverage our scale.
We enhanced our financial flexibility by achieving record operating
revenues, gross margin and EBITDA, and re-emphasizing
capital discipline. This allowed us to reduce our net debt by
$678 million from the first quarter, despite significant market
headwinds. In addition, we decreased the interest rate, increased
the limits and extended the term of our revolving credit facilities
to create ample liquidity for the short to medium term. We
expect to realize significant net operating cash flow again this
year, despite weak North American market conditions. Our
enhanced financial strength provides us with flexibility and
capacity to pursue any number of value-enhancing strategies,
such as buying back stock or paying dividends to return capital
to shareholders, or funding core business growth investments
or pursuing attractive acquisition opportunities to improve
our competitive position, all of which can be challenging in
a cyclical business.
Our technology and innovation initiatives have been a significant
beneficiary of our enhanced financial strength. The majority
of our capital expenditures in 2012 were in the U.S. Lower 48
operations, as we deployed new-builds and advanced our revolutionary
PACE-X rig from design to manufacturing and field
deployment. This rig is the culmination of more than 40 years
of technical ingenuity and Nabors dedication to innovating
technologies for large-scale development of unconventional
resources. With 17 rigs contracted to deploy this year across
most of the U.S. shale plays, we are helping customers improve
efficiency, while addressing their concerns for minimal environmental
impact. The PACE-X rig represents the next generation
of drilling in both domestic and international environments.
We also worked to streamline our business by selling non-core
assets, particularly our oil and gas businesses. Although interest
in these assets waned toward the end of 2012, we have generated
nearly half a billion dollars by divesting non-core assets
since our streamlining efforts began in late 2011. We remain
committed to the orderly monetization of these assets and
to redeploying that capital.
To serve our customers in a more cost-effective and disciplined
manner, we merged our pressure pumping and U.S. well servicing
units to create our Completion & Production Services business
line. We also consolidated our U.S. Offshore and Alaska drilling
operations with our U.S. Lower 48 drilling operations which,
along with our Canada, International and manufacturing and
technology arms, form our second business line, Drilling & Rig
Services. We bring an extraordinary amount of experience and
extensive technical knowledge to these businesses. Realigning
them allows us to enhance operational excellence, create new
growth opportunities, develop new technologies, and improve
our customer service and interface.

Our safety initiatives produced Nabors best safety record ever
and helped us foster operational excellence. Although we continued
to outperform industry incident statistics in 2012, our
unwavering goal is zero incidents. We have invested a significant
amount of time and capital in safety initiatives over the last
decade, making us one of the safest contractors in the world.
We will continue to aggressively focus on safety, and that
commitment permeates our entire organization.
Looking ahead, the near-term market in North America is challenging
and opaque. Lower customer spending levels will likely
continue, but that does not diminish our optimism; we are well
positioned globally and on increasingly firm financial ground
to weather any impact on our operations. International markets
also remain challenging in the near term, but we see encouraging
signs and believe the longer term looks increasingly bright.
The market for high-quality rigs is tightening and, as more
opportunities emerge, we will continue to regain operating
leverage. Nabors is active in virtually all established and developing
markets and is uniquely positioned to capitalize on these
trends with our existing international infrastructure, local labor
force and know-how, rig availability and favorable tax structure.
While we continue to focus on generating cash flow, we will
not waver from investing wisely in core markets to grow our
business. We believe a key driver of future success will be
technological differentiation. With our Canrig and Ryan services
and products, Nabors is uniquely positioned to lead the industry
through emerging trends. The ability to offer a higher degree
of remote monitoring and control and to optimize drilling
parameters is the future of drilling. Canrig is a leading developer
of technology
in these areas and has the technical capability to
implement innovative solutions, while Ryans directional drilling
expertise brings all of the necessary intelligence under one roof.
Our strategic focus supports our twofold commitment to
make Nabors the global provider of choice in optimizing the
well life cycle for our customers, particularly in unconventional
resources, and to generate long-term shareholder value.
Our financial strength and strong cash flow generation enabled
our recent initiation of a quarterly cash dividend. We cant
do it all in one year, but we will continue to deliver value while
we maintain an appropriate level of capital investment for
future growth.
My admiration for our talented and dedicated workforce
reinforces the conviction I have in our strategy. Working
together, we will achieve our goals.
Sincerely,
Anthony G. Petrello
Chairman, President & Chief Executive Officer