Dear Shareholders,
I am pleased to report that
Time Warner Cable had a good
year in 2011, despite intense
competition and a continued
weak economy. We rolled
out more than two dozen
new products and features,
enhanced our existing
offerings and marketing
capabilities, and took steps
to expand our footprint and
make our company more
competitive. As a result of our
achievements during the year,
we were able to reinvest in
the business and still return a
significant amount of capital
to you, our shareholders.
FINANCIAL AND OPERATIONAL
PERFORMANCE
Notwithstanding the external
factors that continue to
challenge us, our company
posted good financial results
in 2011:
 Revenues increased 4.3%
over 2010 to $19.7 billion.
 Business Services revenues
grew an impressive 32.7%
to $1.5 billion.
 Advertising revenues
matched 2010 levels,
despite not having the
benefit of election-year
political spending.
 Operating income increased
10.3% to $4.1 billion,
driven by rigorous cost
managementeven as we
invested in new growth
opportunities.
 Diluted earnings per share
grew 36.5% to $4.97.
 We returned $3.3 billion
to shareholders through
dividends and share
repurchases.
Primary service units
(PSUs)total video, high-speed
data and voice subscribers
grew to 27.1 million. Combined
double and triple play net
additions grew year over
year, as well. At the end of
2011, more than 60% of our
customers subscribed to a
bundle of at least two of
our services.
STRENGTHENING AND
EVOLVING OUR SERVICES
Our business is very
dynamic, and the appetite
of consumers is continually
evolving. We understand
the need to evolve with our
customers and drive changes
in the business that will
make us stronger and more
competitive. To successfully
navigate this ever-changing
environment, in 2011 we
focused on developing and
implementing both near- and
longer-term growth strategies
and enhancing the underlying
strength of our services

2011 HIGHLIGHTS Last year, we:
?? Introduced new products
and features to give our
customers more choices
and control. We were the
first cable company to
launch an app for the iPad
(TWC TV) that featured
live viewing of cable
channels at home.
We continued to improve
the app throughout the
year as it grew in popularity.
We also delivered an app
that enables customers
to use their Android
tablets and smartphones
as remote controls. I am
pleased with the robust
pipeline of innovative
products we have in store
for our customers in 2012
and beyond.
?? Drove very strong
residential broadband
subscriber growth. We also
expanded our Wideband
footprint, which offers the
next generation of Internet.
In addition, we enhanced
our broadband offerings by
giving subscribers mobile
access to the Internet
through the deployment
of Wifiaccess points in Los
Angeles. We will continue to
deploy WiFi, with a focus on
expanding coverage in L.A.
in 2012.
?? Took concrete steps to
accelerate growth in
Business Services, which
was our biggest success
story in 2011. We acquired
NaviSite, a premier
provider of managed and
outsourced information
technology solutions and
cloud services. With the
NaviSite acquisition, we
accelerated year-over-year
revenue growth in Business
Services to 33%. Excluding
NaviSite, Business Services
revenues increased an
impressive 24% in the same
period. We believe we have
only begun to tap the
potential growth in this area
of our business.
?? Renewed our commitment
to growing our core cable
business and expanded
our footprint, acquiring
NewWave Communications
cable systems in
Kentucky and Tennessee
and entering into an
agreement to acquire
Insight Communications,
which served more than
760,000 customers in
Indiana, Kentucky, and
Ohio. We closed the Insight
deal in February 2012.
?? Announced several
wireless-related
agreements with Verizon
Wireless. In the first
agreement, SpectrumCo,
a joint venture between
Time Warner Cable,
Comcast and Bright House
Networks, agreed to sell
its wireless spectrum
licenses to Verizon Wireless.
We also announced agency
agreements that will allow
us to sell Verizon Wirelessbranded
wireless services
and for Verizon Wireless
to sell our services. In
addition, we entered into
an agreement that provides
for the creation of an
innovation technology joint
venture to better integrate
wireless and cable services.

MAXIMIZING
PROFITABLE GROWTH
We entered 2012 positioned
for sustainable growth and
poised for further operational
improvements. As we look to
build on our momentum, we
are undertaking a number of
initiatives, including:
 Expanding our Business
Services product offerings,
growing our sales force,
improving productivity,
and increasing our
serviceable footprint
 Remaining focused on
maintaining and growing
our Residential Services,
through targeted sales
and marketing, an
improved customer
experience, and continued
product innovation
 Driving advertising revenue
growth, by capturing
political spending and
expanding our third-party
rep deals
 Launching new business
initiatives and product
enhancementsincluding
two previously announced
Regional Sports Networks
(RSNs) featuring Los
Angeles Lakers games and
content, Time Warner Cable
IntelligentHome (our
advanced home security
and monitoring service),
and the expansion of our
Wificapabilities
 Capturing synergies related
to our recent acquisitions
 Making prudent capital
investments in our
business to improve our
infrastructure and support
our plans for growth
A COMMITMENT TO LEAD
IN OUR INDUSTRY AND
OUR COMMUNITIES
I am proud of the steps
we took in 2011 to solidify
Time Warner Cable as a
leader in the industry and in
the communities we serve.
This commitment starts with
our own leaders. In 2010, we
strengthened our companys
leadership with the elevation
of Rob Marcus to President
and Chief Operating Officer.
In July 2011, we rounded
out the team with the
arrival of Irene Esteves as
Executive Vice President
and Chief Financial Officer,
succeeding Rob. With Rob,
Irene and other leaders
throughout the company, I
believe we have the
best leadership team in
the business.
I am also very proud of
the innovative work we
are doing to support our
communities at both the
local and national levelsby
increasing student awareness
of and interest in science,
technology, engineering,
and math (STEM). We are
inspiring the next generation
of problem solvers through
our Connect a Million Minds
(connectamillionminds.com)
initiative, which encourages
students to pursue STEM
education and careers.
I thank the more than 50,000
Time Warner Cable employees
for the genuine dedication
they bring to their work, day in
and day out. They are smart,
creative, and very committed
to our mission and values.
Finally, I extend my
appreciation to all of you.
You have our steadfast
commitment to continuously
strive to return long-term
value on the investment you
have entrusted to us.
Sincerely,
Glenn A. Britt
Chairman and
Chief Executive Officer
March 2012