DEAR VALUED
SHAREHOLDERS,

This years annual report theme  Deep Roots, Dynamic
Vision  is an apt descriptor of Brown-Forman. The intent is
to convey that our company is blessed with the values, traditions,
and know-how derived from decades of experience and
family ownership, while also possessing the creative spirit and
growth mindset that is so vital to enduring success. I can think
of no finer example of Deep Roots and Dynamic Vision than
our own Jack Daniels brand, which continues to propel our
company forward at the same time that it celebrates its own
150th anniversary.
FISCAL YEAR 2016
OUR UNDERLYING GROWTH STORY CONTINUES

Building on so many years of excellent progress, this past year
was another rewarding year for Brown-Forman. And the story
of fiscal 2016 was not unlike recent years.
We grew net sales 5% (-2% as reported) and operating
income 8% (49% as reported), with both of these metrics
ahead of our industry competitive set. Our reported results
were again hurt by the strong dollar, but those same results
did benefit significantly from a one-time gain on the sale of
Southern Comfort and Tuaca.
Brown-Forman was propelled once again by our portfolio
of premium American Whiskeys, with the Jack Daniels trademark
continuing to be the primary driver. The Jack Daniels
family performance continued to benefit from innovation as
Jack Daniels Tennessee Honey and Jack Daniels Tennessee
Fire contributed nicely to growth, and reached 1.9 million
cases in total volumes. Gentleman Jack, Woodford Reserve,
Old Forester, and Sonoma-Cutrer continued to post excellent
results, and our Casa Herradura tequila portfolio had perhaps
its best year yet since becoming part of the Brown-Forman
family nearly a decade ago.
We continued to benefit from our balanced geographic
diversification. While fiscal 2016 was a more challenging
year for our emerging markets business and travel retail
channel, the United States and other developed markets
delivered consistently solid growth, helping to pick up some
of the slack.

Brown-Formans Return on Invested Capital (ROIC) was
23%, the highest in a decade. Our excellent ROIC is emblematic
of the companys capital efficiency and organic growth
emphasis. Total Shareholder Return (TSR) for the year was a
solid 8%. The companys TSRs over the 5-year (+18%) and
10-year (+12%) horizons remain well above our industry, the
S&P 500, and Consumer Staples.
While fiscal 2016 results were similar in many ways to prior
years, you should not interpret that they were easily attained.
We operate in an intensely competitive industry that is exposed
to a dizzying array of economic, social, and regulatory influences.
Rest assured, the Brown-Forman management team
is constantly making adjustments that we believe will ensure
continued success.
INVESTING TODAY FOR TOMORROWS GROWTH

In addition to delivering solid short-term results in fiscal 2016,
I believe this past year will be remembered for the work we
undertook to position the company for success in 2025 and
beyond. While we have always taken the long view of our business
(somewhat by necessity, given the aging cycles for many
of our products), I believe that the last 12 months have been
even more exemplary of Brown-Formans long-term orientation.
The acquisition of GlenDronach, BenRiach, and Glenglassaugh
single malt Scotches, the investment in Slane Irish
Whiskey, and the creation of Coopers Craft Bourbon all
transpired in the last year. When considered alongside the
disposition of Southern Comfort and Tuaca liqueurs, we again
demonstrated a willingness to sacrifice some short-term sales
and profits for the benefit of improved rates of growth and
superior value creation over the longer term.
This long-term view is also evident through our continuing
capital investments behind Jack Daniels, Woodford Reserve,
and Old Forester. These investments will expand production
capacity and enhance our hospitality platforms as we plan
for greater consumption and additional visitors to our brand
homeplaces in the years ahead.
This current period of investment for Brown-Forman is
reminiscent of similar occasions in our recent history when
we made meaningful changes to benefit our long-term
future. Our geographic expansion of the early 1990s certainly
comes to mind, as do our dispositions of consumer durables
in the mid-2000s and the sale of our popular-priced wines to
enable a stronger focus on premium spirits. Similar to today,
these prior moves directed the company toward the very best
opportunities for growth and value creation over the foreseeable
future.
A FUTURE AS BRIGHT AS OUR PAST

I am often asked to discuss the companys prospects for the
future. Stakeholders of Brown-Forman appreciate our combination
of high margins, high returns, organic growth, strong
capital stewardship, and acceptable risk tolerance. Of these
five, the sustainability of our growth is the most probed by
followers of the company. They wonder, Can we keep it going,
and if so, for how long?
My response will sometimes reference our 10-year Total
Shareholder Return. Since past performance is no guarantee
of future performance, my audience usually wants more proof.
I offer this by citing my belief in our brands, their long runways
for growth as evidenced by their relatively low market shares,
and the quality of both our people and our culture (these last
two being the most essential ingredients to our companys
enduring success).
In recent years, curiosity in the United States has naturally
centered on the resurgence (and sustainability) of the American
Whiskey category. The category momentum is observable
by studying industry metrics, by looking at any restaurants
back bar, or by listening to someone order an Old Forester or
Gentleman Jack at a bar.
Bourbons and American Whiskeys are on a roll of late. And
while this is a source of both momentum and competition for
Brown-Forman, the categorys growth, in my view, is decisively
more of an opportunity than a threat. Regardless of how
one views this particular point, it is important to remember
that Brown-Forman had exceptional success prior to American
Whiskeys surge  and I believe we will continue to succeed
even if the category momentum slows at some point down the
road. Its my personal belief, however, that the category growth
can continue well into the future (if for no other reason than
the fact that Brown-Forman is its leader and we intend to do
our very best to grow our brands).
To be convinced that American Whiskey can continue
the roll it has been on, its necessary to understand why it
enjoys the momentum it does today. While it would be easier
to understand if there were a singular silver bullet explanation,
a comprehensive consideration yields many factors that,
in combination, are some of the most likely contributors to
todays American Whiskey boom.
To start, on a broader level, todays consumers are increasingly
seeking out brands that are authentic and real. As part
of this, they often appreciate things that are down-to-earth,
unpretentious, humble, folksy, and sometimes rugged.
Evidence of this can be observed in the beer business (craft
beers), the soft drink business (old-time sodas), the apparel
business (where more substantive materials like flannel or
rough-hewn designs are more prominent), and even in our
own personal appearances (as heavy beards are far more
commonplace).
In addition to this desire for authenticity is a longing for a
simpler, less hectic time than we typically experience today.
As a result, things that have retro or throwback appeal
are attractive to todays consumer. Consider vinyl records, the
Mad Men television series, and even Old Forester as such
examples. Important to this is having a real history and the
interesting stories that support it. Today, its pretty cool to celebrate
a 150th anniversary or to have lasted through a couple
of World Wars or, in the case of Old Forester, to have survived
the outright prohibition of your industry.
Provenance is also playing an important role. When we are
introduced to another person, we often ask them, Where are
you from? The same is increasingly true of brands. And it
seems the smaller the locale the better. Reflecting the longing
for a simpler life, rural towns, town squares, and neighborhoods
are more hip today than big cities or suburbia.
Paramount to American Whiskeys appeal has been a
growing interest by consumers in how (and by whom)
things are made. Bourbon has always been well-crafted, and
when consumers decide to learn more about their brands,
bourbons story as well as its easygoing hospitality fit
beautifully with their newfound curiosity. How nice that
Americas whiskey distilleries will open their front doors and
welcome guests to observe their production operations
and hear their stories. The fact that visitors seem to love the
experience does not surprise me. Visiting Woodford Reserve
or Jack Daniels Distillery sounds a lot more fun to me than
touring the manufacturing facility of my favorite ketchup or
cereal brand.
As a person who has worked my entire adult life in the
American Whiskey business, I can honestly say that I have
been waiting three decades for some things to become cool 
like flannel and ball caps, overalls and beards, and small towns
and old-time virtues. And guess what? It is happening.
I also believe that our increasingly chaotic lives have played
a role in American Whiskeys appeal. The realities and threat
of terrorism alone have made life more stressful. Additionally,
technological advances have enabled a world that is on call
and over-stimulated, far beyond what our brains and bodies
have been conditioned to absorb. We struggle to unplug and
when we do, we are woeful in our attempts to truly relax for a
sustained period.
As a result of this stress, the world decided it needed a real
drink  and bourbon is it.
Robust and flavorful on the palate, yet accessible and versatile,
American Whiskey is not too challenging of an acquired
taste for the experimental drinker. And, of added benefit for
consumers, it mixes as easily as it goes down straight. Its a
real drink that you can actually enjoy drinking.
So, American Whiskey possesses appealing traits such as
authenticity, history, craftsmanship, hospitality, unpretentiousness,
humility, great taste, and versatility. And most of the
worlds more than 7 billion people have never even tried it  yet.
Is American Whiskey here to stay? I believe it is, and at
Brown-Forman, we are certainly going to do our part to ensure
that it remains. And in doing so, we will strive to prove that
these timeless attributes indeed stand the test of time.
In closing, let me again thank our valued employees and
partners across the world for another year of excellent progress.
Their actions make Brown-Forman a place with Deep
Roots and Dynamic Vision. I also thank you, our shareholders,
for your continuous support of the company.
Sincerely,
Paul C. Varga
Chairman and Chief Executive Officer
June 28, 2016
