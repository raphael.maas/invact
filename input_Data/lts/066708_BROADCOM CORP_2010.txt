2010 WAS ONE OF THE BEST YEARS IN BROADCOM HISTORY
We brought to market innovative products that enabled us to gain market share, 
delivered record revenue and earnings per share, accelerated the return of capital to our
shareholders, expanded our technology and IP portfolios and generated record cash
flow from operations.
Scott A. McGregor
President and
Chief Executive Officer
Broadcoms financial commitment for 2010 was to outgrow the competition
and to grow profits even faster than revenue  and we delivered. Revenue
grew twice as fast as our competition in 2010, increasing $2.3 billion (or 52%)
year-on-year, to $6.8 billion. Forty-four cents of each incremental revenue
dollar flowed to the bottom line, resulting in record GAAP earnings per
share of $1.99 and record cash flow from operations of $1.37 billion. 
We continued to build our IP portfolio with more than 6,800 U.S. and
foreign patents issued and 7,800 additional patent pending applications.
The strength of our IP portfolio was recognized in 2010 when the IEEE
ranked us as one of the top 20 companies worldwide, the fourth strongest
among all semiconductor companies and having the number one portfolio
among fabless semiconductor companies.
Broadcoms manufacturing strategy continued to deliver solid results in
2010. As fab capacity tightened during the year, Broadcom continued to
deliver across all of its businesses. This ability to supply is a competitive 
advantage recognized by our customers who continue to see Broadcom as
a reliable partner through economic downturns or strong global recoveries.
We ended 2010 selling almost 500 million chips per quarter, with two thirds
of our devices in 65nm and half of our tape-outs in 40nm.
In 2010, Broadcom accelerated the return of capital to our shareholders.
We returned almost $450 million to shareholders through share repurchases
of $282 million and dividends of $163 million. Early in 2011, Broadcom
announced a 12.5% increase to our dividend and an accelerated share 
repurchase program, indicators of Broadcoms financial strength and
optimism for the future.BROADCOM CORPORATION 2010 ANNUAL REPORT PAGE 2
If we step back from these strong results and look at
the products and technologies that drive our success,
heres the real bottom line: Broadcom, more than ever,
continues to play a key role in powering the global
communications revolution and is firmly establishing 
itself as the innovation leader in communications 
semiconductors. Our tag line, Connecting everything
,
which we developed more than a decade ago, is more
fitting than ever as Broadcom
technology powers
some of the most cutting-edge products on the market
today, such as tablets, smartphones, Internet-connected
and 3DTVs, highly integrated set-top boxes, and
high-speed network switches and fabrics. There is a
voracious global appetite for connectivity, and Broadcom is helping to address that hunger.  
Our products are seemingly everywhere, embedded
within portable devices in the digital home and in
service provider and enterprise infrastructure equipment. Late last year, one of our employees challenged
his family to count the number of Broadcom products
within their home  mobile phones, set-top boxes,
high speed broadband modems, TVs, electronic
games, tablets, netbooks  and they tallied more
than a dozen chips! Tr y that in your home or business,
and see what you come up with.
Following are some of the highlights from 2010 and
early 2011 in our three target markets:
Home: Broadband Communications Products 
Broadcom continued to redefine the user experience
in the connected home to make life simpler by enabling
connectivity on a greater number of devices. Broadcoms technology powers consumer and service
provider devices, including those that address the
set-top box, broadband access and consumer electronics market segments.
From full resolution 3DTV to multi-room digital video
recording, Broadcom enabled innovative new applications to experience entertainment and information
multimedia throughout the home. With the addition of
powerline networking to its already extensive home
networking portfolio, Broadcom allows the design of
more highly integrated system-on-a-chip (SoC)
solutions that allow whole-home network coverage in
a wide variety of global deployment scenarios.
Playing a key role in driving the convergence of
telecommunications, Internet and cable television
networks in China, Broadcom announced its DOCSIS
-based Ethernet passive optical network (EPON)
SoC solution that is built on the companys proven
ability to design standards-based, highly integrated
and cost-effective cable and EPON technologies 
that meet the government and service provider
requirements in China. The company also announced
widespread support for its technology from seven
leading Chinese service providers and manufacturers.
In 2010, Broadcom launched FastRTV channel
change acceleration technology that improves 
television channel change speeds by up to five times
on a wide range of cable set-top box and universal
digital transport adapter (uDTA) chipsets. Comcast
became the first service provider to include Broadcom's FastRTV technology in its uDTA deployments to
transition from analog to standard definition (SD)
digital TV broadcasts. Broadcom also globally
expanded its digital-to-analog conversion technology
with targeted solutions for South America, China and
Japan, driving all-digital programming worldwide.
Broadcom also expanded its broadband carrier access
portfolio with femtocell technology, (small, low power
cellular base stations for the home that extend coverage
indoors where signals are weak). With wireless data
usage surging, Broadcom is accelerating the time-tomarket for this technology that has two major advantages: (1) it allows users to continue using their mobile 
devices without losing connectivity; (2) it enables 
wireless carriers to offload data and voice traffic when
new converged mobile broadband services are
being introduced into the home.
Infrastructure: Infrastructure & Networking Products
Broadcoms Infrastructure business experienced
a year of tremendous growth with record revenue 
 up 50% year over year. This growth resonated
throughout the portfolio, driven by a number of market trends and opportunities, including the industrys
transition to 10 Gigabit Ethernet (GbE). The 10GbE
market is positioned for growth and Broadcom continues to lead the way in enabling mass deployment
of 10GbE in the data center. To address this market
opportunity, Broadcom made a number of significant
industry-first announcements in 2010, including the
worlds fastest single chip Ethernet switch, as well as
the industrys smallest and most powerful multi-port
10GbE controllers. 
Both the 2009 Dune and 2010 Teknovus acquisitions
have exceeded expectations with the introduction of
a new product series from each within ten months
post acquisition, demonstrating our ability to successfully integrate acquired technologies without slowing
down innovation. As a result of the Dune acquisition,
Broadcom delivered the worlds first 100 Terabytesper-second Ethernet switch, enabling a new generation
of ultra high bandwidth networking solutions. As a 
result of the Teknovus integration, Broadcom recently
began sampling the industrys first 10 Gigabit passive
optical network (PON) SoC solution designed specifically
to address the high-speed connectivity requirements
of converging triple-play services together with 3G/4G
mobile backhaul and business services.
In 2010, energy efficiency continued to gain traction in
computing and networking circles, with Broadcom
paving the way to a greener data center. Networking
equipment, like any other appliance, is subject to rising energy prices and related increases in operational
expenditures. Through its AutogrEEEn technology
initiative, Broadcom helps alleviate the cost burden
for end-customers by enabling network devices that
are much more energy efficient. Broadcom now offers
the industrys broadest portfolio of end-to-end silicon
and software solutions, enabling faster deployment of
energy efficient networks.
Data center build-outs for enterprise and cloud
computing providers represent yet another long-term
secular growth opportunity for Broadcom. Service
provider build-outs continued to be very strong, and
Broadcom realized significant growth in this segment
in 2010 with switch and physical layer (PHY) products
deployed in large scale 3G/4G wireless and PON
infrastructure build-outs, particularly in Asia.
Hand: Mobile & Wireless Products
In 2010, Broadcom fortified its strong positions in
mobile and wireless technologies, including its cellular,
multimedia processing, GPS, Bluetooth
and Wi-Fi
products, driven by the continued adoption of leading
smartphone features in handset and tablet products.
Broadcom wireless connectivity combo chips, which
integrate key technologies such as Bluetooth, Wi-Fi,
GPS and FM on a single-chip, remain the most popular
way to integrate these features into mobile devices.
2010 also saw significant growth in Broadcoms cellular
business as the company continued to gain momentum
with leading handset makers. Looking ahead to further
secure our position in the evolving mobile market, we
made key acquisitions in 2010, adding NFC and 4G
baseband technologies to our portfolio.
Broadcom is addressing the growing consumer demand
for over-the-top video on consumer electronics 
devices by connecting media platforms such as TVs,
Blu-ray Disc
players and set-top box products to the
Internet using Wi-Fi. Additionally, Bluetooth has become
increasingly more attractive as a wireless connectivity
technology in consumer electronics, enabling a variety
of peripherials such as non-line-of-sight 3D glasses,
QWERTY remote controls and gestural controls 
for gaming.
In 2010, Broadcom continued its strong support of
emerging wireless standards, with the companys
products being among the first to obtain certification
and selection for the Wi-Fi CERTIFIED Wi-Fi Direct
test suite. With the industrys broadest range of
Wi-Fi chips designed into myriad product categories
including mobile phones, tablets, notebooks/netbooks,
Blu-ray players, printers, televisions, set-top boxes,
games, and portable media players, Broadcom can
enable an entire portfolio of devices that connect 
using Wi-Fi Direct without the need for a router or
access point. 
Broadcom also added support for Bluetooth
Version 4.0 across its core Bluetooth SoC solutions.
Broadcom was one of the first companies to achieve
Bluetooth 4.0 controller and host stack certification
solidifying its continued leadership in Bluetooth while
helping to bolster its growth and adoption. Broadcoms
support of Bluetooth low energy (BLE) opens up new
segments for this wireless technology including fitness,
sports, medical devices, security, and home entertainment that are powered by small, coin-cell batteries.
The year 2010 also presented a significant increase in
the adoption of GPS and location-based technologies,
with Broadcoms single-chip and combo GPS solutions
shipping with many of the leading handset manufacturers worldwide. Broadcom also enhanced its location
software and data services offerings to better support
ubiquitous coverage in deep urban and indoor locations, enabling growing trends in social networking,
couponing and mobile advertising applications.
In conclusion, I believe we can mark 2010 as the year
when Broadcom established its place among the
ranks of the worlds blue chip companies. We continued to demonstrate financial and operational discipline
that enabled revenue growth and profit, strong R&D
funding and solid shareholder return, and we continued
to produce industry-leading products that enabled
our customers to introduce some of the hottest, most
sought after devices around the world. Beyond that,
we took a leadership role through our Broadcom
Foundation in supporting science, technology, 
engineering and math (STEM) education programs
worldwide. Last year, Broadcom Foundation announced a multi-year commitment to support U.S.
middle school science fair competitions, those timehonored gatherings that serve as incubators for the
next generation of engineers and scientists. 
We have reached these milestones in the companys
history on the cusp of our 20th anniversary, which
falls on August 16th of this year. Our successes in
2010, and over the last two decades, are the result of
a culture of innovation and a delight in doing the 
impossible. Over 20 years, we have built Broadcom
on the foundation of best-in-class employees who 
deliver superior engineering execution on behalf of
our customers.
Indeed, at the heart of Broadcom, are our employees
who are the key ingredient to our success. We thank
them for their spirit, amazing contributions, hard
work and often personal sacrifices in our drive to make
Broadcom the worlds leader in communications
semiconductor innovation. Additionally, we thank
our customers who create the great devices that are
changing the way the world communicates and
connects, our suppliers who play a central role in our
ability to execute at such high levels, and our shareholders who believe in our vision for a connected world.
