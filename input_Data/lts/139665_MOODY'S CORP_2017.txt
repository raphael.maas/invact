2
MOODY�S  
2017 ANNUAL REPORT
Global economic expansion was strong and stable in 2017 
with all major economies experiencing growth. The U.S. 
economy remained on solid footing, featuring high levels 
of consumer and business confidence as well as sustained 
employment gains. In Europe, a broad-based recovery gained 
momentum as electoral and related geopolitical concerns 
over more exits from the European Union abated. Emerging 
market countries also exhibited improved prospects. 
Capital markets activity proved to be highly resilient to 
geopolitical, macroeconomic and other risks that could 
have restrained debt issuance during the year. Funding and 
refinancing conditions were favorable, reflecting healthy 
economic growth, still-accommodative monetary policies 
and declining corporate default rates, all of which combined 
to support low borrowing costs and readily available capital.
Synchronous global growth and healthy capital markets 
in 2017 provided the backdrop for Moody�s highest rate of 
revenue growth since 2012. Revenue of $4.2 billion in 2017 
increased 17% from 2016. Moody�s Investors Service (MIS) 
and Moody�s Analytics (MA) both reported record revenues, 
with seven out of eight lines of business showing growth. 
MIS revenue rose 17% to $2.8 billion. 2017 saw broad gains 
in corporate and financial sector bond and loan issuance, 
including record volumes of U.S. and European bank loans and 
Asian bonds, while important sectors of structured finance, 
such as collateralized loan obligations (CLOs), experienced 
robust activity. MIS also benefited from a record number of 
new rating mandates in 2017, reflecting the ongoing trend of 
disintermediation of fixed-income markets in both developed 
and emerging economies. 
MA revenue increased 16% to $1.4 billion, driven by organic 
growth in all three lines of business as well as the addition of 
Bureau van Dijk, which Moody�s acquired on August 10, 2017. 
Research, Data and Analytics achieved very strong 
organic growth reflecting a combination of new sales, product 
upgrades and high customer retention rates. Enterprise Risk 
Solutions grew revenue at a solid, albeit slower pace than in 
recent years as the business adjusts to a shift in demand and 
customers� technology preferences while emphasizing more 
scalable product sales. Finally, Professional Services achieved 
positive results as both Moody�s Analytics Knowledge  
Services and Financial Services Training & Certifications 
returned to growth.  
Other 2017 reported financial measures for Moody�s 
Corporation include:  
�  Operating income of $1,809 million, up from $639  
    million in 2016
1
�  Operating margin of 43.0%, up from 17.7%
1
�  Adjusted operating income of $1,990 million, up  
    from $1,641 million
2
�  Adjusted operating margin of 47.3%, up 180 basis  
    points from 45.5%
2
�  Diluted EPS of $5.15, up from $1.36
1
�  Adjusted diluted EPS of $6.07, up 23% from $4.94
2
Moody�
s strong results helped our stock outperform the 
overall market. 2017�s total return of 59% compared  
to 22% for the S&P 500 and represented the strongest  
annual performance for Moody�s stock since becoming a 
public company.
I am confident that our business today is resilient. Moody�s 
remains an essential component of the global capital markets, 
providing credit ratings, research, tools and analysis that 
contribute to transparent and integrated financial markets. 
While defending and enhancing our core business is our 
primary objective, we have the resources, capabilities and 
brand to achieve broader success. 
1 
Comparisons to 2016 include the impact in that year of an $863.8 million settlement charge - $700.7 million net of tax - related to an agreement with the U.S.  
  Department of Justice and the attorneys general of 21 U.S. states and the District of Columbia.
2 
Refer to �Non-GAAP Financial Measures� on page 55 in Item 7 of Moody�s 2017 Form 10-K for a discussion of the Company�s non-GAAP financial measures.
Raymond W. McDaniel, Jr.
Letter from the 
President & Chief Executive Officer
3
MOODY�S  
2017 ANNUAL REPORT
As previously mentioned, in early August Moody�s completed 
the largest acquisition in our history  
�the �3 billion 
acquisition of Bureau van Dijk. Bureau van Dijk aggregates, 
standardizes and distributes one of the world�s most extensive 
private company datasets. We are very pleased to be able 
to add this successful and highly complementary business, 
which builds on Moody�s role as a global provider of credit risk 
measures and analytical insight. 
While Bureau van Dijk was the headline story on acquisitions, 
we also made smaller investments in companies and 
technologies that have the potential to help us strengthen 
existing offerings and enter new areas of opportunity. In 
2017, Moody�s made minority investments in CompStak and 
Rockport VAL, both in the commercial real estate analytics 
space, and Security-Scorecard, a cybersecurity ratings firm. 
We also acquired the structured finance data and analytics 
business of SCDM, a leading provider of analytical tools for 
participants in securitization markets.  
As part of expanding Moody�s global footprint, we are 
positioning ourselves to play a larger role in China 
�one of 
the largest debt markets in the world. By the end of 2017, 
MIS rated over 250 cross-border Chinese entities, up 15% 
from 2016 and more than double the number rated just 
five years ago. To help attract international capital, China 
recently announced that foreign rating agencies will be 
allowed to assess the credit risks of the nation�s sizeable 
domestic corporate debt market. Amid these indications that 
the Chinese market will continue to open, MIS completed a 
realignment and expansion of its local affiliate, CCXI, in 2017 
and is increasing its participation in China more broadly. 
Environmental, social and governance (ESG) initiatives 
represent another area of opportunity for MIS, where its 
practice continues to gain momentum. MIS�s initiative to 
incorporate ESG risks into its credit analysis is growing. To 
further its reputation as a thought leader in the ESG space, 
MIS is increasing its outreach and research, all supported  
by a dedicated ESG analytical team and a green bond 
assessment team.
Fostering innovation is crucial to success in a market 
environment that is increasingly digital and where the rate 
of technology-enabled change is accelerating. The Emerging 
Business Unit in MA and the new Analytic and Technology 
Solutions Group in MIS continue to study developments in 
applied technologies that can help Moody�s better reach 
customers, create new products and improve the Company�s 
internal processes, including across multiple compliance and 
control functions. 
In January 2018, Moody�s announced that Linda Huber, 
Executive Vice President and Chief Financial Officer, is leaving 
the Company to pursue other opportunities. Linda has given 
Moody�s more than 12 years of outstanding service. She has 
been an exceptional steward of our financial performance, 
and I wish her every success in the next phase of her career. 
I would also like to thank two of Moody�s board members, 
Darrell Duffie and Ewald Kist, who recently announced they 
will not be standing for re-election to Moody�s board of 
directors after 9 and 13 years of service, respectively. Their 
experiences, insights and contributions will continue to serve 
as positive influences on Moody�s management team and our 
business for years to come.    
At Moody�s, we are proud of our open and diverse workplace, 
which helps attract and retain top talent and fosters analytical 
rigor, intellectual curiosity and collegiality. In 2017, Moody�s 
was again recognized as one of the best and most inclusive 
places to work by multiple institutions, including Working 
Mother and the Human Rights Campaign via its Corporate 
Equality Index. I am confident that the dedication and 
capabilities of our employees will enable Moody�s to continue 
to perform at a high level as we execute on our strategy to 
create ever broader, sustainable growth in our core business 
while expanding into areas that strengthen Moody�s position 
as the world�s most respected authority serving risk-sensitive 
financial markets. 
While challenges will inevitably persist, I am proud of all we 
achieved in 2017 and optimistic about the future. I thank our 
employees for their hard work and commitment in making 
2017 a particularly successful year. I also welcome our new 
colleagues from Bureau van Dijk to Moody�s. We look forward 
to continued success as we maintain our focus on achieving 
the highest standards while growing, innovating and creating 
value for all our stakeholders.
Raymond W. McDaniel, Jr.
President & Chief Executive Office