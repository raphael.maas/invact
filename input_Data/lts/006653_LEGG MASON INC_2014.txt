DEAR CLIENTS AND FELLOW SHAREHOLDERS:
I write you following the completion of my first year as CEO, very pleased
to share the results of a year of great progress for our Legg Mason.
During this past year, I have been so proud to see our organization move
forward, together, in service of our clients and shareholders, as we begin
returning Legg Mason to a position of industry leadership. This new fiscal
year, it is our commitment to build further on the clear momentum that
we now enjoy.
Our Fiscal 2014 was a year of difficult but important decisions that
involved streamlining our Company where appropriate, while adding
resources and capabilities opportunistically, all with a clear focus on
Building a Better Legg Mason and positioning the Company for
growth: our number one priority for Fiscal 2015.
As we look ahead, fully appreciating the sacred trust our clients place in
us, we feel an even greater sense of purpose and determination to help
solve the financial challenges we all face: achieving financial security and
growth in what remains a turbulent global economy.
We strongly believe that we have the talent and resources, properly
aligned and focused, to help our clients navigate these challenges.
We remain confident that our business model, combining highlyperforming,
independent investment managers and global retail distribution,
is not only differentiated in the marketplace, but distinctive. This model,
successfully executed, provides the foundation to deliver for clients and,
in turn, our shareholders, in two essential ways: by achieving compelling
investment returns in appropriate products and strategies that are
effectively distributed on a global basis. We are quite pleased to have
made progress on both investment performance and distribution last year,
and we expect to build upon those improvements in the year ahead.
We see the Legg Mason of the future with fewer but larger Affiliates,
focused on delivering compelling investment results for investors, more
comprehensive investment capabilities that reflect evolving client demand
and needs, and an increasingly productive operating team to support
both our Affiliates and global distribution platform. We believe that
strong execution of this vision will improve our organic AUM growth rate;
increase revenue generation; and further diversify our business by client,
asset class and geography to yield increasingly better results for both our
clients and fellow shareholders this coming year and beyond.
The Year in Review: Change and Action
Our work this past year was one of deliberate action, centered on three
fundamental imperatives: improving operating productivity, leveraging
our global distribution platform and strengthening our independent, multi-
Affiliate model to be more competitive and effective.
Leadership Changes
We strengthened our Board of Directors with the
addition of our new non-executive Chairman, Dennis Kass, and Directors
John Myers, John Murphy and John Davidson (elected in May), all of
whom add diverse and broad business and asset management expertise.

Im grateful for the insight and counsel of these new Directors
and look forward to our collaboration as we progress.
Additionally, I am very pleased that Tom Hoops joined Legg
Mason to lead the critical business and product development
effort at the senior executive level. Having most recently
managed the Affiliated Managers Division within the
asset management group at Wells Fargo, Tom appreciates
our diverse, multi-Affiliate model and, as such, is quite
comfortable and effective in bringing together our Affiliates,
the executive team and the broader organization around
common objectives.
Improving Operating ProductivityWe introduced
a LEAN/Six Sigma efficiency initiative to the Company
that resulted in both short- and long-term benefits to
the organization. Several hundred employees worldwide
were trained to lead an evolution of our corporate DNA
to embrace the practice of Kaizen, or continuous
improvement. The Legg Mason of the future is committed
to operating the firm with a high degree of efficiency and
effectiveness, which is more than just a matter of fiscal
prudence, but also a competitive necessity. This important
productivity initiative has already yielded a meaningful
reduction in our spend, which we are redeploying to our
critical growth engine, Legg Mason Global Distribution.
Leveraging Our Global Distribution PlatformIt is clear
that we now operate in a more complex market environment
with low real economic growth and interest rates,
particularly in mature economies. This sober reality means
that, to accomplish their long-term objectives, investors
need to consider looking across asset classes and beyond
the borders of regional geographies in new ways when
investing. This is the real new reality that our investor
clients face, and the implications for asset managers
are significant. Size, reach and diversity of products and
strategies are essential to meet these evolving client needs;
and managers that offer a broad, truly global perspective and
investment strategy palette will succeed disproportionately
in the marketplace.
We believe that Legg Mason is competitively positioned to
capture just such opportunity.
While still in modest long-term outflow, we made
significant progress this past year in distribution across
the Company, with net long-term asset flows improving
by approximately $28 billion. Strong performance in both
our institutional and retail channels was highlighted by
our global retail platform achieving record gross sales,
increasing 15% over the prior year.
Despite that improvement, we believe that we can
dramatically grow our sales and improve net flows over the
next few years, but to do so will require thinking and acting
differently, embracing change as our clients and the markets
change. Specifically, to better serve a larger and broader set
of global clients, our sales and client service teams must
have even deeper product knowledge, listen and engage with
our clients more effectively, and be better prepared to offer
innovative products and solutions that meet their needs.
In addition to working harder and being opportunistic,
winning firms will also need to work smarter, with costs
for all managers rising, as more is expected of us by clients,
distribution partners and regulators than ever before.
In working smarter, we are investing thoughtfully and
strategically to innovate and harness the power of
technology, enabling us to better understand and serve our
clients more effectively and efficiently.
Utilizing technology to collect and analyze client
segmentation and geographic dispersion data, we created

a differentiated U.S. distribution model. As part of this
work, we segmented the business needs of nearly 200,000
advisors in the U.S. retail market, including both those we
now serve and those whom weve identified as having
significant business potential. Further, we are applying the
lessons weve learned in the meaningful success of our
innovative cross channel sales model, launched in 2011.
Given that success, our conviction is high that multiple
coverage models can succeed and lift the productivity of all
sales teams to create a more powerful distribution system.
Weve introduced a new solutions-oriented, capital marketsbased
selling approach and a modified sales compensation
plan to support it. Going forward, we will use metrics to
reinforce collaboration throughout the organization and
strengthen accountability for results.
Were very excited about the opportunity to accelerate our
success in our global distribution platform with the launch in
April of these new sales initiatives in the United States, and
we will apply a similar, though regionally adapted, approach
internationally in the year ahead.
Strengthening Our Independent Multi-Affiliate Model
While firmly committed to our independent, multi-Affiliate
model, we recognize that there is room for meaningful
improvement in how we execute this model. The economic
structure, the relationship governance and mix of our
Affiliate portfolio must be modified and enhanced.
With that in mind, we successfully completed two
Management Equity Plans with Permal and ClearBridge
during the year to create greater alignment in building longterm
franchise value at the Affiliate level.
These plans are designed to improve the Affiliates ability to
recruit and retain talent, support succession planning and
yield incremental margin for Legg Mason shareholders, as
the Affiliate grows. We expect to complete similar plans
with each of our major Affiliates.
At the same time, we substantially modified the mix of our
Affiliate portfolio by rationalizing non-strategic, sub-scale
and underperforming managers and adding new investment
capabilities, through a series of difficult but important actions.
To address small or non-core Affiliates in a thoughtful
manner, we reached an agreement to sell Private Capital
Management back to its management team in August and
made the decision to wind down operations at our Londonbased
emerging market Affiliate, Esemplia, last fall.
Additionally, we combined Legg Mason Capital Management
with ClearBridge Investments, rebranding LMCM products
under the ClearBridge name. We will now leverage their
shared history and success, focused on fundamental
investing, under a single brand.
In March, we were very pleased to announce the acquisition
of QS Investors, which was completed in May, bringing to
Legg Mason a leading manager of customized solutions and
global quantitative equities. We are very fortunate to add

this highly talented team with a distinctive and differentiated investment process and a scalable, proprietary investment platform capable of accommodating significant growth.
In tandem with the acquisition of QS, we have begun the process of combining, over time, the investment capabilities and operations of QS with those of Batterymarch Financial, our existing quantitative equity manager, and Legg Mason Global Asset Allocation, our multi-manager platform. Were confident that the combined platform, bringing together the best of these three firms, will powerfully position Legg Mason to meet the rapidly growing client demand for global solutions, liquid alternatives and smart beta categories.
Each of these actions involved difficult but necessary decisions that have helped streamline our Affiliate lineup and clarify our brand in the marketplace. As a result, we now have six larger, highly focused Affiliates in Brandywine Global, ClearBridge Investments, Permal, QS Investors, Royce & Associates and Western Asset Management, each with a distinct investment expertise and brand.
And, as it relates to expanding and refining our investment capabilities and Affiliate mix, were not done.
We do expect to continue adding investment capability through lift-outs and bolt-ins and will likely add modestly to our Affiliate lineup, but only if and as we find a quality firm with a leverageable investment platform, again, with a focus on having fewer and larger Affiliates.
As we expand our investment capability, we seek to add investment professionals and new Affiliate relationships with people and firms that are entrepreneurial and innovative, that deliver compelling investment results for investors, whose culture is consistent with our clients first commitment and who seek the benefits of a true partnership with a large, global firm.
While these attributes represent a high bar for admission, we believe that Legg Mason is a wonderful home for great investors. Our current Affiliates share these critical characteristics and are aligned with us in strong partnership to serve our clients, a mutual commitment that will drive our collective success.
Were proud of the significant and successful work on these three critical imperatives this past year and are confident that we are now better positioned in the marketplace, realizing that the heavy lifting of growth has just begun.
Year in Review: Business and Financials
We are certainly pleased with the performance of our stock over the past year, which we believe reflects significant

progress on the three fundamental imperatives and improvement on key business metrics.
Most importantly, investment performance remained strong overall, with three-quarters or more of our strategy AUM exceeding benchmarks for all time periods as of year-end.
Our results include an improvement of $28 billion in long-term net flows. This improvement, led by fixed income inflows and significantly reduced equity outflows, resulted
in a modest total outflow for the fiscal year.
We achieved a 5% increase in revenues and a meaningful improvement in our operating margin, as adjusted, year
over year.
For Fiscal Year 2014, Legg Mason reported net income
of $285 million, or $2.33 per diluted share, and adjusted income of $418 million, or $3.41 per diluted share.
Assets under management at fiscal year-end were
$702 billion, up 6% from March 2013.
We refinanced our debt, deleveraged our balance sheet and locked in long-term debt capital at current historically low rates.
Finally, over the past year we returned capital to shareholders by repurchasing 9.7 million shares and increasing our dividend by 23%.
We are very pleased that over the past three years the combination of our share buyback and dividend has resulted in Legg Mason delivering one of the highest total shareholder payout rates in the industry.
The Year Ahead
As an asset management firm, we generate a significant amount of cash that we can return to shareholders and invest strategically in future growth, which is the foundation of Building a Better Legg Mason.
In the year ahead, we remain committed to continuing to
fill investment capability gaps, most importantly in non-
U.S. equities, a goal that remains very high on my personal list of priorities.
We will also continue to look thoughtfully and opportunistically at adding alternative investment capabilities in such asset classes as real estate, energy and private equity.
We are excited about the potential for the solutions capability we now have with QS Investors to deliver the outcome-oriented products that our clients want, both through QSs own expertise and through partnerships with our other investment Affiliates.
We intend to complete more Management Equity Plans in the coming year, as doing so aligns the Company more deeply with our Affiliates, provides a mechanism for long-term growth and sustainability and, very importantly, affords Legg Mason the opportunity to achieve incremental margin as each Affiliate grows.
We fully expect continued improvement in distribution in both the institutional and retail channels and are investing thoughtfully and meaningfully to support that growth.
And finally, as we run our business, we will continue to pursue managing this Company with the highest degree of efficiency and effectiveness possibleKaizen!
While we seek to deliver on these operating goals, well continue to appropriately balance the allocation of our capital between investing in the business for the long term and returning it to shareholders.
I am extremely proud of the dedication and commitment
of our nearly 3,000 professionals around the world, as
they continue to work tirelessly to serve both our clients
and shareholders.
We recognize the many challenges faced by investors today, challenges that may, at times, seem almost overwhelming. We have the resources, the capability and the opportunity to play a meaningful role with investors to solve many of these challenges. This opportunity is also a responsibility to lead, to bring more to the clients we now serve and to reach more investors around the world. In fulfilling this responsibility, we will return our Legg Mason to organic growth, to the benefit of the clients we serve, our shareholders and our fellow colleagues. We eagerly and gratefully accept this important responsibility.
We appreciate your continued trust and confidence.
Joseph A. Sullivan
President and Chief Executive Officer