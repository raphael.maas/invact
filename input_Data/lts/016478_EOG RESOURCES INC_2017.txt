LETTER TO SHAREHOLDERS
Chairman of the Board and Chief Executive Officer
EOG is a rate-of-return incentivized company and has been since its founding. Our return-focused culture prompted a rigorous upgrade to our investment return standard at the bottom of the commodity price cycle in 2016: In order to be considered for capital dollars, new wells are required to earn a minimum 30 percent rate of returnA at $40 oil and $250 natural gas.
In our net Income, cash flow and return on capital employed (ROCE) and increased U.S. oil production 20 percent while reducing debt, paying our dividend and generating free cash flow. These results are particularly remarkable given that oil averaged a modest $50 a barrel last year - a testament to the capital efficiency
of our premium drilling inventory. An average premium well's first year gross oil production is twice as much for about half the finding cost of a non-premium well, which translates to a roughly fivefold increase in returns.R
What is equally important is how we consistently replace our inventory. We continue to improve both the size and quality by organically adding better locations substantially faster than our drilling pace. We added 2,000 net premium locations in 2017, which is almost four times the number of net wells completed.
free cash flow. Moreover, we can deliver this first-class performance at oil prices as low as $50. That is the power of premium.
EOG has a history of earning peer-leading ROCE, averaging 13 percent over
the past 20 years.' Our return-focus and diverse portfolio
of assets, supported by a bottom- up, flat, decentralized organization. continues to drive differentiated ROCE performance in the Exploration and Production (E&P) space.
Going forward, our vision is to be the E&P company earning ROCE that is not only the best among our peers, but also competitive with the best companies outside our industry. How do we do that? Capital discipline and a premium capital allocation standard that will sustain ROCE throughout the commodity price cycle. We believe this is the best way to create sustainable long- term shareholder value.
EOG Is a high-return organic growtl company poised to be a leader in both returns and growth for decades to come.
This "premium well" capital allocation standard has permanently changed the game. We did not relax the standard in the face of improving prices in  2017. All seven of EOG's operating teams across every major North American basin and our international teams know that to receive funding, investments need to exceed the premium return hurdle rate.
We are proud to report that our operational teams delivered in 2017. Our drilling program earned an average rate of return's of 63% calculated on the premium $40 oil price deck and 92% using 2017 realized prices.
More importantly, these rates of return are reflected In our bottom- line financial performance and In our operational performance. In 2017, we realized significant improvement
New drilling inventory was sourced from both existing and emerging plays - we added the First Bone Spring Sand target in the Delaware Basin and introduced the Woodford Oil Window in the Eastern Anadarko Basin.
Our premium inventory now totals a massive 8,000 net locations and 7.3 billion barrels of oil equivalentc in geologic sweet spots across six  areas, the Delaware Basin, Eagle Ford, Bakken, Powder River Basin, DJ Basin and Anadarko Basin. The diversity of our horizontal oil assets is unmatched in the industry. This unique competitive advantage provides the scale and flexibility critical to sustain both healthy growth and high returns.
Looking ahead to 2018. we expect to grow oil 18 percent. earn double digit ROCE, pay the dividend and generate
a.
William R. Thomas
Chairman and Chief Executive Officer February 27, 2018


