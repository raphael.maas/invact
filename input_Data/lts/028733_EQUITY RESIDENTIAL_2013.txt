Dear Fellow Shareholders,
2013 was an extraordinary year for Equity Residential:
 We completed a ten-year strategic plan to totally transform our portfolio of assets by
closing a $9 billion acquisition and seamlessly integrating over 20,000 high-quality, welllocated
apartment units across our core markets.
 We met our pricing expectations in the sale of more than $4 billion of non-core assets and
completed our exit from several low-barrier, commodity markets across the country. These
sales produced an economic gain of over $900 million and an unlevered internal rate of
return of 10%.
 We delivered our third consecutive year of above-trend operating results by producing
4.5% revenue growth from our same store portfolio of more than 80,000 apartment units.
 We celebrated our 20th anniversary as a public company and are pleased to have delivered
an annual total shareholder return of 13% over those twenty years.
Many of you have been shareholders in Equity Residential from the start and you know our
growth story. When we went public in August of 1993, we owned 21,000 garden-style
apartment units across the Midwest, South and Texas. At the time we were the largest public
apartment company with an enterprise value of approximately $800 million.
Over the next seven years we rapidly grew through mergers and acquisitions to more than
220,000 apartment units across more than 50 markets. However, as we entered the new
millennium, we recognized that the country and our economy were changing and that our
portfolio of surfaced-parked garden apartments would not likely deliver optimum results going
forward. We knew that higher-density urban markets with strong demand and limited supply
would produce the best long-term results. We also knew that it would not be easy to make such
a significant change. Nevertheless, we embarked on a major portfolio transformation during
which we have sold more than $19 billion of primarily garden-style apartment assets in lowbarrier,
commodity markets like Phoenix, Dallas and Atlanta and acquired more than $22 billion
of primarily mid- and high-rise assets in large, high-density, high-barrier to entry gateway cities
such as Boston, New York, San Francisco and Seattle.
Today, Equity Residential owns and operates the highest quality, best located portfolio of
apartment assets in the public apartment space. Our focus on high-quality assets in top growth
markets is reflected in the fact that we have one of the highest average monthly rents in the
public apartment space. Our enterprise value has grown to more than $30 billion and we remain
the largest public apartment company.
Throughout this journey, we have created significant value in our portfolio because assets in our
core markets have increased in value far more than assets in the markets which we have exited.
However, along the way, we also experienced earnings and FFO (Funds from Operations)
dilution as we sold assets at higher initial yields than that realized on our new acquisitions.
Though this activity was net asset value (NAV) accretive, it was also FFO dilutive; but necessary
as we moved capital into the markets we believe will create the most long-term value. With our
portfolio transformation complete, our annual earnings growth should return to the cycle we
enjoyed previously when FFO consistently grew several points higher than our mid-single-digit
same store net operating income (NOI) growth. Our dividend, now pegged to grow in line with
our Normalized FFO, should combine with our earnings growth to consistently deliver high
single- and to low double-digit annual returns to our shareholders over the long-term.
As we have shared with you over the years, we have focused our investment activity on our core
markets of Boston, New York, Washington, D.C., Southern California, San Francisco and Seattle
because they are the gateway cities where our countrys knowledge-based economy will
continue to grow and create high-paying jobs. These markets feature incredibly attractive

lifestyles for our residents as well as the nations highest average incomes and rents. They also
feature a very favorable renter profile and a high cost of home ownership

We also target these markets because the jobs they create and the lifestyles they offer attract a
broad spectrum of renters from young people getting their first job and striking out on their
own, to empty-nesters who are downsizing and attracted by all that centrally located urban
living has to offer. Our core markets feature diverse economies with good job growth across
many industry sectors

Our core markets also feature renter households with healthy incomes that are far in excess of
the national average. This creates a broad pool of potential renters for our assets, while lifestyle
choices and the high cost of home ownership will keep them renting with us longer.

As we have made investments in our core markets, whether through acquisition or
development, we have focused on properties that are located in the urban core. From these
locations, our residents can walk or take public transportation to their jobs, restaurants, night
life, recreation, education, culture and health care and the many other places they value in their
lives. Gone are the days of good freeway visibility as an important property feature! Today, we
are focused on walk scores, an interesting new metric that rates a location based on its
proximity to all of the goods, services and attractions that our residents value. The average walk
score for Equity Residentials properties is 69 compared to 58 for our competitors in the public
apartment space. We believe that this matters because of the continued urbanization of the
country and the way more and more people are choosing to live.
As a society, we are moving away from the notion that ownership is important and toward a
more rental society. And this is not just in our housing, but includes many other aspects of our
lives. For instance, in many cities owning a car is an expensive hassle and not necessary when
living in a building with a high walk score. For those rare occasions when one does need a car
our residents use car-sharing services like Zipcar that have been created to fill this need. As an
amenity at many of our buildings, we are pleased to have a national partnership with Zipcar,
which has placed many cars in our garages for easy access by our residents. This modern
amenity, along with so much else that we do to enhance our residents living experience in our
well-located urban assets, will encourage them to stay longer with us and say good things
about their experience to their friends and colleagues.
While our apartment properties appeal to a broad spectrum, there is little doubt that a great
amount of demand for rental apartments is being driven by the generation referred to as Echo
Boomers, Millennials or Gen Y. More than 4 million members of this generation are turning
21 each year, creating huge demand for rental housing by entering the workforce and creating
millions of new households. The data tells us that they are travelling light and embracing the
flexibility in their lives provided by rental housing, because while they may start their careers in
Boston, Seattle or DC, they know that they could soon be packing up for a move to San
Francisco, New York or Los Angeles. And they dont want to live in the suburbs but in the urban
core near their friends, their jobs and in the center of all the action these great cities offer. As a
result, our core markets continue to experience tremendous growth in this key demographic.

We see our residents staying with us longer as
they delay the life choices that most commonly
drive the decision to buy a single family home.
The data quite clearly shows that the members
of the Millennial Generation are marrying later
and having children later in life than recent
generations. And that data also says that the
primary reason people buy a single family home
is a lifestyle choice, one primarily made when
they are having children and looking for a place
to raise their families. So it does not come as a
surprise that the most likely purchaser of a single
family home is a couple with one or more
children. We see little threat to our performance
as a result of increases in single family home
sales activity because, across our portfolio, only about 8% of our units are occupied by residents
who fit this profile. More importantly, nearly 50% of our units are occupied by a single resident,
a household type that has an extremely low propensity to purchase a home.

We also like our markets because they have historically had high barriers to entry when it comes
to the construction of new apartment supply. A lack of available land for development creates a
steep price for the few sites that can be found. That and a long entitlement process work to
keep new supply in check in these markets, which has been a great benefit to existing landlords,
like us, as new supply has not kept up with incremental demand.
Following several years with virtually no new supply as very few projects were started during
the last economic downturn, we will soon see an abundance of new deliveries into some of our
core markets. In Washington, D.C. for example, more than 19,000 new apartment units will be
delivered in 2014. This surge of new supply is a result of a backlog of projects that were put on
hold during the last economic downturn being green-lighted in a market that has always been
one of the best rental markets in the country. This new supply will impact our portfolio in
Washington, D.C., which represents approximately 18.6% of our NOI in 2014. This year we expect
our same store revenue from Washington, D.C. to decrease approximately 1%. And, while new
deliveries are expected to drop to approximately 10,000 apartment units in DC next year, we
expect our 2015 same store revenue from this market to be negative again as the impact of this
new supply works its way through our rent roll.
Nationwide, we believe that this new supply will peak in 2014 and we will see a decline of more
than 40% in the number of new units being delivered into our markets in 2015. We also project

that incremental demand for these new units being driven by strong household formations will
exceed this new supply and greatly benefit a portfolio like ours that is currently 95% occupied

At Equity Residential, we augment our acquisition activity with our development capability to
build new assets in our core markets. Unlike many developers who build assets to sell soon after
stabilization for a quick profit, we seek to build assets that will enhance our portfolio,
consistently create value and deliver strong earnings growth for many years. We currently have
more than 4,000 apartment units under construction at a total cost of approximately $1.7 billion
in fantastic locations in New York, San Francisco, Seattle and Southern California. We also own
vacant land sites in our core markets for future development projects which have potential for
more than 4,300 additional apartment units at a cost of $1.9 billion.
While we pride ourselves on being opportunistic and believe we have a strong record as a
capital allocator, we are not currently seeing much opportunity to add to our portfolio through
acquisitions. There is great demand for high-quality, well-located assets in our core markets and
property values remain at record highs. We have given guidance for acquisitions and
dispositions of $500 million each in 2014, but our disposition activity will be guided by what
opportunities we see to acquire assets in our core markets.
We will be opportunistic this year in reinvesting in our own portfolio through our rehab
program by spending approximately $45 million on 5,000 kitchen and bath renovations. This
program provides mid-teen incremental returns, attracts a higher income resident and either
maintains or increases the overall quality of the property being rehabbed.
We are also investing in our assets to make them more sustainable and do our part as good
members of our communities and good stewards of the environment. To us, sustainability
requires an all-inclusive approach, addressing not only specific environmental impacts such as
water conservation, waste reduction and energy efficiency but also the environmental and
social impact to our community, including the relationships we have with our residents, our
employees and our shareholders. This year we are participating in the Global Real Estate
Sustainability Benchmark (GRESB), a survey that is becoming the standard for our industry in
measuring these key issues. Details of our sustainability activity can be found in our Corporate
Social Responsibility and Sustainability report, which is available on our website,
www.equityapartments.com/corporate.
Having a terrific portfolio in the right markets is only part of the equation. Producing best-inclass
performance is only possible because of the team we have and the organization and
platform we have built.
It all starts with our people  the 3,600 employees of Equity Residential who are committed to
serving their customers and creating value for our shareholders on a daily basis. Our
organizational focus is on collaboration across the enterprise. It is the bedrock of the Equity
culture. We work as one team to accomplish this goal by valuing everyones input and putting
egos aside to make decisions that will create the best long-term value for our shareholders.

Never was that more clear than last year when we integrated over 20,000 apartment units and
welcomed more than 700 highly professional and motivated new team members into the Equity
family. No other apartment company has bought and assimilated more properties than we
have. Its hard work and takes the efforts of everyone. And we accomplished all of this while
simultaneously selling nearly 30,000 apartment units and producing same store revenue growth
of 4.5%  right on our target. We could not be more proud of what our colleagues accomplished
in 2013. We cannot thank them enough and do so again now.
Great properties. Great people. Add a great platform and you have the recipe of our success. We
continue to invest our time and money improving an operating platform that is already state of
the art. The daily advances in technology are amazing and we continue to harness them in order
to deliver the customer experience in the fashion that our residents prefer and to drive value for
our shareholders. In addition to our systems that price our rents daily and track our expenses,
we are utilizing technology to connect our residents to their property, their fellow residents and
the local community. Our prime demographic of young people has grown up using technology
for much of the daily interaction in their lives and we continue to shape the resident experience
to meet this demand.
We were very pleased in 2013 to have appointed David Santee as our Chief Operating Officer.
David has had a 30-year career in property management right out of college beginning as a
leasing manager. David joined Equity Residential in 1994 managing properties for us in the
Southeast and has been a rising star ever since. We are delighted to have David as part of our
executive leadership team. He is one of the industrys finest operators, a great leader and has
been a major contributor to our success.
We expect 2014 to be another good year for the apartment business. Each of our markets, with
the exception of Washington, D.C., should deliver same store revenue growth of 3.5% or greater,
with San Francisco, Seattle and Denver expected to produce combined same store revenue
growth of 5.5% or higher. With Washington, D.C. expected to deliver a same store revenue
decrease of approximately 1%, our guidance for 2014 same store revenue growth for the entire
portfolio is between 3% and 4%. As in years past, we should continue to limit the increase in our
property level operating expenses by using technology and our size and scale to operate more
efficiently. We currently expect our operating expenses to grow between 2% and 3% this year,
primarily as a result of significant and uncontrollable increases in property taxes which comprise
over 30% of total operating expenses. These factors should combine to deliver expected same
store NOI growth of between 3.5% to 4.75% and Normalized FFO of between $3.03 and $3.13
per share. At the midpoint of our expected range, Normalized FFO and the annual dividend are
expected to grow 8% this year.
We look back on the last twenty years with a tremendous sense of pride and appreciation to all
of the employees who have been part of this terrific journey with us. They have built a company
of which you can be very proud. We also have great appreciation for you, our shareholders, who
have been with us for so long. We look forward to the next twenty years and beyond.
Regards,
Sam Zell David J. Neithercut
Chairman President and CEO
April 1, 2014