﻿Dear Recipient,
Reflecting back on 2018, I am pleased to report that it
was a year of continued strength in global lottery,
resilience in Italy, and stabilization in the North America
Gaming business. Despite a decline in International
revenue, our performance in the other segments,
combined with our recurring revenue base and the
diversity of our products, allowed us to achieve our
financial objectives for the year.
Global Strength in Lottery
IGT’s global lottery results were impressive throughout
the year. Innovation was a key factor in driving those
results, and supported growth in instant ticket and
draw-based sales. In fact, IGT won the International
Gaming Awards’ Lottery Product of the Year for the
second year in a row. Momentum in lottery was
accentuated by large jackpots in North America and
Europe.
North America Lottery performance was robust. Instant
tickets and draw-based games performed well, and the
large Mega Millions jackpot helped increase same-store
revenue by five percent. IGT also secured a 10-year
contract with the South Carolina Lottery and multiple
contract extensions with several prominent lotteries.
International lottery growth was largely led by a
recovery in U.K. wagers due to new scratch tickets and
instant win games. Increased EuroMillions jackpots
were also important contributors to growth.
Our lottery results in Italy were a true testament to
innovation driving growth, and the new MillionDAY
game was launched in early February. 10eLotto wagers
did exceptionally well throughout the year and were up
11 percent primarily due the additional game option,
Doppio Numero Oro. The successful re-launch of the
Miliardario and Multiplier games led to Scratch & Win
wagers being up for the second consecutive year.
Stabilizing the Global Gaming Business
Our gaming business saw encouraging progress in
North America driven by new cabinet introductions,
such as the CrystalDual® 27 and CrystalCurve™. Both
were cornerstones of IGT’s ICE and G2E portfolios. The
strength of the Wheel of Fortune® franchise was
accentuated by new products such as the Megatower™
cabinet and the Wheel of Fortune® Triple Red Hot 7s™
Gold Spin™ game.
Replacement unit shipments benefited from the new
hardware. We also saw the power of our compelling
core video content. Our games Scarab™, Solar Disc™,
and Mistress of Egypt™ are recognized as some of the
best performing games in North America, upgrading our
standing in the core video market.
Gaming made headway in strategic International
markets, including Australia, where the Crystal cabinets
are driving growth with successful new games such as
Star Stax™ and Fortune Gong™.
It was a strong year for systems and software sales for
our Gaming customers. IGT’s Advantage® was the
casino management system of choice for several of the
largest casinos that opened in 2018. We continue to
focus on mobile systems innovations such as Cardless
Connect®, Resort Wallet, and Mobile Intelligent Offer
that our customers can leverage to drive the player
experience forward.
Despite new regulations and the impact of higher taxes,
our Italian Gaming operations remained resilient. Even
with fewer AWP machines in the market, overall gaming
machine wagers grew due to improved productivity of
remaining AWPs and VLTs.
Sports Betting in North America
It was an important year of progress for sports betting in
North America. In May, the U.S. Supreme Court
overturned the Professional and Amateur Sports
Protection Act of 1992 (PASPA), legalizing sports
betting at the federal level. To date, IGT’s PlayShot™
sports betting platform is live in six states. IGT also
formed strategic sports betting partnerships with
William Hill and FanDuel. Significant strides were made

in sports betting in 2018, and we are well-positioned to
continue this momentum.
Our Commitment to Corporate Social
Responsibility
Our goal is to provide value for all our stakeholders,
including creating solutions that help our customers
protect their players. In 2018, we committed to the
United Nations Global Compact, which calls on
companies to align strategies and operations around
human rights, labor, environment, and anticorruption. Of the UN’s 17 Sustainable Development
Goals, IGT has identified nine that the Company can
influence as part of its long-term plan.
IGT strengthened its commitment to responsible
gaming in 2018 with recertification by the World Lottery
Association (WLA) for its Corporate Social
Responsibility Standards and Certification Framework.
In addition, we successfully achieved an intermediate
assessment by the Global Gaming Guidance Group
(G4) for its Responsible Gaming accreditation and
received the Internet Responsible Gambling
Compliance Assessment Program (iCAP) recertification.
We strive to enrich to and strengthen the communities
in which we operate. We launched our first-ever Global
Giving week and participation in our employee
charitable programs more than doubled.
Diversity & Inclusion
In 2018, IGT established the Office of Diversity and
Inclusion which is responsible for implementing the
Company’s Global Strategic Plan for Diversity and
Inclusion, including programs that address increasing
workplace diversity and inclusion. We became a
founding member of the All-in Diversity Project to
champion diversity and inclusion across the gaming
industry. We also broadened our diversity and inclusion
efforts by establishing a variety of Employee Business
Resource Groups (EBRGs) to build business networks
around groups of people that are historically underrepresented in the workplace. There are currently
EBRGs throughout our company that support women,
veterans, people with disabilities, and the LGBTQ
community.
Looking Ahead
Over the last four years, we have established a solid
foundation, securing long-term lottery contracts,
stabilizing North America Gaming, and investing in new
growth opportunities with sports betting. Looking ahead
to 2019, we are confident there are plenty of underlying
growth opportunities to create value for all
shareholders.
Marco Sala
Chief Executive Officer