DEAR FELLOW SHAREHOLDERS:

Fiscal 2002 was a landmark year for The J. M. Smucker
Company. In October 2001, we announced an agreement with
The Procter & Gamble Company to merge the Jif and Crisco
brands into The J. M. Smucker Company. Our shareholders
overwhelmingly approved the merger at a special shareholder
meeting held last April, and we thank you for your vote of
support. The transaction closed this past June 1, and we now
have under our banner three American icon brands: Smuckers,
Jif, and Crisco. Each is number one in its category, and each
has a long and proven history with strong consumer equities.
The addition of Jif and Crisco will nearly double our sales
and more than double our profits in the coming year. In the
past, approximately 50 percent of our sales came from fruit
spreads across several business areas. The new Smucker
Company, on the other hand, will be predominantly a consumer
retail, branded business, with a good balance of product categories.
Roughly a quarter of our business will be in the peanut
butter category; a quarter will be in fruit spreads; and another
quarter will be in shortening and oils. We think this provides
us with a more balanced, profitable basis for future growth.
This transaction also adds more than 700,000 new shareholders
to our existing shareholder base. As a result, many
of you are receiving our annual report for the first time, and
we wholeheartedly welcome you into the Smucker family.
Our number of shares outstanding has doubled to more than
49,500,000, increasing the liquidity of our stock. Most important,
in the course of the 2002 fiscal year, the market value of
a common share increased more than 30 percent. Increasing
shareholder value has always been a priority, and we will
continue to manage the Company with the goal of providing
our shareholders with a fair return on their investment.

Our aim has been to complete the transition to the new
Smucker Company as smoothly and seamlessly as possible,
and we are pleased to report that our integration activities are
virtually complete. Of course, the process required significant
time and attention from all levels of our organization. We nevertheless
maintained careful oversight of our core business and
achieved record results in terms of sales and earnings per share.
Sales for the year reached $687 million, an increase of six
percent, while basic earnings per share, excluding merger costs
and nonrecurring charges, grew from $1.16 to $1.39, a 20 percent
increase. We are very appreciative of our employees extra
efforts, and we are grateful to our customers and consumers for
once again making the Smuckers brand their number-one choice
for fruit spreads, natural peanut butter, and ice cream toppings.
We will review the years accomplishments by strategic
business area, beginning with consumer, which is our largest
and most profitable business area.
CONSUMER
Our U.S. consumer business had a record year, with sales
and profits increasing four percent and five percent respectively.
We continued to expand our share of the fruit spread market,
reaching an all-time high in excess of 40 percent across all
retail segments.
The increase in our core business was driven primarily by
double-digit growth of our Sugar Free fruit spreads and natural
peanut butter products. Several exciting new products also
contributed to our growth, including two new squeeze fruit
spreads that exceeded our expectations in test markets and that
we will offer in expanded distribution in fiscal 2003. Continuing
our relationship with Masterfoods USA, a division of Mars, Inc.,
we also introduced two new cobranded ice cream toppings:
Twix Magic Shell and MilkyWay spoonable topping.
Among the keys to long-term success are efforts to support
our brands in ways that benefit our Company as well as our
customers. Through television and print advertising, which this
year emphasized our Sugar Free fruit spreads, and through
carefully selected sponsorships, we seek to promote our brands
and bring smiles to consumers the world over.

Our ongoing support of Willard Scotts birthday segment on
NBCs Today is a prime example. This year we were a major
sponsor of Walt Disney World Resorts 100 Years of Magic
Celebration, the World Figure Skating Championships, and a
series of skating specials airing on NBC. Especially exciting is
the recent announcement of our multiyear sponsorship of
Smuckers Stars on Ice, a premier ice skating tour that features
Olympians and World Champions such as Tara Lipinski,
Todd Eldredge, Alexei Yagudin, Kurt Browning, Jamie Sale and
David Pelletier, and Elena Berezhnaya and Anton Silkharulidze.
As mentioned, the Jif peanut butter and Crisco shortening
and oils businesses became part of the Smucker Company in
June. Jif will be added to our consumer business area. For
Crisco, we have created a new strategic business area that will
help provide focus on shortening and oils, and although this
is a new category for us, we believe the Crisco brand is a great
fit with our Company. Our intent is to successfully implement
the strategies we have developed to fortify both brands already
strong consumer image and gain market share.
In the fourth quarter, we began to expand the retail availability
of Smuckers Uncrustables to an additional 45 percent of
the United States, meaning that the product will be available
in more than half of the country. This thaw-and-serve frozen
peanut butter and jelly sandwich continues to be a hit with
consumers of all ages, and we expect it to remain a star
performer. Significant investment spending will be needed
behind this unique product for the next couple of years, but
we believe it has great potential and offers the Company a
good platform for future growth.
FOODSERVICE
This business, which sells to restaurants, caterers, hotels,
airlines, hospitals, and schools, saw its most challenging year
as a result of a slow economy and the aftermath of the events
of September 11. By the end of the year, however, foodservice
sales were growing again. We were especially encouraged by
the strength of our core Smuckers portion control line, as it
generated modest growth even in a difficult environment.

Earlier in this letter, we discussed the consumer business
areas plan to expand distribution of Smuckers Uncrustables in
the retail channel. This product is also the fastest growing line
in the foodservice area. In foodservice, Smuckers Uncrustables
are sold primarily to school systems. Distribution to schools
continues to increase at a high double-digit rate because the
product meets the need on the part of schools to provide a
healthful, protein-rich menu alternative that is readily accepted
by children. Uncrustables sales in this channel reached
$16 million for the year, and we anticipate continued growth
thanks to the addition in the last quarter of the year of a
prebrowned grilled cheese sandwich. The foodservice area
also expects to reap dividends from expansions made in fiscal
2002 to our PlateScapers line of dessert decorating products.
BEVERAGE
Three health and natural foods brands  R.W. Knudsen
Family, Santa Cruz Organic, and After The Fall  are the
mainstays of this area. Interest in healthful eating continues to
rise in America. As a result, our beverage business grew
steadily this past year. Santa Cruz Organic remains the fastest
growing line in our beverage area, as consumers remain very
interested in products made from certified organic ingredients.
We are the clear leader in all-natural and organic juices and
introduced a number of new flavors this past year. At the
same time, we discontinued certain items that did not meet
our growth objectives. This allowed us to increase efficiencies
in our production facilities, which translated into enhanced
margins for this business area.
INTERNATIONAL
Our international business also saw record results this year,
in spite of unfavorable exchange rates. In constant dollars,
sales in the international business area grew by three percent.
Our two biggest operations outside the United States continue
to be in Canada and Australia. In Canada, we experienced
solid growth and achieved record results by emphasizing our
Smuckers pure and No Sugar Added jams, which we supported

with an expanded national print advertising campaign. The
Smuckers line of retail ice cream toppings and foodservice
portion control fruit spreads also were significant contributors.
Our Canadian group looks forward to managing the Crisco
brand, which has had a strong presence in that market for a
number of years.
In Australia, with our IXL and Allowrie fruit spread
brands, we became the largest manufacturer of fruit spreads 
a first-time milestone for our Henry Jones Foods subsidiary.
Competition in Australia continues to be a challenge, though,
with two new fruit spread competitors entering the market in
the past several years. In spite of that, IXL has maintained its
number-one market share position thanks to growing total
sales of nontraditional jam product lines, such as Light,
Reduced Sugar, and 100% Fruit.
We also are pleased with the performance of our businesses
in Latin America and Mexico, where we experienced a total
sales increase of 22 percent. Another focus for the international
area is on profitable export business. Currently we distribute
our products in more than 45 countries.
INDUSTRIAL INGREDIENTS
For the most part, this business area produces fruit fillings
and preparations and markets them to other manufacturers
for use in their food products. This past year, we acquired
International Flavors and Fragrances Inc.s formulated fruit
and ingredient business. The addition of this business and its
quality line of customers added about $15 million in sales
for the Company in fiscal 2002. Also, as we announced in
our third quarter report, we are continuing to review our
industrial contracts and rationalizing those that do not meet
our long-term margin objectives. As we also announced, the
discontinuation of this business will reduce sales by $40 to
$50 million over the next year or two, but will have only a
minor impact on earnings. The new accounts acquired, combined
with the elimination of lower-margin existing contracts,
will provide us with a stronger, more viable ingredients business
overall and a stellar list of branded customers.

THE FUTURE
The future of The J. M. Smucker Company is more promising
than ever. We have just finished a year in which our core businesses
grew at a steady pace, both top- and bottom-line, and
in which we added two more American icon brands, Jif and
Crisco. This gives us brand leadership in seven U.S. food categories
and significantly supports our vision of a company
composed of American icon brands with the leadership position
in their respective categories. As we look to the years ahead,
we expect to prosper by three strategies: (1) growing market
share of our existing brands, (2) introducing new products,
and (3) making strategic acquisitions. We have an extremely
strong balance sheet that provides us with the ability to continue
to invest in our current brands and at the same time to
support future growth through acquisitions of other leading
brands. Most important, ours is a Company of very talented
people who work well individually and in teams. All of us
are dedicated to doing what is right as we strive to grow the
business at a sustained rate.
Although The J. M. Smucker Company has made significant
changes this past year, the Company will continue to be
grounded on its founding values, what we refer to as our
Basic Beliefs: People, Quality, Ethics, Growth, and Independence.
We are grateful to you, our shareholders, for your continued
support; to our employees for their dedicated and talented
service; to our customers for their faith in us; and to our
consumers for making our products marketplace leaders.

Sincerely,
Tim Smucker
Richard Smucker