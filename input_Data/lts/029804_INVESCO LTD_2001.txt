Chairman's Letter 

Dear Fellow Shareholders: 
The market corrections of 2001, compounded by the tragic events of September 11, tested the strength of the global economy, the asset 
management industry, and AMVESCAP. By drawing on our core strengths -- a diversified product line and an outstanding team of 
professionals -- we successfully met these challenges. The year reaffirmed the soundness of our strategy, and we entered 2002 in a 
strong position. 
Our financial performance reflected market conditions felt throughout our industry. Profit before tax, goodwill amortization, 
and exceptional items was 477.9 million, a 13.8% decrease from our record earnings in 2000. Diluted earnings per share before goodwill 
amortization and exceptional items decreased to 40.0p from 54.7p, which was restated to reflect new reporting requirements for deferred 
income tax. Revenues amounted to 1.62 billion compared with 1.63 billion in 2000. Earnings before interest, tax, depreciation, and 
amortization reached 603.4 million compared with 659.7 million in 2000. 
Funds under management at December 31, 2001, totaled $397.9 billion, a decrease of $4.7 billion from the previous year. 
Acquisitions in 2001 added $32.4 billion in funds under management at the dates of these acquisitions. Net new business amounted to 
$1.8 billion. 
We raised the dividend for the ninth consecutive year. The Board has recommended a final dividend of 6.5p for a total dividend 
of 11.0p in 2001, an increase of 10% from 2000. 
Strength through Diversification 
Building diversified product lines across our businesses and regions is central to AMVESCAP's strategy. In 2001, our diversification helped 
soften the impact of the declining equity markets. Our institutional money market funds and stable value product attracted record new 
assets as clients sought more conservative investments. Our defined contribution business also turned in a record year, adding more new 
participants and assets under administration in 401(k)type plans than ever before. The acquisitions of Perpetual and Trimark in late 2000 
moved us into the ranks of the top retail fund managers in the U.K. and Canada, respectively. Both acquisitions brought awardwinning 
core and valueoriented funds to our lineups at a time when the market was turning against growth funds. 
AMVESCAP also launched our Private Wealth Management business to serve high net worth individuals and families. The 
acquisition of Pell Rudman gave us a platform in the U.S. with a successful history and a solid client base. We expanded our office network 
and launched a new Atlantic Trust brand early in 2002. 
In Asia Pacific we increased our presence in Taiwan by acquiring Grand Pacific, a leading domestic fund business, and 
in Australia we acquired County Investment Management as a platform for growth in one of the world's largest pension markets. In the 
U.S., the acquisition of National Asset Management added core and growth equity expertise to complement our existing value 
equity products. AMVESCAP also developed a variety of new products and services to keep pace with the evolving needs of our 
individual and institutional clients. 
 
Strength through People 
Around the world, 20 million people look to us to help build their financial security. We uphold their trust through the strength and 
quality of our people. That is why our strategy emphasizes the importance of attracting highquality individuals and developing them 
throughout the organization. 
During 2001, the AMVESCAP team proved itself in many ways. Our customer service groups continued to win awards for 
outstanding support and responsiveness. The Houston team ran its business seamlessly during one of the worst floods on record. In the 
U.K., our people successfully completed one of the largest and most complex operating platform integrations in that market. 
These are only a few examples of the commitment we see every day from our people around the world, but at no time did our 
team display greater strength of character than during the tragic events of September 11. On behalf of the Board, I extend my thanks 
to everyone at AMVESCAP for their hard work throughout the year. 
Board of Directors 
We recently welcomed two new nonexecutive directors to the AMVESCAP Board of Directors. Rex Adams, a former senior executive 
at Mobil Corporation and dean of the Fuqua School of Business at Duke University, joined the Board in November. Denis Kessler, a 
noted economist and Executive Chairman of the French Federation of Insurance Companies, joined the Board in March 2002. At the 
same time, Roberto A. de Guardiola stepped down as a director. Roberto's knowledge and insight added an important dimension to 
AMVESCAP's Board. I speak for the entire Board in thanking him for his dedicated service. 
Business Outlook 
Although concerns continue over corporate earnings and the uncertain economic and political climate around the world, the long 
term outlook for our industry remains bright. Demographic shifts and government reforms worldwide continue to point to a dramatic 
increase in retirement savings, the driving force of our industry. 
This year proved our resilience and strong positioning. Looking ahead, we believe the key to creating lasting value for clients and 
shareholders is to maintain our focus on the essential aspects of our business: investment performance and customer service. With our 
sound strategy and the industry's strong underlying fundamentals, I remain as excited as ever about the opportunities to grow 
our company in the years ahead. 
Charles W. Brady 
Executive Chairman 

