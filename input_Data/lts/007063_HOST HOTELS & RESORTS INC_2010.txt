TO OUR STOCKHOLDERS
Steady improvements in the economic climate and our operations, as well as the strategic allocation
of resources for ac quisitions and capital investments to our existing portfolio, helped Host Hotels
& Resor ts begin to recover from 2009, one of the most difficult years in the history of the lodging
industry. By year end, our strategy helped drive a 53% increase in our stock price, significantly
outpac ing both the Standard & Pors and the Morgan Stanley REIT indices for the year.

2010 Accomplishments
2010 turned out to be much better than anyone in the
lodging industry anticipated. We were able to take
advantage of the nascent
recovery from the deep
global recession because
of effective and forwardlooking
financial management
that left us
well-positioned to pursue
opportunities to grow and
improve on our portfolio
of premium assets. We are
in a unique position to
thrive during what we
believe are the early stages
of a sustained growth
period in the lodging industry that will be driven by low
levels of new supply and increasing demand.
When analyzing the accomplishments of 2010, its
important to remember where we began the year.
The lodging industry had just experienced a decline
in RevPAR of over 16% in 2009 and weakness in the
credit markets had created an environment where
financing for hotel transactions was generally unavailable.
As a result of our careful financial stewardship
over the past several years, we were poised to take
advantage of opportunities as the operating and
financing environment improved, shifting our focus
from increasing liquidity to strategically raising and
deploying capital. Some of the highlights for 2010 and
early 2011 include:
 A revenue increase of 7.1% to over $4.4 billion for 2010
driven by comparable hotel RevPAR growth of 5.8%
that significantly exceeded our expectations and industrywide
performance of 5.5%
growth. The improvement
in RevPAR was driven by
increases in occupancy of 3.8
percentage points, while
average room rates were
essentially flat. However,
as
the year progressed, average
rates showed substantial
improvement, driven
by both a shift to higherrated
transient business
and actual rate increases;
 An increase in Funds from Operations in 2010 of 33%
to $.68 per diluted share, while Adjusted EBITDA
grew 3.3% to $824 million. Diluted loss per share
improved 53% to $.21 per share for the year;
 Completing over $500 million in acquisitions in 2010
including the purchase of the W New York  Union
Square, the Westin Chicago River North, the Le
Mridien Piccadilly in London and the JW Marriott
Hotel Rio de Janeiro;
 Acquiring $1 billion in assets in the first three months
of 2011 by purchasing the Manchester Grand Hyatt
San Diego, the New York Helmsley Hotel and a
portfolio of seven properties in New Zealand managed
by Accor under the ibis and Novotel brands;

Investing in our existing assets to enhance the overall
return on our investment provides some of our
highest investment yields. In 2010, we invested over
$114 million in these projects, including the addition
of a 21,500 square foot ballroom and 4,500
square foot outdoor venue space at the Westin
Kierland Resort & Spa, as well as several green initiatives
such as the installation of energy efficient
AquaRecycle systems that reclaim and reuse laundry
wastewater at our properties; and
 Strengthening our balance sheet by lowering our
debt-to-equity ratio and reducing our average
interest rate through the reduction in debt and
preferred stock of over $460 million and issuing
more than $400 million of equity, adding ample
capital to fund acquisitions.
2011 Expectations
Looking forward, its easy to get excited by the potential
for our company. Despite the turmoil in the Middle
East and the recent tragedy in Japan, the current consensus
economic forecast still predicts an improving
economy, which, when combined with reduced unemployment
levels, bode well for lodging demand.
Consistent with the early stages of an upturn in the
lodging cycle, we expect to continue to benefit from
rising demand as occupancy levels are currently well
below our prior stabilized levels. With these expected
increases in occupancy, our managers will be focused
on improving RevPAR by increasing rate and shifting
business to higher-rated group and transient business,
all of which drive the bottom line. We will benefit
from limited supply growth as development financing
for hotels was in the past very limited and there is a
long lead-time from inception to completion of the
development of an upper upscale or luxury hotel.
This positive supply/demand relationship and the
improving economic outlook is a strong indicator for
improving financial performance in 2011. We are
more convinced than ever that our combination of
quality assets, financial strength and management
expertise creates an attractive opportunity for our
company and our investors. We will continue to build
on our industry leading position and work to provide
superior returns to our stockholders.
Richard E. Marriott
Chairman of the Board
W. Edward Walter
President and Chief Executive Officer
March 23, 2011