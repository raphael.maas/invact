Dear Shareholders

Better healthcare. Thats the essence of what Medtronic provides. For some people, better merely connotes the aim of
modest or incremental improvement. But for us, this statement is an everyday reminder that our Mission challenges us
to strive without reserve to do better. Hence, by our definition, to do better is nothing less than a daily clarion call to
actionfundamentally dynamic, innovative, aspirational and never fully achievable.
It is in this spirit that I am proud of the strong progress we made during this past fiscal yearour 60th anniversary
towards enabling both better health for the more than 7 million patients who benefit from our therapies every year
and better healthcare by innovating technologies that enable earlier and better diagnosis, reduce procedure and
recovery times, and, ultimately, remove costs from healthcare systems. In short, our Mission has taken on new meaning
as we expand its focus to include both restoring the health of patients and the health of the system that serves them.
Strong Leadership for Turbulent Times
This fiscal year saw an unprecedented convergence of challenges in the economy and our industry. The global
economic crisis, public and government demands for greater financial transparency, challenges to the existing federal
regulatory regime, a new administration, and looming healthcare reform have all contributed to an uncertainty that is
expected to prevail into 2010.
While the markets continue to reflect these uncertainties, we look forward confidently, knowing that our company
and our industry have faced many other seemingly insurmountable challenges. We navigated the new U.S. healthcare
landscape created by Medicare in the 1960s; in the 1970s, it was the establishment of institutionalized regulatory
oversight through the FDAs Medical Device Act; in the 1980s, the introduction of HMOs; in the 1990s, the Clinton
healthcare reform debate. Today, we face the uncertainty of the current administrations healthcare reform proposals.
As weve expanded internationally, weve learned successfully to navigate a myriad of local and country-level
regulatory and government regimes, each of which has posed its own unique challenges that require adaptation.
And, as we have in the past, well navigate todays complex and demanding challenges with equal determination
and success.
Despite these challengesperhaps because of these challengeswe ended this past fiscal year stronger, more
nimble and more resilient than ever before. We led the dialogue on healthcare reform and took early stands on tough
issues, while continuing to accelerate our investment in innovation, driving sustainable growth and delivering marketleading
performance.
Guided by Our Mission
The uniqueness of our success is owed to our Mission. For the past 50 years, the Medtronic Mission has served as an
unfailing compass, guiding our ability to continuously adapt to the changing economic and regulatory landscape, by
grounding us in our purpose and our passion: alleviating pain, restoring health and extending life for the millions of
patients that depend on us.
The Foundation of Our Success
Our Mission also specifically calls for us to practice good citizenship as a company. This is in recognition that we must
share our prosperity with the global communities that contribute to our success. Of course, our products and therapies
carry an inherent social benefit, but we are committed to do more, and to focus our reach and resources where we can
make unique contributions by tapping into the expertise and passions of our employees.

This past fiscal year, weve done more to further this
commitment, in more parts of the world, than ever before. The
Medtronic Foundation gave nearly $30 million to organizations
in more than 30 countries. Our contributions funded many
worthwhile endeavors, from building healthcare infrastructure in
India for better treating diabetes and heart disease to supporting
a network of patient-focused organizations in Western Europe,
Japan and North America.
Our long-standing commitment to exceptional global
corporate citizenry is increasingly relevant in a world where the
growing epidemic of chronic disease elevates both our
responsibility and our impact.
Market-Leading Performance
In fiscal year 2009, we continued to deliver sustainable growth
and market- leading performance. Our revenue grew by 8 percent
to $14.599 billion. Net earnings and diluted earnings per share
were $2.169 billion and $1.93, respectively. After adjusting for
special, restructuring, certain litigation and IPR&D charges and
certain tax adjustments, adjusted fiscal year 2009 net earnings of
$3.283(3) billion and diluted earnings per share of $2.92(3) increased
over the prior year by 10(3) percent and 12(3) percent, respectively.
Strong double-digit constant currency growth in five of our seven
businesses reflected both the strength and diversity of our
portfolio. In fact, during fiscal year 2009, we held the number one
market position in five of our businessesCRDM, Diabetes,
Neuromodulation, Spinal and Surgical Technologies.
One Medtronic
Innovation is central to our ability
to deliver market-leading
performance. To ensure that innovation remains vital and thriving
throughout the company, we continued to execute our One
Medtronic strategic framework. One Medtronic means
leveraging
our size, scale, capabilities, information, best practices, technologies,
people, and common systems and processes in ways that allow
us to
realize synergies that only Medtronic can bring to bear in the
service of our customers and our patients. To achieve this, we
continue to focus on four key strategic initiatives, each critical to
our long-term success:
 Drive sustainable long-term growth through innovation
 Maintain strong focus on improving operating margins
 Deliver earnings per share growth and disciplined capital allocation
 Align the organization for consistent and market-leading execution

Throughout this past fiscal year, we continued to focus on execution of these key initiatives. In doing so, we
strengthened our common approach to treating chronic disease, better integrated our business processes and
operations across our core businesses, made solid progress advancing our pipelines, and increased our focus on
driving innovation and improving R&D productivity across the company.
Fueling Innovation
If it could be said that One Medtronic is designed to keep the heart of our organization pumping optimally, then
innovation is the lifeblood coursing through our veins. Evidence of One Medtronics success has been demonstrated in
each of our businesses, where innovation is paying off, bringing better therapies to patientsand providing better
solutions to todays healthcare challenges.
 CRDMweve delivered market-exclusive technologies like OptiVol; the EnRhythm MRI SureScan pacing system,*
the worlds first for use in MRI machines; MVP (Minimal Ventricular Pacing) and ATP (pain-free shock reduction). Plus,
we are poised to become the leader in atrial fibrillation through our acquisitions of CryoCath and Ablation Frontiers.
 Neuromodulationwere pioneering some of the most compelling opportunities in neuroscience, including the use
of our Activa Deep Brain Stimulation products for the treatment of epilepsy and psychiatric disorders.
 Diabeteswe continued to advance our pioneering leadership in continuous glucose monitoring and the
development of control algorithms that are bringing the possibility of closed loop automated insulin delivery closer
than ever before.
 Surgical Technologieswe launched our Pillar technology to treat snoring and obstructive sleep apnea through a
minimally invasive, in-office procedure that is revolutionizing the way these chronic and health-threatening
conditions are treated.
 CardioVascularclinical data continued to confirm the durability of our Endeavor drug-eluting stents long-term
clinical efficacy, which is now approved in all major markets, including the $500 million-plus Japanese drug-eluting
stent market. Coupled with our acquisitions of CoreValve and Ventor, which provide us with the industrys strongest
platform for market leadership in transcatheter valves, our CardioVascular business is poised to achieve marketleading
performance.
 Spinalwe recently launched our PEEK PREVAIL Cervical Interbody Device and made significant progress towards
our goal of refreshing 40 percent of our Core Spinal products over a 24-month period. We continue to have the most
comprehensive line of innovative products on the market.
There is no greater evidence of our success than the millions of patients we restore to health every year. Pictured on
the previous page with me is Dr. Gunther Faber, a former pharmaceutical company executive who lives in the United
Kingdom. An amazing individual, Dr. Faber is passionate about working to increase access to healthcare in the
developing world and, specifically, Africa. As he so eloquently puts it, A healthy nation is a wealthy nation.

A self-described over-enthusiast of athletic activity ranging
from rugby to running, Gunther was diagnosed in 2004 with atrial
fibrillation. After having managed his condition with medication
for 3 years, Gunther sought a more permanent and effective
solution. In 2007, he underwent our CryoCath Arctic Front
cryoablation procedure.* I had my surgery on the 18th, was home
by the 19th and back to work by the 22nd, Gunther tells me, still
slightly amazed. I shall never forget the feeling I had when I
awoke to see my pulse steadily coming down, coupled with an
excellent heart rhythm. It was like winning the World Cup!
This is truly our passion: restoring people like Gunther to better
health so they can continue to pursue their own passions, more
quickly and with better results and durability.
Uniquely Positioned to Address the Global Burden of
Chronic Disease
Chronic diseases are now the major cause of death and disability
worldwide. The World Health Organization has confirmed that
noncommunicable conditions, including cardiovascular diseases,
diabetes, obesity, cancer and respiratory diseases, now account
for 60 percent of deaths worldwide and 46 percent of the total
global burden of disease.
This past year, I participated in the World Economic Forum in
Davos, Switzerland, where I met with numerous leaders from
government, non-governmental organizations and private
industry to discuss the global pandemic of chronic disease. These
discussions, many of which centered on the critical role that
technology and innovation can play in creating solutions,
both reinforced and emboldened my strong conviction that
Medtronic is uniquely positioned to address the global burden
of chronic disease.
Our diversified portfolio of therapies and deep experience
across a broad spectrum of the most prevalent chronic diseases
coupled with a global footprint that today includes 38,000 of
the industrys most talented employees spread across 120
countriesgive us the scale and perspective to contribute to the
fight against global chronic disease in a way that few companies
are able. By providing more patients across the globe with access
to our life-saving and life-enhancing therapies, we are making an
immediate and sustained impact.
On the following pages, you can read about a few of the many
ways were addressing the key challenges of chronic disease,
whether by helping to monitor and intervene early, speed
recovery, or improve healthcare economics.
Our Promise Has Never Been GreaterOur Mission Never
More Relevant
As we look toward 2010 and beyond, Im struck by the monumental
and exciting inflection point upon which our company sits. Our
focus has never been so sharp, our people so well aligned and
our pipeline so rich with innovation.
We are poised to enter an unprecedented global era in which
the health of entire nations, peoples and economies are
dependent upon real and durable solutions to their long-term
healthcare needs. Never before has the worldwide need for our
therapies been so great and the urgency of our Mission so
apparent. As countries both developed and developing grapple
to find solutions to the exponentially growing burden of chronic
disease, Medtronic will continue to provide the leadership and
vision demanded by these challenging times.
These unprecedented times also provide us with unprecedented
opportunity to bring our therapies to more markets, to more
people and, in turn, continue to deliver sustainable growth and
market-leading performance. I have never been so excited about
our companys ability to deliver.
Sincerely,
William A. Hawkins
Chairman and Chief Executive Office