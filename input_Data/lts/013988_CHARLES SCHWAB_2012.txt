TO MY FELLOW STOCKHOLDERS:


In my letter last year, I made a few observations about
the challenges we would likely face in 2012: a bruising
presidential election, ongoing sovereign debt problems
in Europe, continued deleveraging by consumers, and the
Federal Reserve�s doubling down on their extraordinary
efforts to anchor interest rates at record low levels. These
all came to pass, making the environment difficult for our
company and our clients.
For Schwab, this environment once again depressed earnings
to a level below what we expect to deliver to stockholders
in a more normal environment. For investors, the continued
uncertainty prompted a move out of equities in search of
yield and security. Similarly, trading was muted as investors
struggled to see any signs of confidence and conviction in the
equity market. Perhaps the most damaging aspect of 2012
was the ongoing lack of trust that consumers felt toward
financial services firms. According to a recent major survey,
trust in financial services companies ranked near the bottom
among all industries.
For many of our competitors, this lack of trust is a building
crisis. But at Charles Schwab, we see an enormous opportunity.
Our vision is clear: We want to be the most trusted leader in
investment services. Because when investors trust Schwab,
we grow our market share and build robust earnings power for
our stockholders. Building trust with our clients has been the
hallmark of Schwab for 40 years. We build trust by aligning
ourselves with our clients, by seeing the world through their
eyes, and by disrupting the traditional investing and banking
practices on our clients� behalf.
TRUST MATTERS
Trust matters in our business. Chuck Schwab created this firm
to be safe and dependable, operated with a high level of
integrity, without making compromises at the expense of our
clients. We still believe that�s the way to build client trust, which
leads to growth.
Even in the face of an ongoing difficult economic environment,
in 2012 we continued to grow and prosper. In fact, when 2012
is reviewed years from now, it will go down as an exceptional
year for Charles Schwab. Schwab once again surpassed our
competitors in terms of growth as measured by Net New Assets
(NNA). In the fourth quarter of 2012 alone, clients brought more
than $64.4 billion of their wealth to us, including core NNA of
$47.8 billion. This extraordinary figure is a reflection of the trust
that we have earned among investors with our daily actions. 

As an innovator focused on transforming the financial services
industry, we continued to invest aggressively in building our
market share across our businesses, and we introduced a
series of new products and services to benefit investors. And
importantly, our stockholders were rewarded with an overall
return for the year of approximately 30 percent from growth in
our share price and reinvestment of dividends. For most firms,
this would be cause for celebration. But for those of us referred
to as �Schwabbies,� we reflect only briefly on the successes of
2012, for we are directing our attention to finding new ways to
delight our valued clients and future clients in 2013 and beyond.
2012 RESULTS
Despite the environmental challenges of 2012, Schwab
continued to grow NNA at a pace unrivaled among our
competitors. How? It�s pretty simple: We strive to see through
our clients� eyes. But these words are more than a successful
strategy. They are part of our corporate DNA. And they are
what make Schwab different from the traditional model for
investment services. Our strategy provides a sharply focused
lens that helps us make decisions that should lead to long-term
earnings growth and value creation. I invite you to not simply
take my word that a strategy based on seeing through clients�
eyes works. Let�s look at the results.
For the 12 months ended December 31, 2012, The Charles
Schwab Corporation reported a record $1.95 trillion in total
client assets. Core NNA of $112.4 billion was up 37 percent
over the prior year, and we added 900,000 new brokerage
accounts during 2012, bringing active brokerage accounts to
a record 8.8 million, up 3 percent year over year. The number
of banking accounts grew 11 percent to 865,000, and the
number of corporate retirement plan participants increased
5 percent to 1.6 million as of December 31, 2012.
A growing and engaged client base helped Schwab deliver strong
financial results in 2012.
�	 Net revenues increased 4 percent to $4.88 billion
�	 Net income gained 7 percent to $928 million
�	 Pre-tax profit margin of 29.7 percent remained robust
and even with last year
�	 Diluted earnings of 69 cents per common share compared
to 70 cents per share in 2011
RETHINKING INVESTING
Seeing through clients� eyes has always been the Schwab
way. By listening thoughtfully to our clients, we�ve been able to
deliver services that make investing more accessible, affordable,
and understandable.
Schwab pioneered discount brokerage and opened investing
to the masses. We created an innovative and convenient way
to invest through a supermarket of no-load, no-transaction-fee
mutual funds. We led the way in online investing. We introduced
institutional-quality investment advisory services for the Main
Street investor. We helped thousands of independent advisors
serve their clients. And we disrupted the 401(k) industry to
deliver a better value for employees.
A lot has changed over the past 40 years, but even more has
stayed the same. Investors can still count on Schwab
to champion their financial goals � and to do so with passion
and integrity.
Today, when we look through the eyes of investors, we continue
to see areas where they feel compromised � areas that present
opportunities for a firm like Schwab to challenge and disrupt the
status quo on their behalf.
Investors deserve better. And at Schwab, we�re constantly
innovating and championing better ways for our clients to reach
their goals. In every business segment, we�re focused on the
things that matter most to our clients � service, value, and
convenient access, all delivered with a high level of ethics and
integrity. Let�s take a closer look at how our client-focused
strategy shapes our beliefs and our actions in each area.
1. Rethinking Service
Clients deserve prompt and personalized service �
whenever and wherever they desire it. Last year, we logged
millions of interactions through multiple touchpoints, serving
all types of clients, including individual investors, registered
investment advisors (RIAs), corporate benefit plan sponsors,
and their employees.

Our online and mobile channels continued to be a go-to
resource, primarily for our self-directed investors. Last year on
Schwab.com, we processed more than 296 million client log-ins
and ranked #1 in the Keynote Brokerage Performance Index for
most weeks in 2012, ending the year with a top index speed of
3.08 seconds to log in and place a stock order.
When clients need personalized assistance, many turn to our
toll-free phone lines. In 2012, Schwab answered 13.4 million
calls in an average time of 22 seconds, with an additional
10.2 million calls handled through automated channels. Our
knowledgeable phone reps also received a 9.8 out of 10.0
satisfaction score in an interactive voice response survey.
Of course, we also provide face-to-face assistance at more than
300 Schwab branches nationwide. Last year, our employees
hosted more than 600,000 in-person client meetings and
presented 56,500 personalized financial and investing plans.
2. Rethinking Value
Every client deserves to know what they�re paying for and
what they will get in return. In an industry where few things are
certain beyond the fees an investor pays, we have dramatically
lowered the cost of investing.
We began 2012 with the launch of Schwab Index Advantage�
� a 401(k) plan offering that provides index mutual funds with
low operating expenses and delivers added value by including
a professional, independent advisory service. Later in the
year, we slashed the operating expense ratios for each of
the Schwab ETFs, offering the lowest OERs in their respective
Lipper categories.
But there�s more to value than just low fees. Delivering great
value means giving clients more than they expect. In our
Advisor Services segment, we conducted free Insight to Action
workshops on business development topics such as strategic
planning, and we continued to expand our Schwab Intelligent
Integration� platform as a one-stop resource for customer
relationship management.
3. Rethinking Access
Every client deserves convenient access to the products,
services, and information needed to achieve their goals.
A number of launches in 2012 helped make investing easier
and more accessible. We continued to expand our lineup of
mobile apps for both smartphones and tablets, giving investors
and independent advisors convenient access to their Schwab
accounts. We also introduced new tools for options traders
and a cloud version of our StreetSmart Edge� platform for
active traders.
Through an agreement with Piper Jaffray, we expanded access
to new-issue municipal bonds via the Schwab BondSource�
platform. And our acquisition of ThomasPartners, Inc. will
expand client access to growth-oriented investment portfolios
designed to generate dividend streams � a convenient investing
alternative for those nearing or in retirement.
INTEGRITY MATTERS
Clients deserve to work with smart and caring people who have
their interests at heart. It�s not simply what we do that makes
Schwab different. It�s how we do it.
Many third-party awards have acknowledged Schwab�s clientcentric approach. Last year, The Charles Schwab Corporation
received the number-one ranking in the Securities industry
category in FORTUNE magazine�s 2012 list of the World�s Most
Admired Companies.1
 Charles Schwab also was named �Highest
in Investor Satisfaction with Self-Directed Services� by J.D.
Power and Associates.2
 Also in 2012, for the third straight
year, Schwab ranked number one in customer satisfaction in
an independent survey conducted by the American Customer
Satisfaction Index.
When it comes to service, value, convenience, and integrity, we
want every client to have such a great experience with Schwab
that they recommend us to family and friends. Schwab receives
an astonishing 40 percent of our new clients as a result of
referral from delighted clients.
RISING TO THE CHALLENGE
Our success at building the Schwab franchise in 2012 is clear.
And many of the strong headwinds that have impacted our
earnings the last few years also remain as we move into 2013.
Our nation and its political leaders remain split by competing
views about the role and size of government, the level and
manner of taxing its citizens, and the future means by which we
should meet financial promises made to our citizens. Much of
Europe remains awash in red ink and faces an uncertain fiscal
future. Consumers, although resilient, continue their broadbased deleveraging from the excesses of the last decade. And
maybe most challenging for Schwab, the Federal Reserve, in its
efforts to drive businesses and consumers to spend and invest,
remains committed to a set of ultra-low interest rates that
obscure market forces.
Serious headwinds? Yes. But at Schwab we keep moving
consistently forward, gathering billions of dollars in new
assets as we gain market share.
As we look to 2013 and beyond, we will continue to challenge
the traditional investing services model � to create a better way
to serve investors and their advisors and to earn their trust. Our
clients depend on us to champion their financial goals. We will
speak up on their behalf, changing what needs to be changed,
and reinventing what no longer works. Schwab has never been
about the status quo and never will be.
Clients tell us what matters most to them. For example, we
plan to open select branches on Saturdays to better serve 

clients who prefer face-to-face contact, while still maintaining
the highest level of service over the phone and online. Clients
want an easier way to invest in ETFs, so we responded with
ground-breaking innovation � our new Schwab ETF OneSource�
platform featuring commission-free trades on ETFs from most
of the major ETF providers. Later in 2013, we plan to introduce
an industry-first full-service 401(k) program that will offer
employees the ability to invest in low-cost, index ETFs.
Schwab will continue being a challenger and innovator,
consistent with our heritage. Ultimately, trust is built when
someone shares your values and puts your interests at the
forefront. And that is what Schwab is all about. Chuck Schwab
reinvented investing once four decades ago, and we are doing
it again... one trusting relationship at a time.
Thank you for your confidence.
Warmly,
Walt Bettinger
March 8, 2013