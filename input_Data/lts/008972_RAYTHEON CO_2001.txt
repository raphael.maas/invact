Dear Fellow Shareholders:
The year 2001 will be forever remembered for the horrific
events of September 11. A void separates us from
the world before that day. At Raytheon, we will always
remember the employees we lost and those they left
behind. This Annual Report is dedicated to them and
to all who lost their lives that day.
No accounting of 2001 can adequately describe humanitys
loss. At the same time, it is our determination to
push ahead.
Raytheons transformation
In this spirit, I report to you that the transformation of
Raytheon continued in 2001, albeit with some detours:
 The companys defense businesses, which account
for more than 80 percent of our sales, performed very
well, as their sales and operating income increased six
percent and seven percent, respectively, year-over-year
in 2001. They generated $676 million of cash flow.
 We significantly reduced debt and strengthened our
balance sheet, helped by two successful equity offerings
during the year and the favorable settlement of an
acquisition purchase price dispute with Hughes
Electronics Corporation. Raytheons net debt was cut to
$7.0 billion at the end of 2001, down $2.0 billion for the
year. In addition, Raytheon expects to receive approximately
$1.1 billion from the sale of Aircraft Integration
Systems. (Completion of the sale was announced
March 11, 2002.)
 Raytheon Aircraft Company (RAC) had a tough year
in a difficult market for aviation, but a new Leadership
Team at RAC has moved decisively to cut costs
and drive new programs, and RAC was awarded a
major military aircraft order in the fourth quarter.
 Commercial Electronics had an operating loss due
primarily to lagging demand in telecommunications
and a divestiture. The efforts of Commercial Electronics
and its new Leadership Team are described in
more detail later in this Annual Report.
 We successfully negotiated a walk-away settlement
with Washington Group International (WGI) of the
litigation over the issues arising out of the sale of the
engineering and construction business; however, we
continue to work on completion of contracts rejected
by WGI in bankruptcy on which we had outstanding
guarantee obligations.
The big story is that were achieving financial flexibility
to focus resources on the highest growth areas of our
business  and defense is clearly a growth business,
given the renewed focus of the United States on security
and defense. In this context, Id like to share with you
Raytheons strategy going forward.
Building a global presence
Raytheon seeks to engage the world not just as an
exporter of systems and services, but also through
joint ventures and with a strong presence in-country.
We announced in mid-year the formation of Raytheons
joint venture with Thales. The new enterprise, Thales
Raytheon Systems, has operating subsidiaries in
Fullerton, Calif., and in greater Paris. It encompasses
air defense/command and control centers, and groundbased
air surveillance and weapon-locating radars.
Our in-country presence includes U.K.-based Raytheon
Systems Limited, which is the prime contractor for the
U.K. Ministry of Defence Airborne Stand-Off Radar, a
long-range airborne surveillance system; and Raytheon
Australia, which plans to provide the Royal Australian
Navy with a combat control system and heavyweight
torpedoes for its Collins class of submarines. In
Canada, Raytheon Canada has developed a major new
automated air traffic management system that will be
installed across the country.
Growth
Raytheons strategy has identified several areas in which
we seek to be the industry leader. These are: Missile
Defense; Intelligence, Surveillance and Reconnaissance;
Precision Strike; and Homeland Defense. These are so
important, I want to discuss each separately.
Missile Defense
Layered missile defense is needed to protect U.S. and
allied forces, as well as the U.S. homeland, from longrange,
medium-range, and short-range air and missile
attack. Raytheons technologies and capabilities are
all-encompassing:
 The Exoatmospheric Kill Vehicle (EKV), Ground-
Based Radar-Prototype (GBR-P), and PAVE PAWS
Early Warning Radar for the Ground-based Midcourse
Defense Segment, which used to be called
National Missile Defense;
 The Space-Based Infrared System-Low, for which
we are competing to provide sensor and ground systems;
 The Standard Missile-3 for theater-wide, sea-based
missile defense;
 The Ground-Based Radar element and the Battle
Management Command and Control element for
the Theater High-Altitude Area Defense system,
called THAAD;
 The Patriot Air and Missile Defense System for
U.S. and allied forces (Raytheon is the Patriot prime
contractor  and system integrator for the PAC-3
system, working with Lockheed Martin);
 And the Complementary Low Altitude Weapon
System, which uses combat-proven AMRAAM
missiles in an air defense mode.
The U.S.Missile Defense Agency conducted back-toback
successful ground-based missile defense tests in
2001 using Raytheons EKV, GBR-P, and PAVE PAWS
radar. Raytheon was disappointed that the Department
of Defense cancelled the Navy Area missile defense program,
which curtailed our Standard Missile-2 Block
IVA development program. Raytheon was making good
progress on cost and schedule issues, and the program
was poised to test the systems capability in early 2002.
The mission requirement continues, and were working
hard to help shape the way forward. In longer-range,
sea-based missile defense, the Missile Defense Agency
and the U.S. Navy, in January 2002, intercepted a ballistic
missile target using the Raytheon ship-launched
Standard Missile-3 with a kinetic warhead, a first.
Intelligence, Surveillance and Reconnaissance (ISR)
The elements of ISR can be thought of as follows:
intelligence is the ability to gain insight; surveillance is
the ability to monitor; and reconnaissance is the ability
to discover. The goal is not simply to develop information,
but to task, process, exploit, and disseminate
that information to our men and women in uniform
when and where they need it. In discussing ISR, we
sometimes add to it the power of command, control,
communications, and computers, which is described
by the acronym C4ISR. Either way, this is an extremely
important defense area.
Raytheon is a world leader in capabilities to support
these needs.Were deeply involved, for example, in
new programs such as the Global Hawk and Predator
unmanned aerial vehicles, or UAVs; the Airborne
Stand-Off Radar, or ASTOR; and the ship-based
Cooperative Engagement Capability, or CEC, a sensor
networking system to bring network-centric warfare
to the U.S. Navy.
Just as ISR is critical to defense, it is also important to
the civilian sector, especially in areas such as air traffic
management and environmental monitoring. Raytheon
is a world leader in both disciplines, with the U.S.
governments new Standard Terminal Automation
Replacement System, called STARS, and the government
of Brazils environmental monitoring system
for the Amazon region, called SIVAM.
Precision Strike Systems
Precision strike systems draw on advanced technologies
to respond precisely to a threat, thus increasing
the likelihood of success the first time and reducing
risk to noncombatants. Though relatively new, precision
strike technology has quickly become the norm
for U.S. forces.While the vast majority of weapons
used in Operation Desert Storm were not precision
guided, the situation has now totally reversed. Our
precision strike systems have played an important
role in the war on terrorism.
Precision strike is built on situational awareness, battle
management/command and control, and precision
munitions. Using the Raytheon APQ-181 radar, the
B-2 bomber provides the U.S. Air Force with precision
strike capability that can be delivered anywhere in the
world. In just 90 days, Raytheon delivered three AAS-52
Multi-Spectral Targeting Systems for the Predator
unmanned aerial vehicle. Raytheon also provides the
Tomahawk sea-launched cruise missile and Paveway
laser-guided bombs, as well as other systems that are
bringing new levels of precision to the battlespace.
Homeland Security
Raytheon has created a special mission area to investigate
ways to further strengthen homeland security.
The company has a wealth of experience with the
kinds of technologies, systems, and services that can
bolster defenses in the new war against terrorism.
Raytheon is applying its advanced technologies and
resources to detect threats, protect lives, and respond
to disasters.
Were focusing on the protection of public access facilities
such as airports, where we are already installing
baggage screening systems; the development of global
solutions to help manage the flow of visitors to the
United States; assistance to first responders through
command and control systems and lightweight
infrared imaging devices; portable chemical agent
detectors; and computer network security.Were
already working on security for the Navy-Marine
Corps Intranet and have formed a wholly owned
subsidiary that offers customers the powerful
SilentRunner system for internal network security
analysis.Were focusing on other needs as well, including
our traditional capabilities in airspace surveillance
and missile defense.
Restoring preeminence in aviation
While Raytheon Aircraft has struggled with a difficult
aviation market caused by the economic downturn, a
new Leadership Team has reduced costs, announced an
intention to form a joint venture in the fractional ownership
market, and moved forward on three new aircraft
programs: the Premier I entry-level business jet,
the T-6A Texan II military aircraft trainer, and the
Hawker Horizon super mid-sized business jet.
The Premier I received FAA certification in 2001 and
has entered production. The T-6A Texan II got the goahead
from the U.S. Air Force for full-rate production
in the fourth quarter, followed by a contract with a
potential value of $1.2 billion. The Hawker Horizon
had its first flight during the year.We expect the new
fractional ownership joint venture to be finalized soon.
It would combine Raytheon Travel Air, which is
RACs fractional ownership business, with Flight
Options, Inc. to form Flight Options LLC.
Equally heartening is the commitment of Raytheon
Aircrafts employees. They are drawing on their experience
from prior downturns in this cyclical industry and
on their Beechcraft and Hawker heritage, to refresh our
product line and rebalance the operation for the future.
Offering a full range of customer services
One of our less heralded strengths is the depth and
breadth of our service business. Technical services and
logistics support have assumed even greater importance
as defense technologies have evolved in complexity
and defense coordination has become increasingly
sophisticated.
Raytheon Technical Services Company (RTSC) provides
technical, scientific, and professional services to defense,
federal, and commercial customers around the world.
In 2001, RTSC was awarded contracts from a wide
range of defense and civilian agencies. In June, a joint
venture half-owned by RTSC was awarded an Air Force
Joint Range Technical Services contract with a potential
value of $1.7 billion, a major achievement. RTSC had a
significant increase in sales and operating income in 2001.
This business is intensely customer driven. It is an organization
that, as logistics manager for the U.S. Antarctic
program, worked with the National Science Foundation
and a team at Kenn Borek Air to extract an ailing doctor
from the South Pole during the Antarctic winter.
The temperature at the time of the mission was near
minus-90 degrees Fahrenheit.
Leveraging technology in commercial markets
Raytheon Commercial Electronics (RCE) focused its
efforts on strategic restructuring in 2001 in the face
of weakened markets. The business discontinued the
Crosspan communications operation and finalized
the sale of the Raytheon Marine recreational products
group.
In early 2002, we took the latest step in our strategy to
commercialize defense electronics, launching a venturecapital
backed startup, TelASIC Communications Inc.
Based in El Segundo, Calif., it will design high performance,
cost-effective, application-specific integrated
circuits for use in advanced wireless products. Both the
technology and the core team needed to create TelASIC
were spun out of Raytheons $8 billion Electronic
Systems business. TelASIC is Raytheons second spin-off
since the formation of Raytheon Commercial Ventures
Inc. (RCVI).
RCVI is the Raytheon subsidiary responsible for
exploiting the commercial value of defense technologies,
largely by creating equity interests for Raytheon in
stand-alone commercial businesses. In November 2001,
we announced the combination of our wireless local
area network activities with Systemonic. The goal is
to provide a cost-effective solution in the wireless
connectivity market for laptop computers as well as
many cellphones and PDAs (Personal Digital Assistants).
We have retained equity in both new ventures.
Excellence through Raytheon Six Sigma
Raytheon Six Sigma is our approach to improving
everything we do. It teaches us to address a problem by
defining where we are today, using data for an objective
assessment, and then defining where we want to be 
the desired end state. The challenge is to close the gap
using disciplined processes.
In 2001, Raytheon Six Sigma initiatives generated
approximately $150 million in operating profit and
$300 million in cash flow.We have qualified over 6,000
Raytheon Six Sigma Specialists to date, with thousands
of others in development.We now have 865 Experts
who have completed training. And in 2001 and early
2002 we identified 28 Master Experts. Their assignment
is to work with business leaders in our operations to
develop ways to leverage Raytheon Six Sigma to help us
achieve operational goals. Raytheon Six Sigma is
becoming an integral part of our culture.
Working as One Company
Raytheon is creating a culture of performance across
the breadth of our company and its businesses to provide
superior solutions to our customers needs. A good
example is the one company perspective our team has
brought to cruise missile defense. By building internal
relationships among 16 locations across our businesses,
by constantly focusing on the customers total needs,
our cruise missile defense enterprise team has demonstrated
that Raytheon is looking at the big picture in
this key defense area. In this way, we strive to make
Raytheon greater than the sum of its parts.
The mission ahead
Our company strategy, then, has seven elements. All are
highlighted on page 6 of this letter.We will execute this
strategy in the context of four goals for 2002, which are
displayed on this page.
Raytheon has come a long way in the last few years.
We still have work to do to increase shareholder value.
The precepts are clear: consistent execution on customer
programs and elimination of waste and variation
to drive down costs and working capital  by the full
engagement of each of our more than 80,000 employees.
The entire company is driven by an even higher imperative:
to support our men and women in uniform who
are on the front lines defending freedom.
It is, as President Bush defined it, a struggle between
freedom and fear. Our challenge is to make sure that
those who go into harms way in defense of freedom
are always far better prepared and equipped than any
who try to conquer us through fear.
We are honored to have the opportunity to meet this
challenge by working our hardest to provide the very
best technology, defense systems, and services to those
who defend us. The people of Raytheon have a history
of success in this mission.We are determined to build
on this success in the period ahead.
Sincerely,
Daniel P. Burnham
Strengthening the business
Strong defense business, aligned with customer priorities
More than $2 billion in successful equity offerings
Selected divestitures and joint ventures

Vision
Raytheons vision is to be the most admired defense
and aerospace systems supplier through world-class
people and technology.

Strategy
Build a global presence
Be the industry leader in Missile Defense;
Intelligence, Surveillance and Reconnaissance;
Precision Strike; and Homeland Defense
Restore Raytheon Aircraft to preeminence in aviation
Offer a full range of services to our customers
Create value in commercial markets by leveraging
our technology
Continually demonstrate operational excellence
through Raytheon Six Sigma
Work as One Company to provide superior
solutions to customer needs

Goals for 2002
Customer Satisfaction: Enhance customer success by
anticipating and exceeding expectations, shaping
technology to the need, and executing consistently;
strive to be flawless.
Growth: Drive revenue growth by exploiting our
combined strengths across the company to serve
the customer and the mission.
People: Make progress as an employer of choice,
with emphasis on creating a work environment that
encourages learning, professional development, and
open and direct communication.
Productivity: Make substantial progress in cash flow
and productivity by improving program execution
through the Integrated Product Development System
(IPDS), contemporary supply chain management
practices, and Raytheon Six Sigma.