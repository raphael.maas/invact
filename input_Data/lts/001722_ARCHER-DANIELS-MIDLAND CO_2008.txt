                                                              Chairman, Chief Executive Of cer and President
                                                       Patricia A. Woertz at ADM's of ces in So Paulo, Brazil.




           TO THE SHAREHOLDERS AND EMPLOYEES OF ADM,



           ADM's strength has long been in the synergy of acumen
           and assets. The deep experience and insight of our
           people, combined with our global network of large-scale
           assets -- balanced across the agricultural processing
           value chain -- allow us to identify and then add value to
           opportunities as they arise. In 2008, we deployed our
           unique combination of acumen and assets against
           an exceptional set of global opportunities and challenges
           and again delivered excellent nancial results. Even as
           strong demand for crops and commodities challenged
           the global supply chain, we continued to capture value
           for our shareholders as we met our customers' needs for
           food, feed, fuel and industrial products.



 
   For the year, net earnings reached $1.8 billion, or $2.79     are constrained, ADM's ability to produce renewable alter-
   per share, compared to 2007 results of $2.2 billion in        natives becomes essential.
   net earnings, or $3.30 per share. Net earnings for 2007
                                                                 To ful ll our vital role requires something of "bifocal" vision.
   included after-tax gains on asset sales of $665 million,
                                                                 We must look ahead to the future -- planning and investing
   or $1.01 per share.
                                                                 now to ensure we continue to have the scale and reach
   Results for the year were highlighted by record segment       to serve growing global needs. At the same time, we are
   operating pro t of $3.4 billion, a 9% increase over 2007.     mindful that each quarter and each year external factors --
   This was our third consecutive year of record segment         volatile commodity prices, planting decisions, weather
   operating pro ts. Net sales rose 59%, to $70 billion.         conditions -- present new sets of challenges and opportu-
   This increase re ects higher average selling prices due       nities. That's why we devoted effort this year to building
   primarily to increases in underlying commodity prices.        our internal capacity and our collaborative culture to sustain
   And we increased our dividend and extended our record         the growth of our global enterprise well into the future. We
   of uninterrupted payments to 76 years.                        strongly believe that the positive steps we've taken stand
                                                                 to enhance our pro tability now and for years to come.
   Our commitment is to achieve the right results, the right
   way, which includes working safely. In 2008, we reduced       ALIGNED FOR GROWTH
   lost work day frequency by 22%, and total recordable          Our actions to build success align with our historic
   incidents by 12%, from 2007 totals. This was our eighth       core strengths:
   consecutive year of double-digit improvements in safety, as
   we pursue our commitment to zero incidents, zero injuries.
                                                                                                                      ADM's
                                                                  PROCUREMENT AND TRANSPORTATION ASSETS.
                                                                  procurement network includes more than 300 facilities
   VITAL TO THE WORLD
   Excellent earnings and record segment operating pro ts         on six continents, and we continue to add capacity in
   underscore the vitality of our business model, which           strategically important geographic areas as customer
   spans the agricultural value chain from procurement            demand and new opportunities warrant. For example,
   and transportation, through processing, to distribution,       last year we acquired a large network of grain elevators
   marketing and sales. With this model, we serve a critical      and rail shipping terminals in Illinois. This additional
   function of reliably connecting crops to domestic and          infrastructure enables ADM to increase service to local
   global markets. We produce the right products and deliver      farmers while ensuring a steady ow of commodities
   them at the right time to serve vital needs.                   for ADM's own food, feed and fuel production, and
                                                                  merchandising needs.
   To say that ADM today is vital to the world is not a boast;
   rather, it is an acknowledgement of the role we ful ll as
   one of the world's largest agricultural processors.                                                           Our more
                                                                  than 230 processing plants around the world generate
   In a world where global demand for food is projected to        a remarkable array of products made from corn,
   double by 2050, ADM's ability to source, store, transport      wheat, cocoa, oilseeds and other feedstocks, and the
   and transform crops into essential nutrients for people        breadth of our operations gives us exibility to adjust
   and livestock grows more vital every day. And in a world       our manufacturing activities to respond to shifts in
   where energy demands are rising while traditional supplies     a dynamic global marketplace. In 2008, we continued
                                                                  expanding our oilseed processing capacity to meet
                                                                  increasing demand for protein meal and for low-transfat
                                                                  and transfat-free oils. We recently acquired a rapeseed
 
                     crushing facility in Germany, giving us new access to
                                                                  markets in Central and Eastern Europe. We are continu-
                   ally investing in our corn wet mills to expand output and

                                                            lower operating costs. And we are increasing our cocoa
                                                                  processing infrastructure with the addition of new plants
                                                       in Kumasi, Ghana, and Hazelton, Pa., both of which are
                  

                                                                  expected to commence operations in scal 2009.




                                                                                                            
                                                                               such as cellulosic fermentation, liquefaction, pyrolysis
                                                                               and gasi cation is pointing the way to second- and third-
    Our insight into crop availability -- combined                              generation renewable fuels with the potential to foster
                                                                                greater energy ef ciency and environmental bene ts.
    with our global sourcing network, delivery
    capabilities and processing efficiencies -- helped
    to keep products moving to customers despite
    market- and weather-related disruptions.
                                                                                        In 2008, we reorganized to streamline our opera-
                                                                               VALUE.
                                                                               tions, enhance accountability across the Company, and
                                                                               provide leadership opportunities for more colleagues.
                                                                               We also chartered several cross-functional, international
                                                                               teams focused on areas such as sustainability and risk
             DELIVER INGREDIENTS WITH THE NUTRITIONAL PROFILE
                                                                               management, and we created development plans for
                               In a changing world, with volatile commodity
             THEY NEED.
                                                                               more than 3,000 colleagues.
             prices and evolving consumer preferences, we work
             with customers to provide cost-effective solutions from
                                                                              As we consider the challenges and opportunities that lie
             among the array of ingredients we create from the
                                                                              ahead for our Company, I want to touch on an issue of
             crops we process. This year, as crop prices rose, we
                                                                              importance to the agribusiness sector as a whole. This
             collaborated with feed customers to identify affordable
                                                                              past year, there was considerable public discussion
             nutrient blends using alternatives such as canola meal,
                                                                              about the ability of agriculture to provide for both food
             soybean meal and distiller's dried grains combined with
                                                                              and energy needs in a responsible, sustainable manner.
             advanced nutritional additives. Our insight into crop
                                                                              ADM shares the view of many that farmers in the U.S. and
             availability -- combined with our global sourcing network,
                                                                              around the globe have the ability to increase productivity
             delivery capabilities and processing ef ciencies -- helped
                                                                              to meet growing global demand. That ability will be en-
             to keep products moving to customers despite market-
                                                                              hanced in coming years by technological advances that
             and weather-related disruptions.
                                                                              will boost crop yields, reduce waste, improve processing
                                                                              methods, and reduce water and energy usage. We are
                                                                              con dent that, with these advances, agriculture can
                                                                     For a
                                                                              effectively and sustainably meet global needs for food
             well-balanced company like ADM, with existing scale in
                                                                              and feed and play an increasing role in the production of
             corn wet milling and oilseed processing, the bioenergy
                                                                              renewable fuels and chemicals.
             business presents unique opportunities. With key crop
             inputs leveraged across the product chain, total value is
             enhanced. In 2008, we continued to build to capture this
                                                                              "As variable as the weather" is just a gure of speech to
             value. Our two new U.S. corn dry mills will come on line
                                                                              many. To us, it is a reality we address every day. We look
             in 2009 and 2010, and will initially increase our annual
                                                                              ahead knowing that, whatever may come, our assets
             U.S. ethanol production capacity to 1.65 billion gallons.
                                                                              are well positioned and our people are ready to seek out
             In scal 2008, we opened biodiesel plants in Velva, N.D.,
                                                                              and capture value -- with integrity, responsibility and an
             and Rondonopolis, Brazil. And, we advanced our collab-
                                                                              abiding commitment to the interests of our shareholders,
             orative effort to develop renewable transportation fuels
                                                                              customers, employees, communities and the world at
             derived from biomass.
                                                                              large. We are vital to the world. And we intend to ful ll our
                                                                              role, with passion and vitality.
                                                         We continually
             deploy technologies to reduce resource consumption
             in our manufacturing processes. For example, over the
             past ve years, we have reduced water consumption for
             ethanol production by more than 35%. Our two new coal
             cogeneration plants, which will cost-effectively provide
             steam and electricity for two large integrated corn pro-         PATRICIA A. WOERTZ
                                                                              Chairman, Chief Executive Officer and President
             cessing complexes, are scheduled to be fully operational
             by year-end 2009. And our research into technologies             September 2008



