                                                                                                     Corporate Report 2014

To my fellow shareholders:
I could not be more pleased to share my thoughts on E*TRADE's
significant accomplishments throughout 2014, a year which, by all
accounts, was nothing short of outstanding. First the highlights, we:

      ntensified our commitment to better serve customers
     I
      which, accompanied by an improving macroeconomic
      environment, helped propel the Company to its strongest
      financial performance in eight years.

       et multiple brokerage performance records, particularly in
     S
      asset and account growth.

       volved the public face of E*TRADE through the launch of
     E
      a new brand platform.

       ontinued to invest in the franchise by enhancing the customer
     C
      experience, strengthening our foundation, and building out
      enterprise risk management capabilities.

       ucceeded in reducing legacy risks, amplified by the proactive
     S
      elimination of asymmetric risk through the sales of a non-core
      business and legacy assets.

       elivered on several critical capital plan initiatives, culminating
     D                                                                            Paul T. Idzik, Chief Executive Officer
      in the paydown of a portion of our corporate debt with cash.

In all, 2014 demonstrated the value of the collaborative efforts of the entire E*TRADE team, positioning the Company to
better deliver for our customers and shareholders alike.

Brokerage and financial performance
The brokerage business is the lifeblood of your Company, and its power, scalability, and resilience have been tested time
and again. Set off by positive shifts across a number of macroeconomic factors and further boosted by our vigilant focus
on customer needs, our brokerage flexed a bit of muscle in 2014 -- setting a number of records across key metrics.

We added net new brokerage accounts of 146,000 -- the highest in more than a decade, while again recording a
Company best for account retention. For the third consecutive year, net new assets set a record high, reaching $10.9
billion in 2014. Our customers engaged more with the markets, as evidenced by a 12 percent increase in trading,
exceptionally robust net buying activity of $7.7 billion in securities, and record levels of margin loan balances -- which
ended the year up 20 percent from 2013.

Our strong brokerage results, in conjunction with continued improvement to our legacy risks and costs, propelled us to
record our best operating results since 2006. We earned $330 million for the year, or $1.12 per share, excluding costs
associated with our debt reduction and refinance in November.

Investments in the business
In 2013, we worked diligently to realign our internal operating structure and refocus colleague efforts to better serve our
customers. 2014 built on that momentum to invest in the business -- in the customer experience, in our foundation, and
in our enterprise risk management culture and capabilities. We are just starting to see the benefits of the increased rigor
and improved connectivity across the firm as well as the upgraded quality of our delivery. We are well-positioned to deliver
the industry-leading products and top-notch services our customers expect and deserve from E*TRADE.
                                                                                                    Corporate Report 2014


To this end, we made noteworthy enhancements to the customer experience. Starting with our digital channels, we rolled
out redesigned navigation to deliver a more elegant and intuitive Web experience. We also launched an innovative browser
trading Web app that delivers real-time quotes, research, and trade execution on any Web page being browsed.

The future of personal finance is increasingly, and quite literally, in the palm of our customers' hands, with mobile access
having become a crucial component of our digital experience. In 2014, approximately a quarter of our unique monthly
logins and more than 11 percent of all trades occurred through our mobile applications. E*TRADE has consistently been
at the forefront of the mobile revolution, and we continued to build on that legacy with our new iOS 8 app's first-to-market
functionality, including biometric log in, new ETF screeners, and stock plan functionality.

During the year, we took some big steps forward, evolving the public face of E*TRADE with the launch of the Type
E* brand platform. This campaign underscores our commitment to do more for customers -- to become a trusted
destination for assets and advice. It emphasizes the ability to meet customers on their terms. Above all, it recognizes and
celebrates our customers. This new platform also leverages digital in a way that resonates with customers and aids more
effective communication with them.

In order to offer customers a best-in-class investing experience, it is paramount that we maintain world-class technology
and operations. To that end, significant investment spend was directed at enhancing our already robust foundation.
Investments in technology, operating platforms, back-end systems, and the professionals who support these capabilities
are critical to your company's success, ensuring E*TRADE is positioned to lead the industry forward as an innovator.

Finally, throughout 2014, we continued to direct resources and investments toward a category of our business that has
increasingly become a prerequisite given the heightened expectations of today's regulatory environment -- Enterprise
Risk Management. Efforts here underscore a cultural shift in the very fabric of our organization. Investments in legal, risk,
audit, and compliance in 2014 totaled approximately $90 million, nearly double what we spent just two years prior. Central
to this regulatory progress, our broader efforts go beyond this level of spend and are exemplified by the fact that risk
management is now truly ingrained in everything done across the Company.

Capital plan
We made monumental progress on our capital plan in 2014, catalyzing a number of accomplishments,
including the following:

       uccessful completion of our first round of Dodd-Frank Stress Tests, including positive results and
     S
      dialogue with regulators with respect to our stress testing capabilities and methodologies,
      E*TRADE's planning process, risk management, and controls.

       eduction of significant risks through the sale of $800 million of modified loans, along with the
     R
      elimination of remaining non-agency CMO portfolio and the early termination of $100 million of
      costly legacy wholesale funding.

     Regulatory approval of four consecutive quarterly dividends totaling $300 million from the bank to the parent.

     Use of corporate cash to reduce corporate debt outstanding by $400 million  the first time in our history.

       efinance of $540 million at the lowest coupon in the Company's history and the significant
     R
      extension of the maturity profile.

     Installation of a $200 million revolving credit facility, meaningfully enhancing our liquidity position.

Starting 2015 strong
On the heels of such a results-driven year, we started 2015 with the most momentous announcements to date on our
capital plan. We received regulatory approval to operate E*TRADE Bank at a Tier 1 leverage ratio of 9.0 percent --
providing us with much greater flexibility for bank capital utilization. We also received approval to realign our corporate
                                                                                                  Corporate Report 2014


structure by moving our two broker-dealers, E*TRADE Securities and E*TRADE Clearing, from under E*TRADE Bank to
simplify the distribution of capital generated by those entities to the parent. In connection with the legal entity realignment,
we moved approximately $430 million of excess capital from the broker-dealers to the parent in Q1. Importantly, these
developments underscore the marked improvements to our financial and regulatory standing, create a more streamlined
corporate structure, and give us more freedom to move and utilize capital.

In closing, I want to stress how proud I am of all we accomplished in 2014. As we enter 2015, we have the team,
backbone, and focus in place to further enhance our relationships with our customers, continue to grow the business,
reclaim our roots as a digital disruptor, and continue to bolster the Company -- in ruthless pursuit of excellence for our
customers, owners, and colleagues. I look forward to sharing E*TRADE's future progress with you.

Respectfully,




Paul T. Idzik
                                                                                                Corporate Report 2014


Company Overview
E*TRADE Financial Corporation is a financial services company that provides brokerage and related products and
services primarily to individual retail investors under the brand "E*TRADE Financial." We also provide investor-focused
banking products, primarily sweep deposits, to retail investors. Our competitive strategy is to attract and retain customers
by emphasizing a hybrid model of digital and technology-intensive channels, backed by human support and guidance.

We provide services to customers through our website www.etrade.com, our desktop software E*TRADE Pro, and our
mobile applications. We also provide services through our network of customer service representatives and financial
consultants, over the phone or in person through our 30 E*TRADE branches.

Strategy
Our business strategy is centered on two core objectives: accelerating the growth of our core brokerage business to
improve market share, and strengthening our overall financial and franchise position.

Accelerate Growth of Core Brokerage Business

    C
      apitalize on secular growth within the direct brokerage industry.
     The direct brokerage industry is growing at a faster rate than the traditional brokerage industry.
     We are focused on capitalizing on this growth through ensuring our customers' trading and investing
     needs are met through our direct relationships.

    E
      nhance digital and offline customer experience.
     We are focused on maintaining our competitive position in trading, margin lending and cash management,
     while expanding our customer share of wallet in retirement, investing and savings. Through these offerings,
     we aim to continue acquiring new customers while deepening engagement with both new and existing ones.

    C
      apitalize on value of corporate services business.
     This includes leveraging our industry-leading position to improve client acquisition, and bolstering
     awareness among plan participants of our full suite of offerings. This channel is a strategically important
     driver of brokerage account growth for us.

    M
      aximize value of deposits through the Company's bank.
     Our brokerage business generates a significant amount of deposits, which we monetize through the
     bank by investing primarily in low-risk, agency mortgage-backed securities.

Strengthen Overall Financial and Franchise Position

    M
      anage down legacy investments and mitigate credit losses.
     We continue to manage down the size and risks associated with our legacy loan portfolio, while
     mitigating credit losses where possible.

    C
      ontinue to execute on our capital plan.
     We are now focused on utilizing excess capital created through earnings and by achieving lower capital
     requirements at E*TRADE Bank, while continuing to enhance our enterprise risk management culture and
     capabilities.



Technology
Our success and ability to execute on our strategy is largely dependent upon the continued development of our
technologies. We believe our focus on being a technological leader in the financial services industry enhances our
competitive position. This focus allows us to deploy a secure, scalable, and reliable technology and back office platform
that promotes innovative product development and delivery. We continued to invest in these critical platforms in 2014,
leveraging the latest technologies to drive significant efficiencies as well as enhancing our service and operational support
                                                                                                Corporate Report 2014


capabilities. Our sophisticated and proprietary technology platform also enabled us to deliver many upgrades to our
retirement, investing and savings customer products and tools across all digital channels.

Products and Services
We offer a comprehensive suite of financial products and services to individual retail investors. The most significant of
these products and services are described below:

Trading and Investing

    access to our best-in-class customer website, etrade.com;

      utomated order placement and execution of U.S. equities, futures, options, exchange-traded funds,
    a
     forex and bond orders;

      ccess to E*TRADE Mobile, which allows customers to securely trade, monitor real-time investment,
    a
     market and account information, access educational videos and other content, pay bills and transfer funds
     between accounts via iPhone, iPad, AndroidTM phones and tablets, Windows Phone or Kindle Fire;

      se of E*TRADE Pro, our desktop trading software for qualified active traders, which provides
    u
     customers with customizable trading technology, continuous market visibility, news and information,
     plus live streaming news via CNBC TV;

      argin accounts allowing customers to borrow against their securities, complete with margin analysis
    m
     tools to help customers manage positions and risk;

    cross-border trading, which allows customers residing outside of the U.S. to trade in U.S. securities;

      ccess to 77 international markets with American depositary receipts ("ADRs"), exchange-traded
    a
     funds ("ETFs"), and mutual funds, plus online equity trading in local currencies in Canada, France,
     Germany, Hong Kong, Japan and the United Kingdom;

      esearch and investing idea generation tools that assist customers with identifying investment opportunities
    r
     including fundamental and technical research, consensus ratings, and market commentary from
     Morningstar, Dreyfus and BondDesk Group; and

    access to advice from our financial consultants at our 30 branches across the country and via phone and email.

Retirement, Investing and Savings

      o annual fee and no minimum individual retirement accounts, along with Rollover Specialists to
    n
     guide customers through the rollover process;

      ccess to our retirement center which offers easy-to-use tools, calculators, education, retirement
    a
     solutions, and access to Chartered Retirement Planning CounselorsSM who can provide customers
     with one-on-one portfolio evaluations and personalized plans;

    OneStop Rollover  a simplified, online rollover program that enables investors to invest their 401(k)
     savings from a previous employer into a professionally-managed portfolio;

    access to all ETFs sold, including over 100 commission-free ETFs from leading independent providers,
     and over 7,300 non-proprietary mutual funds;

    Managed Investment Portfolio advisory services from an affiliated registered investment advisor, with
     an investment of $25,000 or more, which provides one-on-one professional portfolio management;

    Unified Managed Account advisory services from an affiliated registered investment advisor, with an
     investment of $250,000 or more, which provides customers the opportunity to work with a dedicated
     investment professional to obtain a comprehensive, integrated approach to asset allocation, investments,
     portfolio rebalancing and tax management;
                                                                                                 Corporate Report 2014


       omprehensive Online Portfolio Advisor to help customers identify the right asset allocation and
     c
      provide a range of solutions including a one-time investment portfolio or a managed investment account;

       xed income tools in our Fixed Income Solutions Center aimed at helping customers identify,
     fi
      evaluate and implement fixed income investment strategies;

       ccess to our redesigned investor education center with over 450 individual educational articles and
     a
      videos from over 10 independent sources and E*TRADE's financial experts, along with live events,
      webcasts; web seminars; tutorials and demos totaling more than 50 courses; and

       DIC insured deposit accounts, including checking, savings and money market accounts, including
     F
      those that transfer funds to and from customer brokerage accounts.

Corporate Services
We offer software and services for managing equity compensation plans for corporate customers. Our Equity Edge
OnlineTM platform facilitates the management of employee stock option plans, employee stock purchase plans and
restricted stock plans, including necessary accounting and reporting functions. This product serves as an important
introductory channel to E*TRADE for our corporate services account holders, with our goal being that these individuals
will also use our brokerage products and services. Equity Edge OnlineTM recordkeeping and reporting was rated #1 in
overall loyalty and satisfaction for the third year in a row by Group Five, an independent consulting and research firm, in its
2014 Stock Plan Administration Study Industry Report.
                                                                                        Corporate Report 2014


E*TRADE Financial Board of                               E*TRADE Financial Executive
Directors                                                Leadership
Rodger A. Lawson, Chairman                               Paul T. Idzik
Retired Financial Services Executive                     Chief Executive Officer

Richard J. Carbone                                       Navtej S. Nandra
Retired Financial Services Executive                     President

Christopher M. Flink                                     Matthew J. Audette
Partner, IDEO                                            Chief Financial Officer

James P. Healy                                           Michael E. Foley
Chief Executive Officer, Capra Ibex Advisors             Chief Administrative Officer

Paul T. Idzik                                            Michael A. Pizzi
Chief Executive Officer, E*TRADE Financial Corporation   Chief Risk Officer

Frederick W. Kanner                                      Karl A. Roessner
Senior Of Counsel, Covington & Burling LLP               General Counsel

James Lam
President, James Lam & Associates

Shelley B. Leibowitz
Retired Banking Executive

Rebecca Saeger
Retired Marketing Executive

Joseph L. Sclafani
Retired Banking Executive

Gary H. Stern
Retired President of the
Federal Reserve Bank of Minneapolis

Donna L. Weaver
Retired Corporate Executive
                                                                                                                   Corporate Report 2014


Shareholder Services
Corporate Headquarters
1271 Avenue of the Americas
14th Floor
New York, NY 10020-1302

Shareholder Information
ir@etrade.com
646-521-4340

Investor Relations
Brett Goodman
brett.goodman@etrade.com
646-521-4406

Media Relations
Thayer Fox
thayer.fox@etrade.com
646-521-4418

Wholly-Owned Subsidiaries
For a list of wholly-owned
subsidiaries, please refer to
our Form 10-K.

Transfer Agent and Registrar
American Stock Transfer & Trust Company
6201 15th Avenue
Brooklyn, NY 11219
1-800-937-5449

Independent Auditors
Deloitte & Touche LLP
McLean, VA




Forward-Looking Statements
The statements contained in this report that are forward looking, including statements regarding the realignment of the Company's legal entity
structure, distributions of excess capital to the parent, greater capital flexibility, enhancing customer relationships, and future prospects for the
Company are "forward-looking statements" within the meaning of the safe harbor provisions of the U.S. Private Securities Litigation Reform Act
of 1995, and are subject to a number of uncertainties and risks. Actual results may differ materially from those indicated in the forward-looking
statements. The uncertainties and risks include, but are not limited to, macro trends of the economy in general and the residential real estate market,
instability in the consumer credit markets and credit trends, increased mortgage loan delinquency and default rates, portfolio growth, portfolio
seasoning and resolution through collections, sales or charge-offs, the uncertainty surrounding the foreclosure process, and the potential negative
regulatory consequences resulting from the implementation of financial regulatory reform as well as from actions by or more restrictive policies or
interpretations of the Federal Reserve and the Office of the Comptroller of the Currency or other regulators. Further information about these risks and
uncertainties can be found in the annual, quarterly, and current reports on Form 10-K, Form 10-Q, and Form 8-K previously filed by E*TRADE Financial
Corporation with the Securities and Exchange Commission (including information in these reports under the caption "Risk Factors"). Any forward-
looking statement included in this report speaks only as of the date of this communication; the Company disclaims any obligation to update any
information.

 2015 E*TRADE Financial Corporation. All rights reserved.
