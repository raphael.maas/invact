DEAR FELLOW STOCKHOLDERS:
Thank you for your investment in Newfield Exploration Company.
2009 will be a year long-remembered. It opened with
unprecedented turmoil in the worlds financial markets.
Although the outlook for crude oil and natural gas demand
was weak, Newfield was well positioned with a strong capital
structure, ample liquidity and a commodity hedge position
that provided our stockholders with financial certainty. The
year ended with a stronger and healthier Newfield. We enter
2010 with a diversified portfolio positioned for future growth.
Throughout our 21-year history, we have maintained a healthy
balance sheet and this tradition has served us well.
For the year 2009, we had a net loss of $542 million, or $4.18 per diluted share. The loss relates primarily to low
natural gas prices in early 2009 and a resulting ceiling test writedown of $854 million (after-tax). Excluding this
charge, $387 million (after-tax) in unrealized commodity derivative losses (mark-to-market) and a $24 million
international tax benefit, our net income for the year would have been $676 million, or $5.13 per share.
From the Newfield perspective, 2009 was an excellent year. We took full advantage of the many options afforded
us from our diversified portfolio of assets. The years highlights included:
Improved Capital AllocationThe economic conditions in the marketplace required us to make proactive business
decisions. We repositioned our capital budget several times in 2009 as we shifted both human and capital
resources to the projects that we felt would provide the best growth and return options. Our portfolio contains
both oil and gas opportunities, providing us with an added element of flexibility. By better targeting our
investments, we delivered improved returns.
Healthy Growth in Production and ReservesOur production increased 9% in 2009 to 257 Bcfe. We delivered results
in the upper half of our original guidance (250-260 Bcfe), despite a voluntary curtailment of about 3 Bcfe in the
second half of 2009 due to low natural gas prices.
Our proved reserves at year-end 2009 were 3.6 Tcfe, a 23% increase over year-end 2008 reserves. Our
improved allocation of capital and reductions in service costs resulted in lower cost reserve additions. We
invested approximately $1.4 billion in 2009 and our finding and development (F&D) costs, including price and

performance revisions, averaged approximately $1.50 per Mcfe. Excluding the negative impact of price related
reserve revisions, our F&D costs would have been approximately $1.18 per Mcfe. Approximately half of our increase
in proved reserves was related to changes in the Securities and Exchange Commission reserve reporting rules, which
allowed us to record significant proved undeveloped reserves in our growing resource plays. Although natural gas
prices were low during 2009, we increased our proved developed reserves by four percent during the year.
Delivered on our Growth Targets, Within Cash Flow, While Reducing DebtWe made a pledge early in the year that we
would live within our cash flow from operations. We not only met our growth projections, but we had ample funds
remaining to add new projects throughout the year and to reduce our debt by approximately $200 million. Lower
service costs and the deferral of projects into future periods also allowed us to spend less during the year.
Lowered our Cash Costs in our Core AreasIn every division, reducing costs was a paramount goal in 2009. We
coupled reduced service costs with continued gains in operational efficiency.
Attainment of our goals and the efforts we made to improve our capital allocation were recognized by Wall Street. Our
stock was one of the top performers in our industry during 2009, up 144% from our low at the beginning of the year.
Our sights are set on 2010 and our goals are once again clearly defined:
1. We expect our 2010 production to grow 8-12% over 2009 levels and expect to have healthy growth in our
proved reserve base.
2. We will remain focused on large, domestic resource plays of scale. Over the last six months, we added more
than 500,000 net acres in resource plays. These will be our plays of the future.
Our largest transaction was the acquisition of a significant portion of TXCO Resources Inc.s assets in the
Maverick Basin for $215 million. This provided more than 300,000 net acres in the emerging Eagle Ford
and Pearsall Shales in southwest Texas.
We established a presence in the Appalachian region through our entry into the Marcellus Shale in
northeastern Pennsylvania. Through a joint exploration agreement that we operate, we have interests in
approximately 35,000 net acres, and we continue to lease in the area. We expect to begin assessment
drilling in mid-2010.
We also expanded our presence in oil plays through a new agreement with the Blackfeet Nation in
northwestern Montana. We now have about 221,000 net acres in a geologically-similar play to the Williston
Basin. In addition, we extended our Monument Butte area footprint, which now covers 180,000 net acres in
the Uinta Basin, and includes our 63,000 net acre position on Ute Tribal acreage.

We are confident in our ability to invest in these types of plays and to
deliver sustainable, repeatable growth at attractive returns.
3. We will continue to increase our investments in oil plays. At year-end 2009,
approximately 30% of our proved reserves, stated on a Mcf-equivalent
basis (6:1), were oil. With todays significant disparity in oil and gas prices,
a substantial portion of our 2010 revenues are expected to come from oil.
In fact, on a 12:1 basis, about 45% of our total proved reserves at year-end
2009 would be oil. We expect that approximately 35% of our 2010 budget
will be directed to oil projects. This is an advantage that we have over many
of our gassy peer companies.
We have long-lived oil plays in the Rocky Mountains and, with continued
improvements in U.S. oil markets, we are confident that these areas are
capable of higher growth rates. We are targeting 15% production growth at
Monument Butte and 40% from our Williston Basin assets. Internationally,
our offshore oil properties in Malaysia and China are generating high returns
with an inventory of attractive development drilling opportunities.
4. We have core competencies in marine environments and our assets
in Southeast Asia and the deepwater Gulf of Mexico provide attractive
investment options. Our long and successful history in the Gulf of Mexico
has been used as the foundation for our entries into deepwater and
International areas. Our Gulf of Mexico and International areas represent
approximately 25% of our total planned 2010 investments and their
potential impact is significant. We are finding attractive investment
opportunities to add reserves and production.
5. We will continue to harvest assets, as appropriate, and re-invest cash flow
into higher return areas. In 2007, we monetized $1.8 billion in assets, which
included the sale of our shallow Gulf of Mexico and North Sea properties.
In 2009, we elected to invest less in our conventional onshore Texas assets,
therefore, harvesting cash flow from these assets and accelerating growth
in our Rocky Mountain and Mid-Continent areas. A substantial portion of
our onshore Texas investments in 2010 will be directed toward our new
properties acquired from TXCO Resources.

6. As always, we are committed to a strong balance sheet. Our longevity as a Company is directly related to sound
business decisions and a capital structure that provides stability in periods of low commodity prices. In early
2010, we issued $700 million in 6 7/8% Senior Subordinated Notes, using the proceeds to retire a significant
portion of our $175 million of notes due in 2011, finance our recent acquisition of assets from TXCO Resources
and repay outstanding borrowings under our credit arrangements.
HIGHLIGHTS FROM OUR CORE OPERATING AREAS ARE BELOW:
Unconventional Growth Areas
The Mid-Continent has been our fastest growing focus area. It represented 55% of our total proved reserves at
year-end 2009. The region is expected to contribute more than 40% of our 2010 production.
Our two major growth drivers in the Mid-Continent are the Woodford Shale and the Granite Wash. Our Woodford
production is now about 200 MMcfe/d net and is expected to grow more than 25% in 2010. Our operating
personnel have made great strides in lowering development costs and improving our margins.
One of the primary drivers behind our improved results is the drilling of longer lateral wells. In 2009, the average
lateral length of our Woodford completions was about 5,000, or double our average lateral length completion in
2006. We estimate that our average 2010 lateral completion will be about 6,000.
In the Granite Wash, our production in 2009 grew to record levels as a result of our first horizontal completions
in this mature play. We have effectively applied our experience from the Woodford to this area and have seen
phenomenal results. Our largest field is Stiles/Britt Ranch. Since late 2008, we have drilled and completed
13 horizontal wells in the Granite Wash, with an average initial production rate of approximately 20 MMcfe/d
gross. We have interests in more than 44,000 net acres in the Granite Wash.
In the Rocky Mountains, our primary investment regions are the proven Uinta and Williston Basins. We also
will focus on the potential of the Southern Alberta Basin. Our properties here are substantially all oil assets. At
year-end 2009, our Rocky Mountains region represented 26% of our total proved reserves and it is expected to
comprise 18% of our 2010 production.
Our centerpiece oil asset in the Rocky Mountains is Monument Butte, where we expect to drill approximately
275 wells in 2010. We have an inventory of thousands of low risk development locations. Area production in
mid-February 2010 was more than 17,000 BOPD gross and is expected to grow by 15% in 2010. We have taken
advantage of improving demand and price differentials for our Black Wax crude and have increased our 2010
planned operated rig count to five.

We have an interest in about 150,000 prospective net acres
in the Williston Basin. In addition, we own an interest in
approximately 54,000 net acres in the mature Elm Coulee
field. At year-end 2009, we had drilled 14 successful wells in
the Bakken and Sanish/Three Forks formations. Our year-end
2009 production of 2,500 BOEPD net is expected to grow
approximately 40% in 2010. We plan to run three rigs in this
area in 2010.
Conventional Assets
In addition to our focus on unconventional resource plays
of scale in the U.S., we also have a portfolio of conventional
plays onshore Texas, in the deepwater Gulf of Mexico and
offshore Malaysia and China.
In the deepwater Gulf of Mexico, we have drilled six
successful operated wells out of seven attempts since 2005.
We have five active developments underway that are expected
to provide significant growth. We also have an inventory of
prospects and anticipate drilling 3-5 deepwater wells per year
over the next several years.
Our largest deepwater Gulf of Mexico discovery to date came
in 2009 on our Pyrenees prospect. We continue to evaluate an
11-block area around Pyrenees where additional targets exist.
Onshore Texas, we slowed our activities in conventional gas
plays during the year in response to lower natural gas prices.
Due to this lack of investment and natural declines, we expect
that production from our existing conventional fields onshore
Texas will decline about 15% during 2010. However, our
newly acquired assets in the Maverick Basin should allow our
2010 production from the region to remain relatively flat.

All of our international assets are oil. With the strength in oil prices in 2009, our average daily production of nearly
17,000 BOPD net contributed 27% of our total revenues. We made an oil discovery in the South China Sea in
2009 on our Pearl prospect and we have additional exploration drilling planned in 2010. The Pearl field is under
development with first production expected in late 2012.
2010 Outlook
Our planned 2010 investments align with our vision. We will focus and invest approximately 70% of our capital
budget in domestic resource plays of scale. We are confident in the ability of these plays to deliver double-digit,
sustainable production and reserve growth at attractive returns.
Our 2010 capital budget is aligned with our anticipated 2010 cash flow and is set at $1.6 billion. This level of
investment should allow our production to grow 8-12% over 2009 levels.
We are in the second year of our third decade as a company and our future is bright. We will continue to be good
stewards of capital and will invest in the right projects at the right time.
While we retain the entrepreneurial spirit of our founding, our diverse portfolio of assets today is providing
optionality and flexibility in our investments. We have evolved into a larger, stronger and more diversified
independent exploration and production company.
Our leadership is experienced and works with talented professionals throughout our operating regions. We
enthusiastically share a confidence in our ability to execute on our plans in 2010.
Thank you for your continued interest and investment in our Company.
Lee K. Boothby
President and CEO