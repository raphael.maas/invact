DEAR SHAREHOLDERS,

It is my pleasure to update you after another record year
at Roper Technologies.
In 2015, we overcame several challenges to produce
record results. For the year, we grew our free cash flow
11% and continued to expand margins to record levels.
We grew our revenue despite nearly $200 million in
headwinds from foreign exchange translation and sharp
macroeconomic declines impacting our businesses that
serve oil & gas markets.
At Roper, daily business decisions are made by those
closest to their markets and customers, which enables
swift reactions to changing conditions. Our business
leaders executed nimbly to once again produce a record
year across all meaningful financial metrics. Our assetlight, niche market businesses are built to deliver high
levels of profitability in all economic environments. Last
year presented a great test and we are pleased with the
performance of our business leaders.
At Roper, earnings growth is only one part of the equation.
We consistently generate cash at levels well above our
net earnings as outlined on the previous pages. This high
level of free cash flow conversion, measured at 130%
over the past 5 years, provides Roper with a sustainable
source of cash to fund investments, which create additional shareholder value.
While some companies focus much of their investment
on short-term measures such as stock buybacks, we
remain focused on continued investment for future
growth, both within our existing businesses and through
new acquisitions. We believe the best way to generate
shareholder value is to continually increase our ability
to generate cash flow in the future.
We accelerate our cash flow to deploy capital in acquisitions well above the level of the cash we generate.
The table below provides a snapshot of both our cash
conversion and our capital deployment over the past 

five years. During that period, we generated $2.8 billion
in net income and deployed $4.8 billion in acquisition
investment. This consistent cash conversion and acceleration of cash through capital deployment is the engine
that drives Roper�s ability to consistently compound both
cash flow and shareholder value.
We expect the next five years to show more of the same,
although at much higher levels as we continue to compound our growth.
Cash is the key to Roper�s success.
In 2015, we deployed $1.8 billion in eight exciting acquisitions with continued focus on software, SaaS and niche
product applications. Our acquisitions are detailed on
the table above, organized from largest to smallest.
Each business is a leader in its niche market, provides
high value to its customers and has strong growth prospects for the future. In January 2016, we completed an
additional acquisition, CliniSys, a leading provider of
hospital laboratory software in Europe.
Our acquisition process remains disciplined and rigorous. We cast a wide net and review a large funnel of
potential acquisitions of private companies. Because we
provide a permanent home for great companies, we
offer a highly differentiated set of opportunities for leaders dedicated to growing their businesses over the long
run. We focus on growth measured not just in quarters,
but years and decades.
We evaluate the design of the business model and the
potential for sustainable success. We require high gross
margins, which we view as a key indicator of whether
the business is providing a valuable solution to its customers. We look at the business� structural ability to
generate high levels of cash. Importantly, we focus on
the balance sheet and whether the business is designed
to avoid high levels of working capital and costly fixed
assets. If it is asset heavy, it is not a Roper business.

For those businesses that fit our financial criteria, our
focus shifts to several critical questions that must be
answered during our process:
� Is the business a leader in its niche market?
� Does it have sustainable competitive advantages?
� Does the business create value for the customers
it serves?
� Does it have a long runway to continue its growth?
� Is the management team committed to building
the business?
We pride ourselves on adding only exceptional businesses to the Roper family. Our patient and thorough
diligence process allows us to invest capital wisely,
while compounding our cash flow.
Our new businesses quickly learn more value is created
from localized innovation than centralized control.
Our common tools and metrics enable them to focus
resources to enhance growth.
SIMPLE IDEAS, POWERFUL RESULTS
This important theme first appeared on the cover of
Roper�s Annual Report twelve years ago. Since then, our
compound annual shareholder return has been greater
than 20%.
Our future performance will continue to be driven by
the simple ideas that have transformed the enterprise�
focus on cash return disciplines, seek and maintain
leadership positions in niche markets, generate high
gross margins, develop outstanding operational teams,
deliver compelling cash flow using our asset light business model, deploy capital to drive internal growth, and
continue to acquire new businesses with great growth
prospects. These ideas are deeply embedded in the
Roper culture and you can be confident they will continue to drive our performance for many years to come.
We thank you once again for being a Roper shareholder
and we look forward to another record year in 2016.
Sincerely,

Brian Jellison