LETTER TO SHAREHOLDERS

                                        At CarMax, one of our ongoing beliefs is that the uniqueness of our consumer offer
                                        will help support our performance even in more challenging economic times. Fiscal
                                        2008 was certainly one of those challenging times, as it was for many U.S. businesses.
                                               While we were disappointed that our results fell significantly below our early
                                        expectations for the year, we were pleased nevertheless to report positive compara-
                                        ble store used unit sales of 3%. Our basic belief is that CarMax will outperform our
                                        competitors in most economic environments due to a variety of aspects of our con-
                                        sumer offer. Our data indicates that we continue to gain market share in the late-model
                                        used vehicle market.
                                        Our fiscal 2008 results prove, however, that we are not immune to changes in
                                 economic conditions, both from the standpoint of consumer demand and the impact

                                 of the financial marketplace. The unprecedented and well-publicized turmoil in the
                                 credit markets, especially for asset-backed securities, increased our funding costs, and
reduced the demand for the bonds we sell in the public securitization market. In addition, we modestly increased our
net loss assumptions, primarily due to the stresses of the current economy. As a result, profitability fell at CarMax Auto
Finance (CAF), our finance operation.
       But regardless of short-term funding challenges or periodic profit shortfalls, we remain strongly committed to
CAF. Because we believe that most car buyers require financing, it is vital that CarMax provides a variety of financing
alternatives. CAF continues to be an integral part of our business, offering competitive rates to qualifying customers
that represent more than 40% of our retail unit sales. In fact, it was for just this kind of environment that we originally






conceived of CAF, to continue to protect us and our customers with access to credit when other lenders might con-
sider tightening credit availability for reasons unrelated to our business.
        During fiscal 2008, we once again achieved our annual goal of growing our superstore base by 15%. We opened
12 stores, including stores in new markets for CarMax such as Tucson, Milwaukee, Omaha and San Diego, bringing our
total used car superstores to 89 at year end. In fiscal 2009, we plan to continue investing to support our long-term growth
initiatives. We plan to open 14 superstores, entering 8 new markets and expanding our presence in 5 existing markets.
       While we estimate that we have stores in locations where more than 43% of the U.S. population lives, our aver-
age market share in most of these cities is below the 8-10% we believe we have already achieved in our most mature
markets. We believe our superior business model will continue to drive the market share expansion that we expect to
achieve regardless of the external environment. This, along with expansion into new markets, represents a tremendous
future trajectory of potential growth for CarMax and our shareholders.
       We plan to open two more car-buying centers in fiscal 2009. The five car-buying centers we then will have open
will represent our controlled concept test to learn whether we can achieve a meaningful increase in the percentage of
vehicles sourced directly from consumers, which currently represents more than 50% of the used vehicles we retail.
These cars are generally more profitable and also represent a superior variety of currently popular makes and models.
We will evaluate whether we are making an adequate return on our investment based on the number of incremental
retail and wholesale vehicles we are able to purchase at these centers.
      We also plan to roll out a centralized reconditioning facility near Baltimore, Maryland, in fiscal 2009. In a region
as busy for us as the Baltimore-Washington, D.C. corridor, a centralized facility to recondition vehicles will support our
growth, relieve production congestion and provide further opportunity to increase efficiency in the reconditioning
process. We plan to add reconditioning capacity in the Los Angeles market, as well.


         There are two basic aspects of our business that truly differentiate CarMax -- the skill and dedication of our
  more than 15,000 associates and the strength of our consumer offer. The variety of awards we continue to win attest
  to the uniqueness of the CarMax culture and the engagement of our associates.
         For the fourth year in a row, we were named one of Fortune's "100 Best Companies to Work For," moving this
  year from a ranking in the high 80's into the top 50. Because our ability to attract and retain skilled associates is crit-
  ical to our growth, we are doubly glad to again win this award, which assists us in our recruiting efforts. CarMax also
  ranked No. 1 on Fortune's list of America's Most Admired Companies in the automotive retailing, services industry, was
  awarded the 2008 International Torch Award for Marketplace Excellence by the Council of Better Business Bureaus
  and received the Gallup Great Workplace Award, all further commentaries on our exceptional corporate reputation.
        In closing, let me thank all of our associates for their hard work and dedication, our customers for their
  loyalty and our shareholders for their confidence in CarMax. We look forward to the unique and exciting long-term
  opportunities ahead.
  Sincerely,




  Tom Folliard
  President and Chief Executive Officer
  April 25, 2008
                             