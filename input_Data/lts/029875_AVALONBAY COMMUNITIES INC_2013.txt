THE YEAR IN REVIEW
2013 was a record-setting year for AvalonBay. We delivered
another year of exceptional operating results, completed
the largest acquisition in Company history, expanded
development to an all-time high and raised a record amount
of capital. For the year, adjusted for non-routine items, we
delivered sector-leading FFO per Share growth totaling
nearly 15%. Over the last three years, we have increased FFO
per Share, adjusted for non-routine items, by 57%, well above
the sector average. This growth has been driven by accretive
investment activity and strong portfolio performance, with
same-store NOI up 22% cumulatively over the last three years.
In February, we closed on the $6.5 billion acquisition of the
Archstone portfolio. This was a rare opportunity to acquire
a high-quality portfolio concentrated in our markets. We
increased brand penetration, largely met our geographic
portfolio allocation objectives, and enhanced G&A and
operating synergies at the corporate and property levels
while preserving the strength of our balance sheet, liquidity
and financial flexibility.
We continued to invest aggressively in new development
in 2013, starting 13 new communities representing a
projected Total Capital Cost of $1.3 billion. We completed
the development of 12 communities containing nearly 2,900
apartment homes, on schedule and below budget. These
completed communities represent a total capital investment
of approximately $630 million and are projected to produce
a weighted average initial stabilized yield of approximately
7.3%, generating value creation of approximately $260
million(4) based on current market capitalization rates. By
year end, total development under construction stood at
$2.8 billion. In addition, we acquired new development
rights, mostly in the form of land options, which provide
us with the opportunity to invest an estimated $2 billion
in future development activity and will support sustained
future earnings growth.
We raised a record level of attractively priced capital to fund
this investment activity. For the Archstone acquisition, we
raised $2.1 billion in equity and $250 million of long-term
unsecured debt in late 2012 after announcing the acquisition.
In February 2013, we assumed $2.0 billion of indebtedness
and issued $1.9 billion of additional equity to the seller when
we completed the acquisition. To fund our ongoing business,
including our development activity, we raised another $2
billion of capital, composed of $1.3 billion from the sale of
existing apartment communities at an average cap rate of less
than 5%, and $750 million of long-term unsecured debt at
an average interest rate of less than 4%. After a record year of
raising and deploying capital, our balance sheet and liquidity
remains the strongest in the sector, with net debt-to-EBITDA
running at less than 6x and with more than $1.5 billion
of cash and undrawn capacity under our corporate line of
credit. This financial strength and flexibility, when combined
with our sector-leading development capability, provides
AvalonBay with an unmatched opportunity in our sector
to continue building lasting value through a developmentfocused
investment strategy in one of the healthiest
apartment cycles in several decades.
LOOKIN G BACK
Over the past year, the Company celebrated several
important milestones: the 20th anniversaries of the
Initial Public Offerings (IPOs) of Avalon and Bay, and
the 15th anniversary of the two companies joining forces
to form AvalonBay.
It is remarkable to reflect back on the achievements of
the past 20 years. At the combined IPOs of Avalon and
Bay, the companies owned 32 communities with just over
9,000 apartments and had an Enterprise Value of less than
$1 billion. Today, AvalonBay owns or holds an interest
in 273 apartment communities, containing over 81,000
apartment homes, with an Enterprise Value of approximately
$23 billion. At the time of Avalons and Bays IPOs, each
companys portfolio consisted largely of wood-frame,
suburban garden apartments. Today, AvalonBays portfolio
is remarkably diverse, containing a mix of garden, mid-rise,
high-rise and mixed-use communities, located in suburban,
urban and transit-oriented submarkets.

In 1994, Avalon and Bay were led by entrepreneurial
executives who had a vision of creating an evergreen
apartment company, united by a common purpose and set
of values, which could become a leader in the modern REIT
era. Early on, our founders established a culture emphasizing
excellence, collaboration, continuous improvement and a
relentless focus on hiring and growing talent.
AvalonBay today is largely a fulfillment of
that visionan apartment company that
has successfully transitioned from one
generation of leaders to another, all while
maintaining our position as a leading
apartment REIT serving the most attractive
customer segments in the best performing
markets. In addition to having best-in-class
capabilities in the areas of development,
construction and operations, we are a fully
integrated company with important strategic
capabilities in capital management, market
research, customer insight and design. Our
platform includes a team of sophisticated
professionals, processes and technologies that
support hiring and retaining the best talent,
delivering exceptional customer experiences,
and operating a large and diverse portfolio.
It is with tremendous pride that we reflect on all we have
accomplished as a public company. While our strategy
continues to evolve with the changing landscape, in the end,
our results speak for themselves. Over the last 20 years, weve
consistently delivered extraordinary returns to shareholders.
Weve provided a workplace where our associates are highly
engaged and motivated, and weve built communities that our
residents are proud to call home.
LOOKIN G AHEAD
We are just as optimistic about AvalonBays prospects in
2014 and beyond, with healthy fundamentals and a strong
competitive position providing a stage for continued
outperformance.
U.S. economic conditions appear stronger than at any time
in the last five years. Fiscal headwinds are fading and U.S.
corporations are reporting record profits. Lending institutions
are liquid and well-capitalized. U.S. household debt burdens
are lower than they have been in nearly 30 years, and
business sentiment and consumer confidence are improving.
These favorable conditions are projected to support apartment
market fundamentals through stronger job and income growth
in our markets, which are forecasted to grow by 1.6% and 5%
in 2014, respectively, and accelerating household formation,
forecasted to increase by 1.4 million this year and by 9 million
by the end of the decade (NABE, Moodys).
Rental demand is expected to be further bolstered by
favorable demographics. Our prime renter cohort, those
aged 2534, is projected to increase by 1.2 million in the next
two years and by nearly 4 million by the end of the decade.
Apartment fundamentals should also benefit from the forsale
housing industrys challenge in ramping up production
to meet household growth over the next few
years. Housing starts contracted significantly
in the downturn, averaging 50% of the postwar
average, as home builders substantially
reduced investments in land, entitlements
and infrastructure, and production capacity
has not returned to pre-recession levels.
Today, most of the excess housing inventory
from the mid/late 2000s has been absorbed.
With 930,000 total housing starts in 2013
and 1.5 million households expected to
be formed, on average, in 2014 and 2015,
housing production would have to increase
by 60% to meet household demand (BOC,
NABE, Moodys).
For us, improving fundamentals should
support solid cash flow growth from our
stabilized portfolio and provide an attractive
environment for our 5,000 new apartment home deliveries in
2014. Overall, we remain extremely well positioned given our
coastal market focus, attractive pipeline of new opportunities
and robust organizational capabilities that allow us to create
value by building in strong market conditions. Given this
outlook, in January of this year we announced a dividend
increase of over 8%, continuing a 20-year tradition of strong
dividend growth that has averaged over 5% on an annual
compounded basis.
2014 is set to be another exceptional year for AvalonBay as
we deliver a record amount of new product to the market
and continue a tradition of Building Strong. I am extremely
proud of our accomplishments in 2013 and want to thank
our shareholders for their support, our associates for their
dedication and our residents for choosing AvalonBay.
Finally, I wish to thank our Chief Financial Officer, Tom
Sargeant, who will be retiring at the end of May 2014. Tom
has worked with AvalonBay and its predecessors for 28 years,
including the last 19 years as CFO. Tom is one of the most
highly regarded CFOs in our industry and has played an
influential leadership role throughout AvalonBays history.
We are grateful for Toms contribution and wish him the best
in his retirement.
Timothy J. Naughton
Chairman and CEO