To Our Shareholders
The impact of current economic conditions is being felt by companies across the
globe. Grainger is no exception. Yet amidst the pressures of the recession and
weakened sales, we see this climate as a great opportunity to strengthen the value
we bring our customers, communities and shareholders.
When things get tough, strong companies shine. We are faring better than many,
especially other industrial suppliers. Grainger is a solid company. Our strong balance
sheet, low level of debt and healthy cash generation are clear strengths in todays
environment. We have kept inventory levels up, maintained our service levels and have
returned cash to shareholders in good times and bad. We take the long view, in
bringing value to customers and focusing on what we can controlas we did in 2008.
2008 PERFORMANCE
Grainger produced record results in 2008, despite a tough fourth quarter as the
economy weakened.
For the year, the company posted $6.9 billion in sales, up 7percent from 2007.
Net earnings of $475 million were up 13 percent. Earnings per share of $6.04
were up 22 percent year over year.
As a continued sign of the companys commitment to increasing shareholder
value, Graingers directors voted last April to raise the quarterly dividend more
than 14 percent, marking the 37th consecutive year of dividend increases. The
company also returned more than $394 million in cash to shareholders in the
form of share repurchases.
The years excellent performance is due to a number of factors. None ranks
more important than serving our nearly 2 million customers exceptionally well.
Recent customer-satisfaction scores, which evaluate our customers last interaction
with us, are the companys highest ever.
A CLOSER LOOK AT THE YEAR
In the United States, we continued to benefit from our investments to expand
in the major metropolitan markets, increase our product offering and operate
more productively.
We announced that we would merge Grainger Industrial Supply and Lab
Safety Supply. Combining these units should benefit the Lab Safety brand, which
saw sales decline in 2008. We will stock Lab Safety products across the U.S.
Supply Chain to substantially improve service to Lab Safety customers coast to
coast. Most of Labs product offering will now be available to all Grainger customers
through our Web site and sales force, helping strengthen Graingers overall position
in many product categories. We expect this consolidation will also yield purchasing
synergies and back-office efficiencies.
Our Canadian business, AcklandsGrainger, posted strong sales growth
and increased profitability. AcklandsGrainger, the second-largest business unit,
performed especially well in agriculture, energy and government.
In Mexico, we reached our goal of being the countrys first national distributor
of MRO supplies by opening seven additional branches. In China, we focused on
achieving greater market penetration in and around Shanghai. Also in 2008, we
extended our international reach by opening Graingers first branch in Panama.
Other successes included e-commerce sales, which rose by13% to $1.5 billion.
Graingers U.S. customers who order online buy more products and purchase more
often. More than 90 percent of orders placed on grainger.com are shipped, the
companys most efficient form of fulfillment.
Productivity gains, the result of continuous improvement in our operations, and
discipline in cost management also aided overall results. By controlling operating
costs and expanding gross margins, we added almost 100 basis points to the years
operating margin.
Throughout the year, employees across the company reinforced Graingers
reputation as a neighbor in their community by helping victims of floods, earthquakes
and hurricanes; supporting technical education; and contributing to countless other
important causes.
In 2008, as good corporate citizens, we also focused on sustainability. We
renewed our commitment to greening our facilities and partnered with customers
by helping them with the green solutions they need.
As the nation rebuilds itself by repairing its infrastructure, the shortage of skilled
labor will become an issue. Grainger and Mike Rowe, creator and host of Discovery
Channels Dirty Jobs, have teamed up to encourage more young people to consider
careers in the trades.
ASSESSING GRAINGER TODAY
The companys long-term prospects remain promising for several reasons.
We have a diverse customer and geographic base. We do business with
customers in virtually every industry and locale in North America. That kind of
unmatched diversity is a big advantage in a down economy because the companys
revenues are less tied to the performance of a single sector or region.
Additionally, Graingers broad product line and steady investment in sales
coverage, branches and inventory allow us to get customers the products they
need, when they need them. Customers, in turn, can improve their cash flow by
eliminating inventory when they rely on Grainger to have what they need.
Another strength is our reputation for being there for customers when they
need help the most. Whether its a natural disaster or the tough economy, we
offer ways to help them get back up and running and to operate more efficiently.
IN SUMMARY
Were in a solid financial and operating position not only to ride out this uncertain
time but also to gain market share and emerge even stronger when the economic
climate brightens.
We have the best products and services, the most respected brand, the best
supply chain and, most importantly, the best people.
We are confident that we will emerge from this period in good shape. We are
seizing the opportunity to gain more market share.
To our employees, we want to say thanks for staying focused on whats
important: serving our customers with distinction and with a human touch. There is
no finer group of people anywhere.
Were also grateful for the loyalty of shareholders like you.
Sincerely,
Richard L. Keyser James T. Ryan
Chairman of the Board President and Chief Executive Officer
February 26, 2009