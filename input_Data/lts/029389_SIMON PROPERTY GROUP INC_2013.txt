Dear Fellow Stockholders,
First and foremost, if I may, let me start by thanking my
colleagues for delivering a record-breaking year to you, our
stockholders. From our maintenance and management staff
to our leasing and development personnel at headquarters
and all in between, everyone did their part in delivering these
impressive results. The quality of the people at Simon Property
Group (SPG) and, of course, our properties combined to
generate record-setting results. Our funds from operations
(FFO) increased from a strong 2012 by 10.9% to $8.85 per
share. Since the great recession, the growth per share of FFO
has totaled 76%. We increased our dividend for 2013 by 13.4%
to a total of $4.65 per common share, an increase of 79% since
the great recession, and with the recent increase in the first
quarter of 2014 to $1.25 per share we are now on track to pay
$5.00 per share this year. Our FFO per share of $8.85 is also a
record for the REIT industry. Once again, I am so proud of how
the company has delivered financial and operational results
coming out of the great recession, as well as over our 20-year
history as a public company which we quietly celebrated at the
the end of last year.

Now if I can, lets briefly look back over the last 20 years. Our
company has achieved growth and scale that few could have
imagined possible: our FFO has grown from $150 million in
1993 to $3.2 billion in 2013, total consolidated revenues have
increased more than twelve times from $424 million to
$5.2 billion, and the market value of our portfolio increased
from $3 billion to $85 billion. In addition to adding quality real
estate to our portfolio, weve added new markets and new
product types, while always sticking with what we know best:
retail real estate. Across 20 years of history, our commitment
to business growth that generates shareholder value has
remained our top priority. From our IPO in December of
1993 through year-end 2013, ownership of common stock
of Simon Property Group (SPG) provided a total return to
stockholders of 1,915%. If I may quote Adam Sandler as
Mr. Longfellow Deeds, thats not too shabby.
Our core strengths of capital allocation, balance sheet
management, and operating expertise continue to fuel
our growth, making it possible to deliver our impressive
financial results. We continually set the bar higher as
property owners and as stewards of capital. This is not
new for us, and we will always be looking for additional
opportunities to deliver results.
We measure our success through growth of our operating
income, and I am pleased to report that we continued
this positive trend for our twentieth year. Our share of net
operating income (NOI) in 2013 increased 10% to more than
$4.8 billion. By comparison, our share of NOI in 1993 was
$296 million. It would be easy to rest on our laurels and take
a breath and not find new and exciting ways to improve
our product and ultimately our cash flow, but our culture
wont allow that nor will our people. So that was the last
20 years, and now we are focusing on the future.
Active management, smart acquisitions, and continued
investment in our properties will continue. And let me
reiterate what I said in my 2008 shareholder letter paraphrasing
Mark Twain, The death of the mall is greatly exaggerated.
The proof as they say is in the pudding. I respectfully ask you
to review our results and hopefully you will come to the same
conclusion. As I look back on 2013, there was little debate
on this for the first six months of the year as our retailers
continued to post sales increases. However, as the year went
on and the weaker than expected holiday shopping season
transpired, it became the topic du jour. Of course, I was
disappointed in overall retail sales, but I dont attribute it to
a paradigm shift away from quality retail real estate. Instead,
I believe we had a perfect storm (literally and figuratively)
that led to anemic sales for our retailers (not SPG), including
a short holiday selling season compared to 2012, a shift
to durable goods that was cyclical in nature, an increase in
income taxes, and general economic and political uncertainty.
However temporary this slowdown in the fourth quarter and
the early part of 2014 is, we will react as if it is not and will
continue to improve our customer experience while driving
traffic to our shopping centers.
Before we turn to 2013 highlights, lets focus on our stock
performance for 2013. Clearly we were disappointed the
stock was relatively flat given the S&P 500 had such a strong
year. This is a rarity for us. In fact, we have outperformed the
S&P 500 12 times in the last 14 years. Also, given the strength
of our earnings it is not something we would have anticipated.
However, it happens and we wont waiver from our strategy
that has produced industry-leading results year after year.
We can attribute last years stock price performance (or lack
thereof) to sector rotation and investor concern over the
impact of potentially rising interest rates that overlooked our
strong fundamentals. Fortunately, your stock has increased
this year to date.
Now lets turn to 2013 highlights.
2013 FINANCIAL AND OPERATIONAL HIGHLIGHTS
We reached new heights in 2013 for consolidated
revenue and FFO:
 Consolidated revenue increased 5.9% to $5.170 billion.
 Net income was $1.316 billion.
 FFO increased to $3.206 billion, or $8.85 per diluted share,
a year over year increase of 10.9%.
Our U.S. Malls and Premium Outlets once again delivered
strong financial and operational results:
 Comparable property NOI growth was 5.2%.
 Total sales on a rolling 12 month basis increased by
2.5% to $582 per square foot.
 Occupancy improved by 80 basis points to 96.1%.
 The releasing spread for the rolling 12 months was
$8.94 per square foot  rent for spaces leased in 2013 was
16.8% higher than prior rent paid for the same spaces.

2013 NEW INVESTMENTS
We have a strong investment track record of making smart,
accretive, value-enhancing acquisitions to diversify and
increase the quality of our portfolio, which takes advantage
of our scale, and allows us to enter new markets. In 2013 we
invested approximately $1.05 billion in strategic acquisitions
and joint ventures, enhancing our domestic footprint and
extending our international asset base on an accretive basis.
McArthurGlen
In June 2013, we announced a joint venture with the
McArthurGlen Group, the leader in upscale European
designer outlets, for an investment of approximately
$500 million. We are excited about this partnership and its
contribution to the global footprint of our Premium Outlet
portfolio. Europe, now combined with North America and
Asia, gives us an unparalleled global platform for our retailers
and consumers in the outlet sector. We are also pleased
to expand our interests within the European market, which
we believe has positive long-term growth prospects for
high-quality retail real estate and where we have entered
at a cyclical trough. With all elements of the joint venture
transaction closed in October, we now have an ownership
interest in six assets, including five existing designer outlet
properties located in Austria, Italy, the Netherlands, and the
United Kingdom, as well as one property under construction
in Vancouver, Canada expected to open in spring 2015.
We have also become an equal partner in McArthurGlens
property management and development company, which
benefits from an established pipeline of future expansion and
new development opportunities.
Other positive changes to our property portfolio in 2013
include the acquisition of Woodburn Premium Outlets, a
productive 390,000 square foot center located near Portland,
Oregon, that is a strong addition to our Premium Outlets
business. We also disposed of our interests in 14 non-core
retail assets during the year, showing our commitment to
active management of our property portfolio in order to
recycle capital and prioritize managements focus.
Klpierre
Our 2012 investment in Klpierre continues to grow in value
with the companys progress in redirecting its strategy and its
continued solid performance. As Chairman of the Supervisory
Board, I am pleased that operating efficiency has improved
and the repositioning of their property portfolio through
divestment of office properties and smaller retail centers
has accelerated. While retail sales in its key markets were
flat through 2013, the business has stabilized and Klpierre
is positioned for growth.

The market has responded positively to Klpierres new
strategic direction, as well as its execution since we made our
investment. We bought our Klpierre stake at 28.00 per share
and the stock closed at 33.68 on December 31. Including
dividends and currency impact, we have realized a 37% return
on our investment in the first 21 months of ownership.
2013 OPENINGS
New Development
In 2013, we opened a record number of five new Premium
Outlets in North America and Asia, a positive reflection of
the growth opportunity, capacity and reach of the Premium
Outlets platform.
In Japan, Shisui Premium Outlets opened to visitors in April.
The first phase of this project, located near Tokyos Narita
International Airport, has 120 stores in 235,000 square feet and
features a mix of international and Japanese brands, as well as
restaurants with an expansion of 131,000 square feet about to
commence. Our third center in Korea, Busan Premium Outlets,
opened in August. The property features 180 stores and
360,000 square feet of retail space and is located just north of
Busan, the countrys second largest city, on the Korean Peninsula.
In North America, Phoenix Premium Outlets, located in
Chandler, Arizona adjacent to the Wild Horse Pass Hotel
& Casino, opened its first phase in April with 90 stores in
356,000 square feet. Toronto Premium Outlets, Canadas
first Premium Outlet Center, opened in August with 90 stores
in 358,000 square feet. Also in August, we opened St. Louis
Premium Outlets, with 90 stores in 351,000 square feet
serving the greater St. Louis area.
A busy but very satisfying year for us on the new development
front and happily all are performing above our expectations.
Redevelopment
We continue to invest in many of our top properties to meet
consumer demand for more popular brands, dining and
entertainment experiences.
Malls: Notable mall projects include a complete redevelopment
for the New York metropolitan area and Northern New Jersey
area with The Shops at Nanuet, a 750,000 square foot open-air,
state-of-the-art main street center providing customers with
shopping, entertainment, dining and fitness opportunities.
The Shops demonstrate our ability to redevelop a troubled
shopping center into a vibrant and exciting destination on an
accretive basis. We also opened a variety of dining establishments
as part of the redevelopment of Walt Whitman Shops, which
opened in November and included the addition of 74,000
square feet of exterior-facing shops.

Significant redevelopment projects were started in 2013 at
a number of our top properties. An expansion at Roosevelt
Field in Garden City, New York will accommodate the addition
of Neiman Marcus and a number of other luxury retailers and
upscale restaurants; the redevelopment of Del Amo Fashion
Center in Torrance, California, will add Nordstrom and provide
a more upscale environment for this well-located property,
starting with a collection of patio cafes opened early in 2014.
At Houston Galleria, Saks will relocate with a new flagship
format to open in 2016, with other luxury retailer additions to
follow. We expect these initiatives and other similar projects
to further enhance the productivity and appeal of our centers
in major markets and tourist destinations.
Premium Outlets: Multiple expansions were completed within
the Premium Outlets global portfolio. In Korea, a 96,000
square foot expansion of Paju Premium Outlets opened in
May. In Seattle, more than 100,000 square feet of additional
retail space were completed in June. Late in 2013, the
expansion of Orlando Premium Outlets-Vineland Ave was
completed, adding 105,000 square feet of retail space.
The second phase of Johor Premium Outlets in Malaysia was
also completed, adding 40 new stores and 90,000 square
feet to meet demand for this popular center near Singapore,
attracting visitors from around the region. An expansion of
Desert Hills Premium Outlets was in progress throughout
2013, and is opening on April 24th of this year. We also
began a multi-year redevelopment of the iconic Woodbury
Common Premium Outlets that expands the available retail
space, provides a new upgraded dining pavilion, adds more
parking and reorganizes the center into five unique districts
that correspond to popular New York tourist destinations
such as the Adirondacks and the Hamptons. Expansion is also
underway at Las Vegas Premium Outlets-North, a leading
tourist destination for luxury outlet shopping. Very busy indeed.
The Mills: We continued to enhance The Mills portfolio through
redevelopment projects and expansions, such as the addition
of Macys to Gurnee Mills in Gurnee (Chicago), Illinois and the
expansion of Sawgrass Mills in Sunrise (Miami), Florida, including
an addition of 36,000 square feet to The Colonnade  the luxury
outdoor component of the leading shopping center in Florida.
At the end of 2013, we had redevelopment and expansion
projects underway at 25 properties in the U.S., Asia and
Mexico at a cost of approximately $1.1 billion and expect
this rate of activity to continue for the next few years. As this
pipeline is brought on it will continue to fuel our growth.
2013 BALANCE SHEET ACTIVITY
Balance sheet management is a fundamental strength of
our company. We continue to have the strongest balance
sheet in the industry and access to capital that allows us to
pursue our growth strategy. We have the highest investment
grade ratings among U.S. retail real estate companies, and
we remain one of only two U.S. REITs with A ratings from
Moodys Investors Service and Standard & Poors.
We successfully executed our first euro-denominated debt
offering of 750 million of seven-year senior unsecured notes
at 2.375%. Not only did this open another source of capital
to us, but it also provides a natural hedge to our European

investment. We were also active in the secured debt markets
throughout the year, and closed or locked rates on 30 new
mortgages totaling approximately $5.0 billion, of which our share
is $3.0 billion. The weighted average interest rate on these new
loans is 3.31%, and the weighted average term is 7.5 years.
We have reduced the weighted average interest rate of our debt
from 5.46% at the end of 2009 to 4.78% at the end of 2013.
Over this same period of time, the weighted average years
to maturity was extended from 4.2 years to 5.5 years. Our
refinancing plans will continue to be aggressive, and we are
confident in our ability to continue to lower our cost of capital.
2014 FOCUS
As we continue to set the bar higher as property owners and
stewards of capital, we remain laser-focused on optimizing
results across our global portfolio. In 2014, this will include
our planned spin-off of the strip center business and smaller
enclosed malls into an independent, publicly-traded REIT.
Washington Prime Group (WPG) will be led by Mark Ordan as
CEO who has a terrific track record of creating shareholder value.
We believe this will create a retail real estate company poised
to capitalize on its unique growth opportunities while allowing
SPG to focus on its global portfolio of larger malls, Premium
Outlets and Mills. The spin-off will focus on many of the principles
that made SPG what it is today including cash flow growth and
increasing returns on equity. As a future shareholder and director
of WPG, I expect the company to prosper. Rick Sokolov, as
Chairman of WPG, and I will ensure a smooth and successful
transition. Within the SPG portfolio, we will continue to invest
in new development, redevelopment and expansion projects that
meet our return expectations and will generate future growth.
New Development
In 2014, we expect three new properties to be completed in
North America: Charlotte Premium Outlets in Charlotte, North
Carolina, Twin Cities Premium Outlets in Eagan (Minneapolis-
St. Paul), Minnesota and Montreal Premium Outlets in Mirabel
(Montreal), Quebec, Canada. All three of these properties are
addressing unmet consumer demand for top brands at value
pricing in dynamic, growing markets. We will also continue to
pursue selective new development opportunities within our
Premium Outlet pipeline in the U.S. and abroad.
Redevelopment and Expansion
Our commitment to investing to enhance our top properties
remains unabated. Our redevelopment pipeline remains
robust across all of our property platforms, and competition
for capital at SPG is decided in favor of meeting our expected
return hurdles and strategic advancements of our properties

Branding
In the second half of last year, we started a rebranding project
that will go live during the second quarter of 2014. Our aim is to
create a truly world class brand for Simon that evokes the high
quality of our retail real estate. We believe a strong brand will
enhance our competitive advantage and create opportunities
to engage directly with our consumers. The new brand approach
will create a unified presence across all of our platforms that
promotes fashion, discovery and community  key elements
defining who we are as a company. This report provides a glimpse
of our new look and feel. We will launch with a coordinated
multimedia campaign across major fashion publications and
through our corporate website and digital marketing mediums.
Focus on the Consumer
Visiting the mall is becoming more than just a shopping
experience. Malls are increasingly a destination for family
and friends, for entertainment and for community, and we
are pushing ourselves on how we deliver this comprehensive
experience to meet the evolving needs of our customers.
You see it in the significant upgrades to our dining experiences
including the addition of healthy choices. It is reflected in the
growth of quality childrens play areas, new areas of soft seating,
charging stations, Wi-Fi, and other must-have conveniences.
We are increasing our emphasis on the level of service and
engagement with our consumers, and providing new ways
to make their shopping more enjoyable, convenient, and
efficient  whatever their needs may be. Continual product
improvement is a vital attribute for all successful companies.
We are no different. This will always be a priority for us.
All Things Digital
Contrary to a belief held by some, malls are not becoming
obsolete. If anything, they are becoming more fully integrated
into our communities by serving the multiple needs of our
consumers whether they are shopping, dining, exercising,
or going to be entertained. While online buying presents a
competitive option to our retailers stores, that very same
technology is complementary to our business with more and
more retailers incorporating it as an integral part of their in-store
customer experience. We are also actively supporting our
centers through multiple forms of digital engagement with the
consumer, ranging from providing directions through the Simon
Malls smartphone app, to frequent social media interactions,
to special offers direct to consumers through our online
retailer showcase, to investments in emerging technologies
and services such as Deliv, a same-day delivery service that
leverages the malls proximity to its customer base, or Jifiti,
an application that makes gifting easier. Our experimentation
in this area will continue, and will result in a better experience
through the creation of the mall of the future.
DIVIDENDS
Our policy in 2013 and in the future is to pay dividends to our
stockholders equal to taxable income. Given our expected
growth in taxable income, we expect to continue to grow our
dividends. As I mentioned before, common stock dividends
paid in 2013 were $4.65 per share, an increase of 13.4% from
2012, and we expect to distribute at least $5.00 per share
in 2014, not including dividends expected to be paid by our
spin-off company, WPG.
OUR STRATEGY
Early on, we developed a long-term strategy which continues
to serve us and our stockholders well:
 Focus on the ownership of high-quality retail real estate
 Increase our presence in major metropolitan markets
 Own assets along the price spectrum of retail real estate
 Lead the industry in successful and profitable acquisitions
 Lead the industry in promoting the Mall as a Marketing
Medium and connecting with our community and consumer
 Export our know how internationally
We remain committed to this strategy as the best path to
achieving continued business success and consistently
strong financial performance, but we are flexible if the
environment requires change.
I look forward to another excellent year in 2014. With the hard
work of my colleagues at Simon Property Group we anticipate
continuing to deliver the kind of results you, our stockholders,
have come to expect from us. I also want to thank our Board
for their service, duty and loyalty, and our stockholders for
your continued support. Please take the time to visit any of
our properties (and shop!) and, as always, I welcome your
comments and feedback.
David Simon
Chairman and Chief Executive Officer
March 31, 2014