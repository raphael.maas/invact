Dear Fellow Shareholders,
This was an extraordinary year for STERIS. As a result of the collective contributions of our people, our
business grew both organically and through strategic acquisitions to deliver 21% growth in revenue and record
adjusted earnings per share of $3.39.
Topping the year�s achievements was the landmark completion of the Synergy Health combination in addition
to closing two other strategic acquisitions, General Econopak and Black Diamond. At the same time, we grew
organically as a result of our continued investment in product development, and manufacturing and service
operations, that enable us to bring improved products and services to our Customers and the people whose
health and safety they improve.
Full year 2016 organic revenue growth was 5%, with growth in all four segments. In particular, our IMS
business, which is the legacy STERIS component of our new Healthcare Specialty Services segment, delivered
double digit growth even as the multiple companies that form IMS settled into new, combined sales territories.
We also saw solid, mid-single-digit organic revenue growth in our Life Sciences and Applied Sterilization
Technologies (AST) segments, which I will review in more detail later on. Our Healthcare Products segment�s
organic revenue grew 3% for the full year, with strong growth in the United States offsetting softness outside of
the U.S.
Synergy was a meaningful contributor to our overall growth as well, growing revenue 4% and profit 9% in
constant currency for the full year 2016. As is always the case, some parts of the business are doing better
than others. We are particularly pleased with the strength of the AST portion of the company, and believe the
Hospital Sterilization Services (HSS) business in the United Kingdom and Europe have good opportunity for
continued growth and profitability. As we have said all along, the U.S. HSS business is a nascent business
opportunity that we believe could be significant in the long run, but will take time and investment to develop.
While we continue to engage in conversations with Customers in the U.S. about potential outsourcing
opportunities, we believe there is still substantial lead time before significant new contracts will materially
impact STERIS.
We have generated approximately $5 million of cost synergies in fiscal 2016 related to the Synergy combination
and continue to expect that we will save about $15 million more in fiscal 2017, and an additional $20 million
thereafter. We will incur some additional cash expenses during fiscal 2017 to obtain those overall synergies.
This is all consistent with our original Synergy plan, except for the extended time it took to close the deal.
Diving into the segments a bit further:
Healthcare Products grew 6% for the year, with contributions from Black Diamond and Synergy Health and
low-single-digit organic revenue growth. Capital equipment in healthcare products increased low-single-digits
for the year, driven by double-digit growth in the U.S. which was offset by declines in all other regions. Newer
capital products that contributed to the year include a new AMSCO washer line, Harmony lights and booms,
the Orthovision table and V-PRO 60.
Healthcare Products consumable revenue climbed low-double-digits, with organic revenue making up more
than half of that growth. We saw healthy increases for consumables in the major geographic regions other
than the EMEA. The Middle East in particular reflects lower revenue due to the current macroeconomic issues
in the region and follows particularly strong consumable orders in the prior year. Our instrument cleaning
chemistries, V-PRO dedicated chemistries and new products for US Endoscopy, all saw solid improvements
this year. Service revenue grew mid-single digits driven by strength in the U.S. and Latin America.
The Healthcare Specialty Services segment grew 70% in the year with strong organic revenue growth
bolstered by the addition of the two businesses from Synergy, Hospital Sterilization Services and Linen
Management Services. As mentioned earlier, our strong organic revenue growth was produced by IMS, our
instrument repair business. IMS�s double-digit revenue growth was fueled by several large contract wins as
well as our ability to capitalize on shorter-term engagements that arose during the year.
Our Life Sciences team had an outstanding year with 18% revenue growth, about one-third of which was
organic. We completed the purchase of General Econopak last summer. That has been a strategic addition to
the STERIS portfolio which capitalized on our strong, global field support for our pharmaceutical Customers.
We believe the addition of these product lines strengthen our overall position around the world. Even in the
face of economic headwinds outside the U.S., all three Life Sciences product areas: capital equipment, 
consumables, and service experienced organic growth last year. All major Life Sciences geographic regions
grew as well. Operating margins for Life Sciences continued to expand as a result of the volume increases and
favorable product mix.
Applied Sterilization Technologies (AST) grew 51% for the year, with 7% organic revenue growth and the
previously discussed contribution from Synergy. The integration of Synergy and STERIS people is complete
across our global platform. Combined, we have a network of 59 facilities in 16 countries strategically located
around the world. Our Customers rely on us to sterilize over 1 billion medical products each year.
We continue to be excited about the opportunities ahead of this combined organization. If anything, we feel
that we may have been conservative in our original thinking about how well we will be able to serve our global
Customers. We are working on a number of facility expansions - both in the U.S. and in Europe - that will
facilitate our ability to meet anticipated Customer demand, and will impact our growth in the coming years.
Switching gears to total Company profitability, our adjusted operating profit improved 30% year-over-year due
to the inclusion of our new businesses, our organic volume growth, favorable foreign currency and our cost
reduction efforts. We did have increased interest expense and a higher share count impacting earnings per
share for the year, some of which was offset by a lower tax rate.
All-in-all, we are quite pleased with our organic achievements as well as the strategic businesses that joined
STERIS. The combination of the two allowed us to post another year of record results. More importantly, it
provides a springboard for an anticipated fifth consecutive year of record performance in fiscal 2017.
Free cash flow exceeded our expectations, ending the year at $129 million. This was driven by lower capital
spending and improved working capital. Fiscal 2016 was an unusual year for free cash flow, as we had almost
$100 million in cash expenses due to acquisitions and the elimination of our U.S. pension liabilities, which
reduced our free cash for the year. We no longer have any defined benefit pension exposure in the U.S.
We ended the year with a solid balance sheet, having secured favorable refinancing of our debt in conjunction
with closing the Synergy Health deal. Our leverage is higher than it has been in the past, but well within
comfortable ranges. We remain committed to our capital allocation priorities; maintaining and growing our
dividend, investing for organic growth in our current businesses, targeting acquisitions in adjacent product and
market areas, reducing our total Company leverage, and finally, share repurchases if the other uses of cash are
lower than our desires and do not offset dilution. And speaking of dividends, we increased our dividend for the
10th consecutive year.
We have made meaningful progress in achieving our strategic goals over the past several years, and it is
remarkable to look back at all our people have achieved in just this past year. We have been and will continue to
look across our business portfolio for opportunities to continue to optimize our products and services.
In closing, I would like to thank each of our STERIS team members around the world. Your unique contributions
made this a truly memorable year � one that is paving the way for STERIS�s continued success.
I would also like to thank our Board of Directors for their counsel during a particularly challenging year, and
would like to welcome our new board members from Synergy Health.
I am honored to have the opportunity to lead your Company, and appreciate your ongoing support. I believe the
future for STERIS is bright, indeed.
Until next year,
Walt Rosebrough
President and Chief Executive Officer
June 2016