Letter From the Chairman
Our performance in 2004 was nothing less than
impressive. Net income was up more than 35 percent,
sales reached a record high and we enjoyed volume
growth in all regions of the world. Our coatings segment
generated record sales and earnings. Our flat glass
and chlor-alkali plants, meanwhile, saw their operating
rates climb throughout the year. And we generated more
than $1 billion in cash from operations.
Our success in 2004 is a direct result of our actions to
unlock the vast potential of PPG and its people, enabling
us to make the most of the improved global economy.
And the proof is in our results.
We recorded net income of $683 million, or $3.95 a share,
on sales of $9.5 billion. Our results benefited from higher volumes
in all our businesses, higher selling prices in our chloralkali
business and strong manufacturing efficiencies, producing
higher operating margins despite significantly higher raw material
and energy costs. In 2003 we recorded net income of $494
million, or $2.89 a share, on sales of $8.8 billion.
CASH FLOW, COST REDUCTIONS
REMAIN COMPANY HALLMARKS
Once again in 2004 we delivered strong cash flow, and once
again we used our cash position to benefit shareholders.
We repurchased $100 million of our shares in 2004, and
announced plans in early 2005 to purchase up to $500 million of
shares by year-end. Since the mid-1980s, we have reduced outstanding
shares by more than a third.
Also in 2004 we increased the dividend payment, now at
an annual rate of $1.80. This marked the 33rd consecutive year
of increased shareholder payments.
Other uses of cash in 2004 included the prudent funding
of our businesses, with capital spending, including
acquisitions, totaling about $300 million. Acquisitions in
optical products and architectural and industrial coatings
continue to interest us, though we do not anticipate any large
transactions in the near term.
Also in 2004 we reduced debt by more than $300 million,
lowering our debt-to-total capital ratio to 27 percent from
36 percent in 2003. In addition, we made voluntary contributions
to our pension plans in 2004. Given all these
payments, we still ended the year with $700 million of cash
on hand.
An important and continuing source of cash at PPG is
our relentless quest to reduce costs. In 2004 we generated
more than $150 million in manufacturing efficiencies, which
was a contributing factor in our 10-percent improvement in
productivity. During the past six years, manufacturing
efficiencies have reduced costs by more than $500 million,
driven by our continuing commitment to our Quality Process.
Our hallmarks of generating cash and reducing costs form
the foundation upon which rests our three-point plan to increase
earnings growth and consistency. Those strategies are to:
 Build A Better Mix Of Businesses
 Create Breakthrough Products
 Improve Our Customers Results
BUILD A BETTER MIX OF BUSINESSES
We have invested $3.2 billion in our coatings segment
during the past eight years, two-thirds in the form of acquisitions.
Combined with the 1998 divestiture of European flat and
automotive glass, one of our most cyclical businesses, we have
reduced our capital intensity, or fixed assets as a percent of sales,
to 26 percent in 2004 from 37 percent in 1997, better enabling
us to manage the ups and downs of the economy.
This better mix of businesses has also significantly
improved our cash flow. In the recession of 2001, we actually
increased cash from operations. And in the three years since, we
have generated more than $3 billion in cash from operations, a
more than 40-percent improvement over the three years after the
1991 recession. While providing growth today, our acquisitions
have created a foundation from which we can generate further
growth. Some of those businesses are featured on these pages.
Meanwhile, the businesses we targeted for cash generation 
such as flat glass, automotive OEM glass and chlor-alkali
and derivatives  are producing strong results thanks to
improvements they have made in efficiency and productivity.
CREATE BREAKTHROUGH PRODUCTS
Technology has been the backbone of our company since
the day it was founded, and in recent years we have expanded
our collaborations with universities, national labs and other
companies throughout the world, successfully accelerating our
ability to bring new products to market. A perfect example is
Trivex lens material, the first and only to feature all three highly
desirable attributes for eyewear: excellent optics, impact resistance
and light weight. We teamed with a company that created a
lightweight, bullet-resistant material for eyeshields in military applications, and in less than two years, developed an awardwinning
product that is taking share from competitive technologies.
Our quest to form technical collaborations extends in all
directions, even across our own company. Researchers at our
coatings labs in Allison Park, Pa., and their counterparts at our
chemical labs in Monroeville, Pa., worked together in developing
a breakthrough process for bringing the power of the
photochromic technology in Transitions lenses to the fastestgrowing
lens substrates in the world  polycarbonate, high index
and Trivex monomers. As a direct result of our research and
development collaborations, PPGs optical products business is
positioned to generate continued growth.
IMPROVE OUR CUSTOMERS RESULTS
The purpose of building a better mix of businesses and creating
breakthrough products is to improve our customers
results. Whether its our coated glass products that can help
reduce heating and cooling costs by as much as 30 percent or
more, or the automotive refinish primer that reduces application
and cure time from one hour to 10 minutes, or any of our
products and services, we continuously apply technology
and innovation so that we can meet and anticipate customer
requirements.
Our success in building a better mix of businesses, creating
breakthrough products and improving our customers results has
not only improved our performance but positioned us to generate
further growth. During the past four economic cycles, dating
to 1968, we have grown earnings per share at a rate of 9 percent
per year, surpassing the S&P 500 by three points.
PPG PEOPLE VITAL TO OUR SUCCESS
Our performance in 2004 is a direct reflection on PPG
employees throughout the world. Their hard work and commitment
to continuous improvement played a vital role in our
success. Among their accomplishments in 2004 was a 15-percent
reduction in our companys injury and illness rate. Although Im
pleased with this improvement, one injury, one illness, either on
the job or away from work, is one too many.
Retiring from our board of directors in April will be
Allen J. Krowe. Since 1987 he has provided PPG with leadership
and counsel, which have contributed significantly to our
companys growth and success.
Sadly, we lost a key member of the PPG family in 2004 when
Mike Ludlow, senior vice president, OEM coatings, died in
November. He was an outstanding leader, a good friend and an
inspiration throughout his courageous, year-long struggle
against leukemia.
CONTINUED GROWTH LIKELY IN 2005
We believe global economic expansion will continue
in 2005, although probably at a slower pace. As the new
year unfolds, we face the challenge of higher raw material
costs in our coatings segment as a result of increased
industrial activity, supply-demand imbalances and
increased supplier costs. These same forces should benefit
our chlor-alkali business through at least the first half
of 2005.
Nonetheless, we are better positioned to face variations in
the global economy than we were five or 10 years ago  and
were better positioned than many of our peers  largely
because of our better business mix.
Regardless of economic conditions, we remain committed
to generating cash, driving down costs and generating profitable
growth. Im convinced that with your ongoing support we can
continue to unlock the potential of this organization for the
benefit of customers, employees and shareholders, delivering
meaningful and long-term value.
RAYMOND W. LeBOEUF
Chairman and Chief Executive Officer