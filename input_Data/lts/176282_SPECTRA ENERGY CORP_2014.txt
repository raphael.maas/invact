Fellow Shareholders

North America�s energy landscape is in the midst of significant change,
driven by the forces of �supply push� from North America�s immense
oil and natural gas resources and �demand pull� from markets across
the continent and increasingly beyond our shores that require energy
to heat homes, fuel industry and contribute to economic and societal
progress. Spectra Energy is situated squarely in the center of these
intersecting dynamics, connecting producers with consumers and
investors with value. We�re acting on the opportunities presented by
this transformational time, and we are also addressing the challenges
that accompany such significant change.
As one of North America�s leading pipeline and
midstream companies, we build and operate natural
gas, natural gas liquids and crude oil infrastructure.
We draw upon an impressive base of resources
that includes a powerful fleet of pipelines, storage
facilities and processing plants � a talented team
of experienced energy professionals � and the
financial capacity to fund growth well into the
future. Those are the defining attributes that set
us apart from � and ahead of � others in our
competitive space.
Your company continues to have what it takes
to succeed � to serve � and to lead. This year�s
report focuses on the qualities that make us who
we are � and prepare us to deliver real, reliable
results through real world cycles. And that�s
important: because it�s easy to deliver positive
results when the wind�s at your back; more
challenging to do so when you�re facing headwinds
from the economic uncertainty and oil and natural
gas liquids price declines that confront us today.
Fortunately, we�ve designed our business model
to thrive in all cycles and all macro-environments.
Spectra Energy is not only a fair-weather investment;
we are a constant performer � a �must-own� in
your portfolio � and a pipeline company committed
to a secure and vibrant energy future.
What We Achieved in 2014
Spectra Energy delivered strong financial results in
2014, underlining our relative strength even in the
face of the lower commodity cycle that began in the
last half of the year and persists today. We exceeded
our earnings target for the year, delivering ongoing 

earnings before interest, taxes, depreciation
and amortization of $3.15 billion, and ongoing
earnings per share of $1.61. We recorded
distributable cash flow in 2014 of almost $1.5
billion, nearly 19 percent above our expectation
for the year.
We increased our annual dividend growth to 14 cents
per share, up from the 12 cents per share growth
we had pledged at this time last year. Even with
this higher than promised dividend growth, we
were able to handily surpass our 2014 dividend
coverage target.
We made significant progress on our capital expansion
plans. During the year we placed almost $800
million of growth projects into service and secured
an additional $3.5 billion in new growth projects
supported by firm, fee-based contracts. Since 2013,
we�ve delivered $16.5 billion in new projects either
into service or into execution, putting us near the
halfway mark in our drive to secure contracts for
$35 billion in new project growth by 2020.
We were responsive to the needs of our customers
by operating reliably and responsibly. We achieved
record volume send-outs across our system � on the
Texas Eastern, Algonquin, East Tennessee, Platte, Union
Gas, Western Canada and Maritimes & Northeast
systems. Nothing speaks to customer satisfaction
like repeat business, and for the last decade we�ve
averaged an annual contract renewal rate on our major
U.S. pipelines of more than 95 percent, and in 2014
we achieved a contract renewal rate of 99 percent.
Our premier asset footprint
In our business, success hinges on being in the
right place at the right time. And in 2014, we again
proved that we are right where we need to be.
Spectra Energy�s asset footprint is ideally situated
in the midst of North America�s most prolific
supply basins � and connected to the continent�s
fastest growing demand markets. The reach of
our infrastructure benefits producers, end-use
customers, markets and the U.S. and Canadian
communities we serve. It means we can deliver
energy resources reliably by employing the pipes
we already have in place, or building entirely
new extensions of our pipeline infrastructure to
further our competitive reach. It means we can
serve growing needs efficiently and cost effectively
by expanding from a strong base. And it means
we are looking around the corner to the future
� enabling us to build vital new infrastructure by
utilizing established resources, rights-of-way and
relationships so that we can provide just-in-time
service for our customers� growing needs.
Our execution advantage
It�s not overreaching to suggest that nobody does
midstream infrastructure better than Spectra Energy.
We�ve earned a reputation for designing, constructing
and operating our assets with unrivaled
excellence. We deliver projects into service on
time, on budget and to exacting engineering and
safety standards. We�re not alone in our view.
Our New Jersey-New York Expansion won the
Platts Global Energy 2014 Premier Construction 

Project Award, recognized for excellence in project
execution and management.
The projects delivered into service in 2014 join
a list of more than two dozen major expansion
projects we�ve delivered into service since 2010
and underscore our ability to execute effectively
on large-scale, complex projects that address the
needs of an evolving energy marketplace.
Our TEAM 2014 and TEAM South projects expand
our Texas Eastern system by approximately 600
billion cubic feet per day � and they address the
need for bi-directional flows that can move
increased supplies of natural gas from the Marcellus
and Utica to demand markets both north and south
of these prolific supply basins. Spectra Energy�s
pipelines are ideally situated to benefit from this
changing gas flow dynamic. In fact, by 2017, with
TEAM 2014, TEAM South and other projects we
currently have in execution, we will make
Texas Eastern fully bi-directional and provide
unprecedented flexibility to our customers.
In Western Canada, we placed the North Montney
Project into service, providing an additional 210 million
cubic feet per day of gas processing capacity to serve
customers� needs in the prolific Montney play.

We�ve got a healthy backlog of projects due to
commence service over the next several years �
large, strategic projects that extend our portfolio
and build a platform for future growth. Our
projects in execution include Algonquin Incremental
Market, or AIM, which expands the capacity of
our Algonquin system and enables the flow of
Appalachian natural gas to desperately constrained
New England markets � Ohio Pipeline Energy
Network, or OPEN, set to move incremental
production from the Utica and Marcellus shales to
Midwest, Southeast and Gulf Coast markets �
NEXUS, which will further deliver Utica gas to
markets in Michigan, Ontario and beyond � Sabal
Trail, a new 500-mile interstate pipeline that will
serve power generation and industrial needs in
Florida and Southeast markets � and a multiphased expansion of our Dawn-Parkway system in
Ontario that will serve growing demand for natural
gas in Ontario, Quebec and the U.S. Northeast.
And we have a number of great projects in the
development phase, such as Access Northeast, a
joint project with Eversource Energy and National
Grid that will upgrade existing pipeline facilities and
market area storage assets in New England to deliver
up to one billion cubic feet per day of natural gas to
serve regional power generation needs.
Spectra Energy Liquids has experienced steady
profitable growth since being added to our
portfolio two years ago. We continue to optimize
our Express-Platte pipeline system to meet
market demand with expansions to the system
and associated storage, and we�re executing on
connections, laterals and extensions to enable
increased liquids flow through our Sand Hills
and Southern Hills pipelines. Our DCP Midstream
business, a 50-50 joint venture with Phillips 66,
is a must-run business that processes about 12
percent of the U.S. gas supply and is strategically
positioned in key domestic basins. The business
is navigating through the current commodity
down cycle while continuing to add facilities that
customers still require.
Successful execution goes hand in hand with
engagement amongst our various stakeholders �
the communities where we operate, the customers
we serve, the local citizens and regulatory agencies
who monitor our performance and hold us to a
high standard. Early and ongoing communication
with stakeholder groups is central to how we
operate our existing assets and develop new ones.
We build infrastructure; but the trust we build with
community partners is the foundation for every
project, our daily operations, and all of our progress.
Our experienced team
Our success in 2014 � and always � is a reflection
of our accomplished team. The men and women
of Spectra Energy performed exceptionally well
during the year, and focused intently on delivering
improved results, growing value, better service and
new solutions. Their steadfast commitment to safe
and reliable operations underlines the fact that
our license to build and operate is in good hands.
Serving with them each day is a humbling honor.

We are especially proud of the safety results
delivered last year. On many fronts, 2014 was our
best-ever year in terms of safety performance.
Our employee injury rate improved 40 percent
over 2013, and our other safety metrics likewise
improved year-over-year. Those advances are
testament to the focus and hard work our team
has committed to ensuring a work place where
safety is considered above all else and where
true success can only be realized when each shift
finishes the day with zero work-related injuries
or illnesses.
Our progress on all fronts is made possible by
a culture of accountability, empowerment and
integrity. The men and women of Spectra Energy
demonstrate our values every day, and in 2014
their dedication was well recognized. We
were honored to once again be named to the
Ethisphere Institute�s annual list of the World�s
Most Ethical Companies, and to important
sustainability metrics like the Dow Jones
Sustainability Indexes and the CDP Indexes. I
encourage you to visit our sustainability report
which details our performance in the areas of
environmental integrity, economic value and
social responsibility.
Your dedicated board of directors plays a
key role in defining our culture, setting our
strategic course, and ensuring that management
is accountable for the results we achieve for
investors. I am grateful to our entire board,
including our independent lead director Tony
Comper, for their wise counsel, guidance,
perspective and support.
What you can expect
We did what we set out to do in 2014 � and
then some. But we are now focused on 2015
and beyond. This means executing on the $9.5
billion in projects we have contractually secured,
while also ensuring that we are rigorous in our
assessment of both the opportunities and risks
facing our business in the current and longer-term
environment. We have worked diligently to buffer
your company from the type of volatility that the
current commodity, currency and economic cycles
are showing can quickly occur. We have, as a result,
built a powerhouse of high-performing fee-based
businesses that have the ability to deliver secure,
ongoing earnings, cash flow and dividend growth.
Although we are not immune to the dramatic and
recent drop in crude oil and natural gas liquids
prices, we have structured Spectra Energy such
that well in excess of 90 percent of our earnings
are sheltered from commodity price movements.
That puts us in a good place to weather this
current commodity cycle without compromising
our commitment to investors or curtailing our
growth expectations.
Over the past five years we�ve doubled the number
of projects we have in execution, which means 

we�re growing earnings, cash flows and value
for investors. This value can be seen in the 115
percent total shareholder return our investors have
realized in that same period. We�re well on our
way to achieving our goal of $35 billion in secured
growth projects by the end of the decade. And
our track record of successfully building both
infrastructure and relationships makes us a
trusted, preferred energy partner in communities
across North America.
The talented men and women of Spectra Energy
commit passion and purpose to ensuring that
stakeholders realize value from their association
with us. We strive to prove ourselves anew with
each project we take on, each challenge we tackle,
each decision we make.
We are confident that we have what it takes to
excel in this exciting new era of energy. I believe
you can share equally in that confidence, and I
thank you for your investment.
Gregory L. Ebel
Chairman, President and
Chief Executive Officer