To Our Shareholders,
For SBA, 2008 was a year of extreme opposites.
Operationally, we had a very good year. We posted
industry-leading growth, we beat our budget, we met
or exceeded our financial guidance each quarter, and
we increased the size of our tower portfolio by 26%.
Additionally, we completed two financing transactions,
and ended the year with solid cash and liquidity positions.
All in all, we were very pleased with, and proud of, our
operational performance in 2008.

However, our stock did not perform well in 2008. We believe the
poor performance of our stock was directly related to concerns
over access to capital and future costs of capital. In September
2008, worldwide capital markets began a steep decline and,
for a period of time in the fourth quarter, credit markets were
essentially closed. This set of circumstances created concerns
in the minds of investors, particularly for a company like SBA
that uses debt as an essential component of its capital structure.
Even though SBA has no debt maturities until the second half
of 2010, we believe the state of the credit markets was so bad
that investors feared the worst about our refinancing prospects
and bid our stock down in November 2008 to a three-year low.
Although the general economy remains challenging, the credit
markets have improved since the fourth quarter 2008 and our
stock price has partially recovered as of the date of this letter.
We have learned a few things from the events of 2008 that we
will apply in our future planning. Most importantly, the events
of 2008 have confirmed our belief that wireless is a strong and
growing business which is more resilient in challenging economic times than many other businesses. Throughout 2008
our wireless carrier customers showed continued growth and
strength in their wireless businesses. The wireless carriers
enjoyed subscriber growth in general with even higher growth
in the use of data services. It became evident that more and
more consumers were choosing wireless services over their
wireline phones. Some wireless customers have chosen to use
“smartphones” instead of personal computers. We hesitate to
say that the recent tough economic times have now proven that
wireless service is a consumer staple, but the trend is certainly
in that direction. We believe the continued growth in wireless
in general and data services in particular will lead to additional
network demands for our customers. While our customers’
capital expenditures may vary short-term with the economy, 

over time we believe the expected continued growth in wireless
will require material capital investment in additional antenna
and related infrastructure by our customers. We expect to
benefit from that investment, and the recent economic troubles
have not dimmed our long-term optimism about the growth of
our business.
The growth in wireless and the resulting demand for antenna
space, even in a difficult economy, has reaffirmed our view of
the appropriateness of our strategic goal of continued portfolio
growth. We completed three large acquisitions of tower portfolios in 2008 as well as a number of smaller ones. We are very
good at acquiring and integrating towers into our company. In
2008 we integrated almost 1,700 towers ahead of schedule and
below budget. We are very pleased with the assets we acquired,
and they are performing well. In response to changing capital
market conditions, we have reduced our appetite for additional
portfolio growth and instead are focused primarily on reducing
our debt leverage. However, we intend to continue our strategy
of additional portfolio growth as capital market conditions
permit because we believe the growth profile of the assets we
acquire will continue to be very favorable. We will have many
opportunities to grow our portfolio at prices that we believe
will produce excellent returns for our shareholders.
We also demonstrated our continued ability to perform well
operationally. Unlike many companies, we did not experience
any material adverse impact to our revenue or gross profit in
2008 as a result of the declining economy. We increased our
leasing revenue materially, at the highest growth rate in our
industry, while at the same time keeping our overhead, or cash
selling, general and administrative expenses, in line. As a result,
we materially increased our Adjusted EBITDA and Adjusted
EBITDA margin in 2008. Our same tower revenue and tower
cash flow growth once again led the industry. We increased our
new tower build production to our highest output in six years,

and ended the year in a position to build even more towers in
2009. We believe we are well positioned to efficiently and
profitably handle material additional growth, whether from our
existing assets or through new towers added to our portfolio.
We have responded to deteriorating economic conditions
operationally by refreshing our focus on cost control and
operational efficiency. For 2009, we anticipate holding our
headcount relatively flat, with only minor increases to our cash
SG&A expense. We expect to be able to achieve these efficiencies notwithstanding material portfolio and leasing revenue
growth. Our ability to produce these results is a testament
to both our operational performance and the fundamental
attractiveness of our business model, which continues to
accommodate very large increases in portfolio size and leasing
revenue with only minor increases in overhead. Our overhead
expense continues to decline as a percentage of our revenue.
Our success operationally in both good and challenging
economic times has given us the confidence to continue our
current path without the need for any material change in our
operational structure.
One of the most important areas where change is likely to
occur is in the area of capital structure. We have always sought
to have a capital structure at SBA which facilitated maximum
shareholder appreciation over a five-year period of time with an
appropriate balance of risk and reward. Prior to the Fall of 2008,
we believed we had the appropriate structure and our shareholder returns over the preceding five years were evidence of
our success. We maintained our debt leverage at the high end
of our target range, our operational results easily serviced our
annual debt requirements and debt capital was readily available. 

As a result, we grew the company and equity free cash flow per
share materially. We believed our debt structure had optimized
value creation for our shareholders.
All of that changed with the collapse of the capital markets in
the Fall of 2008. Credit market conditions deteriorated to a level
not seen in generations. Although we did not access or need
to access any debt financing in the second half of 2008, the
perceived cost of new debt financing for SBA rose dramatically,
as evidenced by declining prices in our outstanding publiclytraded debt. Given the fact that the value we can create for
shareholders with our business model is significantly impacted
by the cost of our debt, we believe the perceived increase in
the cost of new debt for SBA directly led to the decline in our
stock price which we experienced in the last four months of
2008, notwithstanding the continued strong operational performance in our business.
Our primary long-term goal continues to be above-average
capital appreciation for our shareholders. In light of the magnitude of the capital markets decline that occurred in late 2008
and the impact it had on our stock price, we are taking a fresh
look at our capital structure to determine the structure that
we believe will optimize future shareholder value. Our capital 

structure goals remain the same. We want to maintain a capital
structure that facilitates material growth without undue refinancing or interest rate risk. Our views around eliminating the undue
refinancing or interest rate risk have been most influenced by
the condition of the capital markets. At this time, it is our belief
that the optimal capital structure for SBA going forward will
likely have less debt leverage than we believed was appropriate
prior to the collapse of the capital markets in the Fall of 2008.
During 2009 we will focus more on balance sheet management
and less on portfolio growth. It is our goal to attain an optimal
capital structure as fast as is prudent for long-term shareholder
value creation, at which time material portfolio growth, by seizing the right opportunities, will once again be a primary objective.
In closing, I would say that it is through the more difficult economic times that the strength and stability of our company are
most appreciated. We expect 2009 to be a difficult year for
the general economy, so much so that many other companies
are struggling with their views of and certainty around the future.
Fortunately, that is not the case with SBA.
Clear Multi-Year Visibility
Our business is stable,
predictable and certain.

Our business is stable, predictable and certain. The long-term
nature of our leasing revenue and the strength of our customers
give us tremendous comfort around our future cash flows. We
intend to continue our practice of providing regular quarterly
financial guidance, and we expect to meet or exceed that guidance as we have done in the past. We are projecting material
organic growth in 2009 in leasing revenue, Adjusted EBITDA
and equity free cash flow. As we move through 2009, I am
confident that the strength of our operating performance and
our business model will increasingly differentiate SBA as the
high-quality company we have become. Move than ever, we
remain confident and optimistic about our prospects for creating additional value for our shareholders. As always, I want
to thank our shareholders, customers and employees for their
part in our success, and we look forward to reporting our
future results.
Sincerely,
Jeffrey A. Stoops
President & Chief Executive Officer
