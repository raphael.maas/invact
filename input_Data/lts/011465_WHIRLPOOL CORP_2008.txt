
By many measures, 2008 was a challenging year
for Whirlpool Corporation. Our global business
was impacted by one of the most volatile economic
environments we have seen in decades. We
experienced a steep decline in consumer demand
for appliances, significant and rapid fluctuations
in currency exchange rates, and we continued to
face high material and oil-related costs.
Despite the global economic volatility, we
maintained  and in some areas grew  market
position for our leading global brands. This is a
testament to the strength of our global brand
portfolio, product innovation and the value we
bring to consumers.

We have taken, and will continue to implement,
significant actions in all areas of the business to
address this highly volatile and uncertain economic
environment. Now, more than ever, we are
aggressively delivering all elements of our brand
value creation strategy  focusing on attracting
and retaining consumers to our brands with
innovation, providing excellent service and value to
trade customers, and driving lower costs and higher
product quality across our global operations.
These steps, combined with continued investment
in  and consumer preference for  our brands,
position us well to endure this difficult period. During
our nearly 100-year history, we have endured other
difficult times. I am confident that, at some point,
the economy will improve and consumer demand will
return to more normal levels. And when it does, we
stand ready with even better products, more
consumer-relevant innovations, more efficient
operations and, ultimately, an even stronger company.

financial performance
Net sales for the year fell 3 percent to $18.9 billion.
Diluted earnings from continuing operations were
$5.50 versus $8.10 in the prior year. Our operating
earnings fell by 48 percent, and free cash flow
fell by more than $600 million to negative levels.
Our Latin America and Asia operations delivered
strong sales and operating profit even as growth in
these markets slowed. Our results in North America
and Europe were impacted by weak industry demand
and higher material and freight costs.
2009 priorities
As we continue to navigate through what we
expect to be a prolonged economic downturn, we
are focused on the following three priorities:
Reduce our global cost structure
We are taking aggressive actions to redesign our
products with global standards for parts and
components. This proven global approach lowers
costs, improves quality and speeds our time from
design to market. The difficult decision to close five
manufacturing facilities and reduce approximately
5,000 positions globally will allow us to further
reduce costs and operate more efficiently in 2009.
We are aggressively managing all costs in every part
of our business to rapidly adjust our cost structure
to current and expected global demand levels.

Focus on cash generation
We are taking steps to ensure that we can
appropriately invest in our business, reduce overall
debt levels and lower our overall working capital.
We have reduced spending in all non-product areas
while still investing in new products and innovations,
including exciting new product launches in every
major product category in all regions in 2009.
Innovation attracts consumers to our global portfolio
of brands and generates higher margins. As such, we
are making continued and focused investments in
innovation, and our pipeline remains robust.
We expect the combination of lower costs in nonproduct
areas, lower inventory levels and higher
margins from new products to generate the resources
needed to fund our business.
Strong market focus
While we expect consumer demand to decline
further in 2009, given the current economic outlook,
we also expect to maintain and grow our global
market position. We will do this by ensuring that
our branded products, particularly our newest
innovations, represent a strong value to our trade
customers and consumers.

One recent example is the introduction of our high efficiency
Maytag performance series laundry pair.
This innovative laundry pair commands higher
margins while delivering great value to consumers
in the form of lower energy and water bills. Energy
efficiency represents a great financial value to
consumers, especially during these tough economic
times. Whirlpool Corporation offers the widest
array of energy and water efficient appliances to
the global marketplace.
T h e Road Ahead
We have a clear understanding of the global dynamics
of every part of our business, and we expect the
external economic volatility we experienced in 2008
to continue through 2009. We are taking all the
necessary actions to deliver the best results possible
during this period, but equally important, we are
taking the right actions to ensure that we emerge
from this economic period as an even stronger
company that will offer significant value creation
for you, our shareholders.

The decisive and thoughtful actions we are taking
today will significantly lower our overall cost
structure. Our brands, our product innovation and
our overall execution will only get better during
this challenging period. With these fundamentals
in place, we will be very well positioned to capitalize
on our growth opportunities when the global
economy recovers to normal growth levels.
Our greatest asset remains the talented, dedicated
Whirlpool employees worldwide who work tirelessly
to create better, more innovative products that
improve the lives of people  in and around the
home  each and every day. In the pages that follow,
you will see the Whirlpool Corporation value-creation
story through the eyes of our employees.
Thank you for your confidence and your continued
investment in Whirlpool Corporation.

Sincerely,

Chairman of the Board and Chief Executive Officer

