It can be hard to assess the progress of a
major transformation from a single years
results. And yet, when you look back after
a few years, it becomes clear just how much
has changed.
Before I offer a broader perspective, let
me describe how your company performed
in 2016. We made substantial progress
last year:
 We achieved operating earnings per
share of $13.59 and free cash flow of
$11.6 billion.
 We invested heavily for our long-term
competitivenessnearly $6 billion in
research and development, nearly $4
billion in capital expenditures and nearly
$6 billion to make 15 acquisitions, adding
to our capabilities in the high-growth
areas of cognitive, cloud and security.
 We returned $8.8 billion to you, our
shareholders, including dividends of $5.3
billion and $3.5 billion in gross share
repurchases. We raised our dividend for
the 21st consecutive yearand it was
IBMs 101st straight year of providing
one. We are proud of both milestones.
 IBMs technical community outdid itself.
For the 24th year in a row, IBM led in
U.S. patents earned, becoming the first
company to break the 8,000 mark
in one year.
 All of this and more is due to the work
and passion of 380,000 IBMers. They are
our greatest competitive advantage and
the most important reason for confidence
in IBMs prospects in the years ahead.
This is why its so encouraging that
employee engagement scores continue
to rise year after year.
But understanding IBM at the start of
2017 requires a longer viewin particular,
remembering why we embarked on this
journey of transformation. It had nothing
to do with economic downturn or fixing
something broken. Rather, our experience
over more than a century has trained us
to be vigilant. We began to see that the IT
industry would radically reorder, driven by
the phenomenon of data, the maturation of
cloud computing and the emergence of what
many call artificial intelligence or AI. We
were convinced that the source of value
for our clients and for our ownerswould
shift, and that the convergence of these
innovations would usher in a new era in
both technology and business.
We didnt wait. We got out of commoditizing
businessestrimming $8 billion from our
top line (and nothing from our bottom line)
over the past five years. We redoubled
our efforts to reinvent our core hardware,
software and services franchises, while
investing to create new onesin cloud, data,
cognitive, security and the other businesses
that comprise our strategic imperatives. We
did this not simply to manage a portfolio
of businesses, but to build the integrated
capability that our clients would need for
the new era.
While our transformation continues, a strong
foundation is now in place.
Our strategic imperatives have reached
critical mass. At $33 billion, they now
contribute 41 percent of IBMs revenue.
With double-digit growth, were on pace
to achieve our goal of growing these
businesses to $40 billion by 2018. Most
importantly, within these businesses we
have built unique and powerful platforms
with Watson and the IBM Cloud:
 Watson has become the AI platform
for business. I will have much more to
say about Watson, but just consider
that, through our clients and ecosystem
partners, we expect Watson to touch one
billion people this year.
 IBM is now the global leader in cloud
for the enterprise. In 2016, our cloud
business grew an outstanding 35 percent,
to $13.7 billion (adjusted for the effects
of currency, as are all revenue growth
numbers in this letter). Cloud by itself is
now more than 17 percent of our total
revenues. Our as-a-Service exit run rate
was $8.6 billion, up 63 percent.
I am so proud of the teams who have
incubated and scaled these businesses.
They are the IBM investors unicorns. At
the same time, we have transformed much
of IBMs corethe other 59 percent of our
company.
Global Technology Services (GTS) is the
market leader in IT services. The next time
you use your credit or debit card, take
out an insurance policy, board a plane or
drive your car, chances are you are being
touched by GTS. Their services run the
infrastructure that powers 60 percent of the
banking industrys daily card transactions,
61 percent of the automobile industrys
production of passenger vehicles and nearly
half of mobile connections worldwide. And
while our GTS team is superb at managing
infrastructure, they are innovators at heart.
In recent years, they have built strong
capabilities in hybrid cloud and are applying
automation and Watson wherever and as
fast as they can. This unique combination
of innovation and trust enables GTS to grow
signings and revenue, and win new clients.
Global Business Services continues to
reinvent itself and is growing robustly where
we have created new practices. Last year,
our cloud practice was up more than 60
percent, mobile more than 40 percent and
analytics 10 percent. Thats goodthough
not enough yet to offset declines in some
traditional practices, such as enterprise
resource planning. But I remain confident
in our ability to transform this business,
and we will continue to invest in these
new areas. For example, we created a new
Watson IoT (Internet of Things) consulting
practice that will include 1,500 consultants,
data scientists and security experts.
When it comes to our ability to reinvent
large, significant businesses, I must say
a word about the mainframe. Installed
mainframe capacity has increased 20-fold
in the past 20 yearsand is still growing, as
you saw in our fourth-quarter 2016 results.
Why do clients love the mainframe? Because
no other system can do what it can do, and
because our mainframe team continues to
create new capabilities for new workloads.
Our latest mainframe runs Linux, processes
mobile payments instantly, supports hybrid
clouds and is becoming preferred as the
most secure platform for the important new
technology blockchain. This is why I call this
part of IBM our youngest oldest business.
Leading a new era
With Watsons victory on Jeopardy! in 2011,
IBM ended the latest so-called AI winter
and led our industry and the world into the
cognitive era. Today, were no longer alone:
The land rush into AI is onincluding a lot of
hype. But we expect to maintain and extend
our lead in cognitive business, because we
uniquely serve the needs of the enterprise.
Enterprises need cognitive solutions that
turn vast amounts of data into insights and
competitive advantage. They need access to
a cloud platform not only for IT capability,
but for speed and agility. Its architecture
must be hybrid, spanning both public and
private clouds, because businesses will
want to leverage their existing investments
in applications, IT infrastructure and, most
of all, their data. And they need a partner
they trust, who understands their industry
and process flows, and whose platform is
secure, scalable, global in scope and local
in presence.
IBM provides this powerful AI cloud
platform, and we are using it to build
industry-based solutions to real-world
problems. We are creating domain-specific
datasets that we either own or partner
to access, and were applying our deep
industry expertise to build vertical solutions
and to train Watson in specific professional
fields.
In short, to win in the cognitive era,
data matters. Client control of their data
and insights matters. Public, hybrid and
enterprise-strength cloud matters.
Industry expertise matters. Trust and
security matter. That is what sets apart
Watson on the IBM Cloudand why
thousands of clients, partners and IBM
itself are building powerful solutions and
businesses on it:
 IBM Watson Health leverages broad
datasets, including 100 million electronic
health records, 200 million healthcare
claims records and millions of images.
You also need clinical data, payer data
and images, all of which are created
and owned by clients. (Consider that an
estimated 80 percent of the worlds data
cannot be accessed by search engines.)
We capture and integrate all this data,
because publicly available data alone is
not sufficient for work like transforming
healthcare or making advances in
cancer. Whats more, you need deep
domain expertise. Watson Healths 7,000
employees include doctors, nurses,
health policy experts and data scientists.
Finally, we provide a quality management
system that is HIPAA-compliant. Only
IBM Watson Health offers all of this
capability.
 IBM Watson IoT has similar unique
advantages. The number of new clients
and developers using our platform more
than doubled last year. Our new global
Watson IoT center in Munich opened in
2017, including the first-ever Cognitive
IoT Collaboratories, where clients and
IBMers work together to drive innovation
in automotive, electronics, manufacturing,
healthcare and insurance.
 Late last year, we launched Watson
Financial Services. And because top
industry specialists are needed to
train Watson in the risk management
requirements of financial services, we
acquired Promontory Financial Group, a
leader in regulatory compliance and risk
management consulting.
IBM is changing industries around the
world. Watson on the IBM Cloud is available
to more than 200 million consumers to
answer questions, find what they need
online and make recommendations. Half a
million students can choose courses and
master a subject with Watson, which also
helps teachers address each students
unique learning needs. IBM Securitywhich
monitors 35 billion security events a day
for 12,000 clients spanning 133 countries
launched the worlds first commercial
cyber range, where clients can simulate
and prepare for real-world attacks and draw
on the power of Watson to fight cybercrime.
With Watson, buildings are reducing
CO2 emissions by 10 million tons a year;
businesses are accelerating hiring cycles
by 75 percent; and airlines are improving
maintenance efficiency by 80 percent.
It is also becoming clear that as IT moves
to the cloud, transactions must be trusted
by all parties involved. That is why we are
building a complete blockchain platform.
Blockchain brings together shared ledgers
with smart contracts to allow the secure
transfer of any assetwhether a physical
asset like a shipping container, a financial
asset like a bond or a digital asset like
musicacross any business network.
Blockchain will do for trusted transactions
what the Internet did for information.
IBM is already working with more than 400
clients to pioneer blockchain for business.
They include clients who manage foreign
exchange settlements, smart contracts,
identity management and trade financebut
the potential goes well beyond financial
services. For instance, we are collaborating
with Walmart to ensure transparency in the
way food is tracked, transported and sold
to consumers across China and the United
States. And Everledger is using a cloudbased
blockchain to track the provenance
of diamonds and other high-value goods as
they move through the supply chain.
This is the real world of cognitive business.
Unlike the AI we see in movies and pop
culturewhich depict machines achieving
consciousness or independent agency
the true promise of this revolutionary
technology comes not from replacing but
from augmenting human intelligence. It
comes from embedding cognitive capability
in the processes, systems, products and
services that permeate business, society
and our daily lives.
New roads and new rules
for a cognitive future
As 2017 begins, our industry stands at
an inflection point. The next few years
will be critical for information technology
providers, as businesses and institutions
around the world will make key architectural
decisionsabout cloud, about data,
about AI.
IBM is well positioned to help clients make
wise choicesand to lead our competition.
We take seriously our responsibility to
ensure that new technology is adopted in
ways that are both ethical and enduring
never more essential than in times of rapid
economic and societal change. We will
continue to engage across our industry and
society, and to advocate for a business and
policy environment that is open, inclusive,
global and equitable. And we will do more
than advocate. We will innovate.
An example of such innovation is IBMs
Principles for Transparency and Trust in the
Cognitive Era, which we issued in January
of this year. Its main tenets:
 We believe AIs purpose is to augment
human intelligence.
 We will be transparent about when and
where AI is being applied, and about
the data and training that went into its
recommendations.
 We believe our clients data and insights
are theirs.
 We are committed to helping students,
workers and citizens acquire the skills to
engage safely, securely and effectively
with cognitive systems, and to do the
new kinds of work that will emerge in a
cognitive economy.
These principles will be our touchstone for
everything we do to build this new world.
We will practice them with our clients, and
we believe they can form an important
foundation for business and society at large.
A world with Watson
Last October, IBM hosted more than 15,000
entrepreneurs, innovators and other leaders
from business, government, healthcare and
civil society who are working with Watson
to transform their industries and sectors.
Their work showed that Watson has, indeed,
entered the world at scale.
But this signature gathering did something
more than showcase the reality and reach
of Watson. It also presented a vision of
the world with Watson. In example after
example, we saw a world that is healthier,
more secure, less wasteful, more productive,
more personalized, more sustainable.
That is a world my colleagues and I want to
live in. Indeed, this is, in the end, why we
are IBMersand why we remain so deeply
committed to the hopeful future that is now
within our grasp.
As IBM has done throughout its history, we
remain dedicated to leading the world into a
more prosperous and progressive future; to
creating a world that is fairer, more diverse,
more tolerant, more just; and to creating
long-term value for you. Our strategy to
reinvent our company is being validated
by our clients around the world, and has
the support of our Board of Directors. Im
grateful to them and to you, our owners, for
your continued support. Most of all, I am
deeply proud of the IBM team for bringing
us here and for what they do every day for
our great company.
Virginia M. Rometty
Chairman, President and
Chief Executive Officer