To Cerner's Shareholders, Clients and Associates:





                  We implemented nearly 1,300 solutions during 2006 and ended the year with more than 6,000 Cerner Millennium
                                                                 In total, our strategic client footprint now includes approximately



                 Cerner Millennium 2007
     We had another record year in Bookings, Backlog, Revenues, Operating Earnings, Net Earnings, Earnings Per Share, Cash



                                                          Consistently high compound growth rates over 5, 10 and 20 years are created by

thinking long term (we call it vision); being able to create substantial value for our clients; executing our business plans with discipline
and precision; and occupying a growing market                                              Although there is no guarantee of being able to
continue this rate of growth for the next 25 years, we strongly believe that the lessons learned along the way will be an advantage as




Note: Several years ago, we used this letter to document our business model and describe each segment of our business in detail.

business model discussion, please see the Appendix following this letter.



In the Right Place at the Right Time  with the Right Stuff
                                                                                                                              As an
                                                            Most growth markets are created by new innovations adopted over a period of




The Right Place
                                                                                                  Although the starting points are different, every
                                                                                                             This is a stunning reallocation of our
                                                                                                                                       Innovations



                                                      These waves of innovation thrust us into fundamentally new ways of connecting
and collaborating, driving increases of productivity and effectiveness into nearly every sector of our economy, and impacting our daily




The Right Time
                                                                                                                                           not to do
              to




                                             Although the methodology and range of estimates used in the Institute of Medicine's




The Right Stuff
                                                    In the beginning, we focused on selling into the departments laboratory, radiology,



                                                                                We called it Health Network Architecture (HNA Our growth


well accepted, it became apparent that the IT waves
                                                                        Health Economy
                         The healthcare enterprise was                     Era: 2000




                                                                                                                                           
                                                                                                                                           
                              We also discovered the missing




                                                                                                                                               
                                                                                                                                                
element in the design--everything needed to be designed around                     Community




                                                                                                                                                
                                                                                    Era: 1995




the person                                              There was a




                                                                                                                                                    
need to stop and rethink, redesign and rebuild everything we had done




                                                                                                                                                       
                                                                                            Enterprise



                                                                 Others                      Era: 1992



This commitment led to the creation of Cerner Millennium                                                        Clinical Enterprise



                                                                                                                                                                                e
                                                                                                                  


Focus for Finishing this Decade

                                                                     The fundamental facts of healthcare, plus the forces exerted by demographic



                                                        Cerner Millennium
Each day our platform delivers value to thousands of clients and millions of healthcare consumers around the world, in venues from
                                                                                                                                We
                                                                                                                             What


Designing for Role-Venue-Condition




                                                                                                                                It is how healthcare really




                                                                                                                                                      It is, therefore, a




                                                                                                                                             About
                                                                            These are very small businesses, frequently running on limited resources



                                                                                                                                                 We started the
PowerWorks

                           We revamped our core business processes to address the needs and realities of this market, reducing our
cost structures in order to offer these lower price points, leveraging our CernerWorks                                    This work is
starting to pay off in the form of steadily increasing levels of new physician practice footprints; this momentum has led to substantial
increases in our PowerWorks                                                     Today we are providing solutions to about 34,000
physicians inside PowerWorks


Making Our Platform Industrial Strength
The Cerner Millennium
                                                                                We have reached a very exciting time in the maturity of the architecture
                                                                                                             We work hard to ensure that our architecture,




Industrial Strength
                                                                                                                                                              Less than
                                                                                                                                        In 2006, this
                                          
                                                                The table that follows shows the real downtime minutes associated with various




In 2006, we also introduced the concept of the Lights On Network                   


                                                                Lights On Network




2006 was also a year during which we redesigned our implementation methodology with MethodM , Cerner's single engagement
                                                                                                    SM



delivery model for providing value through solutions, with a goal of cutting the effort of implementing and operating our systems in


Increasing the Value to Our Clients

                                                                            Our current clients deeply embed Cerner Millennium in their

                                                            This creates both the pressure and the opportunity to make substantial, rapid
                                  Over the past two years, we have developed a new consulting practice called Lighthouse, which uses
                                                                                                      Together we are making profound
                                                                           As more of our clients create truly digital hospitals and
                                                           Millennium Lighthouse SM




Increasing the Value to Our Shareholders


top line growth in revenues, increase the net earnings and earnings per share through expanding our operating margins and increase
                                                                                                                            The next
section of this letter outlines how we plan to continue our growth well into the next decade, which is an important prerequisite for


The Next Decade: 2010 -- 2020
The healthcare system                                                                                    Many close to the industry



                                                  Anyone who is unfortunate enough to have a complex or chronic medical condition

inconsistent quality of care, tragic medical errors, tremendous waste through redundancy, unfortunate and costly delays in diagnosing
                                                                                                                          The people

        At Cerner, we see a decade of opportunities for




automating the clinical and business processes within

record into electronic medical records (the EMR) and
                                                       We



               That is, because of these advances, other
                                                It will
be this environment that creates Cerner's next decade



Most of Cerner's current business comes from the center
                                    We will spend the
remainder of this letter walking you through the other four




                                                                 
Cerner Millennium as a Global Platform

                                                                  But regardless of policy and funding differences, the need for
                                                                                                                       In many

                                                             We believe this trend will continue well into the next decade, fueling


                                     In our opinion, Cerner Millennium
                                                                         Cerner is the most global company in this industry, with
Cerner Millennium

                                                      This growth represents great execution each day by a growing number of




        We were successful in winning a smaller contract to implement the Choose and Book national appointment booking and
                                                                                    In 2005, our ability to deliver on this smaller
                                                                                                                  We are partnering
                                                                           In 2006, we replaced the software provider for the



                                                                           Our successful performance in England will position


2006 also saw success stories out of the Middle East, France, Australia, Malaysia and Canada, with the Cerner Millennium architecture
                                                               Cerner Millennium

Cerner Millennium as a Device Platform
                                                                                    Today, most diagnostic procedures are highly

                                                                                                   The ICU is now its own world,
                                                                               Whether proximate, attached or embedded, the
                                                                                    Infusion pumps can automatically give the
                                Implanted devices monitor every heartbeat and are programmed to automatically alter the heart's
                                                                                                                         Each
                                                                                         None of these devices were designed to




Cerner Millennium
                                                              We are leveraging this status to create a device architecture we call
CareAwareTM                                                                               We are also rolling out a related line of
medication dispensing devices called CareAware RxStationTM that help make the medication administration process safer and more




Cerner Millennium as a Platform for Pharmaceutical Development & Clinical Research
                                                                                                           In the United States,
                                                                                                       At the federal level, the Food
                                                                                       Many of the practices used in clinical trials to




                                                             11
By the end of the next decade, we believe there will be a fundamental shift in how drugs are researched, how clinical trials are

allowing for rapid development of safer, more effective drugs, along with careful performance monitoring of drugs already in use in




                                                                                               In the next decade, research that today


Cerner Millennium as a Frictionless Transactional Platform -- Employer Services

                                               In this latter half of the current decade, we are targeting two highly entrenched icons--


Eliminating the Clipboard
                                                             the clipboard



the questionnaire, the amount of time given to complete it, your perceptions of what is important that day, whether your memory is
                                                      If you do complete the clipboard to the best of your ability, remembering your
                                                                                                                 What if you are the

                                                                                The clipboard is the start of a series of clinical and
                                                                                                                 To do so requires new
                                                                                                  Like other areas of commerce, we


Eliminating the Insurance Companies--As We Know Them Today
                                                                                                                               1)
through our taxes, which fund the federal, state and provincial programs; 2) through our salaries, which are lessened in order for our




                                               It is a complex process, full of rules and regulations, and it usually takes months




It is a rat's nest of rules, regulations, systems, processes, contracts and networks that make it nearly impossible to comprehend

                                                                                In a particularly telling example of insurance industry
                                                                                                                        Recently, The Wall
Street Journal                                                                                            The title captures the point well,



                                                                Per year                                                       We must


At Cerner, we have envisioned a frictionless transaction system in which the physician and hospital are paid immediately at the point
                                             This will require a systems approach to redesigning the middle, paying the right price for




                                                                    12
And inside Cerner, we have gone further and are eliminating the clipboard and are implementing a new payment system that, at the point
                                                                                                                  We call this new
approach HealtheTM. Over the next few years, we will be offering the Health




                                  But as the leadership team, we must ensure that there is healthy growth at the periphery of the

                                                                               We have the depth of experience, the resources, the
                                                                                                                           With




      FOUNDER                                     FOUNDER                           FOUNDER




      President

