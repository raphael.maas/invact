To My Fellow Stockholders:

When I look back on 2010, it reminds me of a typical early
morning in my office. On most mornings, the darkness and
fog give way to incredible sunshine and magical views of
the San Francisco Bay. Similarly, the environment in 2010
evolved from a time of bleakness early in the year to a time
of building optimism as the year came to a close.
Yes, our near-term financial performance continues
to suffer from record low interest rates, but our valued
clients are increasingly engaged, and we are more
focused than ever on clients and their needs. Looking
forward to 2011, I feel a sense of optimism and
excitement. We are aggressively investing in building
our business for the long term ... and our clients are
rewarding us with their business.
In 2010, our clients brought $78.1 billion in net new
assets* to Schwab � a figure that handily outpaces the
results reported by other brokerage firms. More importantly,
client momentum strengthened as the year went on. During
the fourth quarter of 2010, clients entrusted us with $26.2
billion of net new assets, our best performance since the
first quarter of 2008. By year-end, clients had entrusted us
with almost $1.6 trillion of their hard-earned money, up 11
percent over 2009.
How do we achieve these results? It all starts with our
unwavering belief that if you do the right thing by your
clients, they will choose to do more business with you. It�s
a simple philosophy, but one that drives every decision we
make. We call it �Through Clients� Eyes.�
This philosophy works. Clients opened 829,000 new
brokerage accounts last year, with 99,000 of these
in December � our strongest month for new accounts
in more than eight years. We now serve more than
10 million client accounts, including approximately
8 million brokerage, 700,000 bank, and 1.5 million
retirement plan accounts.
These and many other signs indicate that the darkness
of early 2010 is beginning to transition to daylight as we
move into 2011.
improving financial results
In 2010, our net revenues of $4.2 billion grew 1 percent
over 2009. However, consistent with momentum across
the firm, we achieved revenue growth of 14 percent
when viewing the fourth quarter of 2010 relative to the
fourth quarter of 2009. In addition, we concluded the
year with sequential quarterly improvement across all
three major sources of revenue � asset management
and administration fees, net interest revenue, and
trading revenue.
Our operating financial performance in 2010 was quite
strong. However, our reported results were masked by
several one-time charges related to the recent financial
crisis. Specifically, we incurred charges of $320 million
for settling litigation and regulatory matters related to
a mutual fund we manage; we incurred charges of
$132 million to cover remaining losses recognized by our 


money market funds� investments in a single structured
investment vehicle; and we chose to exit the credit card
business at a cost of $30 million. Although as stockholders
we are pained by these charges and are disappointed in
ourselves, we move into 2011 and begin a new year with
a clean slate and confidence that we have put the major
issues from the financial crisis behind us.
Our net income in 2010 was $775 million before these
charges and $454 million after, with earnings per share
calculated at $0.65 before and $0.38 after these charges.
Our pre-tax profit margin was 29.7 percent before and
18.3 percent after these charges. (See Letter from the
Chief Financial Officer, page 6, and reconciliation of these
financial measures on page 8.)
investing for growth
As we saw client momentum build over the past 12
months, we continued to make significant investments
in enhancing the quality of the client experience. These
investments lay the foundation for long-term growth.
Last year, we nearly doubled our project spending to
$130 million as we invested to achieve two primary goals.
First, we worked to make it easier for clients to access
our solutions and to do business with us. Second, we
broadened the services available through Schwab in
order to stay ahead of the evolving needs of a diverse
client population.
While we are pleased with our ability to generate organic
growth within our existing business lines, occasionally we
look outside to complement our ability to serve our clients�
needs. That was the case last November when we acquired
assets of Windward Investment Management, Inc., for
$150 million in cash and stock. Windward has a track
record of managing client portfolios with above-benchmark
returns and lower relative volatility. With the renamed
Windhaven� Portfolios, we have strengthened our set
of investing solutions for both individual investors and
independent investment advisors who want to outsource
a portion of their money management.
focusing on client needs
Schwab is, first and foremost, a client-driven company.
And seeing the world through the eyes of our clients
helps ensure we operate the company in a way that
leads to long-term growth. We think this approach
makes us different.
In 2010, our actions demonstrated how we�re different
and how that makes a difference in the financial lives of
millions of Americans. Let me share a few examples.
1. We believe in financial opportunity for all.
We began last year with two announcements that
support our corporate purpose of �helping everyone
be financially fit.�
�	 We reduced and simplified online equity trade
commissions to a flat rate of $8.95, regardless
of the number of shares traded or the size of the
client�s account.
�	 We launched Schwab Managed Portfolios-ETFs � a
relatively sophisticated and diversified set of portfolios
that invest in low-cost Exchange Traded Funds.
By January 2011, we had expanded our ETF offerings to
a total of 13 proprietary equity and fixed-income funds,
all of which feature low operating expense ratios and
commission-free online trading in Schwab accounts.
As we continue to expand our reach to more Americans,
we recently announced a relationship with AARP to provide
financial guidance services to its large and growing
organization dedicated to helping people aged 50 and
up. This program will serve millions of eligible AARP
members with special discounts, complimentary financial
consultations, and access to our On Investing magazine.
2. We believe people need the right tools to
achieve financial fitness.
During 2010, we introduced new products, entered
new business alliances, and upgraded client tools
and technology.
Through our agreement with J.P. Morgan, a leading U.S.
bond underwriter, we expanded our clients� access to new
municipal and corporate bond issues. Since announcing
this agreement last April, Schwab clients have had the
opportunity to participate in more than 500 new bond
offerings. Fixed income investors at Schwab now have
access to more than 30,000 individual fixed income
securities. Schwab clients also gained access to J.P.
Morgan�s award-winning fixed income research. Later in 

the year, we launched a web-based new issue municipal
bond calendar, making it easier for clients to review,
research, and order new issue municipal bonds online.
We also expanded our fixed income offerings with
five new PIMCO�-managed Municipal Bond Ladder
Separately Managed Accounts. These strategies are
designed for fixed income investors seeking low-cost,
tax-advantaged solutions and income generation �
many of whom are in retirement.
Last year, we continued to invest in technology, including
the first Schwab mobile app for the Apple� iPhone�. Of
course, transacting business over the phone is nothing
new for Schwab. We launched our TeleBroker� phone
service in 1989 and pioneered wireless access with
PocketBroker in 2000. But our newest mobile application
integrates brokerage and banking capabilities, with many
more exciting features to come this year, including the
ability to transfer funds and deposit checks directly from
a mobile device.
Our technology investments also included a major
rebuild of our platform for active traders with the new
StreetSmart Edge�, and we are working closely with
independent investment advisors on the development of
a state-of-the-art technology platform for managing their
businesses, Schwab Intelligent Integration�. Lastly, on
Schwab.com, we registered more than 250 million client
log-ins, with plans for substantial enhancements to our
client websites this year.
3. We believe investors should be informed
and empowered.
Last year, we shared our knowledge and expertise with
clients and non-clients alike.
In Schwab Investor Services, we provided help and
guidance to millions of individual investors while supporting
their increased engagement with their investments. For
example, attendance was up at more than 10,000 financial
workshops held at local branches, with additional topics
available �on demand� on Schwab.com. Our quarterly
On Investing magazine was distributed to approximately
2.5 million households, and by year-end we introduced a
version for the Apple� iPad�.
In Schwab Institutional Services, we launched Schwab
Advisor University� for independent investment advisors
� an interactive online training program with a dozen
self-paced courses ranging from new employee orientation
to client account opening and back-office processing.
We supplemented online access with face-to-face events,
including our regional SOLUTIONS� workshops and the
industry-leading IMPACT� conference attended by nearly
2,000 independent advisors.
For participants in Schwab-managed retirement plans, we
continued to offer high-quality, low-cost programs that can
help employees put more of their money into their own
pockets when they retire. We also published a landmark
study, �The New Rules of Engagement for 401(k) Plans,�
to help companies identify features that drive positive
participant outcomes, including advice, the employer
match, automatic enrollment, and automatic savings
increases. Millions of 401(k) participants can benefit
because their employer has retained Schwab to help
manage their retirement plan.
For Americans at every stage of life � even non-clients
� we strengthened our commitment to financial literacy
with funding, expertise, and volunteer support. Through our
alliance with Boys & Girls Clubs of America, we revamped
our Money Matters: Make It Count financial education
program and launched the Make Change Count pledge
campaign to inspire teens to improve their financial
futures. We also began working with Goodwill Industries
International in Indianapolis to improve the financial wellbeing of the underserved and working poor. The program
is modeled after similar efforts in San Francisco and
Austin, Texas, where we provide financial coaching for lowincome families. We also continued to expand our Schwab
MoneyWise� website, which helps thousands of Americans
master money basics.
4. We believe the best view of our business
is through clients� eyes.
As I mentioned earlier, we continued to follow a
straightforward business philosophy: When you
put client needs first, business results follow.
Last year, our branch employees conducted more than
600,000 personal interactions, while our phone-based
representatives fielded almost 14 million calls from
individual investors, independent investment advisors,
and 401(k) plan participants. Not only did our investment
professionals handle a large volume of calls, but calls were
answered, on average, in just 21 seconds. And we made 


it easier than ever to reach a live representative � by
pushing a single button at the outset of the call, or by
simply asking for a live representative when the call
is answered. In addition, nearly 14 million calls were
handled efficiently through our automated systems when
the client preferred this approach. We also engaged in
dialog on rapidly expanding social network websites,
such as Twitter, Facebook, and YouTube.
Client feedback is essential if we hope to see the
world through clients� eyes. Last year, we continued
our Client Promoter Score (CPS) program in which we
survey clients and ask them to rate us, from 0 to 10,
on their willingness to recommend Schwab. The CPS
calculates the number of �promoters� minus the
number of �detractors� to arrive at a net indicator of
client loyalty. Our CPS for individual investors reached
a record 37 percent, with significant gains for our
value proposition, investment help and guidance, and
customer service. CPS scores also remained strong
for independent investment advisors, who praised our
responsive service, and for retirement plan sponsors.
focusing on what matters
As we look ahead to 2011 and beyond, we believe our
strategy is working and our diversified business model is
sound. Most of all, we continue to see the world through
the eyes of our clients as we strive to help everyone be
financially fit.
Going forward, we will continue to focus on what matters
� and what drives stockholder value over the long term.
�	 We will scale our business to make financial
services more accessible to all.
�	 We will innovate with new products and tools that
put financial fitness within reach for every investor.
�	 We will inform and empower investors, as
we believe that the best investor is a
knowledgeable one.
�	 We will view our business and the decisions
we make through clients� eyes.
As the darkness of the last few years transitions to a
brighter day, I am grateful to our clients, stockholders,
and employees. To our clients, thank you for entrusting
us with your hard-earned dollars. To our stockholders,
thank you for investing in our unique approach to
financial services. To our employees, thank you for your
hard work and passion for serving our clients.
Your support and confidence in our company provides
the foundation for our success.
walt bettinger
March 12, 2011