To our shareholders:

In 2003, Williams had significant success in our efforts to reshape, resize and refocus our
company  advancing us well ahead of our planned progress for the year. Our company entered
2004 with the clear direction and discipline required to sustain that momentum. While we have
made great progress, we still have several key restructuring steps to complete.

Williams also is beginning the transition from our multi-year
restructuring to our re-emergence as a company focused on creating
sustainable value for our shareholders. We expect to emerge
with highly competitive, strategically located natural gas assets
and the financial capacity to allow us to play an increasingly
important role in serving the United States growing energy needs.
2003 Financial Performance
Before addressing our restructuring accomplishments, its appropriate
to review our performance on standard financial measures
in 2003. As a result of our restructuring progress, expectations for
our performance have shifted once again toward measures that
reflect the current and future potential of our natural gas businesses
to create economic value. That said, while we did significantly
improve our performance in 2003, we still reported a
net loss of $492.2 million vs. a loss of $754.7 million in 2002.
Unusual items continued to heavily influence our results and are
key to understanding our 2003 performance. In the first quarter,
we recorded an after-tax charge of $761.3 million that primarily
reflects the cumulative effect of adopting the newly mandated
accounting standard for contracts involved in energy trading and
risk management activities.
The exploration and production, midstream and interstate
pipeline businesses that are core to Williams strategy reported
$1.24 billion in segment profit  the same level as in 2002. That
performance is outstanding in the context of last years significant
asset sales in these businesses, and the priority we put on using
cash for debt reduction. We sustained the earnings capacity of our
core businesses by completing expansions on our interstate gas
pipelines, adding production from newly drilled natural gas wells
and continuing our investment in gathering infrastructure in the
deepwater Gulf of Mexico.
Income from continuing operations, which includes our core natural
gas businesses and our power business, moved to marginal
profitability of $15.2 million in 2003 from a significant loss of
$611.7 million in 2002. A major contributor to that change was a
$778.9 million year-over-year segment-profit improvement in our
power business unit. Other factors contributing to the change
include significantly reduced levels of asset- and investmentimpairment
charges, reduced losses associated with interest rate
swaps and lower corporate general expenses.
A detailed review of our financial results is available in the Form
10-K report that follows.
Early last year we outlined our strategy to restructure Williams.
The new Williams will be a strong, but smaller, portfolio of natural
gas businesses in key growth markets where we enjoy the competitive
advantages of scale, low-cost position and market
leadership. And we will support and grow those businesses while
striving to achieve investment-grade characteristics and ultimately,
investment-grade ratings. An essential part of that strategy
is continuing to reduce the risk inherent in our power business
and pursuing a complete exit. While our efforts to exit Power
have been disappointing, our other 2003 accomplishments put
us well on the way to creating that new Williams.
Financial Strengthening
We continued to firm up our financial footing and regained
access to the public capital markets. We repaid $3.2 billion in
debt through both scheduled maturities and early retirements.
We also realized the benefit of the public debt markets
restored confidence in Williams as we refinanced $2 billion of
debt at much more favorable terms. We significantly reduced
our interest expense and lowered the average cost of debt at
year-end 2003 to 7.7 percent from 10 percent a year earlier. At
the end of 2003, we had $2.3 billion in available cash and
cash equivalents. Even with the retirement earlier this month of
$679 million of 9.25 percent notes, we have approximately $1.5
billion in unrestricted cash and cash equivalents.
Asset Sales
Our asset sales program is 90 percent complete. This effort 
which has been key to our debt reduction and the redefinition of
our business focus  yielded about $3 billion in proceeds in
2003 and a like amount the previous year. We are scheduled to
generate approximately $265 million in cash proceeds as a
result of closing the sale of our assets in Alaska, which is
scheduled within days of this writing. We expect to complete our
defined asset divestiture program by year-end with the sale of
$500 million to $600 million of assets that are in our midstream
business but located outside of our concentration areas in the
deepwater Gulf of Mexico and the U.S. Rocky Mountains.
Reduced Cost Structure
In 2003, we made solid progress in reducing our cost structure
and the size of our organization to reflect Williams smaller, less
complex base of businesses. We reduced selling, general and
administrative (SG&A) costs in our continuing operations by 28
percent and reduced general corporate expenses by 39 percent.
Our work force at year-end 2003 was about 4,800  close to half
the size of where we began the year. We will continue to reduce
our costs and our work force in 2004. Asset sales will drive
some of those reductions. Others will come as a result of our
continuing efforts to align our support services with our
restructured company.
Power Business
We made headway in reducing the risks associated with our
power business through the sale, termination or liquidation of
contracts and assets. Those efforts in 2003 yielded more than
$600 million in cash. We also made progress on reducing the
capital required for margins, capital assurances and working
capital. Our inability to fully exit the power business after 18
months of efforts, though, is clearly the part of our restructuring
effort where our progress has been less than planned.
Economic Value
We broadened our performance measures to focus on operating
cash flow and return on capital  and on Jan. 1, 2004, implemented
the Economic Value Added financial management system.
We will use EVA as a financial framework to evaluate our
business decisions and as a key component to determine incentive
compensation. At its most basic level, EVA provides a more
complete picture of financial performance  historical and projected
returns on investment  than accounting measures alone
because it accounts for the cost of all capital. We will invest
selectively in those projects that are projected to add value to
the company through EVA improvement.
Resolving Issues
We made significant progress in resolving litigation and investigations
through settlements with the Federal Energy Regulatory
Commission, with western U.S. states and California utilities and
with the Commodity Futures Trading Commission. We are continuing
to pursue favorable resolution of remaining issues.
Corporate Governance
Our board of directors in 2003 formally adopted principles of
corporate governance that specify how the board will operate, its
structure, committees and other practices. These principles,
along with a code of ethics for Williams chief executive officer,
chief financial officer, controller and others performing similar
functions, are published on Williams web site.
Our Businesses
While we are focusing the majority of our available cash toward
debt reduction, we still are making disciplined investments in our
natural gas businesses that are creating economic value. In the
near-term, we are focusing our investments on maintaining or
improving our favorable market position so that we create opportunities
for more substantial growth in the future.
Our exploration and production business currently is Williams
primary growth vehicle because of the strong returns on additional
investment, a high success rate and a 45- to 60-day cycle
from investment to cash flow. We drill, produce and manage tightsands
and coal-bed methane natural gas reserves in the U.S.
Rocky Mountains and Midcontinent. In terms of scale, our most
significant reserves are located in Colorados Piceance Basin,
followed by the San Juan, Powder River and Arkoma basins.
We increased our level of drilling activity in the fourth quarter last
year, reversing the trend in what had been a divestiture-driven
decline in production. At year-end 2003, we reported 2.7 trillion
cubic feet of natural gas reserves, down 0.1 Tcf from year-end
2002. After adjusting for divested properties, Williams replaced
its 2003 production of 175 billion cubic feet equivalent at a ratio
of 254 percent. Approximately 98 percent of our 2003 U.S. proved
reserves estimates were audited and/or prepared annually by
independent reservoir engineers.
The deepwater Gulf of Mexico is a growth area for our midstream
gathering and processing business. Our existing investments
there generate strong cash flow and provide attractive returns on
investment in excess of our cost of capital. We continue to be
successful in reducing the volatility that commodity prices
introduce into portions of the midstream business by increasing
the share of our volumes that are fee-based. In addition to
the deepwater Gulf, our midstream business has significant
operations with strong competitive positions in the U.S. Rocky
Mountains and the San Juan Basin.
Late last year we began operating our new pipeline that transports
oil produced from a platform in 3,150 feet of water in the
Gunnison Field to our shallow-water platform near Galveston.
We expect to bring our Devils Tower production facility, which
is located in 5,610 feet of water about 180 miles southeast of
New Orleans, into full service in the second quarter this year.
In the Rockies, we just brought into service an expansion of our
processing operations at our Opal, Wyo., facility.
Williams interstate gas pipeline business has a long history
as a steady cash contributor. This business with a cost-based
regulated rate of return includes three pipelines that serve
markets where energy demand growth is projected to continue to
exceed the norm. To meet that demand growth, last year we
brought four system expansions into service  three on our
Northwest system, which serves markets including Seattle and
Portland, and one on our Transco system, whose major growth
markets include New York City and New Jersey. Williams also is
50 percent owner of the Gulfstream system, which we and our
partner are expanding so that we can serve central Florida,
increase system utilization and improve results.
Challenges in our gas pipeline business last year centered around
two line breaks on our 26-inch pipeline that traverses western
Washington state, resulting in that pipeline being idled for testing
and repair work. We are meeting market demand today through
our 30-inch system that runs parallel to the 26-inch line and are
working with the Office of Pipeline Safety and others to determine
when we will bring the repaired line back into service, albeit at a
lower pressure, to serve higher summer demand. Our more comprehensive
solution calls for new infrastructure within three years
to replace the capacity historically served by the 26-inch pipeline.
We expect to complete the replacement during late 2006 and file
a rate case that would allow us to recover the costs, including a
return on capital in future rates.
We are continuing efforts to sell the remainder of our power
business, but the timing and value of a sale is uncertain. Our
continuing interim strategy is to operate our power business in a
manner that reduces risk, generates cash and honors our contractual
obligations. We have hedged revenues, of varying degrees
of certainty, to cover approximately 98 percent of the annual
demand payment obligations through 2010 that this business
carries as a result of tolling contracts. Indeed, it is the unhedged
portion of our obligations in the years 2011 through 2022 and the
weak power market that have served as a barrier to a sale during
the last 18 months.
2004 and Beyond
Our goals are clear. The progress weve made toward strengthening
our finances since this time last year defines the kind of
discipline we will continue to exercise this year and in the years
ahead.
On the financial front, we expect to continue the progress weve
begun to reduce our debt  both through scheduled maturities
and early redemptions. Our goal is to pare our debt during the next
two years while at the same time maintaining adequate liquidity
and investing in our businesses. Also, we expect to capitalize on
our sharply improved financial condition and prospects by establishing
new credit facilities at more favorable terms.
These efforts all are important contributors to our goal of achieving
investment-grade credit characteristics.
We will make disciplined investments with a focus on balancing
the risk/reward characteristics of our core businesses and generating
cash. Our plans for 2004 include participating in 1,400 new
natural gas wells and continuing the growth of our midstream
business in the deepwater Gulf of Mexico.
Williams is on track to complete our cost reductions and the
realignment of our work force with our smaller, less complex
company.
We are continuing the momentum of our restructuring. Each
achievement moves us closer to our goal of being a strong, integrated
natural gas company with the right assets, the right
strategy and the right financial capability to seize growth
opportunities that create sustainable value.
We remain confident that we can build value over the long term
and we look forward to your support.

Steve Malcolm
Chairman, President and Chief Executive Officer