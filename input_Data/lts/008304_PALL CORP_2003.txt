TO OUR SHAREHOLDERS
Fiscal 2003 was an excellent year for Pall
shareholders. Here are the headlines. Sales up 25%. Earnings increased
47% and earnings per share grew from 82 cents in 2002 to $1.20, on
a pro forma basis. What began with many market uncertainties and
macroeconomic question marks ended as a banner year. What those
challenges were and how we met them is a story worth telling.
In April 2002, we made the largest and most
complex acquisition in our history. We purchased the Filtration and
Separations Group (FSG) from the USFilter Division of Vivendi Environnement
for $360 million. FSG was an amalgam of five companies that had been
assembled over the previous five years. The
acquisition offered great promise but only if
costs could be cut and FSGs operating margins,
half Palls, rapidly improved without harming revenues.
The business case was clear and compelling.
FSG offered products and technologies that were mainly complementary
to Palls. Their products were sold mostly into Palls major markets but for
different points in the customers manufacturing process. Two of the FSG
plants produce ceramic filters, materials that Pall previously outsourced.
Our initial cost reduction estimates were for
$15 million over a two-year period and this was soon raised to $30 million.
Remarkably, we achieved this run rate in the third quarter and now
are working to exceed the revised target. We assembled a powerful
team, led by Donald Stevens, our COO, to manage the integration. This
same team is now focused on additional cost reduction initiatives.
Cost reduction/productivity improvement is a
key part of our culture. Your companys management sees ongoing and
substantial opportunities to continue reducing costs in ways that improve
our processes. Manufacturing and infrastructure rationalization are key
drivers for future savings. Aggressive cost reduction programs are making
Pall a faster innovator of products and an ever-more-capable supplier of
goods and services.
Beneath the complex mix of end markets and
rich diversity of products, Palls business is simple. We filter, separate and purify the constituents of liquid and gas streams. Originally, the
Earth was entirely composed of fluids. While there has been some cooling
and crusting over, there are still more than enough fluids left over to
provide a world of opportunity for our business.
Novel fluid compounds are invented every day.
In biotechnology, small molecules and proteins
are being developed and refined at a staggering
rate. In chemicals and microelectronics, new
formulations are changing the way products are
made and how they perform. Exotic materials are also staples of the aerospace
and medical industries.
Coatings, fuels and lubricants are fluids used
in every manufacturing process. Products themselves are almost always
manufactured in their liquid or gaseous state. Metals and plastics are
good examples. So are complex items such as microchips and drug
tablets. Whatever the fluid, Pall technologies provide the means to
achieve consistent and efficient production.
We are told that no two snowflakes are the
same. That may well be true. But Pall scientists know that no two
droplets of water are identical. They prove it in our laboratories every day. No two customer problems are alike either. So, the support of our
Scientific and Laboratory Services group is crucial in providing customized
solutions to individual customers.
One of the first science lessons I recall is that
water is the universal solvent. It is the most common fluid that Pall filters
(blood is second), and among the dirtiest. Every second hospital
bed in the world is occupied by someone who became ill because of
polluted water. Our water strategy is exemplary of how we approach
each of our markets.
Every Pall customer has stringent requirements
for their water. Palls broad water processing platform leverages
core technologies to meet these vast and varied requirements. Our
hollow fiber membranes purify the water that flows through municipal
systems and into our homes. Palls liquid/liquid coalescers remove
water from water-loathing fluids like jet fuel. We meet the most critical
requirements for sterility and purity in medical, pharmaceutical and
food and beverage production.
Whatever the fluid, Pall provides Total Fluid
Management (TFM). Typically, Pall filters fluids
as they are prepared to enter a production
process, then again as they course through it,
and for a third time, to condition the final
waste streams so they can be recycled or
safely discharged. TFM allows customers to
rationalize their manufacturing processes and enables us to design the
most economical and comprehensive filtration, separations and purification
systems for their applications.
TFM benefits customers in other key ways.
Customers have one filtration company to call for the design and support
of their integrated systems. And customers can relocate or replicate
their production operations anywhere in the world with the comfort of
knowing that local Pall support is nearby.
We leverage TFM through our first-rate global
engineering organization. It designs elegant solutions for the most
intractable contamination problems, gets them built, assists with
start-up and then provides ongoing service. This last area is an
increasingly important focus for revenue growth. We are seeing increased demand for our paid services in areas like consulting, filter
validation and plant audits.
Palls systems business now represents about
10% of sales. As the installed base of systems increases, so do the
annuity sales for disposable filters within them. We believe that we
reached the inflection point in fiscal 2003, where lower margin systems
sales were compensated for by more profitable replacement sales. So,
the margin contribution from these sales will continue to grow.
Now lets look at the performance of our
business segments. Aerospace, Palls oldest business, had a wild ride.
While the Commercial Aerospace business remained level, Military
Aerospace grew dramatically. About $15 million of this was a war dividend
relating to the Iraqi conflict. Interestingly, most of the extra
sales came out of Europe.
For 2004, we expect, at a minimum, to see
continued growth in the base military business and a recovery in the
distressed commercial aviation markets. Pall has had unprecedented
success the past year in winning the filtration contracts for each of the
major new airframe build programs, from both military and commercial
customers, around the world.
Microelectronics posted acquisition-assisted
sales growth of 37%. This notoriously cyclical market has begun a slow
recovery, which we expect to accelerate as we proceed through the next
calendar year. The commitment of major capital to the next generation
of semiconductor chips, when it comes, will add real bulk to sales.
General Industrial was another beneficiary of
the acquisition. Here sales grew 46% in the year. Within this segment,
Water Processing is still our fastest growing business. Many of the other
basic manufacturing sectors served by this group, pulp and paper and
power generation as examples, were depressed. Many factors lead us to
expect improved market conditions this year.
On the Life Sciences side, the Medical business
focused on consolidating operations with the goal of dramatically
improving profits. Despite essentially flat revenue growth, Medical
achieved the goal and increased operating margins from 13.9% to 18.2%
in a single year.
Major new product introductions and growing
demand for existing products are expected to boost sales in 2004. The first of these new products is the Pall BDS bacteria detection system. As
Palls first entrance into the blood diagnostics market, it detects bacterial
contamination of blood products before they are transfused to a patient.
With so much awareness of the risk of viral
transmission from blood transfusions, most people are shocked to learn
that it is actually bacterial contamination that causes the most morbidity
and mortality. With one simple test, the Pall BDS system can detect
the 10 key bacteria implicated in transfusion-associated illnesses.
The BioPharmaceuticals Group had a great year,
growing 20%. Its fortunes are closely tied to the nascent biotechnology
industry. The drug development pipeline from discovery to full-scale production
has always been promising and now, it is increasingly productive
as the FDA approves more biotech drugs for commercialization. This
industrys appetite for Palls technologies is enormous and so are our
expectations for this Group.
So essentially, 2003 was a year in which
organic growth, an acquisition and favorable
currency rates all contributed to a jump in revenues.
It was also a year of great earnings
growth. With a few exceptions, we achieved
decent results despite relative stagnation and lackluster fundamentals in
many of the key European and North American markets we serve.
Asia continues to be a driving force for
growth and represents about 20% of total sales. Now at $312 million,
that number approximates what our sales in the Western Hemisphere
were just nine years ago. We will continue to increase our already substantial
presence in Asia in both sales and manufacturing. Pall is also
purchasing the remaining shares of its India joint venture and plans
another expansion of its China manufacturing operation.
At the end of this fiscal year, our President and
long-time colleague Jeremy Hayward-Surry retired from the company.
He will also be leaving his seat on our Board this November. Jeremy has
been a major contributor to Palls growth and success and, a good
friend. His business acumen and good humor will be missed.
We are fortunate to have a highly talented Pall
veteran to take on the role of President. Marcus Wilson has managed our
European operations to great effect and been a key member of our
Operating Committee for a number of years. He recently relocated to the United States and is applying his considerable talents to propelling Palls
global business. Marc has been nominated to join our Board of Directors
beginning in November. Donald Stevens, who so ably led our FSG integration
efforts, has assumed the new post of Chief Operating Officer.
Our management team is fully supportive of the
broad legal and regulatory initiatives directed at strengthening the corporate
governance of publicly traded companies in the United States. Pall has
a well-earned reputation as a fiscally conservative company which operates
to the highest ethical standards. Our Board of Directors is active and firstrate.
This company is acutely aware of
its responsibilities to shareholders,
employees and the communities in
which we operate.
Pall has developed a unique recipe to
succeed in the global filtration, separations
and purification marketplace.
The TFM approach lets us leverage
new product platforms, systems
engineering and services over our
many diverse markets. Common technology platforms, integrated
manufacturing, and shared infrastructure allow the sharp end of Pall to
focus on customers in our many specialized markets and do so efficiently.
This strategy gives us a wealth of continued opportunity for
new product development, market expansion and cost reductions with
improved productivity.
And so, we look confidently at the present and
eagerly to the future.

Chairman and Chief Executive Officer