Letter from the Chairman 
Dear Arconic Shareholder, 
Your Company has completed its first full year since the November 1, 2016 separation of Alcoa Inc. into Arconic and Alcoa Corporation. Its been a productive year with solid shareholder returns, a stronger governance profile, the addition of five board members, and the appointment of 
a new CEO with ideal credentials. 
The changes instituted in 2017structural, governance, and leadershipbetter prepare Arconic to build on its many strengths and to create sustainable value for its shareholders. 
In 2017, Arconics total shareholder return was 48% compared to 22% for the S&P 500 Index. That performance reinforces the fundamental soundness of Arconics products and technologies. Arconics potential to capitalize on those strengths under new leadership provides an opportunity for further value creation. 
To increase accountability and participation for our investors, weve made changes to Arconics governance policies since the separation. Our decision to reincorporate in Delaware provides Arconic access to well-developed corporate law, and it allows the Company to institute more robust shareholder protections and rights. For example,to increase the influence of Arconics shareholders, weve eliminated the former supermajority requirement that 80% of shareholder votes are necessary to approve many major decisions; now such decisions are approved with a simple majority vote. To increase accountability of Arconic 
directors, we eliminated our staggered board structure and shareholders now vote on every director every year. Arconic has also adopted a practice called proxy access that enables shareholders owning at least 3% of the Companys common stock for at least three years to include their nominees for election to the Board in Arconics annual proxy statement for the annual shareholders meeting. 
In 2017, five new directors joined Arconics Boardof Directors, and seven have experience and unique qualifications in Arconic's largest marketAerospace and Defense. We were fortunate that one of those new directors stepped up as Interim CEO. David Hess, former Presidentof Pratt & Whitney, led Arconic for nine months. David continues as a director following our new CEO appointment. 
In the fall, the Board chose Charles Chip Blankenship to be the next Arconic CEO. He has the perfect combination of experience, expertise and education to lead Arconicand serve its shareholders. His affinity for advanced technologies, operational execution and financial management enabled him to rise through the ranks of GE to become Vice President of the commercial aircraft engines business. Then, as CEO of GE Appliances, he reshored manufacturing to the U.S. and led a sale process to Haier, a global appliance company. Chip is uniquely qualified to capitalize on the dramatic impact that materials technology is having on the aerospace, automotive, and industrial markets where Arconic competes. 
The changes instituted in 2017structural, governance, and leadershipbetter prepare Arconic to build onits many strengths and to create sustainable value for its shareholders. On behalf of your Board of Directors, 
I commit that we will focus on that goal. 
JOHN PLANT 
chairman of the board 
02 
Letter from the CEO 
Dear Arconic Shareholder, 
It was my honor to join Arconic as your new Chief Executive Officer in January this year. I believe our Company has potential, built on the strength of our team, our capabilities, and our customer relationships. Our challenge now is to reinforce these strengths, close gaps, and identify new opportunities to assume our rightful position as the industry leader everywhere we choose to compete. To that end, I have initiated a review of Arconics strategy, which I expect to complete by the end of the year. My commitment to you, our shareholders, is to ensure that all of our businesses execute well. 
In my first 100 days as CEO, I am visiting at least 20 facilities and meeting with at least 14 of our largest customers. My plan for the foreseeable future is to continue meeting with customers, industry experts, and employees, so I can learn more about the capabilities of our businesses, technology, team, and functions. I have a lot of questions, and I intend to do a lot of listening. 
I believe a focus on four key areas will be important for our long-term success: customers, people, operational excellence, and technology. 
CUSTOMERS: We have a relentless passion to win in the marketplace and deliver on customer commitments. 
As a former customer of Arconics aircraft engine business, I saw firsthand the opportunity we have to grow with our customers across all our markets. We must have deep relationships with multiple functions in each customer 
organization, forged over time, that lead to understanding how we solve their biggest problems and make their products and business results better. 
PEOPLE: People make all the difference in any company. 
Having the right talent in the right place, aligned and rewarded as a team, is important to our success. Fostering an inclusive environment for all team members is extremely importantit starts with the way we lead and show respect for each other. 
I believe our Company has potential, built on the strength of our team, our capabilities, and our customer relationships. 
OPERATIONAL EXCELLENCE: We will focus on Safety, Quality, Delivery, and Cost. 
A robust continuous-improvement business process across the Company, using lean principles, will allow us to achieve workforce engagement, new levels of problem-solving accuracy and velocity, process control, and quality of delivered product. Our goal is to be the industry leader and sustain that leadership with continuous improvement. 
TECHNOLOGY: The right technology investments help us win more business and achieve profitable growth. 
Creating relevant, differentiated technology that solves our customers biggest problems or makes their products more valuable is one way to win with technology. Another way we win is by developing technology that makes us more competitive on quality, delivery or cost in the marketplace. Our goal is to succeed on both fronts. 
As part of our drive to continue reducing overhead, in February I announced that we will relocate our global headquarters away from New York City to a more cost-effective location with good access to talent and transportation. We expect the move to take place by the end of this year. 
ARCONIC | 2017 ANNUAL REPORT | 03 
I am convinced that if we stay focused onfour prioritiescustomers, people, operational excellence, and technologywe will deliveron Arconics potential. 
The year 2017 was one of exceptional change for us as a new standalone company. The resilience and dedicationof Arconics employees and leaders throughout that turbulent year is a testament to our strong values orientation. The safety and the well-being of our employees remain paramount in all we do. Underpinning all Arconic values 
is our commitment to integrity, which is anchored by a strong ethics and compliance program. To me, integrity also implies accountability for meeting commitments. Our ability to deliver on those commitments, very simply, comes down to execution. And that is our key focus. 
I would like to thank David Hess, who served as Arconics Interim CEO through most of 2017. He strengthenedthe management team, shored up relationships with customers, and provided the stability of leadership that Arconic needed. We are fortunate to have his continued counsel, alongside that of our new Chairman, John Plant, and the rest of the Arconic Board. 
I am convinced that if we stay focused on four prioritiescustomers, people, operational excellence, and technologywe will deliver on Arconics potential. 
Thank you for your continued support. 
CHARLES CHIP BLANKENSHIP 
chief executive officer 
