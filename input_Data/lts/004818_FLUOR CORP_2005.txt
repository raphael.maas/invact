
DEAR VALUED SHAREHOLDERS

I am pleased to report that 2005 was a very successful year on a number of fronts,
and the company has never been better positioned to capitalize on its position as the
preeminent global provider of engineering, procurement, construction and maintenance
(EPCM) services. Building on substantial new awards in 2004, earnings grew for the
fifth consecutive year with the company posting record earnings on a substantial
increase in revenues.




I am particularly encouraged by our continued success in winning new projects. With an unprecedented level of
capital investment in many of the industries we serve, the entire Fluor team is focused on successfully meeting the
challenge of delivering growth and value to our customers and shareholders.

2005 OPERATING PERFORMANCE

With a substantial pick-up in project activity, Fluor posted strong double-digit improvements in revenue, earnings and
earnings per share during 2005. Revenues grew 40 percent to $13.2 billion while net earnings increased 22 percent
to $227 million, or $2.62 per share. This compares with net earnings of $187 million, or $2.25 per share, in 2004.
     We are very encouraged by these results, with strong operating performance from four of our five business
segments. Operating profits from Oil & Gas rose 50 percent, reflecting outstanding performance in a robust global
marketplace driven by escalating world energy demand. Global Services posted a double-digit increase in a strong
market for construction and maintenance services, while our Government unit enjoyed another positive year with
significant contributions from the Department of Energy (DOE), reconstruction work in Iraq and disaster recovery
work for the Federal Emergency Management Agency (FEMA). The Power business produced profits on par with
2004, positioning itself for an upturn in coal-fired power plants and environmentally driven scrubber programs.
The performance of our Industrial and Infrastructure (I&I) group resulted in a loss in that unit for the year, and
as a result we have strengthened the management team and refined the business model, which we believe will
substantially improve their results going forward.
     We also continued to win very high levels of new project awards across our diversified portfolio of businesses,
booking $12.5 billion  slightly below last year's company record of $13.0 billion. All segments made significant
contributions with Oil & Gas leading with over $4 billion of new bookings, I&I, Global Services and Government each
with more than $2 billion, and Power with over $1 billion. Fluor's backlog of business grew for the third straight year,
to $14.9 billion, with a number of large prospects in the pipeline for 2006.


    



                                                                     activity in the Middle East, Europe, Asia, Canada and the United
          
                                                                     States. Our clients plan to invest billions of dollars in expanding
                                     gas supplies, including new liquefied natural gas (LNG) capacity,
                                                                     and for the first time in decades, in new oil refinery facilities
                                                                     to meet ever-increasing global demand. The market for petro-
                                                                     chemicals is also extremely active, with demand stimulating
                                                                     a large wave of investment.
                                                                           The market for new coal-fired power generation continues

                                                                     to progress. While lead times are long and permitting is a

                                                                     key issue, we were released to begin construction on one coal-
                             fired plant during 2005 and are pursuing several additional


                                                                     major prospects. Demand for the environmental remediation
      Earnings were helped by the resolution of the longstanding     of pollutants from existing coal-fired facilities is another growth
claims on the Hamaca crude upgrader project in Venezuela,            area with great potential.
but hurt by the settlement of a legal dispute involving a hotel            The I&I segment, serving a wide range of industrial and
project in the Caribbean and provisions for losses on several        infrastructure markets, has a number of opportunities going
projects in I&I and Government. We believe that our strategy         forward. During 2005, the unit made good progress on the
of selectivity, combined with our rigorous risk management           major copper mining projects that were booked in 2004 and
processes, continue to serve us well. However, we must remain        on iron ore work won during 2005. We continue to track a
diligent in executing all of our projects well, and will move        number of major new prospects to expand copper, iron ore,
forward with an ever-sharpened focus on our clients, execution,      nickel and gold production capacity worldwide. Fluor is a leader
and the fundamentals of our business.                                in this market, and we expect to continue to capture profitable
      I am also proud to note that our world class safety            market share.
performance continues to lead the industry. Our commitment                 Large infrastructure opportunities are progressing in
to the health and safety of our employees and subcontractors         the United States and Europe and Fluor's strategy of pursuing
is our number one priority each and every day.                       complex, privatized design/build projects represents a strong
                                                                     growth area.
BUSINESS STRATEGY AND OUTLOOK
                                                                           We continue to make measurable strides in our strategy to
Fluor's strategy is to maintain and grow a diverse portfolio         grow our share of less-cyclical markets thereby establishing a
of global businesses focused on the EPCM marketplace and             significant base of continuing business. We view the U.S.
capitalizing on the company's industry and geographic                Government as a key market where Fluor can profitably grow
strengths. Many of our markets are cyclical by nature, yet our       over the long term. Our work for the DOE 's nuclear remediation
challenge is to grow the overall business on a consistent basis.     programs is progressing with superior results, and we are
We have achieved consistent growth by maintaining industry-          very proud to report that after 13 years at Fernald, Fluor
leading positions across a combination of stable non-cyclical        will successfully close the site in 2006, years ahead of schedule
markets, such as government and operations and maintenance,          and substantially under budget. Leveraging our experience and
as well as traditionally cyclical markets like oil, gas, petro-      outstanding reputation, we plan to pursue several new contracts
chemicals and power. We are extremely well positioned globally,      which the DOE is expected to re-compete in 2006. In Iraq, our
which enables us to capitalize on developing growth markets.         team has earned high marks on our reconstruction work while
     The outlook for the majority of Fluor's markets remains         maintaining an outstanding safety record in this very challeng-
positive, with exciting prospects in the oil, gas, petrochemicals,   ing location. We are also proud to have been selected by FEMA,
power, mining, transportation and government markets. We             a long-term Fluor client, to support their disaster recovery
continue to expand globally, with about two-thirds of our new        efforts following hurricanes Katrina and Rita.
awards and backlog coming from projects outside of the                     Our most stable business, Global Services, comprised of
United States and nearly a quarter of new business coming            our operations and maintenance, procurement and construction
from the Middle East.                                                services, equipment and tool leasing, and temporary employee
     Driven by strong economic and market supply/demand              businesses, has established itself as a strong growth contributor.
fundamentals, our Oil & Gas business is up significantly.
Opportunities across the full spectrum of upstream and
downstream processing industries are plentiful, with major





The unit is winning long-term contracts with major industrial             Our Board of Directors continues to provide valuable
clients, and will also grow as a result of serving Fluor's overall   guidance and insight, and I am very pleased with their wealth
EPCM business groups.                                                of knowledge and their individual and collective strengths.
     With most of our markets in an upturn, we continue to be        In April 2005, we announced the election of Peter S. Watson to
very encouraged by the outlook for significant ongoing client        Fluor's Board. Peter recently served as chairman, president
spending across our portfolio. Continuing strength in new            and CEO of the U.S. Overseas Private Investment Corporation.
awards and a strong backlog are expected to drive growth                  At year-end, Dr. David Gardner, one of our longest-tenured
in 2006.                                                             directors, retired from the Board. With a passion for education
                                                                     and depth of experience as the former president of both the
FINANCIAL STRENGTH
                                                                     University of California System and the University of Utah,
                                                                     David made many invaluable contributions, particularly
Fluor's financial condition was further strengthened during
                                                                     around the development of people resources. As chair of
this past year. Our balance sheet was materially improved with
                                                                     the Governance committee, he has also helped Fluor during
the resolution of the Hamaca claims, eliminating this major
                                                                     a period of rapidly evolving corporate regulations. We thank
uncertainty and converting tied-up working capital to cash.
                                                                     David for his distinguished service to Fluor's shareholders
Bolstered by positive contributions from operations, our cash
                                                                     since 1988.
position grew from $605 to $789 million during a period in
which we paid off our commercial paper borrowings and
                                                                     MEETING THE CHALLENGE
reduced our debt-to-capital ratio to 21 percent, from just
over 26 percent a year ago. We are positioned to use our ample       In closing, the demand for quality EPCM services is growing
resources to support both future organic growth initiatives          at a very rapid pace, and most of that growth is occurring
and potential acquisitions.                                          outside of the United States. Our capabilities and diversity are
                                                                     unmatched globally, which is why I believe that our prospects
FIGHT AGAINST CORRUPTION
                                                                     and potential have never been better. Fluor had a very good year
                                                                     in 2005, and I am excited about the considerable opportunities
For the past three years, Fluor, along with Newmont Mining
                                                                     in 2006. With the talent and dedication of our people and the
and Petronas, has led the World Economic Forum's Partnering
                                                                     trust of our clients, there is no doubt that we will successfully
Against Corruption Initiative (PACI). This set of business
                                                                     meet the challenges that lie ahead and continue to serve you,
principles is intended to combat corruption and bribery has
                                                                     our valued shareholders.
been signed by nearly 100 global companies representing over
$500 billion of annual revenue. By signing, the leaders of each
company commit to a no-bribe policy and also commit to
installing preventative systems and procedures to guard against
corruption.

FLUOR MOVES                                                          Alan L. Boeckmann
                                                                     Chairman and Chief Executive Officer
In May of 2005, we announced that we will be moving our
                                                                     March 1, 2006
corporate headquarters from Aliso Viejo, California to the
metroplex of Dallas,Texas. This move was prompted by the
improvements in operating efficiency that can be gained from a
central time zone with better travel access and communications
with our business constituents. Our new facility in the Dallas
suburb of Las Colinas is under construction and will be occupied
in late April, 2006. Fluor will continue to have a significant
engineering presence in Southern California.

ACKNOWLEDGEMENTS AND APPRECIATION

Fluor employees are doing an outstanding job during this period
of unprecedented growth. Their dedication to delivering quality
services to our valued clients will continue to be the cornerstone
of our success and world-class reputation in the industry.


