THE POWER OF INVESTMENT

DEAR SHAREHOLDERS, CUSTOMERS
AND EMPLOYEES:
Theres no better way to unleash the potential of a company than to engage smart,
accomplished people and give them the opportunity to add value and be held
accountable for their contributions. Since taking over as CEO at Berkshire 14 years
ago, Ive made it a priority to build a culture where employees are passionate about
their work, excited about confronting challenges and driven to achieve more than they
ever thought possible. Its this investment in developing the America's Most Exciting
Bank culture that transformed our bank into a regional franchise, positioning us as a
preferred financial partner to customers across New England and New York.
Its also this investment that has helped us to achieve double-digit returns for our
shareholders in 2015. For the year, we recorded 18% revenue growth, 16% core EPS
growth, double-digit loan and deposit growth, and substantial improvements in our
profitability ratios. Our core results improved steadily in every quarter. We bolstered
our presence in the middle of our footprint through the acquisition of Hampden
Bancorp and diversified our business lending through the expansion of our SBA lending
program and the acquisition of Firestone Financial. In short, I'm pleased with our
results and our momentum heading into 2016.
Growing Our Franchise
The last year was marked by solid organic growth, expanding margins and disciplined
acquisitions. We continued to enhance our presence across the footprint, investing in
employees and communities, and establishing ourselves as a significant contender in
newer markets. We are actively growing our influence in Eastern Mass., Connecticut
and New York, and helping to fuel the vitality of these regions, while maintaining our
commitment to our long-standing markets of Western Mass. and Vermont. 

CULTURE As America's Most
Exciting Bank, we
are passionate about
practicing and living
by the RIGHT values
(Respect, Integrity,
Guts, Having Fun
and Teamwork).
FOOTPRINT
We offer services
across New England
and New York
reaching customers
from Boston to
Syracuse and multiple
regions in between.
VISION
By bringing together
a strong employee base,
a dedicated strategy
and operational
efficiency, we can
offer our customers
the products and
services they need,
our employees an
inspirational place to
work, our communities
the support they are
looking for and our
shareholders the
returns they deserve

Berkshires total loan growth for the year was 22%,
including 8% organic growth coupled with contributions
from the Hampden and Firestone acquisitions. Organic
commercial loan growth measured 9%, and our teams
continued to drive relationship-based C&I business across the
region. We enhanced our commercial lending teams, brought in
a new auto lending leader to build relationships throughout our
footprint and continued to expand our residential lending group.
Meanwhile, deposit growth kept pace, growing 20% year-overyear,
including double-digit organic growth in demand deposit
balances. We continued to enhance our products and services
suite for our deposit customers with the addition of Apple Pay*,
instant issue debit cards and an instant customer feedback
tool available in branches. We brought in a new leader for our
cash management team and rolled out additional services for
our business customers. We also expanded our private banker
and MyBanker groups across the footprint. These teams work
hand in hand with our wealth and commercial teams to provide
concierge-level service to our customers.
During the second quarter, we closed and integrated the
acquisition of Hampden Bank. This brought with it seven
new branches, a strong customer base and well-established
commercial and business banking teams. It also bumped us into
the top four in deposit market share in the Springfield, Mass.
region and positioned us well to stay competitive in that market.
We followed up with the acquisition of Firestone Financial, a
specialty equipment financing company headquartered in
Needham, Mass. The Firestone team, made up of disciplined and
dedicated lenders who bring years of experience in specialty
lending, has already made an impact, helping us to diversify our
loan portfolio and our product suite. Both deals were accretive
to earnings, capital and profitability measures, and are good
examples of our disciplined acquisition strategy.
On the business banking side, we've been focused on
streamlining our processes and upgrading our offerings, and
we're proud of what the team has accomplished. For the fiscal
year 2015, Berkshire was named the #1 Small Business Lender
in Western Mass., the state of Vermont, and the Albany to Syracuse
region of New York. We also announced the planned addition of a
nationally ranked SBA team based in the Philadelphia, Penn. area and
are excited about these continued prospects.
We upgraded and expanded our wealth management group in 2015,
bringing in veteran portfolio managers and trust advisors, and
promoting a new leadership team. The enhanced team has strong
ties in our communities and produced good results last year, beating
the benchmarks in a tough market. We also continued to build on the
insurance side, where we've added a new benefits team and increased
our product offerings.
Importantly, weve expanded our margins and improved our
profitability through all of this growth. We ended the year with doubledigit
improvement in both return on assets and return on equity. Our
efficiency ratio has improved to just over 60%, and our tangible book
value per share grew by 4%, including the impact of acquisitions. At
least as important was our ability to expand our net interest margin in a
competitive, low-interest-rate environment. I'm proud of how hard our
teams worked to accomplish this, focusing on extremely disciplined
growth throughout the year.
Contributing to Our Communities
Having a positive impact on the communities we serve is integral to
the values of our bank. Our employees take pride in the work they do
to support local charities, and we do our best to support their efforts
through paid time off for volunteering and by running a foundation that
has funded millions in community grants across our footprint. In 2015,
we added to our program with focused efforts on supporting local
veterans, active military and their families through volunteer projects,
grants, and new product and service offerings. We also launched a
corporate initiative to become a more environmentally sustainable 

company, with goals to reduce our carbon footprint
in the coming years.
Investment in People
One of the most important things we did this year
was strengthen the leadership team through the
reorganization of responsibilities and subsequent
promotions at the executive level. In September,
Richard Marotta was promoted to President of the
Bank and Sean Gray was named Chief Operating
Officer. Each has been essential to the growth of this
company, and their promotions reflect that fact.
We also expanded the executive team, promoting eight
members to executive vice president, representing
each of our primary business lines and the roles
they play in this company going forward. Each of
these new executives have demonstrated strong
integrity and high energy, and have new ideas to drive
higher performance. Each is also deeply committed
to the AMEB culture and to fostering growth and
opportunities for employees at this company. As I
said at the beginning of this letter, I don't think there's
a better way to unleash the potential of a company
than through engagement and accountability, and
I'm excited about our future opportunities, having
empowered this kind of talent and energy

Coming Full Circle
As I look back over 2015, I'm happy with the results we've produced. Its been a year of smart, disciplined growth.
We have the right people in place with the right priorities to take this company to the next level. As a fellow
shareholder, I'm pleased with the double-digit stock return this year. Our tangible book value continues to grow as
does our market cap and the liquidity in our stock. We raised the dividend by 6% at the beginning of the year and
followed that up by another 5% raise at the beginning of 2016.
I'm also excited about our prospects for the year ahead. Were focused on growing revenues, maintaining our
expense discipline, and improving our profitability and shareholder returns. We take very seriously our promises to
deliver for our shareholders, and I feel it's important that we continue to listen to and engage them.
Weve worked hard to create a culture and build a footprint that sets us apart from our competitors, and I believe
those things will continue to be differentiators for us. I'm confident about our future opportunities and our ability to
capitalize on them. With your support, I'm looking forward to continuing to grow and strengthen this franchise.
Sincerely,
Michael P. Daly
President & CEO of Berkshire Hills Bancorp, Inc

