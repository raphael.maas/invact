Fellow Shareholders:
Prudential Financials life as a public company began
with a historic and very successful IPO in 2001. In 2003,
our second full year under public ownership, we continued our positive momentum, delivering solid financial
results and created substantial value for shareholders.
On a before-tax adjusted operating income basisa
financial measure we use to analyze operating performanc e*our  
Businesses (FSB) earned $2.0 billion.
Based on after-tax adjusted operating
income, earnings per share of Common
Stock were $2.54, an increase of 23 percent over the previous year. 
On a generally accepted accounting
principles (GAAP) basis, our FSB generated net income of $1.0 billion, or $1.98
in earnings per share of Common Stock.
Again, a substantial year-over-year
increase.
Throughout 2003, we continued to
demonstrate efficient capital management that sustained our already strong
balance sheet and capital position. We
repurchased $1 billion in Common
Stock, while maintaining the firms
financial fiexibility. A buyback of $1.5
billion was approved by the Board of
Directors for 2004. In November, we
raised our dividend to $0.50 per share of
Common Stock, a 25-percent increase,
which was an important expression of
our commitment and ability to distribute
income to investors.
Our financial strength is reflected in

Standard & Poors affirmed all ratings on Prudential
Financial, including its positive outlook on Prudential
Insurance. In early 2004, A.M. Best upgraded
Prudential Insurances financial strength rating to A+
(Superior) from A (Excellent).

investorsis expressed in our stock price. Shares of
Prudential Financial Common Stock rose 31.6 percent
over the 12-month period, significantly outperforming
the broad market, which rose 26.4 percent as measured
by the S&P 500. It was the second consecutive year that
Prudential has outperformed the S&P 500.
Growing our core domestic 
businesses through acquisition
In May of 2003, Prudential Financial finalized the acquisition of American Skandia. This transaction propelled
us into the top-10 of annuity providers, and established
Prudential Financial as a leading distributor of variable
annuities through independent financial planners in the
United States. The company is now poised to become a
leader in the annuity market by virtually any measure.
In November, we announced an

Corp.s retirement business. When
complete, this transaction will position Prudential Financial as one of
the nations leading providers of
retirement products and services. It
will add significantly to the companys assets under management,
further strengthening our position
as a global asset manager.
Making necessary changes
in underperforming
businesses
In July, we combined the retail 
Securities with Wachovia Securities,
and retained a 38-percent ownership in the combined business. This
created the nations third-largest
retail financial advisory organization. The combined entity offers
greater scale and profit potential,
and it presents significant product
distribution capability.
In May, we also made the difficult decision to divest
our property and casualty companies. We are pleased
that the buyers are fully committed to the property and
casualty business, plan to continue offering auto and
homeowners insurance through Prudential agents, and
offered continued employment to most of our employees.
These two transactions underscore an important
Prudential Financial commitment: We want to be in businesses that contribute to our return-on-equity goalsand
that present us with an opportunity to achieve market
leadership. If a business does not fit this profile, we will
evaluate it and take necessary actions. 
Enhancing domestic product and distribution
In July, we reorganized our proprietary mutual funds
under the brand JennisonDryden Funds. This unique
fund family leverages the capabilities of three respected
investment managers with distinctly different investment disciplines: Jennison Associates, Prudential Fixed
Income and Quantitative Management. As a complement to JennisonDryden Funds, we offer Strategic
Partners Funds, which feature high-quality investment
managers from throughout the industry. Together,
JennisonDryden and Strategic Partners present extraordinary choice and value for investors and investment
professionals alike.
We also made significant product enhancements in
the domestic life insurance segment, introducing new
Universal Life products and re-pricing our Term portfolio. As a result, both products now claim a larger
percentage of our sales. 
In 2003, we made notable improvements to retail
distribution, growing our third-party channel and
strengthening our own sales force through highly selective recruitment and rigorous training programs.
In the Group Insurance segment, we continue to seek
controlled growth, retaining and adding business that will
enhance both profitability and scale. The Group
Insurance business delivered strong performance in
2003 and is well positioned for success in the years ahead.
Expanding our powerful international presence
In 2003, our International Life Insurance business had another
very successful year, accounting for more than 40 percent
of the companys before-tax adjusted operating income.
Our life planner force, made up of highly trained insurance professionals, grew more than 10 percent with
double-digit increases in our two major markets, Japan
and Korea. Globally, new annualized premiums for the life
planner businesses grew 13 percent on a constant currency
basis, with Japanour largest life planner marketgrowing at 20 percent. Our life planner activities continue to
set high standards for service, generating excellent results
in sales, customer satisfaction and earnings.
Gibraltar Life, our traditional Japanese insurance
business, also had a very solid year. On a constant 
currency basis, new annualized premiums increased 18
percent and adjusted operating income was 3 percent
ahead of 2002. Gibraltars success reflects our dedication
to quality recruitment, sales training and product
enhancement, which has helped stabilize our number of
life advisors and increase productivity.
In the International Investments segment, we continue to grow assets under management in our major
markets in Mexico and Taiwan, and in our joint ventures
in Italy and Germany. In addition, we announced in
November an agreement to acquire an 80-percent stake
in Hyundai Investment and Securities Company from the
Korean Government. This transaction, which closed in
February 2004, will significantly increase our market
position in Korea and add scale to our overall
International Investments business.
Positioning Prudential 
to meet its strategic goals
Prudential Financial is making the right moves to be in
the right businesses at the right time. We are striving
toward leadership positions in growing businesses and
exiting those that have underperformed. We are
extremely diligent about managing capital and reducing
expenses. As a result, the company has made significant
strides to achieve three key goals:
  To be one of the top three life insurers in the world
  To be a top-10 global asset manager
  To be a leader in distribution
Most important, we have demonstrated our resolve
to achieve our goal of 12 percent return on equity (ROE)
in 2005, based on after-tax adjusted operating income,**
and we have made substantial progress toward that goal.
Through the efforts of all our employees, Prudential
Financial has proven over the past two years as a public
company that we can deliver for our investors. Now we
must work hard to sustain investor confidence and build
on it. We will continue to deliver outstanding service to
our 15 million valued customers around the world. We
will grow our businesses domestically and internationally,
striving for excellence in each one. We will strengthen
our powerful brand, assuring that more people worldwide recognize Prudential Financial as a leader in
growing and protecting wealth. I am confident we have
the talent, the resources and the drive to succeed.
ARTHUR F. RYAN
Chairman and Chief Executive Officer