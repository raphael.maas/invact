Fellow
Shareholders
In 2013, QEP Resources achieved a number of
all-time company records, including:
record Adjusted EBITDA * of
$1.54 bi llion
record total proved reserves of
4.1 trillion cubi c feet equivalent
record crude oil production of
10.2 million barrels
record crude oil reserves of
149 million barrels
record fee-based gas processi ng revenue OF
$75 MILLION

When QEP Resources debuted as a standalone public company in
July 2010, we were a leading low-cost developer and producer of
natural gas. At the time, we made a decision to grow our crude oil
production and reserves to develop a more balanced portfolio of
both crude oil and natural gas.

We firmly believedand still dothat this
approach provides us with better flexibility
to run our business over the long term,
generating superior risk-adjusted returns throughout
all commodity market cycles.
In 2013, this strategy served us well as oil prices
remained strong, averaging $98 per barrel, while
natural gas prices averaged $3.73 per MMBtu. In spite
of lower natural gas prices, we were able to deliver
record Adjusted EBITDA as we allocated our 2013 capital
investments to high-return areas, including the
Williston Basin crude oil play, Pinedale liquids-rich gas
play, Uinta Basin (Lower Mesaverde) liquids-rich gas
play, and midstream gathering and processing projects.

As a result of our returns-focused capital allocation
strategy, QEPs crude oil production volumes and
reserves increased an enviable 62 percent and 25 percent
year-over-year, respectively. And for the first time
in our company history, field-level revenue from crude
oil sales exceeded natural gas sales. Furthermore,
crude oil production represented 20 percent of our
total company-
wide production in 2013, a substantial
increase from 12 percent in 2012 and 8 percent in 2011.
QEPs 2013 results demonstrate clear and substantial
progress toward our strategy of building a balanced
portfolio of premier crude oil and natural gas assets.
It is important to note that we have been successful in
executing this strategy while maintaining our financial
strength and flexibility. Our balance sheet remains
healthy with a debt-to-capitalization ratio of 47 percent
at the end of 2013, and we continue to uphold our heritage
as one of the industrys lowest-cost producers.
In early 2014, our Board of Directors authorized the return
of capital to shareholders through an up to $500 million
share repurchase program, further underscoring our
financial discipline and our prudent approach to managing
the business. As we continue to profitably grow our
company, we will maintain our relentless focus on careful
allocation of capital to determine the appropriate
combination of E&P investments, debt reductions, share
repurchases and dividend payments to maximize longterm
shareholder value.
QEP has made tremendous strides in 2013, but theres
more work to do. We will remain vigilant in building a
FOCUSED and BALANCED portfolio of premier oil and
natural gas assets that will strengthen our competitive
positioning and deliver value for our stakeholders,
including investors, employees, communities and the
public. In the following pages, I will outline specific
actions that QEP is taking to execute this strategy.


focused
In 2013, we implemented a number of strategic initiatives to focus
our E&P asset portfolio on high-margin, high-return production
and reserve growth.
Over the past several years, we have evaluated
a number of strategic alternatives with our
midstream business, QEP Field Services.
On January 7, 2013, we announced our intention to form
a Master Limited Partnership (MLP) and on August 14,
2013, we completed the initial public offering (IPO) of
QEP Midstream Partners, which is now trading on the
NYSE under the ticker QEPM.
From start to finish, the IPO was one of the fastest and
the largest midstream MLP IPOs in history raising
approximately $450 million in net proceeds. To create
additional value for our shareholders, we made a strategic
decision in December 2013 to fully separate QEP
Field Services, including our interest in QEPM, from
QEP Resources.
The separation of QEP Field Services from QEP Resources
creates a number of significant advantages. First,
separate E&P and midstream businesseseach with
experienced and incentivized leadership teamswill
compete more effectively in their respective markets.
Second, each business will be able to independently
deploy resources and allocate capital according to
their respective strategic initiatives and growth strategies.
Third, it is clear that the value of our midstream business
is not currently fully recognized in the QEP share price
and a separation is expected to unlock that value. In
2014, we will be evaluating multiple paths for separation
to determine and pursue the one that we believe will
deliver the most value to our shareholders.
To further focus our E&P asset portfolio, during 2013
we divested non-core E&P assets in the Powder River
Basin, San Juan Basin and the Marmaton/Tonkawa
plays in the Anadarko Basin, and announced plans to
divest other non-core E&P assets in the Midcontinent,
including the Granite Wash and the Woodford Cana
during the first half of 2014. These assets have performed
well for the company, but fall short on some of our key
criteria for core assetscontiguous acreage, QEP
operated and high working interest. In 2013, asset
sales and the QEPM IPO proceeds raised more than
$650 million.

balanced
We have made tremendous progress on our multiyear strategy
of growing our crude oil and natural gas liquids production and
reserves to build a balanced
portfolio. When dry-gas economics
inevitably improve at some point in the future, we can easily shift
our capital back to our premier dry gas asset, the Haynesville Shale.
In 2013, the majority of our capital was invested in
our cornerstone crude oil development project in the
Williston Basin, which includes the South Antelope
properties that we acquired for approximately $1.4 billion
in the fall of 2012. This acquisition is a great example
of our asset evaluation and follow-on development
skills and of our sound and stringent capital allocation
process. From the time of the acquisition through the
end of 2013, cash capital expenditures have exceeded
Adjusted EBITDA from the properties by approximately
$100 million. At the end of 2013, our South Antelope
proved reserves had a pre-tax PV-10 (present value of
net cash flows, after capital expenditures, discounted
10% per year and using prices consistent with Securities
and Exchange Commission guidelines) of over $2.2 billion,
or approximately $700 million more than the cumulative
net investment through year end 2013. Including
probable reserves and associated development cost, the
year-end pre-tax PV-10 value was more than $2.8 billion,
substantially above our net cash investment to date.
The QEP team has done a tremendous job of integrating
the South Antelope properties, as we have decreased
well costs, derisked unproven reserves, and increased
production, future drilling locations and estimates of
recoverable reserves. In 2014, we will continue the
development of this world-class asset.
The results of our South Antelope acquisition serve
as a credible marker for our recently acquired crude
oil properties in the Permian Basin for an aggregate
purchase price of approximately $950 million. This
acquisition allows us to leverage our strength of drilling
horizontal wells in unconventional reservoirs and
bolsters our crude oil inventory with the potential for
ten years or more of horizontal development drilling
locations. In 2014, we plan to allocate roughly $270
million to begin our development program on this exciting
new asset while going from two rigs drilling vertical
wells at the time of closing to six rigs by the end of
2014three drilling vertical wells and three drilling
horizontal wells.
We now have an expanded footprint in two world-class
crude oil provinces of North America, the Williston
Basin and the Permian Basin. In 2014, we will also
allocate capital to our liquids-rich plays, Pinedale and
the Uinta Basin. In Pinedale, drilling and completion
efficiencies have allowed QEP to maintain industryleading
well costs and we have reduced average drill
times from spud to total depth to less than 12 days.
In the Uinta Basin, we are encouraged by early results
from our first well utilizing a fundamentally different
design that could significantly alter the development
economics of this liquids-rich gas asset.

summary
As we implement our business plans, we remain committed to
protecting the health and safety of all people who are a part
of our operations or who live in the communities in which we
operate. Wherever we operate, we will conduct our business
with respect and care for the environment. These are fundamental
responsibilities of each employee. We believe that no aspect of
our operations is of greater importance.
QEP is home to some of the smartest, hardestworking
people in our industry. I would like to
thank each of our employees for everything
that they do to make QEP successful. I am very proud
of what we have already accomplished, and I look forward
to more great achievements.
We will continue to work together to maintain a relentless
focus on superior operational and financial performance
as well as implementing important company-wide initiatives,
such as the launch of our Enterprise Resource
Planning system, making QEP a more competitive and
efficient company with stronger capabilities, and our
new corporate giving program, QEP Cares, a program
that builds on our record of responsible corporate citizenship
dating back to 1922.
Throughout the year, we experienced a number of significant
and important changes to our business. Each
of these decisions was part of our deliberate strategy
to focus the company on high-margin, high-return
crude oil and liquids-rich natural gas production and
reserve growth. As we begin the next stage of our journey,
I could not be more excited about our future.
Sincerely,
Charles B. Stanley
Chairman, President and Chief Executive Officer