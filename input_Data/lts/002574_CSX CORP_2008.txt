dear shareholders:
Our ability to deliver this performance across a range                                  Longer-term, we have enormous confidence in the role
of external conditions is the result of a committed                                     that railroads will play in bringing solutions to some of
workforce, the improvements we have made in our                                         our nation's most critical problems. It's a simple formula.
company over the past four years and the resiliency                                     As the population grows and consumes more, products
of our business. This gives us a solid footing in these                                 need to be moved. We offer the most efficient and
difficult economic times and the ability to emerge as                                   environmentally-friendly mode of surface transportation
an even stronger company.                                                               by far. And as our nation's leaders look to stimulate the







To do that, CSX is taking swift and decisive action to                                  economy, they will turn to railroads to relieve congestion
right-size our train network, enhance productivity and                                  on the highways and carry materials that are critical to
efficiency, and control costs. We have an exceptional team                              building roads, bridges and other infrastructure.
of employees who are driving change without sacrificing
safety. Regrettably, we've had to furlough a number of great                            CSX is positioned to meet those needs. We have an
people in the past several months, and we're committed                                  excellent transportation network that serves critical
to bringing them back as business conditions allow.                                     markets where two-thirds of the country's population




*Earnings Per Share are from continuing operations and fully diluted.
lives and where three-quarters of all
consumption occurs. Our balanced portfolio
of businesses is poised to benefit as various
sectors of the economy improve in stages.


In 2009 we plan to invest $1.6 billion to
                                                         ports and the Midwest in partnership with North
position our train network to help serve the country's
                                                         Carolina, Virginia, West Virginia, Maryland, The District
critical transportation needs. We're also seeking to
                                                         of Columbia, Pennsylvania and Ohio. This will allow
partner with federal, state and local governments. For
                                                         CSX to haul more freight in each train while taking
example, our National Gateway initiative will create
                                                         traffic off the highways and improving the environment.
double-stack train clearances between the Mid-Atlantic
For more than                                                                             reducing printing
180 years, CSX                                                                            by more than 25
has been making a                                                                         million pages, which
positive impact on                                                                        helps preserve our
the communities                                                                           forests and provides
we serve.                                                                                 cost savings to our
As part of that                                                                           shareholders.
commitment, it
is important that we protect the environment by        CSX is truly How Tomorrow Moves. The more than
continually finding new ways to conserve energy and    30,000 men and women of our company believe that
resources, and by developing long-term comprehensive   our commitment to tomorrow begins today. We're
climate change strategies.                             dedicated to doing the right things with your
                                                       investment, and doing them in a way that is good for
This annual report itself represents a small but       our customers and the country.
important example of our environmental leadership.
By providing this document electronically, we are


                                                       Michael J. Ward