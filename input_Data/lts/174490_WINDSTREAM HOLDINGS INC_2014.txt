Windstream generated substantial adjusted free cash flow in 2014 and paid more than $600 million in dividends to shareholders. Although overall results fell short of our expectations, we are taking the necessary actions to put us on the path for success.

In the short time I have been leading Windstream, we have already made a number of significant changes to better serve customers, grow revenues and improve profitability. These changes, combined with planned initiatives, will advance our goals of improving the customer experience, operating a best-in-class network and delivering improved financial results and increased value for shareholders.

We also are working diligently to close a groundbreaking strategic transaction to spin off fiber, copper and other assets into an independent publicly traded real estate investment trust, called Communications Sales
& Leasing, Inc. (CS&L). Windstream will lease these assets from CS&L to operate the network and deliver services.

There are many financial and strategic benefits to this spinoff. The transaction is tax-free and will ensure an attractive dividend payment for shareholders. The separation significantly reduces Windstream's debt and enables increased network investments. Most importantly, the transaction creates the opportunity to unlock shareholder value by creating two companies with distinct investment characteristics.

Upon close of the transaction, shareholders will retain their Windstream shares and receive one share of CS&L for every five shares of Windstream held. CS&L expects to pay an annual dividend of $2.40 per share.

I, along with our board of directors, appreciate the overwhelming support of shareholders recently to approve a 1-for-6 reverse stock split, which will be completed following the closing of the transaction and distribution of CS&L shares to shareholders. Windstream expects to pay an annual dividend of $.60 per share after the spinoff and stock split.

I am very excited about the positive steps we are taking with our operational and strategic initiatives. We will make significant progress this year on our goals. In order to strengthen Windstream for the benefit of our customers and shareholders we must do many things faster and better, and we will.

Sincerely,
President and Chief Executive Officer
March 31, 2015
