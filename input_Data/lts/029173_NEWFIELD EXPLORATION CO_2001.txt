DEAR FELLOW STOCKHOLDERS:
What a year! As the cover of this years
annual report depicts  this was a year of
unprecedented volatility in the energy
industry. I often felt like I was sharing the front
car on a thrilling rollercoaster ride; some turns
more fun than others. But we stayed in our seat.
We did not wander from our Founding Business Principles
and exited 2001 a stronger, more diverse company than we
entered it. In this years letter, I want to discuss the volatile 
environment we operated in and outline some reactive and
proactive steps we took to manage our business. Ill also provide
an update on each of our focus areas and share with you our
plans going into 2002. Im very proud of the accomplishments
weve made and the people who made them happen.

T
he first year of the new millennium was significant in so 
many ways. We were horrified by the devastation created 
on September 11. Prior to 9/11, many key economic 
indicators made it increasingly clear that our economy was 
struggling. The uncertainty that was created post 9/11 further
hampered the economy, impacting demand perceptions for oil
and gas in the U.S. and the world. Recently, weve seen positive
signs that our economy is recovering. Our nation emerged from
the tragic events stronger and more united with a renewed sense
of patriotism, values and priorities. 
While every American business has been impacted in some
way by the events of 9/11, the real volatility in our sector began
more than 18 months ago. With the perception of increasing
demand for natural gas and inadequate supply, gas prices moved
above $4 per Mcf in August 2000. Although not in dire straights,
the U.S. entered the winter season of 2000-01 with gas storage
levels below those of recent years. In late 2000, our country was
reminded what real winter weather feels like as we were gripped
with the coldest November-December in more than 100 years.
Natural gas prices averaged nearly $6.50 per Mcf in the fourth
quarter of 2000 and natural gas was withdrawn from storage at a
record pace. The rollercoaster was gaining momentum. Natural
gas prices continued to soar, peaking at nearly $10 per Mcf in
December 2000.
E&P companies enjoyed the spike in cash flow, but behind
the scenes, natural gas demand was responding to unprecedented
prices. Without a doubt, high prices proved that demand is more
elastic than we ever thought. Power generators and industrial
users began switching to lower cost fuels and residential consumers lowered thermostats and dimmed the lights. Some industrial sectors (i.e. ammonia and fertilizer) were sidelined during this period, opting to close facilities rather than
pay high gas prices for fuel stock. However,
the initial demand loss in the industrial sector was masked by increased heating demand
due to the frigid weather in late 2000. But as
gas prices continued to climb, demand continued to erode. 
As the weather warmed and the first
quarter of 2001 came to a close, it was apparent that a significant portion of gas demand
was missing. At the peak in early 2001, industry experts estimate 5-7 Bcf/d of gas demand
was lost due to high prices. The rollercoaster
had crested and was on its way down.
Demand continued to decline as our economy
slipped toward recession. 
In spring and early summer, gas injections into storage set record highs. Increased
drilling activity added some supply and
imported gas from Canada exacerbated the
issue. We had come full circle: In early 2001,
storage levels were nearly 600 Bcf below 
five-year average levels and today they are
running about 600 Bcf above the five-year
average. Thats a storage swing of 1.2 Tcf in
about one year! 
As gas prices soared, so did the natural
gas rig count. At a feverish pace, rigs were
brought out of cold stack and the gas rig count
peaked at 1,068 in July 2001, up from a low
of 362 rigs in April 1999. Although there is
conflicting data, it appears that there was 
not a significant, sustainable supply response
from increased drilling activity. In large part,
many of the plays the industry focused on 
in this cycle were exploitative in nature.
Supply modestly increased, but significant
new reserves were not added. This reflects the
accelerating treadmill of our countrys mature
producing provinces. 
At the time this letter was released, there
was about 1.7 Tcf of gas in storage and winter weather in 2001-02 was averaging 17%
warmer than normal. Gas prices were $1.89
and $2.65 per MMBtu, respectively, at the
end of the third and fourth quarters of 2001.
The industry responded to price erosion by
laying down rigs nearly as quickly as they had
been put to work. Today, the gas rig count is
under 700 and falling. Service costs continue
to correct to reflect the current environment.
We believe gas supply will likely erode faster
than many predict. We are seeing positive
signs for gas demand.

Looking forward, we believe higher 
natural gas prices will be required to meet 
the demand needs of our country. The
nations major producing basins are mature
and prospect sizes continue to shrink.
Without a doubt, the $5-10 per Mcf prices
experienced in the winter of 2000-01
destroyed demand. The market needs to find
a level that allows new sources of supply to 
be found and developed and allows natural 
gas to fulfill its promise as the fuel of choice. 
How did Newfield handle the challenges
of the past year? We shared all the ups and
downs with the industry, but we finished the
ride and have the fare to go again. 
We exited 2001 a stronger company. 
We diversified our asset base with our
largest-ever acquisition and established a new
focus area in the Mid-Continent. At the same
time, we increased our Gulf Coast production
11% in 2001. We have successfully transitioned from a company with exclusively 
offshore operations to one with a balanced 
portfolio of properties. Looking forward, we
have a more diverse set of opportunities before us than at any other time in our 
history. Our optimism for Newfields future 
is also fueled by a belief in the fundamentals
of the natural gas business. 

Lets look at how we handled a few of the 
years major challenges:
Gas Price Volatility
We utilize hedging to manage our business and
view this tool as an integral part of our risk
management program. By hedging a portion of
our expected future production, it helps ensure
that we will have the cash flow to implement
our drilling programs. In addition, we have used
hedging in connection with some of our acquisitions to manage price risks and rates of return.
We have been an active hedger since 1992.
A close friend forwarded an Oliver Wendell
Holmes quote to me that I really liked: Prophesy
as much as you like, but always hedge. Every
research analyst, institution and company has 
a unique opinion on the gas market. We have
strong convictions too, but we hedge.
We employ an active hedging program to help manage our business. There are many risks in the
E&P business. A disciplined hedging program helps ensure adequate cash flow to implement our
drilling programs. We often hedge expected production associated with acquisitions, helping to
manage price risks and rates of return.
Our hedging program generated more than $70 million in revenues in the second half
of 2001. We gave back a similar amount in the first half of the year. But this is what the
program is designed to do. We dont pretend to be smarter than the market. Hedging is
used to smooth the cycles and help ensure adequate cash flow to manage our business. 
For the first half of 2002, about 80% of our gas is hedged. In the third quarter, we have
about 50% of our gas hedged, dropping to about 15% in the fourth quarter. About 60%
of our estimated 2002 oil production is hedged. The weighted average price of these hedges
is above current NYMEX futures prices.
We have allocated $360 million for capital spending in 2002, in line with our 
expected cash flow. Our hedge position gives us confidence in our cash flow estimates. 
As always, we dont budget for acquisitions but do take advantage of opportunities as they
come along. We spent $855 million in 2001, including $435 million on acquisitions.

Rig Rates and Service Costs
The spike in gas prices allowed service
providers to charge what the market would
bear  and it bore a lot! To help offset high
costs, we worked hard on our pre-drill planning and engineering to minimize days on
location. 
We laid rigs down when day rates
remained high and gas prices fell. At our 
peak, we were running 12 rigs in the Gulf of
Mexico, 12 in the Mid-Continent and six in
the onshore Gulf Coast. In the fourth quarter, we were running only two rigs in the Gulf 
of Mexico, two in the Mid-Continent and 
none in the onshore Gulf Coast. Despite the
decrease in activity, we were still able to meet
our production numbers. But our efforts were
not enough to keep our finding costs from
escalating.
Our finding and development costs in
2001 were $1.97 per Mcfe, a 25% increase
over the $1.58 per Mcfe we posted in 2000.
For Newfield, and the industry, finding costs
escalated to levels similar to those seen in
1997-98. Our finding costs in 2001 were too
high and we must do better. In contrast to
1998, however, we were able to use our hedging program to lock in higher gas prices and
protect returns. We realized $4.25 per Mcfe
on our 2001 production compared to $2.11
per Mcfe in 1998. Our hedging program
ensures that a majority of our gas production
in 2002 will receive not less than $3.35 per Mcf. 
Curtailed Production
Gas prices at the end of the third quarter were
$1.89 per MMBtu. Although we were more
than 80% hedged for the second half of 
2001, we elected to curtail a portion of our
unhedged production in response to weak gas
prices. At the peak in late September and early
October, we had about 55 MMcf/d of gas 
curtailed. Despite voluntary curtailments, 
we met our 2001 production target of 175 
Bcfe  a 25% increase over 2000 production.
In February 2002, we voluntarily curtailed
about 30 MMcfe/d of our production.
Repurchased NFX Shares
In early 2001, our board of directors authorized the repurchase of up to $50 million of
our stock. We utilized this program when our
share price moved to low levels, purchasing
823,000 shares in 2001 at about $30 per share.
More than 300,000 shares were purchased 
at about $27 per share following 9/11. This
was another tool we used to add value during
periods of volatility. 
Diversified the Asset Base
We kicked off 2001 by acquiring a company
in the Mid-Continent  Lariat Petroleum.
This was a $333 million acquisition to establish a new focus area and this decision did 
not come hastily. 
In Lariat, we found a company with a
track record of growth that matched our own.
Whats more, their corporate culture was very
similar to ours and that helped smooth the
transition. Several months after closing this
acquisition, we ranked as one of the most
active drillers in Oklahoma. That is the value
of having people and properties in place from
the beginning. 
Today, we have a growing business in one
of the nations most prolific natural gas
basins. Whats more, we see room to consolidate, building our business there much like
we have in the Gulf Coast. We see the opportunity to acquire both producing properties
and companies in the Anadarko Basin and
have active efforts underway to grow our
Mid-Continent operations. 

We did a good job managing volatility,
but not well enough to avoid all the inherent
pit falls of the typical boom-to-bust cycle. 
We announced a non-cash ceiling test writedown of $68 million, after tax, in the fourth
quarter of 2001. The impairment was related
to our Mid-Continent acquisition. At the 
time of closing, gas prices were significantly 
higher and we protected our acquisition 
economics by hedging 2001-03 expected natural gas production at attractive prices. These are 11-year life
reserves. Under SEC rules, we
are required to use commodity
prices at year-end 2001, offset by
our hedges, to value future revenues from these assets. The value
of our hedges at year-end 2001 
was about $78 million. The writedown will increase our earnings in
future periods.
We earned $119 million, or
$2.56 per share, in 2001  our
second-best year ever. These
results include the ceiling test
charge of $68 million, after-tax.
Our 2001 revenues were nearly
$750 million, the highest in our
history, and reflect a 25% increase
in production and strong realized
prices. Our operating cash flow
increased 37% to $527 million, or
$10.90 per share. With respect to
these measurements, 2001 was a
strong year.
We are a much larger and
more diverse company today
than we were just three years ago.
We ended 2001 with 936 Bcfe of
proved reserves, an increase of
82% over year-end 1998 reserves.
In 1998, nearly all of our reserves
were located in the Gulf of
Mexico. Now, nearly half of our
proved reserves are located in
onshore basins. Our diversification efforts have been successful
and we have posted solid production and reserve growth both
on- and offshore. A brief overview of our focus areas and our
early planning for 2002 follows.
Onshore Gulf Coast
Over the last three years, our
net production from the onshore
Gulf Coast has grown 20-fold to
a current rate of nearly 100
MMcfe/d. Since 1995, weve put
together a 60,000-acre position
along the coastal regions of Texas
and Louisiana.
In 2001, we drilled four 
successful exploration wells out
of eight attempts and we were 
10 for 12 in our development
drilling efforts. We are planning
to drill about 10-15 wells in
2002, with the primary focus of
this years drilling program being
exploratory. 
The largest component of
our onshore Gulf Coast growth 
in 2001 came from the three
South Texas gas fields we acquired
in early 2000 for $139 million.
This acquisition provided quality
producing fields with significant
undeveloped reserves that could
be quickly exploited. The largest
field in the package was East
Sarita, located in Kenedy County,
Texas.
At the time the acquisition
closed, the East Sarita Field was
producing about 25 MMcfe/d.
We spent the first half of 2000
mapping the fields complex geologic structure and identifying
drilling locations. We kicked off 
a six-well drilling campaign in late
2000. The result: field production
tripled to a peak rate of about 80
MMcfe/d in the third quarter of
2001.
Today, our efforts in South
Texas are largely centered on
exploration. We purchased a 250
square mile 3-D seismic data set
just west of our current production areas and are working to
develop additional Wilcox leads
and prospects. Our geoscientists
are working in and around large,
mature fields in this area and we
expect to drill 2-4 wells during
2002. We are also working other
nearby areas, but due to the competitive nature of these developing
plays, we must defer a discussion
of the details. We should have
more to report later this year.
Moving up the coast, we
were also active in the Provident
City area in the Lower Wilcox
trend of Lavaca, Colorado and
Dewitt Counties, Texas. We
drilled four successful wells in 
this area during 2001. Deliverability is currently 15 MMcfe/d,
an increase of 36% over our production at this time last year. 
In 2002, we are planning to drill
several high-potential wildcats in
the region.
In South Louisiana, our current efforts are also weighted
toward exploration. We are currently participating in four 3-D
seismic programs and expect to
drill one or more wells in two of
these areas in 2002.
Gulf of Mexico
We had a very active year in
the Gulf of Mexico, drilling or
participating in 27 successful wells out of 35 attempts. We produced about 116
Bcfe from the Gulf in 2001 and more than
replaced our production through our drilling
program and two modest-sized acquisitions.
Our success in the Gulf of Mexico is attributable to the quality of our people. They continually work our properties and supplement
our prospects with acquisitions and farm-in
opportunities. We are confident that we will
continue to grow our production and reserves
in the Gulf.
How? Thats the challenge that awaits our
employees when they arrive at work each day.
There are four areas that we are actively
working in the Gulf of Mexico:
1. The Traditional Shelf  Today, we have more
than 140 production platforms and own an
interest in more than 170 lease blocks. We 
are among the top operators of production in
the Gulf. 
In 2001, all of the wells we drilled in the
Gulf of Mexico were in less than 400 feet of
water. In fact, 27 of these wells were drilled
from existing platforms. Our vast infrastructure is a competitive advantage that allows us
to do things cheaper and quicker than many
of our competitors. Our infrastructure will
have immense value as new play types and
trends are found in, around and beneath the
Shelfs producing fields.
One of 2001s highlights was our Eugene
Island 251 #5 exploratory well. Located adjacent to our largest producing asset on the
shelf, our geoscientists identified this prospect
by analyzing regional seismic data. We found
more than 120 feet of net gas pay and believe
this is a significant discovery. We will set a
small structure, drill a second well and expect
first production in the second quarter of 2002.
2. Offshore Texas Frio  Just east of our East
Sarita Field in South Texas, recent industry
activity in state coastal waters has discovered
significant reserves. Over the last three years,
wildcat discoveries added 200-400 Bcfe of
reserves in water depths of less than 100 feet.
The play is known as the Offshore Texas Frio.
The Frio Trend is the major producing
interval along the Texas and Louisiana coasts.
Recent successes have indicated that the
quality rocks seen onshore do not stop at 
the shoreline. The geology of this region is 
nearly identical to the trends we are working
in South Texas and we believe our people have
the expertise to be successful. 
We entered this emerging play through
an acquisition in the third quarter of 2001.
We purchased a package of assets and gained
control of an inventory of prospects and leads and a stake in the North Padre Island 883
Field. Discovered in 1999, the NPI 883 Field
has a series of stacked pays in the Upper and
Middle Frio sections and is now producing 
30 MMcf/d and 1,000 BCPD. We have a 
35% working interest in the field and are evaluating the potential for additional drilling. 
3. The Deep Shelf  We are one of the companies actively working to open the door to 
new plays in deeper horizons on the shelf. 
We consider the deep shelf to be prospects
located at depths greater than 13,000 feet in
areas where there has been limited or no 
production from deeper stratigraphic zones.
Deep shelf prospects will generally be characterized by high pressure and temperatures,
making drilling more difficult and expensive.
Seismic amplitudes associated with these
structures are typically subtle and not easily
identifiable through traditional seismic processing. Although more than 55,000 wells
have been drilled in the shallow waters of 
the Gulf, only 7,000 have been drilled 
below 13,000 feet and less than 800 have been
drilled below 17,000 feet. These deeper
intervals, although higher risk, offer the
potential for significant discoveries. 
We kicked off 2001 with a successful
deep shelf discovery at West Cameron 294.
Our WC 294 #3 well was drilled to nearly
15,000 feet and found 110 feet of net gas 
pay. We immediately spud the #4 well, 
another success. The #4 well found nearly 100
feet of net hydrocarbon pay. Fabrication of a
caisson began immediately and we had first
production of 35 MMcf/d and 300 BCPD
from two wells in less than six months. Seeing
additional drilling opportunities, we negotiated to become operator and increased our
working interest in the field from 35% to
65%. We will begin receiving a larger share
of production from the first two wells later
this year. In late 2001, we drilled the WC 
294 #5 well with an 85% working interest.
This well also was successful, testing at 23
MMcf/d. We expect gross field production 
to increase to more than 50 MMcf/d in 
mid-2002. WC 294 is a significant discovery
and it may prove to be one of the largest 
fields we have discovered in the Gulf of
Mexico. 
In early 2002, we made a significant deep
shelf discovery with our West Delta 21 #6
well. The well was drilled to a total depth of
16,000 feet and encountered 165 feet of net pay. We expect first production
from this new field in the fourth
quarter of 2002. We are planning
to drill 8-10 deep shelf prospects
this year. As always, we will balance our opportunity set, coupling
deep shelf wells with moderate
risk ideas in our traditional shelf
trends.
4. The Deepwater  We are making plans to enter the deepwater
of the Gulf of Mexico and have 
a strategy to do so over the next 
12-18 months. 
In our view, todays deepwater play is in many ways 
similar to the state of the traditional shelf when Newfield was
formed in 1989. Technology has
developed to the point that what
was considered cutting edge a
decade ago is standard operation
today. Technological strides in
drilling and completion have
reduced risks, capital costs and
lead times to first production.
The major oil and gas companies, in large part, are now
hunting elephants in 5,000-9,000
feet of water. Initially, we plan to
explore in water depths of less
than 5,000 feet where we believe
there are opportunities in and
around existing infrastructure
that fall beneath the radar screen
of larger companies. 
The deepwater play has
matured. A large percentage of
the initial 10 year leases are
expiring in 2006-07 and much of
the acreage remains underexplored. The ability to prospect on
this acreage prior to expiry could
create some opportunities. 
While the deepwater is
maturing, so is Newfield. Today,
we are a much larger and financially capable company than we
were just three years ago. Deepwater projects will complement
our vast asset base on the shelf,
mixing a few higher-risk, higherpotential projects with our lowerrisk, moderate potential activities.
Over the last year, we have hired
some personnel with deepwater
experience and assembled a team
to find attractive opportunities.
We expect to have more to report
on our progress later this year.
Mid-Continent
We entered the Mid-Continent with a running start, acquiring daily gas production that
ranked us in the top 25 gas producers in Oklahoma. Since closing our Mid-Continent acquisition in January 2001, we have
drilled about 90 wells. Our success
rate has been greater than 80%. 
We are planning to drill
about 60 wells in 2002. On average, we ran seven rigs per month
in 2001, peaking at 12 operated
rigs during the third quarter. We
were one of the top 10 most active
companies in Oklahoma in 2001. 
In 2001, we shot three proprietary 3-D seismic programs
totaling 163 square miles. We
expect this newly acquired data,
together with our existing database, will continue to give us a
technical edge in this competitive
region. 
We intend to actively screen
for potential acquisition opportunities in the Mid-Continent in
order to balance our drill bit
exposure. We have a strong group
of technical employees in our
Tulsa, Oklahoma office and believe they have the tools they need
for continued success.
International
We have actively pursued
international opportunities since
mid-1997. Our international program is a small part of our business today, but it gives us exposure
to high potential prospects.
In the East Timor Sea of
Australia, production in 2001 from
our two operated fields averaged
over 8,000 BOPD (4,000 BOPD
net). We have produced 4.0 million
barrels in Australia since acquiring
these fields in mid-1999 and have
sold that production for an average price of $26.88 per barrel.
Our 2001 production sold for an 
average price of about $24 per
barrel. Since mid-1999, our net
cash flows from Australia have
exceeded $30 million, including
dry hole investments. Although
our exploratory drilling results
have been disappointing, this
acquisition has far exceeded our
expectations. We exposed ourselves to impact reserve prospects
with little capital riskand made
money in the meantime.
Efforts today in Australia are focused 
on exploitation projects. Our Australian 
team has been charged with finding opportunities to build the business. In the third 
quarter of 2001, we reached an agreement to
farm-in to the retention license area AC/RL3,
located in the Territories of the Ashmore
Cartier, about 55 miles southwest of our 
existing production. An Australian chemical
company previously held a 100% interest in
the license area and has agreed to farm-out a
50% interest to us and allow us to operate.
We plan to drill an appraisal well on the
Montara discovery  one of four discoveries
on the license area. We expect that well to
spud in March 2002.
In China, we continued to appraise our
CFD 12-1 Field discovery on Block 05/36 in
the Bohai Bay following the interpretation of
a new 3-D seismic survey. We drilled five
wells in 2001, including two wells in a new
discovery, the CFD 12-1 South Field. 
We have drilled four appraisal wells in the
CFD 12-1 and CFD 12-1 South Fields. Each
well, while different, confirms oil reserves 
and production testing has been encouraging.
Our  CFD  12-1 South #1 well tested more
than 5,000 BOPD of 40-degree gravity oil 
the best rate and oil quality we have seen 
in our testing program. In 2002, we will 
work with the operator to further define 
the field, assess reserves and, if commercial,
file a plan of development with the Chinese 
government. An additional appraisal well is
expected to spud in the second quarter of
2002. We own a 35% interest in Block 05/36. 
No doubt, volatilitys rollercoaster will 
continue to run in 2002. Recognizing this, we
will continue our effort to manage it, just 
as we did in 2001. We hope that our hedging
program will smooth the ride and help to
ensure the necessary cash flow to implement
our 2002 capital program. We are very
enthusiastic about the exploration programs
that we have underway in all of our focus 
areas and are very encouraged by the early
success of our deep shelf program in the Gulf
of Mexico. 
We will be actively looking for acquisition opportunities in 2002. A good supply 
of properties is already available, in large 
part because of the tough ride the industry
experienced in 2001 and early 2002. Our
employees have a good track record of finding  acquisitions that add long-term value for
our shareholders.
We are excited about the opportunities
we have in front of us and are confident that
we have the people to take advantage of 
them. We will continue to run our company
for long-term, profitable growth. As always, 
we appreciate your support of Newfield
Exploration.
Sincerely, 
David A. Trice 
President and Chief Executive Officer