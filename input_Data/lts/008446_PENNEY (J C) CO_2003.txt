To Our Stockholders

In 2003, the Company made further significant progress in the
turnaround of our core Department Store and Catalog/Internet
business. As promised, the Company also reached a very
important strategic decision with respect to our Eckerd Drugstore
business.
Over the last three years, our Department Stores have posted
the best comparable store sales performance of any mall-based
department store competitor. This year, after a difficult first quarter,
our team built momentum throughout the remainder of the
year with steady improvements in each successive quarter. With
sales improvement of over 3% in the fourth quarter, by year-end
the Department Stores achieved a comparable store sales increase
of almost 1%.
Catalog/Internet also made excellent progress, benefiting from
major repositioning of the operating model over the past several
years. Sales and operating profit contributions to the Department
Store and Catalog/Internet segment received an enormous boost
from our Internet site, which recorded impressive sales growth of
50% versus the prior year. We expect our Internet business to
continue to provide a competitive advantage for the Company.
I am pleased with the performance improvements we have
made in our Department Store and Catalog/Internet business.
For the full year, we achieved an operating profit of 4.4%, a 13%
improvement from a year ago. I remain confident in our ability to
achieve the stated turnaround objective of 6% to 8% operating
profit in 2005.
However, 2003 was not without disappointment. The sales and
profit performance of our Eckerd Drugstore business fell far short
of our expectations. In contrast to the progress made in the prior
two years, we experienced a performance decline throughout
2003 due to a failure to properly execute the fundamentals of the
business at competitive levels.
As the year progressed, it became increasingly clear that a turnaround
of Eckerd was going to take much longer and require
greater management focus than originally anticipated. This caused
us to re-evaluate the challenge of managing turnarounds in two
distinctly different segments of the retail industry. Our analysis led
us to the conclusion that while we continued to believe in the
long-term prospects for the drugstore industry, we needed to
focus on the JCPenney Department Store and Catalog/Internet
opportunities as our priority in order to maximize shareholder
value. Therefore, we elected to pursue a disposition of the Eckerd
Drugstore business. As we move forward, I believe that returning
our singular focus on the Department Store and Catalog/Internet
business will ultimately result in a more successful company for
our customers, our associates and our shareholders.
DEPARTMENT STORES AND CATALOG/INTERNET
Department Stores
In our Department Stores, we continue to focus on providing
the customer with clear reasons to shop JCPenney first.
For most customers, the first priority is being able to find the
right merchandise, and it continues as our priority as well.
Merchandise assortments  with
a blend of JCPenney private
brands, national brands and
brands exclusive to JCPenney 
are more fashionable and trend
right while also delivering the
appropriate value for our moderate
customer. We are promoting
JCPenney as Americas
year-round gift headquarters
with dominant gift-giving merchandise
statements located
throughout the store. In categories
where we already enjoy a
leadership position with moderate customers  areas such as fine
jewelry, window coverings and mens clothing and furnishings 
we have grown our leadership position over the past year. And,
we have strengthened our dominant position in merchandise
basics, such as towels, jeans and underwear, through improvements
in distribution and depth of store stock on key sizes.
Our marketing message has been strengthened with greater
emphasis on customer convenience, another clear reason to
choose JCPenney first. We are the only major apparel and home
furnishings retailer with a comprehensive presence in three channels
 Stores, Catalog and Internet  providing our customer with
the convenience and flexibility to shop with us 24 hours a day, 7
days a week. Our three-channel advantage also gives customers
an unparalleled breadth of merchandise offerings plus an expanded
range of key item size and color choices that is unmatched in
the industry.
Our in-store presentation has been refined with an eye toward
added customer convenience as well. The goal is twofold: to create
dominant and appealing fashion statements for our customer,
and to enhance her ease of shopping. As a result, fashionable
merchandise statements are more compelling, signs and in-store
graphics have become more streamlined and effective, and a
more consistent visual presentation is now in evidence from store
to store.
The American Customer Satisfaction Index (ACSI), an annual
and independent measurement commissioned by the University
of Michigan Business Schools National Quality Research Center in
partnership with other sponsors, showed that JCPenney achieved
the highest score among any of our targeted mall-based department
store competitors for 2003. This survey, designed to
measure customer satisfaction with products and services at the
point of delivery, also indicated that customers perceived
JCPenney as the most significantly improved of the listed direct
competitors in the categories of quality of merchandise and customer
service from the prior year.
As we continue to gain experience and mature within our centralized
environment, the focus will be on improved execution
and consistency. The lessons we have learned during the first three
years of our turnaround should pave the way for further progress
in 2004.
We are planning to open 15 department stores in 2004, the

largest increase in the last five years. About half of these stores will
be off-mall. Off-mall locations give us an opportunity to add
stores without having to rely solely on the development of new
malls and provide our moderate customer another convenient
alternative for shopping at JCPenney.
We will also devote our attention to another key priority in
2004: reducing operating expenses without compromising our
level of customer service. Earlier this year, we launched a major initiative
to capture the full benefits of our new centralized business
model by streamlining operations, eliminating redundancies and
moving toward a more competitive cost structure. By the time
this initiative is completed over the next two years, we expect to
achieve savings in excess of $200 million annually.
Catalog and Internet
Our efforts to reposition our Big Book and specialty catalog
business over the past three years have centered on refining the
infrastructure of the business, as well as improving the fashion,
quality and value of our merchandise assortments. These efforts
were rewarded in 2003 with solid sales improvement and a positive
contribution to the Companys overall results. With an
improved mix of fashionable merchandise at compelling price
points and a corresponding emphasis on value, the JCPenney
Catalog has become more relevant to our customer.
I am especially pleased with the rapid growth of the Internet
channel. The online shopping experience has increasingly become
a favored outlet for a broad group of consumers, both young and
old, and is the fastest growing selling channel for JCPenney.
Whether the measurement is response time or site availability,
jcpenney.com is an industry leader. This performance enhances
our customers experience, and leads to one of the highest shopper-
to-buyer conversion ratios among online retail merchants.
With sales of over $600 million in 2003, we are well on our way to
achieving sales of $1 billion in the next few years.
Our ability to offer broad assortments of appealing merchandise
in three channels  any way, time and place our customer
wants to shop  is unparalleled in the retail industry. Our
Catalog/Internet infrastructure provides the flexibility to support
our current business model, as well as the model of an everchanging
future. We lead the industry in offering shopping solutions
for our customers, a leadership position about which we are
justifiably proud.
FINANCIAL CONDITION
The Companys financial position further strengthened during
2003 and continues to provide the resources to support the
strategic and operational needs of our business. 2003 represented
our fourth consecutive year of generating positive free cash
flow from operations, a significant accomplishment during the
course of a turnaround.
With positive free cash flow and an unsecured debt offering
early in the year, we finished 2003 with almost $3 billion in cash
investments. The ability to maintain strong liquidity and an overall
financial position that continued to improve, despite a challenging
first half environment, provide evidence that our financing
strategy remains sound.
As we proceed with the divestiture of our drugstore operations,
we expect to use proceeds from such a transaction to further
strengthen the Companys financial position and capital structure.
Through an appropriate mix of both common stock repurchases
and debt retirements, we will seek to deliver value to our shareholders
and support our long-term objective of improving our
bond credit ratings. Going forward, our financing strategy will
remain focused on preserving both strong liquidity and financial
flexibility as we work to improve the performance of our
Department Store and Catalog/Internet business.
THE FUTURE
At any point in time, prognostication of future events and conditions
can always include both good and bad. At JCPenney, we
prefer to see the future in a positive light. Economic indicators,
corporate earnings and consumer confidence have improved
from a year ago. Interest rates are at historical lows, and more
people are finding jobs. Closer to home, last years tax reform is
providing increased levels of disposable income for our target
moderate customers.
To continue our turnaround, each step forward must be followed
by another one. As I mentioned in my remarks last year, our
turnaround effort is not unlike climbing a mountain. Now, we are
further up the mountain, but the climb is still difficult. Our merchandise
offerings, our value equation and our service all continue
to improve, yet we must endeavor to work on every aspect of our
business and to keep our merchandise relevant to our customers.
For 2004 and beyond, we will continue to focus our efforts on
improving execution. We have overcome many of the initial hurdles
associated with our shift to a centralized business model, and
now superior and consistent execution will be the key to our continued
success. Going forward we must utilize  and maximize 
the resources and new processes that are now in place.
Although the path to restoring competitive profitability in a
turnaround situation is rarely a smooth one, I remain impressed
with the commitment and ingenuity demonstrated by the associates
in every part of our business. We are progressing toward
completion of a major turnaround that many thought impossible.
For that I must express my sincere gratitude to the entire
JCPenney organization  the associates in this company who,
through their dedication and hard work, are making it happen.
Together, we are determined to reach the summit of this
mountain.
Allen Questrom
Chairman of the Board and
Chief Executive Officer