TO OUR SHAREHOLDERS
Seven years into the current console cycle, we knew 2012 would be tough for the video game industry, so we entered the year with disciplined
resolve to control expenses and persist in executing our strategic plan. As a result, we significantly outperformed the market and again
expanded our gross margin rates despite a decrease in topline sales.
As expected, digital receipts grew nearly 40% in 2012, driven by our ability to leverage our store base to bring digital content and currency to our
customers. We expect strong growth in this category and are excited about the opportunity a new console cycle presents for us.
We continue to utilize consumer insights gathered through our PowerUp RewardsTM loyalty program to find new revenue streams and adapt to the
changing marketplace. Our mobile and recommerce business is a compelling example of how we are identifying and integrating
complementary categories into our unique buy-sell-trade model. Though only in its second year, this segment greatly contributed to our margin
expansion and has added a considerable amount of currency to our buy-sell-trade ecosystem.
Were also optimistic about our international business, which performed well in 2012. We continued to refine our business model in every country
by implementing best practices from our United States operations and introducing new product categories to our stores around the world.
Along with driving new business initiatives, we continued to demonstrate capital discipline by returning more than 100% of free cash flow to our
shareholders through the combination of stock buybacks and dividends. In fact, we increased our dividend payout by 67% in 2012. Our board
and management team are focused on increasing shareholder return.
In a cyclical business like this, it is essential to drive a high rate of internal change to remain agile. Our entrepreneurial culture continues to propel
the organization forward to meet the changing needs of our customers. As we prepare to level up to the next console cycle, we are in a stronger
market share position than ever before, with the right strategies and tactics to drive continued growth.
On behalf of our board, executive leadership we thank you for your continued support of GameStop.
Daniel A. DeMatteo
Executive Chairman
J. Paul Raines
Chief Executive Officer

team and 40,000 team members worldwide