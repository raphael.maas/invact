Celebrate. Challenge. Consider It Solved.




  our Shareholders,

As Emerson moves from 2014 into 2015, we will                     ONE-YEAR PERFORMANCE
celebrate 125 years in business  a milestone few                                                                        PERCENT
companies can claim. For a century and a quarter,                 Dollars in millions, except per share amounts       2013      2014     CHANGE

Emerson has been a leader and an innovator of                     Sales                                           $24,669 $24,537           (1)%
the important technologies of our times. We take                  Net earnings                                    $ 2,004 $ 2,147           7%
tremendous pride in that legacy, which fuels us to                Diluted earnings per share                      $ 2.76 $ 3.03            10 %
                                                                  Diluted earnings per share
grow and improve for generations to come. This                    excluding charges*                              $    3.54 $    3.75        6%
noteworthy anniversary offers us an opportunity                   Dividends per share                             $ 1.64 $ 1.72              5%
to celebrate the past, evaluate present challenges                Operating cash flow                             $ 3,649 $ 3,692            1%
and define our vision for the future.                             Return on total capital                           16.4%   17.5%
                                                                   on equity                                          19.2%     20.7%
As we enter our 125th year, I cannot help but reflect
                                                                  B2014 earnings per share excludes a $0.72 goodwill impairment charge and
on our long and rich history. Emerson has evolved from             2013 excludes $0.78 of goodwill impairment and income tax charges.
a regional supplier of motors and fans, to a national
manufacturer of market-leading technologies, to a
                                                                  FIVE-YEAR PERFORMANCE
global engineering and solutions provider that drives
                                                                                                                        FIVE-YEAR
innovation across the world's most complex industries.            Dollars in millions, except per share amounts       2009      2014         CAGR

In a bygone era, Emerson helped consumers live and                Sales                                           $20,102 $24,537            4%
work more comfortably. Today, Emerson enables                     Net earnings                                    $ 1,724 $ 2,147            4%
the production and processing of energy in ways                   Net earnings per share                          $ 2.27 $ 3.03              6%
never before imagined; management of the digital                  Diluted earnings per share
                                                                  excluding charges*                              $    2.27 $    3.75      11 %
economy through state-of-the-art data centers;                    Dividends per share                             $  1.32 $ 1.72             5%
automation of factories that drives productivity and              Operating cash flow                             $ 3,086 $ 3,692            4%
savings; and climate control for people and food,                 Return on total capital                           16.2%   17.5%
providing unparalleled energy efficiency, reliability and         Return on equity                                  19.5%   20.7%
sustainability. It has been a challenging and rewarding           B2014 earnings per share excludes a $0.72 goodwill impairment charge.
journey  and it is just getting started.

As we have experienced over the decades, the world
is unpredictable and fast-changing, and this past
year was no different. The rate of underlying sales         continued, improving to 41.4 percent, exceeding last
change in 2014 improved from the previous year, but         year's record performance. Excluding an impairment
global macroeconomic expansion remained tepid,              charge related to the 2010 Chloride acquisition,
limiting growth. Underlying sales increased in all five     EBIT margin improved to 16.5 percent, even with
                                                            substantial investment acceleration to build upon our
                                                                                                                                                    Emerson > 03




segments, with growth in Network Power offset by
divestitures that will enhance our portfolio for future     technology leadership position, drive future growth
growth and profitability. Overcoming the challenges         and enhance the value of our global franchises.
of this past year, gross profit margin expansion
                                                                                                                                                             !
                                                                                                                                                             P
             Celebrate. Challenge. Consider It Solved.




                   i"#$%&'() charges, earnings per share increased           leaders to strengthen the company through an
                   6 percent to $3.75, a solid performance given the         intense focus on creating long-term shareholder value.
                   modest global growth environment.
                                                                             This mindset drove the strategic decision this past year
                   Emerson continued to generate robust cash flow,           to look beyond the uncertain economic environment,
                   reflecting the quality of our portfolio, continued        and to pivot toward accelerating internal investments
                   investment in our businesses and our commitment           to drive higher levels of growth. As we shared at our
                   to execution. Of the $3.7 billion in operating cash       annual investor conference in February, moving into
                   flow, 61 percent was returned to shareholders             2014 we recognized an unprecedented number of
                   through dividends and share repurchase, the fourth        long-term growth opportunities. Consistent with our
                   consecutive year exceeding a 60 percent payout. This      determination to look ahead to future value creation
                   capital allocation performance demonstrates our focus     and resist the temptation to think short term when the
                   on generating sustainable returns for shareholders,       environment is challenging, we aggressively pursued
                   which is further evidenced by another milestone: the      growth initiatives focused on global penetration,
                   completion of our 58th consecutive year of dividend       technology development, customer engagement and
                   increases. In November, the Board of Directors            transformative innovation. We made great strides
                   increased the first quarter dividend by 9 percent to      toward implementation of key strategic programs,
                   an annual rate of $1.88.                                  enhancing our position to create value in the near term
                                                                             and far beyond.
                   Emerson's foresight and willingness during the past
                   125 years to adapt and remake itself to meet the          A rigorous focus on creating and sustaining value
                   changing needs of customers, markets and investors        for our shareholders, customers, employees and
                   are remarkable. The ability to change, restructure        communities  regardless of economic trends  has
                   and reposition have long been fundamental to our          always been at the core of how we operate. As we
                   ongoing business success. Responding intelligently,       have continued to build upon our technology position
                   strategically and quickly to change, and in many cases    and lead our served industries, our customers have
                   initiating it, continues to be essential to maintaining   increasingly relied on us to solve their most difficult
                   our industry leadership.                                  challenges. The following pages of our annual report
                                                                             provide a glimpse into a few of these complex
                   Our perseverance and longevity are rooted in a culture
                                                                             challenges. But make no mistake, there are countless
                   of innovation, leadership, discipline, execution and
                                                                             examples of our Consider It Solved commitment to
                   long-term planning. These principles have guided
                                                                             our customers that define us and underscore what
                   us through periods of transition, disruptions and
                                                                             inspires us. And as we help our customers succeed,
                   growth since our beginning. They compel us to
                                                                             with a dedication to execution and superior quality,
                   invest when times are tough in order to ensure we
                                                                             our shareholders are rewarded as well, as reflected in
                   protect our global franchises and better position the
                                                                             robust returns decade after decade. Strong alignment
Emerson > 04




                   company for the future. As we have grown over time,
                                                                             between our culture and our commitment to our
                   the commitment to our foundational management
                                                                             customers, shareholders and employees has been 
                   process has been continuously renewed as we have
                                                                             and will continue to be  fundamental to our success.
                   seen its benefits allow each generation of Emerson
         !
         P
e0 we celebrate 125 years, we remain grateful for
your support. Rest assured that we are not slowing
down or looking back. In the near term, we are sharply
focused on improving growth and execution in 2015.
Orders momentum and record year-end backlog
suggest an improving environment for sales and
margin expansion, and we expect cash flow to remain
at high levels to support future investment. And, yes,
while next year is important, we constantly plan and
strategize much farther out. Emerson is positioning
itself for market and economic trends five, 10 and
20 years into the future. Our next milestone anniversary
may be many years away, but we are confident
Emerson will be stronger then than it is today, and our
customers, shareholders, employees and communities
will continue to benefit from our success.

Also key to Emerson's enduring success has been a
strong partnership between the Board of Directors
and management teams over time. I am grateful to
our Board for the invaluable insight and unmatched
integrity, support and leadership that it continues
to provide. We strengthened the Board further this
year with the addition of Dr. Candace Kendle, who
brings deep management capabilities and extensive
corporate governance experience that will be an asset
to Emerson and its shareholders for years to come.

On behalf of the Board of Directors and all of Emerson's
employees around the world, we invite you to join us
                                                           The Office of the Chief Executive (from left): Frank J. Dellaquila, Executive
as we celebrate our milestone next year, and we thank
                                                           Vice President and Chief Financial Officer; Edward L. Monser, President and
you for your confidence and trust.                         Chief Operating Officer; David N. Farr, Chairman and Chief Executive Officer;
                                                           Charles A. Peters, Senior Executive Vice President; Steven J. Pelch,
Sincerely,                                                 Vice President Organization Planning and Development
                                                                                                                                           Emerson > 05




David N. Farr
Chairman and Chief Executive Officer
                                                                                                                                                    !
                                                                                                                                                    P
