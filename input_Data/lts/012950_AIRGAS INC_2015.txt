To Our Shareholders,


From the day Airgas came into existence in 1982 through today, we
have put ourselves in our customers� shoes and asked ourselves
what we need to do to help them become bigger, stronger, more
profitable and more competitive. Every time we make a major
investment, acquire a new company or build out our capabilities,
we ask ourselves how it will impact and benefit our customers.
In the process, we have become a partner who our more than
1 million customers can trust to deliver quality products, provide
outstanding service and go above-and-beyond to meet their needs.
By focusing on our customers and remembering who owns
the company, Airgas has achieved more than three decades of
nearly uninterrupted growth. We grew again in fiscal year 2015,
though not as much as we anticipated. For the year, net sales
increased 5 percent to $5.3 billion, earnings per diluted share rose
4 percent to a record $4.85, and while our free cash flow was down
from all-time high levels the previous year, it was still strong,
allowing us to pay out quarterly dividends that were 15 percent
higher than the previous year.
Our strong cash flow levels also enabled us to continue
making acquisitions in fiscal year 2015. We acquired 14 industrial
gas and welding supply businesses, including facilities in seven
states across the U.S. and a rental welding equipment business in
the U.K., and began construction on two new air separation units.
By expanding through acquisitions and building better geographic
density, we are better able to serve local and national customers in
all market sectors. Our strong cash flow also supports the $500
million stock repurchase program which we announced in late May.
We continue to invest in new capabilities that are centered
on our customers. In this report, you will find examples of the
investments and efforts we are making to help our customers
succeed. This is hard work in a tough economic environment,
but I truly believe it will pay off.
From our new SAP operating system that gives customers
improved order tracking, reporting and transactional processing
capabilities � to our welding hardgoods vending machines that
can be located on-site for instant product access � to a more
robust eBusiness platform and an increased number of sales
channels that allow customers to access Airgas in the way that
works best for them, it�s easier than ever to do business with
Airgas.
And whichever way our customers access us, they know they
will receive the unmatched range of services, solutions, products
and expertise they�ve come to expect from Airgas. That�s because
our culture includes a strong drive to help our customers succeed
by any means necessary. During the past year, we continued to
make great progress in building our growing array of capabilities
aimed at keeping in front of customers� needs and providing a
tailor-fit selection of products and services to deliver exceptional
value in a variety of industries and markets.
For example, we have focused on providing the wine industry
with the supplies and products they need, including our newest
offering of sulfur dioxide used in conjunction with wine preservation
and fermentation, to produce a great vintage. To support our
laboratory customers, we released our new Guide to Gas Cabinet
Safety and Code Conformance in conjunction with our line of
standard cabinets for hazardous gases to help our laboratory
customers understand requirements and provide a safe and reliable
on-site storage solution for hazardous gases. We�ve invested in 

new specialty gas plant capacity in Tulsa, OK; Tooele, UT; and
Stryker, OH, to meet the growing needs in the analytical, industrial
and energy marketplaces, and have focused on developing
hydrogen packaged gas solutions and key relationships for
fuel-cell-powered materials handling equipment. We also continued
to refine and optimize our offering for our food and beverage
customers, ensuring the high quality, safe and fully compliant
systems, products, and certifications they demand.
Further, we continued to serve as the primary supplier of
gases and hardgoods to the contractor for the Cheniere Energy
liquified natural gas (LNG) project, one of the largest construction
projects in the country. The logistics of servicing this project are
impressive, including more than 30 on-site bulk gas tanks,
hundreds of cylinders, and the supply expertise needed to manage
it all. We take great pride in seamlessly providing this customer with
what they need, when and where they need it, so they can focus on
completing the job.
These initiatives are just a few among many examples of
the great, value-added work our associates have been doing in the
past year to support our customers in all markets�and this great
work bolsters my confidence in the long-term future of Airgas.
Yet, overall, fiscal year 2015 was disappointing. The year
was slow to start, but showed accelerating year-over-year
improvement through the December quarter. However, the fourth
quarter slowed considerably due in part to disruptions caused by
the strengthening of the U.S. dollar, the collapse of oil prices and,
yet again, another winter of extreme conditions.
The current sluggish economy has many similarities to the
period from 1998 through 2002 when the industrial economy
flat-lined and the dot-com bubble burst�causing a double dip
recession and a lot of caution on the part of our customers. Back
then, it felt like we were pushing on a string. The past few years
have been similarly frustrating, not in terms of the work we are
doing to create a more customer-focused company, but in the
results we are reaping from these efforts.
Still, we see many reasons to be optimistic, both on the micro
and macro level. During the past few years, while the economy
has struggled to gain momentum, we have performed very well
compared to our peers. We still believe the American manufacturing
renaissance is near and that the outlook for the U.S. economy in
the next 10 years is good. The signs are there. America is on track
to be energy independent by the end of the decade, and right
now a lot of companies from Europe and Japan are focusing their
investment dollars on the U.S.
Airgas is a very resilient business with a diverse customer
base. We always come out of these down economic periods
with a sustained growth burst, and I see no reason to believe it will
be any different this time. For the past 28 years, our company
has delivered a total compounded annual return to shareholders of
18 percent. In good economic times or bad, we move forward with
the build out of capabilities that will benefit Airgas, our customers
and our shareholders in the long run.
We truly understand our business and our customers. I know
we are focused on the right areas, and with a little help from the
economy, we are well positioned to outperform our competitors and
achieve outstanding results for our customers and our shareholders
going forward.
Peter McCausland
Executive Chairman
July 2015