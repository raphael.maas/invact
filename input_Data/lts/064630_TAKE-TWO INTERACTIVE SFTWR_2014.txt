Fiscal year 2014 was a record-breaking year for Take-Two, both commercially
and financially. Driven by our unique ability to produce consistently the
highest-quality entertainment experiences, our Company delivered 94% net
revenue growth, along with record net income and cash flow. These results
illustrate the innovation and talent of our creative teams, the depth of our
portfolio of intellectual property, and the strength of our marketing and
distribution capabilities. Today, Take-Two has a strong financial foundation and
is better positioned than ever for long-term growth and consistent profitability.
OUR KEY ACHIEVEMENTS
Over the past year, our team:
n Earned the position of top console and
handheld video game publisher of 2013
in North America and Latin America*.
n Launched Grand Theft Auto V, which
received more than 300 perfect scores
and shattered numerous industry records,
including reaching $1 billion in retail sales
faster than any entertainment release in
history. Developed by Rockstar Games,
the title has sold-in more than 33 million
units worldwide and continues to attract
new fans as millions of players enrich
their experience through the dynamic and
evolving world of Grand Theft Auto Online.
More than 70% of those who have played
Grand Theft Auto V while connected to
the Internet have played Grand Theft
Auto Online, which was the single largest
contributor to our digitally-delivered
revenue in fiscal 2014.
n Delivered the highest-rated and topselling basketball video game for the 13th
consecutive year with NBA 2K14. The
title was our first for next-gen and has
surpassed NBA 2K13 as our highestselling sports release, with sell-in of
more than 6.5 million units to date.
The success of NBA 2K14 was enhanced
by strong demand for the game�s virtual
currency, sales of which increased 150%
versus NBA 2K13.
n Launched WWE 2K14, which has sold more
units than its predecessor and proven to
be a successful addition to our portfolio.
n Released 8 new downloadable content
packs for Borderlands 2, which has
become the top-selling release in the
history of 2K, with over 9 million units
sold-in to date.
n Launched numerous titles for tablets and
smartphones, including core titles such
as Grand Theft Auto San Andreas, XCOM:
Enemy Unknown and NBA 2K14.
n Generated record digitally-delivered
revenue, nearly half of which was derived
from recurrent consumer spending,
including virtual currency, downloadable
add-on content, and online games.
n Ended the year with nearly $1 billion
in cash, after spending $277 million to
repurchase our stock at a meaningful
discount to today�s share price.
DEAR
SHAREHOLDERS,
2
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2014 ANNUAL REPORT
PROVEN ORGANIC GROWTH STRATEGY
Take-Two�s strategy is to develop the highest-quality, most compelling interactive entertainment
franchises and deliver them through all relevant platforms and distribution channels. Creating
opportunities to drive ongoing engagement with our titles after their initial release is a key strategic
focus for our Company, and we have started to generate significant revenue and profits from
recurrent consumer spending. Our record fiscal 2014 results demonstrate that our strategy works.
World-class creative teams: Take-Two has 14 studios around the world and approximately 1,900
employees working in game development, including some of the most talented visionaries in our
industry. Our creative teams at Rockstar Games and 2K are renowned for their ability to deliver
games that consistently set new benchmarks for excellence, with Rockstar Games recently being
awarded the BAFTA Fellowship for their contributions to the games industry. High quality games
tend to have prolonged lifecycles, which is reflected in the strong catalog sales that our titles enjoy.
Diverse portfolio of industry-leading intellectual property: We have built one of the most valuable
collections of intellectual property in our industry. Take-Two has 10 franchises with individual titles
that have sold-in 5 million or more units. These franchises are diversified across a range of genres.
To date, we have released more than 40 distinct titles that are each multi-million unit sellers. We
continually strive to balance our development efforts between new intellectual property and new
offerings from our established franchises. Since 2007, we have added 7 new franchises, including
such hits as BioShock, Borderlands, WWE 2K and XCOM.
Capitalizing on growth of digital distribution: Our expanding portfolio of digitally-delivered content
is contributing significantly to our growth and profitability, both as video game distribution shifts
towards full game downloads and we introduce additional offerings that drive recurrent consumer
spending. During fiscal 2014, digitally-delivered revenue grew 45% and accounted for 16% of our
total net revenue. The majority of this growth was driven by recurrent consumer spending within
Grand Theft Auto Online, NBA 2K14 and NBA 2K Online in China, and downloadable add-on content
for Borderlands 2, BioShock Infinite and Sid Meier�s Civilization V. We will continue to focus on
developing additional offerings that promote engagement with our titles, deepen our relationships
with consumers and boost profits over time.
Expanding offerings for tablets and smartphones: We believe that the popularity of video games on
smaller screens, especially tablets, will grow at a rapid pace. This represents an exciting opportunity
for Take-Two. Our releases to date have focused primarily on popular catalog titles from both
Rockstar Games and 2K, as well as current sports titles. As mobile technology continues to evolve,
we fully expect to release our most groundbreaking and immersive new titles on every mobile
device that core gamers choose to embrace.
Innovative marketing and global distribution: Creating great games is only part of our formula
for success. Our marketing teams roll-out well-coordinated global campaigns that leverage
both traditional and online media. And, we work in lockstep with our key retail partners to drive
promotions when they matter the most � at point of sale. Our global distribution network ensures
that our products are available to consumers wherever they want them � physically or digitally.
Sound financial foundation: Take-Two has a strong balance sheet and ample capital to pursue a
variety of investments. As of March 31, 2014, we had $935 million in cash and no borrowings on our
$100 million credit line. Going forward, we are very excited about our opportunities for growth, and
our first priority is to invest in our business, both organically and potentially through acquisitions.
We may also consider returning additional cash to shareholders through share repurchases
and/or dividends.
3
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2014 ANNUAL REPORT
ROBUST LINEUP OF NEW RELEASES
Fiscal 2015 is poised to be another strong year for our organization, highlighted by a diverse array
of the highest-quality new releases and innovative offerings that drive incremental profits from
recurrent consumer spending. To date, we have announced the following titles planned for release
during the current fiscal year:
n On October 7, 2K will enhance their unparalleled basketball legacy with the launch of
NBA 2K15, featuring four-time NBA scoring champion and 2014 NBA Most Valuable Player,
Kevin Durant, as its cover athlete. We�re confident that NBA 2K15 will once again set new
benchmarks for sports realism and gameplay.
n On October 14, 2K will release Borderlands: The-Pre-Sequel that will continue the franchise�s
tradition of innovative, cooperative multiplayer action.
n On October 21, 2K will release Evolve, an exciting new intellectual property for next-gen
and PC developed by Turtle Rock Studios, the creators of the cooperative shooter classic,
Left 4 Dead. The title blends first-person and third-person shooter action with cooperative
and competitive multiplayer in a unique, 4 versus 1 gameplay experience. Evolve is one of
this year�s most eagerly anticipated games and won more than 50 editorial honors at the
2014 Electronic Entertainment Expo, including the coveted Best of Show from the E3
2014 Game Critics.
n On October 24, 2K will build on their successful Civilization franchise that has entertained
audiences for nearly a quarter of a century and sold-in more than 24 million units worldwide,
with the release of Sid Meier�s Civilization: Beyond Earth for PC. The title will feature the core
and tactical elements for which the series is famous, while propelling players beyond the
traditional timeline of a Civilization game by exploring humanity�s future on an alien world.
n On October 28, 2K�s WWE franchise will make its next-gen debut with the release of
WWE 2K15, which promises to take this series to new heights. 14-time WWE Champion,
John Cena, will be its cover athlete and Sting � one of wrestling�s most beloved superstars �
will play a key role in the title�s pre-order campaign.
n This fall, Rockstar Games will bring their blockbuster hit, Grand Theft Auto V, to PS4,
Xbox One and PC, bringing across-the-board graphical and technical enhancements to
the game�s already breathtaking open world experience. In addition, the current community
of players will have the ability to transfer their Grand Theft Auto Online characters and
progression to their choice of next-gen platforms. All new content and gameplay created
for both Grand Theft Auto V and Grand Theft Auto Online since launch will also be available
for the PS4, Xbox One and PC with more to come. Rockstar Games will continue to support
both Grand Theft Auto V and Grand Theft Auto Online with new offerings throughout the
year, including regular content drops, limited-time event updates and additional Story
Mode content.
4
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2014 ANNUAL REPORT
Our robust pipeline extends far beyond fiscal 2015. For example, in fiscal 2016 2K plans to
introduce Battleborn, an all-new intellectual property developed for next-gen and PC by
Gearbox Software, the creative team behind our Borderlands franchise. Battleborn is a highly
stylized blended-genre game, combining frenetic first-person shooting, cooperative combat,
and an expansive collection of diverse heroes to deliver a never-before-played hero-shooter
experience. We also have numerous, yet-to-be-announced next-gen titles in development,
including both groundbreaking new intellectual properties and offerings from our established
franchises that promise to delight consumers.
OUR FUTURE
Over the past several years, Take-Two has been transformed into a financially strong, global
interactive entertainment enterprise with numerous successful franchises encompassing a variety
of genres. Today, our Company is defined by its top creative talent; unwavering commitment to
quality; and world-class marketing that turns product launches into pop-culture events.
The successful evolution of Take-Two and its vast potential is reflected in our significant
profit outlook for fiscal 2015 and our expectation for continued positive momentum.
We�d like to thank our colleagues for delivering a record year and setting the foundation for
our consistent long-term success. To our shareholders, we want to express our appreciation
for your continued support.
Sincerely,
July 15, 2014
* According to The NPD Group and International Development Group
Strauss Zelnick
Chairman and
Chief Executive Officer
Karl Slatoff
President