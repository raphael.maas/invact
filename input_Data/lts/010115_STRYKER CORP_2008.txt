Charles Darwin said that it is not the strongest of the species that survives, nor the
most intelligent, but the one most responsive to change. This quote may be 150
years old, but it seems remarkably fitting at this time. Few of us could have fully
envisioned many of the events that unfolded in 2008: a global economic meltdown,
unprecedented commodity price increases followed by rapid decreases,
similar effects in the global currency markets, the failure of centuries-old financial
institutions, and further unprecedented bailout packages in country after country
around the worldfrom the United States to Europe to Asia and South America.
These events created enormous challenges for companies in all industries. We at
Stryker were not immune, as many of our hospital customers and government
payors around the world cut back on spending, especially capital purchases, late
in the year. Combined with a heightened regulatory environment within the
healthcare industry, the events of 2008 clearly brought a unique set of challenges
to your Company as well, which resulted in a disappointing decline in our stock
price during the year.
Nevertheless, we are pleased to report that revenue grew a very healthy 12%, and
adjusted diluted net earnings per share from continuing operations were up 18%.
In a year where liquidity and cash flow were vital, our free
cash flow grew a healthy 22%, and exceeded $1.0 billion for
the first time ever. While our earnings growth fell short of
the 20% we were aiming for at the start of the year, our sales
and earnings performance still ranked at the very top despite
these challenging times.
For perspective, we invested over $50 million more than
budgeted in quality and compliance initiatives. Importantly,
we could have delivered the original 20% goal by taking an
action that many companies took in 2008by eliminating
the Companys discretionary contribution to our 401(k)
plans, which provide retirement benefits to our U.S. employees.
In an era where many big companies risk undermining
employee loyalty for short-term gain, and at a time when the
hard-working middle class in America is facing greater challenges,
we are confident this decision will pay off in the years
ahead through the continued loyalty and hard work of our
teams. While we certainly feel the pressures to deliver in the
short term, we also continue to manage the Company for the
long term, and hope you support us in this decision.
We are also very proud of achieving an eighth straight year
of double-digit revenue growth. To put this accomplishment
into perspective, it appears that only around a dozen
companies in the Fortune 500 have achieved this goal for
eight consecutive yearsputting us in a very select group.
Furthermore, we are one of only two manufacturing companies
on this list, and the only healthcare manufacturer. In
a period when many manufacturing industries face great
challenges, we think this accomplishment reflects the hard
work of our teams around the world.
A Position of Strength
While we face both the internal hurdles mentioned above, as
well as the external forces of the global economy and heightened
regulatory scrutiny, we are pleased to be able to tackle
these matters from a position of strength. In these turbulent
times, our financial position and our people have never been
stronger.
In 2008, six of our eight main franchises achieved industry-
leading growth rates, our best-ever performances across
many businesses. Our commitment to globalization and
innovation has been driving our knee, spine, trauma, CMF,
instruments and medical franchises to industry-leading levels.
And we are achieving this growth in different ways. For
example, much of our growth in our spine, trauma and CMF
franchises over the last few years occurred by strengthening
our resources in the United Stateswhere these franchises
were historically weaker. Conversely, our important instruments
and endoscopy franchises achieved significant growth
outside the United States during this time, where these
franchises have great upside opportunities. As we look to
the future, we are pleased by our progress, yet also encouraged
that we have meaningful opportunities for continued
expansion.
Our Evolution
As many of our long-term shareholders know, we have been
a strong Company generating superior results over a long
period of time. However, in recent years, our industry and
our Company have been increasingly facing new challenges.
In addition to new accounting regulations for all businesses,
the medical technology industry has also experienced
enhanced scrutiny, as demonstrated by increased actions
of government agencies in both the U.S. and abroad. In
response, we have redirected numerous resourcespeople
and financialto address investigations by the Department
of Justice and the Securities and Exchange Commission into
our industry. We have spent a great deal of time on these
matters and have made significant changes in how we do
business as a result of our own heightened awareness in these
areas. While we are involved in fiercely competitive markets
with difficult challenges, we want to ensure that we deliver
our numbers the right way.
As part of our own evolution, you have read over the last
few years about the four imperatives driving our continued
growth: globalization, innovation, people development and
leveraging across divisions. We developed these imperatives
to be long-term and enduring, to provide consistency and
clarity to guide our evolution. Events of the past few years,
however, also demonstrated a need for us to significantly
dial up our commitment to compliance throughout the
organization.
As a result of Food and Drug Administration (FDA)
inspections in 2006 and through 2007, we received three
regulatory Warning Letters citing deficiencies in our quality
systems, a clear signal that we could do better. In reflecting
on the situation, two observations become apparent to us.
Two of the tenets of our successan intense focus on customer
service levels and our decentralized structurehave
resulted in different approaches to quality and compliance
systems across different divisions. Now it is time for us to
evolve in these areas. As with all of our evolutions, our goals
are to retain the strengths we established while adapting to
the new and energizing challenges.
We are now developing more robust systems to document,
investigate and improve deficiencies, and to better
manage an increasing number of external suppliers. The
result will be a more consistent and harmonized approach
to compliance systems across the Company. Simply put, this
presents a big challenge for us and will require increased
resources and investments, but will make us a much better
company over time.
Our goal here is to ultimately exceed the FDAs expectations
and become much stronger. While we would prefer to
be in a different situation, you can be assured that our leadership
team has fully embraced this challenge, and our teams in
every plant around the world are attacking this challenge with
tremendous Stryker can do spirit. We feel that we achieved
significant progress in 2008, but we know that much work
remains to achieve our goals. In the long run, we are confi-
dent that taking these steps now will make us even stronger,
as well as create efficiencies and cost-savings opportunities
in the years ahead. Thus, todays investments should yield
future gross margin improvements through greater yields,
reduced scrap rates and lower warranty expenses.
Our Evolving Leadership Team
As we continue to grow, our leadership team also continues
to evolve. As 2008 came to a close, we announced that Si
Johnson and Dean Bergy, two outstanding members of the
Stryker leadership team, would hand over their reins in 2009.
All of our long-term shareholders recognize Si Johnson as
the exceptional leader of our dynamic MedSurg businesses
for roughly the last decade, delivering results that have consistently
exceeded expectations. While those results have
been visible to the outside world, Si has quietly built on the
exceptional leadership development of his predecessor, Ron
Elenbaas, and has been developing a cadre of outstanding
leaders throughout his career.
While no single individual can fill Sis shoes, we have
been grooming a number of executives under Sis mentorship
for a significant period of time. In 2009, three of these executives
Tim Scannell, Lonny Carpenter and Curt Hartman
assumed expanded leadership posts. Having three strong
executives ready to take on greater responsibilities has given
us several options we did not anticipate several years ago when
we initially looked ahead to this inevitable moment. In a testimony
to our management depth, these three executives are
no rookiesas each has at least 18 years of experience with
the Company, and all bring tremendous insight and perspectives
to their new roles. Tim, who began his Stryker career in
1990 at our Endoscopy division and has most recently been
president of our top-performing Spine division for the past
five years, will now have responsibility for both Spine and
Endoscopy. Lonny, who began in our Instruments division
in 1989, has most recently been president of our fast-growing
Medical division and will now oversee both divisions. As
you can see, these moves leverage experience and continuity
while also providing new perspectives from executives who
have been running our fastest-growing divisions.
Meanwhile, Curt Hartman, who joined Stryker in 1990
and has guided our exceptional Instruments division since
1999 following Si Johnsons move to group president of
MedSurg, will take over as CFO in April 2009 from Stryker
veteran, Dean Bergy. In addition to being a very close business
partner to me, Dean has ably guided our finance team
over the last six years and is applying his usual dedication to
assuring a smooth hand-off to Curt during these turbulent
times.
While much attention has been focused on CEOs over the
last few decades, Stryker is fortunate to have an experienced
group of senior executives and leaders throughout the organization
with a deep understanding of our need to evolve,
and tremendous pride and ownership in our Company. As
we enter 2009, we know that the members of this team can
challenge each other, including me, to become better every
day. Together, we faced a number of difficulties in 2008, yet
still delivered strong resultsand we are inspired to continue
to make our Company even better in the years ahead.
Outlook
As I write this, 2009 is shaping up to be every bit as challenging,
if not more so, than 2008. Externally, it is sure to
be a very difficult year for the global economy, and we are
likely to see widespread layoffs at other companies and oncestrong
companies fighting for their survival. In the United
States, the healthcare reform debate is likely to intensify, creating
both pressures and opportunities alike. We expect that
many of our hospital customers will continue to hold back
on capital expenditures, and the dramatic currency swings
of late 2008 are likely to further depress sales and earnings
for global companies in the first several quarters. Simply put,
there is greater uncertainty entering 2009 than at any time in
most of our business lives.
While the external environment will likely be tough,
including a soft market for hospital capital expenditures,
many of our franchises have solid underlying momentum,
good new products and significant geographic expansion
opportunities. Additionally, our own excellent cash position
combined with current valuationsis likely to create
opportunities for us on the acquisition front. We have signifi
cantly increased the number of ideas we have evaluated in
the last year, and this will be an area of even greater focus in
2009. Operationally, we are heavily resourcing our compliance
activities, while applying more stringent cost and headcount
controls in order to maintain and enhance our strong
financial position. While the challenges and distractions may
be at an all-time high, our teams commitment and people
are well prepared. At the end of the day, we still believe that
talented teams with great, life-changing products will help us
generate strong results in 2009 and beyond. While the shortterm
environment is a daunting one, we remain optimistic
about the long-term growth of our Company.

Sincerely,
Stephen P. MacMillan
President and Chief Executive Officer