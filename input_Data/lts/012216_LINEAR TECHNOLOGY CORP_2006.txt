TO OUR STOCKHOLDERS

25years from startup to the analog high-performance
franchise. 25 years of performance, leadership and results.
The job is both well done and just begun.
The beginning1981. The analog market was $ 2 billion
serviced by divisions of large multiproduct semiconductor
manufacturers. Consumer, industrial and military were
the primary end-markets; most analog products were commodity generic functions.
The vision. Start a specialty analog company predicated
on progress in digital technology fostering the need for
new high-performance analog.
The strategy. Continuously design and develop cutting edge analog integrated circuits. Attract, develop and
reward the very best technical talent to invent new, more precise, faster, smaller, better integrated products.
Support those products with an energetic, technical sales force and with a flawlessly executing manufac-
turing infrastructure. Sell the products for their functional value. Reward customers with feature-rich products, stockholders with industry leading financial returns, and
employees with a challenging professional environment and unmatched total compensation.
The execution. Its a good 25 years. We have been a
leader in technology in introducing industry renowned products including multiphase switching, Hot SwapTM controllers, Power-over-Ethernet drivers, micromodule power supplies, etc. In total we have introduced roughly 7,500 products, spanning four analog product groups: power management, mixed signal, signal conditioning and high frequency.
These products have permeated several diverse end-
markets, evolving over time in response to then current
demand. In our initial five years we serviced primarily the military market followed by industrial; in our second

five years industrial was the leader with computing
becoming 25 % of our business by year 10; in the
third five years computing became the leader, growing
to 35 % by year 15 with communications reaching
19 %; in our fourth five years communications became
dominant at 42 % in year 20. In our most recent five years
communications and industrial were the strongest,
with high-end consumer and automotive beginning
to show their promise for the upcoming period. In fiscal
year 2006 34 % of our business was in communications,
within which 15 % was in networking, 12 % in cell phone
infrastructure, and 7 % in handsets. 33 % of our business
was in industrial, including medical, 14 % was in
computing, 9 % in high-end consumer, 7 % in automotive,
and finally, 3 % in satellite and military.
After initially focusing primarily on the domestic US market
we have become multinational with talented support
personnel in the US, Europe, Japan and Asia. This past year 30 % of our revenues were in the US; 18 % in Europe;
14 % in Japan and 38 % in the rest of Asia.
Manufacturing execution has always been critical to our success. We were one of very few companies to build a wafer fabrication plant in which we manufactured
our very first product. Today we continue this heritage
and are vertically integrated, performing 95 % of our own
wafer fabrication, 80 % of our own assembly and 100 %
of our own testing. This year we added to our capacity
by increasing our cleanroom capacity in both our wafer
fabrication plants in Camas, Washington and Milpitas,
California. We also completed and occupied a new
building at our Singapore test and worldwide distribution
location. These capacity additions give us the basic infra-
structure to roughly double our existing revenue capacity
when we add the required equipment and labor.

We strive to present our customers with excellent quality,
reliability and on-time delivery. Accordingly, over the 25 years we have received many outstanding supplier awards from customers around the world.
The results. Our 25 years had a good 25th year. We
reported record revenues of $ 1.093 billion, an increase
of $ 43.3 million, or 4 % over the previous year, which had
benefited from a $ 40.0 million royalty payment. Net
income of $ 428.7 million was down $ 5.3 million. The
resulting diluted earnings per share were $ 1.37 versus
$ 1.38 in fiscal year 2005. Fiscal year 2006 results
include the effects of the implementation of Statement
of Financial Accounting Standards 123(R) (SFAS
123R) Share-based Payment. The after-tax charge
for all forms of stock compensation, including stock
options, restricted stock and the employee stock
purchase plan, was $ 38.3 million versus $ 14.5 million
for only restricted stock in fiscal year 2005. Consequently, net income on a pro forma basis would have
been $ 466.9 million versus pro forma net income of
$ 448.5 million in fiscal year 2005. On a pro forma basis,
before the impact of SFAS 123R, fiscal year 2006
diluted earnings per share would have been $ 1.50
versus $ 1.42 per share on a similar pro forma basis
in fiscal year 2005.
Once again the Company generated positive cash flow
from operations. The overall cash and short-term invest-
ments balance increased by $ 28.7 million, net of
spending $ 342.8 million to buy back 9.5 million shares
of common stock and paying $ 153.9 million in cash
dividends. During the year we increased the quarterly
cash dividend payment by 50 % from 10  per share to
15  per share per quarter. We initially began paying a
dividend in 1992 and have increased it every year since.

During our 20 years as a publicly traded company we
have been profitable every single year. The cumulative
20 year return on sales was 38 %. For fiscal year 2006
return on sales was 39 % and return on equity was 21 %.
Profits before tax exceeded 50 % of revenues for the
45th consecutive quarter, over 11 years. Cash flow from
operations was positive for the 81st consecutive quarter, over 20 years.
The talent. Results take talent. Analog is a complex craft.
We have recruited, developed, challenged and com-
pensated a disproportionate share of uniquely skilled
individuals. In 2006 we opened two new design centers in Munich, Germany and Dallas, Texas. We closed the year with 12 circuit design locations including our headquarters in Milpitas, California.
The franchise. In September 2006 the Company will be 25 years old. Old enough to know our craft; young enough to be driven and focused to be the best: the best products,
the best service, the best financials.
To our employeeskeep raising the bar.
To our customers and stockholderswe plan to be the high-performance analog franchise for years to come.
Thank you all for your support.
Sincerely,

Robert H. Swanson, Jr., Executive Chairman

David B. Bell, President

Paul Coghlan, Vice President,
Finance and Chief Financial Officer