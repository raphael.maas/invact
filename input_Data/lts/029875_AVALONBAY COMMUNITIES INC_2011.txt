to our Shareholders

2011 was a great year for AvalonBay. Led by impressive same store Net
Operating Income (NOI) growth of over 8%, we enjoyed Funds from Operations
per Share (FFO) growth of just over 14%  our best performance in over
ten years. Our development, investments, and redevelopment platforms were all
active, laying the foundation for strong growth in the years to come. We continued
to fortify our already best-in-class balance sheet with additional capital
raising activity of nearly $1 billion. For the year, our Total Shareholder Return
was 19%, while our ten-year average Total Shareholder Return stands at 16%.

Well established demographic trends created a powerful tailwind for rental performance, marked
by strong growth in younger age segments more likely to rent than own, and a continued decline
in the homeownership rate, particularly among young adults. This increased demand was met
with exceptionally low levels of new rental supply, with completions running at the lowest levels
in over 30 years.
We continue to enjoy an advantaged position within our industry, distinguished by our 25-year
history in some of the best markets in the US -- markets with key structural advantages. We use that
experience, including the established relationships and deep market knowledge that come with
it, to further add value through a diverse set of capabilities including operations, development,
redevelopment, and asset management. Additionally, our capital markets expertise and our conservative
balance sheet management allow us to access capital on terms which further extend our
competitive advantage and provide opportunities that others may not be able to exploit.
As we begin 2012, Tim steps into the CEO role with a sharper focus on our time-tested
strategy. We plan to continue to build on our advantaged position and more deeply penetrate our
chosen markets. To appeal more specifically to the most profitable customer segments within our
markets, we recently announced the introduction of two new brands to complement our core
Avalon product offering. We are confident that our multi-brand strategy will contribute to outperformance
in the future, and we see tremendous opportunities to apply it within our portfolio
and submarket allocation strategy. By utilizing our proprietary insights from our growing capabilities
in market research and consumer insight, we look forward to Targeted Growth in 2012
and beyond.
The Year in Review
On a national and international level, 2011 was a year of uncertainty and unmet expectations
for economic recovery. The debt ceiling debate in Washington, sovereign debt crisis in Europe,
and continued fragile consumer confidence contributed to disappointing job growth, averaging
just shy of 150,000 net new jobs per month. The corporate sector, enjoying record liquidity, was
reluctant to expand due to lack of confidence in future growth given the uncertainty that seemed to
be everywhere.
Despite the backdrop of this weak macro environment, we did experience strong demand/supply
fundamentals in the apartment sector. Growth in jobs and households was concentrated among
young adults, who are more likely to rent than own. In fact, rental household growth of 1.2 million for
the year represented 84% of total household growth, a far higher share than long-run averages. And

with new completions of only 80,000 apartments, representing just 37% of the 15-year average,
demand/supply conditions were favorable for raising rents in 2011.
In our same store portfolio, we benefited from this imbalance of demand and supply with revenue
growth of 5%. Combined with a decline in operating expenses of over 1%, same store NOI
growth ended the year at over 8%. As the year progressed, rental revenue accelerated in our
West Coast markets, particularly those markets driven by technology, while East Coast markets,
which led our portfolio earlier in the year, enjoyed strong but moderating growth.
In addition to the operating performance of our existing portfolio, increased investment activity
also drove results, reflecting an organization creating value from multiple platforms, including:
Development
Development is the primary driver of Targeted Growth over time. We were early in ramping
up our development activity, investing in anticipation of the strong fundamentals our
markets currently enjoy. The benefit of this foresight will accrue to us over time as we deliver
new development at an attractive cost basis and bring new communities on line in what we
expect will be a great environment for new lease-ups. In 2011, we completed six new
communities totaling over 1,100 apartment homes for a Total Capital Cost of almost $300
million, and started 11 additional communities representing 3,100 apartment homes at a
projected Total Capital Cost of approximately $900 million. At year end, we had about
$1.5 billion of development underway.
Portfolio Management
2011 was an ideal time to redevelop assets with greater Targeted Growth potential.
We invested additional capital early in the cycle and used our expanded brand portfolio
to target different customer segments based on the profile and location of the asset and
its residents. We started ten redevelopments during the year for a Total Capital Cost of
approximately $85 million, and completed seven redevelopments representing approximately
$70 million of additional investment.[3] We also phased in our occupied turn
program at all but the most complex redevelopment communities. Under this pioneering
initiative, we typically renovate our apartment homes while our customer remains in residence,
allowing for shorter, more predictable redevelopment schedules and lower
vacancy loss. This program is a real competitive advantage, and we are now applying it
as standard practice in redevelopment.
In addition, we were opportunistic in pursuing our broader portfolio allocation objectives
by selling several communities in the Washington, DC and Boston areas and acquiring
eight communities in Southern California, significantly increasing our presence there.

Capital Management
As we grow our investment activity to take advantage of the opportunities we are finding
in the market, we are ever mindful of the need to source cost effective capital to fund this
activity while preserving and enhancing the strength of our balance sheet. To this end,
we raised over $950 million of equity in 2011, and reduced our debt outstanding by
$430 million. This reduction in leverage, combined with strong earnings growth,
resulted in a significant improvement in key credit metrics by year end:
?? Debt-to-Total Market Capitalization of approximately 23%, as compared to
the sector average of 38%.
?? Debt-to-EBITDA of 5.3x, as compared to the sector average of 7.4x.
?? An Interest Coverage ratio of 4.0 compared to a sector average of 2.8.
With significant Targeted Growth planned for 2012 and beyond, we enhanced our
liquidity position, finishing the year with $700 million in cash on hand and $750 million
of available capacity under our credit facility.
Adding it all up, FFO per share increased 14% for the full year, or 17% after adjusting
for non-routine items. Earnings per Share (EPS) increased 135%, driven by gains on assets
sold as we harvested value created over the past real estate cycle. [4] On the strength
of this performance and our outlook for 2012 and beyond, we announced a 9% dividend
increase in the first quarter of 2012.
Looking Forward: Targeted Growth
Outlook for 2012
Prospects for 2012 look promising. The consensus forecast for the US economy is for modestly
stronger growth, as the corporate sector starts to put some of its liquidity to work through increased
hiring and capital investment. Wage growth is also expected to improve after years of productivity
gains and suppressed income growth from soft labor market conditions. In the apartment market,
demand should remain strong, as the demographic tailwind continues and consumer psychology
continues to favor the flexibility of renting. Meanwhile, new completions nationwide in 2012 will
continue to run below the long-term average.

Similar to last year, we expect Targeted Growth to come from multiple platforms in 2012. With compelling
industry fundamentals, we expect another strong year from our stabilized portfolio. We will
continue to expand our investment activity through new development, with over $1.1 billion in new
starts planned for the year, while we bring on line approximately 2,100 apartments in communities
currently under construction. We also plan to invest in our existing portfolio through redevelopment
starts of another $125 million. Finally, we expect to address our portfolio allocation goals through
transaction activity, including both acquisitions and dispositions. This Targeted Growth will be
supported by our strong balance sheet and robust organizational capabilities.
Strategic Evolution
Weve had a long-standing focus on structurally advantaged coastal markets with high household
incomes, lower home affordability and constraints on new supply. In last years report,
we discussed the importance of submarket focus and price point/asset quality in optimizing
overall portfolio performance. Because no single strategy or product type offers consistent
outperformance in each of our markets, the right market position for any particular asset will vary
depending on the asset and the submarket  our business is not one size fits all. Our brand strategy
is a natural extension of this conclusion, and provides a key vehicle for us to better target our
product and service offerings to multiple customer segments and geographic submarkets within
our footprint. Our expanded brand lineup includes three distinct offerings:
Avalon remains our core offering, focusing on upscale apartment living and high-end
amenities and services in leading urban and suburban submarkets. Avalon branded
properties will continue to grow primarily through new development.
AVA is designed to attract the increasing number of people, particularly the younger
Gen Y segment, who want to live in or near high energy, transit-served urban neighborhoods.
AVA communities will generally have smaller apartments, many designed for
roommate living, and feature modern design, a technology focus and amenities that
maximize the social experience of residents. The AVA brand will grow primarily through
new development and redevelopment.
eaves by Avalon rounds out our brand lineup, and is targeted to the resident seeking
moderately-priced, good quality apartment living with practical and functional amenities
and services. eaves by Avalon communities will tend to be located in suburban areas
and will generally be established assets, with the brand growing through redevelopment
and acquisitions, and, in certain circumstances, through the re-branding of existing assets.
We opened our first three AVA communities in 2011 (two redevelopments and one development
community), and anticipate having approximately 20 open or under construction by the end of
2013. We plan to re-brand over 40 existing communities to eaves by Avalon by the end of 2013.
The addition of these new brands will allow us to further penetrate our existing markets, and provide
for Targeted Growth by segmenting our markets by consumer preference and attitude as well
as by location and price. By delivering distinctive offerings to different customer segments, AVA
and eaves by Avalon will help us reach new customers and better serve our existing customers,
all while staying within our established geography.
Conclusion
This is an exciting time for our industry and for AvalonBay. Tims transition to CEO is a reflection of
stable and steady leadership, resulting from a senior management team that has worked together
for 20 years with leadership from Bryce over the last ten years, providing deep institutional knowledge
and perspective. The fundamentals look great, our markets continue to be the strongest in the
country, and we are in an enviable competitive position given our track record and long-standing
commitment to our markets. Through a well-designed extension of our time-tested strategy, we are
positioned for Targeted Growth in the future that will enhance our sector-leading performance.
As always, thank you to our shareholders for your continued support, to our associates for your
extraordinary efforts, and to our residents who have chosen to make an AvalonBay community
their home.

Bryce Blair,
Chairman
Avalon Wilton, Tim Naughton,
Chief Executive Officer & President
