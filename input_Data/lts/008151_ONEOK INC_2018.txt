CONNECTIONS ARE CENTRAL
TO OUR SUCCESS.

Whether we�re physically connecting customers to our integrated
network of assets; connecting our assets to one another; or connecting
with our stakeholders by building relationships � the connections
we�ve made throughout our company�s history have strengthened and
sustained us through the many ups and downs in the energy business.
Thankfully, 2018 wasn�t simply a good year for ONEOK � it was an
exceptional year of exciting growth opportunities and realized potential.
Over the course of 2018, we announced approximately $5.5 billion in
additional natural gas liquids (NGL) and natural gas projects, bringing the
total investment in our current capital-growth program to more than
$6 billion. These projects are anchored by long-term, fee-based contracts,
volume commitments and/or acreage dedications.
A $6 billion growth program is impressive by most standards, but it�s the
capital discipline and attractive returns we expect from these projects
that are reflective of our core strategy to provide high-quality midstream
services to our customers while creating long-term shareholder value.
Once completed, these projects will significantly increase capacity in some
of the most prolific shale plays in the country where we already operate an
extensive network of assets. We expect this investment in our business to
be the foundation for even stronger returns to our investors and position
us to not only continue to provide but also expand the reliable services our
customers expect and need both in the near and long term.
In addition to executing on the capital-growth projects announced in
2018, we:
� Significantly improved over 2017 our environment, safety and health
(ESH) performance, meeting or exceeding our 2018 targets, which
translated into fewer injuries, fewer vehicle incidents and a reduced
impact to the environment because of our operational performance.
� Increased NGL volumes gathered and natural gas volumes processed
across our system by 12 percent and 16 percent, respectively,
compared with 2017.
� Completed construction of more than $500 million of capital-growth
projects, including expansions of our West Texas LPG system,
Sterling III Pipeline and Canadian Valley plant.
� Increased operating income and adjusted earnings before interest,
taxes, depreciation and amortization (adjusted EBITDA) by 32 percent
and 23 percent, respectively, compared with 2017.
� Increased our dividends paid in 2018 to $3.245 per share, a
19 percent increase compared with 2017.
� Achieved full-year dividend coverage of nearly 1.37 times.
� Ended the year with a strong balance sheet and a debt-to-EBITDA
ratio of 3.83 times on a trailing 12-month basis, compared with
4.6 times at the end of 2017.
This performance further strengthens our businesses as we evaluate
future opportunities while remaining focused on sustaining our
investment-grade balance sheet by operating and investing in a
disciplined manner that delivers strong returns for our stakeholders.
In addition to the 2018 achievements already mentioned, we�re proud
to have been named Best of the Best � Great Companies to Work For by
Oklahoma Magazine; awarded the Horizon Award by the Sidney, Montana,
Area Chamber of Commerce and Agriculture; given the Gas Processors
Association�s Environmental Excellence Award for our MB-1 fractionation
facility in Mont Belvieu, Texas; and recognized as a Veteran Employer
Champion by the Oklahoma Veteran Employer Champion Network.
It�s been an outstanding year on all fronts � financially, operationally,
commercially and from an ESH standpoint. The successes of 2018 are
attributable to our talented, diverse and committed employees whom we
thank for not only executing on our vision but also doing so in a manner that
exemplifies our core values � ethics, quality, diversity, value and service.
Our employees continue to work safely, reliably and in an
environmentally sustainable manner that ensures the integrity of
our assets and company, and we are very proud of what they�ve
accomplished this year.
Thank you to our board of directors for its guidance and support
as we navigate this exciting time for our company. We continue to
refresh our board of directors as demonstrated by the election of
Mark Helderman to the board in February 2019. Mark recently retired
from Sasco Capital Inc., an independent, institutional investment firm,
and his experience and expertise will further strengthen the board and
benefit our shareholders.
And finally, thank you to our investors for your continued trust and
investment in ONEOK. 2019 will be a year of execution, as we bring more
of our announced projects online that are expected to drive significant
earnings growth in the years to come and look for new opportunities
both in the U.S. and abroad. It will take the hard work of everyone to
complete these projects on time and on budget, but we will continue
to challenge ourselves to think of new and innovative ways to solve
problems and build long-lasting connections.

John W. Gibson
Chairman

Terry K. Spencer
President and Chief Executive Officer

March 5, 2019