To our stockholders

                                                                     
2006 was a year of great success and accomplishment for Eastman Chemical Company. We
strengthened our portfolio. We launched new technologies. And we met most of the high goals
we set for ourselves.

More important, however, we set the stage for the Company to continue taking the initiative to
grow profitably. After several years of reshaping our Company into a leaner, more stable and
profitable operation, we're now setting our sights squarely on the future and moving Eastman
into a new and exciting chapter of its transformation.



                                                        A BASE OF STRENGTH
A STRONG 2006
                                                        The results from the past year showcase a
The year was a good one for the Company,
                                                        base of strength for Eastman. Four of our five
demonstrating strength across most of our
                                                        segments performed exceedingly well, continu-
businesses:
                                                        ing to provide us with a solid base from which
 Sales revenue hit another record 
                                                        to grow our Company.
  $7.45 billion;
 Operating earnings were a healthy
                                                        Operating earnings in our strong base of
  $640 million, down a bit from our very
                                                        businesses  Fibers, CASPI, and Specialty
  strong 2005 levels, primarily due to results
                                                        Plastics  were just over $500 million with
  in our PET polymers business;
                                                        an operating margin of 16 percent. The
 Return on Invested Capital was 14 percent,
                                                                                                                    
                                                        Performance Chemicals and Intermediates
  well above our cost of capital;
                                                        segment also continued to deliver strong
 We ended the year with net debt (total
                                                        results, with operating earnings of
  debt minus cash and cash equivalents) of
                                                        $132 million and an operating margin
  $653 million  down almost $450 million.


                                                        Coal gasification has been a vital part of Eastman
                                                        Chemical Company for nearly a quarter of a century.
                                                        We were the first company in the United States to
                                                        commercialize a coal gasification facility and we are
                                                        the only one that produces commercial quantities of
                                                        acetyl chemicals from coal.

                                                        Given the cost advantages of using raw materials
                                                        produced from coal and other feedstocks in a




      of 8 percent. Combined, our strong base of        facility by another 100,000 metric tons over
      earnings and the PCI segment contributed          the next twelve to eighteen months.
      almost all of the Company's operating earnings
      for the year. And their 2006 operating earnings   Second, we are rationalizing 350,000 metric
      were comparable with 2005  which were our        tons of our higher-cost PET capacity at
      best earnings in 10 years.                        our South Carolina facility. We expect to
                                                        complete this action by the middle of 2008,
                                                        at which point we will have over 50 percent
      TAKING AGGRESSIVE ACTIONS
      Only our polyethylene terephthalate (PET)         of our U.S. PET production based on IntegRex
      product lines within the Performance Polymers     technology.
      segment did not perform up to our standards
      during the year. This business continues to be    Third, we are aggressively pursuing options,
      impacted by difficult global market conditions    which could include selling or shutting
      and high and volatile paraxylene prices.          down our PET sites outside the United States.
                                                        Earlier this year, for instance, we announced
      As a result, we are taking a three-pronged        an agreement to sell our PET plant in Spain.
      approach to improve this challenging business.
      First, we have built a 350,000 metric ton         The combined result of these actions will
      PET manufacturing facility based on our           be to transform our PET business so that in
      IntegRex technology and we are getting            the second half of 2008 it can have operating
      excellent feedback from our customers on          margins of approximately 10 percent. We are
      the ParaStar Next Generation PET that we          on track to meet this target.
{2}
      are producing from it. This technology
      dramatically reduces capital and conversion       Two other actions we took in 2006 will help
      costs relative to existing technology. In         our business going forward. First, we sold our
      addition, we will increase the capacity of this   polyethylene and Epolene businesses at our
North American-based gasification facility  as           propylene to our Texas operations and a second
opposed to natural gas and oil-based products            project that would supply methanol to Eastman
we intend to aggressively build on our gasification       for conversion into ethylene glycol.
and process technology expertise going forward.
                                                          These projects represent significant first steps
We have said, for example, that we are pursuing           in our plan to transform our raw material cost
two projects, one being an industrial gasification        position and will position us for growth in existing
facility in the United States Gulf Coast to provide       and new products.




         Texas facility. This action, along with our         The IntegRex technology, for example,
         commitment to shut down at least two of             gives us an unprecedented cost position
         the four cracking units at our Texas facility,      in the manufacturing of PET polymers.
         will contribute to improving our overall
         operating results.                                  But technology and innovation does not stop
                                                             with our PET polymers businesses. Several
         Second, we divested our manufacturing facility      projects involving industrial gasification 
         in Arkansas. This site mostly produced fine         a natural outgrowth of our long experience
         chemicals and was no longer strategic to            with coal gasification  are currently in
         our future.                                         development. We have said, for example, that
                                                             we are pursuing two projects, one being an
         Taking the initiative to address these remain-      industrial gasification facility in the United
         ing trouble spots for the Company will move         States Gulf Coast to provide propylene to our
         us into a new chapter in our life. By moving        Texas operations and a second project that
         beyond the "fix-it" stage, we are now looking       would supply methanol to Eastman for
         to the future as a more stable, profitable and      conversion into ethylene glycol.
         revitalized company.
                                                             With these and other projects, the Company
                                                             expects over time to increase its product
         RESPECTING THE PAST 
                                                             volume derived from gasification-based raw
         BUILDING TOWARD THE FUTURE
         One of the historical strengths of Eastman          materials to 50 percent from 20 percent today.
         Chemical Company has been its legacy of             By taking advantage of the lower costs of solid     {3}
         innovation and technology. Our future will          hydrocarbons versus traditional gas and oil, we
         be built on that tradition.                         can enable the Company to achieve low, stable
                                                             cost positions, and we can potentially enter new
                                                             markets for chemical products.
            


      The future of PET polymers is in Columbia,        The world's most innovative, efficient, and
      South Carolina.                                   cost-effective process to produce PET.

      That's where we've begun operations based on      Producing this new generation of PET poly-
                                                        mers  which we call ParaStar Next Generation
      a new manufacturing process to produce PET.
                                                        PET  with our unique IntegRex technology 
      We envisioned and developed the technology. We
      built a new plant based on the technology. And    positions Eastman as the lowest-cost, highest-
      we did it all in about three years. The result?   quality PET producer in the world.




                                                        to improve the overall portfolio of assets
      IMPROVED FINANCIAL PROFILE
                                                        and businesses. And we have the financial
      ENABLES GROWTH
      I'm proud to report that Eastman's manage-        strength  and discipline  to pursue all
      ment team has worked very hard over the past      of these initiatives.
      several years to improve our financial profile,
      and that effort has paid off. Our stockholders'   The challenge for us now is to take hold of
      equity is the highest in our Company's history,   the opportunities in front of us  the ones that
      while we have reduced our net debt by over        we've worked so hard to develop  and make
      70 percent in the last three years.               them work. And if I know one thing about
                                                        Eastman's men and women, I know that they
      This improvement we have made is grounded         are capable of delivering on that promise.
      in the strong cash generation from all of our
      businesses that we expect will continue. Our      I am committed  we are committed  to our
      strong financial profile leaves us well posi-     pledge to use our unique technologies, our
      tioned to pursue our corporate strategy while     unique capabilities and our unique heritage to
      maintaining the disciplined approach that has     create a long and lasting value for you. That's
      served us well.                                   what Taking the Initiative is all about.

                                                        Sincerely,
      TAKING THE INITIATIVE
      The bottom line is that Eastman is a stronger,
      more focused and more stable company today.
      We have a tremendously solid foundation of
{4}
      businesses that I believe will provide strong
      earnings going forward. We have many              J. Brian Ferguson
      exciting investment and growth opportunities      Chairman and Chief Executive Officer
      in development. We will continue our efforts
