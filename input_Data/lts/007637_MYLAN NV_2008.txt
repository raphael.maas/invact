Dear Fellow Mylan Shareholder:
Never in Mylans history has there been a year of such
tremendous change. Our overnight transformation from
the third largest generic pharmaceuticals company in the U.S.
to the third largest generics and specialty pharmaceutical
company in the world is nothing short of amazing.
Having substantially completed the successful integration of our 2007 game-changing
purchases of the former Merck Generics business and a majority interest in Matrix
Laboratories, 2008 represents the first full year that Mylan operated as one of the worlds
largest and most efficient producers of generic and specialty pharmaceutical products.
Throughout the year, we integrated operations, cultivated a talented management team
and installed global systems. We also significantly broadened our product portfolio and
leveraged our commercial footprint while further streamlining our cost structure.
We now provide high quality products to our customers in more than 140 countries and
territories and are managing the largest U.S.-based generic pharmaceuticals company
in the world.
The commercial and operational scale we now control allows us to offer more products
to more people in more places than ever before. We view this opportunity as a great
privilege and look forward to the tremendous global impact Mylan will have in the
years to come.
Mylans strong, highly motivated management team and talented, high-caliber
employees are responsible for the execution of this extraordinary achievement. Their
collective resolve to excel, make tough decisions and take on additional responsibility,
all while maintaining the quality for which were well known, have revolutionized our
company and set a course for a successful future. On behalf of our board members and
fellow shareholders we extend to them our sincere appreciation for a job well done.

Swift integration
Among our top priorities in 2008 was completing the integration
of the former Merck Generics business, whose acquisition we
closed in October 2007. This transaction greatly expanded our
commercial footprint, by about 90 countries, and presented us
with numerous opportunities to streamline our cost structure.
To take full advantage of these opportunities we rationalized
research and development (R&D) activities throughout our
global network by eliminating duplicate projects and relocating
others. We also put in motion plans to optimize our finisheddosage-
form manufacturing capacity.
And perhaps most significantly, Matrix-furnished active
pharmaceutical ingredients (API) are now a key element of
many products in our pipeline.
Overall, integration efforts produced in excess of $100 million
of synergies in 2008. Moreover, in 2009 and 2010 we expect
to capture additional value as we reduce expenses and
increase efficiency further.
Robust pipeline
One of the major components of the long-term health of any
generics and specialty pharmaceutical company is its ability to
continually introduce new products while building its pipeline
for future launches. Mylan delivered exceptionally in 2008 on
both fronts.
Our R&D productivity grew substantially as we leveraged
Matrixs scientific capabilities. We submitted 154 product
applications globally, 72 of which were abbreviated new drug
applications, or ANDAs, filed with the U.S. Food and Drug
Administration (FDA). This is almost triple the number of
ANDAs we filed in 2006, with nearly half contributed by
the efforts of Matrix. We ended 2008 with a total of 120
ANDAs pending FDA approval, 32 of which were first-to-file
opportunities  both record highs for Mylan.
We also received final approval on 24 ANDAs from the
FDA during the year. Among them were five first-to-file
opportunities, such as Nisoldipine ER and Levetiracetam,
again demonstrating our proven ability to pursue and
monetize attractive product opportunities.
In addition to leveraging our extensive internal product
development capabilities to expand our offerings, we partner
periodically with other companies that value our extensive
global footprint and long-standing reputation for product
quality, reliability and customer service.
Solid commercial performance
Just as remarkable as our integration and pipeline
accomplishments were the results of our commercial operations.
With respect to our Generics segment in North America, the
strongest product mix in our history enabled us to generate
revenues in this region of $1.85 billion. This was due in part
to multiple launches, including some where we were the sole
generics supplier in the U.S., the worlds largest market.
We also benefited from continued strong sales of Fentanyl in
the U.S., as recalls and other issues hampered certain
manufacturers ability to meet market demand. Our ability to
quickly ramp up production and ensure a reliable supply of our
products is a proud tradition on which we continue to build.

The Europe, Middle East and Africa (EMEA) and Asia Pacific
(APAC) regions of our Generics segment delivered solid
2008 results, including revenues of $1.52 billion and $537
million, respectively.
The generics markets in most countries comprising these two
regions are less mature than in the U.S. However, theyre also
very dynamic, as some governments act to increase generic
utilization rates and reduce costs. We employ varied
commercial strategies to seize opportunities and manage risks
as these markets continue to evolve. For instance, in 2008
we reorganized operations in the highly competitive United
Kingdom and rapidly changing Australian markets, and
launched a pan-European rebranding campaign to broadly
and formally introduce Mylan throughout the regions
promising markets.
Meanwhile, our Matrix segment generated third-party revenues
of $376 million in 2008. In addition to growth in the sales of
API, revenues from Matrixs rapidly growing antiretroviral
(ARV) franchise benefited from a full year of operation of its
newly created finished-dosage platform.
Finally, Dey, our Specialty segment, contributed third-party
revenues of $386 million in 2008. Behind this healthy
performance was the continued success of the businesss
flagship EpiPen (Epinephrine) product, which treats
severe allergic reactions, as well as Perforomist Inhalation
Solution (Formoterol Fumarate), which relieves the symptoms
of chronic obstructive pulmonary disease, or COPD.
In September we announced, after an extensive strategic
review, that we intend to retain Dey, realign its operations
and realize its full potential.
On the strength of this solid commercial performance, Mylan
earned, on an adjusted basis, $0.80 per diluted common share
for the full year, exceeding our stated financial expectations.
Strong financial foundation
Complementing our solid commercial results, we delivered
strong operating cash flows and continued to proactively and
opportunistically manage our capital structure.
In the fall we issued $575 million in cash convertible notes
and used the net proceeds to pay off outstanding borrowings
under our revolving credit facility. This reduced and fixed a
portion of our cash borrowing costs and extended the maturity
of some of our borrowings out to 2015. We also prepaid all
2009 debt maturities under our term loans.
Based on these actions, combined with our unrestricted,
year-end cash balance of approximately $550 million and our
expectation for higher operating cash flow, we believe Mylan
has a strong financial foundation and is well positioned from
a balance sheet and liquidity perspective.
Bright future
Looking ahead at 2011, 2012 and beyond, we anticipate
action on multiple growth initiatives, some of which we have
already begun. For example, we continue to see growth in
APAC by optimizing our No. 1 position in Australia and
leveraging strong prospects in Japan. In EMEA, we are poised
to leverage the progress we have already achieved and will

now be looking for additional growth from our Central and Eastern European operations,
which we acquired in June by exercising an option under our original agreement with
Merck KGaA.
In the U.S., we will be looking for business development opportunities where we can
leverage our leadership position, and we will continue to put an emphasis on Dey,
fully expecting it to continue contributing to Mylans growth strategy going forward.
The next several years will also be particularly important from a U.S. legislative
perspective. With the new presidential administration in place, we will continue to
work on policy issues that matter most to our industry and to consumers throughout
the country.
Our path  both near- and long-term  is clear: We intend to build upon the market
leadership weve established. We expect to deliver performance and quality through
outstanding execution. And we plan to continue expanding our portfolio, a commitment
that will include  when the time is right  a high-value generic biologics portfolio.
In closing, 2008 was an amazing year for us and reflects our ambition to accomplish
everything we set out to do. Our actions and results continue to demonstrate an
unyielding commitment to deliver the most in terms of shareholder value. We took
calculated strategic risks, and now, as we look ahead, we see those decisions beginning
to form enhanced long-term growth prospects for an even brighter future for Mylan
and its stakeholders.
We want to thank those shareholders who have stood by us faithfully as we transformed
Mylan, and we welcome our new investors, who understand the value of our strategy.
It is our objective to reward both groups with superior financial performance in the
years to come.
Sincerely,

Milan Puskar 
Chairman
Robert J. Coury
Vice Chairman and CEO
