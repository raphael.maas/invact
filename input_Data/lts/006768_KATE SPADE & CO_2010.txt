DEAR STOCKHOLDERS, ASSOCIATES, BUSINESS PARTNERS, AND FRIENDS:
Brand strength, brand power. Thats what fuels the performance of the most admired companies in
our industry.
In the past four years, weve made sweeping changes to our organization to support and nurture brand
power. These changes have included decentralizing management of our brandsuniting control of
decisions across channels, geographies, and product categories under separate and distinct brand
leadership. This adds to brand strength by ensuring a consistent vision is implemented everywhere. We
also chose to invest more aggressively in direct-to-consumer retail capabilitiesto build a company
with ultimately better and more sustainable margins.
Given the magnitude of change required to deliver on our vision and to restore our financial acuityand
the treacherous economy we did it inI am pleased to say we are beginning to see it come together
operationally, and we are getting closer financially.
In 2010, these brand-strengthening strategies started to play out. We saw major success with the
Liz Claiborne brand in the U.S. By choosing one highly motivated and deserving in-store partner,
we have effectively re-launched the brand in mens and womens apparel, in accessories, and even
in categories across home. Our license with JCPenney and the vertical integration we have achieved
have returned the brand to high growth rates, capital efficiency, and profitability, with a commanding
presence in JCPenney stores and marketing campaigns. Coupled with the success of Liz Claiborne
New York on QVC, this brand is on the path to return to its status as an industry leader.
The Kate Spade brand also demonstrates how our strategy can work. We spent 2007-2009 in this
brand quietly investing in talent and teams, investing in direct-to-consumer capabilities, and investing
in brand marketing with razor sharp targeting and positioning. Weve taken what was a sleepy, little
company with a strong brand but weak execution, and turned it into one of the industrys fastest
growing affordable luxury, lifestyle brands. And this business is just getting startedwith significant
potential in markets all across the world.
Juicy Couture is a brand with a clear target audience globallyand a positioning that is enduring
and sustainable. In 2010, Juicy Couture grew three-fold overseas, while the business retrenched in
the U.S. Consumer research continues to show this to be a very buoyant brand with a large and
enthusiastic following, but we recognized the need to re-energize the brand creatively. In 2010, we
hired LeAnn Nealz as President and Creative Director. Now, we are undergoing the same kind of
internal transformation at Juicy Couture that we implemented two years ago at Kate Spade. With a
strong international presence and demand as well as a developed channel in direct-to-consumer in the
U.S., we can move quickly to refresh this already iconic business.
At Lucky Brand Jeans and Mexx, our transformation is still a work-in-progress. The operational and
talent improvements we saw in both of these businesses during 2010 can only be categorized as
world class. But even the most experienced leaders and teams need time in their roles to get the
positioning and targeting of a brand just right. This means real-time learning, real-time tweaking. 
Already I see discipline and creativity coming together in both businesses, and in 2011, they will both
push these processes further along toward building lasting success.
With these steps forward, I remain very enthusiastic about the future of our Company. I wish it came
together faster. I wish the recovery and growth in these businesses would happen more consistently
and simultaneously. But we remain focused on doing it right, and getting it right.
I am pleased that, in 2010, Liz Claiborne Inc. reduced its losses compared to 2009, cutting our
full year operating losses and improving gross margin. The balance sheet improved for the year,
highlighted by an $80 million reduction in total debt with bank debt of $23 million and availability
of $240 million under the Companys bank credit facility at year-end. These signs of recovery are
encouraging, but our leadership team has not taken its collective eye off the prizean industry
leading portfolio of brands with the power and strength to deliver top tier financial performance.
Throughout the turnaround, we have always kept our diverse and valuable portfolio of brands front of mind
and recognize that there are many different ways to create value for our shareholders over time.
We appreciate your ongoing patience and support, and we look forward to delivering for you.
Sincerely yours,
William L. McComb
Chief Executive Officer