To Our Shareholders

Johnson & Johnson achieved strong, balanced performance
in 2004, posting record results in sales, earnings and cash
flows. It has been an outstanding year in which each of
our business segments made important contributions.
Worldwide sales reached $47.3 billion, an increase of
13.1 percent over 2003, reflecting operational growth of
9.7 percent and a positive currency impact of 3.4 percent.
This represented the 72nd year of consecutive sales growth.
Net earnings and diluted earnings per share for the
year, as reported, were $8.5 billion and $2.84, increases of
18.2 percent and 18.3 percent, respectively, as compared
with 2003. Adjusted net earnings for the year were $9.3
billion and earnings per share were $3.10(1).
Thus, Johnson & Johnson in 2004 completed two full
decades of consecutive double-digit earnings increases,
excluding special charges, a record of consistent long-term
performance matched by few, if any, companies.
Our cash flow from operations continued to be very
strong, at $11.1 billion, and facilitated our ability to enter
into the largest planned acquisition in our history  a definitive
agreement to acquire Guidant Corporation for $25.4
billion. We hope to conclude this transaction in the third
quarter of 2005, creating an unparalleled cardiovascular
device business that addresses a broad range of conditions
from blocked coronary arteries to arrhythmias to congestive
heart failure.
Strong cash flow also enabled many smaller but important
business building investments throughout the year, even
as we increased our quarterly dividend for the 42nd year
in a row, up 18.8 percent from $.24 to $.285.We accomplished
all of this while maintaining a triple-A credit rating, a
designation earned by very few companies.
Financial strength is a great asset for Johnson & Johnson.
It gives us exceptional flexibility in managing our operations
and aggressively pursuing strategies for growth. It brings us
added confidence and gives us extraordinary staying power
as we pursue innovation and build our businesses for the
decades to come.
Were pleased to see that the returns enjoyed by our
shareholders once again reflect the strong underlying performance
of our Company. We believe this reflects at least
in part a growing recognition of the value of our broadly
based business  a business with a record for both superior
growth and consistency.
In fact, we are the worlds most comprehensive and
broadly based company in human health care.
Being broadly based gives us enormous advantages as an
organization. It helps drive consistent performance. When
one area of the business is challenged, another area is likely
to be growing robustly. When one area of the business
requires investment for future growth, another is likely to
be generating particularly strong cash flows.

No single franchise or brand accounts for even 10 percent
of our sales. And while our portfolio is well balanced across
a breadth of areas, most of our brands, in their own right,
are leaders in their particular fields.
As important as consistent performance is, breadth
brings us much more. It actually elevates our performance
by promoting synergy and collaboration between Johnson
& Johnson units in different areas of health care.
In categories ranging from wound care adhesives to
drug-coated devices, experts in pharmaceuticals, devices,
diagnostics and consumer products are finding that collaboration
can lead to breakthroughs. Internal collaborations
are occurring with increasing frequency throughout
Johnson & Johnson.
Our breadth gives us a unique line-of-sight and close proximity
to many advancing areas in the science and technology
of human health. This makes us aware of opportunities that
may be missed by others with a narrower focus in their
business. In effect, our broad base lets us pursue medical
advances no matter what course they take.
For Johnson & Johnson, breadth becomes strength,
because we continually build leadership positions in the
fastest growing segments of health care. The fastest growing
areas are usually those with the highest levels of unmet
medical need. They also tend to be areas where innovative
technology plays the largest role in improving patients lives.
Whether these growth opportunities are in medicines,
devices, diagnostics, consumer products or nutritionals,
the extraordinary balance of our portfolio typically puts us
in a good position to pursue them. Upon the anticipated
closing of the Guidant Corporation transaction, we will
have a projected portfolio of products comprising roughly
42 percent pharmaceuticals, 41 percent medical devices
and diagnostics, and 17 percent consumer products. So
we are well balanced. And, through our decentralized
structure that puts management at the levels closest to the
customer, each of our diverse businesses is growing at an
impressive rate.
In the stories that follow in this report, you will see
example after example of leadership positions that contribute
to strong performance today and growth potential
for the future. Let me mention just a few highlights of
the past year.
In recent years, our Consumer segment has consistently
achieved high levels of growth relative to the industry.
This growth has been achieved in part by an aggressive
schedule of innovative product launches in major markets.
These numbered over 200 last year. We continue to
emphasize technology-driven, clinically-proven products to
meet growing consumer health needs.
Weve seen unprecedented growth in skin care. And,
driven by SPLENDA No Calorie Sweetener, our nutritionals
business is also gaining great momentum.
Besides providing attractive levels of growth, the
brands that make up our Consumer segment are remarkably
enduring. Generations of consumers recognize these
names: JOHNSONS Baby products, BAND-AID Brand,
NEUTROGENA, TYLENOL.
Consider that BAND-AID Brand is an 85-year-old
product line that still shows strong growth. Endurance plus
growth is a unique proposition offered by our consumer
products.
Our Medical Devices and Diagnostics segment is characterized
by strong leadership positions in high-growth medical
specialties like cardiology, orthopaedics, minimally invasive
surgery, diabetes testing, and cancer detection. The primary
focus of the group is on serious life-threatening and debilitating
diseases. In these categories patient demand for
high-technology solutions is strong.

Many programs and projects in Medical Devices and
Diagnostics helped drive the excellent results. For Cordis,
2004 was a successful  though challenging  year. Although
the CYPHER Sirolimus-eluting Stent faced competition in
the United States and Europe, it continued to build a body
of strong clinical evidence supporting its efficacy in the
treatment of coronary artery disease. A major factor contributing
to the franchises success was CYPHER Stent
marketing and reimbursement approval in Japan. This made
CYPHER Stent the first drug-eluting stent available in the
worlds second largest interventional cardiology market.
DePuy delivered the first artificial lumbar disc available
in the United States when the U.S. Food and Drug Administration
approved the CHARIT Artificial Disc in October.
In diagnostics, Veridex announced a breakthrough diagnostic
system based on detection of circulating tumor cells. Called
CELLSEARCH, the system helps physicians manage treatment
for patients with metastatic breast cancer.
In Pharmaceuticals, we are unique among our competitors
because of our unparalleled balance in both small molecules
and biologics. In small molecules, we are a therapeutic leader
in central nervous system, pain and reproductive health
with products such as RISPERDAL, DURAGESIC and hormonal
contraceptives. In biotechnology, we are the second
largest global biotech company with therapeutic leadership
in anemia management and immune-mediated inflammatory
disease, as well as promising new growth engines in cardiology,
oncology and virology.
Our leadership in Pharmaceuticals is based on superior
innovation. Although we aggressively invest in research
and development, we realize that the best ideas are not
all homegrown. Because other companies increasingly
consider us the partner of choice, we have extensive and
growing access to alliances and collaborations, which
numbered nearly 100 last year alone.
Indicative of our breadth is that we now have eight
product categories with sales greater than $1 billion. We
dont rely on a handful of blockbuster products to support
our growth. With 18 products having sales over $200 million,
our balance allows us to weather the ups and downs
of individual products and deliver more consistent growth.
Beyond financial success, 2004 has been a very successful
year in building for the future in Pharmaceuticals. In
addition to 14 regulatory filings around the world, we
received 21 regulatory approvals. These new formulations
and line extensions will be key to continued growth in our
Pharmaceutical segment.
Ive described a number of factors that reflect our enthusiasm
for the future. But, of course, were not unaware of
challenges ahead. DURAGESIC, for example, already faces
generic competition, and similar competitors are likely for
products like CONCERTA. Viewed in the context of our longterm
growth, challenges such as these can be characterized
as bumps in the road. We have faced challenges such as these
successfully in the past and we will do so in the future.
But there is a broader challenge whose resolution is less
predictable. That is the business environment for pursuing
health care as a private enterprise. Clearly the global regulatory
environment is growing tougher; pressure is being
put on companies over the cost of health care, and private
enterprise is under close public scrutiny.
As we work in partnership with others to ensure a sound
future for health care, we keep the tenets of Our Credo
foremost in mind. We are convinced that an honest focus
on the long-term interests of patients, their families, and
the doctors and nurses that serve them will yield good
public health policy and a business environment in which
we can play a productive role.
Our Credo reminds us that, if we serve patients and
customers, our employees and our communities well, our

shareholders will prosper. And, in fact, our shareholders
have been receiving excellent returns for an exceptionally
long time. Our Credo has stood the test of time and continues
to deliver its promise just as we expect to continue
delivering excellent business results.
We recognize that we have a responsibility to help society
meet the challenges of making medical advancements accessible
and affordable. Two U.S.-based programs in particular
demonstrate our willingness to live up to that responsibility
 the Together Rx Access Card Program and the Partnership
for Prescription Assistance. Johnson & Johnson, along
with nine pharmaceutical companies, launched the Together
Rx Access Card to provide savings of 25 to 40 percent and
more on prescription drugs and other prescription products
to many of the more than 45 million Americans who dont
have prescription drug coverage. The Partnership for Prescription
Assistance, a voluntary collaboration among
doctors, nurses, patient organizations and companies like
ours, helps low-income, uninsured patients secure access to
free or nearly free prescription medicines in an efficient way.
We are also mindful of our commitment to the communities
where we do business and where our employees live
and work. Our world witnessed the most devastating natural
disaster of our generation in the earthquake and resultant
tsunami in Asia late last year. Johnson & Johnson, as a
company and as a collection of concerned private citizens,
responded to the need by providing in excess of $81 million
in product donations and committing more than $3 million in
financial aid. We are committed to continuing our support for
the redevelopment of infrastructure that supports the people
of the region over the intermediate and long term, as well.
I want to close now by encouraging your optimism for
the future of Johnson & Johnson.
We expect to continue delivering excellent results. Our
focus on enduring brands, on serving areas of significant
medical need, and on building a platform of science into all
our businesses, will ensure our continued leadership in
health care. Internal product development and collaboration,
external partnerships and alliances, and strategic acquisitions
will enable us to bring forward innovation to address emerging
health care needs. Most importantly, our results will be
built on the strength, quality, and character of our people.
It is our diverse and committed staff from all around the
world that will continue to build this remarkable business.
They deserve our thanks for their hard work and dedication.
We achieve leadership through strategic growth, and
our growth is fueled by visionary leadership. We are mindful
of our need to develop leaders within our Company and to
identify strong advisors for our Board of Directors whose
guidance will help chart our course. In the past year, there
were a number of important developments in this area.
Christine Poon, Worldwide Chairman, Medicines and
Nutritionals, joined the Office of the Chairman as Vice
Chairman in January 2005 and will stand for election to
the Board of Directors next month. Nicholas J. Valeriani
assumed responsibility as Worldwide Chairman, Cardiovascular
Devices, upon the announcement of the planned
acquisition of Guidant Corporation. He will retain his
responsibility as Worldwide Chairman, Diagnostics. Kaye
Foster-Cheek succeeds Nick as Vice President, Human
Resources, and member of the Executive Committee.
We also recognize three Directors who will retire from
our Board of Directors in April. Dr. Gerard Burrow, who
joined our Board in 1993, and Dr. Judah Folkman, who
joined in 1998, will both be missed for their guidance
on health care trends and research and development as
members of our Science & Technology Advisory Committee.
Henry Schacht, who joined the Board in 1997, has capably
chaired our Nominating & Corporate Governance Committee,
an area of shareholder focus in the past few years, and his
leadership will be missed.
Thanks to all our associates for your dedication, and
thank you to our customers and shareholders for your
continued confidence. Its been a great year, and we are
looking ahead with enthusiasm.
William C. Weldon
Chairman, Board of Directors,
and Chief Executive Officer
March 16, 2005