FELLOW SHAREHOLDERS:
TransDigm�s consistent strategy delivered another year of strong value creation for
our shareholders. The combination of consistently high cash flow and less attractive
2017 acquisition candidates led us to return approximately $3 billion, or $53 per share,
to shareholders, through a combination of special dividends and opportunistic
stock buybacks.

Despite distractions and challenges in fiscal 2017, our key operating
performance metrics improved significantly over the prior year. Net
sales (excluding discontinued operations) were up 11% and EBITDA As
Defined increased 14%. The company generated almost $800 million
of cash from operating activities. We ended the fiscal year with
substantial and growing liquidity, providing us the financial flexibility
for ongoing growth and future acquisitions.
Our shareholders continued to do well. As of January 22, 2018,
the date of this letter, our shareholders realized a 42% return over
the last 12 months. Due to unusual volatility in our stock, this return
can vary widely depending on the measurement date. Additionally, our
long-term returns to our shareholders of 29% over the last five years
and 32% over the last 10 years are well in excess of the S&P 500.
In recognition of our performance and our unique business model,
TransDigm was named to the Forbes 2017 World�s Most Innovative
Companies list, an honor our company has also received multiple
times in the past.
UNIQUE BUSINESS MODEL AND VALUE-FOCUSED STRATEGY
Our consistently strong performance reflects the ability of our unique
business model and strategy to create and sustain intrinsic shareholder
value through all phases of the aerospace market cycle.
We consistently provide reliable, well-engineered products, and deliver
them on time. Proprietary products generate more than 90% of our net
sales, and about 80% come from products for which we are the sole
source provider. More than half of our revenues and a much higher
percentage of our EBITDA come from aftermarket sales. Aftermarket
revenues have historically produced a higher gross margin and have
provided relative stability through the aerospace and defense cycles. 

Our longstanding goal is to give our shareholders private
equity-like returns with the liquidity of a public market.
To do this we follow a consistent long-term strategy:
Q Own and operate a wide range of proprietary niche aerospace
 businesses with significant aftermarket content.
Q Follow a simple, well-proven value-based operating methodology
 based on our three value driver concepts: generate profitable new
 business, improvements to our cost structure and value-based pricing.
Q Maintain a decentralized organization structure that keeps us close
 to the customer and a compensation system that closely aligns our
 management with shareholders. This enables us to attract and
 retain entrepreneurial managers who think and act like owners.
Q Execute a focused and disciplined acquisition process where we
 see a clear path to value creation.
Q Remain appropriately leveraged to optimize shareholder returns.
Effective long-term capital allocation is also fundamental to creating
value. We view our capital structure and capital allocation as key parts
of our effort to create shareholder value. Our priorities for capital
allocation are, in order, (1) invest in our existing businesses, (2) make
accretive acquisitions consistent with our strategy and return requirements,
(3) give back to the shareholders either through special dividends or
stock buybacks, and (4) pay off debt. Under current capital market
conditions, the low cost of capital makes paying down debt our
lowest priority.
Over the past three years, we have experienced different business
conditions and managed capital allocation accordingly. In 2015
and 2016, we found a number of attractive acquisition opportunities
and acquired approximately $3 billion of proprietary aerospace
businesses that met our strategic and shareholder return criteria.
In fiscal 2017, we were in a different environment. We found few
attractive acquisition candidates, acquiring only three add-on
aerospace product lines. Given the situation, we elected to allocate
about $3 billion of our capital to return to shareholders.
Since the beginning of fiscal 2015, we have returned $3.2 billion
to our shareholders and acquired $3.2 billion of proprietary aerospace
businesses. We continued to fully invest in our existing businesses,
and maintain a healthy cash balance and the financial flexibility
for attractive acquisitions. As we�ve done consistently in the past,
in 2018 we will evaluate business and capital market conditions,
allocate capital and structure our balance sheet in what we determine
to be the best way to maximize the return to our shareholders.
WELL-POSITIONED FOR ONGOING GROWTH
Sales to the commercial aerospace market account for almost 70%
of our revenue, with sales to the defense market making up most
of the remaining revenue. Over the past five years, the commercial 

OEM market has been strong. The high percentage of new aircraft
has likely had some tempering impact on aftermarket growth. Planes
that are less than five years old are often covered by warranty and
even if not, there is typically a lower level of aftermarket demand
during the initial years.
We have an attractive mix of products and platform positions that
bode well for the future. We have invested significantly in new platform
positions over the past 10 years and substantially expanded our
content on most major new aircraft designs.
Due to our investment in new platform positions, we are also well positioned
on stable and growing platforms on the defense side of our business.
The combination of dynamic new product activity, strong platform
positions, potentially solid growth in our markets, and our well proven
ability to find and successfully integrate acquisitions positions us
well for the future.
BOARD CHANGES
In October 2017, TransDigm�s Board of Directors appointed Gary E.
McCullough and David A. Barr to the board. Both bring significant
public company experience and also the background to support the
company�s private equity approach. We are fortunate to attract
executives of this caliber to our board and look forward to their insight.
FORECAST
Source: June 2017 Airline Monitor
WORLDWIDE REVENUE
PASSENGER MILES
(in millions)
5.9%
CAGR
VALUE CREATION THROUGH ALL CYCLES
TransDigm�s unique business model and consistent strategy have
steadily created shareholder value through up and down aerospace
industry market cycles. Our methodology has worked well over many
years and our entire company is focused on the elements of execution.
In summary, 2017 was a good and busy year. I�m confident that with
our consistent value-focused strategy and strong mix of businesses
we can continue to create long-term intrinsic value for our investors.
We thank our shareholders for their investment, our customers
for choosing TransDigm, and our managers and employees for their
hard work and dedication. We look forward to reporting to you on
our progress during 2018.
Sincerely,
W. Nicholas Howley
Chief Executive Officer and
Chairman of the Board of Directors
January 22, 2018 