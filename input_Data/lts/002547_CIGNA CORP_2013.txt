A message from David Cordani
Cigna once again delivered strong revenue and earnings*
growth in 2013, marking our fourth consecutive year of
competitively attractive financial results.
We achieved these results in the face of a disruptive and
dynamic global environment, with contributions from
multiple geographies and each of Cignas three primary
business segments: Global Health Care, Global
Supplemental Benefits and Group Disability and Life.
In the four years since implementing our strategy of
Go Deep, Go Global, Go Individual, Cigna has achieved
compound annual growth of 15% for both revenues and
adjusted income from operations per share.*
I am proud of Cignas performance, which has outpaced
that of our industry for more than four years, providing
healthy returns for our investors. Cigna has proven its
ability to anticipate and adapt to significant changes
and difficult market conditions. We remain well-prepared
to successfully confront disruptive market forces such as
ongoing market pressures on Medicare Advantage, and
changing client and customer needs our industry faces
in 2014.

In the following conversation, David Cordani discusses Cignas 2013 performance; the trends, opportunities
and challenges shaping Cignas go-forward strategies for 2014 and beyond; and the companys approach
to bringing more personalized products and services to its customers and clients.
We also had many important strategic achievements
that made a tangible, positive impact on our performance.
To name just a few:
We entered a transaction with Berkshire Hathaway, Inc.
to effectively exit our run-off reinsurance business. This
transaction, completed during the first quarter, significantly
improved our financial flexibility and strengthened our
balance sheet.
We made further improvement to our Pharmacy Benefits
Management capabilities through a new strategic
arrangement. This further enhanced our pharmacy
offerings for our customers and clients, and it allows
us to leverage an enhanced technology platform and
streamlined operating capabilities  to drive greater
flexibility and affordability for our customers and clients.
And we continued to deepen our global footprint, with
meaningful progress in important growth markets such
as Turkey, India and China. Our capabilities outside of
the United States clearly give us a more diverse platform
for growth.
All of these results and actions helped us to deliver more
personalized products and services, which we view as
essential to excelling in a highly competitive marketplace,
and to meeting the individual needs of our customers.

2013 Performance
How do you characterize Cignas 2013
performance from an investor perspective?
Cigna delivered strong, competitively differentiated
results for our shareholders in 2013  extending an
outstanding track record of financial performance over
the past four years. We generated a total shareholder
return of approximately 64% in 2013, which ranked as
the highest among our core managed care competitors.
We adapted to an environment of considerable market
disruption, most notably from Health Care Reform and
changes to Medicare Advantage in the United States.
In addition to solid performance from each of our operating
business segments, we strengthened our balance sheet
and generated significant free cash flow from our businesses,
enabling us to return approximately $1 billion in value to
shareholders in 2013 through share repurchases.
We delivered a full-year medical cost trend of below
5% for our U.S. Commercial business for 2013, which
was among the best in our industry and which directly
benefited the approximately 85% of Cignas U.S.
customers who are in self-funding arrangements.

This marks four consecutive years of overall
growth for Cigna. What is driving this?
I attribute our growth to our emphasis on targeted
customer and client segments, and focus on our
targeted markets.
Four years ago, we made the choice to focus our
organization on the strategy of Go Deep, Go Global,
Go Individual. Our Go strategy has served as a
foundation for pursuing three goals that have clearly
contributed to our long-term performance: repositioning
our portfolio for growth in target markets; improving
our strategic and financial flexibility; and pursuing new
opportunities in high-growth markets, with a special
emphasis on individuals.
Our strategy puts our customers at the center of all
we do, and guides our organization of approximately
35,000 employees in more than 30 countries.
Can you discuss the Go Global part of your
strategy? What sorts of contributions did
you see from your businesses outside of the
United States?
For Cigna, Go Global is about creating borderless
environments to leverage our capabilities in new
geographies. Its about supporting an expanding, globally
mobile population through our proprietary expatriate
network, which remains the broadest and best-established
in the world.
To mention just a few examples from 2013: Korea continued
its long, sustained track record of growth; we deepened our
partnership in Thailand with Tesco, to offer personal health
and accident insurance; and we got off to a good start in
Turkey with our Cigna Finans joint venture, where we
market life and pension products in one of the fastestgrowing,
least-penetrated global insurance markets. We
also celebrated the tenth anniversary of our very successful
venture in China with China Merchants Bank, meeting the
health, life and accident insurance needs of that countrys
growing middle class.
Finally, we launched our venture with TTK in India, where
were the first U.S. insurer to be part of a stand-alone joint
venture health insurance company.
Our Global Supplemental business brings valuable health,
life and other solutions to serve the rapidly growing
middle classes in these countries  also consistent with
our Go strategy. In 2013, we continued to see examples
of how Cignas global presence allowed us to share best
practices among geographies. For instance, we are
leveraging our market-leading, direct-to-consumer sales
capabilities from Asia to the U.S., to be deployed in public
and private exchanges.
Building on that, is Cigna exploring new
geographies or regions?
We will continue our global growth strategy 
particularly our geographic expansion. In the United
States, this means new cities for Medicare and individual
solutions  and, outside of the United States, it means
evaluating opportunities in new countries.
What didnt go as well as you anticipated in 2013?
The industry experienced uncertainty surrounding
Medicare Advantage reimbursement levels. In the
short term, we expect these pressures will continue even
as we focus on opportunities to improve our medical
costs for our Seniors business.
Whats especially gratifying, even given these pressures
and disruptions, is that we were the only publicly traded
company to achieve a five-star CMS rating  for our
Medicare Advantage business in Florida. We remain
confident that initiatives such as our 2013 acquisition of
Alegis Care, a company focused on physician-based home
health care, further strengthen our Cigna-HealthSpring
Seniors business and physician engagement capabilities 
and that we will continue to create even greater value for
our customers.

The disruptive global environment
Lets talk about the disruptive environment:
How do you describe the global competitive
environment? Whats changing? What makes
it so challenging?
Were seeing a fundamental reshaping of virtually
every aspect of how we do business, and in the
demographics we serve. Populations are aging. Middle
classes are expanding. Chronic disease levels are rising,
and affordability pressures for health care are increasing
for all.
At the same time, a digital world means that everything
today is faster and more personalized. Information flows
faster; decisions get made faster; expectations change
faster, and customers expect higher levels of personalization.
Evolution in technology and health care systems across
the globe is transforming how people interact with
their health care partners, and the levels of transparency
they expect.
In the U.S., consumers have more health care choices 
in part driven by the public and private exchanges, as
well as more convenient access to health care through
employer clinics and telemedicine, to give just a couple
of examples. Outside of the U.S., consumers with growing
incomes are looking for trusted, easy-to-use, personal and
reliable plans to protect their families.
In short: distribution channels, supply chains, primary
purchasers, political and health care systems are all in
flux. Within this environment of change, at Cigna we
continue to drive innovation with our physician partners
for the benefit of our customers and clients. And we
continue to view information and supporting technology
as critical resources to support our partners, customers
and clients.

Why do demographic changes matter so much?
The ability to respond to these changes will determine
who succeeds and who does not. Anticipating and
proactively responding to these challenges positions
Cigna to continue to meet the needs of our customers
and clients, to deliver differentiated value  and, as a
result, to achieve our performance goals.
Evolving demographics result in a different balance of
needs for our customers. The needs of a 65-year-old differ
in many ways from those of a 30-year-old  an older
population is more prone to chronic disease. Looking
beyond age, the expectations of a larger, global middle
class differ from those of an historically less affluent
population. Quality of life, health protection and asset
protection become major factors.
At Cigna, we have strategically positioned our company
to anticipate and adapt to these types of shifts. Weve
broken out of the traditional mode of being a health
insurer focused on financing sick care, and transformed
ourselves into a global health service company that
focuses on health, wellness and preventive care, and
sense of security solutions.
Health Care Reform
Shifting gears, what is Cignas perspective on
Health Care Reform thus far?
While the question is U.S.-biased, in reality, were
seeing transforming health care systems throughout
the world  not only in the U.S.  in response to changing
customer needs. Theres tremendous pressure on these
systems to improve affordability, and in many cases to
create more consumer choice, such as products that
supplement the governments traditional, base-level
offerings.
In the U.S., were just beginning to see the initial impact
of the Affordable Care Act. The law generally affects
Cigna less than many of our competitors, because about
85% of our Commercial business relationships are
self-funded employer relationships, rather than fully
insured. At the same time, its a significant change: for
our customers, our clients and even our own employees.
When many people talk about Health Care
Reform in the U.S., they think first in terms
of the public exchanges. What is Cignas
perspective on the exchanges?
In the near term, the public exchanges are not
a significant business driver for us  weve taken
a focused approach, participating in only five states
that most closely align with our Go Deep strategy.
In these states, we are focused on select metropolitan
areas where we have our strongest physician networks
and collaborative accountable care relationships.
How about the private exchanges?
We view private exchanges as a potential longterm
growth opportunity. Well continue to monitor
evolving programs, and were poised to go deeper into
private exchanges should they prove to deliver differentiated
value to consumers.
While we are positioned in the majority of third-party
private exchanges, we recently introduced a new, proprietary
private exchange. Here, we tailor benefit programs for the
needs of employer clients and brokers, providing greater
choice and personalization for our customers.

Where does Cigna believe Health Care Reform
should go from here?
From the beginning of the health care debate,
Cigna believed that the expansion of affordable
solutions, improvement of clinical and health quality,
and improvements in affordability are all critical to
create sustainable change.
More specifically, a sustainable health care system starts
with individuals making healthy lifestyle and behavior
decisions, to maximize their personal health, quality of
life and productivity. Consumers must pursue highvalue
health care alternatives. And, when health care is
delivered, health care providers should be compensated
based on the value they bring, not on the volume of
services they provide.
It will be important for aspects of the law to evolve, which
is only achievable through true collaboration involving
policymakers, health care professionals and business
leaders. Cigna actively plays a convening role in encouraging
this direction in Washington, and in our industry.
We recognize this is harder than ever in a polarized
environment. However, in recognition of the long-term
societal impact, we remain steadfast in our active,
principle-based engagement with policy leaders.
What is most distinctive about Cignas point of
view on achieving the goals of reform?
We believe employers play an essential role in
creating access to high-quality health care. They
are uniquely capable of driving increased consumer
health engagement due to the shared culture and
common purpose they create. In short, they represent
distinctive communities.
Employers are also the best situated to drive what we
refer to as positive disruption, through their existing
communications platform, and their ability to incentivize
healthier behaviors; aggressive adoption of preventive
and chronic care programs; and use of the highestperforming,
value-based physician networks.
But, again, its important to remember that no single public
or private entity can take our health care system to where
it needs to go. We need to work together to lead change.
In a larger sense, its all about being attuned to the
changing global environment, and being nimble enough
to adapt to the changing needs of our customers and
clients  which can only happen by listening to them,
understanding what they need, and partnering with the
clinical community for their betterment.
The importance of personalization
You often reference personalization and
personalized products and services.
What do you mean by this?
This gets to the heart of our Go Individual strategy.
Personalization is what customers demand today.
They expect our products and services to be personally
relevant to them, and adaptable to their needs; its part of
an increased retail orientation.
Cigna is committed to personalizing our products and
services across customer segments and around the world
 whether its for a Government or Voluntary products
customer, a Supplemental Benefits customer, or a customer
from one of our Regional or Select employer-group clients.
We do this in collaboration with the physicians we engage;
to drive quality health outcomes and affordability, and
with more than 150 affinity partners who help make our
products and services locally relevant to our global
clients around the world.

The advantages for our customers are better health outcomes
and a greater sense of security. The advantages for our
employer clients are lower costs and higher productivity.
That provides a logical segue to how Cigna is
working with its customers and clients on a daily
basis. The world is changing: How are your
customer and client relationships changing along
with it?
First, let me offer some background, as context.
Traditional, reactive models of treating illness,
disease and disability are collapsing under the weight of
economic and demographic disruptions. To effectively
respond, health care systems have to evolve and reorient
rapidly to focus on the achievement and preservation of
health and productivity  not only on the provision of
treatment to those who are sick.
To date, the modern health care system has undervalued
the engagement of individuals in their own health, wellbeing
and health care purchasing decisions. We believe
when individuals are incentivized to engage in valuebased
health care; to make healthier lifestyle and health
care access choices; and are empowered by effective
support programs, information and communication 
they can and will make better decisions. This, in turn,
will have a dramatic impact on overall quality of life and
the health care cost curve.
However, particularly in the United States today, individuals
are highly disconnected from, and unaware of, the underlying
cost of health care and the role that individual choice
plays as a key driver of those costs. Although lifestyle and
behavioral choices are major drivers of individual health
care costs, Americans are less healthy than ever, and
medical costs continue to rise. In no other consumptionbased
market or industry is there such a disconnect
between individual choice and financial obligation.
The only way to help customers navigate through these
changes is to be much more personally relevant to them,
and more personally engaged with them.
In health care, it also means being personally engaged in
partnerships with the physicians who treat our customers 
and with the employers who hire them and rely on their
productivity. At Cigna, we seek to support and connect
employees, clients and physicians.
What is an example of this?
Our care delivery and physician partnerships
provide a great example of how Cigna is delivering
superior health and productivity outcomes in a local and
personalized fashion.
In 2013, we increased the number of customers benefiting
from our collaborative relationships with physicians by
50%. By the end of the year, we had 86 collaborative
accountable care initiatives, up from 52 at the end of 2012.
Were seeing positive cost and quality outcomes, and health
care professionals give us frequent feedback that Cignas
approach to these initiatives is differentiated in the market.
What makes our collaborative approach unique, and so
personalized, is the degree to which it harnesses aligned
incentives around closing gaps in health care delivery
and desirable health behaviors; specific, actionable
information; and care extenders such as health advocates
and case managers.
In addition, our global network of doctors and hospitals,
coupled with our localized health management and
coordination resources, enable us to offer more expansive
care options for customers in our target markets around
the world.

How does all of this relate to the investments
Cigna makes in its capabilities?
Actually, its a virtuous circle: We invest to further
our ability to personalize our services and, as a result,
further our performance. Then, based on what we achieve
and what we learn, we revisit our investments.
We invest in several areas, some of which include
collaborative physician relationships; joint ventures;
technologies and tools; and expansion of our service
capabilities.
Ill reference three key areas in which Cigna continues to
invest, to ensure we are positioned for ongoing success:
customer insights and engagement, consultative
distribution and innovative physician partnership models.
Insights give us deep knowledge that allows us to better
match distribution channels to our customers needs.
When combined with our consultative distribution
capabilities  which identify the best solutions for the
organizations we work with  our insights give Cigna
a unique ability to bring our customers personalized
solutions as their life and health stages change.
Our third key area  our care delivery and physician
partnership capabilities  also drives our successful
collaborative accountable care and facilitates the delivery
of quality, affordable care.

Many of your investments and innovations are
technology driven. Discuss the role technology
plays at Cigna.
Technology enables our team to better communicate
with each other, and to in turn better communicate
with all of our stakeholders. Weve evolved to the point
where our technology people think first in terms of our
customers needs, and then identify the best technologies
to deliver differentiated value.
Were sharply focused on making ongoing, targeted
investments to help position us for sustained growth.
These investments are enabling us to create deeper,
more personal relationships with our customers  to
better understand their needs, and to align, integrate
and develop our products and services with those
needs. Technology is fueling our ability to create a new,
personalized health care experience for our customers,
where mobile, social, gamification and web-based
incentives intersect to drive improved health behaviors,
health outcomes and affordability of health care services.
One example from 2013 was our new initiative with
the digital health and social engagement company,
SocialWellth, to help individuals choose which health
and fitness apps are most appropriate to help them meet
their health and wellness goals.

In the community
Discuss Cignas community involvement in 2013.
Cignas community involvement is one of the things
Im most proud of as our president and CEO.
Our global employees step up, time and again, to serve
people in the communities where we live and work  from
assisting people impacted by environmental and other
tragedies, to our ongoing participation with organizations
such as Habitat for Humanity, ChildObesity180, the
March of Dimes, Achilles International and Blessings
in a Backpack, a U.S. charity that provides weekend
nourishment to school children.
Our philanthropic arm, the Cigna Foundation, further
expanded its global focus last year. In addition to numerous
charitable activities and support, we sponsored the first
Global Healthy Workplace Summit in London, where the
global business community shared innovative health
practices to extend and enhance better workforce health
worldwide. The Cigna Foundation also led charitable
efforts from China to Korea, and from the U.S. to Europe.

2014 and beyond
Lets conclude with a discussion about what investors should expect
from Cigna in the coming years.
I remain confident that Cigna will continue to extend our track record for outstanding
performance, into a fifth consecutive year.
We have multiple sources for growth, including the addition of new customers, and
expansion of existing relationships through increased market share and increased
contributions from our ancillary product offerings.
We expect to drive continued growth in our Commercial Health Care business around
the world, as well as continued growth in our Global Supplemental Benefits business. We
also see long-term opportunity for Medicare Advantage growth based on our positioning
relative to demographics and market expansion, and we expect an improving economic
environment for our Disability and Life segment.
In addition, we anticipate that benefits from our Pharmacy Benefits Management
arrangement and operating expense efficiencies will contribute to our long-term growth.
We will also continue the effective deployment of our capital, and will have good capital
flexibility driven by the strong free cash flows generated from our businesses.
Based on all of these factors  along with the continued execution of our Go Deep, Go
Global, Go Individual strategy in our target markets, and in new ones where we see
opportunities  Cigna expects to further improve individual health and productivity, and
deliver attractive and sustainable long-term growth, all while continuing to invest back
in our company. Our clear direction, strong product portfolio, financial flexibility and
outstanding global team give us a solid foundation for continued success.
On behalf of my approximately 35,000 colleagues around the world, we appreciate the
confidence that our shareholders have placed in our company. We look forward to growing
Cigna together over the years ahead.
David M. Cordani
President and Chief
Executive Officer
Cigna Corporation