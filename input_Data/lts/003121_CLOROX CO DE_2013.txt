Clorox Stakeholders:
In fiscal year 2013, The Clorox Company reached an
important milestone. On May 3, we celebrated our
100th anniversary, marking a century of providing
products that consumers value. Since our founding,
weve grown into a $5.6 billion multinational manufacturer
and marketer of products used every day
in millions of homes, healthcare settings and other
businesses around the world. As we honor our past,
we also recognize our recent accomplishments and
look forward, setting our sights on the future.

Our Centennial Strategy was successful in a
challenging environment.
Almost six years ago, Clorox embarked on our Centennial
Strategy, which was anchored in our mission of We make
everyday life better, every day. Our mission speaks to the
fact that all of our products have a meaningful impact on
consumers everyday lives. And it speaks to the companys
legacy of making a positive difference in our communities.
While our mission is simple, its a powerful source of inspiration
for our employees who have focused relentlessly on delivering
results against our Centennial Strategy objectives.
We delivered strong total stockholder returns.
Despite a challenging external environment, including the
worst U.S. recession since World War II, we delivered total
stockholder returns in the top third of our peer group. For the
past five-year period beginning in fiscal year 2008, Clorox total
stockholder return was 88 percent, compared to an average of
82 percent for our peer group and an average of 40 percent
for the S&P 500. And Cloroxs track record of generating strong
cash flow allowed us to significantly increase our dividend
from $1.60 to $2.56. Between share repurchases and total
annual dividends paid to stockholders, we have returned
more than $2 billion in cash to our stockholders over the
last five years.
We delivered solid sales growth.
Over a five-year period, weve grown sales at a compounded
annual growth rate of about 3 percent, due, in part, to strategic
and bolt-on acquisitions as well as price increases implemented
across our brands to address higher commodity costs and
inflation. Weve also delivered record levels of product
innovation, which contributed more than 3 percentage points
of incremental sales growth in the last two fiscal years. Were
also constantly striving to make products that consumers want
over competitors products, and today, more than 50 percent
of our U.S. retail portfolio is made up of consumer-preferred*
products. And, weve made sustainability improvements to
35 percent of our product portfolio by enhancing our product
formulas or reducing packaging materials.
In addition, were proud of our teams excellent execution of
our 3D demand-creation model of desire, decide and delight,
the cornerstone of our Centennial Strategy. Weve created
high-impact marketing communications to drive consumer
desire, best-in-class, in-store promotions to influence purchase
decisions at the point of decide and delivered superior-quality
products to delight consumers.
We strengthened our portfolio of leading brands.
Today, nearly 90 percent of our brands are the No.1 or No. 2
market share leaders in their categories. Weve made progress
reshaping our portfolio toward faster-growing, profitable
businesses by focusing on global consumer megatrends
of health and wellness, sustainability, multiculturalism and
affordability/value:
 Addressing the emerging consumer demand for natural
products, we acquired Burts Bees, the leader in natural
personal care, and launched Green Works naturally
derived cleaners and gd natural personal care products
for millennial consumers.

 Building on the companys strength in disinfecting products
to help reduce healthcare-acquired infections, we
expanded our Professional Products business into
healthcare channels. In fiscal year 2010, we acquired
Caltech Industries, a U.S. leader in healthcare disinfecting
products. In fiscal year 2012, we purchased Aplicare,
Inc., a leader in infection prevention products for the
skin, and HealthLink, which bundles a range of products
for individual physicians, doctors offices, outpatient care
centers and other small healthcare facilities.
 Responding to the changing demographics in the U.S.,
weve developed and marketed products for Hispanic
consumers, including the Clorox Fraganzia brand of
cleaners, air fresheners and spray disinfectants; Clorox
CloroGel, a multipurpose, bleach-based cleaner with a
thicker formula; and new scents for Pine-Sol cleaners that
address Hispanic consumers appreciation for fragrance.
 In our International business, we refocused our efforts
on strategic geographies and categories where we have
scale and competitive advantage. We grew our Home
Care business in Latin America and significantly grew
our international Burts Bees business.
We drove high levels of employee engagement and
elevated a new generation of senior leaders.
Cloroxs business strategy begins with our people. Our strong
focus on the engagement, development and well-being of
our 8,400 employees around the world helps ensure we have
motivated people who give their best every day. We regularly
survey employees to determine their level of engagement by
covering such topics as their pride in the company and in
their work, their satisfaction in professional development, as
well as the level of discretionary effort in their day-to-day jobs.
In the last few years, our employee engagement scores have
significantly surpassed global benchmarks of companies. In
fiscal 2013, the percentage of engaged and highly engaged
employees reached 87 percent.
We also surveyed employees around the world to capture
the core attributes they value about working at Clorox. These
attributes include living our values, pride in our trusted brands
and products, and having opportunities to make a real impact
on our business. We distilled these insights into a core employer
brand  Our Clorox: Powered by everyones leadership, every
day  that will help us preserve and nurture the qualities that
make Clorox unique.
Our commitment to fostering a culture of diversity and inclusion
is a priority that sets Clorox apart. Focused recruiting, comprehensive
training and support for our employee resource groups
all contribute to our ongoing efforts to ensure diversity and
inclusion is a business imperative. Since 2006, Clorox has
received a perfect score in the Human Rights Campaigns
Corporate Equality Index for our benefits and programs for
lesbian, gay, bisexual and transgender employees. And,
this year, the company was named one of the 2013 Best
of the Best for top diversity employers by Hispanic
Network Magazine.
Another important part of our people strategy is robust
succession planning that creates opportunities for new leaders
and ensures the continuation of strong governance. In fiscal
2013, Larry Peiros, a longtime Clorox veteran who made
extensive contributions to the business, retired from his post as
chief operating officer (COO). We then moved to a structure
of two COOs, with the appointments of Benno Dor er to
executive vice president and chief operating officer  Cleaning,
International and Corporate Strategy, and George Roeth
to executive vice president and chief operating officer 
Household and Lifestyle. Im pleased about the current structure
of our executive team, with leaders whose extensive industry
experience and strategic capabilities will continue our focus
on delivering stockholder value.
We integrated corporate responsibility into our business,
because its good business.
More than ever, consumers and investors are voting with their
wallets when it comes to companies that do the right thing.
While our values have guided our success for more than
100 years, in fiscal 2010, the company established a formal
corporate responsibility strategy that is tightly integrated
with the business

From 2007 to 2012, we made a lot of progress in reducing the
environmental impact of our operations. We reduced greenhouse
gas emissions by 26 percent, energy consumption by 15 percent,
water consumption by 22 percent and solid waste to landfill by
27 percent (per case of product sold). Earlier this year, Clorox
was ranked No. 41 on the 2013 Global 100 Most Sustainable
Corporations in the World List by Corporate Knights, a Torontobased
media and investment research company. The same listing
placed us second among household and personal products
companies. The company also received the Most Innovative
Corporate Social Responsibility Disclosure Policy Award by
Corporate Secretary magazine. The award recognized our 2011
annual report Think Outside the Bottle, which was Cloroxs first
time to combine its financial, environmental, social and governance
performance in an integrated report. And finally, for the third
consecutive year, Clorox made it to Corporate Responsibility
magazines Top 100 list of most r esponsible companies.
We continue to also believe that vibrant, healthy communities play
a large role in the longevity of our business. Thats why The Clorox
Company Foundation has awarded cash grants totaling more than
$90 million to nonprofit organizations, schools and colleges since its
founding in 1980. In fiscal 2013 alone, the foundation awarded
more than $4 million in cash grants, while the company made
product donations valued at about $15 million1, and our brands
cause marketing programs contributed another $1.3 million to
deserving nonprofits.
We delivered solid results in fiscal year 2013.
Turning to our fiscal 2013 results2, we delivered 3 percent sales
growth, driven in part by record levels of product innovation,
while continuing to face a bumpy economic recovery and a
fragile consumer. Overall, we are pleased with our results,
which also include:
 Earnings from continuing operations that increased to
$574 million, compared to $543 million in fiscal 2012
 Diluted net earnings per share from continuing operations of
$4.31, an increase of 5 percent, versus fiscal 2012 of $4.10
 Gross margin expansion of 80 basis points, up to
42.9 percent versus 42.1 per cent in fiscal 2012
 Economic profit of $426 million, an increase of 6 percent,
compared to $402 million in fiscal 2012*
 Free cash flow of $583 million, more than 10 percent of net
sales, versus $428 million, or about 8 percent of net sales, in
fiscal 2012**
Despite this solid performance, our top-line results came in slightly
lower than anticipated, primarily due to challenges we faced in the
second half of the fiscal year, including unusually cold weather,
which significantly impacted our Charcoal business and the overall
category; declining foreign currencies in multiple international
markets; and increased competitive activity in disinfecting wipes
and laundry. We have plans in place to help address these challenges,
including increased merchandising activity and product
innovation scheduled for fiscal 2014.
While one of our biggest challenges recently has been
continued pressure on our margins  particularly from high
inflation, price controls and foreign currency declines in
Venezuela and Argentina  we made strong progress in
rebuilding our margins in fiscal 2013, with gr oss margin up
almost 1 percentage point to 42.9 percent. Once again, our track
record of delivering strong cost savings made a difference in margin
improvement, with a savings of more than $100 million this year. In

fact, fiscal 2013 marks more than 10 consecutive years weve
delivered at least 1.5 percentage points of margin improvement
from cost savings.
Our 2013 year total stockholder return was a robust 19 percent,
which was slightly below the averages of our peer group and
S&P 500 as the broader stock market experienced a strong rebound
in early 2013. Delivering superior total stockholder returns continues
to be a priority in our long-term plans, as evidenced by our
superior five-year returns during our Centennial Strategy period.
To this end, in May we increased our quarterly cash dividend by
nearly 11 percent, making fiscal 2013 the 36th consecutive year
weve increased total annual dividends paid to our stockholders.
Our 2020 Strategy continues the companys
focus on delivering stockholder value.
Our sights are now set on 2020, with a strategy that builds on the
success of the past six years and directs the company to the
highest-value opportunities for long-term profitable growth and
strong total stockholder returns. Specifically, our long-term financial
targets include sales growth of 3 to 5 percent and EBIT1
margin growth of 25 to 50 basis points. With a commitment to
maintaining a healthy dividend, our goal is to continue delivering
total stockholder returns in the top third of our peer group.
To accomplish our 2020 goals, weve chosen to focus on the
following strategies:
Engage our people as business owners.
We want to enable our team across the entire company to
become even more effective by empowering them to make
decisions faster and supporting them to work more efficiently
by simplifying operational processes. At the core, its about
moving toward a more agile enterprise  harnessing everyones
leadership and doing everything we can to enable Clorox
people to make an impact each and every day.
Support the long-term health of our brands through
more targeted 3D plans and product innovation.
We plan to evolve our 3D demand-creation model of desire,
decide and delight to engage with consumers in a more meaningful
way and ensure were addressing their product needs.
This evolution will include seeking more granular insights about
when and where consumers are open to communications,
and expanding our digital capabilities to better target and
personalize consumer messages and address the growing
role of e-commerce. Well look to enhance our supply chain to
be more flexible and better anticipate the needs of consumers
and customers. Well also continue to focus on the value
proposition of our brands, emphasizing product superiority
and meaningful innovation.
Grow in profitable new categories, channels and countries.
Our strategy also includes pursuing businesses in expanding
markets with strong profit potential. These opportunities 
whether theyre adjacent categories in which our brands can
be successful, new sales channels such as healthcare facilities,
or countries where we want to expand our product offerings 
should be large enough to be meaningful to Clorox and have
competitive dynamics that leverage our brand strengths and
our superior demand-creation capabilities.
Fund growth by reducing waste in our work,
products and supply chain.
As we continue our transformation into an even more agile
enterprise, were placing the consumer at the center of everything
we do. When it comes to our day-to-day tasks and
operational processes, well focus on work that drives the
highest value to our consumers  and ultimately leads to
profitable growth. This means eliminating waste, or ineffective
work, across the enterprise. Delivering strong cost savings,
reducing selling and administrative expenses to 14 percent or
less of sales over time and driving our sustainability initiatives
are priorities that will help reduce waste and fund growth.
When I think about our Centennial Strategy performance and
our vision for 2020, the term constancy of purpose comes to
mind. Weve set clear goals and aspirations for our next set of
milestones. Were leveraging the power of our brands, building
on proven results and focused on a purposeful strategy, while
remaining grounded in strong principles of corporate responsibility
and always remembering our greatest competitive
advantage is our people.
Thank you for your continued support.
Sincerely,
Donald R. Knauss
Chairman and Chief Executive Officer
August 23, 2013