In my letter to you two years ago, I introduced our new visiona clean,
energy-efficient worldand predicted it would re-energize our company.
Today, our broader perspective has resulted in a number of positive changes
in our company. From our ever-evolving product portfolio to our renewed
sustainability efforts, our vision is guiding us toward a brighter future. 

Building for the Future
2015 was a building year for BorgWarner.
We relocated two Western European facilities in our Drivetrain Group to Poland
to enhance our competitive position in
Europe. We opened two new plants at
our campus in Hungary to manufacture
all-wheel drive solutions and exhaust gas
recirculation (EGR) technologies. And,
we expanded our emissions business in
Mexico and South Korea to meet growing
global demand for our advanced ignition
and EGR systems.
Remy Acquisition 
Our Largest Yet
In the fourth quarter of 2015, we
completed the acquisition of Remy
International, Inc., a leading manufacturer of rotating electrical components
such as alternators, starter motors
and electric traction motors for the
automotive and commercial vehicle industry. Around the world, our
customers are developing plans for powertrain electrificationincluding
stop/start, 48-volt battery systems,
hybrids and plug-in electric vehicles.
This acquisition is critically important
to maintaining our strong leadership
position in providing high-value powertrain technology to the market.
The Remy business is combining with
BorgWarner TorqTransfer Systems
to form BorgWarner PowerDrive
Systems. The complementary combination of advanced powertrain and
rotating electrical device technology
provides a unique market approach
and competitive advantage that will
drive growth for many years to come.
Not only was the Remy acquisition
the largest in our history, but it may
prove to be the most transformative.
Earning Accolades for
Quality and Innovation
BorgWarners innovations and highquality products were recognized by
industry experts and customers alike.
We received a 2015 Automotive News
PACE Award for our first-to-market frontwheel drive electronic limited slip differential (FXD) technology. The innovation
greatly enhances the traction, handling
and stability of front-wheel drive vehicles,
and is a cost-effective and fuel-efficient
alternative to all-wheel drive systems.
BorgWarners FXD technology debuted
on the Volkswagen Golf GTI with Performance Pack and SEAT Leon CUPRA.
Programs are also under development
with several other global automakers.
BorgWarner received quality and innovation awards from Daimler Trucks North
America, Ford, Fiat Chrysler, GM, Honda,
Maruti Suzuki, Mercedes-Benz, Toyota
and Volvo. For example, Volvo Cars honored BorgWarner with an Award of Excellence for our cost-effective advanced
turbocharging technologies. Our plant in
Seneca, South Carolina, was presented with a Toyota Regional Contribution
Award, Honda North America Excellence
in Quality Award, and South Carolina
Silver Crescent Award for Manufacturing
Excellence. BorgWarner also received
a 2014 World Excellence Award in the
Aligned Business Framework category for
exemplifying the Ford Motor Companys
principles of quality, value and innovation.
Performing for Stockholders
Our portfolio of powertrain technologies
continues to drive global growth for the
company. The company grew over 4%
in 2015 (excluding the impact of foreign
currencies and acquisitions), with that
growth coming in China, Europe, Korea
and North America. As we look ahead, we
see sustained growth as our customers
partner with us to meet their fuel economy,
emissions and performance targets. We
project a 4% to 6% growth rate for the
company over the next three years
(excluding the impact of foreign currencies
and acquisitions).
Our operating performance continued to be
a bright spot for the company. Operating
income as a percentage of net sales was
13.1% in 2015 (excluding the impact of
non-comparable items, foreign currencies
and acquisitions), an improvement over
2014 and higher for the sixth consecutive
year. Most notable is the improvement
in the Drivetrain segment. In 2012, the
Drivetrain segment adjusted EBIT margin
was 9.1% and, at that time, we committed
to driving that margin higher. In 2015, the
Drivetrain segment adjusted EBIT margin
was 11.5%, and we believe there is room
for further improvement. Our commitment to innovation and product
development continued in 2015 with R&D
investment of nearly 4% of net sales, in
line with our target.
Our strong track record of returning cash
to stockholders continued in 2015. We
maintained our quarterly dividend and initiated a $1 billion share repurchase program
over a three-year period beginning in the
first quarter of 2015. In aggregate, we spent
over 150% of our free cash flow on dividend
payments and share repurchases in 2015.
THE AUTOMOTIVE INDUSTRY
IS EVOLVING
BorgWarners success today is driven
by our ability to envision the automotive
industry in the future. How will people get
around? What regulatory requirements
will automakers need to satisfy? How will
shifting demographics and information
systems affect mobility? Of course, the
most important question for BorgWarner is:
Will gasoline engines, clean diesel engines,
hybrids or electric drives power vehicles?
The automotive industry is in the midst
of tremendous evolution. The disruptive
impact of macro trendsthe drive for improved efficiency, scarcity of natural
resources, heightened focus on the
environment, demographic shifts, connectivity, increasing urbanizationcannot be
underestimated.
BorgWarner not only embraces this
evolution but is positioned to lead it.
BorgWarner has been a product leader
for nearly a century, and we plan to continue leading the industry. Our strategy
is to drive every type of powertrain, no
matter what type of fuel or energy powers
it. If its a powertrain, we can make it
more efficient.
Electrification is one of the biggest trends
driving the industrys evolution. Ten years
from now, we estimate about 25% of
vehicles produced will have some form
of hybrid or electric powertrain on board.
We will be focusing on making our current
products more electrically capable in the near future while developing more products
for pure electric vehicles for the long term.
Thats what makes the Remy acquisition
so excitingit brings motor expertise that
will accelerate our development of products
for 48-volt systems, hybrids and increased
electrification.
While world events create an ever changing
business climate, the fundamentals of our
company have not changed. At our core, we
are innovators. Our powertrain technologies, which help our customers improve
fuel economy, emissions and performance,
are adopted the world over in every major
region and by every major automaker. We
also remain disciplined with our costs and
focus on return on invested capital and
creating economic value. These principles
have driven success for many years and
will guide us going forward.
Throughout BorgWarners long history,
we have succeeded by identifying technology waves and developing the must-have
products to make them possible. Our
employees thrive on change. We enjoy
a challenge. We are passionate about
powertrains and relentlessly focused on the future. We are a product leader in
the various powertrains and applications
of today precisely because we had the
foresight to predict industry trends and
customer needs decades ago.
Our world view and our goals are broad and
high. We dont just make auto partsWe
make a cleaner, more energy efficient world.
Sincerely,
James Verrier
President and Chief Executive Officer