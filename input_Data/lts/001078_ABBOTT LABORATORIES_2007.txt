2007 AnnuAl Report 

Dear Fellow Shareholder: 2007 was an outstanding year for Abbott as 
we delivered strong growth across our broad mix of leading health care 
businesses. Together, these businesses provide our company  and 
our investors  with a diverse mix of cash flows and multiple sources 
of profitable growth. Our strength is our balance. With each goal 
we achieve, we build further on this balance, positioning Abbott for 
sustained, double-digit performance in the years ahead. 
 
When we look at the uncertainty of the global economic 
environment, we see challenges for most companies across 
virtually every market sector. Nonetheless, we see Abbott 
becoming stronger and more successful as we continue 
to shape it around three main defining attributes: today, our 
company is balanced, global and strong. 

Balanced 

The diverse mix of our business portfolio is our core strength 
and Abbotts greatest differentiator. Its the foundation of our 
stability, and it provides us the opportunity for sustained growth 
across our four major business groups: pharmaceuticals, 
nutritional products, diagnostics and medical devices. 

Weve chosen to compete in attractive health care businesses, 
where our science can have the greatest impact on patients 
lives and where we have strong commercial positions. Weve 
aligned our commitments with societys most pressing 
health care needs. In doing so, were addressing pervasive, 
high-priority health conditions through multiple technologies 
and approaches. 

For instance, Abbott helps patients and physicians treat heart 
disease through all of its major businesses: we are a leader in 
testing to identify cardiovascular conditions, in pharmaceutical 
treatments to improve patients cardiovascular health, in 
nutritional products that help to improve cardiovascular status 
and in stents to relieve acute vascular conditions by reopening 
blocked arteries. We provide similarly balanced arrays of products 
to address other high-priority public health challenges, 
such as diabetes and HIV. 

This broad base of technological expertise and market 
strength provides Abbott a wealth of opportunity that distributes 
our growth across a number of different sectors  and 
that allows us to sustain the quality of our performance. Longer 
term, our goal remains a balanced mix of sales among our 
broad base of businesses. 

GloBal 

We strive to balance our portfolio among businesses, as well 
as geographically. Abbott has long been an international business; 
today, we are truly a global company. While the United 
States is still by far our largest single national market, it no 
longer accounts for the majority of our total sales, as it had 
since the companys founding. In 2007, for the first time ever, 
our revenues outside the United States slightly exceeded 
those within. 

While we expect the United States to remain an important market 
for many years to come, we also recognize that, with the rise 
of major new market economies, such as China, India, Russia 
and Latin America, the rest of the world will play a much larger 
role than ever before in our future growth. Consequently, we are 
building our business around the world to capture the continued 
emergence of new international markets. 

Separating our international nutritional products and pharmaceuticals 
businesses into distinct, focused organizations has 
paid significant dividends. Both businesses have grown stronger 
apart than they did previously as a single, multiline division. In 
2007, our international pharmaceuticals business delivered sales 
growth of nearly 17 percent, and our international nutritional 
products business grew more than 18 percent. 

To meet future market demand, 
Abbott invested in a state-of-the-art 
biologics facility in Puerto Rico (left) 
and a nutrition manufacturing plant in 
Singapore to support its growing inter-
national nutritional products business. 
Weve seen similar results with medical products, where Abbott 
Diabetes Care achieved revenue growth of 18 percent internationally. 
In addition, most of our diagnostics sales are outside 
the United States and performing well in emerging markets. 
Achieving this kind of international growth is a priority in all our 
major businesses. As rapid population growth in emerging 
economies increases demand for health care products, we see 
significant long-term opportunities around the world. 

StronG 

Of course, our breadth is an advantage only because of the 
strength across our businesses. Abbott is a major player with 
promising opportunities in every market in which we participate. 

Pharmaceuticals 

In 2007, Humira, our treatment for a variety of autoimmune 
diseases, became Abbotts most successful product in our 
history. It was approved for the treatment of Crohns disease 
and quickly captured 30 percent of the U.S. market. Since 
February, weve launched Humira for the treatment of psoriasis 
in both Europe and the United States, as well as for juvenile 
rheumatoid arthritis in the United States. We continue to 
develop Humira for ulcerative colitis. To keep up with Humiras 
growth and expected demand, last year, we completed construction 
of our largest capital investment ever, the state-of-theart 
Abbott biotechnology manufacturing plant in Puerto Rico. 

With our rapid integration of Kos Pharmaceuticals in 2007, 
we established Abbott as a significant player in the lipid management 
market. The addition of the Kos portfolio, particularly 
Niaspan, the leading product for raising HDL, or good cholesterol, 
complements the position wed built with TriCor, our 

treatment for patients with high triglycerides. With our strong 
lipid pipeline, we expect to sustain the growth of this franchise 
in one of the worlds largest health care marketplaces. 

Nutritional Products 

Our U.S. nutritional products business, of course, is a longtime 
leader. Were finding new ways to meet consumer needs 
with a renewed emphasis on product innovation, with products 
such as Similac Sensitive and NutriPals fruit bars. This is in 
addition to our strong international performance, perhaps best 
characterized by our second consecutive year of rapid growth 
in major Asian markets. To meet this surging regional demand, 
we are building a new production facility in Singapore that will 
come online in 2009. 

Medical Products 

In 2007, our medical products businesses grew double digits, 
with significant contributions to sales from our vascular business 
and nearly double-digit growth in our diabetes care business, 
driven largely by the success of our more convenient FreeStyle 
Lite meter, which is steadily gaining new user share. 

After a late 2006 launch of our Xience V drug-eluting stent in 
Europe, we saw excellent acceptance and share gains throughout 
2007. In November, the U.S. Food and Drug Administration 
(FDA) Circulatory System Devices Advisory Panel recommended 
approval in the United States, which we anticipate this year. 

Our diagnostics business introduced new instruments and 
assays, and is growing faster than its market worldwide. Our 
molecular diagnostics unit grew sales significantly faster than its 
market in 2007, thanks to the successful launch of our m2000 
system in Europe and the United States. 

Abbott Science 

The key to our strength in all of these markets is the quality of 
our products and of the scientific advances they provide. We 
carefully shape our considerable investment in research and 
development around what patients and health care professionals 
need. Our scientists and engineers are performing distinguished 
and distinguishing work, providing major advances across the 
spectrum of care. 

Weve made refinements to our pharmaceutical discovery 
process that are enhancing our productivity and delivering 
compounds that have breakthrough potential. Were doing 
highly innovative work in neuroscience, where weve developed 
compounds that target receptors in the brain that 
help regulate pain, mood, memory and other neurological 
functions to address such conditions as attention deficit 
hyperactivity disorder and Alzheimers disease. Were also 
doing leading-edge work in oncology research. The National 
Cancer Institute and the journal Nature have recognized the 
strength of Abbotts science. In 2007, we also entered into an 
agreement with Genentech to codevelop two novel, Abbottdiscovered 
cancer therapeutics. 

With each goal we achieve, 

we build further on this balance, 

positioning Abbott for sustained, 

double-digit performance in the 

years ahead. 

The effectiveness of our science spans our businesses. For 
example, were developing next-generation drug-eluting stents, 
including a new bioabsorbable stent that could represent a significant advance in this area of medicine. 

Our Organization 
In 2007, Rick Gonzalez, my longtime friend and colleague, 
retired as Abbotts chief operating officer after an exemplary 
30-year career. We thank him for his contributions over the years.
With Ricks departure, we realigned our operating structure 
under four executive vice presidents  of pharmaceuticals, 
nutritional products, diagnostics and medical devices  
reporting directly to me. 


excellinG 

In summary, Abbott is well prepared for its future. We understand 
the economic and environmental pressures that face 
companies today. Our confidence in delivering consistent results 
to our shareholders despite these challenges speaks to our 
ability to adapt, execute and optimize the many opportunities 
that our mix of businesses provides. 

We are ambitious, ready 

and determined to continue 

capturing the vast array of 

opportunities before us. 

Business diversity alone is not enough. Its the nature of Abbotts 
business diversity that sets us apart. We have chosen to participate 
in businesses with a high degree of relevance for the future 
of medicine, where technological and scientific superiority can 
advance the state of care and provide competitive advantage. 
As a result, we are ambitious, ready and determined to continue 
capturing the vast array of opportunities before us. 

In 2007, we made Abbott a better and stronger company. 
Our goal is to do the same this year and every year. By doing 
so, well continue to deliver outstanding performance for 
the people who rely upon us, including patients, health care 
professionals and shareholders. 


Miles D. White 
Chairman of the Board and Chief Executive Officer 
March 3, 2008 






