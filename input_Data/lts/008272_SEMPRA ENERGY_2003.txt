The past year marked the fifth anniversary of the merger that created Sempra Energy. Im pleased that, for the fifth
consecutive year, we produced outstanding results. Sempra Energys earnings per share have grown, on average, by
nearly 20 percent annually since 1998.

In 2003, we posted earnings of $649 million, up from $591 million in 2002.
Earnings per diluted share were $3.03, a 6-percent increase over 2002. Sempra
Energys outstanding financial performance derives from our ability to capitalize
on new opportunities, our focused strategy and our consistent execution. I am
particularly proud of Sempra Energys performance compared to many other
energy companies in recent years.
Our growth strategy is focused and methodical. We develop a deep understanding
of markets and customer needs, apply a rigorous risk-management analysis
of potential investments and nurture positive relationships with our key
constituents, including the communities in which we operate.
A clearer path ahead.
In 2003, we saw significant progress toward resolving many of the major
issues facing our company. Sempra Energy Resources long-term energy-supply
contract with the California Department of Water Resources was upheld by
both the Federal Energy Regulatory Commission (FERC) and a Superior Court
of California. While these rulings are being appealed by the state, we expect
the contract to continue to be upheld in all venues.
Sempra Energy Trading also has entered into a settlement agreement related to
the FERCs investigation of marketers activities in Western U.S. energy markets
from 2000 to 2001.

In 2003, we received key permits to proceed with the development of two liquefied
natural gas (LNG) receipt terminalsEnerga Costa Azul in Baja California,
Mexico, and Cameron LNG near Lake Charles, Louisiana. These are scheduled
to be the first new LNG receipt facilities built in North America in the past two
decades. We also entered into a preliminary agreement to bring a daily supply
of 500 million cubic feet of natural gas to Energa Costa Azul from Indonesia.
Additionally, Sempra Energy LNG announced its intention to form a 50/50
joint venture with Shell International Gas Limited to develop the Energa Costa
Azul facility.
In our home state of California, the California Public Utilities Commission
(CPUC) has shown a willingness to put energy policy back on solid footing by
acknowledging a more realistic balance between the interests of consumers
and the financial health of the states utilities.
Our solid foundation: Sempra Energy utilities.
Sempra Energy utilitiesSan Diego Gas & Electric and Southern California Gas
Companyprovide a solid foundation for Sempra Energy through consistently
strong earnings and cash flow. Both utilities again performed well in 2003,
exceeding their authorized return on equity and earning operational incentives.
SoCalGas reached an all-party settlement, and SDG&E, a multiparty settlement, in
their respective Cost-of-Service rate cases pending before the CPUC. Approval of
these settlements by the CPUC will allow the utilities to make the investments
necessary to continue their long tradition of delivering safe and reliable energy,
exceptional customer service and consistently strong earnings. It also will enable
the utilities to expand their rate bases over time and ensure a reasonable return
for our investors.
None of the utilities accomplishments would be possible without the determined
efforts of thousands of dedicated employees. This was most evident during last
falls devastating wildfires in California. I am proud of our employees, many of
whom worked around the clock in difficult conditions to restore gas and electric
service to more than 100,000 customers.
New opportunities for growth.
In 2003, Sempra Energy Global Enterprises continued to grow a well-balanced
and profitable mix of businesses that develop energy infrastructure and offer
risk-management services.
Sempra Energy Resources, our wholesale power-generation unit, enjoyed a
landmark year for both profitability and operational achievement. The company
brought four new power plants online in the Pacific Southwest totaling 2,125
megawatts. The projects, which include Elk Hills, Termoelctrica de Mexicali
and Mesquites two generating units, were completed on time and under
budget, with more than 80 percent of all capacity contracted forward through
the year 2010.
Sempra Energy International operates energy utilities and develops gas pipelines
principally in Latin America. In Mexico, the company obtained 20-year contract
commitments for nearly 100 percent of its Gasoducto Bajanorte natural gas
pipeline capacity from major industrial users and power plants in the northern
border region. Sempra Energy Internationals expert market knowledge and
project management experience in Mexico have helped shape the success of
other Sempra Energy projects in the region, including the Termoelctrica de
Mexicali power plant in Mexicali and the Energa Costa Azul LNG receipt facility
north of Ensenada.
In 2003, we formally established a new business unit, Sempra Energy LNG,
to oversee our LNG operations. Plans for the Energa Costa Azul LNG terminal
and for the Cameron LNG facility in Louisiana result from our recognition three
years ago of the growing imbalance between natural gas supply and demand
in North America. We determined that LNG would be critical in addressing this
imbalance and launched efforts to develop an LNG regasification business. The
company expects to break ground on both LNG receipt terminals in mid-2004
and begin operations in 2007.
Working with our communities.
In our LNG initiativesas in other endeavorswe have worked closely with
local communities and key constituents to identify and resolve potentially
difficult issues. These community efforts are part of every major project we
undertake. They help to build trust and establish long-term relationships.
Working closely with local communities also helps to ensure that we can address
concerns before they become problems. A good example was the decision to
build the Mexicali power plant, Termoelctrica de Mexicali, to meet strict
California environmental standards, even though Mexico did not require us
to do so.
In 2003, Fortune magazine recognized Sempra Energy as one of Americas top
five companies for minoritiesone of only two companies consistently to
achieve this distinction since the rankings were introduced in 1998. Also in 2003,
Sempra Energy and its employees contributed more than $9 million and 40,000
hours of volunteer time to charitable causes. The character of our employees
drives Sempra Energys success and enriches the communities we serve.
Looking forward.
As the economy has rebounded, so too has the demand for energy.
With our utilities expanding customer base, we plan new investments in
electric generation, and gas and electric-transmission and distribution infrastructure.
We expect these new investments to bring solid earnings from
our utilities in the years ahead.
Similarly, with long-term contracts in place, Sempra Energy Resources
has a more predictable and stable business, contributing steady earnings to
Sempra Energy.
Our LNG business will shift into full gear this year, with construction on our
two receipt terminals expected to be under way by mid-2004.
On the following pages is an overview of the strategies were executing to
capture the opportunities ahead of us, including the perspectives of some of
the key members of our leadership team.
On behalf of the nearly 13,000 employees of Sempra Energy, I thank you for
your continued support.

Sincerely,

CHAIRMAN, PRESIDENT AND CHIEF EXECUTIVE OFFICER