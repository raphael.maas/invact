Dear Time Warner Cable Stockholders, Employees and Communities:
Overall, 2008 was a good year for Time Warner Cable,
despite the severe economic slowdown.
While many businesses and industries contracted,
we continued to grow, although at a slower pace than
in 2007. We made progress in enhancing our services
and distinguishing them from competitors off erings.
Further, our sales of video, data and voice services to
businesses grew signifi cantly as we focused more resources
on commercial growth opportunities.
Yet Time Warner Cable was not immune to the
unprecedented economic conditions and to the pressures
of expanding competition. The recession negatively
aff ected our ad sales in 2008, just as it aff ected most
advertising businesses. In addition, we began to see
slower customer growth across our residential services
late in the year.
Still, our subscription relationships with more than
14 million customers provide a buffer against the
economic challenges. Were working to strengthen
those relationships by making our customers lives
simpler and easier through the powerful connections
we provide in their homes and businesses.

2008 Highlights
In 2008, we differentiated Time Warner
Cables services like never before, with
numerous accomplishments across all of
our services, including:
 Rapidly growing our high-defi nition
video lineups. We now offer our
customers an average of 57 HD channels,
with many more available in several
areas (including New York City, where
we deliver over 100 HD channels as
of early 2009).
 Expanding Start Over, our time-shifted
viewing service. Where deployed, this
value-added feature allows our Digital
Cable customers to restart a program
in progress, without advance planning.
Customers have embraced it enthusiastically,
enjoying more than 60 million
Start Over streams in 2008.
 Delivering Caller ID on TV. At the end
of 2008, more than two-thirds of our
Digital Phone subscribers could see
whos calling without leaving the comfort
of the couch. In December alone,
we delivered more than half a billion
Caller ID messages to customers TVs.
 Enhancing the speed and capability
of our high-speed data services.
PowerBoost can deliver burst speeds of
up to 22 mbps. We offer it as a valueadded
feature to our Road Runner
Turbo subscribers and are rolling it out
to Road Runner Standard customers.
Additionally, in 2009, we will further
expand the capability of our high-speed
data network and begin off ering even
faster data services over the DOCSIS 3.0
platform in some service areas.
Delivering powerful services at a great
value is now more critical than ever. Our
customers are increasingly turning to bundling
and our Price Lock Guarantee to help them
manage household budgets more eff ectively.
At the end of 2008, more than 54 percent of
our customers purchased at least two of our
three primary services. More than three million
customers enjoyed the full triple play bundle
and the value it delivers. In fact, nearly three
quarters of a million customers became triple
play customers in 2008, alone.
Inside the company, were streamlining
our operational structures and standardizing
processes to be more effi cient. These steps help
establish consistent companywide network
standards that are crucial for launching new
products and ensuring the highest-quality
customer care. They also help us project a
consistent Time Warner Cable identity to
consumers, regardless of where we operate,
while also retaining the local presence and
community commitment our customers value.
of their phone customers, so we know what
to expect and are well-prepared to compete
against them in the video marketplace. We also
have long faced competition from satellite TV
companies and are pleased with our performance
against them. Our ability to deliver bundles of
video, data and voice services over a single
integrated network is a key differentiator
against satellite providers.
2009 Objectives
In addition to actively diff erentiating our services
from competitors, a key focus in 2009 will be
managing costs and striking the right balance
between subscriber growth and profi tability.
Our costs of doing business are rising. In
particular, ever-increasing programming costs
Intensifi ed Marketing
In 2008, we sharpened our marketing messages
and invested more heavily in advertising our
services. We communicated more aggressively
what sets Time Warner Cable apart from
competitors as the best choice for video, data
and voice services.
Competition has increased dramatically
over the past few years. However, we have
prepared for it and are confident in our
competitive strength. The traditional telephone
companies are expanding their video services
into a greater portion of our footprint. As they
do so, we expect to lose some video customers,
at least in the initial launch phase when these
new providers receive heavy media attention.
However, weve competed against them for
years and have attracted a signifi cant number
make it challenging to continue to deliver
cable TV at a reasonable price. However, we
are committed to delivering great products
and a great value  especially as the economic
downturn continues  so we will continue to
manage our costs carefully. We also will focus
on reducing debt and continuing to generate
signifi cant free cash fl ow.
Enhancing customer service is another
crucial goal. While customers tell us they
want technology and the latest services and
features, they also want them delivered in
ways that are simple and easy. Were focusing
on additional ways for our customers to do
business with us easily and effi ciently. Were
doing that through online tools and new
technologies, revitalized retail sales centers
and retail partnerships. Weve also revamped
our website to create a seamless online
purchase process, provide additional self-care
options and empower customers to solve
problems quickly.
Consumers today expect anytime, anywhere
access to any content, from any device. While
we arent interested in creating another cellular
phone company, we are pursuing wireless
technology to further integrate our services
and deliver on consumers thirst for mobility.
As a fi rst step, we expect to launch a broadband
wireless off ering in one or more cities in 2009
via our investment in Clearwire.
We also are pursuing advanced advertising
opportunities. Americans are watching more TV
than ever. Advertisers want targeted, interactive
and measurable solutions to reach them. Canoe
Ventures, a joint venture of Time Warner Cable
and other major cable companies, is striving
to make TV advertising easier to buy, use and
measure. We think Canoe has the potential to
reenergize TV advertising. We expect signifi cant
steps toward launching advanced advertising
products this year.
The year ahead certainly will present
questions and challenges. As Ive mentioned,
weve experienced a slowdown in growth,
which has required that we make some diffi cult
but necessary changes to ensure our long-term
success. But, while we are not immune to
economic forces, we feel confi dent in our ability
to operate effi ciently and compete successfully.
We believe our subscription relationships put
our company in a much better position than
many others. We will approach opportunities
with our typical mix of innovative thinking
and measured, consistent strategic sensibility.
An Independent, Stand-Alone Company
A few weeks ago, we completed our
separation from Time Warner Inc. With that,
Time Warner Cable is now an independent,
stand-alone public company. We are excited
to off er investors the opportunity to hold stock
in a completely publicly held cable distribution
company. Our spinoff also provides us an
opportunity to more clearly defi ne what
Time Warner Cable stands for and the
value we provide customers, communities
and employees.
Im pleased to introduce you to a handful
of our employees on the pages that follow.
They represent all 46,600 of us. Together,
we are dedicated to providing indispensible
products and services and great customer
care. Together, we are devoted to continuing
to earn our customers trust and loyalty.
Together, we are Time Warner Cable.
Sincerely,
Glenn A. Britt
Chairman, President & Chief Executive Offi cer
April 2009