
                                           To Our Employees and Shareholders


                                                I am pleased with FMC Technologies' performance in 2005, as well as the entire
                                           five years in which we have been a public company. Our accomplishments have deliv-
                                           ered shareholder value and have built a strong foundation for what we believe lies
                                           ahead -- a sustained period of growth, particularly in our energy markets.

                                                2005 was an excellent year of progress for the Company, characterized primarily
                                           by successes, but we did have disappointments. Here are the details:

                                           Our successes
                                              Our subsea production systems revenues reached $1.4 billion, a 39 percent
                                               increase over 2004 and 30 percent compound annual growth since 2001.

                                              Inbound orders for subsea production systems were $1.6 billion in 2005, and our
                                               total Company backlog reached another record high of $1.9 billion.

                                              We realized good value on the sale of two non-core assets: a $15.4 million after-tax
                                               gain from the sale of MODEC, Inc. stock and a $7.4 million after-tax gain on the
                                               sale of our 60 percent interest in GTL Microsystems.

                                              We repatriated $473 million of foreign dividends under the American Jobs
                                               Creation Act, which should help us maintain a lower tax rate in the coming years.

                                              We repurchased 1.75 million shares of our outstanding common stock and plan to
                                               continue to repurchase.

                                              Our Energy Processing group reported improved operating margins in every busi-
                                               ness due to strong market demand and better operational execution.



                                           Our disappointments
                                              Higher costs and delays for construction of offshore oil loading facilities in Algeria
                                               for Sonatrach-TRC, the Algerian Oil and Gas Company, caused $54.9 million in
                                               operating losses for Energy Production Systems in 2005.

                                              Reduced customer expenditures in the tomato processing equipment industry and
                                               the effects of four devastating hurricanes on Florida's citrus business adversely
                                               impacted FoodTech's results.

                                              Continued sales growth in certain product lines taxed our vendor base. We are
                                               continuing our efforts to work with and develop our global vendor resources to
                         ensure that they do not limit our ability to grow sales while keeping costs low.
                          We closely monitor our growth opportunities and our after-tax return on capital.
                                           We are fortunate to have businesses, particularly in energy, that are experiencing rapid
                                           growth and provide opportunities for deploying a portion of our strong cash flow. This
                                           growth is requiring us to make increased capital investments, primarily in the energy
                                           businesses, for additional manufacturing facilities and working capital. We are watch-
                                           ing this closely to ensure that these expenditures are prudent and that our returns on
                                           this expanded capital base remain high.
                                                                                         

Financial Performance
     Over the past five years as an independent public company, our Energy Systems
businesses have been the main driver behind our earnings and revenue growth.
This was once again the case in 2005.

     Oil and gas industry activity increased substantially in 2005 as energy prices
reached all-time highs and concerns about long-term availability of supply
grew. For our Energy Systems businesses, these factors translated to
record revenue of $2.4 billion, up 20 percent from 2004, and a
31 percent increase in operating profit.

      Our success in 2005 was tempered somewhat by the challenge
associated with the delays and cost increases for the Sonatrach-TRC
oil offloading project in Algeria. Over the course of the contract, cost
increases totaled $76.3 million, $54.9 million of which we recorded in 2005.
Our reported earnings in 2004 and 2005 include a $0.19 and $0.47 per
diluted share charge, respectively, for losses on the Sonatrach contract.
Without the Sonatrach losses, 2005 operating profit would have
exceeded 2004 by 42 percent. In late February, we received from
Sonatrach the final acceptance of the entire project. While we still
have a one-year warranty period which began in February of 2006,
we are well on our way to putting this project behind us.

     For the year, we reported a 17 percent growth in total Company revenue, to




                                                                                          &
$3.2 billion. Profit before tax was $162.3 million, and earnings per diluted share were
$1.50. We had some unusual items during the year that are nearly offsetting. These
include a positive $0.32 diluted earnings per share contribution from the sale of
MODEC, Inc. stock and our 60 percent investment in GTL Microsystems, and a $0.36
diluted earnings per share tax charge associated with the repatriation of foreign
earnings under the American Jobs Creation Act. Adjusting for these items, 2005
adjusted income per diluted share (a non-GAAP measure) was $1.54, up 23 percent
from 2005 adjusted income of $1.25 per diluted share (a non-GAAP measure).




achievement
                                                   growth
      In our Energy Production Systems businesses, revenue increased 24 percent over
the previous year, reaching $1.9 billion, and operating
profit was up 41 percent, excluding the Sonatrach
losses. Both our subsea systems and surface
wellhead businesses reported record revenue
and operating profits. Orders were received
totaling $2.1 billion, including important new
contracts in West Africa, the North Sea, offshore
Australia, Brazil and the Gulf of Mexico.

     Energy Processing Systems reported revenue of $522 million and operating profit
of $54.1 million, almost double that of 2004 due to stronger demand for fluid control
products and operating margin improvements across all business lines. Higher sales
volume and operating margins in our WECO/Chiksan products and operational
execution improvements in loading systems were drivers in the year-over-year
improvement.



                                                        FoodTech's revenue of $539 million was up modestly from 2004, primarily due to
                                                   the strength of freezing and cooking systems in North American poultry markets.
                                                   Operating profit of $37.9 million was also up slightly from 2004 despite the impact of a
                                                   series of hurricanes that adversely affected the Florida citrus crop in both 2004 and
                                                   2005.
                               Airport Systems reported higher revenue and operating profit due to stronger vol-

                                                   umes for our ground support and airport services business, despite difficult airline
                                                   market conditions. Revenue increased 17 percent, to $327 million, and operating profit
350
                                                   of $23.8 million was up 49 percent over 2004.
300
250                                                     As we enter 2006, FMC Technologies' balance sheet remains strong. The healthy
200                                                cash flow of our businesses, coupled with our ongoing emphasis on working capital
                                                   management, provides us with the financial strength to pursue new opportunities. At
150
                                                   the end of 2005, debt, less cash, was $103 million. Through mid-2008, we will also con-
100
                                                   tinue to benefit from low-cost debt with a 3.2 percent locked-in interest rate for up to
 50
                                                   $150 million in debt.
  0
                   We made progress in our goal of delivering value to shareholders. Our sharehold-
                                                   ers earned a total return of 33.3 percent on FMC Technologies' Common Stock during
                                                   the year. This improvement compares with the Oil Service Index (OSX), which was up
                                                   47.6 percent, and the Standard and Poor's (S&P) 500 Index, which increased 4.9 per-
                                 cent. Since our IPO in June 2001, we have performed extremely well as the value of our
                                                   common stock has appreciated over 100 percent, compared with a 57.8 percent total
250
                                                   return for the OSX and a 7.5 percent total return for the S&P.

200                                                     As part of our ongoing effort to increase shareholder value, we continued to repur-
                                                   chase stock. During 2005, approximately 1.75 million common shares were purchased
150
                                                   for a total of $63.9 million under a 2.0-million-share buyback authorization.

                                                   Technology and solutions for our customers
100


                                                        Technology has long been an important FMC Technologies strength. Our technical
 50
       
                                                   expertise not only gives us a competitive advantage throughout our worldwide busi-

                                                   ness operations but also is a key reason why we are the acknowledged leader in most
             
                                                   of our markets, especially in subsea. Oil companies, regardless of size, consider us their
                                                   partner of choice in developing solutions for offshore projects.

                                                        A key competitive advantage and one of the most important contributors to our
                                                   success is how we work with customers in developing solutions to their most difficult

                                                   challenges. In all of our businesses, we have been successful in forming alliances, frame
                                                   agreements and other types of partnerships that have enabled us to develop some of
0.15
                                                   the industry's most innovative technologies. Through these types of working relation-
                                                   ships, we gain an in-depth understanding of our customers' most critical needs that
0.12
                                                   leads to innovations that are applicable throughout the industry.
0.09

                                                       Since the mid-1990s, we have formed alliances and working agreements with such
0.06
                                                   important customers as BP, Shell, Kerr-McGee, Statoil, Woodside Energy, Petro-Canada
0.03                                               and Norsk Hydro. In 2005, we built upon this legacy.

0.00
                                                        In the Gulf of Mexico, Anadarko Petroleum chose us to supply production systems
        
                                                   for their wells in the eastern Gulf of Mexico which connect into the Independence
                                                   Hub.

                                                        We were pleased to be chosen by Chevron in 2005 for two of their most important
                                                   projects under development -- Blind Faith in the Gulf of Mexico, and Agbami, offshore
                                                                                            


Nigeria. Since Chevron is a relatively new customer for us, we are especially pleased to
be supplying their needs in two major producing basins.

     2005 was an active year with our existing alliance partners as well. We renewed
our subsea systems frame agreement with Petro-Canada for offshore developments in
the Terra Nova Field off the East Coast of Canada.

     Statoil selected our subsea separation and process technology for use in the Tordis
Field in the North Sea. Together with Statoil, we are developing the first full-scale sub-
sea separation system using what we believe is potentially "game-changing" technology.
Our technology alone is expected to improve the field's recovery factor and enable
Statoil to extract an additional 19 million barrels from the field.

     In addition, our electric subsea control module will be retrofitted on equipment at
Statoil's Norne Field in the North Sea. This is a further development of our electric sub-
sea production system technology, which began with the installation of an electric
choke system on Statoil's Statfjord Field in 2001-2002.

     We entered into an agreement to provide equipment and services for Through
Tubing Rotary Drilling/Completion and Well Intervention (TTRD) in the North Sea.
This new technology, jointly developed with Statoil, is an important building block in
recovering more reserves from subsea fields less expensively by drilling new wells
through producing subsea wells.
                                                                                              
     Under our preferred-supplier agreement with Woodside Energy, Ltd., we were               
awarded contracts for their Perseus-over-Goodwyn and Angel Development projects,
both offshore Australia. We were also chosen by Norsk Hydro ASA to supply subsea              2.5
systems for their Oseberg Delta Field in the North Sea and by Total for their Moho
                                                                                              2.0
Bilondo project, offshore Congo.                                                                    
                                                                                              1.5
     Subsea production systems was not the only business that benefited from cus-
                                                                                                                
tomer relationships and technology leadership. Our surface wellhead business had              1.0               
record revenue growth in 2005. Surface wellhead focused on expanding our North
                                                                                              0.5                              
Africa presence and growing our market share in Algeria and Egypt and continued its                                        
long history of technological innovations, installing 20,000 psi wellheads for Burlington     0.0
                                                                                                        
Resources, McMoran and BP.

     In the Energy Processing segment, we successfully combined technologies from
across five businesses to provide a solution to Petrom, Romania's national oil company,
which allowed that company to bring four existing railcar loading stations at their           
Petrobrazi Refinery terminal up to European Union requirements. The fully automated           
loading system incorporated metering, vapor-recovery, fire-fighting and gas-detection         10
systems that enhance safety at the installation.
     In FoodTech, we introduced the D-Scan Product Attribute Processor, designed to
                                                                                               6
help poultry producers identify defective products on the production line and expand-
ed our market position in the freezing equipment business with a new "crust-freezing"                           
application for our ADVANTECTM line of commercial freezers. FoodTech has developed
                                                                                                                               
retort technology for Tetra Pak that processes carton packages at an economically                                           
attractive cost per unit. This technology is now being used by numerous leading food          
processors around the world, including Hormel for packaging of their chili products.

    The Airport Systems business is working closely with airline and air freight cus-
tomers and airports to develop the equipment to serve the needs of the new Airbus A-
380 aircraft. We have developed both boarding bridge technology and ground support
equipment for this market.


                                             C. Maury Devine, Vice Chairman of the
A proven team                                                                            To achieve this profitable growth,
                                             Board of Directors of Det Norske Veritas    we will:
     The solutions we provide to our         of Oslo, Norway, to our Board.
                                                                                             Build on the already strong posi-
customers' most difficult challenges are     Ms. Devine served 12 years in executive
                                                                                              tions of our businesses, each of
the backbone of our success. These solu-     positions for ExxonMobil as well as 15
                                                                                              which is a market leader.
tions are delivered by a proven team         years in U.S. government positions at
of people whose talents and commit-          the White House, the American
                                                                                             Fund internal growth initiatives
ment are the driving force behind            Embassy in Paris, and the U.S.
                                                                                              and evaluate selective acquisitions
FMC Technologies. Their commitment           Department of Justice. I believe her dis-
                                                                                              that expand our technology and
to helping our customers succeed is the      tinguished business and government
                                                                                              product portfolios in high-return
foundation of the actions taken by the       experience and leadership will be a
                                                                                              segments.
Company throughout the year.                 valuable asset to FMC Technologies.
                                                                                             Make capital expenditures to
                                             Looking ahead
      At FMC Technologies, we believe
                                                                                              increase manufacturing capacity
that our continuing commitment to
                                                                                              required to meet the growing mar-
                                                  As we look to the year ahead,
ethics is essential to maintaining the
                                                                                              ket for subsea trees worldwide.
                                             we have a solid platform for growth
trust that investors, customers, employ-
                                             with a total year-end 2005 backlog of
ees and suppliers place in us. The com-                                                      Continue to manage our portfolio
                                             $1.9 billion.
mitment is an integral part of our oper-                                                      of businesses profitably while
ating culture at all levels of the                                                            maintaining high returns on
                                                   We ended 2005 with a subsea tree
Company. During 2005, all of our key                                                          capital.
                                             market share of 40 percent. To service
managers worldwide were required to
                                             our growing subsea opportunities, we
                                                                                         
receive additional training on our code                                                       Expand our global reach into inter-
                                             have expanded our manufacturing facil-
of ethics and to certify compliance.                                                          national markets that are experi-
                                             ities in Malaysia and added machine-
With the help of an outstanding finan-                                                        encing growth and are seeking
                                             tooling capacity at our manufacturing
cial team, we complied at the outset                                                          new products and services.
                                             facilities in Houston and Scotland.
with Section 404 of the Sarbanes-Oxley
                                             We have expanded our service base in            Make the safety of people and the
Act.
                                             Angola and added a new facility in               quality of our products our highest
                                             Nigeria to serve that growing market.
      Outstanding quality, health, safety                                                     priorities.
and environment (QHSE) performance
                                                   We are intensely focused on execu-
is also one of our core values. We are
                                             tion by ensuring that we have the peo-
extremely proud of our safety perfor-
                                                                                               I believe that we have reason to be
                                             ple, the systems and the manufacturing
mance, which is consistently one of the
                                                                                         optimistic about the future of FMC
                                             capacity to deliver the highest-quality
best in the industries in which we
                                                                                         Technologies. We have a portfolio of
                                             and most innovative products and serv-
participate.
                                                                                         businesses that should yield excellent
                                             ices. Each of our businesses has
                                                                                         returns in the coming years. More
                                             strengths that provide platforms for
     In February 2006, our Board of
                                                                                         important, we have a talented, commit-
                                             creating additional value.
Directors appointed Peter D. Kinnear as
                                                                                         ted team of people who have demon-
President and Chief Operating Officer
                                                  In the coming year, these strengths    strated, over the past five years, the abil-
(featured on page 22). Peter's proven
                                             should allow us to grow our business        ity to deliver results.
leadership, operations expertise, vision
                                             while continuing to earn returns on our
of growth, and important industry rela-
                                             investment above those of our peers.
tionships will be vital to our continued
efforts to grow the Company. During his
34 years with us, Peter's successes are
                                                                                         Joseph H. Netherland
numerous, including building our sub-
                                                                                         Chairman and Chief Executive Officer
sea franchise to the industry leader it is
today and significantly expanding the
                                                                                         February 24, 2006
Company's technology base.

     Our team of employees is guided
by our exceptionally qualified Board of
Directors. We are pleased to welcome
