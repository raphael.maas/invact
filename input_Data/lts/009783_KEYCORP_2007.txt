Key reported income from continuing operations of $941 million in 2007, or $2.38 per
diluted common share, compared with $1.193 billion, or $2.91 per share, for the previous
year. Following two consecutive years of record income, the companys 2007 results
were negatively affected by extraordinary disruptions in the financial markets.
Indeed, capital market conditions in the second half of 2007 were unprecedented in modern
times, and in the words of a respected banking industry analyst, led to the third worst absolute
bank stock performance since 1939.
Nonetheless, Key reported solid commercial loan growth, favorable performances in several
fee-based businesses and strong capital levels. Total assets reached $100 billion for the first
time and, on January 1, 2008, Key completed the acquisition of U.S.B. Holding Company,
a $2.8-billion banking organization in the Hudson Valley area of New York State. The company
also made significant investments in several of its businesses to enhance Key client experiences
and expand product offerings.
In December, KeyCorps Board of Directors increased the companys first quarter 2008
dividend to $0.375 per share, an increase of 2.7 percent. This marks the 43rd consecutive
year Key has increased its dividend.
In the following interview, Henry Meyer, Keys chief executive officer since 2001, comments
on a wide range of topics, including Keys performance, additional steps the company has taken
in light of volatile market conditions, and longer-term strategic developments and investments.
Questions reflect those Meyer is asked most frequently by individual and institutional investors,
industry analysts, employees, the news media and community leaders.

Henry, you indicated early last year that
2007 likely would be a tough year for
banks. It turned out to be far more challenging
for the industry than anyone
might have predicted. How would you
broadly describe Keys performance?
Looking back, 2007 can be divided
into two markedly different periods.
During the first half of the year, we
were effectively executing on our
strategy, exceeding our profit plan
and earning recognition for strong
financial results. We were on track to
achieve our third consecutive year of
record earnings. Then came serious
market disruptions, beginning in
August, that negatively affected all
financial services companies. Keys
homebuilder portfolio and certain
types of capital markets and trading
activities were the major areas of
underperformance.
How would you characterize 2007 financial
results?
As noted, we had every intention of
achieving another record year. We are,
of course, disappointed that we did
not. However, given market conditions
in the second half of the year,
our 2007 results were very comparable
to those of our peer banks. I would
add a reminder that Keys results in
2006 and 2005 demonstrate what we
are capable of achieving in a more
stable interest rate environment.
In 2007, we reported $5.0 billion in
revenues, a modest increase over the
previous year, and our total assets
reached $100 billion for the first time.
Still, our earnings of $941 million
fell short of our record earnings of
$1.193 billion in 2006.
How do you analyze that performance?
On a relative basis, we avoided
some of the major issues that heavily
affected others in our industry. The
strategic decisions we made in prior
years protected us to a large extent
from the more severe earnings decline
of financial institutions active in the
residential or subprime mortgage
markets, or that issued certain types
of mortgage-backed securities or
complex investment vehicles.
Though the market disruptions definitely
slowed our financial momentum
in the last half of the year, and will
continue to affect us as we enter 2008,
we moved ahead with several major
projects to improve our businesses,
and adopted additional expensecontrol
actions at year end to sustain
our competitiveness.
Would you elaborate on Keys strategic
decisions?
In recent years weve made a number
of strategic decisions regarding our
mix of businesses and loan portfolio,
including exiting the subprime home
equity and indirect automobile lending
businesses. We decided two years ago
to sharply curtail our condominium
development lending in places where
we thought the market was becoming
overbuilt. Key also has limited its
participation in large and leveraged
syndicated financing transactions.
Moreover, we have no meaningful
exposure to complex securities such
as collateralized loan obligations
(CLOs), collateralized debt obligations
(CDOs), asset-backed commercial
paper (ABCP) and structured investment
vehicles (SIVs).
What were the bright spots in the companys
2007 performance?
Commercial loan growth was solid
last year, increasing to $52.7 billion at
year end, from $48.3 billion a year ago.
Our institutional asset management
unit, Victory Capital Management,
as well as several other fee-based businesses,
had favorable performances.
Excluding the impact of the sale of our
McDonald Investments branch network
last year, trust and investment
services income increased by 14 percent,
driven by growth in both personal and
institutional asset management income.
Keys Board of Directors in December
increased the quarterly dividend for the
43rd consecutive year. That puts Key in
a pretty select group of American companies,
right?
Absolutely. Very few U.S. companies
have such an impressive record. In fact,
Standard and Poors tracks companies
that have increased their dividends

for 25 consecutive years or more,
and Key is one of 59 that merit its
Dividend Aristocrat designation.
The Boards action to increase the
dividend sends a strong message to
our investors, clients and employees
about its confidence in the underlying
strength of the company and its longerterm
prospects.
SUSTAINING COMPETITIVENESS
Key announced actions in December that
are designed to sustain competitiveness.
Would you briefly describe these?
Three significant actions were taken:
We curtailed our origination of homebuilder
loans outside our 13-state
footprint; completed a company-wide
review to reduce expenses; and moved
to exit in 2008 two non-scale or outof-
footprint operations  dealer-originated
prime home improvement lending
and online payroll services.
Key also increased its loan loss reserves
at year end, right?
Yes, we did. Key is a modest player
in home mortgage lending; however,
we do make loans to residential real
estate developers, whose businesses
have been hurt by the downturn in
the U.S. housing market. As it became
increasingly evident that mortgagemarket
issues would persist in 2008,
we analyzed our loan portfolio exposure
and increased our loan loss reserves
by $245 million in the fourth quarter.
Further, based on our earlier decisions
to curtail out-of-footprint homebuilder
lending and condominium development
lending, we transferred $1.9 billion in
loans to Keys special asset management
group, which will supervise the
assets as we exit the underlying relationships
over time. It is important to
note that the majority of these credits
were performing loans  and we expect
them to continue to perform.
MAJOR EVENTS AND STRATEGY
In June, bank regulators lifted the regulatory
agreements pertaining to Keys
Anti-money Laundering/Bank Secrecy Act
(AML/BSA) compliance program. Could
you share your perspective on that?
Enhancing our training, detection
and reporting policies and procedures
in this important area was a major
accomplishment for Key, and we were
pleased when regulators lifted their
enforcement actions, which allowed
us to once again pursue bank acquisitions.
Our goal is to be ranked in the
top tier among our peers in the area of
compliance, and so it remains a strategic
priority of Keys Board and our
leadership. We believe we will succeed
because our work to date has affected
Keys culture in such a way that AML/
BSA-related practices have become an
integral part of how we do business.
During your tenure as CEO, Key has
completed 17 acquisitions and divestitures
to adjust its business mix, including
several in 2007. Are you satisfied with
your current position?
Our core strategy is to build enduring,
profitable relationships with our clients,
and we have, over time, developed a
diverse mix of relationship-focused
businesses. That said, one of the important
lessons of 2007 is that we operate
in a very dynamic and challenging
environment, and that we must continually
evaluate that mix against our
core strategy, growth and margin
prospects, and risk profile. So the short
answer is yes, but we will continue to
evaluate our position.
Our earlier moves to exit the subprime
mortgage business and certain other
lending activities proved to be wise.
We also believe shareholders will
benefit from our decision this past year
to exit the dealer-originated prime
home improvement lending and payroll
processing businesses. As important,
we have continued to invest in our
equipment leasing, institutional asset
management, education finance and
commercial real estate businesses in
National Banking and we are building
out our Community Banking franchise
with selective acquisitions.
Would you describe a couple of recent
examples?
In our institutional asset management
business  Victory Capital Management
 we added international and
hedge fund capabilities (see related
story on page 8). We also invested in
our Key Education Resources unit this
past year by adding Tuition Management
Systems, Inc., one of the nations
largest providers of tuition planning,
counseling and payment services for
families and students. Headquartered
in Warwick, Rhode Island, the company
maintains relationships with more
than 700 colleges, universities and
elementary and secondary educational
institutions. This acquisition also expands
the tuition payment plan options
we can provide, making our offerings
among the industrys most robust.
Through a series of acquisitions,
weve very successfully built one of
the top commercial real estate loan
servicers in the U.S.  now ranking
among the top five servicers nationwide
in a business that provides

substantial fee income and a stable
source of deposits.
Do you intend to make additional investments
in some businesses?
Yes. Wed like to build scale in some
of the geographic markets that comprise
our 13-state Community Banking
branch network. We believe that
returns to shareholders are strongest
when our share of the business in a
given market ranks us among the
largest three or four providers.
Which geographic markets are most
attractive to you?
Keys successful, long-term core
strategy has been to operate in four
regions of the country, as it offers us
diversity in terms of deposit pricing,
loan demand and risk management.
Certainly, we like our presence in the
higher growth markets of the Northwest
and Rocky Mountains, and would
like to expand through acquisitions in
those regions.
However, because much bank consolidation
has already occurred in those
western states, opportunities are fewer
than in the Midwest and Northeast,
where we see relatively greater room
for acquisitions. Merger activity is
most often a matter of being at the
right place at the right time and, of
course, having other banks come to
the realization that they can benefit
their shareholders and clients by being
part of a larger, more product- and
technology-rich company that shares
their values.
Our most recent bank acquisition in
the attractive Hudson Valley area of
New York State, near New York City,
is a good example of the type of fillin
opportunities we seek for our
Community Banking business.
How so?
The addition of U.S.B. Holding
Company, Inc., the holding company
for Union State Bank, effectively
doubles  to 61  the number of Key
branches serving the communities of
this attractive market. Our specialties
in small business, middle market and
private banking are excellent fits for
current and prospective clients. Another
benefit is that deposits gathered in our
branches represent a relatively stable
and cost-effective form of funding.
What are your priorities for Key in 2008?
We have five of them: Profitably grow
revenue; execute on our client-relationship
banking models; maintain a
disciplined, proactive risk management
culture; focus on expense management
and enhance the core capabilities of our
workforce.
INVESTING IN THE CLIENT EXPERIENCE
Key has launched two major projects to
enhance clients experiences in its
branches. Can you describe them briefly?
We launched a program this year
that will modernize about two-thirds
of our branches (see related story on
page 8). The makeovers will give the
branches more of a community feel;
reinforce the Key brand; and improve
decorating, lighting and landscaping.
The work began in the second half of
2007, and will continue over the next
few years. Second, we will begin installing
new technologies this year at
teller stations in our branches. These
improvements will build on our investments
in check-imaging capabilities in
recent years and speed transaction
processing. In addition, they will offer
more useful information to our frontline
team and free up time for more
interaction, providing an enhanced
experience for our clients.
KeyDRIVE is another important initiative.
What is it and how does it work?
Clients today expect speed and
ease when they do business with us.
KeyDRIVE, which stands for Deliver
Revenue  Improve Value and Efficiency,
involves meeting that expectation
by simplifying, standardizing or
expediting how we operate. The work
is accomplished by small teams of
employees, who devote 100 days to
tackling projects that make a difference.
At any given time, we have about
12 teams at work on KeyDRIVE
projects.
Whats an example of a KeyDRIVE project?
Replacing lost or stolen debit cards
used to be a cumbersome process for
our clients. Theyd look for help at
their local branch, get referred to call
centers, and then endure lengthy

telephone exchanges. A KeyDRIVE
team shaved a full day off the replacement
time and further simplified the
process so that it can be easily accomplished
at a Key branch or through
our call center  whichever channel
clients prefer.
Youve also been working to reach the
unbanked market.
Our KeyBank Plus program, launched
in Cleveland in 2004, has since been
expanded to branches in Akron-Canton;
Cincinnati; Albany; Syracuse; Denver
and Portland, Oregon. The program
meets the needs of the unbanked, that
is, people without a bank account,
and the underbanked, or people
without access to credit. Its success
has earned us the respect of both new
clients and community leaders, as
well as a favorable front-page article
in The Wall Street Journal.
Weve worked hard to understand and
serve the needs of this client segment.
Recognizing that one of the most
frequent concerns among the unbanked
is having convenient, safe access to
their money  especially cashing
paychecks  we created KeyBank Plus.
Clients can cash payroll or government
checks for a small fee, based on
the value of the check  a fee that
typically is a fraction of that charged
by store-front check-cashing operations.
As important, KeyBank Plus
provides financial education, including
courses on managing money and debt.
To date, more than 13,000 clients have
enrolled in the program and cashed
more than $32 million in checks. Losses
on cashed checks have been minimal.
BOARD OF DIRECTORS ACTIONS AND
CHANGES
Shareholders are being asked to vote on
declassifying the Board at this years
Annual Meeting on May 15. Whats prompted
that move?
A majority of Key shareholders voted
last year in favor of a proposal for
declassification, after which our Nominating
and Corporate Governance
Committee studied the matter and
made its recommendation to place the
proposal on this years Annual Meeting
agenda for a vote. If the proposal
passes, well begin to elect directors
annually, starting in 2009. Presently,
Board members serve three-year
terms. To honor the unexpired terms
of previously elected directors, we
would, under the proposed amendment,
elect one-third of the Board for
a one-year term in 2009, two-thirds
for a one-year term in 2010, and the
full Board would be elected for oneyear
terms in 2011.
Are there any other changes in the Board
this year?
Charles R. Hogan, who joined the
Board in 1993, has reached our mandatory
retirement age for directors, as
established by our Corporate Governance
Guidelines. Charlie has provided
effective counsel and advice throughout
his 15 years as a director. We will miss
him, and sincerely thank him for his
outstanding service.
Charlies commitment and engagement
is representative of our entire Board.
Our directors take an active, thoughtful
interest in our strategy and management
decisions, offering valuable perspectives
on a wide range of issues. I believe Keys
shareholders are well represented by
the Board, whose guidance and support
have contributed greatly to Keys
strategic direction and outlook.
Finally, Henry, what gives you confidence
that Key is on the right track?
Weve come a long way since 2001.
I have great confidence in the leadership
team we have assembled, not only within
the lines of business, but also in our
staff groups. Building a strong management
team that shares Key values
was one of my first priorities. I believe
we have accomplished that. Weve
demonstrated discipline and success in
adjusting our business mix with acquisitions
and divestitures to improve
our financial performance. And the
momentum in our Community Banking
organization continues to build.
Im not sure anyone could have foreseen
the market environment we
witnessed in the second half of 2007,
but our overall results in recent years
demonstrate a significant turnaround
and a solid strategic basis for further
progress. My team and I remain very
confident about Keys future.

