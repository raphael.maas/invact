DEAR FELLOW SHAREHOLDERS

Arizona Public Service celebrated its 125th year of business in 2011 by rewriting the record books.
The company achieved new bests for reliable service, customer satisfaction, power generation and
employee safety. These achievements and others marked an outstanding year for APS, the principal
operating subsidiary of Pinnacle West Capital Corporation.
Our strategy to translate this performance into long-term value for our shareholders is simple and powerful.
This proven strategy begins with the ground beneath our feet. The potential for sustained, long-term
economic growth in our Arizona service area is among the best in the nation. To maximize this potential,
we must get the basics right: Provide safe, reliable electricity and excellent customer service. Prepare for
the future with wise energy-related investments. Promote a collaborative effort with public officials and
our diverse stakeholders to create a sound regulatory climate and smart energy policy. Practice financial
discipline at all times.
If we do these things well, our business will grow along with Arizona.
Success requires us to deliver on these fundamentals. As our performance in 2011 demonstrated again,
APS employees do that extremely well. Let me offer a few highlights.
PROVIDE RELIABLE ELECTRICITY AND EXCELLENT SERVICE
During a year that included the hottest monsoon season on record, APS provided 1.1 million Arizona customers
with power reliability among the best in the industry. We averaged 0.79 power outages per customer, 29 percent
better than the national median. When outages did occur, we restored power quickly. The typical APS customer
experienced 69 minutes of interrupted service over the course of the year, 40 percent better than the median.
APS set new records in both of these critical performance measurements. It is not a coincidence that APS earned
its highest-ever score in the J.D. Power and Associates survey of customer satisfaction with electric utilities.
APS ranked fourth in the nation among large investor-owned electric utilities.

Part of providing great service includes assisting customers with the expanding options for managing
their home energy use. In 2011, we helped APS customers take actions that collectively saved 440,000
megawatt-hours of electricity. Our performance earned the U.S. Environmental Protection Agencys
highest honor for continued ENERGY STAR leadership for the third year in a row. As customer interest
in residential solar energy continued to grow, more customers took advantage of APS incentives to
install solar panels on their homes and businesses in 2011 than in any prior year.
Power generation achieved a milestone year as well. The Palo Verde Nuclear Generating Station reached
its 20th consecutive year as the largest power producer in the United States. Our fossil-fueled generation
fleet also had another solid year. The fleet achieved a commercial availability factor (available to run when
commercially viable to do so) exceeding 90 percent.
Our company receives numerous honors for sustainable business practices. We regard as especially
meaningful Corporate Responsibility Magazines recognition of Pinnacle West as the top-ranked U.S.
electric utility on its list of 100 Best Corporate Citizens.
Employee safety is another area in which we achieved all-time best performance, reducing on-the-job
injuries by 15 percent compared with 2010. Safety performance has improved in each of the last four
years and now ranks in the top quartile of our industry.
INVEST WISELY FOR THE FUTURE
In last years annual report, I highlighted our plans to purchase Southern California Edisons share of the
APS-operated Four Corners Power Plant, invest $300 million in new environmental controls for the two
newest, most e?cient units and shut down the three oldest units. Our plan would protect a major source
of a?ordable electricity for APS customers, preserve jobs on the Navajo Nation where the plant is located
and significantly reduce carbon emissions. We achieved a milestone in 2011 when the Navajo Nation
Council approved a 25-year lease extension for the plant. Much work remains, including authorization
from the Arizona Corporation Commission (ACC) and the negotiation of a new coal contract for the plant.

While one of our companys greatest assets is our historically fast-growing service area, another is
the sun shining in the wide Arizona skies. Because the sun shines 300 days each year, solar energy
will become an important part of our diverse resource portfolio.
We have now received approval from the ACC to construct 200 megawatts of APS-owned solar
generation by the end of 2015. Demonstrating the effectiveness of this initiative, APS and project
partner First Solar achieved a first in 2011 when the 17-megawatt Paloma Solar Plant moved from
breaking ground to commercial availability in only four months, a new record for both companies.
Paloma is joined by the 17-megawatt Cotton Center Solar Plant and the 16-megawatt Hyder Solar
Plant as the first completed projects in our AZ Sun portfolio.
In addition to pursuing the continued operation of Four Corners and developing Arizonas solar potential,
we plan to meet future electricity demand with new investments in transmission. Our 10-year plan
includes $550 million for 269 miles of new transmission, which will help maintain reliability and open
new areas for development of renewable energy sources.
PROMOTE A SOUND REGULATORY CLIMATE
Inclusive, open, collaborative and constructive. That is how one participant, testifying to the ACC,
described the negotiations that produced a settlement agreement among 22 parties in the pending
APS general rate case. The settlement drew support from a diverse array of groups, including those
representing residential customers, commercial and industrial customers, low-income customers,
realtors, unions, competitive power suppliers, federal agencies and utility investors.
Hearings on the settlement agreement concluded in February 2012 and we anticipate a final decision
by mid-year. For current information on the APS rate case, I encourage you to visit azenergyfuture.com,
where we post summaries of the settlement agreement, testimony and other useful information.
One other regulatory accomplishment merits special note. After more than two years of intense review
and scrutiny, the U.S. Nuclear Regulatory Commission approved Palo Verdes license renewal application.
The NRC extended the operating licenses for all three Palo Verde units 20 years beyond the original 40-
year licenses, allowing Unit 1 to operate until 2045, Unit 2 until 2046 and Unit 3 until 2047.
The devastating tsunami in March 2011, which led to an accident at Japans Fukushima Daiichi nuclear
plant, had repercussions throughout the world. The resulting events led to questions about the safety
of nuclear power. Palo Verdes design and location differ significantly from those of the Fukushima plant.
The tragedy put the spotlight on one of the defining attributes of the U.S. nuclear power industry: its
commitment to continuous improvement. Within hours of the tsunami, we assembled a team of our
most experienced engineers to track the situation in Japan and analyze potential lessons. Palo Verde
will be safer and stronger as a result.

PRACTICE FINANCIAL DISCIPLINE
Pinnacle West achieved solid financial results again in 2011. The company earned net income of $339
million or $3.09 per share, compared with $350 million or $3.27 per share the previous year. Ongoing
earnings amounted to $2.99 per share, which represents a slight decrease from 2010.
Pinnacle Wests total return to shareholders  a combination of stock price appreciation and dividends
paid  reached 22 percent. Our results slightly bettered the return for the S&P 1500 Electric Utilities Index
and significantly outperformed the 2 percent return of the S&P 500 Index.
Pinnacle Wests total return has exceeded market averages for the last one-, two-, three- and five-year
periods. Pinnacle Wests shareholder value increased nearly $1 billion in 2011 and nearly $2.7 billion over
the past three years.
In June, Standard and Poors upgraded its corporate credit ratings for Pinnacle West and APS from
BBB- to BBB. S&P cited the companys stronger credit metrics, debt reduction, improving regulatory
environment and prudent financial management. The upgrade allows our company to borrow at lower
interest rates and improves our access to debt markets, thereby reducing our financing costs for new
infrastructure and system improvement projects.
We continue to narrow our focus to what we do best  run an electric utility. To this end, we sold our
competitive energy services subsidiary, APS Energy Services, in 2011 for a gain of $10 million.
GROW OUR BUSINESS AS ARIZONA GROWS
Over the next three years, we expect our customer base to grow an average of 1.6 percent per year as
economic recovery takes hold. We project that growth will return to Arizonas traditional levels over the
long term, resulting in 2.6 percent compound annual customer growth through 2030. That equates to
approximately 1.8 million customers then, up from 1.1 million today. Our investment in Arizonas future
goes well beyond the electricity grid. According to an economic impact report prepared by Arizona State
Universitys W.P. Carey School of Business, APSs business activity contributed $3.4 billion to the Arizona
economy in 2010 and supported 39,000 Arizona jobs. APS also has the distinction of being Arizonas largest
taxpayer. The company paid $400 million in taxes to the state and to municipal governments in 2010.
Our impact does not end there. APS employees and their families volunteered 157,000 hours to worthwhile
community organizations, which translated into $3.3 million in service to the community. Our employees also
support our communities through the annual Community Services Fund campaign, which benefits United
Way and other local non-profit agencies. APS employees contributed $4 million to the community in 2011.

LOOKING AHEAD
After more than 30 years of outstanding service to our company, Pamela Grant retired from our board
of directors. Pam is a knowledgeable business executive, a committed civic leader and a trusted advisor.
Her contributions have made us a better company. Doug Parker, a smart and experienced business
leader, also left our board this year. We will miss their thoughtful guidance and support.
Moving forward, I see our impressive long-term potential, resulting not only from our tested strategy,
but also because of the people and teams we have assembled and empowered to put the strategy into
action. The skill, dedication and ingenuity of our employees stand out. They take special pride in their
day-to-day work on behalf of our customers and have committed themselves to achieve our vision of
a sustainable energy future for Arizona.
Thank you for your support and your investment in Pinnacle West.
DONALD E. BRANDT
Chairman, President and Chief Executive Officer
March 7, 2012