Dear Fellow Shareholder:
Im pleased to report that IntercontinentalExchange
in 2012 once again achieved record operating
results, including our seventh consecutive year of
record revenues and earnings. Our team worked
diligently amid ongoing regulatory uncertainty
to deliver results for our stakeholders. And
while many rightly characterize it as yet another
challenging year for global economies, it was
also a year that we outperformed our sector
and invested our resources to position ICE for
whats next. We engineered our business model
to anticipate and serve new opportunities across
the globe while maintaining our industry leading
growth profile. And in December, we announced
the acquisition of NYSE Euronext to form a
leading global market operator, which we expect
to finalize in the second half of 2013, subject to
regulatory approvals.
ICE posted record revenue of $1.4 billion and
net income attributable to ICE grew 8% to $552
million in 2012, driven primarily by record
trading volume in our energy markets, coupled
with our leading central clearing business. Our
efforts are directed at serving the needs of our
customers and delivering on our commitment to
shareholders to remain a growth leader. We will do
this by capitalizing on the secular trends occurring
globally, while expanding prudently to reach new
markets and meet demand for risk management.
While I am pleased to recognize our achievements
to date, I want to emphasize our intense focus
on expanding our earnings power for the future.
Coach Mike Krzyzewski of Duke University is
known for his next play philosophy, which is
essentially that what you have just done is not
nearly as important as what you are doing right
now. With that in mind, we reflect on 2012 and
ask ourselves how we have prepared our company,
our employees and our customers for the next
play.
Building on our Track Record
In 2012, ICE produced industry-leading
returns on invested capital of 18%. These results
meaningfully outperformed our competitors and
the broader market - and this focus on returns
reflects ICEs commitment to disciplined growth
and investment. Average daily volume (ADV)
reached 3.4 million contracts in 2012, an increase
of 10% from 2011. This was significant given low
commodity price volatility and the uncertain
political and economic environment that
produced double-digit volume declines for most
of our competitors.
In our global energy markets, ICE once again
posted growth on top of growth -- and we
have grown our volume annually in the energy
markets since I wrote our first letter as a public
company for our 2005 report. In fact, we
recorded our 15th consecutive record volume
year at ICE Futures Europe. And, ADV in our
global energy markets in 2012 was up 13%,
building on a 23% increase the prior year.
Our energy markets represent a diverse range of
products, primarily relied upon by commercial
hedgers, and they are led by ICEs North Sea
Brent crude futures contract. Brent crude
continued to expand its role as the global oil
benchmark in 2012. For the first time in its 25-
year history, the ICE Brent contract out-traded
the NYMEX WTI contract by roughly 1 billion
equivalent barrels. The Brent crude oil contract
offers many advantages to the global oil market,
including the leading underlying physical
production of any of the worlds notable and
transparently traded oil benchmarks. Seaborne
North Sea crude can be shipped and exported
globally.
The Energy Information Administration (EIA),
the U.S. energy statistics agency, noted its shift to
Brent as the reference point for its annual
report on global oil markets. And the S&P GSCI
index raised the weighting of Brent again this
year. Importantly, the U.S. CFTC commitment
of trader reports demonstrate that the producer

community continues to rely upon Brent for
its global hedging needs. These are just a few of
the reasons that the Brent contract remains well
positioned for future growth.
Similarly, in 2012, European emission contract
volume increased 23% compared to 2011. With
Phase III of the EU emissions trading program
now underway in 2013, continued demand for
our leading carbon markets is evident in terms of
both record volume and open interest.
We introduced 130 new energy products in
2012 to ensure that our customers have the
tools to manage risk in an evolving economic
and regulatory environment. As part of our
new product development, we have focused
on expanding our products and technology to
serve the options markets. The results of these
plans have begun to play out, with European
energy options volume up nearly 200% in
2012. ICEs total options volume surpassed 100
million contracts in 2012. We believe that we
are still in the early stages of the options market
opportunity.
In 2012, ADV in our agricultural commodity
markets rose 7% following a year that was
impacted by challenging commodity credit and
financing trends, as well as the failure of two
futures commission merchants. Open interest
rose 9% above 2011. In 2012 ICE Sugar, also
known as World Sugar, volume rose 3%. We also
introduced new U.S. and Canadian grain and
oilseed contracts that offer customers greater
contract diversity to satisfy their hedging needs.
Volume in Russell index products, foreign
exchange (FX) and credit default swaps
(CDS) were muted in 2012, amid ongoing
regulatory and economic headwinds. Yet, within
the important financial products space, ICE
continued to prepare to support these markets
recovery as the cyclical impacts of the fiscal and
economic environment begin to moderate.
These new initiatives range from developing
OTC FX clearing services that are responsive to
new regulatory requirements for our customers,
as well as new FX product launches, such as the
Indian rupee and the Brazilian real.
In the credit derivatives markets, we believe that
the progress of regulatory reform and improving
market conditions will reduce these headwinds.
With the U.S. mandate for clearing CDS
indexes having taken effect in March 2013, our
leadership continues to play an important role
in participants ability to meet new regulatory
requirements. ICE cleared a cumulative $36
trillion in gross notional value of CDS contracts
through 2012, beginning with our launch
in 2009. We also recognized demand for a
complementary credit futures product to the
existing swap market, and in October 2012, ICE
licensed Markits CDS indices to develop futures
contracts that we expect to introduce in the first
half of 2013. Our team is working closely with
the industry on this effort, just as it did to achieve
regulatory approval of portfolio margining for
buy-side participants earlier this year.
One of our most important initiatives in 2012,
which involved approximately 800 natural gas,
power and oil contract types, was the timely
transition of the markets cleared energy swaps
portfolio to futures markets. This action was the
natural next stage in an evolution that began
over 12 years ago when ICE first entered the
energy markets with a transparent electronic
platform and a plan for a cleared solution for
the swaps markets. As standardization, clearing
and transparency became commonplace in the
energy market since that time, our OTC swaps
markets had come to function and be regulated
in largely the same manner as futures markets.
This included increased regulation that ICE
uniquely adopted, including position limits and
reporting following the 2008 Farm Bill. Because
of that groundwork, the focus of my colleagues
and the cooperation of our customers, the
October transition to regulated futures contracts
was seamless.
In the U.S., we have worked through much of
the rulemaking process relating to the swaps
markets that started with the passage of Dodd-
Frank in 2010 and is now being implemented
or finalized. In addition to established risk
management procedures that weve had in place
for years at ICE, we began several initiatives to
provide additional solutions to ensure market
participants can readily comply with increasing
regulatory requirements. Building the industrys
first swaps data repository, continuing to invest in
our leading CDS clearing houses, and expanding
our products to support mandatory clearing

requirements, are just a few examples of the investments we are making in
our markets to support our customers. In Europe, the rulemaking process
is in an earlier stage and we are working to help inform the process and
develop the tools required to be responsive.
The Business of the Future
Our leading, global energy markets are at the root of our companys
formation and our heritage. We are proud of the positive impact that weve
had in driving the transparency, security and evolution of these markets.
Being a part of this progression has given us the ability and expertise to
enter new markets and to support their evolution. That is why we continue
to diversify and expand our markets globally -- and 2012 was no exception
to that effort. In the first part of the year, we announced an agreement with
Griffin Markets Group to partner to expand our offering for the European
energy markets. This announcement was followed by our agreement to
acquire a majority stake in the derivatives and spot gas business of APXENDEX,
a continental European energy exchange. This will enable us to
support the development of the European natural gas and power markets by
growing a greater liquidity pool for end users to hedge their risk and bring
enhanced transparency and competition to these markets.
Creating a Premier Global Market Operator
I believe that the combination of ICE with NYSE Euronext will result in
an expanded portfolio of products and services available to our customers
and provide new growth and diversification opportunities for investors.
Importantly, we will enter the interest rate and cash equities markets at
cyclical lows, and at a time ripe for market structure innovation and growth.
The portfolio of opportunities offer an exciting range of initiatives that
we can develop to benefit the global capital markets. These opportunities
are well suited to ICEs strengths across clearing,
technology, new product development, and
regulatory compliance.
We are very pleased with the progress
and pace of our planning to acquire
NYSE Euronext, and are eager to begin
executing on the growth opportunities
this expanded portfolio brings us.
Notably, we have begun the transition
of NYSE Liffes UK markets to the ICE
clearing platform under the clearing
services agreement announced in December.
NYSE Liffe concluded that an outsourced
solution provides the greatest
certainty under
proposed EMIR
compliance
requirements
and
timelines.
ICE, with
its
proven execution and previous experience with clearing transitions, proved
to be the best partner. As a result of this relationship, market participants
will benefit from the enhanced product development opportunities and
efficiencies that ICE Clear Europe offers.
Market Design for the Future
ICEs technology is more than just hardware and software - it is how we
connect with our customers - and it is designed to improve and enhance
their experience in connecting with our markets. We continue to invest in
building platforms and tools that keep our customers ahead of the curve.
ICE today offers the industrys most intuitive instant messaging platform
for traders and brokers in the commodity and equity derivatives market
through ICE Chat.
In 2012, we acquired WhenTech, an advanced option pricing and risk
management platform, to further enhance our options markets. We made
significant upgrades to our front end system, WebICE. And today the ICE
platform remains the leading central limit order book platform for global
energy markets. We also worked with Brazils largest custody and settlement
firm for the fixed income market, Cetip, to build a bond trading platform
operational in both English and Portuguese. Developing and supporting
our own technology allows us to introduce new products, serve new
markets and improve capital efficiencies that translate into solutions for our
customers when and where they need them.
A Focus on Growth and Leadership
The key to ensuring that we are offering the products and services that our
customers require today, and that we are working on the ones that they will
need tomorrow, is having a deep bench of talented professionals across our
company. Our entire team is focused on results and driving our business
forward. The ICE culture is one that encourages employees to pursue the
next opportunity to meet our growth and innovation goals. This focus
on the future and on results has yielded many accomplishments in
2012: we have extended our international reach, we expanded our
clearing services, we added to our technology infrastructure through
acquisitions and partnerships, and we maintained our lean operating
model while growing profits consistently.
So whats next for ICE in 2013? We are confident that it will be another
strong year, one in which we maintain our focus on our core business
while we work to complete the NYSE Euronext acquisition. We know
that change is inevitable, and that ICE will be at the forefront to
ensure that we capture new opportunities. And given
our teams ability to innovate and adapt, I am
confident that the steps we took in 2012,
and those that we are focused on today,
will continue to position us to capitalize
on the next play.
Thank you for your continued support
of and interest in our company.
Jeffrey C. Sprecher
Chairman and Chief Executive Officer