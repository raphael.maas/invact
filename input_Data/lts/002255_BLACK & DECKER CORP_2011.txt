Total revenues were up 12.0%, on a pro forma basis, to
$10.4 billion. Earnings per share (EPS) grew 26% to $5.24*
and our dividend was increased 20%. Working capital
turns increased 18% to 6.7, as the tenets of the Stanley
Fulfillment System were further embedded into the
global enterprise.
2011 was also important because we began to see
tangible evidence of the successful crystallization of the
benefits of merging Stanley and Black & Decker, two iconic
companies in their own right, which together comprise
something very compelling to our customers, investors and employees. Some might be surprised to learn
that Stanley Black & Decker is a growth company with a
forward-looking approach and a track record to be proud
of. Since 2003, revenues and EPS have grown at a 20%
and 13%* compound annual growth rate (CAGR), respectively. We are stewards of capital and focus acutely on
aggressively generating cash, reinvesting it wisely and
returning substantial amounts to our shareholders every
year. Perhaps that is one reason why we have considerably
outperformed the S&P500s total return on each of a
three-, a five- and a ten-year basis. These metrics are
very important to us because we are not satisfied with
simply performing at benchmark. Outperformance is
what we strive for day in and day out and, thus far, we
have delivered.As we look forward in the short term, we believe that
our Company is well-positioned to enjoy the benefits
of a potentially more favorable construction cycle and
overall economic conditions in the U.S. but also poised to
deliver if these benefits dont materialize and if Europes
economic woes worsen to some extent.
For the long term, we expect to continue to deliver
above-average revenue, EPS and cash flow growth while
continuing an investor-friendly orientation to our capital
allocation. We would like to take this opportunity to reiterate the mid-decade goals we communicated a year ago
at our analyst meeting:
 $15 Billion in Revenue
 Greater than 15% Operating Margin Rate
 Return on Capital Employed of 15%
 10 Working Capital Turns
 20%+ of Revenues from Emerging Markets
We remain confident we are on track to meet, if not
exceed, these goals and that, in doing so, we will continue
to create outstanding and dependable shareholder value.
Our stated strategy of continuous evolution as a diversified industrial company remains in place, even as we look
to enjoy the benefits of an improving construction cycle as it ramps up. We remain focused on a balanced capital allocation plan over the long term, in which roughly two-thirds
of our free cash flow will be deployed to promote organic
growth across all businesses and to complete acquisitions
within our four growth platforms. As has been the case
for years, our portfolio strategy centers around a steady
and methodical reallocation of capital to activities where
superior returns are sustainable and above-portfolioaverage organic growth is achievable.
The remaining one-third of free cash flow is returned to
shareholders in the form of dividends and share repurchases. Inherent in this is our focus on maintaining
a strong investment grade credit rating. 2011 was an
exemplary year to illustrate this, as we increased our
dividend 20%, implemented a $350 million share repurchase, lowered our debt to EBITDA ratio to 2.2x from 2.8x,
and purchased a leading European commercial electronic
security and monitoring company, Niscayah.
Stanley Black & Decker: The Next Chapter
Almost two years into the integration of Black & Decker we
continue to be pleased with success that has surpassed
even our own high expectations. When we announced the
transaction in November of 2009 we laid out the following
three-year targets: $350 million in cost synergies, $1.5
billion in EBITDA, $1 billion in FCF and $5.00 in EPS.
As we close out 2011and the first twenty-two months
as a combined enterprisewe have already achieved
approximately $350 million in cost synergies and raised
our estimate to $450 million by the end of 2012. During
our first full year of management, the FCF target has
been achieved and the EPS and EBITDA targets have
been exceeded.
In January of 2011, we announced the expected revenue
benefit from the combination to total between $300 and
$400 million by the end of 2013, in addition to our normal
organic growth initiatives. This incremental revenue is
expected to drive $0.35$0.50 of EPS accretion over the
same time period, and while the focus of 2011 was on
the initial investment in these programs, the results met
our expectations handily. These synergies result from a
myriad of opportunities around the globe arising from the
merger, including, but not limited to, brand expansion
and leveraging complementary geographic and channel
strengths. One notable example was the over 50% growth
of the legacy Stanley hand tool business in Latin America
due to the post-merger access to legacy Black & Deckers
well-established distribution channels, and the ongoing
expansion of the BDK manufacturing plant in Uberaba,
Brazil to enable in-region hand tool manufacturing, a key
cultural factor to successful growth.
The success of the integration has not just been evident
through our financial performance versus our stated goals,
but also through the opinions of our employees. In 2011,
we conducted a global employee opinion survey in which
our approximately 40,000 associates were able to anonymously submit their views on 60 questions. The feedback was encouraging, as close to 90% of the questions
surveyed were considered favorable or highly favorable,
and the responses from each legacy company were virtually identical. This feedback was particularly encouraging
because the soft issues of merging two cultures can often
undermine the success of a major acquisition. We are
pleased to report that, if anything, the cultural integration
has been a major positive for this transaction.
Niscayah
When we announced the combination with Black &
Decker, many of you asked how it made sense in light of
our long-standing strategy to diversify away from the U.S.
building products sector and expand upon our attractive growth platforms, in our journey to become a global
diversified industrial company. Our response was that
the old playbook remained the same and that the compelling strategic and economic rationale behind the merger
would create a stronger, more valuable franchise, and the
powerful free cash flow of the combined Stanley Black &
Decker would enable us to achieve our goals of diversification at a faster pace than as a standalone company. Our
acquisition of Niscayah is a case in point.
Niscayah, our largest acquisition within our Security
segment to date, which closed in September 2011, is a
leading European commercial electronic security and
monitoring company we had pursued for over five years,
because of its strategic fit. Niscayahs integrated security solutions include video surveillance, access control,
intrusion alarms and fire alarm systems, and its offerings
include design and installation services, maintenance and
repair, and monitoring systems. The acquisition expands
and complements Stanley Black & Deckers existing
security product offerings, and further diversifies the
Companys operations and international presence. The
addition of Niscayah brings total pro forma Convergent
Security Solutions annual revenues to approximately $1.8
billion and the pro forma Security segment to $3.3 billion,
roughly 30% of the entire Company.
We expect the transaction to result in annual cost savings
of approximately $80 million$45 million of which is
expected to be realized by the end of 2012. The acquisition
is also expected to be immediately accretive to Stanley
Black & Deckers EPS, with accretion of $0.20 in 2012 and
$0.45 by 2014. Importantly, the acquisition was funded
with existing offshore cash resources, with no additional
debt or equity issuances, which means that $1.2 billion of
nonproductive cash was put to work.
Since 2004, when our journey to broader diversification began, we have made more than 60 acquisitions
and cultivated a potent, proven and effective integration
process along the way that we deploy no matter what the
size or nature of the business being acquired. We believe
our ability to integrate companies successfully is a key
component to driving shareholder value and a competitive advantage of Stanley Black & Decker. This being said,
having the management and personnel bandwidth to successfully focus on the crucial rhythms and rigors of an
integration is of utmost importance to us, and we have
walked away and will continue to walk away from acquisitive growth opportunities if we feel the timing conflicts
with the successful navigation and development of our
core business franchise.
Organic Growth: Innovative New Products and
Emerging Markets
Inherent in our legacy and crucial to our future is our ability
to bring innovative, world-class products and services to
our customers. Our success in this was key to the organic
growth we drove throughout the year, particularly in certain
markets that were flat or retracting. For example, our professional power tool and accessories business grew 13%,
due to the successful launch of the DeWALT 18- and 20-volt
MAX* cordless lithium ion product line and the late-2010
launch of the 12-volt cordless lithium ion products. Within
our industrial segment, our $900 million engineered fastening business, which largely operates under the Emhart
brand, grew 13%, compared to the 3% growth of global light
vehicle automotive production, due to new products and
increased customer platform penetration, which resulted in
strong market share gains.
As we wrote last year, significant emphasis has been,
and will continue to be placed on growth in the emerging
markets, both organically and through acquisition. In 2011alone, we increased our revenues in emerging markets
from 11% of the total Company to 14%, well on our way to
our mid-decade goal of more than 20%. Also important to
note is that our profitability in a majority of these regions is
higher than corporate line average, aiding us towards our
mid-decade goal of operating margins higher than 15%.
Global organic growth is of utmost important to us and
this year we have focused on building teams in
many regions where we historically have had an underwhelming presence.
Stanley Fulfillment System
A key ingredient in the success of our ability to service
our customers, integrate acquisitions, increase efficiencies and grow our free cash flow base continues to be the
Stanley Fulfillment System, or SFS. In 2011, we increased
total Company working capital turns 18%, from 5.7 to 6.7,
and up 23% to 7.0 turns, excluding the impact of Niscayah.
While these results are still distant from the 8.6 turns that
legacy Stanley reached prior to the merger in 2010, we are
confident that the tenets and processes of SFS that took
our working capital turns from the mid-4s to this level in
three years will be just as successful for our combined
enterprise. We continue to believe SFS represents one of
the biggest opportunities to create additional value from
the Stanley Black & Decker merger, with the potential
to unlock over half a billion dollars when we are able to
return to 8 turns, let alone our mid-decade goal of 10.
Summary
As we said four years ago in our Letter to Shareholders,
during the depths of the global economic crisis, this is
a world in which strong, reality-based companies that
are well-prepared emerge as winners and reward their
investorsa world that places a premium on management teams that operate with agility, courage and common
sense. Our senior management team is both seasoned and
energetic, with our officers having been in key leadership
positions with Stanley Black & Decker for an average of
13 years. Needless to say, this is the same team that took
the Company through the 20082009 downturn, where we
illustrated agility and proactively took the necessary steps
to navigate through both the foreseen and unexpected
headwinds we faced.
2012 will undoubtedly come with its own set of challenges,
but we are armed with our proven strategic roadmap for
growth, our financial discipline and strength, and
most importantly, our team of close to 45,000 associates
around the globe, with a daily focus on outperformanceall of which will enable us to continue to drive
meaningful value for our shareholders.
John F. Lundgren
President & Chief
Executive Officer
February 23, 2012
New Britain, CT
James M. Loree
Executive Vice President &
Chief Operating Officer
