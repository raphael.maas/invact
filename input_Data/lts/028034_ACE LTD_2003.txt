Dear Fellow Shareholder:
I am pleased to report that 2003 was an outstanding year of
achievement for Chubb, with record earnings per share. Moreover,
Chubbs stock price reflected our strong operating performance, with the
value of Chubb shares increasing by more than 30% during the year.
Let me summarize Chubbs 2003 highlights. Net written premiums
grew 22% to $11.1 billion, and earned premiums were up 26% to
$10.2 billion. This marked the first time in Chubbs history that our
annual earned and written premiums passed the milestone of $10 billion.
Net income grew to $4.46 per share from $1.29 per share in 2002.
Operating income, which we define as net income excluding realized
investment gains and losses, grew to a record $4.16 per share, up from
$1.16 per share in 2002. These record per-share earnings reflected a
continued favorable insurance environment, combined with far greater
focus and execution in Chubbs core property and casualty businesses.
Equally important, we took a number of actions that resulted in our ending

2003 with considerably fewer troubling uncertainties than were hanging
over us at the onset of the year.
Chubb Commercial Insurance (CCI) was our standout performer in
2003. CCI grew its net written premiums by 21% and produced an
outstanding combined ratio of 89.2%, a 3.9 percentage point improvement
over 2002, excluding asbestos and environmental (A&E) charges in both
years. Including the A&E charges, the combined ratio was 95.9% in 2003
and 118.6% in 2002. CCI wrote $1.2 billion of new business worldwide
in 2003.
CCIs continued excellent performance represents the culmination
of actions initiated in the soft market of the late 1990s. At that time,
following unacceptable loss experience, we began re-underwriting our
book of business. We exited historically loss-prone segments of business,
and for those we continued to write, we raised rates and tightened terms
and conditions. All of this seems easy now, but these actions were taken
with great risk of losing business at the time. However, we stayed the
course, and as a result today we have a bigger and far stronger CCI.
For Chubb Specialty Insurance (CSI), financial results were affected
by unfavorable developments in our Executive Protection (EP) and
Financial Institutions (FI) lines related to claims occurring before 2003.
The adverse development was due in large part to directors & officers and
errors & omissions claims related to corporate failures and allegations of
management misconduct and accounting irregularities in recent years.
In response to these problems, we have substantially increased rates,
walked away from some of the most loss-prone segments of our business,
lowered limits, and tightened terms and conditions. As a result, we believe

the EP and FI business we are currently putting on the books should be
profitable, as it realizes the benefits of these higher rates, lower limits and
improved contractual terms.
Overall, CSI premiums grew 31% in 2003, and the combined ratio
was 100.0%. Our unfavorable results in EP and FI were offset by earnings
in our other specialty lines. Notably, Chubb Re had a spectacular year,
doubling premiums to $984 million and producing a combined ratio of
93.5%.
Chubb Personal Insurance (CPI) produced premium growth of 12%
and had a combined ratio of 98.2%, including 7.9 points of catastrophe
losses. Excluding catastrophe losses in both years, CPIs combined ratio
improved 5.4 points over 2002. To curb mold losses resulting from water
damage, we have instituted exclusions or sublimits for mold and are
charging higher premiums for mold coverage.
Chubb also benefited in 2003 from a turnaround in our overseas
operations. Canada had a record year, and our Asia Pacific and Latin
American operations were profitable. Most importantly, Chubb Europe
enjoyed a dramatic turnaround, becoming a substantial profit contributor
after five successive years of underwriting losses.
Early in 2003, we decided to exit the personal lines business on the
Continent and concentrate on our profitable personal lines business in
the United Kingdom and in Ireland. This facilitated a comprehensive
restructuring in Europe, which involved consolidating some branches and
trimming back our work force.
In April 2003, I announced that we would exit the credit derivatives
business of Chubb Financial Solutions (CFS) and run off its portfolio. CFS

consumed capital and management resources that could be more profitably
deployed in our core property and casualty insurance businesses. Over the
past few quarters, we have made great progress in running off the CFS
portfolio. From a peak level of $43 billion as of mid-year 2003, we have
reduced our notional exposure to $21 billion as of the end of February 2004.
The Chubb organization in 2003 demonstrated a new vigor in
execution by maintaining underwriting discipline in a heightened
competitive environment. Everyone in the Chubb organization is now
focused on enhancing the bottom line through disciplined underwriting.
In addition to our underwriting initiatives, we achieved significant
gains in expense management by enhancing our focus on the cost side of
the business. This resulted in substantial productivity increases, as we
reduced the size of our core insurance operations work force by 5% while
continuing to grow the business. Our expense ratio improved to 30.4%
from 31.3% in 2002 and fell below 30% in the fourth quarter.

The financial impact of our emphasis on focus and execution is seen
in our results for 2003. Our combined ratio of 95.5% was 2 percentage
points better than in 2002, excluding A&E charges in both years.
Including the A&E charges, the combined ratio was 98.0% in 2003 and
106.7% in 2002.
During 2003 we also took several actions to reduce risk.
Our gross losses from the September 11, 2001 attack on the World
Trade Center were about $3.2 billion, but because we had more than
$2.5 billion of reinsurance, we announced in 2001 that we estimated our
net cost would be approximately $645 million. As we announced in 2002,
certain of our reinsurers questioned the interpretation and/or application
of some provisions of our property per risk reinsurance agreements as they
related to September 11th claims. Early in 2003, we reached agreements
with all of our reinsurers, enabling us to reaffirm our net cost estimate of
$645 million.
Regarding CFS, we reached an agreement that replaces our
$3.9 billion of notional exposure on two troubled credit default swaps
with a new principal and interest guarantee that caps our exposure at
$500 million, while also postponing by several years the dates of any
potential payments we might have to make. This agreement entailed a
net cost of $96 million, reducing our 2003 earnings per share by 34 cents.
The substantial progress in our core businesses in 2003 was partly
offset by the cost of strengthening our asbestos reserves for the second
consecutive year. When we took $700 million of asbestos and
environmental reserve strengthening charges in the second half of 2002,
we thought we had put the problem behind us. After a review in late 2003

of our reserve needs based on the most recent data available, we
strengthened our asbestos reserves by $250 million, reducing our operating
income per share by 90 cents.
During the year, we strengthened our capital position by completing
two successful offerings totaling $1.8 billion of equity and debt, bringing
our 2003 year-end debt-to-total-capital ratio to 24.8%. If we treat the
equity units sold in 2002 and 2003 as equity (as most rating agencies do),
the ratio was 15.5%. We now have the capital we need to take advantage
of growth opportunities over the next several years.
Although insurance rates are increasing at a slower pace than they
were a year ago, they are still increasing. The outlook for 2004 is good,
and were expecting another year of progress and record earnings. We are
determined to translate the value of the Chubb franchise into sustained
profitability and rewards for our shareholders.
I am grateful for the warm welcome and support I have enjoyed
over my first full year at Chubb from employees, customers, producers,
shareholders and our Board of Directors. I am especially grateful to our
lead director, Joel J. Cohen, for his wisdom, support and encouragement
during the period he served as Chairman of the Board.
John D. Finnegan
Chairman, President and Chief Executive Officer
March 5, 2004