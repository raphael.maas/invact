Vornados Funds From Operations for the year ended
December 31, 2003 was $518.2 million, $4.44 per diluted
share, compared to $439.8 million, $3.91 per diluted share,
for the year ended December 31, 2002.
Net Income applicable to common shares for the year ended
December 31, 2003 was $439.9 million, $3.80 per diluted
share, versus $209.7 million, $1.91 per diluted share, for the
previous year. Here are the financial results by segment:

Internally, at budget meetings and such, Joe Macnow
uses as an earnings metric Funds from Operations
Adjusted for Comparability. This metric allows us to focus
on the core business by eliminating certain one-time
items. One-timers are inevitable, can be either good or
bad, but I admit we sometimes have a few too many of
them. The following chart reconciles Funds from
Operations as Reported to Funds from Operations
Adjusted for Comparability:

Mike (1) and I are more than satisfied with Vornados financial
performance in 2003. In a soft office market (our
Company is about 60% office), we benefited greatly from
the fact that our office investments are concentrated
almost entirely in New York and Washington, DC, the two
strongest office markets in the country (and this did not
happen by chance) (2). We also benefited from the fact that
our office rents are still below market (see page 5 of
the financial section of this Annual Report for full leasing
details) and, as a stool gains strength from its multiple
legs, we also benefited from 2003s strong financial performance
of our retail business and Mart business.

In 2003, Vornados Funds From Operations per share
increased by 13.6% and FFO Adjusted for Comparability
per share increased by 6.5%. EBITDA for each of our
business units increased for the year and each business
unit (with the exception of Washington Office (3)) had
handsome same store increasesall this performance in
a challenging market.
It is a testament to Vornados scale and balance sheet
strength that, since January 2003, we generated over
$1 billion internally from the capital side of our business
(not including cash flow from operations). This was accomplished
while maintaining our credit rating (which we are
committed to) and maintaining virtually the same credit
ratios. Here is a list of cash proceeds by transaction:
Financings
$170 million 06/03 Financing of 770 Broadway for 5 years at LIBOR plus 1.05%.
200 million 11/03 Unsecured borrowing for 7 years at 4.75%
75 million 11/03 Distribution from Newkirk MLP funded from a $525 million refinancing
of a $225 million credit facility
150 million 02/04 Refinancing Two Penn Plazas old loan of $150 million at 7.08% with
$300 million for 7 years at 4.97%
135 million 02/04 Our 60% share of the net proceeds of AmeriColds $254 million financing
for 5 years at LIBOR plus 2.95%
Dispositions
$292 million 10/03 Sale of Two Park Avenue, a 965,000 square foot office building, resulting in a gain of $156 million
of which $44 million ($.32 per share) was distributed to shareholders as a capital gains dividend
8 million 01/03 Sale of Baltimore and Hagerstown Shopping Centers resulting in a gain of $4.5 million
11/03
95 million 07/04 Pending sale of the Palisades Residential Apartment Complex (4)
And we could have generated more. Vornado has substantial
unused financial capacity that could be realized either
from borrowings or from recycling existing assets. We have
$10s of millions, and in some instances as much as $100
million or more, of unrealized profits in dozens and dozens
of our assets.
As promised at our September 2003 investor conference,
in February 2004, AmeriCold completed a $254 million
financing, which was the source of Vornados repatriating
$135 million of its investment in AmeriCold. But this
financing did much moreit went a long way to demonstrate
the value of this business. Think about it
AmeriCold now carries $800 million of debt (recourse
solely to various groups of its assets) at a weighted
average interest rate of 6.1%, or about $49 million
of annual interest expense, still leaving FFO of $64 million
from actual cash rents. Mike and I believe that these
facts support a value for this investment that is greater
than most of our investors and analysts carry. 
Vornado has multiple assets that have substantial value, but
are currently earning either no return or very low returns in
relation to their value. Some examples of these are:
 Cash balances, which at year-end were $320 million,
yielding 1%.
 Hotel Pennsylvania, which has been on a roller coaster,
earning EBITDA of $13 million in 1997 when we first
acquired 40% of it, peaking at $27 million in 2000, and
dropping to $4.6 million last year. (6) Mike and I believe
this asset could be sold for in excess of $250 million.
Our to-do list for 2004 includes finalizing a plan for
the Hotel Pennsylvania which may involve a sale,
conversion to apartments or even razing the building
for new construction. Any plan will involve realizing the
sites great retail potential.
 Our FFO in 2003 from the Palisades apartment complex
represented a 1.5% return on the $95 million of proceeds
we will receive when the sale of this asset closes.
 $25 million of Prime Group Inc. common stock for which
we record no income.
The totals are $690 million of capital that earned $9 million
in 2003. As we harvest these values and reinvest proceeds
at normalized returns, our FFO will increase. Our FFO
would also increase if we refinance the $880 million portion
of our preferred shares which are approaching their
call date and are above market. On the flipside, a
100 basis point increase in LIBOR would reduce FFO by
$.08 per share. 
The mission of our business is to create value for our
shareholders by growing our asset base through the
addition of carefully selected properties and by adding
value through intensive and efficient management. As in
past years, Mike and I are pleased to present leasing
statistics for our businesses. In our business, leasing is
what its all about.

Mike and I congratulate David Greenbaum, Mitchell Schear,
Sandeep Mathrani and Chris Kennedy for a superb 2003.
Generally speaking, most times we can only get rents that
the market offers, but each of these franchise players,
through aggressiveness and ability, do a marvelous job of
achieving above-market occupancy, year in and year out.
David Greenbaum reports that in New York activity is
increasing, occupancy rates are rising and rental rates
have stabilized. David, ever measured, continues to be
cautious and aggressively defensivefocusing on leasing.
Sandeep Mathrani and team give us superb skills in developing,
leasing, and managing strip shopping centers, malls
and New York street retail. After all, these retail businesses
are our heritage. We continue to seek retail investment
opportunities. In December, we acquired the Bergen Mall
for $145 million, a 900,000 square foot fixer-upper on 80
acres in Paramus, New Jersey. We plan to redevelop this
property into a lifestyle center and will invest at least an
additional $100 million. We have owned Green Acres, a
super regional mall, in Valley Stream, Long Island, New
York for six years. Sandeep is now working hard on plans
for a renovation and expansion of this productive property.
Chris Kennedys Mart team is also a leasing machine. Chris
keeps our showroom space at a very respectable 95%
occupancy and in 2003 leased 1,157,000 square feet of
showroom space in 465 separate transactions. Also
in 2003, our trade show personnel rented thousands of
booths to exhibitors in 29 trade shows. Importantly,
Chris is also working on several large office leasing deals
for our Chicago assets.

Capital Expenditures
Managing capital expenditures is an important part of our business. The table below presents capital expenditures*
since 2000, in dollars on a segment basis.
Since leasing activity varies year-to-year and the term of leases fluctuate, we think per square foot numbers tell a
meaningful story. In the table below, the Expenditures to Maintain Assets are presented based on total square feet in
the segment. Tenant Improvements & Leasing Commissions are presented based on square feet leased in the year on
a per annum basis based upon the average term of leases.

Washington Office
Vornados Charles E. Smith Division is the largest owner of
office space in the Washington, DC area and is the largest
landlord to the Federal Government. Vornado made minority
investments in Smith in 1997, and then again in 1999,
and acquired the remainder on January 1, 2002, so that
we now own 100%. Smith today owns 14.0 million square
feet, about half of which is located in 26 buildings in
Crystal City, Arlington, VA, adjacent to Reagan National
Airport, overlooking the Capitol. (8)
Mitchell Schear has now completed his first full year as
President of Charles E. Smith. He has finished the
integration of Smith and Kaempfer and is fully focused
on leasingour Washington office team leased 2,848,000
square feet in 2003. He and David have become quite a
team. Mike and I are delighted.
When we acquired Smith our underwriting accounted for
the fact that its largest tenant, the Patent and Trademark
Office (PTO), would be relocating in late 2004 and 2005,
vacating approximately 1.9 million square feet in Crystal
City. Our underwriting accounted for this both in terms of
price (9)we acquired the portfolio at an above market
10.3% cap rateand by mentally allocating $75 million for
capital expenditures to re-lease these buildings.
The table below shows the move-out schedule of PTO.

Our current financial model of the effect on Vornados FFO
of PTOs move-outs and our forecasted subsequent leaseup
is based upon (i) an increase in average straight-lined
escalated rent from the current $26.61 per square foot to
replacement straight-lined rent of $33.50 per square foot,
(ii) one year of downtime with a corresponding reduction
in variable expenses, and (iii) taking 901,000 square feet
out of service for 918 months including capitalizing applicable
costs.
To summarize:
 PTO is moving out of 1,939,000 square feet over the next
couple of years. (Recently, the PTO advised us they
intend to retain approximately 200,000 square feet
through at least 2007.)
 We will take 901,000 square feet in the four oldest buildings
out of service for modernization that will take 9-18
months. Our capital budget for this is $122 million ($135
per square foot). Our capital budget for the remaining
space is approximately $30 million. (10)

 We forecast FFO to decline as PTO vacates and then
rebound as we lease-up as follows.

We believe our business is certainly large enough and
strong enough to absorb the PTO blip in 20042006 without
missing a beat.
At the end of the day, we forecast an increase in FFO of
$13.5 million from the PTO spacethe result of replacing
$26.61 per square foot rents with $33.50 per square foot
rents. Looking at it another way, we expect about a 9%
return on the incremental capital invested.

Real Estate Lending
We like the real estate lending business. From time to
time, we provide real estate financing on assets that we
understand as well as the assets we own.
Mike and I twice declined to buy New Yorks General
Motors Building for about $800 million, in 1998 and again
in 2000. In September 2003, the General Motors Building
sold for $1.4 billion to a private New York investor. We bid
$1.2 billion. This time around, we participated in the
financing of this deal by lending $225 million of mezzanine
debt (11) at an average rate of LIBOR plus 8.833% subordinate
to $900 million of senior debt. (See Note 3 to the
Financial Statements for the full details of this investment.)
Recently, the borrower began discussions with us about
prepaying/recasting a portion of the loan.
Alexanders
This year Alexanders deserves its own section. And this
year we intend, in conjunction with Alexanders Board of
Directors, to determine the end game for this investment
which may include selling it, or simply leaving it to seek its
natural value as a free standing, separately traded REIT, as
it now is, or other options.
Vornado owns 1,654,000 shares of Alexanders, Inc., a
33% stake in this NYSE-listed REIT. The shares, which
had a value of $65 on January 1, 2003 are trading now at
about $150, making Vornados equity investment worth
$248 million at market (12) which, when taken together with
Vornados $124 million loan to Alexanders, represents a
total investment of $372 million.
Alexanders has no corporate level employees. Vornado
serves as its for-fee external manager, leasing agent,
developer, etc.
Alexanders was a New York area department store chain
which was rich in real estate, but long suffering as a retail
operation. Alexanders is now a real estate business with a
small, static collection of wonderful assets:
 Bloomberg Tower/ One Beacon Court mixed-use
development at 59th Street and Lexington Avenue, NY
 Kings Plaza Regional Shopping Center, Brooklyn, NY
 Rego Park Shopping Center, Queens, NY
 Ikea Property, Paramus, NJ
Please see page 22 for the full property table.
The development of the 59th Street propertya full
square block on the Upper East Side of Manhattanhas
been a long, wonderful adventure. It is situated where
Manhattans Plaza District meets the Silk Stocking District,
blocks from the best hotels in the world and, of course,
directly across the street from Bloomingdales. This 814-
foot tower is now topped out, curtain wall is nearly complete
and tenants Bloomberg, Home Depot and H&M are
doing fit-out.
On behalf of Alexanders, we did some things right here.
 We had the patience to hold this great site for years
until the New York market recovered. I think we got the
timing right.
 David and Mike made a great 700,000 square foot anchor
lease with Bloomberg. (13)
 We were able to internally finance the cost of this project
(about $650 million of hard and soft costs without land)
without diluting shareholders by selling stock or taking in
partners. We generated the needed capital by leasing and
financing Paramus, refinancing Kings Plaza, and the debt
from a Hypo Bank construction loan arranged by Wendy
Silverstein, our capital markets queen.
 Alexanders hired Vornado as a for-fee developer, again
without suffering any dilution. (14)
 We engaged Cesar Pelli as design architect. (15) Creating a
great building rather than a developers building has
paid off in spades.
 The buildings grand gesture is an 11,000 square foot,
open-to-the-sky, mid-block courtyardan off street
refuge available for both vehicles and pedestrians that
serves as the entrance for residential tenants and
Bloomberg. This six-story high, oval-shaped space is
unique and quite wonderful from a design perspective,
and quite wonderfully profitable too. (16)
 With a full block in Bloomingdales country, the retail value
was obvious. We discarded the idea of building a multistory
interior mall and chose instead to have all stores

front on the wide avenues. We expanded the site by
building two, twenty foot high below-grade floors (nine
foot basements would not have worked), one of which
Sandeep leased to The Home Depot and the other was
leased to Bloomberg.
 The office component makes money, and also serves as a
pedestal 475 feet in the air on which sits the 23 story apartment
component, creating towering views and terrific value.
In February 2004, Alexanders completed a $400 million
ten-year financing of the office portion of the 59th Street
project, at a 5.33% interest rate. Thanks again Wendy.
Last years annual letter had a section entitled The Anti-
Reality Show Called Accounting. Heres another interesting
accounting quirk. Alexanders has outstanding 100,000
stock options and 850,000 stock appreciation rights (SARs)
at an average strike price of about $72. Accounting requires
that these SARs be marked-to-market each quarter. Lets
look at the effect. Alexanders trading price has been
bouncing around, but lets assume a price of $150 per
share, a price that exceeds the strike price by about $78.
Therefore, Alexanders earnings has or will be charged
about $66 million and Vornados share of this charge is
about $22 millionall this at the same time Vornado is
enjoying a $129 million mark-to-market increase in the value
of its Alexanders investment, which under todays accounting
is not reflected in earningsstrange.
Corporate Governance
It is our foremost priority to conduct our business with
the highest level of ethics and corporate responsibility
and we do. Our trustees refined sense of right and wrong,
knowledge of our business, questioning nature, devotion to
and ownership stake in Vornado are the foundation of our
governance.
We have now completed codification of our Corporate
Governance Program, putting us in compliance with the
applicable securities laws and stock exchange requirements.
This program included the adoption/amendment of charters,
guidelines and codes which are all available on our
website. In addition, our Board has determined that five
existing trustees, constituting a majority, are independent.
Our Corporate Governance Committee and the Board
have the following observations:
 Six of our nine Trustees have a nine-figure investment in
Vornado and a seventh has an eight-figure investment
extraordinary. And, believe me, these Trustees really care
about their investment.
 Six of our nine Trustees are life-long real estate professionals,
which is very helpful. But, it should be noted that
the Board believes that it would benefit from the addition
of one or more generalist members.
 Our Board believes it would benefit from a dose of youth.
 Our Board thinks that it would be better to increase the
current five to four ratio of independent to non-inde-
pendent Trustees by adding one or two independent
Trustees, and we will.
 For the moment, our Board has determined to maintain
its classified structure and that it is okay for me to serve
as Chairman and CEO. 

State of the REIT Market

REIT stocks have outperformed the S&P and almost all
other financial indices over every measuring period for the
last ten years. (18) Vornados stock has also done well. The
following table presents the data.


In real estate, the private market has driven cap rates (19)
down to the 5s and 6s, leading the public market. Cap
rates are stickythey may bounce a bit, but I believe they
will stay lower for longer than people seem to think. We
wont see 8% cap rates again for better properties for
years. This swing to lower cap rates is a secular phenomenon
that will survive a bounce in interest rates.
Its obvious that for a long time now we have been in a
worldwide period of easy money. The universal consensus
is that America is recovering, inflation is in the wind and
rates are going up. I guess I sort of agree, but I still feel
that easy money is a secular phenomenon. In any event, in
the current environment, we will run the business defensively
with respect to rates.
As I write this letter, REIT stocks are bouncing. The logic
seems to be that stronger jobs reports point to a stronger
economy, which points to higher interest rates, which
points to declining real estate values. I dont get it.
History clearly shows that a dose of inflation and growth
has been good to great for rents and the value of existing
real estate. And while I dont wish for it, higher rates, if
they were to happen, will take out new-builds as competition
for our in-place real estate. My guess is that REIT
stocks just got a little ahead of themselves.
Mike and I think that our great assets, financial might,
brand recognition, and deal flow are important to our
future success. While we have become a large company,
we believe we still can achieve a good rate of growth.
We continue to think that simplicity is a virtue and on-thefairway
investments (mainstream) are our franchise.
Perhaps we should have been even more aggressive over
the past couple of yearswe will see. In the current
environment, while it is difficult to buy assets, we have and
we will continue to find our opportunities. And, our best
business today seems to be working our assets while we
watch the market mark them up.

People
Mike and I appreciate and value Vornados human capital
as highly as our financial capital. Starting with the eight
Executive Vice Presidents, who are division heads and our
direct reports, and extending to the 108 Vice Presidents,
who are so essential to the success of our businesses,
they have our praise and thanks.
We welcome Alan Rice, a talented attorney as Corporation
Counsel and Secretary. We also welcome Cathy Creswell,
Director of Investor Relations, a talented professional well
known to many of our investors, who is helping our efforts
to continually improve communications and accessibility.
This year Paul Larner, EVP of Administration, resigned to
pursue his private entrepreneurial instincts and Ken
McVearry, long time EVP of Smith, left to pursue other
interests. Paul will not be replaced, and Mitchell Schear,
President of Smith, has assumed Kens duties.
As announced in February 2004, Alec Covington,
President and CEO of AmeriCold, resigned to pursue an
opportunity with a food distribution company. Alec has left
us with a stable business and a sound organization. We
wish him well. Mike OConnell, who has been with
AmeriCold for over ten years, has been promoted to be in
charge of all warehouse operations and, until a successor
is in place, will report to Anthony Cossentino, Chief
Financial Officer.
After 34 years, Ralph Richards, a super-duper retail leasing
representative and a nice man, has retired. Thanks and
Godspeed. Michelle is still always right, Clarice is some
fine painter, and Sherri had a baby boy.
An important part of our business is keeping our properties
very well-maintained, which involves periodic remodels
and freshening, as needed. Long-time readers of
Vornados annual report may notice that this year we have
freshened and updated this reports presentationbut,
be assured, our culture will remain the same. Similarly, we
are in the process of freshening, updating and improving
our website.
Mike and I deeply believe in the future of Vornado. We
thank our colleagues and shareholders.



