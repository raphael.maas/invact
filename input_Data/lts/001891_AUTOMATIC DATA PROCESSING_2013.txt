IN ADPS FISCAL 2013, we grew in multiple top- and
bottom-line financial measures while working through
an improving but still challenging global economy. We
introduced innovative products and services that helped
to improve our competitiveness and drive new business
bookings. We also continued to help existing clients
migrate to new, easier to use platforms while helping
them deal with complex regulatory changes.

But there is something else happening that is just as important. This past year, our belief in our strategic direction
to become a leading human capital management (HCM) company was validated in many ways. In conversations with
our clients, I hear a common theme  that human capital is a companys biggest area of investment, opportunity and
concern. They say its ADPs expertise, service and technology that enables them to ensure a more engaged workforce
and offers the assurance of support for the many changes and HCM challenges they face.
We have seen the adoption of our integrated HCM solutions by more than 40,000 clients. And we have been helping
clients prepare to comply with the United States Affordable Care Act, with resources needed to manage various
aspects of this complex and rapidly evolving law provided by our integrated HCM solutions.
Our strategy, delivered through an approach focused on simplification, innovation and growth, is the key to continuing
ADPs success as a company  and continuing to reward you for your investment. This letter provides a closer look at
each of these three objectives.
GROWTH
Two years ago we made the strategic decision to concentrate on integrated, cloud-based HCM solutions, with a global
market that we estimated at $90 billion. The strategy is paying off as we perform well both for clients and against
competitors.
Our fiscal 2013 results continue to reflect the strength of our underlying business model, including the diversity of our
client base and products, against the uneven global economic recovery. We saw continued strength in our key metrics,
solid revenue growth, and continued improvements across our business segments:
 As stated in our Aug. 1, 2013 earnings release, we grew revenues 7% over fiscal 2012, with 6% of this growth
organic, to $11.31 billion. This included revenue growth in Employer Services (up 7%, 6% organic), Professional
Employer Organization (PEO) Services (up 11%, all organic), and Dealer Services (up 9%, 8% organic).
 We achieved strong new business bookings growth for Employer Services and PEO Services of 11% over last year
to $1.35 billion, a new high. We also achieved record client revenue retention of 91.3% (up 0.4 percentage points).
This combination of sales success and client loyalty demonstrates the strength of both our products and services.
 Diluted earnings per share, after adjusting for a fiscal 2013 pretax non-cash goodwill impairment charge and a
fiscal 2012 pretax gain on a sale of assets, grew 6% to $2.89.
These financial results are thanks in part to an increase in market demand for newer solutions, leading to growth in
our client count to about 620,000.

Existing clients demonstrated an eagerness to upgrade to newer platforms, allowing our migration strategy to
gain momentum. Our ADP Vantage HCM (for large companies), ADP Workforce Now (midsize), GlobalView
(multinationals), and RUN Powered by ADP (small) solutions all saw solid gains in revenues and new bookings.
For illustration of the pace of adoption, we doubled the number of clients on ADP Workforce Now, from 20,000 to
40,000, in only 17 months, from November 2011 to April 2013. ADP Vantage HCM, which has been generally available
for just over a year, also grew rapidly and exited the fiscal year with tremendous momentum.
We have paired our innovative platforms with our unique expertise relevant to the Affordable Care Act (ACA) in the
U.S. to present employers with a compelling value proposition. The ACA requires businesses to comply with reporting
requirements on employment levels, salaries, hours worked and more. ADP has been consulting with clients on these
issues on a one-to-one basis while creating tools to help facilitate their compliance with various requirements of the
ACA. This combination helped us close numerous deals and achieve strong client retention. The strong market move to
integrated HCM platforms, combined with ACA-related activity, has created a larger market for ADP to address.
Our organic growth demonstrated that our products and services are well regarded, and that our sales force is
talented and well equipped to capitalize on evolving market conditions. Our sales force is also increasing its efficiency
to make an even greater contribution to our profitability. We will continue to make strategic acquisitions, such as our
acquisition of Payroll S.A. in June 2013, but our continued ability to grow organically puts us in the best position to
achieve our overall growth objectives.
The Payroll S.A. acquisition increased our outsourcing, payroll and employee benefits services presence in the highgrowth
Latin America region. One of ADPs strongest opportunities for growth is further expansion of our services
across the globe. With roughly 80 percent of our revenues coming from the U.S., we have huge upside potential
internationally. We made excellent strides in this direction in fiscal 2013, growing both single-country and multinational
bookings with our ADP Streamline and GlobalView offers, respectively.
Employer Services and Dealer Services now conduct business in 125 countries in the aggregate. We offer our cloudbased
HCM technology in 90 countries. ADPs value proposition for companies outside the U.S. is only getting stronger.
Dealer Services helped ADPs growth, in both revenue and earnings, aided in part by a strong North American
auto market. Our digital capabilities have received a favorable reception in the marketplace, with more opportunity
remaining globally.
ADP ServiceEdgeSM has quickly gained popularity throughout the automotive industry since its launch in July 2012,
changing the way dealerships approach their Service Retail Workflow. With a single service workflow tool integrating
multiple dealership departments, dealers are saving time, increasing productivity, and keeping their customers
satisfied.
ADP is also applying improved analytic capabilities through Lot Management, a new retail solution for automotive
dealers that helps them effectively manage their used vehicle inventories, an increasingly important area.
INNOVATION
ADP is a technology-enabled service company. Service is a key differentiator. Our ultimate success depends on our
ability to serve our clients and help them maximize the contributions and productivity of their employee bases. There
is no doubt, however, that our technology enablement has become more critical to ADPs future as new techfocused
companies compete with us.
This is why we are happy to see the strong market acceptance of our HCM platforms, which give employers integrated
solutions to simplify the service experience along with sophisticated workforce management tools for recruiting, talent
management, analytics, and even document management.
Mobile capabilities are an important element of HCM, so we were proud to see continued rapid adoption of ADPs
mobile solutions. In June, we announced the 1 millionth download of our mobile app, which allows workers to access
their pay, benefits, and other HR information, wherever they are.
The RUN Powered by ADP payroll platform for small businesses has been a mobile hit, exceeding the 250,000 client
mark far ahead of our initial projections. One reason for its popularity is another ADP innovation: live, 24/7 client
service that accommodates the dynamic and busy schedules of small business owners.
Our Added Value Services (AVS) team developed an exciting new solution, ADP SmartComplianceSM, to help clients with
compliance issues. AVS also secured over $1.4 billion in tax credits for clients in fiscal 2013.
SIMPLIFICATION
Developments in technologies for consumers over the past couple of decades have been remarkable not just for
their innovations in power and capabilities, but also for their ease of use. These developments have easily outpaced
business technologies in this regard. We are working hard to close the gap.
Our new platforms make processes easier for employers and their employees at all levels. We make it possible for a
visiting nurse to punch in and punch out on a smartphone, from the patients front porch. We make it possible for a
small business owner to run payroll while sitting in a golf cart. We make it possible for a department manager to rate
and rank her team with an interface that has the interactivity and engagement of an online game.
We created a tablet application that contains the entire sales process, allowing a prospective client to get back to
business faster while giving the salesperson a more effective sales process and more time to spend with clients. It
even allows a prospective client to sign a contract on the tablet screen electronically.
Were also working to simplify legacy processes  for instance, moving formerly paper-intensive tasks online. This
saves clients time, decreases our carbon footprint, and saves us money.
Simplification also means moving clients from legacy products to our strategic modern platforms and, where possible,
reducing the number of overlapping or redundant solutions.
Simplification will only become more important as technology and competition continue to develop. We are determined
to stay on top of both.
MANAGEMEMENT AND BOARD OF DIRECTORS
John Ayala was appointed Corporate Vice President, Client Experience and Continuous Improvement, with
responsibility for leading business process improvement efforts to transform the client experience and drive our
simplification objectives.
Mark Benjamin, previously the President of our Employer Services International (ESI) business, took on the newly
created role of President, Global Enterprise Solutions. We created this new organization by combining ESI with our
large-account North American businesses.
Doug Politi, an ADP veteran of 20-plus years, was named President, Added Value Services, which is taking an
increasingly central role in helping businesses comply with the ACA.
Jan Siegmund, previously President, Added Value Services and Chief Strategy Officer, was appointed Chief Financial
Officer.
Joe Timko joined ADP as Chief Strategy Officer, bringing us a wealth of strategy and management experience from
consulting and a broad array of technology companies.
There were no changes to the Board of Directors in fiscal 2013. However, in August, after the reporting period, Enrique
Salem announced that he intends not to stand for re-election to the Board of Directors at the end of his current term
after three years as an ADP Board Member. I would like to thank Enrique for his leadership and contributions to ADP.
And while he no longer was serving an active role with ADP, the passing of former CEO Senator Frank Lautenberg was
a loss felt by all ADP associates, past and present. There have been only six CEOs in ADPs 64-year history, and I have


been fortunate to receive the advice and counsel of all of them. This leadership continuum is part of what makes ADP
such a solid company. Franks legacy at ADP is our strong sales team and our sharp focus on values and ethics. He
will be missed, but his imprint remains.
COMMITMENT TO DIVERSITY AND ASSOCIATE DEVELOPMENT
ADP believes that promoting, cultivating, and developing a diverse workforce is critical to delivering superior client
value and financial results. Its so important to us that weve made it a part of our core values (Each Person Counts),
and we are pleased to have received a number of awards recognizing our progress in this area in fiscal 2013, including:
 DiversityInc magazine honored ADP in its Top 50 Companies for Diversity for the second year in a row.
 Training Magazine ranked ADP as a top company in its Training Top 125 list for the third year in a row.
 The Human Rights Campaign Foundation named ADP one of its Best Places to Work for Lesbian, Gay, Bisexual
and Transgender Equality for the fourth year in a row.
CAPITAL STRUCTURE
ADPs financial strength also continues to be a market differentiator. ADP remains one of only four publicly traded
U.S. companies rated AAA by the two leading credit rating agencies, reflecting the strength of our business model and
of our balance sheet. To our clients, this means the highest level of financial soundness in their payroll and money
movement partner.
ADP remains committed to shareholder friendly actions and returning excess cash to shareholders. In addition
to ongoing investments in our core business, our priorities for the use of cash remain tuck-in acquisitions that
complement our existing solution set or expand our geographic footprint, followed by dividends and share buybacks.
In fiscal 2013, we increased our cash dividend by 10 percent, the 38th consecutive year of dividend increases, paying
out $806 million in cash dividends. We also repurchased $647 million of ADP stock.
The return of excess cash through dividends and, when market conditions are favorable, share buybacks, are important
elements of our commitment to driving strong Total Shareholder Return (TSR). These actions, combined with our
revenue growth and margin improvement, are helping us move closer to our goal of driving TSR in the top quartile of
publicly traded U.S. companies over the long term.
GROWTH OPPORTUNITY AND OUTLOOK
As indicated in the beginning of this letter, ADP is firmly focused on growth. At the time of this writing, some economic
indicators in the U.S. are moving in our favor, and we are seeing some encouraging signs in other countries and
regions.
I and the ADP management team believe that the markets we are pursuing provide excellent potential, which led us
to our previously disclosed guidance of 7% revenue growth and 8% to 10% earnings per share growth in fiscal 2014.
Numerous factors could affect that  including employment levels and interest rates  but if ADP associates continue
to execute on our strategy as well as they did in fiscal 2013, our targets should be achievable.
CARLOS RODRIGUEZ
President and Chief Executive Officer
September 26, 2013