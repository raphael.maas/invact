To Our Stockholders,
It has been 18 months since Agilent announced it would spin-off its electronic measurement business into a
new company. The intention of the spin-off was to create two great companies with increased strategic and
management focus, each well-positioned for growth in their respective markets.
On November 1, 2014, Keysight Technologies became an independent company and closed the books on a
successful year. It was quite a year. As you read on, you will gain some insight into how we launched a
$2.9 billion start-up.
Launching Keysight
On January 7, 2014, although still a business within Agilent, we attached a tangible identity to our new
electronic measurement company by announcing its name as Keysight Technologies. Our name conveys the
ability to see what others cannot. It means we have the critical insight to understand and unlock the
answers to a rapidly and ever-changing technology landscape. Our initial tagline of ��unlocking
measurement insights for 75 years�� acknowledges our roots in the original Hewlett-Packard Company
from which our company was born.
The Keysight name is meant to represent the value that we bring to our customers. With our industryleading measurement insights, engineers accelerate their understanding of new technology and their
successes�whether it is being first to market, increasing differentiation, ramping production, or achieving
lower cost of test. We exist to unlock key insights for our customers, and as a result, we create value for our
stockholders.
As you can imagine, it takes work and focus to launch a new company while continuing to meet our
commitments to customers and stockholders. We created a global team to separate our business and to
bring our customers smoothly along this journey. We partnered with our customer base of more than
10,000 to transition their business from Agilent to Keysight without impacting order rates, shipments or
satisfaction.
On November 1, we became an independent company, and the separation went exceptionally well. Two
days later, on Monday, November 3, a small group of employees representing all those who did such an
outstanding job of launching our company joined me on the podium of the New York Stock Exchange.
Together, we rang the opening bell as Keysight Technologies stock began to trade on the NYSE under the
ticker symbol ��KEYS.�� It was a proud moment for all 9,500 of us.
2014 Results and Operations
Even while separating from Agilent, our key focus was on running the day-to-day operations of our global
business. Here�s a recap of 2014:
For the full year, as Agilent�s Electronic Measurement Group, Keysight generated revenues of
$2.93 billion, growing 2 percent on both an absolute and core basis, with an operating margin of
19.1 percent. With only modest topline growth in FY14, Keysight generated $559 million in total operating
profit, while investing 12.2 percent of revenue in research and development.
For the year, our Aerospace Defense business was down 4 percent, marked by a distinct contrast between
the halves, and largely driven by the U.S. market. In the first half, Aerospace Defense was down 18 percent
as it took time for the U.S. budget approvals to work their way through the government and prime
contractors, and for spending to ramp. As we expected, the second half of the year improved and our
Aerospace Defense revenues grew 11 percent year-over-year as a more typical seasonal demand returned.

Communications revenues grew 3 percent year-over-year with wireless manufacturing growth driven by
strength in base station and component test. Industrial, Computers, and Semiconductor revenues grew
4 percent year-over-year with growth trends balanced across these end markets.
Turning to our products, in FY14, we reached several milestones for our key product categories. As an
example of our ability to drive growth when we focus our efforts over time, we achieved record revenue for
our oscilloscope product line. Our success in oscilloscopes is a result of a multi-year effort to invest in, and
grow that business. And while we have been technology leaders in hardware for 75 years, in 2014, our
annual software sales surpassed $300 million.
2014 was a great year for Keysight. And we are just getting started. As an independent public company, we
are focused�from our front-line employees to our executive team�on creating long-term value for our
stockholders.
Strategy
We enable our customers to bring leading electronic products to market faster and at a lower cost. Our key
differentiation is that we provide value to our customers across their entire electronic product
development cycle�from design simulation, to prototype test, to volume manufacturing. We deliver this
value with leading technology and world-class support.
Our strategic imperative is to grow the business in order to create long-term shareholder value. We will do
this by leveraging the strength of our business model, which delivers solid profitability and generates
sustainable cash flows throughout the cycle, and by focusing on higher growth market opportunities,
including wireless communications, modular solutions, and software. In all these areas, the ongoing
evolution of technology creates demand for new measurement contributions and insights from Keysight.
I firmly believe that our focus to maintain strong profitability while increasing our growth rate, and our
innate drive to innovate, will deliver long-term value for both our stockholders and customers.
Thank You
At this point in our journey, I extend my heartfelt thanks to many key people:
� To you, our stockholders for your trust, and funding our commitment to create value.
� To our customers in more than 100 countries. Every day they challenge us to be better, and to help them
bring electronic products to market faster and at a lower cost.
� To our Keysight employees for delivering excellent results while launching a new company. I am grateful
for the many long hours our employees worked, as well as for their families and the sacrifices they made
in support of launching Keysight.
While we deeply thank our past partners from Hewlett Packard and Agilent for bringing us to this pivotal
time, we know that our success is not about the past 75 years, but about our future.
��This is our time�� has been our rallying cry inside Keysight since announcing the decision to launch our
company. This is our time to innovate and grow in electronic measurement, and this is our time to create
value for you.
Ron Nersesian
President and Chief Executive Officer
February 6, 2015