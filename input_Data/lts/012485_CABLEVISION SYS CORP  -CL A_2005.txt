2005 was a remarkable year for Cablevision. We celebrated the companys
best operational year yet, generating more than $5 billion in net revenue for
the first time in our history and recording double-digit adjusted operating
cash flow growth.
The only U.S. cable operator to add basic subscribers in every quarter of 2005, as of this writing, we
have delivered seven consecutive quarters of basic subscriber growth, and we posted record revenuegenerating
unit (RGU) growth this past year. Customers embraced our Optimum Voice service in 2005,
more than doubling the number of voice customers, while the penetration rates of our Optimum
Online high-speed Internet service continued to lead the cable industry.
As Cablevision excelled on the telecommunications front, Madison Square Garden's properties
welcomed more than 6 million people to more than 1,200 events, and Rainbow Media continued to
increase advertising revenue, while securing long-term carriage agreements.
Simply put, 2005 was a year of outstanding execution and very strong results. And 2006 is poised to
follow in its footsteps.
Cablevision declared a special cash dividend of $10
per share in April 2006, payable to all of the companys
shareholders. Going forward, we will continue to pursue
operational excellence and business opportunities that
unlock value for our shareholders.
Cablevision maintained its industry-leading
penetration rates across all of its consumer
video, voice and data services in 2005.
5
Both the companys Optimum Online and Optimum Voice
services were 2005 recipients of PC Magazines
prestigious Readers Choice Award.
5 5
TM
N
Passing 4.5 million homes in the New York metropolitan area,
Cablevision continued to exceed the entertainment, information
and communications demands of the nations largest and most
demographically desirable market in 2005.
Our Telecommunications Services division, which includes the
companys Optimum branded video, high-speed data and
voice services as well as its Optimum Lightpath commercial
telecommunications business, experienced double-digit
growth in net revenue this past year, up 15.5 percent to
$3.6 billion, while adjusted operating cash flow increased
15.1 percent. Meanwhile, RGUs climbed 22 percent to 7.4 million
total combined video, data and voice customers, and the
ongoing advancement of our digital services led to average
monthly revenue per basic video customer of more than
$100 by year-end, the highest in the cable industry.
Our Optimum Voice service experienced phenomenal growth in
2005, adding more than 450,000 new customers to top 730,000.
Named the nations top Voice over Internet Protocol (VoIP)
service by readers of PC Magazine, Optimum Voice offers
13 consumer-friendly voice features, in addition to an innovative
Web portal, whole-house wiring to connect with home security
systems and enhanced 911 access for every customer.
The companys iO: Interactive Optimum digital video service
also continued to win market share in 2005, ending the year
with nearly 2 million customers, up almost 480,000 subscribers
from 2004. The Emmy Award-winning service launched the
interactive real estate channel Optimum Homes to join
classified ad network Optimum Autos in 2005 as it continued to
dominate the industry with 65 percent digital penetration. Also
advancing, our 18 high-definition (HD) services  the most in
the cable industry and offered at no additional cost to our
digital cable customers  finished the year with more than
325,000 customers.
Today, more than half of our video customers also take our
Optimum Online (OOL) service. In 2005, OOL remained far
superior to anything else on the market, ending the year with a
38 percent penetration rate and 1.7 million customers, up
25 percent from year-end 2004. Named the nations top highspeed
Internet service by readers of PC Magazine for the
second consecutive year, OOL offered significant speed
upgrades to all of its customers at no additional cost in
2005. The company also introduced two new
premium tiers of OOL service for residential and
business customers, with downstream speeds of
30 and 50 megabits per second, respectively.
Comprised of Cablevisions complete suite of video, voice and
data services, our Optimum Triple Play offering captivated
customers, creating a new paradigm that our
competitors  and the cable industry  now follow.
Optimum Lightpath also had a strong year, fully shifting from
its traditional communications services to its IP-based,
Ethernet data services, which it markets to medium and largersize
businesses.
Going forward, we have a significant opportunity to serve a
business market that is larger in annual revenue than
our current consumer telecommunications business. By
leveraging the strength of our fully deployed fiber-backed
network and our success in offering highly reliable voice and
high-speed Internet services to residential customers,
Cablevision can establish itself as the provider of choice in this
lucrative business market.
Cablevisions entertainment venues, sports
teams, and local and regional operations
also remain valuable assets with a unique
connection to consumers.
Billboard s "Venue of the Year" for
the sixth consecutive year,
Madison Square Garden
welcomed 4 million people to
437 events in 2005, including
the only U.S. appearance
of Cream; eight sold-out
performances by U2; and
top concerts by The Rolling Stones, Gwen Stefani, Eminem
and the Eagles. Also lighting up the Garden marquee this past
year were the New York Rangers, Knicks and Liberty. Coming
off the 2004-2005 lockout, the Rangers secured a spot in
the 2006 Stanley Cup playoffs. Jaromir Jagr set new team
records for goals and points in a single season, and is the
National Hockey League's MVP frontrunner, while teammate,
Olympic gold medal goalie Henrik Lundqvist is a Rookie of the
Year contender.
As part of a rebuilding effort by management, in 2005, the Knicks
welcomed coaching legend Larry Brown to pilot the teams
youngest squad in more than 26 years. A work in progress, the
popular franchise continued to draw crowds, remaining number
one in NBA ticket revenue for the 14th consecutive year.
Meanwhile, leading its league in sponsor revenue and net ticket
sales in 2005, the Liberty posted a winning record for the eighth
time in its nine-year history.
Across town, Radio City Music Hall hosted a variety of concerts,
events and attractions this past year, including Carole King,
Maroon 5 and Dora The Explorer Live! The Great Stage was also
the backdrop for the Tony Awards and the Daytime Emmy
Awards. Meanwhile, The Radio City Christmas Spectacular,
featuring the world-famous Rockettes, welcomed 2 million people,
with more than 1 million attending the show in New York and
another million seeing it in a record nine markets nationwide.
In September 2005, MSG and the music hall teamed up to host
From The Big Apple to The Big Easy, New York Citys Concerts
for the Gulf Coast. This unprecedented two-venue benefit
featured a host of music greats, including Elton John, Simon &
Garfunkel, Jimmy Buffett, Elvis Costello, Allen
Toussaint and Buckwheat Zydeco, and raised
nearly $9 million for Hurricane Katrina relief.
Cablevisions local and regional sports and news
programming also touched millions of people this
past year. MSG Networks completed a number of
key long-term affiliate agreements in 2005 and
began airing HD telecasts of several away games
for the New York Knicks, Rangers and Islanders,
and the New Jersey Devils. Rainbow Sports
Networks expanded its HD telecasts as well,
The Radio City Christmas Spectacular, featuring the worldfamous
offering HD programming to all of its affiliates in 2005 and
televising 200 of its more than 840 live events this past year in HD.
Viewers first choice for local news according to Nielsen
surveys, the companys News 12 Networks welcomed two new
systems in 2005. Launched in June, News 12 Brooklyn and
News 12 Hudson Valley together added nearly 400,000
subscribers to the now seven-channel network, which
received 11 New York Emmys this past year.
Rainbow Media Holdings LLC also generated headlines this past
year, with affiliate and subscriber growth, and increases in
advertising revenue, household ratings and original program
production. Its national services  AMC, IFC and WE: Womens
Entertainment (WE)  saw net revenue rise 7 percent to
$557 million in 2005, while advertising revenue grew 22 percent.
AMCs investment in movies and original programming yielded
its highest-rated year ever, with an 11 percent rise in total-day
household ratings in 2005. Bringing its mix of popular movies
and original series like Sunday Morning Shootout and Movies
101 to more than 77 million viewing subscribers, this past year,
AMC also produced its first original movie, Broken Trail,
starring Academy Award winner Robert Duvall.
With more than 37 million viewing subscribers, in 2005, IFC TV
unveiled its unique movie rant series, The Henry Rollins Show,
hosted by the former punk rocker. The official home of the IFC
brand, the IFC Center opened in New York City in June 2005 as a
leading exhibition and production facility for independent film.
Meanwhile, IFC Films, IFC Entertainments distribution company,
released 11 films in 2005, including Me and You and Everyone
We Know, which was produced by IFC Productions, opened at
the IFC Center and grossed $4 million at the box office to
become one of the most successful indie releases of the year.
With more than 50 million viewing subscribers, WE posted a
10 percent rise in primetime household ratings in 2005 as it
continued to ramp up original production with hit series such as
Bridezillas and American Princess, which yielded the highest
rating ever for a WE original telecast. At the same time, music
network fuse, the top network for people ages 12 to 34 for the
fourth consecutive year, achieved record ad sales growth as it
shared live performances by artists such as 50 Cent, Coldplay
and Kanye West with its 35 million viewing subscribers.
Mag Rack, Rainbows suite of original on-demand television
programs, is now available to nearly 10 million video-ondemand-
enabled subscribers. Meanwhile, sportskool, the
nations first VOD network dedicated to expert sports and fitness
instruction, saw rapid distribution growth in 2005, adding nearly
12 million viewing subscribers by year-end.
This past year, Rainbow also began marketing its VOOM HD
Networks, the largest, most diverse suite of HD channels
available. In April 2005, VOOM concluded its first major affiliate
deal with Echostar, whose DISH Network now carries all 15 of
VOOMs nonstop, commercial-free channels.
Cablevision is pleased with its prospects for growth. In 2005, we
capitalized on our unique operating efficiencies and resources
to expand and advance our industry-leading products, programs
and entertainment offerings.
Never a company to rest on our laurels, we continue to look for
ways to strengthen Cablevisions competitive position.
Helping us accomplish our mission are our dedicated co-workers,
customers, financial partners and shareholders. Thank you, as
always, for your continued support. You, more than anything, are
the reason Cablevision remains an industry leader, a formidable
competitor and a company focused on delivering the best value
to our customers.