The past year was one of the most eventful and productive in our history. We successfully completed numerous transactions consistent with our strategy
to own highly productive, high quality retail real estate in the best markets and to maintain sufficient financial flexibility to profitably grow our business.
We generated record net income and funds from operations in a challenging economic climate and raised our common stock dividend.
We acquired nine highly productive malls in premier locations and disposed of ?? non-core properties for a net gain of ???? million. We took advantage of the low interest rate environment by refinancing ??.?? billion of maturing mortgage debt at very favorable interest rates and issuing ???? million of six and ten year unsecured notes. We renovated and expanded several properties, increasing their overall attractiveness to our retailers and our shoppers.
In addition, our company was added to the S&P 500 index.

This inclusion is an honor for our Company and heightened the awareness
of Simon Property Group within the investing public. We sold nine million shares of common stock in connection with our inclusion in the S&P at ???.?? per share, generating ???? million of net proceeds.
Performance: 2002 vs. 2001
? Net income increased ???% to ???? million from ???? million. After payment of preferred stock dividends, net income available to common stockholders on a diluted basis rose ???% to ???? million from ???? million or ??.?? per share versus ??.?? per share.
? Diluted funds from operations increased ?% to ??.?? per share from ??.?? per share.
? Year-end occupancy in our mall portfolio increased ?? basis points to ??.?% from ??.?%.
? Average base rents for mall and freestanding stores in our regional mall portfolio increased ?% to ???.?? per square foot from ???.?? per square foot.

This inclusion is an honor for our Company and heightened the awareness
of Simon Property Group within the investing public. We sold nine million shares of common stock in connection with our inclusion in the S&P at ???.?? per share, generating ???? million of net proceeds.
Performance: 2002 vs. 2001
? Net income increased ???% to ???? million from ???? million. After payment of preferred stock dividends, net income available to common stockholders on a diluted basis rose ???% to ???? million from ???? million or ??.?? per share versus ??.?? per share.
? Diluted funds from operations increased ?% to ??.?? per share from ??.?? per share.
? Year-end occupancy in our mall portfolio increased ?? basis points to ??.?% from ??.?%.
? Average base rents for mall and freestanding stores in our regional mall portfolio increased ?% to ???.?? per square foot from ???.?? per square foot.

foot. By year-end ????, our portfolio was ??.?% occupied and generated sales of nearly ???? per square foot.
Ten years ago, we owned a portfolio of primarily middle-market malls; today we own nearly ??% of the best malls in the United States  defined as those malls generating over ???? million in annual sales.
At our IPO, we had a ?? billion total market capitalization; today our investment grade rated company has a total market capitalization in excess of ??? billion and is a member of the S&P 500 Index. We have grown our revenues from ???? million in ???? to ??.? billion in ????, and produced a total shareholder return, including the reinvestment of dividends, of ???% or ??.?% on an annualized basis.
Vision of the Future
Building on our past success, we have found that our properties have additional
revenue potential through the implementation of various branding and marketing programs. Our branding
initiatives capitalize on the value

inherent in the two billion visits that more than ??? million unique shoppers make to Simon malls every year.
Access to this immense, captive, consumer audience has significant revenue potential. We have already experienced early success by providing promotional access to brands that want to interact with our shoppers such as The Coca Cola Company and Visa U.S.A. We are continually adding national consumer brands that choose to extend their marketing message through our malls. Our goal is to make the mall a widely recognized marketing medium.
Revisiting Retailing
We are frequently asked about the impact of the challenging economy on our retailers and the ultimate effect it will have on our results. Our position as the largest landlord to mall-based tenants affords us the opportunity to create unparalleled relationships with our tenants. Many specialty retailers generate a meaningful amount of their top-line sales and profitability from locations in Simon malls. Despite the
softness in the economy and sluggish retail sales, many of Simons key retail partners are experiencing enhanced margins and positive operating results and have maintained stable balance sheets that allow them to grow through the opening of new stores. Many retailers have successfully adjusted their business models to operate more profitably in a low sales growth, low-inflation environment.
This resulted in much lower bankruptcy fallout in ???? than in the prior year. We lost ???,??? square feet of leased space in our mall portfolio as a result of bankruptcies, as compared to more than ?.? million square feet lost in ????. It is too early to gauge what our experience will be in ????, but given the mixed holiday season results and the weak economy, we are prepared for an increase in bankruptcies
and related store closings in ????.
Our ongoing leasing strategy is to replace these weaker tenants with more productive operators, adding to the excitement of our properties. We are encouraged that there continues to
be an emergence of new, vibrant retail concepts that want access to our high quality properties.
Corporate Governance
Over the past year, there has been significant discussion in the public markets regarding the integrity of periodic reporting processes and the involvement of senior executives in those processes. It has always been normal course of business at Simon for all material items and transactions
to be discussed at length among all members of senior management and reviewed with, and approved by, our Board of Directors. One of the hallmarks of our business practices is the ongoing involvement in the financial
reporting processes and detailed review of results by the chief accounting
officer, chief financial officer, chief operating officer and myself.
Our company may have its roots in an entrepreneurial real estate environment,
but I assure you that we operate as a public company, not a private one. We have a very engaged and
knowledgeable Board of Directors and a financially astute Audit Committee.
When we completed our IPO in ????, we sought to provide investors the opportunity to invest in a quality real estate company that was actively managed for the benefit of all shareholders.
That has been and will always be our philosophy.
Conclusion
Our focus for ???? is to improve our operating margins through increases in rental rates on new lease executions, increases in portfolio occupancy and a continued focus on utilizing our economies of scale to drive down operating costs. We will continue our strategy of recycling capital to invest in our core properties  highly productive, high quality retail real estate.
In February of ????, our company was recognized as the top-ranked real estate company in Fortunes ???? List of Americas Most Admired Companies. These rankings were based upon surveys of ??,???
executives, directors, and securities analysts who rated companies within each industry using eight criteria  social responsibility, innovation, long-term investment value, use of corporate assets, employee talent, financial soundness, quality of products/services and quality of management. We are very pleased and honored to receive this recognition.
???? was one of the most active years in the history of our company. I am very proud of our accomplishments
and of our colleagues who made it happen. I would also like to thank our retailers, shoppers, partners, lenders, bondholders, shareholders, and our Board of Directors who all contributed to our strong results. Ours is a business built on long-term, lasting relationships and we are grateful for your continued support.

David Simon
Chief Executive Officer