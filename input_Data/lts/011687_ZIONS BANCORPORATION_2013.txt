To Our Shareholders:
The past year saw a continued strengthening of the foundations of Zions Bancorporations business.
We made significant strides in bolstering our enterprise risk management practices, further building
our capital and the processes by which we determine its adequacy, and further reducing the risk in our
loan and securities portfolios.
Although economic conditions in the western states in which we operate continued to show steady
improvement, the banking environment remains extremely challenging. Industry-wide total loans held
by banks increased only 2.8% during 2013, reflecting a continued reluctance by consumers and smaller
businesses, in particular, to borrow. Despite recent steps to taper the quantitative easing that has
artificially lowered long-term interest rates in recent years, the Federal Reserve Systems monetary
policies remain extremely accommodative. And while the prospect of less aggressive bond purchases
by the Federal Reserve led to moderately higher medium and longer-term interest rates during the year,
short-term rates remain at historically low levels, a phenomenon that continued to pressure our net
interest margin  an important variable in determining our total revenue. And while the heightened regulatory
requirements we are meeting are strengthening our capacity to manage risk and position us to
better weather future challenges, these requirements have become increasingly costly to the company.
Despite these pressures, we continued to exhibit improved financial performance in 2013. Net earnings
applicable to common shareholders increased to $294 million or $1.58 per share, from $179 million or
$0.97 per share in 2012. Our financial results in 2013 nevertheless contain a fair amount of noise 
items that we do not expect to recur as a matter of course in future years. These include $120 million
in pretax expense in conjunction with the redemption of high-cost debt issued during the depths of

the financial crisis, and an elevated level of other than temporary impairment on certain securities we hold,
amounting to $165 million before tax, as we took steps to reduce risk in our securities portfolio. On the other side
of the coin, we experienced the benefit of $104 million before tax from releasing reserves from our allowances
for loan losses and unfunded lending commitments, and a $126 million after-tax benefit from the redemption of
our Series C preferred stock.
As noted, a fundamental challenge we have been grappling with is the very low level of interest rates that has
persisted for the past half decade. The effective overnight federal funds rate at the close of 2013 was 7 basis
points (hundredths of a percentage point), a level that has persisted since the end of 2008. With our average rate
on interest-bearing deposits at a meager 20 basis points at year-end, there is little remaining room to reduce rates
on deposit balances. At the same time, fixed rate term loans made with higher yields several years ago continue
to be replaced at lower yields as they mature or are refinanced, resulting in margin pressure. The combination of
low interest rates and tighter industry-wide credit spreads  the premium over the risk-free rate we receive for
taking credit risk  contributed to a decrease in our net interest margin to 3.36% from 3.57% a year ago.
Margin pressure was offset in part by modest growth in average outstanding balances in our loan portfolio,
which increased $1.1 billion to $38.1 billion in 2013. Loan demand has been sluggish across the industry, and that
was also the case at Zions. However, we saw improved loan growth in the fourth quarter, and were reasonably
optimistic
that we will see somewhat greater loan growth in the coming year. We also experienced continued
strong growth in average noninterest bearing demand deposits, which increased to a record $18.0 billion in 2013
from $16.7 billion the prior year.

While the opportunities for reducing deposit funding costs are limited, we took a number of actions to reduce
the cost of long-term debt and preferred equity over the past year. Much of the debt and preferred equity we
issued during the depths of the financial crisis carried very high interest and dividend rates, but was structured
to provide us the ability to refinance these instruments at lower rates within a reasonably short period of time.
Accordingly, we redeemed all of the 9.5% Series C preferred stock with a book value of $926 million and a par
value of $800 million, financing the redemption with the proceeds from several preferred stock issuances totaling
$800 million and with an average cost of 6.23%. Total preferred dividends decreased to $96 million from $171
million the prior year, primarily as a result of the redemption of the Series D TARP preferred shares in 2012. The
steps we took with respect to preferred stock in 2013 are expected to further reduce preferred dividend levels by
more than $20 million in 2014.
We also redeemed $508 million of various senior and subordinated notes with a weighted average cost of approximately
14.6%, and $285 million of trust preferred securities with a cost of 8.0%; and issued $550 million of both
subordinated and senior notes with an average cost of approximately 5.2%. We have an additional $336 million
(book value) of subordinated and senior debt maturing during 2014 with an average effective cost of 13.6%,
and $202 million (book value) of subordinated debt maturing in 2015 with an average effective cost of 14.6%.
We currently
do not anticipate needing to replace much of this debt, in light of our strong parent company cash
balances. Accordingly, we expect that these upcoming debt maturities, together with the actions weve taken to
redeem a portion of these issues during 2013, will greatly help in reducing the impact of low interest rates on our
margin over the coming two years.
Margin compression has led us to increase our focus on fee income growth in areas such as wealth management,
business credit cards, mortgage banking and treasury management. Were also carefully managing
noninterest
expenses, which, when adjusted for debt extinguishment costs and the provision for unfunded lending
commitments,
increased a modest 1.2% in 2013, despite greatly increased costs for non-discretionary regulatory
and risk management initiatives.
Credit risk has historically been the most material of the risks we manage, so were pleased with the continued
improvement we saw in credit metrics during 2013. Net loan and lease charge-offs were $52 million, a reduction
from $155 million in 2012, and a very modest 14 basis points of average total loans and leases. Nonperforming
lending-related assets as a percentage of total loans, leases and other real estate owned decreased to 1.15%
from 1.96% in 2012. While the total allowance for loan losses decreased almost 17% during the year, it remains
conservative relative to peer levels, and totaled 184% of nonperforming loans at the end of 2013, as compared
to 138% at the end of 2012.
We continued to maintain a highly liquid balance sheet in 2013. A large portion of our liquid assets is held in
accounts at the Federal Reserve. Weve continued to avoid investments such as mortgage-backed securities

that pose the risk of extended maturities at lower-than-market yields during periods of rising interest rates.
Accordingly, we tend to be more asset sensitive than most of our peers, meaning that as rates  and particularly
short-term rates  rise, we would expect to experience greater than average benefit. As intermediate term
interest
rates rise further, we would expect to reduce our asset sensitivity accordingly. However, we have not
felt that the relatively modest interest rate movements experienced to date have warranted a material change
in our posture.
We took actions during 2013 to greatly reduce the risk in our portfolio of collateralized debt obligations, or
CDOs, primarily backed by trust preferred securities issued by numerous community banks. Market conditions
for such investments also improved during the year, resulting in a substantial increase in the fair market values
of these securities. On December 10, 2013, five federal government agencies, including each of the three banking
regulatory agencies, issued a final rule implementing section 619, also known as the Volcker Rule, of the Dodd-
Frank Act. The rule, which was primarily intended to prohibit speculative trading by Wall Street firms with major
trading operations, and to curtail investments by banks in private equity and hedge funds, included language that
would have required banks to divest most all CDOs by mid-2015.
Because the market for such investments had become relatively illiquid following the financial crisis, these
securities fair value has been significantly lower than their book value. Accounting standards require that this
discount be reflected as a reduction in shareholders equity through an offset known as Accumulated Other
Comprehensive Income, predicated upon the representation of our intent and ability to hold the securities until
the fair value equals or exceeds their book value or until maturity. Upon publication of the Volcker Rule, it became
clear that the required divestiture of these securities would preclude our ability to continue to make such a

representation, thus requiring that the unrealized losses be expensed through the Companys income statement.
Accordingly, we filed notice with the Securities and Exchange Commission (the SEC) on December 16, 2013,
indicating that the Volcker Rule would trigger a substantial loss in the fourth quarter.
Many other banks hold such CDOs, and it became apparent that they, too, would be hard-hit by what appears
to have been an unintended consequence of the Volcker Rule, resulting in substantial impairments of regulatory
capital at many banks throughout the nation. The American Bankers Association initiated litigation on behalf of
the industry, and politicians from across the ideological spectrum weighed in, criticizing the impact of the rule
on Americas Main Street banks. Consequently, the regulatory agencies issued a modified interim final rule
repealing the prohibition with regard to ownership by banks of most such CDOs.
We nevertheless concluded that the liquidation of some of the higher-risk tranches of our CDO portfolio would
significantly reduce our risk profile and the significant capital required to be held against such assets while also
allowing for the cancellation of a total return swap that has reduced the risk-weighted capital impact of these
holdings, but that has cost us more than $20 million annually. As a result, we booked a $142 million pretax charge
in the fourth quarter in anticipation of the liquidation of these securities. We successfully concluded the sale of
these securities in February, 2014, realizing a $65 million pretax gain that will be reflected in first quarter 2014
results.
The Volcker Rule was but one of the many challenges we dealt with in the past year related to the greatly
heightened regulatory requirements and expectations arising from the financial crisis, the Dodd-Frank Act and
other legislation. We spent a great deal of time and many millions of dollars this past year developing increased
capabilities to enable us to participate in the Comprehensive Capital Analysis and Review (or CCAR) conducted
by the Federal Reserve in concert with the nations 30 largest banking companies. This was our first round of

participation in this very stringent stress-test exercise, which is run in parallel with Dodd-Frank Act stress tests
for our three largest bank subsidiaries. We built or rebuilt numerous primary and challenger models for virtually
every major component of our balance sheet and income statement, independently validated them, and prepared
extensive documentation of each model. Our board of directors reviewed and approved the many significant
assumptions used in each of the models, as well as the elements in the various stress scenarios we used to
forecast operating income, credit losses and capital positions and requirements for the nine forward-looking
quarters embodied in the stress tests. Because the Volcker Rule was announced in mid-December, as the exercise
was being concluded, substantial last-minute modifications were required to produce the stress test results and
capital plan submitted to the Federal Reserve. With the subsequent modifications to the Volcker Rule, and the
steps weve taken to reduce risk in the securities portfolio, we expect to resubmit a new set of stress tests and
an accompanying capital plan in early 2014.
Many other projects were undertaken to bring the Company into compliance with new and revised regulations
during the year. These included the development of a comprehensive resolution plan, or living will, outlining
in detail the steps that would need to be taken to liquidate the Company in a manner designed to minimize or
eliminate taxpayer exposure in the event of insolvency. We also installed a new mortgage system, centralized our
back-office mortgage operations and dramatically simplified our product set in preparation for the implementation
of new Qualified Mortgage rules that became effective in January, 2014 with respect to residential mortgage
lending. We appointed a new Chief Risk Officer, Ed Schreiber, and a new Chief Credit Officer, Michael Morris.
Together with the rest of our Enterprise Risk Management team, and working with the board of directors and its
Risk Oversight Committee, they prepared a new Risk Appetite Framework, more carefully defining our tolerance
for risk, and engaging employees throughout the enterprise in better understanding their respective roles in
risk management. We also have made extensive revisions to the manner in which we establish and administer
incentive
compensation plans for hundreds of our employees, in accordance with recent guidelines established
by the Federal Reserve.
Very recently, the Federal Reserve Board published its final rule enacting section 165 of the Dodd-Frank Act,
implementing Enhanced Prudential Standards that further raise the bar for enterprise risk management
and governance, including extensive new requirements to measure and manage the firms liquidity. The new
requirements
will require a great deal of additional data collection for daily submission to regulators, and monthly
Comprehensive Liquidity Analysis and Review stress tests that will complement the CCAR exercises. The Office
of the Comptroller of the Currency, or OCC, also recently issued proposed rules elevating requirements for risk
management and governance at larger national banks.
In order to prepare ourselves for new regulatory data requirements, as well as the challenges and opportunities
of the fast-changing digital world in which we live, we embarked on a major initiative, known within the Company

as FutureCore, to replace our major legacy loan and deposit software systems with a modern, integrated system
known as BaNCS. This new software platform will be installed in phases over the next three years, and together
with ancillary projects upgrading the front-end systems used by many of our lenders, and a revised and simplified
chart of accounts, will allow for faster product development, greater customization at the account level and realtime
processing of all loan and deposit transactions. It will also allow us to more efficiently handle the millions
of transactions we process each year.
The regulations and guidelines banks must comply with pose their own risks to the economy and to individual
firms. The availability and cost of credit and reduced rates paid to depositors are among the effects already being
seen. Over the longer term, the industrys ability to attract highly talented and innovative young people to an
industry beset by an increasingly complex thicket of regulation is likely to become an increasing challenge.
While the environment is difficult, we remain very optimistic about the opportunities ahead. We serve some of
the most dynamic markets in the nation  markets that will grow faster than the country as a whole in coming
years. Weve built a strong balance sheet, and are poised to see much stronger earnings in the years ahead as the
economy strengthens and interest rates rise.
I want to express my appreciation to the colleagues and employees with whom I work. They are incredibly dedicated
people, and they have been going the extra mile not only this past year, but for the past several years as
the industry and Zions Bancorporation have gone through an unprecedented amount of challenge and change. I
especially wish to thank your board of directors. They give selflessly of their time, and provide a great deal of
value to our management team and to the Company.
I want to particularly acknowledge R. Don Cash, who has served on the board of directors for 25 years, and will
retire this spring. I also wish to congratulate Scott J. McLean, formerly CEO of Amegy Bank of Texas, in his new
role as Zions Bancorporations President. His contributions to our success have been substantial in recent years,
and I look forward to working with him in his new assignment.
We are most grateful for our many shareholders who recognize the long-term value of our franchise. We appreciate
your continued loyalty and support.

Harris H. Simmons
Chairman and CEO