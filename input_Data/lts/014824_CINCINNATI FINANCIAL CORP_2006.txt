To Our Shareholders:
                                                                                                       reinsurance premiums that we pay. We also


                                                   
                                                            or better and for worse, your company
    
                                                                                    anticipate a combined ratio in the range of
                                                            set many records in 2007. Revenues
    
                                                                                                       97 percent to 99 percent, up from 93.9 percent
                                             rose to an all-time high, including
     
                                                                                                       on a statutory basis in 2006. This target
                                                   record pretax investment income. Record new
     
                                                                                                       considers a number of factors, including
                                                   business premiums boosted total net written

                                                                                                       5.5 percentage points on the combined ratio for
                                                   premiums. Market value appreciation
                                                                                                       catastrophe losses, just as high as in 2006.
                                                   contributed to record book value at
                                                                                                           We are not at all discouraged. We believe
                                                   December 31, 2006. We reported our highest
       

                                                                                                       it's a great time to be in the insurance business.
                                                   ever realized gains on the sale of one large
   
                                                                                                       Through all markets  for better or for worse 
                                                   equity holding, leading to record net income
   
   
                                                                                                       we're going to continue growing ahead of the
                of $930 million, or $5.30 per share.
    
                                                                  overall industry, balancing growth and
                                                       Excluding those substantial investment
    
    
                                                                                                       profitability by careful underwriting and
                                                   gains, operating income declined 11.8 percent
    

                                                                                                       pricing, and identifying new ways to build on
                                                   to $2.82 per share. Lower underwriting profits
                                                                                                       the strengths that give us a competitive edge.
                                                   from property casualty insurance, our main
                                                                                                           Three commitments differentiate your
                                                   business, reflected record catastrophe losses and
                                                                                                       company, making it possible over time to
                                                   record large losses in the $1 million-plus
                                                                                                       increase your shareholders' equity and
                                                   category. In addition, expenses rose more

                                                                                                       shareholder dividends. We measure our
                                                   rapidly than premiums as we continued
                                                                                                       achievements, opportunities and initiatives to
                                                   investing in people and infrastructure, including

                                                                                                       tap those opportunities in terms of their ability
                                                   technology that makes it easier for independent
      




                                                                                                       to help us meet those commitments:
                                                   agents to do business with our company.
                                                       We expect this higher level of catastrophe      Strong Agency Relationships
                                                   losses and expenses may continue in 2007.           We establish strong relationships with the most
                                                            professional independent agencies in each
                                                   Further, price competition is accelerating in our
   
                                                                                                       community, supporting them with a team of field
                 industry, making it harder to achieve premium
                                                                     representatives authorized to make decisions at
                                                   growth and underwriting profitability. While the
    
                                                                                                       the local level.
                                                   industry overall was very profitable in 2006,
                                                                                                           Over the past 10 years, we have selectively
                                                   A.M. Best Co. estimates that total premiums
                                                                                                       added more than 400 highly professional
                                                   may be flat in 2007, and the industry
                                                                                                       agencies. We finished 2006 with 1,289 agency
                                                   combined ratio may rise 3.5 percentage points
                                                                                                       locations marketing our policies in 32 states,
                                                   to 96.8 percent.
                                                                                                       including 55 new agency appointments over the
                                                       Our expectations for our own near-term
                                                                                                       course of the year. As part of our plans for
                                                   results consider these trends. We anticipate a
                                                                                                       2007, we expect to appoint at least 50 more
                                                   2007 growth rate in the low single digits,
                                                                                                       agencies. We are working on plans to enter
                                                   reduced by an increase of $22 million in the




New Mexico and eastern Washington,
appointing our first agencies during 2007.
By the fifth year following an appointment, we
typically earn a prominent position among the
carriers serving that agency, with annual
premiums rising to an average of approximately
$2 million.
    Our agency-centered focus led to healthy
commercial lines growth in 2006, with
net written premiums up 6.7 percent and new
business written by our agencies up
14.9 percent to a record $324 million. While
pricing trended down over the year, agents
appreciate the personal attention our field teams
provide to their accounts. Agents gave us plenty
                                                    
of chances to quote new commercial accounts        
                                                    
and to offer loss control or other value-added
services that made the sale. Additionally, we       homeowner and personal auto policies. With
made commercial renewals easier for                 more competitive rates, our agents found it
policyholders and agents, in many cases, by         easier to again sell Cincinnati service and
extending the policy period under the same          value to their preferred clientele. We don't
terms and conditions. Many policyholders            expect to quickly solve our personal lines
respond positively to such policy extensions,       challenges, but personal lines is moving in the
choosing to keep their coverage with an insurer     right direction. Our pricing is more competitive;
that brings stability to the marketplace, carries   our Web-based processing system is active in
an A++ A.M. Best rating and has high standards      13 states and deploying to several more in
for claims service.                                 2007; and our product portfolio is expanding
    Personal lines net written premiums             with several new or improved coverage
decreased 6.4 percent for the year; however, in     endorsements. Identity Theft Expense and
the second half of the year, retention improved     Advocacy Services Coverage is available now
and new business growth reached 17.6 percent,       in the 25 states where 794 agency locations
rebounding after several quarters of lower          actively market homeowner policies.
new business.                                       Replacement Cost Auto and Personal Auto Plus
    That second half improvement led from our       will be available later this year in the 22 states
July introduction of policy credits that            where we have 772 agency locations actively
incorporate insurance scores into pricing of        marketing personal auto policies.



                                                                                                                       
                                                    In Florida, we requested in February 2007            Our three non-property casualty insurance
                         that our agents hold off on sending us new           subsidiaries also operate primarily to extend the
       
                                                business of any type due to uncertain market         capabilities of our agents to provide full service
       
                                                conditions. We are renewing and servicing            to the families and businesses in their
      
                
                                                policies already on the books, but prefer to take    communities. In 2006, The Cincinnati Life
      

                                                a wait-and-see approach until there is more free     Insurance Company focused mainly on term
              




                                                market competition that supports stable              insurance, introducing a return-of-premium
                          



                                                insurance markets.                                   product series that was well received by agents.
                                                    Over the years, we have increased our            The company's two financial services
                                                share of business from each of the agencies that     subsidiaries continued to successfully leverage
      
   
             markets our policies by offering a full line of      our insurance relationships and broaden
    
   
                                                coverages that meet the needs of their clients.      our offerings. As of December 31, 2006,
   
                                                In recent years, they have indicated a desire to     CFC Investment Company, which offers
    
            offer our products and services to commercial        equipment and vehicle leases and loans,
    
                                                accounts that require the flexibility of excess      reported 2,897 accounts representing
    
                                                and surplus lines. Generally, excess and surplus     $108 million of contract receivables. CinFin
                                                lines insurance carriers provide insurance that is   Capital Management Company, which
                                                unavailable to businesses in the standard            offers asset management services, reported
    
                                                market due to market conditions or due to            $960 million under management in 64 accounts.
    

      
                                                characteristics of the insured risk that are         Together, the CFC Investment and CinFin
      
                                                caused by nature, the insured's history or the       Capital Management Companies contributed
      

                                                nature of their business.                            2 cents to 2006 earnings.
     


                                                    We have studied the option of putting some       Superior Claims Service
                                                of our capital to work by starting a new             Prompt, fair claims service with a human touch
                                                company for this purpose, and we believe it can      proves the value of our insurance programs and
                                                                                                                    validates the agent's decision to make us the
      contribute to our long-term objectives. We have
                                                                                                     carrier of choice for value-oriented clients.
                                                started the process of incorporating a new
                                                                    Again in 2006, policyholders had ample
                                                subsidiary, determining its structure and
    
                                                                                                     opportunities to benefit from the Cincinnati
              forming a team to research and develop
                                                                       relationship. Severe weather in 2006, mainly
                                                appropriate policy terms and conditions, rates
    
                                                                                                     across the Midwest, contributed to $175 million
                                                and underwriting guidelines. While we don't
                                                                                                     of catastrophe losses. As a result, policyholders
                                                anticipate premiums from excess and surplus
                                                                                                     reported approximately 13,000 claims through
                                                lines this year, we do expect our increased
                                                                                                     February 28, 2007. Our field claims
                                                ability to compete for additional commercial
                                                                                                     representatives have closed 95 percent of the
                                                accounts to contribute to long-term growth.
                                                                                                     claims, their prompt responses and personal



approach reflecting positively on our agents.         continued growth in the range of 6.5 percent
    Our claims management system and new              to 7.0 percent.
tools, such as tablet computers, add speed and            Our equity investments also drive the
efficiency to all of our claims processes, and that   company's long-term net worth, financial
was especially beneficial in these catastrophe        flexibility and stability. Your shareholders'
situations. We focus in the following pages of        equity rose to an all-time high of $6.808 billion,
this report on our claims operation, catastrophe      or $39.38 per share, at the end of 2006, an
response and claims technology. Here, we will         increase of 12.9 percent on a per share basis.
simply note that the promotion in May of our          While the sale of our Alltel Corporation
senior claims officer, Jim Benoski, to president      common stock holdings early in the year
and chief operating officer of Cincinnati             accounted for a large portion of the increase in
Financial and president and chief executive           2006 net income, it was not a factor in the
officer of The Cincinnati Insurance Company           increase in book value. That increase was
speaks a thousand words about the centrality of       attributable to the contribution of insurance
our claims operation to our business structure        operations and investment income, along with
and strategy.                                         significant appreciation over the course of the
    Also in May, Chief Financial Officer Ken          year in the rest of the equity portfolio.
Stecher was named executive vice president of             The equity portfolio supports the
Cincinnati Financial and The Cincinnati               accumulation over time of unrealized gains that
Insurance Company, and chairman of the latter.        build book value for shareholders. This cushion
Ken's leadership extends beyond his areas of          of financial strength and flexibility also
responsibility, influencing our company's             benefits agents and policyholders, supporting a
success as he continues to capably oversee the        long-term perspective that leads us to behave
continuous improvement of our financial data,         consistently in the marketplace; make prompt,
transparency and estimates, including reserves        fair claims payments; set adequate reserves; and
for not yet paid claims.                              continue investing in the infrastructure for
                                                      growth. Our insurance strategies and investment
Successful Total Return Investing
                                                      strategies are a good match, and we believe
We use available cash flow to cover
current insurance liabilities by purchasing           their combined results will continue bringing
fixed-maturity securities, then purchase equity       you value in 2007.
securities with the potential to bring us                 During 2006, three independent ratings
increasing dividend income and long-term
                                                      organizations affirmed our high financial
appreciation.
                                                      strength ratings. A.M. Best awards our property
    Our buy-and-hold equity investing
                                                      casualty companies its highest rating,
strategy led to another year of record pretax
                                                      A++ (Superior), assigned to fewer than
net investment income, up 8.4 percent to
                                                      2 percent of insurers. Fitch Ratings awards the
$570 million. Our outlook for 2007 is for


                                                                                                           5
                                          AA (Very Strong) rating to all of our insurance   The board determined that 10 of the current
 
                         companies. Standard & Poor's assigns the          15 members meet the applicable criteria
                     AA- (Very Strong) rating to the insurance         for independence.
                                          companies, and revised its outlook to stable           Early in 2007, the board formalized several
                                          from negative in July. Moody's Investors          current company practices with updates to the
                             



                                          Service maintains an excellent Aa3 rating on      corporate governance guidelines on board
                                          the property casualty insurance companies.        membership criteria, director elections and
                                                                                            stock ownership guidelines for directors and
                                          Working for You
                                                                                            officers. Your company's management and
                                              We look beyond 2006 and 2007 with
                                                             directors purposefully align business decisions
                                          confidence. We remain committed to providing
                                                           with our mission, which includes fulfilling the
       a stable market for our agents' high-quality
                                                                company's obligations to independent agents,
                                          business, underwriting this business carefully
                                                                                            policyholders and shareholders as well as
                                          and producing steady value for our
                                                                                            associates, suppliers and communities we serve.
                                          shareholders, as represented by the board of
                                                                                            We are working diligently to act with integrity
                                          directors' recent decision to increase our
                                                                                            and to assure we will meet those obligations far
                                          2007 indicated annual dividend by 6 percent,
                                                                                            into the future.
                                          which would mark the 47th consecutive year
                                          of increase in that measure. Their action
                                                                                            Respectfully,
                                          reflected our belief that we can achieve
                                          above-industry-average growth in written
                                          premiums and industry-leading profitability
                                          over the long term by building on our proven       John J. Schiff, Jr.    James E. Benoski

                                          strategies: strong agency relationships           John J. Schiff, Jr., CPCU   James E. Benoski
                                                                                            Chairman                    Vice Chairman
                                          supported by local decision making; superior
                                                                                            Chief Executive Officer     President
                                          claims service including solid reserves; and                                  Chief Operating Officer
                                                                                                                        Chief Insurance Officer
                                          total return investing that drives financial
                                          strength.
                                                                                            March 1, 2007
                                              The board also announced director
                                          transitions in 2006. Michael Brown and
                                          John M. Shepherd, current directors, will not
                                          stand for re-election on May 5, 2007, due to
                                          the company's guidelines on director age.
                                          Gregory T. Bier, CPA (Ret.), appointed by the
                                          board in November, will stand for election at
                                          the annual shareholders' meeting this spring.


