Letter to shareholders
SCOREBOARD.
Thats how youre measured in sports, and thats how were measured every day as
a public company. Some might look to a real-time window on their monitor, the
crawler on one of the financial networks or the stock tables of a major newspaper,
where those scoreboards are posted ubiquitously and prominently to remind us the
world is viewingand judgingour performance.
While our success from this standpoint is highly visible, Our Scoreboard focuses on
different numbers:
 Our financial performance for fiscal 2005 demonstrated growth across all lines. Net
revenues for the year increased 37% to $281.1 million compared to $205.2 million in
the prior year. Driving this sales growth was our Mens category with a net sales
increase of 25% to $189.6 million for the year.
 Our Womens and Youth categories were significant contributors. Womens net
sales grew 87% to $53.5 million and net sales in our Youth category grew 48% to
$18.8 million.
We also have another column on Our Scoreboard where we measure our progress
in building a solid foundation for long-term growth. At the top of this column is our successful Initial Public Offering in
November 2005, which enabled us to end the year with little debt and $63 million in cash. This provides us with
additional working capital to fund our Brands future growth while building on our recent accomplishments:
 Weve broadened our product portfolio on either end of the pricing spectrum as we continue to innovate with high
performance lines like our UA Metal and Streaker Running collections. We produced the Tech T, a synthetic performance
piece that looks and feels like cotton, but performs like Under Armour. It was a one million unit program in its first year.
 We launched our first Brand campaign around our womens business. Meanwhile, our PROTECT THIS HOUSE campaign
further entrenched our brand with our core consumers, the team athletes, and our CLICK-CLACK campaign
announced to the world that our performance footwear is on its way in June 2006.
 We signed the next generation of great athletes, like up-and-coming tennis star Robby Ginepri, and most recently, two
top-ten NFL prospects in A.J. Hawk (Ohio State) and Vernon Davis (Maryland). We also signed on to outfit two more
high profile collegiate football programs head-to-toe, including Southeastern Conference powerhouse Auburn
University, and Texas Tech University of the Big 12 Conference.
 Weve strengthened the management team here in the US at the same time we assembled a sales force in our new
European headquarters in Amsterdam, led by one of my original partners, Ryan Wood, and his well balanced team of
Brand, industry and local experts.
 Weve been able to expand our high profile floor space in large sporting goods stores while creating outposts of
specialized Under Armour product throughout the store to tell a better Brand story consistent with our advertising,
product placement, and sports and product marketing. This strategy has allowed us to make inroads into new consumer
segments where our performance fabrics have reinvigorated the norm, especially in categories like Outdoor and Golf.
Ive said many times my job description comes in four parts: (1) Build a Great Product, (2) Tell a Great Story, (3) Provide
Great Service, and (4) Build a Great Team. Lately, however, as I prepare this company for future growth, building a great
team becomes even more critical to our success. That means putting the best peopleand more importantly, the right
peoplein place to accomplish the first three items on the list. We continue to add talent and experience to the
management team, and were dedicated to adding the proper systems, like our recent implementation of SAP, to ensure
that both our people and our systems are best in class.
The scoreboard for 2005 speaks for itself, and as you read the following report, you will see that were positioning
ourselves for the long term as we strive every day to stay committed to our Brand Vision: To be the #1 Performance
Athletic Brand in the World.
Kevin A. Plank
Chairman, Chief Executive Officer and President
Under Armour, Inc. 