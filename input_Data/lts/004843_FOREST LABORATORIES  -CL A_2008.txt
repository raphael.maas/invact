The economic turmoil around us has perhaps one
moral, that the most vulnerable people are those
who try to make money too fast on credit.
Trading incalculable amounts of money, most
of it borrowed, based simply on whether trading
prices are going to go up or down, without regard
to the value of the assets, if there are any at all,
that are the subject of the trade, is quite simply,
reckless gambling.
That is the one simple generalization that characterizes
the economic distress in the investment
community, leading to the downfall of Bear Stearns
and the severe losses of even the survivors,
running at incalculable billions of dollars.
That, in turn, has led to the lack of liquid capital
throughout the system, affecting home owners,
businesses, employees and companies. It has
had less impact on pharmaceutical companies
which, in general, have adequate cash, receive a
part of their income indirectly through government
reimbursement and market products with less flexible
demand.
Within the pharmaceutical industry there are various
business models followed by different companies.
I thought it would be useful in this letter to
discuss what we discuss among our senior management,
and with our board, and perhaps not
often enough with all of our employees, which is
what is our current business model. We have
been on a similar path, in general, for several
years, but challenges and our own solutions have
evolved over the years as we have matured and
as the pharmaceutical business and its environment
has changed.
To begin with, we still acquire our molecules either
by license or purchase, rather than through discovery
or basic research, although we do have several
collaborative discovery projects for innovative
compounds outstanding with companies that primarily
engage in discovery. Those products are
too early to project outcomes. We principally
license or purchase our products because that
route is less risky, and the time to marketing
approval is shorter by years. We do not mind paying
the necessary milestones and royalties in
exchange for years on the market. The products
we acquire run the range from not too long out of
the test tube to products virtually ready for
approval.
There are many, many products within that range
that are available for purchase or license, from
small biotech companies that need a development
and marketing partner, from companies outside the
United States that need partners here to complete

development and to market their products in the
worlds best pharmaceutical market, and also from
major pharmaceutical companies that have decided
to abandon a particular product or therapeutic
area, like Johnson & Johnson, which originally
developed our recently launched vasodilating
beta-blocker Bystolic.
We literally review hundreds of product opportunities
every year. We decide that only a few provide
the product and the terms that are interesting to
us, and we have to vigorously pursue them and
reach satisfactory agreements for them.
Sometimes a product we desire goes elsewhere,
but on the whole we rarely lose an opportunity to a
major pharmaceutical company. Many companies
prefer to deal with us because we are smaller and
quicker, and because their product will not be lost
among a vast portfolio, as with a major pharmaceutical
company.
The process of evaluating opportunities demands
time, experience, sometimes outside help, and ultimately
judgment. It is a two pronged program  a
scientific and a marketing evaluation, and we have
a group in each of those categories that painstakingly
analyzes those products that pass an initial
screening. If the evaluation suggests we should
obtain the opportunity, there is the process of
negotiating final terms and reaching agreement.
Following the initial license agreement, the relationship
with a partner requires continuous management
which is ultimately handled by our
Alliance Management group. Our Life Cycle management
group has the responsibility for coordinating
the strategies for further expanding the utilization
of our products by developing new indications,
new formulations and combinations with other
products.
It is not a simple process and we do not have the
luxury that some big pharma companies have of
splurging on an alluring opportunity where the
return is nevertheless too conjectural. If we
license a product, it has been through an arduous
evaluation process. We are well equipped financially
for very large projects, but we are very serious
about risk. We paid $494,000,000 for Cerexa
(not to be confused with our antidepressant
Celexa) and would pay as much or more for an
appropriate opportunity.
This process has produced a number of important
opportunities for us which is precisely what we
need, because we have major products, Lexapro
and Namenda, whose patents will be expiring early
in the next decade.
Once products are identified and rights obtained,
they have to be developed by our scientific group
so that an NDA can be prepared and submitted to
the FDA for approval. That can be a massive project,
taking years, and requiring great skill and
complicated clinical studies. We have a superb
group that performs all those functions.
Once the drug is approved, and even while it is
being developed and in anticipation of approval,
our marketing group is deeply involved and
already doing the research and developing the
best possible marketing plan. On approval, the
sales force has already been trained. The combination
of our marketing and sales organizations is
preeminent in achieving successful results with
prescribing physicians. Their extraordinary historical
results are based on talent and very hard work,
the traditional means of achieving excellence.

First of our next generation of products is Bystolic,
our unique beta-blocker, launched in January of
this year and licensed from Mylan, which had
obtained the product from Johnson & Johnson.
Beta-blockers have been a standard treatment
for treating patients with hypertension for over fifty
years. And the reason is that what beta-blockers
do is the most obvious way of reducing the risk of
hypertension  simply slowing down the heart so
that it doesnt work so hard. But if you slow down
the heart, you also slow down blood circulation
due to the reduction in cardiac output. And that
reduces your energy, which depends on the flow
of blood to nourish and energize our muscles.
And that is why the conventional beta-blockers
make many patients lethargic and tired. And that
is the reason why many patients do not like to
take beta-blockers.
But Bystolic is different. It does slow down the
heart, but it also dilates the blood vessels, and the
dilation of blood vessels allows for less change in
cardiac blood flow, so that patients do not usually
have the fatigue and lethargy they have with many
other beta-blockers. Dilated blood vessels require
less effort by the heart to maintain adequate blood
flow. Bystolic is having what we consider a very
successful launch, and we expect that within a few
years it will be a major product for us.
Then there are three products from Almirall, the
largest Spanish pharmaceutical company.
The first product, aclidinium, will be available
before 2012, the other two soon after. They are
for the treatment of COPD  chronic obstructive
pulmonary disease, emphysema and chronic
bronchitis, which involve a serious inflammation
of the lungs that can be fatal. The lead product is
aclidinium, a novel anticholinergic bronchodilator
developed by Almirall, and the next two are combination
products with aclidinium.
All are inhaled products administered through a
cutting edge inhalation device developed by
Almirall, called the Novolizor.
Two Phase III studies for aclidinium monotherapy
will be completed this year, with approximately
1,600 patients having been on medication for a
year. The other two aclidinium based combination
products are further behind in development.
If these products are approved and successfully
marketed, we believe their sales could represent a
significant improvement in the treatment of COPD
and a significant contribution to Forests earnings
in the next decade.
And then there is our antibiotic group  ceftaroline
and ceftaroline/NXL-104. These are both intravenous
products for hospital use. Antibiotics are
a new and potentially very important therapeutic
area for us. Ceftaroline is active against many
types of pathogens, including methacillin resistant
staphylococcus aures, known as MRSA, which is
a growing problem and has been responsible in
recent years for a growing number of deaths in
the United States.
We have just announced the conclusion of two
positive Phase III studies for ceftaroline for skin
and skin structure infections, which is the single
largest category of use for intravenous antibiotics.
A second pair of clinical studies for community
acquired pneumonia will be completed in 2009
and if successful, we are planning to file the NDA
for both uses in 2009 with a possible launch in
2010.

There are a wide variety of pathogenic bacteria
involved in different infectious illnesses. So it is
necessary to develop antibiotics like ceftaroline
that are active against a wide range of pathogens,
some of which are resistant to many traditional
antibiotics.
To be able to further broaden the range of coverage
of ceftaroline, we have acquired NXL-104,
which is a novel compound licensed from the
French company, Novexel, which can be combined
with ceftaroline. Even though NXL-104 is not by
itself an effective antibiotic, it is active in defeating
the most widely used defense that bacteria utilize
in achieving resistance to antibiotics. NXL-104,
therefore, added to ceftaroline, may result in the
broadest spectrum of antibiotic treatments presently
available.
Some pathogenic bacteria, especially the gramnegative
organisms have evolved to develop
resistance to beta-lactam type antibiotics, like
cephalosporins and penicillins, by their ability to
produce an enzyme called beta-lactamase that
destroys the antibiotic before the antibiotic can
destroy them. Our product NXL-104 is a
beta-lactamase inhibitor. It makes the bacterias
beta-lactamase ineffective, and therefore enables
the antibiotic to be active against certain bacteria
where it had previously been ineffective.
But that, of course, is not the end of the story.
The cycle starts with an antibiotic which scientists
have created through brilliant innovation that
destroys pathogens by, for example, breaking
down their cell wall, which is how penicillin works.
To defend themselves, the pathogens then developed
beta-lactamase, which breaks down the
crucial molecular ring in the antibiotic, thereby
destroying it. Then scientists developed
beta-lactamase inhibitors, like NXL-104.
The beta- lactamase inhibitor binds to beta-lactamase,
thereby preventing it from destroying betalactamase
type antibiotics, and therefore antibiotics
such as ceftaroline/NXL-104 are protected
from this type of resistance mechanism.
And so we have won a major victory, which may
turn out to be a major reprieve. Because that is
certainly not the end of the cycle. The pathogens
will undoubtedly develop some way to destroy or
inhibit the beta-lactamase inhibitor or make it
irrelevant. We dont know how long it will take.
It could be many years. And then scientists will
develop a defense against the pathogens new
offense. It is in fact a perpetual warfare we are
engaged in  human intelligence against
pathogens with no intelligence at all, but that
blindly and endlessly mutate with an amazing
rapidity and variety. And some of those millions
and millions of mutations, by chance, ultimately
will defeat our latest scientific achievements.
And then the surviving pathogens will reproduce
by the millions either by cell division or by just
passing around their DNA. Its a war that will
never end, certainly not what Fleming anticipated
when he discovered penicillin.
Nevertheless, for today, ceftaroline and ceftaroline
combined with NXL-104 can be expected to
achieve very significant sales in the next decade.
We have another product, milnacipran, for which
we filed the NDA in December of last year, and we
expect an FDA action in October. We obtained the
license to milnacipran from Cypress Bioscience, an
American company, which, in turn, licensed it from
Pierre Fabre, a preeminent French company which
originally developed the compound. Milnacipran is
for the treatment of fibromyalgia, and perhaps
ultimately for other indications. It is believed that
fibromyalgia affects 6,000,000 people in the United

States. It is characterized by recurrent pain in various
parts of the body. The cause is unknown.
There are many theories, one of which is that it is
an autoimmune condition. The symptoms are
relieved by milnacipran based on the clinical
studies submitted in our NDA. If milnacipran is
approved by the FDA, it could be launched in
the beginning of 2009.
Another product, which we hope to launch by
2012, is linaclotide, which we have licensed from
Ironwood Pharmaceuticals (formerly called
Microbia), an American company located in
Boston. Ironwood recently completed Phase IIb
studies for chronic constipation and constipation
predominant irritable bowel syndrome (IBS-C) with
excellent results and Phase III studies in both
indications will soon be commenced. Both those
indications are widespread.
If the product is successful in the Phase III studies
and approved, it would be the first product of its
type for the treatment of chronic constipation and
IBS-C. Importantly, linaclotide is the first remedy
for those conditions that is not absorbed by the
body after it is ingested. Therefore it never enters
the blood stream and has less potential to produce
systemic effects or side effects which has been the
problem with other products for these indications.
Linaclotide works locally in the gut to stimulate the
secretion of fluids, and thus relieve IBS-C and
chronic constipation symptoms. The size of the
market is substantial. Zelnorm, which had to be
withdrawn because of safety concerns, had sales
which were annualizing at about $600,000,000
when it was withdrawn.
Another important product in late stage development
by us is RGH-188, for schizophrenia,
developed by Gedeon Richter, a Hungarian company,
and the largest pharmaceutical company in
Eastern Europe. We announced late last year the
results of the Phase II study in 400 patients, which
indicated successful outcomes, both in antipsychotic
action and in better tolerability, which therefore,
compares favorably to the antipsychotics
presently available. We are presently conducting
additional Phase II studies in schizophrenia and
bipolar mania before commencing Phase III
studies.
RGH-188 could potentially become a leading
product in its category, if the early therapeutic
action and side effect profile are reproduced in
future Phase III studies. The product for both
indications could be available in 2012.
That adds up to five late stage products  one
recently approved, one waiting for FDA approval,
one with successful Phase III studies, and two
with successful Phase II data, all of which we
expect will make a significant contribution to
Forests sales and earnings in the years ahead.
The cycle of exclusivity demise and new product
birth is characteristic of the pharmaceutical
business. Fortunately, we expect to be birthing
products to ameliorate the demise of the older
ones, with an enviable continuity.
The research efforts involved in developing and
attempting to comprehend how drugs work is no
easy matter. We have to deal with the greatest
mystery of all, the functioning of the human body
and the effect of our interventions, which is almost
always surprising us or defeating us. At best,
we are intruding in a vast unknown, how the body
works, how the mind works, the effects of our
meddling with what is really the intricate result of
thousands of millennia of evolution, and which
often fiercely defends itself against our interventions.

And to make it more complicated, all the members
of our species are different in ways that make
developing universal remedies, with universal side
effects, not just very difficult, but impossible.
And so our intervention efforts are variable, and
that is why a drug apparently beneficial for so
many people can have an undesirable effect in
some other people. Not everyone understands
that variability. They expect a drugs effect to
be universal, even though we vary in size,
shape, appetites, in our internal organs and
symptoms, and ultimately in our individual
genetic components and the stimuli they have
been subjected to. So why wouldnt we
respond differently to drugs?
Nevertheless, we have critics in Congress, in
the media, and the strike suit lawyers, who all
benefit by making a cause out of something
which is really perfectly natural.
I have summarized some of our more important
products under current development which
we expect will become important products for
us in the next decade, offsetting and ultimately
surpassing the products we expect to lose during
that period. There are more products in
development, more we are negotiating for and
more we will be negotiating for every year.
All this is the result of a prodigious work ethic
throughout the Company, in locating products,
in struggling to evaluate them accurately, in
developing them, further and further back in
their development, in developing brilliant
marketing plans and in marketing our products
to physicians. We have avoided direct to
consumer advertising because we believe our
audience is physicians. And in everything we
have done we have tried very hard to stay well
within the law and best industry practices.
It is our employees, year after year, who do it all,
whose exacting standards and good judgment and
hours of gritty hard work, have enabled Forest to
earn the profits it has, and enjoy the prospects it
has  even in these difficult economic times.

Howard Solomon
Chairman & Chief Executive Officer
Lawrence S. Olanoff, M.D., Ph.D.
President and Chief Operating Officer