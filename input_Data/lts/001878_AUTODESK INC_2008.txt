Whether you know it or not, Autodesk software touches your life on a daily basis. Our design software is used to
help create virtually everything you come across in an average daymanufactured products such as automobiles,
office products, and airplanes; the buildings, factories, and labs you work in; as well as the special effects and
animation you see in feature films and video games.
Autodesk has come a long way since our founding 25 years ago as a pioneer in the world of computer-aided design
(CAD). Our flagship product, AutoCAD software, has become synonymous with CAD and is a fixture in design
shops worldwide. We have also created 3D solutions for industrial design and manufacturing, architecture, civil and
structural engineering, construction, as well as media and entertainment. Today, we stand as the worlds leader in 3D
softwareand were still growing strong.
Fiscal 2008 was another successful year for Autodesk, and we believe our market-leading position is stronger than
ever. Autodesk has grown substantially since its inception, but nothing compares to the past five years. To put our
growth in perspective, it took Autodesk 22 years to reach $1 billion in revenue. It took only three more years to top
$2 billion. Our business today is being driven by important trends weve seen develop over the past few years, and
we believe we can continue to capitalize on these trends going forward.
Trends Driving Todays Business
Todays companies face unprecedented challenges and opportunities. Design is more important than ever as
understanding form and function constitutes the foundation for solving todays problems. Companies are challenged
to better differentiate their products from the competition. Autodesk gives customers the technologies they need
to respond to these challenges. Design and engineering are principal methods in which todays companies look to
distance themselves from their competition and avoid commoditization.
The increase in innovation and productivity that our model-based 3D products provide is compelling our customers
to migrate to new design tools. As a result, our model-based 3D solutions are quickly gaining attention and our
revenue in this area is growing. During fiscal 2008, revenue from our 3D products increased 26 percent over fiscal
2007 and represented 23 percent of total revenue. Although we are the leader in the 3D design market, we believe
there is still a significant growth opportunity for Autodesk in this market, as 3D penetration of our customer base
remains under 15 percent.
Today, Autodesk sells products in countries
around the world. While the majority of our
revenue is generated in developed countries,
another key growth driver for the company is
revenue generated in emerging economies, which
has been growing at roughly twice the pace of
revenue generated in developed countries. In
emerging economies, there is a high demand
for infrastructure as these countries prepare for
increased industrialization. Such infrastructure
includes airports, railways, roads, clean water,
and sanitation, to name just a few. In addition,
emerging economies are becoming major centers
for manufacturing of both consumer and industrial
products. In fiscal 2008, Autodesk revenue from
emerging economies increased 40 percent and
now represents 17 percent of total revenue.
Since 2004, our customers have migrated to
purchasing maintenance under our subscription
program. There are many benefits when customers
renew maintenance annually, such as freeing
channel partners to focus their efforts on new
customers. Increasing our maintenance base also
creates a more predictable revenue stream for
Autodesk, partially mitigating the risk of economic
uncertainty. Our maintenance base has grown
substantially over the past several years and now
consists of nearly 1.5 million users.
Other macro trends that continue to drive our
markets include the globalization of industry; the
building boom in which countries are building,
rebuilding, and repairing their infrastructure;
and global climate change, which requires
designers and engineers to make their products
and buildings more energy efficient and
environmentally friendly.
Record Financial Performance
Fiscal 2008 also marked the fifth straight year in
which we achieved record financial performance.
For the first time, Autodesk crossed the $2
billion revenue threshold. Net revenue increased
18 percent to $2.17 billion, and revenue from
new licenses increased 19 percent compared to
fiscal 2007.
Maintenance revenue increased 31 percent
compared to fiscal 2007, and was 25 percent of
total revenue in fiscal 2008.
While we have experienced very strong growth
in the 3D market, its important to recognize that
our core base of 2D solutions continues to post
healthy double-digit growth, led by outstanding
results for AutoCAD LT software. Strong growth
in 2D creates future opportunity as well, because
our 2D solutions provide the easiest migration
path to our 3D products and then to Digital
Prototyping.
While increasing revenue is always important,
increasing profitability and shareholder value is
equally important. In fiscal 2008 we increased our
GAAP diluted earnings per share by 24 percent to
$1.47, compared to $1.19 in fiscal 2007. Non-GAAP
diluted earnings per share increased 23 percent to
$1.88, compared to $1.53 in fiscal 2007.
CEOs Letter
Across the board, we were pleased with our
financial performance in fiscal 2008. Here are a
few highlights:
?? GAAP gross margin increased to 90 percent
from 88 percent in fiscal 2007. Non-GAAP
gross margin increased to 91 percent
compared to 89 percent in fiscal 2007.
?? GAAP operating margin increased to 21
percent from 19 percent in fiscal 2007.
Non-GAAP operating margin increased to
27 percent from 25 percent in fiscal 2007.
 Cash generated from operating activities
increased 23 percent to $709 million,
compared to fiscal 2007.
 Total deferred revenue increased 34
percent to $506 million, compared to
fiscal 2007.
Strong Divisional Performance
The Design Solutions Group represented 87 percent
of total revenue in fiscal 2008 and increased 19
percent to $1.90 billion, led by strong growth in
each design division.
???? Platform Solutions and Emerging Business
revenue increased 13 percent to $997
million, compared to fiscal 2007. This
division represented 46 percent of total
revenue. Our core products of AutoCAD and
AutoCAD LT continue to be the preeminent
design tools and performed very well in the
market. AutoCAD LT posted exceptionally
strong growth compared to fiscal 2007.
???? Architecture, Engineering, and
Construction revenue increased 26 percent
to $480 million, compared to fiscal 2007.
This division represented 22 percent of
total revenue. Revit-based products and
AutoCAD Civil 3D software continue to
post strong growth within this division. We
are gaining traction with our rich suite of
design solutions built on the Revit platform,
enabling a design concept pioneered by
Autodesk called building information
modeling (BIM). BIM is the creation
and use of coordinated, computable
information about a building project in
design and construction. The ability to keep
this information up-to-date and accessible
in an integrated digital environment gives
architects, engineers, builders, and owners
a clear overall vision of their projects and
contributes to the ability to make better
decisions faster, helping to improve quality
and increase the profitability of projects.
Manufacturing Solutions revenue increased
25 percent to $418 million, compared to
fiscal 2007. This division represented 19
percent of total revenue. Performance
continues to be positively influenced by
the continued adoption of 3D modeling
solutions. Our Inventor family of products
is still the worlds best-selling 3D
mechanical design software. AutoCAD
Mechanical software also posted very
strong year-over-year growth.
Media & Entertainment revenue increased
10 percent to $259 million, compared
to fiscal 2007. This division represented
12 percent of total revenue. Within this
division, the animation products generated
strong growth of 22 percent year over year.
Autodesk 3ds max and Autodesk Maya
animation products are moving across
disciplines and are now being used by
todays cutting-edge design engineers as
well as leading animation studios. Revenue
growth of this division was somewhat
slowed by the continued migration of
our Advanced Systems solutions from
SGI hardware to mainstream systems.
This change, however, has substantially
improved the Advanced Systems gross
margin, which is a better measure of the
health of our systems business.
Thought Leadership
Autodesks growth over the past 25 years falls
into three distinct stages. Stage one was when we
changed the future of design with the introduction
of AutoCAD software. This represented a truly
disruptive, and democratizing, technology. In
stage two we made the move to 3D digital models
and developed a portfolio of industry-specific
products to address the needs of particular market
segments. Today, were in stage three, where
our 3D modeling technologies help customers
understand, and improve, the performance of
their designs before they build them, so they
can experience them before they are real. This
capability is one of the reasons that design has
become such an exciting field, enabling customers
to do things that were not possible a few years ago.
Looking Ahead
Over time our technologies have become more
and more sophisticated. We are facilitating the
evolution of the way design is done all over the
worldand are committed to democratizing
those technologies by making them available
to the widest range of customers possible. This
commitment means continuing to invest in
research and development as well as acquiring
technology and businesses that we believe will
complement or integrate with our current portfolio
of products.
Throughout our history, weve focused on helping
customers meet the challenges they face. We strive
to anticipate, and respond to, customers changing
needs, looking ahead to see where theyre going
so that we can use our technologies to remove any
roadblocks they may encounter.
Our business is diversified on many levels. Our
revenue base is geographically diversified. We are
diversified across the major industrial segments in
the economy, including building, manufacturing,
infrastructure, and media. In addition, we serve
customers of all sizes, from Fortune 100 companies
to small post-production facilities. We believe such
diversification helps to moderate the impact on our
business from large swings in external factors.
And finally, we are mindful of the mixed
economic indicators in the market today. While
this mixed data requires us to be more cautious,
we believe we have the right strategy, the right
CEOs Letter
products, and the right team to continue to
succeed. Our balance sheet is strong and we
believe that will allow us to continue to pursue
our strategy.
On behalf of the Board of Directors, I thank our
customers, employees, channel partners, and
investors for another successful year. Autodesk has
come a long way in 25 years and we look forward to
leading the design revolution for the next 25 years
and beyond.
Carl Bass
Chief Executive Officer and President