DEAR FELLOW STOCKHOLDER,

It is with great pleasure that American Water is sharing with you another solid year of results. As this company
celebrates its 125th anniversary in 2011, we are reminded that our success is rooted in traditions of reliability and
performance and our 2010 results are a refl ection of that proud history. It can be seen in the year-over-year increase
we achieved in revenue, net income and earnings per share. It is demonstrated by the continuation of consecutive
dividend payments and refl ected in the investments we have made, approximately $800 million in 2010, to help ensure
the quality and reliability of the services we provide.
And while our foundation is based in tradition and experience, it is innovation and expertise that supports our evolution.
That, too, can be seen in our 2010 results. Last year, American Waters employees delivered water solutions that
spanned the country. In Kentucky, we put into service a $164 million water treatment plant and pipeline that will
help ensure a sustainable water supply in Central Kentucky today and for future generations. In Battery Park City,
Manhattan, we started full operations of a water recycling system in a 32-story residential building that is the latest
in a series of fi ve green high-rise condominiums for which American Water designed, managed construction and
operates the water reuse systems. In total, these fi ve buildings save approximately 56 million gallons of water per year.
And in California, American Waters partnership with the city of Fillmore was recognized by the National Council for
Public-Private Partnerships for building and operating a wastewater treatment facility that provides one million gallons of
treated water per day that is reused for irrigation and groundwater recharge.
We also continue to strategically review our business activities. We are taking steps to ensure we are operating
in areas where we can best serve customers and meet business objectives. This effort is about creating value by
investing capital and resources and strategically growing where we can drive operational excellence and take advantage
of our existing critical mass. Recent examples of our portfolio optimization efforts include our agreement to sell our
regulated water and wastewater systems located in Arizona and New Mexico for an estimated purchase price of $470
million and our agreement to acquire 11 water and 59 wastewater systems in Missouri, leveraging the strength of our
large-scale operations in that state.
This disciplined growth will continue as we expand the reliable services we provide to new customers. Last year,
American Water acquired water systems in states where we have some of our largest regulated operations including
Pennsylvania, Missouri and Indiana. In our Market-Based businesses, we are matching the services we provide to
current and future needs. From our contracts with the military, which include 10 contracts in nine states, to Homeowner
Services, where now more than 60 percent of its customers are covered by more than one program, our growth is
strategic.
American Water continues to drive operational excellence through a multi-year effort to enhance the technology that
supports the business and customers needs and improve key business processes. This is part of our sharpened focus
on driving costs out of the business by leveraging technology and the talent of our people. Most importantly, this effort
will enhance the services we provide to more than 15 million people.
As we build on the successes of 2010, we are positioning the company to continue to succeed in 2011 and beyond.
One hundred twenty-fi ve years ago, our predecessors created a company to meet the water service needs of a growing
country. Now, as water and wastewater infrastructure is critically challenged, American Water is evolving to meet the
new needs of a nation.
I invite you to visit www.amwaterannualreport.com to learn more about our accomplishments in 2010, and our plans for
the future.
Thank you for your interest in American Water.

Jeff Sterba
President and Chief Executive Officer