To our hareholders:
As expected, 2009 proved to be a year of deep and painful recession.
The Companys safe balance sheet and conservative dividend payout ratio
allowed Essex to withstand the resulting shocks to the financial system
and, at the same time, pursue non-traditional investments to add shareholder value. Ultimately, we were able to identify and close on opportunities in this environment, resulting in gains that allowed us to exceed our
forecasted growth in Funds from Operations.
With over 10% of the US workforce unemployed and a loss of approximately
7 million jobs, 2009 was a year that required aggressive and thoughtful
management, along with financial and operational flexibility. Rather than
acquiring or building apartments, our focus was drawn to apartmentrelated investment opportunities with attractive risk-adjusted returns that
emanated from disruption in the credit markets. Amid declining market
rents, our operations group focused on resident retention, occupancy
and longer lease terms. Fortunately, the unprecedented actions by the US
government and the Federal Reserve Bank helped stabilize the financial
markets in mid-2009, which led to moderating rent losses for the remainder of the year.
Following is a summary of Essexs important achievements in 2009:
 Reported FFO per share of $6.74, representing a 11.8% increase compared to 2008.
 Revenues declined 2.8%, which compares favorably with our peers, due
in part, to resident retention programs that allowed us to average 97%
occupancy on our apartment portfolio.
 Disposed of five non-core apartment communities, using the proceeds
to opportunistically repurchase 350,000 shares of common stock below
$58 per share.
 Acquired two condominium-quality assets located in Southern California
for a total expected investment of $52 million.
 Invested approximately $100 million in investment grade unsecured
REIT bonds at yields of approximately 10%.
 Originated $178 million in 10-year secured mortgage loans with an
average interest rate of 5.8%, and repaid $24 million of secured debt
with an average interest rate of 6.9%.
 Retired $167 million in exchangeable bonds resulting in a gain of
$4.7 million, and retired $142 million of Series G Preferred Stock at
a discount to carrying value, resulting in a $50 million increase in net
income available to common stockholders.
 Originated a new $200 million unsecured credit facility to replace an
expired facility of the same size.
 Completed construction and lease-up of The Grand, Studio 40-41,
and Cielo on time and on budget.
Looking A h ead: We believe that apartment rent growth will resume in
mid-2010, and will gradually accelerate thereafter. This outlook is based
on our belief that job losses will abate in early 2010, and housing demand,
driven principally by job growth and better economic conditions, will
return in the second half of the year. Deliveries of new housing (apartment
and for-sale) will continue through most of 2010. Beginning in 2011, we
expect a prolonged period of negligible housing supply resulting, in part,
from the prevailing reality that apartment rent levels and prices of for-sale
housing are insufficient to justify new construction in most markets. The
impact of these conditions will lead to modest market rent growth in 2010,
increasing to strong rent growth in 2011 and 2012.
Despite the improved long-term outlook, we expect Essexs 2010 revenue
declines will exceed those reported in 2009, as we roll existing leases to
lower prevailing market rents. We believe that Southern California will be
the first west coast apartment market to recover, followed by Northern
California and finally Seattle.
In 2010 we plan to increase apartment acquisitions of well located, desirable apartment communities, as lenders begin to foreclose on secured real estate loans with balances greater than the fair value of the underlying
collateral. We also believe there will be opportunities to acquire high-end
condominium projects at cap rates attractive to apartment investors. Many
of these opportunities will allow financially sound entities such as Essex to
acquire real estate for prices well below replacement cost. We expect to
fund these initiatives through a combination of debt, equity, joint venture
capital and sales of marketable securities.
In Conclusion: With one of the strongest balance sheets in the
multifamily REIT sector, we are well positioned to take advantage of
investment opportunities and the favorable economic conditions in future
years. We believe that the dynamic and diverse nature of the west coast
economies will once again allow Essex to be a leader in apartment
rental growth.
Economic uncertainty ultimately tests the tenacity of management and its
ability to execute in a rapidly-changing environment. We believe that our
2009 activities and results prove that we have successfully navigated
difficult times, and are positioned to benefit from renewed growth. Once
again, we thank our shareholders, employees and partners for their contribution to Essexs continued success.