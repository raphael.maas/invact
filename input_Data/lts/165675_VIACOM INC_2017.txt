January 19, 2018 
Dear Viacom Stockholders:  
We are pleased to invite you to attend the Viacom Inc. 2018 Annual Meeting of Stockholders. The 
meeting will be held on Thursday, Ma
rch 8, 2018 at Viacom
�s corporate headquarte
rs at 1515 Broadway, 
New York, New York, beginning at 9:00 a.m., Eastern Standard Time.  
At this year�s meeting, we will be electing nine me
mbers of our Board of Di
rectors and ratifying the 
appointment of our independent auditor, as
 described in the proxy statement.  
To  help  reduce  costs  and  the  environmental  impact
  of  printing  the  proxy  materials,  we  encourage  
you  to  take  advantage  of  electronic  delivery  of  proxy  
materials  by  following  the  instructions  in  the  proxy  
statement.  Stockholders  who  have  not  
elected  to  receive  proxy  materials  electronically  or  in  print  will  
receive in the mail a Notice of Internet Availab
ility of Proxy Materials that tells you how to:  
?
   Access   the   Notice   of   2018   Annual   Meeting   of   Stockholders   and   Proxy   Statement,   our   
Stockholder Letter and our Annual Report on Form 
10-K for the fiscal year ended September 30, 
2017 through http://proxymaterials.viacom.com; and  
?
  Submit your vote if you hold 
shares of Class A common stock. Class A common stockholders can 
submit their vote by telephone, the Internet or 
in person at the Annual Meeting. Class A holders 
will also find instructions on how to vote their shar
es on their proxy card or voting instruction card.  
We  appreciate  your  continued  support  of  Viacom  
and  look  forward  to  seeing  you  at  the  Annual  
Meeting.  
THOMAS J. MAY 
ROBERT M. BAKISH 
Chairman of the Board of Directors
President and Chief Executive Offic