Dear Shareholders:
Motorola Solutions enjoyed another record year of revenues and operating earnings
in 2012, a strong financial performance that reflects our disciplined focus on
customers, innovation and value creation.
Motorola Solutions is well positioned to grow and expand globally. Our strategy is
focused on providing mission-critical communication solutions to government and
enterprise customers. Today, were trusted by our customers because of the 85-year
history of partnership and innovation weve established with them. Were not just
selling products and solutions. Instead, were showing our customers a vision of the
future and extending the dialogue around their needs, challenges and expectations
and how were uniquely positioned to help.
Our vision of the future is backed by our commitment to innovation. We continue to improve and deepen our industryleading
product portfolio, and our sales growth demonstrates a keen focus on new product innovation, our strong go-tomarket
capability and solutions that deliver a high return on investment for our customers.
We have a strong balance sheet, which provides a solid foundation and flexibility that should enable our company to
thrive in the years ahead. We also are positioned to access capital markets in a way that complements our growth
strategies, and we are making significant progress in establishing an appropriate capital structure for our unique
business needs.
Here are just a few of our 2012 highlights:
 We posted record full-year revenues and operating earnings.
 Sales increased 6 percent, driven by strength in our Government segment.
 We returned $2.7 billion in capital to our shareholders through share repurchases and dividends.
 We generated $1.1 billion in operating cash flow and ended the year with a total cash balance of $3.6 billion.
 We acquired Psion to strengthen our enterprise mobile-computing portfolio with solutions for manufacturing,
ports and warehousing.
 We continued to invest in our core value of innovation, funding more than $1 billion in research
and development.
 The value of our stock increased 20 percent in 2012 and 49 percent since it began trading as MSI on
January 4, 2011.

As a shareholder, you can be confident of our focus to drive profitable growth, leverage our distinctive assets
and continue to develop and grow the talent within our ranks on every level.
On behalf of our employees who are committed to fulfilling our purpose to help people be their best in the moments
that matter, we thank you for continued confidence in our company.
Gregory Q. Brown
Chairman and Chief Executive Officer