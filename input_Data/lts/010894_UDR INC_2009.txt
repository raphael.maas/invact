Dear Shareholder,

To say 2009 was a challenging year is certainly an understatement. The primary driver
of our business jobs disappeared at an unprecedented pace, leading to the highest
unemployment rate in almost thirty years. Despite these challenges, our experienced
management team took the necessary steps to navigate the business through the
downturn. We focused our efforts on improving our financial flexibility and strengthening
our operating platform to position the company for the future economic recovery.
Our operating results for the year were negatively impacted by the recession, with Funds
from Operations (FFO) down 6 cents to $1.24 per share, excluding one-time items.
2009 same-store net operating income (NOI) fell by 2.2% on a decrease in same-store
revenues of 2.0% and a reduction of same-store operating expenses of 1.6%. While we
will never be satisfied with negative results, our performance for the year was relatively
strong given the severe economic challenges.
Balance Sheet and Liquidity The dislocation in the capital markets that started in 2008 continued in 2009
and presented us with opportunities to restructure our balance sheet and improve our financial flexibility
for the future. During the year we:
B Repurchased over $300 million of debt and preferred shares at a discount
of more than $16 million;
B Refinanced or extended nearly $1 billion of debt to strengthen our balance sheet
while taking advantage of the low interest rate environment;
B Executed a new ten-year, $200 million secured facility with Fannie Mae
at a weighted average interest rate of 5.28%;
B Raised $68 million through sales of our common stock at a weighted average price within
12% of our 2009 high; and
B Entered into a $450 million acquisition joint venture with Kuwait Finance House
that allows us to expand our portfolio with the judicious use of our capital.
As a result, we ended 2009 with a strong balance sheet and over $730 million in
available cash and undrawn capacity on our credit facilities.
In May, we further enhanced our liquidity by reducing our annual common stock dividend
from $1.32 to $0.72 per share to reflect the current economic environment and the
operating fundamentals of our business. We will continue to review our dividend policy
and look forward to improving fundamentals that will allow us to grow our dividend.

Operating Platform and Technology While it may have been tempting in 2009 to reduce or eliminate
spending on our technology initiatives and enhancements, we elected to continue investing in our
operating platform. We made this decision to better appeal to our primary customer 20 to 35 year olds
who have a propensity to rent at nearly twice the national average. This group prefers to interact through
technology and we want to enable them to do business with us using their preferred channels.
The benefits of our investment are apparent in the following results:
B We increased 2009 same-store occupancy by 60 basis points to 95.4%,
which exceeds the peer group average of 94.6%; and
B 2009 same-store operating margin was 68%, which is substantially higher
than the peer average of 62%.
Looking Ahead We entered 2010 with a strengthened capital position, an industry-leading operating
platform and cautious optimism while recognizing that we still have challenges ahead. We believe the
strength of the recovery will occur in 2011 and 2012 as the fundamentals of the multi-family business
improve. The drivers of the recovery will be:
B Pent-up Demand: The re-emergence of job growth in our markets as the economy improves,
further benefiting from the influence of the demographic trends of the Echo Boom generation
(15  28 years old). This generation consists of over 60 million individuals who have pushed
college enrollment to all time highs. As they graduate from college and find employment
they will join the rental market and increase the demand for apartments.
B Lack of Supply: The delivery of newly-constructed apartment homes in 2010 and 2011 is forecast
to be lower than at any point over the last 40 years.
In anticipation of the improvement in the fundamentals of our business, we will remain focused on:
B Continuing to look for attractive acquisitions and selective disposition opportunities;
B Completing the development or redevelopment of 2,424 homes and preparing
to start new communities when we see firm signs of a recovery; and
B Continuing to drive our efficiency, performance and customer satisfaction by investing
in our operating platform and technology initiatives.
We believe the hard work and dedication of our more than 1,300 associates were
key contributors to our 2009 achievements, and we are confident that with the strength
of our team we can continue our success.
We announced with great sadness that on March 11, 2010 that Robert C. Larson,
Chairman of the Board passed away. Mr. Larson served as a board member of
UDR for 10 years. We will all miss his insight, knowledge and friendship.
Thank you for your continued loyalty and support.

Thomas W. Toomey
President and Chief Executive Officer
