At Nielsen, we combine data, science, technology, and the talents of our people to provide our clients with independent measurement of their performance and useful analytics to help drive improvement. This is valuable because it increases efficiency for the clients and markets we serve, and that ultimately creates value for all stakeholders. This has been our mission for decades and we remain fully committed to it.
In our efforts to deliver on our mission, we're guided by strong and meaningful values, including an unwavering commitment to integrity. We reinforce and build on our ability to deliver on our mission with continuous innovation and periodic transformation of our business. In fact, we are currently
in the midst of transforming three key parts of our business. In our Watch segment, we have been transforming our product portfolio to better help our clients plan, act, and measure in a dynamic, fragmenting media marketplace. In our Buy segment, we are transforming our core products and our data delivery platform to bring more speed and efficiency to our clients. Watch is further along, with Buy progressing rapidly. Our newest transformation effort is occurring in our Operations function, where we are automating significant portions of our data collection and data delivery operations and consolidating our operations centers.

OUR DEDICATION TO PROGRESS THAT DRIVES GROWTH AND EFFICIENCY WAS RECENTLY RECOGNIZED BY FORBES MAGAZINE, WHICH LISTED NIELSEN AT #30 ON THEIR 2017 LIST OF THE "WORLD'S MOST INNOVATIVE COMPANIES."

Through multiple waves of tremendous marketplace change over the past 95 years, we have responded in ways that have strengthened our company and extended our leadership position. We've done it through periodic transformations, complemented by continuous innovation. This inventive spirit is part of our DNA; our company was built by entrepreneurs and energetic, resilient inventors committed to continually developing new ways to meet the needs of our clients. Today, faced with an unprecedented pace of change in the two main markets in which we operate-media and fast-moving consumer goods-we are following that same tradition. Our dedication to progress that drives growth and efficiency was recently recognized by Forbes magazine, which listed Nielsen at #30 on their 2017 list of the "World's Most Innovative Companies."
 
Ill
Throughout our history, successful innovation has often come in the form of new products to generate growth, whether developed in-house or in collaboration with other innovative
companies, both large and small. One notable example from our Watch segment is Total Audience Measurement . Nielsen has a long-standing position in the U.S., and many other markets around the world, as the trusted custodian of the TV currency on which media is bought and sold. As advances in technology have driven fragmentation of audiences across screens and platforms, we have adapted our measurement solutions to cover the expanding range of viewing environments. The major industry players are looking for independent, comparable, and de-duplicated measurement across all platforms, and this is exactly what we have provided with our Total Audience Measurement system.
We've done it by investing in research and development and by innovating through partnerships
and acquisitions, enabling us to bring our vision for this comprehensive solution fully to life in the marketplace. Nevertheless, there is always more to do, so our innovation will continue . As the market looks more towards outcome-based metrics such as attribution, advertising return on investment, and audience-based buying, we'll continue to leverage our Watch and Buy assets, partnerships, and acquisitions to deliver what's needed next for the market.
To that point, in February 2017, we acquired Gracenote, the industry's leading provider of media and entertainment  metadata . Gracenote performed incredibly well in 2017 on a standalone  basis, exceeding expectations on all key metrics. We're also leveraging Gracenote's assets across almost all aspects of our Watch business to enhance our capabilities and increase our operating efficiency.




>>	INVESTING AND INNOVATING FOR GROWTH
The Connected System is an open, platform-based system that integrates Nielsen's data and other data sources and then seamlessly connects measurement with powerful analytics to drive speed and efficiency for our clients. Total Audience Measurement is the foundation for our future, providing measurement capability, scale, and flexibility as the market evolves.


We've employed a similar approach to innovation in our Buy segment with the development of  the Connected System . This open, cloud-based solution integrates Nielsen's powerful measurement data and analytics capabilities into a single platform, addressing our clients' key needs of speed and
efficiency. In an environment where fast-moving consumer goods clients are challenged to grow their top line, we're increasing the utility of our data, helping clients to understand not only what happened, but also why it happened, and what action to take next. Adding to this, our Connected Partner Program enables third-party analytics companies to run apps on our open data platform, amplifying the value of the underlying measurement data that is the core of our business. Clients can now get answers to a wider range of questions in a highly efficient and useful manner. These innovative, collaborative models are an important part of our business today and this should only grow in the future.
Innovation isn't only about growth; we're also investing in innovation to drive efficiency. With a proven track record of successful transformations within Nielsen, this, too, is part of our legacy.
In 2008, we set in place our plan to globalize the organization, standardizing operational processes and leveraging offshoring opportunities . From there, we re-imagined the way the business looked, integrating teams and simplifying processes, leading to faster, more efficient decision making. In 2017, we embarked on a new plan to transform our business yet again; this transformation is fueled by digitization, automation, and consolidation, supported by a growing use of artificial intelligence and machine learning. This is a major part of our Path to 2020 plan, a three-year roadmap towards a faster-growing, higher-margin business.

20l7 IN R[Vl[W
Total company revenues grew 3.8% on a constant currency basis in 2017. Buy segment revenues declined 3.3% on a constant currency basis. Emerging markets showed continued strength, with constant currency revenues up 8.8% in 2017. With operations in more than 100 markets around the world, we have a truly global footprint that affords us a significant competitive advantage with the large multinationals. This is coupled with a strong roster of local and regional players where, in many markets, Nielsen is the only full-scale measurement provider. Our balanced client portfolio and ongoing investments in coverage position us well for future growth.

EMERGING MARKETS SHOWED CONTINUED STRENGTH . . . .WITH OPERATIONS IN MORE THAN IOO MARKETS AROUND THE WORLD, WE HAVE A TRULY GLOBAL FOOTPRINT THAT AFFORDS USA SIGNIFICANT COMPETITIVE ADVANTAGE WITH THE LARGE MULTINATIONALS.

In developed markets, the operating environment in the U.S. remained challenging, as our large, fast-moving consumer goods clients faced pressure in their own businesses. Clients were carefully managing their spend across all functions, putting downward pressure on their investment in services from companies like ours. While we expect this environment to continue into 2018, our key initiatives are designed to strengthen our underlying business, further differentiate Nielsen, and, ultimately, return developed markets to growth, despite the environment. Our Connected System initiative is a key part of this and, in 2017, we made significant progress on its development and initial rollout. We delivered on our commitment to have 25 clients engaged with the end-to-end Connected System by year-end 2017, and we'll expand that to approximately 100 clients in 2018.
We moved forward on Total Consumer Measurement, where our aim is to provide our clients with a comprehensive view of their sales across all retail formats, including specialty retailers, hard discounters, direct-to-consumer models, and, of course, e-commerce, which we're now measuring in 17 key markets. We also continued to invest in our relationships with retailers. In November 2017,
we were named the provider of Wal mart's new data collaboration program, "Walmart One Version
 
Ill
of Truth." In the spirit of Wal mart's customer-centric philosophy, this program will empower the world's largest retailer and its supplier partners with shared data and analytics that fuel faster, better-aligned decisions to drive sales, reduce costs, and improve the customer experience .
Watch segment revenues increased by 11.7% on a constant currency basis, led by strong momentum in Total Audience Measurement and Marketing Effectiveness. Excluding the acquisition of Gracenote, Watch revenues increased 4.5% on a constant currency basis. Our Total Audience Measurement system is now well established in the market, and we'll continue to build on it from a base of strength, with both linear and digital solutions.
We've significantly expanded the range of viewing captured by the C3/C7 currency metric for linear ad models through offerings such as Digital in TV Ratings and Out-of-Home measurement, and we'll further expand on that in 2018 by enabling crediting of both linear and dynamically inserted ads in the same program, via the C3/C7 currency metric and Digital Ad Ratings, respectively. This expanded
definition, coupled with the long list of industry leaders already using Digital Ad Ratings for guarantees, puts us in a very strong position to become the currency for digital advertising, along with our existing position as the currency provider for linear TV.
As the market continues to evolve, the need for high quality, independent, third-party measurement is as great as ever, and this spans all types of media engagement : mass reach-based advertising,targeted audience-based advertising, and subscription services . We're constantly innovating to keep pace with the ongoing change and create incremental growth opportunities . One trend in the industry is the shift in advertising spending towards audience-based buying segments.


>>	INVESTING AND INNOVATING FOR EFFICIENCY
Throughout  our operations supporting both Watch and Buy, we are investing in innovation to drive efficiency. Automation  initiatives, consolidation  of our operations, and platform convergence,  supported by a growing use of machine learning, will enable more scalable growth and transform our business.
This will help us provide uniquely better products that drive value for our clients and our shareholders.



 
AUTOMATE BUY DATA COLLECTION
 
AUTOMATE WATCH OPERATIONS
 




 	 	 


 
INCREASED  EFFICIENCY, SPEED, QUALITY
 
CONSOLIDATION  OF OPERATIONS AND PLATFORMS
 
NANO METER, REMOTE MONITORING
 





As the market leader, we are already in a strong position with our Nielsen Catalina Solutions and Nielsen Buyer Insights solutions. We're  building on that position with additional capabilities to meet the needs of a wider range of advertisers, agencies, and media sellers.

AS OUTCOME-BASED METRICS BECOME AN INCREASINGLY IMPORTANT PART OF THE MARKET, OUR ONGOING INVESTMENTS AND INNOVATION HAVE USWELL POSITIONED FOR THE FUTURE.

Much of what we do in this area falls into our Marketing Effectiveness practice, where we leverage our Watch and Buy assets to help advertisers, agencies, publishers, and content owners measure the return on investment in media and execute on audience-based  buying. We saw strong growth in demand in 2017, with revenues up 21 .1% on a constant currency basis. As outcome-based
metrics become an increasingly important part of the market, our ongoing investments and innovation have us well positioned for the future. Our October 2017 acquisition of Visual IQ added to our already strong capabilities for outcome-based metrics, and we're gaining synergies by leveraging their highly automated platform to take in and process large datasets more efficiently.
We generated free cash flow of $863 million in 2017, enabling us to consistently invest in our growth initiatives while also delivering incremental value to our shareholders through dividends and share repurchases.

Global Responsibility & Sustainability remains an integral component of our strategy as we strive to give back to the communities and markets where we live and operate through responsible, sustainable practices. This includes all environmental, social, and governance ("ESG") areas that affect our business, operations, and all internal and external stakeholders . In 2017:
�	We continued to focus on data privacy and data security, reflecting heightened regulatory attention, especially in Europe. This includes development of a dedicated, cross-functional team to ensure readiness for General Data Protection Regulation ("GDPR"), new legislation slated to go into effect
in the European Union in May 2018. We have long been committed to responsible stewardship of the data we handle, and we will remain committed to protecting the privacy of our panelists, clients, associates, and the public.
�	We were included in two prominent indices, the Dow Jones Sustainability  North America  Index and the FTSE4Good Index, for the first time in 2017, recognizing leaders in corporate responsibility and sustainability . We were also named a "JUST 100" company (and the Media industry leader) in an annual ranking by JUST Capital and Forbes recognizing America's "most just" companies .
�	We continued to advance our Diversity & Inclusion objectives, which are key to our growth, strength, and ability to innovate. For the fourth year in a row, we were recognized by Diversitylncs Top 50 Companies for Diversity, moving up 9 spots in 2017 to #32 .

OUR fUlUR[
Looking to the future, we'll build on our 95 years of continuous innovation and proven market leadership as we move forward on our Path to 2020 to create value for our clients and our shareholders.  Our strategy and our confidence  are underpinned by our fundamental  strengths: global presence, complete coverage of what consumers watch and buy, a wealth of metadata, an unmatched client base, our open approach to innovation, and the talented and dedicated people of Nielsen.
The Path to 2020 is highlighted by three objectives. First, grow revenue. We expect our key growth initiatives to generate mid-single-digit growth; importantly, this will be a more scalable form of growth as we lean in to our efficiency plays. We'll innovate to deliver products that are
 
Ill
uniquely better than anything else available in the marketplace. In Watch, our Total Audience system provides measurement that is person-level and truly representative across all segments of the population. Our best-in-class technology enables us to provide granular, currency-quality
measurement at scale. In 2018, we'll add viewability, fraud detection, and duration-weighted metrics for digital video . And as media buyers and sellers shift their focus towards audience-based buying and return-on-investment metrics, we are well positioned with leading-edge solutions. In Buy, we'll continue the development and rollout of the Connected System, which aligns key metrics across
all client user groups-sales, marketing,finance, and operations- enabling users to stay in sync as they act on our data. This alignment provides speed and efficiency inside the client organization at a level that has historically been difficult to achieve . Moreover, our data is standardized cross-country, addressing a crucial need for global players; we are the only one that can offer this. Across Watch and Buy, our uniquely better products are the key to driving sustainable growth.
The second objective relates to margins. Our investments in efficiency are expected to yield annualized cost savings of $500 million, leading to accelerated margin expansion of roughly 400 basis points over the next three years. In our Buy segment, where our collection of sales data across over 100 countries includes the faster-growing emerging markets, we are digitizing and automating the data collection process. We'll improve our efficiency, deliver with increased speed and quality, and add new and improved capabilities . We'll also consolidate platforms and operations centers, simplifying processes globally and enabling faster deployment of product updates. In Watch, we're automating




>>	INVESTING AND INNOVATING FOR THE FUTURE
Innovation is not only a function of how much we invest, but also how we approach it. Collaborative models across Watch and Buy will play a growing role in our future. In Marketing Effectiveness,
we draw on the strengths of Watch and Buy, as well as third-party data sets, to further add to our leadership position in outcome-based  metrics.



operations through the deployment of new metering technology and new remote monitoring and maintenance capabilities that will significantly reduce our field operating costs.
Finally, capital allocation. While 2018 will be an investment year, we expect to generate $3 billion of cumulative free cash flow through 2020. This gives us significant flexibility to invest back into our business, while also returning capital to our shareholders through dividends and share repurchases.
All told, when we look out to 2020 and beyond, we see a gold mine of opportunity. Our strategy to invest in innovation for growth and efficiency is clear and achievable. But we know that our strategy only counts if we execute. We are committed to delivering on our goals and I have confidence that we will. And in doing so, we'll drive sustainable, long-term, incremental value for our clients and for
our shareholders.


Thank you for your investment and trust in our company.

 

Mitch Barns
Chief Executive Officer
