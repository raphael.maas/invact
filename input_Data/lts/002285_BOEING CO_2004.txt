Message to the
Shareholders and Employees
of The Boeing Company




An intense focus on execution led to strongly im-           Two, we understand the U.S. government's
proved results in 2004. Earnings were up 161 percent,    desire -- and indeed its obligation -- to leave no
and Boeing outperformed the S&P 500 Aerospace            stone unturned in investigating lingering allegations
and Defense Index in total shareholder return for        of favoritism or abuse of the public trust. We are co-
the year. Based on the numbers, the company is           operating with the government in its investigations.
doing well. Even so, it was a distressing year for all      Three, as a result of our own internal reviews, I
who admire this company or feel privileged to be a       am more convinced than ever that the misdeeds are
part of it. Simply put, our reputation suffered as the   not systemic or characteristic of the overall behavior
company operated under a cloud of uncertainty            of the 159,000 people that make up this company.
throughout 2004 and into 2005.                           That said, we have spent an enormous amount of
   While the U.S. Air Force lifted the suspension of     time and effort to ensure that the people throughout
our launch business in early 2005, we continue to        Boeing understand fully that integrity is a business
bear the effects of events involving a very few peo-     fundamental. Our guiding principle is to exhibit the
ple who are no longer part of the company. We did        highest standards of business conduct at all times.
not conclude a long-sought contract with the U.S.        We have drawn bright lines to define ethical decision
Air Force for a new fleet of tankers. And sadly, the     making, and we have made it clear to all of our peo-
man who had started to turn the company around           ple that there will be zero tolerance for anyone who
made a serious error in judgment. The board asked        crosses the line into dishonest or unethical behavior.
for Harry Stonecipher's resignation because it deter-
                                                         Strategy and Execution
mined his actions surrounding a personal relation-
ship were inconsistent with Boeing's code of
conduct, reflected poor judgment and eroded his          Boeing is an aerospace company of unrivaled bal-
ability to lead this company.                            ance and breadth, and our strategy is well known.
   As the senior leader of this company, I wish to       Most Boeing employees could recite the three main
make three main points from the outset.                  elements of our strategy in their sleep. First, we will
   One, no one should underestimate the strength         run healthy core businesses. Next, we will leverage
of our business or the firmness of our resolve. It is    our strengths and move into adjacent businesses
harder to rebuild a reputation than it is to build one   where we have a competitive advantage in technol-
in the first place. We've taken decisive action to fix   ogy or in customer knowledge and insight. Last, we
the problems identified and feel confident that we       will lead, not follow, in opening the new frontiers
will succeed in that endeavor because we have            that are critical to long-term growth and competi-
attracted the right people, and we continue to do        tiveness within the aerospace industry.
so. Our people are motivated by a fierce pride in           That is our strategy. It is working well, and it is
what they do and what it means to be part of The         not about to change. What has changed is a return
Boeing Company.                                          to the fundamentals of executing the business. We




                                                                                                                  3
All in all, there is exceptional upside for
Boeing in our defense businesses.

took a good, hard look at the business case behind        that strategy to a wide array of conventional
every one of our programs and businesses -- re-           and nonconventional threats. It excels as the lead
examining targets, establishing new and more              integrator of complex defense systems that involve
meaningful ones where needed, and really holding          different platforms (such as aircraft, tanks, ships
people accountable for their performance.                 and satellites) and has dramatically increased the
    As a result of these thorough reviews, we took        sharing of information and capabilities among them.
corrective action in several areas. We decided, for           During 2004, IDS captured more than $30 billion
instance, that, even though our 717 brings tremen-        in orders, including major contract awards from the
dous value to the airlines that operate it, the overall   U.S. Navy, Army and Air Force:
                                                          I
market does not support continuing production                 A $3.9 billion Navy contract to build the Multi-
beyond delivery on our current commitments. We                mission Maritime Aircraft that will replace the
also determined that Boeing Air Traffic Management            Navy's aging fleet of 223 P-3s with new aircraft
simply isn't a business right now. So we scaled               based on the Boeing 737 airframe. This program
back the program and moved it into our Phantom                represents $44 billion in potential revenues.
                                                          I
Works research and development group. There, it               A $6 billion extension of the Army's transforma-
will be nurtured as an extremely promising technol-           tional Future Combat Systems program to expand
ogy and business concept until customers are ready            the program's scope and accelerate delivery of
to make a financial commitment to radical improve-            key technologies and capabilities to current
ment of the global air traffic control system.                forces. This brings the total value for Boeing's
    But the biggest payoff came in the improved               part of the program to $21 billion.
                                                          I
performance of our primary businesses --Integrated            A contract with a potential value of $4.9 billion
Defense Systems and Commercial Airplanes.                     to provide maintenance and support work for the
                                                              Air Force's C-17 fleet.
Integrated Defense Systems                                    In addition to resolving issues from an earlier
                                                          time period that affected the tanker procurement,
Boeing Integrated Defense Systems (IDS) performed         the one remaining challenge for IDS is the commer-
extraordinarily well in 2004. Revenues grew by 11         cial satellite business. This business continues to
percent; operating margins improved to 9.6 percent;       suffer from cost growth and technical problems.
and IDS continued to add to its backlog, already the      However, we have assigned good people to the
highest in the defense industry.                          business, and we expect to fix those problems in
    IDS has established Boeing as the leading indus-      the coming year.
try partner to the U.S. government in developing a            All in all, there is exceptional upside for Boeing
"network-centric" view of the world and applying          in our defense businesses. Defense customers have





a high level of confidence in our company. They see     of 126 new 787s. With announced sales in early
Boeing as a clear leader in understanding the com-      2005, that total reached 193.
plex challenges of today's defense environment and         Through the use of new materials and advances
in being able to marshal the resources (and espe-       in aerodynamics, we've designed the sleek-looking
cially the information and communications) that are     787 to open a new frontier in commercial flight. As
needed to meet them.                                    the first widebody with high-strength, lightweight
                                                        composite body, wings and tail, it is expected to
Commercial Airplanes                                    outperform other midsize airplanes, delivering
                                                        higher speed, more range, reduced fuel burn and
In the leanest of times, Boeing Commercial Airplanes    emissions, greater interior comfort, reduced noise
(BCA) learned to excel at lean production and assem-    on landings and takeoffs, and superior economics.
bly. Since the collapse of the commercial airplane      In opening nonstop service between scores of new
market in September 2001, BCA has exercised great       city pairs, the 787 is designed to bring an unrivaled
ingenuity in finding new ways to reduce unit costs      combination of convenience and comfort to long-
and speed cycle times. As a result, it has remained     distance travel. Even more compelling, with a 10
profitable despite a huge downturn in production,       percent improvement in direct operating costs and
and it has continued to invest in the future. Now       45 percent more cargo revenue capacity than com-
BCA is in an excellent position to grow and prosper     peting aircraft, the 787 can accommodate both low
in what promises to be a sustained upturn in the        ticket prices for the traveling public and high mar-
commercial airplane market.                             gins for airlines. That makes it a winner on all counts.
  Airline traffic finally pushed ahead of pre-9/11
                                                        Time to Level the Playing Field
levels in 2004. Outside the United States, many air-
lines have achieved strongly improved profitability.
With load factors at record highs, airlines need        While we continue to create value for our customers
more seats. Boeing is primed to meet that need          through Boeing products, we must also insist on a
with a full family of airplanes--including the most     level competitive playing field in the marketplace.
exciting new airplane since the dawn of the jet age.    Four years ago, Airbus drew on substantial launch
  We launched the Boeing 787 (formerly 7E7)             aid from four European governments to begin de-
Dreamliner on the biggest single order for a new air-   velopment of the A380 super jumbo jet, which is a
plane in Boeing history--a firm order for 50 787s       direct competitor to our 747. Now it is asking for ad-
from ANA (All Nippon Airways), with first deliveries    ditional launch aid to support a competitor to the 787.
scheduled in 2008. At the end of 2004, we reached          Enough is enough. Subsidized competition is
56 firm orders, with airline commitments for a total    unfair competition, and it is clearly forbidden by



Boeing is prepared to make the tough
decisions to ensure real leadership
in the aerospace industry for a long
time to come.
World Trade Organization rules. If Airbus wants to        performance substantially. On a competitive basis,
develop another airplane, it should pay for it out of     this company is better than ever, with great strength
cash from operations, or it should borrow the             both in existing programs and in the leading-edge
money at market rates -- just as Boeing does.             developments that will define the future.
    Founded in 1970, Airbus is 35 years old. It has         Our defense systems business accounted for
been treated as an "infant industry" for far too long.    more than 58 percent of total revenues in 2004. This
It's time for Airbus to play by the rules that apply to   is a business that is broad and deep. And, as long
large, mature enterprises.                                as we execute, I am confident that it will continue to
                                                          grow despite potential defense budget reductions.
Connecting Global Travelers                               At the same time, we are on the cusp of what we
                                                          believe will be a sustained recovery in the commer-
With the launch of our in-flight Connexion by             cial airplane market, with an all-new airplane that
BoeingSM service in 2004, we continued to solidify our    will set whole new standards for operating perform-
leadership in providing secure, high-speed Internet       ance and passenger comfort in long-distance travel.
and e-mail connectivity to travelers worldwide. At          In every way, we are executing the strategy. We
year-end, the service was offered to passengers on        are running healthy core businesses. We are experi-
four global airlines, on routes between Europe, Asia      encing strong growth in adjacent businesses such
and the United States, with additional airlines plan-     as aerospace services. Last, but not least, we have
ning to offer the in-flight service in 2005 and           shown that Boeing is prepared to make the tough
beyond. With in-flight service launched, Connexion        decisions to ensure real leadership in the aerospace
by Boeing expanded its efforts to the open seas,          industry for a long time to come.
conducting a successful three-month maritime
demonstration of its real-time connectivity solution.


Boeing's Future
                                                          Lewis E. Platt
Despite major distractions, people around the com-        Non-Executive Chairman
pany kept their eyes on the ball throughout 2004. As
a result, almost all parts of Boeing increased their


