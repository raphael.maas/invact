Chief Executive Officers Letter
The last year has been among the most challenging in our Companys 56-year
history. The entire media industry is not only coping with the global recession
but, at the same time, learning to navigate some of the most profound shifts to our
business models in decades.
While our results for the year were disappointing, I am happy to report we
are entering fi scal 2010 with a solid portfolio of businesses, a management
team with a proven record of imagination and achievement, and expectations
of a stronger year ahead. We also have a robust cash balance of $6.5 billion,
which allows us to continue developing our great franchises and make prudent
investments for the future.
In the past year, we have reduced our already lean headcount by more than fi ve
percent, disposed of non-core businesses, and streamlined operations. In addition,
we continue to conduct strategic reviews at all of our companies to make ourselves
more efficient. Looking ahead, I believe we should see some upswing in the latter half of the year.
In fi scal 2010, we will depend on advertising for signifi cantly less of our total
revenues, and as I write this letter all other sources are showing robust health.
In fact, I see a great opportunity to grow the business over the next few years by
sticking to our core principle: News Corporation is a creative media company
that attracts and retains customers by giving them the news and entertainment that
they value.
We believe there are no forms of media too old to thrive or too young to take off.
Meanwhile, we are managing all our great franchises to expand their leadership
and extend their reach.
We are a company whose great strength has always been getting ahead of
change, instead of waiting until it is forced upon us. We are always adapting
and developing and looking for ways to improve our quality and expand
our presence.
With every passing day, the media is playing a larger and
larger role in the lives of people across the world. The
teenager in Shanghai watching his favorite show on
his mobile device and the 50-year-old investment
banker in Los Angeles using hers to look for
market updates have one thing in common:
they want the news and entertainment that
refl ects their particular interests they want it
when they want it and they want it delivered
on the platforms most convenient for their individual
lifestyles. They are willing to pay, but they are only
willing to pay for what they value. These trends affect all markets. In developing nations such as India and
China, hundreds of millions of people are entering the middle class and
becoming consumers of news and entertainment. Around the world, people
constantly and rightly demand more and better  whether it be high-defi nition
video recorders to watch their favorite movies at home, news delivered to
their mobile devices, a television channel devoted to the particular team or
sport they follow, or the new 3-D fi lm technologies that will fi nd their way
eventually to television. This new digital world is an enormous opportunity,
and we continue to change News Corporation to take advantage of it.
As we work to provide our customers with the quality they expect from our
brands, we are fortunate to have a terrifi c management team. This team has been
strengthened with the return of Chase Carey as president and chief operating
offi cer. Chase has a long track record in the industry, having served as the highly
successful chief executive offi cer of DIRECTV after long stints at FOX and our
corporate offi ce. He has proved both inside and outside of News Corporation
that he knows how to build audiences, manage complex staffs, and operate
businesses profi tably. I am confi dent Chase will use those talents to ensure the
Company takes full advantage of the incredible opportunities before us across
all of our businesses.
As chairman and chief executive of Europe and Asia, James Murdoch is responsible
for looking for new ways to make the digital future work for us in key global
markets. At News International in London, Rebekah Wade has been promoted
to managing director. Rebekah has proved herself a great executive in 10 years
as editor of The Sun, and she will be taking her success there to all of our
U. K. newspapers.
We have also created a new position within the company  chief digital offi cer
 and brought in Jonathan Miller to fi ll it. He is already making his mark across
our global digital portfolio, tackling head-on the restructuring at MySpace and
our other digital media properties, as well as leading a company-wide effort
to establish a new economic model to transition all of our print properties to
a profi table digital future. For our news businesses, the challenge is clear: the digital revolution
is changing print and television. Also, it will enable us to identify our customers and
their wants and interests more clearly, which is why we are looking to build direct
customer bases wherever possible. The more we know who is watching us on television
or subscribing to our papers, the more opportunities we will have to generate revenue.
For our newspapers, this means that advertising alone will no longer have to pay all the
bills. In addition to carrying advertising, successful newspapers of the future will charge
for their content.
And our stable of newspapers is very successful. In Australia, in addition to the leading
national paper  The Australian  we have many other titles that are close to their
communities and offer many vital local services. In Britain, The Times has made great
progress in taking the lead in attracting upscale, young readers. In the U.S., The Wall
Street Journal is the only newspaper that has actually expanded both its print and
online subscriptions during this recession  making WSJ.com the worlds most successful
paid news site.
Quality journalism is not cheap. Within the industry, people have come to recognize
that when you give your hard-earned copyright away for free, you are undermining your
ability to produce good journalism. I believe the increase we have seen in our Journal
subscriptions since we acquired the paper shows that our readers recognize and are
willing to pay for that quality. Readers are also looking for convenience. Today there can be
no rigid template for all readers  and we may even be beyond the idea
of a typical subscriber. They all have different needs, and many of them
increasingly rely on their mobile devices for personalized services. In addition
to offering the Journal in its traditional print form  where demand is still growing
 you can also read the Journal on your mobile device, on your e-reader, and
on your computer. In the future, we see subscribers paying modest fees to get
the news they want delivered in a way that is most convenient for their lifestyles
 and we believe the more specialized the news, the less modest the fees.
When it comes to television, we are particularly encouraged by the strength
of our cable network programming businesses, where full-year operating income
increased by 32% to $1.7 billion.
Our audience is large and growing. In U.S. cable, our Big Ten Network has
gained distribution on all major pay-TV platforms in the Big Ten markets and is
already profi table in its second season. The FOX Business Network has seen its
subscribers surge to more than 50 million, well ahead of our targets. FOX News
Channel has seen its ratings skyrocket  to the point where it now has as many
viewers as all other cable news networks combined. People increasingly want to
watch FOX News, and its operating profi t was a full 50% higher than 2008. Were seeing even more growth abroad. We believe that having strong
brands with a good foothold in international markets that are just opening up
gives us a chance to grow. At Fox International Channels, we have added more
than three dozen additional channels in Europe, Latin America and Asia.
And profi ts are rising strongly.
When it comes to broadcast television, clearly the model is challenged. Good
programming is expensive and can no longer be supported solely by advertising
revenues. In a three-network universe, broadcasters could offer advertisers a mass
market. Today, with the exception of mega-events such as the Super Bowl and
primetime juggernauts like American Idol, the audience is fragmenting. Viewers
are no longer loyal to particular channels. Their loyalty is to programs. We need
to get more value for our event programming and build new distribution models
that generate real value in a digital world. We are in a good position to
do this given that FOX Broadcasting just proudly fi nished its fi fth consecutive
season as the number one U.S. television network among the most desired
demographic, Adults 18-49.
Our pay-TV business in Europe is a good example of what we need to do in
other parts of the Company: provide consumers with premium products and
advanced technology they cannot get elsewhere for a subscription. In Italy, for
example, despite increased taxes, high competition, and a dismal economy,
SKY Italia has grown its revenues and broadened its market penetration to
4.8 million subscribers. One big reason people are willing to pay for SKY Italia
is that it is the only company in Italy to offer sophisticated digital video recorders,
allowing customers to save their favorite movies and shows in high-definition. In Britain, our equity affi liate BSkyBs lead in high-defi nition personal recording
and broadband services has been a catalyst for increased subscriptions. BSkyB is also the
fastest growing telephony business in the United Kingdom.
In Germany, our equity affi liate Sky Deutschland intends to bring the same high service and
premium products that have done so well in other European markets to perhaps the continents
richest economy.
Its not just Western markets, either. In Turkey, our national television station is growing strongly,
and we expect it soon to be profi table. In the worlds largest democracy, STAR India is now
a true national broadcaster of Hindi entertainment. Of the countrys 20 major language groups,
we now reach half. With our recent launch of STAR Jupiter in Southern India, we will only
increase our regional strength. Finally, in Filmed Entertainment, although we knew we would be below 2008s
record numbers, we did enjoy a very good second half of the year with major releases
beginning with Marley & Me and Taken. More recently, weve had hits with Night at
the Museum: Battle of the Smithsonian, X-Men Origins: Wolverine, and the blockbuster
Ice Age: Dawn of the Dinosaurs, the highest grossing animated fi lm ever outside the U.S.
Together, these three releases have grossed more than one and a half billion dollars to
date, and they are poised to generate robust profi ts in secondary markets in the coming
year. Box-offi ce receipts for the fi rst half of the calendar year are up seven percent
over 2008 industry-wide, thanks in good part to the popularity of 3-D. We have high
expectations for James Camerons 3-D fi lm Avatar and the Alvin and the Chipmunks
sequel  both planned for this coming Christmas. These will be followed by a very
promising and full slate of new films for 2010. We are excited about our future. As a creative media company with global
operations, we are taking a hard look at every one of our businesses with a renewed
sense of urgency and focus. We will work together to change some of our business
models and turn old assumptions on their heads.
A media company lives or dies by the creativity of its people. We may live in a more
competitive world, but the new customers we are gaining for our products  for our
books and newspapers, for our broadcast and cable programming, for our satellite
services, for our fi lms, for our marketing services, for our digital media  tell us that
people value our products and want more. And the talent that we continue to attract
at all levels shows that our Company is a beacon for the smartest and most creative
minds in this business.
If a company works hard to give its customers products they cant get anywhere else
at prices they think are fair, they will reward that company with their business and their
loyalty. News Corporation will honor that loyalty by continuing to create with a belief
that our viewers and readers know best what they want to watch and read.
Rupert Murdoch
Chairman and Chief Executive Officer
News Corporation