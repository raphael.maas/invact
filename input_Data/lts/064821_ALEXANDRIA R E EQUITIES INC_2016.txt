ALEXANDRIA IS PROUD TO REPORT 2016 AS ANOTHER TRULY STELLAR YEAR. Our best-in-class team delivered very strong
operating and financial results while continuing to uphold our industry-leading and award-winning reputation for transparency,
integrity, and accountability. We congratulate the entire Alexandria family on this extremely successful year, which is a direct
result of their collective dedication to consistent and effective execution of our differentiated and multifaceted business model
focused on collaborative life science and technology campuses in AAA innovation cluster locations.
What we began in 1994 as a garage startup with $19 million in seed capital and a unique vision to create a new kind of real estate
company has evolved into a $14 billion blue-chip company built around four core business verticals and a mission that drives
everything we do. Through our four core business verticals of real estate, venture investments, thought leadership, and corporate
social responsibility, Alexandria provides the collaborative and vibrant environments required to support the critical efforts of
innovative life science and technology entities in their development of breakthrough technologies and therapies that endeavor
to prevent, treat, and cure disease; end world hunger; and enhance the quality of life in the communities in which we live and work.
Since our initial public offering (IPO) on May 27, 1997, our unique business model has sustained our competitive advantage and
distinguished us from all other office REITs. Nearly 20years later, we continue to pursue our primary business objectives of maximizing
long-term asset value and total shareholder return (TSR).In 2016, Alexandria achieved an impressive 27%TSR, assuming reinvestment
of dividends, and outperformed various indices, including the FTSE NAREIT Equity Office, the MSCI US REIT, the Russell 2000, and
the S&P 500. Our primary strategic goals for 2016 were part of a multiyear strategy to deliver significant achievements toward per
share growth in funds from operations (FFO), as adjusted; net asset value (NAV); and common stock dividends, which resulted in
significant shareholder value. These goals included delivering (1) solid operating performance from our core operating asset base
generating growth in total revenues, net operating income, and cash flows; (2) disciplined allocation of capital to the development and
redevelopment of highly leased new Class A properties in urban innovation cluster locations; and (3) continued strengthening of our
credit profile and balance sheet. We met these goals for 2016, which are also an extension of our impressive performance in recent
years.During the three years ended December 31, 2016, the Alexandria team�s execution of our business model resulted in exceptional
per share growth in FFO, as adjusted, of 25%; NAV of 59%; and common stock dividends of 24%.
The Alexandria team is also incredibly honored to have been added to the S&P 500 in March 2017, alongside many of the leading
blue-chip companies in the United States. This recognition by S&P Dow Jones Indices further reflects our dedicated team�s proven
track record of successfully executing our business model, which drives Alexandria�s very strong operating and financial performance.
UNIQUE BUSINESS MODEL
Alexandria is the only publicly traded pure-play office/laboratory REIT, and we remain steadfast in our focus on collaborative life
science and technology campuses in AAA innovation cluster locations. These key locations, in close proximity to concentrations of
specialized skills, knowledge, institutions, and related businesses, drive strong demand from highly innovative entities for Alexandria�s
Class A properties. These urban innovation cluster locations are also characterized by high barriers to entry for new landlords, high
barriers to exit for tenants, and a limited supply of existing and future Class A space. Our model draws upon our key relationships
across the real estate, life science, and technology industries, which help us identify and attract new, innovative, and leading tenants,
as well as source additional value-creation real estate projects. Together, the advantages afforded by our business model provide
Alexandria with a stable, focused, and differentiated platform that delivers higher occupancy levels, longer lease terms, increasing
cash flows, higher returns, and greater long-term asset value.
INTERNAL GROWTH: CONTINUING STRENGTH OF DEMAND
Driven by our unique business model focused on Class A properties in AAA locations and robust demand from high-quality tenants,
Alexandria, once again, delivered solid internal growth in 2016. Real estate market fundamentals remain strong in our dynamic urban
centers of innovation, including Greater Boston, San Francisco, New York City, San Diego, and Seattle. These key locations continue
to benefit from solid demand and very limited supply of Class A space, which also includes the limited capacity for other developers
to build new properties.

We also delivered solid same property net operating income growth of 4.7% and 6.0% (cash basis). Our same properties continued
their solid 10-year average cash same property net operating income growth of 5% and outperformance relative to the average for
office REITs. The key drivers, in a typical year, of our cash same property net operating income growth of 5% are (1) 3% growth
from average contractual annual rent escalations of 3%; (2) 1% to 3% growth from solid rental rate increases on lease renewals and
re-leasing of space, including early lease renewals; and (3) stability from our 10-year average occupancy of 95%.
In light of starting 2016 with our minimal contractual lease expirations, our high occupancy of 97%, and our highly leased
value-creation pipeline of development and redevelopment projects, we can proudly report that 2016 was another outstanding
year of leasing activity. We executed nearly 180 leases, aggregating 3.4 million RSF, including 1.0 million RSF related to new Class A
properties undergoing ground-up development and redevelopment. This significant volume of leasing activity for the year highlights
the continued strong demand for our collaborative urban campuses from innovative tenants. Rental rate increases were also solid for
2016 at 27.6% and 12.0% (cash basis) on 2.1 million RSF of lease renewals and re-leasing of space.
In addition to the solid demand for tenancy in our buildings and our favorable lease structure, Alexandria�s operational success is a
testament to our ability to strategically partner with tenants in a way that is solutions focused and distinctive in the real estate industry.
While we provide mission-critical, world-class infrastructure and amenitize it with top-tier services designed to foster collaborative
environments for our tenants, we also bring to bear our entire operating model�a platform that includes our highly experienced
management and underwriting teams, whose expertise and up-to-date research help us understand our tenants� businesses, their
real estate needs, and the trends and opportunities within their crucial industries.
With the benefit of solid fundamentals in the real estate and life science industries in our urban clusters, our unique business model
will continue to drive our strong internal growth outlook for 2017, as exemplified by our guidance for cash same property net operating
income growth of a range from 5.5% to 7.5% and our cash rental rate growth on leasing activity of a range from 6.5% to 9.5%.
OUTSTANDING EXTERNAL GROWTH: $187 MILLION TO $197 MILLION OF INCREMENTAL ANNUAL NET OPERATING INCOME
Our best-in-class team continues to expertly execute on our visible and highly leased value-creation pipeline of new Class A properties
through ground-up development and redevelopment. Our development and redevelopment strategy is to pursue selective projects
in AAA urban innovation cluster locations with substantial pre-leasing where we expect to achieve appropriate investment returns.
Our value-creation projects focus on collaborative and innovative Class A office/laboratory space to meet the mission-critical real
estate needs of our diverse and high-quality tenants. We expect to generate incremental annual net operating income, from deliveries
of our value-creation projects in 2016 and 2017, in a range from $187 million to $197 million, representing tremendous growth of
33% at the midpoint as compared to 2015. During 2016, we completed 10 new Class A properties that are expected to generate
$92 million of incremental annual net operating income. Approximately 20% of this net operating income was recognized in 2016, and
the remaining 80% will drive growth in 2017. As of December 31, 2016, we had seven new Class A properties undergoing ground-up
development that are expected to generate additional incremental annual net operating income in 2017 in a range from $95 million to
$105 million. On average, these new properties are expected to be placed into operation in the latter part of 2017, and as a result are
expected to drive significant net operating income growth in 2018.
In particular, our asset base of Class A properties in urban innovation clusters at the intersection of life science and technology continues
to capture the demand from some of the world�s most innovative entities. We take great pride in the quality of the tenants that have
leased our value-creation projects. These entities, which are focused on developing new therapies and technologies to improve quality
of life, include Bristol-Myers Squibb, Editas, Eli Lilly, Illumina, Juno Therapeutics, Merck, Sanofi Genzyme, Uber, and Vertex.

As of December 31, 2016, we also had two new Class A properties in development and redevelopment that are expected to be placed
into operation in 2018 and 2019 and a pipeline of future value-creation development projects supporting an aggregate of 6.4 million
square feet of ground-up development. These projects represent terrific opportunities for Alexandria to meet additional demand for
new Class A properties from leading life science and technology entities.
STRATEGIC AND VALUE-ADDED ACQUISITION OF ONE OF THE MOST WELL-LOCATED AND LARGEST LIFE SCIENCE AND
TECHNOLOGY CAMPUSES IN THE TOP LIFE SCIENCE INNOVATION CLUSTER SUBMARKET
In November 2016, we acquired One Kendall Square, a 644,771 RSF, nine-building collaborative life science and technology campus.
The iconic Alexandria Center� at One Kendall Square campus is located in the Kendall Square innovation district, the top life
science cluster submarket in the nation. The campus is adjacent to two of Alexandria�s most prominent life science and technology
campuses�Alexandria Technology Square� and Alexandria Center� at Kendall Square. Key innovative tenants at our three campuses
in the Kendall Square innovation district include Biogen, GlaxoSmithKline, Massachusetts Institute of Technology, Novartis,
Sanofi Genzyme, and Takeda.
One Kendall Square was acquired for a purchase price of $725 million. The campus is 97.3% occupied and provides us with a significant
opportunity to increase cash flows through (1) near-term mark-to-market of below-market in-place rents averaging $47 per RSF, with
55% of contractual lease expirations occurring through 2019; (2) conversion of office space into office/laboratory space through
redevelopment; and (3) construction of a new Class A building aggregating 172,500 square feet on the campus.
SIGNIFICANT IMPROVEMENT IN OUR CREDIT PROFILE AND BALANCE SHEET, AND DISCIPLINED ALLOCATION OF CAPITAL
Our team is proud to be recognized for the continued and significant improvement in our credit profile and balance sheet. In February
2017, S&P Global Ratings upgraded our corporate credit rating from BBB- to BBB with a stable outlook. We believe key attributes
supporting our strong credit profile include our unique business model, our highly experienced and tenured leadership team, the
exceptional enhancements to our high-quality asset base of Class A properties, a REIT industry-leading credit tenant roster, our
successful track record of on-time and on-budget development projects, and our consistent growth in net operating income and cash
flows. Additionally, over the past couple of years, we made meaningful improvement in our balance sheet leverage and fixed-charge
coverage ratio and maintained significant liquidity. As of the fourth quarter of 2016 annualized, our net debt to adjusted EBITDA and
our fixed-charge coverage ratio was also very solid at 6.1x and 3.8x, respectively. We also had $2.2 billion in liquidity at the end of 2016.
During 2016, we also raised important long-term capital for investment into the construction of new Class A properties, with solid
cash returns on our total investment in a range from 6.8% to 8.8% for properties delivered into operation in 2016. In June 2016, we
completed an offering of $350 million of unsecured senior notes due in 2027 with an interest rate of 3.95%. In 2016, we also continued
with our strategic sales of real estate focused on attractive and cost-efficient equity capital. We raised $381 million from real estate
sales, including $256 million from two real estate joint ventures.
Alexandria remains disciplined in our allocation of capital to our visible, well-leased, multiyear growth pipeline of new Class A
properties through ground-up development and redevelopment. In 2016, 97% of our precious capital was invested in new properties in
high-barrier and long-term, high-value submarkets�our Cambridge submarket of Greater Boston, our Mission Bay/SoMa and
South San Francisco submarkets of San Francisco, our Manhattan submarket of New York City, our Torrey Pines and University Town
Center submarkets of San Diego, and our Lake Union submarket of Seattle.
Our Board of Directors continues to support a consistent dividend policy of sharing growth in cash flows from operating activities 

with our shareholders while also retaining significant cash flows for investment into new Class A properties through ground-up
development. For the year ended December 31, 2016, cash flows from operating activities after dividends were $130 million and
our FFO payout ratio was solid at 60%. Since 2013, we have increased our annual common stock dividend per share by 24% and
anticipate future growth in operating cash flows to support continued increases in our common stock dividend per share.
Looking forward in 2017, we will continue to be disciplined in our allocation of capital to long-term, high-value urban submarkets and
will prudently manage our balance sheet in order to drive further improvement in our credit profile and long-term cost of capital.
STRONG CREDIT TENANT ROSTER AND CONTINUED SOLID LIFE SCIENCE INDUSTRY FUNDAMENTALS
Alexandria continues to strengthen our high-quality and diverse tenant base, with 53% of our total annual rental revenue and 81% of
annual rental revenue from our top 20 tenants, generated from investment-grade tenants, as of December 31, 2016. These two REIT
industry-leading statistics reflect the February 2017 acquisition of ARIAD by Takeda, an investment-grade company. The impressive
quality, diversity, and depth of our tenant base is a true testament to the distinguished underwriting capabilities of Alexandria�s Science
& Technology team, a talented group with deep knowledge and experience in the life science, technology, and real estate industries.
Moreover, remaining lease terms for our top 20 tenants are also solid at a weighted average of 13.4 years. The strength of our tenant
base combined with long remaining lease terms provide Alexandria with high-quality, diverse, and stable cash flows.
There are a number of favorable government initiatives supporting innovation in healthcare. The 21st Century Cures Act, which was
enacted by Congress in 2016 with broad bipartisan support, was designed to accelerate the discovery, development, and delivery of
new therapies and cures. The hallmark legislation authorized $6.3 billion over 10 years in additional research funding, including funding
for the National Institutes of Health to spur an increase in translational research, external innovation, and collaboration opportunities,
and for the U.S. Food and Drug Administration (FDA) to engender faster and more efficient drug review processes. It is important
that we as a nation continue to invest in biomedical innovation to ensure that the United States continues to maintain its leadership
position in producing life-saving treatments and cures for patients.
Reduced FDA review time and beneficial legislation enables innovative new treatments to reach patients faster, and of the new drug
approvals the FDA granted in 2016, 73% were received by Alexandria tenants. We look forward to future FDA new drug approvals for
our tenants as companies such as Adaptive Biotechnologies, Bayer, and Editas push forward development of emerging technologies,
including CRISPR-based gene editing and immuno-oncology therapies.
In recent years, the life science industry has continued to drive innovation as well as the specific advancement of new therapies with
significant continued annual research and development (R&D) investment in excess of $200 billion. The top 20 biopharmaceutical
companies, 15 of which are Alexandria tenants, have $220 billion in total cash across their balance sheets and continue to invest
heavily in R&D. Given their strong cash flows and eagerness to bolster clinical pipelines, biopharmaceutical companies continue to
actively partner and pursue mergers and acquisitions, which have benefitted many of our tenants. For example, Stemcentrx, both an
Alexandria tenant and an investment of Alexandria Venture Investments, was acquired by AbbVie for up to $10 billion, based on its
early success targeting cancer stem cells. AbbVie now plans to expand its presence in our South San Francisco submarket based on
this acquisition. Additionally, Medivation was acquired by Pfizer for $14 billion, and ARIAD was acquired by Takeda for $5 billion.
Never before has the understanding of disease biology been so vast, as new modalities have driven us to the precipice of discovering
better, faster, and more effective therapies for the most devastating diseases, including Alzheimer�s disease, rare genetic disorders, and
cancers. Advances in immuno-oncology, gene therapy, gene editing, and next-generation sequencing have just begun to scratch the

surface of their paradigm-shifting potential. The increasing convergence of life science and technology has created new opportunities
to leverage digital platforms and big data to focus on disease prevention and diagnostics, to make better informed clinical decisions,
and to improve patient outcomes. We have seen this directly in our cluster locations with the moves of IBM Watson Health and Verily
into our Class A properties, where they have partnered with numerous life science companies, including other Alexandria tenants
such as Biogen, Celgene, Novartis, Pfizer, and Sanofi. We expect the intersection of life science and technology companies to only
increase, as they represent the leading innovation sectors, and as life science companies increasingly look to leverage technology in
their pursuit to accelerate solutions to major healthcare challenges.
Proximity to successful early-stage innovators and academic and medical research institutions is key to accessing novel technologies
as biopharmaceutical companies increasingly rely on external innovation to bolster their pipelines. More than 50% of revenues of the
top 20 biopharmaceutical companies were driven by assets sourced outside of the parent company, primarily from entities located
within our core urban innovation cluster locations. The fast pace of scientific discovery has made it increasingly challenging to innovate
in isolated environments. In order to stay competitive, companies need to access innovation and tap into the high concentrations of
talent, expertise, and knowledge found in Alexandria�s urban innovation clusters.
IMPACTFUL THOUGHT LEADERSHIP
In addition to our core business of real estate, Alexandria�s thought leadership core business vertical is also critical to supporting
our mission. Our unparalleled thought leadership programs engage our world-class network in transformative discussions and
collaborations that help drive the discovery and development of novel, cost-effective therapies that will save lives, manage and cure
disease, address hunger and agricultural sustainability, and significantly reduce the economic burden of disease on society.
In 2016, we held two events in our groundbreaking Alexandria Summit� series�Infectious Diseases 2016 and Healthcare Economics
2016. The Alexandria Summit convenes a diverse group of key stakeholders�from the pharmaceutical, biotechnology, agribusiness,
technology, medical, academic, venture and private equity capital, philanthropy, patient advocacy, and government communities�to
drive progress in addressing the most critical challenges in global healthcare, agriculture, and the environment. To date, Alexandria
has made noteworthy advances as a result of the Alexandria Summits. We will continue to create meaningful opportunities for
collaboration that shape the future of innovation and improve human health.
COMMITMENT TO CORPORATE SOCIAL RESPONSIBILITY
As part of our commitment to make a positive impact on the world, Alexandria�s fourth core business vertical, corporate social
responsibility, focuses on sustainability and philanthropy. Alexandria, a Platinum Member of the U.S. Green Building Council, is a
recognized industry partner that pursues and promotes resource-efficient and health-promoting business practices that enhance
employee performance for our innovative tenants. Of the annual rental revenues in Alexandria�s portfolio, 51% are expected to be
generated by LEED�-certified buildings upon completion of in-process certification projects, including 41 LEED-certified projects
aggregating 8 million RSF. Highlighting our dedication to sustainable and healthy environments on our campuses, in November 2016,
we became the first REIT to be named a First-in-Class Fitwel Champion, for promoting health and wellness in the workplace, in addition
to the first company of any kind to earn Fitwel building certifications. We are thrilled to be recognized as the first REIT to achieve this
award and look forward to continuing to build our sustainability program.
While through our primary core business vertical we build highly sustainable and collaborative environments that enable our tenants
to develop innovative solutions that advance human health and improve the way we live, it is through our strategic corporate giving that
we can more directly support impactful medical research organizations that promise the next generation of treatments for some of

the world�s most challenging diseases; fund science education programs and organizations that cultivate future scientific leaders;
and back military support services that ensure the health and safety of the brave men and women who defend our nation. Likewise,
our volunteer program engages Alexandria team members across the country to work to improve the health and vitality of our local
communities. Additionally, individual Alexandria team members lend their time to local non-profit organizations throughout the
year and also dedicate pro bono service hours to benefit non-profit organizations in our local communities.
All of our sustainability and philanthropic efforts together make up our important and impactful corporate social sustainability core
business vertical. We are dedicated to continuing to improve and grow this vertical going forward.
BEST-IN-CLASS TEAM
People are our most important asset, and we are deeply thankful for our dedicated team of employees that directly contributed
to Alexandria�s strong performance in 2016 and to our success since the founding of the company in 1994. Our experienced team
brings an impressive and diverse breadth of knowledge, understanding, and expertise in real estate, life science, and technology.
Alexandria�s senior management team averages more than 25 years of real estate experience, including more than 13 years with
Alexandria. Our sophisticated management team also includes regional market directors with leading reputations and longstanding
relationships with all major real estate, life science, and technology industry stakeholders within their respective innovation
clusters. They lead Alexandria�s fully integrated regional teams, which have expertise in design, leasing and asset management,
construction and development, laboratory operations, finance, and sustainability.
LOOKING AHEAD
This year, as an S&P 500 company, we celebrate our 20th anniversary on the New York Stock Exchange and look to the future
with confidence that comes from knowing that our business is well positioned. We at Alexandria are never comfortable doing what
we have always done with a few incremental changes. In point of fact, we make bold strategic decisions, which are innovative and
profound. One ideal example was our decision in 2004 and 2005 to pursue the cluster campus strategy in Cambridge, Mission Bay,
and New York City. This crucial strategic decision presaged today�s trend of urbanization fostering innovation and collaboration,
and has resulted in superior results and total return for our shareholders.
Movingforward, what will set us apart will be our highly experienced and talented team, enabled by our unique culture. By embodying
our principles of caring deeply and working passionately in everything we do, we expect to drive progress and innovation. We are
making investments to build a culture that will drive excellence. Our colleagues are fully aligned in purpose and our leadership team
is positioned to guide us toward success well into the future.
Respectfully,
Joel S. Marcus Dean A. Shigenaga
Chairman of the Board, Executive Vice President,
Chief Executive Officer Chief Financial Officer
& Founder & Treasurer