LETTER TO SHAREHOLDERS

In completing my first year as President and CEO of
MEMC, I am reminded of the Chinese character for
crisis, which has two basic meanings: danger and
opportunity. Clearly, we faced both in 2009.
There is no doubt that it was a diffi cult and challenging
year for MEMC, for the semiconductor and solar
industries and for the worldwide economy. Polysilicon
and wafer prices were down signifi cantly and semiconductor
units were down for an unprecedented second
consecutive year. The reduced demand and diffi cult
pricing, which were offset to some degree by increased
market share, resulted in MEMC sales declining
by over 40%, and a net loss of $70 million.
However, we know that market declines also create
opportunities, and we made signifi cant progress
in strengthening our company for the future. This progress
is the direct result of the commitment to the longterm
priorities we identifi ed last year when I became
CEO. Here are some examples:
Priority: Enable customer success, with quality products
and timely delivery.
Progress: At every level of our company we have been
focusing on our most valuable asset  our customers.
Our efforts include, among many others, expanding our
sales and fi eld applications engineering staff worldwide,
launching an expansion in Taiwan and announcing
300mm wafer production in Korea to support strategic
customers. Our customers are recognizing these efforts,
allowing us to double our semiconductor market share
from the 2009 fi rst quarter to the fourth, giving us
momentum entering 2010.
Priority: Drive technology innovation.
Progress: Among our efforts here, we are launching
internal solar wafering production to reduce our risk
and to accelerate the drive to reduce solar energy costs
to grid parity.
Priority: Retain, develop and recruit the best talent in
the industry.
Progress: We continue to make progress on this front. A
few notable examples include the addition of Tim Oliver
as Chief Financial Offi cer, and the implementation of a
new organizational structure, led by three strong leaders,
Shaker Sadasivam (Semiconductor Materials), Ken Hannah
(Solar Materials) and Carlos Domenech (SunEdison).
These leaders in turn recognize the importance of talent
development within their organizations.
Priority: Reduce product cost.
Progress: We have launched strategic cost reduction
initiatives at all plants. Two standout examples are good
early improvement in Pasadena, Texas, and procurement
cost reductions of $100 million on an annualized basis.
This is a great start, but there is more work to be done.
Priority: Expand our customer base.
Progress: We have broadened our solar wafer customer
base, signifi cantly reducing risk and adding stability to
the business. Among the top 25 solar customers worldwide,
we now serve 10 compared to only two last year.
And we hope to add at least six more in 2010.

Priority: Participate in downstream solar opportunities
through potential investments or acquisitions.
Progress: We took a major step when we acquired
SunEdison, a developer of solar power projects and
North Americas largest solar energy services provider.
This adds a third engine of growth for MEMC and
quadruples our served available market. We also
completed a 50-megawatt power plant in Germany
as part of a joint venture with Q-Cells, and generated
a 20% return on the overall project.
While a lot has changed over the past year, our
fundamental strengths, challenges and strategic goals
remain largely the same. By focusing on these priorities,
we can continue to grow revenue, improve profitability
and expand our market share over the long term. The
key now is to execute flawlessly.
T his is a tremendous company with powerful brands,
great products and a bright future. Now is the time to
focus more than ever on excellence.
I thank you for your support and look forward to
showing you how great MEMC can be.
Sincerely,
Letter To Shareholders
Ahmad Chatila
President and Chief Executive Officer