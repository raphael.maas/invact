                                                         To Our Shareholders




                                2001 was a profoundly challenging year for DuPont. We faced the worst economic environment
                                in two decades, unusually high energy prices, and unfavorable currency exchange rates. Yet we
                                remained focused on executing our plans and implemented important strategic actions. As we
                                begin 2002, we are still on track to achieve our long-term goals.
                                Because of the nature of our businesses and the many markets we serve, DuPont was one of the
                                first to see the economic troubles on the horizon. We made immediate adjustments to deal with
                                recession even as debate continued about whether or not the United States was headed for one.
                                We worked to hone our competitive strengths and solidify our customer relationships so that we
     "I remember well that
                                will be able to outpace the competition when the upturn inevitably arrives.
 Toyota used DuPont paint
 for our first passenger car,
                                There were bright spots in the year. The sale of DuPont Pharmaceuticals for almost $8 billion to
the "Toyota Crown" in 1955.
                                Bristol-Myers Squibb was very favorable for our shareholders. DuPont Pharmaceuticals was an
Toyota continues to pursue
                                excellent business with an attractive pipeline of products. However, it was a small company
           challenging new
                                competing in a market dominated by giants. This infusion of cash provides us with the flexibility
technologies everyday, just
                                to complete the $2.5 billion stock repurchase program announced in 2000, to reduce net debt,
    like DuPont who totally
                                and to further invest in our businesses.
   changed the automotive
                                We ended 2001 with our strongest balance sheet in nearly two decades in the worst economic
        industry in the past
                                environment in two decades. Business Week listed us among 20 companies with the financial
century. We expect DuPont
                                strength and flexibility to take advantage of the bear market's acquisition opportunities. In fact,
 to create a new paradigm
                                we made seven selective acquisitions during the year. These new businesses bring technology
    in our industry through
                                and know-how to markets that we believe will be important growth areas for DuPont in the future.
innovative materials in the
              new century."     Although the year's performance was very disappointing with underlying earnings per share
                                declining 56 percent, we are confident that we possess the correct strategies and are doing the
      Dr. Shoichiro Toyoda,     right things to continue the transformation of our company. We know where we are going and we
       Honorary Chairman,       have the plans in place to get us there. We will continue on the course we have charted, and we
  Toyota Motor Corporation      believe we are well positioned to realize our objective of creating an exciting, profitable and
                                growing 21st century DuPont.
                                Despite the challenges we faced during the year, we stayed focused on our core values. DuPont
                                received safety, health or environmental awards in Brazil, Canada, Taiwan, Singapore, the United
                                Kingdom and the United States. DuPont was one of only two chemical companies named to
                                London's Financial Times Stock Exchange list of 100 socially responsible global companies.
                                The high regard in which DuPont is held by our customers and communities around the world is
                                a reflection of the values that drive us. In the wake of the ongoing corporate financial scandals in
                                the news, we reaffirmed uncompromising commitment to our core values, to strong and active
                                oversight by our Board of Directors, and to rigorous control processes driven by management.

                             

                                We responded to the economic downturn with a series of definite actions to generate revenue in
                                2001 while simultaneously improving our competitive position to maximize growth when the
                                economy recovers, including:


                                                                                                      "DuPont delivers an
                                                                                                      outstanding service to
                                                                                                      their customers in
> In April, we aligned our capacity to meet reduced demand. We eliminated more than 5,000             relation to product
    positions to accomplish the alignment, or 6 percent of our worldwide workforce.                   development and
                                                                                                      involves their customers
    We eliminated or offset 100 percent of the residual cost associated with the sale of
>
                                                                                                      in developing a
    DuPont Pharmaceuticals.
                                                                                                      professional
    We completed 230 cross-business improvement projects which yielded $20 million pretax
>                                                                                                     partnership. What
    earnings in 2001 and which will yield $90 million pretax in 2002.                                 impressed me most with
                                                                                                      the DuPont organization
    We concentrated on increasing market share in developing countries, including China
>
                                                                                                      is their integrity and
    and India. This initiative yielded $40 million in annualized pretax earnings in 2001.
                                                                                                      their commitment to
    We purchased $400 million of materials and services via Internet reverse auctions, saving
>                                                                                                     customers.
                                                                                                               "
    over 15 percent. We expect to make about $800 million of reverse auction purchases in 2002.
                                                                                                      Finn Johnsson, CEO,
     Mlnlycke Health Care AB
Our overall mission is sustainable growth  creating value for our shareholders and society while
reducing our environmental footprint. We are continuing the transformation of the company
to accomplish that.
The most profound change was our announcement in early 2002 of the creation of five market
growth platforms for organizing our businesses. These are DuPont Electronic & Communication
Technologies, DuPont Performance Materials, DuPont Coatings & Color Technologies, DuPont
Safety & Protection, and DuPont Agriculture & Nutrition. We announced the creation of a new
subsidiary, DuPont Textiles & Interiors, with the ultimate intent of separating these businesses by
year-end 2003, market conditions permitting.
The businesses in DuPont Textiles & Interiors  nylon fibers, polyester fibers and Lycra  brand
fibers  have been bedrock businesses for our company. Employees of our fibers businesses have
long been recognized throughout the industry for their achievement and dedication. Likewise, we
count among our most honored customers the textile market firms who have worked so closely
with DuPont for so many decades. However, these businesses participate in industries that have
                                                                                                      "DuPont has been able
changed dramatically in recent years. Our new subsidiary will be the largest integrated fibers
                                                                                                      to propose cutting-edge
company in the world. We believe that separated from DuPont it will have the most flexibility to
                                                                                                      materials wrapped in
respond as the industry restructures, new opportunities to reduce costs and strengthen customer
                                                                                                      intelligent brand
commitments, and the ability to lead innovation and maximize its long-term potential.
                                                                                                      packages. DuPont's
Our new growth platforms will enable us to maintain our progress in the three strategic thrusts I     science and technology
have discussed in the Chairman's Letter each year since 1998 and which remain central to our          inform us as to what is
business plans and objectives. Here are some highlights in each area from the past year.              possible, whereas
                                                                                                      DuPont's branding
Integrated Science. The technology platforms that drove DuPont's growth for the last six decades
                                                                                                      informs us as to what is
 polymer chemistry and chemical engineering  are maturing. They remain vital to our future, but
                                                                                                      imaginable."
cannot alone drive growth over the long term. We are revitalizing our science by investing today in
biology-based technology platforms (sometimes called"life sciences") that can support significant
                                                                                                      Alberto de Conti,
long-term future innovation and extend the market potential of our existing technology base.
                                                                                                      Innovation Team Leader,
                                                                                                      Levi Strauss




                                                                   3
            "DuPont brings me
        peace of mind. I know
              that when I use a
                                            In July, we launched the DuPont TM SolaeTM brand of soy protein which is available in 8 th Continent TM
       DuPont product I don't
                                            soymilk, now in select U.S. dairy cases. Made with a high sucrose soybean developed by DuPont,
           have to worry about
                                            8 th Continent TM is the first product from the joint venture between DuPont and General Mills.
      effectiveness  it works.
                                            Knowledge Intensity. We have always provided a unique DuPont "value add"with the materials
       DuPont continues to be
                                            we sell, and customers have long depended on our knowledge and technical know-how. In
            a leader in product
                                            recent years, however, we have changed our business models to capture more of the value
                 innovation and
                                            created by our innovation, our knowledge of markets, our understanding of end-user needs,
                demonstrates a
                                            and our strong brands.
                 commitment to
                                            An example of this is in our businesses related to safety and protection where our knowledge
       providing farmers with
                                            base is unparalleled. Our safety consulting business tripled in 2001. There are now more than
      safe, reliable products."
                                            100,000 employees of other companies covered under DuPont SafeReturnsTM contracts. The
                                            creation of the DuPont Safety & Protection growth platform will better leverage our
                 Vince Ferrante,
                                            knowledge and our $3 billion worth of safety and protection products and services. This will
                          Grower,
                                            make it easier to respond to customer needs, many of which are now more apparent after
       Santa Maria, California
                                            September 11.
                                            Productivity. We worked rigorously to improve productivity. The success we are experiencing
                                            reflects personal accountability for results and the tenacity to see projects through to completion.
                                            In 2001, our productivity efforts resulted in fixed-cost reductions more than offsetting inflation.
                                            Total fixed costs declined approximately $400 million (including divestitures and acquisitions).
                                            We also continue to reap the benefits of Six Sigma. We did not invent Six Sigma, but few com-
                                            panies have embraced it as effectively as DuPont. By the end of 2001, one in every six employees
                                            had participated in a Six Sigma project. This disciplined, data-based, problem solving methodology
                                            is becoming ingrained in the DuPont culture.
                                            I could list hundreds of Six Sigma examples, but consider two from DuPont Performance
                                            Coatings. At our facility in Tlalnepantla, Mexico, we saved nearly $3.5 million in operating




            expense by cutting cycle time almost in half for paint manufacturing. Another project, related
            to commercialization of a new automotive clearcoat product, generated $20 million in revenue.

         

            As I conclude this letter, I am keenly aware that 2002 is no ordinary year. We have taken dynamic
            transformational steps to ensure DuPont's growth in the 21st century. Our new growth platforms,
            more tightly focused on markets and technologies, will enable faster execution and an improved
            capability for innovation, acquisitions and value creation.
            The willingness to undertake profound change to remake the company is in the"DNA"of DuPont.
            Our predecessors never shrank from it. The constants have been our core values  safety, health
            and the environment, ethics, and respect for people. This is especially important to remember this
            year as we celebrate the 200 th anniversary of DuPont.
            In our 200th year, the many people who have worked for this company have good reason to be proud.
            The scientific innovations we have brought to the world have helped generations of people around
            the globe live more productive and prosperous lives. They have joined in the cause of freedom
            during conflict, and they have amplified hope and happiness in times of peace. Our products have
            been to the bottom of oceans, to the poles, to the highest mountains, to the Moon, Mars and Jupiter.
            Yet they are as close to us as the everyday world of our kitchens and cars, our farms and fashions.
            Transforming a science company is a living work. It takes flexibility and imagination as well as a
            favorable business environment and market opportunities. We will continue to commercialize
            science in order to provide solutions for our customers and to fuel tomorrow's dreams. For 200
            years, that's what DuPont has always done.



            March 1, 2002                                                                     Chad Holliday, Chairman and CEO


