
To Our Shareholders and Employees:

We are very pleased to present our 2005 Annual Report and file our       includes our focus on low-cost sourcing and investments in India,
2005 Form 10-K with the Securities and Exchange Commission.              Mexico, Eastern Europe and China as well as increasing our global
                                                                         diversity. This builds upon our expansive global platform already in
Flowserve's mission is to move, control and protect the flow of          place to support our worldwide customer base.
materials in some of the world's most critical industries (such as oil
and gas, chemical, power and water). We put that commitment in           Process excellence was driven by our Continuous Improvement
motion everyday in 2005. From providing support to our customers         Process and incentive programs designed to deliver maximum
and employees who were affected by Hurricanes Rita and Katrina,          productivity and customer fulfillment. Improvements in on-time
to the day-to-day delivery of our products, services and solutions,      delivery, lower material costs and reduced time-to-market are key
Flowserve employees have remained focused on demonstrating our           goals. We also began an initiative to migrate our older information
core values and providing exceptional service at every possible          technology systems to a standard, more efficient platform. This will
opportunity. Our more than 13,000 employees realize that our             prepare the company for a less complex operating environment to
commitment and partnership to our customers is what sets                 enable additional productivity and process improvement.
us apart from our competitors and allows us to focus on continued
                                                                         We are also building an organizational capability that centers on
organic growth and delivering a world-class, consistent,
                                                                         ethics, learning, leadership and an empowered culture focused on
Flowserve experience.
                                                                         delivering consistent performance and results.
Our vision is to distinguish ourselves to our customers by delivering
                                                                         These six strategic initiatives continue to be the foundation for our
flow management products and solutions as a global, unified,
                                                                         current and future plans for success.
customer-centric business. In 2005, we continued to support this
vision by focusing on our six key strategies:
                                                                         CORE MARKETS
                                                                         Since our last annual report, we have seen continued strength in our
We delivered organic growth in bookings of more than 13%, excluding
                                                                         core markets, especially the oil and gas, power and chemical sectors.
currency, with new products and services and expansion into emerging
                                                                         The oil and gas markets specifically have seen a significant increase
geographies that will create future aftermarket revenue opportunities.
                                                                         in capital expenditures for exploration and production driven by higher
Our active expansion efforts in China, India, Russia and the Middle
                                                                         oil prices and increased demand. This coupled with the urbanization
East have been critical to our success. We continue to drive a unified
                                                                         of emerging market countries is driving a continued demand for
organizational presence under the Flowserve brand name as a means
                                                                         Flowserve products and services. This market strength, combined
to be seen as a broader business partner by our customers.
                                                                         with our portfolio of leading products, global manufacturing footprint
We strengthened our core business through strategic portfolio            and dedicated employees continue to provide increased opportunities
management of our products and services. The divestiture of the          for profitable growth.
General Services Group and smaller bolt-on acquisitions were in-line
with our focus on improving our current platform and maintaining our     FINANCIAL PERFORMANCE
strategic initiatives.                                                   The following are some key financial highlights of 2005:

We infused innovative technology into our products and services in        Booked record orders of $3 billion representing 13% growth,
order to differentiate ourselves from our competitors and help             excluding currency, compared with 2004
maximize our customers' ability to expand revenues and margins. Our       Improved consolidated gross margin percentage to 32% from 30%
Intelligent Pump Series (IPS), IPS Tempo, was recently showcased and       in 2004, reflecting successful operational excellence initiatives
is an example of our cross-divisional technology focus.                   Completed $1 billion debt refinancing resulting in a more
                                                                           flexible capital structure and savings of more than $20 million in
We increased the globalization of our business in emerging                 interest expense
geographies as a means to improve our competitiveness and help
secure the future of our company. The increased globalization also
 Increased net earnings from continuing operations to $0.82 per          
  diluted share from $0.46 in 2004
 Generated strong cash flow, which was used to repay
                                                                                               
  $78 million of debt and other financing obligations, and
  for other strategic purposes                                                 
 Divested the General Services Group as part of our ongoing                                                                        
                                                                                          
  portfolio management
 Supported the successful February 2006 completion of our 2000                                                           1398                   10.3%
                                                                                   
  through first quarter 2004 financial restatement and the filing of                                                                     8.3%
                                                                          
  the 2004 Form 10-K with the SEC                                                                     


We are very pleased to report that full year 2005 bookings increased
13%, excluding currency, to a record $3.02 billion compared with
$2.66 billion in the prior year. Year-end 2005 backlog stood at a
record $994.1 million compared with $836.4 million at the end of the        

prior year reflecting, in part, the strength of customer investments
being made in the oil and gas and power businesses. Sales increased
7% to $2.70 billion in 2005 compared with $2.52 billion in 2004.
                                                                                                
In association with the strong bookings and sales growth, Flowserve
                                                                               
has also experienced margin expansion in all segments. Gross profit
                                                                                                                                   
from continuing operations in 2005 increased 14% to $861.8                                 
million, compared with $758.6 million in 2004. These increases                                                            894
                                                                                    852                         839
primarily reflect cost savings resulting from the company's ongoing                                                                              10.0%

continuous improvement initiatives, improved pricing discipline and          765                                                         7.8%
                                                                                                      758
                                                                                                                                 6.6%
increased sales, which favorably impacted the absorption of
fixed costs.

Operating income increased to $190.1 million from $161.5 million
driven by gross margin contribution and fixed cost leverage, which was     
partially offset by higher SG&A expenses resulting from an increase in
compliance costs, professional fees and management transition
expenses. This higher level of unusual expenses has offset some of the
efficiency improvements and productivity gains we have been making
                                                                                             
in our operations. In response, we have strengthened the global finance
organization with people, training and improved controls to support our        
operations worldwide.
SEGMENT PERFORMANCE                                                                                                         
Each of the Flowserve business segments experienced improvements            
as seen in the attached metrics.

LOOKING AHEAD
In the near term, the company continues to focus on becoming fully
current on our SEC filings by the end of the third quarter.
                                                                          
The leading indicators in all of our markets continue to signal growth
opportunities in 2006 and beyond. As previously announced, we began
the first quarter of 2006 with a 34% year-over-year quarterly increase
in organic bookings. This represented a record number of orders for
the company.
                                                                          Lewis M. Kling
Our outlook continues to be strong. We believe that the combination of    President and Chief
positive internal and external factors impacting Flowserve, as well       Executive Officer
as a dedicated workforce, operational excellence initiatives, a leading
portfolio of products and solutions and our organizational capability
provide us the strength and competitiveness to continue to expand our
leading position in our industry.
                                                                          Kevin E. Sheehan
                                                                          Chairman of the Board
