At Bristol-Myers Squibb we come to work every day with one
mission in mind: to discover, develop, and deliver transformational
medicines to help patients facing serious diseases. This is what we
do. This is who we are.
In 2018, performance was strong across the company and created
great momentum for our business. We expanded the indications
of our oncology portfolio with the important approval of Opdivo
plus low-dose Yervoy for the treatment of renal cell carcinoma
(RCC) in the U.S. and Europe. We reached an important milestone
with Eliquis in the U.S., taking a leadership position in the prevention
of stroke for patients with atrial fibrillation. We advanced a broad
and deep pipeline in oncology, cardiovascular disease, fibrosis,
and immunoscience. And we continued our efforts to be a good
global citizen through our green initiatives, by investing in our
local communities, and by supporting those in need.

2018 Results

2018 was marked by outstanding commercial execution. We
ended the year with $22.6 billion in revenues � a nine percent
increase over 2017. This was due to growth in nearly all of our
priority medicines. Our results translate directly into more medicines
delivered to more patients. Our strong operating performance
resulted in 2018 GAAP earnings per share of $3.01 and
Non-GAAP earnings per share of $3.98, which represents
year-over-year growth of 32 percent.
Transforming cancer care remains a priority for our company as we
focus on increasing survival rates for more patients across a greater
range of tumor types. Our oncology franchise performed well in
highly competitive markets, with Opdivo now approved in nine
tumor types in 17 indications. In 2018, Opdivo revenue grew
by 36 percent, ending the year with $6.7 billion in global sales.
Opdivo continues to maintain and grow its market share in its
approved indications while expanding into new tumors and earlier
lines of therapy. We saw significant growth from the launch of
Opdivo in adjuvant treatment in melanoma and the approval of
Opdivo plus low-dose Yervoy for the treatment of first line RCC in
the U.S. Additionally, Opdivo plus low-dose Yervoy were approved
by the European Medicines Agency for first line RCC in January
2019, further expanding our Immuno-Oncology (I-O) combination
therapy as a treatment option for patients with RCC in markets
across Europe.
Opdivo distinguished itself as the first Immuno-Oncology therapy
approved in China in 2018. This milestone is a tribute to the focused
efforts of our teams in both the U.S. and in China to ensure speed
in delivering this important medicine to cancer patients in China.

Eliquis continued to grow in the U.S. and internationally, with
revenues totaling $6.4 billion, representing a 32 percent
increase over 2017. In 2018, we achieved a major milestone
when new prescriptions for Eliquis surpassed warfarin, the
previous standard of care for stroke prevention in atrial fibrillation,
to become the number one prescribed oral anti-coagulant
in the U.S. Eliquis continues to be the leading novel oral
anti-coagulant globally for stroke prevention in patients with
atrial fibrillation.
The Eliquis clinical profile is supported through our continued
investment in real-world data analysis. In 2018, we published
the ACROPOLIS (Apixaban ExperienCe Through Real-WOrld
POpuLatIon Studies) study, which now includes more than one
million patient records, and is the largest body of real-world
evidence in existence for analyzing the effectiveness and safety of
anticoagulants, including Eliquis, among patients with non-valvular
atrial fibrillation and venous thromboembolism.

Delivering for Tomorrow
The scientists of Bristol-Myers Squibb understand the urgent
needs of patients for new treatment options. They are hard at work
on the next generation of transformational medicines for patients
as we continue to invest in the research and development of new
treatment options to address significant unmet medical needs.

In 2018, we made significant progress in advancing our
Innovative Medicines pipeline, which includes clinical programs in
immunoscience, fibrosis, and cardiovascular disease. Results from
the Phase 2 study of our selective TYK2 inhibitor for the treatment
of moderate to severe plaque psoriasis were published in the New
England Journal of Medicine in September, and registrational
trials in this indication are well underway. We continue to advance
our fibrosis portfolio with our lead compound in the area, FGF21,
for the treatment of nonalcoholic steatohepatitis (NASH), which
is currently enrolling a Phase 2b trial. In heart failure research,
our nitroxyl donor asset is currently in Phase 2 trials and we look
forward to data readouts in the coming year. Additionally, we
signed a collaboration agreement with Janssen Pharmaceuticals
for the development of our Factor XIa inhibitor for secondary
stroke prevention and initiated a Phase 2 trial.
Our broad development program in oncology includes the
important and growing body of work in translational medicine,
giving us greater insights into which patients can benefit the most
from our medicines. In 2018, we opened a new research center in
Cambridge, Massachusetts to focus on resistance to treatment
with current Immuno-Oncology agents, and we look forward to
leveraging the rich scientific ecosystem of the location to drive
innovation in this field.
Our clinical programs are focused on expanding the benefits of
our Immuno-Oncology portfolio and moving into earlier lines of
therapy, including the adjuvant setting which follows surgery to
prevent recurrence.
In 2019, we expect to see a number of data readouts from our lung
cancer program as well as from studies of Opdivo for the treatment
of glioblastoma, head & neck, liver and renal
cancers. Starting in 2020, we will begin to see
results from studies of adjuvant treatment that
will bring greater understanding of the role of
Opdivo in earlier stages of disease. These will
include results in adjuvant melanoma, bladder,
and esophageal cancers next year with results
in other tumors such as renal, liver, and lung
cancers coming in subsequent years.

With innovation as our focus, we believe
that tomorrow�s medicines will come from
our own internal efforts as well as the rich
ecosystem of scientific innovation that exists
outside our company. In 2018, our business
development teams were hard at work to find
those medicines. This year, we entered into a
global strategic collaboration to jointly develop
and commercialize Nektar Therapeutics�
lead Immuno-Oncology program, NKTR214, in combination with Opdivo and Opdivo
plus Yervoy. The Nektar program provides
Bristol-Myers Squibb with a third validated
mechanism in Immuno-Oncology, providing us
the opportunity to build on our experience with
Opdivo and Yervoy.


Throughout the year, the Bristol-Myers Squibb
Foundation continued to build capacity and
expand access to care for underserved
populations by supporting innovative programs
to train health care providers and mobilize
communities in the fight against disease.


This effort included extensive programs in
the U.S. to remove barriers and increase
access to specialized care for vulnerable
populations, improve lung cancer awareness
and care, and support the reintegration of our
returning veterans and their families. It also
included significant innovations in access to
cardiovascular care.

COUNTRIES

The Bristol-Myers Squibb Foundation is also
working to bring quality cancer treatment to
African communities, focusing on cervical and
lung cancer in southern and east Africa, and
pediatric cancers in southern and east Africa
and west Africa. Leveraging the extensive
learnings from 20 years of its �SECURE THE
FUTURE� program, the first and largest private
initiative to address HIV and AIDS in southern
and east Africa, the Foundation is working
with community partners to promote cancer
screening, train healthcare providers, and
change the outcomes for cancer patients.
Following the launch of Project ECHO for
cancer in the U.S., the Foundation is funding
an expansion of the tele-mentoring program to
rural hospitals in South Africa.
We recognize that helping to improve access
to our medicines is of critical importance to
ensure all patients benefit from our innovations.
Through the Bristol-Myers Squibb Patient
Assistance Foundation, under- or uninsured patients, or those facing
financial hardships, are able to obtain the medicines they need to
fight serious disease.
Our commitment to good citizenship extends beyond medicines to
communities impacted by natural disasters. This year again brought
devastation through wildfires in California, hurricanes in North Carolina
and Florida, floods and landslides in Japan, and an earthquake and
tsunami in Indonesia. The Bristol-Myers Squibb Foundation responded
quickly with cash donations to its disaster relief partners to provide
essential supplies, and the company donated much-needed products
to help impacted communities.

We are committed to creating and sustaining a strong culture of
inclusion to foster a highly innovative work environment. Our People and
Business Resource Groups (PBRGs) leverage the diverse experiences
and perspectives of our employees and drive transformative business
results. In 2018, members of our PAN Asian Network partnered with our
R&D organization in China to launch a global buddy system designed to
mentor colleagues new to clinical development in China, resulting in the
accelerated launch of Opdivo. Additionally, our LGBTA PBRG introduced
a set of transgender guidelines in the U.S. to ensure a respectful and
inclusive workplace environment for employees in the process of
gender transition.

Embedded throughout our culture is a deep commitment to integrity
and uncompromising ethics. We work to ensure that the highest ethics
and integrity are at the foundation of all we do and how we do it.

Looking Forward

At Bristol-Myers Squibb, we are looking forward to an exciting future.
We will build on the strong foundation of 2018 to grow our business and
to help more patients. We have great medicines. We have great people.
By focusing on patients and their families, and by demanding the very
best from our people, the possibilities are endless. We will continue to
create value for our patients, our shareholders, and our employees.
This is why I am very excited for our future.

Thank you.

Giovanni Caforio, M.D., Chairman of the Board
and Chief Executive Officer

April 1, 2019