IN 2016, L3 ACHIEVED SOLID FINANCIAL AND PROGRAM
PERFORMANCE ACROSS THE BUSINESS. WE STRENGTHENED OUR
PORTFOLIO WITH THE GOAL TO OPTIMIZE OPERATIONS, IMPROVE
MARGINS AND ENHANCE VALUE. WE INTENSIFIED OUR FOCUS ON
GROWTH MARKETS THAT BETTER ALIGN WITH OUR STRENGTHS
AND OUR CUSTOMERS PRIORITIES. L3 IS NOW EMBARKING ON
OUR NEXT PHASE  DISCIPLINED GROWTH.

To achieve this objective, we have refined our business
strategy to address the global environment. The U.S.
defense budget is growing. Around the world, air traffic
continues to rise and our international allies have
increased security demands. We have positioned L3 to
capitalize on these trends by expanding our business development
efforts, and by making thoughtful investments in
R&D and acquisitions in markets where we are strongest.
2017 marks L3s 20th anniversary, an important
milestone in our history. Over the last two decades, we
have built a company driven by continuous innovation
and an unwavering commitment to developing technologies
our customers need to execute their missions. We
started this year with a new name, L3 Technologies, Inc.,
to reflect our ascent into a well-respected industry leader
and trusted partner with diverse capabilities that reach far
beyond communications.
Also, effective March 1, 2017, we realigned our
businesses to better pursue global opportunities by
splitting our Electronic Systems segment in two. The
new Sensor Systems segment develops market-leading
technologies for gathering and securely sharing information.
L3s segments, which include Aerospace Systems,
Communication Systems, Sensor Systems and the
reorganized Electronic Systems, will enhance our
drive for higher levels of organic growth and
operational efficiency.
Throughout 2016, we improved our program
performance, strengthening our position for follow-on
work and new contracts with existing customers. We
also streamlined operations by continuing to selectively
consolidate business units, manufacturing facilities and
back-office functions to reduce costs and combine
capabilities.
The success of L3s strategy showed in our performance.
We exceeded our 2016 financial plan in key
metrics, including sales, orders, earnings per share and
free cash flow. We generated positive organic growth and
margin improvement in all of our segments. We won
major new contracts in the defense, commercial aviation
and security markets, and we entered 2017 with
positive momentum.
L3 is fortunate to have a diverse and talented
workforce of 38,000 employees who adhere to the highest
standards of integrity, excellence and accountability. Their
determination to deliver solutions for our customers is
unparalleled, and we owe L3s continued success to their
hard work and unwavering dedication.
A RETURN TO GROWTH
One of L3s central objectives is to create value for our
shareholders and customers. By that measure, 2016 was
a solid year. We achieved 2 percent organic growth, led
by a 5 percent increase in our Department of Defense
(DoD) sales, which offset declines in our commercial and
international end markets. Our defense business grew at a
rate that exceeded DoD budget growth, indicating that we
are well-positioned in DoD priority areas. We posted consolidated
net sales of $10.5 billion and operating income
of $1.0 billion. Orders increased 11 percent compared
with the previous year, resulting in a healthy book-to-bill
ratio of 1.05. Diluted earnings per share from continuing
operations was $8.21 in 2016, a 19 percent improvement
compared to 2015. We converted net income to cash at an
impressive rate of 136 percent.
DISCIPLINED CAPITAL ALLOCATION: DELIVERING VALUE
L3 continued to generate robust cash flow and deploy it
strategically while maintaining a strong capital structure to
support growth initiatives. Net cash from operating activities
was $1,097 million in 2016, and we returned $593
million to our shareholders through share repurchases
and dividends. We stabilized our investment-grade credit
ratings and extended our debt maturity profile during
the year, reducing debt by $300 million while refinancing
$550 million of debt with new 10-year senior notes at
3.85 percent, L3s lowest coupon rate ever for 10-year
fixed-rate notes. We also renewed our $1 billion 5-year
revolving credit facility.
We shifted the balance of our capital allocation to
accelerate disciplined growth by increasing investments
in our businesses. Complementing our 12th consecutive
annual dividend increase, L3 boosted research and
development (R&D) spending by 13 percent yearover-
year to $258 million and accelerated our mergers
and acquisitions (M&A) activities with a number of
bolt-on acquisitions.
M&A: STRENGTHENING CORE CAPABILITIES
Our acquisition strategy focuses on the intersection
of identifying market opportunities, building on our
core capabilities and evaluating companies with complementary
strengths at attractive valuations. In 2016,
we enhanced our portfolio through acquisitions that
provide more high-value, comprehensive solutions for
our customers.
We bolstered our Commercial Training Solutions
with the addition of Aerosim Technologies and Aerosim
Flight Academy, leading U.S.-based commercial pilot
instruction businesses. This acquisition adds innovative
and flexible maintenance training products, distance
learning and low-cost training devices to L3s offerings,
enabling us to deliver complete training, resourcing and
simulation solutions to commercial aviation customers,
including pilots at every proficiency level.
We also significantly strengthened L3s ability to
provide total system solutions in aviation security with
the acquisitions of MacDonald Humfrey (Automation)
in November 2016 and the explosives trace detection
assets of Implant Sciences Corporation in January 2017.
MacDonald Humfrey is a U.K.-based systems integrator
of networked software-based airport checkpoint security
and baggage handling systems. Both companies brought
critical automation and screening technologies to our
growing security and detection business.
Early in 2016, we expanded L3s advanced communications
business with the acquisition of Advanced
Technical Materials (ATM), and integrated its
coaxial and waveguide RF microwave products into
L3s Narda-MITEQ division. We enhanced our electronic
warfare capabilities and expanded our sensor
offerings with the acquisition of Micreo, an Australian
developer of high-performance microwave, millimeter
wave and photonic components. The addition of
Micreo also positions L3 for broader penetration into
the Australian defense market.
TECHNOLOGY LEADERSHIP: DRIVING INNOVATION
L3s investments in R&D help develop solutions that
define us as a leader in innovative and affordable technologies.
In 2016, we continued to build a pipeline of
discriminating next-generation products by investing
in projects with near-term potential, including ways
to reduce size, weight, power and cost, as well as those
out on the development horizon. We are engaging in a
number of initiatives and collaborative relationships with
commercial enterprises and are incorporating advanced
commercial technologies into defense applications in
attractive growth areas, such as virtual reality, with the
objective of anticipating customer needs.
One example is our next-generation Military Code
(M-Code) GPS User Equipment. In 2016, we successfully
obtained U.S. government security certification for this
product, making L3 the first DoD contractor to receive
this designation. To support the manned-unmanned
teaming MUMT-X program and other advanced platforms,
we continue to invest in developing an advanced
signal processing engine that is an agile enabler for secure
communications. Another technology we introduced in
2016 was a high-performing, high-resolution night vision
image intensifier. L3 is also creating new simulation
systems that utilize the visual spectrum in both strategic
and tactical environments to heighten the advantage
to our warfighters.
U.S. DEFENSE: INCREASED SUPPORT TO THE DOD
Defense has always been L3s core business. The DoD
remains our largest customer  constituting approximately
70 percent of our total sales. L3 has a long track
record of combining our broad capabilities to deliver
effective solutions in priority areas on time and on budget.
This positions us to benefit from defense spending that is
trending upward for the first time since fiscal year 2010.
We are confident L3 has the right mix of in-demand
capabilities, including ISR solutions, secure communications,
precision strike, electronic warfare, sensors, night
vision equipment and naval systems. We also believe
readiness and special operations forces support are areas
of increased opportunity. Whatever is needed, we have
the products, technologies and expertise to help the DoD
achieve its objectives.
L3 has a well-earned reputation as a highly responsive
and proven provider of ISR systems and solutions for a
wide variety of aircraft. In 2016, less than one year after
the original award, L3 completed the inaugural flight
of the U.S. Armys Enhanced Medium Altitude Reconnaissance
and Surveillance System EMARSS-M1
prototype aircraft  completing a major milestone in
the program.
Underscoring L3s value as a trusted provider of
military flight training services, we were selected to supply
two additional trainers and associated services in support
of the UH-60M helicopter. This critical training program
has logged more than 250,000 hours. L3 also received
a contract modification to deliver the first full-motion,
high-fidelity aircrew simulators as part of the Flight
School XXI program.
For the U.S. Army, we extended our night vision and
laser rangefinder technology leadership with a production
contract to deliver our Small Tactical Optical Rifle-
Mounted (STORM) SLX precision targeting system. Size
and weight are critical design factors for our customer,
and the improved SLX system delivers increased performance
while reducing the weight of the current STORM
by one-third.
For the U.S. Navy, L3 won a contract to provide
depot-level maintenance for its F/A-18 fleet. Building
upon our international F/A-18 experience, this competitive
win represents the first time we will perform sustainment
on this Navy platform at our facilities in Canada
and Waco, Texas. L3 is developing an optical system that
increases shipboard situational awareness, threat detection
and cueing as part of the Combined EO/IR Surveillance
and Response System (CESARS). CESARS represents
an opportunity to apply our leading airborne widefield-
of-view ISR capabilities to protect U.S. Navy ships
and sailors.
Our contractor logistics support (CLS) services
maintain a variety of military platforms all over the
world. In 2016, we won the right to provide full CLS
services for the U.S. Air Forces (USAF) KC-10 aircraft,
a contract that is expected to be valued at $1.9 billion
over nine years.
L3 has maintained a longtime position as the prime
contractor on the USAFs E-3 Airborne Warning and
Control System (AWACS) flight crew training program.
This includes performing the majority of the programs
ground-based flight crew training using our devices at
an L3 training facility. In 2016, L3 won a contract for
the new AWACS Diminishing Manufacturing Sources
Replacement of Avionics for Global Operations and
Navigation (DRAGON) flight crew training program.
INTERNATIONAL DEFENSE: SUPPORTING ALLIES IN A
DANGEROUS WORLD
In a persistent and unpredictable global threat
environment, delivering solutions that aid and protect
our U.S. allies is of crucial importance for us. We are
investing and expanding in key markets by opening
additional facilities and forming new partnerships, while
strengthening our established relationships around the
world. We continue to see long-term growth potential for
our EO/IR sensors and night vision equipment, airport
security systems and secure communications. L3s strong
reputation for expertise, quality and affordability
contributed to a number of significant international
wins in 2016.
L3 continued our long-term support of Canada with
a contract for the sustainment of mission systems and
avionics installed in the Royal Canadian Air Forces
CP-140 aircraft, which ensures that the platform maintains
mission-capable avionics for ISR operations.
The U.K. Ministry of Defence (MOD) awarded L3
a contract to deliver our ROVER transceivers for the
Joint Common Remote Viewing Terminal (JCRVT). The
JCRVT enables the secure exchange of ISR information,
including full-motion video, across the battlespace via a
wideband IP network. In a separate MOD contract, L3
will deliver TacNet ROVERs and ROVER 6i receivers
 flexible, highly mobile systems that transmit secure
full-motion video and other data to support real-time
situational awareness in land and maritime applications.
Demand for our night vision equipment continues
to grow internationally, including the purchase of our
Clip-On Ruggedized Advanced Thermal Optical Sight
(CRATOS) for a European law enforcement customer
and a Middle Eastern ally. CRATOS is a miniaturized,
all-in-one thermal weapon sight designed for handheld,
weapon clip-on or stand-alone use in degraded
visual environments.
L3 received a significant contract valued at approximately
$200 million over five years under the Australian
Defence Force Soldier Modernisation Systems Program
to provide the Australian Army with innovative targeting
and detection capabilities designed to perform at
night and under all weather conditions. These include
binocular night vision goggles, which incorporate white
phosphor image intensification (I2) technology, a compact
multifunction aiming laser with integrated white
light capability, and the next generation of lightweight,
miniature laser rangefinders. Other awards in the region
included a contract for the Integrated Starter Generator
(ISG) for Australias Hawkei armored vehicle, an order for
an F-16 mission training system from Taiwan, and funding
for communications and ISR integration on South
Koreas Global Hawk program.
We are identifying areas where our industry-leading
expertise and technologies can contribute to security in
the Middle East and Africa. In 2016, we were selected
under a Foreign Military Sales (FMS) contract to provide
our MX-10D EO/IR turrets for a North African nation
in support of counterterrorism operations aboard newly
acquired UH-60 helicopters. Other important wins for
the year included a contract to supply MX-15 EO/IR
turrets to a Middle Eastern country for internal security
and a major integration contract for ISR/data link technology
on multiple rotary-wing aircraft for another North
African customer.
COMMERCIAL AVIATION: EXPANDING BUSINESS WITH
ADVANCED PRODUCTS AND TRAINING
The commercial aviation market represents a significant
long-term growth opportunity for L3. Global air traffic
revenue continues to grow at a faster rate than global
GDP. Pilot shortages are projected to worsen over the
next decade and beyond, enhancing demand for our pilot
training capabilities around the world. Mature markets in
North America and Europe continue to recapitalize their
fleets, while Asia and the Middle East are investing in new
or expanded commercial air travel capacity. The outlook
remains robust for pilot training and commercial avionics,
including displays, flight recorders, and Traffic Alert and
Collision Avoidance System (TCAS) solutions  areas
where L3 is a well-recognized industry leader.
L3 has built a broad portfolio of pilot training services
and equipment that enables us to deliver a suite of
solutions to commercial aviation customers worldwide.
Capitalizing on the trend for airlines to cut costs by outsourcing
this function, L3 developed flexible, scalable endto-
end pilot training that ranges from ab initio coursework
for beginners to specific training tailored to experienced
aviators. Over the years, we have expanded our high-fidelity
simulation and training offerings by adding flight
schools and instructors worldwide and becoming one
of the few licensed authorities in Europe and Asia. This
enables L3 to offer a variety of innovative and cost-effective
programs to airlines, such as pay-by-the-hour flight
instruction, that train and certify pilots for a broad range
of commercial aircraft.
All Nippon Airways (ANA) awarded L3 a contract
to build and deliver a customized full flight simulator
for A320 aircraft based on our RealitySeven flight
simulation platform. The new device will be installed at
ANAs Tokyo flight training facility, joining an existing
RealitySeven B787 full flight simulator. The contract
affirms our relationship with ANA to support its long-term
fleet expansion plans. L3 also received full flight simulator
orders from Hainan Airlines and Sichuan Airlines
during the year.
In commercial avionics, L3 further increased our
market share with contracts to provide our solutions on
new and existing aircraft for airlines around the world.
In 2016, we reached agreements with various operators
to install a mix of our aviation products, including flight
data recorders, SATCOM systems, T3CAS collision
avoidance systems and NXT-800 systems, on more
than 2,000 aircraft, including AirAsia, easyJet and
China Eastern Airlines. In addition, as they equip their
fleets to meet European Aviation Safety Agency (EASA)
and the FAAs 2020 ADS-B mandates, increasing
numbers of airline customers are selecting L3 to
retrofit in-service platforms with our NXT-800 ADS-B
Out transponder.
In the commercial SATCOM marketplace, L3 is
reshaping Traveling Wave Tube Amplifier (TWTA)
capabilities by introducing new technologies to better
serve high-throughput satellites. As the frequency spectrum
becomes more congested, L3 has products ready
to fill the need for higher-frequency TWTAs through
V-Band and beyond. In 2016, we continued to deliver
space-qualified TWTAs to satellite customers and anticipate
that we will announce the first sale of our new
Q-Band TWTA product in 2017.
AVIATION SECURITY: MEETING A GLOBAL NEED
Growth in commercial air traffic worldwide and enduring
security threats continue to drive the requirement for
increasingly more sophisticated, safe, fast and reliable
screening solutions.
Our recent acquisitions added highly advanced
baggage screening and explosives trace detection technologies,
broadening and deepening L3s whole-system solutions
to streamline airport security and make the passenger
experience easier without compromising safety. Our
systems are operating at several major airports around
the world, and we are developing more compact equipment
to accommodate the smaller footprints of regional
airports. In 2016, L3 expanded our market share with
steady sales growth of our ProVision and ClearScan
passenger and cabin baggage screening brands worldwide.
Additional orders from the U.S. Transportation
Security Administration (TSA), the Canadian Air Transport
Security Authority (CATSA) and other international
regulators currently give us an installed base of more than
1,800 ProVisions at over 300 airports worldwide, making
this the industry-leading solution for safe, reliable and
efficient personnel screening. We also received orders for
our MV3D checked baggage screening equipment from
customers in Istanbul and Bahrain.
COMMITMENT TO COMMUNITY
L3 remains committed to principles of good stewardship
and corporate responsibility for the communities in which
we live and work. We make contributions at the corporate
and local levels and place the utmost importance
on those initiatives that assist, serve and honor our U.S.
military personnel, veterans and families. L3 is a member
of the Employer Partnership of the Armed Forces, an
organization that provides employment opportunities
to former service members by leveraging their military
training and experience. We contribute to charities such
as the Bob Woodruff Foundation, which funds programs
that meet the needs of our injured heroes, and Homes
for Our Troops (HFOT), which builds adaptable housing
for veterans. In the last six years, employees from across
L3 have donated funds, specialized items and manpower
to build six HFOT homes, with more in the works. L3
honors the legacy and sacrifices of our armed services
by supporting the capital campaign to build the first-ever
National Museum of the U.S. Army. We provide financial
assistance to the Special Operations Warrior Foundation,
the Tragedy Assistance Program for Survivors (TAPS),
the Marine Corps Scholarship Foundation and the Coast
Guard Foundation, just to highlight a few.
As a company founded on technology leadership and
engineering expertise, L3 has long valued the importance
of science, technology, engineering and mathematics
(STEM) education. We actively support a wide variety
of programs at all education levels through mentoring
and educational partnerships in STEM-oriented curricula,
and we donate funds to university and college
research programs.
L3 employees and divisions also contribute significant
time, labor and money to causes outside of industry
that provide critical support in their communities. In
addition to fundraising efforts for medical research, our
businesses have raised funds as part of local United Way
campaigns and donated food, toys and manpower to help
children, families and the elderly.
TAKING A STRONGER L3 FORWARD
Over the past few years, we have moved with decisiveness
and clarity to forge a stronger L3 for the future. Our
portfolio is well-aligned with customer needs and
market trends in higher-growth areas of domestic and
international defense, commercial aviation and global
security. We have streamlined operations, maintained a
strong, flexible balance sheet and returned to growth.
In short, we did what we said we would do, but our work
is never done.
Looking ahead, we will continue to optimize for
increased efficiency and cross-business collaboration.
We will employ a balanced capital allocation strategy
that supports innovation and growth to drive long-term
shareholder value. We remain committed to maintaining
high standards, and meeting and exceeding the needs of
all of our stakeholders.
L3s strength is our agility, responsiveness and market-
leading technologies. Since 1997, we have grown to
become one of the worlds leading defense companies.
Each of us is proud of the role our company plays in
making our nation and the world safer and more secure.
In an ever more complex global threat environment,
no one knows for certain the impact of changing geopolitical
and economic forces. What we do know is that
L3 stands ready to deliver for our customers and our
shareholders  with purpose, integrity and discipline.
Sincerely,
MICHAEL T. STRIANESE
Chairman and Chief Executive Officer

