Fellow Shareholders:

Fiscal 2016 was the tenth anniversary of our
initial public offering and another year of strong
value creation for our shareholders. Adjusted
earnings per share and EBITDA As Defined,
key metrics of value creation, both grew by
more than 20%. We acquired three proprietary
aerospace businesses for approximately
$1.4 billion in purchase price. We also returned
about $1.6 billion to our shareholders in the
form of approximately $200 million of share
repurchases and a $1.4 billion special dividend
paid in early 2017. Shareholder return for
fiscal 2016 was an impressive 36%.
All this was accomplished while continuing
to create real intrinsic value. We again invested
fully in our new and existing businesses to
continue producing innovative proprietary
products, and we successfully integrated a
number of recent acquisitions � all while
maintaining our financial flexibility. We were
once again named to Forbes� list of the world�s
most innovative companies. The award
identifies TransDigm as one of the top 100
global companies for innovation. We are
the only European or American aerospace
company so recognized.
CREATING VALUE:
CONSISTENTLY PROFITABLE
Fiscal 2016 net sales rose 17% to $3.17 billion,
from $2.7 billion in fiscal 2015. Our commercial
business, which accounts for about 70% of
revenue, increased over the prior year. Growth
in both the commercial transport OEM and
Fellow Shareholders:
aftermarket was partially offset by weakness in
the smaller business jet, helicopter and freighter
markets. Defense revenues for the year were
modestly better than we anticipated.
Net income for fiscal 2016 increased 31% over
last year to $586 million, or $10.39 per share.
On an adjusted basis, net income rose 27%
to $645 million, or $11.49 per share, compared
to fiscal 2015.
EBITDA As Defined, a meaningful indicator
of operating performance, increased 21%
to $1.5 billion over last year. Our EBITDA As
Defined margin remained strong at 47.1%.
Fiscal 2016 EBITDA As Defined margin, without
dilution of the acquisitions purchased in 2015
and 2016, was approximately 49%, up 2.5
margin points from last year. The margins in
our core businesses again improved year over
year, reflecting the power of our steady value
generation strategy.
CREATING VALUE:
UNIQUE BUSINESS MODEL
This strong performance reflects the ability
of our uniquely consistent business model
and strategy to create and sustain intrinsic
shareholder value through all phases of the
aerospace market cycle.
Proprietary products generate about 90%
of our sales, and about 80% of our sales come
from products for which we believe we are
the sole source provider. More than half of our
revenues and a higher percent of our EBITDA 

come from aftermarket sales. Aftermarket
revenues have historically produced a higher
gross margin and have provided relative
stability through the aerospace and defense
cycles.
Our longstanding goal is to give our shareholders private equity-like returns with the liquidity
of a public market. To do this, we stay focused
on both the basic elements of value creation
as well as careful management of our balance
sheet. We follow a consistent long-term
strategy:
� We own and operate proprietary aerospace
 businesses with significant aftermarket
 content.
� We have a simple, well-proven operating
 methodology based on our three value
 driver concepts: profitable new business,
improvements to our cost structure and
 value-based pricing.
� We maintain a decentralized organization
 structure that keeps us close to the
 customer and a compensation system
 that is closely aligned with shareholders.
 This enables us to attract and retain
 entrepreneurial managers who think
 and act like owners.
� We execute a focused and disciplined
 acquisition process, buying proprietary
aerospace businesses where we see
 a clear path to value creation.
� We remain appropriately leveraged
 to optimize shareholder returns.
We view our capital structure and the efficient
allocation of capital as a key part of creating
shareholder value. We are willing to increase
our debt leverage when we see good opportunities or view our leverage as suboptimal for
value creation. We also are willing to return
money to our shareholders when and if that
seems an appropriate way to create value.
Our capital allocation choices over the past
three years of distinct business environments
reflect our willingness to manage our capital
structure to take advantage of current business
and capital market environments. 

TransDigm�s business units continue to develop innovative product solutions for
the Company�s broad base of customers. Here are some examples:
ADELWIGGINS was awarded both the fuel and hydraulic lightning isolators for the new
Boeing 777X aircraft and the Bombardier Global 7000 and 8000 programs. These parts
protect the aircraft in the event of a lightning strike by preventing high electric energy
from flowing through the fuel and hydraulic lines within the fuel tanks. These isolators are
increasingly important on new composite-skin aircraft.
AERO FLUID PRODUCTS was selected by Pratt and Whitney to supply the Oil Control
Manifold for the Airbus A320neo platform leveraging their expertise in integrating
system valves, providing for line replaceable units for improved maintenance access,
and eliminating oil line assemblies and leakage concerns with weight reductions.
PNEUDRAULICS was awarded components in four major hydraulic system areas by
Textron Aviation for the Cessna Longitude Program. The new awards consisted of
hydraulic components for spoiler actuation, brake systems, landing gear and central
hydraulic systems. Additionally, this represents the largest total shipset award for
PneuDraulics to date.
TELAIR US CARGO GROUP was able to overcome a difficult engineering challenge on
the Airbus A400M and recently upgraded the design of the X-Lock cargo lockdown
mechanism, which holds containers in place during flight. This retrofit program is a
one-time upgrade and will become part of the OEM package sold to Airbus going
forward. We anticipate normal defense aftermarket demand for maintenance
of this critical cargo handling system.

In fiscal 2014, with limited acquisition
opportunities that met our criteria, we
returned about $1.6 billion to our shareholders
in the form of a special dividend and modest
share repurchase program. Fiscal 2015
we saw a number of attractive acquisition
candidates. We acquired $1.6 billion of
proprietary aerospace businesses that met our
strategic and shareholder return requirements.
In fiscal 2016, we acquired another $1.4 billion
of attractive aerospace businesses. The
combination of our usual high cash flow and
continuing attractive credit markets allowed
us also to return $1.6 billion more to our
shareholders through share repurchases and
large special dividend, paid out in early 2017.
In total, over the last three years, we have
returned $3.2 billion to our shareholders.
In that same period, we made nine acquisitions
for about $3.3 billion. We also invested in
our existing businesses, kept a healthy
balance sheet and have sufficient capacity
for additional acquisitions. Since our IPO in
2006, TransDigm�s total return to shareholders
totals almost 1,400%.
CREATING VALUE:
ACQUISITIONS AND INTEGRATION
We raised $3.8 billion during fiscal 2016
and early 2017, primarily to fund acquisitions
and pay the special dividend. At the end
of fiscal 2016 (September 30, 2016), based
on the current capital market conditions and
adjusting for the $1.4 billion special dividend
and recent financing, we have adequate
capacity to make more than $1 billion of
additional acquisitions without issuing equity.
This capacity grows steadily to more than
$2 billion as the year proceeds. We will
continue to evaluate acquisition opportunities
that meet our selective criteria.
We acquired three proprietary aerospace
businesses in 2016: Breeze-Eastern, a
leading global designer and manufacturer
of high performance lifting and pulling devices
for military and civilian aircraft; Data Device
Corporation, the world leader in the design and
manufacture of high reliability data bus control
components and systems products for
aerospace and defense vehicles; and
Young & Franklin and the subsidiary
Tactair Fluid Controls, which manufacture
highly engineered valves and actuators.
The 2016 acquisitions are all consistent
with our disciplined focus. The integration
of those acquisitions is proceeding well and
meeting our expectation for value creation.
Since TransDigm�s founding in 1993, the
company has acquired 58 businesses, including
43 since the IPO in 2006. The ability to quickly
integrate acquired businesses and improve
their margins is critical to our business strategy,
propelling us ever onward and upward.
MANAGEMENT CHANGES
Effective January 1, 2017, Kevin Stein,
previously Chief Operating Officer of the
Power segment, was appointed President
and Chief Operating Officer of TransDigm
Group Incorporated. He is responsible
for almost all aspects of the Company�s
operations. Bob Henderson, who was
Chief Operating Officer of the Airframe
segment, is now Vice Chairman of the
Company. He will continue to oversee select
operating units and will be more focused
on acquisition, integration and business
development activities. Kevin and Bob
are valued members of our executive
management team and esteemed partners
and advisers to me as I continue in the role
of Chairman and Chief Executive Officer.
Two members of our management team
have recently retired: Greg Rufus, Senior
Executive Vice President and former
Chief Financial Officer, and John Leary,
Executive Vice President and Acting
President of Adams Rite Aerospace, Inc.,
a TransDigm Group company.
Greg Rufus served as a major contributor
during his 16 years with the company,
helping it to grow enterprise value from
$400 million to $25 billion, transitioning from
a private company to a NYSE-listed member
of the S&P 500 and completing approximately
50 acquisitions, among many other accomplishments. Greg�s retirement was previously
announced when Terry Paradie was
appointed Chief Financial Officer in 2015.
We are extremely grateful for Greg�s
leadership and partnership.
John Leary has also been with TransDigm
for about the same 16 years. Like Greg,
he has been a substantial contributor to
the outstanding growth of TransDigm over
that period. John served as President of a
number of our larger divisions and driven
substantial value creation in these businesses.
He has also been a key member of the senior
management team as Executive Vice President.
John will continue to assist us on an as-needed
basis with interim management and training.
We are very grateful for his leadership over
the last 16 years.
In summary, 2016 was a good and busy
year for TransDigm. I�m confident with our
consistent value-focused strategy and strong
mix of business we can continue to create
long-term intrinsic value for our investors.
We thank our shareholders for their investment,
our customers for choosing TransDigm, and
our managers and employees for their hard
work and dedication. We look forward to
reporting to you on our progress during 2017.
Sincerely,
W. Nicholas Howley
Chief Executive Officer and
Chairman of the Board of Directors
January 20, 2017 