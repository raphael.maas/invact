              To Our Shareholders

             How do you make something great
             even greater?                                                           They turn it up.
                Here at Ecolab, that's the question we                               Here are some of the specific ways we turned it up at Ecolab
                                                                                  in 2002:
             ask ourselves at the dawn of each new
             year. How do we take this company,
TURN IT UP




                                                                                  We turned up our financial performance
             which over the past eight decades has                                 Sales from wholly owned operations were $3.4 billion, up 47 per-
             grown from a tiny, one-man enterprise to                             cent. This included the first full year of sales from our November 2001
                                                                                  acquisition of the remaining 50 percent of the former Henkel-Ecolab
             a $3 billion-plus global leader, and thrust
                                                                                  joint venture we did not previously own. Adjusting for that acquisi-

             it to the next level? How do we not only                             tion and the others that will be discussed shortly, as well as divesti-
             equal Ecolab's remarkable record of growth,





                                                                                  tures, our sales grew 4 percent. This excellent performance was
             but surpass it?                                                      thanks to the diligent efforts of our sales-and-service force, which
                                                                                  worked very aggressively to offset a difficult economy by utilizing
                In other words, how do we turn it up?
                                                                                  our newest products and programs, as well as our unrivaled ser-
                                                                                  vice, to outperform the industry and the competition.
                The way we see it, a company should always be taking the next
                                                                                   Our net income rose 11 percent to $210 million. Diluted net income
             step. Always be moving forward. Always growing. In today's busi-
                                                                                  per share went up 10 percent to $1.60. Our results for 2002 included
             ness world, you either grow or you die. It's that simple. But you
                                                                                  restructuring charges and several other unusual items. Comparability
             must have the right people and the right plan. At Ecolab, we have
                                                                                  with prior years is also affected by the acquisition of Henkel-Ecolab
             both. We employ more than 20,000 of the greatest individuals in
                                                                                  and the elimination of goodwill amortization. Excluding these items,
             the world and have a proven, highly potent Circle the Customer -
                                                                                  our diluted pro forma income from ongoing operations per share
             Circle the Globe strategy. By remaining focused on this strategy,
                                                                                  rose 13 percent to $1.84.
             to which we have committed our future  and by staying true to
                                                                                   Our stock price finished the year up 23 percent at $49.50, beating
             the ethical values and convictions upon which Ecolab has been
                                                                                  the Standard & Poor's 500's 23 percent decline by a wide margin.
             built  we've been able to turn it up year after year.
                                                                                  This was the third consecutive year  and 10th year out of the last
                2002 was no exception. Despite lingering market challenges,
                                                                                  12  that Ecolab's stock outperformed the S&P 500. That is an
             we began the year confident that we could take everything we
                                                                                  outstanding record.
             already do so well  things like advanced products and systems,
                                                                                   Our return on beginning shareholders' equity was 24 percent,
             unbeatable service, innovation, global scope, top-quality people 
                                                                                  exceeding our corporate goal of 20 percent for the 11th year
             and do it all even better. We knew it wouldn't be an easy job, but
                                                                                  in a row.
             that's what true leaders do: take what others see as big obstacles
             and turn them into even bigger opportunities.

 In December, we raised our quarterly cash dividend 7 percent to             We turned up our product offerings and service capabilities
$0.145 per common share. As a result, our indicated annual rate                In 2002, we introduced many exciting new additions to our
went up to $0.58 per share. It was our 11th consecutive annual                always-growing lineup of innovative, highly differentiated products,
dividend rate increase.                                                       systems and services. They include Oasis Pro, a complete, cost-
 Ecolab continued to enjoy strong cash flow, with record cash                effective housekeeping program for hospitality and healthcare
provided by operating activities of $423 million. This strong cash            operations featuring high-performance products that meet the
flow contributed once again to a strong balance sheet. Our 2002               widest variety of cleaning needs; Inspexx 100, which is utilized in
total debt to capitalization ratio was 39 percent, strengthening our          chill water systems to control salmonella and reduce other poten-
"A" category balance sheet from all the major rating agencies, as             tially dangerous pathogens on poultry; DermaFoam, a handcare
well as our Prime-1 commercial paper rating.                                  program for quickservice restaurants that helps combat food
 We continued to work during 2002 to streamline and improve our              safety problems by promoting the highest standard of employee
worldwide operations, as well as produce costs savings. We did                hygiene; the Triplex Energy System, a low-temperature laundry
this through a global effort that resulted in a small workforce reduc-        program that provides gentle-yet-effective cleaning power and
tion, the closing of several less-efficient facilities, the discontinuation   reduces energy costs, is helping us gain customers in Europe's
of low-volume products, and certain benefit plan changes. We also             growing textile leasing market; the Rodent Interceptor, which
made progress in the ongoing integration of Europe into our global            channels rodents to mechanical traps and is a key element of a
operating infrastructure. I am confident our actions optimized our            comprehensive new pest elimination program targeted specifically




                                                                                                                                                    
business and made us an even more efficient, effective company for            at the food retail industry; and the unique PhaZer Mobile Floor
the long term, and improved our profitability and growth potential.           Care System, which helps office, healthcare, retail, educational



                                                                              up
             facilities and other high-volume floor care customers finish floors     and the rest of our management team to develop and leverage the
             up to four times faster and at a lower total cost.                      tremendous opportunities that lie ahead for Ecolab.
                To learn about more of our latest offerings, check out the "2002      In July, I regretfully accepted the resignation of Rick Marcantonio,
             Review of Operations" section of this report.                           who had served as president of our Industrial and Service sectors.
              We continued to strengthen our industry-leading sales-and-service     During Rick's five years at Ecolab, we strategically refocused our
             organization by adding 700 new field associates, for a global total     industrial businesses and expanded our service businesses, laying




             urn it
             that exceeds 11,400. This represents a 7 percent increase over 2001.    the groundwork for their further development. We thank Rick for
                                                                                     his contributions to Ecolab and wish him every success in his new
             We turned up our growth through strategic acquisitions                  career endeavors.
              We took another major step forward in our growth plans in
             September 2002, when we announced that we were expanding                We turned up our reputation in the business world
             our highly successful Pest Elimination business across the Atlantic      For the third consecutive year, Ecolab was once again named
             to Europe. To this end, we gave ourselves a ready-made foothold         one of America's "100 Best Corporate Citizens" by Business Ethics
             in the region by acquiring Terminix Ltd., a subsidiary of The           magazine. We simply couldn't have been more proud of this honor,
             ServiceMaster Co. of Downer's Grove, Illinois. Based in London,         particularly in light of the public scrutiny regarding corporate ethics.
             Terminix reported annual sales of $65 million. It provides commercial   At Ecolab, we have long committed ourselves to conducting our
             pest elimination services throughout the United Kingdom and the         business with the utmost integrity and responsibility, and we





             Republic of Ireland. This important action was a natural extension      greatly appreciate this recognition.
             of our Circle the Customer - Circle the Globe strategy. Further, the     Also in 2002, I was honored to represent our company and its
             move is just one way in which we are realizing our pledge to intro-     incredible associates by accepting two awards from our peers
             duce new offerings worldwide, made when we acquired 100 per-            in the business world. In March, I was recognized as the 2001
             cent ownership of our former joint venture.                             Industrial Chemical CEO of the Year by international marketing


              In December, we made another noteworthy acquisition in Europe         consulting firm Frost & Sullivan for my role in "making a series of





             by purchasing Adams Healthcare, the leading supplier of hospital        acquisitions that have created new channels in which to grow
             hygiene products in the United Kingdom with annual sales of about       and, in general, making acquisitions to add capabilities to develop
             $19 million. Formerly operated by Medical Solutions plc, this trans-    new services, new technologies or new geographies." Then in
             action not only helps us better serve the United Kingdom's grow-        November, the Penn State Hotel and Restaurant Society named
             ing healthcare market, but it also will allow us to strengthen our      me the 2002 Hospitality Executive of the Year for an "ongoing
             existing Hospital Hygiene business in Europe.                           commitment to service to the culinary, hospitality, educational
              Also in December, we sold Darenas, a janitorial products distri-      and business communities."
             bution business based in Birmingham, England, to Bunzl plc of
             London. This divestiture was consistent with our stated aim to          Outlook
             remain focused on our core competencies and leverage growth             2002 was a start, but  mark my words  Ecolab has only just
             opportunities via markets and businesses in which we can clearly        begun to turn it up!
             differentiate and add value.                                               During 2003 and beyond, we're going to continue to turn up
                                                                                     the heat on the competition. We're going to keep turning up the
             We turned up development of our leadership team                         energy, productivity and creativity within our ranks. And we're going
              Doug Baker was named Ecolab's president and chief operating           to further turn up the volume, to make sure that the entire world
             officer in August. A 14-year Ecolab veteran, Doug had previously        knows what a remarkable company Ecolab really is. By doing all
             held a number of key management positions within our company,           this, we are going to turn up the results for our customers and our
             most recently serving as president of the Institutional Sector. Doug    shareholders alike.
             has proven well-suited to this critical leadership role, and he is         Think we sound confident? Good. Aggressive? Great. At Ecolab,
             working diligently with me, International President John Spooner        that's what we're all about. We're not going to spend our time
                                                                                     worrying about what some people call this "age of uncertainty."
We know that certainty itself is an illusion. You have to make your               Finally, we're going to amplify our research and development
own luck, taking full advantage of each opportunity as it presents             efforts, taking full advantage  as we always have  of the latest
itself. You have to make it happen. What we've done here at Ecolab             technologies to bring our customers the most advanced products
is turn our company into a strong, steady growth machine, contin-              and systems available. The scientists and engineers at Ecolab's
ually forging ahead. We don't rely on external forces to determine             world-class R&D facilities are the best in the business, and they're
our success. Rather, we're busy shaping our own destiny, every                 busy creating tomorrow's cleaning and sanitizing solutions today.
single day, fueled by the incredible power and momentum that                      All this adds up to greater opportunity. For me, that's the mark
pervades our entire global organization.                                       of a true growth company  one that not only expands its market
   We know exactly how we will grow. Our core businesses                      share but, at the same time, grows its overall market potential. Look
Institutional, Food & Beverage, Kay and Pest Elimination  will                at it this way: You can keep taking bigger and bigger pieces of the
obviously lead the way, but we also have a formidable stable of                pie. But unless you figure out a way to grow the pie, pretty soon
newer offerings  including GCS Service, Facilitec and EcoSure                 you're going to end up with an empty pan. At Ecolab, we've dou-
Food Safety Management, among others  that will make                          bled the size of our pie over the last seven years. In 1995, we saw
increasingly significant contributions to our success. Together, these         our market potential as $18 billion. Today it's $36 billion. And we
established businesses will expand Ecolab's market share by                    have a strategic, long-term growth plan in place that will allow us
doing three things: winning new customers through accelerated                  to continue growing the pie. That gives us an awful lot of room to
cross-divisional selling; improving account penetration (in other              keep expanding our market share.




                                                                                                                                                       
words, selling additional solutions to existing accounts); and                    Going forward, we remain firmly committed to providing the
introducing new products, systems and services that meet our                   highest-caliber solutions and service for our customers, and superior
customers' and potential customers' many needs.                                value for our shareholders. And we're going to do it the right way.
   Ecolab will also grow by leveraging the strengths of our industry-          No shortcuts. No excuses. Just results.
leading sales-and-service force, increasing productivity, adding                  That is how we'll turn it up during 2003 and in the years to come.   

quality employees and enhancing the various ways we partner                    It's going to be fun. We hope you'll come along for the ride.




                                                                                                                                                       
with our customers  never forgetting, of course, that personal,
on-demand service is what has always made Ecolab unique in an
industry largely populated by commodity players. We're also unique
                                                                               Allan L. Schuman
in that we have a stronger global presence than anyone else.
                                                                               Chairman of the Board and Chief Executive Officer
We're the world leader because when we say "Circle the Globe,"
we mean it.


