To our shareholders,
When I assumed the role of WellPoints Interim President and CEO last August, I made three commitments
to our investors, our customers, our Board, and our associates: we would meet our financial
and operational objectives, close the Amerigroup transaction, and take the necessary steps to position
your company for success. I am pleased to say we delivered on each of these commitments, and I am
proud of what weve accomplished.
WellPoints financial performance improved over the course of 2012, driven by a combination of better
core operating performance and favorability in our capital management areas. Membership was up
5.5 percent, bringing our total medical membership to more than 36 million, and we continued to
manage our expenses responsibly while funding a diverse portfolio of strategic initiatives. Together,
these factors led to full year net income of $2.7 billion and adjusted net income of $7.56 per share, an
8.0 percent increase over 2011.
Outside of our financial performance, we took important steps in 2012 to position our company
for the opportunities ahead. We centered our strategy around four key growth areas: Medicare, the
Medicare-Medicaid dual eligibles, the emerging health insurance exchanges, and our specialty businesses,
specifically vision and dental. We then matched the top talent from our company and our
recent acquisitions to create a best-in-class leadership team with deep market knowledge and specialized
operational expertise. By aligning our strategy and structure, weve empowered our leaders, enabling
them to better influence and more nimbly react to the factors that impact their businesses.
Near the end of 2012, we also successfully completed our acquisition of Amerigroup, a significant
accomplishment and testament to the leaders and workgroups on both sides of the transaction. We
now serve over 4.5 million Medicaid beneficiaries in 20 states, giving us the largest Medicaid footprint
in the industry. By retaining key operating talent and leveraging the proven care management models
of both Amerigroup and our CareMore subsidiary, we now have unmatched capabilities to manage
care for the nations growing high risk, high needs populations.

We are encouraged by the progress made in 2012, and anticipate a solid 2013. This year will be one of
continued investment as we prepare for the market changes and full implementation of health reform
in 2014. We remain focused on growth, and will continue to refine our operating procedures while
making meaningful investments in infrastructure, technology, and new capabilities. A number of these
initiatives are highlighted throughout this report, and, together with our other strategic investments,
they represent our roadmap to becoming the strongest competitor in our markets and delivering
steady, predictable results.
I am optimistic about our long-term outlook, and my confidence in our future is reinforced by the
Boards selection in February 2013 of Joseph R. Swedish to serve as WellPoints next CEO. Health care
is undergoing an unprecedented transformation, and success will require innovative thinking, new
collaborative relationships, and a willingness to take bold action. Joe is a seasoned CEO with a proven
track record of leading large, complex health care organizations in challenging market and policy environments.
I know I speak for all of WellPoint when I say we look forward to working with Joe, and are
confident his vision and his passion for operational excellence will accelerate our positive momentum.
It has been my sincere privilege to serve as WellPoints Interim President and CEO, and I have enjoyed
the opportunity to meet and interact with many of you over the past several months. Your investment
in our company is an investment in a strategy, a leadership team, and more than 43,000 associates
that are committed to our customers, dedicated to our communities, and sharply focused on delivering
exceptional value to our shareholders. Thank you for your continued confidence in our company and
your support of WellPoints mission.
In closing, Id like to recognize our associates for their hard work and their unwavering support
throughout this transition. They approached the challenges of 2012 with tenacity and resolve, and
both our success and our bright future belong to them. I am proud of how theyve performed, humbled
to have led them, and honored to work in partnership with them each and every day.
John Cannon,
President and CEO