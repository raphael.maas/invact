Dear
Fellow
Shareholders,

Fiscal 2017 was a challenging but ultimately positive year for the organization. For the
last two years the organization has done the hard work of putting two organizations
together in over 100 countries around the world. This has taken a tremendous amount
of resources in terms of time, money and people�s attention. It has certainly tested
the resolve of the organization. As the year closed, however, we have begun to turn
the page on how we can focus on growing our portfolio of products and services.
All shareholders can be proud of the resilience, tenacity and resourcefulness the entire
Dentsply Sirona team has demonstrated throughout the process.
One of the most important lessons from 2017 is our
need to refocus on our fundamentals. We are in a very
attractive market with a great portfolio of brands and
products and have more than 16,000 passionate and
committed employees at Dentsply Sirona. We have the
opportunity to transform dentistry by delivering products
and solutions that enable better, safer and faster care.
If we provide our customers with unique dental solutions,
we should be able to grow faster than the market and
create significant shareholder value.
Financial Review
Revenues of $4.0 billion grew 6.6% compared to the prior
year, while sales of our combined businesses1
 grew
1.6% excluding the effects of exchange rates. For the
year, the Company reported a loss of $6.76 per common
share which reflects the impact of a non-cash goodwill
impairment charge of $1,650.9 million and a non-cash
indefinite-lived intangible asset impairment charge of
$346.7 million, both driven by management�s projections,
tax reform and foreign exchange rate changes. On
an adjusted basis, we delivered earnings per share of
$2.66, down 5% from the prior year, reflecting some of
our challenges, including the transition to expanding
distribution of our equipment business in North America.
During the year we made the strategic decision to
expand our distribution of equipment from an exclusive
relationship to a non-exclusive relationship. Our intent
was to accelerate adoption of our technologies in the
market by leveraging multiple partners who would
support our offering. While we continue to believe
that this is the best course of action for our future, the
transition slowed decision making at the end user level.
Potential customers now are weighing the decision of
not only whether or not to buy technology, but also
from whom to buy. As retail sales did not materialize, we
built up inventory in the distribution channel. This has
impacted our performance.

In 2017, we did not meet our initial financial guidance.
The aforementioned transition coupled with integration
challenges hindered the Company�s ability to execute.
Under new leadership, we met our new, lower financial
guidance and improved performance in the back half of
the year, returning to both sales and earnings growth.
We are beginning to gain momentum and we expect to
deliver faster sales and earnings growth in 2018.
Growth and Margin Improvement
are Two Imperatives That
Will Create Shareholder Value
Our entire team is excited about taking the potential of
Dentsply Sirona and turning it into performance. Our
team will focus on two commitments that I make to you:
growth and margin expansion.
Our growth plan going forward will be built on three
pillars: sales force effectiveness, accelerating the pace of
innovation and improving our performance in developing
economies. At Dentsply Sirona, we have over 30 sales
forces and around 5,000 people around the globe
focused on sales and sales support. It is essential that
we improve the performance of this group. This includes
targeting, messaging, training, outstanding customer
service and sales force productivity tools including
teaching our groups how to cross-sell.
We will accelerate the pace of innovation by putting
the customer first, looking globally at their needs,
reallocating investments and creating urgency. We have
the industry�s leading R&D budget and by focusing it on
higher impact projects, we expect to be in a much better
competitive position.
Our final pillar is to grow in the rapidly developing areas
around the globe�such as Asia, Latin America and the
Middle East�where we have an excellent foundation�
and also tremendous opportunity. This is going to require
more investment and resources but we expect will pay
off significantly.
Our entire employee base believes that we can and
should be a growth company built on an industry leading
go-to-market capability, world class research and
development and being a global solutions provider. We
plan to selectively add to our organic efforts by deploying
capital to support faster sales or earnings growth.
Margin improvement will be predicated on leveraging our
scale and operating more efficiently. Our team is focused
on delivering significant cost savings over the next two
years. Given our early stage of development in areas like
procurement, rationalizing our manufacturing footprint
and looking at our operating model, we believe we have
significant runway. We believe these savings can be
reinvested to support our pillars of growth, but we expect
will ultimately yield margin improvement both in the back
half of the year and beyond.
To the extent we accelerate our growth trajectory and
expand our margin profile, our business should become
more profitable and more valuable as an enterprise. Our
success here would not only enable reinvestment to
fuel long-term sustainable sales and earnings growth but
differentiate Dentsply Sirona as a truly unique asset.

An Integrated Approach to Financial and
Sustainability Reporting
At Dentsply Sirona, we believe that being a responsible
corporate citizen creates value for all of our stakeholders,
including our shareholders. For the first time, we have
decided to integrate our financial and sustainability
reporting to increase awareness and transparency about
our corporate social responsibility platform.
As the global market leader in dental solutions, Dentsply
Sirona strives to be the innovator and a pillar of the
communities in which we operate. Recognizing the global
reach of our business, Dentsply Sirona seeks to make a
positive impact first and foremost by staying true to our
vision of improving oral health worldwide. By continuing
to develop products, solutions and services that
empower dental professionals, we give them the tools to
provide their patients with better, safer, faster dental care.
For us, corporate philanthropy refers to our responsibility
to our employees, communities, society, and the
environment. Our corporate social responsibility platform
is focused on initiatives that aim to improve prevention,
education, and access to oral health care. The platform
also includes a commitment to ethics in the way in which
the Company operates; support for the development,
health and well-being of our employees; support for
communities both locally and globally; transparency
within the marketplace, and a commitment to safety
and reducing our environmental footprint.
As leaders in our industry, we believe in leading by
example. There are many ways in which Dentsply Sirona
and its employees work to make a difference and give
back. We believe that continuing to strengthen our
corporate philanthropy initiatives will help forge longlasting and trustworthy relationships with customers,
employees, business partners, and shareholders.
Our Transformation Will Help Us Achieve
Our Mission and Vision
As we innovate, grow and create value for our
stakeholders, we will have more resources to commit
to Our Mission, to empower dental professionals to
provide better, safer, faster dental care. As we fulfill our
Mission, Dentsply Sirona will change the way dentistry is
practiced and improve oral health worldwide, a critical
part of our Vision.
I want to thank our employees around the world for
helping us develop, market, deliver and service our
products and solutions around the world. Thank you to
the clinical community�both internally and externally�
for all of your insights and support of advancing
dental care. Finally, thank you to our shareholders,
for your confidence in us as we aim to deliver leading
shareholder returns.
Respectfully yours,
Don Casey
Chief Executive Officer