To our stakeholders:
2012 was a year of unprecedented change
for our company and industry, brought on by
intense competition from natural gas and aggressive
regulations that are forcing many power plants  our
customers  to close, often prematurely.
At the same time, were operating in a global economy still
recovering from major downturns in the U.S. and Europe,
even as we contend with slower growth rates in Asia.
Facing these new realities, Alpha took decisive action last
year to reposition itself. In this year of significant transition,
we idled production to match our business to a smaller
market, and streamlined our organization by removing layers
of overhead and costs.
Knowing they were necessary did not make these changes
any easier to carry out. Closing mines and cutting jobs was a
last resort in a tough situation. In the long term, however, we
believe these decisions will leave Alpha in a position not just
to survive, but to thrive.
Looking ahead, we know that our future depends heavily on
broadening our customer base, finding new ways to create
value and continuing to produce a vital source of energy while
keeping with the best environmental and safety standards.
Demand for our product is growing worldwide, and Alpha has
the reserves, production infrastructure and export capacity to
serve both developed and developing nations. Forecasts indicate
that by the end of this decade, more than 100 million tons of
additional metallurgical coal will be required to produce the
steel that goes into durable goods and modern infrastructure.
Enhancing Alphas status as the leading U.S. producer of

quality metallurgical coals for steelmakers around the world
was central to our 2012 repositioning plan.
Demand for thermal coal, too, is growing overseas. According
to a recent report from the International Energy Agency, by
2017  if not before  coal will overtake oil as the worlds
most consumed fuel.
Coal is abundant, reliable and affordable. As such, it offers the
only realistic solution in many areas of the developing world
where the need for electricity is great and growing. Reliable
and affordable energy brings many benefits, including clean
water, better sanitation and, in general, a chance to escape
poverty and start building a modern economy.
For hundreds of millions of people  no less than one-fifth of
humanity  securing basic access to electricity marks the first
step from subsistence to prosperity.
In the pages that follow, you will find a detailed assessment of
the past year and our plans going forward. In challenging times
we remain optimistic. We have also stayed grounded in the
values and principles that Alpha was founded upon 10 years
ago. First among these is our belief in the 12,000 men and
women who work in this organization and our commitment to
getting them home safely at the end of every shift.
Anyone involved in mining understands that the work of
safety requires constant vigilance and improvement. This
summer we will open the doors to the first-of-its kind Running
Right Leadership Academy, a state-of-the-art center for
education, training and development that eventually will be
open to the entire global mining community.
We take responsibility for the environment, where our duties
extend from mine restoration, to assuring water quality,
to auditing and measuring how were performing. Weve
made progress and will continue to push the boundaries of
innovation, research and best-practice stewardship.
Finally, we invest in the communities we call home, especially
in giving assistance to children and families. Alpha helps
support education, arts and culture, and social services
initiatives that meet local needs. All the more when the
economy is down, these investments make a difference.
We move on in 2013 ready and renewed, setting our minds to
new challenges. We know that further adversity may come,
but we wont lose heart and we wont stop evolving as we
confront these new challenges. Across the world, what some
call yesterdays fuel is tomorrows irreplaceable resource,
and the world cant wait. Millions of people are counting on
coal to bring them a better life, and serving that aspiration is
the great opportunity of today.
Alpha has dedicated itself to that mission, and in carrying it
out we strive to serve the interests of all our stakeholders.

Kevin S. Crutchfield, Chairman and CEO
