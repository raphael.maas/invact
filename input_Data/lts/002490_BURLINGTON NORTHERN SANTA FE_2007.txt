The year 2007 was a year of challenges and successes.
In a difficult economic environment, the strength
of BNSF was evident as our dedicated employees once
again delivered reliable service, captured market
opportunities and controlled costs.
While we faced many challenges, including a soft
economy and record fuel prices, we produced solid
returns for our shareholders because of the diverse
nature of our business portfolio. We generated record
free cash flow in excess of $1 billion before dividends,
which allowed us to increase our dividend for the
fifth consecutive year; continue our share repurchase
program, which began in 1998; and invest in capital
to maintain our infrastructure and selectively add
capacity for long-term growth.
Despite these successes, however, 2007 was a
disappointing year for safety. Sadly, we lost four colleagues to fatal injuries in 2007. And, after about a
decade of year-over-year improvement, our employee
injury frequency rate increased in 2006 and again in
2007. These results are especially frustrating because
we are a leader in most other areas of safety, including
highway-rail grade crossing safety. We clearly have
more work to do toward our goal of an injury-free
and accident-free workplace. We are increasing our
focus on safety performance across the railroad with
determination to improve. We believe that we have
an excellent employee safety framework in place; our
challenge is to ensure that these programs are consistently implemented and that we better disseminate
best practices within these areas.
As I review the year in more detail, I will outline
how the diversity of our business portfolio helped
our results and how we adjusted our costs and capital
investments in response to the soft business conditions.
I will also review our ability to provide reliable service
and a strong network to meet customer needs, our
focus on capital investment to support long-term
growth, and our commitment to corporate citizenship.
Maintaining a balanced portfolio
Our 2007 revenues increased approximately
$800 million, or 5 percent, to an all-time record of
$15.8 billion. Our results were helped by our diverse
portfolio of business. The wide variety of commodities
we handle gives our company the ability to weather
cyclical downturns. Even though consumer products
volumes and commodities related to housing were
down, we had strong demand in agricultural products. The diversity of our portfolio makes us less
vulnerable to changing market demand than many
other industries, and our 2007 results underscored
that strength.
Our biggest success in 2007 was our agricultural
business, which set an all-time revenue record at
$2.7 billion. Our volume increased 6 percent in this
area, as we handled record levels of wheat, ethanol
and bulk foods. A strong domestic harvest, high
global demand for wheat, and significant growth in
ethanol volume combined for a very successful year
for this business segment.
For coal, we also had record volumes and record revenues of $3.3 billion. Although mine production issues
during part of the year impacted our volume, we were
able to improve service delivery year-over-year. We are
optimistic that demand will remain strong for the lowsulfur coal we transport from the Powder River Basin
in Wyoming and Montana. 
Matthew K. Rose, Chairman, President and Chief Executive Officer
BNSF2007 Annual Report and Form 10-K | 13
Industrial products volumes decreased slightly; however,
revenues increased 3 percent to $3.7 billion. Continued
strong demand for petroleum products helped to offset
the decline in the housing market, which had a negative impact on all of our building products, including
lumber, panel products and other building materials.
Our consumer products revenues increased to a
record $5.7 billion. Our unit volumes for consumer
products, however, decreased due to softening demand
for imported consumer goods and a shift in strategy
by one of our largest international intermodal customers. We are optimistic that the long-term demand
for Asian goods will rebound as the economy improves.
During a year of mixed demand, we worked hard
to control costs. With regard to our largest expense
category, compensation and benefits, year-over-year
expense was down 1 percent on flat employee headcount. Wages and benefits increases were offset by
lower variable compensation costs and other costcontrol measures.
Our second-largest expense area, fuel, was up 17 percent, due to higher prices and reduced hedge benefits.
We were able to recover a portion of the additional
expense through our fuel surcharge program. Additionally, we were able to achieve record fuel efficiency
with a nearly 3 percent improvement over 2006, avoiding the purchase of more than 38 million gallons
of fuel, saving $90 million, and benefiting the
environment by burning less fuel per ton-mile.
Focusing on velocity and service
We recognize that providing reliable service is the
best way to ensure our continued growth, especially
when the economy begins to recover. We must
continue to realize our vision by providing transportation services that consistently meet our
customers expectations.
In 2007, we continued our companywide focus on
velocity. This initiative, which started in 2005, is
increasing our on-time performance and asset utilization. Velocity is much more than the speed at which
trains travel. It includes the number of times that
We were able to achieve record fuel
efficiency with a nearly 3 percent
improvement over 2006, avoiding
the purchase of more than 38 million
gallons of fuel, saving $90 million.
equipment is handled and how long freight remains
stationary. Our people examine every process to eliminate inefficiencies and gain more productive use of
our track, equipment, terminals and other assets.
Efforts throughout the company helped us improve
our performance for all six of the velocity measures
we track. I am proud of our team, who achieved this
while coping with severe flooding midyear in the
Southeast and Upper Midwest, wildfires in October
in Southern California, and record flooding in
December in the Puget Sound region. In 2008, our
continued focus on velocity will further improve
our efficiency and increase the capacity of our existing assets, while helping us better serve new and
existing customers. 
Our 2007 revenues increased approximately $800 million,
or 5 percent, to an all-time record of $15.8 billion.
I am also proud of our team for achieving a perfect
peak season for United Parcel Service (UPS). We
handled 34,366 UPS loads with zero service failures
between Thanksgiving and December 23 in 2007, and
the streak of failure-free service continued for 53 days
through mid-January 2008. This was our fourth UPS
peak season with perfect performance since 1999.
Planning for long-term growth
Over the long term, I am very optimistic about the
future of rail transportation. My perspective comes,
in part, from my work as a member of the National
Surface Transportation Policy and Revenue Study
Commission. Formed by Congress in 2005, this
commission included some of our nations top transportation experts and leaders in business, government
and academics. We were asked to consider the future
needs of the nations surface transportation system
and to recommend funding options; the results of our
two-year study were released in January 2008.
This independent commission found that a strong
freight railroad industry is vital to the future of
our country. Today, the U.S. rail industry transports
about 40 percent of the nations goods, in terms of
tons handled and distance moved, for only 13 percent
of the overall transportation cost. And the value
of rail continues to rise. The U.S. Department of
Transportation projects that demand for rail freight
transportation, measured in tonnage, will increase
88 percent by 2035.
Unlike other modes of transportation, U.S. railroads,
including BNSF, own and maintain their transportation rights of way. Simply put, we fund the building,
maintenance and repair of a private rail highway
that delivers tremendous value for our customers and
the nation. Our transportation network is critical
to the movement of goods and our nations economic
growth, global competitiveness, environmental
sustainability, safety and overall quality of life.
In the 10 years that ended in 2007, BNSF spent nearly
$24 billion to improve our infrastructure. That includes
maintaining a strong infrastructure through strategic
investments in expanded track, yards and terminals;
track renewal; technology; and acquiring more than
2,700 new, high-efficiency locomotives. All of our
investments in infrastructure, equipment, asset utilization, people and technology help ensure we have the
capacity to meet current and future freight transportation needs, while also improving our operating
reliability and efficiency.
In 2007, we added a total of 183 miles of new track,
focusing on expanding capacity of our transcontinental main line linking California to Chicago and
our track serving coal territory in Wyoming and
Nebraska. These two freight rail corridors are some
of the busiest stretches of track in the world. We also
acquired another 200 high-horsepower, high-efficiency
locomotives. Our newest locomotives are 15 percent
more fuel efficient than the units they replaced, which
also reduces emissions and benefits the environment.
Our investments help ensure that we have the capacity
we need to meet our customers and our nations
transportation needs today and in the future.
But we were also careful to calibrate our capital
spending in response to market conditions. In all, our
2007 capital commitments totaled about $2.6 billion.
This number was down from 2006, as we reduced our
projected spending during the year in response to
the softer demand. We are committed to expanding 
BNSF2007 Annual Report and Form 10-K | 15
our capacity to handle growth over the long term, but
we always balance those investments with short-term
market conditions and demand.
Over the past several years, the public policy debate
around economic regulation of the railroad industry
has heated up. The outcome of this debate has a direct
impact on our ability to continue to invest to meet the
projected future demand. Changes in economic regulation have implications for our customers as well as
the national highway network.
The Commission found that our nation will require
significant increases in rail capacity to encourage
economic growth and help meet energy efficiency and
environmental goals. Any change to public policy
needs to be measured against whether it will expand
or shrink rail capacity. Public policy should support
the expansion of rail capacity.
Demonstrating responsible corporate citizenship
We recognize that, as one of our nations largest railroads, we are a critical part of the global supply chain.
We take that responsibility very seriously. We add
value by providing freight capacity that, in most cases,
is lower cost, more fuel-efficient, more environmentally
friendly and safer than other modes of surface transportation. At BNSF, we are working to align our
business decisions and our environmental, social and
economic commitments to create value for customers
and all stakeholders today and in the future.
To raise awareness of the contributions we are making
today and what we are working to achieve, we have
issued our first Corporate Citizenship Report. You can
find this Corporate Citizenship Report on our website
at www.bnsf.com. This report provides significant
insight into four key areas  our Environmental
Leadership, our Safety Commitment, our People and
Communities, and our Economic Impact.
Investing in our people
While our hiring has slowed down to reflect market
conditions, we are proud of our efforts to replace
retiring workers with military veterans who possess
experience and technical knowledge that we need. In
2007, nearly one-third of our new hires were veterans.
In recognition of our commitment, magazine
put us at the top of their 2007 list of Americas
50 Most Military Friendly Employers. After ranking
high on this list for several years, we are proud of
achieving this first-place position in 2007.
Ultimately, our success rests on the quality of our
people and our sensitivity to the needs and concerns
of our shippers, our investors and our communities.
We believe that we made significant progress in
2007, despite a soft economy and other challenges,
and we are well-positioned for the future. As always,
I credit the dedication, focus and resilience of our
people, and I thank all of our stakeholders for their
interest and support. Together, we can look forward
to the continued growth and strength of our rail
franchise in 2008.
Matthew K. Rose
Chairman, President and Chief Executive Officer
February 14, 2008