2017 was a transitional year for The Manitowoc Company.
The accomplishments of the past twelve months are many and represent
the best eorts of our 4,900 dedicated Manitowoc employees worldwide.
Our results indicate that our strategy is working and we are experiencing
solid progress in all facets of our business.
2017 marked our second year as a stand-alone crane company
and the second full year of the companys cultural and operational
transformation, which has been guided by the principles of
The Manitowoc Way. I am pleased to report we have made and
continue to make significant progress in our journey to be
the leading manufacturer in the markets we serve.
Market conditions remained di?cult in 2017, reinforcing the need to focus
our eorts on matters within our control. We took further actions to
flatten our organizational structure, streamline processes, reduce the
cost base and improve productivity. These actions included completing
the relocation of our crawler crane production and maximizing our manufacturing capabilities in Europe. Despite
the depressed market conditions, we continued to invest in technology and innovation, largely directed by
customer input and feedback in a process we call the Voice of the Customer. In 2017, Manitowoc delivered some
of the most ground-breaking and value-enhancing products in company history, which are clearly resonating well
with our customers. These accomplishments translated to improved earnings and cash generation and an order
backlog that continues to improve.
As 2017 closed, we began to see signs of improvement in several markets and improving customer sentiment,
particularly in North America. The many changes that have been implemented have positioned us extremely well
to capitalize on opportunities as end markets improve. I am cautiously optimistic the global crane market has
troughed, and I could not be more excited about our future. Two years ago, we embarked on our journey to
transform our companys culture and business
practices through the implementation of
The Manitowoc Way. The goal was to create a
customer focused culture with continuous
improvement aecting every aspect of our
business. I am pleased to report the organization
is fully embracing this ideology, and we are living
and breathing the principles of The Manitowoc Way,
each and every day.
The Manitowoc Way also forms the foundation for
our strategic priorities: margin expansion, growth,
innovation and velocity. These principles are not
just implemented on the shop floor; instead they
represent a paradigm shift in our culture, and how we
approach daily tasks and engage with our customers.
They have not only provided the means for us to
improve operational eciency, but also to forge
better relationships with our customers and deliver
improving financial results. I strongly believe that this
new way of thinking has been the driving
force behind the success we have achieved
over the past year. Guided by the principles of
The Manitowoc Way, we are building a stronger,
resilient and more valuable company for our
key shareholders.2017 proved to be another challenging year, as we continued to face prolonged weakness in numerous markets,
particularly those affected by depressed oil prices and geopolitical disruptions. European sales stabilized and the
U.S. markets continue to recover, primarily driven by strong customer reaction to our new products that drove
growth. However, crane sales remained soft in the Middle East and Asia-Pacific in the markets we serve.
Our full-year net sales were down $32 million, or 2%, which is a sharp improvement compared to a decline
of $252 million in 2016, or 14%. We achieved sequential improvement throughout the course of the year,
culminating in healthy year-over-year 27% sales growth in the fourth quarter. We also finished the year with
a 78% increase in our fourth quarter year-over-year order intake. I am pleased to report that we have now
seen year-over-year growth in orders for four consecutive quarters. For the full year, orders were up 32% to
approximately $1.9 billion, and year-end backlog hit a two-year high of $607 million.
We remained focused on expense control and operational excellence throughout the year. We reduced our
Selling, General, and Administrative costs by another 10% year-over-year. For the full year, we reported adjusted
EBITDA of $67 million, which reflects an increase of $49 million over 2016, despite lower revenue. The fourth
quarter marked the third consecutive quarter we generated positive adjusted EBITDA since we became a
stand-alone crane company.
Equally important, we continued to manage our balance sheet prudently, balancing the need to maintain
sufficient liquidity while reinvesting in our growth initiatives. In 2017, we generated $79 million in cash flow from
operations, which reflected an improvement of more than $200 million over 2016. This was driven largely by
steadfast efforts to manage inventory and working capital more effectively. We reduced net debt by $55 million
and ended the year with $223 million in liquidity.The first key element of our strategy is
margin expansion. I am very pleased to
report that we continue to make solid and
steady progress on this strategic priority,
having generated more than 350 basis points of
improvement in adjusted operating income margin
in 2017 alone.
Our early initiatives focused largely on optimizing
our global footprint and rationalizing our capacity.
In June of 2017, we completed the relocation of our
crawler crane manufacturing from Manitowoc,
Wisconsin to Shady Grove, Pennsylvania. We
did this ahead of plan and under budget. In Europe,
we are realigning our factories by reallocating
capacity among the plants to increase velocity
and quickly meet the evolution of our
customers demand.
Our 2017 eorts have been largely directed at process
improvement and enhanced productivity. We focused
on rationalizing product lines, re-engineering plants
and standardizing platforms and components. We
have also introduced robotics wherever possible.
By adding automation to our Niella Tanaro, Italy
facility, we increased our daily production by 30%.
In our German facility, the capital investments we
have made are paying dividends as demand for
their product continues to increase globally. In the
aggregate, the initiatives undertaken are enabling
us to reduce lead times considerably and increase
output in less time and less space.
As a leaner, more agile organization, we are very
well-positioned to achieve significant operating
leverage and margin expansion as end markets
continue to improve. We remain committed to
achieving our goal of double-digit operating
margins by 2020, or 10 x 20.Innovation is deeply rooted in our culture
and serves as a key driver of growth.
Even in down cycles, we continue to
invest in resources to strengthen and
broaden technologies to maintain our leadership
position. Our goal is to design solutions to support
customer needs while also meeting their cost and
productivity objectives. Our development strategy
is to introduce products that customers want
and perform well above the standard set by our
competition and customer expectations, which
enables us to grow by gaining share regardless
of market conditions.
We launched 17 new cranes since becoming a
stand-alone crane company, all of which were
designed and built based upon customer input. New
and dierentiated features of these cranes include
industry-leading lifting capacities, remote diagnostic
tools and standardized operating systems/CCS.
In March at CONEXPO, one of the industrys
most important trade shows, we showcased 12
new cranes, eight of which were developed from
design-to-prototype build within 12 months and
two within six months. This accelerated speed to
market allows us to remain on the cusp of market
trends and enables us to quickly translate the
Voice of the Customer into a saleable product.
Customer feedback on new products has been
nothing short of remarkable. We saw a record
number of orders relating to these products,
and more than 40% of our fourth quarter revenue
came from new models.
Going forward, we will sustain the cadence of our
new product roll-outs, including the introduction of
a number of new products during the Crane Days
event at our Shady Grove, PA facility in June 2018.While end market demand remained
sluggish in 2017, we drove growth through
unique product and service oerings,
and gained market share in traditional
channels by delivering superior products. Our Grove
TMS9000-2 truck-mounted crane was particularly
well-received as its functionality far exceeded
the existing industry standard. It has become
Manitowocs best-seller in this crane category.
We are achieving measurable progress in our military
business to drive growth in a down market. Field
testing is being finalized to support our $192 million
contract award for purpose-built all-terrain cranes
for the U.S. Army. We expect all testing to conclude
in first quarter of 2018. The first production cranes
are anticipated to be fielded later this year, with
production beginning in the second quarter of 2018.
We continue to grow our Potain brand, which
celebrates 90 years of producing tower cranes
in 2018 and is the most respected global name in
tower cranes. 2017 was another year of above-market
growth for these products, most notably in
Europe where we saw over 30% revenue growth
year-over-year. Potain launched two new models
of tower cranes in 2017, both of which were
enthusiastically accepted by our customers,
and position us as the worlds most innovative tower
crane manufacturer.
EnCORE, our crane reconditioning program
is also a key dierentiator for Manitowoc. By
remanufacturing used cranes, we oer an attractively
priced entry-level product to meet the customers
preference. Demand for these cranes has been
robust, and margins are proving to be lucrative as we
leverage our ability to increase factory utilization of
our plant assets. This business is extremely important
to us as it provides stability during down markets.
We also delivered continued growth in our
aftermarket business, Manitowoc Crane Care, which
was a key focus area in 2017. Aftermarket sales create
an attractive recurring, less cyclical revenue stream
and also generate higher margins.
Overall, results of these eorts to drive growth were
a resounding success. We returned to sales growth
in the third and fourth quarters. In fact, our fourth
quarter sales were the strongest we have reported in
the past eight quarters. We also added nearly $300
million to backlog during the year, an increase of 87%
over the backlog at the end of 2016, the majority of
which is expected to ship in the first half of 2018.Velocity is at the core of The Manitowoc Way.
Velocity does not equate solely to speed,
rather it reflects the concept of moving
quickly but with direction and purpose.
In business, it signifies the ability of an organization
to respond swiftly and eectively to ever-changing
market conditions.
Velocity touches all our strategic priorities. It is
a key element of most lean initiatives and a key
driver of margin expansion. It fuels accelerated
new product development and supports advanced
manufacturing techniques.
Ultimately, it translates to empowering employees
to generate more output at a faster speed in less
space for less cost. I am proud of the hundreds of
improvements, large and small, that our employees
have contributed over the course of the last two years
which have been incorporated into our operations.
While much progress has been made to enhance the
agility and flexibility of our operations, significant
opportunity remains to drive further productivity and
process improvement throughout the organization. As we look to 2018, we are enthusiastic about our future. We are seeing stabilization in many of our markets and
improvement in others. Leading indicators such as utilization rates, rental rates, and used equipment pricing
continue to trend positively, and sentiment within our distribution channel has improved markedly. We remain
hopeful a new federal infrastructure program will progress.
Our company is very well-positioned to benefit from an improving global macroeconomic backdrop; however, if
conditions remain at these lower levels of demand, we will continue to attack those matters within our control.
We have a healthy pipeline of new products to be launched this year, and we expect various cost savings and
productivity initiatives to continue to improve financial performance. We are poised to achieve our annual
margin improvement targets, despite continuing trade, regulatory and geopolitical uncertainty.
CONCLUSION
2017 was a successful year for Manitowoc, driven by the power of The Manitowoc Way. We accelerated new
product development while improving the quality and value proposition for our customers. As a result, we
are enhancing our market position and expanding share. The implementation of various lean initiatives has
improved productivity and efficiency dramatically, while creating significantly more flexibility. And it is evident
that the organization is embracing our new culture wholeheartedly. All of this has translated to dramatically
improved financial performance.
As a leaner and more agile company, we are poised to capture growth as market conditions continue to
improve. We are set up very nicely for a solid 2018, and I could not be more optimistic about our future.
Our 2017 accomplishments reflect the hard work and commitment of our key stakeholders, those being
our customers, shareholders and employees, and I would like to personally thank all of them for their
contributions. I would also like to thank our Board of Directors for their continued support, and those
investors that remain committed to us in rapidly changing times as we forge ahead on our journey of
continuous improvement.
Looking Forward
Barry L. Pennypacker
President and Chief Executive Ocer
March 2018 