Theres no doubt about it.
Scripps Networks Interactive, Inc. had an exceptional year in 2009.
Despite the most difficult economy in a generation, the company emerged stronger than ever
and is in excellent position for meaningful, long-term growth.
In addition to the solid operating performance of our Lifestyle Media businesses, we took great
strides forward on key business objectives and moved decisively to take advantage of promising
opportunities in the global media marketplace.
Consider these accomplishments:
 New, multi-year carriage agreements with distributors that better reflect the value and
tremendous popularity of our networks.
 Record-shattering audience growth at HGTV and Food Network.
 The addition of the Travel Channel, giving us our third, fully distributed U.S. television
network and another promising lifestyle category to exploit.
 The conception  and soon the birth  of the new Cooking Channel, a premium-tier
network intended to satisfy the growing hunger media consumers and advertisers have
for more food programming.
 International expansion  starting with the Food Network in the U.K. and Asia  through
promising new joint ventures with knowledgeable and established partners around the globe.
In other words, we had a productive, rewarding year.
Lifestyle Media Growth
With all that positive momentum, its difficult to recall that we started 2009 with a good measure
of trepidation.
The economy was struggling, and industry forecasts for advertising were dire. We were prepared
for the worst. But as the year progressed, the economy, though still struggling, began to stabilize,
and ad sales improved. Our Lifestyle Media businesses, excluding 16 days of Travel Channel results,
finished 2009 with $1 billion in advertising revenue, which was equal to the prior year. In the fourth
quarter, ad sales were up a very solid 7 percent, and that positive trend has continued into 2010.
Combined with 16-percent growth in affiliate fee revenue, total revenue from our Lifestyle Media
businesses grew 3 percent to $1.4 billion in 2009. Lifestyle Media segment profit, excluding
Travel, reached about $632 million, which was down just slightly from the prior year. Finishing the
year as strongly as we did underscores the sturdiness of our lifestyle content businesses and their
ability to withstand an economic downturn. Our Lifestyle Media business segment, of course,
is our largest, accounting for 89 percent of total revenue.
Consolidated Results
On the strength of our networks, total consolidated revenue for the company topped $1.5 billion
again in 2009, down slightly from the prior year. Total segment profit for 2009 was $602 million
compared with $645 million in 2008. Including one-time items in both years, earnings per share
were $1.81 in 2009 compared with $0.14 in 2008.
Consolidated results were held back in 2009 by a decline in revenue and segment profit at our online
comparison shopping subsidiary, Shopzilla, which includes the Shopzilla, bizrate and beso Web sites.
The decline in Shopzilla results was anticipated as we worked during the year to differentiate the
business from competitors by creating a richer Internet shopping experience for consumers and by
delivering more marketing value to our merchant partners.
Key metrics, including the comprehensiveness of search results and double-digit growth in the
number of direct merchant leads, tell us that were moving the business in the right direction.
Looking ahead, we expect Shopzilla to be growing again by the second half of 2010.
Successful Renewals
The companys top priority in 2009 was renewing carriage agreements at rates that are more in line with
industry peers and, more importantly, capture the value weve created at our programming businesses,
particularly Food Network. Were happy to report that we achieved what we set out to accomplish.
With new multiyear distribution agreements in hand for Food Network and HGTV, we expect affiliate
fee revenue to grow from $322 million in 2009 to between $430 million and $440 million in 2010, not
including the $100 million in affiliate fee revenue were expecting from Travel Channel. Our new,
improved agreements provide meaningful growth for years to come in one of the companys two
primary sources of revenue. Our 2009 affiliate renewals were a success by just about anyones measure.
Shattering Records
Supporting our case for higher affiliate rates was the tremendous audience growth at both HGTV
and Food Network. Viewership at both networks reached all-time highs during 2009 and, importantly,
each is attracting increasingly younger audiences.
At Food Network, the average age of our viewers has gotten younger over the past five years and is
now about 45  right in the sweet spot for advertisers. Total household viewership grew 22 percent
in primetime during 2009. Breakout hits such as Diners, Drive-Ins and Dives; Ace of Cakes; and
Iron Chef America continued during the year to delight and entertain a growing fan base. The
finale of our popular Next Food Network Star competition show was the most-watched program
on all of television when it aired on a Sunday night in August. About 4.7 million viewers tuned in.
Food Network Magazine is highly successful as well, outpacing our expectations with a very
impressive 1.3 million readers counting subscriptions and single-copy sales. Food Network,
as one of our advertisers described it, has become a power brand.
At HGTV, we repositioned the networks programming strategy to better reflect the economic
times, and viewers responded enthusiastically. December total household viewership was up
16 percent in primetime on the strength of shows like Property Virgins, My First Place and Property
Income that were aimed at first-time homebuyers and homeowners looking for creative solutions in
tough times. Successful tent-pole programming events like HGTV Design Star and the HGTV Dream
Home Giveaway are perennial successes, as are our sturdy and most popular series, House Hunters
and House Hunters International.
As popular as HGTV and Food Network are today, we believe theres plenty of opportunity for
growth. One of our primary business objectives is to build on the success weve achieved at HGTV
and Food Network by attracting larger audiences and further leveraging the marketing power of
these universally recognized brands.
The Travel Bug
And now, with the addition of Travel Channel, we have the opportunity to create even more value for
shareholders in yet another promising lifestyle category. Fully distributed in more than 95 million
homes, Travel Channel had considerable success of its own building a larger audience in 2009. Total
household primetime viewership grew 8 percent during the year as young viewers turned creative
shows like Man v. Food and Ghost Adventures into pop-culture favorites. We completed our
acquisition of the Travel Channel on December 15 and are right on plan to fully integrate it as
a Scripps Networks Interactive brand in 2010.
The Cooking Channel
Also by mid-year 2010  Memorial Day Weekend  well unveil the new Cooking Channel. Designed
as a premium-tier network, the Cooking Channel and its companion Web site will offer foodies
a deeper-dive viewing opportunity. And for advertisers, the Cooking Channel presents another
valuable marketing platform in one of televisions fastest-growing programming genres. The
Cooking Channel is expected to step directly into the valuable distribution footprint that we
created with its predecessor network, Fine Living.
Going Global
International expansion was another top priority for the company in 2009, and much has been
accomplished. Food Network can now be seen in more than 10 million households in the United
Kingdom as part of a joint venture we control and created with Liberty Globals Chello Zone. And, we
intend to be in business in India when we complete a transaction that will give us controlling interest
in the countrys leading English-language, lifestyle television network. Our decision to invest modest
amounts of capital to take ownership in international ventures, we believe, provides the company
and its shareholders with an opportunity to benefit more directly from the value were able to create.
Financial Strength
Overall, the companys financial condition couldnt be stronger.
We finished the year with a solid balance sheet: $254 million in cash and $884 million in debt that
we used to finance the Travel Channel transaction. With our long-term debt obligation at just more
than one-times annual segment profit, the company has more than enough financial flexibility
to meet its operating requirements and to act on opportunities to expand in the marketplace
should they present themselves.
So there you have it.
Scripps Networks Interactive, with its valuable portfolio of television and interactive brands,
is strong today, and it will be stronger tomorrow.
As always, I consider it a privilege to be leading this outstanding enterprise.

Sincerely,
Kenneth W. Lowe
Chairman, President and Chief Executive Officer