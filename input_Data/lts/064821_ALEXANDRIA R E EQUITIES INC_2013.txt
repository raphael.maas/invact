2013 was a year of strategic long-term positioning for Alexandria. As a result,
we are now fully prepared for a return to solid and stable per share earnings and
asset value growth in 2014 and beyond.
The continuing strength of Alexandria�s core real estate operations
is undeniable. Our best-in-class urban science and technology clusters,
including Cambridge, San Francisco, New York City, Seattle, and San Diego,
are unequivocally the most important real estate markets for the life science
and innovation industries. Our irreplaceable AAA locations, which, as the first
mover in our niche, we have strategically assembled adjacent to each cluster�s
leading academic and medical institutions � such as the Massachusetts Institute
of Technology and the University of California, San Francisco (UCSF) � provide
our client tenants with unique and extremely valuable opportunities to access
innovation and collaboration. Our Class A real estate, made up of state-of-theart urban science campuses at the forefront of sustainability and creativity,
meaningfully enhances our client tenants� productivity and ability to recruit
and retain the best and brightest talent. And our fully integrated regional teams,
with decades of experience in life science and real estate, work diligently every
day to provide first-class service to our client tenants. These factors combine to
deliver record leasing activity and strong occupancy growth. As my friend and
colleague Dr. Marc Tessier-Lavigne, former chief scientific officer and executive
vice president of Genentech and current president of Rockefeller University,
succinctly noted of our new and expanding campus and the New York City
cluster, �It doesn�t get better than that.�
In 2013, Alexandria enhanced this solid foundation with
the completion of significant high-value Class A development
and redevelopment projects in our best-in-class urban science
and technology clusters. In New York City, we began to deliver
certain floors of the West Tower at the Alexandria Center� for
Life Science. With core and shell development completed in
only 13 months, the approximately 419,000 RSF West Tower
is the second phase of our flagship campus � the first and
only commercial science park in Manhattan � and represents
a critical milestone in the development of New York City�s
vibrant life science cluster.

At our West Tower opening ceremony, which we co-hosted with our anchor client tenant,
Roche, we were humbled to hear Dr. John Reed, a longtime friend and head of Roche Pharmaceutical
Research and Early Development, tell the audience, �We are pleased to have the Translational
Clinical Research Center (TCRC) based at the Alexandria Center for Life Science and excited by
the collaborative opportunities afforded by its proximity to patients and world-class expertise in
experimental medicine. New York City has much to offer � it is the most populated city in America, has
seven outstanding medical schools, and also ranks second in the U.S. in medical research funding.
Therefore, we believe this is the best location to pursue the goals of the TCRC as well as our global
research and development organization.� It was a proud moment for me and the entire Alexandria
team, as John�s statement affirmed our long-held vision to develop a world-class life science cluster
in New York City.
In Cambridge, Alexandria delivered 225 Binney Street to Biogen Idec for its premier
headquarters building in late 2013. Approximately 1,200 employees have relocated from Biogen
Idec�s former Weston, Massachusetts, headquarters to the unparalleled life science cluster in
Kendall Square. The oldest and largest biotech company based in Massachusetts, Biogen Idec
now leases approximately 305,000 RSF of high-quality life science space at Alexandria�s 225 Binney
Street project.
Recently named the Phillip A. Sharp building in honor of the Nobel laureate and Biogen
Idec co-founder and former chief executive officer, 225 Binney Street is the first completed groundup development in what will become Alexandria�s 2.6 million RSF world-class urban science and
technology campus, the Alexandria Center at Kendall Square. Alexandria�s campus is located in the
epicenter of world-renowned Kendall Square and is Cambridge�s newest destination for innovation,
collaboration, and the translation of breakthrough discoveries into high-impact, cost-effective
products that will save lives, manage illness, and reduce the economic burden of disease on society.
The urban science and technology campus will include Class A facilities and collaborative amenities
that will provide leading life science entities with a unique opportunity to drive productivity,
accelerate innovation, and recruit and retain the brightest scientific minds.
In addition to the completion of these and other key development and redevelopment
projects, Alexandria made many important improvements to its long-term capital structure in 2013.
Along with monetizing significant non-income-producing land parcels through developments such
as the West Tower, we sold certain non-strategic income-producing assets and land for approximately
$275 million in 2012 and 2013. Proceeds from these sales were invested into high-value, Class A
development projects located in our best-in-class urban science and technology clusters with an
estimated value of approximately $450 million, representing an increase of approximately $175
million above the value of the properties sold.
The fourth-quarter 2013 $55 million sale of a land parcel in San Francisco to Kaiser
Permanente was a particularly remarkable accomplishment, as it represented the completion of our
development and/or monetization of our land in the Mission Bay cluster. In 2008, at the beginning
of the worldwide financial crisis, Alexandria carried land that could support the development of
approximately 2.5 million RSF of laboratory/office facilities in Mission Bay. As of the end of 2013,
we have fully built or monetized this land. Today, our operating portfolio in the Mission Bay cluster
is 100% leased to world-class client tenants including Bayer, Celgene, Illumina, Nektar, and Pfizer.
It was a challenging road, but we are proud of our unique capability to develop a world-class life
science cluster in Mission Bay and, most importantly, certain that it added significant long-term
value for our shareholders.

In 2013, we were also pleased with the successful execution and excellent timing of our second
10-year unsecured bond issuance of $500 million at an interest rate of 3.90%, as well as a strategic
$536 million follow-on common stock offering. The long-term debt and common stock offerings
improved our capital structure and funded our Class A developments, including our world-class
Alexandria Center at Kendall Square campus in Cambridge. As a result of these efforts, Alexandria�s
balance sheet is capable of supporting solid and stable per share earnings and asset value growth
with the continuous onboarding of EBITDA from our value-creation pipeline.
In the life science industry, 2013 was an exceptional year, particularly for pharma and biotech.
In the U.S., 46 biotech companies completed initial public offerings (IPO) and raised approximately
$3.6 billion in the strongest IPO market in more than a decade. Impressively, the Nasdaq Biotech
Index outperformed the general market with a 66% increase through the end of 2013, driven by
meaningful clinical, regulatory, and commercial achievements. The Food and Drug Administration
(FDA) continued to approve a significant number of innovative, safe, and effective new treatments.
I�m pleased to report that 59% of all drug approvals granted by the FDA in 2013 were granted to
Alexandria�s client tenants. Additionally, in 2013, Alexandria continued to lead its REIT peers with
the highest percentage of investment-grade client tenants. As of the end of 2013, 51% of Alexandria�s
total annualized base rent and 82% of Alexandria�s annualized base rent from its top 20 client tenants
were generated from investment-grade client tenants. It�s an amazing metric and a true testament to
the strong underwriting capabilities of Alexandria�s life science team.
Looking forward in the life science industry, I am excited by the emergence of several
new classes of drugs and drug targets including the immuno-oncology field, the microbiome,
and, importantly, ribonucleic acid therapeutics. The era of precision medicine is also driving the
growth of a new class of medicines for rare and orphan diseases and for oncology. In addition, the
convergence of life science and technology through digital health is poised to have a transformative
impact on healthcare by enhancing collaborations, improving outcomes for patients, and reducing
healthcare costs.
Anticipating the digital health revolution, Alexandria strategically recruited the premier
full-service seed fund in digital health, Rock Health, to locate its headquarters in our Mission Bay
cluster, which is ideally situated proximate to San Francisco�s tech hub South of Market. Founded
in 2010, Rock Health collaborates with top-tier partners, including GE, Genentech, Harvard Medical
School, Kleiner Perkins Caufield & Byers, the Mayo Clinic, and Qualcomm Life, to fund and support
digital health entrepreneurs through an innovative startup accelerator model, dynamic science and
technology events, and open-source research. Rock Health�s founder and chief executive officer,
Halle Tecco, said, �Rock Health is committed to engaging with the brightest minds in healthcare
to further innovation. Our choice of Mission Bay and new partnership with Alexandria reaffirms
this commitment, and will enable us to support not just our portfolio companies but also the
broader startup ecosystem. This move will enhance our ability to collaborate with groundbreaking
researchers, clinicians, and corporate partners in our industry.�
Good citizenship continued to drive our efforts in 2013. By protecting the environment and
its natural resources, by supporting leading non-profit organizations in our clusters, by helping
those who faithfully serve our country, and by enabling scientific breakthroughs via our world-class
facilities and services, we work every day to make a difference in our communities.
Alexandria continued to advance its real estate sustainability platform. Including inprocess certifications, 50% of Alexandria�s total RSF is LEED� certified. Beyond LEED certifications,
Alexandria seeks to advance the resource efficiency and environmental ecosystem of its facilities to 

produce the most collaborative, innovative, productive, and sustainable work environments for its
client tenants. Other initiatives have included the implementation of energy optimization projects,
eco-friendly transportation, on-site healthy meal choices, fitness centers, and sustainable gardens.
In addition to supporting the environment, Alexandria continued to make meaningful
contributions to a selection of top-tier non-profit organizations in 2013, including Autism Speaks,
Citizens United for Research in Epilepsy, City of Hope, Friends of Cancer Research, Multiple Myeloma
Research Foundation, Sanford-Burnham Medical Research Institute, the Michael J. Fox Foundation
for Parkinson�s Research, Rockefeller University, and UCSF Medical Center, as well as a number
of military service support groups, including the Congressional Medal of Honor Foundation, Navy
SEAL Foundation, and Wounded Warrior Project.
This year, we also held two groundbreaking Alexandria Summits at the Alexandria Center for
Life Science in New York City. The Alexandria Summit� � Oncology 2013 and the Alexandria Summit
� Medical Research Philanthropy 2013 brought together many of the world�s foremost thought
leaders to tackle critical issues in life science. It was remarkable to assemble diverse pioneers from
across our industry, including Dr. Craig Thompson, president and chief executive officer of Memorial
Sloan-Kettering Cancer Center; Dr. Margaret Hamburg, commissioner of the FDA; Dr. David
Schenkein, chief executive officer of Agios Pharmaceuticals; Dr. Ellen Sigal, chairperson and
founder of Friends of Cancer Research; Bob Hugin, chairman and chief executive officer of Celgene
Corporation; and Dr. Mikael Dolsten, president of worldwide research and development at Pfizer,
for an elucidating, productive, and inspiring open dialogue at our campus. Through the high-impact
and collective voices that are represented at the annual Alexandria Summit, Alexandria helps foster
collaboration and drive forward actionable outcomes that address some of the most critical issues in
the life science industry.
Even as we have witnessed rapidly changing macroeconomic environments, we remain
steadfast in our core fundamentals: our unique business model, our belief in delivering the highestquality facilities and services to our client tenants, our quest for valuable new opportunities,
and our commitment to our society, all of which support our primary objective of creating value for
our shareholders.
As a result of our strategic efforts in 2013, including important improvements in our capital
structure and onboarding of EBITDA from development and redevelopment projects, we now
have the foundation and fuel to grow consistently, much as we did for the decade prior to the 2008
financial crisis.
Importantly, Alexandria has access to a full range of capital choices and will focus on
optionality and flexibility as we continue to utilize the most capital-efficient and effective tools in our
arsenal. We are confident in our ability to deliver solid and stable per share earnings growth and to
continue to increase long-term asset value in 2014 and beyond.
joel s. marcus
Chief Executive Officer,
Chairman of the Board,
and Founder