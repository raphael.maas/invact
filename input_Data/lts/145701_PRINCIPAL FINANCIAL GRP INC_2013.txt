DEAR
FELLOW
SHAREHOLDERS
Ive been fortunate to spend my entire career at the Principal
Financial Group (The Principal). You may wonder why, after
more than 40 years, Im still excited to come to work each day.
There are lots of reasons.
This is why:
Purpose
The need for our products and services is greater than ever
before. That gives us purpose.
 There are currently more than 1 billion people over the
age of 60 worldwide. That number is expected to double
by 20501.
 $2.4 trillion. Thats what U.S. households spent on credit
cards in 20122  more than 10 times the amount employees
contributed to 401(k)s and other private defi ned contribution
retirement plans3. We have to change this.
 According to the Employee Benefi ts Research Institute, less
than 2 percent of U.S. workers identify retirement planning
as their most pressing fi nancial issue, and 43 percent
of workers reported neither they nor their spouse are
currently saving for retirement4. We need to do a better job
of teaching fi nancial literacy.
 By 2025, millennials, also known as Gen Y or those born
in the 80s and 90s, will make up the majority of the
workforce5. We need to help them secure their fi nancial
future.
Pride
The Principal is uniquely positioned to fulfi ll these massive
needs. Were executing the right strategy at the right time in
the right markets. That gives all of us at The Principal pride
in what we do.
People
In fulfi lling those needs, I get to work with some of the most
talented, committed individuals youll ever fi ndin offi ces
around the globe. Our people keep me coming back, day
after day.
So theres the why. But in a competitive and ever-changing
industry, how is The Principal able to fulfi ll our commitments
to customers, advisors and shareholders?
This is how:
Executing our strategy
A sound strategy, strong business fundamentals and effective
execution yield strong fi nancial results. Thats exactly what
The Principal achieved in 2013:
 Total company operating earnings were a record at nearly
$1.1 billion.
 Net income grew 14 percent compared to 2012.
 Return on equity increased to 12.1 percent.
 Assets under management were a record $483 billion as of
Dec. 31, 2013.
 Total company net cash fl ows were $17.4 billion.
Three of our businesses hit a signifi cant milestone in 2013.
Principal International and Principal Funds each crossed the
$100 billion mark in assets under management. And Principal
Global Investors crossed the same mark in third-party assets.
All three were aspirational businesses for us a little more than
a decade ago; today they account for the majority of our assets
under management.
Each day were executing our global investment management
strategy, which has been in place for more than a decade. We
have a unique business model:
 Our diversified set of businesses allows us to weather
challenging economic environments, as we demonstrated
during the financial crisis. Our model enabled us to emerge
from the recent crisis in a position of strength, allowing us to
complete seven strategic acquisitions since 2010.
 What makes our business model especially powerful is the
synergy and collaboration between our businesses. This
synergy results in a valuation thats greater than the sum of its
parts and generates a higher return on equity over time.
As we continue to increase our global presence, were confident
The Principal is in the right geographic markets with the right
partners. Weve purposefully gone where middle class populations
are growing; where the need and appetite for long-term
savings increases each day.
Our largest acquisition to date, Cuprum, closed in 2013 and is
the latest example of successfully executing on our investment
management strategy. Cuprum is the premier pension provider
in Chile, so our expectations for the acquisition were high.
Those expectations have been fulfilled. Cuprum is already a
meaningful contributor to our operating earnings. And the
market holds great long-term potential as Chileans add
voluntary products to their mandatory savings. Combined with
our businesses in Mexico and Brazil, were now the second
largest retirement provider in Latin America6 .
Our successful business ventures and partnerships in Asia have
also delivered impressive results and hold similar potential over
timeas retirement industries develop in markets like China,
Malaysia and India.
Meeting the growing need for financial assistance
Our strategy was built around three core trends that today are
stronger than ever.
1. Aging populations  millions of people around the world are
living longer and need more money in retirement.
2. Financially constrained employers  global competition limits
what employers can spend on their benefit offerings. As a
result, flexibility in packaging benefits and strong workplace
education are keys to success. This plays to our strengths.
3. Financially constrained governments  the strain on
government-provided retirement programs continues to
grow worldwide, leading individuals to look for other ways
to save.
All of this adds up to an era of personal responsibility. And for
individuals and business owners who need to take action, but
lack the confidence and/or knowledge, The Principal can help.
For example:
 In 2013 our employees sat down with more than 115,000
individualsright in their workplacesto come up with a
plan to take action.
 To help individuals better understand their savings needs,
Principal PlanWorks was introduced to help plan sponsors
and financial professionals improve participant retirement
outcomes. This holistic program delivered nearly 14,000
Retirement Readiness Reports to plan sponsors and more than
400,000 Retirement Wellness Reports to participants to help
raise awareness about income replacement needs, encourage
effective plan design and positively impact participant
savings behavior.
 For employers who need comprehensive retirement plans for
all of their employees, The Principal Total Retirement SuiteSM
combines defined contribution, defined benefit, employee
stock ownership plans and nonqualified solutions all with
one provider.
 When companies cant afford to offer full coverage, we
provide voluntary benefitsan increasingly important way
individuals can round out their benefit portfolios.
It might be the era of personal responsibility, but our customers
dont have to go it alone.
Quality leadership team
None of what Ive already discussed would be possible without
a well-run organization. Im proud to say our employees and
leadership team continue to make smart decisions about where
to spend resources and how to nurture talent.
An important part of a well-run public company is effective capital
deployment. Our business mix is now 60 percent fee-based,
giving us more financial flexibility. That allows us to be strategic
and, at the same time, opportunistic in deciding how to use our
capital.
In 2013, we again put our capital to work in multiple ways:
 We deployed $480 million through a mix of common stock
dividends, share repurchase and strategic acquisitions.
 Our 98-cent 2013 common stock dividend was a record and
represented 33 percent of our net income. We continue to
increase our dividend payout ratio, moving toward 40 percent
in three to five years.
 We closed on the Cuprum acquisition and completed our
seventh strategic acquisition since the financial crisis
acquiring a majority stake in Liongate Limited, a
London-based investment boutique that enhances our
capabilities in alternative investments, an area where client
demand continues to grow. Through these acquisitions,
totaling $2.2 billion, its clear were on our front foot when
it comes to opportunistic acquisitions.
We also continue to invest in our employees and in creating a
winning environment in which they can work. As global competition
for talent heats up, these efforts are critical to helping
us recruit and retain high-performing employees in our offices
around the world.
 In 2013 we earned third-party recognition as the Best Place
to Work in Money Management (topping the list in our size
category), one of the 100 Best Companies for Working
Mothers, one of the 100 Best Places to Work in Information
Technology and one of the Top 50 Companies for Executive
Women.
 We began a multi-year, $238 million renovation of our global
headquarters in Des Moines, Iowa, making changes that will
adapt to how employees work todayin terms of technology,
collaboration, flexible work spaces and more. This significant
investment will help us attract and retain employees
where a majority of them work.
This is why. This is how.
So now you know a bit more about why we do what we do at
The Principal every day. We take pride in the meaningful work
were fortunate to dohelping a growing number of people
achieve financial security. And we enjoy the people with whom
we do that work.
You also know a bit more about how weve succeededstrong
execution of a smart strategy, understanding and meeting the
emerging financial needs and running a quality company.
As we look ahead to 2014, its important to note that The Principal
is at the strongest point in our long and rich history. Our
strategy is on target, the momentum of our businesses remains
strong and we have the best people in the industry focused on
meeting customer needs and maximizing shareholder value.
I have no doubt our best days are yet to come.
Before I close, I urge you to vote your shares before the annual
meeting on May 20th. Your voice matters to me, to our Board
and to all who represent The Principal on your behalf.
Larry Zimpleman
Chairman, President and CEO