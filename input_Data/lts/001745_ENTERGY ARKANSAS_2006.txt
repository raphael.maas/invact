The theme of this year's annual report is         from what we really did well. Every              you articulate your point of view when
"having it all." Obviously, that phrase           decision seemed to come down to                  doing so only attracts more of the herd
means different things to different people.       either/or, and in hindsight many of the          back to your side of the pasture?
To many of our customers it's the dream           choices seemed all wrong.                          How do you articulate the value of
of home ownership, stable employment,               We vowed to dig out of that hole and           sustainable growth or being a business
adequate healthcare and seeing their children     never make the same mistakes again  in          with a conscience without sounding like
head off to college for the first time in their   particular, as my mother would say, "having      you've forgotten investors bought the
family's history. Unfortunately, to a great       eyes bigger than your stomach." Through a        stock for one reason  to make money?
many, it seems little more than a dream.          lot of good fortune and the incredible           How do you articulate what 34 years in
  For some "having it all" means the              efforts of 14,000 players, those dark times      the business have taught you about the
freedom to use the air and water or other         are a distant memory. But the temptation         importance of having dedicated employees
natural resources as if they're free for          to get away from what got us here always         who know that the values of the company
the taking or the sole property of this           exists. Particularly when the market is          align with their own? Or the boost it gives
generation. To others, it may mean                always looking for the next big thing.           employee spirit in knowing we have no
simply accumulating more of anything                Compared to many others, our stated            hesitation in telling investors or customers
than you can possibly use.                        strategy sounds just too complicated or          that the safety of our employees always
  When I joined Entergy over eight years          too fluid. It doesn't fit on a bumper sticker.   comes before anything else; something we
ago we were on the brink. Having strained         How do you describe a point-of-view              prove in our actions every day?
our financial resources in order to seek          driven strategy when that point of view            You can count diversity in the
"growth," we were rapidly losing touch            constantly changes as the "herd" moves in        organization in any number of ways. But
with what really mattered, moving away            unison from one idea to the next? How do         how do you compute the real value of




                                                                     P
                                        i$%i&'( )0&10&2%30$ 2$4 56753432&3 i5                   2 0 0 6




cultural, socio-economic or ethnic             in financial, operational and societal         n e          
diversity when it comes to making the          performance, but to separate Entergy from        worked to educate and influence
everyday decisions that affect one of the      the rest. We have demonstrated it is             communities to adopt environmentally
most diverse customer bases in the             possible to deliver consistently superior        smart building standards in order to
country? What is the increased value of        shareholder returns (multiples of                lower future utility bills for our customers
serving communities that know the              the industry average), create a safe and         and reduce environmental emissions.
company is making a difference in the          inclusive workplace, preserve our              n           
lives of the people who can least afford our   environment and contribute to a                  wetland restoration to reduce flooding
product; not because we have to but            productive society. Using our dynamic            risks from future storms in New
because it's simply the right thing to do?     point-of-view driven business model,             Orleans and other communities along
What is the value to the bottom line of        our aspirations will be tomorrow's               the Gulf Coast.
adhering to the principles of sustainable      benchmark for what "having it all"             n        
growth whether the times are good or not?      means in business.                               securitization legislation to preserve
Whether it's expedient or not? Whether it
affects this generation or the next?
  How do you explain to investors the
seeming paradox that "having it all"                        "The principles of sustainable growth have worked for
means giving back and taking no more                    Entergy ... contributed to the achievement of the highest total
than absolutely essential from other                      shareholder return in the industry over these eight years."
stakeholders as you go?
  The fact is, I've never felt overly
compelled to woo those looking to get rich
overnight. Maybe because I've never            Sustainability in Good Times and Bad             affordable rates for our customers and
figured how to do that. The principles of      The 2005 hurricanes put everything we            worked to establish storm reserves to
sustainable growth have worked for             believe and have practiced to the test.          lower future risks for both our
Entergy even though I couldn't predict         2006 could easily have been consumed             customers and our owners.
how or how much eight years ago when           with the near-term recovery of storm-          n        ! " 
I was given the opportunity to become          related costs but we maintained our              for Proposals for long-term and
CEO, nor can I even explain today how it       long-term commitment to the principles           limited/intermediate-term resources
contributed to the achievement of the          of sustainability. For example, even as we       under our utility Resource Supply
highest total shareholder return in the        pushed diligently for storm restoration          Plan in order to drive down prices
industry over these eight years. Entergy's     cost recovery at local, state and federal        for our customers and to maintain
average annual TSR was almost three            levels in 2006:                                  superior reliability.
times the industry average. And we started     n                        n         #      
with no financial headroom or margin for         emergency restoration not only in our          798-megawatt Palisades Nuclear Plant,
error and have had our share of adversity        own service area, but in assisting others.     continuing the impressive profitable
along the way, like the unprecedented            We were honored to receive Edison              growth in our nuclear business.
120,000 square miles of damage from              Electric Institute's Emergency Assistance    n          # 
hurricanes Katrina and Rita in 2005.             Award. Entergy has received either EEI's       reliably than ever, setting new records
  Today, we continue to assert that the          Emergency Assistance Award or                  for total output at both our regulated
principles of sustainable growth are the         Emergency Response Award for nine              and Northeast plants.
only way to run the business. We continue        consecutive years.
to set aspirations to not only be a leader




                                                                        Q
n   #       8                       that lead their field in terms of corporate   Mississippi
  commitment to voluntarily stabilize        sustainability on a global basis. This year   Last June, the Mississippi Public Service
  our CO2 emissions at 20 percent below      we were the only company in the U.S.          Commission approved recovery of $89
  year 2000 levels from 2006 to 2010         electricity sector to be so honored. In the   million in storm restoration costs for
  after successfully completing our first    electricity sector, Entergy ranked best in    Hurricane Katrina with no finding of
  commitment with emission levels that       class for social responsibility, corporate    imprudence. Ninety cents on the dollar
  were 23 percent lower than our target.     governance, climate strategy, corporate       was funded by the $81 million
n s 9@@A                          citizenship/philanthropy, stakeholder         Community Development Block Grant
  return of 38 percent relative to the 20    engagement and occupational health            funding received in October. The balance,
  percent returned by the Philadelphia       and safety. All of which is evidence that     plus $40 million to increase the storm
  Utility Index  reinforcing our belief     the principles of sustainable growth work     reserve and lower future risks for Entergy
  that the real market is not just day        in good times and bad.                      Mississippi and its customers, is being
                                                                                           funded by securitized bonds to be issued
                                                                                           by the state of Mississippi.

              "We made our second five-year commitment to
                                                                                           New Orleans
         voluntarily stabilize our CO2 emissions at 20 percent below
                                                                                           In October, the City Council of New
            year 2000 levels from 2006 to 2010 after successfully
                                                                                           Orleans unanimously approved a
         completing our first commitment with emission levels that
                   were 23 percent lower than our target."                                 settlement agreement that calls for a
                                                                                           phased-in rate increase, and the creation
                                                                                           of a $75 million storm reserve and an
                                                                                           emergency rate relief provision in the
  traders looking at a screen for a quick      Entergy is a stronger company today         event of another Katrina-type disaster.
  buck, but sophisticated investors who      than it was before the hurricanes hit in      The settlement also called for an
  understand long-term value. We             2005. That does not happen when you           independent process for certification of
  restored market confidence by              build on sand, only when you have a solid     storm costs. In early 2007, the City
  demonstrating that Entergy's long-term     foundation of support from employees          Council advisors issued their reports on
  value proposition remains intact. For      and the communities you serve. One that       storm costs incurred through December
  the year 2006, Entergy's as-reported       is built on trust and mutual respect.         2006 with no finding of imprudence.
  earnings were $5.36 per share, up 28                                                     This rate relief outcome, combined with
  percent from $4.19 per share in 2005.      Recovering Storm Costs, Managing Risks        the award by the Louisiana Recovery
  Operational earnings were $4.72 per        We began 2006 with a comprehensive            Authority of a $200 million Community
  share, up seven percent from $4.40 per     total storm cost recovery plan that sought    Development Block Grant, will assist
  share in 2005.                             to minimize the impact on our customers.      Entergy New Orleans with its efforts to
n w #  9@@A                      We successfully pursued recovery from         emerge from bankruptcy.
  employees achieved the safest year in      insurance carriers, federal funds allocated
  the history of the company.                for community development and state           Texas
                                             securitization of remaining costs. All of     In December, the Public Utility
  In 2006, for the fifth consecutive year,   which substantially lowered the remaining     Commission of Texas unanimously
Entergy was named to the Dow Jones           costs to customers.                           approved a constructive settlement that
Sustainability Index  World, an index         Here are the basic elements of our storm    included $353 million of hurricane
that tracks the performance of companies     recovery results in each jurisdiction.        recovery costs, an amount net of




                                                                 4
                                      BCDBEFG HIERIESDTIC SCU VWXVTUTSET BV                      2 0 0 6




                           States-Louisiana and Entergy Louisiana          2006 Results: Generating Clean,
no finding of imprudence. In February          storm restoration cost recovery cases. Both     Affordable, Reliable Power
2007, the Public Utility Commission            the staff and intervenors expressed support     for Our Customers
of Texas voted to approve securitization       for securitization of costs and the creation    The physical aspects of the storm
of the $353 million with an offset of          of a storm reserve. While the cases are still   restoration effort may well have been the
$31.6 million which the commission             pending, we are encouraged that the             greatest challenge this company has ever
determined was the net present value of        resolution will be as constructive as the       faced. The cost and financial recovery
accumulated deferred income tax                results achieved in Mississippi, New            process consumed considerable time and
benefits related to storm costs. A             Orleans and Texas.                              resources as well. Nonetheless, we refused
financing order is expected from the              In 2007, we expect closure for all           to let the storms slow down momentum
commission authorizing issuance of             remaining storm-related regulatory matters,     in other initiatives in our utility and
securitized debt in March 2007.                receipt of additional insurance proceeds,       nuclear businesses.
                                               receipt of funds from the securitization
Louisiana                                      process and the removal of the remaining        Utilities: Executing the
Testimony was filed in early 2007 by the       negative credit agency outlook triggered as     Resource Supply Plan
Louisiana Public Service Commission staff      a result of the hurricanes.                     In 2006, we continued to execute our
and intervenors in the Entergy Gulf                                                            Resource Supply Plan to meet our
                                                                                                                                          continued on page 7




Climate Change
      D EFINING I SSUE         O UR T IME
 HE                       OF


At Entergy, we believe that we are at a defining point in time as a society  a time that
future generations will look back on with either admiration or despair. Today, our global
society is emitting greenhouse gases at ever-escalating rates. While scientists,
politicians and academics debate the extent of the impact of greenhouse gas emissions,
we as a society are running out of time.                                                                              C L I MATE C H A N G E :

                                                                                                                      The Defining Issue
                                                                                                                        of Our Time




When we look up at the sky at night we see the light from stars that was emitted
millions of years ago, only now completing its journey to earth. Greenhouse gas can be
viewed the same way. Once emitted into the atmosphere, the outcome is inevitable.
CO 2 stays in the atmosphere for approximately 100 years. Every day that goes by our
future becomes more certain. There is no ability to turn back.


At Entergy, we believe now is the time to act. We know the issue is complex and the
necessary actions difficult. To further both discussion and action, we have invited
experts on climate change to share their thoughts in the centerpiece to this year's
annual report. We invite you to form your own opinion and support action on climate
change  the defining issue of our time.




                                                                        5
               t uvxy yy

                                     `abcd fghg pqrd


      n 2006, Robert v.d. Luft retired as the chairman of our Board of Directors after
     eight years of service in that position. Under his leadership, Entergy's market
     capitalization grew from less than $7 billion at the time Bob became chairman to
more than $19 billion at the end of 2006.
  A retired senior vice president of DuPont who also served as chairman of the board
of DuPont Dow Elastomers, president of DuPont Europe and chairman of DuPont
International, Bob was elected chairman of Entergy Corporation in mid-1998.
Shortly after Bob became chairman, the Board of Directors launched a turnaround
strategy designed to increase shareholder value by narrowing the company's focus on
fewer businesses that were aligned with Entergy's core competencies, by divesting
underperforming businesses and by restoring the company's overall financial health.
  The strategy has delivered strong results for owners and earned recognition from
various industry organizations. Entergy was recognized by Edison Electric Institute for
having the top total shareholder return for the five-year periods ending September 30,
2003 and September 30, 2004. The company twice received EEI's highest honor, the
Edison Award, in 2002 and 2005 and was named 2002 Power Company of the Year and
2003 Global Energy Company of the Year by Platts/Business Week Global Energy Awards.
  With his focus, discipline, attention to the details that matter most to our
customers and owners, and generosity of spirit in dealing with employees, Bob has
had a significant impact on our organization  an impact that has forever changed the
way we do business and the way we see the world around us.




                  Y
                                       klmkopq rsotsoumvsl ulw xyzxvwvuov kx                     2 0 0 6




 def ghi j

#   #                          Nuclear: Strong Earnings                        acquisitions, management service contracts
and modernize our generation fleet and         and Opportunity                                 and other types of agreements. We have
create opportunities to lower our customers'   We continue to focus on safety and              also continued to preserve our option to
rates. Our portfolio transformation            security first in all our nuclear operations.   pursue new nuclear development through
framework assures consistency with our         While our operational excellence is second      the construction and operating license
market-based point of view in order to         to none and our aspiration is to continue       initiatives at Grand Gulf Nuclear Station
produce the long-term least cost to            our growth in this business, we have            and River Bend Station.
customers after considering and pricing        remained disciplined investors.
all risks. We are actively pursuing a number     In 2006, we seized an opportunity that        The Time to Aspire for More
of attractive opportunities to meet our        aligned with how we create value for            By any measure, 2006 was a defining year
customers' demand requirements                 sellers of plants and for our owners. We        for Entergy. Thanks to a lot of hard work
conditioned on the receipt of regulatory       announced our intent to purchase the            by our employees, the support of our
approval for rate recovery.                    798-megawatt Palisades Nuclear Plant            customers, communities and owners and
  We also continued to pursue constructive     near South Haven, Michigan, for a net           the collaborative efforts of too many to
resolution of other outstanding regulatory     amount of $350 million, or $303 per KW.         mention at the federal, state and local
issues in 2006. Proving the value of
creativity and persistence, our Independent
Coordinator of Transmission was approved,
installed and began performing all of its                  "Most importantly, in 2006, our employees achieved the
functions, including tariff administration,                       safest year in the history of the company."
available flowgate capacity calculation,
transmission planning and operation of the
Entergy OASIS. Crossing these milestones
were significant events for Entergy, as we     The Palisades plant is of a design and          jurisdiction levels, we recovered from two
have worked for nearly a decade to align       vintage that is problematic for most            unprecedented storms that devastated our
our transmission operations with changing      operators. Entergy's experience with this       service territories and much of our
federal policy, while meeting local            technology and its technical challenges         infrastructure. Then we moved beyond
regulators' and policymakers' needs. In a      was a clear advantage in constructing the       survival and recovery. We took steps to
testament to the hard work of all parties      successful bid. We are confident in our         manage our regulatory and operating risks,
involved, on November 17, 2006, the            operating plan to maintain the reliability      reduce our impact on the environment
transition was completed seamlessly.           and safety of the plant as it meets technical   and ensure our customers have access to
  We still have work ahead to resolve          challenges in the years ahead. When this        reliable, affordable power for years to
outstanding issues associated with the         transaction closes  expected in the second     come. Our shareholder return was top-
System Agreement litigation and Entergy        quarter of 2007  we will own and operate       quartile at 38 percent for the year. While
Arkansas' decision to provide its eight-year   11 nuclear plants in the South, Northeast       we are pleased to report progress, we are
notice to exit from this agreement. We also    and Midwest. We also continue to operate        far from done. We recognize that after
expect Entergy Gulf States to separate into    the Cooper Nuclear Station under a service      overcoming such adversity there is a great
Louisiana and Texas operating companies        contract with the Nebraska Public Power         temptation to become satisfied and
by the end of 2007, allowing the separate      District. With our positive point of view       complacent. Instead, now is the time to
companies to develop plans that are            relative to clean, affordable nuclear           aspire for more.
consistent with local public policy,           power generation, we continue to seek
including potentially the pursuit of           opportunities to further expand our
retail customer choice in Texas.               nuclear business portfolio through




                                                                        7
{|}~                                          n  }                                          premature to consider dividend action at
The legacy of 2005 and 2006 was not that      quartile total shareholder returns.           this time. I assure you, it is at the top of
we proved we had achieved our aspirations     We aspire to grow earnings per share by       our list when more certainty and clarity
of operational excellence or financial        $1.00 each year through 2010, earn            is achieved on other initiatives.
resilience. What we proved was as an          returns at or above our risk-adjusted
organization we are capable of more than      cost of capital and maintain solid            n  }   }
we might have ever imagined. The              investment grade credit with flexibility      reliable and affordable power
adversity in the last two years was beyond    to manage risk and act on opportunities.      in our utility business.
anything the industry has experienced.        We aspire to return cash to our owners        We aspire to safe, efficient and effective
The organization's response was equally       through common stock repurchases of           operations in all areas  generation,
unprecedented. As 2006 drew to a close,       up to $500 million each year and/or           transmission and distribution. Our
we took a hard look at our aspirations. We    dividends targeting a 60 percent payout       service area has unique operational and
assessed our current position, evaluated      ratio over time.                              economic challenges. We strive to
                                                                                            address these challenges while
                                                                                            continually improving customer
                                                                                            satisfaction, increasing productivity
                                                                                            and decreasing costs.
     "Entergy was named to the Dow Jones Sustainability Index  World,
       the only company in the U.S. electricity sector to be so honored."
                                                                                            n  }  } } 
                                                                                            and vital nuclear resources in an
                                                                                            environment that is both growing
                                                                                            and carbon-constrained.
multiple scenarios using our dynamic            At the end of 2006, we ended the            Safety and security come first in our
points of view on market conditions,          suspension of and completed our 2004-         nuclear operations. After that, we
our stated strategies, our change in risk     2006 $1.5 billion common stock                aspire to uninterrupted operations
profile and our experiences over the last     repurchase program. In January 2007,          and continuous improvement in
few years, and refined our aspirations.       we announced a new $1.5 billion               productivity. We are committed to
  Underlying our refined aspirations is       common stock repurchase program to            aggressively pursue opportunities to
our timeless belief in the principles of      be implemented over the next two years.       expand our nuclear fleet more fully,
sustainable growth, the unlimited human       These actions were consistent with our        utilizing our talented and deeply
potential and basic human goodness.           commitment to fairly compensate               skilled workforce.
The long-term success of our company          those who provided capital for storm
depends on our ability to ensure our          restoration and those who made that           n  }  }   
employees' safety, meet our customers'        possible by continuing to believe in our      poverty for our customers and
expectations, deliver superior returns to     long-term value proposition and not           contribute to a society that is
our owners, conserve and protect our          selling into a market when we were            healthy, educated and productive.
environment and contribute to a healthy,      issuing securities.                           Our societal aspirations encompass our
educated and productive society. It takes       Regarding the dividend, no one needs        communities, our employees and our
steady progress along every dimension to      to remind us that the current level is well   environment. No one should have to
generate lasting growth and value. With       below our 60 percent target and also well     choose between food and heat. We
that in mind, our five-year aspirations for   below the average relative to our peers.      strive to make our service as affordable
2006 through 2010 are outlined below:         Your Board of Directors continues to be       as possible. But we do not accept that as
                                              disappointed that we believe it is still      the end of our responsibility. We strive




                                                                 8
                                                                  2 0 0 6




  to contribute to the eradication of           focused, financially healthy organization       giving us the ability to adapt our strategies
  poverty in our utility service territories.   driven to produce tangible benefits for         and positions quickly as market conditions
  We aspire to eliminate lost-time              customers and owners. Safety is Bob's           change. Finally, our commitment to
  accidents in every area of our business       passion. He has engrained that passion in       sustainability and our focus on total
  and create a workforce as diverse as the      the entire Entergy organization. Bob has        shareholder return set a clear framework
  communities in which we operate. We           been an inspirational leader and mentor to      for all our decisions and actions. Entergy
  strive to be the cleanest power generator     me personally. We know Bob's contribution       as an organization remains committed,
  in America  one that voluntarily             to the good of mankind, while already           focused and ready to adapt to whatever
  adheres to greenhouse gas emission            great, is far from finished. We wish him        the future may bring.
  levels and conserves natural resources in     all the best in retirement.                       As a leadership team, we are always
  as many ways as possible.                       With Bob Luft's retirement from the           mindful of our fiduciary obligations to
                                                chairman role, the Board of Directors           our owners and our moral responsibilities
  While all aspirations involve some            elected Gary W. Edwards as the Presiding        to our customers and communities. We
stretch, our refined aspirations are firmly     Director. Gary is widely respected for his      believe we have the power to not only
grounded in the reality of our current          character and unwavering commitment to          financially succeed in the years ahead,
business as well as the future scenarios we     the highest standards of conduct. With          but to help assure the sustainability of
believe most likely based on our current        Gary's leadership, the Board will continue to   the world around us. At Entergy,
point of view. We put these aspirations         operate at these same standards of corporate    "having it all" means no less.
before you to share our vision for what we      governance and provide meaningful and
believe is possible and to challenge            expert insights to our management team.
ourselves to continuously reach beyond
our current grasp.                              The Power to Sustain
                                                We begin 2007 with great momentum and
A Lifetime of Achievement                       anticipation. Our diverse, dedicated family     J. Wayne Leonard
In 2006, Robert v.d. Luft retired as the        of employees continues to deliver truly         Chairman and Chief Executive Officer
chairman of our Board of Directors after        outstanding performance, whatever the
eight years of service in that position.        obstacles. Our point-of-view driven
Under Bob's guidance, Entergy became a          business model continues to serve us well,








                                                                         
