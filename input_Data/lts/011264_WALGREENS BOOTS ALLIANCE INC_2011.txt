Letter to Shareholders

Fiscal 2011 was a year of strong performance for Walgreens. We achieved
our 37th consecutive year of record sales despite a challenging economy.
Our focus on delivering exceptional value and service, fueled by our culture
of operational excellence and innovation, produced record profits,
our strongest increase in net income in a decade and our largest
growth in earnings per share in more than 15 years.
We made substantial progress in the transformation of the Company,
putting Walgreens in a strong position for future success by executing 
and advancing  our bold strategy for near- and long-term growth.
Three years ago, we launched our Plan to Win  one of the most
important strategic and operational transformations in our 110-year history.
The plan focused on leveraging our center of gravity  the best store
network in America; enhancing the customer experience; and achieving
major cost reduction and productivity gain. Our goals were to become
Americas most trusted and convenient provider of consumer goods
and services and pharmacy, health and wellness solutions, and to
return the Company to strong double-digit growth in earnings per
share, increasing return on invested capital and top-tier shareholder
return just as soon as we possibly could.
In fiscal 2011, as we completed key initiatives and progressed in our
Plan to Win, we also seized the opportunity to crystallize and advance
our vision and strategies, and set our sights even higher  to become
My Walgreens for everyone in America, the first choice for health
and daily living across the nation, and a central part of peoples lives
and the communities where they live and work.
Our vision of My Walgreens is more than a drugstore  we are becoming
a retail health and daily living destination, providing convenient,
multi-channel access to goods and services with best-in-class customer
experience. Our pharmacies  on the front lines of health care in America
 serve as a centerpiece in improving patients overall health, and
improving access to care and lowering costs through an expanded scope
of community-based health and wellness solutions.
The core, fundamental strengths of our Company, developed over more
than a century  the best, most convenient store network in America,
our trusted and iconic brand, and our strong balance sheet and financial
flexibility  serve as the bedrock of our ongoing transformation.
Strong performance through innovation and operating excellence
As we advanced our strategic vision, the Company recorded strong
results across a broad spectrum of measures in fiscal 2011.
Disciplined focus on our core business strategy helped set new records
in fiscal 2011  sales of $72.2 billion, gross profit of $20.5 billion and
earnings per diluted share of $2.94, which included a $0.30 per diluted
share after-tax gain on the sale of our pharmacy benefit management
business. In addition, our cash flow from operations was $3.6 billion.
All of these contributed to a dividend increase of 28.6 percent in
July and cash returned to shareholders through dividends and share
repurchases of $2.4 billion in fiscal 2011. Our capital allocation strategy
was reflected in the purchase of drugstore.com and our decision to sell
our pharmacy benefit management business.
Our Company continued to achieve important milestones in our transformation
strategy, building on our historic strengths and sharpening
our focus on our core business to deliver even better performance in
our more than 7,700 stores:
 As we integrated the Duane Reade drugstore chain in New York
City, we began building many of its unique urban retailing and
store design concepts into Walgreens stores across the country.
In turn, we powered Duane Reades pharmacy operations with the
best of Walgreens systems and expertise.
 We have converted more than 5,000 stores and opened more than
500 new stores with our Customer Centric Retailing (CCR) initiative
launched in 2009. With this program, which enhances the stores
look and feel and the shoppers experience, we have reported higher
customer satisfaction and increased sales. Openings and conversions
to the new format reached a peak of 140 stores per week, totaling more
than 3,700 stores last year alone.
 We greatly expanded our answer to food deserts  underserved
communities lacking ready access to fresh, healthy foods  with a plan
to offer convenient access to an expanded selection of fruits, vegetables
and other staples at 1,000 Walgreens stores across the country, building
on our successful 12-store pilot in Chicago.

 As we refresh our stores, we continue to broaden the range of health
and daily living products we offer. In fiscal 2011, we expanded our
highly successful Walgreens private brand selections for the valueminded
consumer with the launch of a new brand name, Nice!.
With Nice! and Duane Reades Good & Delish, we now offer more
than 400 high-quality grocery and household private-brand products.
Beer and wine selections  including Walgreens Big Flats 1901 
have now been added to nearly 5,300 stores.
 In pharmacy, we continue to make great progress on our initiative to
advance community pharmacy  enhancing the patients experience,
expanding our scope of services, and improving productivity to lower
cost. Patients are beginning to see something new at Walgreens;
in our new pilot store design, we are bringing our pharmacists out
into the store to promote more face-to-face conversations and personal
interactions with patients.
 Through our pharmacies and health centers, we advanced our
leadership in providing flu shots and other immunizations, delivering
6.4 million flu shots last year as we continued to serve as the numberone
provider of flu shots other than the federal government. Teaming
with the U.S. Department of Health and Human Services, Walgreens
donated more than $10 million worth of flu shots to uninsured and
underserved families.
 Building on our core business, we advanced our multi-channel strategy
with the acquisition of a leading e-commerce site, drugstore.com.
We gained not only the drugstore.com and Beauty.com websites
and their customer service and distribution centers, but also access
to more than 3 million online customers and 60,000 drug, health,
beauty and skincare products  expanding our already strong online
offering. As well, we introduced our new Web Pickup services at many
Chicago-area and San Jose, California, locations, enabling customers to
shop online and pick up their orders at a store in as little as an hour.
 Today, no retailer in America is better positioned than Walgreens
to bring bricks and clicks together, combining our thousands of
convenient locations in communities across the country with the
convenience of outstanding online access.
 As we continue to focus on  and invest in  our core business,
we completed the sale of our pharmacy benefit management business
to Catalyst Health Solutions, Inc., for $525 million, subject to
certain adjustments.
 Importantly, we continue to take steps to control costs and increase
productivity. We achieved our three-year goal to deliver $1 billion in
annual pre-tax savings through our Rewiring for Growth initiative,
and advanced this strategy by launching Fuel Well  building continuous
improvement and innovative cost-reduction and productivity gain into
our daily business operations.
The evolving consumer and health care system
Through our ongoing transformation, Walgreens is ready to meet
the evolving needs of consumers and patients in todays challenging
economy and changing health care system.
Three years after the Great Recession began, research shows that
value-minded consumers are seeking more than good quality at low
prices  they also want connection and community, kindness and
empathy, and a positive shopping experience.
Patients are seeking value, quality and personal service as the health care
system continues to change through both legislation and market dynamics.
The Patient Protection and Affordable Care Act of 2010 is expected to
bring 32 million more Americans into the health care system in 2014,
and the aging population faces a higher incidence of chronic and
complex conditions. Patients and payers are seeking new solutions to
address the growing need to provide convenient access to quality
health care and, at the same time, rein in skyrocketing health care costs.
Pharmacy is a key part of the solution  and a leading provider of
accessible, affordable health care in communities across the nation.
Our pharmacies help reduce costs by driving greater penetration of
generic drugs  roughly 74 percent last year  resulting in significant
savings on each prescription. For chronic conditions, we also promote
90-day refills at our retail pharmacies, which provide on average 6 to 8
percent savings compared with three 30-day prescriptions.
Walgreens also provides a broad range of health care and clinical services
as extensions of our traditional pharmacy services:
 Our more than 350 in-store Take Care health clinics across the nation,
with a plan to expand this number over the next few years, offer a wide
range of convenient, affordable health care services as an alternative
to costlier primary care physicians and emergency room care.
 In addition to our extensive immunizations program, Walgreens
pharmacies provide medication adherence services, counseling and
other assistance that help lower medical costs by improving outcomes.
Our range of health testing services, including blood glucose,
A1C hemoglobin levels and free blood pressure tests, also serve
to help lower costs through prevention.

 Walgreens is the nations leading provider of home infusion and a
major provider of specialty pharmacy services, providing cost-effective
solutions for patients who need infused medication, including chemotherapy,
respiratory services and oxygen at home or in other ambulatory settings.
 As well, Walgreens is the nations leading operator of hospital outpatient
pharmacies, serving 137 health systems nationwide. We are also the
largest and most comprehensive manager of worksite health and
wellness centers on employer campuses, with more than 350 locations
across the country.
As we expand affordable health care choices, we also seek fair, acceptable
and predictable reimbursements for the value we provide. Last year,
Walgreens sought to renew our contract with pharmacy benefit manager
Express Scripts to continue serving its network in 2012 and beyond. We
offered a number of cost-saving proposals, including controlling prescription
reimbursements. However, negotiations have been unsuccessful as
Express Scripts set forth terms, including reimbursement rates that were
below the industry average cost to provide a prescription, that were not in
the best interests of our Company, our customers, our employees or our
shareholders. Walgreens began informing health plans and patients that we
plan not to be part of the Express Scripts network after December 31, 2011,
when our contract with Express Scripts expires. We are moving forward
with relationships for partners that value the choice, cost-effectiveness,
convenience and service we provide and want to ensure their patients
continue to have access to Walgreens.
My Walgreens for everyone in America
When we hear consumers using the phrase My Walgreens, we are
delighted to be part of their lives and their communities, and to know
they have a deep personal connection with our people and our stores.
Becoming My Walgreens in every community across America is a lofty
goal. But with our expansive network of stores, expanding health care
services and commitment to creating a satisfying customer experience,
we can own the strategic territory of well  where health and
happiness come together  in every community we serve.
With this aspiration, last year we took a fresh look at our Plan to Win
in light of our progress, the initiatives we completed, and the new strategic
challenges and opportunities before us. We refined, sharpened and
crystallized our vision into five strategies, and took important steps
to execute each strategy.
Transform our traditional drugstore to a retail health and daily living destination
As we complete the refresh of our stores, Walgreens is also moving
forward to completely redefine  even revolutionize  the drugstore
experience, setting us apart in our industry, and establishing
My Walgreens as a destination for consumers to satisfy a broad
range of health and daily living needs.
We are bringing together all our transformational strategies developed
over the past three years to pilot exciting new concept stores for
Walgreens, combining enhancements in our pharmacies and health
clinics, our improved CCR merchandising, our learning from Duane
Reade, and new e-commerce techniques and technologies from our
multi-channel strategy.
Through calendar year-end 2011, we will have converted or opened
20 of these new concept stores in the Chicago area, as well as our
22,000 square-foot flagship Duane Reade store at 40 Wall Street
in New York City, and we have begun expanding the pilot to
Indianapolis, Indiana. Customers, employees and community leaders
visiting these stores have reacted with delight. In turn, we are applying
what we learn from these new concept stores to enhance Walgreens
health and daily living offerings in our stores across the country.
Advance community pharmacy
Building on our industry leadership in pharmacy-based health services,
we plan to expand our reach beyond traditional community pharmacy,
coordinate and integrate with national, regional and local health systems,
and play an expanded role in delivering affordable health care.
With regard to health systems and physician groups, we are moving
quickly to establish closer relationships with premier partners in markets
nationwide. Last year, we joined with Northwestern Memorial Physicians
Group of Chicago to undertake a new coordinated health care program
to improve patient outcomes. In addition, our Take Care Clinics
joined with Memorial Health of Jacksonville, Florida, and with Ochsner
Health Systems of New Orleans to increase opportunities for clinical
collaboration and improve patient access to high-quality, convenient,
affordable health care options.
We entered into an agreement with Johns Hopkins Medicine to promote
collaboration on population-based research and to jointly review and
develop protocols to improve outcomes for patients with chronic diseases.
Together, we will explore the development of new models for improving
care for individuals. This will include the creation of new educational and
training programs for Walgreens 75,000 health care service providers.
Expanding our preventive health care services, Walgreens last year added
testing for total cholesterol and HDL (high-density lipoprotein), blood
glucose and A1C at more than 1,600 pharmacies in 33 states. Each test
includes a free blood pressure reading and personal consultation with a
Walgreens pharmacist.
In fiscal 2011, we began working with the National Foundation for
Infectious Diseases to educate the public and health care professionals
regarding flu prevention resources, and joined with Families Fighting
Flu, a non-profit organization that comprises families and health care
practitioners, to heighten flu awareness and encourage vaccinations
for children and families.
Illustrating our expanding role in health care, the Care Continuum
Alliance recently welcomed Walgreens among its newest members, noting
our work to promote healthful lifestyles and empower consumers to
reduce risk aligns with [the Alliances] mission to improve care quality
and value, particularly for people with or at risk of chronic disease. Our
participation will help Walgreens learn more from other members regarding
strategies to improve health care outcomes and reduce avoidable care
costs for our customers and patients.
Outstanding customer experience through enhanced employee engagement
Becoming My Walgreens for everyone in America means providing the
consumer more than the products and services, choice, convenience,
savings and multi-channel access that they demand. We must also offer
an experience that gives the consumer a sense of delight and connection
with Walgreens and our people.
For most of our history, Walgreens core strength has been our convenient
locations on the best corners in America. Today, we are building
on the strength of our locations to make each Walgreens a place that
consumers prefer because we provide an outstanding  even memorable
 shopping experience.
That starts with our people, who work hard every day to provide best-in-class
customer service. Our customer and patient satisfaction continues to
improve as we work to create a sustainable competitive advantage by
delivering memorable customer interactions. To this end, we began
our Well Cared For initiative to strengthen employee engagement
by setting new standards, providing training and developing leadership
skills. As well, in 2012 Walgreens plans to introduce a nationwide
customer-loyalty program truly differentiated from our competitors.
Last May, the courageous, caring response by Walgreens team members
to the devastating tornados that tore through Joplin, Missouri, illustrated
their incredible spirit of customer and community service. Of our three
stores in Joplin, one was destroyed, one sustained broken windows and
roof damage, and the third, which was not affected, we converted to a
24-hour location. A Red Cross triage unit to help residents was set up
under the canopy of our damaged building, and employees used store
stock to provide the Red Cross nurses with needed resources, such as
antiseptic, glucose machines, test strips, blood pressure machines,
crutches, batteries and flashlights. This location became one of the
hub triage centers in the city.

As team members across the country embrace the spirit that Walgreens
employees demonstrated in Joplin, outstanding and caring customer
experience becomes synonymous with the Walgreens brand.
My Walgreens for everyone in America means establishing an
emotional connection for our patients and customers with our people,
our stores, our brand, who we are as a Company, and the role we
play in communities as a cornerstone of health and daily living.
New channels and markets
Walgreens continues to invest heavily in e-commerce and mobile technology
to leverage our brick-and-mortar locations with convenient,
sophisticated online options. Through our expanding multi-channel
strategy, customers will be able to access what they want  a broader
selection of products; where they want it  whether delivered to their
homes or available for pickup at the store; and when they want it 
two days, next day or next hour.
Multi-channel shoppers are estimated to be three times more valuable
than a single-channel shopper. And, when they choose to shop at the
store, they want a great experience. So we continue to invest in our
bricks and expand our choice of clicks to provide our shoppers the
best of both worlds.
In fiscal 2011, as we put new smartphone enhancements in the hands of
Walgreens customers nationwide, the response has been phenomenal.
Walgreens newest mobile phone application, which includes Refill by
Scan, continues to grow in popularity, with nearly 3 million downloads
last year. By broadening our social media presence, surpassing
1 million Facebook fans and integrating our store network last year
through Facebook Places and Foursquare, we continue to strengthen
our personal connection with consumers.
Reinvent our cost structure
A key element of our transformational strategy is cost reduction and
productivity gain, and our Rewiring for Growth initiative launched in
2008 delivered more than $1 billion in annual pre-tax cost savings.
Completing this initiative, we are now advancing this strategy by
embedding continuous process improvement and innovation into
our corporate DNA, constantly looking for new ideas to improve
processes and reduce cost structures.
Summary
Walgreens strong performance and substantial progress in fiscal 2011
puts the Company in an excellent position to execute the next phase
in our transformation, play a bigger role in peoples lives and in their
communities, and create more value for consumers, patients, payers
and Walgreen shareholders. Our sharp focus and relentless commitment
to executing with discipline are now wired into our daily work
across the Company.
In fiscal 2012, we will continue our sharp focus on revenue growth,
cost savings and capital allocation, and enhancing the relevance of our
products and services to customers, patients, payers and the communities
we serve. In doing so, we remain committed to our long-term goal of
achieving double-digit growth in earnings per share, increasing return
on invested capital and top-tier shareholder return.
The Company continues to benefit from our outstanding senior leadership
team that blends a broad range of internal and external experience
and expertise, and the strength and dedication of our Board of Directors.
Last year, we announced the appointment of Joseph Magnacca, from
Duane Reade, as our new president of Daily Living Products and
Solutions; Thomas J. Sabatino, Jr., as executive vice president, general
counsel and corporate secretary, succeeding Dana Green, who retired
after 37 years of outstanding leadership and service with the Company;
Jeffrey Berkowitz as senior vice president of pharmaceutical development
and market access; and Graham W. Atkinson as senior vice president
and chief customer experience officer.
Walgreens continued strong performance and progress is a tribute to our
superb team of 247,000 employees and their dedicated service to our
customers and our Company. We greatly appreciate and applaud their hard
work, commitment and support as we seize new opportunities. We most
sincerely thank and appreciate our customers, patients and payers, our
suppliers and communities across America for making Walgreens an
important part of their lives. And we offer our deepest gratitude to our
shareholders for your trust and confidence in our Company, making
possible our vision of My Walgreens for everyone in America.
Sincerely,
Alan G. McNally Gregory D. Wasson
Chairman of the Board President and Chief Executive Officer
November 16, 2011