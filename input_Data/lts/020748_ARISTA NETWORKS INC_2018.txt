Dear Arista Networks Stockholders:
I am pleased to report that Arista Networks demonstrated another year of strong execution in 2018,
with continued momentum from our cloud customers and expanded business in the enterprise vertical. We
are extremely proud of the strategic role that Arista is earning, with a broad set of customers deploying
transformative cloud networking.
2018 Highlights:
� Revenue for our fiscal year 2018 was $2.15 billion representing an increase of 30.7% from the
prior year. We now serve over 5,600 customers, having shipped more than twenty million cloud
networking ports worldwide, leveraging EOS our advanced network operating system.
� Arista introduced Cognitive Cloud Networking for the campus encompassing a new network
architecture designed to address transitional changes as the enterprise moves to an IoT ready
campus.
� Arista acquired WiFi pioneer Mojo Networks for cloud networking expansion, entering the
wireless LAN market with a portfolio of WiFi edge products.
� Arista introduced the next generation 400G version of our switch routing platforms with two new
400G fixed systems, delivering increased performance for the growth of applications such as AI
(artificial intelligence), machine learning, and serverless computing.
� Arista acquired Metamako, a leader in low-latency, FPGA-enabled network solutions. This
acquisition plays a key role in the delivery of next generation platforms for low-latency
applications.
� The Forrester WaveTM Hardware Platforms for SDN, Q1 2018, recognized Arista as a leader in
the current offering and strategy categories.
� Arista maintained its leadership position in the Gartner July 2018 Magic Quadrant for Data
Center Networking for the fourth consecutive year.
Looking ahead, we see opportunities in delivering new technologies across our cloud networking and
cognitive campus platforms in support of a broader customer base.
I would like to thank our stockholders, customers, partners and our employees for their support.
Jayshree Ullal
Chief Executive Officer, President and Director
Arista Networks, Inc.
April 17, 2019