To Our Shareholders:
In a year that challenged our company, our industry
and our world, the people of Morgan Stanley displayed
great discipline and resolve. We believe our company
has emerged even stronger and better prepared for
the future. The long-term growth trends that shape
our strategy are intact. And more than ever, we are
focused on the working relationships with clients
that will drive our future growth.

When we think back on 2001, we are filled with
deep sorrow and outrage over the events of
September 11. Who among us will ever forget
the shock and horror of that day? For the
Morgan Stanley family, the attacks hit us right
at home  3,700 of our people worked in
the World Trade Center. Sadly, seven of them
and six contract service professionals did not
make it out that day.

But we also take pride in the way our company responded
in the immediate aftermath of the attacks and the weeks
that followed. Through resolve, discipline and some individual
acts of heroism, nearly all our employees evacuated
the South Tower immediately after the North Tower was hit,
executing planned and frequently practiced safety procedures.
Many of our people immediately went to backup sites
to begin to take care of customers and to protect the firm.
We believe it was a defining moment for us  a summation
of what we stand for and how we respond when tested by
extreme circumstances. We believe the response of our
people should give shareholders great confidence.
Few, if any, events will ever have the impact of September 11,
but the year 2001 also tested our people and our businesses
in other ways. For most of the year, the world economy
was gripped in a recession that still continues as we write
this letter. As a result, our business volume was down from
the record levels of late 1999 and early 2000, and the
steady growth in our profitability was interrupted.
We have responded to this challenge by focusing even
more intently on client relationships. Despite the difficult
environment, we continued to achieve substantial returns
for shareholders.

Financial Results
> Our net income for fiscal year 2001 was $3.6 billion, a
decline of 34 percent from last years record $5.5 billion.
Earnings per share were $3.19, down 33 percent from
$4.73 a year ago.
> Return on equity (ROE) for the year was 19 percent, which
is very strong considering the difficult environment in the
securities industry. We believe the two principal reasons
for this strong ROE are our discipline in controlling
expenses and the breadth and diversity of our businesses,
several of which had excellent results despite the
generally difficult environment.
> Net income in our securities segment was down 42 percent
from $4 billion in 2000 to $2.4 billion this past year.
Fixed income, however, was the exception within the
securities segment, with a record year in net revenues.
Our equities business also continued to deliver high
returns, even though volume was down from the peaks of
the first half of 2000. Both investment banking and the
individual investor group suffered significant decreases
in net revenues.
> Our investment management business was down 19 percent
in net income, but our investor base remained solid,
and the business continued to produce high returns.
> Credit services net income was roughly equal to last year
at $702 million, resulting in 19 percent ROE.
> We were pleased that our market share gains continued
in the equity and fixed income trading businesses, with
particularly strong gains in Europe.
> In equities, we achieved significant market share gains in
secondary trading for institutional clients in the U.S. and in
Europe, especially in listed stocks, over-the-counter (OTC)
stocks, convertible securities and listed options. Fixed
income showed similar gains in investment grade, assetbacked,
government securities and credit derivatives.
> In investment banking, we held our global position as #2
in completed mergers and acquisitions (M&A), #2 in initial
public offerings (IPO) and #3 in equity products. During
the year, we reorganized the division, repositioning our
resources to allow for more client coverage while reducing
headcount by more than 10 percent. Despite
increased competitive pressure from commercial
banks, we were able to maintain our market position in
investment banking. We are intent on improving that
leadership position over the next several years as the
impact of our reoriented client coverage strategy begins
to take effect.

> Our market share in retail securities increased in terms
of number of financial advisors, but our percentage of
industry-wide revenues decreased.
> In 2001, Discover Card again increased market share
measured by percentage of total receivables.
In summary, we increased market shares in our trading
businesses and maintained market shares in most of our
other major businesses.
CHALLENGES OF 2001
The year was not an easy one in which to achieve a 19 percent
ROE. Most markets were down significantly from 2000.
> M&A activity declined by 51 percent.
> IPOs globally were down 57 percent.
> Retail securities activity in the U.S. was off at least
25 percent.
> Equity prices in the U.S. were down 13 percent for S&P
500 and 21 percent for the NASDAQ Composite.
European markets suffered from similar declines.
> The U.S. officially slipped into a recession, leading the
Federal Reserve to reduce interest rates 11 times during
the year. In Europe, gross domestic product (GDP) annual
growth fell from 3.3 percent to 1.6 percent.
> Technology and telecom stocks and bonds were particularly
hard hit with stocks off over 50 percent, sharp
declines in most bonds and many bankruptcies.
The impact of a difficult economic environment in 2001
reaffirmed the wisdom of focusing on our clients as well
as our strategy of diversification by both products and
markets. In general, certain businesses in which we carried
proprietary positions, such as high yield and private
equity, experienced write-downs. Through sales and writedowns,
however, we significantly reduced the size of both
our high-yield and private equity portfolios in 2001. Our
restructured high-yield business focuses on servicing
client flows, and private equity is concentrated on the fund
business where we invest alongside our clients in vehicles
that we manage. While it represents a small part of
our business currently, we also continue to invest in our
commercial lending business. Disciplined underwriting,
advantageous collateral arrangements, increased diversification
and an increased investment in distribution
infrastructure will help us to better manage these portfolios
in the future.

FOCUS ON CLIENTS
A major accomplishment in 2001 was the continued move
closer to true client centricity in every business. Each business
has restructured to focus on and better serve our
clients. It is no coincidence that those businesses that
enjoyed the biggest market share gains  institutional
equities, for example  are the ones that have made the
most progress in reorganizing around the client.
As 2002 begins, we are confident the changes made in
2001 will make us more valuable to our clients in every
business. Several examples of excellent client focus
deserve highlighting:
> In the individual investor group, virtually all of our 4 million
individual clients received a call soon after
September 11 from their financial advisor, many of whom
worked around the clock to stay in touch, provide reassurance
and continue business.
> Our investment bankers worked with Comcast
Corporation for more than a year designing a successful
plan to complete a merger with AT&Ts cable subsidiary
in a $72 billion transaction.
> Our investment bankers and equity and fixed income capital
markets specialists helped raise $16.4 billion in debt,
$7.3 billion in convertible securities and $5.8 billion in
equity from the IPO of Orange Wireless on behalf of
France Telecom despite the extremely challenging markets
for the telecommunications industry.
> In investment management, our portfolio professionals
achieved superior performance for clients in spite of an
extremely difficult market environment. With 56 of our funds
receiving four or five stars at the close of November 2001,
we had more funds receiving Morningstars two highest
ratings than any other full-service firm.
> In credit services, when U.S. mail delivery of payments
was delayed immediately after September 11, Discover
was the only large credit card issuer to suspend customer
late fees during the disruption of service.
CONTROLLING COSTS
In the three years ending in 2000, our net revenues grew
27 percent per year. Incremental investments and operations
above capacity allowed us to fulfill the extraordinary

demand across all our businesses. In a rapid reversal,
revenues declined 16 percent in 2001, presenting a real
challenge to manage costs to protect profits for shareholders.
Through a combination of reducing incentive
compensation, restructuring existing businesses and closing
selected operations, we were able to reduce the
expense base in 2001 and create profit leverage for when
the economy turns.
Compensation was down 14 percent overall and was down
by a much larger percentage in selected businesses.
Headcount was down 4 percent from peak levels despite
a significant increase in headcount at Discover. Noncompensation
expenses came down throughout the year
and by the fourth quarter were 11 percent below fourth
quarter last year, excluding costs associated with our aircraft
leasing operations. Our focus on costs will continue
in 2002, and we expect to achieve further savings as we
get out of businesses with poor current economics. For
example, we closed our retail business in Japan and several
retail branches in the U.S., as well as our freestanding
retail Internet business, and temporarily halted further international
expansion of our credit card business.
While reducing our cost structure, we have kept in mind that
the factors driving secular growth in financial services  an
aging population, globalization, privatization and productivity
 remain in place and that growth will resume. We
therefore have balanced the need to reduce expenses with
the need to have talent in place to take full advantage of
future opportunities.
TO DO IN 2002
We continue to see the accelerated combination of firms
in pursuit of a global capital markets strategy. In the past
18 months, UBS acquired PaineWebber for $12 billion to
gain retail distribution and asset management in the U.S.,
and Chase completed its acquisition of J.P. Morgan to
strengthen its investment banking and equity business
globally. Also, Credit Suisse First Boston acquired DLJ for
$13.5 billion to strengthen its investment banking and
equity business in the U.S. As a result of these strategic
mergers, there now are about eight firms vying for leadership
in global investment banking and securities distribution,
some of them straining to carve out a viable market share
in a business where there is enough demand for no more
than three or four profitable competitors. This competitive
dynamic will put continued pressure on margins.

Our response in 2002 will be to:
> Continue our strategy of client centricity to understand
and meet our clients needs ahead of competitors.
> Align our resources by client to bring all of our firms talent
and capital to bear on the needs of each client.
> Continue to invest in people and capital, notably in both
our equity and fixed income trading and sales activities
in Europe.
> Maintain cost control while continuing to support our core
businesses and delivering value to clients.
As always, downturns in the economy present the highest
risk to the returns we can generate for shareholders.
Specific challenges also present themselves in 2002. We
must protect ourselves from the decline in credit quality that
always accompanies recessions and higher unemployment.
In addition, the precipitous decline in air travel will likely
pressure the profitability of our aircraft leasing business.
We will continue to measure success, in every business,
by our ability to increase market share and deliver profitability.
We are confident that by building on the progress
we made in 2001, we will be able to achieve market share
gains in 2002. The combination of market share increases
and secular growth in financial markets will provide the
offset to margin pressures.
We know our geographic and product breadth, innovative
and talented people, and strong client-centered culture give
us the competitive advantage to continue to be a market
share leader in our core businesses.
Our employees are the key to our success. We have been,
and continue to be, committed to providing an environment
in which outstanding individuals with diverse skills, experiences
and backgrounds are valued and rewarded. With our
global operations and broad client base, we believe that diversity
in our workforce maximizes the level of creativity and
problem solving that is critical to serving our clients well. This
year we published our first Diversity Report, which communicates
the importance we place on, and some of the action
we take to foster, diversity. We were pleased to receive external
recognition of our commitment from leading organizations
and publications. For example, Fortune magazine named us

one of the top 10 companies to work for in Europe, Latina
Style magazine said we were one of the 50 Best Companies
for Latinas to Work for in the U.S. and Working Mother magazine
selected us as one of the Top Ten best companies
for working mothers. Respect for individuals and cultures,
one of our core values, will continue to drive our commitment.
During the year, we were fortunate to add John E. Jacob,
Executive Vice President & Chief Communications Officer of
Anheuser-Busch Companies, Inc., to our Board of Directors.
Reflecting any more on 2001 would be belaboring the obvious.
Let us just say it is clear that our company has a
superb collection of talented and dedicated people who
can and will rise to meet any crisis or challenge. Morgan
Stanleys employees responded to the needs of each other
and the needs of our clients during the September 11 crisis.
We, indeed, are blessed with a unique resource in our human
capital, and our people ensure a bright future for our company.
We can take this group of talented, dedicated
professionals into any competitive environment and be confident
of success.

PHILIP J. PURCELL
Chairman & Chief Executive Officer

ROBERT G. SCOTT
President & Chief Operating Officer