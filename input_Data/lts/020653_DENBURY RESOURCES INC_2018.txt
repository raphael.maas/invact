DEAR FELLOW SHAREHOLDERS,
2018 continued to form the foundation for Denbury�s
promising future, providing a prelude to Denbury�s
dynamic potential in a strengthening oil market. While we
certainly did not desire the steep fall in oil prices in the
fourth quarter, it provided a strong reinforcement of the
importance of Denbury�s sustained emphasis on balance
sheet improvement, cost discipline and capital flexibility.
Many factors and uncertainties contribute to the oil
supply and demand equation. It is our belief that
projections for strong long-term oil demand, together
with the likelihood that supply growth from tight oil
development will slow and potentially peak around the
middle of the next decade, combine to support a
conclusion that significant long-term additional oil supply
will be critical. At the same time, however, investor
sentiment for the industry has been challenging, with a
desire for companies to show value creation instead of
growing at all costs, all amid increasing public pressure
for oil producing companies to limit their carbon impact
while safely and responsibly providing this vital
commodity that powers much of the world�s economy.
This combination of circumstances and sentiments aligns
perfectly with the core of Denbury�s business, both
currently as well as in the future. With our strong focus
on CO2 enhanced oil recovery (EOR), our current and
planned developments provide decades of low decline,
high margin oil production while annually injecting into
our reservoirs over 3 million tons of industrial-sourced
CO2 that would otherwise be emitted into the
atmosphere. Putting that number in perspective, this
amount of CO2 is equivalent to the amount of CO2 emitted
annually by roughly 700,000 vehicles.
The IEA World Energy Outlook 2018 projects strong growth
in worldwide EOR production, more than doubling from
current levels to approach nearly five million barrels of
production per day by 2040. As the only sizable U.S. public
E&P company whose EOR operations generate the
majority of its production, Denbury�s broad EOR
experience and expertise, our deep inventory of existing
oil fields, and our significant CO2 sources and
infrastructure position us very well to continue to be the
industry leader in EOR for its long future.
Reflecting on 2018, I am inspired by the sustained
dedication and skills of our employees, and I am more
than encouraged and impressed by the great strides we
have taken together as a company. Importantly, we set
Company records in each of our safety and spill prevention
measures, a strong indicator that we continue to be on
the right track in our operations and priorities. The
Company had several other important accomplishments
during the year, including:
� Significant strengthening of our balance sheet,
reducing our leverage ratio by over two turns, and
lowering our net debt by over $280 million;
� Reducing year-on-year G&A by an incremental
$30 million, or 30%;
� Sustained capital discipline, maintaining total capital
within our original guidance range and continuing
our longstanding objective of spending within cash
flow, generating over $80 million in free cash;
� Replacing reserves in excess of our annual production,
with year-end 2018 reserves up 111% compared to
year-end 2017 reserves, less 2018 production;
� Meaningful success in our high value, organic
exploitation program, highlighted by seven
successful wells and a strong inventory for continued
exploitation development in 2019 and beyond;
� Sanctioning of the Cedar Creek Anticline EOR
development, a cornerstone project with the
potential to recover in excess of 400 million barrels
of oil from this great asset;
� Significant success with Phase Five development at
Bell Creek, exceeding our expectations; and
� Extending our bank credit facility by two years
to 2021, while simultaneously refinancing the full
outstanding balance of the facility into a new note
due in 2024, leaving a fully undrawn credit facility.

The fourth quarter of 2018 also provided a strong
reminder of the reasons we must continue to focus our
business in 2019 around $50 oil. The collapse in oil price
was nearly unprecedented in terms of how far and how
fast the price fell, but our cost discipline, coupled with the
capital flexibility afforded by our low-decline assets,
allowed us to quickly adjust plans for a lower price
environment in 2019 while still generating meaningful
free cash.
We constantly consider the future of EOR, and we see
great potential in the Eagle Ford shale, where only around
10% of original hydrocarbons in place are recovered
through primary production. The transaction we
announced in late October � where we would have
acquired Penn Virginia Corporation � was the result of
careful and deliberate consideration of this great
potential and opportunity, and, if consummated, would
have provided the Company with a platform to commence
work on this new frontier for EOR. While we had to make
the tough decision to mutually terminate the transaction
in early 2019 - primarily due to the poor market conditions
and opposition from certain Penn Virginia shareholders �
we continue to believe in the significant potential for EOR
in the Eagle Ford and will continue to pursue practical
opportunities to expand our business in areas where our
EOR expertise, experience, and extensive CO2 resources can
create value for the benefit of all Denbury stakeholders.
Looking ahead to 2019, our priorities are clear. While we
have made significant improvements in our balance
sheet, we will continue with a sharp focus on progressing
additional impactful initiatives. As we have done in past
years, we will manage spending across our high-margin
asset base to generate significant free cash flow, we will
continue to drive the CCA EOR project along its path
toward first oil, and we will expand our exploitation work
to test new, high potential concepts across our asset base.
We will also continue to optimize and expand production
from our great set of legacy fields, where our technical
teams continue to find opportunities to extract even
greater value.
Shareholders, I deeply value your longstanding
commitment to Denbury. Your faith in the Company and
your unwavering support of our team is inspirational and
energizing. We share a great vision for what Denbury can
become, and I, along with the rest of the Denbury team,
are committed to bringing that vision to reality.
Sincerely

Chris Kendall
President and
Chief Executive Officer
March 29, 2019