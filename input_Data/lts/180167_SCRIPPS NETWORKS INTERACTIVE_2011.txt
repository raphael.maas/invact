To Our Shareholders:

At Scripps Networks Interactive, we build brands for life.
Were dedicated to developing creative and compelling television
programming and digital content that captures the essence of
the three major lifestyle categories  home, food and travel. Its
because of this focus and through the integrity and popularity
of our lifestyle content that weve been able to forge a strong,
emotional bond with a highly engaged audience of media
consumers whom advertisers value.
Theres no doubt about it. Fans incorporate our brands into
their everyday lives. We influence the homes they live in, the
foods they eat, and the places they want to travel. This level of
engagement is rare for television networks, and its what sets
Scripps Networks Interactive apart from  and ahead of 
the competition. Its why were the leaders in lifestyle media.

Solid Performance
Scripps Networks Interactive had another
very solid year of growth in 2011, with key
metrics moving in the right direction.
At HGTV, the network maintained its number one ranking
as the destination for upscale women, outpacing all other
ad-supported pay television networks. Thats a remarkable
data point, and one of the many reasons why HGTV is a
must-buy for an impressive list of national advertisers.
HGTV finished the year on a high note with the highestrated
December ever in total day for adults 25 to 54
thanks to successful tent-pole programming events like
HGTV Design Star and Selling Spelling Manor and popular
franchises and fan favorites like Holmes on Homes,
House Hunters and House Hunters International.
Weve stayed true to our mission of creating inspiring,
entertaining and informative programming, and being
faithful to that core concept is why nearly 1 million U.S.
households tune in to HGTV during primetime every night.
Within the food category, Food Network is still in command
of the genre, maintaining its status as a Top 10 ad-supported
network. And practically 20 years after its launch,
Food Network is still setting new records. The network
experienced its best year ever and ended 2011 with the
highest-rated fourth quarter in its history due to the
success of programs like Chopped, Next Iron Chef
America, Restaurant: Impossible and Rachael vs. Guy.
Consumer interest in food programming is as strong as
ever, which is reassuring and affirms our confidence in the
dominance of the brand and our ability to maintain and build
on the powerful audience weve aggregated. Were proud
to have defined this tight but extremely lucrative content
category, and were pushing for even stronger momentum
in 2012.
The success of our flagship brands  HGTV and Food
Network  drove the outstanding operating results in
2011, and were determined to transform Travel Channel
into an equally powerful brand as we grow the audience
and develop the network into a strong marketing platform
for our advertising and distribution partners.

Travel Channel remains the companys single largest
long-term growth opportunity, and the positive viewership
trends in 2011 affirm this. The network finished the year
as the second highest-rated year ever in primetime and
total day thanks to continued strong performance from
series like Ghost Adventures and Anthony Bourdains
No Reservations. Anthonys new show, The Layover,
premiered in the fourth quarter and helped close out
a strong 2011.
Our focus on building a solid foundation for Travel Channel
in 2011 has enabled us to start 2012 with triple the number
of returning series and more than double the number of
talent on the network. Our objective going forward is to
create a signature series or two and discover new, breakout
talent who will define this lifestyle genre and solidify the
Travel Channel brand in the minds of media consumers.
Weve done it before with HGTV and Food Network, and
were confident about developing Travel Channel into a
leading multi-platform lifestyle brand.
Our flanker brands  DIY Network and Cooking Channel
 also experienced great success in 2011, finishing the year
as the highest-rated year in their history. At DIY Network,
the record-breaking performance in the fourth quarter was
bolstered by top programs like Rescue Renovation, I Hate
My Bath and Bath Crashers. At Cooking Channel, Good
Eats, hosted by Alton Brown, drove the strong ratings
performance for the year, as well as Bitchin Kitchen, the
exciting comedy cooking show, which generated media
buzz around the Cooking Channel brand.
At Great American Country (GAC), December 2011 proved
to be a hugely successful month thanks in large part to
the National Finals Rodeo, which aired live for ten days.
All ten telecasts ranked among the top 30 most watched
primetime telecasts in GAC history. The final episode
scored the highest rating in GACs history, and were
pleased to have National Finals Rodeo return to the
network in 2012. Were also enthusiastic about GACs new
direction as we shape its Living Country format and focus

Were confident in our ability to continue building ratings
momentum across all networks as we increase our
investment in quality programming for 2012.
Successful Brand Extensions
Audience engagement with our brands
extends beyond the television. In 2011, we
leveraged our power brands to create
important new revenue streams for the
company, and today they are on the way
to claiming a unique competitive advantage
in the publishing and digital worlds.
Food Network Magazine maintained its rank as the
number one culinary magazine on newsstands. Its also the
sixth-ranked magazine overall based on newsstand sales.
Following in its footsteps is the new HGTV Magazine, which
is already off to an amazing start. In just its second issue,
the magazine has surpassed our expectations and those
of our partner Hearst Magazines, with newsstands sales
exceeding several hundred thousand copies.
On the digital side, our home and food category websites
are building on the lead theyve established in their
respective content categories, each generating millions of
page views and unique visitors a month for us to monetize.
Were also seeing increased consumer engagement in the
mobile app space. Our Food Network In The Kitchen app
has had more than 450,000 paid downloads, making it the
number one paid culinary app in the iTunes store.
The profile for HGTV and Food Network brands is rapidly
expanding through literally millions of consumer touch
points that weve created with retailers, manufacturers
and other business partners. In 2011, we launched several
new products including HGTV HOME Flooring by Shaw,
HGTV HOME paints by Sherwin-Williams, and HGTV-branded
furniture by Bassett.
We also now have more than 1,000 Food Network licensed
products at Kohls Department Stores, and that number
continues to grow. And in February 2011 we announced a
partnership with Wente Vineyards to create a Food Network
wine portfolio, featuring four different wines. After
launching in the third quarter, Entwine currently ranks 34th
in the premium wine category out of almost 400 labels.
Were committed to exploring additional licensing
opportunities around our popular brands, and we expect
to see our licensing business expand in 2012 as well as
significant growth in the digital space with new online,
mobile and social products.
Global Growth
In 2011, Scripps Networks Interactive
moved decisively to take advantage of
promising opportunities in the global
media marketplace.
We completed the UKTV partnership transaction in
September 2011, taking an important step forward in our
international development efforts. Scripps Networks
Interactive partnered with BBC Worldwide and now owns

50 percent of UKTV and its portfolio of ten lifestyle and
entertainment channels. Were enthusiastic about this
opportunity to be a part of a thriving, multichannel, dual
revenue stream media business in one of the worlds
largest television markets.
Also in the U.K., our other investment, Food Network UK,
doubled audience ratings and maintains its enviable status
as the leading food-themed channel and a top lifestyle
network in the country. Weve launched some new locally
developed programs in the U.K. that have proven to be
contributors to this international success story, with
Andy Bates Street Feasts and Reza Mahammads
Spice Prince of India leading the way.
Food Network also launched in five markets in Asia and
in six territories in Europe, Middle East and Africa. We
also expanded our distribution in two markets, making
Food Network available in virtually every home in the U.K.
through the Freeview platform and gaining new audiences
in Portugal on the ZON platform.
We plan to continue growing our international business
both organically in existing markets and through
acquisitions and joint ventures. We also look forward to
expanding into new regions that hold the potential for
profitable growth across the globe. Compelling markets
for us include Eastern Europe, Latin America and Asia,
and we want to strengthen the already solid relationship
we have with our Canadian partner.
We believe we can help our international partners by
contributing our lifestyle programming expertise and
participating in whatever growth we can create together.
Strong Financial Results
The performance and popularity of our
lifestyle media brands is evident in our strong
financial results for 2011. Total consolidated
revenue topped $2.1 billion, segment profit
increased 17 percent and earnings per share
grew 21 percent, continuing our tradition of
year-over-year growth.
Also, as promised, we returned capital to shareholders
through the stock repurchase plan that we initiated. And
our solid balance sheet affords us the flexibility to invest
further in programming, marketing and new business
initiatives while meeting operating requirements.
In summary, 2011 was an outstanding year for Scripps
Networks Interactive. We look forward to building
and expanding on these very positive trends in 2012
and creating long-term value for our shareholders.
We remain committed to sustaining the success and
continued growth of the company. As always, it is a
privilege to lead this great enterprise.
Sincerely,
Kenneth W. Lowe
Chairman, President and Chief Executive Officer