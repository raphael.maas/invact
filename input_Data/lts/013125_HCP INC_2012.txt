To Our Shareholders,
2012 was a terrific year for HCP! In the past, my annual letters have focused on various performance
metrics for the recently completed year, most notably acquisition volumes. 2012 is no exception:
HCP closed $2.5 billion of accretive investments across each of our five property types within our
5 x 5 business model.
Our investment portfolio realized strong performance
with a same property increase of 4.2%. This excellent
result enabled our Board of Directors to increase the
2013 dividend by 5% in January of this yearour largest
dividend increase in over ten years. As important, our
2013 earnings guidance forecasts that our dividend
payout ratio will continue to trend lower, representing
over a decade of improvement. This is significant in that
it provides a substantial margin of safety for the existing
dividend and augurs well for continued dividend increases
in future years.
In addition to our superb investment results, HCP
achieved historical bests for several important corporate
objectives, including balance sheet management, operating
efficiency and sustainability

> Balance Sheet Management: HCPs current credit
ratings of Baa1/BBB+/BBB+ represent our highest
ratings in the past decade. In fact, in the past two and
a half years, HCP has received six separate positive
changes of its ratings from the credit agencies. This
has substantially reduced the Companys cost of capital,
increasing the accretive results of our acquisition
activity. In addition, HCP re-priced its $1.5 billion lineof-
credit realizing a 55 basis point reduction in borrowing
costs.
> Operating Efficiency: One of the benefits of being a
relatively large organization is that scale can produce
attractive operating efficiencies. At the present time,
HCPs business model is fully built out with critical
mass in each of our property types and investment
products. As a result, each invested dollar of acquisition
volume requires a lower amount of incremental general
and administrative (G&A) costsoften referred to
as overheadto support our transaction activity.
HCPs G&A as a percentage of total assets at year end
2012 was 0.36%, the lowest in the Companys history.
> Sustainability: HCP has fully integrated sustainability
into our corporate culture from the Board of Directors
through the entire employee base. Our commitment
to sustainability is one of the eight core values that
define our Companys mission statement. We have
created a Sustainability Committee composed of senior
management that strategically develops and incorporates
sustainability policies for our organization and provides
quarterly reports to our Board of Directors. Similarly, the
leaders of each of our business sectors report monthly
on their sustainability progress. HCP has institutionalized
sustainability by implementing a top-down approach,
and the support of our Board of Directors, executive
management, tenants/operators and all employees has
been critical to the success of our program. We will continue
to prioritize a wide range of sustainability initiatives
in the year ahead.
The United States healthcare industry continues to
experience an increased focus on government reimbursement
policies, cost savings and utilization. This dynamic
environment will undergo substantial change and, we
believe as a consequence, create opportunities for additional
HCP transaction activity. HCP is well positioned
and we look forward to reporting on our future successes
in subsequent annual reports.
Thank you for your support of HCP!
James F. Flaherty III
Chairman and Chief Executive Officer
Long Beach, California
March 2013