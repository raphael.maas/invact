Im fortunate to have assumed
my new role as president and chief
executive officer at this time in
Wal-Marts history. Our Company is
so well positioned for todays difficult
economy and tomorrows changing
world. We have an exceptionally
strong management team, able to
execute our strategy, perform every
single day, and deliver results. This
success is a tribute to the hard work
of many people, but especially Lee
Scott. We appreciate Lee for his service
and outstanding leadership of
our Company.
At Wal-Mart, were very pleased with
last years annual performance, especially
relative to competitors. Our U.S.
stores are delivering faster checkouts,
a friendlier shopping experience and
cleaner presentations. Simply, they
are just operating better. We are on
the move internationally and today
have more stores in more markets.
People who have never shopped with
us previously are now loyal customers.
Sams continues to show the distinct
value of club membership.
We have so much to be proud of at
Wal-Mart when it comes to last years
financial results. Earnings per share
from continuing operations were up
6 percent to $3.35. We delivered
strong returns to our shareholders
through almost $7.3 billion in share
repurchases and dividends. Total
net sales increased 7.2 percent, as we
helped customers save money, so
they can live better.
Operating Segments Deliver
Strong Performance
Each operating segment stepped
up and contributed to our strong
performance. Walmart U.S. had an
extraordinary year by driving home
our price message and driving up
customer experience scores to record
levels. At Wal-Mart International,
currency fluctuations affected our
reported sales, but overall results
were solid. Doors continue to open
for international growth, such as
with our strategic acquisition of
Distribucin y Servicio  D&S  in Chile.
This also continues to be a great time
to be in the warehouse club segment.
Sams Club grew sales 5.6 percent
for the year, and is further strengthening
the member experience.
Continued Focus on ROI
Our team is very focused on working
to improve return on investment (ROI).
Our capital efficiency process drives
expansion decisions. The Company
has stepped up investments in technology
to maintain leadership in an
area that always has been a driver
of our success. These efforts will
contribute to Wal-Marts increased
efficiency through our use of capital,
technology and logistics.
The fact is, Wal-Marts performance
last year would be considered strong
at any time and for any retailer, and
certainly during one of the most diffi-
cult global economies in decades.
This tremendous success is a real
tribute to our culture and every one
of our 2.1 million associates around
the world. Retail was tested this past
year, and our associates made the
diff erence for Wal-Mart shareholders.
Already this year, the global economy
continues to be challenging. But even
with this uncertainty, were optimistic
about Wal-Marts opportunity, because
we are so well positioned. Our customers
expect us to off er the lowest prices.
We simply cannot forget that what we
do makes a diff erence in their lives.
Price Leadership Drives
Global Performance
Theres no doubt that we are
bringing to life our global vision for
Wal-Mart. Our strategy is working,
and were building more and more
momentum. We will maintain our
focus on price leadership in every
market, whether were talking to the
working mom in So Paulo or the
businessman in Tokyo.
As vice chairman responsible for
Wal-Mart International, I saw firsthand
how much our business model
resonates around the world. I make
it a habit not only to walk our stores,
but also to visit our customers in
their homes. I remember one womans
tiny house in Costa Rica. She told
me that everything she needs is at
our Pali store. We owe every customer
our very best  lower prices,
higher-quality goods and a better
store experience.
We also must continue assuming the
larger role that Lee helped us realize
we can play in the world. People have
been asking me, Mike, during these
difficult times, can we aff ord to do
things like sustainability, responsible
sourcing, associate opportunity and
health care? Every time my response
has been, We cant aff ord not to. At
Wal-Mart, we have an opportunity and
a responsibility to lead in the world.
Sustainability is a permanent part of
our culture. It helps us remove waste,
lower costs and provide savings to our
customers. There will be no part of
this Company anywhere in the world
that does not contribute to making
Wal-Mart more sustainable.
One way is through responsible
sourcing. Last year, I stood in front of
more than 1,000 suppliers in Beijing,
China and committed our Company
to building a more socially and environmentally
responsible supply chain.
Well make a diff erence in responsible
sourcing that no other retailer and no
other company can make.
Job Creation and Advancement
Were also off ering opportunities for
associates. Last year, Wal-Mart created
33,000 jobs in the United States and
thousands more in our other markets.
These good jobs offer competitive
wages and benefits and the opportunity
to advance. We plan to create
tens of thousands of jobs again this
year. And, well continue to be a force
for inclusiveness everywhere we
operate. We need our associates
more than ever.
We will find ways to make a
diff erence through our participation
in debates on issues, including energy,
health care and trade. At Wal-Mart,
we wont sit back and say, Thats
someone elses job. We have a role to
play. Wal-Mart has great relationships
with governmental leaders in many
countries around the world. Whether,
for example, its with Democrats or
Republicans, or President Obamas
new administration, Wal-Mart will
play a role.
My first day in the new job fell on
the first Sunday in February. So my
wife Susan and I took a few hours to
move some boxes and belongings
into my new office. But we didnt
replace the carpet, furniture, or even
Sam Waltons old wood paneling. With
the exception of a couple of pictures
on the wall, we kept it as the same
office in which Sam Walton, David
Glass and Lee Scott made the decisions
that built our great Company.
I could not be more honored or more
humbled to sit at their desk now.
Although there will continue to be
many changes at Wal-Mart, there is
one thing that will never change  our
culture. Wal-Mart associates operate
with honesty and integrity. We respect
people as individuals and strive for
excellence. And, most important, we
obsess about our customers and ways
to serve them better than ever before.
Im certain of our strategy, our
opportunity and our ability to perform
as individuals and as a Company. By
executing well and adhering to our
values, we plan to distance ourselves
even further from the competition and
do even more to save people money
so they can live better.

Michael T. Duke
President and Chief Executive Officer,