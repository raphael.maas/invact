Cimarex had an extraordinary year and our accomplishments were reflected in the
stocks strong performance. Our share price increased 82 percent, making it one of the
best performing E&P stocks in 2013. This performance was driven by our companys
success in defining additional long term asset value through the delineation and
expansion of our Delaware Basin acreage position, exposing our deep inventory of
high quality investment opportunities.
While others may be driven by growth targets, we see growth in reserves and
production as the logical outcome of good investment decisions. We seek to invest
aggressively, yet wisely. Over time, we believe that a bold but disciplined investment
approach will lead to profitable growth.
The past year was no exception. We invested $1.6 billion in exploration and development in 2013 which resulted in reserves and production both growing 11 percent.
We added 727 Bcfe of proved reserves which replaced 288 percent of production.
This was achieved through highly profitable investments that added real value on a
debt-adjusted basis. We ended the year with a strong balance sheet with debt to
capitalization of 19 percent.
The Delaware Basin was key to our success in 2013 and will be a cornerstone of future
growth for Cimarex. We saw continued high rates of return from our Bone Spring
drilling in both New Mexico and Texas; we unlocked oil prone Avalon shale development
on our Lea County, New Mexico acreage through new completion technology; and we
saw our opportunity in the multi-pay Wolfcamp shale expand across our Texas acreage.
In 2013, the Delaware Basin Wolfcamp came to the forefront as a premier U.S. shale
play bringing with it capital and competition. Usually we do not welcome competition,
but the new entrants meant more wells being drilled, more information, and,
most importantly, a significant increase in the rate of technical change. All of this
contributed to the delineation of the productive fairway and resulted in further
high-grading of our position in the core of the play. Cimarex was an early mover in the
Wolfcamp play. We began amassing our 180,000 net acre position in the Wolfcamp shale
fairway in 2007. We completed one of the industrys first Delaware Basin horizontal
Wolfcamp wells in 2008 and now have over 60 wells producing from the Wolfcamp.
In Culberson County, in addition to the Wolfcamp D, we established production in
two more Wolfcamp benches, the C and the A. In June, we signed a Joint Development
Agreement with Chevron which solidified our Culberson acreage, making it possible
to test long laterals that should ultimately optimize development. We also share
development of gathering and midstream assets, a win-win for both companies.
In 2013, Cimarex found additional stacked Wolfcamp pay across our acreage in
Reeves and Ward County, Texas. We successfully tested longer horizontal wells (7500
and 10,000) with very encouraging results and we applied new completion techniques
that led to a significant uplift in our per-well results. In various parts of the Delaware
Basin Wolfcamp trend, we have established as many as three separate Wolfcamp
benches to exploit. would like to comment on the extraordinary contribution that our industry is
making to the welfare of our country, and to the world. While most major energy
producing countries are facing flat or declining production, the United States is
experiencing an unprecedented boom in both oil and natural gas production. We are
the third largest oil producer in the world and have experienced a 60-plus percent
rise in oil production over the past seven years. During 2013, the United States
passed Russia as the largest energy producer in the world (oil and natural gas). At our
current trajectory of growth, the U.S. is on track to pass Saudi Arabia as the worlds
largest oil producer by the latter part of the decade.
How did this happen? It happened because of the structure and freedom of the
American economic system. The growth in U.S. production did not come from central
planning, it did not result from government programs, nor did it happen on federal
lands. It did not originate in the boardrooms of large multinational corporations.
It came from a group of relatively small, independent exploration and production
companies that were allowed to do what Americans do bestinnovate and compete
in the search for profits. It is a remarkable story of technology, perseverance, innovation, entrepreneurism, capitalism, contract law and property rights. There is a reason
why this energy renaissance is a particularly American phenomenon. It bespeaks the
very best of our nation and of the tremendous power of private enterprise and free,
well-regulated markets.
The benefits of this American energy boom are numerous. The United States, with
one of the largest economies, has one of the lowest energy cost structures in the
world. We are seeing manufacturing return to the United States. Increased U.S. oil
and gas production has changed the fundamental supply dynamics and long term
outlook. Our nation has the luxury of having a secure, stable energy supply while we
seek out the next energy technology and provide leadership to the world.
There are many fine companies within our industry and I am proud that Cimarex is
one of the finest. We have a talented and dedicated workforce that is focused on results.
Good ideas spread rapidly within our organization. We embrace small company
impact and accountability rather than big company processes and bureaucracy.
We believe that talented, driven professionals want to be objectively measured and
rewarded for their results. We have a management team that has been together for
many years with common values and a philosophy that defines our corporate culture.
We have a Board of Directors that understands our business and adds perspective
and oversight that makes our organization stronger. I want to express my sincere
appreciation and gratitude to our employees, our Board and our shareholders for
their contribution in making Cimarex the premier company that it is today.
Sincerely,
THOMAS E. JORDEN
Chairman, President and Chief Executive Officer
March 13, 2014