fellow shareholders:
In the first year of a new millennium, Verizon
Communications was born. Formed by the merger of GTE
and Bell Atlantic and the creation of Verizon Wireless in a
joint venture with Vodafone, we immediately established
ourselves as one of the leading companies in the global
communications industry. Our assets, financial strength
and management focus will enable us to take advantage of
the growth trends that are transforming our industry, as
well as execute our game plan in an increasingly competitive
marketplace.
Our new company is built on an age-old truth: Customer
service and world-class technology produce real long-term
winners.
Verizon turned in a solid financial performance in its first
year, confirming the validity of our business model and our
ability to execute on it. We registered 7.2 percent adjusted
revenue growth and added thousands of customers
from new and growing services such as long distance, DSL
and wireless. Our reported earnings for 2000, which
include the effects of certain gains, charges and other
adjustments, were $11.8 billion or $4.31 per share, compared
with $8.3 billion or $2.97 per share in 1999. Our
adjusted earnings per share were $2.91, up from $2.84
in 1999, in line with investors expectations.
We are disappointed in our stock performance in 2000,
which  like that of most of the large-cap telecom companies
 trailed the broader market. We believe our track record
of meeting our financial and operational targets, achieving
merger synergies, and generating cash for reinvestment
will turn that around in 2001. Verizons combination of a
strong balance sheet, steady cash flow, and expanding top-line
growth offers investors both stability and growth  a potent
one-two punch in the increasingly competitive telecom space.
For all the uncertainty of the investment environment in
2000, the big story continues to be growth: the Internet
is spreading faster than any disruptive technology in
history, data communications is growing by 30 percent a
year, and wireless continues its relentless drive toward a
billion users worldwide by mid-decade. Verizon is in the
middle of all these trends. Therefore, we have the opportunity
to create shareowner value as the market places a
more appropriate valuation on our assets and growth
prospects.
We wish to extend our thanks to the extraordinary
management team who not only integrated these new
companies, but also achieved excellent operating results
in some of the most competitive markets in the country.
We also want to commend the 260,000 women and
men of Verizon who came together under the banner of
a new company and worked tirelessly to deliver the benefits
of communications to customers everywhere.
Together, we are committed to making Verizon the most
respected name in communications.
We pursue this goal on the strength of some of the best
assets in the industry:
 One of every three telephone access lines in
the country, covering two-thirds of the top 100
U.S. markets.
 Wireless services in 96 of the countrys top
100 markets.
 One-third of all the directories published in the U.S.,
along with the leading online directory, SuperPages.
 International wireless and wireline investments in
19 countries.
 Access to and, ultimately, control of broadband
transport through an ownership stake in a top-tier
Internet backbone provider, Genuity.
 A growing global network that will position us to
better serve the large business market.
 And the hardest asset of all for competitors to
replicate: a huge base of skilled employees who
collectively engage in millions of interactions with
customers every day.
We brought these world-class networks and millions of
customer connections together under a single brand and a
unified management approach. More than that, we demonstrated
our ability to compete, execute and grow.
We met or exceeded some ambitious operational goals for
2000. In our domestic telecom business, we successfully
launched two businesses  long distance in New York and
high-speed Internet access over DSL  and registered a
third straight year of 30 percent growth in data. Verizon
Wireless added 3.7 million new customers on the year 
including 1.2 million in the fourth quarter alone  for an
industry-leading total of 27.5 million, and made a strong
push into wireless data. Our international investments contributed
$2 billion in revenues, fueled by 47 percent growth
in proportionate wireless subscribers. And Information
Services continued to be a strong source of cash flow, and
provides a foothold on the future as we shift increasingly to
electronic and Web-based directory products.
We achieved this record of solid growth and operational
excellence while continuing to invest for the future by
directing capital toward new technologies and growth
businesses. You can see the result of this focus on growth
in the transformation of our revenue profile; 41
percent of our revenues derive from wireless, data,
international and information services, and we expect that
to grow to 55 percent by 2003.
Looking ahead to 2001 and beyond, we see continued strong
demand for our services. We expect to expand the footprint
of our long distance business, increase our DSL subscriber
base, and make further inroads in the growing field
of wireless data.
We also believe our premier position in the U.S. market will
give us tremendous advantages as we further nationalize and
globalize our business. Our focus will be on leveraging the
huge amount of international traffic that originates in our territory
and expanding our ability to serve the communications
needs of our existing base of large business customers.
With strong core businesses, an expanding position in
growth markets, and the financial strength and flexibility to
reinvest and innovate, Verizon is well positioned to remain
one of the flagship companies in our industry.
We recognize the obligations that go with being the
market leader. Weve stepped up our capital investment,
accelerated the pace of innovation and transformed
ourselves for the competitive era  all to make sure were
ready for the new technologies, applications and
networked products that will ride our data-centric
networks.
In the process, we have raised the bar of excellence for
our competitors and redefined what it means to be a
full-service, integrated communications company.
We wish to thank the Board of Directors for overseeing
our amazing progress in 2000 and our fabulous employees
for building the engine for growth.
In 2001, we will press on the accelerator. Our customers
are hungry for the new services that will usher in the next
phase of the broadband era, and we intend to deliver them
with a level of service unparalleled in the industry.
Thats the Verizon promise.

Charles R. Lee
Chairman
CoChief Executive Office

Ivan G. Seidenberg
President
CoChief Executive Officer