Dear Fellow Shareholders: We begin this letter by paying tribute to our employees. They met
the challenge to effect 2001s enormous changes and turned out a record performance. In the wake
of our mid-year 2000 purchase of USA Group, we expected 2001 to begin a period of stronger
growth and new direction for Sallie Mae. Those expectations proved accurate2001 was indeed a
very good year. Our 6,000-plus employees stepped up to make it happen.
For starters, we passed a major milestone: $10 billion of preferred channel loan originations, 40 percent
above 2000. Our expanding sales force, armed with the best array of financial aid products, successfully
added many new schools and enhanced our business with existing school customers.
We are particularly proud of our operations teams. They processed $14.5 billion of loan acquisitions
(preferred channel plus $5 billion of additional loan purchases) and implemented most of our complex
integration activities. They executed flawlessly through peak season. Their can-do approach kept
our spirits high during that critical period.
Our late summer peak season proved even stronger than anticipated. Confronted with record application
and disbursement activity, we blinked and decided to put customer satisfaction ahead of meeting
every self-imposed integration deadline. Our productivity goals were postponed, our cost-reduction
goals were not metbut not canceledand we expect to deliver further efficiencies again in 2002.
Effective Integration. Sallie Maes successes of 2001 resulted in large part from the value inherent
in the 2000 USA Group transaction and effective integration of their activities. We believe we have
blended the talent and dedication of our new workforce. Our customers report that we passed the
most important test: They did not even notice we integrated. The merger put us into the guarantor
services and loan collection businesseswith growth potential that fits our capabilities and produces
fee income using very little balance sheet and capital. We grew USA Funds guarantees 8 percent in
2001, up from just 6 percent in 2000. We expect to step up that performance significantly in 2002.
We like the student loan collection business and will aggressively pursue market share. This business
can cut taxpayer cost dramatically. We see a challenge in reducing the more than $30 billion of student
loan paper that remains uncollected. Today, we manage only $6 billion of that total. Already
in 2002, we have strengthened ourselves for this task with the acquisitions of Pioneer Credit and
General Revenue Corporation, two highly successful student loan collection experts.
Last year, our corporate finance group effectively solidified the right side of our balance sheet. They
used 2001s benign interest rate environment to provide lower and more stable borrowing costs for
several years. We grew capital, yet maintained an active share repurchase program. These and other
financial successes led us to accelerate our planned GSE exit to September 30, 2006, two years earlier
than originally scheduled.
Yes, we had a good year in 2001. We achieved core cash basis diluted earnings per share of $3.75, a
28 percent increase over 2000 and well beyond our 15 percent, long-term earnings target. We are in an
enviable position; we can see meeting our growth targets well into the future in businesses we know.
Financial Integrity. Few 2001 annual reports will be issued without some commentary about
financial integrity. You probably already know that Sallie Mae reports earnings on both GAAP and
core cash basis. The operating results of each approach are presented in this report, and we hope
you find the extra information more useful than confusing. The core cash basis results are critical for
us to judge our own financial performance accurately. Sadly, todays GAAP distorts the economics of
several of our asset/liability funding mechanisms and creates misleading earnings volatility. The inherent
conservatism of the core cash methodology will lead to lower reported earnings over time than
will GAAP, though short-term fluctuations in either direction can be very material.
Giving Back. In 2001, Sallie Mae demonstrated a commitment to serving the communities where
our customers and our employees live and work. Our philanthropic arm, The Sallie Mae Fund, undertook
the launch of First in My Family, an effort designed to encourage young Hispanic-Americans to
be first in their family to go to college. Also, we created the Sallie Mae 911 Education Fund shortly
after the terrorist attacks to relieve affected families of their student loan debt and provide scholarship
funds. We are pleased that numerous families have participated in this program, and we expect to see
many more as days pass. Like you, we were horrified by the events of September 11, and we hope
our efforts are meaningful to the victims of these atrocities.
The next several years present dramatic market opportunities for Sallie Mae. We will take advantage
of each chance we see to enhance our scale, build markets for our key services, grow student loan
share and diversify our funding sources. We expect an exciting 2002.
Strategic Priorities. Immediate strategic priorities are the Higher Education Act Reauthorization
process and our accelerated transition from GSE status to a fully private-sector corporation. We see
these as opportunities to strengthen the nations student loan delivery system and Sallie Mae. Sallie
Mae is very proud of its role in delivering the FFELP which is, in our opinion, a tremendous bargain for
the American taxpayer. Taxpayers pay less today to make and maintain FFELP loans than they did 10
years ago. Better collection practices can lower that cost even further. We believe Congress and the

administration appreciate the FFELPs effectiveness as evidenced in January 2002 when they permanently
restructured the mechanism for setting student loan interest rates with a private-sector finance
index90-day commercial paper. That legislation should cut the program cost and encourage the
private-sector competition so necessary for innovation.
Completing our exit to a fully private charter will test our financing abilities. We are anxious to
completely shed the GSE mantle and get on with our new status. This year is pivotal as we begin to
accelerate securitization activity and debt issuance for the holding company without impairing the
achievement of our long-term earnings goals.
Keeping Our Promises. Along the way, we have been keeping our long-standing promise to
transform the company from a wholesale to a retail business. Our business plan has proven solid and
true. It has put us in position to surpass the direct lending program soon as the No. 1 originator of
student loans in the United States. The plan has provided the patient investor good returns. Now, we
are ready for Sallie Maes next chapter. Shortly after year-end 2001, we both executed employment
contracts that focus us on a new set of long-range performance targets. We thank the Board of
Directors for its confidence and look forward to our next set of accomplishments.
We want to acknowledge our pride in achieving success for each of our key constituencies: students,
schools, taxpayers, shareholders and employees. We are heartened by Wall Streets recognition of our
potential and confidence in our ability to deliver on our promises. We also want to thank our very
loyal investors. We know who you are, and we appreciate you.
The first several pages of our Annual Report chronicle how Sallie Mae is making a difference in the
lives of the individuals we serve  the faces of Sallie Mae. They are just seven people, but their stories
represent Sallie Maes involvement, in meaningful ways, in every step of the student loan life cycle.
Our involvement throughout the process is at the core of what we do as a company, and it allows
us to fulfill our mission: to make higher education affordable and accessible to all Americans at all
times of their lives.
We thank you for your support of this mission, and we hope that you find this report to be of interest.

Albert L. Lord 
Vice Chairman & Chief Executive Officer 

Thomas J. Fitzpatrick
President & Chief Operating Officer