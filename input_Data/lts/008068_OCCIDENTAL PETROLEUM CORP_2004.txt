Report to Stockholders
2004 was a year of exceptional performance as Occidentals
net income and operating cash flow reached historic highs. Our
achievements were the result of executing our long-term business
strategy and a series of management actions over the past decade
to position the company for sustainable growth and profitability.
These actions made it possible for the oil and gas segment to
capitalize on robust energy prices and for the chemical segment to
reap the benefits of an improving economy in 2004.
I believe last years performance shows that our business model for
creating significant value for stockholders is sound and that the
management team is leading the company in the right direction.
Our success is, above all, a tribute to the hard work and dedication
of more than 7,000 employees around the world.
I want to express appreciation on behalf of the board of directors to
Dale Laurance, who served the company with distinction for over
21 years in various roles, including president, head of its oil and
gas operations and a valued member of the board. We thank him
for his dedication and many contributions to our success and wish
him well in his retirement.

Occidentals year-end closing stock price of $58.36 per share was
the highest year-end price in the companys history. Our 2004 total
return to stockholders of 41 percent, based on stock price
appreciation plus dividend reinvestment, ranked in the top quartile
among 11 oil and gas industry competitors. We also outperformed
the Standard & Poors 500 Index and the Dow Jones Industrial
Average, which produced total returns of 11 percent and 5 percent,
respectively. Over the last ten years, Occidentals total return of 346
percent ranked in the top quartile among our industry peers and
surpassed both the Dows 243 percent and S&Ps 212 percent returns.
During that same ten-year period, Occidentals market capitalization
nearly quadrupled, rising from $6.1 billion to $23.2 billion.

The steps we took in prior years to improve the business
fundamentals of both our oil and gas and chemical segments
were critical factors in driving our 2004 consolidated net income
to an historic high of $2.57 billion, 64 percent higher than the
previous record of $1.57 billion in 2000. Cash flow from operations also rose to a record high of $3.9 billion. Total debt
was reduced from $4.6 billion at year-end 2003 to $3.9 billion at
the end of 2004  a reduction of 15 percent. The net effect of lower
debt and higher equity reduced our debt-to-capitalization ratio from
37 percent to 27 percent  the lowest level in the companys history.
Our disciplined approach in strengthening the balance sheet has
resulted in a significant reduction in our interest expense. The
base interest expense for 2004 was $223 million, compared to
$272 million in 2003 and $546 million in 2000.
A key element in our strategy to create value is the disciplined
investment of capital resulting in rates of return that rank in the top
quartile among our oil and gas industry peers. We expect 2005
capital expenditures to increase by approximately 12 percent,
compared to the 2004 level, rising to an estimated $2.1 billion. The
oil and gas segment accounts for about 90 percent of the total
capital allocations. The 2004 return on capital employed rose to 20
percent. In addition, the return on equity increased to 28 percent
while stockholders equity climbed to a record $10.6 billion.
In February 2005, the board of directors announced the third
dividend increase since 2002. The quarterly dividend rate was
raised by 13 percent from $0.275 to $0.31 per share for an
annualized payout rate of $1.24 per share. The board will
continue to review the dividend policy annually.
Oil & Gas

Our consolidated financial results were driven primarily by record
high oil and gas segment earnings of $3.5 billion, a 33-percent
increase over the previous record set in 2003. Oil and gas consolidated
net income on a barrel of oil equivalent (BOE) basis  after
taxes and before interest expense  was $14.04, which we believe
will place Occidental in the top quartile of large capitalization oil
and gas industry peers for the sixth consecutive year.
Worldwide oil and natural gas production averaged 566,000 BOE
per day in 2004, or a total of 207 million BOE for the year  an
increase of 3.5 percent from 2003. Last year, we continued adding
proved oil and gas reserves at a pace well ahead of production. At
year-end 2004, Occidentals total proved reserves from all sources
rose to a record high of 2.53 billion BOE  with oil making up
80 percent of the total. The United States accounted for 73 percent
of our total proved reserves followed by the Middle East with
18 percent, Latin America with 6 percent and 3 percent for
other operations.
United States
Occidental produced an average of approximately 339,000 BOE per
day in our U.S. operations, or about 60 percent of total worldwide
production. The largest operation is in the Permian Basin of West
Texas and Southeastern New Mexico, which accounted for an
average of 175,000 BOE per day, or 31 percent of our worldwide
production. Production from California averaged about 118,000
BOE per day, which was 21 percent of the worldwide total. The
other domestic operations averaged 46,000 BOE per day.
Middle East
Production from our Middle East operations, which benefited from
the start up of a new gas project in Oman, increased by 7 percent
from 2003 to an average of 100,000 BOE in 2004, and accounted
for 18 percent of our total worldwide production.
During the year, our Middle East capital expenditure program was
dominated by the expansion of oil production and processing
facilities in the Idd El Shargi North Dome field in Qatar, which is
scheduled for completion in 2005.
Work on the $4 billion Dolphin Project, the premier transborder
natural gas project in the Middle East, is proceeding on schedule
toward a projected start up in late 2006. Occidental has a 24.5-
percent interest. There are two components of this project. One
involves the initial development of approximately two billion cubic
feet per day of natural gas from Qatars giant North Field and
construction of related processing facilities in Qatar. The other
entails construction of the new 48-inch diameter, 260-mile long
Dolphin Energy Pipeline to transport the gas from Qatar to markets
in the United Arab Emirates. When this 25-year project becomes
fully operational, Occidentals net share of production is expected to
average approximately 275 million cubic feet of gas per day and
20,000 barrels of liquids per day  or about 66,000 BOE per day.
Latin America
Our production from Colombia and Ecuador averaged 79,000 barrels
of oil per day  an increase of 39 percent compared to 2003.
Our development of the Eden-Yuturi field in Ecuador and the
commissioning of a new export pipeline were key factors behind
the increase.
In April 2004, Occidental signed an agreement with the Colombian
Government, which extended the term of Occidentals contract
as operator of the Cao Limn oil field through 2018. In 2004,
Occidentals net share of production from Colombia averaged
33,000 barrels of oil per day.
Chemicals

Our chemical segment earnings of $412 million were 87 percent
higher than in 2003. The increase was due to continued improvement
in most sectors of the economy in which the companys core
products are sold, particularly the building and construction
markets. Improved economic growth resulted in a tightening of the
supply/demand imbalance for our major products, leading to
margin improvement through the year as higher prices more than
offset increased energy and feedstock costs.
In October 2004, Occidental agreed to purchase three chemical
manufacturing facilities from Vulcan Materials Company. Upon
completion, this transaction would increase Occidentals annual
chlor-alkali (chlorine and caustic soda) production capacity of
3.3 million tons by 23 percent. This transaction, which is subject
to regulatory approval, is expected to close in 2005.
Health Environment & Safety
In 2004, Occidentals commitment to exemplary performance in the
health, environment and safety arenas resulted in the best safety
performance in the companys history with an Injury and Illness
Rate (IIR) of just 0.33 injuries per 100 employees per year. This is
the ninth consecutive year that Occidental recorded a rate of less
than one. The average IIR for all industries in 2003 was 5.0. This
achievement is a testimonial to the dedication of our employees
whose goal is to continue driving this rate toward zero.
Occidental monitors the research and debate on climate change
as we strive to cost-effectively manage greenhouse gas
emissions from all of our operations. We made progress along
this path in 2004 by becoming a partner in the U.S. Environmental
Protection Agencys Natural Gas STAR program, which requires
participants to report progress in reducing methane emissions
from oil and gas operations. We welcome this partnership, which
also provides a forum to share new technologies and best practices
with our industry peers. We expect our involvement to enhance
our existing efforts. Occidental also supports President Bushs
Climate VISION (Voluntary Innovative Sector Initiatives: Opportunities
Now) program and, through the American Petroleum Institute and
the American Chemistry Council, we are implementing voluntary
initiatives to improve air quality.
Corporate Social Responsibility
Occidental employees have always been dedicated to making
positive contributions to society in our drive to create value for our
stockholders. We recognize how our actions fit into the larger
ecosystem, but we also understand that we play a role in the
communities where we operate. Responsible corporate citizenship
means that we take seriously our commitment to maintain high
standards of business ethics and to manage our businesses with
transparency and accountability. That includes striving to maintain
a safe workplace, protecting the environment and respecting
human rights. It means having the kind of company in which
employees and stockholders can take genuine pride.
Last year, the company adopted a human rights policy that
was integrated with the existing Code of Business Conduct. The
policy was developed in collaboration with representatives of the
Interfaith Center on Corporate Social Responsibility, a 33-year-old
coalition of faith-based institutional investors with a combined
portfolio worth an estimated $100 billion. In addition, the board
of directors added oversight for social responsibility issues to
the newly named Nominating, Corporate Governance and Social
Responsibility Committee.
In January 2005, Occidental signed a statement supporting the
Partnering Against Corruption Principles for Countering Bribery,
at the World Economic Forum in Davos, Switzerland. This initiative
is consistent with the companys Code of Business Conduct and
serves as another milestone in our commitment to operate safely,
responsibly and ethically.
Looking Ahead
The strategic decisions we made over the past decade, and the
resulting actions, provide a solid foundation for the future.
We are committed to maintaining a disciplined investment strategy
by allocating capital to projects that support our ultimate goal of
producing top quartile total returns. Our investment strategy aims
at balancing near-term profitability with long-term growth. That
means we must remain focused on maximizing profits and cash
flow from our core businesses today while making carefully
targeted investments to capture new opportunities for profitable
growth tomorrow.
We are pursuing several projects that we believe fit our growth
strategy. These projects include asset acquisition and
enhanced oil recovery projects in our core areas, and increased
exposure to high potential exploration prospects. Libya,
for example, will play a key role in the companys future
exploration efforts.
Occidental has a long history of exploration success in Libya,
where the company made a series of world class discoveries. We
hope to add to that legacy. On January 29, 2005, Occidental was
the big winner in Libyas first exploration licensing round since the
lifting of U.S. sanctions last year. Occidental was awarded
interests in nine of the 15 blocks that were offered. These new
blocks, together with the blocks we held prior to our 1986
departure from Libya, encompass approximately 72,500 square
miles, making Occidental the largest oil and gas acreage holder in
Libya. Preliminary work for the new blocks is already under way, and
we expect to be very active in Libya this year and in the future.
The outlook for our chemical segment is favorable. If the U.S. and
global economy continue to grow at the current rate, we believe
our chemical segment is positioned to have its best year since the
record performance of 1995. Nevertheless, this is a volatile
business, and we are aggressively managing our costs in order to
further improve the efficiency of our operations.
Guided by a strong board and a talented management team, I
believe Occidental is in excellent shape to continue delivering the
kind of strong performance our stockholders have come to expect.
Dr. Ray R. Irani
Chairman, President and Chief Executive Officer