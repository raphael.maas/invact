Dear Fellow Shareholder: 
I'm pleased to be able to write a far more positive letter 
to you today than a year ago. Raytheon made consider 
able progress during the year 2000. One of our top 
priorities has been to restore investor confidence --- 
that is, to restore your confidence in us. We've focused 
on the fundamentals, on achieving operational and 
financial reliability, and on generating operating cash 
flow. We're seeing results from Raytheon Six Sigma, 
improving basic processes, and focusing on program 
management. 
Looking ahead, we're well positioned as a leader in 
defense electronics. We have tremendous breadth and 
expertise in both defense and government electronics. 
We can draw on our capabilities across several businesses; this creates strategic growth opportunities. We have 
a strong franchise in business and special mission 
aircraft. We have a chance to penetrate high growth, 
high valuation commercial markets with leading edge 
technologies. 
Generating cash flow 
Operating cash flow experienced almost a $1.3 billion 
positive swing --- from a cash outflow of $317 million 
in 1999 to a positive $960 million in 2000. Operating 
cash flow from our defense electronics businesses was 
especially strong, exceeding our plans and prior year 
results. Net debt at yearend 2000 was $9.1 billion, 
improved from $9.5 billion at the end of the prior year. 
We have more work to do in the area of cash management, but our employees made a good start in 2000. 
We've established a direct link between compensation 
and cash management. 
We reported sales for 2000 of $16.9 billion, down 
slightly from 1999. Income from continuing operations 
for the year totaled $498 million, or $1.46 per diluted 
share, in line with the company's projections. 
Improving basic processes 
We worked hard in 2000 to improve our basic processes 
to provide structure and discipline across our organization. We've introduced or expanded the use of several 
disciplines: 
The Integrated Product Development System (IPDS), 
a process to support program management and engineering development throughout the product lifecycle, 
from program pursuit to definition to design and production. IPDS enhances customer success and provides 
the discipline necessary for program predictability, 
performance, and reliability. 
The Earned Value Management System (EVMS), which 
provides the program manager with powerful tools and 
data to help track program costs and keep programs on 
schedule. These tools help program managers identify 
potential problems before they become issues. 
Improved information systems. We're completing installations of new enterprise resource planning (ERP) 
systems in three of our business operations, and are 
focusing now on two others. We've committed significant resources to transitioning old legacy systems and 
designing new systems. 

Supply Chain Management. The goods and services 
we use cost approximately $7 billion annually. That 
presents a significant opportunity for savings. We're 
addressing this opportunity by developing improved 
supplier alliances, a more focused approach to inventory management, and electronic procurement solutions. 
Deploying Raytheon Six Sigma 
I'm pleased to report that Raytheon Six Sigma, just 
two years old, is kicking in across the company, as 
highlighted below. 
Our employees will be the first to tell you that 
Raytheon Six Sigma is more than a tool to increase 
productivity and quality, though these are clearly 
important. It's a way of thinking, working, and 
communicating. 
Our projects start with a clear description of where we 
are today and the desired end state. We then use proven 
tools and methodologies to get from here to there. It's 
not magic. It's tied to our business goals. We prioritize, 
execute, and measure. We're practical. 
Our employees are encouraged to use their training to 
effect common sense solutions. On occasion, we need 
to develop solutions that require considerable time and 
expert training; however, Raytheon Six Sigma is not 
intended to be the way of the few; it is the way of the 
many. 
Raytheon Six Sigma is helping to make us a supplier 
and an employer of choice. I am proud of what our 
employees have accomplished so far, and this is just 
the beginning. 
Raytheon Six Sigma is helping to plant 
seeds for the future. Since its inception 
two years ago: 
Over 7,000 leaders have received 
Raytheon Six Sigma education; 
A total of 527 employees have completed expert training, with more 
experts in the pipeline; 
Some 1,200 employees have been qualified as specialists, also with many more 
on the way; 
A total of 62,000 employees have 
received orientation; 
Employees have initiated more than 990 
projects. 
In the year 2000, Raytheon Six Sigma initiatives generated approximately $100 
million in pretax profit and $200 million 
in cash flow --- and many satisfied customers and empowered employees. 
In 2001, Raytheon will continue to focus 
on working capital management, which 
lends itself to Raytheon Six Sigma 
solutions. 


Leadership in defense electronics technologies 
We ended 2000 with a record backlog of $26.5 billion, 
an indicator of customer confidence. While the new 
U.S. Administration will need time to evaluate future 
defense requirements, we have a broad base of electronics capabilities and are positioned well in areas that 
match the direction of U.S. defense priorities, including 
missile defense; intelligence, surveillance, and recon 
naissance (ISR); federal information technology needs; 
and network-centric defense solutions. We have 
proven competencies in government electronics, especially in key markets such as air traffic management. 
Here is a sampling of some, but by no means all, of 
our core competencies in defense and government 
electronics: 
Missile defense technologies --- we are leaders in both 
National Missile Defense and ground and sea-based 
theater missile defense systems; 
Advanced tactical missiles --- our new AIM9X next 
generation, short-range, air-to-air missile has received 
the go-ahead for low rate initial production; 
Advanced radar technology --- the Advanced 
Electronically Scanned Array (AESA) radar will be 
adaptable to multiple airborne platforms of various 
sizes, providing greater flexibility to meet mission 
requirements at very low risk; 
Network-centric, information focused solutions --- 
Raytheon's Cooperative Engagement Capability (CEC) 
for the U.S. Navy fuses information to provide the fleet 
with a more comprehensive picture of the battle space; 
Information security --- we were selected to provide 
information security for the new Navy-Marine Corps 
Intranet; 
Network-centric solutions for the UK --- Raytheon 
Systems Limited is providing the United Kingdom with 
the Airborne Standoff Radar surveillance system and 
with the Successor Identification Friend-or-Foe recognition system; 
Air traffic management automation systems, air traffic 
control surveillance radar, and navigation systems --- we 
help meet global air traffic management needs; 
Specialized skills in aircraft modification --- we are transforming a Boeing 747 jet into an airborne observatory 
that will carry a 40,000-pound infrared telescope high 
into the atmosphere to peer into space; 
Engineering, technical, and logistics support services --- 
we were awarded a Navy contract to provide integration 
and installation services for the Navy's ship and shore 
communities' command, control, communications, 
computer, and intelligence systems. 
A leader in business aviation 
Raytheon is a leader in business aviation and special 
mission aircraft. Raytheon Aircraft Company (RAC) 
delivered more new airplanes in 2000 than in any year 
in nearly 20 years, increasing deliveries more than 30 
percent over the prior year. The year was not without 
disappointments: the delay in certification of our new 
entry-level jet, the Premier I aircraft; costs associated 
with the new T6A Texan II military training aircraft; 
and other production costs. 
RAC enters 2001 with some of the most exciting new 
business jets under development, and with an excellent 
balance of products and services. I've watched customers take delivery of our new turboprops and jets. I 
wish you could experience the emotion and pride our 
people feel, and the joy our customers feel; they know 
they're getting something special. 
Penetrating potential high growth markets 
While the defense industry has a modest record of 
successfully commercializing defense technologies, we 
have a number of technologies that appear to be well 
positioned for potential high growth markets. Optical 
communications is an example. Raytheon's ELCAN 
Optical Technologies business is a world leader in its 
field. We have begun production delivery of 100GHz 
DWDM (dense wave division multiplexing) thin film 
filters. DWDM permits larger amounts of data traffic 
using multiple wavelengths of light on fiber optic lines. 


RAYTHEON'S GOALS FOR 2001 
Another example is thermal imaging, where we have 
applied our expertise in this defense technology to 
commercial markets, including automotive safety and 
special cameras used by firefighters. We're also enjoying 
solid growth in our gallium arsenide semiconductor 
business for wireless communications. 
We see an additional opportunity to penetrate commercial markets through our new ventures subsidiary, 
called Raytheon Commercial Ventures Inc., which was 
announced in early 2001. It will identify and seize 
opportunities to commercialize defense technologies. 
Multiple channels to the global marketplace 
We are reaching out in several ways to serve the global 
marketplace. For example, Raytheon and Thales (formerly Thomson-CSF) announced the formation of a 
trans-Atlantic joint venture in 2000 that encompasses 
air defense/command and control centers and ground 
based air surveillance and weapons locating radars. 
The transaction, which is subject to U.S. and French 
government approval, is a pioneering initiative in 
trans-Atlantic alliance building that may serve as a 
model for the future. 
We also serve the global market through Raytheon 
Systems Limited, our UK-based subsidiary, which has a 
major presence in the UK and a history of technology 
leadership in areas such as Identification Friend-or-Foe 
systems. In addition, we have a global network of 
business development offices, called Raytheon Inter 
national, Inc., to give us a presence on the ground in 
strategic regions. 
Focusing on core businesses 
We have focused our portfolio and generated funds to 
reduce debt by divesting noncore businesses. In 2000, 
the company sold Raytheon Engineers & Constructors 
(RE&C) to Washington Group International, Inc. 
(WGI), formerly known as Morrison Knudsen, while 
retaining certain letters of credit, performance bonds, 
and parent guarantees outstanding at the time of sale. 
Issues recently surfaced with WGI related to the RE&C 
sale. We are working to resolve these issues. 
We have also sold other noncore businesses. We sold 
our flight simulation business, and we sold a business 
focused on space based optical systems. In January 
2001, we sold the recreational products division of 
Raytheon Marine Company. 
Striving to be the employer of choice 
We must provide our employees with an environment 
in which to grow, and I'm pleased to report that we 
have made considerable progress in this area. The 
human resources review process is now firmly embedded in all of our operations. It is enormously helpful in 
cascading our HR tools for recruiting and retaining talent, developing leaders and teams, succession planning 
at all levels, and providing performance feedback and 
career opportunities for our employees. There's also a 
heightened commitment to leadership development 
and professional training via the Raytheon Learning 
Institutes. 
We believe that there is no more exciting place to be an 
engineer than at Raytheon. But we know that we must 
compete to attract and retain engineering talent, and 
we're doing so. We have developed a series of initiatives 
specifically designed to support our tens of thousands 
of engineers, including an added focus on engineering 
leadership development and implementation of mentor 
training. We will go online to connect those in our 
engineering community who want to teach with those 
who are anxious to learn. Many of our engineering 
achievements are highlighted throughout this Annual 
Report. 
Nurturing a diverse, inclusive culture 
Raytheon wants to provide a work environment that 
is challenging and that recognizes and rewards achievement. We believe strongly in having a diverse, inclusive 

Enhance our customers' success, especially through technology leadership 
and program execution. 
Reduce debt, using Raytheon Six 
Sigma to lower inventories, receivables, and contracts in process. 
Make progress as an employer of 
choice, with emphasis on creating a 
work environment that encourages 
learning and professional development. 

culture. As a technology company, we believe that great 
ideas thrive in an atmosphere energized by different 
perspectives. 
Our company and our employees are honored to have 
received recognition in various venues. Most recently, 
on February 23, 2001, the Society of Hispanic Professional Engineers (SHPE) honored Raytheon as Corp 
oration of the Year for demonstrating outstanding 
support for SHPE and its programs. 
We want to encourage new thinking, to create a daring, 
exciting environment, and we want to maintain our 
companywide commitment to integrity: being honest 
and conducting our business ethically. 
Raytheon's goals for 2001 
To achieve a high rate of progress, we need to work 
toward goals that are clear and realistic. Here are 
Raytheon's goals for 2001: 
1. Enhance our customers' success, especially through 
technology leadership and program execution. 
We are a technology company. We must strive to 
develop the technologies our customers need and then 
flawlessly execute programs. We must work seamlessly 
together and with our customers to enable them 
to achieve success. If our customers are successful, 
so are we. 
2. Reduce debt, using Raytheon Six Sigma to lower 
inventories, receivables, and contracts in process. 
We need to lower our debt to have more resources 
available to shape our future. We have an immediate 
opportunity to reduce inventories, receivables, and 
contracts in process. Raytheon Six Sigma tools can help 
us free up resources in these areas by reducing cycle 
times and errors, and connecting us more closely to 
our suppliers. In the months ahead, we will continue 
to focus on working capital management, which lends 
itself to Raytheon Six Sigma solutions. 
3. Make progress as an employer of choice, with emphasis 
on creating a work environment that encourages learning 
and professional development. 
Every employee deserves to be treated with respect 
and dignity, and to have opportunities to grow professionally. In 2001, we will continue our focus on 
diversity and will put increasing emphasis on providing 
employees with opportunities to learn and develop 
professionally. We will offer traditional teaching, but 
increasingly, learning will be derived from challenging 
job assignments and access to Web-based learning. 
The path ahead 
Our goals are set. We will continue to focus on the 
fundamentals; but having successfully focused on the 
fundamentals in 2000, we can also turn our attention 
to achieving strategic growth --- to seize opportunities 
for growth in established markets and new ones, and 
to draw on our capabilities across our businesses. 
I want to thank all of our employees for helping 
Raytheon make real progress in the year 2000 and 
for dedicating yourselves to enhancing our customers' 
success. And I want to thank our investors for your 
confidence in us; we will work our hardest to continue 
to earn it. 
Although there are challenges ahead, I believe that our 
performance in the year 2000 demonstrates we are on 
the right track. Thank you for your continued support. 


