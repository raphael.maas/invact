In 2017, we continued making
progress on our strategic journey
to create a stronger TI and to
deliver sustained, long-term results.
Our revenue grew 12% and free
cash flow expanded to $4.7 billion,
or 31.2% of revenue. We are
investing in the best products –
analog and embedded processing
– and the best markets – industrial
and automotive – while also
strengthening and leveraging our
unique competitive advantages
that allow us to grow, generate
cash and return all free cash flow
to our owners. While our work will
never be done, we are committed
to making TI stronger every day for
our customers, our employees and
our owners.”
Rich Templeton
Chairman, president and CEO
Brian Crutcher
Executive vice president and COO
In January 2018, TI announced that
Mr. Crutcher will become president and
CEO, effective June 1, and Mr. Templeton
will continue as chairman.