Message to shareholders

We made solid progress in 2005 and continued to
position your Company for success in the years ahead.
Our key accomplishments included:
* Increasing our common stock dividend
payment by 14.7 percent;
* Achieving an investment-grade credit rating
from Standard & Poor's for all of our debt
and completing our debt-reduction program;
* Producing record electricity output from our
generating units;
* Enhancing our reliability and customer service; and
Gaining approval for our Rate Certainty Plan in
Ohio and for our generation asset transfer.
These and other accomplishments reflect our
overall strategy, which is focused on reinvesting in our
business and continuous improvement. For example,
we are upgrading our transmission and distribution
system to improve service reliability; implementing
new technologies and industry best practices to provide
more responsive customer service; increasing generating
capacity and maximizing the efficient utilization of our
plants as we prepare for fully deregulated markets in
Ohio and Pennsylvania; and maintaining an unwavering
focus on safety. These actions will help enhance the
long-term value of your investment in FirstEnergy.
Solid Financial Results
Our financial performance in 2005 was strong,
particularly in the key areas of earnings, cash flow
and debt reduction.
We produced four quarters of solid financial
results, ending the year with basic earnings per share
of $3.00 on a non-GAAP* basis, which reached the
top of our 2005 guidance to the financial community
of $2.85 to $3.00 per share. Net cash from operations
increased to $2.2 billion - up from $1.9 billion in
2004. We also reduced our debt-to-capitalization ratio

to approximately 56 percent, bringing this
important metric to within our target range. In
addition, we successfully completed the $4-billion
debt-reduction program we started four years ago
and regained our investment-grade credit rating for
all of our debt.
We delivered a total return to investors -
a measure of stock price appreciation plus reinvested
dividends - of 28.5 percent in 2005. And, our five-year
annualized total return ranks us 7th among the 64
U.S. investor-owned electric utilities that comprise
the Edison Electric Institute's (EEI) index.
The increase in our stock price during 2005 added
more than $3 billion of value to shareholders. Our
performance led your Company to be named to the
Forbes Platinum 400, also known as the list of
"America's Best Big Companies."
Based on the Company's performance, your Board
of Directors increased the common stock dividend
payment by 14.7 percent. The Board also authorized
our subsidiaries to make a voluntary contribution
totaling $500 million to the pension plans in late
2005. While the pension contribution is expected to
be accretive to earnings, it also increases the security
of future plan benefits and represents a major
investment in our employees and retirees.
Operational Improvements
In 2005, we took steps to improve our customer
service and reliability. We launched our Accelerated
Reliability Improvement Program - a five-year,
$600-million effort that involves replacing and
upgrading equipment on our transmission and
distribution systems. These systemwide infrastructure
improvements will enhance overall reliability at our
utility companies well into the future.
In addition to spending about $150 million in
2005 on these types of improvements, we ordered
431 new vehicles - part of a multiyear fleet upgrade
of more than 1,500 new vehicles - to ensure that our
workforce has the equipment needed to get the job
done safely and efficiently.
We're also installing new technologies that will
benefit customer service and system performance. For
example, our engineers developed a new storm-detection
system to help safeguard distribution equipment
during severe weather. The system automatically
switches equipment during storms to protect key
components from lightning and high winds. As a pilot
project, we installed about 40 of these devices throughout
our service area, and expect to move forward with
full-scale implementation this year. These and other
improvements are designed to reduce the frequency
and duration of outages - as well as the number of
customers affected when outages do occur.
Additionally, we completed a major renovation
of our transmission system control center in Ohio,
and we are in the process of rebuilding and
consolidating distribution system control centers.
Our commitment to customer service received
national recognition in February 2006, when we were
named a recipient of EEI's Customer Service Award
for being ranked among the top five electric companies
by the country's leading retail chains.
On the generation front, we continued to optimize
the performance of our plants. We set a total production
record of 80.2 million megawatt-hours (MWH),
surpassing the previous record set in 2004 by nearly
4 million MWH. Our coal-based generation fleet led
the way with a record 49.9 million MWH, and our
nuclear fleet produced 28.7 million MWH.
Our baseload fossil plants achieved a top-decile
capacity factor for the year. And, Unit 2 at our
2,233-megawatt (MW) W. H. Sammis Plant reached
1,017 days of continuous operation, setting a national
record for any single-boiler turbine generating unit.
At our largest coal-based facility, the 2,410-MW
Bruce Mansfield Plant, we initiated a program to
upgrade Unit l's turbine and scrubber system -
boosting net demonstrated capacity by about 50 MW
while reducing emissions. Similar projects are slated
for units 2 and 3 in the future. Together, these projects
will provide enough capacity to increase the plant's
output by up to 900,000 MWH annually.

Our nuclear fleet also made solid progress in
2005. Davis-Besse returned to the standard Nuclear
Regulatory Commission oversight process in July. We
also closed an important chapter on the Davis-Besse
reactor head issue in January 2006, when we entered
into a deferred prosecution agreement with the U.S.
Attorney's Office and the U.S. Department of Justice.
We strengthened our nuclear management team
and enhanced our fleet-management practices - steps
designed to maintain a strong focus on nuclear safety
and to achieve continued operational success.
As a result of our performance, the nuclear fleet
garnered several awards and honors during the year.
For example, Davis-Besse was recognized by the World
Association of Nuclear Operators for achieving the
lowest radiation exposure among all U.S. pressurized
water reactors. And recently, Beaver Valley was awarded
the 2005 World Class ALARA Performance Award by
an international organization that tracks radiological
exposure to employees at nuclear plants (ALARA is
an acronym referring to keeping exposure as low as
reasonably achievable).
For the year, Beaver Valley and Davis-Besse
operated at better than 90-percent capacity factors,
and our entire nuclear fleet averaged a 100-percent
capacity factor during the months of June through
December. Our fleet performance should further
benefit from the completion of a major steam
generator and reactor vessel head replacement
this year at Beaver Valley Unit 1, the most substantial
construction project at this unit since it was built
in the 1970s. We also expect to complete nuclear
plant capacity uprates between 2006 and 2009 that
would add up to 156 MW to our non-emitting
generating capacity.
Safety remains a top priority - both within our
nuclear fleet and across our organization. We continued
our efforts to strengthen the safety culture at all our
nuclear facilities. We also achieved a Companywide
Occupational Safety and Health Administration
(OSHA) rate of 1.23 incidents per 100 employees in
2005, a 17-percent reduction compared with 2004,
when our performance ranked just short of the top
decile in our industry. Our fossil fleet posted an OSHA
rate of 0.96 incidents per 100 employees, a 90-percent
reduction from 2004, and our nuclear fleet recorded
an OSHA rate of 0.41
incidents per 100 employees.
We are especially proud
of our employees at Toledo
Edison, who worked the entire year with only one
incident and achieved an OSHA rate of 0.26, a best-ever
rate for one of our operating companies.
Protecting the Environment
As one of the nation's leading energy companies,
we are committed to help protect the environment
while meeting our customers' need for safe, reliable
electricity. We're proud of the progress we've made in
this key area. In 2005, more than 60 percent of the
electricity we produced came from our non-emitting
nuclear fleet and scrubber-equipped units at our
Mansfield Plant.
In one of our most ambitious projects to further
reduce emissions, we have begun a multiyear installation
of state-of-the-art air quality control systems at our
Sammis Plant. This five-year project will cost approximately
$1.5 billion and should allow for continued
use of this essential asset for many years.
Over the next five years, FirstEnergy also expects
to spend approximately $50 million on efforts to reduce
greenhouse-gas (GHG) emissions, ranging from participation
in the Global Roundtable on Climate Change, to
partnerships with industry and government groups to
develop technologies for GHG reduction, carbon-dioxide
(CO2) capture and storage, and advanced generation.
We're building on our leadership role in testing
and developing environmental technologies. For
example, we plan to install an Electro-Catalytic
Oxidation (ECOO) system, developed through our
partnership with Powerspan Corp., at our Bay Shore
Plant in Oregon, Ohio. ECO, a multipollutant control
technology for coal-based plants, is currently being
demonstrated at our R. E. Burger Plant. Design
engineering on the $125-million Bay Shore ECO
system will begin this summer. Further development
and testing will help determine whether ECO
technology can be used to capture C02.
We plan to seek renewal of our licenses for nuclear
and hydroelectric facilities. And, we have contracted
to acquire additional wind power generation output,
bringing the total we will have available to 360 MW.

All of these strategic investments are designed to
support our environmental programs, and where
practical, increase generating capacity.
We also produced an Air Issues Report to shareholders
that provides a comprehensive assessment
of our past environmental performance as well as
our future risks and mitigation efforts. FirstEnergy is
better positioned than many electricity providers to
operate in a carbon-constrained environment because
of our diverse generation mix. The report is available on
our Web site at www.firstenergycorp.com/environmental.
Building on Our Momentum
We took an important step toward strengthening
our financial stability with the Rate Certainty Plan
(RCP), which essentially maintains current electricity
prices in Ohio through 2008.
The RCP enables us to collect certain fuel cost
increases and to defer for future recovery certain
other fuel and distribution-related expenses during
the plan's term. While keeping electricity prices stable
for our customers, the RCP provides us with more
predictable revenues.
Also during the year, the New Jersey Board of
Public Utilities approved settlement agreements involving
rate filings, which had a positive impact on earnings.
And, we intend to file a comprehensive rate proceeding
during 2006 to address revenue requirements in
Pennsylvania.
We also successfully completed an intra-system
transfer of both nuclear and non-nuclear generation
assets from our Ohio companies and Penn Power
to separate, wholly owned generating company
subsidiaries. This transfer enhances our flexibility
as both Ohio and Pennsylvania move toward the
end of their respective market development periods.
And, it addresses corporate separation provisions
of electric deregulation laws in both states.
Addressing Our Workforce Needs
Over the next several years, we anticipate
hiring thousands of new employees to offset expected
attrition as a significant portion of our workforce
approaches retirement age. To support this process,
we've established hiring goals for each business
unit, expanded recruiting initiatives, and enhanced
programs for introducing new employees to the
Company. We're also focusing on ways to retain
our dedicated, hardworking veterans.
In addition, we've developed a number of programs
designed to help employees better understand our
key strategies and goals. These programs enhance
teamwork and provide employees with opportunities
for personal development and advancement.
Positioned for Success
Your Company built on the achievements of recent
years and delivered on goals established for 2005. With
the ongoing efforts and expertise of our employees and
your continued support, I look forward to achieving
greater success in the years ahead.
Sincerely,
ANTHONY J. ALEXANDER
President and Chief Executive Officer
March 20,2006