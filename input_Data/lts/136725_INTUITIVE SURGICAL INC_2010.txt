Dear Owner,
The past year has been one of change at Intuitive Surgical. Externally, this change is being fueled on multiple
fronts in both domestic and international markets. In the United States, the marketplace for medical products is
experiencing pressure driven by financial constraints in healthcare, slow growth in the broader economy and
changes in overall regulatory practice. In the face of these changes, Intuitive performed well in procedure
growth, system sales and financial performance. In 2010, our focus was in the following areas. We drove the
adoption of da Vinci surgery within the gynecology and urology segments. We enabled and supported the growth
of new procedures within new specialties. We worked on right-sizing our teams within the company to support
our growth, particularly in sales. We strengthened our clinical and commercial teams in Europe and Asia. Lastly,
we made significant investments and progress in research and development to advance the art and science of
robotic surgery. In this letter, I will report on how I think weve done in these focus areas and point to our key
priorities in 2011.
Procedures
Wow, what a difference between the surgery I had 30 years ago when I was unable to stand up straight and
barely able to shuffle around for the first couple of weeks, and the one I just experienced with the da Vinci robot.
compared to other options, it is truly amazing.
Linda, Reno NV
As the quote above from a da Vinci Sacralcolpopexy patient highlights, da Vinci surgery is driven by those
seeking highly efficacious and less invasive resolution to their underlying disease. For those conditions in which
da Vinci surgery creates more value for patients than competing therapeutic approaches, we have seen our
procedures expand. In the United States, procedures that have undergone or are undergoing the greatest adoption
are da Vinci Prostatectomy (dVP) and da Vinci Hysterectomy (dVH). Follow-on procedures, those procedures
that are within the specialties of urology and gynecology but are at earlier stages of adoption are partial
Nephrectomy for kidney cancer in urology, Sacralcolpopexy for pelvic prolapse in gynecology and Myomectomy
for removal of fibroids while preserving fertility, also in gynecology. Our overall da Vinci procedure growth for
the year was approximately 35% over 2009, during a year in which weakness in the global economy pressured
hospital admissions.

Procedures that are showing high quarterly sequential growth on small absolute volume we designate as
emerging procedures. For emerging procedures, it is too early to predict full scale adoptions, but their growth can
be an indicator that patients and surgeons are finding strong patient value with the use of da Vinci. Three
categories displaying these encouraging trends are trans-oral robotic surgery (TORS), da Vinci rectal and low
anterior resections (dV LAR) for cancer indications and non-cardiac thoracic resections for lung cancer. Within
the category of da Vinci trans-oral surgery the published patient value has been reported as very high, with strong
cancer control and functional outcomes (such as speech and swallowing preservation) relative to alternative
approaches. da Vinci rectal resections for cancer and da Vinci Low Anterior Resection are likewise showing
strong early clinical results. These resections are low in the pelvis and difficult to access by other means, aligning
well to our system. Lastly, we are seeing strong early growth in non-cardiac thoracic procedures, typically for
lung cancer indications. All three of these emerging procedures are the result of the continuous pursuit of
efficacious and minimally invasive options for complex open surgical procedures.
Europe experienced solid procedure growth in the year, with procedures growing approximately 48% over 2009.
da Vinci Prostatectomy is the leading procedure in Europe, followed by da Vinci Hysterectomy for malignant
conditions and da Vinci partial Nephrectomy. The stage of adoption of dVP varies by country in Europe, and
much of the growth in our procedures came from increased use of da Vinci in Germany and France.
In Japan, we received Shonin approval in Q4 2009 and we sold 13 systems in 2010. We are building our direct
presence in Japan and are working with our partners to obtain re-imbursement for da Vinci procedures. Until
national re-imbursement is achieved, procedure and system growth in Japan will be slow; however we believe
that the long term opportunity for da Vinci surgery is significant.
Financial Highlights
Our financial performance for the year was the result of increased demand for da Vinci procedures and continued
execution by our operations and commercial teams. In the year, we sold 441 da Vinci Surgical Systems, up from
338 in 2009. Total revenue grew to $1,413 million, up 34% over 2009 in a year in which average medical device
growth was approximately 5%. Recurring revenue on the sale of instruments, accessories and service rose to
$753 million for 2010, up 34% over 2009 and comprising 53% of total revenue. This resulted in an operating

profit of $673 million before non-cash stock compensation expense, up 42% from 2009. We believe that our
financial performance is a lagging indicator of the value created by our research, design and development teams
working with talented and innovative surgeons on procedure and product development.
Products
2010 has been an investment year in products, with several products in late to mid stages of development. Our
investments focus in four areas; (1) instrumentation that can enable new procedures or optimize existing ones;
(2) imaging technologies that increase visual acuity and provide additional information to the surgeon;
(3) simulation, networking and training technologies that facilitate learning; and (4) robotic technologies that
enable new procedures or interact with the body in new ways.
We received the CE mark for our da Vinci Single-Site instrument kit in February 2011. Single-Site is a set of
instruments and accessories that allow da Vinci Si systems to work through a single incision rather than three
incisions. Single incision surgery is intended to minimize trauma to patients by reducing the number of ports
required to enter the body. Single site surgery today is typically performed with modified laparoscopic
instruments. Early clinical results with manual instruments have been encouraging, however, manual single
incision surgery is challenging due to a difficult user interface and the requirement to manage collisions in a
small space near the patient. Single-Site instruments and accessories are designed to avoid collisions at the
patient while the surgeon operates them from the surgeons console with Intuitive control. We continue to work
with FDA on clearance for Single-site in the US.
We received FDA clearance in February 2011 for our da Vinci fluorescence imaging system and are in the initial
stages of launch for this product. This system allows surgeons to image vasculature in three dimensions beneath
tissue surfaces to assess blood flow and other conditions that cannot be seen with direct imaging. The system
integrates into the da Vinci system seamlessly and uses a da Vinci endoscope, console and display electronics to
present the image in real time. We believe da Vinci fluorescence imaging will enable surgeons to work with more
confidence and precision in several procedures including partial Nephrectomy and rectal resections.
In December 2010, we launched our da Vinci Si Skills Simulator. This simulator works with any da Vinci Si
console and allows surgeons to learn and practice the use of da Vinci and to track their progress. In this first
release of the simulator, exercises are focused on the basic use of the system and its features. Combined with the
Si dual console launched in 2009, the simulator enriches the da Vinci training experience. In networking for da
Vinci systems, we connected approximately 1000 systems within our installed base to form a da Vinci network.
This network allows us to monitor the status and health of da Vinci systems and provide our customers with
predictive maintenance and accurate diagnostics of system performance. In addition, we have obtained FDA
clearance for remote proctoring on our da Vinci S systems and are working on expanding this clearance to our da
Vinci Si product. Remote proctoring allows an experienced surgeon to remotely tap into a da Vinci case in
progress using the internet, view video in real time and exchange information in video and voice with the
operating surgeon. Surgical training is a demanding task for any new surgical technique. Overall, our teams are
investing in and making substantial progress developing new methods for training and supporting training with
new technologies.
We have a vibrant innovation culture and our patent portfolio now stands at approximately 850 owned and
exclusively licensed patents relating to technologies important for robotic surgery with approximately 950
additional patent applications. Furthermore, we have partnered with several companies to co-develop
technologies that we believe will improve patient outcomes and surgeon experiences in surgery, from stapling
technology, to tissue interaction and energy-based ligation instruments to imaging technologies.
Our product development activities will be intense for 2011, 2012 and 2013 and we are focusing on outstanding
execution, responding to changes in the regulatory environment and disciplined product launches to the field as
we bring to market several products that will increase surgical performance using our system.

Organization
Intuitive has grown procedures by approximately 220% from 2008 to 2010, annual system shipments grew by
83% while Endo-wrist instruments production grew by approximately 150% over this same period. We now ship
to 43 countries in 25 languages. In the same period, headcount has grown by 117%. We believe that the optimal
size for an Intuitive team is the minimum number required to get the job done and right sizing our teams during
the rapid growth of the past several years has been one of our major challenges. In 2010, our organizational
emphasis was on balancing our commercial teams worldwide. We will continue to build our teams in 2011,
though the rate of growth in headcount will moderate.
In 2011, our organizational emphasis will be in depth and execution in our product development organization, in
balancing our commercial organization in the US to manage multiple procedure adoptions effectively and in
investing in growth and depth in our international teams. We are the beneficiaries of the effort of an outstanding
team of remarkably talented, passionate and dedicated individuals and we seek to maintain our high performance
and vibrant culture as we meet the challenge of growth.
Conclusion
It is not the strongest of the species that survive, nor the most intelligent, but the one most responsive to change.
Author unknown
The marketplace for medical products is experiencing significant pressure to adapt to new healthcare financial
constraints, broad economic pressure and changes in regulatory practices. We are focused on bringing products to
market that significantly improve patient value, are dependable and offer attractive financial returns to healthcare
systems and payers. In closing, we appreciate your support in helping Intuitive improve surgery and I assure you
that our team is focused on working on those things that truly make a difference.
Gary GuthartCEO