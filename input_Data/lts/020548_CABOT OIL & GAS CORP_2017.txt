Fellow Shareholders:
Coming out of 2016, a year that saw the worst natural gas price realizations
in our public company history, we set our sights on an improved outlook
for 2017 with a focus on returning to profitability and executing on a program
that provided returns-focused growth, a continued improvement in our cost
structure, and a further enhancement of our strong balance sheet. I am proud
to say that we delivered on all of these fronts and much more in 2017. 

Where we stand:
� Operationally, Cabot grew its daily production
by 10 percent year-over-year to a record 1,878
Mmcfe per day and increased its total proved
reserves 13 percent year-over-year to a record
9.7 Tcfe.
� On the cost side, our all-sources finding costs
from the 2017 investment program were $0.35
per Mcfe, a five percent improvement compared
to 2016. Additionally, our all-in operating
expenses for the year averaged $2.02 per Mcfe,
a seven percent improvement relative to 2016.
Our continued commitment to cost control was
further evident during the year by the Company
underspending its capital budget guidance by
three percent.
� Financially, the Company benefited from increased
production volumes, improved realized commodity
prices, and lower costs, resulting in net income of
$100 million for the year compared to a net loss of
$417 million in 2016. We also generated positive
free cash flow for the second consecutive year.
� The Company used its free cash flow to
increase its dividend 150 percent and to
repurchase over five million shares of its own
stock. The combination of dividends and share
purchases in 2017 resulted in Cabot returning
over $200 million of capital to shareholders.
� On the strategic front, the Company executed
on a non-core divestiture program that included
its Eagle Ford Shale and Haynesville Shale
properties as well as its legacy conventional
positions in West Virginia and South Texas.
Cabot anticipates these transactions will
provide over $800 million of proceeds (net
of customary closing adjustments), further
bolstering its top-tier financial position. By
high-grading the Company�s portfolio through
the divestiture of these higher-cost, lower-return
assets, Cabot is now well-positioned to deliver
industry-leading returns on capital employed
throughout the natural gas price cycle.
� Lastly, we were pleased to see significant
construction progress made on the infrastructure
projects that will allow Cabot to generate peerleading growth over the coming years � the
Atlantic Sunrise pipeline, the Lackawanna Energy
Center power plant, and the Moxie Freedom
power plant. Cumulatively, these projects will
enable Cabot to access approximately 1.4 Bcf
per day of capacity to new markets beginning
mid-year 2018.
Cabot�s operational execution, financial results,
and strategic initiatives were rewarded in 2017 as
the Company recorded the highest total shareholder
return in our peer group and outperformed the
S&P 500 index. While 2017 was an outstanding year
for Cabot Oil & Gas on all measures, we believe the
best is still yet to come. 

Where we are going:

As a result of our strategic transformation in 2017,
Cabot enters 2018 as a pure-play Marcellus Shale
company focused on our low-cost, high-return
position in Susquehanna County, Pennsylvania. Our
engagement with shareholders coming out of the
recent downturn highlighted the continued frustration
investors had with the destruction of capital across
the exploration and production sector as many
industry participants continually chased uneconomic
growth at any cost while diluting shareholders and
overleveraging balance sheets. While Cabot�s focus
during my 16 years at the helm has always been
on providing returns-focused growth and balancing
capital spending with operating cash flow and
proceeds from asset sales, I am pleased to say that
as a result of our team�s recent efforts, our company
is now better positioned than ever to execute on
our strategy for creating long-term shareholder
value by delivering debt-adjusted per share growth;
generating positive free cash flow; improving 

corporate returns on capital employed; increasing return of capital to shareholders; and maintaining a
strong balance sheet.
Our historical philosophy of disciplined capital allocation remains unchanged in 2018 as we have
designed a program that generates double-digit growth per debt-adjusted share, positive free cash flow,
and a double-digit return on capital employed even at natural gas price realizations significantly below
the current market outlook. Our confidence in our team�s ability to execute on this program resulted in
the decision to increase our dividend by 20 percent in January 2018�our second dividend increase in
eight months�and increase our share repurchase program authorization to 30 million shares, highlighting
our commitment to increase our return of capital to shareholders. We believe this sets us apart from
the majority of the industry and highlights the compelling investment case for Cabot Oil & Gas. While
we remain optimistic that the robust level of natural gas demand growth that is anticipated through the
balance of the decade will result in higher natural gas prices than today, we have positioned the Company
to continue to provide top-tier results even at the lows of the natural gas price cycle.
We have also provided a three-year plan through 2020 that delivers a production compound annual
growth rate of 17 to 21 percent (or 20 to 24 percent on a divestiture-adjusted basis). Based on a range
of NYMEX prices of $2.75 to $3.25 per Mmbtu during this three-year period, the Company expects to
generate between $1.6 and $2.5 billion of after-tax cumulative free cash flow while delivering significant
growth in net income, discretionary cash flow, and return on capital employed. Our conviction in this 

plan is driven by the continued improvements we have seen from our best-in-class resource base in
the Marcellus Shale, our team�s operational excellence, and the increased likelihood of the successful
completion of several infrastructure projects that are expected to be placed in-service this year. By the
end of the decade, our expectation is that Cabot Oil & Gas will be one of the top two producers of natural
gas in the United States�an impressive feat for a company that only has approximately 300 upstream
employees currently.
Separately, we will use a modest portion of our asset sale proceeds as seed capital for our testing of two
exploration ideas in 2018. While it is still too early to make any comments regarding these exploratory
areas, I will reiterate that we do not plan to allocate incremental capital to these projects long-term unless
they can compete for capital based on full-cycle rates of return, provide meaningful inventory depth and
resource life, and have the ability to be self-funding in today�s commodity price environment.
I would like to thank all of our employees for their continued dedication to our company. With the
significant level of changes to our portfolio in 2017, including a complete enterprise resource planning
system implementation, they have remained steadfastly committed to helping us execute on our strategy.
I would also like to thank our Board of Directors for their insight and service to the Company and its
shareholders. Lastly, I would like to thank our shareholders for their continued support. We believe
the combination of debt-adjusted per share growth, free cash flow and corporate returns that we expect
to deliver over the coming years positions Cabot Oil & Gas to continue to enhance long-term value for
our shareholders.
Sincerely,
Dan O. Dinges
Chairman, President and Chief Executive Officer
 