I am pleased to report that 2006 was another
record year for sales and earnings.We strengthened
our leadership position in Life Science and High
Technology, a key to achieving sustainable longterm
growth. Our 7,300 dedicated employees operating
in 35 countries continued to accelerate
customers success through innovative products,
customized solutions and unsurpassed service. With
our comprehensive product offering, including
100,000 chemicals and 30,000 laboratory equipment
items, Sigma-Aldrich plays a significant role in
enabling Life Science and High Technology research
and manufacturing globally. Backed by our capabilities
that give us this leadership position in Life
Science and High Technology, we remain focused
on helping researchers find solutions that will
improve the quality of life worldwide. Our products
support a wide array of applications, ranging from
the early detection and faster treatment of debilitating
diseases to understanding how genes function,
enabling scientists to develop more effective
treatments for these diseases.
A Record-Setting Year
Growth in sales across each of our business units
and geographic regions again resulted in strong top
and bottom line performance in 2006. This extended
our record of consecutive years of continuous
growth in sales and
earnings per share (EPS)
for Sigma-Aldrichs
chemical business to
32 years. Net sales
increased 7.9% to
$1.8 billion. Each of our
four customer-centric
business units (% of
sales) contributed to
sales growth, with
SAFC (28%) leading
the group with a reported
gain of 13.4%,
followed by Research
Specialties (37%) at
6.9%, Research Biotech
(15%) at 5.6% and
Research Essentials (20%) at 4.2%. Each geographic
region (% of sales) posted sales growth,
led by strong double-digit growth in developing
international markets. Sales in CAPLA (Canada,
Asia Pacific, Latin America) countries (18%) continued
to grow fastest at 13.9%, with Europe (42%)
and the U.S. (40%), achieving growth rates of
8.2% and 4.9%, respectively. These sales growth
rates reflect
Sigma-Aldrichs continued
ability to gain share
in all markets.
Our earnings performance
also set another
record, with net income
of $276.8 million and
diluted EPS of $2.05,
reflecting 7.2% and
9.0% increases over
2005, respectively. A
Return on Equity (ROE)
of 20.9% again exceeded
our long-term goal of
20%.We also maintained
a strong overall
financial position with a
debt-to-capital ratio of 27.2% at December 31,
2006 and a 17.8% increase in cash from
operations, adding $75 million to our cash position
during 2006.We remain one of the most
profitable companies in the chemical industry with
the financial resources to invest for future growth.
Foundation for Sustainable
Growth
At the beginning of 2006, we formally launched
a new strategic plan with five key initiatives
focused on accelerating customers success and
driving top and bottom line growth. It is, therefore,
appropriate to review our results on these
five initiatives:
1. Align Organizational Structure with
Customers and Markets: To accelerate customers
success, we aligned our organizational
structure around four distinct customer groups
in mid-2005. The migration to a four-business
unit structure with a common account management
team has strengthened our relationships
with large global accounts. It also enabled us to
create customized solutions to address each
customers specific needs. Examples of some of
the customized solutions such as Compound
Management Services (CMS) and Custom
Sourcing Services (CSS) are
included in the reviews of
the plans five key initiatives
in the following
pages. During the year, we
were recognized
by
two of the
leading
life
science
companies
for our superior service, receiving Abbotts
Supplier Excellence award and U.S. Surgicals
Recognized for Excellence award. In February
2007, we also received Mercks 2006
Sector Operational Award.
Our customer success-driven approach proved
to be good for business, as sales to the top 25
global accounts grew at a rate of 14% in
2006, exceeding our overall organic growth by
more than a factor of two.
2. Expand in Faster-Growing Geographic
Markets: Sales in CAPLA markets in 2006
grew at nearly
three times the
rate in the U.S.
and more than 1.5
times the rate in
Europe. This outstanding
sales performance
came
from implementing
new initiatives,
which included increasing inventory levels at
our international warehouses, developing customized
solutions to meet our customers
needs and expanding our presence in China
and India. Our increased focus in China and
India resulted in growth rates of over 40%. To
increase our presence in China, we acquired
Beijing Superior, our largest dealer there,
increasing our employee count ten-fold, and
expanded our warehouse in Shanghai. In
Bangalore, India, we relocated to a new 10-
acre campus that includes an expanded warehouse
and state-of-the-art laboratories.We
are also pleased with our growing presence in
other key emerging markets such as Brazil,
where our 2006 sales grew in excess of 35%.
In South America, we significantly strengthened
our management team to enhance our
capabilities as we seek to achieve accelerated
long-term growth in that region.
3. Build on Internet Superiority: The number of
customers reaching us through e-commerce
channels continued to accelerate last year.
Over 660,000 registered users visited our
award-winning Web site, with an average of
1.5 million visits each month. In the second
half of 2006, sales through e-commerce
channels reached an all-time high of 36% of
total research sales. In
industry surveys, customers
once again rated our Web
site as a preferred channel
for obtaining technical
information and transacting
business. In the middle of
last year, we launched a
major initiative to enhance
our e-commerce platform
that is expected to be available in early 2007
to further enhance the functionality, speed and
content of our existing platform.
4. Leverage Process Improvements for
Excellent Reinvestment Returns: Our goal is
to grow the business profitably by
continuously improving service and internal
efficiencies. Process improvement initiatives,
using the time-tested Plan, Do, Study, Act
(PDSA) methodology, are now part of
Sigma-Aldrichs DNA. Last years efforts, with
dozens of improvement initiatives at our 35
worldwide locations  several of which were
achieved jointly with customers  resulted in
savings of $20 million. These improvements
enabled us to continue to invest for our
future. To build on our process improvement
achievements and provide new sales growth
opportunities, weve now launched a
team to extend our process improvement
expertise to optimize our global supply chain.
5. Fill Growth Targets through Acquisitions: We
continue to believe that strategic and bolt-on
acquisitions are important to help propel our
future growth. In addition to acquiring our dealer
in China to bolster CAPLA sales, we made three
other acquisitions during 2006, each for a very
specific reason: a plant in Arklow, Ireland,
acquired from Honeywell increases SAFCs largescale
cGMP manufacturing capacity; Pharmorphix
in Cambridge, UK, adds proprietary technology in
solid-form chemistry services; and Advanced
Separation Technologies in Whippany, New
Jersey, provides proprietary technology to produce
chiral chromatography columns. In February
2007, we added $40 million in revenue to our
SAFC Hitech capabilities with the acquisition of
Epichem, Ltd., a UK-based supplier to the fast
growing semi-conductor and electronics industries.
Enabling Science Globally
through Innovation
Sigma-Aldrichs core strengths include competencies
that matter most to our customers: a broad
and innovative product offering, global presence
to support their research and manufacturing
needs, and service that leads our industry.
In 2006, we added more than 4,000 new products
to our already extensive product portfolio,
many of these from our in-house R&D efforts. In
our ongoing commitment to be the global leader in
offering innovative products for genomic research,
we introduced several new products used to enable
breakthrough research in drug discovery and development.
These include the MISSION TRC shRNA
libraries for gene silencing and the GenomePlex
Single Cell Whole Genome Amplification kit.We
also enhanced our product portfolio with 110 new
licensing, partnership or equity investments with
leading researchers in academia and industry.
Sharing Success with
Our Shareholders
In 2006, as in prior years, we converted our earnings
growth into strong cash flow, much of which
was effectively reinvested into the business in a
disciplined manner. Last year, $95 million was reinvested
in capital expenditures and acquisitions,
and $194 million was returned to shareholders
through a combination of dividends and share
repurchases. At year end, we also announced a
two-for-one stock split in the form of a 100%
stock dividend to our stockholders. Our cash flow,
capital structure and borrowing capacity provide us
with resources at a reasonable cost to continue to
invest for our growth.
Operational Excellence
Progress in implementing the initiatives was supported
by many other accomplishments, which
are also expected to contribute to our future
growth. Key highlights in 2006 include:
 Operating our plants without sacrificing reliability
or the safety of our employees. Our
recordable injury rate  a key measure of
safety  reached an all-time low of 1.03
incidents per 100 employees;
 Increasing the utilization for our Companywide
SAP information system to 80% of our
major worldwide locations;
 Redesigning our Environmental, Health and
Safety approach to create a competitive
advantage and ensure compliance with
changing regulations in the U.S., Europe and
Asia Pacific; and
 Investing in new technology to enhance our
online product catalog, giving customers a
better chance of finding what they are looking
for, while also providing a more customized
search experience.
The quality and commitment of employees represent
our most significant source of competitive
advantage. During the year, we stepped up our
efforts to recruit, develop and retain the most
talented and dedicated work force so that we
are well positioned to execute our strategies.We
added two new corporate officers to strengthen
our management team. Rich Keffer joined us as
Vice President, General Counsel and Secretary
and Carl Turza as Chief Information Officer.
It is also my pleasure to welcome Steven Paul,
Executive Vice President for Science and
Technology at Eli Lilly and Company, to our Board
of Directors. Steves experience brings new perspective
to our already strong group of Directors.
We have a healthy company with dedicated
employees poised to accelerate our success in the
global markets we serve. With that energy and
drive to implement each of our strategic plan initiatives,
we look to our future with confidence.
On behalf of our leadership team and all of our
employees, I thank you for your continuing support
and confidence in our Company.

Jai Nagarkatti
President and Chief Executive Officer