                             


To Our Shareholders,
I am very pleased to report that 2006 was a strong year for the company. We posted
record revenues of $2.8 billion, along with a 13 percent increase in our operating income.
We generated positive free cash flow for the ninth consecutive year, ending 2006 with a
five-year high of $257 million in free cash flow. And, we delivered record earnings of
$1.17 per diluted share, up 36 percent from the previous year's $0.86 per diluted share.
   Convergys made these gains through our commitment to a unique brand promise--
"Outthinking. Outdoing." By leveraging our specialized knowledge and superior execution,
we delivered differentiated value to our clients. This in turn drove solid results for
shareholders, as the value of our shares increased 50 percent in 2006.
    Contributing also to our strong performance were the things we have done, and are
doing, to transform Convergys into a higher performing company. We continue to
streamline the organization, take costs out of the business to improve our cost structure,
and strengthen our talent management to develop the leaders we need to ensure the
future success of the business. Our ongoing integration of a disciplined, metrics-based
management approach into our culture is helping us better assess business conditions and
market trends, and better align people and budget resources across our business segments.
    During 2006, we continued to make progress across our three business segments:
customer care, information management, and employee care.
    In Customer Care, we grew our business and improved our operating income in this
opportunity-rich market. Our Customer Care revenues for 2006 increased 10 percent, to
$1.8 billion, while we improved our operating income (OI) by 31 percent and ended the
year with a healthy 11.2 percent OI margin.
   We grew our outsourcing business across many vertical markets, including
Communications (up 8 percent), Financial Services (up 6 percent), Technology (up 13
percent) and "Other," including Healthcare and Automotive, (up 17 percent). To help meet
growing client demand for outsourced agent support with our U.S. clients, we added
contact center capacity and launched a new U.S. home agent program.
    To address the in-house contact center market, we introduced a new diagnostic
approach to assessing contact center productivity (the "Contact Center Health Check").
The overwhelming majority of customer care spend is spent by clients on in-house
operations. Our ability to penetrate those operations and realize revenue and profit from
them will increasingly be driven by our ability to consult and help companies run their
internal operations more efficiently and effectively.
    In this large and growing market, our capabilities, strong client relationships, and global
presence will help us further improve our revenue and OI performance.
   In Information Management, revenues were flat for the year because of declining
data processing revenue from the Cingular migration. Through strong growth in our license
and professional services revenue, we offset the anticipated impact of this client migration.
    Information Management's international operations contributed significantly to our year-
end results by successfully addressing the demand for convergent services around the world
with InfinysTM, our convergent billing solution, and our professional services expertise. In
2006, we announced 17 contracts and implementations in global markets with wireline,
wireless, cable, and satellite operators.
    We also continued to invest in building innovative products and services that leverage
our combined billing and care capabilities. In 2006, we introduced our Infinys Customer
Service Management (CSM) software to help communications providers better integrate
their front and back office operations, thereby improving the response time of agents
handling inquiries on complex communications services. Initially developed for the
communications industry, CSM will ultimately be applied to improve the efficiency of
contact center agents across multiple business sectors and markets.
   We continue to focus on ways to drive Information Management towards higher revenue
and profitability, and expect continued license and services growth outside North America.
    In Employee Care, we increased revenue for the full year by 30 percent, and cut our
operating loss by 24 percent. Our metrics-based management approach, combined with
strong project management, helped us manage multiple, large-scale, and complex client
implementations simultaneously throughout the year. We are committed to growing this
business and quickly moving it to profitability.
   While continuing to make progress with our numerous global implementations, we did
experience some delays in 2006 due to the size, scope, and complexity of these implementations.
    Our Employee Care sales organization is focused on selectively building our sales
pipeline by securing additional new profitable clients.
   We continue to believe in the potential of this business, and are confident that Employee
Care will be an important contributor to the company's profitable growth long term.
   As we look ahead to 2007, I am confident in our prospects in the large and growing
markets we serve.
   In 2006, the company performed well by leveraging our unique combination of core
competencies that will continue to be a mainstay of our business strategy in 2007:
   People. Our proven strength in recruiting, training, equipping, deploying and effectively
   managing very large groups of people with diverse skills on a global basis will enable us to
   effectively leverage our talent to quickly solve client problems and deliver what we promise.
   Process. By applying our operating expertise in cost-effective service delivery, we
   will deliver exceptional value to our clients and reduce the cost of managing their
   customers or employees.
   Technology. Through the development and delivery of scalable transaction and
   interaction applications, we will drive innovation and enable our customers to
   continually improve their customer or employee management experience.
    By effectively leveraging these core competencies, we can continue to deliver exceptional
value to our clients, improve our profitable growth, and sustain our earnings growth.
    As we recently announced, I have decided to retire at the end of 2007. I will have
spent the past 19 years, with the last 9 years as CEO, working with many others to build
Convergys into the company it is today. Critical to the timing of my decision was a
thorough succession process which ensures that the company has the leadership it needs
to succeed and prosper in the future. Our business is strong and, following a year of record
revenues and earnings, we are well positioned for future growth. Our balance sheet is solid,
and we have a management team with the talent, experience, and dedication to drive the
company to even higher levels of success and to deliver improved returns to shareholders
over the long term. I have great confidence in our company, our management team, and
especially in Dave Dougherty, who will soon become the company's Chief Executive Officer.
   I would like to thank our employees for their hard work and commitment to our vision
and strategy, our Board of Directors for their guidance and support, and our clients and
shareholders for their continued trust in us.




James F. Orr
Chairman and Chief Executive Officer
