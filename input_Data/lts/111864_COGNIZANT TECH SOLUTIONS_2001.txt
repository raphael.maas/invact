TO            OUR                  STAKEHOLDERS:




Cognizant Technology Solutions has built its reputation by providing superior IT services and solutions       
                                                                                                               
that give clients a competitive edge in running their business. In 2001, more companies than ever before
                                                                                                                                                          $177,778
responded to our reputation for quality and our Company's compelling offshore value proposition, resulting
                                                                                                                                             $137,031
in another record-breaking year for Cognizant. No longer merely a side option for pioneering CIOs,
                                                                                                                                 $88,904
offshore IT outsourcing moved into the mainstream, becoming a core strategy for large corporations during                  $58,606
2001. In addition, high quality offshore services became even more appealing, due to the heightened                  $24,744
                                                                                                               1997 1998
focus on the bottom line. As a result, many Blue Chip companies set out to engage long-term strategic                       1999    2000       2001
partners for their large-scale offshore outsourcing needs. Cognizant emerged as one of only a handful of
vendors able to handle these large IT deployments.
                                                                                                               
                                                                                                               
Cognizant is asserting leadership in offshore IT services by continually raising the bar for quality, speed,                                              $35,620
value, and range and depth of expertise. Moreover, we are the only company in our industry that helps its
                                                                                                                                             $26,128
clients achieve real business change, by offering complete business solutions that substantially lower cost
                                                                                                                                   $16,645
structure and improve business performance. These strengths, combined with our state-of-the-art infra-
                                                                                                                         $8,918
                                                                                                                   $2,129
structure, human and financial resources, and strategic alliances, position us to not only reap the rewards
                                                                                                               1997 1998
                                                                                                                          1999 2000
from the growing mainstream acceptance of offshore, but also to act as a catalyst for that acceptance.                                         2001



Asserting Our Strength
                                                                                                              
Our financial results for 2001 reflect the success of a company that has asserted leadership in a fast-
                                                                                                              
growing industry. We have achieved this success not only by expanding our client roster, but also by                                                      $98,792

deepening our relationships with existing clients, which generates significant recurring revenue and
                                                                                                                                             $66,116
provides us with excellent revenue visibility. Revenue for 2001 increased 30 percent to a record $177.8
                                                                                                                                  $45,461
million from $137.0 million for 2000. About 80 percent of our revenue comes from long-term clients who                     $32,616
                                                                                                                   $3,419
have been with Cognizant at least one year. Pro forma net income, which excludes the effect of a one-
                                                                                                               1997 1998
                                                                                                                          1999
time, non-cash, after-tax charge, increased 32 percent to $23.4 million, or $1.15 per diluted share for                             2000       2001

2001, from $17.7 million, or $0.87 per diluted share for the prior year. The one-time charge related to an
impairment loss of a venture capital investment. Net income for 2001, inclusive of the one-time charge,
                                                                                                               
rose to a record $22.2 million, or $1.09 per diluted share, from $17.7 million, or $0.87 per diluted share
                                                                                                                                                          3,926
for 2000. In addition, we continued to improve margins in 2001 by maintaining our focus on winning
                                                                                                                                             3,164
larger contracts and selling high-end services to our newer clients.
                                                                                                                                   2,220
                                                                                                                           1,560
Our strong balance sheet remains a testament to our philosophy of responsible financial management.                 

No debt, significant cash flow and approximately $85 million in cash as of December 31, 2001, mean that        
                                                                                                                           
Cognizant has the financial freedom to expand its operations to accommodate future growth, without
compromising our Company's underlying financial strength.







                                        There are a number of factors fueling our growth.        expertise to companies in such areas as financial
  Typical Client Partnerships
                                        During 2001, Cognizant significantly increased the       services, insurance, banking, media, information
                                        size and overall quality of its active customer base     services, healthcare, retail, manufacturing and
                                         about 70 percent of our revenue now comes from         telecommunications. This diversification proved
 Customer since 2001
                                        Fortune 1000 companies. In 2001, we competed for         extremely valuable during 2001, because it enabled
 We provide Coors with e-business
  solutions.                            more than 30 long-term IT outsourcing partnership        us to more than offset the weakness of certain
 Coors uses Cognizant to help
                                        opportunities with Fortune 1000 corporations, and        industries with the strength of others. We are
  reduce costs while increasing
                                        we were selected as a strategic partner for 58 percent   committed to adding new verticals in other IT-reliant
  quality  key components of
  delivering a great product at
                                        of these deals. This is a particularly impressive win    industries and increasing our penetration in verticals
  the right price.
                                        rate, since these deals typically involve the leading    in which we have already achieved critical mass. By
                                        offshore competitors as well as leading domestic         enhancing our technology strength during 2001, we
                                        companies with offshore capabilities. The strong         increased the range and depth of capabilities that
 Customer since 2000                   market for application management and a high win         can be applied across all of our vertical markets to
 Cognizant has worked with
                                        rate drove our results  revenue from application        help our clients achieve stronger top-line and bottom-
  RadioShack on many projects,
  including its award-winning           management increased 43 percent for the year.            line results. This approach makes Cognizant not just
  "Forcefeed" inventory-management
                                        Moreover, since many of these new strategic part-        valuable, but invaluable as a strategic technical and
  application.
                                        nerships are with large customers and are in their       business solutions partner with the experience,
 The 7,200-store chain relies on
  Cognizant to control IT support       early stages, we expect continued growth from them       capabilities and intellectual resources to take on
  costs and support development
                                        in the future.                                           large, end-to-end projects.
  of supply chain initiatives.
 Cognizant helps RadioShack use
  IT in new and innovative ways to
                                        Our ability to quickly ramp up our staff, which          The success of our strategy is evident not only from
  help serve customers better.
                                        we demonstrated during 2001 with a 25 percent            our financial results, but also from the amount of
                                        increase in our headcount, and our reputation for        independent recognition we received during 2001.
                                        delivering quality, value and speed elevated our         Cognizant topped a Forrester Research report as a
                                        standing in the industry. We recently won contracts      "Pivotal Offshore Provider" and ranked in the top
 Customer since 1997
 First Data is a leader in
                                        from such prestigious names as MetLife, General          five on a list of "Smart 100" companies by IT-
  electronic payments.
                                                                                                 services magazine SmartPartner. Moreover, four
                                        Motors, RadioShack, UnitedHealth Group, Providian
 The services we provide ensure that
  they are cost efficient in a very     Financial Corp., and several other industry leaders.     of our clients were nominated for the prestigious
  competitive industry.
                                        In addition, we have continued to expand our             Computerworld and Smithsonian Honors Program for
                                        relationship with many of our established customers.     the work we did with them. This program recognizes
                                                                                                 leaders of the information technology revolution.
                                        Our growth strategy is multi-faceted, concentrating      The attributes that earned us this recognition also
                                        not only on increasing the number of clients            helped us move up two slots in 2001 to number six
                                        especially in our key vertical markets, but also on      on India's National Association of Software Service
                                        enhancing the quality of those relationships by          Companies' list of largest software-services
                                        building up our strength in industry-specific            exporters.
                                        solutions and technology competencies. Our vertical
                                        strategy enables us to provide industry-specific
                                                                                                                 



Redefining the Value Proposition
Cognizant was able to make such meaningful
progress during 2001, despite the difficult global
economic environment, because our compelling
value proposition addresses every aspect of the
value equation for application management, devel-
opment and integration work. With Cognizant's
onsite/offshore model, projects are completed by
                                                         move more work offshore faster than our competi-
tightly integrated teams  30 percent of which work
                                                         tors, which results in greater savings for customers.
at the client site and 70 percent of which work at
                                                         The quality of our services combined with the
our state-of-the-art development centers in India.
                                                         benefits of low cost, better speed to market and
Our onsite/offshore model allows clients to save
                                                         domain expertise gives Cognizant the power to
money and complete IT projects faster because of
                                                         develop its relationships with application manage-
the 24x7 "follow the sun" work cycle. Cognizant
                                                         ment clients to include our innovative development
delivers the highest quality software services, and
                                                         and integration services.
all of our processes and industry solutions are
assessed at SEI/CMM Level 5 by an independent
                                                         We further strengthened our development and
organization. Onsite Cognizant associates work
                                                         integration services last year by extending our
seamlessly with our clients' own IT staff, using
                                                         Enterprise Consulting Practice  a key strategic
proven metrics to assess the details and complexity
                                                         focus for us in 2002  enabling Cognizant to take
of their requirements and identify the ways in which
                                                         end-to-end responsibility for delivering complex,
our business model will help them achieve the
                                                         large-scale projects that drive substantial improve-
greatest quantified savings.
                                                         ment in business performance. In fact, our Company
                                                         has executed some of the largest development and
Cognizant now goes much farther than other
                                                         integration projects in the industry and unlike our
offshore companies in its application management
                                                         offshore competitors, Cognizant focuses exclusively
services to ensure that clients receive their desired
                                                         on business applications. This has allowed us to
results by helping them with change management;
                                                         position ourselves higher on the value chain of
taking their personnel, if needed; and performing
                                                         premium service providers by helping our customers
portfolio analysis to identify all systems suitable
                                                         deploy the right business and IT strategies to further
to offshore and uncover opportunities for infra-
                                                         their growth and solve their business problems,
structure cost savings through consolidation or
                                                         thus maximizing their return on their technology
re-platforming. In typical large-scale application
                                                         investment. In addition, it differentiates us from
outsourcing assignments, Cognizant has cut costs
for customers by 50 percent, while increasing
system stability by 55 percent. With our sophisticated
processes, Cognizant can transfer knowledge and






                                    our offshore competitors and helps us to effectively   We developed a CRM strategy aligned with their
                                    compete against the "Big Five" and other leading       overall business objectives, and built and imple-
                                    domestic consultants. As the economy improves,         mented a system to accomplish these objectives.
                                    we see tremendous revenue potential for such           The CRM system is state-of-the-art and will eventu-
                                    higher-rate, higher-margin services.                   ally bring together sales, marketing and service on
                                                                                           one platform. This business-critical project is driven
                                    The Enterprise Consulting Practice adds to             by the client's CEO and CIO, and is a core part of
                                    Cognizant's value proposition by helping clients       the client's next-generation business architecture.
                                    select and integrate technology. It consists of the    We won this assignment against nine competitors,
                                    architecture, strategy and consulting practice;        including several of the "Big Five", based on our
                                    e-business; data warehousing; Customer                 CRM and healthcare domain expertise.
                                    Relationship Management (CRM) and Enterprise
                                    Resource Planning (ERP), and several other compe-      Progress From the Inside Out
                                    tencies. For example, our CRM practice works with      Cognizant's success is a product of careful strategic
                                    clients on strategies to boost revenue by increasing   planning and the dedication, innovation and hard
                                    their sales, achieving cross-selling goals and         work of our 3,900 employees  the people who
                                    improving customer satisfaction. The expertise of      execute our ambitious plans to consistently deliver
                                    our CRM practice leadership combined with our          the value, speed and quality that cements our client
                                    innovative suite of services and valuable strategic    relationships. In recognition of their integral role
                                    alliances with the leading CRM technology innova-      in the success of our Company, Cognizant offers a
                                    tors and the world's leading provider of e-business    dynamic workplace environment that encourages
                                    application software give Cognizant a powerful set     employee growth and advancement. The Cognizant
                                    of cutting-edge capabilities to attract new clients    Academy, our in-house corporate university, is an
                                    and deepen our relationship with established clients   excellent example of this. The Academy offered
                                    across our key verticals.                              nearly 46,000 person-hours of technical training
                                                                                           in 2001 and an array of programs to advance the
                                    As an example of the value of adding new technology    personal, managerial and cross-cultural skills of
Global Services
                                    capabilities and confirming the strength of our CRM    our associates.
Dedicated Competency and Solution
Centers position Cognizant higher
                                    practice, we won an end-to-end CRM assignment
on the value chain.
                                    with a multi-billion dollar pharmaceutical firm.       The Cognizant Academy is one of many reasons that
                                                                                           we are considered an employer of choice in India, as
                                                                                           rated by students at the leading universities, and
                                                                                           increases our ability to attract exceptional people.
                                                                                           Cognizant's employees are the most satisfied
                                                                                           employees in India's competitive IT service industry
                                                                                            a factor that is crucial to a company's overall
                                                                                           performance  according to IDC India's "Best
                                                                                           Employer Survey". Our Company also measures
                                                                                                                 
                                                                                                                 
                                                                                                                 
employee satisfaction in an annual survey conducted       plan to return to this year's event, indicating that
by the Hay Group  the most recent of which               Cognizant is right on track in terms of responding to
indicated that our employees, who have always             the needs of the IT community.
expressed a very high level of overall satisfaction,
are more satisfied with Cognizant than they have          Our rapid growth in staff, and the steep increase in
ever been before. High employee satisfaction not          the size and number of client projects spurred us
only helps us attract and retain India's top IT talent,   to boost our investment in infrastructure last year.
but also motivates our employees to deliver the type      During 2001, we launched the construction of three
of outstanding service that results in Cognizant's        state-of-the-art, company-owned technocomplexes,
consistently high level of customer satisfaction. IDC     which will accommodate approximately 6,000
India, which evaluated 150 Indian IT companies on         employees in Calcutta, Chennai and Pune. We
16 parameters, also concluded that Cognizant has          recently opened the Pune center  a modern seven-
the best facilities and resources, including connec-      story structure, built to support new clients for
tivity, project development facilities and high band-     application outsourcing and high-value engagements.
width into customer systems. In addition, Cognizant
has strong, established relationships with India's                                                                
                                                          Leading the Way Into the Future
                                                                                                                
top academic institutions, providing us with a            The outlook for offshore services is extremely posi-
                                                                                                                  
reliable pipeline of superior IT talent, which allows     tive. Forrester Research predicts that the percentage
us to respond to demand by ramping up our staff           of IT budgets going to offshore firms will more than
quickly with the best people.                             double by 2003 to 28 percent. Empowered by
                                                          dedicated, highly capable employees, strong client
The strength of our corporate culture is a key differ-    relationships and the support of our shareholders
entiator for Cognizant that gives us a significant        and the financial community, we will continue to
competitive edge in our industry and makes the            assert our leadership as the mainstream acceptance
Cognizant business model difficult to replicate.          of offshore continues to grow.


Our focused and innovative sales and marketing            Sincerely,
teams continued to build recognition of Cognizant
among prospective clients and to increase apprecia-
tion for Cognizant's highly effective onsite/offshore
business model by spearheading a number of                Wijeyaraj Mahadeva
successful initiatives during 2001. The Cognizant         Chairman and Chief Executive Officer
Community, our unique customer conference, and
our other thought-leading events give CIOs and CTOs
the valuable opportunity to benefit from each
other's experience and ideas. The attendance at our       Lakshmi Narayanan
events is impressive, and 100 percent of last year's      President and Chief Operating Officer
Cognizant Community participants stated that they
