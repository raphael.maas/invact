TO OUR SHAREHOLDERS
Three years ago we outlined our strategy to transform
H&R Block from the worlds largest tax preparation company
to a leading provider of year-round financial products and
services. This year we achieved several critical milestones
that moved us significantly closer to our goal.
We acquired the non-attest assets of McGladrey & Pullen,
LLP, the countrys seventh largest accounting and business
consulting firm, as well as Olde Financial Corporation, the
parent of one of Americas largest discount brokerage firms.
Our operations now include the accounting, financial services
and mortgage businesses  in addition to our core tax
preparation business  needed to attain our vision of becoming
the preferred tax and financial partner for our clients in all
markets we serve.
We are well positioned not only to extend financial services
to our more than 19 million tax customers around the
world, but also to appeal to a much broader base of clients.
These acquisitions expanded our office networks, our
product and service offerings, and our distribution
capabilities, and they will enable us to develop, package
and cross-market financial products and services tailored
to our clients diverse needs.
While we made progress in building our business this year,
we also faced several challenges in our U.S. tax operations
and new e-commerce initiative. We staffed to meet the
increased tax client demand that traditionally occurs in
April, however, the end-of-tax-season rush did not meet our
expectations. Our new e-commerce operation experienced
unexpected technical difficulties, which impacted our ability
to generate revenue during key filing periods of the tax season.
The result is that although our overall financial performance
exceeded fiscal 1999, it did not achieve the earnings results
we know we are capable of delivering and have delivered for
the past three years.
Revenues increased in fiscal 2000 by 49.1 percent, due in large
part to the five months of revenue from Olde and nine months
of revenue from McGladrey acquisitions. Earnings per share
from continuing operations, however, failed to meet our targeted
increase of 15 percent or greater, rising 8.1 percent to $2.55
per diluted share from $2.36 per diluted share in fiscal 1999.
Two other factors impacted our earnings. We incurred
increased amortization expense related to the intangible
assets from the McGladrey and Olde acquisitions. In
addition, we experienced a non-recurring loss on the sale of
certain assets related to mortgage operations, which was
partially offset by a one-time financing gain at Option One
Mortgage Corporation.
With this backdrop, we are pleased that the companys
balance sheet and cash flows remain strong. Our performance,
as measured by earnings from continuing operations before
interest (including interest expense on acquisition debt,
investment income and interest allocated to operating business
units), taxes, depreciation and amortization (EBITDA), grew
$159.3 million to $598 million, a 36.3 percent increase. Net
earnings from continuing operations, excluding the after-tax
amortization expense of acquired intangible assets, increased
19.7 percent to $304.4 million, or $3.08 per diluted share, in
fiscal 2000. This compares to $254.2 million, or $2.52 per
diluted share, the prior year.
This past year we considered a variety of alternatives for our
interest in Option One Mortgage Corporation. The companys
intent was to realize the value inherent in a business whose
annual loan originations have tripled in three years.
Ultimately, we implemented a combination of off-balance
sheet financing and whole-loan sale arrangements to free up
significant cash. This action will provide stability in dealing
with the secondary market and allow us to realize the
continuing benefit of Option Ones strong performance
without bearing as much of the capital risk.
A portion of the capital previously required to fund subprime
mortgage loans may be used in a new 12 million share
repurchase program, representing 12.2 percent of the shares
outstanding at April 30, 2000. Through June 30, 2000, the
company has repurchased 4.8 million of the 12 million share
authorization. Since February 1998, the company has
repurchased 17.8 million shares as part of our continued
commitment to build shareholder value.
U . S . TAX SERVICES
With 19.2 million clients worldwide, more than 10,000
offices, and some 90,000 tax preparers, our tax preparation
network is the core component of our financial partnership
strategy. As the center of the H&R Block brand, the steady
growth of our traditional tax business has enabled us to create
a business model distinct in the competitive financial services
industry and one uniquely tailored to the needs of our clients.
Our U.S. tax services subsidiaries expanded their market
share again this year, increasing from 13.7 percent in tax
year 1999 to 14 percent of tax returns filed with the IRS in
tax year 2000. We also grew our share of the market for
paid tax preparation from 24.6 percent to 25.3 percent.
Fees from tax preparation and related services increased
13.5 percent, a slower growth rate than last year due, in
part, to the absence of significant tax law changes. Our
retention of clients increased, reflecting our efforts to
enhance the value we provide to them.
Despite our gains in revenue, client retention and market share,
the tax season did not finish as strongly as we anticipated.
Because we did not experience the usual peak in business
that typically occurs during the last two weeks of tax season,
office staffing levels  and likewise staffing costs  were
higher than necessary and subsequently impacted our
margins and earnings.
Our total number of U.S. returns prepared increased 3.3
percent to 16.3 million, while the number of U.S. clients
served rose 2.4 percent to 16.9 million taxpayers. More than
74 percent of our U.S. clients chose to file their returns electronically,
an increase of 10.5 percent over the prior year.
The pretax earnings contribution from U.S. tax operations,
which includes U.S.-based tax services, interests in refund
anticipation loans, e-commerce and software products,
increased 1.9 percent to $320 million. Revenues rose 13.7
percent to $1.4 billion. Pretax margins in our U.S. tax
operations declined almost three points to 22.4 percent from
25 percent in fiscal 1999. Our earnings growth was impacted
by increased spending on new initiatives and marketing, and
expanding our office network.
To improve our performance for fiscal 2001, we have
established investment-spending levels assuming conservative
growth rates and identified significant opportunities for
margin improvement. In addition, we are investing in new
information and database marketing systems that we expect
will improve our efficiency and productivity.
Our focus on strengthening client relationships led to the
introduction and expansion of several services this year,
including free electronic filing in most company-owned
offices. Tests over the last two years showed that the increase
in clients far outweighed the loss in revenue from free e-filing,
prompting us to expand the program nationally. E-filing
increases accuracy and speeds refunds to our clients. In a
similar vein, we tested the idea of offering refund anticipation
loans (RALs) with no additional bank fees in several test
markets, including California. Companywide, the number of
RALs increased 66 percent over last year, due in part to our
participation in the IRS debt indicator pilot program, which
helped reduce the risk of bad debts. Reduced financial risk
for us was translated into an approximate 40 percent reduction
in the banks RAL fees for our clients. While the acceptance
rate for our RAL product was strong, we did not see the
number of new clients that we had anticipated as a result of
broader product availability this year.
We also introduced Refund Rewards in cooperation with
leading companies like General Motors, Sears, Household
International and MasterCard. The program provides
discounts on an array of products and services for taxpayers
who choose to have the amount of their refund (less tax
preparation fees) loaded onto a prepaid spending card.
While the number of accounts sold through this program was
below our expectations, we received very positive feedback
from those who used Refund Rewards. We expect to continue
the program next year with some modifications.
Keeping our clients satisfied and coming back year after year
is a traditional H&R Block strength and a core element of
our partnership strategy. This year our client retention rate
increased 1.1 percentage points. Meanwhile, 85 percent of our
customers say they are very satisfied with their H&R Block
experience. Client retention and satisfaction likewise are
goals of our multi-year office renovation initiative.
Approximately 1,200 offices were remodeled in fiscal 2000.
SOFTWARE AND E-COMMERCE
H&R Blocks Internet presence grew substantially in fiscal
2000, establishing the foundation for a powerful complement
to our financial partnership strategy and to our network of
offices. Since our enhanced site debuted in January, the
number of visitors has grown from 500,000 to 3 million.
The strength of the H&R Block brand was evident as the
company negotiated strategic alliances with Web portals,
including The Microsoft Network, Yahoo!, go.com, and
AltaVista, for H&R Block to be the exclusive tax preparation
service on their sites.
While the number of U.S. taxpayers preparing and filing
their federal returns on the Internet increased from less than
500,000 last year to more than 2 million this year, our
research shows that less than one-tenth of 1 percent of
H&R Blocks tax office clients migrated to the Web. The vast
majority of online filers switched from a pencil to a mouse
for filing their taxes. Meanwhile, the number of taxpayers
turning to paid preparers continues to increase as people
place greater value on convenience and their own time.
Our first year of broad-based e-commerce services required
significant investments in new staff, hardware and software.
At the same time, we experienced client acquisition and
technical difficulties with our online tax preparation
program on several occasions throughout the tax season. As
a result, our e-commerce operation suffered a pretax loss of
$18.5 million.
Strategically, we view the first year of our e-commerce
initiatives as an important learning experience. Were
reviewing our technology alternatives, taking steps to
enhance our business model, and focusing on integrating
our e-commerce strategy across all of our businesses.
Our award-winning TaxCut tax preparation software
continued to enjoy broad consumer and industry support,
receiving No. 1 ratings from Money magazine, PC
Magazine, PC World, The Wall Street Journal and other
publications. Microsofts entry into the tax software business
cut into our market share and sales growth, but we still
experienced revenue growth of 18.1 percent.
In March, we announced a multi-year strategic alliance with
Microsoft Corporation. Under the agreement, H&R Block
will provide exclusive Web and desktop-based tax preparation
products to users of MSN MoneyCentral and Microsoft
Money. The companies will collaborate on development of
software and online products, and Microsoft will discontinue
development of a separate tax software.
INTERNATIONAL OPERATIONS
H&R Blocks international offices, located primarily in
Canada, Australia and the United Kingdom, showed notable
improvement after disappointing results last year. Led by
Australia, operations in all three countries demonstrated
improved earnings. Pretax earnings nearly doubled to $4.9
million from $2.5 million last year. Revenues increased 9.1
percent to $81.5 million from $74.7 million in fiscal 1999.
We plan to extend the companys financial partnership
strategy into Canada, where we now prepare about 9 percent
of the nations tax returns. We also expect continued growth
in Australia, where we are also the market leader.
FINANCIAL SERVICES
With the December 1 acquisition of Olde Financial
Corporation, owner of Olde Discount Corporation, H&R Block
created a truly comprehensive financial services organization,
combining brokerage, retail mortgage origination, financial
planning and tax preparation. Our financial services
operation reported a 112.3 percent increase in pretax
earnings to $129.8 million, while revenues rose 140 percent
to $623.8 million.
Olde represented the last key building block to begin
implementing our financial partnership strategy, and we
were especially pleased with their five-month performance.
Olde contributed $253.9 million in revenues and $66.8
million in pretax earnings in fiscal 2000, and its acquisition
was accretive by 11 cents per diluted share. Weve
begun enhancing Oldes underlying operation, leveraging
its office network to reach more H&R Block clients and
building on its foundation to serve more clients across the
United States.
We converted 73 of Oldes 181 offices to create a total of 93
new H&R Block Financial Centers, our year-round onestop
financial services offices. We plan to convert the
remaining offices over the next two years. We also
incorporated Oldes online investing capabilities into
hrblock.com.
Option One Mortgage Corporation delivered another
strong performance this year. Option One  which
includes H&R Block Mortgage Corporation, the companys
retail mortgage operation  reported solid earnings of
$95 million, a 49.9 percent increase over the prior year.
Revenues rose 45.3 percent to $322 million. The company
had a 58 percent increase in loan originations, totaling
$5.7 billion of loans. H&R Block Mortgage comprised
more than 7 percent of Option Ones origination volume in
the fourth quarter.
Looking to the future, we are continuing to develop a larger
portfolio of financial products and services to better meet
our customers needs across multiple delivery channels.
The ability to deepen the relationships we have with our
clients will provide marketing cost advantages as well as
position H&R Block as a more relevant and valuable
financial partner.
BUSINESS SERVICES
The August acquisition of the non-attest assets of
McGladrey & Pullen, LLP has allowed us to create a
national tax, accounting and consulting firm focused on
middle market businesses and high net worth individuals.
We are now working to integrate our seven previously
acquired accounting firms under the new RSM McGladrey
brand. When the integration is complete, RSM McGladrey
will have 100 offices and offer services in 13 of the top 25
U.S. markets.
The first-time inclusion of RSM McGladrey drove the
segments annual results significantly higher, with a 140.3
percent increase in earnings to $17.1 million on revenues of
$310.9 million.
While our immediate focus is on integrating operations
and cultures, longer-term we plan to build a presence in
all major U.S. markets and work to make RSM McGladrey
the preferred brand in the targeted middle market business
segment. The financial partnership strategy that
applies in our other businesses is equally applicable here,
only to a different customer base. The fundamental
dynamic is trusting relationships between advisors and
clients, and we have begun to equip our accounting
professionals with more financial solutions tailored to
their clients unique needs.
MANAGEMENT APPOINTMENTS
The building process at H&R Block has been dramatic and
often intense in the past few years. We are a larger and
more complex company, and weve recognized that critical
management skills are needed in specific areas to drive our
strategic initiatives. During the past year, we made a
number of key management appointments to help lead our
organization into the future:
Mark A. Ernst, who joined the company in 1998 as executive
vice president and chief operating officer after holding senior
management positions at American Express, was elected
president and chief operating officer and a member of the
Board of Directors last September. In June, the Board
approved a succession plan that will make Mark president
and chief executive officer on Jan. 1, 2001.
David F. Byers, a former Foote, Cone & Belding advertising
executive, joined the company in June 1999 as senior vice
president and chief marketing officer.
Frank J. Cotroneo became senior vice president and chief
financial officer in February after serving as CFO at
MasterCard International.
David J. Kasper, former executive vice president and national
sales manager for Norwest Investment Services, Inc., became
president of the Financial Services Group in February.
Jeffery W. Yabuki was named president of H&R Block
International in September. He is a former president and
chief executive officer of American Express Tax & Business
Services, Inc.
CONCLUSION
Integrating our financial services operations with our core
tax business and incorporating e-commerce capabilities
across all of our operations continue to be central to our
focus ahead. This past year we began to see significant
benefits that come from deeper client relationships. We are
committed to building upon the H&R Block brand and our
dominant position in tax services as we expand H&R Blocks
reach in select financial services and develop into a financial
partner with our clients.
This was a pivotal year as we accelerated our transformation
and continued to learn a great deal about our clients and how
we can better serve their growing financial services needs. We
have learned from the challenges of the past year and are
encouraged by the excellent opportunities that lie ahead.
Going forward, we believe we have all of the necessary
resources  management, business components and financial
strength  to execute our strategy effectively. Our focus on
building this solid foundation is a demonstration of our
continued commitment to delivering long-term value for our
shareholders. We are well positioned to become Americas
most-trusted financial advisor.
In closing, wed like to acknowledge the contributions of
two long-time members of our Board of Directors and
friends to H&R Block. Morton I. Sosland retires after 37
years of dedicated service. Marvin L. Rich served on our
board 38 years until his death on Oct. 7, 1999. We are
grateful to them for their personal commitment and guidance
to H&R Block.
We also want to thank all of our associates for their skills,
energy and dedication in creating the H&R Block of the
future and positioning the company with bright prospects
for the coming year.
FRANK L. SALIZZONI
Chief Executive Officer
HENRY W. BLOCH
Chairman of the Board