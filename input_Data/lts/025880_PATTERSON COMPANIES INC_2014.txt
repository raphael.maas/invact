To Our Shareholders:
Fiscal 2014 was an important year for Patterson Companies as we took major steps to set the
stage for the future and create a business platform with the scale and leverage that improves our
opportunities for continued success. During the year, we made significant investments in our business,
from expanding our international presence to improving our information technology systems. These
and other investments will provide long-term value to shareholders while still delivering solid
short-term results. We also moved forward on necessary changes to our business model that will
afford us the capability of building shareholder value despite economic cycles. We believe that we are
well on our way to having that platform and are confident that we can achieve our growth objectives.
Looking deeper at fiscal 2014, Patterson Companies hit a major milestone on the revenue side.
Consolidated sales totaled nearly $4.1 billion, a 12-percent increase from the prior fiscal year. The
year-over-year growth was primarily the result of the acquisition of National Veterinary Services
Limited (NVS), which expanded our footprint into the U.K. veterinary market and was immediately
accretive to earnings. We continue to be very pleased with how this acquisition is performing and the
platform it has provided to build our business internationally.
In addition to the acquisition of NVS, further important actions included:
 Continuing to make key investments in our business,
 Reinforcing our product technology leadership position with our Dental customers,
 Strengthening our Medical business by refocusing our efforts on our core competencies,
 Delivering on our capital allocation strategy by increasing the dividend and making share
repurchases, and
 Streamlining parts of the organization and redeploying our resources to high potential initiatives.
All of these moves resulted from the strategic plan that we rolled out to our leadership group at the
beginning of fiscal 2014. In that plan, we outlined key strategic intents that include broadening our view
of the markets that we serve, leading our clients to new market breakthroughs, further improving the
customer experience, operational innovation and continual improvement, and leveraging our financial
strength for competitive advantage. These objectives guide our decision making priorities and we will
continue to focus on them to take advantage of market opportunities.
We also continued implementing our multi-year global information technology initiative that is
designed to support Pattersons future growth, further enhance the customer experience and
secure further productivity gains going forward. Our progress thus far includes making substantial
enhancements to our Dental and Veterinary websites, significant investments in our infrastructure
and networking, a rigorous evaluation and redesign of our business processes, selection of new
enterprise software and the rollout of additional sales tools. Through this phased implementation,
we continue to reinforce our already strong customer service offerings.
When you exclude the costs we incurred to restructure the Medical unit, fiscal 2014 operating income
totaled $361.2 million, up 2 percent from prior-year levels. Excluding the restructuring, our incremental
technology expenses and the NVS acquisition, our operating margin was 9.9 percent, a 20 basis point
improvement from prior-year levels, which was one of our goals entering the year. Net income, excluding
the restructuring, was $213.9 million, or $2.10 per diluted share, compared to net income of $210.3 million,
or $2.03 per diluted share, in fiscal 2013. Please note that the full year fiscal 2014 results include eight
months of contribution from the NVS acquisition, or $0.04 per diluted share; expenses related to our
technology initiatives of $0.07 per share; and $0.13 per share of restructuring costs.
STRONG CASH POSITION
In fiscal 2014, Patterson Companies generated more than $196 million in cash from operations
and returned more than $180 million to our shareholders through a combination of share
repurchases and dividends, reflecting our ongoing commitment to generate value for our
owners. In March, we declared a 25-percent increase in the regular quarterly cash dividend to
$0.20 per share. The dividend increase demonstrates our confidence in Pattersons continued
ability to generate growing cash returns on our business investments and growth opportunities.
During fiscal 2014, we also repurchased 2.4 million shares of our common stock, with a value of
$96 million. With approximately 22 million shares remaining under our current authorization,
we will continue to execute on this program as market conditions allow.
Furthermore, we deployed our cash reserves to fund the NVS acquisition. We believe that
all these metrics point to a company with a solid financial platform and we will continue to
execute against our capital allocation strategy of targeted investments, dividend growth
and share repurchases.
PATTERSON DENTAL
Sales of Patterson Dental, our largest business, totaled $2.4 billion in fiscal 2014, growing
slightly from the prior year. Despite a challenging environment through the majority of fiscal
2014, we were pleased with the solid growth rates in basic equipment sales that we saw in the
second half of the year. Coupled with modest year-over-year growth in sales of consumables,
we believe that Patterson Dental is positioned to capture market share in fiscal 2015.
Additionally, we expect to continue to capitalize on the growing digital trend as we provide the
leading digital radiography and CEREC CAD/CAM technologies in combination with Pattersons
unmatched sales, service and support offering. By the end of fiscal 2014, our North American
CEREC user base totaled over 15,000 users, representing a year-over-year growth rate in the
high single digits. The growth prospects from our technology offerings, combined with the need
for dentists to invest in their practices, make us optimistic about the future.
PATTERSON VETERINARY
This unit now represents nearly one-third of total company sales. In fiscal 2014, sales of
Patterson Veterinary were $1.2 billion, up more than 59 percent from the prior year. Growth
over fiscal 2013 was driven primarily by the acquisition of NVS in August 2013. We continue to
see strong growth opportunities from this acquisition as it provides a platform for Patterson
to leverage our world-class product and service offering into the international markets.
The benefits from this transaction have been substantial, including NVS long-standing
relationships in the U.K. and other markets, their proprietary customer-facing technology,
operational efficiencies and strong operating cash flows.
U.S.-only sales totaled $784 million, up nearly 4 percent from fiscal 2013. Increases in our
domestic markets were primarily due to strong equipment sales, as we saw three consecutive
quarters of double-digit advances to end the year. Gains in equipment sales reflect our
successful investments in partnerships with our equipment manufacturers and Pattersons
best-in-class service and support model as a national provider. We are focused on taking
advantage of the favorable marketplace dynamics we are seeing, e.g., the growth in pet
ownership and the increasing investment owners are making to care for their pets.
Veterinarians will require sophisticated equipment and capabilities in their clinics and
hospitals to meet this market need, and Patterson will be right there to support them!
PATTERSON MEDICAL
Sales of Patterson Medical totaled $479 million in fiscal 2014 and were in line with our
expectations given the actions that we took during the year to realign this unit for future
growth. First, we are excited about the new leadership in this division and their approach to
reposition the business in our selected markets. In combination with our substantial industry
expertise, as well as our extensive branded and proprietary product and service offering,
we are confident we can generate growth.
Next, we announced that Patterson would divest several non-core product lines in our
medical segment. Though the immediate impact from the divestiture was a $0.13 per share
charge in fiscal 2014, we believe the divestiture was the right course of action from both a
financial and strategic perspective. By divesting these product lines, we estimate that we will
generate operational savings of approximately $2 million annually, or $0.01 per diluted
share, beginning in fiscal 2015. Strategically, we can now concentrate our efforts on those
rehabilitation areas with the highest growth potential.
FOCUSED ON THE FUTURE
As we enter fiscal 2015, we are intent on improving our execution within each of our business
units as we continue to meet and exceed our customers expectations. Our very strong strategic
partnerships are prospering across all of our businesses. Wrapped with Pattersons service
and support platform, we continue to receive very positive feedback from our customers.
Further, we are proud to be named to the Forbes 2014 list of Americas 100 Most Trustworthy
Companies. Patterson is one of only four companies in 2014 that have appeared on the list
three times in the past seven years. Congratulations to our employees who have embraced
our core values and are responsible for this recognition.
We enter fiscal 2015 with a conservative view of our markets due to the relatively soft
performances expected in the global economies. We are planning for conditions in our
markets to be much the same as they were in fiscal 2014. However, we are confident in
our ability to execute in any economic environment. Our three business units are operating
in markets where long-term growth is expected to outpace the general economy, and we
remain committed to investing in an improved customer experience. With solid strategies,
growth markets and a focused organization, we are bullish on the long-term prospects for
our company.
We appreciate the ongoing support of all our constituencies and look forward to keeping you
updated as we implement our growth strategies.
Scott P. Anderson
Chairman and Chief Executive Officer