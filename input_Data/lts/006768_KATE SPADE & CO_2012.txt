Dear Stockholders, Associates, Business Partners, and Friends:
Re-named, re-chartered, and re-invigoratedthats what 2012 meant for our people and our Company.
After years reconstructing, rebuilding, and playing defense, we have moved squarely to offense, with some
big wins during the year. And in the cases where we suffered some setbacks or disappointments, we faced
those with appropriate self-examination and bias to action. On balance, we ended the year embracing our
mantrafull steam ahead.
In 2012, on a comparable basis, we grew total sales and adjusted EBITDA significantly. Total shareholder
return for the year was on the high end for the industry at +41%.
Kate Spade is now positioned among the industry greatswith a future marked by significant growth and
long runway expansion. Beyond all the exciting metrics, this brand has now proven itself to be a platform
business. We strengthened its international presence; we achieved industry-leading digital sales and
marketing success; and we debuted an important, high potential sub-brand, Kate Spade SATURDAY. With all
the excitement around this business, nothing is more thrilling than realizing we are only in the early chapters
of what promises to be a long and exciting story.
Lucky Brand Jeans saw growth, profit expansion, and the beginnings of true lifestyle evolution. Led by one
of the industrys strongest management teams, this brand is resonating with its customer. Great fit, the
newest trends, and some of the best in-store service youll find in the mall have contributed to Lucky Brands
momentum. And like Kate Spade, we are only just getting started. The opportunity for sales growth and
operating profit leverage is staggering.
Over at Juicy Couture, we forged ahead with reimaging the brand via product, store, and marketing
improvements. And while we firmly believe we have established the right overall direction, we didnt see the
commercial results we expected in the second half of the year. But we believe this is short term, as we apply
all of the operational and organizational changes at Juicy Couture that have turned our other retail based
businesses around. In total, Juicy Couture remains one of the industrys most alluring assetswith
uncharacteristically strong brand equity around the world, a still-underdeveloped commercial presence, and
special link to Hollywoods emerging starlets.
Corporately, we concluded our last large-scale downsizing to bring our back-end cost structure in line with
benchmarks. And under the stewardship of our Chief Operating Officer, George Carrara, we made critical
investments in point-of-sale technology that promise to unlock a host of omnichannel functions for all of our
brands globallylinking their front-ends and back-ends to enable superior customer service, better
inventory management, and true Customer Relationship Management capability. We also prepared each
brand for a major re-launch of e-commerce sites in 2013 with improved functionality.
With all the excitement surrounding the Company and its opportunities ahead, we ended the year by
asserting that we remain fully committed to delivering value to our shareholders.
In all, I am extremely proud of our teams, our strategy, our portfolio, and our progress. As we forge into 2013,
we continue to fully embrace our mantrafull steam ahead!
Sincerely yours,
William L. McComb
Chief Executive Officer