Dear Shareholders of Cardinal Health:
We are in a period of great transition for the U.S. healthcare system. During this past scal year, the national debate on healthcare
reform resulted in the passage of the Patient Protection and A ordable Care Act. With 1,083 pages of legislation in the nal bill to
interpret and implement, this legislation marks the beginning of the reform process, not the end.

We applaud the e ort to provide security and stability to those who currently have health insurance and coverage to those who
don't. But we also know that we need a system that is much more integrated and easier to navigate. And that ultimately the issue
of cost-e ectiveness will be central to having a healthcare system that is inclusive and a ordable.

We started scal 2010 with the understanding that we needed to position ourselves in anticipation of these changes, while at the
same time strengthening our core business. I feel good about how far we have come. We have made signi cant progress this past
year, and our customers are telling us that we are on the right path.

The customers we serve--from retail pharmacies and hospitals to physician o ces and laboratories--are all looking for new ways
to deliver care and to compete, both in the context of this new legislation and in an environment that will evolve in directions that
are not necessarily clear today. The work we do across the continuum of care to make the system more cost-e ective is especially
relevant now. At Cardinal Health, we are focused on the "business behind healthcare" so our customers can focus on their patients.




Fiscal 2010: Changing in a time of change
                                                                        Over the past year, the Pharmaceutical segment made great
Fiscal 2010 was a year of transformation. With the spino of
                                                                        strides. We worked hard to strengthen our partnerships with
CareFusion complete, we began this journey with a commitment
                                                                        major customers, renewing agreements with many of our largest.
to reinvigorate our performance, our strategic positioning and
                                                                        Our refocus on programs that address the needs of independent
our internal culture. And while the journey continues in scal
                                                                        retail pharmacies allowed us to double our growth rate with them,
2011, the extent and rate of our progress on these dimensions
                                                                        compared to the previous year. We changed our generic sourcing
was better than we anticipated.
                                                                        and selling models, creating new value for customers and for our
Revenue for the company increased 3 percent to $98.5 billion,           generic partners. At the same time, we worked closely with our
with operating earnings up 1 percent to $1.3 billion. Diluted           branded supplier partners, a number of whom have been working
earnings per share (EPS) from continuing operations were $1.62 or       their way through the integration of major mergers. We continue to
$2.22 on a non-GAAP basis*. While our non-GAAP EPS was down             devote considerable energy to strengthening these relationships and
slightly versus scal 2009, we performed considerably better             creating value for our branded partners as their needs evolve in this
than we expected coming into the year, given the signi cant             rapidly changing environment.
strategic investments we made in our businesses. In addition,
                                                                        In addition to reinvigorating our Pharmaceutical segment strategy
our organization did an outstanding job managing our working
                                                                        and performance trajectory, we also tackled the composition of
capital, and we generated $2.1 billion in cash from operations
                                                                        our portfolio of businesses. We enhanced our Medicine Shoppe
for the full year.
                                                                        International network by o ering franchisees an option to switch to
Our Pharmaceutical segment revenue increased 2 percent to               an alternative model that o ers a more exible, fee-based structure
$89.8 billion, with segment pro t down 3 percent to $1 billion.         versus the previous royalty-based model. And we decided to divest
The performance from this segment re ects the progress we               two units that were no longer a strategic t for the segment. Finally,
are making on key strategic priorities, and I am particularly           we made important moves to position the company for continued
pleased with how this segment was able to largely o set some            growth in specialty pharmaceuticals with the acquisition in early
di cult headwinds created by the year-over-year comparison                scal 2011 of Healthcare Solutions Holding.
in generic launches and the severe supply shortages impacting
nuclear pharmacy.
                                                                                                                                                            4
                                                                        * See page 23 for a reconciliation of the di erences between the non-GAAP nancial
                                                                        measures and the most directly comparable GAAP nancial measures.
Our Medical segment had a very solid year, with revenue up             I am particularly proud of how the company managed its capital
7 percent to $8.8 billion and segment pro t up 11 percent to           in an extraordinary economic environment. During this past year,
$428 million. During scal 2010, we also developed our segment          we returned more than $500 million to our shareholders through
strategy around channel and category management, which we              dividends and share repurchases, and we invested in the areas of
believe will better align with our customers' needs and enhance        our business that are critical to support our customers and drive
our ability to apply our core capabilities to help customers be more   future growth. We enter scal 2011 with a very strong balance
cost-e ective. All of this was achieved even as we continued to        sheet, which provides signi cant nancial exibility.
make investments to further improve our customers' experience,
                                                                       We have also made progress in several non- nancial, yet
enhance our technology platform and processes, and position the
                                                                       important, aspects of our company. For instance, our focus on
business for sustainable growth.
                                                                       talent management, leadership development and core capabilities
In scal 2010, our hospital supply business grew, and our margin        training is helping to reinvigorate the culture and enhance the
                                                                       connection between our employees and the positive impact they
rates stabilized. We are well positioned in this space with a strong
footprint across all customer channels. And we completely rebuilt      can make on our health system. Customers are telling us that the
                                                                       e orts we are making to strengthen our partnerships reinforce
our sourcing model for medical products, expanding our global
                                                                       the value we bring to their businesses. This shift of our "center
sourcing capabilities and leadership in this space.
                                                                       of gravity" more decisively out to the customer is re ected in our
As we see the delivery of healthcare moving to smaller and more        loyalty survey scores.
disease-speci c units of care, we are investing in our ambulatory
business to grow our footprint through an expanded sales               And now more than ever, Cardinal Health is a critical partner in the
organization and an enhanced ordering system that supports             communities in which we operate. In scal 2010, Cardinal Health
the unique needs of these customers. Our clinical lab business         donated nearly $20 million in cash and products to improve
                                                                       the delivery of healthcare and make our communities a better
continues to be an important part of this segment and delivered
                                                                       place to live and work. We are also in the third year of our E3 grant
top- and bottom-line growth, helped in part by the irregular u
season in the rst half of the scal year.                               program--which is the rst and largest private fund of its kind--
                                                                       that has helped more than 100 hospitals implement measurable,
Additionally, over the past couple of years, we have been working      evidence-based practices to improve safety and e ciency.
hard to improve the performance in our Presource surgical             We believe that giving back to the community is a fundamental
kitting business. Through a combination of Lean Six Sigma and          responsibility for our company, and we have made charitable
operational excellence initiatives, and our expanded presence          giving and employee volunteerism key underpinnings of our
in surgery centers, we returned the unit to positive growth and        culture and values.
successfully reset its trajectory. And our medical distribution
business in Canada had an exceptional year, with double-digit          None of what we accomplished in scal 2010 would have been
revenue and pro t growth in scal 2010.                                 possible without the tremendous commitment of our employees.
                                                                       I continue to be impressed by their passion for our industry
                                                                       and their tireless e orts to help our customers improve their
                                                                       businesses and make the system more cost-e ective.



5
                                                                     Our channel and category management strategy in the Medical
Continuing the momentum
I am very pleased with the rate of progress we made in scal          segment will enhance our ability to work in closer partnership
2010, and we look forward to continuing this momentum in scal        with our customers and use our core capabilities to address their
2011. As we further strengthen the core of our company, we will      key pain points. And at the beginning of scal 2011, we organized
continue to look for opportunities to expand into adjacent areas     the segment around these two fundamental areas. In tandem with
when prudent.                                                        this approach, our multi-year Medical Business Transformation
                                                                     project continues. By redesigning our information technology
As I mentioned, we recently completed a very important               platforms, processes and policies, we will be able to deliver an
acquisition in the Pharmaceutical segment of Healthcare Solutions    even better customer experience and respond to their changing
Holding. Known in the industry as P4 Healthcare, this business       needs even faster.
o ers a meaningful and di erentiated set of services in the fast-
growing specialty pharmaceutical area--an adjacency to our           And as we look across the industry for the next few years, change
core pharmaceutical distribution services--where we have been        is part of the landscape as the Patient Protection and A ordable
underrepresented in the past. The commercial activities within       Care Act enters the implementation phase. We played an active
P4 Healthcare serve key participants across the chain of specialty   role in Washington prior to the legislation's passage, working with
care, including physicians, pharmaceutical companies and payors,     industry associations and policy leaders. We will remain an active
by providing essential tools, services and data to help improve      partner in this process to ensure that our voice is heard and to
patient outcomes and increase e ciency in the delivery of            serve as an advocate for our customers--both in Washington and
healthcare. While this acquisition will have only a modest impact    at the state level--as decisions are made about how to implement
on scal 2011, it positions us well to participate in the growth of   the healthcare reform provisions.
the specialty category in future years.
                                                                     In short, it is an exciting time to be at Cardinal Health. I am
We are also planning for the future of molecular imaging and         proud of the value we bring to our customers and our supplier
investing in our nuclear pharmacy business to better support this    partners, and I am honored to be part of a team of dedicated
growing eld. We are expanding our manufacturing capabilities         employees who make it a reality. I know we can improve the
in positron emission tomography (PET) and strategically locating     cost-e ectiveness of our healthcare system, and our employees
our PET facilities near our leading network of nuclear pharmacies    are energized around this common purpose. As we improve the
to better serve the needs of diagnostic tests that use the PET       business behind healthcare, we do so with the ultimate goal
modality. As part of this strategy, we have partnered with           of creating value for our shareholders. Now is a critical time in
several leading research universities and the American College       healthcare, and Cardinal Health is well positioned to lead. We
of Radiology Imaging Network to support clinical trials to help      embrace both the challenges and the opportunities that lie ahead.
combat some of the most common and complex disease states            Thank you for your continued support.
including cancer, heart disease and neurological disorders. By
                                                                     Sincerely,
partnering earlier in the development process, we can create
strong partnerships to be ready to help commercialize new
products immediately after they receive regulatory approval.

                                                                     George S. Barrett
                                                                     Chairman and CEO
                                                                     Cardinal Health, Inc.                                             6
