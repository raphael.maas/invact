Fellow Shareholders,

I am pleased to present to you our 2016 Annual Report.
2016 was another successful year for the Company as we continued to execute our operating
strategies and strengthen our position as the preeminent worldwide developer and operator of y py p g
convention-based Integrated Resorts.

We again delivered strong financial results during the year, generating industry-leading
adjusted property EBITDA, cash flows and profit. Our unmatched scale, the diversity of our g g g yg g y g g g g yg g y
cash flows and our critical mass of hotel, retail and Meetings, Incentives, Conventions and
Exhibitions (MICE) offerings are unique in our industry and position us well for future growth.
We remain steadfast in our commitment to returning excess capital to shareholders, and it is gratifying that we were able
to return nearly $2.3 billion of capital to shareholders during the year through our recurring quarterly dividend program. g p g yg
We also increased our recurring dividend for the sixth consecutive year, to $2.92 per share for the 2017 calendar year. y p g y g gq y p g y p g y g gq y p
Our balance sheet strength allows us to continue to return excess capital to shareholders while maintaining our ability to g yp y
invest in promising new development opportunities. g
In Macao, market conditions improved during the year. We successfully opened The Parisian Macao, our latest Integrated
Resort on the Cotai Strip and another �must-see� destination on Cotai. The Parisian greatly expands our critical mass on p g y yp g p g y yp
the Cotai Strip, where our market-leading investments include nearly 13,000 hotel rooms in four interconnected resorts, p g yp
two million square feet of retail-mall offerings and two million square feet of MICE capacity. We believe our Cotai
pgy
Strip property portfolio will be an outstanding platform for growth as Macao continues to evolve and to realize its full q g q py
potential as Asia�s leading business and leisure tourism destination. We regard it as a privilege to contribute to Macao�s pp p yp gp g p yp gp g
success in realizing its objectives of diversifying its economy, supporting the growth of local businesses and providing
p g g pg
career development opportunities for its citizens, including through our Sands Academy. g j y g y pp g g
In Singapore, Marina Bay Sands again delivered both great financial performance and powerful contributions to the
business and leisure tourism appeal of Singapore, further solidifying the property as the world�s most successful convention- gp y g g p p
based Integrated Resort. Marina Bay Sands remains a �must-see� destination in Asia, while its industry-leading MICE pp g p y g p p y
and entertainment offerings continue to contribute to employment, business and leisure tourism and visitor spending in g y yg
Singapore. Marina Bay Sands� consistent success in contributing to Singapore�s economic objectives positions the property g py p g g py
as the most important reference site for major metropolitan areas in emerging Integrated Resort jurisdictions as they gp y g gp j p p p y gp y g gp j p p p
prepare to leverage the economic benefits of convention-based Integrated Resort development. p j p gg g p j p gg
In Las Vegas, the market continued to improve in 2016. We generated growth in adjusted property EBITDA, with
record convention and corporate meeting business contributing to increases in our hotel and non-gaming revenue. In g p g g j pp y g p gg j
Pennsylvania, Sands Bethlehem delivered another record year in terms of net revenue and adjusted property EBITDA. p g g gg
Importantly, the benefits of our convention-based Integrated Resort business model have always extended far beyond
our own financial success. The Company�s properties and service offerings, featuring dining, shopping, convention, py g y y y g
group meeting and entertainment experiences, increase the appeal of our host cities and countries as business and leisure p y p p g g g pp g
tourism destinations, while helping to diversify their economies, attract outside investment and increase employment. We
g p g p pp
believe our convention-based Integrated Resort business model could provide compelling economic benefits to additional pg y py
countries in Asia.
I am always proud to highlight the positive impact the Company and our nearly 50,000 team members have had on
the local communities in which we operate. Through Sands Cares, our corporate giving program, we are committed to yp g g p p p y y
improving our local communities while reducing our environmental impact on the planet.
p g p g gp p g pg
Thank you for your support and the confidence you continue to show in our Company. We look forward to sharing with
you the ongoing success of the Company in the years ahead. y y pp y
Sh ld e on G. Ad le son Chairman and Chief Executive Officer
April 2017