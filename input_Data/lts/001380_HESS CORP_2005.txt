            
                                     To Our Stockholders:
In 2005, we made significant progress in executing our          $13.60 per barrel. Our reserve life improved to 8.8
strategy to grow shareholder value over the long term.          years, marking the third consecutive year in which
That strategy is to grow reserves and production in             we have lengthened our reserve life. We are building
a sustainable and financially disciplined manner and            a sustainable and profitable business with excellent
to deliver consistent financial performance from our            visibility of growth in reserves and production.
Marketing and Refining assets for more immediate
                                                                In Marketing and Refining, we continued to grow our
returns and free cash flow.
                                                                retail and energy marketing businesses and delivered
Our Corporation delivered another year of strong                strong operating performance at our refineries. Our
operating and financial results. Earnings rose to a             tightly focused regional business model and well
record $1.2 billion as the effect of higher crude oil           known brand allow us to be very competitive and
and natural gas prices more than offset the negative            generate strong financial performance.
impact on production from Hurricanes Katrina and
                                                                EXPLORATION AND PRODUCTION
Rita. Our capital and exploratory expenditures in 2005
                                                                In 2005, significant progress was made in our major
totaled $2.5 billion, of which approximately $2.4 billion
                                                                field developments, including three new field start-ups 
was invested in Exploration and Production and about
                                                                Block A-18 in the Joint Development Area between
$100 million in Marketing and Refining. Our Exploration
                                                                Malaysia and Thailand (JDA), the Clair Field in the
and Production expenditures included more than
                                                                United Kingdom, and ACG Phase I in Azerbaijan. In
$1 billion for field developments.
                                                                addition, the Phu Horm gas development in Thailand
In Exploration and Production, we made substantial              was sanctioned in 2005. Approvals of the Shenzi
progress in advancing our field developments, building          development in the deepwater Gulf of Mexico and the
a high-impact exploration program and capturing                 Ujung Pangkah oil development in Indonesia are expected
long-term growth opportunities through several new              during 2006. Over the next several years, we will bring
country entries. Our proved reserves increased to               on production from major new field developments in
1.1 billion barrels of oil equivalent at year-end, and          the deepwater Gulf of Mexico, the North Sea, West Africa
we replaced approximately 140% of our production at             and Southeast Asia. All of our operated development
a finding, development and acquisition cost of about            projects continue to be on schedule and on budget.




                                                              FINANCIAL POSITION
With regard to exploration, we added new acreage in
                                                              In 2005, Amerada Hess Corporation reported record
Libya, Egypt, Brazil and the deepwater Gulf of Mexico.
                                                              net income of $1.2 billion, or $11.94 per share.
Our drilling program for 2006 includes approximately
                                                              Exploration and Production earned nearly $1.1 billion
seven high-impact wells in the Gulf of Mexico.
                                                              and Marketing and Refining earned $515 million.
As part of our strategy to add profitable, long-lived
                                                              As a result of strong operating performance and the
reserves, we established operations in two new
                                                              favorable pricing environment in 2005, our debt to
countries, Russia and Egypt, and re-entered our
                                                              capitalization ratio improved by three percentage
former operations in Libya.
                                                              points to 37.6 percent at the end of the year.
 In early 2005, Amerada Hess acquired a controlling
 interest in the Samara-Nafta joint venture in the
                                                              In response to the tragic devastation caused by
 Volga-Urals region of Russia and added additional
                                                              Hurricanes Katrina and Rita, the Corporation and our
 assets and licenses to the venture during the year.
                                                              employees donated $1.6 million to the American
 We currently have invested about $400 million in
                                                              Red Cross to assist in the relief efforts in the Gulf
 Russia and have recognized 87 million barrels of
                                                              Coast. In addition, the Corporation donated 50,000
 proved reserves.
                                                              of our 2005 Hess Toy Trucks to the United States
 Amerada Hess acquired a 55% interest in, and
                                                              Marine Corps Reserve Toys for Tots program, which
 the operatorship of, the deepwater portion of
                                                              were distributed to children impacted by the storms.
 the West Med Block in Egypt. The block contains
 existing natural gas discoveries as well as
 exploration opportunities.
                                                              We are pleased with the performance of our assets
                                                              and our organization in 2005 and remain optimistic
 After a 19-year absence, we, along with our Oasis
                                                              that the investments we are making for the future will
 Group partners, successfully concluded negotiations
                                                              grow our reserves and production profitably and create
 to resume operations in Libya where we have an
                                                              sustainable long-term value for our shareholders.
 8% interest in the Waha concessions. We began to
                                                              We deeply appreciate our employees for their
 recognize production and reserves in 2006.
                                                              dedication and hard work, our Directors for their
MARKETING AND REFINING                                        guidance and leadership, and our stockholders for
Marketing and Refining had another impressive year of         their continued interest and support.
performance in 2005. The HOVENSA and Port Reading
refineries both underwent successful turnarounds of
their fluid catalytic cracking (FCC) units in the first
quarter and benefited from an exceptional year of
strong margins. Despite a challenging hurricane season
and warmer than normal winter temperatures, our retail
and energy marketing businesses also delivered another
                                                              JOHN B. HESS
year of strong operational results. Retail marketing
                                                              Chairman of the Board
experienced solid growth in 2005, with year-over-year
                                                              and Chief Executive Officer
average gasoline volumes per station increasing by
                                                              March 1, 2006
7%, and convenience store revenue rising by 4%.

