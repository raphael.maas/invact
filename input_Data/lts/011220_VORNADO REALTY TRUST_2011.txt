To Our Shareholders
Vornados Funds from Operations for the year ended December 31, 2011 was $1,231.0 million, $6.42 per
diluted share, compared to $1,251.5 million, $6.59 per diluted share, for the year ended December 31, 2010.
Net Income applicable to common shares for the year ended December 31, 2011 was $601.8 million, $3.23 per
diluted share, versus $596.7 million, $3.24 per diluted share, for the previous year.
Funds from Operations Adjusted for Comparability was $5.27 per share in both 2011 and 2010.
Our core business is concentrated in New York and Washington, the two strongest markets in the nation, is office
and retail centric, and represents 80% of our EBITDA. In the 32 years we have run Vornado, cash flow from the
core business has never declined either in total dollars or on a same-store basis. This was true even in the difficult
recession years of 2008 and 2009.
Here are our financial results (presented in EBITDA format) by business segment:

Growth
As is our custom, we present the chart below that traces our ten-year record of growth, both in absolute dollars and
per share amounts:

Share Performance
Here is a chart showing Vornados total return as compared to the RMS Index* for various periods ending
December 31, 2011 and for 2012 year-to-date:

We believe our shares trade at a discount to spot NAV. As troubling, some analysts believe our NAV has recently
been growing more slowly than some of our peers  undoubtedly a capital allocation/mix of assets issue.
As Mike1 recently said to an industry group, We have lost some luster and we are going to fight to get it back.
And we will.
Shareholders and analysts have not been bashful in offering suggestions. Many believe that the value of our great
assets/business(es) is masked bythis, that and the other. Everyone suggests conference calls  fair enough. Other
suggestions have been: we are too big, nothing moves the needle  buy back shares; we are too complicated 
simplify, sell non-core, Mart, even Retail. Some say, split-up the Company into smaller, pure play(s).
Everything is on the table.
We Will.
We willconduct quarterly conference calls beginning with this years second quarter. In addition, Joe (Joe
Macnow, CFO), Ross (Ross Morrison, SVP - Controller) and Cathy (Cathy Creswell, VP - Investor Relations) will,
as usual, be available following each quarterly release for one-on-ones with shareholders and analysts.
We willcontinue to sell down the Mart business asset by asset2, retaining for now the 3.5 million square foot
Chicago Merchandise Mart. These assets are currently being marketed. In 2011, we sold High Point to the lender
for $237 million, realizing an $84 million gain. In January 2012, we sold the 350 West Mart building for $228
million, realizing a $54 million gain. We entered this business in 1998 with the $630 million acquisition of the
Merchandise Mart and related assets. Over the years, we grew the business with 11 bolt-on acquisitions of buildings
and trade shows investing an additional $338 million. This business was a grower for us until it fell victim to the
housing recession/depression. Valuing the entire business today at what we believe to be building-by-building
market pricing, we would have earned a 10% IRR. Thanks to Chris Kennedy, who ran this business until July 2011.
And thanks to Mark Falanga and Myron Maurer who run this business now.


We will reduce our exposure to the enclosed mall business.3 We certainly have the expertise to lease, manage and
develop mall product, but with only a handful of malls we are in no-mans land.
We will trim non-strategic, non-geographic assets from our strip shopping center portfolio to recycle capital.
We willexplore alternatives for the remaining strip assets.
We willredefine our New York business segment, run by the very capable David Greenbaum, to encompass all of
our Manhattan assets by including the 1.0 million square feet in 21 freestanding Manhattan street retail assets
(currently in the Retail segment)4, and Hotel Pennsylvania and Alexanders (currently in the Other segment).
We willcontinue to harvest, divest, and sell all non-core assets (Toys R Us included). Some are liquid and more
easily sold and a few are illiquid and difficult to sell. We will work all this through.
We will hold for now the non-core 23.4 million shares of JC Penney to reap the benefit of the Companys
transformation5 under CEO Ron Johnson (creator of the Apple retail business) and President Michael Francis.

BRAC
We disclose in our 2011 Form 10-K, page 76, that the effect of BRAC (the Base Realignment and Closure statute
passed in 2005, which finally made landfall this year) on Crystal City and Skyline will cause our Washington
occupancy to decline to the low 80s %; that 2012 EBITDA will be lower than 2011 by approximately $55-65
million; and, that it will take approximately two to three years to fully absorb this vacancy and for EBITDA to fully
recover. Weve handled large move-outs before when in 2004-2005 the Patent and Trademark Office relocated
2 million square feet from Crystal City. Re-leasing then took two to three years. We benefitted then and we will
benefit now by attracting many new private sector tenants to Crystal City to join our cohort of GSA tenants. Of
course, all this is a financial negative, which I guesstimate may cost us as much as $1.50 per share. Heres how I do
the math: lost cash flow of, say, $60 million this year, somewhat less next year and less again the following year,
plus the present value of accelerated TIs and leasing commissions. Damn annoying, but livable.
Skyline is an eight-building, 2.6 million square foot complex on Leesburg Pike, 3.5 miles from the Pentagon.
BRAC will inflict more than 30% vacancies here  the eye of our BRAC storm. These assets are subject to a
non-recourse $678 million ($257 per square foot) 5.74% cross collateralized financing due in 2017. We need relief
here, and accordingly have requested the loan be placed in special servicing.
There is a silver lining. BRAC emptied 1851 South Bell Street, a 370,000 square foot Crystal City office building.
Recently, we formally submitted our 4.1 Site Plan Approval to raze this building and replace it with a 700,000
square foot new-build state-of-the-art office building to be re-addressed 1900 Crystal Drive. This is the maiden
project (the first of many such projects, over time) under the 2010 Crystal City Sector Plan, which entitles us to
increase our 8.3 million square feet in Crystal City to 12.4 million square feet.
Our 26-building Crystal City complex, on the shore of the Potomac, contiguous to Washingtons Reagan National
Airport, a five-iron from the Pentagon, is the flagship of our Washington business. With little fanfare, new rents at
Crystal City have risen from $31.36 in 2005 to $41.81 in 2011. Our Crystal City complex is really a city within a
city, allowing us over the ten years of our ownership to create a unique amenity-rich environment, constantly
improving, creating very significant shareholder value. I am confident the next ten years will be even better

Acquisitions/Dispositions/Fund
Our external growth has never been programmed, formulaic or linear, i.e. we do not budget acquisition activity.
Each year, we mine our deal flow for opportunities and, as such, our acquisition volume is lumpy. Here is a ten-year
schedule of acquisitions and dispositions:

2011 was a very good year for acquisitions (outlined below). Kudos to Michael Franco and Wendy Silverstein,
Co-heads of Acquisitions, and team for outstanding performance.

In 2011, the Fund invested $484 million ($121 million our share) in three deals - One Park Avenue, Crowne Plaza
Times Square and 11 East 68th Street. For further information, see page 5 of our 2011 Form 10-K.
In the period 2011 to date, we disposed of $694 million of assets, principally, $465 million of Mart Division assets,
two District of Columbia mid-block office buildings for $107 million which was recycled into the Class A 1399
New York Avenue District of Columbia office building, and $122 million of other transactions. EVP Mike
DeMarco is taking the lead on dispositions. Our disposition activity will increase in the next several years.

Lease, Lease, Lease
The mission of our business is to create value for shareholders by growing our asset base through the addition of
carefully selected properties and by adding value through intensive and efficient management. As in past years, we
present leasing and occupancy statistics for our businesses.

Capital Markets
Since January 1, 2011, we have executed the following financial transactions:
 Fourteen financings secured by real estate aggregating $2.897 billion at a weighted average interest rate of
4.42% and a weighted average term of 7.3 years. Five of these financings were to support newly acquired
assets; the other nine yielded $368 million of net proceeds.
 In November 2011, we issued $400 million ten-year 5.057% senior unsecured notes to pre-fund the pay-off
of $499 million of 3.875% (5.32% GAAP) Exchangeable Senior Debentures which we have called and will
retire on April 18, 2012.
 We renewed both of our unsecured revolving credit facilities aggregating $2.5 billion. The first facility,
which was renewed in June 2011, bears interest at LIBOR plus 1.35% and has a 0.30% facility fee (drawn
or undrawn). The second facility, which was renewed in November 2011, bears interest at LIBOR plus
1.25% and has a 0.25% facility (drawn or undrawn). The LIBOR spread and facility fee on both facilities
are based on our credit ratings. Both facilities mature in four years and have one-year extension options.
 We repaid the $43 million loan secured by the Washington Design Center and $282 million of unsecured
debt.
 In April 2011 we sold 9,850,000 6.875% Series J Cumulative Redeemable Preferred Shares at a price of
$25.00 per share for $239 million of net proceeds.
At year-end we had $3.155 billion of liquidity, comprised of $793 million of cash, restricted cash and marketable
securities9 and $2.362 billion undrawn under our $2.5 billion revolving credit facilities. Our liquidity today,
adjusting for the retirement of the $499 million of Exchangeable Senior Debentures, is $2.8 billion.
Debt is now 39.4% of our market-value balance sheet. Since stock prices fluctuate, we believe an even better
measure of leverage may be debt to EBITDA  ours is currently 6.7x, down from 7.6x a couple of years ago.
Vornado remains committed to maintaining our investment grade rating.
Our Capital Markets A team is headed by EVPs Michael Franco and Wendy Silverstein with
SVPs Dan Guglielmone and Richard Reczka.

Sustainability
At Vornado, we believe that environmental sustainability is not only responsible citizenry, it is also good business.
Our goal is to be a leader in sustainability by creating a corporate culture that integrates the principles of
environmental responsibility and sustainable growth into our business practices. Please see highlights of the
sustainability section of our website reprinted on pages 14-18 of this annual report.
Top Ten
Heres a top ten list of little known, interesting Vornado factoids:
 Our 6.3 million square feet of leasing activity in 2011 in 735 transactions was our second best leasing year
ever. Thank you to our all-star leasing captains: Glen Weiss, Brendan Owen, Jim Creedon, Sherri White,
Leigh Lyons, Michael Zucker and Paul Heinen.
 Our largest Fifth Avenue retail lease expires in just two years. We expect to re-let this space for a fourbagger.
In the end, I expect this building to be worth more than six-fold what we paid for it in 1997.
 We own the only two Kmarts in Manhattan and three metropolitan New York area Sears stores, which are
among their ten best.
 We hear that the 1,000 foot tall, direct park-view apartment tower under construction on 57th Street is
pricing at $6,500 per square foot. Our 220 Central Park South site, just down the block, is better.
 Manhattan retail is on fire. Sherri is busy.
 We have about $1 billion of non-earning assets, a trove of future value creation.
 We own 2,424 rental apartment units in Washington and land for 4,200 more. These apartments, which we
now manage internally, produced 33% same store growth in 2011 (11% from rate increase and 22% from
occupancy gain). In addition, we have 4.1 million square feet of development rights in Crystal City under
the Sector Plan and 2.9 million square feet of development rights in Rosslyn and Pentagon City.
 1900 Crystal Drive, Mitchells to-be-built 700,000 square foot office building, is an architectural knock
out, will have a halo effect on all of Crystal City and will tower over the Crystal City skyline by 150 feet.
 Our 26% share of special servicer LNRs 2011 earnings were $58 million ($31 million recurring,
$27 million one-timers) versus our investment of $131 million.
 Vornado won the NAREIT Leader in the Light Gold Award for the second year in a row, in recognition of
our industry-leading energy and sustainability programs.


We are in a recovery (but by no means recovered). The recovery is inevitable, if only for the reason that the
governments of the Free World will make it so. Consensus is that the recovery will be shallow and as such, I
believe it will be much longer in duration than the three to five to seven year economic cycles that we are used to.
All this will prove to be a very good environment for our business.
We seem to be in a new paradigm: businesses have record earnings and rock-solid balance sheets; governments,
however, are broke. Government deficits and debt levels are scary and getting worse. I guess this will be okay,
until it isnt.
Markets fluctuate, but they always revert to the mean. Ours is a capital intensive business; money is our largest raw
material and the cost of money is our largest expense. Interest rates will re-normalize, we just dont know exactly
when (the Fed assures us not until after 2014). Market participants are acting as if todays cheap debt will last
forever  it wont.
I must say, I find investing in this market difficult. Nobody expected building prices to bounce back as strongly or as
quickly as they did  but they did. Assets are not cheap, either historically or in relation to current rents. Cap rates
are now 5% or lower for buildings in New York and Washington. Having said all that, capital wants to invest in our
home markets of New York and Washington more than anywhere in the United States  and thats a good thing.
And assets like ours have a long history of doubling in value every ten years  also a good thing

Our Vornado family has grown with 30 marriages and 51 babies this year.
Congratulations to David and Laureine on the birth of their first grandson, River Knight.
Congratulations to Michael and Jill on Kathys bat mitzvah; to Mike and Kelly on Allies sweet sixteen. Shannon
and Elliot had a boy, Ryan Sully.
Nick is going to Brown and Matt is going to Tulane. Sam is graduating and Mollie is going to Vassar.
Zach is going to Dartmouth and Rachel is going to Michigan. And, Blake is going to Michigan too.
Condolences to Bobby and to Mark.
Thanks to Ron Targan for his 12 years of service on the Audit Committee, and thanks to Bob Kogod for stepping in.
We welcome Daniel R. Tisch, who joined our Board in January. Dan is a seasoned, experienced investor, steeped in
the fabric, nuance and culture of business and of New York. And, like all New Yorkers, he loves real estate. Dan
joins a Board whose guidance and counsel is invaluable to our Company.
Mike and I are fortunate to work every day with the gold medal team. Our operating platforms are the best in the
business. We admire and appreciate the talents and energy of our partners, Mark Falanga, Michael Franco, David
Greenbaum, Joe Macnow, Mitchell Schear and Wendy Silverstein and Sherri White, Bob Minutoli, Mike DeMarco
and Mel Blum. Thank you as well to our very talented and hard working 39 Senior Vice-Presidents and 81 Vice-
Presidents who make the trains run on time, every day.
On behalf of Vornados senior management and 4,810 associates, we thank our shareholders, analysts and other
stakeholders for their continued support.
Steven Roth
Chairman
April 6, 2012
Again this year, I offer to assist shareholders with tickets to my wifes theatrical productions on Broadway  Leap of
Faith, Clybourne Park and coming soon, Kinky Boots. Please call if I can be of help.