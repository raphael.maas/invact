By all accounts, 2004 was a year in which businesses 
and consumers everywhere continued to expand their 
dependence on a new generation of network services 
and digital infrastructure to support global commerce 
and communications. At VeriSign, we were focused on 
delivering the Intelligent Infrastructure Services that 
enable and protect the billions of interactions every 
day that are fundamental to driving this change. 
Our growth strategy for 2004 focused on expanding 
the use of our core services, introducing compelling new 
offerings in security and mobile content, and increasing 
our presence in major accounts and international markets. 
We believe our team's execution to this plan resulted in 
strong financial results for the year. As we exited the year 
with record revenues, operating income, and cash flow, 
we also experienced continued momentum in our Internet 
and telecom services, positive early results with our new 
offerings in strong authentication and digital content, 
and initial progress on our longer-term initiatives in Voice 
over Internet Protocol (VoIP) and Radio Frequency 
Identification (RFID). 
Demand for our Intelligent Infrastructure Services is being 
driven by an accelerating global migration from physical 
to digital infrastructure, a dramatic increase in broadband 
and wireless communications and commerce, and an 
emerging convergence of mobility and entertainment. 
We believe these trends will continue and potentially 
accelerate further in 2005. With that in mind, we have 
positioned the company to help our customers rapidly 
introduce new network based services while effectively 
managing cost, complexity, and security on these 
modern networks. 

Financial Highlights. 
In 2004, VeriSign recorded revenues of approximately $1.17 billion and delivered non 
GAAP* operating income of $238 million, while significantly expanding margins. We also 
continued to strengthen our balance sheet, ending the year with over $789 million in 
cash, cash equivalents, restricted cash, and short-term investments driven by strong 
2004 operating cash flow of over $365 million. 
Business Highlights. 
The Internet Services Group (ISG) 
ISG contains our Naming and Directory Services, our Authentication and Managed 
Security Services, and our Payment Gateway Services. Increased demand, deeper 
international penetration, and new product traction combined to make 2004 a strong 
year, driving increases in almost every key business and financial metric in the Internet 
Services Group. 
As the digital economy continued to expand in 2004, our Naming and Directory 
Services business grew significantly. We processed over 14 million new registrations 
for .com and .net domain names during the year and saw greater than 70 percent 
renewal rates for existing names. The total active base of domain names ending in 
.com and .net at the end of the year stood at a record 38.4 million, up over 26 
percent from last year. Also in 2004, the Naming and Directory Services group 
was awarded the contract by EPCglobal to manage the database of Electronic 
Product Codes, establishing us in what we believe is a key role for the emerging 
RFID market. 
In our Security Services group we saw growth of the SSL digital certificate business 
as we ended 2004 with a record 455,000 installed units, up 18 percent from last 
year. This increase was driven primarily by the expansion of ecommerce globally 
and deeper penetration of large enterprise accounts. In early 2004, we completed 
the acquisition of Guardent, which was integrated into our Managed Security Services 
business. We now actively manage over 3,400 network security devices, up 34 percent 
from 2003. In addition, we had several key customer wins with major accounts: Morgan 
Stanley, GlaxoSmithKline, Honeywell, Verizon, and British Petroleum. 
One of the more significant events of the year for ISG was the launch of our VeriSign  
Unified Authentication (VUA) platform and next generation tokens. We believe this 

new offering is already proving to be easier to deploy, more cost-effective, and more flexible 
for our customers than other products on the market. VeriSign has already posted wins 
for VUA with industry leaders including US Bancorp and Bank of America. These early 
successes, along with the growing pipeline of trials and prospects, lead us to believe that 
we will see significant traction with this new product line in 2005. 
In our Payment Gateway business, we closed 2004 with a total of 127,000 active merchants, up 
25 percent from a year ago. Our gateway handled a record 118 million ecommerce transactions, 
for an aggregate value of $9.5 billion during the fourth quarter of 2004. 
VeriSign's domain name registry, Secure Sockets Layer (SSL), VUA, and Payment businesses 
are excellent examples of intelligent infrastructure services that enable network communications 
and commerce on a global basis. Given the recurring nature of the revenue in these lines 
of business, we believe the strong volume growth in 2004 should lead to continued 
revenue momentum for ISG in the present year. 
The Communications Services Group (CSG) 
VeriSign's Communications Services Group provides intelligent communications, commerce, 
and content services to fixed line, mobile, and broadband carriers and service providers. 
Demand for our core services, coupled with strong growth in mobile content in the second 
half of 2004, drove overachievement in the CSG group. We ended the year with nearly 1,300 
carrier customers, delivering over 12.8 billion database queries in the fourth quarter alone. 
We also processed billing and payment services for approximately 6.5 million wireless users. 
VeriSign continued to expand our relationships with several first and second tier carriers 
during 2004, including Cingular, Vodafone, T-Mobile, Leap, MetroPCS, and XO Communications. 
In addition, we made progress on several strategic initiatives during 2004 including our IP 
Connect services for VoIP routing and interoperability, which received the Product of the 
Year Award from Internet and Telephony Magazine. 
Of course, our mobile content services were the highlight of 2004 for CSG. In June of 2004, 
we completed the acquisition of Jamba!, a European mobile content company. Jamba! 
brought to VeriSign a rich portfolio of mobile content, a uniquely scalable platform, and 
a demand generation model that led to an incremental $181 million in revenue. During 2004, 
we continued to expand Jamba!'s presence in Europe, launched the services in Australia, 
and began test marketing in the US under the Jamster! brand. 
In 2005, CSG plans to broaden the mobile content and product catalog, establish additional 
carrier relationships, develop new demand generation spots for popular artists and titles, 
and continue our geographic expansion to drive additional subscriber growth. As part of 
our expanding mobile content strategy, VeriSign also recently announced our intent 
to acquire LightSurf Technologies, a leader in multimedia messaging services (MMS). 
We are committed to offering carriers a broad portfolio of services that can help them 
accelerate new revenues streams and build greater customer loyalty. LightSurf offers 
a suite of carrier grade multimedia messaging service services that allow users to 
capture, view, annotate, and share multimedia messages with any handset or email 
address, regardless of device, file type, or network operator. 
Overall, we worked very diligently this year to revamp our CSG portfolio of services 
to be positioned to help carriers drive new revenue streams in the converging world 
of voice and data. We're pleased with the results and proud that Frost and Sullivan 
recently named VeriSign Telecom Service Provider of the Year. 
In Closing. 
The VeriSign management team and our 3,200 highly passionate and committed 
employees are focused on fulfilling the Intelligent Infrastructure needs of businesses, 
carriers, and consumers as the world marches forward in its migration to a digital 
society and economy. As the Internet and telecommunications networks become 
more converged and complex, VeriSign will continue with its core mission to build 
and operate transformational infrastructures that become indispensable to society. 
The principles driving our growth strategy for 2005 include: 
+ Growing the base in existing services 
+ Driving compelling new services into the market 
+ Capturing key vertical markets and major accounts 
+ Expanding our reach internationally 
+ Leveraging our unique synergies for competitive advantage 
I want to thank our stockholders, employees, customers, and partners for the 
support and trust you place in VeriSign. We look forward to another successful year. 



