To DuPont Shareholders

As we entered 2009, the world's economies remained in the          Our engine of innovation never slowed down. We
grip of the global recession and worldwide financial crisis.        continued research and development investment
                                                                    during the recession at the same level as pre-recession.
At DuPont, we responded by focusing on what we could
                                                                    We introduced more than 1,400 new products in 2009--
control. We put in place directives aimed at maximizing
                                                                    or about 60 percent more than in 2008--and filed 2,086
margin, dramatically reducing spending, zero-basing
                                                                    U.S. patent applications, the most ever in a single year
                                   capital expenditures and
                                                                    for our company.
                                   aggressively reducing
                                                                   We committed to our shareholders to provide greater
                                   working capital. We stayed
                                                                    transparency into our businesses and reoriented our
                                   close to our customers
                                                                    reporting segments. We held an investor day in November,
                                   and took advantage of the
                                                                    and received positive responses from investors about our
                                   recessionary environment
                                                                    growth plans, as
                                   to respond to their needs
                                                                    well as the quality
                                   for product differentiation.                               DUPONT CORE VALUES
                                                                    and depth of the
                                   We did not let economic                                     Safety & Health
                                                                    information we
                                   turmoil distract us from                                    Environmental Stewardship
                                                                    shared with them.
                                   creating and protecting                                         Highest Ethical Behavior
                                                                                              
                                   shareholder value.                                          Respect for People
                                                                                               
                                                                  We were able to
                                  There was no established        achieve our goals and
                                  playbook for a year             take bold actions because of the exceptional commitment
                                  like 2009. Like many            of DuPont employees around the world. They stayed
                                  other companies,                focused on results through two restructurings and a major
                                  unprecedented volume            reorganization of the company. They continued to advance
                                  declines hit us very            our core values which are the absolute bedrock of DuPont.

                                  hard. But we did what           We deeply appreciate their extraordinary efforts in 2009.
we said we would do. Our actions positioned us well for
2010, ready to meet the challenge of delivering earnings          Delivering Growth Through 2012
growth despite declining income from Cozaar and Hyzaar          We will benefit in 2010 from anticipated gradual recovery
pharmaceuticals.                                                  in developed markets and a more rapid acceleration in
                                                                  emerging markets. We expect to grow faster than market
Firm Commitments, Dynamic Action                                  rates over the next few years due to our unique portfolio,
Throughout 2009, we moved with urgency and discipline             global positioning, and the combined impact of our
to meet the directives we set for ourselves. We emphasized        productivity and differential business management.
cash generation which is critical to maintaining our financial
                                                                  We will hold ourselves accountable for the following:
strength. We generated $3.4 billion in free cash flow, and
                                                                   Over the period 2009 to 2012, our target compound
delivered on our goal to offset lower cash earnings with
                                                                    annual growth rate (CAGR) for sales is about 10 percent,
capital productivity. We achieved $1.1 billion in fixed cost
                                                                    and for earnings, our target CAGR is about 20 percent.
productivity--$750 million of which will be permanent.
We lowered the breakeven point for our company which
                                                                   We will deliver another $1 billion in fixed cost productivity
gives us added strength for growth in 2010.
                                                                    and $1 billion in working capital productivity over the next
                                                                    three years. Cost productivity is now a way of life
We delivered these results through creative and dynamic
                                                                    at DuPont.
actions:
 We redeployed hundreds of employees into mega-projects           We will continue research and development investments,
  focused on productivity in inventory and receivables              which will vary by reporting segment from 2 percent to
  which allowed us to retain highly talented people during          9 percent of sales, so our businesses can add value and
  the downturn and create real value for our company.               create new ways to solve customer problems.
 We reorganized the company, eliminating cost structurally        We will allocate research and development funds, capital
  and building capability by integrating our 23 strategic           expenditures, and sales and marketing expenditures to the
  business units into 13 businesses. We removed layers              opportunities that offer us maximum growth.
  of management and moved decision-making closer
                                                                   We will extract maximum return on our investments by
  to our customers. The resulting realignment reduces
                                                                    being paid for the value of new products that we bring to
  organizational complexity, provides greater transparency,
                                                                    the market or for extending the use of existing products
  increases productivity, and allows DuPont to be more
                                                                    into new markets, new applications and new geographies.
  nimble and more responsive to customer needs.
The Needs of the World Drive Our Science and Marketing                   Tyvek protective apparel is used by workers in a wide
                                                                         range of environments. Our Building Innovations materials
Global population growth and associated growth in
                                                                         incorporate protection into new and existing structures.
the middle class are creating distinct needs throughout
                                                                         Our Sustainable Solutions business offers consulting
the world. These needs, which we call megatrends, are
                                                                         and training that help our clients improve the safety and
providing DuPont the opportunity to bring our unmatched
                                                                         effectiveness of their organizations and operations. We
science to the marketplace to create game-changing
                                                                         also provide unique technology that supplies clean air,
solutions in constant collaboration with our customers
                                                                         clean fuel and clean water to our industrial customers.
and key partners.
                                                                         Growth in emerging markets--We're taking the power of
Each megatrend presents opportunities for DuPont's
                                                                          DuPont innovation, partnering with local customers and
collaborative science and solutions:
                                                                          putting it to work in China, India, Brazil and other emerging
 Increase food production--Estimates are that the world
                                                                          economies. We expect our strategy of putting decision-
  will have to nearly double food production by 2050. We
                                                                          making responsibility and development capability closer
  are committed to working closely with farmers around the
                                                                          to our customers in emerging markets will enable DuPont
  world to significantly increase corn and soybean yields
                                                                          to exceed trend-line growth in these markets in 2010 and
  over the next decade with seeds that are higher yielding,
                                                                          achieve $12 billion in revenue by 2012.
  more drought tolerant, more nitrogen-use efficient and
  more resistant to insects and disease. Other DuPont                   Market-driven science will continue to be the cornerstone of
  solutions include herbicides, insecticides and fungicides,            what sets DuPont apart. No competitor collaborating with
  nutrition and health products, and packaging materials                our customers and partners has DuPont's power to innovate
  that keep food safe and fresh, longer.                                in response to the needs of the world. We face the future
                                                                        with confidence because the people of DuPont have always
 Decrease dependence on fossil fuels--DuPont is uniquely
                                                                        been able to take science and technology, collaborate and
  positioned to address the rising demand for secure,
                                                                        create solutions to market needs, and turn innovation into
  environmentally sustainable and affordable energy
                                                                        profits for our customers and our shareholders.
  sources. We have a strong record in energy conservation.
  We have products that help improve energy efficiency in
                                                                        That's our formula for success, and we will continue to
  building construction and provide lightweight solutions
                                                                        build on it.
  in the transportation industries, and a growing suite of
  solutions across alternative energy applications including
  novel biofuel technology and photovoltaic materials.
 Protect people, assets and the environment--We continue               Ellen Kullman
  to develop new capabilities for products like our DuPontTM            Chair of the Board and
  Kevlar and Nomex advanced fibers which are recognized               Chief Executive Officer
  globally for applications in protective apparel for law
  enforcement, first responders and the military. DuPontTM              March 1, 2010