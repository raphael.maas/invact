AS ONE, WE REMAIN STRONG

At ONEOK, we are opportunity driven
and value focused.
As crude oil and natural gas
producers continue to experience
significant improvements in efficiency
and technology, we have proven
time and time again that we are
committed to investing in energy
infrastructure alongside our
customers, while providing long-term
value to our stakeholders.
On June 30, 2017, ONEOK completed
the merger transaction with ONEOK
Partners, the operating company and
master limited partnership that has
served us well. ONEOK emerged from
the transaction as a company with an
approximately $30 billion enterprise
value, and while our corporate
structure may have changed, our core
business operations and strategies
have remained the same:
� Operate our existing assets safely,
reliably and in an environmentally
responsible and sustainable manner.
� Maintain a disciplined approach
to increasing fee-based earnings,
proactively managing our
balance sheet and maintaining our
investment-grade credit ratings.
� Deliver quality service to our
customers and focus on organic,
attractive-return growth
opportunities that enhance our
company�s ability to provide the
most value for our investors.
� Attract and retain a diverse group
of employees to execute on our
key strategies.
The transaction combined two
successful companies, further
strengthening our balance sheet,
lowering our cost of funding and
increasing our access to capital. Since
the closing of the merger, we have
announced more than $4 billion of
investments for new capital-growth
projects that are expected to meet the
needs of producers and customers who
need our midstream services to deliver
their products to the marketplace.
We expect these projects to further
strengthen our position as a premier
midstream service provider and
generate stable and growing cash
flows through fee-based earnings.
The merger also provided a clearer
view into future growth and value
to investors, as we expect annual
dividend increases of 9 to 11 percent
through 2021.
You�ll learn more about our 2017
financial performance and slate of
announced projects later in this report.
This year of tremendous success
in a challenging environment is
attributable to the hard work of
our dedicated employees, who
continue to uphold our company
values and execute on our key
strategies. Their commitment to
operating reliably and responding
to those in need was on full display
this past year as we, along with
our neighbors on the Gulf Coast,
weathered Hurricane Harvey. Their
preparedness, dedication, quick
thinking and resilience ensured that
we continued to operate safely and
provided needed services to our
customers and support to nearby
communities during the storm.
Thank you to our board of directors for
its guidance as we continue executing
on our growth strategy. And, thank
you to Kevin McCarthy, who resigned
from the board in May 2017 due to
increased responsibilities as chairman
of Kayne Anderson Acquisition Corp.
Kevin�s experience and leadership were
invaluable as we navigated the energy
industry�s most recent downturn.
And finally, thank you, investors,
for your continued trust and
investment in ONEOK.

John W. Gibson
Chairman

Terry K. Spencer
President and
Chief Executive Officer

