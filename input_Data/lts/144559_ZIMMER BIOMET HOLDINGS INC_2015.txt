To Our Stockholders:
By all accounts, 2015 was a transformational year for our Company, marked by a landmark combination
that started a new chapter in our rich 89-year history. Through our merger with Biomet, which closed
on June 24, 2015, we formed Zimmer Biomet, a leading innovator and strong global competitor in
the musculoskeletal healthcare industry. As one company, we will continue to partner with the clinical
community and healthcare systems to advance the frontiers of treatment and meet the unique needs
of patients and providers in the evolving healthcare landscape. We will also maintain our steadfast
commitment to driving exceptional stockholder value. Simply stated, we are excited and optimistic
about the future!
With a shared history and culture of musculoskeletal innovation, the combination of Zimmer and
Biomet has resulted in a �best of both� organization that draws from the exceptional talent, resources
and proven practices of the legacy companies. Although the merger closed mid-year, our critical
integration planning began in 2014, which enabled our joint teams to act quickly in realizing the benefits
of the combination. Our success in achieving our 2015 goals is a testament to the effectiveness of our
combined teams, and has set the stage for accelerated future growth.
Notable highlights from 2015 include:
� Substantially completing commercial integration: During the second half of the year, Zimmer
Biomet accomplished a number of significant integration milestones across our global sales
organization, enabling the Company to cross-sell in nearly every category of our combined product
portfolio. As a result, we delivered steady revenue growth for 2015. We believe Zimmer Biomet is
prepared to accelerate top-line performance in the year ahead, backed by the musculoskeletal sector�s
most comprehensive range of clinical offerings and the strength of a focused and growth-driven
sales organization.
� Expanding the Company�s product offerings with enhanced R&D investment: In 2015, Zimmer
Biomet continued to innovate across the full spectrum of musculoskeletal care with the addition of
new service- and solution-oriented offerings, advancing our objective to become the single-solution
provider at every stage of the continuum of care. Our combined R&D investment represented
a substantial increase over Zimmer�s standalone spend, setting the stage for broader and more
differentiated commercial innovation.
� Delivering strong financial results: Consolidated sales for full-year 2015 totaled $6.0 billion, with
fully diluted adjusted earnings per share of $6.90, representing an increase of nearly 8 percent over
the prior year. In addition, our global integration teams successfully outperformed against our initial
net synergy targets. Our outperformance against our initial net synergy targets contributed to a fifth
consecutive year of adjusted operating margin expansion and the ongoing improvement of our SG&A
ratio, which has decreased by 220 basis points over the past five years. We remain on-track to achieve
$350 million of net synergy savings by the conclusion of the third year of our combination, with $155
million of those savings expected to be realized by the end of the second quarter of 2016.
� Returning capital to stockholders: With our teams on-track to meet our synergy and growth targets,
we were able to return a substantial portion of free cash flow to our stockholders, while enhancing
investments in our strategic growth drivers. In 2015, we paid $157.1 million in dividends and
repurchased 1.4 million shares of our common stock for $150 million, or nearly 0.7% of the Company�s
outstanding shares. Our stock repurchase program has now delivered $6.3 billion into the hands of
our stockholders since 2005. 
A Year Marked by Clinical Innovation
As we prepare to bolster our musculoskeletal portfolio with innovative new product releases in
2016, it is important to reflect on the clinical legacy that has formed the basis of our ongoing
success. 2015 marked the 20th anniversary of our clinically trusted NexGen� Knee Replacement
System, which has surpassed five million implantations over two decades and built an outstanding
reputation for its revolutionary design and unsurpassed clinical success. In 2016, we will celebrate
the 40th anniversary of the Oxford� Partial Knee, which allows patients to retain up to 75 percent of
their healthy knee, resulting in faster recovery, more natural motion and higher patient satisfaction.
The NexGen Knee remains one of the most trusted systems in total knee replacement with
continued success in the market and the Oxford Partial Knee is the most widely used and clinically
proven partial knee in the world, highlighting our commitment to solutions that make a meaningful
difference for our surgeon customers and their patients.
Key product introductions during 2015 included:
� Releasing the modular XtraFix� External Fixation system, which supports time- and cost-savings
in the operating room that can allow the surgical team to devote more focus to achieving
excellent outcomes.
� Releasing the G7� OsseoTi� Acetabular Shells, which introduced 3D printing technology to
the G7 Acetabular System. OsseoTi technology uses human CT data, in combination with 3D
printing, to build a structure that directly mimics the architecture of human cancellous bone.
By applying this process to the G7 portfolio, we are able to offer surgeons the benefits of highly
porous technology with more options to resist dislocation than any other acetabular system on
the market today.
� Expanding the use of our Vivacit-E� Advanced Bearing Technology to include liners for our
market-leading Trabecular Metal� Reverse Shoulder System. Vivacit-E Technology also
celebrated a major clinical milestone in 2015. This platform technology demonstrated significant
wear-rate reduction after 96 million use cycles of in vitro testing in our hip acetabular liners; we
believe this reflects low-wear stability and durability over an anticipated lifetime following total
hip replacement. These results underscore the compelling value of Vivacit-E for patients and
healthcare systems, and further validate its use across our reconstructive portfolio.
� Expanding the use of our Subchondroplasty� Procedure for subchondral foot and ankle
bone defects, which has significantly boosted the clinical audience who can benefit from this
minimally invasive treatment. We plan to continue our efforts to grow this technology and build
out its distribution in the years ahead, supported by first-rate surgeon training and education.
Comprehensive Portfolio Fueled by Innovation and Focused on Growth
Zimmer Biomet has the most complete product portfolio in our industry. We represent every
anatomical market and treatment stage along the continuum of musculoskeletal care, from early
intervention and joint preservation technologies to partial, total, revision and salvage arthroplasty
systems. Our merger is enhancing and diversifying that revenue platform. In addition to broader
franchises in our Knee and Hip categories, we now bring a stronger presence in the faster-growing
markets of Sports Medicine, Extremities, Trauma, Foot & Ankle and Dental solutions.
Capitalizing on our enhanced portfolio is of paramount importance to our future progress. To that
end, our teams worked diligently to substantially complete the integration of our global sales
organization in the fourth quarter of 2015. These actions included the appointment of experienced
sales leaders and representatives, as well as product cross-training across our combined portfolio.
This timely work has laid the foundation for 2016 and beyond, particularly as we enhance our focus
on attractive growth markets.
Our integrated sales channel is fully focused on leveraging the broad, complementary nature of our
combined offerings, such as:
� In our market-leading Knee category, three products represent leading opportunities: Persona�
The Personalized Knee System, joined by the clinically proven and bi-cruciate preserving Oxford�
Partial Knee and the Vanguard� 360 Revision Knee System.
� In Hips, we continue to leverage premium offerings such as the multi-bearing G7� and
Continuum� Acetabular Systems, in addition to a host of implant systems that include the Arcos�
Modular Femoral Revision System and the Taperloc� Complete Microplasty� Stem.
� Within our S.E.T. (Surgical, Sports Medicine, Extremities and Trauma) category, Surgical sales
have been supported by the ongoing performance of our differentiated Transposal� Fluid Waste
Management System and A.T.S.� Automatic Tourniquet System. Our Sports Medicine business
has achieved growth with our Gel-One� Hyaluronate and Subchondroplasty early intervention
treatments. Our Comprehensive� Total Shoulder System and Nexel� Total Elbow continue to build
our presence in Extremities. Furthermore, the attractive opportunities within our combined Trauma
portfolio include the DVR� Crosslock Distal Radius Plating System and the Natural Nail� System.
� In Dental, our market-leading regenerative product line continues to perform extremely well.
� And finally, our Spine and CMF category continues to benefit from innovative offerings such as
the Virage� OCT Spinal Fixation System and the Timberline� Lateral Fusion System, as well as the
TraumaOne� and SternaLock� Blu Systems.
Of course, ongoing clinical innovation remains our standing priority and guiding commitment at
Zimmer Biomet. This is the lifeblood of our Company, and the cornerstone of our corporate culture.
In the year ahead, we plan to meaningfully contribute to our growth with an enhanced cadence of
differentiated new products, technologies and services across our musculoskeletal portfolio. This
strategy is designed to support our sustained top-line growth and long-term value creation.
Strengthening our Legacy of Operational Excellence to Drive Stockholder Value
Over the past five years, we have continuously expanded adjusted operating margins through the
achievement of process-driven improvements in all aspects of our global business, as part of our
culture of operational and quality excellence.
Moreover, since announcing the Zimmer Biomet transaction in the second quarter of 2014, we have
modeled significant net operational synergies into our combination. Our integration teams also
made noteworthy progress executing against our well-defined roadmaps to achieve cost savings
and efficiencies. These programs have included our ongoing sourcing initiative to more effectively
leverage our global purchasing power, the implementation of shared service centers and a more
streamlined management structure. In 2015, these programs contributed to our solid adjusted
earnings and adjusted operating margin performance for the year. 
Our achievements on the bottom line have ultimately dovetailed our capital strategy, which drives
stockholder value with a focus on disciplined capital deployment and M&A assessment. Based on the
progress of our integration, in the fourth quarter of 2015 our Board approved the repurchase of $150
million of shares. We also reduced the leverage on our balance sheet in the second half of the year
with an additional $500 million payment against the term debt borrowed to help fund the Biomet
merger. Looking ahead, the pace of our integration gives us optimism concerning our ability to
capitalize on future opportunities. We expect that robust cash flows will continue to fuel the growth
of our stockholder programs, as well as our disciplined approach to capital investment.
Zimmer Biomet: A New Leader in Musculoskeletal Healthcare
We are confident that Zimmer Biomet is positioned for a strong trajectory of clinical innovation
and topline performance, as we continue to pursue excellence in the areas of customer service,
operations and quality. We are motivated and excited for the opportunities to maximize the benefits
of an unparalleled musculoskeletal portfolio, a world-class sales organization and a robust pipeline of
innovative new commercial introductions. With a complete focus on these strategic priorities, we will
strive to continue creating and returning value to our stockholders.
Our team members are integral to our success, and our combined corporate culture has been a
key component of our achievements in 2015. This leadership team is proud to work alongside our
colleagues around the globe, in an industry that restores mobility and improves the lives of millions
of people. We join them in thanking you for the privilege to continue working toward that shared
vision, and for your ongoing support for Zimmer Biomet.

Sincerely,

David C. Dvorak
President and
Chief Executive Officer
Larry C. Glasscock
Chairman of the Board