Dear Shareholders,

In my first annual letter to you, I underscored three words  courage, focus and determination.
At that time, following one of the worst economic downturns in decades, I shared our clear
strategic direction to maximize our opportunities and restore our company as a strong
business that could consistently grow over the long-term. In the months and years that
followed, through courage, focus and determination, we were bold about making the right
moves to fuel our success:

       focus solely on the Harley-Davidson brand
       expand globally
       attract new customers in the U.S.
       transform our manufacturing, product development and retail capabilities




Our relentless commitment to be customer-led in all we do is key to these strategies and our
purpose of fulfilling dreams of personal freedom. It is a commitment we are passionate about,
and it is more important than ever as we aim to realize our full potential as a growing, global
company.

Our many accomplishments over the last several years, including 2014, demonstrate the
power of being customer-led and prove our strategic direction was--and still is--the right
one. Together, we are building on our strategies, driving continuous improvement across the
entire organization and continuing to manage the business for the long-term.

2014 Financial Performance

Through the outstanding efforts of our employees, suppliers and dealers worldwide, we
delivered another year of strong financial performance in 2014. These results speak directly to
the gains we have made as a more flexible and agile organization and our focus on keeping
customers at the forefront of all we do. For the full year:

       Diluted earnings per share increased 18.3 percent, marking our fifth consecutive year of
       double-digit percentage EPS increases.
       Net income increased 15 percent as consolidated revenue topped $6 billion for the first time
       since 2006.
       Dealers worldwide sold 267,999 new Harley-Davidson motorcycles, a 2.7 percent increase
       over 2013, driven by a 5.4 percent increase in international sales.
       Gross margin and operating margin increased to 36.4 percent and 18 percent, respectively.
       The company continued to return value to shareholders, increasing its dividend 31 percent
       and repurchasing $604 million in shares of company stock on a discretionary basis.

"Our many accomplishments over the last several years, including 2014, demonstrate the power of
being customer-led and prove our strategic direction was--and still is--the right one."



Extending the Reach of the Brand
We achieved great success in leveraging the opportunity to expand globally while attracting
new customers in the U.S. In 2014, in line with our strategy, we grew sales to both
international and outreach customers while maintaining our leadership with core customers
and strengthening our competitive position in all regions. In 2014:




        For the third straight year, Harley-Davidson grew U.S. retail sales to outreach customers,
        which includes young adults, women, African Americans and Hispanics, at more than twice
        the rate of the growth in sales to core customers.
        International retail sales of new Harley-Davidson motorcycles grew more than 5 percent and
        accounted for more than 36 percent of total retail Harley-Davidson motorcycle sales, with
        dealers in the Asia-Pacific, EMEA and Latin America regions posting their highest new retail
        motorcycle sales on record for each region.
        In the Asia-Pacific region, Australia, New Zealand, China, India and Asian emerging markets
        posted their best years ever. Sales in India doubled, driven by the availability of Street.
        Harley-Davidson reclaimed the number one market share position in Japan, our second
        largest market in the world.
        In Europe, sales grew in nearly every market as dealers leveraged the strength of Project
        RUSHMORE.

2X Harley-Davidson grew U.S. sales to outreach customers, at more than twice the rate of sales to
core customers.




This success would not be possible without the dedication of our more than 1,460
independently owned Harley-Davidson dealerships in more than 90 countries. Every day, our
dealers help fulfill our customers' dreams of personal freedom by delivering quality products
and a true Harley-Davidson experience that is trusted, personalized and social. Since 2009, we
added 136 new international dealerships, including 18 in 2014, meeting our goal to open 100-
150 new dealerships from the end of 2009 through the end of 2014. Approximately two-thirds
of these dealerships are in emerging markets.

Extending the reach of our brand is about more than selling motorcycles and growing our
world-class dealer network. It's about inviting more people to experience the bond Harley-
Davidson riders share regardless of generation or culture. H.O.G. membership worldwide is
now more than 947,000-members strong and is growing in new and existing markets. We also
continue to bring new riders into the brand through the Harley-Davidson Riding Academy,
where in the U.S., our dealers trained more than 39,000 students in 2014 and more than
400,000 since 2000. The program is also growing outside the U.S., with dealerships in China,
South Africa and Mexico training more than 500 students during the first full year in
operation.
Motorcycles and Momentum

Since 2009, we have maintained a relentless focus on continuous improvement and operating
efficiencies in product development and manufacturing. Not only do these efforts contribute
to bottom-line performance, initiatives like surge production, now in its third year, give us
greater flexibility to respond to customer demand and deliver the right motorcycles at the
right time in the right place.

Our 2015 model year lineup continues to build on the momentum of recent years' successfu l
launches from both a manufacturing and product standpoint. Leading up to the 2015 model
launch, the model year changeover in our plants was the smoothest in recent history, which
speaks to our many advances on the manufacturing front and continued strong results in
operating performance.

The 2015 model year lineup demonstrates our focus on bringing high-impact, high-demand
motorcycles to market. With seven new Project RUSHMORE bikes--including
FreewheelerTM Electra Glide Ultra Classic Low, Ultra Limited Low, CVOTM Street Glide
and return of the Road Glide --we built on the momentum and success of the 2014 Project
RUSHMORE launch and reinforced our commitment to customer-led design through these
amazing, innovative touring motorcycles.




"In addition to introducing our 2015 model year lineup, we surprised the industry and shattered the
idea of what a Harley can be with Project LiveWireTM  Harley-Davidson's first electric motorcycle.
While not for sale, Project LiveWire was specifically designed to get insights into the features and
experience riders would expect from a no-excuses electric Harley."




As part of our 2015 lineup, we also rolled out Harley-Davidson StreetTM 750 and Street 500,
designed for a new generation of urban riders. Since launch, Street has been well-received in
international markets, including India where more than half of all Harley-Davidson
motorcycles sold in 2014 were Streets bikes and where the bike was named 2015 Motorcycle
of the Year. In the U.S., the majority of initial U.S. Street purchasers were new to the Harley-
Davidson brand. And with the Street 500 being used in our Riding Academy, first-time riders
have the opportunity to learn on an authentic Harley-Davidson motorcycle. We are excited
about the possibilities with Street as it expands to nearly all of our markets worldwide in
2015.

In addition to introducing our 2015 model year lineup, we surprised the industry and shattered
the idea of what a Harley can be with Project LiveWireTM  Harley-Davidson's first electric
motorcycle. While not for sale, Project LiveWire was specifically designed to get insights into
the features and experience riders would expect from a no-excuses electric Harley. In 2014,
during the Project LiveWire Experience Tour, we gave more than 6,800 demo rides across 30
stops in the U.S. The response was overwhelmingly positive. In 2015, we are taking the demo
experience to a global audience as the tour expands to Europe, Canada and Asia-Pacific.
HDFS Helps Fulfill Dreams of Personal Freedom

Harley-Davidson Financial Services provides an important strategic advantage for our
business, enabling more customers to make their dreams of owning a Harley-Davidson
motorcycle a reality. In 2014, HDFS delivered by continuing to finance a majority of new
Harley-Davidson motorcycles sold in the U.S. Internationally, HDFS entered into agreements
with local lenders, insurance companies and brokers to add 11 finance and insurance
programs in Latin America, Asia-Pacific and EMEA during the year, including retail finance
programs in Vietnam, Chile, Russia, Portugal and Finland. HDFS now facilitates 82 finance
and insurance programs in 32 countries.

82 Separate Programs in 32 Countries




Keith Wandell (left) with incoming President and CEO of Harley-Davidson, Inc., Matt
Levatich

The Road Ahead: Leveraging our Strategies, Fueled by Opportunity

As I think back on the last six years, I am proud of what our team has accomplished, but more
importantly, how it's been accomplished--through courage, focus, determination and by
working together as one company and one team, moving in one direction. Together, we built a
firm foundation for future success.



"I am proud of what our team has accomplished, but more importantly, how it's been
accomplished--through courage, focus, determination and by working together as one company and
one team, moving in one direction."




A company cannot be great without great leaders at every level of the organization. During
my time at Harley-Davidson, I had the privilege of working with some of the best leaders in
any organization. One of these leaders is Matt Levatich, who will become President and CEO
of Harley-Davidson, Inc. on May 1, 2015, when I retire from the company. Matt is a driving
force behind our strategy and success, our focus as a customer-led organization, and the
innovation and continuous improvement that are expanding Harley-Davidson's industry
leadership. Given his tremendous leadership abilities and track record, his drive for
excellence, deep knowledge of the business and passion for motorcycling, I have every
confidence in the company's future under Matt's guidance.

This leadership transition comes at a time of great brand strength and performance for Harley-
Davidson. Under Matt's leadership, Harley-Davidson will continue to implement the
strategies we established through the lens of realizing our full potential as a growing, global
company. This includes what we see as tremendous opportunities to bring bold new products
to market, extend our international reach, expand our opportunities with outreach customers
and optimize our agility in manufacturing, product development and the customer retail
experience.

With our ongoing strategic direction in clear focus, our business objectives remain extremely
relevant:

        outperform the S&P over time
        grow international retail sales at a faster rate than U.S. sales
        within the U.S., grow sales to outreach customers at a faster rate than sales to core
        customers
        grow earnings at a faster rate than revenue

We believe there are tremendous opportunities at every turn to build on Harley-Davidson's
many strengths as one of the world's most powerful brands and customer -led companies.
With the passion and talent of our leadership team and our employees, dealers and suppliers--
aligned like never before under a clear and compelling strategy--I'm convinced Harley-
Davidson will continue to deliver strong business performance for our stakeholders, expand
the reach of our brand and fulfill dreams of personal freedom for the next 112 years and
beyond.

In closing, I personally thank our employees, suppliers, dealers, customers and you, our
shareholders, for making my time at Harley-Davidson such a rewarding experience. I have
had the opportunity and privilege to get to know many great people who live and breathe this
brand. It is clear the passion is stronger and deeper than anything I could have imagined
before I joined the company in 2009.

It has been an incredible honor serving as CEO of Harley-Davidson and working side by side
with our team to lead a chapter in the storied history of this iconic company. What we have
accomplished together has been remarkable, and I could not be more proud.

It's been a great ride.

Keith E. Wandell
Chairman, President and Chief Executive Officer
Harley-Davidson, Inc.
