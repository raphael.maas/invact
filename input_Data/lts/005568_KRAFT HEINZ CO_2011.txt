Dear Fellow Shareholder:
I am pleased to report that your Company delivered
record sales, net income and operating free cash flow(1)
in Fiscal 2011 while completing key acquisitions in Brazil
and China to accelerate our dynamic growth in Emerging
Markets and expand our global reach.
Guided by a focus on driving sustainable growth that
benefits our shareholders, Heinz achieved:
 Record sales of $10.7 billion, propelled by 12% sales
growth in Emerging Markets as well as growth in our
Top 15 brands and global ketchup;
 Record net income of $990 million, an increase of more
than 14% from the previous year; and
 Record operating free cash flow of $1.26 billion.
Earnings per share from continuing operations grew
nearly 7% to $3.06 from $2.87 a year ago. Reflecting our
focus on productivity and operating discipline, Heinz also
delivered continued improvement in our gross margin
and a return on invested capital of 19.3%.
Our success in delivering consistent top-line growth
and strong operating free cash flow totaling almost
$6 billion since Fiscal 2006 has enhanced the
strategic flexibility of Heinz and provided the financial
resources to:
 Increase investments in marketing and innovation to
fuel the growth of our brands;
 Complete global acquisitions that position Heinz for
future growth; and
 Raise the annualized Heinz common stock dividend for
Fiscal 2012 by 12 cents to $1.92 per share, an increase
of almost 7%. We have now increased the dividend by
almost 80% since Fiscal 2004 and returned more than
$3.5 billion to shareholders through dividend payments
in that time span.
Overall, Heinz has delivered a total shareholder return
of more than 46% over the last five fiscal years, three
times the return of the S&P 500. Our common stock price
finished the fiscal year up 12%.
The excellent results in Fiscal 2011 reflected continued
strong execution of our proven plan to:
 Accelerate Growth in Emerging Markets
 Expand the Core Portfolio

 Strengthen and Leverage Global Scale
 Make Talent a Competitive Advantage for Heinz.
Under this plan, Heinz has delivered 24 consecutive
quarters of organic(2) sales growth despite the recession,
weak consumer confidence, global uncertainty and
rising commodity costs.
Growing Rapidly in Emerging Markets
Our acquisitions in Brazil and China provide new
platforms for delivering sustainable growth in a
rapidly changing world where billions of consumers in
Emerging Markets are discovering the quality, value and
convenience of packaged foods.
In April 2011, we acquired our first major
business in Brazil, the worlds fifth most
populated country, by purchasing an 80%
stake in the manufacturer of Quero,
a rapidly growing brand of tomatobased
sauces, ketchup, condiments
and vegetables with annual sales of
approximately $325 million. We
are greatly enthused about the
growth opportunities in Brazil as
Quero gives us a strong brand,
excellent manufacturing and
distribution capabilities, and a
long-term platform to market Heinz branded products
in a rapidly expanding economy. We expect Quero to
double our sales in Latin America in Fiscal 2012 as we
drive innovation and marketing behind the brand and
expand its distribution.
In November 2010, Heinz expanded in China by
acquiring Foodstar, a leading maker of premium soy
sauce and fermented bean curd with annual sales of
almost $100 million. Foodstar gives us a solid platform
in Chinas rapidly growing $2 billion plus retail soy
sauce market, where its Master brand holds a strong
position, especially in southern China. Following a
smooth integration, Foodstars sales have exceeded
our expectations. We plan to sustain the growth
momentum by launching new products, expanding
distribution across China
and completing Foodstars
new factory in Shanghai to
meet increased demand.

Foodstar is expected to increase our Companys annual
sales in China to more than $300 million in Fiscal 2012.
China represents a significant growth opportunity given
its 1.3 billion consumers and importance as the worlds
second-largest economy after the U.S. Heinz is well
positioned in our three core categories in China, led
by our fast-growing Infant/Nutrition business, which
delivered record sales in Fiscal 2011.
The acquisitions in Brazil and China put Emerging
Markets on track to generate more than 20% of our
Companys total sales in Fiscal 2012, up from 16% in
Fiscal 2011. The acquisitions are the latest examples
of our successful buy and build strategy in Emerging
Markets, where we have acquired and grown strong
local brands and businesses in the key markets of China,
India, Indonesia, Russia, Poland and now Brazil.
In Fiscal 2011, our growth in Emerging Markets was led
by record sales of: Heinz baby food in China; Complan
and Glucon-D nutritional beverages in India; ABC
sauces in Indonesia; and Heinz Ketchup and baby cereal
in Russia, where we hold the number-one position in
both categories. Overall, Heinz has become the most
global U.S.-based food company, with almost two-thirds
of our sales generated outside the U.S. and leading
brands across six continents.
Expanding the Core Portfolio Globally
While Emerging Markets have become the most
powerful growth catalyst for Heinz, Developed Markets
remain the core of our business. Developed Markets
generated 84% of our sales in Fiscal 2011, led by
North American Consumer Products and the
U.K. We see global ketchup and consumer-focused
innovation and marketing as keys to growing our Core
Portfolio in Developed Markets.
Ketchup delivered organic(2) sales growth of almost 4%
globally as Heinz held the number-one share in the U.S.
and six more of the worlds Top 10 ketchup markets.
During Fiscal 2011, we launched Heinz Dip & Squeeze
Ketchup, an innovative dual-function package that
enables consumers to peel away the lid for dipping or
tear off the tip to neatly squeeze the ketchup onto their
favorite foods. U.S. consumers are responding very
favorably to Dip & Squeeze, which contains three times
more ketchup than our traditional packets and is much
more convenient.
In the U.K., we introduced Heinz Tomato
Ketchup with Balsamic Vinegar, a great
example of innovation that is keeping
our iconic condiment fresh and relevant
in Developed Markets. In Europe,
organic sales of ketchup grew for the
year in a difficult economic climate,
led by our dynamic growth in Russia.
Looking forward, we see substantial
opportunities to drive ketchup growth
using a pan-regional approach in
Developed Markets like Europe,
where Heinz is focused on increasing penetration,
trial and usage.
Partnership with Coca-Cola
I am also pleased to report that Heinz announced a
landmark agreement in February 2011 that enables
us to manufacture Heinz Ketchup bottles using
The Coca-Cola Companys innovative PlantBottle

technology. The PlantBottle packaging (featured on the
cover of this report) is more sustainable because up to
30% of the bottle material is made from plants, unlike
traditional plastic bottles made from non-renewable fossil
fuels. Heinz Ketchup will convert globally to PlantBottle
packaging, which is still fully recyclable, starting
with a U.S. rollout of more than 120 million retail and
foodservice bottles this summer. This partnership with
Coca-Cola will help further our sustainability efforts.
Consumer-Focused Innovation
Our focus on innovation that delivers added
value and convenience extended beyond
ketchup. In the U.S., we introduced T.G.I.
Fridays single-serve entres to expand our
growing restaurant-quality line; new Weight
Watchers Smart Ones breakfast wraps;
and Classico Light Creamy Alfredo sauce.
In the U.K., we launched resealable Fridge
Pack Beanz ; Heinz Lemon & Black
Pepper Salad Cream; and HP
Sauce with Guinness, a new twist
on a British classic. In other regions
of the world, we introduced our first
line of Heinz canned vegetables in
Russia; new varieties of ABC soy
sauces in Indonesia; and Heinz Infant Formula
in China.
To support our brands, Heinz has increased marketing
investments around 60% since Fiscal 2006 and
significantly increased our presence in social media
to capitalize on its growing global impact as a forum
for consumers.
Fueled by innovation and effective marketing, reported
sales of our Top 15 brands grew nearly 3% for the year
and generated 70% of the Companys total sales.
Finally, Heinz ranked first in customer satisfaction among
food manufacturing companies for the 11th consecutive
year in the American Customer Satisfaction Index, an
achievement reflecting our focus on quality, innovation
and value.
Making a Healthy Difference
As a Company dedicated to the sustainable health
of people and the planet, Heinz is also combating
iron-deficiency anemia, a health threat that impairs
development and can increase the risk of dying before
the age of five. Our non-profit Heinz Micronutrient
Campaign is providing nutritious powders that deliver
essential vitamins and minerals to infants and children
when stirred into common foods like rice. The campaign
has assisted 5 million children in 15 developing countries
while expanding for the first time to Africa and Haiti.
Separately, Heinz is making good progress under our
global sustainability initiative to cut greenhouse gas
emissions, solid waste, water consumption and energy
usage by at least 20% by Fiscal 2015. We are on track
to achieve or surpass those goals and will review our
progress in our Corporate Social
Responsibility Report this fall.
Outlook for Fiscal 2012
and Fiscal 2013
On May 26, 2011, Heinz announced
our two-year plan and outlook for
Fiscal 2012 and Fiscal 2013. Under this plan, we will
invest in productivity initiatives in the first year and
continue to upgrade processes and systems on a
global scale to enhance our manufacturing efficiency,
partially offset rising commodity costs and drive
sustainable growth.
Excluding the impact of one-time productivity
investments, Heinz expects to deliver Fiscal 2012
constant currency(3) earnings per share in the range of
$3.24 to $3.32. In Fiscal 2013, fueled by our investments,
we expect to deliver even stronger growth, with constant
currency earnings per share in the range of $3.60 to
$3.70. The Company has also raised our long-term
outlook for earnings per share growth to a range of 7% to
10% on a constant currency basis.
In closing, I would like to acknowledge the Board of
Directors, our strong management team and our 35,000
dedicated employees for their commitment to driving the
sustainable growth of Heinz over the past six years and
delivering winning results in Fiscal 2011. Finally, I want
to thank you for investing in Heinz, one of the bestperforming
companies in the packaged foods industry.
William R. Johnson
Chairman, President and Chief Executive Officer