Dear Fellow Stockholders:
The year 2013 was a
transformational one for our
Company in many ways. We
recently celebrated our 20th
anniversary as a public company
and it is remarkable how our
Company has developed over
those years. On March 9, 1994,
Macerich went public with a total
market capitalization of about
$714 million and as of December
31, 2013, our total market
capitalization was approximately
$15 billion. I am pleased to report
that today we are in the top 10%
of all REITs as measured by total
enterprise value.
As I look back over those 20
years, I am proud of our progress
and accomplishments across all
aspects of our business. We have
grown from a base of 12 malls
to over 50 malls and now own
some of the most productive
and iconic retail venues in the
country. Our mall productivity
over this 20-year span has more
than doubled with our current
sales per square foot at $562.
One of the key highlights in our
history occurred in 2013 when
our Company was added to the
S&P 500 Index. When we went
public, Macerich was not even
one of the top 25 mall owners in
the country. Today we are clearly
recognized as one of the top mall
companies in the U.S.
We also are especially proud that
Macerich ranks in the top 25% of
all publicly traded equity REITs
in terms of our total stockholder
return over our 20-year history
and, in the most recent five years,
our total stockholder return ranks
in the top 10% of this REIT group.
Overall, 2013 was a particularly
outstanding year and we achieved
many of the primary goals that
we established for our Company
as we emerged from the global
financial crisis of 2008 and 2009.
You may recall that in spring
2009, our Company elected not
to sell survival equity as we
believed it would unfairly dilute
our stockholders on a permanent
basis. Instead, we pursued a
multi-faceted strategy to initially
generate capital through non-core
asset dispositions and propertyspecific joint ventures. We later
issued equity but at much higher
prices than were available in
spring 2009. This financial crisis
taught us many lessons and
we transformed our Company
into one with a strong balance
sheet in terms of our assets
as well as our liabilities and
equity. We learned our fortress
centers  the must have assets
for retailers and shoppers 
performed significantly better
during that severe recession
than our lesser quality assets.
The lessons we learned from
2009 will never be forgotten. As
a result, we have substantially
upgraded the quality of our asset
base through the acquisition
of properties, including Kings
Plaza and Green Acres, the
consolidation of partnership
interests in Arrowhead Towne
Center and FlatIron Crossing, and
the development of world-class
assets such as Fashion Outlets
of Chicago.
Although we had already
substantially improved our
portfolio, we decided in 2013
to pursue a strategy of further
upgrading our asset base by
disposing of additional noncore assets  those assets with
lower sales productivity and
slower growing net operating
income (NOI). We successfully
disposed of eight malls and one
office complex during 2013 and
generated gross sales proceeds
of $856 million. Effectively, the
entire amount of these proceeds
was used to deleverage our
balance sheet. As a result of our
accomplishments, our Company
has the strongest balance sheet
in our history.
The composition of our A
quality assets dramatically
improved as a result of these dispositions. At December 31,
2013, after giving effect to our
dispositions, acquisitions and
new development, our portfolio
consisted of interests in 54
regional malls averaging sales
per square foot of $562 compared
to 61 regional malls averaging
sales per square foot of $517
at December 31, 2012. Our top
40 malls at December 31, 2012,
generated 77.9% of our pro rata
NOI. By December 31, 2013, our
top 40 malls generated 89% of
our pro rata NOI.
The short-term balance sheet
impact of our dispositions was to
pay off our line of credit and other
debt. Long term, we successfully
rebalanced our portfolio and will
recycle that capital by expanding
and redeveloping our highly
productive, core assets. As I
later outline, our development
and redevelopment projects are
focused on our best and most
productive properties.
Several other companies,
particularly in the mall sector,
pursued the separation of their
core and non-core assets through
either spin-offs or corporate
reorganizations. We elected not
to follow this strategy because
we wanted to retain the proceeds
from our non-core dispositions
to benefit our stockholders by
investing those proceeds into
our proven centers and new
centers, such as Fashion Outlets
of Chicago. Our plan is to focus
on fewer but more dominant
assets in densely populated urban
markets. As several analysts have
noted, our Company has greater
population density per mall than
any other mall company in the
U.S. This is important to note as
ease of shopping and proximity to
retail stores become even more
critical in the years ahead with
retailers, through their omnichannel strategies, implementing
same-day delivery and click-andcollect initiatives which allow
customers to buy goods online
for pick-up at their local stores.
Developments and
Redevelopments
During 2013, Macerich made
substantial progress on our major
development and redevelopment
projects that we believe will fuel
our Companys growth over the
years to come. As we move into
2014 and beyond, we will reinvest
our capital into exciting new
development, redevelopment and
expansion projects.
Fashion Outlets of Chicago. The
most notable development of
2013 within our portfolio was
Fashion Outlets of Chicago.
This breakthrough outlet project
opened in August over 90%
leased, with fashion-leading
anchors Last Call by Neiman
Marcus, Bloomingdales
The Outlet Store, Saks Fifth
Avenue Off 5th and Forever 21.
In addition, the tenant roster
included luxury outlet retailers
Longchamp, Brunello Cucinelli,
Prada, DVF, Moncler, Gucci,
Giorgio Armani, Michael Kors,
Coach, Coach Mens and Tory
Burch. We fully anticipate that
this center will be one of the
top centers of our portfolio
as measured by sales per
square foot.
Tysons Corner Center. We made
significant progress toward the
2014-2015 opening of the office,
residential and hotel towers at
Tysons Corner. We successfully
signed leases for a substantial
portion of the office tower and
it is currently more than 70%
committed, with Intelsat, the
worlds leading provider of
satellite services, as our marquee
anchor. We strongly believe that
our office tower will open in
the third quarter of 2014 more
than 80% leased and with rents
above our original projections.
Tysons 300-room Hyatt Regency
hotel along with our 430-unit
residential tower are scheduled
to open in spring 2015, coupled with a new 1.5-acre plaza that
will provide Tysons Corner with
direct access to the expanded
Metro Silver Line opening in
summer 2014. We are already
reaping the benefits of our
Tysons development and see the
positive impact on retail rents for
the shopping center. The retail
community is recognizing the
dramatic impact the densification
of Tysons Corner will have on
overall sales productivity and
traffic. We remain confident
that upon completion, this
development will be one of the
premier projects of its kind in
the U.S.
The Shops at North Bridge.
Another major addition to
our portfolio in 2013 was the
December opening of the
nations second Eataly at The
Shops at North Bridge. This
63,000 square-foot, dynamic
artisanal Italian food and wine
emporium is already changing
the retail traffic flow on Michigan
Avenue and generating additional
retail interest for North Bridge
and our surrounding properties.
In addition, Eddie Vs restaurant
and a new mall entrance were
added to this center during 2013.
Fashion Outlets of Niagara Falls
USA. We began construction
on the 175,000 square-foot
expansion of this outlet center in
2013 and had lease commitments
for approximately 71% at yearend. During 2014, we anticipate
substantially completing the
expansion of this already highly
successful outlet center.
Broadway Plaza. We recently
began a $260 million expansion
of this extremely productive
retail center in Walnut Creek,
California, that currently has sales over $700 per square
foot. We will be expanding
this open-air center by adding
approximately 235,000 square
feet of net new shop space to
the existing 776,000 square
feet currently anchored by
Nordstrom, Neiman Marcus
and Macys. We are recreating
this new retail complex by
demolishing about 80,000
square feet of existing retail
space as well as two older,
inefficient parking structures
which will be replaced with a
modern, five-level parking deck.
This project will open in phases
beginning in late 2015.
Santa Monica Place. We are very
excited to have recently received
approval to add a 48,000
square-foot state-of-the-art,
12-screen ArcLight Cinemas to
the third-level entertainment
and dining deck of Santa Monica
Place. We are also honored that
Santa Monica Place received
the most prestigious design
and development award from
the International Council of
Shopping Centers in 2013,
winning the Best-of-the-Best
VIVA Global Design and
Development Award which
recognized Santa Monica Place
as the best retail center built in
the world. While we are pleased
with our initial results, we are
confident that Santa Monica
Place has an even brighter
future with our planned theatre
addition, which we believe will
drive significant incremental
traffic and sales productivity to
this iconic center.
Los Cerritos Center. We will
begin the redevelopment of Los
Cerritos Center in 2014 with the
addition of a new state-of-theart theatre and a major sporting
goods junior anchor. Los Cerritos
is a very successful center with
sales per square foot of $674
and we are confident that these
additional anchors will continue
to expand its drawing power.
Scottsdale Fashion Square. We
will begin a 130,000 square-foot
expansion this year at Scottsdale
Fashion Square, involving the
addition of a new major sporting
goods junior anchor and a new
state-of-the-art cinema.
Kings Plaza Shopping Center.
Even though we only recently
acquired Kings Plaza, we began
remerchandising this wellpositioned property in Brooklyn
as well as redeveloping the malls
interior and parking structure.
Green Acres Mall. Shortly after
our acquisition of Green Acres
Mall, we acquired 20 acres of
land contiguous to this center
and we plan to build more than
300,000 square feet of retail
space, which we anticipate
opening in fall 2016.
Excellence in Operations
Our operational fundamentals
are very strong, and our core
metrics reflect this robust
performance. We increased
our AFFO per share-diluted
by approximately 11% during
2013 even after factoring in the
dilution from our dispositions.
Annual sales per square
foot ended the year at $562
compared to $517 for the prior year. Our releasing spreads for
2013 were extremely positive at
17.2% and we ended the year
with mall occupancy at 94.6%.
As a result, we achieved 2013
same center NOI growth in our
portfolio of 4.1%. In addition
to overall occupancy gains and
strong releasing spreads, we
achieved this increase in our
same center NOI growth by
converting temporary leases to
permanent leases, and disposing
of our slower growing and less
productive centers. The average
sales per square foot of the
assets that we sold in 2013 was
$340, substantially below our
portfolio average. By disposing
of assets that historically have
not grown at the same rate as
our more productive assets
and redeploying the proceeds
via redevelopment into our top
20 centers, we believe we will
substantially enhance our net
asset value as well as benefit our
same center NOI growth.
Balance Sheet and Liquidity
In addition to selling
approximately $856 million of
non-core assets during the year,
we also deleveraged by selling
common equity of $171 million
at approximately $70 per share
concurrent with our inclusion in
the S&P 500 Index. As previously
noted, our balance sheet today
is the strongest it has been in
the history of our Company. We
further extended our average
debt maturity schedule to
approximately six years and
reduced our debt-to-EBITDA
ratio to 7.2x. We ended the year
with significant liquidity, with
only $30 million outstanding
on our $2 billion line of credit
and $102 million of cash on the
balance sheet.
Macro Concerns
After meeting with a significant
number of investors over the past
three months, it is clear to me
that there are macro concerns in
the regional mall industry about
the impact of e-commerce on
brick-and-mortar sales as well
as the future of JCPenney and
Sears. In terms of the impact of
e-commerce on brick-and-mortar
sales, it is evident to us that the
dot.com business of each of
our retailers is synergistic with
its other distribution channels,
whether these retailers are full
price, off price or outlet. If, for
example, you think of a retailers
e-commerce distribution channel
as a single store, and that retailer
has 500 stores, most likely the
e-commerce store will always be
the highest-volume and fastestgrowing store. The fact that the
e-commerce distribution channel
may be the highest-volume store,
however, does not mean that any
particular retail chain would ever
consider closing its brick-andmortar stores to rely solely on its
e-commerce channel. Our most
successful retailers fully believe
that each of their channels of
distribution is synergistic with
one another. We believe fortress
centers are a key component of
not only a retailers brick-andmortar strategy but also its
e-commerce strategy. I do believe
that malls and retail stores
located in the center of dense
populations, such as our current
portfolio, will have an advantage
as retailers utilize such options as same-day delivery, clickand-collect and other logistical
conveniences for their customers.
With respect to JCPenney and
Sears, we are always hopeful that
each of our anchor department
stores will be successful.
Macerich substantially reduced
the number, and relative risk,
of JCPenney and Sears stores
in our portfolio through our
recent disposition program. We
experienced numerous anchor
evolutions over the years, and,
in almost every case, when one
anchor goes out of business or
is purchased by another anchor,
the Darwinian nature of the
retail business results in greater
productivity from the replacement
anchor. We are constantly
evaluating how to upgrade the
uses of various spaces at our
properties. As we continue
to enhance our portfolio and
focus on major urban markets,
we believe that a recapture
of our anchors will result in
replacement anchors and/or uses
that upgrade the quality of these
centers over time. For example,
in 2005, Santa Monica Place
was anchored by Robinsons May
and Macys. These two anchors
together generated less business
than either Bloomingdales or
Nordstrom generate in that
center today. We believe owning
dominant, A quality fortress
malls in dense trade areas
provides us with the ability to
remerchandise both small store
space as well as anchor space in
the event of any tenant closure.
Summary
As we emerged from the severe
recession, we committed to
you that Macerich would create
a strong and much improved
balance sheet both in terms of our assets and our liabilities
and equity. We believe that we
accomplished that goal. We
significantly strengthened our
capital and liquidity positions,
bolstered the quality of our
portfolio and expanded our
business opportunities. However,
we are never satisfied and will
strive to further enhance the
productivity of our centers as
well as continue to improve
our balance sheet. As a result
of our accomplishments, 89%
of our NOI is now generated
from properties that average
sales in excess of $600 per
square foot and are located
in some of the most densely
populated and fastest-growing
markets in the U.S. We have a
very strong management team
that is focusing their attention
on recycling capital from our
less productive assets into our
proven winning assets. We are
well-positioned to refinance our
loan maturities due over the next
several years. Based on current
underwriting standards, we
believe we could generate excess
financing proceeds to completely
fund our development/
redevelopment pipeline.
Furthermore, given the high
average existing interest rates on
debt maturing in the next three
years, interest rates would need
to increase dramatically for us not
to realize a lower interest rate on
our refinancings.
I want to thank our stockholders
and our board of directors for
their guidance and support
during the year. In particular, I
would like to thank our directors
that have been with us since
we went public 20 years ago,
including our lead director, Fred
Hubbell, as well as Stan Moore,
Dr. William Sexton and, of
course, our founding directors,
Dana Anderson and Edward
Coppola. We also would like to
welcome Steven Soboroff who
was appointed to the board
earlier this year. Mr. Soboroff has
been a strong community leader
and real estate developer in Los
Angeles for over 35 years and is
currently the President of the Los
Angeles Police Commission. We
are confident that he will be a
valuable addition to our already
strong board.
I look forward to reporting to you
during the balance of this year.
Very truly yours,
Arthur M. Coppola
Chairman and Chief Executive Officer