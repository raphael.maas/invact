Letter to Shareholders
Its not always apparent when meaningful business history is being made and in auto insurance
thats not very often, but in 2011, Progressives introduction of Snapshot just might qualify.

 Im a good driver why dont I get credit for that? What
does my age or occupation tell you about my driving?
... and the list goes on as consumers struggle with group
statistics of correlation versus their sense of controllable
causes. Snapshot, and what is sure to follow in its evolution,
is a meaningful start toward personalized insurance pricing
based on real-time measurement of your driving behavior
the statistics of one.
The power of participation and personalization has strong
appeal, no better demonstrated than when we asked customers,
employees, shareholders, and directors to contribute
artwork
for this Annual eport. A simple request to draw
online a car facing to the right resulted in tens of thousands
of responsesno two the same. Objective curation of the
responses has selected a sample to highlight this report
sadly mine is absent, but the point is not diminished. The
numerosity
of responses provides a visual clue as to the
expansive power that personalization provides to segmentation
in insurance pricing.
As exciting as Snapshot is, and Ill discuss it in more detail
later, there is much more to reflect on for Progressive in 2011.
For the first time in Progressives history net premiums
written topped $15 billion, surpassing that mark in the closing
days of the year. Earned premiums, recognized on a lag to
written, finished around $14.9 billion.
Our combined ratio of 93 was an equally satisfying result,
and while three points better than the objective we communicate
to shareholders, it benefited from what I would classify
as generally higher-than-expected favorable development
from prior year loss reserves, as those estimates were refined
based on actual payment patterns. However, net of any favor-
able
development, we outperformed our target 96 combined
ratio and comfortably handled an active catastrophe year.
Net premiums written growth for the year was 5%, or alternatively
about $670 million, not yet numbers that come
close to taxing our ability or desire to manage growth, but a
strong growth quarter to end the year was encouraging.
eporting last year
I commented that,
T he closing years of
the decade appear to
be the approximate
time frame during
which industrywide
profitability will once
again be moderated
and sustained industry
premium declines will turn positive. This appears to have
some continued veracity and for Progressive the combination
of growth and margin in the last few years supports our turn.

I favor unit counts in analyzing the health of the business
and for 2011 we increased our number of policies in
force by about 580,000 and by extension approximately
a million new customers. Combined with our internal
mandate to serve all of our nearly 13 million policies over
an ever increasing life expectancy, we would assess 2011
to be a healthy yearnotwithstanding considerable room
for improvement.
Our net income of just over $1 billion and the per-share
equivalent of $1.59, aided by share repurchases, are reasonably
comparable to the equivalent results from the prior two years.
Thankfully nothing compares well to 2008. Our objective
however, is not annual comparability but superiority. So whats
in this years results that a closer diagnostic might highlight
and support the optimism I feel for the year ahead?
Closing Speed Our fourth quarter production, a composite
of rate, new policies, and renewal acceptances, across all
products was, at 8% written premium growth, stronger than
any prior quarter for the year and, in fact, for the last 26
quarters. Its always nice to be carrying momentum as the
year closes, but any projections of what this may or may
not mean holds considerably less value than our ongoing
monthly reporting of attained results.
Our Agency business had and continues to maintain very
steady production since regaining positive policy growth in
late 2009. Meaningful trends in production in this channel
are subject to many factors, perhaps the most significant of
which is rate presentation and competitive positioning. Weve
worked diligently to ensure that, regardless of the means an
agent uses to get our rates, our best product options are always
presented. Our unwavering philosophy of continuous rate
review by state and distribution channel often means we are
taking rate ahead of competitors, but it also means that
significant rate action by others may not need to be directly
matched by us. While in many states 2011 rate changes were
moderate, we were still advantaged by our philosophy, as
several competitors in several states took needed rate actions.
Our expectation that our agent-produced policies would
reach an all-time high early in the year in-fact happened,
and we ended the year even higher, fully correcting a negative
trend in this key measure which began in 2006.
Our Direct business received a welcome lift from a very
healthy shopping trend in the early part of 2010, but momentum
was noticeably slowing by year-end and, as expected,
carried into 2011. While attracting a healthy number of
new customers in 2011, we did not match the new business
application counts achieved in 2010 for much of the year.
However, by the fourth quarter, we were once again posting
positive new application growth. New policies are essential
to maintaining a vibrant and growing book, but the flow
through to renewal status is even more critical. For the year,
we grew our renewal applications about 11%. The overall
policies in force growth for Direct auto was an acceptable
6%, but somewhat slower than we hope many of our new
product offerings would allow us to achieve.
New policies are essential to maintaining a
vibrant and growing book, but the flow through
to renewal status is even more critical.
Our market-leading special lines business, insuring motorcycles,
recreational vehicles, boats, watercraft, and snowmobiles,
finished another very successful year. We exceeded $1.2 billion
in written premiums with underwriting margins considerably
better than our targets. We look forward to an economy
where new sales of these vehicles return to prior levels. Our
special lines offerings,
along with our continued growth in
our Progressive Home Advantage program, continued to
drive our important multi-product household measures to
new highs for the year.
After several years of really tough sledding, reflecting the
economy at-large, our Commercial Auto product found
new energy as the year progressed. Declining trends in premium
and policies were arrested and reversed during the
year, and Commercial Auto finished out the year with close
to 6% written premium growth, mostly driven by doubledigit
growth in the 3rd and 4th quarters. This growth reflects
our implementation of needed pricing increases, often before
our competitors, returning unit growth through new applications,
and some expansion of product offerings. With
significant growth comes the added concern of balanced
profitability; for now, we are very satisfied that were finding
a balance that meets our objectives, but remain vigilant.
Investment Headwinds We target no specific mix of income
from underwriting and investments. Its fair to suggest, however,
that while underwriting income is very much in-line
with long-held performance objectives, total return from our
investments, a hugely important contribution to our economic
model, has been both highly variable and at times,
including this year, below the contribution we might desire
in our more global outlook for shareholder return. With that
clear recognition, and the appreciation that managing our
portfolio to a very short duration for some time has left opportunity
on the table, I continue to feel strongly, given the
climate and uncertainty, that we have taken and will continue
to take a position that will not inhibit our ability to underwrite
all the business we can at an appropriately aggressive
premiums-to-surplus ratio. Our fully taxable equivalent total
return for the year was 3.2%, less than half the equivalent
number for 2010 and the reduction was apportioned across
both the fixed-income and common stock portions of the
portfolio. Our preference is to avoid the price volatility of
longer-duration assets at the prevailing low yields, favoring
shorter duration bonds at unquestionably very low yields,
but with the opportunity for faster future reinvestment.
With perfect prospective information, we would all make
the right choice. Without it we are guided by ensuring our
underwriting franchise is a protected asset with no constraints
to do what it does best.
While low yields and highly volatile equity markets made
for challenging times for investment income, it provided
an attractive opportunity to reassess the composition of our
capital structure. We issued $500 million of 10-year senior
debt to strong market acceptance and a coupon interest rate
of 3.75%. With a strong capital position, well-structured
portfolio, solid operations, and what to us appeared to be
depressed equity valuations, including ours, we repurchased
our stock after the capital raise at an accelerated
rate. Consistent
with our long-standing policy to repurchase shares when
our capital balance, view of the future, and the companys
stock price make it attractive to do so, we repurchased 51.3
million shares, for close to $1 billion, or about 7.7% of our
outstanding shares at year end 2010. With those transactions,
our debt-to-total capital ended the year at 29.6%, inside our
30% guidance, but was further reduced upon retirement at
maturity of a $350 million tranche of our outstanding debt
in early 2012.
Our best net expression of underwriting performance
against our goals for the year is our Gainshare score. For 2011
the score was 1.1, mid-range on the 0 to 2 scale. Combining
the Gainshare score with after-tax underwriting income
and the board-established dividend target factor of 33 /%,
our variable dividend payment for 2011 was approximately
$250 million or just short of 41 cents per share.
We spend significant time and effort modeling our capital
requirements and sizing what we call layers of capital to
satisfy regulatory requirements and any manner of contingencies
we could envision for our business. Capital in excess
of these two layers is available for share repurchases, acquisitions,
and shareholder dividends. Our variable dividend
has become an effective, appropriate, and now reasonably
meaningful part of our capital management practices. The
board has determined the parameters for 2012 will remain
the same as in 2011.
With a strong and well-structured investment portfolio
and capital position, we enter 2012 certainly hopeful for an
improved investment environment and returns that more
comfortably match the underwriting contribution, but not
dependent on it.

Product Potential Product design differences between auto
insurance offerings are understandably obscure and certainly
less obvious than those of many consumer products. While
subtle for some, their importance and contribution are the
very essence of success in our business. Progressive, with some
justification, is well recognized in our industry as a leader
in product design and price segmentation. In large part, this
is the statistical matching of auto accident related costs
incurred by an individual to the premium to be paid. But
product design is much more than just price segmentation
and for us involves the consumer and agent presentation
and, increasingly so, their ability to interact with the product
by selecting and packaging combinations of coverage that
best meet their needs.
I doubt any year in Progressives history has
broadened the horizons of just what might be possible
with future product design more than 2011.
I doubt any year in Progressives history has broadened the
horizons of just what might be possible with future product
design more than 2011. The vast majority of the statistical
matching in use today is based on observable and verifiable
characteristics
of the driver and vehicle. While admittedly, its
the science of correlation and grouping, it has extraordinary
power and marginal improvements with disproportionate
gains are available to those truly committed to product research
and development. As good as the base science is, it lacked a
meaningful direct relationship to actual driving behavior.
Our work with measuring driver behavior, over now many
years, provided its share of challenges, but from the earliest
days suggested that, if possible and practical, the data set would
expand the known universe of rating segmentation. 2011
was the year that possible and practical fully merged in our
product offering of Snapshot.
Snapshot, available in both our Agency and Direct channels,
has been met with significant consumer appeal. Weve seen an
increasing number of Direct customers choosing to improve
their initial price estimate by installing our measurement
device and reporting their driving behavior. We provide the
policyholder a portal view into their trip management
when, how many, how far, and selected variables of interest,
which for us are a proxy for defensive driving. After 30 days,
we are able to make an initial estimate of a discount that
reflects the individuals driving characteristics and apply that
for immediate use. A final estimate is applied to the subsequent
renewal and the measurement device is returned and
available for reuse, although we maintain a right for future
calibration. Discounts range between 0% and 30% and the
full range is effectively used. Perhaps more importantly, and
youll excuse me that certain specifics are best closely held,
the expectation of getting a discount is far better than 50/50,
suggesting, as I think we all intuitively know, that a smaller
percentage of drivers are responsible for a disproportionate
share of the costs. Identifying just who they are is the challenge
were exploring. Acceptance
by agents on behalf of their
customers is somewhat slower, and in many ways understandably
so, but as the product feature matures, I fully expect
increased consideration and use.
Snapshot is built on top of our existing product and, as such,
it is an additional level of segmentation rather than a product
in and of itself. The segmentation provided, however, is able
to be isolated and fully analyzed and tuned. Historically, data
quantity, management, and mining have been the keys to
effective segmentation and real-time usage data will be no
different, but the amplification of each is not for the faint
of heart. The data sets are huge, requiring storage, access, and
mining techniques that challenge those that only months
ago seemed state-of-the-art. Without data, only dangerous
inferences can be drawn. Our commitment to building an
initial data set has been a disciplined undertaking over several
years and now, with a viable product in-market, our reward
of ever expanding data is tremendous. We now measure data
in miles of behavioral observation and at last count we have
the equivalent of multiple round trips to Mars.
We now measure data in miles of behavioral
observation and at last count we have the equivalent
of multiple round trips to Mars.
Snapshot and usage-based data in auto insurance rating has
been well accepted by regulators who have allowed us to
protect our proprietary algorithm in 39 states and the District
of Columbia, for which we are very appreciative. The U.S.
patent office has equally affirmed our existing intellectual
property, and has now approved one additional patent.
Consumers and those who speak for them have, with few
exceptions, been enthusiastic about the product, and privacy
concerns, so very prominent in the early years, have been
mitigated by the design and discrete monitoring periods.
Much remains to be done before the full power of usagebased
segmentation will be tapped. I sense that as good
as we feel today we will look back with some humility, in
future years, when we realize just what has been possible.
Product features and packaging, like Name Your Price,
provide consumers wishing to shop online a very real opportunity
to leverage technology to their advantage and
explore a range of options available to them. Agents have
always provided the ability for customers to reach smart
decisions on price and coverage. Now Name Your Price in
combination with Snapshot provides for a powerful and
compelling quote process quite unlike our competitors.
Ever increasing product complexity must be presented to
consumers and agents with great, easy-to-use technology
interfaces. Were fanatics about the presentation layer of our
products. We receive consistent recognition in agent surveys
as their leading company and equally consistent recognition
as the leading insurance website by Keynote. However, the
range of presentation formats has expanded greatly with
tablets and smart phones and that has our full attention. More
than 15% of search activity for Progressive, or other auto
insurance terms, is now mobile-device based and quoting
and sales attributed to mobile devices have grown at rates
that make days and weeks approximate early Internet months.
As noted last year, the potential for devices that are on your
person often with a camera and location software, is a new
and really exciting dimension for all aspects of our business
and I suggested to expect more in 2011. I had the pleasure
of being directly involved with the team leading our mobile
strategy and their progress in 2011 has been nothing short of
impressive. We talk in terms of mobilize not miniaturize,
which simply means using the capabilities versus adapting
the old product and presentation to new devices and formats.
Selecting just one initiative to build on the concept of product
design and presentation is our prototype application that
allows a consumer to use the camera in their device to send
a photo of a drivers license and similarly their current insurance
card with vehicle identification number, and in return
get an insurance quote returned instantaneously. A quote that
then may well be further personalized by Name Your Price
and Snapshot. Expect moreI think thats more...and better.
Old industries do reinvent themselves and this feels like its
going to be quite a ride.

Brand and Distribution Momentum An open question existed
over the last decade or socould we market and retail our
product with the same success we enjoyed, more as a wholesaler,
with our agents representing our consumer face?
Perhaps an even more interesting question, and for me the
correct one, Could we do so while enhancing both our
Agent and Direct distribution options?our very clear and
consistently stated objective.

Today, I believe we have an answer. Progressive is a business
generating brand. One that is more important than any
means of distribution or product feature but in fact a clear
consumer mindset about who we are as a company and
what they should expect as a customer. Our Personal Lines
business today is a very balanced book, between distribution
options, with agent-produced auto business slightly larger
at 55% of policies. Our single brand multi-distribution
company
is a unique and valuable positioning in our space.
The road was not always straight, but the result is extremely
rewarding and powerful.
esearch on consumers reactions to brands, preferences, and
advertising is extensive and often constructive, but rarely
definitive. We are, however, encouraged that consumer awareness
of our brand is both positive and closely associated with
key characteristics such as ease of doing business, distinctive
product features, and exceptional service, all consistent with
our intentions.
Ensuring congruity between internal and external brands
with actions matching expectations has been central to our
global brand development. The brand ambassador status
bestowed on Flo, our most visible employee image, is no
accident and serves as a clear behavioral model for all
the substance perhaps not the humor. The importance of
getting this right was highlighted in 2011 as Flo was voted
Americas #1 advertising icon (as named by Entertainment
Weekly). Im sure her over 3.3 million Facebook fans had
something to do with that, but the point is the same.
For 2011 we had clear objectives to advance our marketing
and advertising efforts.
We wanted the character of Flo to endure and, to do so,
sought to make the character richer and extend her range
of interactions and messages. The results are always subject
to opinion, but for me the creative developed in 2011, for
use in 2011 and 2012, was some of the best in this now long
running campaign. esults trump opinion, but I think
consumers might agree.
We had some clear ideas about a new campaign, its target
audience, and message, and tested our Messenger campaign
late in 2010. However, we found by observing the
Messengers
natural interaction style with all ranges of
people that we had an opportunity to improve on the
concept. By mid-year, we were reworking the campaign
structure. The Messenger is now carrying his message
of Progressives savings and convenience, by interacting
with prospective customers in real and unscripted settings.
Choosing settings like a gas station or baggage claim at
the airport, the Messenger can empathize with the pain
points of gas prices or baggage fees and, while not able to
solve them, is willing to share his savings on auto insurance
to help with the burden. These true slice-of-life scenarios,
where the Messenger can perform what we call random
acts of savings, provide reactions and responses that simply
could not be scripted. Were encouraged by early results and
the developing acceptance of the character fromNot quite
sure, to a guy who understands life and helps when he can
by saving it forward.
No campaign it would seem appeals to all and we are
delighted to have additional assets for use across the range
of audiences we wish to address and the media options available
to us. The most enduring of our objectives is to ensure
all our customer communication and messaging is integrated
and presents a consistent Progressive brand, be it in print,
social media, digital, or television
The media and advertising world is dynamic and expensive
and we must be judicious in all we do. 2011 was a good year
in which we took encouraging steps to ensure we have whats
necessary to continue the momentum weve been building
in our brand and consumer appeal.
The Progressive Journey
Progressive will celebrate 75 years in business this year
and, while I only have visibility into the last third, it is a
business story of some considerable note. However, in so
many ways, it feels like the story is just beginning as we
adapt to the ever changing needs of the consumer, technology,
and available data.
Our efforts to retain customers must reach the competence
we exhibit in acquiring new ones, something we estimate
we now lead.
More than just words, this guides our actions in every area
of the company. Isolating and highlighting any such action
severely understates the efforts of so many, but accepting
that risk, it seems appropriate to comment on the extraordinary
response of our Claims organization to the multiple
catastrophes of 2011. Perhaps a strange compliment, but
they made it look almost too easy. The deployment logistics
and the commitment of our people to leave their regular
routine and go where our customers needed them most,
continues to impress me no matter how familiar I am with
the planning, preparation, and expectations that are in place
for such occasions. Customers know and appreciate good
service and our efforts are rewarded with their increased
Net Promoter scores and renewal behavior following a
satisfying claims response.
Our future wont be without challenges, but the metaphorical
mountains we have climbed are meaningful; there are
bigger ones yet to climb, but, as I said at our 2011 investor
conference, not many get the view from here. I hope this
review of the year leaves you as optimistic as it does me.
Our People and Culture
We are Progressive and we are progressiveyes, a pithy and
perhaps simple statement, but for us its so much more. Its
the crystallization of our culture and what we aspire to
contribute to our industry and consumers. Its an enduring
impetus to continuously refine our work environment to
one where were all motivated to do our best work, where
we can grow constantly, and that others will want to join.
The blend of both practical
and emotional energy in the
statement, effectively communicates the responsibility and
self-imposed aspirations that come from our quest to be
Consumers #1 choice for Auto Insurance. Our people,
culture, and aspirations are what continue to make us special.
Nothing we have achieved has been without the efforts of
so many, and I thank the people of Progressive, our agents
and brokers, customers, and shareholders for their support in
making this all possible.
To all the people who make Progressive, progressive
Thank You.
Glenn M. Renwick
President and Chief Executive Officer