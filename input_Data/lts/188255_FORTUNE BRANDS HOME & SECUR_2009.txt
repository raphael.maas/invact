Dear fellow
                                        Net sales for Fortune Brands were           billion, down %, as flat sales in
                                        the relatively stable spirits category tempered the impact of lower sales

                                        for golf and home products.
                                        Diluted earnings per share from continuing operations were           , up %
        




                                        due to lower net charges.
                




                                        Excluding charges and gains, diluted earnings per share from continuing
                                        operations were         , down %, reflecting the impact of absorbing lower





                                        volumes across our cost base.
                                        Free cash flow reached         million after dividends and net capital
                                        expenditures.
                                        Total shareholder return, including dividends, was %.
                                



                                           Throughout       , we focused on two major goals: outperforming
                      


                                        our markets at both the top and bottom lines; and positioning the company
                                        for strong growth when the economy recovers.
                                           We pursued these goals through three key initiatives across our businesses:

                                                                     to adjust to the evolving consumer;
                                                                    and increasing flexibility in our operations;
                                                                            to strengthen our balance sheet.

                                           Together, these strategies are enabling Fortune Brands to emerge from
                                        the downturn in a very strong position.




                                        During the economic downturn, consumers clearly changed their purchas-
                                        ing behavior, focusing on trusted brands that deliver real value. Many pulled
                                        back from big-ticket discretionary purchases (such as major remodeling
                                        projects and golf clubs), while others shifted more of their spirits purchases
                                        from restaurants and bars to retail outlets. Still others focused purchases on
                                        lower price points, particularly in certain home products and spirits categories.
        


                                           To succeed in this environment, we saw it as critical to reinforce the
                                        strength of our brands, deliver value at various price points, and drive
                                        demand in targeted and creative ways. This included developing innovative
      


                                        new products and targeted brand-building programs to keep consumers
                                        excited about our brands and products. As consumers shifted shopping and



                                        buying behavior, we matched our initiatives to seasonal consumption
                                        patterns and the distribution channels where consumers were most active.

                                                                                 
    In        , we brought excitement to the marketplace in          with
                                                                                                
new-product innovations. The introduction of Red Stag by Jim Beam
                                                                                              
attracted new consumers to the bourbon category. Sauza Margarita-in-a-
                                                                                                     
Box appealed to consumers increasingly entertaining at home. We brought
                                                                                                     
innovations to international markets with Larios gin and DYC Single
                                                                                   
Malt in Spain, and Canadian Club ready-to-drink products in Australia.                              
While we held back on spirits brand investment as we transitioned to                              
new international distribution structures, we boosted brand investment                       
by double digits during the seasonally important fourth-quarter holiday
selling season in the U.S.
    Our spirits business also completed a number of steps that are bringing
us closer to customers and consumers, simplifying our organization,
and enhancing how we position and sell our brands. To bolster our sales
and distribution, we enhanced our organization and our routes to market:
We executed a smooth transition to our new -market international sales
alliance with The Edrington Group (owner of brands such as The Famous
Grouse, The Macallan and Brugal); simplified our international spirits
business structure by merging two units into one European business unit;
initiated new long-term incentive-based contracts with our largest distribu-
tors in the U.S.; and aligned our U.S. sales organization by distributor rather
than region. As a result of the changes we've made in sales and distribution,
we now directly control more than % of our spirits sales, up from just
 % in       .
    We believe we can perform better in the spirits marketplace, and with
these initiatives in place, we see excellent prospects to profitably grow our
key brands and elevate our long-term performance.
                      In                    , we remained proactive in the marketplace and
                  continued to outperform the category. We gained share with successful
                  initiatives, including: expanded customer relationships at major home
                  centers; advanced faucet designs made possible by the new, compact Moen
                  Duralast cartridge; revolutionary security products like the Master Lock
                  Speed Dial; and innovative garage organization products from Waterloo.
                  Even though the economy challenged big-ticket remodeling purchases,
                  we still sold more than billion of kitchen and bath cabinetry. As consumers
                  shifted cabinetry purchases to home centers, we strengthened our position
                  by developing new cabinetry lines to fill price gaps and new styles to appeal
                  to today's consumer.
Spirits
                      We've also emerged as an industry leader in promoting energy efficiency
Home & Security
                  and "green" products. For example, Moen's industry-leading line of
Golf
                  eco-friendly faucets reduces water consumption by %, and the energy
                  conservation benefits of Simonton Windows helped the brand gain share
                  as consumers capitalized on an energy-efficiency tax credit.
                      We continued to outperform the          market in        on the success of
                  our investments in two key areas: innovation and international growth.
                      New-product innovations helped us gain share at the high end of the
                  market. The Titleist Pro V1 golf ball continued to grow share as golfers
                  remained committed to the product's technological and performance
                  benefits. Advanced-performance Titleist clubs gained share and commanded
                  premium pricing. And FootJoy reinforced its industry leadership with
                  innovative products such as the new ICON and SYNR-G shoe lines.
                      Internationally, we've invested to develop the golf industry's finest sales
                  organization in Korea, as well as in growth initiatives in Japan, China and
                  Australia. In local currencies, our golf sales across Asia and Australia were
                  up at a double-digit rate in      . To further support these growth opportu-
                  nities, we're constructing a new ball plant in Thailand to complement our
                  U.S. golf ball production.


                  To help all of our businesses deliver operating margins at the forefront of
                  their categories, we continued to reduce cost structures and enhance the
                  efficiency and flexibility of our supply chains.
                      These included tough but necessary decisions over the course of the
                  downturn to take significant cost out of our businesses as sales volumes
                  declined. These moves  including a % reduction in the number of
positions and facilities in Home & Security and more modest reductions
in Golf  have resulted in competitive cost structures while also retaining
                                                                                #1 ball in golf
flexibility in our supply chains so we can ramp up as demand rebuilds.
Our spirits business has also implemented initiatives to enhance organiza-
                                                                                Titleist
tional effectiveness and supply chains, dedicating savings to reinvest in
brand growth.
                                                                                Buy it for looks.
    Lastly, we aggressively managed our cash to strengthen our balance
                                                                                Buy it for life.
sheet and financial flexibility. Our aggressive cash-management initiatives
included lower capital expenditures and a significant reduction in working
                                                                                Moen
capital. After careful consideration, we also reduced the annual dividend
rate to        per share, a prudent action that aligned our dividend with our
                                                                                Tough Under Fire
historical payout ratio. The combination of these measures enabled us to
generate our strong free cash flow for       .                                  Master Lock


                                                                                TheWorld's
Fortune Brands' performance continues to be built on a foundation of
                                                                                Finest Bourbon
powerful, enduring and profitable brands  in categories with attractive
long-term fundamentals. Moving into          , we see the front end of a        Jim Beam
recovery as an excellent time to stay on offense in the marketplace and
                                                                                #1 shoe in golf
invest to build profitable market share. We also recognize the consumer
won't recover overnight, and we're sure to face continued challenges.
                                                                                FootJoy
However, the success of your company in navigating the historic challenges
of       enhances our prospects in       and beyond. The           people
                                                                                The Brand
of Fortune Brands are inspired by the confidence demonstrated by your
                                                                                Bartenders Trust
ownership of our stock, and we will always remain focused on earning 
and rewarding  your trust.
                                                                                DeKuyper
Sincerely,
                                                                                We Make Lasting
                                                                                Impressions
                                                                                Simonton


                                                                                The Legendary
                                                                                Rum of St. Croix
Chairman & Chief Executive Officer
                                                                                Cruzan
February
