Dear Fellow Shareholders,
By any measure, 2003 was a phenomenal year for Net?ix. 
In this, our second year as a public company, we once again 
surpassed investor expectations with record ?nancial results 
in revenues, net income and free cash ?ow.
Net?ix achieved pro?tability for the ?rst time in 2003. The Company recorded revenues 
of $272.2 million, up 78 percent compared with $152.8 million for 2002. Net income was 
$6.5 million, compared with a net loss of $20.9 million in the prior year. This strong ?nancial 
performance was driven, in part, by our rapidly increasing subscriber base which grew to 
nearly 1.5 million members, up 74 percent over 2002. 
A Mainstream Phenomenon
Perhaps even more signi?cantly, Net?ix has succeeded in becoming a truly integral 
part of the consumer entertainment landscape. At year-end 2003, we believe Net?ix 
had approximately 95 percent of the fast-growing market for online DVD movie rentals.
We currently ship more than two and a half million DVDs a week, with remarkable 
reliability and precision. Customer satisfaction is high, because we have the movies that 
our subscribers want; because we personalize our website to help subscribers discover 
new movies they will love; and because we deliver those movies, generally, the very next 
business day.
The Net?ix model is based on giving todays movie consumers a combination of 
convenience, value and selection that appeals to a broad and diverse group of people  
not just avid movie fans. 
In our most popular plan, for $19.95 a month Net?ix subscribers rent as many DVDs as they 
want, and keep them as long as they want, with three movies out at a time. There are no 
due dates, no late fees and no shipping fees. DVDs are delivered by ?rst-class mail from regional shipping centers throughout the United States. The Net?ix website, from which our 
subscribers request their movies, provides extensive information about titles, as well as critic 
reviews, member reviews, online trailers, ratings and personalized movie recommendations.
Investing in The Net?ix Experience
In 2003 we invested in more inventory to improve the Net?ix subscriber experience. 
We also invested in faster delivery to our subscribers by opening more shipping centers. 
We now operate a national network of shipping centers that allows us to reach more 
than 80 percent of our customers nationwide with generally next-day delivery.
Our investments in continually improving the Net?ix experience have been rewarded 
handsomely. Customer satisfaction is at an all-time high. Churn is at an all-time low. And our 
extraordinary growth in subscribers continues to increase at a viral pace. Satis?ed customers 
tell their friends about Net?ix, these friends join, become satis?ed customers, and then tell 
their friends about the service. This growth, in turn, improves our ef?ciencies and margins. 
Were reaching economies of scale that contribute even more to the bottom line.
Continued, Accelerated Growth
All of these results make us con?dent that we will be able to reach our next goal of ?ve 
million subscribers and $1 billion in revenues in the 2006-2007 timeframe   one to two 
years earlier than our previous projections.
Many factors underlie our increasing con?dence in the business: 
First, DVD adoption continues to grow. More than one-half of all U.S. homes now have 
a DVD player, and in the space of ?ve years it is expected that virtually every U.S. TV 
household 100 million of them  will have one or more players. With todays DVD rental 
market accounting for more than $5 billion in revenue, and 2008 estimates pegged at 
$10 billion, DVD is clearly the rocket propelling studio pro?ts and growth.
The second factor underlying our increased con?dence is growing public acceptance of 
the Internet, not only as a medium for safe and secure shopping, but as an integral part 
of peoples lives. Each year, consumers are getting more comfortable using the Internet in 
more aspects of their lives, and all the leading e-commerce companies, including Net?ix, 
are growing impressively.
The large and growing barriers to competitive entry are the third factor. Very large 
companies have tried to gain a foothold in our market and have not succeeded after 
more than 15 months of effort. Our service is superior, very hard to duplicate, and our 
1.5 million subscribers are very satis?ed. We have great momentum. 
The fourth factor is that each new shipping center we open contributes to our planned 
growth in ways that are consistent with growth rates in other regions. To reach a billion 
dollars in revenues, we need approximately ?ve percent of U.S. households to subscribe 
to Net?ix. Growth in our initial market, the San Francisco Bay Area, has been very steady 
over the past four-and-a-half years. The Bay Area reached the ?ve percent penetration level 
and then kept right on growing with no signs of slowing down. It may be possible that the 
?ve percent penetration level is conservative and we may be able to reach 10 percent of 
households, or even 20 percent. If so, it would indicate that the billion-dollar revenue mark 
is not the ?nish line but merely the ?rst watering station.  
Finally, the ?fth factor behind our increasing confidence is great execution. Every week 
we ship over two and a half million DVDs from our nationwide network of shipping centers 
with incredible reliability and precision. We buy millions of DVDs every quarter and maintain 
strong relations with nearly 100 studios and distributors. We have grown steadily every 
quarter without a material operational misstep. We are vigilant about threats both long- 
and short-term. We care deeply about providing subscribers a great movie experience 
and work every day to make our service better and better.
Looking forward to 2004 and beyond, shareholders and customers alike can expect 
continued steady execution of the kind that has kept us growing at an astounding 
70 percent-plus annual rate.
New Initiatives
To fuel our ambitious growth, we will explore several new and exciting initiatives 
in the year ahead. 
In 2004, we plan to add television advertising to the mix of acquisition channels we 
use to grow our subscriber base. As with last year, online advertising and word of mouth 
will continue to generate the bulk of our new subscribers. In the fourth quarter of 2003, 
we tested TV advertising in several markets. Those test results looked promising. If TV 
advertising continues to perform in 2004, we expect a broader launch in 2005.
During 2004, we will be preparing to expand internationally in 2005. Our ?rst markets are 
likely to be in the U.K. and Canada. These are good expansion markets for us. But in the 
next few years, while the U.S. market continues its rapid growth, we expect that over time 
international operations will contribute less than 20 percent of revenues. Nevertheless, 
it is prudent and exciting for us to begin moving from a domestic to a global company.
Also during 2004, we will be developing solutions for downloading movies to consumers 
in 2005 and beyond. Our core strategy has been and remains to grow a very large DVD 
subscription business and offer our subscribers the choice under one subscription of 
receiving their movies on DVD or by downloading, whichever they prefer. We are currently 
a $100 million-plus customer of the studios, and they have been receptive to our desires 
to expand consumers movie viewing options. 2005 will be early for downloading and we 
expect only modest consumer interest initially. But we predict this market will grow steadily 
for us over the next 10 years.  
Looking Forward
With $135 million of cash and short term investments at December 31, 2003, and steady 
cash ?ow from operations, we believe Net?ix is well positioned ?nancially to achieve the 
growth objectives weve established for the business.
On behalf of the employees of Net?ix, I want to thank you, shareholders and happy 
customers alike, for your encouragement and support in helping to make 2003 such 
a successful year for the Company. I look forward to reporting again on our progress 
12 months from now.
Sincerely,
Reed Hastings
Chief Executive Of?cer, President and Co-Founder