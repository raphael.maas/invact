TO OUR SHAREHOLDERS

Strength in the Dollar Tree banner
continued with its 40th consecutive
quarter of positive same-store sales,
consistent gross margins and our
history of surprising value at $1. Our
customers continue to be delighted
when they find what they need
and surprised with a new and
ever-changing mix of product.
Our 2017 results reflect a year of strength and growth
for the Dollar Tree organization.
Dollar Tree has grown into a leader in the discount
retail sector. Our nearly 15,000 stores across North
America consist of two strong banners, Dollar Tree and
Family Dollar, which combined, serve our customers
more than two billion times a year. Our business
focuses on delivering great value, convenience and a
thrill-of-the-hunt shopping experience that resonates
with our customer base.
Our banners serve a wide range of customer
geographies across 48 states and five Canadian
provinces. Our business models are resilient and can
serve customers almost anywhere, from urban to
suburban and rural communities. Our stores distinctive
assortments come together to deliver basic needs
and convenience at Family Dollar, and essentials and
incredible values at a single price point at Dollar Tree.
Since the acquisition in July of 2015, our management
team has executed the strategy to integrate back office
services and to refine and develop the Family Dollar
business to drive greater top and bottom line
performance. The combined organization has
generated over $300 million in synergies, captured in
sourcing and procurement, format optimization, SG&A
leverage and savings through our distribution network.
Strength in our 2017 results are reflected in the
achievements below:
 Net Sales increasing to a record $22.25 billion.
 Servicing more than 2 billion customer
transactions.
 Same-store sales (on a constant currency basis)
increasing 1.9% on top of a 1.8% increase in the
previous year.
 Dollar Trees 40th consecutive quarter of positive
same-store sales.
 Operating income improving 17.3% to a record
$2.0 billion.
 Earnings, on a GAAP basis, increasing to a record
$7.21 per diluted share.
 Opening 603 new stores, expanding or
relocating 113 stores and increasing selling
square footage by 3.7% to 116.6 million square
feet. We ended the year with 14,835 stores
across North America.
 And at year-end, employing more than 176,000
dedicated associates across our company.
Dollar Tree
Growth and consistency in the Dollar Tree banner
continued with its 40th consecutive quarter of positive
same-store sales, consistent gross margins and our
history of surprising value at $1. Our customers
continue to be delighted when they find what they
need and surprised with a new and ever-changing
mix of product.
The Dollar Tree banner continues to find great
opportunities around each of the seasons and holidays,
relevant basics that stretch our customers budgets
and convenience in a small box that saves time and
money. Our merchandising teams continue to develop
and invent our product assortment that relies on great
value at a single price point and at a margin we decide.
Our promise is to always deliver great value on the
items in our store.
In 2017, we added coolers and freezers to 420
Dollar Tree stores, which add an important category
for our customers needs, drives trips and provides a lift
across our stores in our consumable and discretionary
departments. Our store teams are focused on
Winning the Week to deliver great stores and speedy
customer service with a friendly thank you to all of
our customers. Great merchandise in an exciting,
friendly shopping environment has been the foundation
of our Dollar Tree success.
Family Dollar
Our momentum continues at Family Dollar. We are
pleased with the progress of our important milestones
around the integration. In 2017 we completed
approximately 375 renovations, opened 288 new
stores and relocated or expanded 31 stores. With this
new shopping experience, our customers are seeing
new brand standards at these Family Dollar stores.
Our customers are experiencing better merchandise
adjacencies, greater values on end-caps, $1 WOW
items throughout the store, added frozen food
assortment, and new, exciting choices across our
drink and snack merchandise.
Our Smart Ways To Save strategy drives our
values across Family Dollar stores. We have helped
our customers save every week with our price drop
promotions on items they need most. Our private
brands have been repackaged and reformulated,
comparable to national brands at a great value.
Our Compare and Save messaging highlights these
fantastic values across the store. $1 WOW items
generate surprising values across categories and
reinforce with our customers the opening price
point values at Family Dollar. And finally, our recent
introduction of Smart Coupons, has generated more
than five million loyal Family Dollar shoppers joining
the program seeking additional savings each week.
Our operating income has improved at Family
Dollar through focus on our merchandising gross
margins. As we continue to build out our store
base, renovate our existing stores and deliver on our
initiatives, we expect to see additional foot traffic
and improved same-store sales, which will drive
continued improvement to the bottom line of our
Family Dollar banner.
Our store teams are energized and now have
incentives that align with our mission to drive profitable
sales each week and month, in a better shopping
environment that delivers Smart Ways To Save.
Winning
With several hundred new Dollar Tree and Family Dollar
stores combined with renovations and other real estate
projects, we expect to touch approximately 1,000
stores per year with growth-focused initiatives. We
believe our new store growth will continue to drive our
revenue and we continue to see opportunities for at
least 26,000 stores across North America.
Dollar Tree Direct, while a small segment of our
overall business, continues to deliver double digit
comp increases in sales and site visits. We have a loyal
group of customers from party and wedding planners
to teachers and non-profits who continually use Dollar
Tree Direct for their bulk purchase needs. Combined
with our Smart Coupons, we continue to find ways to
engage our customers across both brands.
Since the Family Dollar acquisition, we have
prepaid more than $3.5 billion in debt, accomplishing
this with our strong free cash flow. We recently achieved
our goal of returning to investment grade, which has
allowed us to refinance our longer term debt and reduce
interest expense, benefiting our bottom line earnings. Commitment to Governance
and Shareholder Value
Dollar Tree has demonstrated a long-standing
commitment to responsible corporate governance
and delivering value to our long-term shareholders.
Our Board of Directors is active and engaged in
our business. The majority of the Board is comprised
of independent directors; we have a lead independent
director and all of the standing committees of the Board
consist entirely of independent directors. The Board
regularly reviews the Companys governance practices
and has made several positive changes in recent
years. We continue to maintain an open dialogue with
shareholders on governance-related matters, and
we seek to enhance our understanding of the
evolving corporate governance best practices within
our industry.
We believe in strict adherence to our core values
of honesty, integrity and transparency in all aspects of
our business. These values are reflected in the strength
of our financial controls and in our relationships
with customers, vendor partners, associates and our
shareholders.
For 2017, we again achieved a clean bill of
health with no material control weaknesses noted
in our assessment, supporting that our accounting
and reporting processes are in compliance with the
requirements of Sarbanes-Oxley legislation.
Dollar Tree has consistently generated significant
cash flow and has been a prudent manager of capital
for the benefit of long-term shareholders. The best use
of capital, in our view, is to support continued growth of
the business at a sustainable pace.
Looking to Our Future
With record net sales of $22.25 billion and earnings
increasing to $7.21 per diluted share, we are very
pleased with our 2017 results and continued progress.
Our management teams, across both banners, are
focused on executing our strategies and delivering
results that grow the bottom line and benefit our
long-term shareholders.
Our people, more than 176,000 strong, deliver
across our network of distribution centers, store
support centers and our stores on a daily basis.
I believe the talent we have been able to attract
and develop within our company has been the
foundation to our success now and into our future.
I am proud to lead this team of talented individuals
and an organization that knows how to win in retail.
I continue to be as excited as ever about our future
and opportunities ahead.
Gary Philbin
President and Chief Executive Officer