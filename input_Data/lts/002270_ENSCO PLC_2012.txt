Dear Fellow Shareholders:
The past year has been a remarkable period of growth for Ensco. We often use the tagline Exceeding Expectations,
and in 2012 we exceeded the expectations of many with the best safety record in our history, significantly higher
fleet utilization, and on-time, on-budget deliveries of three new rigs  all while successfully integrating our 2011
acquisition.
This operational success is reflected in our third consecutive rating as the number one offshore drilling contractor in
total customer satisfaction. The award is a testament to our exceptional crews and operations management around the
globe who consistently deliver a superior customer experience. Through their efforts, we achieved record revenues
and earnings as we solidified and strengthened our position as a leader in an offshore drilling market that is healthy
and growing.
The successful integration of our acquisition has brought Ensco benefits beyond our expectations and beyond the
expense synergies we targeted for 2012. We have achieved not just economies of scale in purchasing, rig construction
and shore-based operations, but also improved vendor responsiveness when rig downtime occurs. Additionally, our
expanded geographic scope has given us advantages in contracting our rigs, allowing us to be even more selective
in choosing the highest margin opportunities. Plus, we are able to recruit from more talent pools and provide our
employees  both rig crews and shore-based staff  more career opportunities in more places around the world.
Financial results
In our first full year since the acquisition, we earned $5.04 in diluted earnings per share, up 64 percent from $3.08
in 2011. Our annual revenues were $4.3 billion, a 54 percent increase over 2011, as strong customer demand drove
utilization and average day rates higher. Fleetwide utilization rose to 88 percent from 80 percent in 2011. The average
day rate increased to $193,000, compared to $161,000 in 2011.
Our strong financial results enabled us to reinvest a record $1.8 billion back into our fleet for future growth, even as
we increased our dividend and reduced debt levels during 2012. In February 2013 we raised our dividend another 33
percent to $2.00 per share annually, given our bullish outlook for future growth.
Safety
A key measure of our success in 2012 was our safety performance. We achieved our best-ever total recordable
incident rate, 0.47 incidents per 200,000 man-hours worked. Our performance against this standardized measure
was well ahead of the offshore industry average. We continue to set more rigorous safety targets with the ultimate
goal of achieving a zero-incident workplace. We congratulate the crew members of 35 Ensco rigs who achieved zero
recordable incidents in 2012.
Review of operations
An active newbuild program is a key element of our continuous high-grading strategy. During 2012 we took delivery
of three new rigs:
 ENSCO 8505, an ultra-deepwater semisubmersible, was delivered in January and began work under a shared
drilling contract for Anadarko, Apache, and Noble Energy in the U.S. Gulf of Mexico. 
 ENSCO DS-6, an ultra-deepwater drillship, was delivered in January and contracted to work for BP in Angola for
a five-year term that added more than $1 billion to our revenue backlog.
 ENSCO 8506 was delivered in June and contracted to work for Anadarko in the U.S. Gulf of Mexico for twoand-a-half years.
ENSCO 8506 is the seventh and final rig in our highly successful ENSCO 8500 Series. This proprietary design has
gained a following as a drillers rig because of its efficient design and high reliability. Utilization for the ENSCO
8500 Series rigs during 2012 was 96 percent for rigs that have worked at least six months. Anadarko, Noble, Nexen
and Apache are all repeat customers.
In 2013, we will deliver the first rigs in a new proprietary series. The ENSCO 120 Series ultra-premium harsh
environment jackup rigs are designed for the most demanding large multi-well platform programs as well as ultra-deep
gas and ultra-long reach wells up to 40,000-foot total drilling depth. Industry-leading design features significantly
increase the area of operability in the Central North Sea and other challenging regions.
ENSCO 120, scheduled for delivery in the third quarter, is already contracted to Nexen through early 2015. ENSCO
121, scheduled for delivery at the end of the year, was recently contracted to Wintershall through 2015.
We will also take delivery of ENSCO DS-7 in the third quarter of 2013; this ultra-deepwater drillship, the fifth in our
Samsung DP3 drillship series, has recently been contracted to Total in Angola for an initial three-year term.
Our strategy of fleet standardization is reflected in each of these series and continues to provide a competitive advantage
in our ability to construct, maintain and crew our rigs.
We continue to high-grade our fleet to drive future growth. During 2012 we ordered two new ultra-deepwater drillships
for delivery in 2014. These two drillships, which will become ENSCO DS-8 and ENSCO DS-9, take our current
Samsung drillship design a step further, with advanced drilling and completion functionality including below-maindeck riser storage.
At the same time, we are strategically divesting less capable rigs. During 2012 we sold three jackups, one floater, and
our sole barge rig; the proceeds from these sales were reinvested into the fleet.
Market conditions
Over the past year, offshore drilling opportunities have continued to improve. A record number of new deepwater
discoveries have been announced by operators and, importantly, they are geographically broad-based. In addition
to new discoveries in the Golden Triangle (West Africa, Brazil and Gulf of Mexico), customers are finding more
reservoirs in Asia, East Africa, the Eastern Mediterranean, Australia and South America. These new discoveries,
coupled with favorable commodity prices, have driven increased customer demand for drilling rigs.
Given the limited supply of available rigs, day rates for floaters rose significantly during 2012. While newbuild
floaters will be entering the market in 2013, we believe growing demand for floating rigs will more than offset the
new supply this year.
Demand for jackup rigs operating in shallower water depths also increased during 2012, driven by the exploration
and production success of our customers around the world. Utilization and day rates increased significantly last year,
and we expect favorable conditions will continue in 2013. Newbuild jackups to be delivered this year will be partially
offset by older rigs being retired. These new rigs also respond to customer demand for more technologically advanced
equipment for certain well programs.
Given rising customer demand, the quality of our fleet and the strong work ethic of our employees, all of Enscos
actively marketed rigs are currently contracted. Revenue backlog is now a record $11 billion.
Executive changes
All that we have accomplished could not have been done without the strong management team that we have in place.
During 2012, Mark Burns was promoted to Executive Vice President and Chief Operating Officer, replacing Bill
Chadwick, who retired after being part of Enscos management since its inception. Steve Brady succeeded Mark as
Senior Vice President  Western Hemisphere.
Jay Swent, our Chief Financial Officer, was promoted to Executive Vice President and added supply chain and human
resources to his responsibilities. I am extremely pleased that, true to our value of success for employees, we were
able to promote from within to fill these and other executive positions during the year.
Looking forward
ENSCO 8505, ENSCO 8506 and ENSCO DS-6, our most recently delivered rigs, will be joined this year by ENSCO
DS-7, ENSCO 120 and ENSCO 121, all of which are already contracted. These fleet additions and continuing strong
market conditions will substantially increase revenues.
In addition to the three rigs being delivered in 2013, we are constructing three additional rigs to be delivered in 2014
 ENSCO 122, ENSCO DS-8 and ENSCO DS-9. Investments in new rigs and fleet upgrades will address current and
future customer demands and maintain the high quality of our global fleet.
I want to thank all of our employees for their accomplishments in 2012. We recognize that it is not just the quality of
our rigs, but the quality of our people that sets us apart. To maintain and build on this strength, we recently launched
our Go Beyond campaign. Taken from our vision statement to go beyond what is expected, this phrase perfectly
captures our goal of continuous improvement in safety, operating performance and customer satisfaction. We believe
this campaign, which recognizes exceptional actions by our employees, will be one of the elements that drive us to
even greater success.
I also thank our customers for their business and their trust and our investors for their continued confidence in our
strategy.
Finally, I thank the Board of Directors for their counsel during the past year. I offer particular gratitude for the service
of two of our Board members who are retiring as of this years annual meeting: Rita M. Rodriguez has served on
the Board since 2003 and Thomas L. Kelly has served on the Board since Enscos inception in 1987. Their years of
wisdom and guidance are greatly appreciated.
While 2012 was a remarkable year for Ensco, we do not believe it was unique. We intend to carry this momentum
forward. With rising customer demand, a continued commitment to safety and operational excellence and a growing
fleet, we are well positioned to Go Beyond 2012s successes.
Sincerely,
Daniel W. Rabun
Chairman, President and CEO
26 March 2013