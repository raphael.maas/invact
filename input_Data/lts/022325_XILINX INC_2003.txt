to our shareholders, customers, employees and partners:

TXilinx made significant progress during a year that remained sluggish for
both the technology industry and the overall economy. Diversification into
new markets and new technologies enabled Xilinx to capture five incremental
percentage points of Programmable Logic Device (PLD) market
segment share during calendar 2002, as measured by market research
firm Gartner Dataquest. Additionally, Xilinx made a number of key breakthroughs
and product announcements during the year, solidifying our
technology and cost leadership. At the same time, we increased our commitment
to providing greater financial transparency for investors and
undertook several key initiatives in the area of corporate governance.
Xilinx revenues were $1.16 billion in fiscal 2003, up 14% from fiscal 2002. By contrast, Xilinx estimates
that the PLD industry grew only 4% over this same time period. Fiscal 2003 gross margin increased to
59%, up from 45% last year. Operating margin and profit margin of 13% and 11% both increased
significantly from 4% and 11%, respectively, in the prior fiscal year, which was impacted by a sizeable
inventory write-down as a result of the semiconductor downturn. Financial improvement was even more
evident in the recently ended March quarter when Xilinx reported sales of $305.5 million, up nearly 36%
from trough revenues of $225 million in the September quarter of 2001. Operating margin and profit
margin for the March quarter were 20% and 16%, respectively, the highest in the PLD industry. Finally,
Xilinx generated $345 million of operating cash flow during the year and increased cash and investments
by $310 million to a record $1.1 billion.
Our market diversification strategies paid off in fiscal 2003 as sales from the Consumer, Industrial
and Other category increased over 120% versus fiscal 2002. This category represented 24% of Xilinx
sales in fiscal 2003, up from 12% in the prior fiscal year. Much of this growth is attributable to the
success of Xilinxs high-volume Spartan FPGA (Field Programmable Gate Array) family, which represented
over 15% of total Xilinx revenues in fiscal 2003, up from 11% in the prior year. In April 2003, we began
shipping the Spartan-3 FPGA family; the worlds first FPGA manufactured using 90-nanometer technology.
This advanced technology enables a chip size reduction of up to 80% when compared to competing
products on 130nm technology. Furthermore, Spartan-3 FPGAs allow us to expand upon our success in
the consumer market, while enabling us to address significant new applications like LCD TVs, video on
demand services and blade servers that have traditionally been served by Application Specific Integrated
Circuits (ASICs).
Our Complex Programmable Logic Devices (CPLDs) are also fueling consumer market expansion with
designs in mobile phones, digital palm cameras, and Internet gaming devices. Led by the innovative
CoolRunner-II family, we estimate our share of the CPLD market segment surpassed 19% at the end of
fiscal 2003, up from 16% a year ago.
The Companys Virtex family continued to lead the industry in a number of key areas including sales,
density, performance, and price. Since its introduction in August 1998, the Virtex family has generated
cumulative revenues exceeding $1.8 billion. Revenues from Virtex-II FPGAs, introduced just two years
ago, were particularly strong during the year increasing nearly 300% versus fiscal 2002 and representing
over 20% of total revenues. The newest Virtex family member, the Virtex-II Pro Series, is gaining rapid
customer acceptance, posting nearly $1 million in sales in the recently ended March quarter, with revenues
expected to triple in the June quarter.1 The Virtex-II Pro family is our primary high-end FPGA
design vehicle and offers customers unprecedented levels of functionality including embedded processing
and high-speed serial interfaces at industry leading price points. In addition, Xilinx migrated the
majority of the Virtex family devices to 300mm wafers during the year, enabling cost reductions of more
than 30%.
In the software area, Xilinx introduced ISE 5.1i, which supports all Xilinx FPGA and CPLD devices and
extends Xilinxs leadership in compile time and design performance. Later in the year, Xilinx introduced
ISE 5.2i, which among other improvements delivered Spartan-3 design capabilities in advance of silicon
availability. In the area of Intellectual Property (IP) Cores, which includes leading edge connectivity and
DSP cores, Xilinx set new records in fiscal 2003, nearly doubling the number of IP cores sold.
The year concluded on a positive note when Fortune Magazine ranked Xilinx #4 in its annual listing
of The Best 100 Companies to Work For. Xilinx placed higher in the rankings than any other public or
high technology company. When combined with the strong financial results we have been able to attain,
this is a strong validation of our corporate culture, and a remarkable accomplishment given the challenging
economic environment.
In the area of corporate governance, Xilinx has taken several steps to strive for the highest standards
of corporate leadership. As I mentioned last year in my letter to you, we discontinued our use of proforma
financial reporting in fiscal 2003. To increase transparency of financial reporting to investors, we
also began routinely incorporating an abbreviated statement of cash flows with our quarterly earnings
release. Additionally, the following principles form the foundation of our corporate governance framework:
 A majority of the Board of Directors are independent directors. Currently six of our eight directors are
independent.
 All members of the Audit, Compensation and Nominating and Governance committees are comprised
of independent directors.
 Independent directors meet regularly without management.
 Our Code of Conduct has been updated and published on our website.
 There is a now a mandatory retirement age for our directors.
 A lead independent director has been appointed.
 Stockholder approval is required on all stock option plans.
Please visit our Investor Relations website at www.investor.xilinx.com for a comprehensive view of
our corporate governance practices. This website details the Board composition and responsibilities as
well as the charter of each of the committees. Additionally, you will find copies of our Code of Conduct
and our Corporate Governance Guidelines.
As we embark on fiscal 2004, we plan to expand our market opportunities by pursuing the following
three strategies:
1. Drive low-cost leadership. The Spartan-3 family exemplifies low-cost leadership as the worlds
first 90nm FPGA. In fiscal 2004, this family will be produced in volume on 300mm wafers. The combination
of the move to 90nm technology coupled with production on 300mm wafers provides Xilinx with
the greatest cost reduction step in our history. We expect to be the first PLD company to recognize these
cost savings, enabling rapid expansion into new consumer-oriented markets that were previously the
exclusive domain of ASICs. 2
2. Accelerate our technology leadership. Today, Virtex-II Pro FPGAs demonstrate unparalleled technology
leadership with industry-leading density and performance as well as embedded processor and
high-speed transceiver functionality. In the coming year, we will continue to engineer new solutions
aimed at increasing the performance and functionality of our chips. The benefits of this are twofold:
First, by integrating discrete components into Xilinx FPGAs, our customers are able to reduce their overall
system cost, while increasing performance and reliability. Secondly, Xilinx benefits by increasing our
share of silicon content in the next generation of our customers systems.
3. Aggressively expand into the ASIC market. This is not a new strategy. In fact, Xilinx has been
taking share from ASIC companies since our inception in 1984. According to Gartner Dataquest, Xilinx is
now the 4th largest ASIC supplier worldwide, up from #5 last year and up from #10 six years ago. We
believe that todays uncertain economic environment coupled with the increasingly high fixed costs associated
with manufacturing ASICs present a compelling case for designing with PLDs. We plan to capitalize
on this trend in the upcoming year with the move to 90nm process technology, 300mm wafer production
and by offering high levels of systems integration to aggressively pursue opportunities in the traditional
ASIC space. Ultimately, we believe that these three strategies facilitate market expansion enabling Xilinx
to continue to outgrow the semiconductor industry as well as many of the end markets that we serve.3
In closing, on behalf of Xilinx, I would like to extend heartfelt thanks to our stockholders, customers,
employees and partners. Without your support, none of these accomplishments would have been possible.

WILLEM P. ROELANDTS
President and Chief Executive Officer
