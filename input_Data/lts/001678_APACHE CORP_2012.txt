Fellow
Shareholders,

Apache posted another year of progress in 2012 and is positioned
to continue to deliver consistent, long-term, profitable growth.
Our strategy to build a global, diversified portfolio of oil and gas
properties  with a focus on high-value, high rate-of-return activities 
again delivered a substantial financial and operating performance.
Significant highlights for the year include:
 Achieved record production for the fourth consecutive year  779,000 barrels of oil
equivalent (boe) per day.
 Delivered cash from operations before changes in operating assets and liabilities*
of $10.2 billion for the second consecutive year.
 Replaced 156 percent of the years production through discoveries, extensions and
acquisitions, excluding negative revisions associated primarily with low Canadian
gas prices.
 Firmly established the Permian and Anadarko basins as U.S. growth engines, adding
312,000 acres in Oklahoma and the Texas Panhandle and accelerating drilling activities
in these areas.
 Expanded our opportunity set and increased production in our UK North Sea region
to 75,000 boe per day, as discoveries and newly acquired properties were brought
onstream under Apache operatorship.
 Announced a second LNG partnership with Chevron in December 2012 at the proposed
Kitimat project in western Canada. We hold a 50-percent working interest in Kitimat
LNG, where estimated gross production from a single train totals in excess of 700 MMcf
per day.
 Generated record annual revenues of $17.1 billion.
We are fiscally strong and have an extensive inventory of near-term and future
development projects, prospects and exploration ventures that will contribute to
our profitable growth over the next five-plus years. Apache is opportunity rich, and a
challenge we face is balancing capital allocation to generate near-term growth with
investments to fund future developments while maintaining our historical fiscal discipline.
Our pipeline of mid- to long-term projects has the potential to produce billions of dollars
in excess cash flow, serving as cornerstones for continuing growth.

Highlights from 2012  Global Progress
Since mid-year 2010, Apache has enhanced our asset base in every core area by
completing seven major oil and gas acquisitions and through targeted acreage purchases.
This past year, we accelerated our drilling activities on newly added properties with a
focus on oil and liquids production in North America.
 Oil and natural gas liquids production in the United States increased 18 percent from
the prior year, with significant contributions from numerous Permian and Anadarko
basin plays.
 Apache had 111 rotary rigs operating worldwide in drilling or completion activities at
year-end 2012, a 29-percent increase compared to the same period in the prior year.
 International gas assets delivered more value, with natural gas price realizations up
13 percent for the year to $4.15 per Mcf, topping our North American gas realizations
for the first time since 1995. International gas production increased 10 percent to
839 MMcf per day, boosting our earnings throughout the year.
 In Australia, we commenced production from our Reindeer gas field and Devil Creek
processing plant, the first new gas processing hub in Western Australia in more than
15 years.
Near-Term Growth: Focus on North America
The United States remains the worlds highest demand market for hydrocarbon products.
While the domestic oil and gas industry is considered mature, technology has revitalized
the industry, reversing a multi-decade decline in domestic oil production. Apache is at the
forefront of this resurgence, and our 2013 drilling program is designed to accelerate value
realization from our expanded property base.
Apache conducted extensive field studies across all of our properties in the Anadarko
and Permian basins during 2012. This initiative identified more than 67,000 prospective
well locations and 9.2 billion boe in resource potential. Drilling activity steadily increased
throughout the year, with the number of operated rigs in our Central Region increasing
from seven to 25. In the Permian Basin, weve ramped up from an average of five rigs
operating in 2010 to 32 rigs in 2012, with an increasing percentage of wells being drilled
for more prolific horizontal completions.
Both regions provide multiple ways to win: thick, oily and liquids-rich, stacked-pay drilling
targets provide attractive economics with reduced geologic risk. Additional upside
potential exists through down-spacing, step-out and horizontal opportunities. While
were not currently targeting any dry gas-bearing zones, we know theyre there, offering
a valuable option when natural gas prices ultimately recover.
A similar field analysis is under way in Canada, and our inventory of drilling locations
in North America is expected to more than double once these studies are complete.
This type of rigorous examination of our properties also will be undertaken across our
international assets.
Creating the Future: Developing Legacy Assets
Apaches portfolio has evolved since 2010, reinforcing our ability to achieve the
longstanding objective of consistent, profitable growth. Whether in frontier regions
not yet fully explored or in projects requiring greater capital and longer cycle times for
infrastructure development, our property base offers the potential for long-life, legacy
assets that can provide robust cash flow year after year.

Highlights from 2012  Global Progress
Since mid-year 2010, Apache has enhanced our asset base in every core area by
completing seven major oil and gas acquisitions and through targeted acreage purchases.
This past year, we accelerated our drilling activities on newly added properties with a
focus on oil and liquids production in North America.
 Oil and natural gas liquids production in the United States increased 18 percent from
the prior year, with significant contributions from numerous Permian and Anadarko
basin plays.
 Apache had 111 rotary rigs operating worldwide in drilling or completion activities at
year-end 2012, a 29-percent increase compared to the same period in the prior year.
 International gas assets delivered more value, with natural gas price realizations up
13 percent for the year to $4.15 per Mcf, topping our North American gas realizations
for the first time since 1995. International gas production increased 10 percent to
839 MMcf per day, boosting our earnings throughout the year.
 In Australia, we commenced production from our Reindeer gas field and Devil Creek
processing plant, the first new gas processing hub in Western Australia in more than
15 years.
Near-Term Growth: Focus on North America
The United States remains the worlds highest demand market for hydrocarbon products.
While the domestic oil and gas industry is considered mature, technology has revitalized
the industry, reversing a multi-decade decline in domestic oil production. Apache is at the
forefront of this resurgence, and our 2013 drilling program is designed to accelerate value
realization from our expanded property base.
Apache conducted extensive field studies across all of our properties in the Anadarko
and Permian basins during 2012. This initiative identified more than 67,000 prospective
well locations and 9.2 billion boe in resource potential. Drilling activity steadily increased
throughout the year, with the number of operated rigs in our Central Region increasing
from seven to 25. In the Permian Basin, weve ramped up from an average of five rigs
operating in 2010 to 32 rigs in 2012, with an increasing percentage of wells being drilled
for more prolific horizontal completions.
Both regions provide multiple ways to win: thick, oily and liquids-rich, stacked-pay drilling
targets provide attractive economics with reduced geologic risk. Additional upside
potential exists through down-spacing, step-out and horizontal opportunities. While
were not currently targeting any dry gas-bearing zones, we know theyre there, offering
a valuable option when natural gas prices ultimately recover.
A similar field analysis is under way in Canada, and our inventory of drilling locations
in North America is expected to more than double once these studies are complete.
This type of rigorous examination of our properties also will be undertaken across our
international assets.
Creating the Future: Developing Legacy Assets
Apaches portfolio has evolved since 2010, reinforcing our ability to achieve the
longstanding objective of consistent, profitable growth. Whether in frontier regions
not yet fully explored or in projects requiring greater capital and longer cycle times for
infrastructure development, our property base offers the potential for long-life, legacy
assets that can provide robust cash flow year after year.

G. STEVEN FARRIS
Chairman and Chief Executive Officer
ROGER B. PLANK
President and Chief Corporate Officer
RODNEY J. EICHLER
President and Chief Operating Officer