DEAR SHAREHOLDERS,

I am pleased to report that in 2003 Moodys produced
revenue and profit growth above our long-term targets.
Moody's 2003 revenue totaled $1,246.6 million, an
increase of 22% from $1,023.3 million for 2002. On a
pro forma basis, assuming that Moodys April 2002
acquisition of KMV had occurred on January 1, 2002,
revenue for 2002 would have been $1,038.4 million and
revenue growth in 2003 would have been 20%.
Operating income for 2003 was $663.1 million,
increasing 23% from $538.1 million for 2002. Net
income for 2003 was $363.9 million, 26% greater than
$288.9 million in 2002. Diluted earnings per share for
2003 were $2.39, an increase of 31% from $1.83 for
2002. Earnings per share for 2003 included a $0.05
non-recurring gain on an insurance recovery in the first
quarter, $0.04 in expense related to stock options and
other stock-based compensation plans, and an $0.11
impact of increased reserves for legacy tax matters
related to income tax exposures that were assumed by
Moodys in connection with its separation from The Dun
& Bradstreet Corporation in 2000.
Better Markets than Expected in the
United States
Our expectations early in 2003 were for a difficult
macroeconomic and capital markets environment.
Actual conditions were more favorable, and we were
able to take advantage of the strong markets to achieve
excellent growth. All of Moodys businesses in the
United States showed growth, led by particularly strong
performance in research and solid strength in structured
finance.
We had expected an important revenue decline in our
U.S. residential mortgage-backed securities business
but were surprised by the continued strength of home
refinancings and purchases and by the robust home
equity loan market. We also anticipated no revenue
growth from rating public finance obligations but
actually reported a modest increase. We entered the
year expecting good results in the high yield segment of
our corporate bond rating business and were pleased to
achieve results that were much stronger than anticipated.

Moodys Global Markets Continue to Expand
During 2003 Moodys also continued its global expansion.
Moodys revenue outside the U.S. grew to $451.3 million
in 2003, an increase of 32% from 2002. For 2003 international
revenue rose to 36% of Moodys total from 18%
just seven years ago. International revenue benefited
from strong corporate debt issuance in Europe in the
second half of the year, excellent growth in global
research, and the strength of the Euro relative to the
dollar. Moodys ratings and research revenue in Europe
grew over 35%, including the impact of currency translation.
Revenue growth in Asia and Latin America was in
the high teens percent, including currency translation.
During the year Moodys expanded our global network
of locations by opening offices in South Africa and
Taiwan and increasing our staffing outside the U.S. by
50 employees to almost 560. In addition we formed new
joint ventures with local partners in Israel and Egypt
and increased our investments in existing joint ventures
in Russia and India.
Building for Future Growth
Moodys has produced exceptional performance in each
of the last three years, with reported revenue growth of
32%, 28%, and 22% and earnings per share growth of
36%, 39%, and 31% in 2001, 2002 and 2003, respectively.
The company has benefited from very low interest rates,
which have encouraged companies to issue or refinance
bonds and consumers to refinance mortgages and
expand their indebtedness. All of these conditions have
resulted in expanding public debt capital markets. We
recognize that there will inevitably be years when
Moodys does not enjoy the favorable capital market conditions
of the last three years and Moodys growth is
less robust. Nevertheless, Moodys continues to have
good prospects for long-term growth and we are
putting in place programs to realize that growth.
We believe that growth of the global debt capital markets
will create new opportunities for Moodys for a number
of years to come. We expect increased issuance of rated
debt driven by global economic growth. On a global
basis rated debt issuance by corporations, financial
institutions, U.S. public finance entities and other local
authorities has risen at a compound annual rate of 16%
since 1996, and rated structured finance issuance has
increased at a rate of 24%.

Particularly in Europe, we expect that the share of business
credit provided by the public debt capital markets
will increase as more borrowers diversify their sources
of funding and use the public capital markets, rather
than the banking system, for at least part of their needs.
We also expect Moodys to benefit from the continued
growth of structured finance on a global basis.
Structured finance enables companies that produce or
hold financial assets to sell them in the public securities
markets rather than collecting cash over time, increasing
returns on assets and returns on equity. Structured
finance also enables companies to produce obligations
of higher credit quality than their own unsecured
obligations, increasing market access and decreasing
funding costs. Finally, structured finance can be used
for credit risk management. Increased acceptance of

structured finance as a basic financing tool, particularly
in Europe and Asia, is fueling growth in issuance.
Around the world, local legal and regulatory changes
are enabling or facilitating the use of structured finance
in new markets, and the continual process of financial
innovation creates new financial instruments for Moodys
to rate and cover with research.
We see particularly attractive longer-term opportunities
in a number of developing markets, including the
potential for sizable markets in China, India, and
Russia. To realize these prospects, we are working with
local partners who have deep business contacts in the
areas and who know the local markets.
New Product Offerings
We are developing new products to help issuers and
investors better assess and manage credit risk. Moodys
Enhanced Analysis Initiative, which we discuss in more
detail later in this annual report, focuses additional
scrutiny on a number of credit topics of particular
concern to investors. These include issuers vulnerability
to short-term liquidity crises, the quality of issuers
financial reporting, their use of structured finance and
other off-balance sheet risk transference structures,
and their corporate governance. The Enhanced Analysis
Initiative will enable Moodys to provide capital market
participants with better ratings and analysis of
corporations and financial institutions, and represents
an important business opportunity for us.

We are expanding our research business to include new
credit information products and are developing ways to
make Moodys extensive research offerings accessible
by and useful for a broader range of subscribers. Many
of Moodys new products leverage Moodys expertise in
credit analysis and our extensive databases of ratings,
defaults, recoveries, and  in structured finance 
underlying asset performance. Several new products,
Moodys Ratings Interactive, Moodys Mortgage Metrics,
Moodys Commercial Mortgage Metrics, Moodys CDO
Analytics, CDO Enhanced Monitoring, Performance
Data Services, and Municipal Financial Ratio Analysis,
are also described later in this report.
Growth at Moodys KMV will Likely
Outpace Moodys for Years
Moodys KMV is the leading provider of quantitative
credit risk assessment and management products. For
2003 revenue at Moodys KMV totaled $111.9 million
compared to $81.5 million in 2002. Assuming Moodys
had acquired KMV on January 1, 2002, revenue for
Moodys KMV in 2002 would have been $96.6 million
and year-over-year growth in 2003 would have been
16%. Moodys KMV products enable our customers to
better manage portfolios of credit-sensitive assets and
to make better commercial lending decisions.
Looking forward, the continued adoption of quantitative
credit risk management will be a source of growth for
Moodys KMV. In addition, we are enhancing existing
Moodys KMV products and developing new offerings
that build off the KMV founders groundbreaking work in
credit analysis based on market inputs and other
important sources of information about default probabilities,
recovery rates and credit correlations.
Recognizing our Responsibilities to the
Global Capital Markets
Moodys recognizes the vital role that credit rating
agencies play in the capital markets. We carefully
manage the potential conflicts of interest inherent in
our business model, where the issuers we rate provide
most of our revenue. We maintain strict controls over
the dissemination of confidential information, both
imparted to us by issuers and produced internally about
our forthcoming ratings and research, and appreciate
the need to treat all market participantsissuers,
intermediaries, and investorsprofessionally and fairly.
Moodys accepts the general principles for Rating
Agencies issued in 2003 by the International
Organization of Securities Commissions and supports
the process recently announced by that organization to
develop a Code of Conduct for Rating Agencies. We have
been working with global securities regulators, central
banks and finance ministries to promote understanding
of our professional activities. Overall, we are pleased
that global financial authorities generally acknowledge
that, on the whole, credit rating agencies have done 
and continue to do  a good job.

Making Moodys Processes More Transparent
We also recognize our obligation to make Moodys
processes as transparent as possible. To address this
obligation, Moodys regularly publishes reports that
provide quantitative and qualitative insight into the
performance of our ratings.
Special reports of particular importance that we published
in 2003 are described later in this annual report.
These reports are available to the public on our website,
www.moodys.com. We encourage you to read these
reports and we welcome your comments.
We are continuing to refine our rating methodologies for
individual industries and types of securities. We expect
to make significant progress during 2004 on making
these rating methodologies more understandable,
particularly as they are applied to ratings of individual
issuers. Consistent with our current practice, we will
consult the market about any important changes in our
methodologies before we implement them.
Developing Management for Succession
During 2003 we made changes in Moodys management
to provide additional development opportunities for senior
managers and to prepare for management succession
in 2005.
Raymond McDaniel was named Chief Operating Officer
of Moodys Corporation in addition to continuing as
President of Moodys Investors Service. Brian Clarkson
and Christopher Mahoney were each appointed Executive
Vice President and Co-Chief Operating Officer of Moody's
Investors Service, reporting to Ray. Brian Clarkson will
be responsible for Moodys global Structured Finance
and U.S. Public Finance businesses. Chris Mahoney will
oversee Moody's global Corporate Finance, Financial
Institutions, and Sovereign Risk businesses. Chris will
also continue to head the companys credit policy
function. The two will share management responsibility
for the Moody's Investors Service research business.
In addition, Chester Murray was named Executive Vice
President - International for Moodys Investors Service.
His responsibilities will include managing Moodys
international business development and investment
activities as well as overseeing our international offices
and joint venture relationships. We have asked Douglas
Woodham, who had been President of Moodys KMV and

Senior Vice President of Moodys Corporation for
Strategy, Corporate Development, and Technology, to
focus exclusively on Moodys KMV. Finally, Jeanne
Dering, our Chief Financial Officer, was given additional
oversight responsibility for Moodys technology efforts.
Committed to Returning Excess
Cash to Shareholders
During 2003 Moodys generated impressive cash flow.
After-tax operating cash flow for the year totaled $422.5
million1, or 116% of net income. We remain committed to
returning excess capital to shareholders primarily
through opportunistic share repurchases. We use a discounted
cash flow model with conservative assumptions
to determine when to repurchase stock and our goal is
to create value for Moodys long-term shareholders as
well as those who are selling.
Since becoming a public company in October 2000 and
through the end of 2003, the company repurchased 23
million shares at a total cost of $881 million, including
9.3 million shares to offset shares issued under
employee stock plans. In addition to share repurchases,
in 2003 we increased the annual dividend on Moodys
common stock to $0.30 per share from $0.18 per share.
As a final note, I would like to thank Clifford Alexander
for his service as Chairman of Moodys. As CEO of
The Dun & Bradstreet Corporation, Cliff inspired the
separation of Moodys and Dun & Bradstreet and the
successful launch of Moody's as an independent, public
company in October 2000. Under Cliffs leadership as
non-executive Chairman, Moodys achieved outstanding
growth, a substantial increase in shareholder value, and
implemented a successful program of responsible
corporate citizenship in the communities in which we
operate. We are very grateful for Cliffs many contributions
to Moodys and we wish him the best.

JOHN RUTHERFURD, JR.
CHAIRMAN AND CHIEF EXECUTIVE OFFICER
MARCH 15, 2004