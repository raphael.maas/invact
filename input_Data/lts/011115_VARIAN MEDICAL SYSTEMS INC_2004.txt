Fiscal year 2004 was a rewarding year for Varian
Medical Systems stockholders, customers, and
employees. We achieved impressive financial results,
introduced six major new products, extended our
technological and market leadership, significantly
expanded three emerging business areas, completed
three strategic acquisitions, entered two new
markets, and enhanced our operational efficiency.
These accomplishments set the stage for continued
strong growth in fiscal year 2005.
PROFITABLE GROWTH
For 2004, in comparison with the previous year:
Net orders rose 21 percent to $1.4 billion
Year-end backlog rose 20 percent to $970 million
Revenues increased 19 percent to $1.2 billion
Operating earnings climbed 29 percent to $256 million
Net earnings rose 28 percent to $167 million
After paying a two-for-one stock dividend during the year,
our company recorded annual earnings of $1.18 per diluted
share, up 28 percent from fiscal year 2003.
Annual net orders, revenues, and operating earnings rose
in all three business segments. Net orders increased 20 percent
in Oncology Systems, 30 percent in X-Ray Products, and
32 percent in the Other segment that includes the Ginzton
Technology Center and BrachyTherapy products. Revenues
rose 20 percent in Oncology Systems, 8 percent in X-Ray
Products, and 20 percent in the Other segment.
Higher unit volumes and a sales mix change toward more
profitable products drove the companys annual gross margin
up by about 1.4 percent to a record 41.9 percent of revenue.
We also reduced selling, general, and administrative expenses
as a percentage of revenues by about one-half percent to
15.3 percent. Our operating earnings for fiscal year 2004
were a record 20.7 percent of revenues, up 29 percent from
the previous year. In keeping with our commitment to technological
leadership, we increased our research-and-development
investment by about 20 percent.
The company generated a record of $234 million in cash
from operations. We ended the year with $393 million in
cash and marketable securities after spending $202 million to
repurchase approximately 5.6 million shares of the companys
common stock and another $72 million on acquisitions.
For the year, Varian Medical Systems delivered a 27.9 percent
return on equityan increase of 2.4 percent from an
already good 25.5 percent return on equity in fiscal year 2003.
MARKET LEADERSHIP
In 2004, Varian Medical Systems introduced several significant
new products for more precise radiotherapy treatments that
will enable better outcomes in cancer treatment. These products
also address the equally important need for greater costefficiency
in modern healthcare.
Varian developed the worlds first clinically practical products
for real-time, image-guided radiation therapy (IGRT), a
new treatment process that addresses the problem of tumor
motion. Our engineers made it possible for the equipment to
track and target tumors more accurately than ever with a new
On-Board Imager accessory that takes still, moving, or threedimensional
X rays of patients at the moment of treatment.
Using these images, clinics are now able to treat patients with
smaller radiation beams to concentrate higher doses in tumors
while improving the protection of surrounding healthy tissue.
To make our IGRT solution clinically practical, Varian has
developed powerful software that integrates and automates
the process so that treatments can be completed easily within
a normal 15-minute session.
Customers welcomed our IGRT solution when it was introduced
last March, and by the end of the fiscal year more than
80 clinics in North America and Europe had placed orders for
the On-Board Imager product, one of the fastest new product
introductions in our history. Meanwhile, clinics continued to
adopt intensity-modulated radiation therapy (IMRT), a modern
treatment process enabled by Varian developments over the last
15 years. Varian-equipped centers using this technique nearly
doubled to more than 860 during the fiscal year.
Equally significant, in fiscal year 2004 we introduced the
Trilogy accelerator, the most comprehensive, versatile, and
cost-effective radiation treatment machine in the worlda
device that can be shared by radiation oncologists and neurosurgeons
to address multiple medical conditions. Trilogy is
optimized for specialized radiosurgical procedures as well as
all standard radiotherapies, including IGRT and IMRT. This
product offers Varian a growth opportunity in the burgeoning
field of radiosurgery, the substitution of focused radiation for
traditional surgical techniques. Customer response again was
gratifying. By the end of the fiscal year, 27 Trilogy accelerators
had been ordered by centers around the world.
Fiscal year 2004 also saw the introduction of Varians
iX Series Clinac accelerator, a modularized product designed
to facilitate more rapid adoption of IMRT and IGRT. Other
IGRT-related developments in the year included cone-beam
CT 3D imaging on our Acuity simulator and the addition of
Zmeds SonArray 3D ultrasound imaging device.
Within the $100 million Oncology Systems software product
line, we added an electronic health record feature to our
VARiS Vision product to support paperless clinical processes.
We acquired OpTx assets and incorporated OpTx medical
oncology practice management software into our product line
for comprehensive cancer clinics. This acquisition also broadened
Varians market, giving us our first offering for stand-alone
chemotherapy clinics.
Our engineers successfully developed two more precise dose
calculating algorithms for our Eclipse treatment planning
software. Varians Eclipse product now supports stereotactic
radiosurgery planning with Trilogy as well as IMRT and IGRT.
In summary, these product introductions extended Varians
technical and market leadership during fiscal 2004. Today,
we offer a complete line of products that can be configured to
support every type of radiation treatment within an integrated,
automated, and cost-efficient system.
Progress in our X-Ray Products segment was similarly
successful in 2004. A second major customer ordered a new
version of the powerful anode-grounded CT scanning tube
that is unique to Varian, and we successfully developed several
other X-ray tubes that meet higher performance standards
set by imaging equipment manufacturers.
EMERGING BUSINESS AREAS AND ACQUISITIONS
For several years, Varian has been investing in three emerging
product lines complementary to our core business in radiation
therapy and X-ray tubes. These are brachytherapy planning
and delivery systems, flat-panel detectors for X-ray imaging,
and Linatron accelerators for nondestructive testing and cargo
screening. In 2004, these products contributed significantly
to our growth, with orders 51 percent higher and revenues
36 percent higher than in fiscal 2003. Each of these products
contributed to our profits for the year. The three emerging
businesses, together with our fiscal year 2004 acquisitions
Zmed, OpTx, and Mitsubishi Electric Corporations radiotherapy
equipment service business in Japangenerated orders in
excess of $130 million during the year.
OPERATIONS
Our strategies are leading to growth in revenues. Our
execution is leading to growth in margins. In fiscal 2004, we
extended our record of continuous improvement in operations.
In Oncology Systems, we stepped up production by more than
10 percent while improving operational efficiencies. We
reduced production times for our accelerators and completed
construction of two new test cells for expanded production in
fiscal 2005 and beyond. We successfully reduced warranty
costs and improved responsiveness to customer needs through
expansion of our help desk and customer education programs.
In our X-Ray Products segment, we enhanced efficiencies
through expanded Six Sigma programs and increased inventory
turns. Sales per employee rose in all of our business segments.
All of Varian Medical Systems accomplishments in 2004
came through talented and committed employees who are
inspired to help others. Above all, we owe our success to our
people around the world.
WHATS NEXT?
We have established ambitious goals for Varian Medical
Systems in 2005. We expect to:
Increase adoption of IMRT and IGRT throughout
the world
Expand acceptance of our new products for radiosurgery
and medical oncology
Aggressively pursue growth opportunities beyond our
core businesses
Continue to enhance operational performance and
customer satisfaction
From the accomplishments of fiscal year 2004, it should
be clear that Varian Medical Systems has the momentum for
continued growth and can make a real difference in the quality
and efficiency of modern healthcare. We look forward to
another rewarding year for our stockholders, customers, and
employees in fiscal 2005, and we thank all of you for your
continued support.
Sincerely yours,

Richard M. Levy
Chairman, President and CEO