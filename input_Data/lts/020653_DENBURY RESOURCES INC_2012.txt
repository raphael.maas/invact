Dear Shareholders :
I am happy to report the past 12 months have been a very active and
productive period at Denbury.
On the operational front, we delivered on our targets for the year as
the actions we took to address the operational challenges we faced in
2011 proved successful. Quarterly oil production from our core business,
enhanced oil recovery with carbon dioxide (CO2 EOR or tertiary
recovery), reached record levels as a result of continued expansion of our
existing CO2 floods and steady production growth at our two newest floods,
Hastings and Oyster Bayou. With our strong tertiary production r ates todate
in 2013, we are off to a positive start to the year and are optimistic
about continuing to deliver on our production growth estimates.
The headline news for last year is the series of acquisitions and
dispositions that aggregated over $4 billion of value and have all been
completed, on a tax efficient basis. This series of Company tr ansforming
transactions worked out even better than we had envisioned. F ollowing
are some of the highlights:

We sharpened our strategic focus on CO2 EOR where we have a
strategic and competitive advantage, setting us up to be a pure CO2 EOR
play. Today, nearly every field we own is either a current or planned
CO2 EOR flood.
 We increased our potential CO2 EOR reserves by nearly 210 million
barrels which, even with the Bakken divestiture, results in a net increase
in our total potential reserves. Further, we expect the additional
potential CO2 EOR will add more value for Denbury shareholders than
the potential Bakken reserves that we sold. We estimate that we now
have over 700 million barrels of potential CO2 EOR reserves in our
inventory, which gives us more than a decade of growth and will create
substantial value for our shareholders.
 We nearly replaced the production of the sold assets with production
from the acquired assets. This was accomplished with a corresponding
minor impact on current cash flow.
 We exchanged proved reserves that were predominantly proved
undeveloped for proved reserves that are primarily proved developed
producing, which significantly increases our free cash flow (cash flow
from operations less capital expenditures). In summary, our proved
developed reserves increased as a result of the transactions while our
total proved reserves decreased. More specifically, it would have required
more than $1.7 billion of future capital expenditures to realize the proved
undeveloped reserves associated with the sold Bakken assets. In contrast,
the acquired assets will require less than $100 million of future
development costs to realize the proved conventional reserves.
 In addition to the exchange of oil properties, with the net funds received
from the transactions, we acquired 1.3 trillion cubic feet of Rocky
Mountain region CO2 reserves, with up to 115 million cubic feet per day
of deliverability. These CO2 reserves will allow us to develop one of the
acquired oil fields in that region, Hartzog Draw, more quickly than would
have been possible using our CO2 reserves from Riley Ridge, our primary
source of CO2 reserves in the Rocky Mountain region. We also intend to
use these CO2 reserves for other future CO2 EOR floods in the region

The final component in this series of tr ansactions, which we completed
in March 2013, was the acquisition of ConocoPhillips property interests
in the Cedar Creek Anticline (CCA) for $1.05 billion, before purchase
price adjustments. We were able to structure the purchase as a like-kindexchange,
allowing us to defer approximately $400 million of taxes on
the gain from our Bakken exchange tr ansaction with ExxonMobil. This
acquisition increased our interests in an area that was already our largest
in the Rocky Mountain region. Consolidating our assets in this area should
allow us to benefit from economies of scale and lever age our planned
CO2 transportation infrastructure. In fact, all of the future CO 2 EOR
fields we acquired over the past 12 months are very close to existing or
planned pipeline infrastructure, allowing us to amortize that pipeline cost
over millions of additional barrels, and improving the returns on these
incremental acquisitions.
While the headline news may be the acquisitions and dispositions, we
had many other positive events during 2012. For example, we completed
on-time and on-budget the construction of our first major CO2 pipeline
in the Rocky Mountain region. The initial 232-mile segment of the 20-inch
Greencore pipeline connects the CO2 coming from ConocoPhillips Lost
Cabin gas plant to our Bell Creek oil field. This pipeline is a str ategic asset
for us as it will ultimately be the backbone of our planned Rocky Mountain
region pipeline infrastructure to transport CO2 to our oil fields.
Another notable development in the past 12 months is that we began
receiving our first man-made or anthropogenic CO2 from Air Products in
the Texas Gulf Coast region. CO2 deliveries from this facility are expected
to approach 50 million cubic feet per day later this year . This project
illustrates our unique ability to use and store anthropogenic CO2 that would
otherwise be released in the atmosphere. Were highly encouraged by the
opportunities we see to further expand our anthropogenic CO2 sources in
the coming years.
During 2012 we continued to opportunistically execute our common
stock repurchase program initially authorized in 2011. We believe our
stock has been undervalued, and even today our stock is tr ading below
LETTER TO SHAREHOLDERS

the net asset value of our proved oil and natur al gas reserves and at levels
that completely ignore the significant incremental value of our potential
CO2 EOR reserves. After a pause during the first three quarters of 2012,
we resumed the repurchase program in earnest after we announced our
Bakken exchange transaction, using the increased liquidity enabled by
the Bakken disposition. As of the date of this letter , we have purchased
approximately 35 million common shares or 9% of our outstanding shares
when we began the program, at an average price of just over $15 per share.
The program has benefitted all shareholders and has also improved our
per-share metrics by approximately 9%. Our repurchase progr am remains
in place with approximately $250 million remaining authorized, although
our repurchases have slowed as our stock price has improved. W e intend
to be opportunistic with this program throughout 2013 depending on a
number of factors.
On the finance front, in early 2013 we issued $1.2 billion of long-term
senior subordinated notes with a coupon rate of 4.625%. The interest
rate for the notes was the lowest on record for a non-investment gr ade
subordinated notes offering, which illustrates the markets confidence
in our company and our outlook. Once fully expended, proceeds from the
offering will have been used to repay about $650 million principal amount
of subordinated notes with a weighted average interest rate of about
9.7%, with most of the remainder used to repay bank debt . Following
the issuance, our capital structure is stronger than ever, with our next
scheduled senior subordinated note maturity not until 2020

In summary, it has been a remarkable past 12 months for Denbury . As
we begin 2013, we are PURE-ly  focused on what we do best: CO 2 EOR,
which we believe offers one of the most compelling risk/reward profiles
in the oil and gas industry today. We have excellent visibility on long-term
oil production growth in our two core regions; our strong balance sheet
provides us tremendous financial flexibility; and our workforce of highly
technical, dedicated and motivated employees is focused on executing our
unique strategy. We look forward to more positive results in 2013 and beyond
as we continue to build on our highly profitable, lower-risk oil platform.
Sincerely,
Phil Rykhoek
President and Chief Executive Officer
March 28, 2013