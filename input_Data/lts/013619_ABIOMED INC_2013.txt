DEAR SHAREHOLDERS,
I A M V ERY PROU D OF A BIOMED
A N D A LL TH AT W E R EPR ESENT.
As always, our company culture is motivated by our ability to impact the lives of
our patients and support our customers. In the past 12 months, I witnessed our
company transform into a resilient commercial organization that is growing,
profitable and debt free. Furthermore, we demonstrated this strength while at
the same time expanding the number of our regulatory approvals, breakthrough
innovative products, and geographical reach -all because we never lost sight of
our mission. We are anchored by our four guiding principles, recovering hearts
and saving lives; leading in technology and innovation; growing shareholder
value; and sustaining a winning culture. 

Patients First
Patients are the singular focus of Abiomed.
Our company has the unique opportunity to
make a difference in people�s lives. Patients,
their families, friends, acquaintances,
and communities are all impacted by the
benefits of our technologies, whether we are
improving quality of life or saving lives. At
Abiomed, we are constantly reminded of and
driven by the impact we have on the patients�
lives that we touch. Stories and photos of
our patients are displayed on our walls and
serve as our inspiration and motivation to
continue making a difference. This is the
foundation that defines and unifies
our company.
With our company principles and dedication to
patients motivating us through the challenges
we faced this year, fiscal year 2013 was an
outstanding success. The clinical demand for
patients requiring percutaneous hemodynamic
support is driven by the size, demographics
and complexity of the heart failure population.
This fiscal year, we were able to capture this
growth and demonstrate the Company�s ability
to execute our strategic goals while driving
operational excellence, as best evidenced by
a tenfold increase in net income over the
prior year.
Every year, our strategic goals for the
fiscal year are modified to best ref lect
the emphasis and resources needed in the

current environment to become the standard
of care in interventional cardiology. Our
goals for fiscal year 2013 were achieved.
Our progress versus goals was as follows:
1. Achieving patient and revenue growth
every quarter by maximizing the productivity
of our commercial organization
Each quarter in fiscal 2013 was a record for
both number of patients supported and total
revenue when compared over prior year.
In the fourth quarter alone, Impella usage
records were set for highest day, highest
week, and highest month in Impella history.
For the entire fiscal year, we achieved 25%
total growth with total revenue at $158.1
million and grew Impella revenue 31%
to $140.3 million. We are expanding our
field resources as well. Our U.S. field team
now consists of more than 120 employees,
and approximately one out of every three
Impella devices is implanted independently
at customer sites. Our dedicated on-site
field and training teams, plus an expanded
24x7 call center, have become a focus to
our customers and a core competency
at Abiomed.
2. Quantifying and publishing the costeffectiveness and the quality of life gains for
patients requiring hemodynamic support
There were 49 publications regarding Impella
this fiscal year, including the PROTECT II
results, which were published in Circulation, 

Journal of the American Heart Association,
as well as the publication of the PROTECT
II cost effectiveness study in the American
Health and Drug Benefits Journal. We
believe that many of these new publications
will continue to strengthen the four separate
medical society guidelines that already
include Impella (three of which were added
this fiscal year). Additionally, in January,
new dedicated physician payment rates
(Current Procedural Terminology, or CPT
codes) were implemented for Impella by the
Centers for Medicare and Medicaid Services
a significant milestone in our quest to
become the standard of care for percutaneous
circulatory support.
3. Executing on our clinical and regulatory
processes in order to provide patient access to
new heart recovery products (Impella: CP, RP,
Pediatric; Symphony; and Japan)
With the 510(k) clearance of the Impella CP in
September 2012, our physicians have the ability
to offer more flow (an incremental liter or more)
on the same percutaneous Impella platform as
the Impella 2.5. Additionally, we received FDA
Investigational Device Exemption (IDE) approval
of the Impella RP (Right-side Peripheral) to
begin the RECOVER RIGHT clinical study. The
first patient was enrolled in April and the study
will include a total of 30 patients with signs of
right side heart failure and require hemodynamic
support. The Symphony device* is also making 

clinical progress, including patient implants
in Canada. The pediatric Impella pump is also
moving forward with an approved Humanitarian
Use Device designation from the FDA. Abiomed is
now in the process of submitting a Humanitarian
Device Exemption (HDE) for the pediatric device.
Finally, we believe we are on track with Impella
approval expected in Japan by the end of calendar
year 2013, with reimbursement to follow in
6-12 months.
4. Driving operational excellence and
customer service
Our product portfolio is expanding to support
the growing patient population that needs
hemodynamic support. Our focus on
operational excellence is driving a controlled
roll-out of the Impella CP with enhanced
training resources aimed to drive the best
possible patient outcomes and customer
satisfaction. As of the end of the fourth quarter
in fiscal 2013, there was strong demand echoed
across our customer base for the Impella CP,
Impella RP** and the new AIC console, which
is now installed in 60% of our customer sites.
In Manufacturing, we completed the
successful start-up of the Impella CP
production, increased our inventory safety
stocks to six months to mitigate risk and 

expanded capacity. Our Manufacturing
organization successfully executed each
and every challenge. Additionally, we
generated $26.4 million in cash from
operating activities in fiscal year 2013,
which increased by 633% from the prior
year. In this economic climate, we have
great pride in this accomplishment and
seeing our long-term investments translate
into continued success and financial stability.
Recovering Hearts. Saving Lives.
Our vision is to become the new standard
of care for percutaneous circulatory support.
This past fiscal year has increased our 

confidence and commitment to our company
mission. I am proud of the people of Abiomed
and their ability to never lose sight of our
ultimate goal � recovering hearts and saving
lives. Our company culture defines success
as our ability to achieve positive results
with honor and integrity in all actions.
We thank you for your continued support
and we look forward to another record year. 

Sincerely,

Michael R. Minogue
Chairman, President and Chief Executive Officer