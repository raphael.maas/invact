LETTER TO OUR STOCKHOLDERS
In 2012, Kansas City Southern (KCS) reported revenues of $2.2 billion, up 7% from
2011. Carloads climbed to 2.1 million, a 5% increase. Operating income grew to $716
million, 17% higher than the previous year. And KCS reported an operating ratio of
68%, a 2.9 point improvement over 2011s operating ratio.
All the above are Company records. Despite unusual weather conditions
that negatively affected our grain and utility coal business, KCS two primary
railroad subsidiaries, The Kansas City Southern Railway Company (KCSR)
and Kansas City Southern de Mexico, S.A. de C.V. (KCSM), performed
exceptionally well.
A companys success is never defined by the performance of just one department
or division, but rather by a combination of departments working together to
achieve common goals. Given 2012s climatic and economic challenges, it
would have been impossible for KCS to attain the record earnings that it did
without the close coordination of our Operations, Marketing and Finance
teams, both in the U.S. and Mexico.
Operations
Probably no statistic better illustrates the contribution of Operations than the
fact that KCS moved 5% more freight in 2012 while keeping headcount flat.
Overall system train velocity and intermodal train speeds improved  again,
all while moving a record volume of freight over our system.
A characteristic of the KCS Operations team that is particularly impressive
is its ability to adapt swiftly to emerging challenges and opportunities. KCS
remains committed to not only controlling headcount but also having very
few management levels. There are many advantages to this streamlined
organizational structure, not the least being the elimination of bureaucratic
red tape from the decision-making process. In the case of our Operations
team, the absence of bureaucracy allows them to be nimble and rapidly
redeploy equipment, personnel and capital resources as situations dictate.
This ability to react quickly and efficiently was clearly evident in 2012. For
example, when a combination of unseasonably warm winter weather and
historically low natural gas prices resulted in a precipitous drop in the demand
for utility coal, KCS Operations immediately redeployed locomotives to other
areas of the system that were experiencing strong traffic flows. Similarly, in the
Michael R. Haverty
Executive Chairman
Kansas City Southern
David L. Starling
President
& Chief Executive Officer
Kansas City Southern
third and fourth quarters, when a serious drought
devastated much of the nations corn crop,
thereby reducing KCS grain volumes, equipment
was moved rapidly to serve other business sectors.
No locomotives were put in storage, and our
asset utilization remained high. While the unusual
conditions cost the Company revenue, through the
efficient redirection of equipment and personnel,
KCS was able to reduce operating expenses and
thus achieve record profitability.
Most important, KCS remains steadfastly committed
to operating a safe railroad for our employees,
customers and the communities we serve. In 2012,
KCSR was recognized for the sixth consecutive year
for best-in-class safety.
Marketing
Just as important to KCS record year was the
contribution of our Sales & Marketing department.
Transportation analysts and investors have correctly
labeled KCS as a growth company with an abundance
of business expansion opportunities. There is no
question that we benefit from geography as KCS
serves some of the most rapidly growing commercial
areas in North America. KCSM operates through the
heart of Mexicos burgeoning industrial centers and
KCSR serves Port Arthur and other Gulf of Mexico
locations that are experiencing dramatic growth
spurred by new crude oil discoveries in the U.S.
and Canada.
Yes, location is important, but so is having the right
people in the right places with the right mission to
turn potential opportunities into real business.
While new business opportunities are widespread
throughout KCS commodity groups, five areas
stood out in 2012 as being exceptional: automotive,
cross-border intermodal, crude oil, frac sand and
traffic out of the Port of Lzaro Crdenas. Collectively,
these areas grew by 39% and went from representing
13% of total freight revenues in 2011 to 17% in 2012.
Automotive revenues grew by 25% as both U.S. and
foreign car manufacturers continued to increase
production in Mexico, and KCS grew its market
share of finished auto business. Four new auto
plants are scheduled to begin production in Mexico
in 2014. With its efficient cross-border rail service,
as well as its direct access to the Port of Lzaro
Crdenas, KCS will be a prominent participant in
serving these new plants. In addition, when the rail
connection with the Port of Veracruz is completed,
KCSM will have the opportunity to move finished
vehicles to that Ports busy automotive export facility.
In the 2011 annual report, we discussed the
evolution of KCS international intermodal corridor,
over which we provide the only single-line, crossborder intermodal service. We pointed out that our
cross-border container traffic over the corridor grew
by 56% over the prior year. The pace of growth
accelerated in 2012, with volumes increasing by
88%. Impressive, yes, but what really gets our
attention is the size of the available market and
the vast potential for future growth of this business.
KCS now estimates that every year almost 3 million
truckloads cross the border between the U.S. and
Mexico in markets that we could serve. These trucks
come from areas that KCS, working with its
intermodal trucking partners, could serve directly or
in partnership with other railroads. Given the price,
security and efficiency advantages of rail over truck
in terms of cross-border transit, an ever-growing
percentage of the total business is likely to continue
to convert to intermodal. Even at the current
exceptionally high growth rate, KCS still has less
than 2% of the addressable market, which suggests
that we have the opportunity to enjoy considerable
growth in this business sector for many years.
The U.S.s growing energy sector has also provided 
exciting growth opportunities for KCS. It wasnt
many years ago that the idea of the U.S. ever
becoming energy independent seemed more of
a pipe dream than a reality. That has definitely
changed. In 2012, the International Energy Agency
issued a report which projects that the U.S. could
attain energy independence as soon as 2030, and
could displace Saudi Arabia as the worlds largest
oil producer by 2020.
The main catalyst for turning what once seemed a
fantasy into a very real possibility has been the
development of new frac drilling technologies that
have led to the discovery of previously untapped
crude oil resources in the U.S. and western Canada.
What makes this important for railroads is that the
demand for crude oil to make refined chemical and
petroleum products is so great that energy analysts
have come to believe that it can be met only through
a combination of both pipelines and rail service.
By virtue of KCS location in the Gulf region, where
we directly serve some of the largest refineries and
crude storage and distribution facilities in the world,
we are experiencing triple-digit volume growth in
the movement of crude oil.
Currently, refineries in the Port Arthur area require
more than a million barrels of crude per day, and
the demand continues to grow. Working with its rail
partners, KCS is moving crude from the Bakken
Region (North Dakota), western Canada, and the
Eagle Ford area in west Texas.
Our industry also plays a key role in supporting new
shale drilling sites by delivering critical input products.
The new drilling technologies that have opened up
new crude and natural gas resources require vast
amounts of frac sand. The movement of frac sand,
predominantly from the upper Midwest to the
Southwest, accounted for another of KCS most
dramatic 2012 growth areas, as volumes grew
by 20%. Again working with other railroads, KCS
provides frac sand for drilling sites for both oil
(Eagle Ford and Permian Basin) and natural gas
(Haynesville).
Another important growth area in 2012 was traffic
from the Port of Lzaro Crdenas, which, has been
the fastest-growing container port in North America.
Over the past three years, KCS volumes in and out
of Lzaro Crdenas have increased at a compounded
annual growth rate of 28%.
Lzaro Crdenas is emerging as a preferred Pacific
port to serve central Mexico, especially for goods
coming from Asia. KCS is also seeing expanded
volume growth from Lzaro Crdenas into the U.S.,
particularly to the Houston market. When the
Hutchison Port Terminal facility is completely built
out, it will have an approximate 2.6 million TEU
capacity. The Mexican government has awarded a
second port concession at Lzaro Crdenas to APM
Terminals. This facility is projected to have an annual
throughput capacity of 4.1 million TEUs. As the sole
railroad serving Lzaro Crdenas, KCS business in
and out of the port will grow steadily, as both container
facilities expand and as the recently opened bulk
and motor vehicle concessions ramp up.
Finance
In 2012, outstanding progress was made in continuing
to strengthen the Companys balance sheet and
maximize cash flows. Executing on the Finance
strategy, KCS used the proceeds of a new term loan
to retire $275 million of KCSRs outstanding 8%
senior notes. This action generated approximately
$17 million in annual pre-tax interest expense savings.
Since the end of 2009, KCS has reduced its debt by
$372 million and its annual pre-tax interest expense
by $73 million.
Throughout the year, Finance maintained an active
dialogue with credit rating agencies to keep them 
fully apprised of company results and developments.
These efforts facilitate the continued progress toward
KCS objective of achieving investment-grade ratings
from the agencies which, once accomplished, will
enable KCS to issue debt with lower interest costs,
longer maturities and less restrictive covenants.
During the year, the Company established a
relationship with Fitch and received investmentgrade ratings from that agency for KCSR and KCSM.
Moodys upgraded the KCSR rating to investmentgrade and the KCSM rating to one notch
below investment grade. Finally, KCSR and KCSM
attained ratings upgrades from Standard & Poors,
putting each entity one notch below investment grade.
Both KCSRs and KCSMs credit facilities were
amended to extend maturities, incorporate more
favorable covenants and allow for conversion from
secured obligations to unsecured when investmentgrade ratings are attained from two of the three
primary rating agencies. In the fourth quarter of
2012, KCSR received the second investment
grade rating and its credit facility was converted
to unsecured.
Perhaps the most profound testament to KCS
strengthened financial position is the initiation of a
quarterly cash dividend on the Companys common
stock by our board of directors in March 2012. To
have arrived at a point where that decision could
be made is a validation of our corporate strategy,
which has turned a vision of a unique rail franchise
linking two countries and two economies into a
successful reality.
While KCS is still, first and foremost, a growth
company that is committed to financially supporting
its multiple new business opportunities, the
Companys cash flow and balance sheet afford us
the opportunity to return some of our profits back to
our stockholders. In January 2013, the board of
directors approved a 10% increase in the dividend
on KCS common stock to $0.215 per share.
Summary
We head into 2013 with sustained conviction in
our ever-expanding growth opportunities and our
ability to execute as a team at a high level in any
environment. Undoubtedly, this past year offered
challenges to our utility coal and grain business.
However, the fact that we produced record top-line
and bottom-line results in the face of those challenges
speaks to the strength of our organization and
our employees.
When we look beyond 2013, our outlook and
conviction in KCS growth thesis continues to be
strong. Our strategically located, single-line rail
network between the U.S. and Mexico is in place
and poised to take advantage of the opportunities
that we see ahead. Near-sourcing to North America,
particularly Mexico, is a reality and we believe the
rate at which companies open manufacturing plants
in areas that we serve will only accelerate. In the
U.S., the shifting dynamics of our countrys energy
sector offer a host of opportunities for crude-byrail and KCS has the opportunity to benefit from
these emerging markets for several years to come.
We believe that these, plus other exciting business
growth opportunities add up to a franchise that
can continue to deliver growth to its stockholders
beyond 2013.
As always, we very much appreciate the ongoing
support of our stockholders and remain committed
to operating the Company in a manner that sustains
and expands KCS extraordinary business growth
opportunities.