TO OUR SHAREHOLDERS

Over the 30-year history of BMC, fiscal 2011 surely
ranks as among the most important for our company
and our shareholders:
 It was a year of record performance, in terms of
total bookings, revenue, non-GAAP diluted earnings
per share (EPS) and cash flow from operations.
Total bookings rose 13% to $2.2 billion, revenue was
up 8% to $2.1 billion, non-GAAP diluted EPS grew
12% to $2.99, and operating cash flow was up 20% to
$765 million. We delivered at or above our expectations
for each of these metrics.
 It was a year in which we demonstrated our ability
to achieve these results while at the same time
making the investments in internal development,
sales and services necessary to increase our
competitive advantage. These investments reflect
our view that there are substantial market opportunities
ahead and our desire to ensure we are
properly positioned to capture them.
 It was a year in which we transitioned from being a
strong, moderate growth company to one characterized
by sustained, accelerating growth led by a
solid Enterprise Service Management (ESM) engine
capable of delivering ongoing double-digit growth.
IT in transition
Underlying our past performance and future prospects
is a powerful trend that is fundamentally
reshaping the enterprise information technology (IT)
landscape. Enterprises of all shapes and sizes, across
all industries and geographic borders, are increasingly
seeking to build agile, flexible IT infrastructures that
provide the right type and level of service at the right
cost. In a competitive, global business environment,
IT needs to be structured, managed, staffed and deployed
as a servicea service that business leaders
can leverage to achieve their goals and objectives.
Thats exactly what we do.
BMC is in the business of building agile, flexible IT
infrastructures. We allow IT to offer a service platform
that enables enterprises to align and manage their
technology resources more quickly, efficiently and productively.
With our platform, the underlying resources
to deliver the services can be dynamically sourced and
brokered, further driving down the total costs of those
services. Importantly, our platform is vendor agnostic,
enabling seamless management across heterogeneous
technology environments.
This vision of IT is what drives our Business Service
Management (BSM) strategy. Its the answer to customer
demands for choice in managing existing and
emerging technologies, in driving standardization of
horizontal IT processes across diverse platforms and
in simplifying and automating complex, heterogeneous
IT infrastructures. Its the framework around which we
plan our internal investmentsin technology research
and development and also in the continued evolution of
our sales and services platforms. Its also the discipline
that surrounds our acquisition activities

Our strategy in action
During fiscal 2011, we took positive steps forward
in all of these areas. We launched two important
new offerings: our BMC Cloud Lifecycle Management
solution as well as our BMC Control-M
Self-Service solution. These are two of the most
successful products launched in our recent history,
and they underscore the robust pipeline of
innovation that were fostering and bringing to
market to enhance our competitive advantage.
We also strengthened our ESM sales team,
achieving a 20 percent increase in the average
productive sales headcount compared to the prior
year while significantly slowing attrition. And we
expanded and enhanced our professional service
capabilities, generating a 37 percent increase in
revenues in this key area.
In addition to these internal initiatives, we also
continued to acquire and integrate complementary
technologies. During fiscal 2011, this included two
businesses that support our core BSM strategy.
The first, GridApp Systems, significantly reduces
the cost, complexity and risk associated with
deploying and configuring enterprise databases
in physical, virtual and cloud environments. The
second, the software business of Neptuny, extends
BMCs existing leadership in physical and virtual
capacity management. Shortly after fiscal 2012
began, we also acquired Coradiant, which makes
us a clear leader in next generation application
performance management. Coradiant provides
us with state-of-the-art end-user experience
monitoring for enterprise, Software-as-a-Service
(SaaS) and cloud-based applications.
Our strategic partnerships and allianceswith
global leaders such as Accenture, Cisco, CSC, Dell,
Ingram Micro, Red Hat, salesforce.com, Wipro and
othersare an important part of our strategy to
expand and deliver value to our customers and
shareholders. Our strategic alliance with Cisco on
their Integrated Cloud Delivery Platform is a case
in point. Its focused on enabling service providers
to develop flexible infrastructure and cloud offerings
for their customers, and BMC technology is at
the heart of this platform.
This and our other alliances create powerful,
practical, complete offerings for our customers
and they enable our partners to offer managed
services to their customers, to build clouds for
their own use and to resell our professional services
and technology. Our focus here and across
the board is on enabling customers to optimize
their IT investments by leveraging new ways of
doing businessvirtualization, cloud computing,
SaaS, mobility and other technologiesthat
increase speed, responsiveness, efficiency and
productivity of their business.
Becoming the platform of choice
All of these effortsinternal development, sales,
professional services, acquisitions, partnerships
and alliancesare ultimately aimed at one
important goal: helping enterprises leverage our
technology to significantly increase their IT flexibility
while containing or reducing costs. During
fiscal 2011, the success of these initiatives became
increasingly clear. We generated nearly 50 cloud
solutions wins from Global Fortune 2000 companies
and many of the worlds largest telecommunications
and service providers. Our success
in helping customers build and manage clouds
demonstrates one important way were helping
them develop a more flexible infrastructure.
Our SaaS offerings are another way in which we
are providing agile, flexible IT solutions.

BMC Remedy OnDemand enables enterprises to
access our full ITIL-based, industry-leading suite
of service desk offerings. Our BMC Remedyforce
offering, which is marketed and sold both through
salesforce.com and directly by BMC, is a powerful,
easy-to-use solution for customers looking for a
service desk built natively on top of the salesforce
platform, Force.com. It was just a little over a year
ago that we launched our initial SaaS offering. In
the past year, we generated approximately 100
SaaS wins from both our BMC Remedy OnDemand
and BMC Remedyforce offerings.
Success across our business units
In these and other ways, fiscal 2011 was all
about leveraging the strength of our technology
and market position into customer success
and ultimately our growth. Both of our business
unitsESM and Mainframe Service Management
(MSM)performed at or above plan for the
year. ESM license bookings increased 21 percent
and ESM revenues rose 12 percent in fiscal 2011.
The number of ESM license transactions over
$1 million increased 36 percent for the year,
demonstrating our success in winning a greater
number of larger, multi-discipline, platform deals.
The number of mid-sized deals, which demonstrates
how we are increasing our footprint, also
increased. Transactions between $500 thousand
and $1 million were up 10 percent for the year.
From an earnings perspective, ESMs non-GAAP
operating margin increased by 1 percentage point
and its non-GAAP operating income rose by 17
percent to $280 million.
Our MSM business continues to play an important
role in our success. Total and annualized MSM
bookings for fiscal 2011 increased 4 percent. Over
the course of the year, we increased the spend
rate on our top transactions, and we saw continued
strength in new product sales adding over
350 new product placements to existing customers.
For the full year, MSMs non-GAAP operating
income was $453 million, up 4 percent, and its
non-GAAP operating margin was 58 percent, up
1 percentage point compared to a year ago.
Workload automationwhich includes our BMC
Control-M product lineis about one-third of our
MSM business. We continued to see strength in this
business, with bookings growth in the mid-single
digits during fiscal 2011. Over the past year, we
added 88 new BMC Control-M customers and expanded
existing relationships with BMC Control-M
customers by adding 195 new product placements.
The year ahead
We expect fiscal 2012 will be another year of opportunity
for BMCan opportunity to broaden
our customer base, expand our relationships
with existing customers, enhance our technology,
increase our footprint, develop new alliances and
partnerships, further strengthen our financial footing,
and build value for shareholders. We are very
excited about the potential that lies ahead for BMC.
The demand from enterprises for a more flexible
IT infrastructure remains strong. Were seeing it
across geographic regions and industry segments.
Were also seeing it across larger and
smaller enterprises. We believe this demand will
continue to fundamentally transform IT management
in the years aheadand we are very well
positioned amidst this change to capture the
market opportunities that lie ahead.
In closing, I would like to thank our board of directors
for their ongoing counsel and insights. I would
also like to express my gratitude to our more than
6,000 employees around the world, who drive our
success each and every day. And I deeply appreciate
the support of our shareholders and your
confidence in our management team.
Sincerely,