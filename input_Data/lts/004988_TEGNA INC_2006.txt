

    LETTER TO SHAREHOLDERS

    We knew 2006 would be an exciting year for Gannett,             mission is to successfully transform Gannett to this new
    and we weren't disappointed.                                    environment; to provide must-have news and informa-
        Inside Gannett and out, remarkable changes occurred tion on demand across all media, ever mindful of our
    that set our company off in new and significant direc-          journalistic responsibilities.
    tions. The consumer and technology revolutions that had             We then set about creating a plan that not only
    been developing for years took hold in 2006 and                 would help us achieve those goals, but also would be
    changed the marketplace for information. Customers              realistic, attainable and flexible enough to adapt to the
    demanded and received information how, when and                 rapid changes occurring in our industry. The plan had to
    where they wanted it.                                           recognize the value of USA TODAY as a national brand,
        Gannett was ready for the challenges, and the terrific       as well as the unequaled ability of our community news-
    opportunities.                                                  papers and television stations to deliver local  indeed
        Early in 2006, we announced a new strategic plan            hyper-local  information.
    designed to embrace these trends and remake                                      Most importantly, the plan had to redefine
    the company to deliver on them. Then we                                         Gannett's relationship with its customers.
    set about putting the plan into action,                                             No longer could we decide what our
    one initiative after another. By year's                                               customers wanted and needed, then
    end, we had made great progress in                                                      deliver it our way. Customers had to
    transforming the company in both                                                        become the central focus of all we do.
    tangible and intangible ways.                                                           From product development, to con-
        Most importantly, we made this                                                      tent gathering, to advertising sales, we
    transformation while delivering for our                                                needed to put the customer first. This
    shareholders as solidly and consistently                                            is the key strategic shift for Gannett.
    as we always have. Our operating revenues                                            The plan we ultimately crafted is elegant
    from continuing operations for 2006 rose to                                     in its simple premise: Gannett will achieve
                                                          
    a record $8.03 billion, a 5.7 percent increase                                  its mission and vision by supporting and
                                                       
    over 2005. Our operating income was $2 bil-                                     enhancing our core businesses  our local
                                                        
    lion; net income was $1.16 billion. Earnings                                    newspapers, USA TODAY and our television
                                                             
    per diluted share for the year were $4.90.                                      stations  while growing a robust, interna-
        These results are among the best in the business. We        tional digital business.
    recognize how important it is to continue to deliver for            First up was the creation of Gannett Digital  an entity
    our shareholders while making this transition. We also          empowered to take the necessary steps we need to grow
    know it is absolutely critical to continue our transforma-      and attract customers. We would do this through our tra-
    tion as a company in 2007 and beyond. We are commit-            ditional smart acquisitions, as well as through alliances,
    ted to being nimble, innovative and customer centric:           partnerships and joint ventures.
    We must be to survive and thrive  and deliver for our              The plan also set out three basic initiatives designed
    shareholders into the future.                                   to fundamentally change the way we operate and deliver
        So let me tell you what we did in this remarkable year, both for the core and for digital. They are:
    and why we did it.                                               Drive innovation throughout the company, beginning
                                                                    with the creation of a Gannett Center for Design and
    Gannett's Strategic Plan: Roadmap to the future...              Innovation. The goal: harness the creativity and ingenu-
                                                                    ity of Gannett's employees to find ways to grow the top
    We began developing our strategic plan by defining our
                                                                    line.
    vision and mission for Gannett. Our vision is that con-
                                                                     Transform our newsrooms into Information Centers to
    sumers will choose Gannett media for their news and
                                                                    gather and disseminate news and information across
    information needs, anytime, anywhere, in any form. Our



multiple platforms 24 hours a day, seven days a week.            reach exactly the right customer.
The goal: to provide what customers want and advertis-               We made targeted affiliations and partnerships in
ers need.                                                        2006 and early 2007 to enhance our mobile and video
 Find, develop and retain the best and the brightest            strategies which, when added to PointRoll  the rich
employees through a robust Leadership and Diversity              media company we acquired in 2005  makes clear the
program. This is the initiative that makes the other ones        breadth of our infrastructure development.
possible.                                                            Toward the end of the year, we announced our
     I am pleased to say we made significant progress on         plan to link media companies in an online advertising
all these fronts in 2006. It was our plan to set the guide-      sales network. The network will ease the way for adver-
posts and let our fabulous employees lead the way. They          tisers who want to place ads on multiple sites across
did, way beyond our expectations.                                companies.
     I'll start with a discussion of Gannett Digital. Its mis-       Meanwhile, our digital revenues for 2006 surpassed
sion: to move Gannett from a newspaper and television            $400 million, substantial growth over 2005.
company with affiliated Web sites to a digital power-                And that's just Year One for Gannett Digital. There is
house, capable of capturing a growing share of what              much more to come in 2007 as we begin to integrate our
pundits believe will be a pool of Internet advertising           three strategic initiatives with the growth of digital as
dollars in excess of $20 billion by 2008.                        well as the enhancement of our core assets.
     With its formation, Digital's top priority quickly              The three initiatives  again, they are Innovation, the
became CareerBuilder, which cemented its position as             Information Center and Leadership and Diversity  are
the No. 1 online site for job postings in 2006. We               designed to have a cultural as well as a practical impact
increased our share in the company, which we own in              on digital as well as our core operations. We had no idea
partnership with Tribune Company and McClatchy, to               when we began 2006 just how profound these changes
42.5%. We did the same with ShopLocal, an online mar-            would be for the company.
ketplace for shopping and advertising also owned by
                                                                 Innovation
the partnership. And we increased our share in Topix.net,
an information aggregator.                                       Gannett always has been a leader in innovation.
     With these enhancements and with our partnership               Arguably, the single most important innovation in the
in Classified Ventures  Cars.com, Apartments.com and a          media industry in the past 50 years came from Gannett 
number of other sites  Gannett is extremely well-posi-          the creation of USA TODAY. But today, innovation is a
tioned in Internet classifieds.                                  whole new way of doing business on a daily basis. To
     Gannett Digital also set about creating the infrastruc-     compete in our industry  in fact, in our economy 
ture our Web sites need to give our customers the news           Gannett must again be at the top of the innovation
and information they want, when they want it. Through            game. This was our goal as we entered 2006.
a variety of ways  acquisitions as well as alliances, part-        We began by creating the Gannett Center for Design
nerships and joint ventures  we will be able to support         and Innovation, nicknamed "THE DIG" in a company-wide
multiple platforms: local search, mobile, mobile search,         contest. THE DIG solicits ideas from employees, and guar-
video: any and all new technologies.                             antees their evaluation. To date, more than 1,000 ideas
     In May 2006, we acquired Planet Discover to enhance         have been submitted, several have been selected for a
our local search capability. Rollout began almost imme-          more thorough scrutiny and business models are being
diately and our customers already are benefiting from            created. You will hear more about these ideas in the
the ability to find what they want and need close to             next several months as some evolve into new Gannett
home. We affiliated with Revenue Science, which pro-             businesses.
vides behaviorally targeted advertising on a national,              Meanwhile, the ideas keep coming and the knack for
regional and local basis. Advertisers crave this ability to      innovation is spreading through the company with local
4


    initiatives, mini-DIGS, and the creation of a company-       their newsrooms into Information Centers.
    wide Innovator of the Year award.                                What happened was remarkable. Changes occurred
                                                                 that showed we were on the right track  as far as
    Information Center                                           appealing to our customers was concerned. For example:
                                                                  Breaking news was distributed to the Web as soon as
    What exactly is an Information Center? A place? A
                                                                 it happened. Page views climbed. The more we moved
    process? A state of mind? Actually, it is all of those.
                                                                 quickly to tell people what was going on, the more they
        For generations, the heart and central nervous system
                                                                 visited the Web sites. At some of our test locations, page
    of journalism has been the newsroom. It is not only a
                                                                 views increased by double digit percentages, year over
    location  the place where news and information is
                                                                 year: more than 35% at Des Moines for instance. As the
    gathered  but also the cultural core of a newspaper or
                                                                 year wound down and more sites installed Information
    television station. It is where the values of accuracy and
                                                                 Center operations  particularly databases  page views
    fairness, and the First Amendment, are held and trans-
                                                                 surged: 57% in Asbury Park, N.J.; 36% in Westchester, N.Y.;
    ferred from one generation to another.
                                                                 33% in Fort Myers, Fla.; 55% in Shreveport, La. Stickiness
        The newsroom also is a process. Its traditional focus
                                                                  the time people spend on the site  increased by
    has been to publish a paper newspaper at the end of the
                                                                 similar margins. Pensacola, for instance, recorded a 36%
    day or produce the evening and late night newscasts.
                                                                 increase in page views and a 71% increase in stickiness.
    Other delivery systems  the Web, mobile phones, e-mail
                                                                 Monetization of these numbers is following close
     were secondary. These priorities needed a fundamental
                                                                 behind as more and more sales people are being trained
    overhaul.
                                                                 in Web sales.
        Our challenge became: How do you maintain the
                                                                  We began teaching print journalists how to produce
    cultural role while transforming the process into one
                                                                 video. A team from our Broadcast Division went from
    that was platform agnostic with a 24/7 mentality?
                                                                 newspaper to newspaper until there were more than 320
        Our answer is the Information Center.
                                                                 videographers at 62 locations in the U.S. and 120 more at
        We began this transformation in the Newspaper
                                                                 Newsquest in the U.K. News video streams jumped from
    Division, with its 89 daily, community newspapers and
                                                                 3,700 at our community newspapers in March to more
    Web sites and nearly 1,000 non-dailies. The goal was to
                                                                 than 900,000 in January. Total numbers of video streams
    shift the focus of the newsgathering to customers 
                                                                 at our U.S. properties, including ads and news content,
    what they want, when they want it  then create a
                                                                 surpassed 6 million by January, an increase of more than
    process that delivers across multiple platforms, all day,
                                                                 2 million during the latter half of 2006.
    every day.
                                                                     Information Centers now are being created through-
        In the case of the Newspaper Division, customers
                                                                 out the Newspaper Division. All are due to be up and
    have told us they want local news; in some cases, very
                                                                 running by the end of May 2007. Newsquest is begin-
    local news. They want data about their communities 
                                                                 ning its transformation and Broadcast, with its similar
    what's happening when and where, as well as what their
                                                                 goal of delivering hyper-local news, is adapting the
    governments are doing. They want to participate in their
                                                                 concepts to its newsrooms across the company.
    community conversation and they want all of that via
                                                                     At USA TODAY, with its national rather than local
    video, podcasts, mobile as well as online, in the daily
                                                                 focus, the challenges are somewhat different. USA
    newspaper and in multiple niche publications.
                                                                 TODAY was among the first in the industry to create a
        A team from the Newspaper Division defined seven
                                                                 news-based Web site in 1995 and is now among the top
    basic tasks  digital, public service, community conversa-
                                                                 10 news sites in the U.S. It has been delivering news
    tion, local, custom content, data and multimedia  and
                                                                 across multiple platforms for years, including video,
    launched pilot projects in 12 locations during the spring
                                                                 mobile and Mp3 players. In many ways, the key goals of
    and summer of 2006. Eight of the sites were assigned
                                                                 the Information Center  delivering information 24/7
    one or more tasks. Three would completely transform


across multiple platforms  already had been achieved.            The Moms sites are the clearest example yet of what
     For instance, during 2006 USA TODAY became the           our strategic initiatives can do. They were the product of
first news operation to have its own location on iTunes       innovation, executed through our Information Center -
for news podcasts. USA TODAY LIVE, the video operation, with its ability to deliver information to multiple plat-
produced TV shows such as Soul of a Champion and              forms  and monetized through our audience-based
USA TODAY's WeightLoss Challenge. Links to 4INFO, the         selling initiative.
mobile search company, were published in the newspa-
                                                              Leadership and Diversity
per next to stories and readers were able to get 24-hour
news updates via their mobile phones.                         As I have said on many occasions, Gannett's employees
     In 2006, USA TODAY began blending its dot-com            are our most valuable asset. Their hard work, intelligence
newsroom with its print newsroom to refocus its               and creativity are the main components of our success.
energy on an Information                                                                 While the Strategic Plan pointed
Center-like, platform-agnostic                                                           the way, our employees are
                                              Underlying all aspects of
strategy.                                                                                responsible for devising what to
                                                the Information Center
     Underlying all aspects of the                                                       do and how to do it. Their com-
Information Center initiative are                                                        mitment to change has made
                                               initiative are customers.
customers. If we understand                                                              our transformation possible.
                                                If we understand what
what they want and how they                                                              We could not have come so far,
                                              they want and how they
want it  and then we deliver it                                                        so quickly without them.
we will be better able to bring                                                             That is why our Leadership
                                                 want it  and then we
them together with advertisers.                                                          and Diversity initiative is per-
                                                 deliver it  we will be
     Linking advertising with the                                                        haps the most important step of
                                             better able to bring them
products developed in the                                                                Gannett's transformation. We
                                             together with advertisers.
Information Center enables us to                                                         must develop and retain the
transform the way we sell ads                                                            best workforce in the industry
from product-based to audi-                                                              through a talent management
ence-based selling. In the past, for instance, we sold ads    program that incorporates diversity as its core precept.
based on traditional factors such as size, cost, location in  By diversity, I mean not only racial, ethnic and gender
the newspaper and number of days to run.                      diversity  although that is vitally important  but also
     Now, with the Information Center, we are able to sell    diversity of thought, skill and ambition.
audiences. Advertisers who want to reach a specific               I believe that to become truly customer centric, we
group will be able to buy ads in multiple products that       must reflect our communities. We can't appeal to cus-
target and reach that particular group. Training in this      tomers unless we know what they want, and we can't
style of selling is underway across the Newspaper             know what they want unless we reflect them.
Division.                                                         To achieve these goals, our plan has identified four
     While this revolution in ad sales is just beginning,     key steps: recruit the best people; train and develop our
we already are seeing successes. An example is                employees across multi-platforms and divisions; retain
Indymoms.com. This began at The Indianapolis Star as a        top talent and grow the future leaders of this company;
way to reach one of our most elusive customers               and execute diversity strategies that ensure our work-
women with incredibly busy lives. Advertisers crave this      force will reflect our communities.
group, and ad sales for this Web site already are exceed-         In late 2006, we re-energized our college recruiting
ing budget. Cincymoms.com, Cincinnati's version of the        programs to include an innovative Entry Level Intern
site, rolled out in January and other sites are launching     Program. We redesigned our careers pages at
across the company.                                           www.gannett.com and linked them to CareerBuilder.
6



    Our in-house training and leadership development pro-              Meanwhile, we continue to produce top-notch
    grams were reconstituted; and we have formed a new             products. Our devotion to quality hasn't diminished one
    Leadership and Diversity Council with members from             iota as we move forward on our plan. In 2006, Gannett
    each division and corporate.                                   operations won more than 1,100 state, local and national
        All of these plans and programs will be tied very          journalism awards, including two National Edward R.
    closely to our other strategic initiatives, including the      Murrows, a White House Press Photographers award, an
    Information Center, audience-based ad sales, innovation        EPpy for usatoday.com, a George Polk Award and four
    and the growth of our digital business. I believe our          seats on Presstime's "20 Under 40" panel.
    Leadership and Diversity initiative makes our other
                                                                   Financial discipline
    initiatives possible.
                                                                   Finally, under the plan, we refinanced debt to assure flexi-
    Acquisitions and enhancing the core                            bility in our fiscal management and acquisition strategy;
    A key part of the strategic plan is to grow our core and       we reauthorized our share repurchase program for up to
    digital businesses with disciplined and targeted acquisi-      an additional $1 billion; and declared a 7% increase in
    tions. In 2006, we made a series of acquisitions and invest-   our dividend. We kept costs in line with revenues 
    ments valued at about three-quarters of a billion dollars.     something we always have done well. We began the
        In addition to Gannett Digital's Planet Discover, we       consolidation of our customer service centers, moved
    acquired second TV stations in two of our strongest            toward regional toning centers and brought together
    markets, Atlanta and Denver. With our Jacksonville, Fla.,      printing facilities wherever it made sense. Newsquest
    stations, we now operate three duopolies.                      kept a close watch on revenues and balanced costs as it
        Duopolies are smart investments. With two stations in      worked through a second year of advertising challenges
    a market, we are able to deliver more diversity of view-       in the U.K. As always, our fiscal management in 2006 was
    point to our communities and better programming to our         second to none.
    viewers. Meanwhile, we can improve efficiency in the back
                                                                   Looking ahead
    room, in areas such as technology and advertising sales.
        We added to our print ownership by expanding our           Culturally, procedurally and philosophically, Gannett
    California Newspapers Partnership with some former             made a huge leap forward in 2006. We created the strat-
    Knight Ridder properties and bought the small but              egy and began the transformation that will create a great
    strategic Marco Island Sun Times in Florida. We acquired       future for Gannett.
     to great fanfare  the Florida State University school          Now the hard work begins. Executing our plan 
    newspaper and in early 2007 acquired our second stu-           becoming truly customer centric, connecting innovation
    dent paper, The Central Florida Future, at the University      with results, growing our digital business by multiples and
    of Central Florida.                                            enriching our core businesses  will be the focus of 2007.
        In the fall, we announced the launch of Gannett Video         With our employees, our discipline and our plan, I am
    Enterprises. GVE's role is to take the work our broadcast-     confident 2007 will be as transformative as 2006.
    ers do everyday, repurpose and customize it, then make it
    available to others on multiple platforms. This will enable
    us to find new audiences for the wonderful stories we do
    that were only being seen once or twice on our stations.
        Significantly, we completed the launches of High-
    Definition newscasts at seven of our television stations
    and wrapped up our progress by streaming the State of          Craig A. Dubow,
    the Union address live in high definition from WUSA-TV         Chairman, President and
    in Washington, D.C.  a first for the television industry.     Chief Executive Officer
