Letter to Stockholders
Last years letter to stockholders detailed the
Companys strong portfolio and fi nancial performance.
Most important, we discussed the merger
with Windrose Medical Properties Trust, a move that
dramatically accelerated our entry into the medical office
building and acute care space. By expanding our investment
reach across the full spectrum of senior housing and health
care properties, we have enhanced our portfolio diversification
and growth potential. As we have added development and
property management capabilities, we have opened new
opportunities for accretive investing and have also firmly
established our position as an industry leader.
Before laying out our program to date and plans for
the future, I will highlight the signifi cant accomplishments
in 2007.
2007 Highlights
1. Investments. We completed gross investments of $1.2 billion in 2007, including the
acquisition of 17 additional medical office buildings and the Paramount property management
group from Rendina Companies. Our investments in medical offi ce buildings
and senior housing drove the private pay revenue percentage of our portfolio to 65%.
2. Portfolio Performance. For 2007, payment coverage at the property level improved to
1.99 to 1.0. Diversification was significantly improved, with our top five operators constituting
only 27% of real estate investments.
3. Company Performance. We outperformed the REIT industry and the broader market
generally with a total stockholder return of 9.2% for 2007. Additionally, the Company
was added to the S&P MidCap 400 Index in November.
4. Dividends. Our February 2008 dividend payment was our 147th consecutive payment.
In January, the Board increased our $2.64 annual dividend to $2.72, commencing in May
2008.
5. Ratings Improvements. We received ratings increases on our senior unsecured debt to
Baa2 from Moodys Investors Service and BBB from Fitch Ratings.

6. Capital. Our access to reasonably priced capital continued in 2007, as we raised a total of
$894 million --- $494 million through the issuance of common stock, including the dividend
reinvestment program, and $400 million through the issuance of 4.75% Convertible
Senior Unsecured Notes.
7. Line of Credit. We increased our unsecured line of credit from $700 million to $1.15
billion. Our borrowing costs on the line were reduced by 20 basis points, to LIBOR plus 60
basis points.

Evolution of Health Care and Senior Housing
We believe that a number of factors are driving changes in health care and senior housing.
Technological advances and pharmacological breakthroughs are making non-invasive and
minimally invasive procedures a focal point of the health care delivery system. Th ese
advances and breakthroughs contribute to the longevity of seniors and a more active
lifestyle later in life.

Another key factor driving changes is the primacy of the patient. In the area of
health care decisions, information relating to health care quality and costs is slowly and
unevenly being made available to the marketplace. Over time, this information will be
refined and increasingly available so that outcomes and costs can be compared on an
apples to apples basis across systems and providers.
In response to these changes, real estate platforms are changing to meet customers
needs. Health systems are building facilities that are accessible and functional, both
for the customer and the physician. Health systems are increasingly decentralizing
their off erings and improving access to their services. They are extending their reach
in an attempt to go to the consumer and broaden the brand of the system. Th ey are
also confi guring their systems to offer one-stop ease for their customers.
Health systems are building medical office buildings, often with ambulatory
surgical centers, in rapidly growing suburbs. Th ey are constructing emergency departments
or wellness facilities, frequently with the longer term goal of building a
smaller hospital on the campus once volumes and patterns are established. Some

systems are jumpstarting this process by initially constructing a hospital with
attached medical office buildings.
Many systems are also expanding their platforms through operating joint
ventures with doctors. Clearly, health systems wish to align with the physician
gatekeepers for health care. Yet, there is some caution in forming such relationships
because of concerns about future legislation that could place additional constraints
on such alignments.
In the senior housing and care space, we are witnessing changes that reflect
the customers desire to be near medical care, retail and family, as well as to experience
intergenerational interaction. Seniors wish to be integrated into communities --- not
isolated, even within superior quality residences.
To meet these needs, we anticipate developing and investing in even larger
and more comprehensive health care and senior housing communities. We are
evaluating projects that include acute care facilities and medical office buildings
surrounded by continuing care retirement centers (CCRCs) or other combination
senior housing projects, frequently as part of a planned community that contains
retail, schools and offices.

Let me describe a number of our projects that demonstrate some of the ongoing
changes both in health care and senior housing.
Acute Care Facilities and Medical Office Buildings
Th e following photographs depict acute care facilities and/or medical office
buildings that are indicative of trends we are supporting in the acute care space.
Some of these facilities involve joint ventures with physicians. Each is designed to
be more functional and user-friendly, both for health care providers and patients.

Recent HCN Investments
Combination Facilities and CCRCs
Senior Housing and Care
On the senior housing and care side, we have a clear preference for combination
facilities. We believe that these facilities allow seniors to age in place.
Moreover, larger combination facilities and CCRCs contain expansive common
areas and attractive campuses. They also generally provide more amenities and
services, including wellness or fitness programs and other activities. Many of
these CCRCs and larger combination facilities are self-contained communities.
Increasingly, we are supporting operators who place these facilities within
larger communities, giving residents access to health care, retail and
intergenerational interaction.
Th e following photograph and renderings depict several of these larger
combination facilities and CCRCs. Th e facilities include those with an array of
services and include both rental and entrance fee communities. Each
attempts to create a community with more focus on wellness and active
lifestyles for the residents. We enthusiastically support these consumer-friendly,
high-end communities. At year end, combination facilities and CCRCs comprised
27% of our portfolio.

Recent HCN Investments
Full Spectrum Platform
Health care and senior housing are evolving in ways that require a partner such as Health
Care REIT, Inc. to have a comprehensive knowledge of the industry and a full set of tools to
provide complete real estate solutions for the systems or providers. To that end, we acquired
Paramount Real Estate Services, a national property management company, in May 2007.
Mike Noto and his team are based in Jupiter, Florida and currently manage our portfolio
of 121 medical office buildings with approximately five million square feet in 18 states. We
intend to be the health care property management company of choice throughout the United
States. We also believe that there are significant additional management, development and
investment opportunities within our owned medical office portfolio. We intend to mine
these opportunities.
We have also made a major commitment to our Development Services Group in
Nashville, Tennessee. Steve Buckeridge and his team give us cutting edge development and
design capabilities that should lead to additional business opportunities for the Company.
We recently purchased a 23,000 square foot building in Brentwood, Tennessee. This
building will exemplify our commitment to state-of-the-art development and design.

Conclusion
During our last several letters, I have identified the sweeping changes that are
occurring in health care and senior housing. We believe that our full-service
platform will allow us to make significant contributions to the design and development
of the new real estate needed to implement these changes. We believe we
have the intellectual capital to take advantage of this change and to be a leader in
this space.
On January 22, 2008, Jeff rey R. Otten joined our Board of Directors. Jeff
is an experienced executive who has operated health systems, including Brigham
and Womens Hospital in Boston, and medical technology and biotech companies.
Jeff s appointment to our Board further demonstrates our commitment to
acute care leadership.

I close by thanking you for your support as we enter our 38th year, with
an ongoing commitment to improving health care and senior housing through
solid and profitable investments.
George L. Chapman
Chairman and Chief Executive Officer
March 12, 2008




