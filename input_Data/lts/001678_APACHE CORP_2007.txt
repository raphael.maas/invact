finding a way
embodies Apache Corporations 53-year quest to build a significant and profitable oil
and gas company, providing energy to meet constantly increasing global demand for
the long-term benefit of our shareholders.
From scraping together $250,000 to launch the company in 1954, to acquiring 1,050 square miles of
three-dimensional seismic data at the environmentally sensitive tip of South America during 2007,
Apaches work ethic and its values have been the guiding principles behind finding a way.
In 2007, your company:
 Increased production 12 percent to
561,000 barrels of oil equivalent (boe)
per day, the 28th annual increase in the
last 29 years;
 Grew proved reserves by 6 percent to
2.4 billion boe, the 22nd consecutive year
of reserve growth;
 Earned a record $2.8 billion, or $8.39
per diluted common share. Excluding
the non-cash impact of foreign currency
fluctuations and tax rate changes
on deferred tax balances, Apaches
2007 adjusted earnings* were $8.65 per
share, or 25 percent above 2006 adjusted
earnings; and
 Generated a record $6.2 billion of cash
from operations before changes in
operating assets and liabilities.*
*Non-GAAP
These are the years key statistics. The rest of the story lies in
the underlying asset value and future resource potential that
the company has built. With 42 million gross acres in seven core
operating regions, Apache has a deep inventory of opportunities
across our portfolio that will enable the company to continue to
grow profitably into the next decade.
Over the course of a half century, a company is confronted with countless
challenges and constant change. Most of all, finding a way means not
giving in to conventional thinking while having the determination
to overcome any challenge to continue our record of profitable
growth. The difference between success and failure is measured in
inches, not miles, and to continue on our path of progress, we must
constantly find new opportunities to fuel our growth  most recently,
through exploration.
In 2007, our exploration and development program provided significant
momentum for future growth. Apaches ACE core growth areas of
Australia, Canada and Egypt will further that momentum.
In Australia, we discovered natural gas with the Julimar-1 well on
the WA-356-P block in the Carnarvon Basin. With two additional
discoveries, Apache booked our share of 650 billion cubic feet
(Bcf) of estimated proved reserves. Five additional appraisal wells
are planned for 2008, and we believe the ultimate size of this gas
accumulation could be in the range of 2 to 4 trillion cubic feet
(Tcf). Apache is developing our Reindeer discovery at a time of strong
demand for Carnarvon Basin natural gas; industry prices recently
have risen to a multiple of the companys average price of $1.89 per
thousand cubic feet (Mcf) in 2007. We hope to sanction the Julimar
gas development in late 2008.
In Canada, Apache had encouraging results in the Ootla area of
British Columbia, where we are continuing to test the commercial
and geological potential of the Muskwa Shale. With our partner,
EnCana, we have amassed 417,000 gross acres  the largest single
acreage position in this sizable emerging play.
In Egypt, we made several deep Jurassic oil and gas discoveries,
including the Jade, Alexandrite and Imhotep discoveries in the
Matruh Concession on the northern side of the Khalda area.
Behind each of these discoveries are teams of experienced, creative
geologists, geophysicists and engineers equipped with Apaches
sense of urgency, the best technological tools, and a determination to
find a way to overcome obstacles.
For example, when the wheels of 3-D seismic survey equipment
became bogged down in the muddy terrain of Argentinas Tierra del
Fuego, Apaches exploration group brought in tracked vehicles used
in the Canadian Arctic Region to accelerate the completion of the
seismic survey in this environmentally sensitive area. This survey
already is yielding results in added production.
Apache also tested a new method of acquiring 3-D seismic
information that speeds the job while increasing the quality of the
data. Using 12 large, independently controlled Vibroseis seismic
source trucks in Egypts Western Desert, we set a new record for
acquiring data with more than 6,000 shots in just over eight hours.
One essential element of Apaches progress over the last 53 years is
our commitment to long-term growth over short-term gratification.
It seems that every week, another large institution is crippled
by the impact of short-term thinking that disregards long-term
consequences. The sub-prime credit debacle that has dragged
the U.S. economy into recession is one such example. Essentially,
mortgage brokers and bankers were rewarded for taking excessive
risks by originating loans with little regard for the borrowers ability
to repay.
Apache employees thrive in a culture that values
and rewards them for growth that is both profitable
and sustainable.
As this report was being prepared, our employees
achieved a goal that, when established in 2005,
appeared to be an impossible dream: doubling the
companys share price by the end of 2008.
Since kicking off the 2005 Share Appreciation Plan,
our long-term shareholders have seen the value
of the company more than double, increasing
market capitalization by more than $19 billion. For
this achievement, 2,800 Apache employees will
receive approximately 2 million shares with total
value equal to about 1 percent of the market value
growth. More than 90 percent of the incentives
will be paid to non-executives, with employees
receiving shares equivalent to at least half of their
annual salaries. The shares will be paid in four
annual installments, providing a strong retention
incentive at a time when the competition for good
people is considerable.
A New Era For Energy
While Apache focuses on its long-term goals, we are
faced with unprecedented short-term volatility in
global crude oil markets. The phenomenal rise of
crude oil prices  from $61 per barrel in January
2007 to $110 per barrel in March 2008  is the
product of many complex factors and signals the
emergence of a new competitive era for energy
across the globe.
Population growth and emerging economies such
as China and India have pushed oil demand from
65 million barrels per day to 85 million barrels
per day in 20 years. At current prices, the nations
of the world are spending more than $3 trillion
annually to secure crude oil. The fastest-growing
demand is from less-developed countries to fuel
progress and advance their standards of living.
For U.S. consumers, the recent rise in energy prices
has been startling, because the nation had become
accustomed to inexpensive energy after the price
spikes in the 1970s and 1980s. At the end of the
1990s, energy took a smaller bite out of disposable
income than it did at the end of World War II.
While increased demand, turmoil in the oilrich
Middle East and the weak U.S. dollar play
large roles in rising oil prices, many analysts
have concluded that the tremendous increase in
speculation in oil futures markets by hedge funds,
pension funds and other investors has become one
of the significant factors in setting oil prices.
Speculative trading brings liquidity to commodity
markets, allowing companies to hedge their
exposure to commodity prices. Apache has used
this strategy to lock in the economic returns of
acquisitions and development projects.
But large-scale speculation can hurt the economy
by distorting market signals regarding supply
and demand in the physical market and lead to
excessive price volatility. We believe todays trading
volume of oil futures contracts  15 times that of
physical demand  is excessive and has had an
undue influence on todays lofty prices.
Raymond Plank
Chairman and Founder
G. Steven Farris
President, Chief Executive Officer and
Chief Operating Officer
Our Role in the New Era
Apaches primary responsibility is to find and develop oil and natural gas
that will provide a competitive return for our shareholders, fuel economic
growth, and help raise living standards. We applaud the recent trends
toward conservation and development of viable alternatives to fossil
fuels. However, these alternatives will not ebb the worlds demand
for fossil fuels for many years.
In 2007, in addition to our effective exploration program, we pushed
ahead on six large development projects that are expected to add
over 100,000 barrels of oil equivalent per day of new production
in 2009, 2010 and 2011. These projects include the Van Gogh and
Pyrenees oil developments and Reindeer and Julimar gas projects
offshore Western Australia, as well as expansion of the Salam gas
plant and secondary oil recovery projects in Egypt.
Apache has traversed enough price cycles over our 53 years to recognize
that it would be imprudent to base our investment decisions on $100
per barrel oil. We use conservative price forecasts, and, when
appropriate, have employed hedges to protect the economic viability
of acquisitions and development projects with long lead times.
In addition to building a profitable corporation that provides
essential energy supplies, we have a responsibility to be stewards
of the environment in which we operate and to do our part
to improve living standards. Our operations in Egypt  where
we are the third-largest producer and an important source of
foreign investment  illustrate the many ways we try to meet
our responsibilities.
We provide direct employment for 2,000 Egyptians, $9 million in
revenue for the government each day, and energy supplies for the
growing economy. In the Western Desert, we are converting diesel
generators to cleaner-burning, less-expensive natural gas. We also
tapped the spirit of our workforce, along with the generosity of our
officers, employees, directors, industry colleagues and friends, to
build 200 one-room schools for girls from rural villages who had
not been able to attend school.
Elsewhere in this annual report, we describe other aspects of
Apaches stewardship commitment, including some exciting
new initiatives.
At Apache, finding a way also means avoiding the beaten path. Our
initial investment in Egypt, at a time when other operators were
leaving, set the stage for the emergence of one of the companys
core growth areas. We see similar opportunities for growth in
Australia, Canada and Argentina.
Apaches 2007 was an outstanding year by most measures for our
shareholders and our skilled and dedicated team. Although we
anticipate turbulent times ahead as the U.S. economy struggles
to right itself, we remain dedicated to adding value for our
shareholders.