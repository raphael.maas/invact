TO OUR SHAREHOLDERS:

ALTHOUGH WE ACHIEVED SIGNIFICANT SALES GROWTH IN THE YEAR 2000, IT WAS
A DISAPPOINTING YEAR IN TERMS OF INITIAL SALES EXPECTATIONS AND ACTUAL
EARNINGS. DEMAND FOR OUR PRODUCTS WAS REDUCED BECAUSE OF THE NEGATIVE
WEALTH EFFECT OF THE DECLINING STOCK MARKET, LOWER CONSUMER CONFIDENCE AND
HIGHER ENERGYCOSTS THATNEGATIVELYIMPACTED CONSUMER DISPOSABLE INCOME.
IN ADDITION, OUR EARNINGS WERE IMPACTED BY GREATER-THAN-EXPECTED COSTS FOR A
NEW PRODUCT LINE, HIGHER INTEREST RATES, HIGHER ENERGY COSTS AND THE DECLINE IN
THE VALUE OF EUROPEAN CURRENCIES. ALLOF THESE FACTORS CONTRIBUTED TO OUR
LOWER-THAN-EXPECTED SALES AND EARNINGS RESULTS.

FINANCIAL HIGHLIGHTS
Despite the negative factors that affected our performance,
Masco posted gains in sales, net
income and earnings per share for the full year
2000. Financial highlights include:
 Net sales were a record $7.2 billion, a 15 percent
increase over the $6.3 billion we achieved in
1999;
 Net income increased four percent to a record
$592 million from $570 million in 1999;
 Earnings per share increased two percent to
$1.31 in 2000 from $1.28 in 1999;
 After-tax return on shareholders equity approximated
19 percent for 2000, just below our 20
percent goal;
 The quarterly cash dividend was increased to
$.13 per common share, marking the 42nd consecutive
year in which dividends have been
increased; and
 Capital expenditures for the year were $388 million
compared with $351 million in 1999, reflecting
major expansion programs.
In April 2000, the Companys Board of Directors
authorized the repurchase of up to 40 million
shares of Masco Common Stock.

Approximately 12.6 million common shares were
repurchased in 2000, including shares purchased
to offset 8.4 million shares of Masco Common
Stock that we issued in July 2000 as a result of the
adoption of an Executive Stock Purchase Program.
This voluntary Program was made available worldwide
to Mascos senior divisional and corporate
management members who are key to leading
Masco Corporation to long-term success.
Approximately 300 employees participated in the
Program and invested approximately $156 million
in shares of Masco Common Stock. The Program
represents a major financial commitment on the
part of its participants, thus further aligning
Mascos key management with the interests of its
shareholders.
OPERATING RESULTS
Specific factors that contributed to our lower-thananticipated
sales and earnings included:
 Lower demand for home improvement products,
in both North America and Europe, and corresponding
inventory reduction programs undertaken
by our retail and wholesale customers;
 Greater-than-expected start-up costs associated
with the introduction of a major line of cabinetry at
one of our cabinet divisions;
 Higher interest rates, which resulted in increased
interest expense on the Companys bank debt;
 Higher energy costs, which negatively affected
virtually every area of our business, particularly
freight and raw materials;
 Weaker European currencies that resulted in a
reduction in European sales and net income
when translated to U.S. dollars; and
 Lower margins in several of our business segments,
which were also adversely affected by
the pre-tax charges discussed later.
The combination of the above factors resulted
in year 2000 earnings being approximately
$385 million pre-tax, or $.50 per share, below what
the Company expected at the beginning of the
year.
BUSINESS SEGMENTS
The Company recently redefined its business
segments for financial reporting purposes and
began reporting its results accordingly. Following is
a brief overview of each business segment.
CABINETS AND RELATED PRODUCTS
Included in this segment are assembled and
ready-to-assemble kitchen and bath cabinets;
home office workstations; entertainment centers;
storage products; bookcases; and kitchen utility
products.

Net sales of cabinets and related products
increased approximately 15 percent to $2,551 million
in 2000 from $2,220 million in 1999.
PLUMBING PRODUCTS
This segment includes faucets; plumbing fittings
and valves; bathtubs and shower enclosures; and
whirlpools and spas.
Net sales of plumbing products increased two percent
to $1,839 million in 2000 from $1,803 million
in 1999.
DECORATIVE ARCHITECTURAL PRODUCTS
This segment includes paints and stains; mechanical
and electronic lock sets; and door, window and
other hardware.
Net sales of decorative architectural products
increased 20 percent to $1,395 million in 2000
from $1,165 million in 1999.
INSULATION INSTALLATION AND OTHER SERVICES
Included in this segment are the sale and installation
of insulation and other products and the restoration
of facilities damaged principally by natural disasters.
Net sales of insulation installation and other services
increased 61 percent to $855 million in 2000
from $532 million in 1999.
OTHER SPECIALTY PRODUCTS
This segment includes staple gun tackers; staples
and other fastening tools; hydronic radiators and
heat convectors; venting and ventilation systems;
modular office workstations; and grilles, registers
and diffusers for heating and cooling systems.
Net sales of other specialty products increased
three percent to $603 million in 2000 from $587
million in 1999.
GEOGRAPHIC RESULTS
More than 80 percent of the Companys sales are
generated by operations in North America, primarily
in the United States. A brief overview of the
Companys sales by geographic region follows:
 Net sales in North America increased 14 percent
to $5,947 million in 2000 from $5,238 million in
1999.
 Net sales of international operations, principally
in Europe, increased 21 percent to $1,296 million
in 2000 from $1,069 million in 1999. Sales
growth in Europe continued to be acquisition
driven, offsetting both unfavorable currency
exchange rates and unfavorable market conditions,
particularly in Germany where new
construction and renovation activities declined
significantly from 1999 levels.

FOCUS ON GROWTH
We continue to focus on our strategies to achieve
consistent, predictable growth and high cash flow.
Through a combination of cost containment and
aggressive marketing and merchandising initiatives,
our annual goal is to achieve the following objectives:
 711 percent average annual internal sales growth;
 510 percent or greater average annual sales
growth through acquisition;
 15 percent or greater average annual growth in
earnings per share;
 1518 percent annual operating margins;
 Annual after-tax return on equity of 20 percent;
 After-tax return on sales above 10 percent; and
 High cash flow.
KEY RETAILER PROGRAM
Initiated in 1986 to capitalize on the exceptional
growth of then-emerging home centers and other
large retailers, the Key Retailer Program has been
a significant driver of incremental growth and market
share gains. In addition, the Program has provided
a competitive advantage for Masco by developing
coordinated programs for key customers
that leverage the collective strength of our individual
operating companies.
Since this Programs inception, aided by strategic
acquisitions and mergers of companies that serve
these large retailers, our key retailer sales have
increased at an average annual rate of 26 percent.
We greatly value our partnerships with large national
and international retailers who participate in this
Program. Examples of key retail customers include
ACE Hardware, The Home Depot, The Kingfisher
Group, Lowes, Sears, TruServ and Wal-Mart.
In 2000, partnerships with our key retail customers
resulted in sales of $2.73 billion. The largest portion
of our key retailer sales are to the three fastest-growing
retailers and our three largest retail customers
The Home Depot, Lowes and Wal-Mart, with 2000
sales approximating $2.2 billion.

INTERNAL GROWTH
Our average annual internal sales growth of nine
percent for the past 10 years has been achieved
through brand leadership, coordinated marketing
initiatives, new product offerings and a broad range
of home improvement and building products that far
exceeds that of any single competitor.
We believe that approximately 90 percent of our
products are brand leaders in their categories, and
Masco products are now sold through home
improvement retailers and builders, contractors
and distributors around the world.
GROWTH THROUGH ACQUISITION
A key component of Mascos growth strategy has
been growth through acquisition of leading, highmargin
home improvement and building products
companies. Our acquisition strategy is to acquire
companies with annual sales in the range of
approximately $100 million to $500 million or more
and operating profit margins of at least 15 percent.
We seek entrepreneurially-oriented companies
that have established positions of product, brand
and customer service leadership in the markets we
servehomebuilders, wholesale distributors,
home centers, mass merchants and other retail
home improvement outlets.
Mascos own entrepreneurial philosophy, along
with our professional management strengths and
capabilities in marketing, manufacturing, engineering
and systems, creates an environment for successful
synergy and continued growth and development
of our acquired companies.
Since 1982, we have invested more than $7.0 billion
by acquiring more than 53 home improvement
and building products companies.
Aided by acquisitions, our sales of home improvement
(repair and remodeling) products have grown
at an average annual rate of approximately 16 percent
during the past 10 years, while the industry
has grown at an average annual rate of five percent.
In building products (new construction),

including acquisitions, our sales have grown at a
10-year average annual rate of approximately 14
percent, compared with industry average annual
growth of seven percent.
During 2000 and early 2001, Masco acquired the
following companies with combined annual sales
of approximately $1.3 billion:
 BSI Holdings, a leading provider of installed
insulation and other products within the United
States and Canada, acquired in January 2001;
 Davenport Insulation Group, one of the
Northeasts leading providers of building products
and insulation installation services;
 Ginger, a designer and distributor of high-end
decorative bath accessories, lighting and related
products;
 Glass Idromassaggio SpA, a manufacturer of
acrylic bathtubs, shower trays and whirlpools
and steam shower enclosures, based in Italy;
 Masterchem Industries, a national leader in the
manufacture of specialty paint products; and
 Tvilum-Scanbirk A/S, a leading European
manufacturer of ready-to-assemble products,
including cabinetry, shelving, storage units and
workstations.
FOCUS ON THE FUTURE
In the second half of 2000, our businesses were
negatively impacted by the factors mentioned previously,
including the decline in the stock market,
the economic slowdown and lower consumer confidence.
We expect the impact of these factors to
continue for most of 2001, resulting in a year of
below-average internal growth for Masco.
Due to our less-than-satisfactory performance for
the year and changing business conditions, we are
reviewing all aspects of our businesses to improve
our financial and operating performance. Our
focus is on creating increased value for our shareholders
through a number of cost reduction, sales
growth, profit and cash flow improvement and
other initiatives.
In the Fall of 2000, corporate teams were assigned
to focus on specific areas of our businesses that will
improve our long-term performance, including:
acquisitions, business consolidation, business portfolios,
cash generation, compensation, corporate
departmental functions and expense,
purchasing, customer initiatives, e-commerce,
financial strategies, management development and
succession planning and new product research and
development.
The initial decisions of certain of these teams, as
well as other factors, resulted in an approximate
$145 million non-cash pre-tax charge that the

Company recorded in the fourth quarter of 2000.
These charges include the writedown of certain
investments ($55 million pre-tax) and a charge
regarding the planned disposition of certain operating
divisions that the Company believes are not
core to its long-term growth strategies ($90 million
pre-tax). These divisions have annual sales of
approximately $600 million and the Company
expects cash proceeds upon disposition to exceed
$400 million.
Despite the weakened economy and correspondingly
lower sales and profitability that we experienced
during the fourth quarter of 2000 and into
the first quarter of 2001, we remain optimistic
about our future prospects.
The Company is taking a number of steps to
improve its performance. We believe that these
steps, when combined with our aggressive costreduction
programs and proven strategies aimed
at expanding our market share during economic
downturns, should result in better performance for
the Company late in 2001, and particularly in 2002.
Our intent is to maintain and enhance our product
and market leadership positions through market
share gains, product innovation and manufacturing
excellence. Through the successful implementation
of our proven strategies over the years, which
is the basis for our internal growth objectives outlined
in our five-year sales forecast, our goal is to
generate sales of approximately $11.5 billion by
the year 2005.
We greatly appreciate the commitment of all of our
48,600 employees. Their capabilities, initiative and
enthusiasm, particularly during these challenging
times, provide the foundation upon which we will
build our future successes. We look forward to
their continued efforts as we all work together
to strive to meet our financial and operational goals
and achieve above-average returns for
our shareholders.
Richard A. Manoogian
Chairman and Chief Executive Officer
Raymond F. Kennedy
President and Chief Operating Officer