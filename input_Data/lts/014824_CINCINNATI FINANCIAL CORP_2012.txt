To Our Shareholders, Friends and Associates:

Highlights
Your companys 2012 net operating income* of
$393 million more than tripled last years result. We
ended 2012 with shareholders equity up 8 percent,
reflecting increased policyholder surplus of our
property casualty insurance subsidiaries. Cash and
marketable securities we hold at the parent-company
level add to our stellar financial strength and liquidity.
Improvements made in 2012 and recent years have
gained momentum. Their cumulative impact feeds
our confidence that our current strategies are the
right ones to take us forward. While high catastrophe
losses early in the year masked improving trends in
our property casualty insurance underwriting, those
trends grew strong enough in the second half of
2012 to transform a $68 million first-half
underwriting loss into a $137 million underwriting
profit for the year.
Our full-year combined ratio was 96.1 percent. On a
statutory basis, it was 95.4 percent, nearly 11 points
points better than the estimated property casualty
industry aggregate of 106.2 percent, per A.M. Best Co.
We outperformed by making core underwriting
progress, as indicated by 8.4 percentage points of
improvement in our loss and loss expense ratio for the
current accident year before catastrophe losses. Net
written premiums rose 12 percent, boosted by new
business premiums that for the first time reached the
half-billion dollar mark.
One Team, One Vision
The growth and profitability initiatives that produced
those results were designed to strengthen our
competitive position and opportunities over the long
term. We have more work to do to build on our

progress  and more upside to realize from our
ongoing efforts. We believe our combined ratio over
any five-year period can be consistently within the
range of 95 percent to 100 percent, and we can grow
faster than the industry average. By the end of 2015,
we estimate full-year direct written premiums from
our property casualty and life operations can reach
$5 billion.
Performing at these levels generates strong cash flow
to expand our investment portfolio and increase
investment income, further supporting our primary
performance target of an annual value creation ratio
averaging 10 percent to 13 percent for the period of
2013 through 2017. Our 2012 value creation ratio was
12.6 percent, resulting in a solid 12.4 percent average
annual ratio for the four years beginning with 2009,
when we adopted this performance metric, which
considers increases in the book value of your company
and your shareholder dividends.
To achieve our performance target, we have
identified goals for every operational area and every
individual. With clarity around our shared goals,
your companys management team and associates at
every level are energized, focused and accountable.
We have improved our data collection, analysis
and communication to closely track progress and
bring everyone together in support of our
priority initiatives.
We are one team, with one vision: to create value
by being the best insurance company for
independent agents.
A Strong Competitor
We manage our business and our enterprise risks to
position your company to perform through all
economic and market cycles, supporting market
stability for our agency customers. Challenges in the
macro environment do create headwinds, and many
of our improvements arise from anticipating and
preparing for them. Here are four industry issues that
have our attention and what we are doing about them.
Interest rates:
Over the past three years, interest rates generally have
declined and credit spreads have tightened, depressing
investment income from fixed-income securities.
We relied in 2012 on dividend income increases from
our common stocks to take our investment income up
1 percent, a good result relative to investment income

declines for our industry. Our equity investment
strategy gives us advantages; it powered $210 million
of the $391 million increase in pretax unrealized
investment gains and $37 million of net realized
gains in 2012.
Low interest rates also led to increased unrealized
gains from our fixed-maturity securities. The
2 percent to 3 percent contribution to our value
creation ratio that resulted from those gains in the
past three years is not expected to occur in the
next five-year period. Strong underwriting results
can help offset the effect of flat or falling bond
valuations on our book value by giving us more
cash to invest. During 2012, we invested close to
half of net operating cash flow, after paying
shareholder dividends, in equity securities.

This was our first year since 2007 of positive net
investment into common stocks.
Weather:
Many worry that severe convective storms and
hurricanes are becoming the new normal. What we
know is that while our 2012 catastrophe losses
did not rise to the prior years record level, the
10 percentage points they added to the combined
ratio exceeded our five-year average of 8.2 points and
our 10-year average of 6.1 points. Our policyholders
in the Midwest and Southeast felt the sting of a
midsummer derecho, and policyholders in areas
bordering Lake Erie were caught in Superstorm
Sandys path.
Our property loss mitigation initiatives will help
counter worsening storm losses. A multidepartmental,
multi-disciplinary task force has
reviewed our property business and made changes
ranging from increasing the specialized expertise of
our claims and loss control staff to revising
underwriting guidelines and increasing property
inspections. For commercial business, we are seeking
approval for wind and hail deductibles in areas prone
to convective storm losses, developing a named storm
deductible for additional states and using new
guidelines for underwriting hail risk.
For personal lines business, we are using more robust
deductibles and covering older roofs at actual cash
value rather than replacement cost. Homeowner rate
increases in recent years also should improve our
property results as the higher premiums are earned.
In recent years, we nonrenewed policies in
southeastern coastal areas that are most exposed to
hurricanes, and we continued our gradual expansion
into states less prone to catastrophes.
We continue to enhance our property catastrophe
reinsurance program. Beginning in 2013, your
companys capital is further protected by a
catastrophe bond that provides collateralized
reinsurance covering severe convective storm losses in
certain regions as well as supplemental coverage in the
event of a New Madrid earthquake.
The marketplace:
With many worthy competitors in our industry and
industry surplus on the rise, the current price firming
in commercial lines could be difficult to sustain. This
was only the second year of rate firming, yet some

carriers reported competitive pressure that
reduced the average size of price increases in the
fourth quarter.
We are working diligently to achieve price adequacy,
while also teaming with our agents to emphasize the
Cincinnati value proposition and provide our best
quote. On renewals and new business, we are using
modeling tools to help our underwriting associates
better target profitability and discuss pricing impacts
with agency personnel. In 2012, our standard
commercial lines policies averaged an estimated price
increase in a mid-single-digit range. The higher
pricing, along with improving economic conditions,
led to an 8 percent increase in our commercial
renewal written premiums. Commercial lines new
business premiums rose 15 percent, with newer
agencies appointed in 2011 and 2012 contributing
more than half of the increase.
Rate increases for our personal lines business in 2012
averaged in the high single digits for homeowner
policies and low single digits for auto policies.
Personal lines renewal premiums rose 11 percent and
new business premiums rose 17 percent, reflecting
recent new agency appointments as well as the rate
increases. For excess and surplus lines, average renewal
pricing rose in the high-single-digit range, and new
business premiums rose 9 percent, a lesser increase
than for standard-market policies as we declined
opportunities to write underpriced policies.
Agency ownership:
Our sampling of the issues we are paying attention to
would not be complete without a comment on the
changing independent agency landscape. The
American Agency System is dynamic and growing,
with the number of independent agencies increasing
to 38,500, according to the 2012 Agency Universe
Study. In 2012, we made 140 new agency
appointments, ranging from start-up agencies with no
revenues to agencies with over $100 million of
premium written annually with all carriers they
represent. That broad range is indicative of our full
agency plant.
The challenge we embrace is to be the best carrier for
each of them, even as they continue to evolve. The
pace of mergers and acquisitions affecting our
agencies has quickened over the past three years, with
almost 60 transactions taking place in 2012. While
many agencies remain independent and locally

owned, others are independent, locally managed
agencies owned by groups, banks or brokers.
For the latter, we maintain relationships at both the
local and organizational level. We are committed to
providing appropriate products and services that
make us a significant contributor to the success of
independent agencies of all sizes and ownership types.
At the same time, we are willing to provide capital

assistance to support private ownership, including
new producer training, technology upgrades and
perpetuation planning.
Good Returns
Each director who joins our board is an asset that we
hope will benefit the company for many years,
accumulating and applying knowledge of our
company and our industry. In November 2012, we
were fortunate to have Dirk J. Debbink return to his
directorship after four years of active duty with the
U.S. Navy. Dirk served as Reserve deputy commander
of the U.S. Pacific Fleet; Commander, Navy Reserve
Force; and as a senior member on the staff of the chief
of naval operations in the Pentagon. We are proud to
renew his relationship with you, our shareholders.
Another standout is the cash dividend record of
your company. For 52 straight years, that dividend
has increased, and now there are only nine U.S.
publicly traded companies with a longer streak. The
payout ratio of dividends to net income improved to
63 percent in 2012, and we believe we can further
improve the ratio in 2013 by continuing to meet our
insurance growth and profitability goals. Through all
economic and market cycles, we intend to be
predictable and trustworthy, returning value to you.
Respectfully,
Kenneth W. Stecher
Chairman of the Board
Steven J. Johnston, FCAS, MAAA, CFA, CERA
President and Chief Executive Officer