
Dear Fellow Shareholders:
Fortune Brands' unique breadth and balance helped us deliver another year of record results in 2006. Consumer
demand across our broad portfolio of powerful brands -- including Moen, Titleist, Jim Beam, Aristokraft, Omega, Master Lock and
Sauza -- drove solid sales growth. The balance provided by our strength in three attractive consumer categories underpinned our
performance, as we achieved record results in each of our business segments. Notably, Fortune Brands' enhanced position in spirits
and wine created new growth and helped offset challenges in the U.S. home products market. Even with those challenges, our home
products brands outperformed the market and continued to gain share. Our industry-leading strength in golf added to our results.

 Total sales were $8.8 billion, up 24%. Strong organic sales growth plus acquisitions drove
  our gains.
 Operating income was 29% higher at $1.5 billion.
 Earnings per share reached $5.42, up 40% even with the first-time impact of the required
  expensing of stock options.
 We increased the dividend another 8% to an annual rate of $1.56 per share.
 Fortune Brands stock rose for the 6th year in a row. Including dividends, an investment in
  Fortune Brands appreciated 12% in 2006.

We see our breadth and balance as a powerful advantage that has been critical to our ability to consistently deliver strong results.
Through the end of 2006, we achieved 22 consecutive quarters of double-digit growth in earnings per share before charges/gains.
During the past five years, our stock price has more than doubled. And as we enter 2007, we feel our breadth and balance position
us well to confront the challenges presented by the downturn in the U.S. housing market.

We have a balance of power -- the benefit of three great consumer categories and a breadth of strong brands in each -- that we
believe has served shareholders well.

This was especially evident in 2006.




Balance of Power Delivers Results
For the first time, our newly expanded Beam Global Spirits & Wine business and our powerful Home & Hardware business delivered
roughly equal operating profits. Benefiting from both the acquisition of more than 25 premium brands in mid-2005 and growth across
our portfolio, revenues in our Spirits & Wine segment were up 68%.

In the first full year following the acquisition, we began realizing the benefits we were looking for -- greater presence and faster growth
at the premium end of the market, greater scale in key international markets to support further growth for core brands, and a lower cost
structure. We drove double-digit volume growth for major brands including Maker's Mark bourbon, Knob Creek small-batch bourbon,
Laphroaig Islay single-malt Scotch whisky and Fundador brandy. Several key brands grew in the mid-to-high-single-digit range,
including Sauza tequila, Teacher's Scotch, Whisky DYC, Clos du Bois wine, and our flagship Jim Beam bourbon, which grew 5%.
Courvoisier cognac, DeKuyper cordials and Harveys sherries all grew solidily. Among our largest brands, only Canadian Club saw
lower volumes. Consistent with our focus on marketing and brand-building, we've begun stepping up investment behind several key
brands, including extension of our Jim Beam NASCAR sponsorship and the Canadian Club Racing program we introduced at the
Indianapolis 500 in 2006.

Against the headwinds of lower housing activity in the U.S., our Home & Hardware brands still grew sales 13% (aided in part by an acqui-
sition). Our strong and growing position in the replace/remodel segment, combined with share gains with large builders, helped deliver
another year of record results. Moen became our first individual brand to surpass $1 billion in annual sales. Our cabinetry brands gained
share with kitchen and bath dealers, at home centers and with large builders. While Therma-Tru continued to increase its percentage of
sales related to replace/remodel projects, the significant decline in U.S. home construction dragged Therma-Tru's sales lower. We contin-
ued to drive strong growth for the iconic Master Lock brand, which more than offset lower sales of Waterloo tool storage products.

Sales also benefited from the acquisition of SBR, Inc., led by the Simonton windows brand, the #3 vinyl-framed windows brand in
North America. Simonton is an innovative brand with a leading position in the replacement windows market. Vinyl-framed windows
offer consumers excellent energy efficiency and durability, and are benefiting from materials conversion at the expense of wood
and aluminum-framed windows.


The breadth and quality of our expanded spirits and wine portfolio were evident at the world's most prestigious spirits
competitions. Nine of our brands captured a total of 31 medals at the 2006 International Wine & Spirit Competition,
and we earned another 29 medals at the San Francisco World Spirits Competition, where Laphroaig won the coveted
"Distiller of the Year" honors.


Our Golf brands drove sales 4% higher and set a new industry record, eclipsing $1.3 billion in revenues for the first time ever. Led by
the flagship Titleist Pro V1, higher golf ball sales fueled the majority of our growth. In addition to Titleist's #1 position in golf balls,
FootJoy continued to grow as the #1 shoe and #1 glove in golf. Our Titleist and Cobra golf club brands developed excellent new
products and maintained their combined #2 position in the U.S. golf club market.

We see our success in the marketplace as the result of two major factors: the advantageous positions we've staked out in our catego-
ries, and the initiatives we pursue to drive sustained, profitable growth for our brands.

We're able to leverage the power of our brands by focusing on the most attractive segments of our consumer categories. In spirits and
wine, we've increased our focus on premium and super-premium products, the category's fastest growing segment. We believe we're
in the best parts of the home products business: The kitchen and bath, as well as windows and doors, are segments that produce sub-
stantial return on investment for the homeowner and that rely significantly on replace/remodel projects, which are less economically
sensitive than new home construction. And in golf, we're growing the game's most powerful brands with industry-leading innovation and
customer service. We further enhance return on investment across Fortune Brands by continuously seeking to improve our cost structures.

To drive top-line growth in 2006, we used a balanced strategy that continued building our powerful consumer brands:

1. Building Powerful Brands with Innovation
We continued to invest in the growth of our leading brands.

By investing in consumer insights to better understand our consumers, and applying that knowledge to research and development,
we're able to inspire consumers with innovative new products. In 2006 we were awarded more than 500 patents, nearly double the
year before, reflecting the ingenuity of our people and our sustained investments in new product development. The power of innova-
tion is evident in the fact that approximately 20% of our sales came from products introduced in just the past three years.

Moen, the inventor of the single-handle faucet, delivered significant advances in both form and function. Elegant new styles and finishes
elevated Moen's fashion image among consumers. And with breakthrough innovations such as the new Velocity showerhead featuring
our exclusive Immersion Rainshower technology, Moen created a way to channel water pressure into a total-body, rain-soaking
                                                                                                               
experience. Innovations extended across our Home & Hardware brands, including Moen's ChoiceFlo filtering faucet; new finishes,
styles and organization solutions in cabinetry; and the new GloSphero combination lock from Master Lock.

Our leadership in spirits flavor innovation paid off again in 2006. We accelerated our product development to bring new flavors to
market faster, helping us to capitalize on trends and set new ones. Consumers continued to support growth of the cocktail culture, and
new flavors -- such as Berry Fusion and Strawberry Passion in our U.S. DeKuyper Pucker line and Cherry in our Sourz line -- created
enticing new options for mixed drinks. We reinvigorated the Harveys sherry line with the new Fino, Reserve and Orange versions, we
extended our successful Starbucks liqueur franchise with the national U.S. launch of super-premium Starbucks Cream Liqueur, and we
added a highly successful Pinot Grigio to our super-premium Clos du Bois wines.

Our golf brands are our most technology intensive, and we believe we have the finest R&D teams in the industry. Go into any golf shop
and you'll see the results. In 2006, we introduced new high-performance offerings in clubs, including the new flagship Titleist 755 irons,
the Titleist 585-H hybrid, the Scotty Cameron Detour putter and the new Titleist 905R driver, which became the #1 460 cc driver on the
PGA Tour. Cobra introduced its most forgiving clubs ever, including the King Cobra HS9 driver, three new multi-material iron models and
the new Baffler DWS utility metals. And FootJoy took golf shoes to the next level with ReelFit, featuring the exclusive Boa lacing system.

2. Capitalizing on International Balance
We're pursuing new international growth opportunities and creating greater global balance.

Global markets outside the United States drove our fastest sales growth in 2006. While total volumes for our spirits and wine brands
grew solidly in the U.S., they grew twice as fast in markets outside the U.S. as we benefited from our enhanced portfolio, stronger
organization, improved distribution in key markets, and entrance into new markets. Our acquisition of the former Allied Domecq brands
and select distribution operations has significantly increased our presence in markets such as the U.K., Spain, Germany, Canada,
Mexico, Brazil and India. At the same time, we're capitalizing on opportunities in high-potential markets by introducing select brands
in Russia, China and India, among others. More than 45% of our spirits and wine case volume came from international markets, up
from 25% prior to our acquisition.

For our golf brands, we've established company-owned distribution to fuel profitable growth in markets such as China, Korea and
Australia. More than 75% of our revenue growth in golf came from international markets. 





In Home products, nearly 40% of sales growth for Moen came from continued expansion in international markets including China, Latin
America and Canada. Master Lock added to strong U.S. sales growth with even stronger growth in Europe, Canada and Latin America.

3. Broadening Markets for Our Brands, Expanding with Key Customers
We're broadening our strength by extending brands into adjacent product segments to create further growth. For example, Therma-
Tru added to its fiberglass entry door offerings with a successful new line of patio doors; Moen continued to enhance its growth
with the ShowHouse luxury brand extension as well as its fast-growing bath accessories line; Master Lock is establishing itself as a
leader in automotive towing accessories; and FootJoy continued to grow as the new leader in performance golf outerwear.

We're also reaching out to the fast-growing Hispanic consumer market in the U.S. with a variety of focused initiatives, including
exclusive merchandising programs with kitchen and bath dealers in Spanish-speaking communities and the www.misgabinetes.com
website for our Aristokraft and Diamond cabinetry brands.

We further add to growth by working hard to serve our customers and by expanding our relationships with them. For example, our
home products sales related to new construction outperformed the industry as our commitment to customer service earned our
faucet, cabinetry and entry door brands more business with large builders. In the replace/remodel segment, we expanded training
for kitchen designers at The Home Depot, where we're the exclusive manufacturer of Thomasville Cabinetry. And just two years after
becoming the exclusive supplier of fiberglass entry doors at Lowe's, Therma-Tru earned recognition as supplier of the year among
all brands sold at Lowe's.

Guided by Sound Corporate Values
As proud as we are of our results, we're equally proud of how we operate your company. While accounting scandals and backdating
of stock options have tarnished the reputations of some companies, integrity and shareholder trust have always been guiding princi-
ples in our corporate culture.

Our commitment to corporate responsibility extends to respecting the environment everywhere we operate. From expanding home
products manufacturing processes that reduce emissions, to initiating fish-friendly farming and ecological restoration programs at
our vineyards, we believe growth and environmental protection can go hand in hand.


                                                            




At the same time, we seek to strengthen the communities where we operate through selective philanthropic partnerships. And we
take very seriously the responsibility of owning one of the world's largest spirits and wine companies. Our Beam Global Spirits &
Wine business is committed to the highest standards of responsible marketing and sends a powerful message through the contin-
ued expansion of consumer education initiatives, led by its Drink Smart program: if you're of legal purchase age and you choose
to drink, do so in moderation and never drive drunk; and if you're below the legal purchase age, don't drink at all.

Enhancing Leadership Team
Fortune Brands has a proud tradition of developing management talent from within. At the start of 2007, we enhanced our manage-
ment team with the promotion of Bruce Carbonari to the new position of president and chief operating officer. A 17-year veteran of
our operations, Bruce led our Home & Hardware business for the past seven years with dramatic results: an intensified consumer
focus, strong organic sales growth and share gains, and successful high-return acquisitions. He previously did an outstanding
job leading our largest single brand, Moen. Bruce and I have worked together for many years. He's a highly talented executive with
a sharp focus on the consumer, and the board and I are confident Bruce will be a valuable addition to our headquarters team.
Rich Forbes, who did a tremendous job building our kitchen and bath cabinetry category over the past seven years, now leads the
Home & Hardware business.

We also saw some changes on our board of directors in 2006. Tom Hays, who served as chairman and CEO of Fortune Brands
from 1995 to 1999, retired from the board. Having led the transformation that resulted in Fortune Brands' focus on leading consumer
brands, Tom will always enjoy an important place in your company's history. We also value the contributions of Christopher Reyes,
who left the board to focus on other business interests.

We were fortunate in 2006 to add two very distinguished executives to the board: David Mackay, president and CEO of Kellogg
Company, and Dick Goldstein, former chairman and CEO of International Flavors and Fragrances.

Continuing Benefits of Our Balance of Power
In 2007, your company will mark its tenth anniversary as Fortune Brands. We approach that milestone with the strongest portfolio
of consumer brands and the best position in our categories that we've ever had. While we expect the downturn in the U.S. housing
market to present significant challenges in 2007, we believe Fortune Brands' breadth and balance will continue to be a big advan-
tage in creating value for shareholders.

On behalf of the 36,000 people in the worldwide Fortune Brands family, thank you for your investment. Your confidence inspires
us, and we look forward to putting Fortune Brands' balance of power to work for you in 2007 and beyond.

Sincerely,




Norm Wesley
Chairman and Chief Executive Officer
February 27, 2007