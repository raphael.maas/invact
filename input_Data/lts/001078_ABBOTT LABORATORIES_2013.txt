Dear Fellow
Shareholder
2013 was an extraordinary year for our
company, captured in simultaneous
milestones: On the years first day we
marked our 125th anniversary and
we became a new company with the
highly successful launch of AbbVie.
This rare combination of achievements says a great deal
about our company. On the one hand, its remarkable
for any enterprise to endure as long as 125 years. On the
other, its unusual for a company to make such a change,
separating its original and largest business.
But doing so was entirely consistent with our long history
of success  another example of Abbott reinventing itself
in new ways that reflect its changing environment and
opportunities. This readiness to adapt for the future is
why the Abbott of today is ready to attain new levels of
achievement in the years ahead.
There are four primary bases for our future success, all
clearly at work in the strong performance we delivered in
2013. Todays Abbott is:
BALANCED
Business diversity has long been our fundamental
strategy. But its essence is balance. We dont want to be
over-dependent on any single part of our mix. And todays
Abbott is better balanced than ever before  and
in multiple dimensions of our profile.
First is business balance. Our four major businesses 
Nutritionals, Medical Devices, Established
Pharmaceuticals, and Diagnostics  are of roughly the
same size. This provides us consistent opportunity to
participate across the spectrum of healthcare, without
over exposure to the challenges of any given sector. Next is
geographical balance. Not even a decade ago, the majority
of Abbotts business was in the United States. Today the
U.S. accounts for a more proportionate amount of our
worldwide sales, approximately 30 percent. Another 30
percent of our sales come from other developed markets,
and 40 percent from faster-growing emerging markets.
Again, this balance provides access to opportunity without
over reliance on any particular area. We have significant
presence and strong positions in the faster-growing
developing markets, and leadership positions that offer
durable performance in developed markets.
And, finally, weve achieved a new kind of customer
balance, as well: today approximately half of our sales
are direct to the consumers who use our products, up
from only 25 percent before our separation. This allows
us to build direct relationships with the people doing the
purchasing, and makes us less reliant on the decisions of
third parties.
GLOBAL
With roughly 70 percent of our sales now outside the U.S.,
todays Abbott is one of the most global of all healthcare
companies. Weve long had sales operations around the
world; but we are now a truly globalized organization,
in every sense, combining worldwide strength and
perspective with deep local roots.
Today approximately 70 percent of Abbott people are
employed outside the U.S. And were locating more
research and manufacturing operations in-region to better
understand and respond to local needs and preferences.

This year we will open new manufacturing plants in China
and India to be closer to our growing customer base.
And weve met this evolution of our profile with a
commensurate reshaping of our structure and operations.
Were continuing major initiatives begun in 2013 to
strengthen and streamline our international operations,
with regional marketing organizations deepening our
localization in key areas such as India, where we are
the largest pharmaceutical company serving the worlds
second-largest population.
ALIGNED
Abbott is in the right businesses and the right markets 
with the right products at the right time. We are very
well aligned with favorable global trends  scientific and
medical, demographic and economic  allowing us to help
more people around the world every year.
With a large and increasing percentage of our business
in emerging markets, were positioned to grow as both
these countries populations and their buying power
expand. This year, emerging economies are expected to
grow almost six percent, compared with just over two
percent for developed countries. And as nations move up
the income curve, one of their first priorities is improving
their peoples access to healthcare. As a result, developing
economies commit in excess of one-third more of their
incremental income growth to healthcare spending than
do people in high-income countries, and more than 50
percent more than those in low-income countries  yet
their healthcare spending remains only a small percentage
of gross domestic product relative to developed countries,
providing great long-term growth opportunity.
As an example of how economic growth can drive
healthcare investment, pharmaceutical spending in the
fastest-growing emerging markets is expected to nearly
double  the largest factor in total global pharmaceutical
market growth. We expect emerging markets to represent
approximately 75 percent of Abbotts pharmaceutical
business within the next several years.
And the worlds population is not only growing, but aging.
Today, approximately 23 percent of the worlds population
is 50 or older; according to United Nations projections,
that percentage will grow to 40 percent by 2050. Abbotts
leadership in treating conditions associated with aging 
such as cataracts, the worlds most-performed surgery, in
which were the second largest provider  promises strong
growth as need for these treatments continues to expand.
This trend offers similar opportunity to our adult nutrition
franchise, today a $3 billion business worldwide.
LEADING
These strengths and others make todays Abbott a leader 
in its major businesses, in product and geographic
markets around the world, and in a broad range of
important business practices.

Abbott is the world leader in immunoassay diagnostics
and blood screening; the global leader in adult nutritional
products, and the U.S. leader in pediatric nutritionals;
were the world leader in LASIK devices, in drug-eluting
and bare-metal stents; and we originated the new category
of bioresorbable vascular scaffolds, in which we continue
to have the worlds leading product.
In 2013 we were again recognized as our industrys
best deal maker, as a top employer, and as one of the
worlds best corporate citizens. And our broad and deep
research and development capability allows us to deliver
advancements across the spectrum of innovation, from
breakthrough technologies to packaging improvements.
In other words, our company continues to not just
maintain its excellence, but to advance standards across its
operations. We have a very strong base on which to build
our future.

PERFORMANCE
For the full-year 2013, we delivered solid growth in sales,
earnings and earnings per share over 2012. At the same
time, we returned nearly $2.5 billion to shareholders in the
form of dividends and share repurchases, and announced a
57 percent increase in our dividend beginning this year.
Strong performance across many of our businesses,
combined with gross and operating margin expansion,
enabled us to deliver on our 2013 expectations despite
some challenges.
At the same time, we executed on our strategic priorities,
delivering or exceeding operational sales growth
expectations across many of our businesses; completing
acquisitions that brought us important, best-in-class
new technologies in our endovascular and vision care
businesses; launching numerous new products across our
portfolio, including our MitraClip structural heart device
in the U.S., multiple innovations in our cataract business
that are driving share gains, and some 60 launches in
Nutrition; and achieving significant operating margin
expansion in Nutrition and Diagnostics.
AIMING H IGHER
While todays Abbott is very much the company youve
known in terms of its well established strengths, were
a very new one in terms of the opportunities before us,
the ways were adjusting and fine-tuning our operations,
and, perhaps most importantly, in a heightened sense of
aspiration for our future. This will be embodied by the
new corporate brand we will launch this year as we build
a powerful corporate identity in keeping with our position
as a long-term global healthcare leader, firmly focused on a
great future.
The essence of our business has been the same since
Dr. Abbott began it: We achieve our potential as a company
by helping people achieve their potential as individuals.
And we believe that potential is unlimited. Our goal is
to build the best Abbott yet. Given our distinguished
125-year legacy, thats a high ambition, indeed; but, with
the excellence of our opportunities and our capabilities,
its one well within our reach.

Miles D. White
Chairman of the Board
and Chief Executive Officer
March 3, 2014