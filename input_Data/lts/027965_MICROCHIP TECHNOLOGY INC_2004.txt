To Our Shareholders,
Microchip Technology continued to generate solid performance in many critical areas during
fiscal year 2004, despite challenging conditions in the semiconductor industry and the worldwide
economy. The long-awaited industry recovery has now materialized, allowing us to further
strengthen our business model during this upcycle.
For the fi scal year ending March 31, 2004, Microchips net sales were $699.3 million, an increase
of 7.3% from net sales of $651.5 million for the fi scal year ending March 31, 2003. Pro forma net
income for the fi scal 2004 period was $156.8 million, an increase of 17.2% over pro forma net
income in the prior fi scal year of $133.9 million. We achieved pro forma gross margins of 54.6%
in fi scal 2004 and maintained pro forma operating profi ts of 29.2%, signifi cantly better than the
performance of many of our competitors. (See Note 1 below.) Our balance sheet is strong, and we
have zero debt. Free cash fl ow is fl ourishing, driven by our sound operating results and successful
business model. During this past fi scal year, Microchip initiated two stock repurchase programs and
increased our quarterly cash dividend payment on three occasions.
Volume production commenced at Microchips Fab 4 semiconductor manufacturing facility
in Gresham, Oregon, during the year, bringing online the necessary manufacturing capacity to
accommodate expected increased demand in the current industry upcycle. Fab 4 is Microchips most
technically advanced facility, providing the foundation for the Company to develop and implement
new process technologies and products that will drive future growth.
In addition, we expanded our manufacturing test and assembly facility near Bangkok, Thailand,
with more than 67,000 square feet of additional capacity and related production equipment.
An innovative strip-testing methodology was implemented, which has significantly increased
production throughput and reduced costs.
In fiscal 2004, Microchip achieved ISO/TS-16949:2002 certification from the International
Organization for Standards/Technical Specification. This certification demonstrates that the
Companys quality systems meet the newest and most stringent industry quality-management
system standards and is expected to replace all existing automotive-industry quality standards in
Europe and the United States by 2006.
Reflecting our continued market share gains and leadership position in microcontrollers,
Microchip delivered its three billionth PIC 8-bit microcontroller to our customer, American Power
Conversion, accomplishing this milestone less than two years after our two billionth shipment
occurred and, more significant, during the worst-ever downturn in the semiconductor industry.

In fi scal 2004, the Company introduced 25 new PIC 8-bit Flash microcontrollers, which feature
larger memories and smaller pin counts, as well as products for electronic motor control
applications. These innovations extend our proprietary PIC microcontroller solution to a variety of
applications not being served today.
Microchip launched 34 new analog and interface products, such as op amps, low dropout linear
regulators and infrared protocol stack controllers, which can be attached to current and new
microcontroller designs. A revolutionary programmable gain amplifier was unveiled, providing
digital control over the amplifier function and design while reducing board space. Additional
innovations were brought to market, including 50 serial EEPROMs with higher speeds and smaller
packaging, a battery monitor with PowerSmart technology and lead (Pb)-free packaging for a
number of devices across our entire product portfolio.
Microchip shipped its first dsPIC 16-bit digital signal controllers for revenue during the fiscal
year, and today we are in volume production. These devices provide a high-performance 16-bit
Flash microcontroller solution with an on-board digital signal processor, allowing Microchip to
penetrate numerous computationally intensive applications that need higher performance. During
the year, we focused our resources on completing the technical infrastructure required by engineers
in this market segment, including high-performance software libraries, development systems and
third-party tool support.
In fiscal 2004, we shipped our 300,000th development system. Very few semiconductor
manufacturers can match Microchip in the total number of development systems deployed among
customers worldwide. During the fi scal year, Microchip introduced new high-performance
development tools, such as the MPLAB PM3 Universal Device Programmer. These development
tools provide faster time to market and lower total system cost for engineers, offering a clear,
competitive advantage to using our silicon solutions.
To shorten design cycle times, Microchip is enabling customers in the United States and most of
Europe to purchase products from our website. The buy.Microchip online catalog
(http://buy.microchip.com) continues to grow in popularity because of its powerful parametric
search capabilities, live inventory status, special offers and ease of use.
As we look to fiscal 2005 and beyond, Microchip is well positioned to take advantage of the
upcycle and to outpace the semiconductor industry. Our Fab 4 is providing the platform from
which Microchip can build the innovative process technologies and products that will continue to
increase shareholder value.
With sincere appreciation to our shareholders, customers and employees for your continued
confidence in Microchip,
Steve Sanghi
President and CEO
Microchip Technology Incorporated