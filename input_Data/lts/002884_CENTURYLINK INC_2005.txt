

                                          Dear Shareholders

                                           The telecommunications industry continued its rapid
                                           evolution during 2005 and will experience further
                                           innovation in 2006 and beyond. While this rapid change
                                           brings new challenges, we at CenturyTel believe these
                                           challenges are outpaced by the opportunities we will
Our high-quality
                                           have to offer a broad array of advanced services 
broadband network,
our committed                              both to our existing customers and to new customers
employee base, our
                                           in new markets.
excellent customer


                                          
                                                      Driving change in our industry is                           We take our relationships with our
service and our
                                                      the rapid advancement in Internet                          customers personally, and we work hard
financial strength                                    Protocol (IP) based technologies                           to retain and grow our customer base by
make us a strong                           that allow telecommunications, video,                                 providing the best value, solutions and
                                           wireless and other service providers to                               customer experience in our markets. Our
competitor now and
                                           offer services they have not traditionally                            high-quality broadband network, our
in the future.                             been able to support. These new                                       committed employee base, our excellent
                                           technologies mean that CenturyTel may                                 customer service and our financial
                                           face greater competition  particularly                               strength make us a strong competitor
                                           in the more densely populated areas                                   now and in the future.
                                           we serve. However, the advancement of
                                           IP technologies also provides us                                      Our Financial Strength
                                           unprecedented opportunities to sell new                               Enables Our Operational Success
                                           products and services over our broadband                              CenturyTel has the financial strength that
                                           network and to expand the geographic                                  we believe will be a key requirement
                                           scope of the markets we serve.                                        for companies to succeed in our rapidly
                                               We believe we are well positioned to                              changing environment. Once again, in
                                           take advantage of these opportunities.                                2005 CenturyTel delivered solid operating







                                                                                             2
results and further strengthened our                                                          Our new tagline,
                                                We Continue to Position Our Business
financial position.                             to Succeed in a Changing Environment
                                                                                              "personal touch ---
                                                As our environment grows more
    During the year we:
                                                                                              advanced communica-
                                                competitive, we know we must improve
    Generated nearly $965 million in net
I

                                                the way we serve our customers to
    cash from operations;                                                                     tions" is more than a
                                                ensure that our customer experience is
    Returned over $580 million of cash to
I
                                                                                              marketing campaign.
                                                unparalleled and that the costs we incur
    shareholders through share repurchases
                                                                                              It is a statement of
                                                to deliver services are competitive. We
    and cash dividends;
                                                believe that excellent customer care will
    Invested $384 million in network                                                          who we are, of the
I

                                                be a key differentiator in markets where
    enhancements;
                                                                                              way we do business,
                                                multiple competitors sell the same
    Improved our financial strength and
I

                                                                                              and of how we treat
                                                product sets at similar prices. We must
    liquidity through the repayment of
                                                challenge ourselves to provide this high
    $349 million in debt, decreasing debt                                                     one another and our
                                                level of service while being the low-cost
    as a percentage of total capitalization
                                                                                              customers.
                                                provider in our markets. We expect
    to 42.3 percent;
                                                this effort to deliver improved market
    Increased operating revenues 3.0
I

                                                penetration and provide additional
    percent and diluted earnings per share                                                    CenturyTel's
                                                pricing flexibility.
    3.3 percent;
                                                                                              Unifying Principles:
                                                  In late 2005 we introduced a new
    Grew high-speed Internet connections
I

                                                                                              Fairness -
                                                branding initiative that emphasizes the
    by more than 106,000, ending 2005
                                                qualities that distinguish us from other
    with over 248,700 broadband                                                                The Golden Rule
                                                service providers. Our new tagline,
    subscribers, an increase of 74.4 percent
                                                                                              Honesty and Integrity
                                                "personal touch --- advanced communi-
    over 2004; and
                                                                                              Commitment
                                                cations" is more than a marketing
    Further secured our customer base by
I

                                                campaign. It is a statement of who we
    increasing bundle penetration from                                                         to Excellence
                                                are, of the way we do business, and of
    12.2 percent to 15.5 percent of total
                                                                                              Positive Attitude
                                                how we treat one another and our
    access lines, driving further penetration
                                                                                              Respect
                                                customers. Together with our Unifying
    of our enhanced calling features and
                                                Principles, our brand is a critical tool in
    long distance services.                                                                   Faith
                                                helping our customers understand who
                                                                                              Perseverance
                                                we are and in helping our employees
    CenturyTel's strong cash flows and solid
                                                understand what is expected of them.
capital structure provide us the financial
strength and flexibility to invest in growth
initiatives, to meet the challenges that        Our Broadband Network Is
lay ahead and to return capital to              the Foundation for Our Future
                                                CenturyTel's network is positioned to
shareholders through share repurchases
                                                support the continued evolution that IP
and dividends. I am pleased with our
                                                technology brings. Over the last five
2005 financial results, and we are
                                                years, we have invested more than
committed to leveraging our financial
                                                $1.5 billion to enable broadband, extend
position to continue driving shareholder
                                                fiber deeper into our network, start
value in 2006 and beyond.
                                                an "on-net" competitive local exchange
                                                business and create a regional fiber




                                           network. Through these investments we                                          services in existing markets and to serve
                                           have begun migrating our fiber network                                         new customers in new markets.
                                           from a "voice-centric" TDM configuration                                            Additionally, more than two-thirds of
                                           to an IP-based system that supports a                                          our existing access lines are within easy
                                           wider range of advanced services.                                              reach of our fiber network. This positions
                                               At year-end 2005, nearly 75 percent of                                     CenturyTel to drive cost efficiencies in the
                                           our local exchange access lines were                                           IP environment as our different markets
                                           broadband capable. As technologies                                             will be able to cost-effectively share
                                           continue to emerge, we expect to be able                                       switches, video head-ends and other
                                           to deliver IP-video over our existing facilities                               centrally located network elements. At
                                           to a large number of customers with                                            the same time, more than 155 million
The broadband capaci-
                                           relatively modest amounts of additional                                        people live within 50 miles of either a
ty we have built into                      capital investment. The broadband capacity                                     CenturyTel service territory or a fiber
                                           we have built into our network is our                                          network. We believe there will be
our network is our
                                           principal strategic advantage in an                                            significant opportunities to grow and
principal strategic
                                           environment where multiple service                                             diversify our revenues by leveraging our
advantage in an envi-                      providers compete to meet a customer's                                         existing network to offer service to
                                           complete communications needs.                                                 some portion of these new markets.
ronment where multi-
                                               In addition to our broadband local
ple service providers
                                           exchange network, we also have nearly                                          We Are Capitalizing
compete to meet a                          10,500 miles of regional fiber, more than                                      on Today's Opportunities
                                           630 carrier points of presence and metro                                       As we plan for the future of our business,
customer's complete
                                           fiber networks in 18 markets. These fiber                                      we are also focused on taking advantage
communications needs.
                                           assets carry much of CenturyTel's internal                                     of the opportunities that are available to
                                           traffic and deliver voice and data services                                    us today. We have established partnerships
                                           to business and wholesale customers in                                         with wireless companies and a Direct
                                           18 states. While we believe there are                                          Broadcast Satellite service provider to
                                           significant opportunities to profitably                                        enable us to offer wireless and video
                                           expand these businesses  our fiber                                            services in our product bundles. We have
                                           and CLEC revenues increased more                                               also introduced switched digital television
                                           than 19 percent in 2005, excluding                                             service to portions of La Crosse,
                                           acquisitions  we also believe new                                             Wisconsin, and the surrounding area.
                                           technologies will allow us to leverage                                              We continue to utilize our flexible
                                           these networks to support broadband                                            product bundles to drive penetration of



                                                                                          4
broadband and other services. We believe        we are encouraged that rational policies     We are a strong
providing bundled services with a single        will result from these efforts.
                                                                                             advocate for regulatory
bill at a discounted price is a key element
                                                                                             policies that:
in customer retention. As of the end            We Enthusiastically
of 2005, 19.6 percent of our residential                                                     1) encourage network
                                                Look Toward Our Future
customers had purchased a Simple                As we review the results of our efforts in
                                                                                             investment and
ChoiceTM product bundle from us.                2005 and look ahead to 2006 and
                                                                                             broadband deployment,
  Additionally, we have increased our           beyond, there is much of which we can
efforts to provide our business customers       be proud and even more to which we can       2) ensure the universal
with the latest in communications services      look forward. We are excited about the
                                                                                             availability of affordable
to help ensure the success of their             potential of our broadband future and the
                                                                                             communications services
operations. For example, we recently            growth opportunities that technological
partnered with a large university to deploy     advancements will bring. We believe we       to customers in rural
a next generation IP communication              have the network, the employees, the
                                                                                             areas and
solution. This partnership allowed the          opportunities and the commitment to
                                                                                             3) create regulatory
university to make a smooth transition to       grow our business in the years ahead.
an IP-based platform, while reducing its                                                     parity among all
capital commitment.
                                                                                             service providers.
                                                Glen F. Post, III
We Are a Strong Advocate for
                                                Chairman and CEO
Sensible Regulatory Policies That
Promote Universal Service and
Broadband Deployment
The future of telecommunications
undoubtedly lies in broadband
deployment and the convergence of
services among various types of service
providers. Unfortunately, it is just as clear
that the regulatory environment has
simply not kept pace with technological
advancements and changes in the
competitive landscape.
  Our positions on the principal
regulatory issues facing our industry are
straightforward. We are a strong
advocate for regulatory policies that:
1) encourage network investment and
broadband deployment, 2) ensure the
universal availability of affordable
communications services to customers in
rural areas and 3) create regulatory parity
among all service providers. We will
continue our advocacy in these areas, and


