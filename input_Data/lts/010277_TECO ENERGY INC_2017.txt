
Letter from the Chair
Fellow Shareholder,
We continued to execute on our strategy in 2017, delivering strong results for the
business and our shareholders.
The Board remained focused on corporate strategy, risk identification and management,
the balance sheet, capital spending, our operations and our people, including leadership
development and organizational capacity across Emera.
Strategy Oversight
With Emera’s new scale and scope in 2017, the company and the Board renewed their
focus on strategy at both the corporate and subsidiary level. We worked to strengthen
the strategic focus areas and ready the organization for the disruptive forces and
changing technology facing the industry.
The company continued to deliver on its strategy of delivering clean affordable energy
to customers in 2017, making significant advances in integrating renewables and
embracing innovation. Emera recently completed the largest acquisition ever made by
the company (TECO Energy, Inc.) and successfully completed the Maritime Link Project
on time and under budget. Emera cut the ribbon on a 23 MW solar facility at Big Bend in
Florida and announced plans to build 600MW of solar generation – installing six million
photovoltaic panels and resulting in what will be the highest penetration of solar in the
generation mix of any utility in Florida. The team also introduced innovative new pilot
projects in Nova Scotia and Barbados that will focus on the capture, use and storage of
renewable energy. These initiatives and others will drive Emera’s continued growth in
2018 and beyond.
Our strategic focus continues to emphasize the greening of our
generation fleet in support of creating the grid of the future. These
changes require a long-term commitment by the Board and
management. Over the long term, Emera has consistently provided
value to shareholders, delivering a total shareholder return (TSR)
that exceeded the industry average. Over the past five years, Em
2     Emera Inc. — Annual Report 2017
Our regulated asset base continues to be strong, offering more earnings diversity,
capacity and quality. Over 90 per cent of our earnings currently come from our
regulated businesses, exceeding our minimum target of 75 per cent.
Consistently delivering returns in a dynamic business environment requires strategy,
planning and disciplined execution. In 2017, the team delivered despite many challenges,
including extreme weather.
Unseasonably warm or cold temperatures affect our financial performance, while
extreme weather events, such as a windstorm in Maine and Hurricanes Irma and Maria,
presented critical challenges in 2017. That’s why the team is continuing to improve our
ability to model weather to better understand system impacts and restore energy faster
for customers. These improvements were evident in Tampa Electric’s response after
Hurricane Irma. The team restored power to more than 425,000 customers, more than
half of its customer base, in only seven days after the storm. Emera Maine restored
90,000 customers, over half of its customer base, in just seven days after a major
windstorm in October.
Commitment to strong governance
Every year, the Board completes an extensive governance review
that sets out priorities and a rigorous action plan that is tracked
throughout the year.
In the first full year of TECO integration into the Emera group of
companies, we applied Emera’s subsidiary governance principles
through the recruitment of new independent directors in both
Florida and New Mexico. These Boards, with a complement of
independent Directors, are well positioned to provide oversight and
strategic direction for the businesses in these regions.
At the corporate level, Emera constituted a new Health, Safety and
Environment (HSE) Committee in 2017. In response to Emera’s
expanded size and diversity of operations, the Committee will focus
on oversight of the safety, health and wellness of employees and
advanced environmental monitoring and reporting. We aspire to be
world class in all of these areas. The Committee monitors and
evaluates HSE matters and emerging issues, reviews compliance, and
identifies and oversees the management of risks in these areas that
could adversely impact operations, strategy or reputation.
Given the tragic incidents that occurred in Florida and in Newfoundland and Labrador,
the Committee spent significant time reviewing the company’s plans and actions to
strengthen safety culture and safety programs across the business.
Emera’s new Health, Safety & Environment Committee is
overseeing how employees take care of each other and
the places we work.
Emera Inc. — Annual Report 2017     3
Letter from the Chair
Sustainability
In 2017, Emera produced its first annual Sustainability Report. The
report illustrates how we continue to advance Emera’s cleaner,
affordable energy strategy and are creating value for our customers,
our communities, and our environment. It highlights how we
continue to invest in initiatives that promote stronger communities
and growth; how our team members volunteer thousands of hours
in support of local causes; and how we are investing in partnerships
that build a culture of entrepreneurship and innovation in the
communities in which we operate.
It is the first time all of Emera’s commitments have been presented
in this way, demonstrating how sustainability is at the core of our
business. It also highlights how we’ve been successfully applying
our strategy for over a decade. We are committed to building a
strong sustainability function at Emera that represents our
strategy in action and ensures that all of our stakeholders
understand our strategy, our commitments and our results beyond
financial performance.
Renewal and succession planning
As Emera continues to grow, the Board is committed to ensuring we have the right
directors in place to guide the company today and into the future. Given the tenure of
the Board, we are engaging in longer term succession planning that balances board
renewal and the need for continuity and smooth transitions. We are continually striving
for diversity of experience, thought and gender, focusing on Board and Committee
effectiveness and instituting best practices.
Consistent with the Board renewal principles adopted in 2016, the Board welcomed
new director Kent Harvey in 2017. Mr. Harvey is an accomplished executive with more
than 30 years of experience in the energy industry in the United States.
I also want to extend a special thanks to my colleague and predecessor Chair of the
Board, John McLennan, who will be retiring from the Board in May 2018. John joined the
Board in 2005 and served as our Chair from May 2009 to May 2014. With his vast
corporate experience John could always be counted on as a voice of rich insight and
wisdom. I’d be remiss if I didn’t mention how much we all enjoyed, and will miss, his
Cape Breton spirit and wit! On behalf of my fellow Board members, we wish John the
very best.
I would like to take the opportunity to thank all of my colleagues on the Board for their
ongoing commitment to Emera and to the highest standards of corporate governance.
We published Emera’s first Sustainability Report in 2017,
showing how sustainability is core to who we are and
what we do at www.emera.com/sustainability.
4     Emera Inc. — Annual Report 2017
CEO succession
In March 2017, we announced our CEO succession plans, providing a 12-month transition
process that allowed for a seamless leadership transition.
On behalf of the Board of Directors, I’d like to thank Chris Huskilson for his remarkable
contribution to the business and congratulate him on an outstanding legacy of growth
and transformation at Emera.
Under Chris’ leadership, Emera’s assets have grown from $4 billion to approximately
$29 billion, we’ve provided annual total shareholder return that outpaces our industry
and increased the value of our shares to over twice what it was when he became
President and CEO.
A big part of Chris’ legacy is the team and culture that he built at Emera. He believed in
developing people by providing all employees equal opportunity to learn diverse skills
across the business. The team is strong and ready for the future.
I would also like to thank Chris for working so constructively with the Board as we all
focused on successfully transitioning the company for new leadership.
Scott Balfour became Emera’s new President and CEO on March 29, 2018. Scott’s proven
leadership ability, financial acumen and business experience make him the natural
choice to be the next CEO for Emera. We are confident that we have the right leader
and the right team in place for Emera’s future growth.
Finally, Emera was recognized as one of Canada’s Best Employers by Forbes Magazine
in 2017. I would like to thank the team across all the Emera companies for consistently
delivering for our customers and our shareholders. Emera thrives because of the
incredible work and commitment of the team.
Sincerely,
.
Jackie Sheppard
Chair,
Emera Inc.
Board of Directors