Dear JCPenney Stockholders:
In the face of a rapidly declining economy, JCPenney took
substantial steps in 2008 to position our business to effectively
deliver results for those most important to us  our Customers,
our Associates and our Investors. Our blueprint for effectively
managing through the economic downturn is our Bridge Plan,
which focuses on those initiatives that we continue to Accelerate,
Maintain or Moderate in order to put us back on the trajectory of
our Long-Range Plan for retail growth and leadership when the
environment improves.
In short, we are focused on tightly managing all aspects of our
business that are in our control, in order to best offset the impact
of the deep decline in consumer confidence and spending that
began in the latter part of 2007. The actions we took allowed us
to bolster our financial position in 2008. While we had operating
income of $1.1 billion compared to $1.9 billion in 2007, we
generated positive free cash flow and ended the year with
approximately $2.4 billion of cash on our balance sheet  making
us one of the best capitalized retailers in the industry with among
the lowest cash burn rates. Based on this, we were able to
maintain our annual cash dividend at $0.80 per share.
As we look ahead, our financial and operating flexibility puts
us in a strong competitive position relative to our mall-based
competitors. Given our expectation that these challenging
economic conditions will persist for the foreseeable future,
we are continuing to execute our Bridge Plan to withstand
these headwinds and grow our market share when the
environment improves.

Showing Our CUSTOMERS That What
Matters to Them, Matters to Us
Our Every Day Matters philosophy centers on showing our
customers that what matters to them, matters to us. This means
demonstrating that we understand the financial pressures they
are under and making it clear why they should choose us above
all others.
Most critical to this is that while we are maintaining the affordability
JCPenney is known for, we are stepping up the style and fashion
across our merchandise assortments by:
 building our portfolio of Exclusive Brands designed by some
of the worlds fashion leaders;
 continuing to enhance American Living, a new tradition in
American style exclusively designed by Polo Ralph Laurens
Global Brand Concepts;
 introducing new Private Brands to address underrepresented
parts of our business, and further refining the style of our
leading private brands: Worthington, Stafford, The Original
Arizona Jean Co. and St. Johns Bay; and,
 reinforcing our position as the headquarters for some of the
countrys most desired National Brands.
Examples of the great style we offer can be seen throughout the
pages of this Summary Annual Report.
Although mall traffic was down substantially in the second half of
2008, we believe that the style and quality of our merchandise,
coupled with our marketing and highly compelling promotions,
helped to drive traffic levels in our stores that were higher than the
mall overall, as well as to attract customers to our off-mall stores
and jcp.com.
We are also bringing excitement into our stores by accelerating
our roll-out of Sephora inside JCPenney. The more than 100
locations we have today are generating strong sales, and leading
customers to shop across the store as well. We plan to have
more than 150 locations by the end of 2009. To ensure we are
most effectively communicating to our customers the newness
and value we offer, we are executing updated marketing strategies
that better leverage new media outlets to reach our customers
and answer the question, Why JCPenney? In addition, we are
showing our existing customers that we appreciate their loyalty
by continuing to expand JCP Rewards, a program that allows
members to receive special offers and benefits.
We are also continuing to invest in our digital platform, which
includes maintaining jcp.coms industry-leading position as the
largest general merchandise website on the Internet. In doing this,
we are focused on staying current with customer-facing technology
trends, and how consumers use this technology, so we can reach
them directly and make it easier for them to shop JCPenney.

Empowering Our ASSOCIATES
Customer service is a key differentiator for JCPenney, and our
Associates are the front line in delivering that service. Central to
JCPenneys approach is ensuring that our Associates are deeply
engaged in their work by giving them the tools to succeed. Last
year, we introduced CustomerFIRST, a comprehensive customer
service platform that empowers every JCPenney Associate
to make customers their No. 1 priority. Its success to date is
evident in:
 a notable increase in our Associate engagement score, with
particularly strong marks for giving Associates the training and
authority necessary to do their jobs;
 a substantial decline in our Associate turnover, which both
supports improvement in our customer service as well as
reduces training costs for new hires; and,
 a significant improvement in our customer service ranking.
In fact, we ranked No. 1 in customer service among department
store retailers and No. 7 among all retailers in the 2008
National Retail Federation Foundation/American Express
Customer Service Survey.
Our Associate engagement work is part of JCPenney C.A.R.E.S.
(Community, Associates, Responsible Sourcing, Environment
and Sustainable Products), our corporate social responsibility
program. The strides we have made in these areas are described
in our annual Corporate Social Responsibility Report, which is
available through jcpenney.net and includes an update on the
growth of the JCPenney Afterschool Fund, the No. 1 national
organization dedicated to inspiring Americas children to be
smart, strong and socially responsible.
Building Value for Our INVESTORS
As we pursue these initiatives, we continue to manage our
business very conservatively. Specifically, we are:
 matching our inventory levels with sales trends by further
reducing product development cycles and controlling our
vendor orders. This allowed us to end 2008 with comparable
store inventory 13.5% below 2007 levels. Looking ahead, we
are planning our inventories for 2009 to be down approximately
10%, in line with our comparable store sales expectation.
 capitalizing on our jTime workforce management system, which
allows us to match store staffing levels with expected traffic,
and generate significant expense savings.
 implementing even stronger expense discipline throughout
our business by thoroughly analyzing processes across the
Company to eliminate unnecessary costs.
 modifying our store opening plans, while improving our existing
locations. In 2008, we incurred capital expenditures of just
under $1 billion, which we used towards opening 35 new or
relocated stores, primarily in our off-mall format. We also
completed significant fixturing and store environment
improvements in about 600 stores across the country, as well
as 24 major renovations and 90 store refurbishments.
For 2009, we have reduced our capital spending expectations
to $600 million, which includes plans to renovate approximately
15 stores and relocate or open 17 additional stores, including
our first location in Manhattan, in one of the countrys busiest
shopping areas. Additionally, we will continue to make
improvements in our store environment as we open more
Sephora inside JCPenney locations in the center core of our
stores, and plan for new merchandise fixturing to support
apparel and home brand launches.
Our Vision Remains Intact: To Be the Preferred
Retail Choice for Americas Families
While the current economic climate is making the path longer, we
have not lost sight of our vision to be the preferred retail choice
for Americas families.
As we work to achieve this, my sincerest appreciation goes to our
Associates for the care, commitment and enthusiasm they bring
to work every day. I also want to thank our Board members for
their solid judgment, intense focus and willingness to share the
benefits of their experience. Finally, to our stockholders, thank
you for your continued support. We are committed to making
JCPenney a rewarding investment for you.
Myron E. (Mike) Ullman, III
Chairman of the Board and Chief Executive Officer
April 2009
