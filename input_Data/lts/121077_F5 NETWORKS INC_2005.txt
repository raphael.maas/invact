To Our Shareholders:
In fiscal 2005, F5 continued to build on the strengths that drove our growth and profitability in fiscal
2004. During the year ended September 30, the company achieved record revenue growth, continued improvement
in our operating margin, further expansion of our addressable market and increased share of our existing
markets. Our growth and market share gains were driven by several factors:
 Accelerating worldwide demand for application delivery networking solutions that enhance
the security, performance and availability of applications that run over the Internet
 Increasing awareness of the F5 brand among enterprise customers and a growing number
of service providers
 An expanding portfolio of industry-leading products and the demonstrable superiority of
our integrated solutions
 Our strong competitive position, reflected in our growing share of the traditional Layer
 4  7 application switch market and our steady gains in the SSL VPN market
Our revenue growth, combined with stable gross margins and the increasing efficiency of our channel distribution
model, was the principal driver of our improving profitability throughout the year. Year over year, total revenue
of $281.4 million grew 64 percent from $171.2 million in fiscal 2004, helped by record fourth quarter revenue
of $80.6 million. Security revenue represented approximately 10 percent of total revenue, and FirePass, which
we acquired for $25 million in 2003, accounted for more than $21 million in product revenue. With gross margin constant at 77 percent for the third consecutive year, we achieved 31 percent pro forma operating
margin in Q4, resulting in a 27 percent pro forma operating margin for the year versus 15 percent in fiscal 2004.
On a pro forma basis, we earned $1.42 per diluted share in fiscal 2005, compared to $0.52 per diluted share in the
prior year. Pro forma results do not include stock compensation expenses which we began recording in the fourth
quarter of fiscal 2005. Including stock compensation expense, our operating margin in the fourth quarter was 25
percent and our fiscal 2005 earnings were $1.34 per diluted share.
Along with the growth of revenue and earnings, we continued to strengthen our balance sheet in fiscal 2005.
During the year we generated approximately $85 million in cash from operations and ended the year with $365
million in cash and long term investments. As a result of steady growth in our services business, we also saw deferred
revenue increase to $39.3 million from $28.1 million at the end of fiscal 2004.
Since F5 was founded nearly ten years ago, innovative technology has been a key driver of our growth. Launched
in the last weeks of fiscal 2004, BIG-IP version 9 accelerated the growing demand for our products that began
following the introduction of BIG-IP version 4.5 a year and a half earlier. While version 4.5 included many new
features and performance enhancements, version 9 introduced new technology that not only made our products
faster and functionally richer than competing products but is differentiated from other application delivery
networking technologies by our Traffic Management Operating System (TMOS).
Developed over a three-year period, the unique full-proxy architecture of TMOS enables our products to terminate
traffic passing to or from applications, inspect content across multiple packets, alter the content as needed, and
make decisions about where and how to route the traffic based on pre-set rules or in response to the changing
needs of the applications themselves  all at network speed. TMOS allows organizations to move work that had
to be done exclusively in the application onto the network, resulting in better control, lower cost and faster time
to market than the traditional approach. It also includes integrated SSL encryption and decryption that allows our
products to handle encrypted and unencrypted traffic with equal facility. In addition, its designed to support an
open-ended array of Layer 7 functions as separate software modules that can be integrated in custom configurations.
Although the bulk of our development is in software, our products are systems that combine our software with
hardware platforms designed to optimize the performance of the software. With the exception of our own Packet
Velocity ASIC for Layer 4 processing, these platforms leverage the speed and low cost of standard, off-the-shelf
components to deliver the fastest Layer 4  7 performance in the industry.
With the launch of BIG-IP version 9 at the beginning of fiscal 2005, we introduced three new systems: BIG-IP 1500,
BIG-IP 3400 and BIG-IP 6400. Last January, we added a new high-end system, BIG-IP 6800. At the time we launched
version 9, we also introduced six software modules that add functionality above the basic functions included in
TMOS: advanced routing; IPv6 gateway; SSL acceleration; intelligent compression; Layer 7 rate shaping; and advanced
client authentication. In April, we added caching in conjunction with the roll-out of Application Accelerator, our
first significant step into the WAN optimization market. The superior performance and functionality of these
products helped drive both our revenue growth and our market share gains in fiscal 2005.
Based on the latest report by the DellOro Group, our share of the Layer 4  7 switch market grew 12 percent during
fiscal 2005 to nearly 31 percent. Worldwide, we continued to narrow the gap with market leader Cisco, currently
with 36 percent, and in Japan we led the market for the second year in a row, according to recent reports by
DellOro and Japan-based Fuji Chimera. The capabilities of the new products and their success in the marketplace
also moved F5 farther up and to the right in Gartners Magic Quadrant for Web-Enabled Application Delivery,
widening the gap in both vision and execution between F5 and our competitors.
As in previous product transitions, sales of our earlier generation products tapered off and sales of BIG-IP version
9 products ramped steadily throughout the year, accounting for approximately 72 percent of BIG-IP revenue in the
fourth quarter. Based on maintenance contracts in force at year end, we estimate that the new products are
currently installed in 20 to 25 percent of our customer base, and roughly a third of those represent new accounts.
Within the next few months we plan to introduce BIG-IP 8400, which will deliver higher performance than the 6800
and enable customers to run multiple software modules simultaneously.
Just prior to the close of the fiscal year, we achieved a major milestone with the shipment of TrafficShield, our
application firewall, as a software module running on TMOS. During fiscal 2005, TrafficShield, acquired in June
2004, was available as a standalone product. Demand for the product was modest, however, and a survey of our
customer base indicated strong interest in the product as a software module integrated with BIG-IP. Porting and
integration were completed in the second half of the year, and we began shipping TrafficShield on BIG-IP in early
September.
While TMOS has a number of built-in security features that can be customized with an embedded scripting language
we call iRules, our acquisitions of FirePass and TrafficShield added new security functions to our core traffic
management solutions and expanded our addressable market. Similarly, our introduction of Application Accelerator
last Spring leveraged built-in features of TMOS to speed up the performance of network applications and gave us
a footprint in the growing WAN optimization market. Our subsequent acquisition of Swan Labs, which closed in
early October, enlarged that footprint with important new features and functions that we plan to integrate with
TMOS and BIG-IP during calendar 2006. Combined with our existing products, Swan Labs WANJet and WebAccelerator
products will give us the most comprehensive, fully-integrated application delivery networking solution in the
market.
Looking out into fiscal 2006, we believe our product portfolio, our technology roadmap, our market position, and
the strength and discipline of our organization will enable us to grow profitably in our core market and in the
adjacent markets into which we are expanding. On behalf of the Board and our employees, thanks for your continuing
support in our drive to become the unqualified leader in delivering products and solutions that optimize the security,
performance and availability of network applications.
John McAdam
President, Chief Executive Officer and Director
November 15, 2005