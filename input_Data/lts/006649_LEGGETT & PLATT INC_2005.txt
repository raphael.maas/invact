Fellow Shareholders,
This was an active year for Leggett. There were several accomplishments, as well as a few disappointments. The
accomplishments include a new sales record, a solid acquisitions year, strong cash flow, repurchase of 5% of the
companys stock, two key operational expansions, a timely debt issuance, and the addition of two non-management
directors to our Board. The disappointments include a large expense for the restructuring we undertook, and lack of
margin improvement in the Fixture & Display operations.
These events created much interest in Leggetts stock and considerable volatility in our stock price. The number of
shares traded during the year was about 25% higher than ever before. Leggetts stock began the year at a price of
about $28, and traded in the $25  $30 range for much of the year. In September, we reduced our sales and earnings
estimates for 2005, causing the stock to trade below $20 for most of October. But by year-end the stock had
rebounded to the $23 level. Of importance to shareholders, in May the Board awarded a 7% dividend increase, and at
year-end the dividend yield was 2.8%.
We are optimistic about 2006 and expect to post record sales and earnings. Sales should grow about 5%, and we
project a 15%  35% increase in per share earnings. Increased sales should come from both internal growth and
acquisitions, and will be offset slightly by divestitures associated with our restructuring activity. The earnings
increase should come from margin improvement, lower restructuring costs, and higher sales.
If events unfold as expected, 2006 should be a good year for our investors. Our executives feel strongly enough about
the Companys prospects that more than 100 of them decided (in December) to forego a portion of their 2006 cash
compensation. Collectively, they have relinquished more than $23 million of cash salary and bonus over the past
three years, and have chosen to accept Leggett stock and options instead. Our management team has a keen personal
interest in seeing that the stock performs well over the long term.
Priorities and Goals Unchanged. The priorities and goals we announced in September 2004 have not changed.
Profitable growth continues to be our main objective. We also intend to extend Leggetts record of dividend increases,
modestly increase leverage, and use excess cash to buy our stock.
Our long-term financial goals also remain unchanged; we are striving for:
 Double-digit earnings and sales growth
 Return-on-equity in the high teens
 Net-debt to net-capitalization at 30%  40%
 Annual dividend increases
 Top quartile performance versus peers
2005 Financial Results. Full year sales increased 4.2%, or $214 million, to a record of $5.3 billion. About one-half
of the growth came from internal sales increases, and one-half from acquisitions. Inflation (mainly in the first quarter
of 2005) caused most of the internal sales growth. We saw a series of acquisitions come together in the fourth
quarter; these should contribute noticeably to 2006 revenue.
Full year EPS decreased from $1.45 in 2004 to $1.30 in 2005. This earnings decline primarily reflects the 18 cents
per share expense for the restructuring-related activity we initiated in September. Other items include 7 cents per
share for unusually high workers compensation costs and an offsetting 5 cents per share for a one-time tax benefit.
Several other factors (including sales growth, purchasing initiatives, declining share count, utility and energy costs,
and currency rates) affected full year per share earnings, but essentially offset one another.
Our cash flow and balance sheet remain quite strong. During the year we generated $448 million in cash from
operations, a 32% increase versus 2004, and significantly more than was required to fund internal growth and dividends.

Solid Acquisitions Year. During 2005 we used $181 million in cash to complete 12 acquisitions, which should yield
about $320 million of incremental annual revenue. Two of these acquisitions created new business platforms for
Leggett, with ample room for future expansion:
 Americas Body Company (ABC) is a designer, manufacturer, and supplier of equipment for light and medium duty
commercial trucks. With revenue of about $150 million, this is the third largest acquisition in Leggetts history. When
combined with existing operations, Leggett became the second-largest supplier in this $1.5 billion U.S. market.
 Ikex / Jarex added $65 million of revenue and significantly enhanced Leggetts presence in the converting and
distribution of geotextiles, silt fencing, erosion control and soil stabilization products for the geotextile, landscape,
and agricultural markets. This addition boosted our annual sales to about $80 million in this $900 million market.
Dividend Growth Continues. In May, the Board of Directors increased the quarterly dividend from $.15 to $.16 per
share. At an indicated annual rate of $.64 per share, 2006 will mark the 35th consecutive year that Leggett has
increased dividends. Compound dividend growth over that time has averaged better than 14%. Only one other
S&P 500 company has matched that record.

In 2006, Leggett was again named to Standard & Poors list of Dividend Aristocrats. And, Mergents Dividend
Achievers lists Leggett among the top 70 companies for 10-year growth in dividends.
We continue to target dividend payout (over the long run) at approximately one-third of the average earnings over the
preceding three years. Leggett is proud of its record of dividend growth, and we expect to extend that record far into
the future.
Leverage Increases. In September 2004 we announced our intent to increase leverage (i.e. net debt as a percent of
net capitalization) toward the companys long-standing target range of 30%  40% while maintaining our decadelong
single A credit rating.
Accordingly, in August we issued $200 million of long-term debt, locking in a 5% interest rate for 10 years. Net debt
to net capital increased from 21.9% at year-end 2004 to 28.5% at year-end 2005. Importantly, our debt rating did not
change, and remains at the single A rating it has enjoyed for over a decade. We intend to gradually increase leverage
until we reach the target range.
The August 2005 issuance was our fourth debt offering since early 2003. These four issuances provide $730 million
of long term debt, with a weighted average remaining life of over 9 years and a weighted average coupon of 4.7%.
In addition to long term debt, we can issue up to $400 million in commercial paper through a program that is backed
by a $400 million, 5-year revolving credit commitment. We believe we have more than adequate availability of funds
to support all ongoing operations and take advantage of growth opportunities.
Significant Stock Repurchase. In 2005, we used $227 million to purchase 10.3 million shares of Leggett stock.
We paid, on average, about $23 per share. Shares outstanding declined to 182.6 million shares. During the first eight
months of the year we bought about one-half million shares each month. But between September and November,
when the stock price dropped to levels not seen since early 2003, we became more aggressive. In a nine-week period
we bought almost 6 million shares at an average cost of about $20.25 per share.
The Board of Directors has authorized the purchase of up to 10 million shares of Leggett stock in 2006; however, we
have established no specific repurchase schedule. In periods when many acquisitions are completed, there may be less
cash available to purchase stock. We look at many factors when deciding how much stock to buy, including the current
stock price and the amount of cash we expect to have available.
Operational Achievements. Operationally, 2005 was a busy and productive year. In September we announced plans
to reduce both the number of operating facilities and the amount of excess plant capacity. For several years we
purposely chose to maintain spare capacity, expecting market demand to eventually return to the levels seen in the
late 1990s. That incremental market demand has not materialized, and it is not clear when or if it will.
We identified 36 operations for closure or consolidation, and that activity is substantially complete. Collectively,
these operations had revenue of approximately $400 million. Most of this volume was shifted to surviving facilities,
but a $90 million revenue reduction is expected as the company divests small, non-core operations or walks away
from unprofitable business. Restructuring-related costs should total approximately $78 million ($55 million in 2005;
$23 million expected in 2006). Consistent with our historical practice, we record these charges as part of our normal
operating costs. Once completed, the ongoing annual earnings benefit from this activity is expected to be about
10-12 cents per share (or about a $30-35 million increase in EBIT).
Two years ago we initiated an effort to improve performance in the Commercial segment, and specifically in our
Fixture & Display group. We made progress in 2004, as Commercial segment margins increased from 2.8% in 2003
to 5.1% in 2004. We expected further improvement in 2005, but it did not happen. Excluding one-time restructuring
costs, Commercial margins in 2005 were essentially unchanged from those of 2004. That is a disappointment to us
and is not acceptable. Margins must improve, or we will embark on further restructuring activity.

Late in 2005, we implemented several projects at our steel rod mill which will increase ultimate capacity by 20%, to
approximately 540,000 tons per year when fully ramped up. These projects include installation of new billet-welding
equipment and implementation of other mill efficiency projects. With this expansion, the mill will help ensure a
consistent supply of quality steel rod for roughly two-thirds of our needs. We initially acquired this steel mill in 2002,
believing it presented a sound vertical integration opportunity for Leggett (as the largest purchaser of steel rod in the
U.S.). With this latest expansion, and considering the turbulence in the global steel industry over the last two years,
our purchase of this mill has turned out to be very attractive.
Finally, in September 2004 we announced an agreement to supply aluminum die castings for Briggs & Strattons
assembly plant in Auburn, Alabama. We have now completed a new 140,000 square foot die casting plant near the
Briggs facility. This project is an outstanding example of our deverticalization strategy, in which Leggett supplies
products to manufacturers who previously made their own components. After ramp up of production, we expect this
project to contribute over $45 million of annual revenue to our Aluminum segment.
Bright Future. We aim for long-term average earnings per share (EPS) growth of 15% annually, primarily from a
combination of margin improvement and sales growth. We are targeting 10%-15% annual sales growth over the
long term, with an increased emphasis on internal expansion expected to yield 4%-6% internal growth per year.
Acquisitions are expected, over the long term, to add 6%-9% to sales annually. In general, we foresee sales increases
coming from four areas: growth of our current markets, increased market share, international expansion, and entry into
new product markets.
We are committed to improving margins over the next few years. In 2006, we expect EBIT margins to improve in four
of our five segments, with gains coming mainly from operational improvements and lower restructuring costs. (The
Industrial Materials EBIT margin is expected to decline due to a lower steel scrap-to-rod price spread.) Over the longer
term, we anticipate higher margins as a result of our efforts to: increase sales and plant throughput, restructure or
eliminate underperforming operations, and reduce costs via purchasing and continuous improvement initiatives.
It is difficult, in a letter like this, to adequately convey the degree of optimism that we have regarding Leggetts future.
In large part, that optimism stems from our personal knowledge of Leggetts people, their level of expertise and desire
to excel, and their integrity and commitment to the company. Since investors cant possess that same level of
familiarity, we understand that some of you may be curious about what the future might bring. For that reason, in this
annual report we decided to address some of the questions and concerns that are most frequently voiced by investors.
We hope you find the Q&A content informative.
Sincerely,
Felix E. Wright David S. Haffner Karl G. Glassman
Chairman of the Board and President and Executive Vice President
Chief Executive Officer Chief Operating Officer
February 28, 2006