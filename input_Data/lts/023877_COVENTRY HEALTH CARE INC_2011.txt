The health care sector is in the midst of the most dynamic, turbulent and challenging period in its 
history. In many ways, the biggest--but certainly not the only--catalyst for change was health 
reform and the Affordable Care Act (ACA). No matter what the future holds for the ACA, the 
health care landscape has been permanently altered. And Coventry Health Care is right in the 
center of these changes. Despite the tumultuous environment, Coventry delivered another strong performance in 2011. We 
understand that's what you expect from us; we expect no less from ourselves. We have also never 
lost sight of the future. We continue to plant the seeds of future growth by strengthening existing 
partnerships and forging new ones that will be critical to our long-term success. As the book is closed on 2011, we can confidently 
state that Coventry finished the year stronger than it 
began. Our balance sheet is stronger, and Coventry 
grew our supply of "dry powder" to capitalize on new 
opportunities. All seven core businesses performed 
well by meeting or beating expectations. We attracted 
new talent and made organizational changes to create 
better alignment and leverage across our businesses. 
yet, while some of the ACA's immediate reforms fade 
into our rear view mirror and simply become how we 
do business, the biggest challenges are still ahead. building momentum Organizations that thrive in tough environments 
understand there are many keys to success--innova-
tion, leadership, discipline, execution--but none 
more important than building momentum. This does 
not happen by accident or come to those who wait for 
opportunities to tap them on the shoulder. Momen-
tum is built with purpose and intention: brick-by-
brick, step-by-step. Each success breeds confidence 
and contributes to the next win.

In 2011, Coventry Health Care achieved outstanding 
results by building upon our pillars of strength--a 
commitment to quality health care, financial disci-
pline, operational excellence, building talent, and 
forging strong relationships. Just as important as 
meeting today's commitments, we took major strides 
to position Coventry to achieve tomorrow's goals. We 
did this by fueling the engines of our future growth: 
Medicaid, Medicare and our commercial health 
plans. Each success was noteworthy on its own. 
Together, they created an enormous amount of busi-
ness momentum that will help propel Coventry to 
new heights. realizing our potential As the economy inched toward stability, purchasers of 
health insurance and coverage--employers, govern-
ments, and individuals--focused on value in 2011. 
This value-focused purchasing played to Coventry's 
traditional strengths and aligned with our "low cost 
wins" mantra. It also presented significant opportuni-
ties for Coventry to enter new geographies and offer 
innovative new products. For our commercial risk business, the sluggish recov-
ery and regulatory challenges from ACA implementa-
tion--such as minimum medical loss ratio rules and 
rate review requirements--dominated 2011. Despite 
these tests, our focus on offering affordable group 
and individual coverage through locally-operated 
health plans with superior customer service enabled 
us to maintain a stable membership base. In many 
respects, the commercial business is in a transition 
period leading up to insurance exchanges in 2014. 
Coventry is taking decisive action to build the health 
plan of the future through our high performance net-
work strategy where we collaborate with health sys-
tems and providers to offer new health plans that 
deliver better outcomes and higher quality at lower 
cost. Much work remains, but we are moving quickly. Coventry's Medicaid business in 2011 was a standout 
performer, as we made huge strides toward becoming 
a nationally-recognized Medicaid leader. Our 
Medicaid membership grew by 224,000, a 48% 
increase over 2010. This was driven by Coventry's 
success in winning a July 2011 award to provide
services to the Medicaid program in the Common-
wealth of Kentucky. The Kentucky program went 
live in November 2011 and by year-end, Coventry 
had attracted 221,000 members--the largest enroll-
ment of any of the three new entrants. Building on our Medicaid momentum, Coventry 
announced in October 2011 the acquisition of Family 
Health partners, a Medicaid health plan operated 
by Children's Mercy Hospital in Kansas City with 
210,000 members in Kansas and Missouri. While 
this acquisition did not quite close before the end of 
2011, it represented another key milestone by expand-
ing our Medicaid presence into Kansas, our tenth 
state with Medicaid operations. As state revenues 
continue their slow recovery and Medicaid costs 
grow, Coventry's Medicaid plans are well positioned 
as a solution to the state budget overhang while 
improving care coordination and quality. Looking 
ahead to new opportunities in 2012 and the health 
care reform expansion in our existing states for 2014, 
Medicaid has the clear potential to change Coventry's 
growth trajectory. Turning to Medicare, 2011 will be known as the year 
when Coventry regained our Medicare momentum in 
a big way. At this time last year, we knew that our 
part D business would take a step backward due to 
government policy changes to reduce the number of 
plans. While that was a major disappointment, our 
business met the challenge to create a new part D 
plan that would be very attractive to seniors during 
the annual election period for 2012. The result was a truly innovative part D plan known 
as First Health value plus. In partnership with some 
of the nation's best known names in retail and phar-
macy--Walgreens, Walmart, and Target--Coventry 
created value plus to be the nation's lowest premium, 
zero-dollar deductible part D plan. We built it on an 
emerging concept in prescription coverage: the pre-
ferred pharmacy network, which allows members to 
pay less--like $0 generic co-payments--when they 
fill prescriptions at a preferred pharmacy. Conse-
quently, Coventry gained just shy of 300,000 new 
part D members--25% annual growth--for the start 
of the 2012 plan year.

Coventry's Medicare momentum was not limited to 
part D, however. Our Medicare Advantage plans 
were attractive to seniors by delivering better benefits 
with higher quality and lower costs than original 
Medicare. Demonstrating our commitment to quality, 
we continued to make significant investments to 
improve our Medicare star ratings. Our strategy and 
efforts are paying off, as we experienced enrollment 
gains of 13% in our Medicare Advantage plans 
heading into 2012. Finally, Coventry's other core businesses--Workers' 
Comp Services, the Federal Employees Health Ben-
efit program, and Network rental--continued 
to diversify our operations and contributed to the 
company's 2011 results and strong cash flow. Our 
Workers' Comp business in particular again had an 
excellent year given the economic headwinds facing 
employers. poised for growth and future success What's interesting about periods of dramatic change 
is that they create the greatest opportunities. By con-
necting with virtually every major stakeholder group 
across our health care system--providers, employers, 
government and consumers--Coventry has a unique 
vantage point. It positions us to see the changes on 
the horizon, anticipate their impact, and quickly 
identify where the future opportunities lie. It also 
puts Coventry in exactly the right place to seize those 
opportunities. We made great strides this year to solidify our core 
and build momentum. I am even more energized 
about the future and Coventry's ability to adapt, get 
ahead of the curve, and emerge as a stronger, more 
successful company. Allen F. Wise
Chief Executive Office