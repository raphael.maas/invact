A Toast to Success
from Paul Varga

Dear Shareholders,
Its my pleasure to share with you once again some
of my perspectives on Brown-Forman and your
investment in it. As a general overview, I believe our
142 year-old company continues to thrive due to
the quality of our brands and the people who
help to build them each and every day in a global
spirits marketplace that offers such enormous
potential for growth.
Fiscal 2012: Our Growth Accelerates
In fiscal 2012, the companys underlying growth
rates in net sales and operating income were
both strong at 9%. These high single-digit growth
rates represent an acceleration over last years
comparable topline growth of 4% and bottom
line growth of 6%. Additionally, they approximate
Brown-Formans long-term historical rates
of growth, which of course is quite encouraging.
Comprehensive Performance The companys
growth story was led in fiscal 2012 by the Jack
Daniels trademark, one of the most valuable
distilled spirits brands in the world. The Jack
Daniels Black Label brand is one of the worlds
largest super-premium brands by both volume
and value. The combination of very large volume
derived from a broad and diverse set of global
markets, and super-premium prices, makes
Jack Daniels a very rare brand indeed. In fiscal
2012, our highly successful launch of Jack
Daniels Tennessee Honey in the U.S. extended
the trademarks reach to new consumers and
occasions, and helped propel our performance
in the largest global market for international
branded spirits

During this past fiscal year, I continued
to be impressed by the comprehensiveness of
our performanceacross geography, portfolio,
and function. Using geography as the example
here, consider that every one of our 12 largest
markets experienced net underlying sales growth
in fiscal 2012; our U.S. business grew market
share for the first time in several years; Europe
grew underlying sales at high single digits against
a very challenging backdrop; and slicing our
business yet another way, emerging markets grew
underlying sales by 17%, contributed about
45% of the incremental underlying net sales growth,
and now account for approximately 25% of the
companys business.
Beyond the geographic breadth of the
companys performance and in addition to the
Jack Daniels trademark, I was pleased with
Finlandias growth acceleration, as well as the
continued excellent progress of the super-premium
brands Woodford Reserve, Herradura, and
Sonoma-Cutrer. Southern Comfort showed signs
of improvement in the latter part of the year
and Im encouraged by our organizations
heightened level of focus and creativity in recent
During this past
fiscal year, I continued
to be impressed by
the comprehensiveness
of our performance
across geography,
portfolio, and function
months. While we are not where we want to be
just yet, I believe the brand may be starting to
turn the corner and we remain highly committed
to getting the brand growing again.
Strategic Considerations Fiscal 2012 was
equally impressive from a qualitative and strategic
standpoint as we continued to position the
company for success in the years ahead via both
operating and capital investments. These efforts
and investments encompassed R&D, Production,
Packaging, Supply Chain, Information Technology,
Digital Communications, Design, and Talent
Development, to name just a few.
Fiscal 2012 was equally
impressive from a
qualitative and strategic
standpoint as we
continued to position the
company for success
in the years ahead via
both operating
and capital investments.
Again in fiscal 2012, Brown-Formans return
on invested capital (ROIC) of 19% was at the top
of our spirits competitive set, and this important
metric illuminates the capital efficiency of our
business, while reinforcing the organic nature
of our continuing growth. I believe our strong fiscal
2012 results, industry-leading ROIC, excellent
financial health, and continuing prospects for
growth were key contributors to Brown-Formans
fiscal 2012 total shareholder return (TSR) of
22%, which compared quite favorably to the 5%
TSR for the S&P 500 over the same period.
10 Years Ending Fiscal 2012: Performing
in Both Good Times and Bad Times
While we were pleased with our progress and
results of the last 12 months, I believe that
longer-term performance is even more important
for a company such as ours that strives to thrive
and endure without end. Using our companys
initials, we often refer to this as Building Forever.
Studying the last ten years, for example, we
find it helpful to break the period into two
segments to illustrate a point. The first five years
of this period (from fiscal 200307), which
Ill refer to as the Good Times, were a period
of strong global expansion and excellent results
for many businesses. The second and more recent
five years (from fiscal 200812), which Ill refer
to loosely as the Bad Times, spanned both the
very difficult economic recession of 200809
and the steady recovery from 2010 to today.
returns for shareholders Brown-Formans
TSR performed very well over the entire ten-year
period and in both of the five-year periods. In
addition to the quality and consistency of our
performance across the three measurement
periods, the companys TSR relative to the S&P
500 is also noteworthy and emphasizes our
ability to perform well over a long-time horizon,
as well as our resiliency to grow the company in
both good and bad economic conditions.
We strive to produce these consistent and
excellent long-term returns for you, our
shareholders, by building strong and enduring
brands market by market, which have multigenerational
growth prospects and yield attractive
returns on invested capital over the long run.
Weve found that these types of brands generate
cash very efficiently, and we put that cash to
excellent use by balancing well our reinvestment
behind the business with our desire to return
some of it to you to enhance long-term returns.
Looking again at the last ten years, BrownForman
has generated $4.6 billion in cash over
the last decade, with virtually all of it
derived directly from the business operations
and only a tiny amount attributable to a
very small increase in net debt. Over the period,
we reinvested 33% of the cash back into the
business via capital expenditures, working capital,
and brand acquisitions (which in this
exercise is a net number that has been reduced
by the cash we received for our divestitures over
the periodLenox, Hartmann, and the Hoplandbased
wine business). Having satisfied the capital
needs and opportunities of the business (and
not discovering any additional acquisitions that fit
our criteria for long-term value creation), we
returned 38% of the cash to shareholders through
regular dividends, special dividends, and capital
distributions. We used the remaining 29% of the cash
to buy back Brown-Forman stock at an average
price of just under $40 per share. This balance of
cash deployment served both the business and
shareholders well over the last decade.
Looking Ahead
I believe our BF150 strategy is positioning
Brown-Forman to continue this track record of
excellent growth and returns. As part of that
strategy, we have high aspirations for both the Jack
Daniels trademark and the rest of our portfolio of
brands. Over time, weve learned a great deal about
the global whiskey business, primarily through our
experience with Jack Daniels, a brand that is global
in scale, premium in price, and has significant
potential for continued long-term growth.
 building brands Since we aspire to be one of
the leading companies in the very attractive global
whiskey category, we plan to apply our knowledge
to other brands. Just as weve been successful in
expanding the Jack Daniels trademark with
full-strength line extensions such as Gentleman
Jack and Tennessee Honey, and in successfully
developing and building the super-premium
Woodford Reserve bourbon brand, we intend to
expand our know-how to other whiskeys that
we acquire or create.
We are very bullish on the global whiskey
category and the increasing role that Jack Daniels
and American whiskeys are playing in it. Were 
also enthusiastic about the valuable trademarks
we own that compete successfully at the premium
end of the vodka, tequila, and liqueur categories.
Our BF150 strategy is
positioning Brown-Forman
to continue this track
record of excellent growth
and returns. As part of that
strategy, we have high
aspirations for
both the Jack Daniels
trademark and the rest of
our portfolio of brands.
We believe they too continue to offer superb longterm
growth potential for our company.
In closing, let me thank and congratulate
our employees worldwide for their individual and
collective efforts on behalf of the company.
It is because of them that the company is able to
report such excellent results, while also being
positioned for continued success in the years ahead.
And I thank you, our shareholders, as well for
your support and encouragement as we continue
to Build Forever.
Paul C. Varga
Chairman and Chief Executive Officer
June 27, 2012
