To the Shareholders and Cast Members of The Walt Disney Company:
Im delighted to share with you that fiscal 2007 was another outstanding year for your
Company, enlivened by significant creative and financial achievements to make all of us
proud. Disneys strong performance  across business divisions and around the world 
is a tribute to the men and women of
The Walt Disney Company and the great
experiences they deliver to consumers.
Their passion to excel is admirable, and
their commitment to quality is as consistent
as it is remarkable.
In 2007, we advanced our strategic priorities, strengthening our financial
results, growing the value of our brands, enhancing our ability to
meet critical challenges and building a solid foundation for future
growth.
Creativity and innovation are at the root of everything we do, and
in 2007 the creativity on display across the Company was simply
amazing.
Disney Channel was a big source of that great creative energy. High
School Musical 2, which premiered in August, was the highest-rated
cable movie of all time and extended what has in two years become a
true global franchise. Hannah Montana has emerged as one of cable
televisions most successful programs, with its star, Miley Cyrus, breaking
out as a top Disney recording artist and concert performer.
At our Studios, Pirates of the Caribbean: At Worlds End was the No. 1
movie of 2007 in global box office while Ratatouille was the best
reviewed film of the year. Ratatouille earned more than $600 million in
worldwide box office, making it the third-highest-grossing Pixar movie
of all time. And, as I write this letter, Enchanted is enchanting critics and
movie goers alike, and we are delighted with its initial success!
Theres nowhere the excitement and magic
of Disney comes to life quite like at our
Parks and Resorts and 2007 was no exception.
The Year of a Million Dreams has been
resonating strongly with our Cast Members
and Guests, so much so we are extending
it into this coming year. At Disneyland,
Finding Nemo Submarine Voyage has been
delighting Guests
and stands as
another great
example of
Pixars creative strength at our parks. Our
next Pixar attraction, Toy Story Mania!,
opens this year in California and Florida.
Our Consumer Products division continued
to come up with new ways to showcase
our characters and stories through quality
products that appeal to a wide range of
consumers. Last year, licensed merchandise
sales posted solid growth, led by our Cars
and High School Musical franchises.
Our non-Disney-branded
businesses also displayed
great dynamism. ESPN, the
preeminent brand in sports
media, sustained its high
level of creative excellence, bolstering its
connections with fans, whether they are
watching television, listening to the radio
or interacting online.
At ABC, our primetime schedule has a
host of successful programs, from the
returning series Greys Anatomy, Lost,
Desperate Housewives, Dancing with the
Stars, Brothers & Sisters and Ugly Betty, to
new shows such as Samantha Who? At
ABC News, we are proud that World News
Tonight is now the nations most-watched
evening news program while our local
newscasts are leaders in such key markets
as Chicago, Los Angeles and New York.
In 2007, we also strengthened our international
operations, forging new bonds with
consumers in some of the worlds most
promising markets. We released our first
original Disney film in China, The Magic
Gourd, built up our management teams in
Russia and India and continued to
grow in other markets, including
Europe and Latin America.
At Disney, we use technology to enhance
our high-quality content and experiences
and deliver them to more people, more
places, more often.
Disney.com was already the Internets most
popular entertainment site among kids and
families when we re-launched it last year.
The addition of a rich array of videos, as
well as casual games and easy access to all
Disney businesses, has made it even more
so. We are hard at work on an even newer
version, to be launched in 2008.
ABC.com provided viewers with a growing
selection of programs and a higher-quality
experience while ESPN.com is a sports
fans paradise, with great personalization
features and incredible dimensionality,
allowing the viewer to watch games, check
stats, get the latest news and participate
in fantasy leagues all at the same time.
Our strategic focus on creativity and innovation
translates directly into an impressive
financial performance. For the 2007 fiscal
year, Disneys revenue hit an all time high
of $35.5 billion, a 5 percent increase over
the previous year. Net income rose 39 percent
to $4.69 billion, driven by growth at
our Media Networks, Studio Entertainment
and Parks and Resorts segments. Earnings
per share, excluding certain items, grew by
24 percent to $1.92. We also delivered $3.8
billion in free cash flow and repurchased
over 200 million shares of Disney stock for
approximately $6.9 billion.
While we are extremely gratified by our
financial performance this past year, we
are also focused on delivering long-term
shareholder value and on making the right
investments to sustain growth, as well as
superior returns and our competitive
advantage.
Maintaining a
strong balance
sheet will
continue to
allow us to
take advantage
of opportunities,
but
any acquisitions
and their
timing will be
based on
sound financial
and strategic
logic as well as
the long-term
value of the
acquisition to
Disney.
We dont take our position for granted.
We live in a challenging and dynamic environment
and feel it is imperative to apply
operational and financial discipline and
to manage costs carefully. While we look
forward to continued artistic success, we
also continuously seek ways to manage
our creative processes and our Company
more efficiently.
The strengthening of our creative engines
remains a priority. They give Disney its
competitive advantage and build brand
and shareholder value. Im proud to say
that no entertainment company has as
many vibrant creative engines as Disney,
or the ability to leverage success across so
many businesses and in so many places.
We call this value creation dynamic the
Disney Difference. To make the most of
our creative content, our portfolio of
Disney businesses combine to create a
highly effective marketing engine that
helps increase revenue while affording
numerous efficiencies.
When the Disney name is on a product, it
enhances our ability to enter new markets,
whether they are technology platforms or
geographic territories, while increasing the
value-generating lifespan of that product.
The Disney Difference drove our decision
to focus more on making Disney-branded
movies at our Studios and on developing
Disney-branded video games at Consumer
Products, and it will continue to drive our
creative and strategic focus across numerous
other businesses.
We made a number of significant decisions
this past year that are designed to reinforce
the Disney Difference while increasing long
term shareholder value.
We purchased Club Penguin, a vibrant
and entertaining online world for kids and
families that will help anchor our strategy
to grow in this important and expanding
space. We are proud to call Club Penguin
our own, and believe that it is a great fit
from a creative and a strategic perspective.
Pirates of the Caribbean: At Worlds End was the years
top-grossing film worldwide.
At Disney.com, we recently
launched a richly detailed
online Pirates world and are
expanding the popular virtual universe
built around Tinker Bell and her friends,
where fans have already created 3.5 million
fairies. We are also developing an exciting
online version of Radiator Springs, making
the world of Cars interactive, one of our
most successful content and merchandising
franchises. The creation of more such compelling
immersive worlds, which feature a
safe, entertaining way for kids to network
and play games, is a key priority of ours.
Weve also recently announced plans to
expand and improve Disneys California
Adventure in celebration of the hope and
optimism that attracted Walt Disney to
California in the 1920s. The addition of new
family attractions, entertainment and an
entirely new land, Cars Land, should make
our successful Disneyland Resort even more
of a must-see destination.
Our decision last year to build two new
cruise ships was an exciting one. This
business has not only delivered impressive
returns, but it has become an important
brand builder for us. Guests love the
experience and appreciate the way weve
extended this family vacation offering. We
have also unveiled plans to develop a new
resort at a stunning location on the
Hawaiian island of Oahu, expanding our
successful Disney Vacation Club concept,
as well as offering a great Disney family
resort experience in Hawaii.
Efforts to build our video game business
are encouraging, and we will continue to
invest in this growth area. We believe it will
become a significant growth driver for us
and can be supported by our numerous
businesses in order to maximize potential
and create long-lasting value.
Our commitment to high-quality creative
work, a persistent focus on new technologies
and intelligent investment in international
markets are the strategies we believe
can continue to carry us forward. But in
upholding the outstanding reputation
afforded to the Disney name, we must also
continuously enhance our commitment
to social responsibility and particularly to
the families and children who are our
biggest fans.
In October 2006, we decided to
put our brands and characters to
work for families with a new
healthy food program, including
healthier food options for kids at
our parks and resorts and in our
licensed consumer products.
This pioneering initiative has
since been rolled out globally
to enthusiasm on the part of
our Guests and consumers.
This past year we also made
the decision to no longer
allow the depiction of cigarette
smoking in Disneybranded
motion pictures.
Over the last year, we
have also broadened some
longstanding environmental
and conservation initiatives.
We appointed an Environment
Council, made up of senior executives
from across the Company, to
analyze and implement sustainable strategies
for minimizing Disneys impact on the
environment. We are taking a measured
and comprehensive approach to this complex,
important task and expect to start
putting enhanced policies in place in the
coming year.
The past year also brought changes to
our Board of Directors. We said goodbye
to one of our longstanding Board mem-
bers and welcomed a new one. Father
Leo ODonovan, President Emeritus of
Georgetown University and a professor
of theology at that fine institution, retired
after 11 years of dedicated and distinguished
service. Father Leos wise counsel, scholarly
insight and sense of fun will be missed.
We are delighted that Susan Arnold has
joined Disneys Board. Susan has had an
illustrious career at Procter & Gamble,
where she is President, Global Business
Units. Her expertise in connecting with consumers
and in producing consistent growth
from a broad portfolio of brands fits perfectly
with our own goals. We are really
excited to have her on our team. Our
Board represents a diverse group of people
with a broad set of interests and experiences,
which I value greatly.
In my two years as Chief Executive, Ive
come to appreciate the breadth and depth
of The Walt Disney Company even more.
We have passionate, committed, talented
and experienced Cast Members. Nurturing
the vibrancy of the creative process, taking
full advantage of the opportunities offered
in emerging markets and by new technology
and building on the huge potential of
this great Company and its fantastic legacy
is a truly inspirational challenge. Im both a
little humbled and totally thrilled to be
leading Disney at such a wonderful time in
its history.
Everyday, the people of The Walt Disney
Company wake up to the challenge of
exceeding the lofty expectations of our
Guests and consumers. Thats a huge
responsibility, but one that we are honored
and excited to live up to. So, on behalf of
the 137,000 Cast Members and employees
of Disney who work to create special memories
and experiences that our consumers
enjoy around the clock and around the
world, Id like to thank you personally for
your continued support.

Robert A. Iger
President and Chief Executive Officer,
The Walt Disney Company