TO OUR SHAREHOLDERS

The recession�s impact on the financial services industry the
primary market for our products and services � has been
unprecedented. Our existing and prospective financial
institution customers were not insulated from these challenges,
and like the industry at-large, many responded by decreasing
discretionary spending and postponing non-essential
buying decisions.
Despite the recession�s impact on our ability to earn new
customers and expand existing customer relationships at
historic levels, our solid balance sheet, conservative business
principles, recurring revenue, long-term contracts, large and
loyal customer base, and diversified product offering enabled
us to continue to generate solid financial results. During fiscal
year 2009 ended June 30, total revenue increased to a record
$746 million. Net income was $103 million or $1.22 per diluted
share, as compared to net income of $104.2 million or $1.16
per diluted share reported in fiscal year 2008. We generated
strong cash flow from operating activities of $207 million,
return on assets was 10 percent, and return on equity was
17 percent.
Our revenue mix for the year consisted of $58 million in
software license fees or eight percent of total revenue,
$614 million in support and services or 82 percent of total
revenue, and $73 million in hardware sales or 10 percent of
total revenue.
Recurring revenue, which provides the financial stability to
support our ongoing growth, was approximately 75 percent
in fiscal year 2009, compared to 70 and 66 percent in fiscal
years 2008 and 2007, respectively.
Backlog, which consists of contracted sales of products and
services that were not delivered by fiscal year-end, reached
$289 million, a 12 percent increase over the $257.4 million
reported last year.
We generated excellent profitability with a 21 percent
operating margin.
Our company-wide focus on expense reduction also positively
impacted the fiscal year�s financial performance. Opportunities
to reduce near- and long-term expenses were carefully
scrutinized and numerous cost containment initiatives were
implemented, including several that reduced our payroll
expenditure which is our largest expense. Voluntary time off
without pay, staff reassignments, and company-wide salary
reductions enabled us to reduce payroll while avoiding layoffs
that negatively impact our customers and employees, and
dilute the highly specialized workforce we have recruited
and trained, and that we will need when the economy
recovers and financial institutions begin buying technology at
historic levels.

For 33 years, our company has prospered in a highly competitive
and consolidating industry and weathered various economic
cycles by narrowly focusing on providing high quality,
technology-driven products and services backed by distinct
levels of customer care. Through this most recent economic
downturn and its impact on our financial performance, our
strategic focus has not changed and each of our three brands
continues to execute its successful business strategy.

We are confident the economy will recover and with our
customers� loyalty and guidance, our associates� determination
and dedication, and our shareholders� confidence Jack Henry
& Associates will emerge from this challenging business
environment strategically and financially positioned to
pursue the burgeoning business opportunities that will once
again generate the company-wide progress and financial
performance we expect.
Chief Executive Officer
President
Chief Financial Officer & Treasurer