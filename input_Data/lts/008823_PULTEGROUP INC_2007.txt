The year 2007 will be remembered as one of the most difficult the homebuilding industry
has faced in decades, as builders and homebuyers struggled with tighter mortgage
liquidity, higher cancellation rates, and an elevated supply of new and existing for-sale
homes. Nevertheless, Pulte Homes succeeded in implementing aggressive revisions
to its strategy to drive operating efficiency and further strengthen its financial position
in the midst of this severe housing downturn.

The past year was not without its high points. As
projected in the 2006 annual report, Pulte Homes proudly
delivered its 500,000th home in 2007, making Pulte the
only domestic homebuilder to achieve such a milestone.
However, this accomplishment was overshadowed by
the difficult operating environment that existed during
the year. According to government statistics, there were
605,000 new home sales in 2007, well below the 1.2 million
units sold in 2004 and 2005. Pulte Homes experience
was similar, with closings down 34 percent in 2007 to
27,540 homes.
Pulte Homes suffered a net loss of $8.94 per share in
2007, compared with net income of $2.66 per diluted
share for the prior year. The $2.3 billion loss was the first
in the Companys 58-year history, and can be primarily
attributed to inventory impairments and land-related
charges. Revenues for 2007 totaled $9.3 billion compared
with 2006 revenues of $14.3 billion.
Under these unprecedented circumstances, Pulte developed
a list of near-term metrics that would optimize
performance and refocus efforts on three aspects of
operational efficiency and balance sheet strength: cash
generation, reduction of house and land inventory positions,
and development of a leaner cost structure. Were
pleased to report that we exceeded our goals on each
of these objectives, positioning us for even more
aggressive goals in the coming months.
The Company made great strides in cash generation,
exceeding a year-end goal to close 2007 with $1.1 billion
in cash. Our 2008 year-end cash target of more than
$2 billion underscores the importance of this critical
metric during an ongoing downturn.
The second metric required us to adjust our land strategies,
including walking away from optioned lots that could
not be successfully renegotiated, delaying option takedowns
where possible and slowing incremental land
investments. These actions led to a 32 percent reduction
in our year-end pipeline to 158,000 controlled lots, and
enabled us to dramatically reduce land development
expenditures in 2007.
We also incrementally reduced our construction volumes
by starting fewer homes without a buyer in place. After
reducing our speculative inventory to 5,000 units in 2006
from a 9,000 unit peak, we further lowered this metric by
25 percent in 2007 to 3,700 units. This accomplishment
is particularly noteworthy in light of elevated cancellation
rates of homes under contract, which can add to the
backlog of spec homes if not closely managed. Going
forward, we will continue to drive spec units to the lowest
possible level in each of our markets. We feel it is
more prudent to have fewer unsold homes on our books
given the lack of visibility this current market provides.
Our third measure called for a reduction in overhead
spending toward the goal of becoming a leaner organization.
We exceeded our goal of lowering SG&A costs as
we reduced this expense by $150 million during the second
half of 2007, partially due to a 31 percent reduction of
our headcount during the year. Although we were forced
to part with many talented and dedicated employees, it
was a decision that we believe will help best position
Pulte Homes for future success. We are on target to
generate additional overhead savings in 2008.
Financial liquidity and flexibility remain Pulte Homes
greatest strengths. Our 12-year average debt maturity on
$3.5 billion of senior notes is the longest in the industry,
and we ended the year with no borrowings outstanding
under our revolving credit facility which has a scheduled
maturity of 2012. Combined with our year-end cash balance,
this puts us in good position going into 2008 to
either weather a lengthier downturn or respond quickly
once the industry shows signs of stabilization.
In either case, we recognize that many short-term challenges
remain. Pressure on already-reduced home prices,
concerns about the overall economy, and the elevated
level of new and existing homes for sale have not begun
to subside. These factors continue to negatively affect
consumer confidence, and until that begins an upswing,
the duration of this housing downturn is simply too difficult
to predict.
Despite the difficult near-term challenges, Pulte always
balances short-term concerns with a focus on long-term
industry prospects. Trends in immigration and household
formation point to an upward trend in housing over time,
and we are taking the necessary steps to be in the best
position once a recovery takes hold. As we move through
2008, we will place greater emphasis on the near-term
goals of additional cash generation, effective management
of our house and land inventory, cost containment
and bolstering our already-strong financial position.
Our success in these efforts will once again be buoyed by
our talented work force as they generate greater value
for our shareholders and business partners. Our people,
process and product are the best combination in the
industry. Pulte employees take pride in what they do
and their dedication serves as the foundation for our
long-term success.
In our 58-year history, we have survived several downturns,
emerging from each a stronger, more efficient
industry player. We are confident that Pulte will once
again experience better days. We thank youour
shareholders, customers, associates and business
partnersfor your continued support, as we strive for
excellence in everything we do.
Sincerely,

RICHARD J. DUGAS, JR.
President and Chief Executive Officer