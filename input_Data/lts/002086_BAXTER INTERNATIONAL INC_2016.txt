LETTER TO STOCKHOLDERS 

Dear Stockholders, 

Baxter is in the midst of a dynamic transformation. 
We are building on the best of what has defined us for decades as a 
healthcare leader, including our mission to save and sustain lives, our 
commitment to innovation, and our strength across hospital products 
and renal care. 
We are also redefining how we do business. Greater focus, discipline 
and momentum are taking hold throughout the company. The 
outcome is enhanced value for patients, investors and our many 
other stakeholders. 

GROWING MOMENTUM IN 2016 

2016 marked Baxters first full year 
of operation following the successful 
spinoff of our BioScience business, 
Baxalta Incorporated. It also marked 
my first year as Baxters CEO. 

Baxter began the year with a solid 
foundation, great people and 
meaningful growth potential. The goal 
has been simple: to convert this 
potential into sustained top-quartile 
performance among our industry peers. 

A year later, we have made clear 
progress. Baxters 2016 worldwide net 
sales were $10.2 billion, a year-over-year 
increase of 2 percent, or 4 percent on a 
constant currency basis. On an adjusted 
basis, excluding special items of $7.05 
per diluted share, full-year income from 
continuing operations was $1.1 billion, 
or $1.96 per diluted share, a 42 percent 
increase over 2015. Full-year income 
from continuing operations, as calculated 
on a GAAP basis, was $5.0 billion in 
2016. Adjusted operating margin was 

13.6 percent, an increase of more than 
4.5 percent versus the prior year, and 
free cash flow of $905 million (which 
reflects operating cash flow of $1.6 billion 
less $719 million in capital expenditures) 
was more than 2.5 times 2015 levels. On 
a GAAP basis, operating margin was 
7.1 percent in 2016. 
Our achievements to date reflect the 
impact of three strategic levers: 

PORTFOLIO AND 
INNOVATION MANAGEMENT: 

Over the past year, we have been 
conducting a thorough review of our 
portfolio, pipeline and geographic 
scope, realigning investment where 
we have the greatest opportunities to 
achieve differential results for patients 
and the business. 

High-value innovation is at the heart 
of our mission. It is also our most 
crucial driver of sustained top-line and 
bottom-line growth. In 2016, we took 
action to accelerate R&D productivity 
while shifting investment to emphasize 
our core strengths and fund some 
compelling strategic bets. Products 
recently launched around the world 
signal this focus, including: 

 The AMIA automated peritoneal dialysis 
(APD) system and HOMECHOICE 
CLARIA APD system, both featuring 
Baxters SHARESOURCE Connectivity 
Platform two-way telemedicine 
technology 
 HDx therapy enabled by the 
THERANOVA dialyzer, providing 
high-performance hemodialysis 
treatments with a filtration profile that 
more closely mimics the natural kidney 
 The next-generation SIGMA SPECTRUM 
infusion system, with its Master Drug 
Library and other enhanced patient 
safety features 
 HEMOPATCH, our advanced surgical 
patch 
 The premix drugs Vancomycin in 
Sodium Chloride and Cefazolin 
Injection, both part of our growing 
generic injectables portfolio 
 NUMETA G13E injection, the only 
triple-chamber commercially 
prepared parenteral nutrition system 
for vulnerable neonatal patients 

OPERATIONAL EXCELLENCE: 

Through our Business Transformation 
Office, we have been tenaciously 
pursuing opportunities to streamline 
expenses and usher in a more efficient 
operating profile. Our first phase of 
actions (begun in 2015) has already 
put us in a better position to control 
spending while addressing patient and 
customer needs. Further steps are now 
underway supporting our transition into 
a leaner, more agile organization. 

CAPITAL ALLOCATION: 

Reinvestment in expansion and innovation 
is pivotal to sustained top-quartile 
performance. In 2016, we successfully 
completed the disposition of our 
retained stake in Baxalta by exchanging 
Baxalta shares for outstanding Baxter 
shares, contributing to U.S. pension 
program funding and reducing debt, 
further strengthening our balance sheet. 
We also initiated the acquisition of 
Claris Injectables Ltd., which will greatly 
enhance our portfolio and pipeline of 
generic injectable medicines and 
provide a platform for future growth. 
We expect the transaction to close in 
the second half of 2017 upon the 
satisfaction of related closing conditions. 

Naturally, we are also deploying 
capital to return enhanced value 
directly to investors. In 2016, Baxter 
increased its dividend 13 percent and 
executed targeted share repurchases 
of approximately $300 million. 

LOOKING AHEAD: TRANSFORMATION 
THROUGH EXECUTION 

Our transformation is off to a positive 
start, and there are more opportunities 
to increase our momentum through the 
relentless execution of our strategy. 

We will continue to drive innovation as a 
platform for accelerated growth through 
a steady flow of new life-sustaining 
Baxter products and technologies. We 
anticipate more than 100 new product 
launches and geographic expansions 

over the next five years, to be further 
augmented upon the anticipated closing 
of the Claris acquisition. 

We will continue to improve operational 
efficiency and manage expenses with 
discipline. Our Business Transformation 
Office is actively pursuing new initiatives 
that will contribute to further margin 
expansion moving forward. 

And we will deploy capital to enhance 
value and profitability for stockholders. 
This includes judiciously evaluating 
business development opportunities to 
enhance top-line growth and margin 
expansion in our areas of core strength 
and key adjacencies. It also includes 
returning value directly to our investors 
through dividends and share repurchases. 

This transformation is being fueled 
by Baxter employees worldwide. Our 
motivated, dedicated team is embracing 
change in an unyielding pursuit of our 
mission. The impact is clear in our work 
for patients, our business results, and 
our well-recognized leadership in 
corporate social responsibility. 

Our accomplishments in 2016 
demonstrate that sustained top-quartile 
performance is within our reach. I am 
energized by our progress and confident 
in our prospects. I look forward to 
providing further updates as we 
continue this journey. 

Best regards, 


Jos (Joe) E. Almeida 

Chairman and Chief Executive Officer 

March 17, 2017

