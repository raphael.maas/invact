


Dear Fellow Shareowner:

As I write to you today, I am pleased with the progress we     products enterprises in the world, with hundreds of thousands
made in 2005 toward our goal of delivering consistent, sus-    of employees and an estimated $80 billion in revenue.
tainable growth. In 2005, both profits and unit case volume
reached a record high, and our employee morale improved.       My priority in 2005 was to continue building on this unrivaled
Our Manifesto for Growth has set our strategic road map,       foundation to deliver long-term sustainable growth while being
and the engagement of our people has given us a solid start.   mindful of our short-term commitments. Our work is far from
                                                               finished, but as we've moved from words to deeds and from
Sustainable growth is how The Coca-Cola Company will           plans to actions, the initial impact of our efforts shows clearly
regain its position as the beverage provider of choice for     in our 2005 results.
consumers, the employer of choice for our people, the
partner of choice for our customers and the investment         In 2005, our Company earned $2.04 per share, an increase
of choice for our shareowners.                                 of $0.04--2 percent--over 2004. Volume grew 4 percent
                                                               to 20.6 billion unit cases, and net operating revenues grew
We understand the unspoken agreement between our Company       6 percent to $23.1 billion. Through Our Manifesto for
and those who choose to purchase and consume our products      Growth, we identified strategic corridors for expansion to
every day around the world. We understand our responsi-        complement our core carbonated soft-drink business. We
bilities as an engaged citizen of the world. We believe        more effectively integrated marketing, strategy and innova-
we lead a system that creates value and makes a positive       tion while reinvesting an incremental $400 million in those
difference everywhere it does business. Taken as a whole,      capabilities. And we introduced new products--capturing
the Coca-Cola system--comprising The Coca-Cola Company         greater share--in juice and juice drinks, water, energy drinks
and our bottling partners--is one of the largest consumer      and sports drinks.

                 


Platform for Sustainable Growth: In my letter to you a             Generate new avenues for growth through core
                                                                   brands, with a focus on diet and light products: Last
year ago, I identified six areas of focus required to accelerate
                                                                   year, we continued to see strong results for carbonated soft
sustainable growth. We made meaningful progress in each
                                                                   drinks, led by our core brands--Coca-Cola, Diet Coke, Sprite
of these areas in 2005.
                                                                   and Fanta. Coca-Cola, the world's best-loved soft drink, grew
                                                                   2 percent in unit case volume in 2005, the highest rate of
Build on our fundamental strengths: To help improve
                                                                   growth in five years. We also introduced several new
execution around the world, the Company revised its orga-
                                                                   Trademark Coca-Cola diet and light products, including
nizational structure in March 2005, creating the East, South
                                                                   Diet Coke Sweetened with Splenda and Coca-Cola Zero
Asia and Pacific Rim Group; the European Union Group;
                                                                   in North America. By the end of 2005, Coca-Cola Zero
and the North Asia, Eurasia and Middle East Group. On
                                                                   approached a 1 percent share in supermarkets--a significant
February 1, 2006, we appointed Muhtar Kent as president,
                                                                   accomplishment for a new beverage--and I believe the
Coca-Cola International, to manage our business outside of
                                                                   product will be among the North America Group's
North America. The leaders of our international operations
                                                                   top-10 carbonated soft-drink brands in 2006.
reporting to Muhtar are Alex Cummings, Africa; Glenn Jordan,
East, South Asia and Pacific Rim; Dominique Reiniche,
                                                                   In North America, we relaunched Fresca--with new flavors,
European Union; and Jos Octavio Reyes, Latin America.
                                                                   graphics and packaging--driving unit case volume growth
Don Knauss continues to report to me and to lead our North
                                                                   of 16 percent in the last quarter of 2005. Diet Sprite Zero/
America Group. Muhtar also leads the North Asia, Eurasia
                                                                   Sprite Zero also posted impressive unit case volume gains,
and Middle East Group.
                                                                   increasing 16 percent globally. As we began 2006, we
                                                                   launched Black Cherry Vanilla Coca-Cola and Diet Black
This strong leadership team--together with a new orga-
                                                                   Cherry Vanilla Coca-Cola in North America, putting
nizational structure--increases our ability to monitor and
                                                                   Vanilla Coke and Diet Vanilla Coke on hiatus as we cycle in
fine-tune execution in individual markets. These operating
                                                                   new products and effectively manage shelf space. In 2006, we
group leaders are members of our Executive Committee,
                                                                   continue our focus on diet and light products to capitalize on
which is composed of 15 leaders with 264 years of
                                                                   the strong 4 percent unit case volume growth we had in 2005.
combined Coca-Cola system experience--a very real
expression of our commitment to talent development within
our system.                                                        Continue to expand into other profitable segments
                                                                   of the nonalcoholic beverage industry: Globally, our
                                                                   Company significantly outperformed the market in juice and
Under the leadership of Sandy Douglas, our chief customer
                                                                   juice drinks and sports drinks, capturing more than one-third
officer, we are working with our worldwide bottling system
                                                                   of the worldwide growth in juice and juice drinks and more
to significantly improve our ability to create value with and
                                                                   than half of the growth in sports drinks.
for our customers. Our new collaborative customer relation-
ship process has been refined in three lead markets--Japan,
                                                                   We are the largest juice and juice drink company in the
Mexico and Switzerland--and is now being implemented
                                                                   world: Minute Maid is available in 80 countries and unit
with key customers in other markets around the world. In an
                                                                   case volume grew 11 percent in 2005, with share improve-
increasingly complex retail environment, we are working
                                                                   ment in a number of key markets driven by products such as
together with our customers to improve shopper marketing
                                                                   Minute Maid Premium Heart Wise and Minute Maid Premium
and supply chain collaboration and to accelerate innovation
                                                                   Kids+. And in Nigeria, our Five Alive juice brand unit case
in order to provide superior beverage selections to every
                                                                   volume grew 47 percent in 2005.
consumer on every shopping trip.


In 2005, our partnership with Coca-Cola Hellenic Bottling        Re-energize marketing and innovation: In 2005, we
Company S.A. allowed us to jointly purchase Multon, one          appointed Mary Minnick to lead our new Marketing, Strategy
of Russia's leading juice providers, further strengthening our   and Innovation (MSI) Group. Mary brings to the group
presence in an important emerging market.                        considerable marketing and operating experience from her
                                                                 work in Asia, particularly Japan, one of the world's most
Our Company is the leader in sports drinks outside of North      competitive and fastest-changing beverage markets. In 2005,
America. In 2005, Aquarius and POWERADE grew 25 percent          the MSI Group led a comprehensive review of our marketing
and 22 percent worldwide, respectively, and we continued to      programs and identified key opportunities for growth. Work
innovate in both brands, transforming Aquarius beyond being      began on the new global campaign, "The Coke Side of Life,"
considered only a sports drink choice in Spain and Japan         along with a new global digital platform, "iCoke." The MSI
and launching POWERADE Option, a low-calorie sports drink,       Group also led development of the "Make Every Drop Count"
in North America. POWERADE gained 2 share points in North        communications platform, an integrated effort to educate
America in 2005 and is now available in 76 countries.            consumers and opinion leaders on our portfolio, policies and
                                                                 community programs.
We also gained worldwide share in the water category in
2005, with Dasani unit case volume growing 29 percent from       Our message--make the most of what you do and how you
international expansion and flavor extensions as well as         do it--resonates in our efforts to promote health, well-being
strong growth in North America. And the Coca-Cola system         and quality of life through physical activity. Our commitment
became the third-largest energy drink provider in the United     has been expressed again and again in the organizations
States, launching Full Throttle in the first quarter of 2005.    and events to which we contribute and the activities we
With the arrival of Tab Energy and Full Throttle Fury in the     support. In 2005, we renewed our partnerships with the
first quarter of 2006, I believe our momentum in this highly     International Olympic Committee and the Fdration Inter-
profitable category will continue to build.                      nationale de Football Association (FIFA) through 2020 and
                                                                 2022, respectively.
Expand our presence in immediate-consumption
channels: Immediate-consumption unit case volume grew            And in cities and towns throughout Asia, Europe and North
more than 3 percent in 2005. While this was a substantial        America, hundreds of thousands of boys and girls enjoyed
improvement over 2004, it is still well below the long-term      Coca-Colasponsored soccer clinics, tournaments and camps
growth potential for the immediate-consumption channel. As       and the joy that comes from exercise and competition. We
a first step to better meeting that potential, we completed an   also continued our support of programs such as "Thai Kids on
occasion-based map and a global immediate-consumption            the Move," which includes classroom instruction and aerobics.
strategy to target high-growth opportunities. Working with our   More than 395,000 students have participated in this program
bottling partners and suppliers, we also made progress on a      in Thailand and have increased their daily activity and knowl-
new generation of equipment that will provide a more distinct    edge of the benefits of exercise and nutrition.
immediate-consumption beverage experience. And we
identified best practices from some of our best immediate-       Drive increased efficiency and effectiveness: Better
consumption markets--merchandising from Ireland, product         efficiency and effectiveness begins with the alignment of our
innovation from Mexico and routes-to-market from Chile--         system around shared goals and performance targets and
for worldwide standardization. As these initiatives continue     depends on how well we collaborate with our bottling partners.
to progress, I believe profit and volume from immediate-         Last year, we continued to work with our bottling partners to
consumption channels will continue to accelerate.                improve execution of our revenue growth strategy to increase

unit case volume profitability and efficiency around the world.   brand image. We will continue an expansion of our portfolio
Using best practices from Argentina, Brazil and other system      that anticipates and satisfies our consumers' preferences and
models, we refined and improved our revenue growth strategy       needs around the world.
and trained thousands of managers on its implementation.
This strategy has improved our operations in South Africa, for    I want to thank the many people who contributed to our
example, contributing to unit case volume growth of 8 percent     success last year--our system's employees and alumni; our
in 2005.                                                          bottling partners, customers and suppliers; and our consum-
                                                                  ers. I also want to thank our Directors who will not stand
Our Role in Communities: In addition to advancing                 for re-election to our Board in April 2006. Warren Buffett,
profitability and identifying growth opportunities throughout     Maria Elena Lagomasino and Pedro Reinhard have been
our system, we aspire to be the world's most respected            astute leaders and trusted friends of the Company during
company. Creating sustainable growth for our system allows        their tenure as Directors. In particular, I would like to express
us to stimulate economic growth, to strengthen the communi-       my appreciation to Warren for his 17 years of service to
ties where we do business, and to protect and preserve the        our Company. We are proud of his association with us and
planet for future generations. Good corporate citizenship has     Berkshire Hathaway's long and continuing stake in our
been an integral part of who we are for 120 years now, and        Company. We will miss the wise counsel and advice they
it will be even more important going forward.                     have contributed over the years.

Last year, we continued our efforts through our Company's         And, of course, I want to express my deepest appreciation to
18 local foundations and one global organization--The             you, our shareowners. You have kept faith in us, believing in
Coca-Cola Foundation--to fund initiatives in the countries        our tremendous opportunity for future growth and our ability
where we operate, including the reconstruction of schools in      to realize that opportunity. We are working hard to repay
Indonesia; Thailand's new e-Community Learning Center;            that trust and to reward you for your investment.
and HIV/AIDS education and awareness programs in Africa.
Also last year, our system donated more than $15 million to       While we take pride in what we have accomplished to date,
the International Red Cross, CARE and other agencies for          we recognize that our efforts have not yet been evidenced
disaster relief efforts around the world, including the areas     in total shareowner return. However, the path we are on to
affected by Hurricane Katrina, the tsunami in 2004 and the        sustainable growth is the only way to build value for share-
Pakistan earthquake, among others. In those crisis situations,    owners over time. There is much more to do, but I believe we
we donated hundreds of thousands of cases of water,               are worthy of your trust because of the significant progress
juice and juice drinks, and other beverages for victims and       we have made.
relief workers.
                                                                  Sincerely,
Conclusion: In 2006, we will continue to accelerate the
growth that enables us to make a positive difference in the
world by refreshing people every day and inspiring them
with the optimism of our brands, the actions of our Company
and the spirit of our people. We will actively protect and        E. Neville Isdell
enhance the reputation of The Coca-Cola Company and our           Chairman and Chief Executive Officer


