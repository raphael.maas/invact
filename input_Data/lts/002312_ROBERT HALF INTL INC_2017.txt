TO OUR STOCKHOLDERS

Roobert Half�s story in 2017 was
one of first regaining and then
increasing business momentum
as the year unfolded. Worldwide
full-year revenues reached a record level,
slightly exceeding that of the prior year.
By the fourth quarter of 2017, growth had
accelerated noticeably. Late-in-the-year
revenue gains were broad-based, both
geographically and within all three of our
reportable business segments.
Full-year global revenues were $5.27 billion,
compared with $5.25 billion reported
in the prior year. U.S. revenues declined
2.3 percent, and international revenues
grew 11.2 percent in 2017. Performance
improved in the fourth quarter of 2017,
with worldwide revenues up 6.4 percent
during that period. Domestic revenues
increased 2.1 percent, and international
revenues grew 23.8 percent in the fourth
quarter of 2017.

Net income for the full year was $291
million, and diluted net income per
share was $2.33. There were 3 percent
fewer average shares outstanding during
2017, reflecting the continuation of our
long-standing share repurchase program.
Last year�s net income was reduced by
a non-cash estimated amount of $34
million, or the equivalent of $0.27 per
share, due to our provision for income
taxes resulting from the recently enacted
Tax Cuts and Jobs Act (TCJA) in the United
States. Adjusted for this fourth-quarter,
one-time charge, full-year net income per
share was $2.60.* Among other things,
the TCJA includes a lower prospective
corporate tax rate. As a result, the value
of deferred tax assets that had been
recorded on our balance sheet earlier,
when tax rates were higher, had to be
remeasured to reflect the lower rate. The
reduced valuation appears in the income
statement as an increased tax provision.

Last year�s return on invested capital (ROIC)
was 26 percent. Excluding the negative effect
of the aforementioned non-cash charge,
return on invested capital was 29 percent in
2017.* Last year�s ROIC performance compares
favorably with our 20-year average of 25 percent.

ECONOMIC BACKGROUND
The economic backdrop for our business during
2017 was generally favorable. Momentum
seemed to build around much of the globe
as the year progressed. In the U.S., real gross
domestic product (GDP) grew 2.3 percent in
2017, a significant step up from the 1.5 percent
growth reported the prior year.
U.S. labor markets were relatively robust
throughout last year. The economy added
approximately 2.2 million jobs, slightly
fewer than the 2.3 million added in 2016.
Non-farm payrolls grew each month in 2017,
thus sustaining the positive trend we�ve seen for
nearly a decade now. The unemployment rate
in each of the last three months of 2017 was 4.1
percent, the lowest rate since December 2000.
Our staffing operations performed well,
considering these trends. Temporary and
consultant staffing revenues were $4.01 billion
in 2017, or 76 percent of total revenues, and
were essentially flat compared to the prior
year. Permanent placement staffing revenues
were $439 million, accounting for 8 percent
of companywide revenues and representing a
year-to-year increase of 5 percent. Operating
income was $356 million and $77 million for
our temporary and consultant and permanent
placement staffing operations, respectively,
in 2017. Total staffing revenue growth for the
fourth quarter of 2017 was 6 percent on a
reported basis, a considerable improvement
from the negative growth rates posted in each
of the previous three quarters.

Global economic conditions also benefited
Protiviti in 2017. Protiviti�s $817 million in
revenues last year represented 16 percent
of the companywide total and were 2 percent
higher than the prior year on a reported basis.
Last year�s $84 million in operating income
produced a solid double-digit percentage
operating margin for the third consecutive year.
Protiviti�s fourth-quarter revenues increased
6 percent year-to-year on a reported basis,
and its operating profit margin that quarter was
the highest interim margin in 2017.
We launched Protiviti in 2002 as a business
unit of Robert Half after hiring more than 700
professionals from Arthur Andersen�s internal
audit and business risk consulting practices.
Our immediate aim was to provide clients with
expertise in internal audit along with business
and technology consulting. Opportunities
also were emerging as several high-profile
corporate collapses and accounting scandals
led to more regulatory focus on governance
and internal control over financial reporting.
From its outset, Protiviti has generated a
reliable core of recurring revenue by providing
clients with internal audit services on an
outsourced or cosourced basis and by helping
them assess and strengthen internal controls.
Protiviti has grown by providing these core
services and by widening the suite of solutions
it offers in the risk and compliance, technology,
data and analytics, and business performance
improvement consulting areas.
Protiviti also enjoys a key differentiator: It can
combine its highly regarded expertise and
technologies with the considerable strengths
of Robert Half�s traditional staffing operations
to provide managed services solutions to the
clients of the enterprise. Blending the capabilities
of both businesses enables us to provide Big
Four-quality consulting services at competitive
prices. Global and regional consulting firms that
compete with Protiviti generally lack the flexible
resource capabilities of our staffing operations.


Late-in-the-year
revenue gains were
broad-based,
both geographically
and within all three
of our reportable
business segments.

FINANCIAL CONDITION
Robert Half�s financial position remains sound.
Total assets at year-end were $1.9 billion. Our
cash balance of $295 million far exceeded
long-term debt of less than $1 million. Accounts
receivable of $732 million were 4 percent
higher than the prior year-end total. Highly
liquid assets represented 55 percent of the total.
Last year�s receivables increase exceeded
the revenue gain as the year-end balance
reflects faster revenue growth produced in
the final months of the year. We benefit from
the collectability of our receivables due to the
granular nature of our small-to-midsize customer
base. We are free of concentrations � in
customers, industries and geographies. Our
receivables� average days sales outstanding
(DSO), as calculated for the full year at 50.6
days, was consistent with past trends.
The strength of our balance sheet reflects the
outstanding cash-generating characteristics
of our business; we have a long record of
producing generous amounts of cash under
both favorable and unfavorable business
conditions. Last year�s net cash provided by
operating activities was an all-time high of
$453 million.
Capital expenditures in 2017 were $41 million.
Important projects included the completion of
the global rollout of an enhanced customer
relationship management (CRM) system. Now,
for the first time, Robert Half�s global branch
network is united on a single CRM platform. In
addition, we have increased spending to take
advantage of an entire suite of cloud-based
applications offered by our CRM vendor. We
also invested in software that consolidates
into a single human resources platform the
previously separate and diverse systems of
our staffing operations and Protiviti. Besides
spending on these internal tools, we continued
to invest in digital service options for our clients
and job candidates. We discuss more about
our technology investments later in this letter.

The $41 million of capital outlays in 2017
was below the previous year�s $83 million
and the five-year average of $63 million. The
lower amount for capital expenditures reflects
the completion of the multiyear CRM and
human resources software implementations
just described. In addition, new technology
spending weighs more toward internal-use
cloud computing arrangements (CCAs), where
implementation costs are expensed rather
than capitalized. U.S. accounting standard
authorities are currently re-examining the
accounting for CCA implementation costs,
but the timing and outcome are uncertain.
Cash provided by operating activities less cash
used for investing activities (free cash flow) in
2017 was $374 million, and $197 million of
that amount was used to repurchase Robert
Half shares in open-market transactions. We
began to repurchase our shares in 1997 and
have acquired 106 million shares since then.
In the last decade alone, we spent $1.6 billion
of the $2.6 billion of free cash flow generated
by the business to purchase 47 million of our
shares, contributing to a 10-year net reduction
of 24 percent of our outstanding shares. For
perspective, we ended 2017 with 124 million
shares. Recently, our board authorized the
purchase of 10 million shares in addition to the
2.3 million authorized shares already in place.
We have paid a quarterly cash dividend
consistently since 2004. Last year�s $0.24 per
share quarterly payout was equivalent to a
total annual outlay of $121 million. The board
recently increased the dividend to $0.28 per
share per quarter, a 17 percent increase. Since
its initial payment, the dividend has been raised
yearly and has compounded at a 12 percent
average annual rate.
We made no large acquisitions last year, which
is consistent with our well-known preference
to grow organically. Our predisposition for
internal growth is based on the fundamental
belief that our industry provides ample
growth opportunities. The staffing industry is
large, global and growing. It�s also evolving,
with digitalization playing an ever-larger role.
While some companies have chosen to acquire
technology-based services, we have opted to
develop proprietary solutions with the expectation
that our path carries less risk and provides us
with a more durable competitive advantage.

The staffing industry
is large, global
and growing. It�s
also evolving, with
digitalization playing
an ever-larger role.

ONGOING INNOVATION
Our business is evolving. Innovations that are
aimed at digital transformation are at the heart
of the evolution. We recognized early on that
clients and candidates would expect to engage
with a staffing firm online, just as they do with
most other businesses. We have responded
by developing proprietary solutions that give
our customers multiple ways to interact with us
online � from submitting job orders through our
website to browsing for job candidates. We have
invested in strengthening our digital storefront to
enable us to reach new segments of the market.
We continue to evaluate and adapt new online
services that can bring our resources to more
customers. For example, we are using artificial
intelligence and machine learning to help us
make better job matches. These and other new
initiatives support our goal of improving the
service experience for clients and candidates.
Over the years, we have learned that digital
solutions alone are usually inadequate to
make effective job matches. A pivotal part of
the customer experience involves a personal
component. In our case, that experience
includes helping a candidate find meaningful
work or a client hire the right candidate to fill
a critical position.
We frequently have seen competitors emerge
that enter the business pursuing a digital-only
strategy. It usually does not take long for them
to realize that a singular approach often is
inadequate. Human involvement is almost
always critically important to completing a
successful match. In today�s world of tightening
labor markets, success often means convincing
someone with sought-after skills to take your
job over other available opportunities. Robert
Half�s staffing professionals are given extensive
training and development in managing the
many aspects of job placements, including
ensuring candidates have the right skills
and personality to fit the position and job
environment. We also provide technical training
so our employees can fully leverage the many
technology tools available to them. We believe
the combination of personal consultation and
digital service options we offer our clients is a
differentiator for Robert Half.

OUR SERVICE SPECTRUM
We offer a suite of solutions for companies
of all sizes � from small businesses to Fortune
500 firms. The core client base of our staffing
operations is the largest and least-served
segment of the economy � small and midsize
businesses (SMBs). Clients of this size often lack
human resources departments. They are less
able to absorb the costs of a poor hiring fit,
particularly compared to larger organizations
that are more likely to have deep bench strength.
The SMB market segment appears poised
for expansion. The Small Business Optimism
Index published by the National Federation of
Independent Business (NFIB) in February 2018
reached the second-strongest reading in the
45 years the NFIB has conducted the survey.
A key priority of ours is to enlarge our share
of this vast market.
For some time now, we also have been targeting
select larger corporations, those with revenues in
the range of $500 million to $2 billion or higher.
These companies have staffing needs that are
typically more complex than those of SMB
clients, including a desire to bring their staffing
efforts in multiple locations under a single point
of contact. We have found that clients of this
size often have more flexibility than much larger
corporations, and they value our personal,
consultative approach.

LOOKING AHEAD
There are many reasons to be optimistic about
the outlook for Robert Half�s business in 2018
and beyond. Near term, the U.S. labor market
continues to tighten, which typically provides a
boost to our business. For several months now,
the U.S. unemployment rate has remained at
a 17-year low. In addition, the U.S. Department
of Labor in late February reported that jobless
claims were near a 49-year low. It also reported
that, as of the third week of February, claims
remained below the 300,000 threshold for
the 156th consecutive week. This trend held
steady as the longest stretch since 1970, when

the labor market was considerably smaller.
Over the past decade, the civilian labor force
has risen more than 5 percent, while the labor
force participation rate has fallen nearly 5
percent. The trend is even more pronounced
in the population of workers 25 and older with
a college degree. That worker population has
grown 25 percent over the past decade, but
its labor force participation rate has declined
6 percent. Even a slight increase in the labor
force participation rate would have a positive
impact on the pool of available talent.
Global economic growth is increasing. The
International Monetary Fund in its World
Economic Outlook recently raised its global
growth outlook to 3.9 percent to reflect
increased momentum and the expected positive
effect of recent U.S. tax policy changes. The
staffing industry continues to increase its
penetration into worldwide labor forces. Annual
global revenues for the staffing industry now
exceed $400 billion. In the United States,
the percentage of temporary workers in the
overall workforce is 2.04 percent. Higher
concentrations of temporary workers in select
European labor markets suggest there is ample
opportunity for further U.S. growth in the
contingent workforce.
Long-term demographic trends indicate the
already heated competition for talent is likely
to intensify. Skills shortages in occupations
that include our professional specialties are
becoming increasingly acute. The U.S. Bureau
of Labor Statistics (BLS) expects job growth
in the United States for financial analysts,
and accountants and auditors, for example,
to outpace the national average between
2016 and 2026. Furthermore, accountants
and auditors are among the top 20 roles with
the highest projected job growth during this
period. The information technology (IT) staffing
market appears to be even more promising:
It is three to four times larger than accounting
staffing. The IT staffing market continues to
produce robust growth as companies seek
to satisfy appetites for help in business and
technology transformation.

There are many
reasons to be optimistic
about the outlook for
Robert Half�s business
in 2018 and beyond.

We believe we are just beginning to exploit
the opportunities that are arising in the space
where Protiviti meets our staffing resources.
It is fortuitous that many of these opportunities
are surfacing at a time when Protiviti has
expanded its technology consulting practice.
The pervasiveness of mission-critical IT systems
in modern businesses and the pace of
change mean finding the right IT talent is
tremendously important.
Thus far in our extended economic recovery,
wage rate increases have not been proportional
to the low unemployment and candidate
shortages we are seeing day to day. Experience
suggests that tight labor market conditions
eventually will translate into faster growth in
wage rates. In fact, over the past few months
we have seen evidence in the form of more
positive business sentiment, particularly in the
United States, suggesting the situation may be
changing. Clients are taking on more projects,
and there appears to be a greater urgency to
ensure they have the right people to grow their
businesses. As evidence mounts, this should
result in faster wage growth and a premium
for high-demand skills.
Our brands are second to none in our
industry, our finances are strong, and our
field management is seasoned and solid.
We are committed to attracting, motivating
and retaining the best people. In short, we are
very enthusiastic about our prospects. We have
never been better positioned as a company
for future success.
None of these accomplishments would have
been possible without the exemplary efforts
of the people who comprise our teams
across the globe and work hard every day
to provide outstanding service to our clients
and candidates. We thank them for their
continued passion and pursuit of excellence.
We would also like to express our appreciation
to our board of directors for their guidance
and counsel during 2017, and to you, our
stockholders, for your continued support of
Robert Half.
Respectfully submitted,

Harold M. Messmer,? Jr.

M. Keith Waddell

Chairman and
Chief Executive Officer

Vice Chairman, President
and Chief Financial Officer

March 9, 2018