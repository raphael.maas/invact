                                                                                                                                                                             All of our modes and services contrib-
                  




                                                                                                                      To our fellow shareholders,
                                                 




                                                                                                                                                                         uted meaningfully to our strong performance.
                                                                                                                           We are proud to report our solid              Our gross profits grew 14.9 percent in 2007
                                                                                                                      success in 2007. Despite a slowing economy         to $1.2 billion. While that was just shy of our
                                                                                                                      and challenging market conditions, our people      longterm target of 15 percent annual growth,
                                                                                                                      again did a great job generating success and       given the overall market conditions, in some ways
                                                                                                                      I would like to thank them for their tremendous    our achievements this year are more impressive
                                                                                                                      efforts. We believe our results are evidence       than the faster growth we had in recent years.
                                                                                                                      of the longterm view we take of our business
                                                                                                                      and the rewards we believe come from placing          Our growth in 2007 was supported by the
                                                                                                                      service, relationships, and great people at the  foundation that we laid in previous years, and we
                                                                                                                      core of our company.                             continue to invest in our business for the long
                                                                                                                                                                       term, building the platform for our future success.
                                                                                                                                                                       In 2007 we expanded our office network to a
                                                                                                                      Generating success today...
                                                                                                                                                                       total of 218 offices in 21 countries. We added
                                                                                                                          In light of the tougher transportation envi- 564 people, bringing our total employee base
                                                                                                                      ronment, many shareholders have asked how to 7,332 globally. Our total customer relation-
                                                                                                                      we were able to achieve such strong results. ships grew to 29,000 worldwide, and we now
                                                                                                                      Our approach toward the market and growing work with over 48,000 transportation providers
                                                                                                                      our business was very consistent with previous around the world. Including 1.4 million shipments
                                                                                                                      years. Our ongoing commitment to providing we handled in our transportation management
                                                                                                                      excellent service to our customers and carriers, service business, we managed over 6.5 million
                                                                                                                      our diverse mix of modes and services, and our shipments in 2007.
                                                                                                                      motivated employees supported by the right
                                                                                                                      tools and technology all enabled us to meet our       Another key to our success, not only in
                                                                                                                      growth targets and capture more market share.    2007 but for the past several years, has been
                                                                                                                                                                       the diversification of our services. While our
                                                                                                                                                                       primary source of revenue continues to be North

American truckload services, throughout our based compensation enable our people to be
history, and especially in the last several decades, more productive than ever. Our variable incentive
we have worked hard to find new ways to serve plans, including our equity awards which reward
customers and suppliers. We continue to expand growth in profitability, do a great job of balancing
our capabilities within all our truckload offerings, employee and shareholder rewards.
such as growing our flatbed business, while also
developing some specialized service offerings           We also believe that our size relative to
and other modes and services. Consistent with the North American truckload marketplace is
this strategy, in 2007 we acquired a domestic becoming an increasingly important competitive
air freight company, giving us greater expertise advantage. Our contract carrier base represents
to further grow the services we can provide to over one million pieces of equipment, and we
our customers. We think there are more ways manage approximately 25,000 shipments per
that we can help our customers today than ever business day. The larger we become, the more
before, and it gives us a lot of opportunities to options we have for serving our customers.
both grow existing relationships and develop This enables us to reduce inefficiencies in the
new ones.                                            marketplace and provide better service.

     Our variable cost, nonasset based busi-            As an integral part of our longterm per-
ness model continues to be an important             spective, we continue to maintain a strong
advantage in all market environments. Our           balance sheet and financial position. At the end
people managed our business efficiently in          of 2007 we had approximately $455 million in
2007, generating 22.0 percent growth in our         cash and shortterm investments, with no long
operating income, to $509 million for the           term debt. While we understand the need to be
year. Our fully diluted earnings per share also     responsible with any retained capital, we believe
exceeded our longterm growth target with an        that our cash position gives us important flexibility
increase of 21.6 percent to $1.86 per share. We     to manage the business, build relationships, and
believe our decentralized structure, empowered      make strategic investments to continue to grow
employees, technology, and performance             the company. As economic uncertainty continues

                                                                                                        
                                    and economic challenges cause others to have        ...While energized by what's on the horizon...
                                    to scale back, we think we are in a great posi-
                                    tion to continue to invest in our people and our         While we focus primarily on gross
                                    network with new offices and new technologies,      profits as the indicator of the value we bring
                                    to keep looking at acquisitions, to keep offering   to the marketplace, our $7.3 billion in gross
                                    quick payment programs to our carriers, and to      revenues is also an important measure of our
                                    explore other opportunities that help us grow       position in the transportation and logistics
                                    and reinforce our commitments to be successful      industry. The transportation industry is extremely
                                    in all markets.                                     fragmented. Our size makes us one of the largest
                                                                                        third party providers in the world, yet the growth
                                         Our first priority for our cash flow is to re- opportunities still ahead of us are tremendous.
                                    invest in the business, which we continued to Industry market size estimates vary, but by any
                                    do in 2007. In addition, we are always looking measure we still represent a small portion of the
                                    for acquisition opportunities. Although we are hundreds of billions of dollars that are spent on
                                    extremely selective and we do not rely on acqui- transportation and logistics in North America,
                                    sitions to attain our longterm growth targets, we and trillions worldwide.
                                    will acquire high quality companies with strong
                                    people and that are a good cultural fit.                 Key trends continue to drive growth in
                                                                                        third party transportation, making our flexible,
                                         We were also able to return more capital nonasset based model more and more
                                    to our shareholders in 2007, increasing our strategically advantageous to the shipping com-
                                    quarterly dividend over 22 percent to $0.22 munity. Globalization and justintime inventory
                                    per share from $0.18 per share. We have paid practices continue to lengthen and complicate
                                    a regular quarterly dividend for nearly 30 years. supply chains, resulting in greater volatility and
                                    We also increased our share repurchase activity. increased transportation and logistics challenges.
                                                                                        Companies today need visibility, innovation, and
                                                                                        efficiency in their supply chains to be competitive
                                                                                        and manage their business. Increased invest-


ments in technology and information reporting tion network and growing our international freight
are instrumental to effective transportation forwarding business.
management. For all of these reasons, the need
for flexibility, high service, and better information ...And powered by the people of Robinson
are increasing the demand for our services.
                                                           As we illustrate in this report, ener-
      As most of our shareholders know, con- gized people and teams are the force behind
sistent with our longterm approach we do C.H. Robinson's success. We feel very good
not give shortterm guidance. Since becom- about the strength of our company, our service
ing a publicly traded company in 1997, our offerings, our position in the marketplace, and
stated longterm annual growth goal has been our tremendous future growth opportunity. Our
15 percent for gross profits, income from opera- results in 2007 highlighted the momentum we
tions, and earnings per share. We plan to attain have built over many years by providing our
our longterm growth targets by continuing to customers and contract carriers with what we
open new offices and hire people, regardless believe is the best service in the industry. Yet
of the economic environment. We do adjust our even with our successes, we still have to work
rate of hiring, based upon productivity metrics hard daily, load by load, to earn and keep our
and growth rates, but we think it is as good a customers' and carriers' business. Based on the
time as any to continue to invest in our network caliber and commitment of our people, we are as
and our people. We plan to open five to ten new confident and excited as ever about the future.
offices in 2008 and we will continue to look to use
our capital to make the right kind of acquisitions Thank you for your support,
to add new people, new expertise, new services,
and new geographies to the team. We will
continue to focus on executing our longterm
growth strategy of growing our share of the North John P. Wiehoff
American truck market, adding new nonasset Chief Executive Officer and
based services, building our European transporta- Chairman of the Board