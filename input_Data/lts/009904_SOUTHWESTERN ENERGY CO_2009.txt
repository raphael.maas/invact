Late in 2009, I had
the pleasure of participating in the celebration of the fifth
anniversary of our first production from the Fayetteville Shale.
The celebration took place in the auditorium of a small college
in a town near that first producing well. As might be expected,
many state and local dignitaries were in attendance.
The part of the celebration I will remember most occurred after many of the attendees had departed.
The small college where this celebration took place is also the home of a two-year training program
for future oil and gas field workers. Southwestern Energy was instrumental in starting the program and
has benefited by hiring several valuable employees from its graduating classes. The current students
were invited to the festivities, and one of them walked up after the presentations, shook my hand firmly
and calmly stated, Dont let your seat get too warm. Im going to have your job in 20 years!
Certainly a bold, exuberant and youthful declaration! I had to admire his sense of the high
adventure in the career that he was just beginning. When you think about it, our Company has
certainly been bold in our aspirations and we could not have reached our level of achievement
without a similar spirit of high adventure.
That encounter caused me to think about our adventure. Adventure requires a bold purpose.
What purpose could be more inspiring than providing a domestic source of clean and cost effective
energy, especially when Southwestern Energy added over 300 high quality jobs in 2009?
I also thought about how adventure was related to risk. Sometimes unexpected risk creates an
ill-fated quest, but true adventure balances the potential reward with the experience and preparation
needed to handle future uncertainties. Take, for instance, rock climbing. Great climbers dont
take unnecessary risks. They use accumulated experiences in reading both the rocks and the changing
environment around them as they dynamically decide on a safe course to the top of the mountain.
Certainly, 2009 was a time of dynamic challenges for the entire country. Southwestern Energy
was able to navigate the difficult economic times with record results. We drilled our 1,000TH well
in the Fayetteville Shale. The Companys net production increased to more than 1 Bcf per day and
our total reserves, which first eclipsed 2 Tcfe in 2008, grew to 3.7 Tcfe in 2009. All of this was
accomplished with a flat capital budget in a gas pricing environment which was 30% lower than
2008, including hedges.
As for reading the rocks, drilling in the Fayetteville Shale in 2009 revealed the need to drill at
least 10 wells per square mile to optimally develop the field. That spacing provides a future drilling
inventory that will span more than 10 years at the current drilling pace and we are still learning more
about how to drill and produce the shales. Time to drill wells decreased from 14 days in 2008 to
12 days in 2009, despite the average horizontal length increasing by 13%. In addition, 61% of our
2009 wells produced with initial rates greater than 3 MMcf per day compared to only 22% of all
of the wells drilled prior to 2009.
In addition, the rocks in East Texas revealed a new discovery in the Haynesville Shale and
we continued to build our acreage position in the Marcellus Shale in northeast Pennsylvania to
almost 150,000 acres.
2010 will be another year of high adventure. Southwestern Energy plans to invest approximately
$2.1 billion with total net production projected to grow 35% to more than 400 Bcfe. We anticipate
drilling more than 600 wells in the Fayetteville Shale. In East Texas, we will continue delineation
of the James Lime and Haynesville intervals and plan to test Pettet oil and Middle Bossier potential.
In addition, we have begun a multi-year drilling program in Pennsylvania.
Success in any adventure is not along a straight line path and
neither is the climb to shareholder value. It requires a passion
to succeed, an intense attention to the current conditions and an
ever-watchful eye on the goal. Our passion for creating value has
resulted in our 2009 lease operating costs of $0.77 per Mcfe and a
finding and development cost that was also an industry leader of
$0.86 per Mcfe, including revisions.
Those numbers only give the result of the passion, not its
spirit. Like the student in northern Arkansas, all of our employees
are excited about the upcoming adventure. For us our climb to
creating shareholder value is driven by our formula  . We
work daily to be the Right People. We challenge ourselves to do
the Right Things in every aspect of our jobs. Our balance sheet
and budget are designed to be flexible so we can wisely respond to
any future situation and we understand that creating Value+
means living Values+.
I started this letter with a story of a young man who set an example of passion in the early stages
of his career. I couldnt end this letter without mentioning another passionate person who is a role
model both within Southwestern Energy and the entire industry. Harold Korell is the Right Person
who has helped all of us understand how to do the Right Things. Harold recently announced his
decision to retire from the day-to-day operations, but he will continue to provide his wisdom as a
member of the Board of Southwestern Energy. Under Harolds leadership, the Companys shares grew
5,776% over the past decade and was named the top performing stock in the S&P 500 over that time
frame. His example of integrity, enthusiasm, intensity and innovation provides a base camp for all
of us to climb to the next level. Our continued adventure is possible and our goal clearer because of
Harold and his passion to add V+.
You do not need to worry about our seats getting too warm. We are too excited about the
adventure ahead.
Thank you for joining us in this high adventure.

Sincerely,
Steven L. Mueller
President & Chief Executive Officer