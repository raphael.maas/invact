


DEAR  FELLOW SHAREHOLDERS
We are pleased to present to you our Annual Report "Powered by Copper." As a leading global copper producer, we are keenly focused on generating long- term values for shareholders through our large-scale and geographically diverse portfolio of long-lived copper reserves and resources.
The fundamental market outlook for copper is decidedly more positive than it has been in several years, driven by a resurgence in global economic growth and by limited supplies. The acceleration of economic activity across the globe, investments in infrastructure including power grids, and the increasing role of renewable energy will continue to drive growth in global copper demand.
Strengthening demand is occurring following an extended period of underinvestment in our industry. Combined with aging mines and declining ore grades, supply growth is expected to be limited in the coming years. While there are risks to the global economy from geo-political events and actions by governments affecting international trade, market fundamentals are pointing to a consensus of anticipated large market deficits and higher copper prices over the next several years.

Freeport-McMoRan is firmly positioned to benefit from the improved market conditions for copper. Our cash flows are underpinned by strong production capacity and ongoing cost management. We also benefit from  a competitive position of strength with our long-lived reserves and resources, an attractive inventory of potential new low-risk development projects largely focused in the Americas and a highly qualified and proven technical team to execute our strategy.
Our team is actively engaged in reviewing a number of expansion opportunities associated with existing operations in the United States and Chile to enhance net present values through the development of large sulfide mineralization encountered through successful exploration activities. Economic and technical studies
are being developed to provide future options for long- term growth as conditions warrant.
Our 2017 results demonstrate our ongoing focus on productivity, cost and capital discipline, and free cash flow generation. During the year, we generated over
$3 billion in cash flow above our capital expenditures, which was used to further strengthen our balance sheet. By year-end 2017, our debt, net of cash, had declined to
$8.7 billion - approximately 1.5 times our 2017 adjusted


As a first step in recognition of our company's improved financial strength, our Board of Directors reinstated a cash
dividend on our common stock. The first quarterly dividend payment of $0.05 per share will occur on May 1, 2018.

earnings before interest, taxes, depreciation and amortization of
$6 billion. We expect continued deleveraging
in 2018.
We also continue to invest in our business. We

Our results are not only "Powered by Copper" but also "Powered by Great People." Our Board and senior management team appreciate the dedication and professionalism of our over 50,000 employees and contractors. Our workforce's strong focus on safe production and our management's commitment to a safe culture and long-term sustainability in the communities where we operate are essential to our long-term
success. Mining is challenging and involves significant safety risks. Regrettably, five fatalities occurred in 2017 throughout our global operations. We are undeterred in

achieved several key milestones in our development of the Grasberg underground mine to prepare for the transition from open-pit mining during the first half of 2019. We commenced development of the Lone Star project in eastern Arizona to leverage our existing
infrastructure in the region and advance the potential for a larger-scale project in the future.
We continue to progress our discussions with the Indonesian government to address the long-term stability of our Grasberg operations. Resolving this situation in a mutually satisfactory manner is our top priority in 2018, and we are encouraged by recent progress of these important discussions.

our focus on fatality prevention. The health and safety of our employees and their families continues to be our paramount focus.
In closing, we want to thank our shareholders for their support in recent years as we executed our plans to restore our balance sheet strength and our credibility with investors. We have been successful in achieving our balance sheet objectives and our future is bright, "Powered by Copper."

Respectfully yours,

We expect to generate significant cash flows in 2018, which will enable us to continue to strengthen our financial position. Our Board has approved a financial policy that will balance additional deleveraging, disciplined investments and the return of excess cash to shareholders. As a first step in recognition of our company's improved financial strength, our Board reinstated a cash dividend on our common stock. The first quarterly dividend payment of $0.05 per share will occur on May 1, 2018.


GERALD J. FORD
Non-Executive Chairman of the Board


April 2, 2018


RICHARD C. ADKERSON
Vice Chairman of the Board, President and Chief Executive Officer


