Dear fellow shareholders,

in 2011, Raytheon once again delivered solid
operating results. We capitalized on
global market opportunities and successfully managed the challenges of
an evolving economic environment both domestically and internationally.
In this environment, we focused on the things that we could control and
what we do best. We executed our strategy. We continued to implement
efficiencies across the businesses, lowering our costs. We provided more
affordable, innovative solutions to our customers.
This ultimately allowed us to deliver solid returns for
our shareholders. Our margins, earnings and cash flow
were all better than expected, and our bookings and
backlog were strong. For the year, our adjusted earnings
per share from continuing operations was $5.90
compared to $5.51 for the full-year 2010. We had strong
operating cash flow from continuing operations of $2.2
billion in 2011, compared to $1.9 billion in 2010. Both
2011 and 2010 included $750 million in discretionary
cash contributions to the companys pension plans.
We reported strong bookings of $26.6 billion in 2011
compared to $24.4 billion in 2010, resulting in a bookto-
bill ratio of 1.07. We ended 2011 with a backlog
of $35.3 billion, compared to $34.6 billion at the end
of 2010.
Our international business was a bright spot and a key
driver of our 2011 bookings strength. International
bookings represented 29 percent of total company
bookings for the year, demonstrating the continued
success of our international strategy. We have the


largest percentage of international sales among our peer
companies, with sales in 81 countries.
During the year, we continued to execute our balanced
approach to capital deployment. We repurchased 27.1
million shares of common stock for approximately $1.3
billion. We also increased our dividend by 15 percent in
2011  the seventh consecutive year with an increase.
A Well-Aligned Portfolio
Our solid results speak to the execution of our strategy.
With some 8,000 programs and more than 15,000
contracts, Raytheon is well positioned with a portfolio
of depth and breadth spanning the five interconnected
domains of air, land, sea, space and cyberspace. We also
have a focus on delivering capabilities and solutions
that offer the best value.
In 2011, our affordable and innovative solutions were
well aligned with priority areas for both our domestic
and international customers, including Missile Defense,
Electronic Warfare, ISR (Intelligence, Surveillance, and
Reconnaissance), Cyber, Communications and Training.
In Missile Defense, we were awarded contracts to
upgrade Saudi Arabias Patriot Air and Missile Defense
System to the latest Configuration-3 and to provide
three new Patriot fire units for Taiwan. Marking the
first international sale of the product, the United Arab
Emirates ordered two AN/TPY-2 radars  the worlds
most advanced forward-looking, mobile radars and a
key sensing component of the THAAD missile defense
system. We also acquired the assets of Ktech Corporation
as part of our strategy to extend and enhance our
offerings in the effects and electronic warfare markets.
The constantly changing global threat environment
drives demand for our ISR capabilities across military,
border control and humanitarian missions. We also
strengthened our advanced global ISR solutions with
the acquisition of Applied Signal Technology, Inc.
As the electronic warfare market evolves from one
largely aimed at self protection to one encompassing
a broader array of capabilities to include cyber,
signals intelligence, and electronic attack, Raytheon is
well positioned to participate at the forefront of these
trends. In 2011, we had significant bookings across the
spectrum of our EW product line, and continued to
mature the technology required for multiple, near-term
competitive procurements.
In the cyber domain, we continued to grow our position,
and we further enhanced our decades of experience in

cybersecurity, information operations and information
assurance with the acquisitions of Henggeler Computer
Consultants, Inc., and Pikewerks Corporation.
We are broadening our market presence in command
and control, and communications by delivering the
power of the network to global customers in growth
markets like air traffic management and public safety.
We expanded our market-leading training capabilities
into new commercial areas and geographies, including
healthcare throughout the U.S., and vocational training
in Europe and Asia.
Controlling What We Can Control
In a challenging and fluid environment, the Raytheon
team has been focused on the things we can control. This
called for us to deliver more affordable and innovative
solutions while reducing our costs, implementing efficiencies
and operating with improved speed and agility.
With our U.S. government customers being asked to
do more with less, we are working closely with them to
reduce costs, shorten timelines and improve efficiency,
while passing back savings to them.
We have streamlined our supply chain, reduced overhead
and optimized utilization. Our companywide,
integrated enterprise platform of common processes
and systems spans all major functions: Finance, Supply
Chain, Contracts and Human Resources, as well as
Manufacturing and Engineering.
This emphasis on efficiencies is not new. It has been
part of a multiyear strategy and, in 2011, our integrated
enterprise platforms supported more than 85 percent
of our business transactions, supply spend and
manufacturing volume  helping drive our costefficiency
efforts.
With our culture of continuous improvement, these
efforts are never truly done. We will continue to look
for, and drive efficiencies across the company.
Committed to Corporate Responsibility
We also take a long view with our corporate responsibility
commitments  to future generations, to our communities
and to our people.
In 2011, we further strengthened our commitment to
inspiring students toward rewarding careers in science,
technology, engineering and mathematics. Our signature
MathMovesU program has now touched the lives
of more than 3 million students, teachers and parents.
These efforts were bolstered by the engagement of
thousands of Raytheon employee
volunteers, who doubled their
logged volunteer hours over the
previous year.
Our commitment to contemporary corporate governance
and ethics is also steadfast. We continually focus on
good governance to ensure we bring value to our shareholders
in all our actions. Raytheons comprehensive ethics
education program leads the industry and supports
the companys values, reinforcing the highest standards
of ethical conduct in all of our business activities.
In the areas of sustainability and safety, we finished 2011
with the best Environmental Health and Safety performance
in the companys history. Our sustainability
program received more recognition, and we set the
bar higher with some ambitious
new sustainability goals. In addition,
our safety performance was
world class and industry leading.
Our Vision, Strategy,
Goals and Values: Consistent
and Proven
As always, it was Raytheons Vision,
Strategy, Goals and Values that
served as a corner stone for
all we accomplished during the
year. Our VSGVs also are a path
for action  a vital compass to
guide us as we create and embrace
new opportunities.
As we approach the 90th anniversary of our company
in 2012, Raytheon remains strong, and we are well
positioned for continued growth in the future, with a
consistent, proven strategy and a solid financial foundation.
We are focused on the right areas and have a
strong portfolio for this challenging environment.
We are also blessed with the world-class talent of
our 71,000 Raytheon teammates, and their hard
work, dedication and focus on customer success.
United by our VSGVs, I like to think there is nothing
they cannot do.
A proud member of the Raytheon
team for 40 years,
William H. Swanson
Chairman and
Chief Executive Officer
March 2012
