TO OUR FELLOW SHAREHOLDERS: Excellent performance
at The Marmaxx Group, the internal combination of
T.J.Maxx and Marshalls, which represents 70% of our revenues
and 86% of profits, drove our solid results in 2004. Diluted
earnings per share grew by 12%*, on a comparable basis, which
was in line with our expectations.We began 2004 with the stated
goal of improving comparable store sales increases, and we are
pleased to have achieved a 5% increase on a consolidated basis,
which was at the high end of our expectations.While results at
our smaller divisions came in below our expectations in 2004,
we believe we have identified the issues affecting them and are
well poised to improve performance at those businesses in the
year ahead. Overall, we grew square footage by 8% in 2004,
adding 162 stores to end the year with a total of 2,224 stores.

We achieved an after-tax return on average shareholders
equity of 41%, while maintaining an excellent financial position.
Two thousand and four marked the sixth consecutive year
that we have delivered an after-tax return of 40% or higher,
placing us in the top tier of the retail industry.Total sales
increased by 12% to $14.9 billion over the 53-week fiscal
period last year.Net income was $664 million and diluted earnings
per share were $1.30, including the impact of a $.04 per
share one-time, non-cash charge related to lease accounting.
On a 52-week comparable basis and without the lease accounting
charge, this represents a 12%* increase, which was in line
with our expectations.
In 2004, we began the year with a significant cash balance
and generated an additional $1.1 billion from operations.
We reinvested $429 million in our businesses, repurchased
$588 million of TJX stock, and increased our dividend substantially.
We continue to view our significant share repurchase
program as an important method of returning value to shareholders.
Once again,we started the new year of 2005 in an
excellent financial position.
 The Marmaxx
Group had an outstanding year, posting results that underscore
our continued view of this major division as a growth driver
for TJX.This division topped $10 billion in total sales, reaching
$10.5 billion,and segment profit (defined as pre-tax income
before general corporate and net interest expense) surpassed
$1billion for the first time.
A chief goal for this division in 2004 was to drive comparable
store sales, and we are delighted that Marmaxx
delivered a 4% comparable store sales increase for the year.Our
major initiative to expand our jewelry/accessories departments
at T.J.Maxx and footwear departments at Marshalls,which further
differentiates these businesses, outperformed our expectations.
With our increased focus,these categories,across both T.J.Maxx
and Marshalls,posted double-digit comparable store sales increases
in 2004.Womens sportswear was another strong category,as we
benefited from a resurgence of womens fashion trends during
the year.The Marmaxx organization did a superb job of executing
our merchandising and inventory strategies, flowing fresh
product to our stores at the right time for every season.We are
also pleased with this divisions expense management in 2004.
As we continue to bring newness and freshness into our most
established division,we look forward to continued successful
growth for Marmaxx in 2005 and beyond.
Winners and HomeSense, in Canada, continued to
offer great off-price values on apparel and home fashions.
Winners had a very solid first half of the year, fueled by strong
trends in womens fashions, followed by a disappointing
second half and finish to 2004. Entering the second half of
2004 with exceptionally high comparable store sales increases,
Winners overcommitted in its buying.When the Canadian
retail environment turned very promotional,Winners took
aggressive markdowns to clear inventory.We have implemented
improvements to both the distribution center network and
planning and allocation area to ensure more effective inventory
management.
HomeSense, which we launched in 2001 to bring the
off-price concept to home fashions in Canada, is that countrys
only off-price home fashions chain.HomeSense had a good year
and continued to expand its store base into new markets. In
addition to our HomeSense standalone format, our Canadian
superstores, which combine a Winners and HomeSense, performed
exceptionally well in 2004. Our Winners division
continues to be a strong business, with excellent returns on
investment,and we believe we are poised to improve results in
Canada in 2005.
T.K.Maxx, the off-price leader in the U.K.and Ireland,had
a solid year.Total sales surpassed $1 billion for the first time, in
2004,the same year that this division celebrated its 10th anniversary
in business.T.K.Maxx delivered admirable results despite the
difficult retail environment that prevailed in the U.K. throughout
the year, with unusually harsh weather during the first half
and one of the most promotional holiday selling seasons in
many years.T.K.Maxx remained extremely disciplined in managing
its inventories and expenses, and significantly grew its
segment profit.T.K. Maxx has done an excellent job of capitalizing
upon various types of real estate opportunities, and we
believe that the development of retail centers in city locations,
where our stores are typically most productive,bodes well for our
plans to grow T.K. Maxx in 2005 and beyond.
HomeGoods is another concept that reached the
$1-billion mark in total sales in 2004. However, we were
disappointed with this divisions results for the year.While
there was a general malaise in home fashions in 2004, we
did not execute our merchandising strategies as well as we
could have at this division. In the early part of the year,we
missed certain opportunities in seasonal categories.We also
moved our merchandise mix to be more upscale, a strategy
that had worked very well for us until this year, when we
took that strategy a bit too far against a weak environment
for home fashions. As we begin a new year, we have
addressed these merchandising issues and we are seeing
positive customer response to our more balanced offerings.
We have continued confidence in our HomeGoods
concept, which, with its great, off-price values on rapidly
turning merchandise assortments sourced from around the
world, remains truly unique in home fashions retailing.
A.J.Wright had a strong start to the year,but with soft sales
for the balance of the year,top- and bottom-line results for 2004
came in below our expectations.A.J.Wrights performance was
hurt by a combination of a spike in gas prices, affecting its moderate-
income customer base, coupled with a softening of
demand for urban fashion, which had been benefiting the business.
A.J.Wright managed admirably through this fashion
reversal by modifying its merchandise mix, and sales began
improving in the third quarter.However, in retrospect,we realize
that when we opened 16 stores in the late fall,we put added
pressure on the organization,which contributed to A.J.Wrights
weak finish for the year.To give this business time to catch its
breath, we have slowed down its aggressive growth plan and
expect to open 25 stores in 2005, with no openings scheduled
for the fourth quarter.We also are looking at ways to reduce
the cost structure throughout this business and are further
conducting market analysis of this moderate-income customer,
to learn how to better serve her.We believe that we are
taking the necessary steps to ensure the success of A.J.Wright,
and, with its huge, moderate-income customer demographic,
that it holds great potential for the future of our Company.
Bobs Stores made solid progress in its first full year as a
TJX division.We significantly strengthened the Bobs merchant
organization, repositioned promotional activity, improved
inventory management, fine-tuned product assortment, and
tested a smaller store size. Our plan is to grow Bobs Stores
slowly and deliberately, taking the time to get the concept right
before we roll it out aggressively. Long term, we view Bobs
Stores as a significant growth opportunity for TJX.
: At TJX, succession
planning is a top priority, and the collective experience of our
management team in off-price and at TJX, which can be
measured in centuries, gives us confidence that we have the
right team in place to support our goals for growth. In 2004,
Ernie Herrman was promoted to succeed Carol Meyrowitz as
President of The Marmaxx Group from his position as Chief
Operating Officer of that division.We extend our sincere
gratitude to Carol for her many years of tremendous service
to our Company and our very best wishes for her future success.
Ernie brings 15 years of merchandising experience and
broad leadership to his new post at Marmaxx as we pursue the
many exciting growth opportunities that lie ahead for our
largest division. Succeeding Ernie as Chief Operating Officer
of Marmaxx is Jerome Rossi, who had been President of
HomeGoods since 2000.With this appointment,we are tapping
Jerrys previous and extensive operational experience
with the Marmaxx organization.We are indeed fortunate to
have a seasoned Group President in Alex Smith,who provides
continuity and leadership for both Marmaxx and HomeGoods.
In January 2005, Richard Lesser, who served in the role
of Senior Corporate Advisor since 2001, retired. Dick has
been an integral part of the culture,growth and success of TJX
for the past quarter of a century.He was promoted to President
of T.J. Maxx in 1986 and led the The Marmaxx Group as
President from 1995 to 2001.We are delighted that Dick
continues as a member of our Board of Directors, and look forward
to his continued, valued involvement in that role.
We were deeply saddened by Stanley Feldbergs passing.
A founder of our predecessor company,Zayre Corp., in 1956,
Stanley served as its President until 1978 and remained with
the Company as a Director until 1989. He was a member of
various corporate and nonprofit boards and was a great philanthropist.
Regularly attending our annual meetings with his wife,
Theodora, up until the time of his death, Stanley will be
missed by all who knew him.
CONFIDENCE IN FUTURE GROWTH : We made
a commitment at the outset of 2004 to embark in new directions
and drive comparable store sales growth. A year later,we
are pleased to have delivered, achieving our highest consolidated
comparable store sales increase in the last five years. In
addition to expanding categories at our Marmaxx,Winners and
T.K. Maxx divisions, we entered the online retail arena,
launching e-commerce sites for our T.J.Maxx and HomeGoods
divisions.While these e-commerce sites represent a small
piece of our business today,we believe the online channel will
become increasingly important to TJX in the long term. In
sum,we are confident that The Marmaxx Group will continue
to be a major growth driver for TJX and, although we were
disappointed with the results of our smaller businesses,we are
also confident that the lessons learned in 2004 create opportunities
to improve performance in 2005 and beyond.We have
significant growth potential at every division of the Company
and continue to target a 15% compound annual growth rate
for earnings per share over the long term.We plan to grow
selling square footage by 8% in 2005 and to net an additional
161 stores, and ultimately grow to over 4,400 stores with our
current portfolio of businesses. Finally, our very strong financial
position continues to serve as a foundation upon which
to grow in 2005 and beyond.
We thank all of our dedicated Associates, who now
number approximately 113,000, our customers, our vendors,
other business associates, and our fellow shareholders for their
ongoing support.

Respectfully,
Bernard Cammarata
Chairman of the Board
Edmond J. English
President and
Chief Executive Officer