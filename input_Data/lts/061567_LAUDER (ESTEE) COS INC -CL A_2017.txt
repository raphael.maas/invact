Dear Fellow Stockholders,
The Est�e Lauder Companies achieved another year of strong financial performance in fiscal 2017,
demonstrating the strength of our winning strategy of multiple engines of growth fueled by our
diversified portfolio of brands. This is the eighth consecutive year we generated strong organic
growth and outperformed the industry. We proved that we can pivot our business, execute with
speed and excellence, and make courageous choices that create momentum for the coming
years. As The Global House of Prestige Beauty, we aim to exceed our consumers� expectations,
and this drive for excellence permeates all that we do.
In fiscal 2017, we delivered net sales of $11.82 billion, net earnings of $1.25 billion and diluted
earnings per share of $3.35. In constant currency, net sales rose 7 percent, which was ahead
of global prestige beauty growth, and adjusted constant currency diluted earnings per share
increased 11 percent.* During fiscal 2017, we returned $899 million to our stockholders through
dividends and stock repurchases; we increased our common stock dividend 13 percent and
repurchased 4.7 million shares of our Class A Common Stock.
We achieved these results against a backdrop of geopolitical and economic challenges.
Declining foot traffic in some brick and mortar U.S. department stores, particularly in smaller
malls and certain tourist-driven doors, weakness in international markets such as the Middle East,
geopolitical tensions and currency volatility are just some of the headwinds that we faced. Once
again, we demonstrated the resilience and sustainability of our strategy and our ability to execute
with excellence. Our overall performance helped us gain share and strengthened our leadership
in global prestige beauty.

Throughout fiscal 2017, we remained focused on long-term value creation. We continue to be
guided by our 10-Year Compass, an important tool that identifies our most promising growth areas
and the future trends we believe will drive growth in prestige beauty. We drew upon our enduring
strengths � distinct brand equities, superior product quality, engaging consumer communication
and High-Touch service � and pivoted towards those areas of prestige beauty where we saw
the greatest opportunities. This broadened our reach across fast-growing channels, younger and
more diverse consumer groups, and in new emerging markets. In the process, we demonstrated
our ability to address changes while positioning our Company for sustainable growth.
GLOBAL PRESTIGE BEAUTY:
DYNAMIC AND GROWING
Beauty is an industry that has been growing steadily for many years, outpacing most other
household and personal care sectors. Within our industry, we�ve kept our focus solely on prestige
beauty, which means delivering quality products, consumer education, customization and High-
Touch service. Global prestige beauty has grown approximately 4 to 5 percent a year, and we
believe our goal of exceeding that pace by at least 1 percent annually continues to be attainable
under our strategy.
With a desire to always be camera-ready, consumers� appetites for beauty products is intensifying,
particularly in the luxury arena. The playing field for high-end cosmetics continues to expand
as the barriers to entry have come down, largely due to digital commerce and social media.
Awareness and sampling of new products is made easier as a result of these new platforms, yet
it is the repeat purchase that is a true sign of success. We recognize the staying power of �hero�
products � iconic products of the highest-quality that are the backbone of our brands. We are
confident that with our mix of innovative new products and services, our coveted collection of
bestsellers, and our digital-first mindset, we are well-positioned to sustain strong and profitable
growth over the long-term.
MULTIPLE ENGINES OF GROWTH
Our long-term strategy is working well as we are responding to new consumer preferences while
identifying new opportunities to fuel growth in our brands, categories, geographies and channels
of distribution. In a world where volatility and change is the new normal, these multiple engines
of growth are driving our business performance and reinforcing our leadership position.
ENRICHING OUR PORTFOLIO
Our portfolio strategy includes growing existing brands and discovering and acquiring highpotential
brands that align with our values and complement our other brands. This past year
we welcomed two exciting additions to The Est�e Lauder Companies� family of brands, BECCA
and Too Faced. Key considerations for us in these acquisitions were the brands� entrepreneurial
approaches to beauty, creative spirits, and distinct strengths in specialty-multi, social media, and
with Millennial consumers. We also made a minority investment in DECIEM, a fast-growing
multi-brand beauty company with a nimble, consumer-centric focus.

Our mid-sized luxury brands each experienced strong double-digit growth this fiscal year.
Tom Ford�s innovative Lips & Boys collection grew exponentially and helped the brand gain
share in the prestige lip sub-category, while the brand overall captured the hearts of the Chinese
consumer by strategically engaging social media influencers and accelerating its Travel Retail
distribution. Jo Malone London had numerous successful product launches, including Star
Magnolia Hair Mist, a new product form for the brand. La Mer�s introduction of the Soft Fluid
Long Wear Foundation, infused with its Miracle Broth, exceeded expectations and is helping to
attract new, younger consumers to the brand.
Among our large brands, Est�e Lauder achieved a solid return to growth in fiscal 2017, with
higher sales in skin care and makeup compared to the prior year. The brand was well-positioned
to capitalize on increased travel and consumption from Chinese consumers, and its makeup
sales excelled internationally. Est�e Lauder continued to support two of its hero franchises,
Advanced Night Repair and Double Wear, with new products and more effective marketing and
communications. The investment paid off with these two highly desirable product lines reaching
new consumers and posting positive growth in every region. Double Wear is now the best-selling
prestige foundation in many markets, and it attracts consumers across generations to the brand,
including Millennials.
Clinique is reaching new consumers and building on its authority in skin care to drive growth.
The brand accelerated distribution in North America specialty-multi, both in-store and online,
and with high-growth, brand-building channels and retailers internationally. The global launch
of Clinique�s Fresh Pressed Daily Booster with Pure Vitamin C 10% was highly successful and
is having a halo effect, increasing sales of the brand�s core moisturizing business because the
product is used in tandem with Moisture Surge and Even Better Moisturizer.
MAC remains the number one prestige makeup brand in the world and continues to increase
consumer reach globally through targeted distribution and increased social media outreach.
The brand grew well internationally, but experienced weakness in the U.S., attributed, in part, to
its overexposure in mid-tier department stores. MAC had a successful fourth quarter launch in
fast-growing Ulta Beauty and on Tmall, a digital platform in China. The brand�s collaborations
with locally relevant social media influencers in various countries also generated high consumer
engagement and positive sales trends. In fact, MAC is mobilizing its global team of 20,000
makeup artists to engage consumers with impactful social media content, using influencer and
user-generated posts to augment its storytelling.
POWERFUL GROWTH IN MAKEUP
Staying ahead of the curve is critical to our success. The desire for instant, camera-ready beauty
is driving growth in makeup, not only with Millennials and Generation Z, but across demographic
groups around the world. We continued to see strong organic growth in this category through
the performance of Est�e Lauder, Tom Ford, and Smashbox, and we added incremental sales
from our recently acquired brands, Too Faced and BECCA. MAC and Bobbi Brown performed
well internationally. One of the most anticipated and successful launches in MAC�s history was
the introduction of MAC Selena, celebrating the life of the late Latina singer Selena Quintanilla.
Bobbi Brown�s Art Stick Liquid Lip became the U.K.�s number one color launch in brand history
and, in Asia, helped drive over 75 percent growth in the brand�s lip business in its launch month.

NEW GROWTH ENGINE IN LUXURY FRAGRANCE
Artisanal and luxury fragrances have emerged as a profitable new growth engine for the
Company. We began cultivating this sub-category a number of years ago based on our strategic
Compass insights. We recognized a growing trend in customization and Millennials� interest in
individuality. We set out to create a more personalized fragrance journey for consumers, which
was a tremendous opportunity for existing brands like Jo Malone London, Tom Ford, and AERIN.
It also encouraged us to identify and acquire brands that emphasize craftsmanship and the art
of fragrance, such as Le Labo, Editions de Parfums Fr�d�ric Malle, and By Kilian. This strategy is
paying off, as our ultra-prestige fragrance portfolio experienced double-digit growth in fiscal 2017,
a trend we expect to continue in fiscal 2018.
REIGNITION OF SKIN CARE
The demand for skin care products is on the rise, and we anticipate it will continue to grow
as Millennials age. We are pleased to see an acceleration of sales growth in this category, as
a result of the resurgence in Chinese demand for Est�e Lauder and La Mer products, as well
as an improved retail strategy for Darphin. We also saw outstanding double-digit growth from
GLAMGLOW through additional product assortments and targeted expanded consumer reach.
Several other brands, including Bobbi Brown, Origins, and Aveda, each posted solid gains in the
category during fiscal 2017.
MOMENTUM AROUND THE GLOBE
Geographically, we benefited from growth in each region and in some key emerging markets.
And, we are pleased to report that our sales in more than a quarter of our markets grew by
double-digits this fiscal year.
Asia/Pacific (APAC) growth accelerated, up 9 percent in constant currency, driven by a resurgence
of our business in China as well as solid sales growth in Japan, Korea, Taiwan, and Indonesia.
Selfie culture, which is thriving in APAC, has revolutionized the way consumers shop and what
they are buying, with makeup growing three to four times faster than skin care. We have been
able to capitalize on this trend with our leading makeup brand portfolio and strategic initiatives
like the launch of MAC on Tmall in the fourth quarter, considered the most successful launch in
Tmall prestige beauty history. Our luxury fragrances Jo Malone London and Tom Ford drove the
growth of the fragrance category in the region.
Our business in Europe, the Middle East & Africa (EMEA) performed strongly with sales rising
10 percent in constant currency over the prior year. Virtually all countries generated sales gains,
from both developed countries like Italy, to emerging markets like Russia. Among the many
successful programs, we expanded our consumer loyalty initiatives to help move consumers
from trial to replenishment, and build brand loyalty among shifting consumer preferences across
our products, brands, and channels. The region also benefited from accelerations in Travel Retail
and online. Online net sales in EMEA grew 32 percent versus last year as we focused on e/mcommerce,
digital, and social media efforts to reach and engage our consumers. Our success
in the region is notable considering headwinds caused by uncertainties surrounding Brexit and
macroeconomic conditions in the Middle East, as well as challenges from increased competition
in makeup. Est�e Lauder�s Double Wear and Advanced Night Repair franchises and Clinique�s
Moisture Surge achieved double-digit growth, proving that our hero products remain strong
engines of growth.

Our sales in the Americas grew 2 percent in constant currency this year, with strong growth
coming from markets in Latin America, including Argentina, Chile, and Peru. Many of our brands
generated strong growth in the region; notably, Tom Ford, Smashbox, and Jo Malone London
all had double-digit gains, and La Mer also performed well. U.S. department stores represented
approximately 17 percent of our global sales last year, and we are encouraged by new signs of
strength in some retailers� top doors. We are working with department stores to improve our
merchandising, service, and communications activities, and to boost their online businesses
which have been healthier.
BUILDING AWARENESS IN EMERGING MARKETS
We continue to build our presence in emerging markets, bringing our brands and high-quality
products to new consumers around the world. Net sales in emerging markets rose 11 percent
in constant currency, with the strongest growth from China, Russia, and Mexico, offsetting
weaker results from the Middle East and Brazil, markets that have been impacted by economic
and political factors. The Est�e Lauder brand significantly lifted its sales in China by leveraging
outstanding products with a combination of in-store and social media campaigns, particularly
around the spokesmodel signing of Yang Mi, famed Chinese actress and beauty icon. Est�e
Lauder China also achieved the number one position �and the only Genius spot� in the L2
Digital IQ Index: Beauty China 2017. Mexico�s e-commerce business grew triple digits this year,
and creative initiatives with Mexican social media influencers for Clinique and MAC were
instrumental in engaging consumers.
REACHING CONSUMERS
IN FAST-GROWING CHANNELS
Our Company has built the most desirable portfolio of prestige brands in the industry, and
we are known for honoring each of our brand�s distinctive qualities when evaluating market
opportunities. We have always been deliberate about operating in channels that are brandbuilders,
and we are actively deploying our brands across the fastest-growing prestige channels
and consumer segments around the world.
INCREASED COVERAGE IN SPECIALTY-MULTI
Channels representing nearly one-third of our sales are growing double-digits, including specialtymulti,
online, and Travel Retail. Specialty-multi has been an effective channel for attracting new
consumers globally, especially Millennials. During fiscal 2017, Jo Malone London created a retail
format and business model to enter the specialty-multi channel through Sephora, where it has
been very well received. In Latin America, multiple brands also launched in Sephora, including
Tom Ford and La Mer, while brands such as Clinique, MAC, and Smashbox continue to have
strong sales in-store and online. MAC launched in Ulta Beauty in the fourth quarter, where it
is being introduced to new consumers. In Asia, as more high-growth, brand-building specialtymulti
retailers emerge, our brands are capturing this opportunity. For example, Clinique was our
first brand in Eve and Boy in Thailand and in Olive Young in Korea.

ONLINE SUCCESS
Fiscal 2017 was another excellent year for our online business, which delivered accelerated growth
of 33 percent in constant currency. Over the past five years, our online sales, which include sales
on our own sites and sales on our authorized retailers� sites, have more than doubled and now
account for 11 percent of our total net sales. The U.S. and the U.K. remain our two largest online
markets, accounting for approximately 70 percent of our global online business. China, our third
largest market, nearly doubled online sales in fiscal 2017. Bright spots included the successful
introduction of MAC and Lab Series on China�s Tmall, and the surge in sales across our brands
on Tmall on Singles Day, where we were the beauty category winner. We also made good
progress online in other emerging markets. We recently launched in India and plan to roll out in
the Middle East and Southeast Asia in fiscal 2018.
NEW
LaMer.com
HEALED BY
THE SEA
Continuous Moisture.
Softens Age Away.
Introducing the
Moisturizing Soft Lotion.
The original secret of
the sea in a fluid new texture.
A continuous wave of
moisture and Miracle Broth�
help heal dryness
and soften signs of aging.
17
Our efforts in support of retailer.com sites also proved fruitful as we helped our authorized
retailers win with our brands online. In fact, many of our brands, particularly Est�e Lauder,
Clinique, and MAC, had an outstanding year on Macys.com. Mobile commerce continues to
drive our results, as it provides consumers with an always-connected and always-on channel.
Currently, two-thirds of our online traffic and nearly half of our online sales come from mobile
devices. We expect this trend to accelerate and therefore, we are expanding our mobile team
and increasing investments in mobile innovation in order to provide our unique High-Touch
services and experiences to consumers. As our Online team likes to say, access to a beauty
expert is as easy as reaching for your phone.
We have also been strategically focused on designing a seamless omnichannel experience across
our in-store and online direct-to-consumer channels, making it easier for consumers to buy what
they want, when and where they want it. Omnichannel initiatives to help enhance this crosschannel
consumer journey have included services like click and collect, buy online/return instore,
and order online from in-store. We also developed new loyalty programs for a number of
our brands in the U.S., Canada, Korea, and the U.K. Additionally, we advanced the concept of
delivering High-Touch service online. For example, we launched the ability to text gift cards in
real time and have developed chat bots to drive online appointments and innovative gifting with
our pop-up store in the U.K.
GROWTH IN TRAVEL RETAIL
International passenger traffic growth was solid in fiscal 2017, helping to accelerate the Travel
Retail channel, where our sales grew 22 percent in constant currency. We are expanding more of
our portfolio into additional airports and are finding success with digital acceleration that enables
pre-order sales, as well as with new store formats such as our Lip Studio in Shanghai Pudong
Airport. Our expanded offerings in luxury fragrance performed very well this year, including
products from Le Labo as well as Jo Malone London, particularly its Blossom Belle collection that
launched exclusively in Travel Retail. Tom Ford achieved record growth in Travel Retail, winning
with traveling Chinese consumers in both makeup and fragrance. Looking forward, we expect
higher passenger traffic, coupled with effective marketing, to increase conversion, leading to
strong sales growth again in fiscal 2018.
THE BEST IN CONSUMER ENGAGEMENT
Our Company was founded by Mrs. Est�e Lauder, an incredible woman who was known for
making instant connections with consumers through her commitment to provide individual
attention, whether personally applying a cream or a lipstick to demonstrate its benefits, offering
a quick lesson in skin care or demonstrating a three-minute beauty regimen. High-Touch service
is in our DNA, and we have evolved this concept through the years, giving us a competitive
advantage in addressing today�s consumer wants and needs. High-Touch in the digital age
includes leveraging technology and social media at all touch points in the consumer journey.
Our brands are using new social media strategies and memorable 360� marketing campaigns
to drive awareness, trial, purchase, and loyalty, and they are using digital tools to provide realtime
analytics and feedback to understand what is resonating. Partnerships with social media influencers are integral to our digital marketing efforts across brands � these efforts have led
to increased share in earned media value (EMV). In fact, we ended fiscal 2017 as the number
one prestige beauty company in the U.S. in EMV according to Tribe Dynamics. Online how-to
content continues to be an effective engagement technique for brands like Bobbi Brown, the
top beauty brand on Facebook Live, where the brand hosts weekly demonstrations, reaching
as many as 400,000 fans. Aveda has leveraged the professional authority of Aveda Artists and
social-savvy influencers to create on-trend, shareable tutorial content, positioning the brand
as a trusted source for �do-it-yourself� Millennial consumers. Jo Malone London introduced
�The Talk of the Townhouse,� a magazine and digital editorial platform offering consumers
original content designed to build brand equity, drive sales and elevate the brand�s unique
boutique positioning, boosting its brand storytelling and voice along the lines of �Est�e Stories�
and Clinique�s �The Wink.�
STRENGTHENING OUR CAPABILITIES
In fiscal 2016, we announced the launch of Leading Beauty Forward, a multi-year initiative to
build on our strengths and better leverage our cost structure to free resources for investment to
continue sustaining our strong growth momentum. This initiative is designed to fuel our longterm,
profitable growth, enhance our go-to-market capabilities, and reinforce our leadership
position in global prestige beauty.
Leading Beauty Forward is on track to help us increase our effectiveness by infusing additional
speed and agility into our business, so that we can more quickly respond to our changing industry.
We�ve begun establishing more efficient structures in certain areas of the Company, including
some of our regional organizations and corporate functions, and redesigned our procurement
organization to generate further savings opportunities. These efforts have already enabled us
to begin accelerating the changes we need to support stronger digital consumer engagement,
enable greater cost leverage, and more aggressively reallocate resources into stronger growth
drivers.
Leading Beauty Forward is strengthening our capabilities in critical growth areas while designing
a lower cost organization for greater net sales leverage. Combined with our more disciplined cost
management efforts, we are helping to drive strong topline growth by providing more resources
to fuel our business.
CORE STRENGTHS
While our industry has been evolving, there are still certain fundamentals that hold true. We have
responded to changing consumer preferences and will continue to leverage our core strengths.
These include: high-quality products and authentic brands that engender loyalty and repeat
purchase; the nurturing of hero products and franchises; and High-Touch service and experiences
in distribution channels committed to brand-building and to excellent consumer engagement.
In an arena marked by incredible variety, where consumers are constantly flooded with options,
creativity is still a differentiator that sets great brands apart. To be noticed, we must be daring
with our products, business models, and in our communications. Creativity also means making
the most of new analytics that technology provides, connecting the market data and consumer
insights in ways that are unique and illuminating. 

There are a number of ways we are committed to cultivating creativity. Diversity of gender,
ethnicity, nationality and experience bring together different perspectives. Fostering an inclusive
and diverse workforce is paramount to generating fresh ideas. We are inspired by the various
generations represented among our employees. Our reverse mentoring program is a rich source
of innovation, and our Millennial Advisory Board is helping us discover and implement new ways
to reach consumers.
We also benefit tremendously from the entrepreneurial spirit of new, innovative companies.
When we have the privilege to acquire a new brand, our goal is to help it be the best and to learn
from it in the process. Our model isn�t focused on assimilation. Rather, it�s a model of listening
and providing a way for our talent to create and succeed.

PIVOTING TO TOMORROW
As The Global House of Prestige Beauty with a 70-year heritage of entrepreneurial success,
we have a solid foundation to build upon for the future. The art of leading through change is
understanding what has not changed�and how to leverage our historical strengths in new and
exciting ways.
We are winning in brand-building, high-growth channels, including online, specialty-multi, and
Travel Retail. We have a coveted and diverse portfolio of prestige brands that we are leveraging
across regions and channels. Our resources are being deployed in growing geographies as we
focus on the biggest opportunities to reach new consumers. We are communicating directly with
consumers in new ways, and applying our digital-first mindset through social media, influencers,
and via new digital/online technologies. We continue to streamline our operations to increase
efficiency and agility, positioning us for greater profitability.
We expect the great momentum we built throughout the past year to continue in fiscal 2018.
Our full-year outlook in constant currency reflects net sales growth of 7 to 8 percent, including
incremental sales from our fiscal 2017 acquisitions, and double-digit adjusted earnings per share
growth. Looking out over the next three years, we continue to target constant currency net sales
growth of 6 to 8 percent and double-digit adjusted EPS growth.

OUR WORLD-CLASS TEAM
Our talented and dedicated employees drive our Company�s achievements by bringing their
passion for beauty, innovation, and success to every aspect of our business. Our Executive
Leadership Team brings an extraordinary mix of industry experience, entrepreneurial spirit,
and commitment to win in this dynamic and highly competitive environment. We announced
a number of strategic leadership appointments this past year including the internal promotion
of talented employees into senior roles, as well as the recruitment of future leaders into the
Company. We are particularly proud that women represent the majority of our global workforce
and senior leadership positions. As we continue to build our world-class team, our focus remains
on inclusion and diversity, and education and leadership development at all levels. Our results in
fiscal 2017 would not be possible without the brilliant execution by our people around the globe,
and we thank them for their hard work and commitment.
We are also grateful to the members of our Board of Directors for their wise counsel and their
guidance and support of our strategic focus on long-term value creation. On behalf of our entire
senior leadership team, I thank the Board for the invaluable advice and oversight as we position
the Company for continued success.
As a family-controlled company, we derive tremendous benefits from the Lauder family�s
dedication to preserving the legacy of our Company while inspiring us to innovate. It is
my privilege to partner with William P. Lauder, whose leadership, commitment to ensuring
long-term sustainability, and dedication to cultivating the leaders of the future are integral to our
Company�s performance.
And importantly, I thank you, our valued stockholders, for your continued support and confidence
as we look ahead to great achievements in fiscal 2018 and beyond.

Fabrizio Freda
President and Chief Executive Officer