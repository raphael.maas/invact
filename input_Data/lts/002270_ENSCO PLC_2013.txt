Dear Fellow Shareholders:
Enscos vision is that we will go beyond what is expected, and our performance in 2013 lived up to that commitment.
We set a new benchmark for safety, maintained our #1 customer satisfaction rating and achieved record revenues and
earnings.
Safety
We continue to advance toward our goal of zero incidents. In 2013, we recorded our lowest-ever total recordable
incident rate (TRIR), an industry safety metric, at 0.39. This TRIR represents a dramatic improvement in our recordable
incident rate over the past several years.
In the North Sea, we were awarded first place for safety among large jackup operators by the International Association
of Drilling Contractors (IADC). Also, in the Middle East and Africa business unit, our jackup rigs achieved a
remarkable safety milestone  operating an entire year without a single recordable incident.
Our positive momentum toward improving our incident rate has continued in 2014. We will continue to invest in
our safety management systems and training programs as we continue on our journey to achieve a safe zero-incident
workplace.
Operational Excellence
Our seven ENSCO 8500 Series ultra-deepwater semisubmersible rigs illustrate the advantages of standardization on a
differentiated rig design. The 8500 Series rigs have consistently achieved high uptime performance and extraordinary
levels of customer satisfaction, evident in the number of repeat customers. For 2013, these rigs averaged 95%
operational utilization, and several made news during the year:
 In March, ENSCO 8506 drilled Anadarkos successful Shenandoah-2 well, which they described as one of its
largest oil discoveries in the Gulf of Mexico. Anadarko also noted in its announcement that the Shenandoah Basin
has the potential to become one of the most prolific new areas in the deepwater Gulf of Mexico.
 In May, ENSCO 8502 hosted the U.S. Secretary of the Interior and the Director of the Bureau of Safety and
Environmental Enforcement (BSEE), on the Secretarys first offshore rig visit.
 In July, ENSCO 8503 drilled to more than 36,000 feet for Cobalt International, setting a new total depth record
for the U.S. Gulf of Mexico. Cobalts North Platte #1 deepwater discovery, also drilled by ENSCO 8503, was
recognized as one of the best new discoveries by Oil and Gas Investor magazine.
Other rigs in our global fleet also received special recognition from customers. PEMEX rated Ensco #1 among
all of their offshore drilling contractors for our four jackups in Mexico. ENSCO 82, in the U.S. Gulf of Mexico,
received the Energy XXI Distinguished Contractor Award for stellar safety performance, minimal downtime and
zero findings during a recent BSEE Safety and Environmental Management Systems audit. ENSCO 76, the first rig to
commence drilling operations for Saudi Aramco in the Red Sea, was visited by Saudi Aramcos President and CEO to
congratulate the Aramco and Ensco team for their safety record and operational excellence. 
Customer Satisfaction
Safety and operational excellence are the two primary drivers of customer satisfaction. Recently, we learned that
Ensco was once again rated the #1 offshore drilling contractor in total customer satisfaction. In fact, Ensco was rated
#1 in eight of 14 categories including Performance and Reliability, Technology, Deepwater Drilling and Shelf Drilling.
We are gratified to receive this recognition from our customers for the fourth consecutive year. It is a testament to the
exceptional dedication of our offshore crews and onshore personnel around the world who consistently deliver safe
and reliable drilling services.
Financial Results
For the year, we earned $6.07 in diluted earnings per share, up 20% from $5.04 in 2012. Annual revenues were $4.9
billion, a 14% increase over 2012, as we added several new rigs to our fleet and experienced strong customer demand
for our drilling services. The average day rate rose to $223,000, compared to $193,000 in 2012. At year-end, our
contracted revenue backlog was $11 billion.
Based on our strong financial results, substantial backlog and positive market outlook, we raised our dividend twice
during the year, by 33% in February and an additional 50% in November. Combined, these increases doubled our
annualized dividend per share from $1.50 to $3.00. The new dividend puts our yield among the top 5% of all Standard
& Poors 500 Index companies.
Our board believes this dividend is prudent and sustainable. Our dividend payout ratio is among the most conservative
of major offshore drillers, and our balance sheet is the strongest as measured by the percentage of debt to total capital.
In addition to returning more capital to shareholders in 2013, we continued to invest in our fleet for future growth.
Enscos 2013 capital expenditures of $1.8 billion support the ongoing expansion and high-grading of our fleet. These
investments were partially funded by sales of older rigs as well as customers that contracted our rigs.
Growing Fleet
Our investments reinforced our standardization strategy. ENSCO 120, the first ultra-premium harsh-environment
jackup in our new ENSCO 120 Series, was delivered and has begun work for Nexen. ENSCO 121 followed soon
thereafter and is contracted to Wintershall. ENSCO 122 will be delivered later this year and is contracted to NAM.
All three rigs will work in the North Sea.
With the ENSCO 120 Series, we have taken a high-performance rig design and further enhanced it with proprietary
technology, such as a patented cantilever system. Enthusiastic customer response has confirmed the competitive
advantages of this series; accordingly, we ordered ENSCO 123, the fourth rig in the series, which will be delivered in
the second quarter of 2016.
During 2013, we also ordered ENSCO 110, a premium jackup rig that will be delivered in the first quarter of 2015.
ENSCO 110 will extend our line of high-specification rigs capable of working in shallow water-basins around the
world.
Within our floater segment, ENSCO DS-7, an ultra-deepwater drillship, was delivered in the third quarter and has
begun work for Total in Angola under a three-year contract. ENSCO DS-10, the eighth Samsung DP3 rig in our ultradeepwater drillship series, will be delivered in the third quarter of 2015. Like ENSCO DS-8 and ENSCO DS-9, both
of which will be delivered this year, ENSCO DS-10 takes our Samsung drillship design a step further with enhanced
efficiency and functionality.
In addition to our newbuild rigs, we invested in our existing fleet by completing major upgrades during 2013. These
upgrades have increased the capabilities of our rigs to keep operational performance at the highest levels. In 2014, 
major upgrades are planned for several rigs, including ENSCO 5006 as well as two of our jackups, ENSCO 54 and
ENSCO 70. For each of these rigs, customers are funding a portion of the upgrades and we expect these rigs to
commence multi-year contracts later this year.
Looking Ahead
In November, we announced that I plan to retire after nearly eight years as CEO. A well-defined succession process is
in place to ensure a smooth transition.
I have truly enjoyed leading Ensco. We have a highly talented management team and exceptionally dedicated
employees who have made our many achievements possible, including record safety performance, the highest levels
of customer satisfaction and record revenues and earnings.
While short-term market conditions can fluctuate, I believe the fundamental drivers of our business are strong.
Successful new discoveries by our customers over the past several years support future demand for additional drilling
rigs and healthy commodity prices continue to support investment by our clients in oil and gas exploration and
development.
The future is bright for Ensco as we transition to a new CEO. With a solid and experienced management team in place,
we are prepared to continue our positive momentum.
I want to extend special thanks to our employees for their commitment to go beyond every day, to our customers
who have consistently rated Ensco #1 in total satisfaction, and to our Board of Directors for their counsel and wisdom.
Sincerely,
Daniel W. Rabun
Chairman, President and CEO
28 March 2014