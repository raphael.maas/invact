2001 was a good year for Torchmark. Our net operating income increased 7% to $393 million.
On a per share basis, our net operating income increased 9% to $3.12.
We remained focused on providing protection-type life and health insurance products to our
target markets in middle income America. Although our underwriting income grew at a rate
less than our expectations at the beginning of the year, our growth in excess investment income
exceeded our expectations. We managed our capital effectively; we repurchased our stock and
we refinanced our debtboth actions enhanced the current and future value of our
shareholders stock.
With respect to life insurance, annualized
premium issued increased 1% to $295
million. Premium income increased 6% to
$1.1 billion. Underwriting margin, which is
the premium income less the amounts
applied (1) to fund current and future
benefits, and (2) to amortize acquisition
expenses, increased 5% to $284 million.
With respect to health insurance, annualized
premium issued decreased 16% to $213
million. Premium income increased 11% to
$1.0 billion. Underwriting margin increased
8% to $173 million.
Annuity premiums increased 14% to $59
million, and the underwriting margin
decreased 17% to $25 million.
The total underwriting margin increased 4%
to $483 million. Other miscellaneous income
was $4 million, and administrative expenses
increased 6% to $119 million.
Underwriting income, which is the sum of
the underwriting margins plus other income
and less administrative expenses, increased
3% to $368 million.
Net investment income increased 3% to $496
million. The required interest on our net
policy liabilities increased 3% to $189
million, and our financing costs declined 27%
to $51 million. Therefore, excess investment
income increased 13% to $256 million.
Net operating income as a return on equity
was 16.1%. Book value, assuming that our
fixed maturity assets are reported at
amortized cost instead of market, was $20.32
per share. Treating our preferred securities as
debt, our debt to capital ratio was 26.2%.
Annualized premium issued increased 17% to
$76 million. Premium income increased 6%
to $297 million, and underwriting margins
increased 8% to $90 million.
American Income is a union label company.
The sales force, with the endorsement
of unions at the local level, sells life
and supplemental health insurance to
union members.
At year end, this sales force was comprised of
1,768 producing agents, over 400 more
agents than at the beginning of the year. The
sales force was responsible for the issuance of
over 185,000 life insurance policies with an
average face amount of just over $26,000.
Of our life distribution systems, American
Income is not only our fastest growing, but
also produces the highest underwriting
margin per dollar of premium income. Going
forward into 2002, we expect American
Incomes growth in life insurance sales,
premium, and underwriting margin to exceed
that in 2001.

Annualized premium issued decreased 1% to
$115 million. Premium income increased 9%
to $307 million, and underwriting margins
increased 2% to $74 million.
A primary objective for both 2000 and 2001
was to issue new business that produced
higher margins than business issued in
preceding years. We accomplished this
objective in both years. To accomplish this
objective in 2001, we expected that our life
insurance sales would decline for the year.
One reason is that for the full year we
implemented tighter underwriting standards
that resulted in a higher percentage of the
submitted business not being issued.
However, I am pleased to report that our
expectations of lower sales did not
materialize. Life insurance issued was
virtually the same as in 2000, and the
expected underwriting margins over the life
of the business written during this two year
period should be greater.
We have significantly reduced our acquisition
costs per dollar of issued premium in the last
two years. In 1999, the acquisition costs were
$.94 per $1 of annualized premium issued;
but in 2000 and 2001, the costs were $.74
and $.71, respectively. And, we expect this
favorable trend to continue.
During the year, we issued 419,000 Young
American (issue ages 0 to 30) life insurance
policies with an average face amount of just
under $10,000 per policy. In addition, we
issued 448,000 policies to older Americans,
with an average face amount of $17,000.
These volumes and average face amounts
illustrate that at the younger ages there is a
need to begin an insurance program, and at
the later ages there is a need to protect the
family from the final expenses associated
with death.
Going forward, we will concentrate our sales
efforts in those areas that produce acceptable
returns. Our premium income will continue
to grow, and the growth rate of our
underwriting margins should be much closer
to the growth rate of our premium income.

Annualized premium issued increased 3% to
$66 million. Premium income increased 2%
to $453 million, and underwriting margins
decreased 1% to $88 million.
Our sales force sold almost 175,000 life
insurance policies during the year with an
average face amount in excess of $35,000. Of
the $453 million of premium income, less
than $50,000 was received by means of the
home collection process. Liberty National
has come a long way since ten years ago when
it was rightfully labeled as a debit life
insurance company.
Although we were disappointed with the
growth in life insurance sales for the year, we
were pleased with the growth in agents; we
ended the year with 2,162 producing agents,
up 6% from the prior year. For a number of
reasons, agents who have been with the
company more than one year produce more
business than agents in their first year.
Currently, over 50% of our sales force have
been with the company less than one year.
With ongoing recruiting and training efforts,
we will continue to grow our sales force, and
we look forward to greater growth in life
insurance sales in 2002.

Annualized premium issued declined 12% to
$98 million. Premium income increased 5%
to $512 million, and underwriting margins
increased 6% to $92 million.
Earlier in the year, we implemented a larger
than normal overall rate increase on our
Medicare supplement business; the overall
rate increase was 16%, and it applied both to
in force business and to new sales. The rate
increase and fewer HMO disenrollees than in
2000 combined to have a dampening effect
on our sales activity for the year.
The need for larger rate increases was not
something that caught us by surprise. In
recent years the regulatory authorities have
been reluctant to approve 100% of the rate
increases for which we filed, even though the
claims experience clearly demonstrated we
were above federal mandated minimum
claims loss ratios. But in 2001, due to our
persistence and to a softened regulatory
resistance, we were able to substantially
improve our rates. The collapse of many
HMOs in the Medicare market has once again
resulted in a realization that Medicare
supplements are a time-proven means of
providing quality protection to Medicare
beneficiaries. This fact may have influenced
the regulatory authorities.
We have diversification in our general
agency sales: 25% of our new business is life
insurance, 49% is Medicare supplement
insurance, and 26% is other supplemental
health insurance, primarily for individuals
under age 65. And although the Medicare
supplement rate increases in 2001 impacted
sales in all product lines, our general agencies
are comprised of seasoned agents who
recognize the importance of representing a
financially strong underwriter; they have
adjusted to the rate increases and they will
continue to grow their businesses.
Going forward in 2002, we expect growth in
sales in all product lines.

Annualized premium issued declined 20% to
$121 million. Premium income increased
25% to $342 million, and underwriting
margins increased 19% to $55 million.
We ended the year with 84 branch offices and
1,644 producing agents. Unlike prior years,
we no longer count an individual as an agent
until he or she has produced a sale, thereby
becoming a producing agent.
Although we havent determined the number
of producing agents at the beginning of 2001,
the number was greater than the number at
the end of the year. The Medicare
supplement rate increases described in the
previous section had an even greater negative
impact on our branch office operations.
There are two reasons for this result: (1) our
branch office agents are much more
dependent on Medicare supplement
business...almost 90% of its sales are
Medicare supplements, and (2) our branch
office sales force currently lacks the maturity
of our general agency agents. Extensive
recruiting efforts in the past several years
have resulted in a branch office sales force
wherein almost 60% of our agents have been
with the company less than one year.
Going forward into 2002, we may continue to
see a decline in producing agents for the first
half of the year. But our branch office system
includes seasoned branch managers who
know that recruiting and training new agents
is the only way to grow our business. We
expect the number of producing agents at the
end of 2002 to exceed the number at the
beginning of the year.

Annualized premium issued declined 14% to
$32 million. Premium income increased 8%
to $245 million, and underwriting margins
increased 6% to $59 million.
Sales declined because of the termination of
our marketing agreement with Waddell &
Reed; sales from this source were $4 million
in 2001 compared to $13 million in the
prior year.
Our primary independent agency relationship
is our military operation which is comprised
of a large agency that sells exclusively to
commissioned and noncommissioned
military officers and their families. This
agency is responsible for over 50% of the
sales, premiums, and underwriting margins
of our Other Independent Agency operations.
The military agency produces new business
for several other insurers, but over the past
decade we have earned an increasingly larger
portion of their total life production. We
will strive to earn more of their production
and the production of other independent
partners in the future. In addition, we will
continue to seek new partners for the sale of
life insurance.
ADMINISTRATIVE EXPENSES
Insurance administrative expenses increased
6% to $119 million, but as a percentage of
premium income, these expenses declined
from 5.5% in 2000 to 5.4% in 2001.
Torchmark has long been recognized as a
low cost administrator. Our efficiency is
derived from our dedication to simplifying
procedures and then automating them; the
result is not only reduced costs per dollar
of premium income, but also improved
service to our customers, both policyholders
and agents.

INVESTMENTS
Our investment strategy is to maximize the
positive difference between investment
yield and required yield on our net liabilities,
and to avoid uncompensated risk. Our
investment portfolio is concentrated in high
quality fixed-maturity assets. Fixed-maturity
assets represented 92% of our invested assets
at year end. For a variety of reasons, not
the least of which is our discomfort with
other types of investment alternatives,
fixed-maturity assets will likely become
an increasing percentage of our invested
assets. The average credit rating quality of
the fixed-maturity portfolio was A- as rated
by Standard & Poors and A3 as rated
by Moodys.
On a tax equivalent basis (i.e., recognizing
that certain bonds are subject to lower federal
taxes), our net investment income was $496
million. Excess investment income is the
difference between our net investment
income and the interest required on our net
interest-bearing liabilities. Required
investment income was $240 million,
resulting in excess investment income of
$256 million.
Primarily because of our stock repurchase
program, comparing the change in excess
investment income from one year to the next
is misleading. A better comparison is on a
per-share basis. Although excess investment
income increased 13% in 2001, the increase
was 15% on a per-share basis.
As noted in the chart below, Torchmark has
entered into derivative agreements known as
interest rate swaps. In 2001, these
agreements produced $8 million of excess
investment income. Should interest rates rise
dramatically over time, the current positive
excess investment income from the swaps
would reverse and become negative. We have
concluded that the risk/reward is in our favor.
Frankly, we hope that interest rates do rise,
even to the point that these swaps produce
negative excess investment income; for if
this should happen, the substantial cash
generated within our insurance and
investment operations would also be invested
at higher interest rates, and the net effect
would be beneficial to our operating earnings.



SHARE REPURCHASE
PROGRAM
Under the Companys active share repurchase
program, during the year we repurchased
almost 4.3 million shares of our outstanding
stock at a cost of $159 million. Since 1986,
we have repurchased our outstanding
stock in all years except one, and the
cumulative effect has been that we have
repurchased 49% of our outstanding stock.
We expect to continue this program into the
future as a means of enhancing shareholder
intrinsic value.
ACCOUNTING PRACTICES
In recent months, corporate America or more
specifically, corporate accounting practices
have been subject to increased scrutiny and
criticism, and rightfully so. With respect to
Torchmark, our earnings are cash driven. For
example, we do not rely on transactions
covered in recent news stories that produce
current revenues through mark-to-market
or other methods with no near term cash
flow. In addition, our earnings come from
two primary sources: underwriting income
from the insurance operations, and excess
investment income from the investment
operations. We present the results of these
operations in the same manner that
management utilizes to manage the business,
and make every effort to explain how each
operation contributes to our overall results.
With respect to the balance sheet, we have no
off-balance sheet liabilities. We believe that
our assets and liabilities are fairly stated and
reported in full accordance with the
directions provided by the Financial
Accounting Standards Board (FASB). In light
of some of the recent criticism directed
toward FASB, maybe its best that I just say we
believe our assets, liabilities, and our
operating results are fairly stated to the best
of our abilities.
SHAREHOLDER WEALTH
As you certainly have noticed, we have not
increased the shareholder dividend rate in
recent years. Furthermore, at least for the
near term, my recommendation to the Board
of Directors will be that we not increase the
dividend rate.
In my opinion, shareholder dividends are
the least effective means of increasing
shareholder wealth, primarily due to double
taxation...first, with the taxes that the
company pays on the money that is earned,
and again when shareholders pay taxes on the
dividends received.


I believe there are better means of increasing
shareholder wealth: The first is efficiently
investing in our businesses in order to grow
our earnings, which we do.
Secondly, making acquisitions that add value.
A few shareholders have expressed concern
that Torchmark has not made a major
insurance acquisition since we acquired
American Income in 1994. I remind them
that we have made significant acquisitions in
every year since 1994. For example, in each
year beginning with 1998, we have spent
between $126 million and $175 million for
the purpose of acquiring our own stock,
which we believe has generated a better
return on investment than any other
insurance acquisitions that have been
available to us. And, as stated earlier, we
expect to continue this acquisition program.
Thirdly, reducing higher cost debt adds
shareholder value. Since 1997, we have
reduced our debt from $1.1 billion to $885
million, and the overall required annual cost
of our debt has been reduced from 8.5% to
6.5%. From our own perspective, reducing
debt adds less shareholder value than
repurchasing our stock, but we also must be
cognizant of the financial rating agencies
and the favorable ratings that our insurance
companies have achieved; these ratings
are most important to our ongoing
insurance operations.
Therefore, for the time being we believe
that using our capital to invest in our
business, make acquisitions (whether other
companies or Torchmark stock) and efficient
management of our debt are better means of
producing shareholder wealth than an
increase in shareholder dividends.
OUTLOOK
In 2002, we expect continued growth in
premium income and underwriting margins
in all of our distribution systems, and we
expect our administrative expenses to again
decline as a percentage of premium income.
We expect investment operations to again
produce outstanding results. We will
continue in our efforts to increase the
intrinsic value of our shareholders stock. We
expect 2002 to be a good year for Torchmark.

C. B. HUDSON
Chairman and Chief Executive Officer