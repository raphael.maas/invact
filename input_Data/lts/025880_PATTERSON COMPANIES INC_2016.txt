Recently, Patterson Companies
established five strategic intents to
guide us to more sustainable, profitable
growth. Our bold actions to realign the
companys business portfolio in fiscal
2016 reflect these five pillars, which are:
 Leading our industries and customers
to new market breakthroughs
 Broadening our view of our
end markets
 Delivering a best-in-class
client experience
 Committing to talent excellence
 Leveraging our financial strength
to work for market advantage and
shareholder value
Our strategic intents address key
aspects of our business and will
continue to lead us in exciting
new directions.
Today, the company is focused on two
long-term growth markets  dental and
animal health. Customers across both
segments share common operational
needs and seek similar value from their
distribution partner. This compatibility
of need matches the go-to-market
approach and core competencies
that we have invested in for both of
our segments.
We are becoming more technologically
advanced, both in how we serve our
customers and in how we operate our
business. We believe our combination
of customer-focused innovation and
innovation from within is reaping
rewards today, putting us on a path for
ever-higher levels of effectiveness, and
enhancing our already market-leading
customer service and support.
Pattersons transformation is driving us
to scrutinize our legacy operations and
ensure we are optimized and positioned
for the future. We believe this continual
improvement will help strengthen
performance across our organization.
Strategic Intents into Action
Guided by our strategic intents, our pace
of execution dramatically accelerated in
fiscal 2016.
 We began with the repositioning of
our portfolio. We sold our medical
business segment and completed
the largest transaction in our history,
the acquisition of Animal Health
International, Inc.
 Integrating our new animal health
platform  bringing together our
companion animal and production
animal businesses  was a key focus.
We are pleased that we met our 2016
targets for synergies and established
this initiative on solid footing moving
forward.
 During the year, we initiated the pilot of
our new enterprise resource planning
(ERP) system. There are several truly
innovative aspects to this powerful
system, which we believe will produce
entirely new levels of operational
effectiveness and significant
enhancements to how we serve
customers. Our pilot locations
generated a steady flow of learnings
that we are using to fine-tune the
system for broader implementation.
We are setting the stage for future
growth, with our new ERP system
providing the scalability to attain it.

We are becoming more technologically advanced, both
in how we serve our customers and how we operate.
A high-performance culture is central to Patterson,
and we preserved our cultural strengths amid
large-scale change.

 A high-performance culture is central
to Patterson, and we preserved our
cultural strengths amid large-scale
change. We also deepened our
leadership team, attracting seasoned
executives with significant industry
and functional experience.
 Lastly, considering the complexity and
pace of the initiatives we are pursuing,
we finished the year with a strong
bottom line, achieving the adjusted
earnings guidance range established
at the beginning of the year.
Consolidated sales from continuing
operations rose nearly 40 percent on a
constant currency basis to $5.4 billion,
reflecting the acquisition, and we
finished the year with adjusted
earnings per diluted share from
continuing operations of $2.47. Fiscal
2016 was also a strong year in terms
of return to our shareholders, with
a total of $291 million given back
through a combination of dividends
and share repurchases.
Dental
The dental market in fiscal 2016
continued to reflect the stable-tostrengthening
conditions that
promoted more frequent dental
office visits by consumers. This
stability has allowed dentists to
focus on technology investment to
advance their service offerings and
improve the patient experience.
This year, we focused heavily on taking
advantage of positive market dynamics
in consumable sales. We also broadened
our traditional dental equipment
offerings, adding new manufacturer
relationships and product lines to
bring additional product choices to
our customers.
In technology equipment, we introduced
the new CEREC Zirconia workflow,
which allows for single-visit restorations
using this high-strength material. Dental
offices are becoming increasingly digital
in nearly every aspect of their
operations. This translates to an
improved patient experience and
greater operational efficiency. It also
elevates the need for service and
support capabilities to help ensure
reliability of this technology, so our
customers can concentrate on dentistry.
Our unparalleled service and support
capabilities, in many ways, enable
todays digital practice  a clear
differentiator for Patterson.
Animal Health
Today, Patterson operates the premier
animal health platform in North America
and the United Kingdom, blending our
traditional strength in companion animal
products and services, with the
continents foremost production
animal business.
In fiscal 2016, companion animal end
markets also benefited from stable-tosteadily
improving economics, which
helped drive favorable performance.
Production animal end markets reflected
some softness, especially in the beef and
dairy cattle sectors. However, we believe
this is part of the natural short-term
fluctuations in livestock markets and
that our animal health segment is
well positioned to capitalize on two
long-term growth catalysts: favorable
trends in consumer spending on pets
and the globes escalating demand for
animal protein.
The first year of any business integration
is critical to future success, and we
achieved our synergy goals for fiscal
2016. We consolidated the segments
headquarters to Greeley, Colorado,
rationalized support functions and
combined our sales teams. Today, we are
presenting a unified team to the market.
Strong Foundation, Bold Future
Fiscal 2017 will mark Patterson
Companies 140th year in business.
For nearly a century and a half, weve
weathered economic cycles, adapted
to market changes, and transformed
our company for the betterment of our
customers, employees and, of course,
our shareholders. We enter the new year
building a more formidable company
with attractive growth prospects.
However, some aspects of our company
remain unchanged. We are committed
to being the partner of choice for
future-focused customers who value
exceptional sales, service and support.
We remain a sought-after employer for
those who desire to excel and create
an impact. Finally, we are dedicated to
being a company that is a destination for
shareholders seeking outstanding value.
We thank all of you for joining us as we
create an exciting future.
Scott P. Anderson
Chairman, President and
Chief Executive Officer
