Dear Fellow Shareholders,

    As you know, last year California experienced an unprecedented energy crisis. That crisis created
turmoil in the energy markets. Our company faced the greatest challenge in its hundred-year history.

     These events tested the determination of the women and men of Edison International. Our people
had to persevere through many months of intense disappointments and work effectively under pressure.
Throughout, they remained committed to finding a solution that would restore the company's full capacity
to serve our customers and provide value to you, our shareholders.

     I am pleased to report that perseverance was rewarded. The key event was a litigation settlement
agreement reached with the California Public Utilities Commission (CPUC) in October. As a result of that,
Southern California Edison (SCE) is now on the path to recovery. Many important challenges lie ahead for
the entire company, but we are committed to continuing the progress in rebuilding financial health. We are
also committed to restoring our long-standing practice of making reliable dividend payments to you. The
goal is to begin dividend payments by the end of next year.

    In early March of this year, we achieved a major milestone in the recovery effort: we paid off the
past-due debt incurred by SCE in buying power to keep our customers' lights on while the crisis raged.
On the strength of a greatly improved cash position, strong cash flow, and the cost recovery settlement
reached with the CPUC, we were able to arrange $1.6 billion of financing and pay off $3.2 billion in past
due obligations. That was a major step toward regaining financial stability.

     We never want to go through another year like the last one, but there is strength in being severely
tested and meeting that test. Our ability to build the company's value and to pay shareholder dividends is
dependent on working successfully through additional challenges. As demonstrated in crisis, however,
our team has the determination, drive and creativity to succeed.

The Power Crisis: Effects on SCE
     Southern California Edison began 2001 deeply in debt for wholesale power purchases. In November
2000, SCE had filed a federal lawsuit against the CPUC seeking prompt recovery of those costs. In
mid-January 2001, our banks lost faith in the regulatory cost-recovery process and declined to lend
further to SCE. To conserve the cash necessary to operate its utility system, the company was forced to
suspend payments on prior obligations to lenders and wholesale power suppliers. With that, the State of
California had to take over responsibility for buying power for SCE's customers.

    Throughout the crisis, SCE was committed to staying out of bankruptcy, paying off our creditors and
doing our part to restore the reliability and predictability of California's energy market. Although we were
confident of our legal position in the federal lawsuit, pursuing full litigation over determined opposition
would likely have taken years to resolve. So we continued to work hard to achieve an earlier resolution.

     In early April 2001, California's largest utility, Pacific Gas and Electric, gave up on its out-of-court
resolution effort and filed for bankruptcy. Shortly thereafter, we were able to work out a Memorandum of
Understanding (MOU) with the Governor. That MOU, however, required implementing legislation to be
effective. The Legislature was called upon to pass judgment on a major, complex commercial agreement,
and was required to do so at a time of extraordinarily intense public and media focus on the crisis at the
national, state and local levels. Scores of proposed resolutions were put forth. In the end, after nearly
five months of developing proposed bills and holding hearings, the Legislature recessed without reaching
agreement.

     At that point in mid-September, prospects for a near-term resolution looked dim. The Governor
persisted, calling another special session of the state Legislature. We then made one additional effort to
work out a resolution with the CPUC. This time, after two weeks of intense negotiations, a settlement of
the federal lawsuit, filed nearly a year earlier, was finally achieved. The settlement agreement provides a
path for SCE to recover its power purchase costs and was approved by U.S. District Judge Ronald S. W.
Lew on October 5.

                                                      1
     Although the settlement allows recovery of previously unreimbursed costs, I want to underscore that
much remains to be done to restore fully SCE's financial health and its full capacity to act as a provider of
essential California infrastructure. As I write this letter, the CPUC has before it a number of key proposed
decisions, the outcome of which will deeply affect SCE's ability to regain full investment-grade
creditworthiness. At the same time, a consumer group, TURN, has appealed to the U.S. Court of Appeals
the decision of Judge Lew approving SCE's settlement with the CPUC. That settlement, which the CPUC
has continued to support steadfastly, is critical to SCE's recovery.

     We have, throughout the crisis, regarded it as a primary obligation to pay the people and businesses
to whom we owed money. For many months, they refrained from forcing us into bankruptcy. In fact, we
may have set a corporate record for the duration of creditor forbearance. Whether or not that is truly a
record, we are grateful to our creditors for finding our recovery efforts credible and giving us the time to
get the job done.

The Power Crisis: Effects Across Edison International
     The adverse impacts of the power crisis were not limited to Southern California Edison. Throughout
last year, lenders--most of them already at risk with large prior commitments to SCE--sought to reduce
their overall exposure to Edison International. Since SCE could not pay down debts, most lenders
focused their risk-mitigation efforts on our need to refinance maturing debt at our other companies. This
required repeated restructuring of debt at our holding company and at Edison Mission Energy (EME) and
Edison Capital, and the cost of rolling over the debt was high.

    Among all our subsidiaries, only EME retained its investment-grade credit rating. That was achieved
through divesting power projects, adopting governance protections for EME creditors, and forgoing new
growth initiatives. The most important of those steps was the sale of the Fiddler's Ferry and Ferrybridge
(FFF) power plants in the United Kingdom, which we purchased in 1999. The loss was large, but the sale
was essential.

     Edison Capital was also adversely impacted by the energy crisis. Without access to capital and
following a sharp drop in its credit rating, the team at Edison Capital had to change course to create
liquidity. They responded quickly. The employee base was halved, operating costs were significantly
reduced, and $600 million was raised through asset sales.

     Finally, we found it necessary to wind up Edison Enterprises. Its principal business line was our
Edison Security business, which would have required greater scale to achieve success and was hurt by
the reputation impacts associated with SCE's financial weakness. The net after-tax losses associated with
the FFF and Edison Security sales totaled $1.4 billion.

Other 2001 Achievements
    Notwithstanding the stress of the power crisis, the people of Edison International last year not only
achieved a sound path to resolve SCE's dire position, but they also met other important goals. Several of
those accomplishments deserve special recognition.

     During the summer of 2000, when it became apparent that California desperately needed additional
power supply, I challenged our people to find a way to help fill that need. EME scoured the state, located
a partially permitted but abandoned project, negotiated to buy both the project rights and turbines for it,
and set out to meet a near impossible deadline. In the end, EME beat the deadline by 45 days, bringing
online in June of last summer California's first new generating station in 13 years. This was an
extraordinary achievement. The 320-MW Sunrise Plant, located in central California, moved from
groundbreaking to ribbon cutting in a record six months, and EME committed Sunrise's electrical output to
serve Californians under cost-based pricing for the next 10 years.

    In Asia, after persevering through the backdrop of a severe five-year economic slump in Indonesia,
the EME Asia team, along with our project partners, secured a binding agreement on terms for a renewed
long-term power sales contract between our Paiton generating station and the Indonesian national utility.
This important step was achieved with the support of the Indonesian government. Detailed agreements
remain to be worked out during 2002.

                                                      2
      In Illinois, the more than 1,000 represented employees who operate seven power plants owned by
EME's Midwest Generation subsidiary went on strike on June 28, 2001. Throughout the summer, our
management team ran the plants, setting new records for plant performance and meeting all-time high
electricity demand. In October, a collective bargaining agreement that provides needed flexibility for the
company and fair compensation to represented employees was finally reached. The strike came at the
worst possible time for our customers in Illinois and for our company, but the management team handled
it with true excellence.
     Throughout the energy crisis, our SCE employees remained intent on providing excellent service to
our utility customers. One key metric is exemplary; in fact, I find it amazing. We ask customers who have
any form of transaction with SCE to evaluate our service. Last year, even as the crisis raged, our
customer service evaluations were the second highest we have had since the customer feedback
program began in 1992. That score qualifies the company for a service incentive award of $8 million from
the CPUC. Also in 2001, SCE earned the highest CPUC award for reliability ($5 million) and again earned
the highest award for safety ($5 million).

Edison International Year 2001 Earnings
      Solvency--not earning power--was, by necessity, our primary focus in the past year. Core Edison
International earnings were down by 26% from the prior year, and down at each of our companies except
EME, whose earnings were up by 17%. Significant factors in SCE's reduced core earnings were a
fire-caused outage at San Onofre Unit 3 and the financial impact of the power crisis.
    Taking into account nonrecurring items, including the restoration to the SCE balance sheet of
regulatory assets previously written off and the losses associated with asset sales at EME and Edison
Enterprises, Edison International's year 2001 recorded earnings were approximately $1 billion. These
results are discussed in greater detail in the "Management's Discussion and Analysis of Results of
Operation and Financial Condition" section of the Annual Report.

Electricity Industry Turmoil
     Last year was a time of turmoil and change, not only at Edison International, but also across much of
the electricity industry. In addition to the California power crisis, three other major events will have
enduring effects.

     September 11 and Electric System Security
     The tragic events of September 11 and their aftermath have raised concerns about the potential for
terrorist acts on our electric systems. Even though in the past we have had strong security systems and
processes, in response to September 11, we further tightened security at all major Edison facilities and
generating stations--especially at our San Onofre Nuclear Generating Station in California.
    I also want to note that the September 11 tragedy touched us in a personal way, with the loss of
22-year-old Lisa Frost, the daughter of SCE transmission system operator Tom Frost. Lisa, who perished
on hijacked United Flight 175, was an exemplary student and person who, in her time with us, had made
the world a little better. With the Frost family, we mourn her loss.

     Enron
     Another event that will likely have enduring effects on our industry was the collapse and bankruptcy
of the Enron Corp. In recent years, Enron has been perhaps the most influential private party affecting
public decision making in the electricity business across the nation. At this time, we do not know what
impact, if any, Enron may have had in influencing prices in wholesale power markets last year in
California and the West. What is certain, however, is that Enron's business practices will, and should be,
investigated with intensity. We hope that the result is improved business policy and regulation, with
respect to both accounting practices and fair, open electricity markets.

     Independent Power Production
     Finally, an extraordinary decline in market valuations for independent power companies that began
late in 2001 and has continued into this year is deeply affecting the industry. Share prices for the publicly
owned segment of the industry peaked in October 2001 and have declined by more than 50% to the
present.

                                                      3
     This decline has some immediate impacts on us. Lenders and credit-rating agencies are applying
more stringent criteria to their judgments about independent power projects. Since last September, six
publicly traded independent power producers in the U.S. have been downgraded or put on "credit watch"
by the credit-rating agencies. The tightening of credit standards has reduced the number of parties with
whom EME can contract. In general, with the Enron collapse and lower credit ratings in the industry, the
sector is in a period of stress and change.

Edison International Outlook: 2002 and 2003
    We have had the view that our company will be best served by building both a strong utility business
and a strong independent power generation business. We have also had the view that our customers and
shareholders will benefit from longer-term and stable arrangements for power supply. The events of the
past year have strengthened our conviction in these respects.
     The major focus for us in the next two years will be to restore our company's overall financial health
and flexibility. That should enable us to reduce rates for SCE customers and resume paying dividends to
you. It will also facilitate taking the State of California out of the power-buying businessa vital step for the
state's taxpayers and our customers. With financial health, SCE will be able to implement the necessary
capital investments and system enhancements to continue to provide superior service and transmission
and distribution reliability. In our nonutility businesses, growth will be constrained as we give priority to
strengthening credit, building liquidity, reducing volatility associated with sales into short-term power
markets, entering into longer-term power purchase agreements, and ensuring that our costs are low and
competitive.
    Finally, and at all times, we will seek to live up to high business and personal values of integrity and
high-quality service.

Board and Management Changes
     Our long-time director, Dr. Edward Zapanta, passed away--far too young--in February of this year.
At our annual meeting in May, Warren Christopher, Carl Huntsinger and Charles Miller will retire. All four
of these men provided wise counsel, consistent support and sound judgment. We will greatly miss their
service to us.
    At the end of 2001, we announced new leadership at SCE and EME. After more than six years in
senior leadership roles at SCE, including the past two years as Chairman, President and CEO,
Stephen E. Frank retired. Steve's stewardship, particularly during the crisis, made a significant and
enduring contribution to our company.
     Succeeding Steve at SCE are Alan J. Fohrer, as Chairman and CEO, and Robert G. Foster, as
President. Al comes to this role after serving as President and CEO of Edison Mission Energy for the past
two years, and in engineering and financial capacities for nearly 30 years with Edison International and
SCE. Bob has led our external affairs for Edison International and SCE during his 17 years with the
company. Succeeding Al as President and CEO of EME is William J. Heller, who previously served as
Division President of EME Europe, and before that, as head of strategic and corporate planning for
Edison International. Finally, we made Theodore F. Craver Executive Vice President of Edison
International. Ted also serves as our Chief Financial Officer. These men bring experience, dedication,
sound judgment and high personal standards to the leadership of our company. I am proud to work with
them.
     Thank you for the trust that you, by your investments, have put in us. We will work hard to reward
that trust.



                                                           John E. Bryson
                                                           Chairman of the Board, President
                                                           and Chief Executive Officer
                                                           March 28, 2002

                                                       4
