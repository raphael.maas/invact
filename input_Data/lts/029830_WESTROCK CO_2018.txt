Dear Fellow Stockholders:
I am pleased to present WestRock�s annual report for fiscal year 2018. The past
year has been characterized by record operating and financial performance
by the WestRock team accompanied by investor concerns about the impact of
announced new capacity additions that have challenged our and others� stock
price performance.
This situation requires both context and perspective. My perspective is that,
on a global basis, the paper and packaging industry�s use of renewable and
recyclable resources to provide sustainable solutions provides an attractive
value proposition that has and will support industry growth over the long term.
Further, WestRock is well-positioned to succeed over the near and long
term. WestRock�s vision is to be the premier partner and unrivaled provider
of winning solutions to our customers. We are achieving this vision through
our differentiated solutions, breadth of capabilities in both corrugated and
consumer packaging, geographic footprint and scale, and financial strength,
each of which we increased in 2018. Our focus on the customer, building our
capabilities and culture, excellent execution and disciplined capital allocation
have served and will serve us well over the short and long term.
Fiscal 2018 Highlights
WestRock achieved record-setting results in 2018. During the fiscal year, we generated record sales, Adjusted Segment
EBITDA and Adjusted Operating Cash Flow. Through a combination of acquisitions and organic growth, we increased
our Net Sales by 10% to $16.3 billion � the highest sales level in our history. Through improved operating performance,
we increased our Net Cash Provided by Operating Activities by 27% and our Segment EBITDA by 28%. Our strategy
leverages the broadest portfolio of differentiated paper and packaging solutions in the industry to grow our sales with
customers that value our unique solutions and optimize our business across our system. Our 2018 financial performance
demonstrates that we are successfully executing this strategy. I am very proud of our WestRock team members for
delivering these impressive results.
When we completed the merger of RockTenn and MeadWestvaco in 2015, we set the ambitious goal to realize $1 billion in
synergy and performance improvements by the end of fiscal 2018. We achieved this goal one quarter earlier than initially
expected. This accomplishment demonstrates that our culture of continuous improvement, innovation, accountability and
excellent execution is driving improved financial results and delivering long-term value for our stockholders.
One of the most impressive financial results for the year was the 22.6% Adjusted EBITDA Margin that we achieved in
our North American corrugated packaging business, nearly 1,000 basis points over our 12.7% margin in 2012. This
reflects the extraordinary progress made by our management team to focus on customers, �no-fail basics,� investing for
competitive advantage and a relentless urgency to perform at the highest level.
In 2018, we used our financial strength to invest in our business to promote long-term value creation for our stockholders.
We deployed $348 million to strategic acquisitions and investments, primarily including the acquisitions of Plymouth
Packaging and its Box on Demand� system, which has helped us to better serve the growing e-commerce market, and
Schl�ter Print Pharma Packaging, which has increased our exposure to the pharmaceutical market and expanded the
geographic scope of our Consumer Packaging segment in Europe.
We invested $1 billion in capital expenditures, including $179 million toward strategic capital projects, including projects
to install a new paper machine at our mill in Florence, South Carolina, build a new box plant in Brazil, and install a new
curtain coater at our Mahrt mill in Cottonton, Alabama. These multi-year investments will enable us to more efficiently
operate our business and better serve our customers.
Steve Voorhees
Chief Executive Officer
In addition, after investing in our business, we returned $636 million to our stockholders, including $441 million in
dividends and $195 million in share repurchases. We recently increased our annualized dividend for the third successive
year since forming WestRock. The increase to $1.82 per share represents a 5.8% increase over our prior dividend.
KapStone Acquisition
In November, we completed the approximately $4.8 billion acquisition of KapStone Paper and Packaging Corporation.
KapStone is a great fit with WestRock. Its complementary corrugated packaging and distribution operations enhance
our ability to serve customers across our system, particularly in the western United States, and the addition of KapStone�s
specialty kraft paper products enhances our differentiated portfolio of paper and packaging solutions. I am thrilled about
the addition of KapStone and confident that the acquisition has already made WestRock an even better company.
Fiscal 2019 Outlook
We entered fiscal 2019 with sustained momentum to implement our strategy. In 2019, we expect our Net Sales to
exceed $19 billion, Adjusted Segment EBITDA to be approximately $3.6 billion, and Adjusted Operating Cash Flow to be
approximately $2.55 billion. Importantly, we will invest approximately $1.5 billion to maintain and improve our business,
including making strategic investments in our Mahrt, Covington and Florence mills in the United States and in our
Tr�s Barras mill and new box plant in Porto Feliz, Brazil. We have a track record of strong execution and effective capital
allocation, and we are focused on continuing to improve our margins and grow our cash flow.
Creating Our Future
Over the past three-and-a-half years, we have created a unique company focused on paper and packaging. Our multinational platform, the breadth of our differentiated solutions, our commercial and operating capabilities and financial
strength provide an exceptional platform for success.
Culture makes our platform work and, to that end, we are fostering a culture that encourages integrity, respect,
accountability and excellence, where our employees are curious, inclusive, empowered and team-oriented -- working
collaboratively to deliver exceptional results. We remain focused on the safety of all of our team members, every day.
We are challenging ourselves to be even more customer centric, to be more diverse and inclusive, to develop and
broaden our capabilities, to grow and scale across our footprint and to operate with excellence across the entire WestRock
organization. As we respond to these challenges, we are becoming even more innovative, building digital strength to
gather insights for ourselves and our customers and, importantly, providing opportunities for WestRock to attract and
develop talent for the next generation.
As we develop our people and our culture, we will build our customer relationships and invest in our business to improve
our margins, build scale and grow our portfolio of differentiated paper and packaging solutions. With that focus, we will
sustain excellence and create a bright future for WestRock that creates significant value for customers and stockholders.
On behalf of our board of directors and the 50,000 WestRock team members around the world who are committed to
delivering outstanding results, thank you for your investment in us.
Sincerely,