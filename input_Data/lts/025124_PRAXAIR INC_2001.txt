to our shareholders
And because we moved quickly to realign our cost structure with new realities in the 
marketplace, Praxair was able to weather a very tough year better than most. As a result, 
by year-end Praxairs stock price rose 25%, signi?cantly outperforming the Standard &
Poors 500 Index, which declined by 13%.
Although sales and operating earnings in 2001 were up just slightly over 2000 levels,
excluding special charges, we were able to maintain after-tax return on capital at 12% 
and increased operating cash ?ow by 13%, demonstrating the resilience of our business.
It was not easy. Despite the outstanding efforts of Praxair employees around the
world, the global economic slowdown severely hampered our ability to meet our near-term
?nancial targets. Lower demand from industrial customers in the Americas was only 
partly offset by growth in businesses such as healthcare, and by good performance in our
European and Asian businesses. Results in South America were adversely affected by 
weak currency and an economic downturn caused by energy shortages. Depressed aviation
markets were re?ected in Praxair Surface Technologies results.
An operating highlight of the year was substantially improved safety performance.
Worldwide, Praxair employees reduced injury rates by 16% and lost work-day case rates 
by 27%. In addition, product vehicle accidents were down 9%. We strengthened our 
safety programs with the implementation of a safety observation system that encourages
employees to coach each other on good safety practices.
I also want to recognize the extraordinary response of employees in North America
to the events of September ??. Within an hour of the terrorist attacks, our North American
Logistics team had swung into action, and volunteer drivers were able to deliver medical
gases to hospitals in the New York and Washington, D.C. areas throughout the day. 
Over the next week, almost ??,??? medical oxygen cylinders were ?lled at Praxair locations
and delivered from as far away as Missouri and Quebec, Canada. Additional thousands 
of cylinders of industrial oxygen and acetylene, along with safety equipment, were delivered 
to Ground Zero for the search and recovery effort. Employees throughout the system put
in long hours, hard work and exceptional creativity that went beyond the normal business 
call of duty.
Moving on to our growth programs, I am pleased with our progress. In our metals
technologies business, a major win during ???? was the ?rst commercial license agreement
for our patented CoJet 
gas injection technology in the important integrated steel market.
The potential is huge  integrated companies produce ??? million tons of steel worldwide
each year, and we estimate that CoJet technology can deliver up to ?? per ton of savings,
depending upon the level of existing process technology and re?ning procedures of the
mill. Our licensing agreement will allow us to share in those savings. Even a modest market
penetration could mean signi?cant licensing revenues, and we are in discussions with 
several additional integrated mills around the world.
In healthcare, we are building on our strong, hospital-based institutional presence
and our existing packaged gases distribution networks to expand our home care business,
which is a fast-growing segment of the overall healthcare market. In North America, we are
creating critical mass in the home care market through small acquisitions that augment
regional hubs; four such acquisitions in the U.S. and Canada were completed during 2001.
As the No.1 supplier of oxygen to hospitals in North America, Praxair is in an ideal position to provide continuing therapy to patients after they are discharged from the hospitals
we serve, so creating a hospital-to-home link.
Hydrogen has emerged as a strong source of future growth for Praxair. Hydrogen
demand has been growing by 10% to 15% per year, and is expected to accelerate to nearly
20% annually over the next ?ve years as new gasoline and diesel regulations in the U.S. are
implemented. Praxair is well positioned to meet this demand, with 300 miles of the most
extensive hydrogen pipeline system on the U.S. Gulf Coast, already connected to a substantial portion of existing Gulf Coast re?ning capacity. We initiated plans in 2001 to build two
new hydrogen plants, scheduled to start up in late 2003 and early 2004, in order to meet
future demand from re?ning customers. This opportunity is expected to account for several
hundred million dollars of new sales over the next ?ve years.
In the electronics business, Praxair held semiconductor sales close to the previous
years level in spite of the worldwide industrys dramatic 35% drop in semiconductor 
shipments. With an eye toward a future upturn in these markets, Praxair Semiconductor
Materials made signi?cant progress in its quest to become a one-stop, integrated supplier 
to semiconductor manufacturers. For the rapidly emerging compound semiconductor segment, we introduced a new grade of high-purity ammonia that is the leading speci?cation
in the industry. High-purity ammonia is used in the production of light-emitting diodes
(LEDs), high-performance semiconductors and ?at-panel displays. We also introduced an
ultra-high-purity bulk ammonia gas-delivery system featuring the highest ?ow rates in the
industry. Both silicon and compound semiconductor manufacturers now have access to 
an expanded product portfolio, which includes bulk and specialty gases, process chemicals,
sputtering targets and electrostatic chucks. Complementing the product portfolio is a 
growing list of service offerings, such as on-site total materials management and spare parts
and consumables inventory management.
In the ?ber-optics market, where Praxair is a leading global helium supplier, demand
during the ?rst half of the year was robust, but our helium team anticipated the second-half
downturn. It redoubled efforts to complete negotiations on new contracts and generate
new sales, partially offsetting falling customer demand and ending the year with helium
sales up 7% over 2000 levels. During 2002, we plan to launch a technology-licensing initiative based on Praxairs patented helium-recovery technology that promises to generate 
substantial cost savings for ?ber-optics customers as well as additional licensing revenues 
for Praxair.
Productivity improvement continues to be a key factor in our overall performance.
In 2001 Praxair made a major commitment to implement Six Sigma worldwide. We believe
the tools and disciplines of Six Sigma are fundamental to achieving the next level of ?awless
transactions with our customers and a step change in productivity. Mo re than 100 employees
became Six Sigma black-belts during the year, and dozens more are in training in Brazil,
Asia and Europe. The savings and ef?ciencies resulting from hundreds of projects currently
underway are expected to make a contribution to ?nancial results beginning in 2002.
We also continue to upgrade our information-technology platforms in order to 
promote ef?ciency through enhanced e-business capabilities and transaction processing,
and improved knowledge management. There has been a substantial increase in the number of customers who monitor their tank readings online through the Praxair Desktop
SM
extranet and we launched a more sophisticated system for customers to order cylinder gases
and related equipment online, called Praxair Express
SM
. An upgraded external website,
www.praxair.com, was launched in September, designed to make it easier for customers to
locate information they need about Praxair products and services.
In the face of uncertain economic conditions and in order to position Praxair for
future pro?tability, in late September we made the tough decision to reduce the workforce
by almost 1,000, in part to implement redirected strategies for Praxair Surface Technologies
and South America.
In Praxair Surface Technologies, our strategy is to shift our focus from traditional
aviation markets and extend our advanced materials sciences capabilities into fastergrowing markets.
Geographically, we are putting our resources in 10 countries where Praxair has, or is
quickly developing, a pro?table, high-return competitive advantage: the U.S., Canada,
Mexico, Brazil, Spain, Italy, South Korea, Thailand, China and India. We are evaluating
businesses in other countries, and plan to divest under-performing operations in 2002.
Our emphasis in South America generally is on generating cash ?ow and leveraging
existing facilities, while limiting investment in new production capacity. Over the last 18
months, we have more than doubled South American revenues from less capital-intensive
services businesses, for example by offering training and inventory-management services 
to metal fabrication customers; introducing ozone-based laundry systems; and expanding
the home care oxygen-therapy business.
Looking ahead, we intend to grow sales faster than underlying economic growth 
by leveraging a strong core business and by developing less capital-intensive sources of new
growth. Over the next 18 months we plan to:
 license CoJet technology to additional integrated steel producers;
 expand our home healthcare business;
 continue to strengthen Praxairs electronics business;
 increase hydrogen capacity to meet higher demand from U.S. re?ners;
 accelerate growth in South American service offerings;
 fully implement Six Sigma in all business units; and
 ?x, reposition or sell under-performing businesses in our global portfolio.
We also are taking steps to further strengthen our organization. I believe leadership 
at all levels  is what lifts adequate performance to excellent performance, and leadership
development is where we focus our efforts. Over the next year, we will be applying more
rigorous criteria to our performance-management system with the aim of continuously
upgrading performance levels throughout the company. In addition, three managementdevelopment programs are helping us reach down into the organization to provide our best
and brightest mid-level managers with opportunities to accelerate their career advancement.
Almost 100 employees from around the world already have bene?ted from our successful
Global Leaders Program, now in its third year.
Within the Of?ce of the Chairman, Stephen Angel, formerly general manager 
of General Electrics Power Equipment Business, joined Praxair as executive vice president
early in the year. Steve brings experience in managing service-oriented operations and in
implementing Six Sigma methodology, among other talents, to our top management team.
We welcomed two new board members in 2001. Wayne Smith, chairman, president
and chief executive of?cer of Community Health Systems, Inc., is an experienced healthcare executive whose insights will be valuable as Praxair expands its presence in this market.
William Wise, chairman, president and chief executive of?cer of El Paso Corporation, 
has extensive knowledge of energy markets which will be a great asset as Praxair continues
to develop its innovative energy-management techniques. Also during the year, 
Fred Fetterolf, former president and chief operating of?cer of the Aluminum Company 
of America, retired from the Praxair board. He has been a board member since 1992 and
chairman of the Audit Committee, and we have valued his wise counsel over the years.
In summary, the silver lining in the current tough economic environment is 
the evidence that our business strategies allow Praxair to weather a variety of marketplace 
scenarios while bringing value to both customers and shareholders. The industry 
fundamentals remain strong, and Praxair holds a premier place in an excellent industry.
Praxairs goals are straightforward: meet customer needs better than anyone; and create
shareholder value. Through a combination of investment discipline, productivity improvement and new sources of growth, Praxair intends to increase sales while improving return
on capital. The key will continue to be the Praxair teams ability to help our customers
become stronger, faster and more pro?table.

Chairman, President & Chief Executive Of?cer