Letter to Shareholders
it was a challenging 2002 for Northern Trust, as the stock market reached
a five-year low and the U.S. economy struggled and was unable to sustain a recovery.
In that environment, Northern Trust reported net income per share of
$1.97, compared with $2.11 in 2001. Net income was $447.1 million, compared with
$487.5 million earned in the previous year, resulting in a return on average common equity
of 16.2 percent.
At year-end 2002, trust assets under administration totaled $1.5 trillion,
compared with $1.67 trillion the previous year. Managed trust assets were $302.5 billion,
a decline of 5.4 percent, compared with December 31, 2001. Equity market conditions were
extremely challenging for all investors, with the S&P dropping 23.4 percent and, for the
first time in 60 years, markets declining for three consecutive years. Despite the impact of
the steep and persistent decline in equity markets, Northern Trust continues to be one
of the largest asset management firms in the world.
Total revenues of $2.2 billion were 3 percent below the prior year.
A continuing positive aspect of our performance has been our success in managing
expenses, which in 2002 increased to $1.4 billion, only 1.6 percent more than last year.
We continue to manage staff growth closely, while making certain that we have in place
the talented and experienced professionals our clients rely upon.
The Corporation declared a quarterly cash dividend of 17 cents per
share. This is the 106th year of consecutive dividends paid.
The performance of Northerns stock in 2002 was a disappointment,
with a decrease of 41.8 percent. However, when viewed over a longer term, our performance
is quite different. The compound annual growth rate of Northerns stock for the 10-year
period ending December 31, 2002, was 14 percent, compared with 11 percent for the KBW50
Bank Index and 7 percent for the S&P 500. Detailed financial results are covered fully in
Managements Discussion and Analysis beginning on page 31.
Reasons for Confidence Since our founding in 1889, Northern Trust
has grown to become one of the countrys strongest and most secure financial institutions.
This strength was recognized last year by Global Finance magazine, which ranked Northern
as one of the Worlds Safest Banks. We were one of only four U.S. banks on the list of
50 institutions. At year end, our nonperforming assets were only .52 percent of total loans.
It is during recessionary periods that balance sheets and capital strength matter most. Ours is
one of the most liquid balance sheets in the financial services industry, and we have
achieved continuous growth in capital. Thus, we are able to make important investments
for the future in people, technology, product development and the expansion of our
national office network. Northern Trust is in an excellent position to rebound when equity
markets improve. The strength of the Northern Trust brand has never been more evident
than this past year when, despite the most severe global bear markets in generations,
we continued to win significant new business.
Strategic Vision Throughout 2002, we continued to focus on two
principal client-focused businessesPersonal Financial Services (PFS) and Corporate
and Institutional Services (C&IS). Both of these businesses are supported by our world-class
investment organization, Northern Trust Global Investments (NTGI), and our leading-edge
Worldwide Operations and Technology (WWOT) group.
Last year, PFS professionals throughout our national office network
brought high-touch trust, investment management and banking services to affluent
individuals and families in our target markets. Hampered by ongoing weak equity markets,
PFS trust fees totaled $606.6 million, compared with $616.7 million a year ago. Personal
trust assets under administration were $156.7 billion, and of that total, $87.7 billion was
managed by Northern Trust.
With our 82 office network in 12 states, we continue to enjoy a
one-of-a-kind distribution capability for personal trust and private banking services. No other
personal trust provider has this extensive reach in these attractive, affluent growth markets.
Our target market in PFS generally includes those individuals with at least $1 million
or more of investable assets. These individuals include corporate executives, retirees,
professionals and owners of small- and mid-size businesses.
In 2002, we opened a new office south of San Francisco in Los Altos,
California, and announced our entry into the Atlanta, Georgia, market with the pending
acquisition of Legacy South, Inc., an Atlanta-based private wealth management firm with
approximately $300 million in assets. This acquisition will mark entry into our 13th state.
In 2003, we will open an additional Illinois office in the Chicago suburb of Park Ridge.
We also continue to invest in our existing office infrastructure and, in 2002, remodeled or
expanded offices in Michigan, Nevada, Illinois, California and Florida. All of our locations
are attractive, strategically located and offer clients a full range of financial services on site.
Our January 2003 acquisition of the passive asset management business
of Deutsche Bank AG offers us an opportunity for expansion into the important New York
market. Since many of the Deutsche staff already are located in New York, we intend to
supplement that office with a client service team focusing on personal clients.
Within PFS, the Wealth Management Group serves families who
generally have $100 million of investable assets, invest globally and employ multiple
investment managers. Northern is an industry leader in this high-end segment of the
personal market and, in 2002, served 288 families in 49 states and nine countries. Trust
assets administered by Wealth Management totaled $65 billion. Of that total, $14 billion
was managed by Northern.
We remain optimistic about the opportunities for growth in our
PFS target markets. Despite the market declines, a recent study by Cap Gemini Ernst and
Young/Merrill Lynch indicates that the affluent market is growing at a favorable pace.
According to the study, the wealth of high-net-worth individuals will rise by an average
of 8 percent a year by 2006.
Traditional PFS marketing strategies have been highly successful,
but the nature of wealth accumulation is changing, and we are changing along with it.
To meet the needs of our sophisticated clients, we have expanded our product offerings
to include hedge funds, private equity and single-stock risk management. As clients
needs evolve, we will work with them to find innovative solutions tailored to their
complex needs.
Our C&IS client base includes corporate and public entity retirement
funds, Taft-Hartley funds, foundations and endowments, government funds, financial
institutions, insurance companies and fund managers. C&IS provides investment management,
investment asset administration, retirement and treasury services to a growing number
of clients worldwide.
C&IS contributes approximately one-half of the Corporations revenues.
In 2002, C&IS trust fees were $626.5 million, compared with $642.4 million the previous
year. Lower securities lending fees resulted in this change. Fees from asset management and
custody each increased 2 percent to $186.8 million and $218.6 million respectively.
Trust assets under administration in C&IS totaled $1.35 trillion at year end, and of that
total, Northern managed $214.8 billion.
Market conditions during the year were challenging, but new business
growth was strong, and within target market segments, C&IS has strong market positions.
In the U.S., C&IS serves 24 to 30 percent of the major U.S. private pension funds, foundations
and endowments. In the United Kingdom, we serve 18 percent of the top 200 U.K. pension
funds and 25 percent of the U.K. local authority market.
Northern has achieved these strong market positions, in part, because
of our array of products and services and our growing global presence. The international
segment is one of the fastest-growing areas of C&IS, and Northern is recognized as a market
leader in global custody. Of $1.35 trillion in C&IS trust assets under administration at
year end, global custody assets accounted for 35 percent of the total, or $472 billion. Global
custody assets have grown at a compound annual rate of 26 percent over the past five years.
C&IS looks forward to a number of significant growth opportunities in
the future. For example, although we already are a major asset manager, there is ample
opportunity to grow. Managing a larger percentage of our existing clients custody assets is
a strategic goal. The privatization of retirement funds and the increased global orientation
of clients offer additional opportunities for the C&IS international segment. Industry
consolidation results in change, and many prospective clients view such change as the
time to review the performances of their current providers. The C&IS relationship
orientation and industry-leading technology platform position us to be a net winner of
new business opportunities.
NTGI is a global, multi-asset class investment manager that combines
research, portfolio and product management, client servicing and market activities.
NTGI is able to leverage these capabilities across Northerns corporate and personal client
bases. At year end, NTGI managed $302.5 billion in assets, making it one of the largest
investment managers in the world. Pensions and Investments magazine ranks Northern Trust
as the 14th-largest investment manager in the U.S., the seventh-largest manager of
index-based assets and the fifth-largest manager of tax-exempt assets. According to industry
publication FRC Monitor, NTGI ranks as the fifth-largest bank-run mutual fund family.
Our commitment to the asset management business is illustrated by
our impressive 15.1 percent compound annual growth rate of our managed asset base since
1996five times that of the S&P 500. Our commitment to the investment business is further
emphasized with our January 31, 2003, acquisition of Deutsche Banks global index
management business. With this acquisition, NTGI now ranks among the top three U.S.
institutional index managers. In addition to a more prominent industry position, the
acquisition will have a number of other positive effects: a wider array of passive products
for clients worldwide; increased trading volume that will benefit our rapidly growing
transition management business; and expanded securities lending activities resulting from
new assets under management.
NTGI remains committed to offering a broad range of investment
capabilities and services, especially important to clients during difficult equity markets.
Clients can select from a wide range of products in the categories of equities, quantitative, fixed income and manager of managers. In addition, our two proprietary mutual fund
families, Northern Funds and Northern Institutional Funds, offer a choice of 54 funds.
At year end, these two fund families had combined net assets of $45 billion. NTGIs
investment performance in these turbulent times has been quite commendable. According
to Lipper rankings as of December 2002, a high percentage of our funds across all styles
falls into the first and second quintiles.
WWOT works closely with our PFS, C&IS and NTGI partners to develop
innovative, effective systems and products for Northerns clients worldwide. We are able
to differentiate Northern Trust in the marketplace by providing clients with a wide array
of leading-edge technology solutions to meet their sophisticated business and personal
needs. Our strong financial position enables us to continue to make substantial investments
in the continued development of our technology platform, with nearly $870 million in
technology spending planned for 2003 to 2005, up from $854 million in the prior three years.
Northern has long recognized the importance of effective business
continuity and disaster recovery plans for supporting critical business needs and key
applications should an emergency occur. Following the September 11, 2001, terrorist
attacks, U.S. financial services and government regulatory agencies have focused on the
potential impact such attacks could have on our countrys financial structure and technical
systems. One important component to our business continuity strategy is the construction
of a new data center in the western suburbs of Chicago that we expect to open in
spring 2003. The center will provide capabilities for enhanced regional data recovery, workspace
for critical function data processing and dedicated disaster recovery workspace.
Recognition Received Money magazine in its January 2003 issue
named Northern Trust as one of the 10 Best Investments for 2003. Money recommended
stocks and funds that their editors felt will brighten your portfolio for years to come.
According to the magazine, those selected not only are well positioned for an economic
recovery, but also offer downside protection if it doesnt materialize. For the fifth time,
Forbes magazine in 2003 named Northern as one of the Best Big Companies in America.
The Forbes Platinum 400 have the best balance of long- and short-term financial performance.
At a time when corporate ethics have come under unprecedented
scrutiny, we were pleased in 2002 to be named as one of the 100 Best Corporate Citizens
by Business Ethics magazine. This is the second time that Northern has received this designation.
The Northern Trust Company in 2002 received an outstanding
Community Reinvestment Act (CRA) rating from the Federal Reserve Bank of Chicago.
This marks the third consecutive outstanding rating that the company has earned for
CRA performance.
For the 11th year, Working Mother magazine named Northern to its list
of the 100 Best Companies for Working Mothers. The annual list recognizes companies
that acknowledge the value and needs of working families.
Northern in the Community During the past year, it was important
for Northern Trust to maintain the Corporations traditional commitment to serving the
communities where we do business, as these communities and their citizens faced the
significant pressure of operating in an unfavorable economic environment. Our commitment
included both financial support and an emphasis on volunteering wherever Northern has
a presence.
Northern Trusts charitable giving totaled $9.6 million, including more
than $3 million the Northern Trust Charitable Trust donated to a broad range of groups
and agencies. The trust supports effective, broad-based inclusive programs that serve a
diverse population through local nonprofit efforts. To maximize the impact of the contributions,
the trust concentrates on issues with potential for the greatest long-term effect
and, in 2002, focused on low- and moderate-income families.
Each year, Northern makes a special effort to support the United Way/
Crusade of Mercy. The 2002 United Way campaign was entitled You Make a Difference,
and more than 76 percent of the Chicago-area staff responded by contributing nearly
$1.7 million. Northerns corporate contribution was $850,000bringing the total to more
than $2.5 million donated to over 400 health and human care agencies in the Chicago area
that serve more than 2 million people. In recognition of Northern Trusts efforts, we were
awarded the Community Spirit Award, the United Ways highest tribute to an organization.
The Corporations community involvement ranged from home-buyer
seminars for Chicago police officers to Florida staff members assisting with a Habitat for
Humanity home-building project to Northerns attorneys providing pro bono legal services
to the disadvantaged. This is just a small sampling of the efforts of the literally thousands
of Northern people who volunteered more than 100,000 hours of community service last
year to assist a wide array of educational, human services, civic and cultural organizations.
To be part of Northern Trust is to be part of the communities we serve.
Management Changes In 2002, we saw a number of management
changes at Northern. Some individuals chose to retire after long careers with the Corporation,
and talented professionals moved into new positions with increased responsibilities.
President Barry G. Hastings retired at year end after a distinguished
29-year career both in Chicago and Florida. Barry brought to his role an unfailing commitment
to client service, which he will continue in his new role as non-executive Chairman of
Northern Trust Bank of Florida N.A. James J. Mitchell, PresidentWorldwide Operations
and Technology, retired in May 2002 after 38 years of service. He was succeeded by
Timothy J. Theriault. Tim, who joined Northern in 1982, previously was Chief
Technology Officer.
In May, we announced the expansion of our Management Committee
to include four new members: William L. Morrison, Chairman and CEO of Northern Trust
Bank of Florida N.A. and President, PFSNational; Alison A. Winter, Executive Vice
President and President, PFSMidwest; Timothy J. Theriault, PresidentWWOT; and
Timothy P. Moen, Executive Vice President and head of Human Resources. These talented
individuals bring to the Committee significant business experience and will contribute
importantly to the management of the Corporation.
In December 2002, Executive Vice President Steven L. Fradkin, who has
served as head of the International Group and Global Funds Services practice in C&IS,
assumed the new position of Executive Vice President of Finance, reporting to Perry R. Pero,
Vice Chairman and Chief Financial Officer. Steves responsibilities were assumed by
Stephen N. Potter, who will continue in his role as General Manager of our London branch
and President of Northern Trust Global Investments (Europe) Limited.
Acknowledgements At Northerns Annual Meeting in April 2002,
shareholders elected John W. Rowe to the Corporations Board of Directors. John is Chairman,
President and Chief Executive Officer of Exelon Corporation. Exelon is a Chicago-based
company formed in 2000 by the merger of Unicom Corporation and PECO Energy and is
one of the nations largest electric utilities. We thank all of our directors for the strong
leadership and sound guidance they bring to the board.
Last June, we were saddened by the death at age 93 of Edward Byron
Smith, Honorary Chairman of Northern Trust. Mr. Smith was named President of Northern
Trust in 1957, Chairman of the Board and CEO in 1963 and became Honorary Chairman in
1981. He was an exceptional leader and a distinguished Chicagoan who served Northern
for a remarkable 70 years. He will be greatly missed.
On behalf of the entire Corporation, I would like to express sincere
appreciation to clients worldwide whom we have had the privilege of serving this past
year. Your loyalty strengthens Northerns commitment to bringing you the highest level of
service in the years ahead.
Northerns achievements during 2002 would not have been possible
without the exceptional efforts of more than 9,000 partners worldwide who, despite a
challenging year, kept their focus on serving clients and each other with professionalism
and good spirit. To each of them, we extend our sincere thanks.
There are certainly challenges ahead, but we at Northern Trust remain
confident that the Corporation is well positioned to address those challenges with the
ongoing support of our shareholders, directors, clients and staff.

William A. Osborn
Chairman, Chief Executive Officer and President