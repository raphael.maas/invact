
To Our Stockholders,
This has been an exciting and challenging year for MetroPCS, full of accomplishment
and innovation. We are proud of where we are today and the growth we have
achieved to date. We firmly believe MetroPCS is well positioned for 2010.
Accomplishments during 2009
 Added more than one million net subscribers for the fourth consecutive year
and served over 6.6 million subscribers as of the end of 2009
 Became the fifth largest facilities-based wireless carrier in the U.S based on
number of subscribers
 Launched the New York City and Boston metropolitan areas, and continued to
expand service in our other metropolitan areas
 Expanded our roaming coverage area through the addition of strategic
roaming partners enabling our customers to expand their use of our
nationwide services in an area covering more than 215 million people
 Introduced our unlimited international calling plan
 Generated both record revenue of approximately $3.5 billion and record
Adjusted EBITDA for the full year of over $950 million
 Offered new handsets including the Blackberry Curve, the Samsung
Finesse and the Samsung Code
 Announced our selection of long term evolution, or LTE, as our fourth
generation network technology, and announced our initial LTE infrastructure
and device vendors for our initial launch of services in selected metropolitan
areas planned for the second half of 2010
One of the most exciting milestones of 2009 was the concurrent launches of the New
York City and Boston metropolitan areas, successfully introducing the MetroPCS
value proposition to a large new population base. Our coverage in the boroughs of
New York City and in Boston is outstanding, and, after initially launching service in
early 2009, we have continued to significantly expand the footprint to cover areas
around New York City, including Long Island, White Plains, NY and Bridgeport, CT
and to expand our footprint in the Boston area to include Worcester, MA, most of the
North and South shores, Providence, RI and areas in New Hampshire, including
Manchester and Portsmouth. We continue to believe there is significant growth
opportunity within these areas.

In June 2009, with a focus on innovation and providing increased value to our
subscribers, we introduced our innovative, unlimited international calling service. This
ground-breaking service enables subscribers to place unlimited calls to over 100
countries. We continue to be pleased with the interest we see for this service
offering.
The Pioneer - the Competitive Marketplace
Pay-in-advance, no long-term contract (no contract) wireless service continued to gain
acceptance during the year as consumers sought out value. MetroPCS has grown
quickly since first launching service in 2002 and, as a pioneer in the unlimited, no
contract, pay-in-advance space, we have helped redefine the wireless experience
for millions of subscribers. Since we began offering services eight years ago, we
changed how wireless services are sold. From our initial launch of service, we have
transformed the company from a provider focused on providing local and regional
service to one focused on providing a nationwide service usable in more than
215 million POPs at the end of 2009. We are proud of where we are today and the
growth we have achieved to date.
Throughout 2009 we increased our subscriber penetration while facing the significant
headwinds of a challenging economy and an increasingly competitive environment.
Voice continues to go wireless, and we increasingly see the Web going mobile. Our
success in offering predictable, affordable and flexible service has been noticed by
other wireless operators. Initially, we faced little competition in serving our
subscribers with unlimited service offerings. As direct competition in our service
offerings has developed, we have met the challenge by leveraging our low cost
structure business model. Now more than ever, we remain focused on delivering the
most value for our subscribers dollar and making adjustments that differentiate us
while driving continued growth and profitability. Now as we progress towards our
planned LTE launch later this year, we look forward to offering subscribers wireless
Web services that will satisfy the ever growing demand for access to Internet content
and services on an unlimited, no contract basis.
With approximately 1.3 million net subscriber additions in 2009, based on third-party
studies, our wireless offerings showed an increase in the share of decision on a yearover-
year basis within our operating markets. In fact, based on industry studies,
within the entire wireless segment, unlimited, no contract, pay-in-advance wireless
net subscriber additions gained more share than traditional post-paid wireless net
subscriber additions in 2009. Looking specifically at MetroPCS operating markets,
according to a third-party study, MetroPCS had more gross additions than any other
wireless carrier in the marketplace today. Our continued growth in 2009 resulted
from:
 The value we continue to provide our subscribers

 Continued operational successes, including our expansion of coverage into
the Northeast
 New product and service offerings such as unlimited international calling
 Our expanded handset lineup which includes Smartphones
 Our footprint expansion through our expanding strategic roaming agreements
Financial Health
Maintaining a strong balance sheet and generating substantial Adjusted EBITDA
enables us to further develop services and products, deploy LTE technology, and
expand service coverage. At the end of 2009, we had approximately $1.2 billion
in cash and hort term investments. We also generated both record revenue of
approximately $3.5 billion and record Adjusted EBITDA for the full year of over
$950 million, representing growth rates of 27% and 22%, respectively, over the prior year.
We reported healthy full-year 2009 operating and financial results that demonstrate
the underlying strengths of our business and our ability to manage effectively in what
has been a challenging environment. We are focused on building long-term value
for our shareholders as well as providing our customers the best value for their dollar.
Profitable growth is the cornerstone of our business, and as we continue to grow, we
actively monitor and manage our Cost Per Gross Addition (CPGA). For the full year
2009, we reported a consolidated CPGA of approximately $146 which was less than
half the average CPGA of the national wireless carriers. With increased awareness of
the value of unlimited offerings, it is important we articulate our unique value
proposition to the consumer, and we have launched an advertising campaign
intended to support this goal. With the value inherent in our plans, our robust handset
lineup, and strong, relevant coverage, we are marketing our strengths and driving
customers into our stores.
Innovations
We are truly delivering Wireless for All. In early January 2010, we unveiled our
Wireless for All family of tax inclusive service plans. Under these exciting new plans,
our prices include all applicable taxes and regulatory fees. These plans position us
as the best deal in town and support our goal to be predictable, affordable, flexible
and simple. Additionally, we eliminated our first month free policy, while
correspondingly reducing our retail handset prices. We believe these changes will
lead to increased customer satisfaction and help drive growth and increase customer
retention. Just as we pioneered no long-term contract, flat-rate unlimited services, we
remain committed to delivering innovative, competitive service plans with a highquality
wireless network and more choices of affordable and desirable phones.
We are even more excited about our plans to launch our 4G LTE network during the
second half of 2010. Our LTE network is expected to provide a substantial
improvement in data-throughput over existing 3G technologies, and will increase

capacity and enable us to offer an increasing array of new services and applications
to smartphones. Ericsson and Samsung Telecommunications, both world-leaders in
providing telecommunications equipment, are our initial infrastructure providers for
our LTE networks. In addition, Samsung Telecommunications is expected to provide
our initial dual mode CDMA/LTE handset. With our initial launch of our LTE network,
and as the Web increasingly goes mobile, we expect to offer true mobile broadband
to our subscribers, ushering in a renaissance within the wireless industry. As
Smartphones and other mobile devices utilizing LTE become increasingly featurerich,
I believe more expansive change in wireless could occur over the next decade
as a result of LTE than we have experienced over the past two decades.
Handsets, greater variety and functionality
As phones become increasingly sophisticated in functionality, we are focused on
enhancing our handset lineup. We are offering more choices of handsets 
Smartphones and messaging and feature-rich handsets  than ever before. In
particular, Smartphones, QWERTY keyboard, and Web-enabled devices are
becoming one of the foundational elements of services we provide. In fact, voice now
accounts for less than half of our traffic, as we see texting and Web service
dominating our traffic. In spring of 2009, we launched the BlackBerry Curve 8330
Smartphone and our first touch-screen phone, the Samsung Finesse. In October,
we launched the Samsung Code, representing our first Windows Mobile 6.1
powered handset. We believe subscribers will continue to demand these types of
handsets, and we plan to continue to add new and compelling handsets to our
portfolio throughout 2010.
Nationally, as more subscribers use smartphones and other advanced handsets,
some national wireless carriers are experiencing data capacity challenges.
Subscribers demands for data and their rate of adoption of advanced handsets have
outpaced the capabilities of some other existing mobile networks. We continue to be
pleased with our network performance and its proven ability to deliver outstanding
service that meets the needs of our current subscribers. Looking forward, we believe
subscribers demand for broadband data and network capacity will only increase, and
we intend to offer not only a compelling choice of smartphones and advanced
handsets, but importantly, provide an LTE network that meets these growing
demands.
Our strength, our future
As the competitive environment in which we operate intensifies, we believe our core
strength in managing our low-cost network while profitably increasing subscriber
penetration clearly demonstrates that MetroPCS is changing the marketplace. We
intend to be in this business for the long-term and our focus on innovation and
offering our subscribers a post-pay wireless experience on a no contract pay-inadvance
basis has become increasingly important. Our strategy of offering

affordable, unlimited wireless communications is clear. We firmly believe MetroPCS
is well positioned to compete and succeed, and that our success will be achieved by
offering a full range of services available in the industry to our subscribers and by
continuing to manage our industry leading low-cost operating structure. This should
enable us to continue generating increasing growth and cash flows, while continuing
to offer innovative products and services and enhance our competitive position in the
industry.
I thank you for your trust and investment in us. I also congratulate all of our
employees on another year of solid work and importantly, I challenge them to deliver
another year of success and quality execution.
We are the leader in providing unlimited, no contract services to our subscribers and
consumers throughout the U.S. We will continue to change the way services are
provided, by delivering Wireless for All.
Sincerely,
Roger D. Linquist
Chairman, President and Chief Executive Officer
