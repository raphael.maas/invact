To Our Shareholders

The year 2015 marked a major growth milestone for Scripps Networks Interactive,
particularly for our international operations. The company made its biggest
international acquisition to date in the shape of TVN, the leading multi-platform
media organization in Poland. The agreement was a significant advance in our
global ambitions, and established a solid platform for future growth in one of
Europe�s most vibrant economies and regions.
In the United States, our television networks enjoyed another strong year, once
again outperforming our peer group, and continuing to attract the upscale, engaged
audiences coveted by our advertisers.
We�ve met the challenge of changing media consumption habits by making our
valued content available on a growing array of delivery platforms including
traditional television, computers, tablets, smart phones, essentially anywhere and
anytime the consumer desires.
We�ve made smart, strategic business moves that have positioned the company for
continued growth and success. Our business model remains strong and our brands
are more respected and beloved than ever. People view our content and visit our
websites and mobile apps in increasing numbers.
Advertisers want to work with us and distributors consider our channels as the
kind of �must-carry� brands their customers demand. All these paths lead to one
result: a vibrant, growing company that consistently delivers compelling value for
shareholders year after year. 

GROWING OUR GLOBAL PRESENCE
On the international front, 2015 was nothing short of transformational for our company.
The addition of TVN provided immediate scale and made Scripps Networks Interactive
a major player in the global media marketplace. And while TVN was the year�s big
international headline, the company experienced substantial growth in other parts of the
world.
Food Network, our most widely distributed global brand, is now available in more than
150 countries and territories. It�s the No. 1 lifestyle channel in the UK and has seen
substantial success in Southeast Asia and, more recently, in Latin America and Australia.
Travel Channel, our second most-distributed brand globally, is viewed in more than 130
countries and territories across Europe, the Middle East, Africa and the Asia-Pacific
region.
HGTV debuted for the first time outside the United States about a year ago, and since then
the network has become the No. 1 lifestyle channel in Singapore and is growing rapidly in
Malaysia and the Philippines.
Additionally, our international-only brands � Asian Food Channel (AFC) and Fine Living
Network�enjoyed strong growth in 2015. AFC allows us to develop locally produced
shows specific to Asian markets, while with Fine Living Network, we can showcase the
best of our content from the home, food and travel categories on a single network in
markets where that approach works best.
RECORD GROWTH IN THE U.S.
Our U.S.-based networks performed exceptionally well in 2015. HGTV hit new highs and
finished the year as the No. 1 television choice for upscale women. This was the tenth
consecutive year in which the network achieved this, reflecting its essential position
with the female consumers who make 80 percent of U.S. household purchasing decisions.
Overall, HGTV ranked 8th among all ad-supported cable networks in primetime for adult
viewers aged 25 to 54, up from 11th a year earlier. Viewers tuned in in record numbers
to popular HGTV shows like Property Brothers, Flip or Flip, Fixer Upper and perennial
favorite, House Hunters. 

Food Network claimed a Top 10 position among its target audience of women aged 25 to
54, and moved up one slot to 11th among adult viewers. The 11th season of Food Network
Star scored double-digit ratings growth, and did exceptionally well among younger
viewers aged 25 and 34. Other top performing shows included Chopped, Holiday Baking
Championship and Great Food Truck Race.
Travel Channel ended 2015 on a positive note, with fourth quarter ratings growing 5
percent. New seasons of Expedition Unknown, Bizarre Foods, and Booze Traveler posted
large increases from the prior year, and were off to a strong start at year�s end.
Cooking Channel continued its phenomenal growth as adult ratings surged 13 percent,
driven primarily by re-airs of popular Food Network fare including Cutthroat Kitchen
and Guy�s Grocery Games, completed by Cooking Channel originals like Unique Eats and
Sweet Genius.
Ratings at DIY Network declined slightly in 2015 following many years of positive
growth. Top performing programs included Tiny House Big Living, Building Belushi and
The Vanilla Ice Project.
Great American Country continued to find its niche as a lifestyle network, with viewership
primarily driven by content from the vast Scripps Networks programming library. Flea
Market Flip and Going RV were among the network�s most watched shows, and helped
drive an increase in audience median income, an important mark for advertising sales.
BEING THERE ANYTIME, ANYWHERE
The company made positive strides in 2015 to expand availability of our content at the
time and on the device of the consumer�s choice. We remain a staunch supporter of the
cable industry�s �TV Everywhere� initiative, aimed at enabling the distributors� consumers
to access video content on laptops, tablets and smartphones. Usage of our branded mobile
apps soared during the year as TV Everywhere consumption continued to expand.
At the same time, we broadened our reach beyond traditional distribution platforms.
With the creation of Scripps Lifestyle Studios, we can quickly adapt and generate more
on-demand and short-form content specific to the platform on which it�s consumed. Our
content partnerships with Snapchat and Apple News reflect our continued progress in this
area.

CONSISTENT FINANCIAL PERFORMANCE
The company once again reached record revenue and profit levels. Total consolidated
revenue increased 13.2 percent, to $3.0 billion, due to solid growth in advertising and
affiliate revenue, as well as a significant increase in international revenue as a result of the
TVN acquisition and growth in other markets. Segment profit improved by 11.0 percent, to
$1.2 billion.
We�ve built powerful brands across the world that connect deeply with consumers. We�re
growing revenues as we monetize audiences on all platforms. The solid, consistent
performance that our company exhibits quarter-after-quarter validates the efforts of
our incredible teams around the world as we continue to deliver value for consumers,
distributors, advertisers and shareholders.
I want to thank you again for your interest in Scripps Networks Interactive.
Sincerely,
Kenneth W. Lowe
Chairman, President and Chief Executive Officer