TO OUR STOCKHOLDERS
We are pleased to report that we achieved record gross sales for the 19th consecutive year. In
2011, our gross sales were $1.95 billion, compared to $1.49 billion in the previous year. This
achievement was primarily attributable to increased sales of Monster Energy drinks both
internationally and in the United States and in particular to our new Monster Rehab line which
was originally launched with one product in March 2011. Notably, we continued to expand the
distribution of our Monster Energy brand into new international markets. Monster Energy
drinks are now sold in more than 70 countries and territories outside of the United States.
During 2011 we introduced a number of new beverages including:
Monster Rehab Tea + Lemonade + Energy
Monster Rehab Green Tea + Energy
Monster Rehab Rojo Tea + Energy
Monster Rehab Protean + Energy
Peace Tea Caddy Shack
We also introduced a number of new beverages and line extensions to our Hansens, Blue
Sky and Huberts brands.
We launched our new non-carbonated Monster Rehab tea + lemonade + energy, energy drink,
which contains electrolytes and additional supplements, in the United States in March 2011.
Following the success achieved with this drink, we expanded the Monster Rehab line with the
introduction of three additional Monster Rehab drinks in the fourth quarter of 2011. In March
2012 we launched a new non-carbonated Monster Rehab tea + orangeade + energy, energy
drink, which is the fifth product in our Monster Rehab line. We intend to expand the Monster
Rehab line to Canada, Mexico and to a number of countries in Europe and elsewhere this
year.
We also launched bermonster energy drinks in 500ml wide mouth glass bottles in the first
quarter of 2012. bermonster energy drinks are non-alcoholic and are manufactured using a
brewed fermentation process.
We intend to continue our innovation and introduction of new Monster Energy drinks as well
as other beverages in 2012, with continued emphasis on lower calorie drinks to meet the
increasing demand from consumers for such products.
In 2011, gross sales outside of the United States increased to $381.0 million from $240.6
million in the prior year. During 2011, we launched Monster Energy drinks into many new
countries including Colombia, Cyprus, Denmark, Estonia, Greece, Iceland, Latvia, Lebanon,
Lithuania, Malta, Mauritius, Portugal, and Tahiti and commenced limited sales in Russia and
Ukraine. In the first quarter of 2012 we launched Monster Energy drinks in Poland. We plan
to expand the distribution of Monster Energy drinks to additional countries in Central and
Eastern Europe as well as to additional countries in South America and Asia in 2012 in order to
achieve our goal of making Monster Energy a global brand.
The energy drink category in the United States continues to grow in excess of the beverage
category in general, which continues to reflect positively for the future of our Company. The 
macro environment continues to remain challenging, but we are nonetheless optimistic about
our ability to continue to grow the Monster Energy brand and achieve increased sales in 2012.
Sales of Peace Tea iced teas continue to grow and we are optimistic that we will continue to
achieve increased sales and market share for our Peace Tea brand in 2012.
Once again, I would like to express my gratitude for the support afforded to the Company by
Mr. Hilton Schlosberg, our President and Chief Operating Officer, and Mr. Mark Hall, President
of our Monster Beverage Division. I would also like to express my personal thanks to our
consumers, stockholders, customers, distributors, and suppliers for their continued support. To
all of our management and employees, my sincere thanks and appreciation for all their efforts,
which are evidenced by the continued success of our Company. To our stockholders, thank you
for the trust you have placed in our management team.
Sincerely,
Rodney C. Sacks
Chairman and Chief Executive Officer