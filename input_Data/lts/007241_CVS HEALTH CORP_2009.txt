
    Dear Fellow Shareholder,




    Health care delivery in the United States is slowly evolving to a new
    model, and we believe that companies best positioned to promote better
    outcomes and lower costs will be among the winners. That thinking guided
    the 2007 merger of CVS and Caremark. Today we're the largest pharmacy
    health care company in the United States, and our strengths across the
    spectrum of pharmacy care are helping us deliver savings and improve the
    plan member experience for PBM clients.

    I'll have more to say about the success of our differentiated   OUR INTEGRATED MODEL BUILDS ON EXISTING
                                                                    PBM STRENGTHS
    approach as well as the core strengths of our PBM and
                                                                    Building on Caremark's long-standing reputation for
    retail businesses. First, let me provide a quick review
                                                                    customer service, clinical excellence, and an ability to
    of our solid financial performance in 2009. In the midst
                                                                    control payor costs, our integrated approach to phar-
    of a challenging economic environment, CVS Caremark
                                                                    macy care is yielding substantial benefits. Our more than
    reported record revenue and earnings. Total revenue rose
                                                                    26,000 pharmacists, nurse practitioners, and physician
    13 percent to $98.7 billion, with income from continuing
                                                                    assistants drive our efforts every day. Based on internal
    operations up 11 percent to $3.7 billion. We generated
                                                                    surveys, colleague engagement stands at an all-time high.
    approximately $3 billion in free cash flow, deploying part
                                                                    Their commitment is clearly reflected in our outstanding
    of it to complete a $2 billion share repurchase program.
                                                                    performance across a number of measures.
    Furthermore, the board of directors authorized an addi-
    tional $2 billion share repurchase program, which we            I'm pleased to report that we have done an outstanding
    began in 2009 and intend to complete in the first half of       job of controlling costs for our PBM clients. We've done
    2010. Our strong free cash flow also allowed us to raise        this by driving generic utilization through unique plan
    the dividend by 15 percent for 2010. This marks our             designs, by controlling specialty pharmacy trend through
    seventh consecutive annual dividend increase. Over              our specialty guideline management programs, and by
    this period, our dividend has risen at an 18 percent            improving adherence to prescribed medications through
    compound annual growth rate.                                    our multiple points of contact with plan members. For
                                                                    many people, a face-to-face interaction with their phar-
    CVS Caremark shares produced a total return of 13 percent
                                                                    macist results in a significantly higher adherence rate.
    for 2009, although we trailed the broad market averages
    due largely to some concerns over our PBM's near-term           We have a broad research effort underway to learn more
    growth outlook. However, over the past five years,              about why patients do not take prescriptions that are
    CVS Caremark delivered a total return to shareholders           prescribed or drop medications in the middle of therapy.
    of 48 percent, while the S&P 500 and the Dow Jones              The research includes continuing internal analysis of
    Industrial Average returned 2 percent and 10 percent,           CVS Caremark client utilization and an external partner-
    respectively, over the same period.                             ship with Harvard and Brigham and Women's Hospital.
                                                                    Separately, a recent study by the New England Healthcare




Institute found that not taking medications as prescribed    We currently serve 2,200 PBM clients, and more than
leads to poorer health, more frequent hospitalization, a     480 have signed up for Maintenance Choice to date. That
higher risk of death, and as much as $290 billion annually   leaves significant upside as more existing and prospective
in increased medical costs across the U.S. health care       clients begin to appreciate its benefits.
system. As the health care debate continues, one thing is
                                                             WE ANTICIPATE HIGHER RETENTION LEVELS AND
clear: We all have to find ways to make health care more
                                                             MORE NEW BUSINESS OPPORTUNITIES FOR 2011
affordable. CVS Caremark is focusing on adherence to
                                                             Obviously, we've faced some headwinds as well in our
impact the health of our customers and help take costs
                                                             PBM business. We won $11 billion dollars in new PBM
out of the health care system.
                                                             revenues over the past two years, but we also lost a similar
                                                             amount of existing business over this same time-frame as
In 2010, we expect adherence rates to be further
                                                             a result of some unique circumstances affecting a handful
enhanced when our Consumer Engagement Engine (CEE)
                                                             of accounts. Client renewals for 2011 are looking strong,
goes live across our major channels during the second
                                                             though, and I'm confident that retention will return to
half of the year. Powered by clinical rules, the CEE will
                                                             Caremark's historically high levels.
identify opportunities to promote better health outcomes
and to achieve costs savings across our unparalleled
                                                             On the new business front, we took steps to reposition
points of contact.
                                                             our sales message to focus first on our industry-leading
                                                             PBM capabilities. We're talking to clients about how we
The marketplace has enthusiastically embraced our
                                                             are able to lower their costs and improve the plan member
innovative Maintenance Choice offering, which gives
                                                             experience. That is resonating well during the 2011 selling
eligible plan members access to 90-day mail pricing
                                                             season, and there are a significant number of large pros-
whether they receive their prescriptions through the mail
                                                             pects out to bid.
or choose to pick them up at one of our approximately
7,000 conveniently located retail locations. This offering
                                                             With Howard McLure's retirement, I'm delighted that we
eliminates the plan member disruption that many payors
                                                             were able to hire Per Lofberg as our new PBM president.
face when they consider mandatory mail programs for
                                                             Per, who joined us in January 2010, brings more than
cost savings. Furthermore, clients that switched from a
                                                             30 years of experience in the health care and PBM indus-
voluntary mail program to Maintenance Choice saw their
                                                             tries. Formerly chairman of Merck-Medco Managed Care
generic dispensing rates (GDR) improve and achieved
                                                             LLC, which later became Medco Health Solutions, he
savings of up to 6 percent of their pharmacy costs.

                                                                                                
    most recently served as president and CEO of our strategic     our focus on private label, our category mix, and the
                                                                   ExtraCare card. We have a solid track record at making
    partner, Generation Health, Inc. Per is widely respected
    in the industry, and his expertise, along with his demon-      the most of our acquisitions, roughly doubling the profit-
    strated ability to execute growth strategies, makes him        ability of the drugstores we acquired from JCPenney in
    the perfect person to guide our PBM in this evolving           2004 and from Albertsons in 2006.
    health care environment.
                                                                   EXTRACARE AND OTHER LONG-TERM INVESTMENTS
                                                                   HAVE HELPED DRIVE PROFITABILITY
    OUR SAME-STORE SALES GROWTH LEADS ALL
                                                                   We can trace our industry-leading performance in no
    PHARMACY RETAILERS
    Despite the recession, our retail stores put up outstanding    small part to the many investments we have made over
    numbers. Same-store sales rose 5.0 percent, while pharmacy     the past decade in technology, in enhancing the layout
    same-store sales increased 6.9 percent. These results led      and "shopability" of our stores, and in driving customer
    our industry throughout 2009, and we gained significant        loyalty. The ExtraCare loyalty program, which we rolled
    market share. Organic growth continued apace as we             out in 2001, is today the most popular among all retailers.
    opened 287 new or relocated stores. Factoring in closings,
                                                                   More than 64 million active cardholders take advantage
    net unit growth was 102 stores. Today, approximately
                                                                   of sales in the store and at CVS.com, and they received
    75 percent of the U.S. population lives within three miles
                                                                   $1.9 billion in ExtraCare savings and Extra Bucks rewards
    of a CVS store. Our stores fill nearly one in five prescrip-
                                                                   throughout 2009. ExtraCare represents a significant com-
    tions nationwide, and we have the #1 or #2 market share
                                                                   petitive advantage for us, and CVS Caremark has a huge
    in 14 of the top 15 U.S. drugstore markets.
                                                                   head start over any drug retailer contemplating its own
    Our industry-leading customer service and the use of           loyalty program.
    advanced technology, combined with the increasing
                                                                   More recent investments position CVS Caremark for
    adoption of Maintenance Choice by PBM clients, all
                                                                   greater profitability in the coming years. For example,
    contributed to our pharmacy growth.
                                                                   our proprietary RxConnectTM computer system, whose
    In the front of the store, we gained share in 82 percent       rollout will be completed during 2010, should improve
    of our core categories. Moreover, sales of private-label and   both efficiency and customer service in our pharmacies.
    CVS-exclusive brands rose faster than they have historically   We've also opened call centers that allow us to redirect
    to account for nearly 17 percent of our front-end total.       much of the telephone call volume from our busiest stores.
    These lower-cost products offer excellent value, which         That frees up retail pharmacists to spend more time
    clearly appealed to cost-conscious consumers in the midst      counseling patients face-to-face.
    of a recession. We, in turn, benefited from the higher
                                                                   WE'VE EXPANDED MINUTECLINIC'S OFFERINGS AND
    margins these products provide compared with national
                                                                   FORGED NEW ALLIANCES
    brands. Our private-label program is ambitious, and
                                                                   At MinuteClinic, our retail-based health clinics, we expanded
    we added more than 900 offerings to our shelves
                                                                   the services offered, further integrated MinuteClinic into our
    during the year.
                                                                   PBM offerings, and forged a number of strategic alliances
                                                                   with highly regarded health care providers such as Humana,
    The Longs Drugs stores we acquired in October 2008
                                                                   Inc., and the Cleveland Clinic. Today we have approxi-
    were integrated on schedule and are on track to be
                                                                   mately 570 clinics in 56 markets across the country.
    accretive to earnings in 2010. Profitability is already
    on the rise as we've begun to leverage our systems,



In 2010, a key focus will be adding protocol-driven             Looking at demographics, the number of people in the
monitoring services for common chronic illnesses, such as       United States who are 65 or older will jump to roughly
diabetes, hypertension, and high cholesterol. That will be      47 million by 2015 and to 55 million by 2020. This age
done in coordination with a patient's treating physician        group fills an average of more than 25 prescriptions per
and is designed to improve adherence and outcomes.              person annually  30 percent more than people between
We also expect to offer additional acute care services          the ages of 55 and 64. That will increase utilization
and improved point of service lab tests. Under its new          dramatically for years to come and will help drive the
president, Andrew Sussman, M.D., this business is moving        growth of both our PBM and retail businesses.
in the right direction.
                                                                Of course, the opportunity in generics is enormous.
                                                                Nearly $100 billion in branded drug sales will lose patent
CVS CAREMARK WILL CONTINUE TO BENEFIT FROM
NEW PRODUCTS AND BROAD INDUSTRY TRENDS                          protection over the next six years. As a result, our GDR
Looking ahead, we will continue to offer new and                could eventually approach 80 percent. That is expected
innovative services that help attract and retain PBM            to further reduce costs for health plans, plan sponsors,
and retail customers. For example, we plan to expand            and their members while expanding our pharmacy
pharmacogenomic clinical and testing services for CVS           margins. We are also hopeful that Congress will pass
Caremark PBM clients through our ownership stake in             legislation that finally paves the way for a biogeneric
Generation Health. We want to improve care for patients         approval process.
who are either non-responsive to their medications or
                                                                As you can see, we're very optimistic about our pros-
who experience adverse reactions as a result of their
                                                                pects, both in the short-term and long-term. Pharmacy
genomic makeup. We expect to begin offering these
                                                                health care in this country has a bright future, and we
services during 2010.
                                                                believe our combined assets will lead to a bright future
We are currently in the process of transitioning our iScribe   for our company for years to come. On behalf of our
clients to Allscripts, the largest provider of e-prescribing    board of directors and our 211,000 colleagues across the
and electronic health record solutions. E-prescribing can       country, thank you for investing in CVS Caremark.
significantly reduce medication errors. With this partner-
ship, we hope to accelerate the adoption of e-perscribing
across our client base.

Broad industry trends will work in our favor as well, from
potential health care reform and an aging population to
                                                                Thomas M. Ryan
new blockbuster and generic drug introductions. CVS
                                                                Chairman of the Board,
Caremark has been participating in the national debate
                                                                President, and Chief Executive Officer
over legislation to reform the U.S. health care system. We
believe the right combination of reform and expansion           February 26, 2010
will be good for the nation. As the largest provider of
cost-effective pharmacy care in this country, we stand
ready to support this effort.


