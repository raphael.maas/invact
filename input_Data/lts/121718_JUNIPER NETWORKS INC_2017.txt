A Message from Rami
Dear Fellow Stockholders,
Reflecting back on 2017, Juniper made progress in a number of key areas, which 
included increased traction with many of our top customers, the release of new 
innovations that enhanced our competitive advantage, and improved momentum 
within our security business and the strategic enterprise vertical. While lumpiness 
at several of our cloud customers negatively impacted our results in the second 
half of the year, I am proud that we delivered another year of revenue and non-
GAAP earnings per share
 1
 growth in 2017, and remain confident that Juniper 
Networks maintains the right products and strategy needed to capitalize on some 
of the largest opportunities in the networking market.
Well Positioned to Benefit from Cloud Transition
We believe Juniper is well positioned to benefit from the ongoing investment in 
cloud network architectures, which is not only occurring at the top public cloud 
companies, but also service provider and fortune 2000 enterprise accounts 
looking to capture network efficiencies. While a few of these large cloud 
operators are choosing to implement new �scale out� architectures that more 
heavily utilize lower-priced purpose-built products, we are leading the way to 
this new network architecture with our PTX and QFX platforms. We believe 
that helping our customers to enable this new network architecture, will allow 
us to maintain our industry leading footprint within many of the world�s leading 
cloud environments. While this decision to disrupt ourselves resulted in revenue 
headwinds in the second half of 2017, most notably in our cloud routing 
business, we believe this decision should position us to return to sustainable 
growth by the end of 2018.  
Executing on Enterprise and Telco Opportunities
We continue to make progress in the Strategic Enterprise vertical and see 
longer-term opportunities for revenue growth in the Telcom/Cable vertical. Our 
enterprise optimism is based on our recent results, which included mid-single 
digit growth in 2017 and a double digit quarter over quarter and year over year 
improvement during Q4 2017. This strength was driven by healthy demand 
for our enterprise switching portfolio and improved momentum within our 
security business, which returned to growth following a prolonged period of 
year over year declines.  With a series of new enterprise switches and security 
products now in the market, we remain optimistic the enterprise success we 
have experienced over the last few quarters will continue to carry forward into 
2018. Not to be overlooked, we believe we remain well positioned in the service 
provider market, as evidenced by IHS Markit�s report suggesting Juniper ranks 
first with global service providers in most areas. While service provider spending 
levels have remained muted for several years, we believe our strong positioning 
with these important customers should allow us to benefit in the event recent 
tax reform, regulatory changes and/or 5G deployments drive improved levels of 
investment over time.  
Best-in-Class Portfolio 
In 2017, we delivered on our innovation pipeline and ended the year with an 
outstanding product portfolio and a determination to enable our customers to 
fuel their cloud businesses or to successfully migrate to cloud architectures. 
Juniper�s core and edge routing portfolios are leading the market with 
significantly greater performance than our nearest competitor while integrating 
into our market-leading Juniper Contrail
�
 Networking SDN controller for 
agile, flexible edge services. While we are seeing some pressure in our routin
business, due primarily to the transition from MX to PTX at certain cloud 
customers, the strength of our product set is providing confidence in the long-
term outlook for this business. To this point, we believe the industry leading 
service richness of our MX platform will remain critical to our telco and cable 
customers, while the superior capacity and price performance of our PTX 
products are ideal to meet the needs of hyperscale operators looking to build 
scale-out network architectures. 
We continue to make progress in the data center and campus switching 
markets, with our overall switching business growing 12% in 2017. This was 
highlighted by ongoing success with our QFX switching portfolio which grew 
25% year over year due to ongoing success in cloud, telco and enterprise 
accounts. 
While our security business declined in 2017, we saw a return to growth during 
the December quarter that is providing confidence in the future trajectory of 
the business. We believe this success is being driven by our refreshed product 
portfolio with new price competitive and feature-rich firewall appliances, 
improved security management with Junos Space Security Director, and 
our Sky ATP SaaS offering, which catches zero-day threats with advanced 
deception techniques and machine learning capabilities. We believe that 
the actions we have taken are continuing to drive increased mindshare from 
customers, partners and analysts.
We continued to see success with our software solutions, including increased 
adoption of Juniper AppFormix
�
 which complements the analytics and 
machine learning capabilities of Juniper�s Contrail platform and helps 
customers enhance their cloud operations. Various independent research 
surveys have concluded that Juniper�s Contrail is one of the most widely 
adopted service provider orchestration solutions in the market. We also gained 
traction in the SD-WAN market securing three tier-1 service provider customers.  
As our customers look to Juniper to be a strong partner in helping them achieve 
their business results, we believe our services offerings will continue to be a 
differentiating asset as we move forward. Increasingly, customers need to 
supplement their own capabilities with a strong service partner to help deliver 
not only network innovation and performance, but also provide virtualization 
and security expertise. Juniper has the portfolio and expertise to meet our 
customers� needs.
I am excited about our entire portfolio of products and am pleased with the 
feedback from our customers.
Continuing to Drive Technology Leadership
Under the leadership of our new CTO, Bikash Koley, who has significant 
experience building and operating cloud scale networks, we are continuing 
to invest in new innovations that challenge the status quo in hardware, 
software and silicon. We believe our investments will not only extend Juniper�s 
leadership in the cloud vertical, but will also strengthen our value proposition 
to telco and enterprise accounts. We believe introducing products that deliver 
superior scale, price performance, security and operational simplicity are likely 
to resonate across a broad set of customers, and plan to introduce solutions 
which seek to achieve this objective across our key industry verticals.  
Deeply Connected to Our Customers 
One of the strengths I am most proud of is Juniper�s relationships with our 
customers. Fostering our deep engineer-to-engineer relationships enables 
us to have an intense and insightful understanding of our customers� precise 
requirements and needs. This, in turn, helps to further fuel our innovation 
engine, allowing Juniper to develop and bring to market the products that help 
our customers evolve their networks. Our close relationship with our customers can be found in the examples I 
highlight below:
�
As traditional telecom customers work to benefit from the innovations around 
virtualization, programmability and automation of their networks, Juniper 
is leveraging our deep-seated roots in network innovation, along with our 
experience from being on the leading edge of the early cloud developments, 
to help our telecom customers transform into distributed Telco-Cloud 
architectures. As our leading Cloud-Scale customers push the boundaries 
of networking, we continue to both learn from and support them while 
developing innovative solutions around programmability, automation and 
open architectures. 
�
Enterprise customers are looking to benefit from the cloud to assist in their 
digital transformation. This means building out their business offerings 
while increasing their agility to respond to changing market conditions. 
Juniper�s Cloud Enabled Enterprise architecture provides a comprehensive 
framework to extend the benefits of the cloud to the campus, branch and 
data center, delivering an integrated, evolutionary approach towards the cloud 
transformation.
Remain Committed to Operational Discipline
We believe that one of Juniper�s core strengths is our ability to consistently 
generate strong operating cash flow. For 2017, we generated $1.3 billion of 
cash flow from operating activities, compared to $1.1 billion in 2016. Capital 
expenditures of $151 million, declined 30% year over year, as we had fewer 
capital investment requirements. 
We ended 2017 with what I believe to be a healthy balance sheet and an 
optimized capital structure, which balances internal investments, capital 
return to our stockholders and the potential for value enhancing M&A. We are 
pleased that in 2017 non-GAAP operating expense
1
  as a percentage of revenue 
improved to 39.1% and we grew non-GAAP diluted EPS
1
 for a fifth consecutive 
year. We remain focused on earnings expansion with long-term consistency.
In 2017, we repurchased $720 million of shares and paid $150 million in 
dividends. We ended the year with approximately 377 million diluted shares, 
down 2% year over year. For 2018, we expect continued strong cash flow 
generation from operations. Following the passage of the U.S. Jobs and Tax 
Cuts Act we announced a new $2 billion buyback authorization, entered 
into a $750 million accelerated share repurchase program, and increased 
our quarterly dividend by 80% to $0.18 from $0.10 per share. Based on 
these terms, we currently plan to return more than 100% of free cash flow 
to shareholders in 2018 and remain committed to returning approximately 
50% of free cash flow to shareholders longer-term. We ended the year with 
approximately $4.0 billion in cash, cash equivalents and investments, up 10% 
year-over-year.
Responsible Corporate Citizens 
Juniper believes in building more than a network. We are focused on improving 
the quality of life for the next generation by strengthening the communities 
where we live and work. We are committed to being responsible corporate 
citizens and encouraging responsible practices in our operations and 
throughout our worldwide supply chain�ensuring that working conditions 
are safe, workers are treated with respect and dignity, and that processes 
and products are environmentally responsible. We are executing on this 
commitment by focusing on those areas where we believe we can have 
a meaningful impact�Product Sustainability, Supply Chain, Operational 
Excellence, and People and Communities�and include those issues that are 
most relevant to our business operations and stakeholders.  
We promote collaborative approaches in driving corporate social responsibility 
standards, transparency, and accountability. To this end, Juniper is a member 
of the CDP Supply Chain program, Responsible Business Alliance, and Raw 
Minerals Initiative. 
Looking Ahead  
As I look to 2018, I am energized by the opportunities we see from the shift 
towards the cloud and network automation. I believe our history as an 
innovation leader and our deep understanding of high-performance networking 
technology position us extremely well to capitalize on this industry transition. 
I want to thank our customers, partners and stockholders for their continued 
support and confidence in Juniper. Finally, I want to extend a very big thank 
you to our employees around the globe, who each play an important role 
in successfully executing our strategy and creating value for all of our 
stakeholders.
With deep appreciation,
Rami Rahim
Chief Executive Office