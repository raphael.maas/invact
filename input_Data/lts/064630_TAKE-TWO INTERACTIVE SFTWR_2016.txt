Fiscal 2016 marked another year in which Take-Two delivered strong results, driven
principally by positive momentum in our core offerings. We generated record digitallydelivered net revenue, including our highest-ever recurrent consumer spending, and
significant cash flow. Today, Take-Two is a global leader in the interactive entertainment
business, with some of the industry�s most commercially successful and critically
acclaimed series that engage and excite audiences around the world across all
relevant platforms.
OUR KEY ACHIEVEMENTS
� Grand Theft Auto V and Grand Theft Auto Online have exceeded our expectations in every quarter
since their release, and continue to expand their audience nearly three years after their initial launch.
Grand Theft Auto V remains the highest-rated title on PlayStation 4 and Xbox One, and is the �musthave� experience for gamers, especially as the installed base of new-generation consoles continues
to grow. To date, Grand Theft Auto V has sold-in more than 65 million units worldwide. Moreover,
engagement with Grand Theft Auto Online continues to increase, with fiscal 2016 revenues up yearover-year driven by Rockstar Games� ongoing release of free content.
� We released NBA 2K16, which remains the highest-rated and top-selling NBA simulation. The title
outperformed our expectations and is poised to become our highest-selling sports game ever, with
sell-in to date of nearly 7.5 million units - up double-digits versus the same period for the prior release.
NBA 2K has benefited from strong player engagement, and sales of the game�s virtual currency were
the largest contributor to recurrent consumer spending after Grand Theft Auto Online. During fiscal
2016, recurrent consumer spending on NBA 2K grew 64% year-over-year, driven both by online play
and the MyNBA2K companion app.
� We launched WWE 2K16, which has sold-in over 3 million units to date. Revenue from the title has
been enhanced by the success of its downloadable add-on content, including a Season Pass. During
fiscal 2016, we extended our long-term partnership with WWE, and look forward to many more years
of successful collaboration.
� We released XCOM 2, which was developed by the strategy game pioneers at Firaxis Games. The title
received stellar reviews, with IGN awarding it a 9.3 out of 10, PC Gamer a 94% out of 100%, and Game
Informer Magazine a 9.5 out of 10. Sales of the title have exceeded our expectations, and are higher than
the PC version of its predecessor, XCOM: Enemy Unknown, during the same period after launch.
� We launched 7 offerings for tablets and smartphones, including catalog titles such as Grand Theft Auto:
Liberty City Stories, new releases including NBA 2K16 and WWE 2K, and free-to-play apps supporting
our core releases, such as MyNBA2K16.
� We generated significant cash flow and ended the fiscal year with nearly $1.3 billion in cash and
short-term investments.
2
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2016 ANNUAL REPORT
SUCCESSFUL GROWTH STRATEGY
Take-Two�s strategy is to develop the highest-quality, most compelling interactive entertainment franchises in the
business, and deliver them on every platform around the world that is relevant to our audience. Complementing
our core business with digitally-delivered offerings that drive ongoing engagement with, and recurrent consumer
spending on, our titles after their initial purchase is one of our most important long-term growth and margin
opportunities and, therefore, a key strategic priority of our organization. Recurrent consumer spending also helps to
strengthen our results between front-line releases, while prolonging consumers� engagement with our franchises.
We now support virtually all of our new releases with innovative offerings designed to achieve this objective.
World-class creative teams: Creativity and innovation are among the core tenets of our organization, and are
integral drivers of our continued success. We have nearly 2,200 employees working in game development in 15
studios around the world � including some of the most talented visionaries in the business. The creative teams at
Rockstar Games and 2K are renowned for their consistent ability to deliver games that set new benchmarks for
excellence. Whether expanding proven franchises, launching new intellectual property or providing innovative ways
to keep consumers engaged, we have an unwavering commitment to producing the highest quality entertainment
experiences that captivate and delight audiences.
Diverse portfolio of industry-leading intellectual property: Take-Two is a financially strong, global interactive
entertainment enterprise with numerous successful franchises encompassing a variety of genres. Our diverse
portfolio of intellectual property includes 11 series with at least one five-million unit selling release, and 50 individual,
multi-million unit selling titles. Since 2007, we have added 9 new brands to our lineup, including such hits as
BioShock, Borderlands, L.A. Noire, WWE 2K and XCOM.
Capitalizing on growth of digital distribution: During fiscal 2016, we continued to capitalize on our industry�s
ongoing transition towards digital distribution. We generated record digitally-delivered net revenue of $697.7 million,
which grew 53% year-over-year and accounted for 49% of our total net revenue. Recurrent consumer spending
grew to its highest level ever, and accounted for 52% of our digitally-delivered net revenue or 25% of our total net
revenue. In addition to virtual currency for Grand Theft Auto Online and NBA 2K, recurrent consumer spending was
enhanced by a variety of offerings, including downloadable add-on content led by Borderlands, WWE 2K, Sid Meier�s
Civilization and Evolve; WWE SuperCard, which has now been downloaded more than 10 million times and is our
most financially successful, free-to-play mobile offering; and NBA 2K Online in China, which delivered record
revenues and now has over 31 million registered users. Our results also benefited from strong growth in full-game
downloads, with over 20% of units for new-generation consoles and over 90% of units for PC delivered digitally.
In addition, approximately half of our catalog sales for old-generation consoles are being delivered through digital
download. Digital distribution is disproportionately benefitting our catalog, as it gives consumers the opportunity
to buy older titles that no longer receive physical shelf space.
Expanding offerings for emerging platforms and developing markets: We are actively pursuing opportunities for
interactive entertainment on emerging platforms such as tablets, smartphones and virtual reality, and in developing
markets, such as China and Korea, with a highly disciplined approach. To date, our mobile releases have focused
primarily on popular catalog titles from both Rockstar Games and 2K, select front line releases and sports titles, and
companion apps that keep consumers connected with our core entertainment experiences. As mobile technology
continues to evolve, we expect to release our most groundbreaking and immersive new titles on every mobile device
that core gamers choose to embrace. We have what we believe is the best collection of owned intellectual property in
the business, and the extraordinary success of our products is breeding greater demand from consumers worldwide,
which in turn creates increased opportunities for our Company.
3
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2016 ANNUAL REPORT
Innovative marketing and global distribution: Creating groundbreaking games is only part of our formula for
success. Our marketing teams execute well-coordinated global campaigns that leverage nearly every form of media �
from traditional to social � to turn our product launches into tent pole events. We also work in lockstep with our key
retail partners, both brick-and-mortar and online, to create promotions that drive consumer engagement at the
point of sale. Our global distribution network ensures that our products are available to consumers throughout the
world, both physically and digitally, and on all relevant platforms. In addition, the rise of video games as a spectator
sport is unquestionably an exciting trend in our industry. This year, we launched our first eSports tournament in
support of NBA 2K16, where more than 100,000 teams competed in 2.3 million games to win $250,000 and a trip to
the NBA Finals. We view eSports as a great marketing tool and an additional way to gain exposure for our games
and expand our audience. We will continue to explore ways to leverage our properties in this growing segment of
the industry.
Sound financial foundation: With approximately $1.3 billion in cash and short-term investments as of March 31, 2016,
Take-Two has the strongest balance sheet in the Company�s history and ample capital to pursue a variety of
investment opportunities. Our first priority is to enhance the long-term growth and profitability of our business,
both organically and potentially through acquisitions. We also have the ability to return capital to shareholders.
During fiscal 2016, Take-Two deployed $26.6 million to repurchase approximately 954,000 shares of our common
stock. Under our existing share repurchase authorization we can still repurchase up to approximately 9 million
shares as of March 31, 2016.
EXCITING LINEUP OF NEW RELEASES
Fiscal 2017 is poised to be another strong year for Take-Two. Our slate of new titles includes the following:
� On May 3, 2K launched Battleborn, a promising new brand from the makers of our popular Borderlands
series. The title is being supported with an array of free and paid additional content, including a Season
Pass and virtual currency, as well as the free-to-play Battleborn Tap companion app.
� On September 6, 2K will bring their smash PC hit, XCOM 2, to new-generation consoles. 2K and Firaxis
Games are excited to broaden the audience for this beloved series.
� On September 20, 2K will release NBA 2K17, which will feature Indiana Pacers All-Star shooting guard
Paul George as the game�s cover athlete and continue the series� proud tradition of working with the
NBA�s most elite athletes. NBA 2K17 also will celebrate the basketball legacy of Kobe Bryant by featuring
the recently retired 18-time NBA All-Star on the cover of the NBA 2K17 Legend Edition. This special
edition will highlight Bryant�s career with themed memorabilia and exclusive digital content.
� On October 7, 2K will launch Mafia III. Currently in development at 2K�s Hangar 13 studio, Mafia III is
the next installment in our successful organized crime series. Set in New Bordeaux, a reimagined New
Orleans circa 1968, Mafia III places players in the role of gifted anti-hero Lincoln Clay, a Vietnam vet
determined to take revenge on the Italian mob for betraying and murdering his surrogate family. Mafia
III will take the series in a bold new direction by combining its trademark cinematic storytelling with a
dynamic open world.
� On October 11, 2K will launch WWE 2K17 and take our popular sports entertainment series to exciting
new heights. Brock Lesnar, perhaps best known for his unparalleled accomplishments in WWE, UFC�
and NCAA Division I wrestling, will be the game�s cover Superstar. We are confident that Yuke�s and
Visual Concepts will continue to innovate this series and build on its positive momentum.
 
4
TAKE-TWO INTERACTIVE SOFTWARE, INC. 2016 ANNUAL REPORT
� On October 21, 2K will release Sid Meier�s Civilization VI, the latest offering in our award-winning
turn-based strategy series that has sold-in over 34 million units worldwide. Developed by Firaxis
Games, Civilization VI will mark the 25th anniversary of the series, and provide the most detailed,
vivid and beautiful experience ever featured in a Civilization game. In this all-new title, active
research in technology and culture will unlock new potential ways to play; cities will physically expand
across the map; and world leaders will pursue their own agendas based on their historical character
traits, as players race to achieve victory.
Looking beyond the current fiscal year, we have a robust long-term development pipeline across both of our labels,
featuring offerings from our renowned franchises along with new intellectual properties that promise to diversify
further our industry-leading portfolio. In addition, we will continue to deliver an array of digitally-delivered offerings
designed to drive engagement with, and recurrent consumer spending on, our recent releases and upcoming titles,
including further free content updates for Grand Theft Auto Online.
OUR FUTURE
These are exciting times for both Take-Two and our industry. While interactive entertainment has been enjoyed for
decades, we are just beginning to see what can be achieved by combining technological innovation with the artistic
passion of our creative talent. We are committed to delivering the highest-quality, most engaging entertainment
experiences that captivate audiences wherever they are, and to generating returns for our shareholders over the
long term. Today, we believe Take-Two is better positioned than ever for continued success.
We�d like to thank our colleagues for delivering another strong year for our Company. To our shareholders, we want
to express our appreciation for your support.
Sincerely,
July 15, 2016
Karl Slatoff
President
Strauss Zelnick
Chairman and Chief Executive Officer