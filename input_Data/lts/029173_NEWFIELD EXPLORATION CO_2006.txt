Dear Fellow Stockholders,

The 5 Things You Need to Know About Newfield, outlined in the initial pages of this annual report, summarize our company and our core
competencies. Although we are bigger and more diversified, our story is easily understood  good people, working in the right places to
add long-term value for our stockholders. We are confident in our ability to execute in 2007 as we continue to build the future of Newfield. 
Here are the major events shaping 2007 and, in part, our future:
Our Woodford Shale play has moved from a concept 
to the development of a major resource.
We first recognized the potential of the Woodford Shale in southeastern Oklahoma in 2003. With encouraging results from a 100%-owned
vertical well test in the Woodford interval, we had the vision and confidence to assemble a large acreage position. We knew that moving
first and fast was critical in todays competitive environment. 
At the time of our initial vertical well test, we controlled about 6,000 net acres in the play. During 2003-05, we kept our results
quiet and aggressively worked to assemble the leading acreage position in the region, today totaling approximately 130,000 net acres.
We are the most active operator in the area. 
Our early drilling efforts consisted of more than 100 vertical wells. The results demonstrated the thickness, prevalence and
consistency of the Woodford Shale across a large geographic region. Our drilling efforts shifted to horizontal wells in late 2005. At that
time, our initial flow rates and our estimated recovery of reserves per well were both on the rise.
We have now spud 65 horizontal wells and we have plenty of company from others in our industry. Over 160 horizontal wells have
now been drilled in the play and 36 rigs are running with more being added each month. Because we were there first, we have an interest
in nearly 60% of the wells. We are operating 13 rigs today with 10 of them dedicated to horizontal drilling. Our gross production in the
play increased more than threefold in 2006 and is now approximately 100 MMcfe/d. Based on increased activity levels, we expect
production to climb significantly in 2007 and beyond.
Our acreage in this play is primary term and we must hold it by drilling at least one well on every 640-acre section. By the end of
2007, approximately 90% of our net acreage will be held by production. Controlling the acreage will allow for the development of the
play on our own schedule. Once in control, we can speed up or slow down based on commodity prices, service costs and our internal
capital allocations. A recent example of how we manage capital is our 2006 gathering arrangement with MarkWest Energy Partners, L.P.
To facilitate our development, they will invest about $350 million over the next three years to build a gathering and compression system
capable of moving more than 500 MMcfe/d. 
We believe that our Woodford Shale play has the potential to add 2.5 to 5 Tcf of net reserves to Newfield. We have the inventory to
drill 150-250 wells annually for the next several years.
We will commence production from our first operated deepwater field. 
First production from our Wrigley Field in the Gulf of Mexico is preparing to commence. The well has been completed and tested at more
than 60 MMcfe/d. The field is being developed with a single subsea well tied back to an existing production platform about eight miles
away. This field is nearly a year behind schedule because of storm damage to the outside-operated host facility.
Deepwater Gulf of Mexico is now an important part of our portfolio and our future. The outside-operated Rigel Field came on-line
in 2006 and produced more than 90 MMcf/d (gross) for most of the year. This is one of three deepwater producing fields where we have
an interest and we expect to add production from two fields in 2007 and two in 2008. We will drill at least four deepwater wells in the
Gulf in 2007, including a development well in our Fastball Field, which is expected to begin producing in 2008. 
We have deepwater prospects in both the Gulf of Mexico and offshore Malaysia. In the Gulf, we used recent lease sales to capture
an inventory of high impact prospects, primarily sub-salt plays, and we expect one or more of these to be tested in 2008. In Malaysia,
we operate the one million-acre Block 2C offshore Sarawak. We drilled our first well of a two-well commitment here in late 2006  a noncommercial gas discovery. We are encouraged by the fact that we proved a working petroleum system exists in this frontier area and our
results are being incorporated into our geologic model. We anticipate drilling a second deepwater well on this block in 2008. 

We will continue development in our South Texas joint venture with Exxon-Mobil.
Weve drilled 12 successful wells in the Sarita JV in South Texas. We believe that the results indicate that we will ultimately prove up 200
 400 Bcfe of gross reserves. In todays mature basins in the U.S., this is significant. 
Our drilling extends the Sarita East Field to the east, but more importantly, we have made what appear to be significant deeper pool
discoveries. We enter 2007 with an inventory of 20 ready-to-drill locations. Our joint venture covers 52,000 acres and we are exploring
today on the 20,000 acres surrounding our Sarita East Field, as well as in two other counties where we operate production.
We will become a North Sea producer in 2007.
Our Grove Field in the Southern Gas Basin of the U.K. North Sea should soon commence production. This will mark our first operated
production from the region. We have an 85% interest in this development. In 2006, we entered into an agreement with another company
to participate in the Grove development and our three-well exploration program in 2007. The first of the three wells has been drilled and
was unsuccessful.
Our Abu Field, offshore Malaysia, will begin production in 2007.
We expect to commence production in the first half of 2007 from our Abu Field, located on PM 318. The operator, Petronas Carigali, is
now commissioning the production platform and the floating, storage and offloading vessel. This will be our third producing field in
Malaysia and we have four additional developments underway. 
The events described are good illustrations of the 5 Things You Need to Know About Newfield. 
 Our portfolio is balanced. Our combined focus areas provide a blend of long-lived, low-risk resource plays and a few higher risk,
high-potential plays which, if successful, could immediately lift us to a new level.
 We remain focused. Our efforts are centered on a few select geographic regions.
 We have creativity. We had the vision to identify the potential of the Woodford Shale and took the lead in assessing and proving the
concept. Our deepwater exploration team has developed complex sub-salt prospects and used them to find partners and gain
access to rigs. At Grove, we were the first to refurbish and use an existing platform in the North Sea. 
 Risk management is critical. There are many risks in our business that can be managed and its prudent to do so. We hedge a
significant portion of our expected natural gas production up to 18 months forward to help eliminate price volatility and ensure
acceptable profit margins. 
 Were in good company and our relationships matter. Our relationships with MarkWest in the Woodford Shale, Exxon-Mobil in South
Texas and Petronas Carigali in Malaysia, among others, benefit all parties and we work to deliver on our obligations and encourage
our people to be ethical, honest and forthright in all their dealings. 
2006 Operational and Financial Highlights
In some respects, 2006 was a challenging year. Although we had a solid year financially, we faced operational challenges on some of
our major development projects and the timing of first production. As a result, some of the oil and gas we originally expected to produce
in 2007 will be pushed into 2008. 
Our toughest challenge in 2006 was the continued recovery from the 2005 Hurricanes Katrina and Rita. The storms negatively
impacted our net production in 2006 by 16 Bcfe. Delays and deferrals were compounded by constraints in the service industry that
continue today. We settled all outstanding and future claims resulting from the storms with our insurance underwriters in the third quarter
of 2006 resulting in $235 million in proceeds. 
Challenges offshore were not confined to the Gulf of Mexico. Severe equipment shortages in international areas coupled with poor
weather in the North Sea and Malaysia delayed first production from new developments in these areas and substantially increased
our costs.
Operationally, 2006 was our busiest year ever. We drilled 687 wells and proved up significant resources in the Mid-Continent
(Mountain Front Wash and Woodford Shale), the Rocky Mountains (Monument Butte) and in South and West Texas (Sarita JV area and
Val Verde Basin, respectively). All of these projects have running room and are an integral part of our future growth.
We replaced 250% of 2006 production by adding 600 Bcfe of proved reserves, virtually all through the drillbit. Year-end 2006
reserves increased 14% to 2.3 Tcfe. We have doubled the size of our proved reserves since 2002 and their geographic diversity makes
us a better company.
Financially, 2006 was another good year. We earned $591 million on revenues of $1.7 billion, or $4.58 per diluted share. Our
results were primarily driven by strong commodity prices in the first half of 2006 and natural gas hedges, which added $159 million tocash flows. We did a good job of controlling operating costs in an environment of rapidly escalating expenses, but we were adversely
affected by huge increases in insurance premiums for our Gulf of Mexico operations following the 2005 hurricanes.
Our strong natural gas hedging position continues into 2007 and early 2008. Nearly 70% of our expected gas production for 2007
is hedged, and, when marked-to-market at the time of this letter, would add approximately $70 million to cash flows. This is in addition
to the $70 million already realized in the first two months of 2007.
Building for the Future
Our $1.8 billion capital program in 2006 not only added reserves and production, it also provided some giant steps in building for 
the future:
 In the Woodford Shale, we statistically sampled our acreage with wells instead of simply looking for sweet spots that would
generate near-term production. Our goal was to determine just how large the play might be and to experiment with horizontal drilling
techniques and fracture stimulation to determine the optimal well design. We are encouraged with our results.
 In our other resource plays  Monument Butte and the Mountain Front Wash  we started pilot programs for the downspacing of
wells. The results from the 20-well pilot program to test 20-acre spacing at Monument Butte have exceeded expectations. Success
could lead to more than 1,000 new locations and an additional 100 million barrels of oil recovery. In addition, we nearly doubled
our acreage in the Uinta Basin during 2006 by adding 87,000 acres through alliances and joint ventures. In the Stiles/Britt Ranch
Field of the Mountain Front Wash area, we are currently developing the field on 80-acre spacing, but started a 40-acre pilot in 2006.
If successful, it could ultimately lead to the drilling of more than 100 new wells and 200 Bcfe of new reserves. 
 Our deepwater Gulf of Mexico program continues to grow. We have fields under development and were capturing prospects to be
drilled in the future. We are preparing for the large lease sales scheduled for 2007 and plan to keep a ready-to-drill inventory of 
4-6 wells. 
 We acquired 3-D seismic surveys over our two new blocks offshore China in the Pearl River Mouth Basin where we expect to drill 
in 2008.
 We obtained approval of three field development plans in Malaysia and work is underway. In addition to Abu on PM 318, we have
two fields under development on PM 318 and two on our operated PM 323. These fields will contribute to production starting in
2008. Our Kuala Lumpur office manages our developments while also seeking new opportunities for us in Southeast Asia. 
 We established a joint venture with Burlington (now ConocoPhillips) to test a deep Bossier/Cotton Valley play in East Texas.
The initial test will reach total depth in the first half of 2007. If successful, we have 12,700 acres on which to continue exploration.
All of these are good examples of how we are building the Newfield of tomorrow. Our 2007 capital investment program balances
the development of in-hand projects, exploration drilling and the identification and capture of new opportunities. Our investments are
expected to add at least 600 Bcfe of new reserves. Although more than 80% of our budget is devoted to exploitation drilling and
development, we will still invest $290 million in exploration. This includes testing approximately 50 prospects across all divisions and
ensuring an aggressive deepwater Gulf of Mexico lease sale effort. 
I am confident that our people are up to the task of building the Newfield of the future. Their experience, creativity and drive will
lead the way. I thank you for your continued support and investment in Newfield.
DAVID A. TRICE
Chairman, President and CEO