To Our Customers, Partners, Shareholders and Colleagues:
Our fiscal 2005 was distinguished by best-ever operating
results, and by the increasing reliance of customers on
Dell as a diversified information-technology provider
and partner.
Our shipments, revenue, operating profit and cash
from operations were all company records. Product
shipments increased 21 percent and revenue grew
19 percent to $49.2 billion. Operating income rose
20 percent to $4.3 billion, representing 8.6 percent of
revenue, our highest rate in four years. We are well
established as the worlds top supplier of computer
systems, and achieved strong growth in every region,
customer segment and product category.
Dells exceptional performance again demonstrated the
dedication and superb execution of a better way of doing
business by our people around the world. Our business is
a model for customer focus, growth and profitability. We
ended the year well ahead of our plan to achieve annual
revenue of $60 billion by the end of fiscal 2007. Never
satisfied, weve already set a new goal, and are following
a plan to reach $80 billion in sales.
Were proud of both what were accomplishing and how
its being done. We begin and end every day working to
deliver a superior customer experience. Were developing
the capabilities of our people. Were sustaining and
raising our tradition of working and succeeding as a
team. And were winning the right way, with a high level
of integrity, on behalf of customers, investors, ourselves
and others who hold stakes in Dell.
Dell directly receives feedback from millions of
customers every day. That knowledge is the basis of
where, when and how we act on their behalf. No other
technology company listens to customers, collaborates
with partners, adds its own significant layer of innovation
and delivers relevant technology more efficiently and
effectively than Dell.
The priorities resulting from that customer-focused
approach are defined by Dells four, multiyear strategic
corporate initiatives: driving Global Growth, achieving
Product Leadership, enhancing the Customer Experience
and developing our Winning Culture.
Global Growth
More customers in more places than ever are realizing
the benefits of Dells lean, direct business. Our growth
last year, representing about 5.5 million computer
systems, would have ranked No. 7 in worldwide share.
Its our objective to lead in all the regions we serve.
The foundation of our success is the same in the United
Kingdom and France, China and Japan, Canada and
other countries. Customers want technology products
and services that are relevant to them, offer great value
and can be easily purchased and used. Thats what our
team around the globe consistently delivers.
In the United States, Dell continues as the preferred
technology provider to businesses of all sizes, educational
institutions, and consumers at home. Our U.S. growth
was more than twice the rate of the rest of the industry.
In the largest countries outside the United States, Dells
growth was more than 30 percent, double the combined
rate of other companies. The global applicability of
our direct approach has never been clearer: our share
of customer demand in every region exceeds what our
U.S. share was after a comparable number of years.
Were also significantly broadening the global base
from which we serve customers. During the last year, Dell
opened, expanded or disclosed plans for operations in
Winston-Salem, N.C.; Oklahoma City, Okla.; West Chester,
Ohio; Waco, Texas; Edmonton, Alberta, Canada; Glasgow,
Scotland; Mohali and Hyderabad, India; and San Salvador,
El Salvador. Last year alone, the company created 9,200
new jobs worldwide.
Product Leadership
While personal computing is an essential part of our
heritage and future, Dells capabilities now reach deeply
into enterprise computing, enhanced services, and printing
and imaging. Customers increasingly rely on us for an
extensive range of products and services to improve their
businesses and personal lives. Mobility products, network
servers, storage systems, services, printers and related
accessories now account for more than half of our revenue
and an even greater portion of our operating income.
Dells innovation is apparent across all of our products
and services, in the way we engage and manage
relationships with customers, and in our world-class
manufacturing capabilities and services.
For example, corporate and institutional customers
remain strongly inclined toward our PowerEdge servers,
and PowerVault and Dell l EMC storage systems, which
allow them to expand their networks and data centers
with the needs of their organizations. Such a scalable
enterprise provides great flexibility, performance
and reliability at a fraction of the cost of systems built
on proprietary technology.
Our growth in printing and imaging represents the
most successful new-product entry in Dell history,
and is the latest example of how prices drop and
customer value increases when we enter a product
segment. Less than two years after introducing our
first printers, we accounted for almost 12 percent
of U.S. printer demand. Printing and imaging revenue
last year reached nearly $1.3 billion.
Customers and others acknowledge Dells product
leadership. Among honors last year, PC Magazine
named the PowerEdge 2850 Best Enterprise Server,
InfoWorld called the Dell l EMC AX100 storage system
SAN Technology of the Year, and PC World recognized
the Dell 3000cn color laser printer as a Best Buy.
Customer Experience
Product leadership helps attract new customers to
Dell. But their subsequent experience is critical to
returning again and again for their technology needs.
Meeting unique customer requirements is our
responsibility and commitment. While doing so is
complex and demands our constant attention, those
requirements follow simple themes: provide quality
products and services, resolve issues when they arise,
and show customers how highly theyre valued at
Dell. For each of those areas, we persistently track
and act in response to how customers say were
performing against a variety of specific standards.
Importantly, the degree to which we achieve objectives
for customer satisfaction is directly tied to the
compensation of people throughout our organization.
Among other benefits, to customers and Dell, such
information helps identify and address issues earlier.
In one example, Dell has been a leader in educating
customers about the existence and severity of
computer security risks, including software viruses
and Internet spyware, and developed services
that help protect systems from unwanted software
and clean computers already infected.
Demand by enterprise customers for Dells enhanced
services has resulted in revenue growth of nearly
40 percent annually for three consecutive years. We
expect services sales to grow at a rate faster than the
company average as we further expand capabilities
based on customer requirements. For instance, we
now have Enterprise Command Centers in the United
States, Ireland, China and Japan to monitor and
respond to real-time needs of enterprise customers
around the world, around the clock.
Winning Culture
Dells commitment to customers is matched only by our
determination to make ours a truly great global company.
That continuous pursuit includes cultivating our unique
winning culture.
We work relentlessly to develop leadership skills at all
levels within the Dell team. We are encouraging personal
accountability for customer satisfaction and company
performance. We are maintaining a higher standard of
integrity and ethical behavior. And we are building and
developing a workforce that is diverse in background,
skill, thought and potential.
We also know that Dells role as a global corporate
citizen extends well beyond the products we build and
the services we deliver. Thats why were increasingly
influencing social issues of importance to our people
and our customers. We are active in global coalitions
working to address HIV and to advance workplace
principles with suppliers, as examples.
Last year we had record participation in our One
Dell: One Community initiative, with nearly 18,000 Dell
employees worldwide volunteering in their communities.
The substantial financial contributions of our people
included more than $2 million to the Asian tsunami
relief effort. In the United States, Dell was recognized
with a national award for our work with food banks
to combat hunger.
Our work on behalf of the environment is considerable.
Dell-sponsored programs and company services last
year accounted for the recycling of nearly 33,000
tons or 30 million kilograms of computers and related
products. Were redesigning our systems for the
environment, reducing the volume and original content
of supplies used in manufacturing and distribution,
and recycling paper, plastic and other resources in
our operations worldwide.
Dell accomplished a great deal during its 21st year. Our
people understand that and are justifiably proud. We are
humbled and honored when others make note of what
weve done, including Fortune magazine, which recently
named Dell Americas Most Admired Company and
ranked us No. 3 worldwide.
However, we see fiscal 2005 as simply the first of
our next 20 years. We see our obligation to customers
growing every day. And we see extraordinary opportunity
for Dell and its stakeholders, through existing and new
customers, products and services.
The future holds great challenge and promise for Dell.
While we approach our future respectfully, we also do
so confidently. Its a view we like very much. And our
intention is to fully realize what it offers over time.

Michael S. Dell Chairman
