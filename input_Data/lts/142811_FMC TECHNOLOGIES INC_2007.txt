To our employees and shareholders

FMC Technologies had an outstanding 2007, recording our sixth
consecutive year of sales and earnings gains. Sales for the year
increased 23 percent to $4.6 billion and earnings per share from
continuing operations rose 53 percent to a split-adjusted $2.30. Our
growth was driven by a talented, worldwide employee base that now
exceeds 13,000 individuals. FMC Technologies accomplished a
number of firsts during 2007, most importantly, the deployment of
our subsea separation technology at the Tordis field in the
Norwegian North Sea. In addition, shareholders were rewarded with
the first-ever stock split in FMCs history as a public company.
Our continued growth during 2007 included the receipt of the largest
subsea order in our history. We also began the process of
spinning-off our FoodTech and Airport Systems businesses so we can
remain focused on providing technology solutions for the energy
industry. It was, indeed, a remarkable journey in my first year as
your President and Chief Executive Officer.
Our Growth in 2007
 Total company backlog increased to a record $4.9 billion.
 Subsea backlog increased to $3.9 billion.
 Energy Production Systems revenue grew 28 percent to
$2.9 billion.
 Energy Processing Systems revenue increased 14 percent to
$768 million.
 FoodTech and Airport Systems generated revenue of
$593 million and $384 million, respectively.

Our Successes
Our advanced technologies and
strong customer relationships
resulted in record inbound
orders during 2007. Among the
projects awarded to FMC last
year was Totals Pazflor project,
located offshore Angola. Valued at
approximately $980 million in
revenue, Pazflor is our largest subsea
project award received to date.
Pazflor is the fourth subsea separation
project announced within our industry
in the last two years, and FMC has
been the recipient of all of these
projects, which are located in four
major deepwater basins across the
globe. We are very honored to have
been awarded all four contracts and
believe our selection is the result of
our advanced technologies and our
position as the subsea pioneer and subsea leader in our industry. In
addition to Totals Pazflor project, our subsea processing technologies
will also be utilized in Shells BC-10 project in Brazil, Shells Perdido
project in the Gulf of Mexico and StatoilHydros Tordis project in the
Norwegian North Sea. Our people and innovative technological
developments have truly set us apart in our industry. Our valued
customers recognize this and rely upon us every day. These
capabilities will continue to drive our successes as our customers
continue to focus on deepwater developments.

Another significant event that occurred in 2007 was the award of our
five-year frame agreement with StatoilHydro to provide subsea
equipment and associated services for the North Sea and other
potential international developments. This agreement represents
potential significant revenue for our operations as the total value of
all orders to be placed under this agreement are estimated to range
from $2.6 billion to $4.4 billion. Only FMC Technologies and one
other company were awarded this opportunity.
These projects are just a sample of the many ongoing efforts within
our Energy Systems businesses, which represented nearly 80 percent
of our 2007 revenues. Other activities during the year included
increasing our ownership stake in CDS Engineering to 100 percent in
December, two years ahead of plan. As the industrys demand for
subsea processing continues to grow, it was vital to accelerate the
timing of our ownership stake so we can leverage their expertise and
enhance our leadership role in the growing marketplace for subsea
separation technologies. Located in The Netherlands, CDS
Engineering is the leading provider of gas and liquids separation
technology and equipment for both onshore and offshore
applications and floating production systems. The innovative and
proprietary systems of CDS Engineering have been critical to our
subsea processing successeses.
Recognizing that energy has become our core business and the
source for our future growth, we announced in October 2007 the
strategic decision to spin-off our FoodTech and Airport Systems
businesses. Following regulatory approval, these businesses will
become a separate, publicly traded company through a tax-free
distribution to our shareholders during 2008. FoodTech and Airport
Systems are becoming an independent public company following a
very successful 2007 that was marked with record revenues,
operating profit and year-end backlog. Both segments are very well
managed businesses, maintain leadership positions in their respective
markets and are growing their global reach. The new company will
be led by a veteran FMC management team. Charlie Cannon, current
Senior Vice President for FoodTech and Airport Systems, will serve as
Chief Executive Officer, and Ron Mambu, our Vice President and
Corporate Controller, will become Chief Financial Officer.
The success of FoodTech and Airport Systems is due to the efforts
of outstanding employees. I am proud of their many
accomplishments and believe FoodTech and Airport Systems will
continue to prosper as an independent company in the future.
Industry Overview and Trends
Our customers have many challenges before them. They are
experiencing declining production rates from existing fields and at
the same time are seeking new opportunities for production,
generally in deeper water and in more extreme environments.
New technologies are needed to address both of these issues.
This trend resulted in a 14 percent increase in worldwide E&P
spending in 2007 and in 2008 worldwide E&P spending is projected
to increase more than 11 percent. In addition, international E&P
spending, which grew in excess of 20 percent in 2007, is expected to
increase by more than 16 percent in 2008. We expect subsea
spending will follow a similar pattern, supported by the above
activity and other industry metrics, including a projected 35 percent
increase in the number of existing deepwater drilling rigs over the
next four years.
The energy industry is also experiencing a shift in dynamics due to
the continued emergence of National Oil Companies (NOCs) such as
Petrobras, StatoilHydro and Sonangol. FMC Technologies must
remain well positioned to continue to address the unique needs of
both International Oil Companies (IOCs) and NOCs. There are more
than 100 NOCs across the world today, controlling approximately
70 percent of the worlds undeveloped oil and gas reservoirs and
producing about 50 percent of the worlds oil. NOCs are expected to
be increasingly active in the recovery of their natural resources and
may take on roles once held solely by the IOCs, such as operators of
field development projects. The IOCs are shifting their strategies as
well, focusing on increasing recovery rates at existing wells and
investing in deeper water exploration projects that help them add
value to the NOCs. By recognizing these and other trends, and by
developing effective solutions to the technology obstacles faced by
our customers, we can enhance our position as the industry leader in
subsea technologies.

The demand for natural resources such as oil and gas is projected to
increase for the foreseeable future. In addition, a growing number of
other energy sources are also being pursued. These include heavy oil,
oil sands, shale oil, coalbed methane and tight gas. Development of,
and investment in, these hydrocarbon sources is expected to increase
as world demand grows. Our strategies position FMC to effectively
and profitably respond to these opportunities.
Shaping the Future
FMCs management team has identified a number of strategies to
effectively respond to our customers needs and industry trends.
Four strategies in particular are key to shaping the future of our
company:
 Growing our energy business
 Developing future technologies
 Strengthening customer relationships
 Investing in our employees
Growing Our Energy Businesses
Our subsea products remain the largest and fastest growing segment
of our business and accounted for 49 percent of FMCs total revenues
in 2007. Over the last five years, revenue in our subsea business has
increased at a 27 percent compound annualized growth rate (CAGR)
and we have clearly remained the market leader in the sector. The
subsea production market is forecast to continue its current growth
rate as customers venture into deeper water in search of
hydrocarbons and the complexity and sophistication of
equipment on the seabed increase.
Industry projections also suggest there will be a continued increase
in the construction and deployment of new deepwater drilling
vessels, or rigs. As these rigs are deployed and the ensuing field
completion activities progress, the result will be an expedited
demand for subsea products and systems. In fact, more than 75 new
rigs are expected to be delivered by 2011, further supporting growth
in the underlying subsea business.
Developing Future Technologies
To support the growth of our subsea energy businesses, we are
continually expanding our range of products and solutions. Two of
our most recent growth initiatives, Riserless Light Well Intervention
(RLWI) and Subsea Processing, which includes separation, boosting
and gas compression, have enhanced our existing portfolio of subsea
technologies and systems. We announced two new RLWI contracts
that will go into operation in 2008, one for StatoilHydro and one for
BP. These and other technologies have provided growth avenues
outside of our traditional subsea completion business. As a result,
FMCs average order value per subsea well has more than doubled in
the last four years to approximately $19 million in 2007. We expect
that new technology developments will also contribute to growth in
areas outside the traditional subsea market.
Equally important to our growth is the continued development and
expansion of our surface wellhead and fluid control businesses. Our
surface wellhead business is driven by global drilling and completion
activity. Growth in this area has been enhanced by recent
acquisitions of businesses, including Netherlands-based VOS
Wellhead as well as the 2006 purchase of Canada-based Galaxy
thermal wellheads. Our surface wellhead business is also benefiting
from our investments in the development of specialized fracturing
rental equipment. FMC Technologies will continue to seek strategic
acquisitions as we look to expand our scope, improve our technology
focus and strengthen our position in key markets. Growth in the fluid
control market is driven by drilling activities, including pressure
pumping activities. We are fortunate to be the leading equipment
supplier to this market and will continue to focus on product
availability, technology and aftermarket services.

We must maintain an appropriate manufacturing capacity to meet
the future equipment and technological needs of our customers.
Capital expenditures for 2007 were approximately $203 million,
primarily for the expansion and construction of new manufacturing
and service facilities across the globe as well as investments in RLWI
tools and equipment. Our subsea tree production capacity will
increase to approximately 335 trees by the end of 2008. This increase
in capacity will accommodate the expected demand levels we project
for our subsea products and systems. We will continue to assess the
need to add future capacity beyond this level.
Strengthening Customer Relationships
We achieved many successes with our customers during the past
year while solidifying our position as a global company with a
regional focus. Projects in the Gulf of Mexico, West Africa, the
North Sea and in deepwater offshore Australia and Brazil have
resulted from our relationship building and collaborative
technological developments. We will continue to add value and
provide technological expertise to our customers in 2008 while
differentiating ourselves from our competition.
We have strong relationships with all of our customers. We value the
trust and confidence they place in our people and our advanced
technologies. Subsea exploration is vital to our growth, to the
sustainability of our industry and to the success of our customers.
As the depths in which we operate continue to increase and become
more challenging, so too must the sophistication and complexity of
our technological solutions. FMC has demonstrated an unparalleled
ability to provide value-added products and services and to respond
to the unique needs of our customers. To expand upon our
leadership position in the industry, we must continue to execute
in the same manner.
We also continue to increase the level of local participation at our
facilities in the communities where we operate. We accomplish
added involvement in our deepwater projects through the hiring of
local employees and the use of local suppliers and other expertise
that exists in each region. By utilizing local businesses, we are able to
meet the expectations of our customers while infusing capital and
economic opportunities into the communities in which we operate.
Whether in Angola, Brazil, Malaysia, Nigeria, Norway or Russia, we
continue to set the standard for inclusiveness by working together
within our communities. For example, two recently announced
projects  Shells Gumusut-Kakap and Totals Pazflor  will be
supported by local suppliers and new employees hired into our
operations in those regions. For Gumusut-Kakap, our Malaysian
facilities in Nusajaya and in Kuala Lumpur will provide local
manufacturing and project assistance, while Pazflor will be supported
by our base in Luanda, Angola. FMC Technologies is aligned across
its subsea businesses with interchangeable project management
processes, quality systems, engineering standards, supply
management, manufacturing/assembly practices and offshore
procedures. As a result, FMC is able to support its customers
seamlessly with solutions so they can successfully execute their
projects anywhere in the world.
Investing in Our Employees
Our investments are not limited to acquisitions and facility
expansions. We understand that to maintain and widen our position
as the industry leader, we must continue to aggressively attract and
retain the most talented employees in the industry. Throughout our
organization  from engineering, manufacturing and project
management professionals to administrative and service personnel
 we must continue to hire individuals who are committed to
developing long-term customer relationships and innovative

technological solutions. Our strong historical performance is
attributable to our talented employees and innovative technologies,
both of which will continue to drive our future growth.
We also recognize that education and training are necessary to
community development and individual successes. By partnering
with local educational institutions in many of the countries in which
we operate, we have laid the foundation to develop future managers
by exposing them to the skills and fundamentals of our industry.
FMC currently maintains recruiting partnerships and programs with
universities in Brazil, Norway, Russia, Scotland, Singapore and the
United States. In many of these partnerships, we even develop
curriculum and provide internship opportunities to students, offering
them a glimpse of the many facets of our industry.
Of particular note is Polytechnic University in St. Petersburg, Russia,
where FMC recently established a post-graduate Subsea Engineering
program. Russia possesses more gas reserves than any other country
in the world, and their government and its national gas company,
Gazprom, intend to develop fields located offshore in the Barents
Sea. By partnering with Polytechnic University, one of the most
respected technical schools in Russia, we are at the leading edge of
developing talent and having access to the best and brightest
engineers in Russia.
Going forward, we plan to expand that strategy to a broader
range of educational institutions as we believe the benefits and
opportunities are mutually beneficial to our industry and our current
and future employees.
Health, Safety and Environment (HSE)
FMC understands that active participation in the areas of Health,
Safety and Environment means adapting a culture that is protective
of our people, our communities and the impact we have on the
environment.
FMC has a very commendable record of driving HSE focus in all we
do. We have made significant advances in workplace safety,
employee health and environmental protection. The statistics bear
this out, including record-setting performance in lost workday and
total recordable incident rates. In light of such excellence, it is also
imperative that we recognize the rapid pace at which our company is
growing, understanding that growth in itself presents a challenge.
We must then constantly educate and raise the accountability and
awareness of all our employees, by cultivating a sound HSE culture.
To enhance our HSE mindset, FMC has adopted four long-term
objectives:
 Maintain a consistent HSE focus
 Achieve an injury-free workplace
 Promote environmental stewardship
 Proactively manage high-risk activities
The start of a new year is always exciting, and I believe 2008 holds
great promise for FMC, its employees and its shareholders. I am
confident we will make great strides this year, facing each and every
challenge. By maintaining an HSE focus in all we do, we will meet
our objectives and finish this year as a safer and stronger company.

Positioned for the Future
During the course of fiscal 2007 we completed a seamless transition
in the companys leadership by leveraging our breadth and depth of
management talent. That transition began at the start of 2007 with
the appointment of John Gremp to Executive Vice President of
Energy Systems. A 32-year FMC veteran with a wealth of experience
and industry knowledge, John now oversees our Energy Processing
and Energy Production divisions. Bob Potter, a 35-year FMC veteran,
was promoted to Senior Vice President of Energy Processing and
Global Surface Wellhead. Tore Halvorsen, a 28-year industry
veteran who has been with FMC since our purchase of Kongsberg
Offshore AS in 1993, was named Senior Vice President of Global
Subsea Production Systems.
As we embark on a new year, we also are saying farewell to
Mike Murray, Vice President of Human Resources and
Administration, who announced that he would retire from the
company in 2008 following a distinguished 35-year career.
From his first days as a Personnel Assistant up to his current role,
Mike has performed with the utmost dignity and professionalism
throughout his career. We wish him well in his retirement and thank
him for his many contributions over the years. Maryann Seaman
succeeds Mike as Vice President of Administration. Maryann brings a
wealth of knowledge and experience from her 21-year FMC career,
and she will continue to apply that knowledge while overseeing the
administration function, which includes Corporate Communications,
Human Resources, and Health, Safety and Environment.
Today, FMC Technologies is at the forefront of technical innovation
and we are recognized as the preeminent solutions provider for
many technical challenges throughout our industry. Our initiatives
and efforts have resulted in some significant achievements and many
future opportunities.
As we embark on a new year, I believe FMC Technologies remains at
a competitive advantage to continue to grow, add value to our
customers and provide a premium return to our shareholders.
We are a unique company, formed of the most talented professionals
in the industry. Our future successes are unlimited as long as we
continue serving our customers well and add value in all activities
that we perform.

Peter D. Kinnear
President and CEO
FMC Technologies, Inc