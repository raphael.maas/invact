

At the same time, we produced cash ow from operations of more than $1 billion this year, despite making an unusually large 
contribution to a U.S. pension fund. We delivered these strong results while continuing to lay groundwork for the future in such 
areas as information technology infrastructure, wellness and nutrition innovation, and our entry into emerging markets.
Our net sales also grew during fiscal 2010, but were below our expectations, finishing up only 1 percent with sales of 
$7.7 billion. In our largest simple meals business, U.S. Soup, sales declined 4 percent for the year. The primary reason was a 
9 percent decline in sales of our ready-to-serve soups. Total soup category sales trends were negative, but we outperformed 
our branded competitors. Condensed soup sales decreased 2 percent and broth sales rose 3 percent. Importantly, our 
other two core areas of focus, healthy beverages and baked snacks, grew sales, earnings and market share during fiscal 
2010. Building on the momentum in these businesses is a key element of Campbells plan to drive growth going forward, 
particularly on the top line.
We were able to deliver robust earnings growth despite the modest sales gains, in part by redoubling our efforts to boost 
productivity. For the first time in recent memory, our cost per unit declined on a year-over-year basis, and we continued to 
gain benefits from our implementation of SAP in North America. It was a strong performance, which I attribute to efforts 
throughout the company. Our EPS also benefitted from favorable currency and our share repurchase program.
Building on Our Strengths
Our mission is to build the worlds most extraordinary food company by nourishing peoples lives everywhere, every day. 
In the marketplace, that means delivering sustainably good performance across our portfolio, and we are energized and 
excited about our ability to do this. Campbell has all of the elements needed to win as a focused food company:
U Our categories of healthy beverages, baked snacks and simple meals are large and growing.
U We have leading brands, including V8, Pepperidge Farm, Arnotts, Swanson, Prego, Pace, Liebig, Erasco and,  
of course, Campbells.
U We have regional scale in three major geographies: North America, Asia Pacific and Europe.
U We have world-class product technologies in such areas as thermal processing, baking, sodium reduction, and  
vegetable and whole-grain nutrition.
U We are in a strong financial position to invest in growth, thanks to our fiscal 2010 performance.
U And, most importantly, we have an experienced, talented, engaged team that understands our consumers,  
customers and categories better than anyone else.
Douglas R. Conant President and CEO

  These amounts are adjusted for certain transactions not considered to be part of the ongoing business.  
For a reconciliation of non-GAAP measures, see page 15.Campbell Soup Company   3
We recognize that, in order to win in this difficult economic environment, we must grow our categories and accelerate our 
organic top-line sales by driving increased volume. We will do this by serving our consumers and customers better, faster 
and more completely than anyone else.
Thinking Differently
With this in mind, we have begun to think differently about our portfolio. We believe that we can place ourselves in a 
stronger position to win by more powerfully focusing on our fastest-growing businesses through stepped-up innovation 
and marketing support, funded through productivity gains and rigorous expense management. These businesses include 
healthy beverages, baked snacks and meal makers, a subset of simple meals, which is comprised of our broths, sauces and 
condensed cooking soups. At the same time, we will continue to competitively build our meals business, our other subset  
of simple meals, which includes our condensed eating and ready-to-serve soups.
For fiscal 2011, we have a full slate of innovation and marketing programs underway to achieve this plan. Here are just  
a few examples:
U In healthy beverages, we are launching V8 V-Fusion +Tea and adding new fiavors to our popular V8 V-Fusion line.
U In baked snacks, we are introducing upgrades for Pepperidge Farm Goldfish and Arnotts Shapes and relaunching 
Pepperidge Farm Chunk cookies. We also are planning to upgrade our research and development capabilities.
U In simple meals, we are increasing the competitiveness of our entire U.S. portfolio of Campbells soup brands, including 
condensed and ready-to-serve, through a new advertising campaign and new varieties. In addition, in condensed soup, 
we are introducing fresh new product labels, ingredient upgrades and an enhanced gravity-feed shelving system in 
stores. We are continuing to invest in our international soups, for example, by beginning the relaunch of our Erasco  
brand in Germany.
At the same time, we will pursue aggressive goals for reducing our total delivered cost, while fully implementing and 
leveraging our new information systems, such as SAP. For example, we plan to make significant headway this year on our 
initiative to simplify the soup-making process in our North American manufacturing network, which is designed to reduce 
the cost of making soup and increase the speed with which we introduce and commercialize innovation. To give you a sense 
of our goals, consider this: We use more than 1,700 packaging materials in various 
formats and sizes. By reducing this variation, we can achieve greater economies of 
scale in procurement and simplify our production processes, all while we continue to 
improve our quality offerings for consumers. We see similar opportunities to reduce 
complexity in other areas as well, such as ingredients and soup bases. 
Our Model for Winning
As we evolve the way we manage our portfolio and increase productivity, we continue 
to follow our time-tested blueprint for building the worlds most extraordinary food 
company, our Campbell success model, which includes four elements.
First, we win in the workplace by creating a high-performance culture with worldclass levels of employee engagement. Our people then enable us to win in the 
marketplace by meeting the needs of our consumers and customers in a superior 
fashion. As we do this, we have the capacity and the responsibility to win in the 
community and help make our world a better place. Through it all, we win with 
integrity by holding ourselves to the highest ethical standards.
In the workplace, marketplace and community, we have set specific goals, and we 
monitor our progress, which has been substantial. Our latest results are outlined below.
Winning in the Workplace
We measure winning in the workplace primarily through an annual survey of employee engagement, conducted by the Gallup 
Organization. Gallup defines world-class engagement through two broad measures: the engagement ratio, or the number of 
engaged employees for each actively disengaged employee; and the engagement mean, or the average of all responses to 
the 12 survey questions. I am pleased to report that, although our numbers declined modestly this year, we remained world 
class on both measures. Indeed, our employee engagement ratio was 17:1, well above the world-class ratio of 12:1. 
In addition, we continued to receive external honors for the quality of our workplace. Most notably, our efforts to develop 
and advance women earned Campbell the prestigious Catalyst Award, given by a leading, global nonprofit organization


that focuses on building inclusive workplaces and expanding opportunities for women. We also upgraded our workplace 
by opening our new Campbell Employee Center at World Headquarters in Camden, N.J. During fiscal 2011, sustaining and 
deepening our world-class employee workplace/engagement is a top priority. 
I firmly believe that personal leadership is the key factor in driving employee engagement, starting with our senior 
leadership team, and fiowing across the organization through each level of management. I have challenged all of our 
managers to live our Campbell promise  Campbell valuing people, people valuing Campbell  in every interaction by 
valuing our people in ways that are meaningful and tangible to them. I am confident that our people will continue to 
respond to this message with stronger engagement and extraordinary performance.
Winning in the Marketplace
We measure winning in the marketplace by comparing our total shareowner returns (TSR), including stock price appreciation  
and dividends, to the average TSR of our peer companies in the S&P Packaged Foods Index. Our goal since 2005 has 
been to deliver industry-leading TSR over the following decade. We believe that above-average performance, consistently 
delivered over time, will distinguish Campbell as one of the very best long-term value creators. Therefore, our performance 
standard is to deliver above-average rolling three-year TSR relative to our peer group each year.
Six years into this 10-year goal, our cumulative TSR was 64.0 percent, significantly higher than that of our peer group, 
which returned 38.0 percent. Our industry also outperformed the broader market: The cumulative TSR for the S&P 500 was 
13.1 percent over the same period. On a rolling three-year basis, our cumulative TSR was 1.5 percent, outpacing that of the 
S&P 500, which declined 6.8 percent, but lagging the peer groups TSR of 3.4 percent. Our performance compared to our 
peers improved this year over fiscal 2009 results.
Winning in the Community
Conducting business in a socially responsible manner is an integral part of how we create shareowner value. Thats why  
we have embedded social responsibility into our mission, success model and strategies.
Our goal for winning in the community is to be recognized as one of the most socially responsible U.S. corporations.  
In fiscal 2010, for the second year in a row, Campbell ranked as one of the 10 most socially responsible U.S. companies 
on a leading survey compiled by the Boston College Center for Corporate Citizenship and the Reputation Institute. In 
addition, our employee volunteerism programs were honored by the Points of Light Institute; we were named to Corporate 
Responsibility magazines list of the 100 Best Corporate Citizens; we were included in the Dow Jones Sustainability Indexes 
(North America) for the second year in a row, and we were added to the Dow Jones Sustainability World Index. Clearly, 
there are many organizations that assess corporate social responsibility (CSR), and their methods continue to evolve. In 
2011, we will explore other benchmarks that may better refiect our overall CSR performance.


During fiscal 2010, we set 10-year destination goals to guide our CSR efforts, and we published these goals as part of  
our second CSR report, which is available on our website: www.campbellsoupcompany.com/csr.
Winning with Integrity
As we aspire to win in the workplace, marketplace and community, it is essential that we comply with the law and hold 
ourselves to the highest standards of business ethics. We cannot build the worlds most extraordinary food company any 
other way. In 2010, Ethisphere magazine honored our efforts by naming us as one of the worlds most ethical companies. 
Our Guidance
In September, when we reported our fourth-quarter earnings for fiscal 2010,  
we gave earnings guidance for fiscal 2011 as follows:
U Net sales growth of 2 to 3 percent;
U Growth in adjusted earnings before interest and taxes (EBIT) of  
4 to 5 percent; and
U Adjusted earnings per share growth of 5 to 7 percent.
Our fiscal 2011 guidance for net sales and adjusted EBIT growth are both  
1 percentage point below our long-term targets, rejecting the challenging 
consumer environment. Our guidance for adjusted EPS growth is consistent  
with our long-term targets.
A New Chapter
In September, Campbell announced my plan to step down as CEO on July 31, 
2011, at the end of our current fiscal year, which also marks the end of my  
10th full fiscal year with the company. The Board elected Denise M. Morrison  
as Executive Vice President and Chief Operating Officer in anticipation of her 
election to succeed me as CEO at the beginning of fiscal 2012. Denise has also 
been elected a Director of the company.
As I refiect on my time with Campbell, I am very pleased with our progress in  
the workplace, the marketplace and the community. When I arrived, Campbell  
was among the worst performing companies in its industry. The company had 
faltered on a number of fronts. While it remained an American icon, Campbell  
had lost touch with its consumers, investors had begun to lose faith, and, as you 
can imagine, employee morale was low.

Campbells 2001 annual report, my first, was titled, Its not enough to be a legend. We set out to renew the legend, 
and we have been doing that ever since. During my first four years, we focused on building a high-performing executive 
team, replacing 300 of our top 350 executives, half through internal promotions. From 2005 forward, this reenergized team 
created a high-engagement culture, which is the foundation of our marketplace success. Together, I am proud to say, we 
placed Campbell back on the path toward extraordinary growth, as befits our venerable, 140-year heritage, our dynamic 
people, and our global opportunities.
Denise is a critical part of the team that transformed the company and I am confident that, under her capable leadership, 
Campbell will continue to advance. I look forward to working closely with her over the coming months to ensure a smooth, 
seamless transition.
I would like to close by thanking our shareowners, employees, customers, consumers and our Board of Directors for 
allowing me to serve this great company over the past decade. It has been an honor and a privilege.
Sincerely,