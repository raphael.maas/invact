Set forth below are IAC managements prepared remarks relating to IACs earnings announcement for
the 4
th quarter of 2015. IAC will audiocast a conference call to answer questions regarding the
Companys 4
th quarter financial results and these prepared remarks on Wednesday, February 3, 2016 at
8:45 a.m. Eastern Time. The live audiocast will be open to the public at www.iac.com/Investors. These
prepared remarks will not be read on the call.
Match Group will audiocast a conference call to answer questions regarding their 4th quarter financial
results and managements prepared remarks on Wednesday, February 3, 2016 at 8:00 a.m. Eastern Time.
The live audiocast will be open to the public at ir.matchgroupinc.com. These prepared remarks will not
be read on their call.
Non-GAAP Financial Measures
These prepared remarks contain references to certain non-GAAP measures which, as a reminder, include
Adjusted EBITDA, to which we'll refer in these prepared remarks as EBITDA for simplicity. These
non-GAAP financial measures should be considered in conjunction with, but not as a substitute for,
financial information presented in accordance with GAAP. Please refer to our 4
th quarter 2015 press
release and the investor relations section of our website for all comparable GAAP measures and full
reconciliations for all material non-GAAP measures.
Please see the Safe Harbor Statement at the end of these remarks.
Joey Levin, CEO, IAC
In the fourth quarter we successfully completed a $460 million IPO of Match Group and $1.25 billion of
debt transactions, creating the 8th public company from what was once one IAC in 2005. Now IAC
begins a new stage with four strong segments beyond Match Group  HomeAdvisor, Publishing,
Applications, and Video. Our key businesses continue to perform well. At Match Group, subscriber
growth accelerated to 30% in the 4th quarter, and HomeAdvisor grew domestic revenue 51%, marking the
9
th straight quarter of revenue acceleration in its core domestic business. We also saw healthy revenue 
Page 2 of 11
growth at Vimeo, Premium Brands within our Publishing group, and the Consumer side of our
Applications business.
Over the last 20+ years, IAC has acquired, created, built and assembled high performing businesses and
category leaders, and, well before splits and spin-offs became fashionable with shareholders, we have
consistently enabled our shareholders to own our businesses directly and separately when sensible. As a
result, an investor who put $1 into IACs predecessor company, Silver King, in 1995 would today have
nearly $12.50, versus $3.50 for $1 invested for the same period in the S&P 500. Weve delivered these
returns through smart acquisitions, through creating new companies, and with a healthy impatience on
execution and patience on vision.
We are constantly looking for new businesses which are complementary to our existing companies or
which could form the nucleus of a new and transforming category for IAC. We are rigorous in our
analysis and seek to deploy our capital only where we see high returns in large addressable markets and at
multiples which we believe ensure such returns. Fifteen of our last twenty acquisitions have been
completed at single digit EBITDA multiples, and all but three of the twenty have been accretive to IAC.
When we look at new businesses, we focus on businesses which we believe: (1) can create considerable
value for both customers and investors, (2) can achieve significant scale in a large and dynamic category,
(3) either have a great team or for which we have a great team, and (4) can benefit from our particular
areas of experience and skills. With approximately $1.5 billion of cash on the IAC balance sheet, we
intend to be both diligent and aggressive, as weve always been.
We also evaluate new opportunities against the option to acquire a greater interest in our existing
businesses through share repurchases. In the personals business, weve been able to capitalize on unique
acquisition opportunities over the last few years given an advantageous position as one of a limited
number of acquirers in the category. But in other areas, weve remained a spectator as the private
fundraising market for internet companies has been exceptionally rich, and opportunities for sensible
acquisitions have been limited. Amidst the disconnect in valuations between public and private markets,
we returned a great deal of capital to shareholders. We have opportunistically bought back over $3 billion
in IAC shares since 2009 at an average price of $29.86 per share, and, given the significant level of cash
flow IAC had on a standalone basis, we instituted a quarterly dividend in 2011 and have paid out nearly
$400 million in dividends over the last 4+ years. 
Page 3 of 11
At the moment, neither Match Group nor IAC is currently positioned to pay a quarterly dividend: Match
Group has leverage of greater than 3.5x, and IAC now has a smaller base of cash flow given the
separation of Match Group. We expect Match Group, excluding any incremental acquisitions, to
deleverage its balance sheet through operating growth, cash flow generation, and debt repayment. At the
same time, IAC is well-capitalized to acquire assets and generate high returns for shareholders. Thus, we
have announced that we are ceasing our quarterly dividend, but may reinstate the dividend at either IAC
or Match Group in the future. This does not mean, of course, that we wont be disciplined stewards of
capital, as we have been for more than two decades  it simply means that our capital will be focused on
high-return acquisitions and opportunistic share buybacks for the time being.
With a strong balance sheet and our extended Google agreement, we have confidence in a strong base of
cash flow at IAC going forward. Our Search & Applications business, which we will now report in two
segments  Publishing and Applications  has been the largest cash flow contributor at IAC for the last 7
years and, with the new Google agreement, will continue to be so for several more. The Search &
Applications businesses produced approximately $300 million in EBITDA in 2015, and we expect that
they will collectively generate approximately $200-$225 million of EBITDA in 2016 and grow from
there. Roughly 70% of the former segments EBITDA will land in the new Applications segment and
20% in the new Publishing segment (including incremental investment at The Daily Beast, which is
moving into Publishing, and excluding PriceRunner, which is moving into Other). More than 3/4 of the
decline from 2015 to 2016 in what formerly comprised the Search & Applications segment comes from
our legacy search business and the partnership (previously B2B) portion of Applications. We expect the
EBITDA from our premium publishing brands and consumer applications businesses to be down together
only mid-single digits in 2016 after all of the changes with the agreement take hold. This new contract
signals a real victory as it positions us well for long-term growth in the best of these businesses in return
for the expansion of existing consumer-friendly practices that we support, and a rate adjustment on mobile
that we believe still leaves us well-positioned in the market. The four year contract term reaffirms our 10-
year relationship with Google and creates a firm foundation for growth in these businesses for years to
come.
HomeAdvisor
HomeAdvisor continued its incredible run of accelerating domestic revenue growth in the 4th quarter,
reminiscent of the stretch we saw at the online travel businesses in the early 2000s. For the full year, the
core domestic business grew revenue 43% while the overall business grew revenue 27%.
Page 4 of 11
The key to HomeAdvisors success over the last 2 years has been the growth in size and quality of our
domestic service professional network, now in excess of 106,000 paying service professionals, up
approximately 45% year-over-year. Additionally, the average revenue per service professional was up
nearly 10%, as we are attracting higher-quality service professionals. We made significant investments in
our salesforce to do this, with sales expense growing modestly faster than revenue in 2015. Weve also
significantly improved retention  our annual retention has increased more than 2,000 basis points over
the last year, which is a huge achievement in an industry where independent service provider turnover
broadly is estimated around 30% annually, and, to maintain quality in the network, an additional level of
forced churn is required. For the most part, we are keeping the pros we want in the network, because
the service works for them. Based on current retention rates, which we aspire to drive yet higher, we
currently estimate the revenue LTV for service professionals sold over the last 3 quarters has increased by
more than 30% versus the prior year period.
Weve also invested heavily in our brand and in our product. We tripled our TV expenditure in 2015 and
grew our marketing spend nearly 60%, in turn growing our total service requests from consumers 49% to
nearly 10 million service requests domestically in 2015. We expect to exceed a million service requests
per month in 2016. Weve also increased our repeat rate double digits percent over the last 2 years  and
that number still has a great deal of room for improvement. In terms of product, weve dramatically
improved the HomeAdvisor mobile user experience this last year and seen our mobile business more than
double. We have also introduced two new products, InstantConnect and InstantBooking, which
meaningfully advance the consumer and service professional user experience and together now account
for nearly 10% of all service requests, up from essentially zero at the beginning of 2015. As you might
imagine from their names, InstantConnect connects a consumer on our platform directly by phone with
the appropriate home service professional for their specific job in their specific geographic area, allowing
the consumer and the pro to work out the details. Our InstantBooking product goes a step further, and
simply allows the consumer to directly book an appointment with the right service pro instantly on our
platform. Were only able to deliver these next-level consumer experiences because of the depth, breadth,
and geographic diversity of our service pro network, and because they are meaningfully engaged on our
platform. We are really pleased with the rapid adoption of these products and even more so with the
satisfaction levels we have seen, with Net Promoter Scores (NPS) on these products for both consumers
and professionals materially higher than on the standard HomeAdvisor service.
Page 5 of 11
All of this investment decreased margins and held EBITDA essentially flat in 2015. In 2016, we plan to
continue to invest in our brand, our product, and our market position, but given the overall revenue scale
in the business today, we could end up doubling EBITDA for the year notwithstanding the investment.
We remain focused on long-term market leadership over near-term margins, but as marketing and sales
investment growth decelerates in 2016 relative to 2015 and we simultaneously begin to enjoy the benefit
of greater retention and revenue per service professional, we will see some of the revenue growth drop
down to the bottom line if we grow, as we expect in 2016, at or above the growth rate we saw in 2015.
Publishing
We restructured Search & Applications into our new Publishing and Applications segments to bring
separate focus to what have become discrete businesses with unique strategies and opportunities. On one
side, we are tightening our focus on digital publishing. There are still tens of billions in offline print ad
spend, shifting from offline to online at an estimated rate of more than $1 billion a year, creating
tremendous opportunities for those who cultivate the right audiences. In looking at our assets, wed
clearly assembled a group of Publishing assets well-positioned across a number of content categories, all
while operating profitably and at significant scale, but the audience reach of the collection was not widely
appreciated. Accordingly, in December of 2015, we announced the launch of IAC Publishing, bringing
together our collection of premier properties to create a clear digital publishing entity with almost $700
million in revenues in 2015. These properties, each with its own heft in brand and audience, are now
together on top of a platform which allows us to extract maximum leverage from investments in core
areas like revenue generation and optimization, technology, analytics and marketing across the entire
group. In addition, were also better organized to take advantage of our overall audience size and data
with advertisers.
Our Premium Brands  comprised of About.com, The Daily Beast, Investopedia and Dictionary  finished
out the year on a very strong note, with revenues up 14% in Q4 and nearly 30% in 2015. These properties
alone reach over 100 million domestic users per comScore, placing Premium Brands as a top 10 publisher
of original content with over $300 million in revenue last year.
At About, page views per visit and time-on-site are up double digits year-over-year (14% and 13%,
respectively) as a result of continued work and improvements in product and UI. Overall RPM is up
25%, driven by both an increase in direct sales (up 73% in the 4th quarter and 43% for the full year) and
continued improvements in programmatic revenue. Were also continuing to see nice growth in traffic 
Page 6 of 11
from new channels, with page views from social, email and other new channels up 3x year-over-year. As
weve said before, we are underpenetrated in these channels and expect to see continued growth as we
invest further in social product and distribution. Results would have been even stronger here, but for
softness in distribution via Google SEO, as traffic from the search engine remains fickle and we believe
recent adjustments have favored vertical publishers over horizontal properties  a trend that has benefited
us in connection with a site like Investopedia, but has been a drag at About. Ultimately, we believe that
quality content will always do well in search, and our focus remains on empowering Abouts experts to
produce the best content in their categories. But we are also going to begin to stress the strength we have
in particular verticals. On a standalone basis, Abouts traffic in Health, Travel, Home and Leisure and
Entertainment are top 10 sites in their respective categories and we are working to position them as deep
verticals in those categories for the benefit of traffic and ad sales.
Investopedia finished the year with total traffic up 22%, driven by success across the board from all
sources, and Q4 set new records for both traffic and revenue. NPS scores for the property improved
sequentially every quarter for the entire year, further highlighting the positive impact from the
investments we are making in content and customer experience. The Daily Beast had nearly 18 million
monthly uniques in December (per comScore), larger than The Atlantic, Vox, Salon and Gawker brands,
and up over 40% year-over-year, growing faster than BuzzFeed and The Huffington Post. Its a testament
to the success we are having in delivering intelligent and irreverent journalism with what we believe is
one of the best editorial staffs in the business, not just in digital.
Looking forward, we expect Premium Brands to drive further revenue gains and traffic growth,
particularly in social. On the revenue side, programmati
Page 7 of 11
Applications
On the other side of the house, we reorganized our Applications businesses under a single management
team to better attack opportunities in digital software across both desktop and mobile. IAC Applications
finished out the year essentially flat, with revenue of $761 million and EBITDA of $184 million. During
the 4th quarter, we generated more than 1 million installs a day.
The Consumer segment of this business  where we develop and market our own applications  remains a
powerhouse on desktop with more than 36 million daily active users spread across 120 products on
desktop, and also encompasses 3.2 million daily active users spread across 53 products on mobile and 1.3
million paid subscribers to SlimWare software products.
The strength in this business has been our consistent and proven ability to develop and market new
products which expand our portfolio and generate growth. With 2 billion desktop computer users in
todays mobile-saturated world, we continue to see robust demand for desktop apps, whether that be a
utility for searching for online TV and video, file conversion, or personalized local weather. Increasingly,
our desktop browser products are distributed through native browser app stores across Chrome, Internet
Explorer and Firefox, which, similar to mobile app stores, offer developers opportunities to reach
audiences and leverage the features and functions of the platforms. In this ecosystem weve created
successful digital consumer products by listening to our audience and other signals to identify new
categories, build quickly, and then market to meet the demand.
The headwind in this business has principally been in making adjustments to competitive and other
changes by the browsers and Google. With each change, weve consistently adjusted and returned to
growth, and we expect the same to happen now as we transition to our new search contract terms. In
Applications, the impact comes as a result of our shift toward opt-in and native applications, meaning
apps or extensions that are native to the app store for a given browser. These native apps will typically
yield a single consumer search touchpoint (e.g., a homepage) rather than a series of browser changes. The
overall trend toward native applications is to a large extent inexorable as each of the major browsers have
clearly signaled their intentions to build out their native stores and push developers to adopt them. While
near-term negative to our Applications business, we believe this trend is a positive result for consumers
and, over the long term, provides us with a simpler and very effective channel to reach the worlds 2 
Page 8 of 11
billion desktop users. That trend, combined with locking up a great deal with Google, should set up the
business well for the future.
We have also seen a headwind in RPQ, which was down 13% in the 4th quarter versus the prior year as a
result of both geographic mix and movements in the Google network RPQ and changes weve made to
benefit the customer experience, which had the effect of decreasing RPQ but increasing overall queries.
At times over the years, we have seen moves of plus or minus 10% or more in RPQ in a particular period,
but the overall trend has been positive. Fortunately, when RPQ falls on the revenue side, cost of
acquisition also falls on the expense side, so we are able to continue to market and grow our installs 
albeit at a lower dollar margin per install. If history is any indicator, we expect RPQ to recover, and we
saw early signs of improvement in January.
In mobile, Apalon continues to expand its product line and audience, with Q4 revenue up over 2x yearover-year and MAUs up double digits due to continued success in distributing well-crafted mobile
applications in non-gaming categories. Notable hits in 2015 included a beautifully designed alarm clock
that determines optimal wakeup time and Speak and Translate, which received Apples Best of 2015
in France and Russia and achieved the #1 rank in iOS Paid Reference Apps in over 140 countries since its
launch, according to App Annie. Apalon and the Slimware subscription business together remain small
on a percentage basis, accounting for about 8% of Consumer revenue, but we expect subscriptions and
mobile apps to reach into the double digits as a percent of total Consumer revenue in 2016 and continue to
grow from there.
Overall, we expect the Consumer segment to hold relatively flat for the year on both revenue and
EBITDA and to be in a position to return to growth by year end.
In our Partnerships business, as weve long discussed, we continue to see erosion and expect to see further
decline in 2016 given both secular trends and the loss of some partners that we expect will look for
alternative solutions as we transition toward opt-in. We continue to appeal to branded partners that put
high value on customer experience, but we will see further declines elsewhere and have begun to
restructure the operations accordingly.
Video
Page 9 of 11
In our Video segment, Vimeo continues to grow as it expands its footprint with creators and consumers.
The service completed a year in which it reached 676,000 subscribers on 19% year-over-year growth,
grew subscription revenue 23% and more than doubled gross video on demand sales in its video
marketplace with more than 1.2 million buyers, up 21% quarter-over-quarter and 2.5x year-over-year.
Successful VOD titles included works such as Con Man, one of the five largest crowdfunded video
projects of all time, Rolodex of Hate, a comedy stand-up special from drag queen Bianca Del Rio, and
Oscars Hotel, a paid series created by a number of video stars who built their careers in ad-supported
online video and who are now generating revenue through a paying audience on Vimeo. In addition,
overall video uploads, plays, and viewers reached all-time highs, with Vimeo holding the #4 position
among US video sites for the fifth consecutive month, behind only YouTube, Facebook, and Yahoo.
Worldwide, Vimeo reached over 200 million unique viewers in December according to comScore,
representing a huge opportunity for our creators.
But our ambition is greater. Over its history, Vimeo has attracted creators who value our clean interface
and creator-friendly business model. As Vimeo attracts more, and a greater share of, quality creators
through the right platform features to share and sell video, and we leverage our viewer traffic and VOD
consumer base as a consumer destination, we see Vimeo emerging as a leader in the online video
marketplace. We are seeing fantastic growth in our marketplace and have just begun to scratch the
surface on connecting the hundreds of millions of viewers that Vimeo reaches each month with more and
more of our creators distinctive, ad-free content.
Jeff Kip, Executive Vice President, IAC
As discussed above, consolidated HomeAdvisor revenue growth, at 27%, trailed the growth of the core
domestic business (approaching 85% of total revenue), at 51%, in the 4th quarter. This gap has existed all
year given the exit of the loss-making European revenue streams at the end of the 4th quarter last year, but
it grew a little wider in Q4 because of accounting adjustments made at the time of the restructuring in
2014. Going forward, we expect the gap to narrow as we annualize the restructuring and the international
businesses again shows growth. For the 1st quarter, we expect to see consolidated revenue growth
accelerate from the 4th quarter as domestic growth continues apace and we annualize the European
restructuring. The business will be modestly profitable overall, compared to a loss in the 1st quarter of
2015.
Page 10 of 11
In the segment formerly known as Search & Applications, we had nearly $4 million in restructuring
charges in the quarter primarily related to cost rationalization in the Partnerships business. In the
Publishing businesses, for the first quarter, we expect moderate revenue declines and a modestly higher
EBITDA decline than the 4th quarter. For the Applications businesses, we expect both revenue and
EBITDA declines in the 1st quarter to increase from the 4th quarter rates as a result of both the RPQ trends
and the ongoing declines in Partnerships revenue, as discussed. For the full year, we expect Publishing
revenue to come down somewhat more in 2016 than it did in 2015, driven entirely by the erosion in the
Ask & Other businesses. Applications revenue will fall as well in 2016, although not as much as
Publishing revenue, driven entirely by declines in the legacy Partnership businesses.
The Video segment grew nicely in both the 4th quarter and for the full year. For both the 1st quarter and
the full year of 2016, we expect revenue to grow at or above the 2015 full year rate. In 2015, we made
material investments in both Vimeo and DailyBurn, and based on those investments we expect to see
improved profitability in the segment. While we are still net investing in those businesses, we expect to
reduce the losses in the segment meaningfully in 2016, with a significant majority of the losses coming in
the 1st quarter.
Safe Harbor Statement
This press release and our conference call, which will be held at 8:45 a.m. Eastern Time on February 3,
2016, may contain "forward-looking statements" within the meaning of the Private Securities Litigation
Reform Act of 1995. The use of words such as "anticipates," "estimates," "expects," "plans" and "believes,"
among others, generally identify forward-looking statements. These forward-looking statements include,
among others, statements relating to: IACs future financial performance, IACs business prospects, strategy
and anticipated trends in the industries in which IACs businesses operate and other similar matters. These
forward-looking statements are based on managements current expectations and assumptions about future
events, which are inherently subject to uncertainties, risks and changes in circumstances that are difficult to
predict. Actual results could differ materially from those contained in these forward-looking statements for
a variety of reasons, including, among others: changes in senior management at IAC and/or its businesses,
changes in our relationship with, or policies implemented by, Google, adverse changes in economic
conditions, either generally or in any of the markets in which IAC's businesses operate, adverse trends in 
Page 11 of 11
any of the industries in which IACs businesses operate (primarily the online advertising, general
advertising and dating industries), our dependence on third parties to drive traffic to our various websites
and distribute our products and services in a cost-effective manner, our ability to attract and convert visitors
to our various websites into users and customers, our ability to offer new or alternative products and
services in a cost-effective manner and consumer acceptance of these products and services, our ability to
build, maintain and/or enhance our various brands, our ability to develop and monetize mobile versions of
our various products and services, foreign currency exchange rate fluctuations, changes in industry
standards and technology, the integrity and scalability or our systems and infrastructure (and those of third
parties), our ability to protect our systems from cyberattacks, operational and financial risks relating to
acquisitions, our ability to expand successfully into international markets and regulatory changes. Certain of
these and other risks and uncertainties are discussed in IACs filings with the Securities and Exchange
Commission (SEC). Other unknown or unpredictable factors that could also adversely affect IAC's
business, financial condition and results of operations may arise from time to time. In light of these risks
and uncertainties, these forward-looking statements may not prove to be accurate. Accordingly, you should
not place undue reliance on these forward-looking statements, which only reflect the views of IAC
management as of the date of this press release. IAC does not undertake to update these forward-looking
statements