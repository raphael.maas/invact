Dear Fellow Shareholder,

Cloud computing is transforming the way information technology is architected, deployed and consumed, and EMC has

become a leader of this transformation, which holds massive opportunity for our customers and for our business.

We believe market adoption of cloud computing represents a fundamental shift in industry dynamics that will be as far-
reaching and impactful over time as the adoption of personal computers was a generation ago.

Simply put, cloud computing is about delivering applications online to users at their desktops, on laptops, tablets or

smart phones. This can be done in-house, through an enterprises private cloud, or through third-party, web-based

service providers in the public cloud.

We expect most large enterprises will prefer a hybrid approach that allows them to access some applications like

payroll processing through service providers while retaining control over their most mission-critical applications in

their private clouds. To achieve this hybrid cloud model, customers look to us for help in transforming their data

centers into fl exible, scalable pools of virtualized infrastructure that can deliver IT as a Service to business units in a

much more agile way, while offering the greatest effi ciency, control and choice. Along with VMware and our ecosystem

of strategic partners and service providers, we are accelerating our customers journey to hybrid cloud computing.

Our vision of the future is resonating in the marketplace. In 2011, we achieved another record year of double-digit growth

in revenue, profi t and free cash fl ow. Consolidated revenues grew to $20 billion in 2011, an increase of 18 percent over

2010. On a GAAP basis, net income attributable to EMC increased 30 percent year-over-year to $2.5 billion. EMC generated

2011 operating cash fl ow of $5.7 billion and free cash fl ow* of $4.4 billion, increases of 25 percent and 29 percent,

respectively, over 2010.

Our demonstrated ability to sustain profi table growth year after year has produced a track record matched by few

technology companies of our size. From 2007 through 2011, EMCs revenues have grown at a compound annual rate of

11 percent; non-GAAP earnings per share* have grown even faster, at a compound annual rate of 14 percent. We ended

2011 with $10.8 billion in cash and investments, after returning $2 billion to shareholders through stock buybacks last

year. We also invested 11 percent of 2011 revenues in research and development to enrich our product portfolio and

maintain technology leadership. We continue to manage our cash prudently to meet our objectives of returning value to

shareholders while investing for our future.

Although 2011 was a record-breaking year for our fi nancial performance, our stock performance of negative six percent in

2011 (vs. a zero percent return for the S&P 500 index) was disappointing as macroeconomic uncertainty weighed on investors

concerns in the second half of the year. Still, we remain very confi dent in the market trends and strategy driving our growth.

Since cloud computing is the most signifi cant trend transforming IT today, it is no coincidence that EMC competes in

many of the fastest growing market segments in IT. Segments we serve rank at, or near, the top of surveys of enterprise

customers IT spending priorities for 2012: storage, virtualization, information security, and data analytics. This dynamic,

along with our solid 2011 performance, puts us squarely on track to achieve the fi nancial objectives we announced early

last year: revenue of more than $28 billion in 2014, which represents compound annual revenue growth of at least 13

percent from 2010. We expect earnings per share (on a non-GAAP basis) to grow at a compound annual rate faster than

revenues over the same time frame.

VMware, the EMC majority-owned and publicly-traded market leader in virtualization and cloud infrastructure, contributed

revenue of $3.8 billion in 2011, an increase of 32 percent from 2010. VMware holds the largest market share by far in one

of the hottest software segments in enterprise IT. Our close product and technology integration with VMware is a major

differentiator for us among VMwares rapidly expanding installed base of customers.

Last year, we also accelerated the growth of VCE, the Virtual Computing Environment Company, our joint venture with Cisco,

which is also supported by investments from VMware and Intel. The order rate for VCEs pre-integrated Vblock platforms

surpassed $800 million on an annualized basis in the fourth quarter of 2011, as more customers standardize on Vblock to

virtualize their mission-critical workloads.

In early 2011, we unveiled a large wave of record-breaking storage systems, technologies and new capabilities in what has

proven to be our biggest product launch and strongest portfolio in EMC history. Last year, we transformed our storage portfolio

and gained market share in the mid-tier and small business segments with our new, award-winning EMC VNXTM family of

unifi ed storage platforms, which has received strong acclaim from customers and partners. We also received industry awards

for designing products and sales programs to leverage the strength of distribution partners worldwide. Last year, we added

several thousand new channel partners and made indirect sales a powerful contributor to our market share gains.

To enhance our industry-leading, high-end EMC Symmetrix VMAXTM family of platforms, we introduced innovative software

to fully automate the placement of data on the most cost-effective storage tiers. This reduces customers operating costs

while making it easy for them to adopt disruptive, groundbreaking, ultra high-performance Flash technology, establishing

a market-leading position for EMC in a new segment that is rich in opportunity. Our largest VMAX customer has installed

more than 50 petabytes of VMAX storage, an amount of data that was almost unthinkable for a single enterprise only a few

years ago.

Big Data represents a new technology frontier and a huge opportunity to transform business and traditional business

models. Organizations that learn how to extract value from the vast troves of data inside and outside their businesses will

fi nd new ways to differentiate themselves, create value for their customers, and gain competitive advantagewhether in

the delivery of personalized medical treatments, the creation of smart grids by electric utilities, or through smarter and

more effi cient retailing.

Big Data refers to data sets that have grown too large and too varied in formats for conventional IT. A one-size-fi ts-all

approach to data storage is not ideal for todays environments. Big Data requires purpose-built platforms and new

methods to analyze and act upon insights gathered from unstructured data (e.g., clickstream data or images) that live

outside of traditional relational databases. Our acquisition of Isilon in 2010 helps us to better meet customers cloud and

Big Data needs. Isilon has thrived as part of EMC: its revenue doubled in 2011. Add the capabilities of our EMC AtmosTM

cloud storage platform for geographically dispersed data, and we have unparalleled storage infrastructure for Big Data.

Greenplum, another 2010 acquisition that enhances our Big Data capabilities, enables customers to sort through

enormous data setsmillions of transactions per day, or hundreds of billions of clicks per month. Last year, we introduced

the industrys only unifi ed solution for Big Data analytics. This new platform combines the co-processing of structured

and unstructured data with social networking tools that enable data science teams to collaborate in real time. With this

next-generation analytics solution, we expect to help more customers fi nd the wealth in their mountains of information.

To sift through Big Data for value also requires new skills. Nearly two-thirds of data science professionals believe demand

for their expertise will outpace supply over the next fi ve years. Only one-third are very confi dent in their companys ability

to make business decisions based on new data, according to the largest ever survey of data scientists, conducted by EMC

last year. To help develop these skills, last year we hosted the worlds fi rst summit dedicated to data scientists and

offered new training and certifi cation programs to prepare IT professionals with the knowledge they need for Big Data and

cloud computing.

Our Global Services division also provides advisory, professional and operational services to our customers to establish a

strategic business rationale for EMC solutions and help them maximize the benefi ts of cloud and Big Data technologies.

Our service excellence, recognized by distinguished awards from the Technology Services Industry Association and

validated last year by our all-time-high customer satisfaction scores, is a differentiator in our commitment to maintain

customer loyalty and deliver the absolute best total customer experience in our industry.

Customers also seek our help in building trust in cloud computing through data protection and security. We have grown

our backup and recovery systems division into an essential part of our business. The combination of Avamar and Data

Domain in our portfolio enables us to offer next-generation backup solutions designed specifi cally for the new data center

architectures. Product synergies across EMC and within our backup portfolio are a competitive advantage. To put this in

perspective, Data Domain had annual revenues of $274 million in the year before EMC acquired the business in 2009.

Data Domain and Avamar combined exited 2011 at an annualized revenue run rate of well over $2 billion.

Organizations also need robust security to head off advanced threats. Our RSA security division rebounded from a widely

publicized cyber attack a year ago to grow revenues 13.5 percent year-over-year in 2011, reaching annualized revenue of

almost $1 billion in the fourth quarter, RSAs best quarter ever. Security is a top priority of corporate boards and CEOs, and

customers are very interested in the RSA security management and compliance suite. It integrates technology from Archer

and NetWitness, acquired in 2010 and 2011, respectively, to present a unique, risk-based, agile and contextual approach

to defend against advanced threats. RSA also incorporates realtime analytics of security-related Big Data to identify

threats that traditional security measures can leave undetected.

Our acquisition of NetWitness last year extends our track record of integrating companies with innovative technology,

world-class talent, and complementary capabilities that fortify our portfolio for customers, enlarge our market opportunity,

and accelerate growth. We also continued to expand in the worlds fastest growing markets. In 2011, we opened a new Center

of Excellence in Chengdu, China, to better serve our global customers. The EMC China Center of Excellence is our seventh such

center globally and one of two in the Asia/Pacifi c region, where we grew 2011 revenues 34 percent year-over-year.

We continued to improve the sustainability and energy effi ciency of our operations, products and packaging and achieved

Gold LEED certifi cation for our headquarters. We were honored by EMCs 2011 inclusion in the Dow Jones Sustainability

Index (DJSI) for North America, which tracks the fi nancial performance of leading sustainability-driven companies. We

believe that our use of sustainability benchmarks is not only good for the environment and the world we live in, but also

good for our business.

We reached another transformation milestone of sorts last year when we quietly retired the EMC tagline, Where Information

Lives, which had embellished our logo since 2000. Clearly, we have grown beyond the storage company we once were into

our customers go-to partner for virtualized data centers, and a leader in hybrid cloud computing and Big Data.

On a personal note, I am blessed to work with an extremely talented leadership team at EMC, and they have encouraged

me to extend my transition timeline. The Board of Directors has requested that I remain Chairman and CEO into 2013,

and I have agreed. The Board and I fully expect that my successor will come from within EMCs experienced management

ranks. When the time is right, my successor will be named. Until then, we have much work to do, and I am grateful to have

the strong support of our Board and our leaders throughout the company as we work to realize the full potential of our

business.

In my 12 years with EMC, I have never been more excited about what this great company and our dedicated, hard-working

peoplenow more than 53,000 strong around the worldcan and will accomplish in the future.

Joseph M. Tucci

Chairman, President and Chief Executive Offi cer

EMC Corporation