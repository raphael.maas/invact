DEAR SHAREHOLDERS,
As the sub-prime mess blossomed into a full blown banking and liquidity crisis in 2008, and
as consumers increasingly began to pull back on spending as unemployment and uncertainty
increased, many investors, financial analysts and other interested parties asked with increasing
frequency: What are you guys going to do now? The question was often asked of me and
my associates at Federal as well as to every other real estate investment trust executive that I
know. Responses were varied and included lots of references to slowing down development
projects, cutting back on administrative expenses, renewing leases earlier and a plethora of
other reasonable responses.
Those responses were, however, tactical in nature and didnt really answer the question. The
fact of the matter is that there was and is very little a company can do about the prevailing
conditions if its business plan didnt adequately prepare over the past five years for the inevitable
cyclical nature of the commercial real estate business as it relates to both the capital markets
and economic conditions. Its why the real answer to What are you guys going to do now? for
companies that have entered into this economy on weak footing is the best that we can but
not much. I am pleased to report to you that this is not the case with Federal Realty.
By always managing this portfolio for the long term, Federal Realty has entered into this
unprecedented time in our economy in an enviable position of strength. The strong economic
years of 2005 through 2007 were the time when balance sheets needed to be strengthened
with equity, when high priced acquisitions needed to be made sparingly and only in the best
locations and paid for with long-term or permanent financing, and when leases with tenants
needed to be written protecting the landlord to the greatest extent possible. Thats what we did.

Debt, as a percentage of total capitalization, is one of the lowest in our industry and, since the
majority of our growth comes from well-established, well-located retail destinations that we
have owned for years, our cash flow stream has shown, and should continue to show, relative
strength in these uncertain times.
None of this is meant to suggest that Federal Realty will escape the impact of this very severe
recession; we wont. There are very few industries that have not been detrimentally affected by
the economy. The continued stream of layoff announcements across most businesses, coupled
with far more limited availability of credit, suggests a prolonged period of reduced spending
on both the consumer and commercial levels. It is the uncertainty surrounding the length and
ultimate depth of this recession that creates the greatest challenge in running a company today.
As the landlord to a wide variety of different types of retailers, we have a front row seat to the
reality of increasing retailer failings over the latter half of 2008 and into the first part of 2009.
We would expect that trend to continue for some period of time and result in a continuation of
increasing vacancy. Our entire retail portfolio was 96.7% leased at December 31, 2007, and by
the last day of 2008 that number was 95.0%. That leased percentage will very likely continue
to fall throughout 2009, though we would certainly expect to fare far better than the national
shopping center averages. It has been our experience in the past and our expectation in the
future that the best hedge against a troubled national economy will always lie in the quality of
the real estate location, and by that measure, we are second to none.
As you read through the balance of this letter, consider our 2008 operating performance in the
context of the retail and capital environment in which we were operating. We think the results
are remarkable and really validate the conservative business plan and operating philosophy
that we have followed for many years now. No large impaired asset charges, no unintended
consequences from various joint venture agreements, no large severance or restructuring
charges. Just solid leasing, measured acquisitions and redevelopment, and conservative capital
planning and structure of some very high-quality real estate. Thats Federal Realty.

CONTINUED STEADY GROWTH FROM WITHIN.
Despite the tough economic environment in 2008, Federal Realty was able to produce solid
operating results. For the year, Federal Realty produced funds from operations (FFO) of $3.87
per share, a 6.6% increase over 2007. Same-store property operating income growth, a key
measure of our core operating performance, was over 4% for the year, the highest growth rate
amongst our publicly traded shopping center REIT peers, even with the slowdown in consumer
spending late in 2008. Although occupancy for our portfolio was down over 2007, our results
were positively supported by significant leasing activity. For the year, we were able to complete
300 new leases on nearly 1.2 million square feet of comparable retail space at an average rent
increase of 21% on a cash basis. The higher rent from those new or renewed leases will be an
important contributor to our 2009 cash flow stream.
We believe that these results demonstrate the benefits of a simple, straightforward business
plan made possible by owning high-quality, well-located real estate that has been conservatively
financed. As in previous years, our results were driven by growth in our core operating portfolio
of properties, disciplined acquisitions, and smartly executed redevelopments.

QUALITY LOCATIONS KEEP LEASING UP.
Leasing remains a key competitive strength, and this years leasing activity capitalized on the
value of our locations and the quality of our properties. In a year when many retailers were pulling
back their expansion plans, our team of leasing professionals utilized every arrow in their quiver
to extract as much value as possible from every lease. While rent is the most obvious financial
attribute of a lease, operating attributes are, in many cases, just as important. Terms such as
opening and operating covenants, which require tenants to build, stock and open their store for
at least one day, provide some level of protection for the Trust as retailers decrease store counts
or exit non-strategic markets. In addition, refraining from co-tenancy agreements in virtually all
of our leases minimizes the potential ripple effect of anchor store closings. Our focus on these
non-financial aspects of our leases has been a fundamental priority that can only be realized
through the leverage we gain by owning high-quality assets in strong real estate locations.

PRUDENT AND OPPORTUNISTIC ACQUISITIONS.
Our 2008 results also benefited from our disciplined approach to acquisitions. Over our 46-year
history we have selectively acquired properties on a one off basis rather than focusing on
large portfolio acquisitions of mixed quality. Over the past several years, Federal Realty didnt
get caught up in the real estate feeding frenzy when prices for real estate assets were high,
regardless of their quality. We maintained our standards and bought only high-quality, welllocated real estate that we believe will create value for our shareholders over the long term.
Federal Realty was able to accomplish a long-standing acquisition goal by entering the South
Florida market in 2008. We made our debut in South Florida with the acquisition of Del Mar
Village in Boca Raton. Del Mar Village is a 154,000-square-foot community shopping center
anchored by Winn-Dixie and a highly productive CVS Pharmacy.
In September, we added to our South Florida portfolio with the acquisition of Courtyard Shops,
a 127,000-square-foot property anchored by Publix in Wellington15 miles southwest of West
Palm Beach. Both of these properties fit our high acquisition standards in terms of population
density, affluence and barriers to entry. While South Florida has been hit especially hard by the
housing crisis and recession, we remain confident in the value that these centers will create over
the long term and we look forward to the opportunities that will undoubtedly avail themselves in
the coming months and years to those buyers with the means to take advantage.

SMART, BALANCED REDEVELOPMENT PROJECTS POSITION US FOR THE LONG TERM.
Federal Realtys history of capitalizing on our strong retail locations for redevelopment
opportunities continued in 2008. Our highest profile project was Arlington East, our ninth
phase of redevelopment at Bethesda Row in Bethesda, Maryland. Arlington East consists
of 45,000 square feet of ground floor retail below 180 luxury apartment units, an additional
244 parking spaces in an underground garage and a pedestrian-friendly street called
Bethesda Lane that runs through the center of the structure. Take a look at the cover of this
Annual Report thats Bethesda Lane. Over the next few years well continue to add to our
award-winning, mixed-use neighborhood in Bethesda, and look forward to our next phase
of redevelopment at the propertythe addition of a three-level retail building anchored by a
high-quality fitness center on Hampden Lanewhich is expected to stabilize in late 2010.
Redevelopment projects like Arlington East continue to be supplemented with our bread and
butter redevelopment opportunities such as grocer expansions, anchor re-tenanting and pad
site opportunities at our proven retail locations. In 2008 we were able to add Trader Joes
as a grocery-anchor and completed a faade renovation at Eastgate Shopping Center in
Chapel Hill, North Carolina. Looking ahead, we are adding new retail space at The Village at
Shirlington, our successful mixed-use destination in Arlington, Virginia, and were expanding
a successful grocer at Lancaster Shopping Center in Lancaster, Pennsylvania. These bitesized redevelopments allow us to continually improve the quality of our core portfolio assets
in a relatively low-risk fashion, which, in turn, allows us to steadily increase rents and fuels
our low-risk growth strategy. Redevelopment is a constant focus of Federal Realty, not a
passing attribute of our business plan. Although the struggles faced by retailers in the current
economic environment are likely to result in higher vacancy rates, they are also sure to create
new redevelopment opportunities that will improve the long-term value of our properties.

Redevelopment is a constant focus
of Federal Realty, not a passing
attribute of our business plan.

RIGHT-SIZING.
As 2008 progressed, it became clear that the economic stagnation that began early in the
year would not turn around quickly. We began to see retailers cut back on expansion plans,
consumers slow down their spending, and capital market liquidity begin to dry up. By the
middle of the year, it was clear that we would need to prepare our organization for a prolonged
downturn. One of the most difficult decisions we made was to reduce staff by 15 to 20 percent
and knowingly impact the lives of over 40 families. We took these actions in order to put our
company in the best position possible to weather 2009 and 2010 and, more importantly, to be
sure that difficult real estate decisions were being made close to their source efficiently and
with the best judgment possible. I am confident that the tough personnel decisions we made
improved our company and the prospects of the remaining team members.

STRONG CAPITAL POSITION MAXIMIZES OUR FINANCIAL FLEXIBILITY.
No discussion of 2008 and 2009 would be complete without appropriate focus on the Trusts
capital position. Our disciplined, long-term approach over the past several years allowed the
Trust to prudently raise capital, including issuing additional common equity, when opportunities
were available. Our focus has always been to fund our long-term real estate assets with longterm or permanent financing, and to limit the impact of rising and falling interest rates on our
financial results. We have entered this downturn with very low levels of leverage, 32% on a
market value basis and 44% on an undepreciated book basis as of December 31, 2008. In
addition, we have a portfolio of unencumbered assets that creates borrowing capacity of at least
$1 billion, which dramatically increases our financial flexibility in these difficult capital market
conditions. We have $375 million of debt maturing in late 2009, yet we remain confident in our
ability to attract capital to refinance our maturing debt months beforehand.
Because of our strong capital position, we were able to increase the common dividend again
in 2008. We strongly believe that a cash dividend by a REIT is fundamental to its very purpose,
and central to the investment rationale of a very large percentage of our owners. At a time when
many companies, both in and out of the real estate market, are suspending or cutting their
dividends to shareholders, 2008 was the 41st consecutive year that Federal Realty was able to
raise its dividend, the longest record in the REIT sector.

IN CLOSING.
Let me end where I started. What are you guys going to do now? is indeed the right question.
The landscape among real estate companies will look far different in the next four years as
our country fights hard to regain strong economic footing, and it will. There will be casualties
along the way, as well as an unprecedented level of opportunities, both on the acquisition and
development fronts. Federal Realty will be there to take advantage of these; were beautifully set
up for it. With the continued support of our engaged, talented, and economically conservative
Board of Directors and the patient, disciplined approach of this committed management team,
we look forward to meeting the challenge head on and realizing the opportunities that will
undoubtedly avail themselves. Thank you for your continued support and we look forward to
serving you for many years ahead.

Donald C. Wood
President & Chief Executive Officer
Federal Realty Investment Trust
March 2009