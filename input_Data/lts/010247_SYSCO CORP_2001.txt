SYSCOs mission  helping customers succeed  is the core principle of our business.
This conviction is first in our thoughts and foremost in our actions, for our success is
but a reflection of the success of our customers. Our focus on that mission in fiscal
2001 produced outstanding sales and earnings growth.
Sales topped the $20 billion mark, increasing 12.8
percent to $21.8 billion, as compared to $19.3 billion last
year. Real sales growth, after eliminating 4.5 percent for
acquisitions and 2.5 percent for food cost inflation, was
5.8 percent. On a calendar year basis, SYSCOs real sales
growth reached 8.9 percent for 2000, while the industry
grew 2.7 percent for the same period, with SYSCO again
outpacing the industry rate as it has traditionally.
Diluted earnings per share were strong, rising to $0.88
or 29 percent more than the $0.68 recorded last year
prior to an accounting change. Net earnings for the
period were $596.9 million, 31.6 percent higher than last
years $453.6 million (before the accounting change).
Return on shareholders equity increased again in fiscal
2001 to 31 percent as did return on average total capital,
which was 20 percent.
This stellar performance was attributable to a variety
of factors, including the progress of our Customers Are
Really Everything to SYSCO (C.A.R.E.S.) customer
relationship management initiatives. In addition, a
persistent focus on marketing associate-served customers
increased sales to this customer type to 55 percent of
traditional foodservice sales compared to 54 percent in
fiscal 2000. Also, the continuing recognition by our
customers of the quality and value of SYSCO Brand
products boosted sales of such products to 53 percent of
marketing associate-served sales, up three percentage
points from 50 percent last year.
The SYGMA Network, Inc., our chain restaurant
distribution specialist, recorded a strong 12 percent sales
increase to $2.42 billion from the $2.15 billion reached
last year. During fiscal 2001, SYGMA secured a
distribution agreement to serve 264 Applebees, Inc.
restaurants in Indiana,Michigan, New England, Ohio
and Western Pennsylvania.
In fiscal 2001 we continued to focus on gaining greater
efficiency and productivity while constantly monitoring
costs. Our technology initiatives enable us to expand our
knowledge in all aspects of our business and improve
customer service to even greater levels. Pretax margins
consistently have been improving and 62 of our broadline
companies increased operating pretax margins compared
to 54 last year.
Although fuel and energy costs have been at record
levels throughout the country, SYSCO was not significantly
impacted. Fuel price increases were mitigated
somewhat by increases in cases delivered per mile, as well
as other measures, resulting in fuel costs of 0.39 percent
of sales for the year compared to 0.35 percent last year.
Energy costs also were higher in certain parts of the
country, particularly the West Coast, but on an overall
basis, such costs did not have a significant impact on
total expenses.
Acquisitions to enhance geographic coverage or
broaden product offerings were components of SYSCOs
growth strategy during fiscal 2001 and will continue to be
important going forward. Five companies, with combined
annual sales volume of $706 million for their most recent
fiscal years, were acquired in fiscal 2001, including two
custom-cutting meat companies, two Canadian broadline
operations and a specialty supplier to the hospitality and
lodging industry. In addition, SYSCO completed the
purchase of another specialty meat company, Fulton
Provision Co., at the beginning of fiscal 2002.
Joining our three existing custom-cutting meat
operations, Fulton, as well as the A. M. Briggs Company
and five Freedman Meats, Inc. facilities, further extends
the reach of our fresh, precision-cut steak offerings. As
part of our ongoing strategy to provide a consistent
supply of quality beef products, properly aged and cut to
customers specifications, products from all 10 locations
now will be uniformly marketed under the Buckhead
BeefTM brand east of the Rocky Mountains and the
Newport PrideTM brand to the west.
The addition of North Douglas Distributors, Ltd.,
(Vancouver Island, B.C.), and HRI Supply (Kelowna,
B.C.) increased SYSCOs Canadian presence to nine
locations.With combined annualized sales of $79 million
(USD) for their latest fiscal years, both operations provide
SYSCO with distribution efficiencies, enhanced
geographic coverage and the ability to further penetrate
Canadas approximately $9 billion (USD) foodservice
distribution market.
The purchase of New Jersey-based Guest Supply, Inc.,
a specialty distributor to the lodging and hospitality
industry, exemplifies SYSCOs stated objective of
acquiring companies that provide a market or product
niche. Guest Supply, which generated sales of $366 million
for its fiscal year ended September 29, 2000, operates
from 16 locations across the United States and Canada.
SYSCOs existing lodging customers now have access to
Guest Supplys extensive product line of guest amenities,
furniture and textile products, while Guest Supplys
customers can benefit from SYSCOs distribution
network and broad selection of foodservice products
and related supplies.
Internal growth also contributed to SYSCOs
geographic expansion. Fold-out operations opened in
Suffolk, Virginia in fiscal 2001, and Sacramento,
California in early fiscal 2002, providing customers
improved service and access to a wider array of products
in these two vital markets. In addition, construction is
progressing on fold-out operations in Las Vegas,
Nevada and Columbia, South Carolina that will expand
our service in the burgeoning Las Vegas market and
allow us to further penetrate the southeastern United
States, which has been a steady contributor to our
growth. The new operations are expected to begin
supplying customers in the spring and fall of calendar
2002, respectively.
SYSCOs expansion activities, as well as ongoing
maintenance programs, resulted in investments of
$341 million in facilities, fleet and equipment, with
facilities and fleet accounting for 76 percent. This
compares to capital expenditures of $266 million in fiscal
2000. Since prudent and timely capital investment is
crucial to managing our growth, we anticipate ongoing
capital expansion needs and project that fiscal 2002
expenditures should range between $425 million and
$450 million. In keeping with our historical results,
SYSCO continues to generate sufficient cash flow from
operations to fund internal growth opportunities. Total
debt was $1.02 billion at fiscal year end, including $961
million long-term debt, resulting in a long-term debt to
total capitalization ratio of 31 percent.
In November 2000, the Board of Directors approved a
two-for-one stock split, payable December 15, 2000. It
was the ninth split in SYSCOs history and the third in
less than a decade. SYSCO also has been actively
repurchasing shares in excess of those issued since 1996
and in November 2000 the Board approved a new
authorization. During fiscal 2001 SYSCO purchased
16 million shares for a total of $428 million, with
approximately 8 million shares remaining to be
purchased under the November 2000 authorization.
Also in November, the Directors increased the
quarterly cash dividend 17 percent, from $0.06 per share
to $0.07 per share, representing the 32nd dividend increase
in SYSCOs 31-year history.
SYSCOs commitment to the success of its customers
requires the ongoing development of talented managers
who will embody this spirit and lead the company well
into the future. Thus, several individuals were promoted
at the corporate level, including the following: K. Susan
Billiot, Assistant Vice President, Human Resources;
Cameron L. Blakely, Assistant Vice President, eBusiness;
James C. Graham, Senior Vice President of Foodservice
Operations, Southwest Region; Charles A. Hastreiter,
Assistant Vice President,Merchandising Services; John T.
McIntyre, Assistant Vice President,Manufactured and
Dry Groceries; Barry Robinson, Assistant Vice President,
Healthcare Sales and Marketing; and Brian M. Sturgeon,
Vice President of SYSCO Corporation and President and
Chief Operating Officer of FreshPoint, Inc.
Mr. John F.Woodhouse, who joined SYSCO at its
inception as Chief Financial Officer and a founding
Director, retired on December 31, 2000 as Senior
Chairman. Mr.Woodhouse was instrumental in shaping
SYSCOs foundations and in guiding the company to
achieve an impressive sales and earnings performance
during his career. In addition, two Directors, Arthur J.
Swenka and Thomas B.Walker, both of whose contributions
have been invaluable, retired from SYSCOs
Board of Directors.
We anticipate that the historical trend of increased
demand for meals prepared away from home will
continue during fiscal 2002 and beyond, fueled by the
limited amount of personal time available to consumers
today as well as the entertainment value of dining out.
We believe that our long-term growth objectives are
achievable: high single-digit real sales growth; a
minimum growth in earnings per share of five
percentage points above the real sales growth rate; a 33
percent return on equity; and a long-term debt to total
capitalization ratio of 35 to 40 percent. Our historically
strong financial position will support our ability to invest
in talented people, consistent quality products and
market-expanding internal and external opportunities.
We remain committed to becoming the supplier of
preference and fulfilling all our customers needs for
food and related products, including specialty products,
and in the process further fueling their success in the
$190 billion foodservice/hospitality industry we serve.

Charles H. Cotros
Chairman and Chief Executive Officer
Richard J. Schnieders
President and Chief Operating Officer