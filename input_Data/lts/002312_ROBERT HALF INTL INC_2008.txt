TO OUR STOCKHOLDERS

The global financial crisis made 2008 one of the most challenging years in the companys 60-year history. All
of our business units felt its effects, especially later in the year as conditions worsened.
Revenues in 2008 totaled $4.6 billion, down 1 percent from the prior year. Net income of $250.2 million
declined 16 percent from 2007. Diluted net income per share slipped 10 percent to $1.63, on 6 percent fewer
average shares outstanding.
Economic activity has continued to lose momentum since the current U.S. recession officially began in December
2007. Gross domestic product (GDP) in the first two quarters of 2008 grew at modest annual rates of 0.9 percent
and 2.8 percent, respectively. Those increases were followed by contractions of 0.5 percent in the third quarter and
6.2 percent in the final three months of the year. The fourth quarters steep decline was the fastest rate of GDP
decrease since 1982.
Downward trends in labor markets generally reflected the weakening global economy. In the United States, the
unemployment rate jumped from 4.8 percent in February 2008 to 8.1 percent a year later. Since the recession
began, nonfarm payroll employment has declined by about 4.4 million jobs. More than half of that decrease occurred
in the last four months.
Our business in 2008 mirrored economic and labor market conditions. During the first half of the year, revenues and
net income from staffing operations were at near-record levels. Revenues in the third and, especially, fourth quarter
slowed as companies dramatically cut back on hiring. While net income in those periods decreased, we were able to
limit the decline through aggressive cost management.
Global economic conditions remain unsettled, but we believe we have effective assets to employ in meeting future
challenges. We are market leaders in the professional disciplines we serve. And we have the resources needed to
strengthen our position, particularly against smaller, less-established competitors. We serve a diverse client base
of primarily small to midsize companies and have no disproportionate customer or industry concentration. We have
a strong financial position with ample cash and essentially no debt. Most important, we have an experienced field
management team that has led the firm successfully through past downturns. We are confident in their abilities.
FINANCIAL CONDITION
In 2008, Robert Half International again demonstrated its ability to generate cash. Full-year cash provided by
operating activities increased to $447 million from $411 million in the prior year. After deducting funds used in
investing activities, free cash flow grew to $352 million from $295 million in 2007. On an interim basis, operating
cash flows showed solid year-to-year gains throughout the year, especially the last two quarters, when revenues and
net income comparisons softened.
Robert Half International is not a capital-intensive business. Capital expenditures in 2008 were $73.4 million, which
was less than expected. We moderated our spending when it became clear the economy was slowing. Last
years outlays were equivalent to 1.6 percent of revenues, slightly below the prior five-year average of 1.7 percent.
Spending was focused primarily on enhancing our information technology capabilities. We upgraded our PeopleSoft
system, which should increase efficiencies in our front- and back-office systems. We also improved web-based
applications with the aim of better serving our clients and candidates. Looking ahead, we are approaching capital
spending in a fiscally prudent manner, given the current uncertain economy.
We returned the bulk of last years $352 million in free cash flow to shareholders through share repurchases
and cash dividends. In 2008, we purchased 9.4 million of our shares for $203 million under our latest board
authorizations. The share repurchases continued a long-standing program that began in 1997. Since then, we have
used $1.7 billion of internally generated funds to acquire a total of 68.2 million shares in open-market transactions.
Last year, we paid a $0.11 per share quarterly cash dividend. Those distributions consumed $69 million in cash.
Our board recently voted to increase the quarterly cash dividend to $0.12 per share. The board initiated a quarterly
dividend in 2004 at $0.06 per share and has increased the payout in each of the succeeding five years.

We made no acquisitions last year. That is consistent with our stated preference for growing the business
organically. We continue to believe that there is an opportunity to expand our professional staffing operations and
Protiviti. It is our view that well-managed organic growth carries less risk than does expansion through acquisitions.
That does not mean we have ruled out acquisitions; instead, it means we remain selective. We are open to acquiring
proven businesses that allow us to diversify and that fit our culture. In particular, we are attracted to companies with
compatible service offerings we can expand through our network.
Our balance sheet is solid. We did not spend all of the cash we generated in 2008. The year-end cash balance
increased to $355 million from $310 million 12 months earlier. We remain virtually debt free. While our business
requires few fixed assets, the importance of accounts receivable does impart a working capital intensity to the
business. Last year ended with receivables of $485 million, representing more than a third of $1.4 billion in total
assets. Each year, we fill hundreds of thousands of job orders to place temporary and full-time workers for tens of
thousands of customers. Ensuring timely collection of associated receivables requires a strong focus, especially in
this economic environment. We have seen no meaningful change in the aging or collection period of receivables as
reflected by the average days sales outstanding (DSO) measure.
DIVISIONAL HIGHLIGHTS
Robert Half International pioneered recruitment in the accounting and finance specialties more than 60 years ago.
Our three accounting and finance staffing divisions remain the backbone of the company, producing more than
60 percent of total revenues.
ACCOUNTEMPS is our largest division, with 2008 revenues of $1.74 billion, or 38 percent of the total. This units
revenues were virtually flat compared to those of the prior year. Like our other divisions, its business declined as the
year progressed, with the fourth quarter being the weakest.
ROBERT HALF MANAGEMENT RESOURCES revenues last year totaled $626 million, or 14 percent of the total
and a 1 percent gain over the prior year. This unit focuses on high-level accounting and finance engagements. It
imparts flexibility to our U.S. business model by deploying Protiviti resources to Robert Half Management Resources
assignments where appropriate.
ROBERT HALF FINANCE & ACCOUNTING, our original business, provides specialized permanent placement
services. Last years revenues of $427 million represented 9 percent of the total and declined 4 percent for the
year. Over time, this business has shown itself to be the most sensitive to swings in the economy. This unit was
negatively affected by deteriorating labor market conditions in the latter half of 2008. In fact, the pace of permanent
placement decline has been much more rapid than in earlier downturns.
OFFICETEAM is our administrative staffing unit. Revenues of $828 million were 18 percent of the total and
fell 4 percent from 2007. This division saw sequential quarterly revenue declines in 2008. That performance was
consistent with prior downturns in which administrative jobs felt the effects of softening hiring demand earlier than
other disciplines.
ROBERT HALF TECHNOLOGY showed a modest revenue gain last year. Its revenues of $437 million were
3 percent ahead of the prior year and accounted for 9 percent of the total. Demand for help desk and technical
support positions was soft. However, the staffing needs in technical development and programming held up relatively
well, partly due to the project nature of the work, much of which is aimed at cost savings. Rather than halt work, many
clients opted to continue funding projects that could be justified by eventual cost savings.
INTERNATIONAL STAFFING revenues of $1.16 billion were 29 percent of the staffing total. Last year was the
first time our annual international staffing revenues exceeded $1 billion. A portion of the 17 percent year-to-year gain
came from the effects of translating stronger foreign currencies into U.S. dollars. The constant currency increase
was 14 percent. Growth in non-U.S. operations was robust in the first half of the year, but revenues slowed in the
third quarter as world markets began to feel the effects of the U.S. slowdown. The United Kingdom has been hit
especially hard. Our business in this competitive market is driven more by financial services than elsewhere in
our network. On the European continent, we saw modest full-year constant currency gains in Belgium, Germany
and France. Economic conditions in the Asia-Pacific region, where our overseas presence is the smallest, have
weakened. We now have staffing operations in 110 locations in 20 countries outside the United States.

PROTIVITI
Protiviti was launched in 2002 and has become an important addition to our business. Its revenues last year
were $547 million, little changed from the prior year and 12 percent of the total. Protivitis international revenues
accounted for 30 percent of the total. Its non-U.S. revenues increased 13 percent while domestic revenues declined
modestly. In Protivitis relatively short existence, it has created a respected brand name and a worldwide presence.
Protiviti ended last year with offices in 62 locations in 17 countries.
Protivitis annual revenues have flattened at about a half billion dollars over the past three years. The primary reason
has been a decline in revenues produced by compliance-related projects, mainly those tied to the Sarbanes-Oxley
Act (SOX). Most large businesses have made good progress becoming compliant with SOX, including its more
demanding sections. Moreover, evolving regulatory guidance has encouraged many companies to reduce their
reliance on outside firms to support their compliance efforts. We are continuing to rebalance Protivitis revenue base
by expanding its suite of noncompliance-related services. Successful efforts in this area have included consulting
engagements addressing controls remediation, life cycle asset management, IT security and privacy, bankruptcy and
restructuring, business continuity, and other solutions practices.
The transition to a more balanced revenue mix has taken longer than expected. This is partly because of setbacks
from last years loss of a handful of major international clients that were either acquired or delisted, and partly due to
the effects of the economic slowdown. Protiviti did not exist when we were last confronted by a recession, and we
did not know how the business would behave in a slowdown. So far, its sensitivity has shown to be similar to that
of our staffing operations. Revenue comparisons held up well early in the year, but they softened in the second half.
In response, Protivitis management has shown a commitment and an ability to control its expenses globally. Its
aggressive actions enabled Protiviti to remain profitable in all four quarters of 2008.
We are positive about Protivitis prospects. We have a solid base of revenue from established service offerings and
are successfully introducing new services. Our global network of offices largely is in place. We expect to continue
to streamline our cost structure and will consider further expense reductions as conditions warrant. We are dealing
with the effects of recessionary pressures on the divisions top and bottom line but continue to believe Protiviti will
be a significant long-term contributor to Robert Half International

FINDING OPPORTUNITY IN
CHALLENGING TIMES
Clients seek our staffing services to gain labor flexibility in their businesses and other enterprises. It is an
inescapable fact that during periods of economic uncertainty, they will require that flexibility, and providing it
is part of our appeal. Our core value proposition applies in good and bad times: We provide skilled accounting,
financial, technology, administrative, marketing and legal talent when needed and for as long as it is needed,
increasing our clients efficiency and often saving them money.
In all of our business lines, clients and prospective clients are recognizing that talented, resourceful people are
essential during a downturn. It is under those conditions that firms can least afford to let quality and customer
service slip. Our staffing solutions can help companies meet these commitments while remaining within budget.
Banks, financial services firms and government entities can all benefit from our services. During the early 1990s,
Robert Half International had a significant role in helping clients through the process of taking over and closing
troubled savings and loans businesses. That experience tells us that todays economic situation presents our
staffing divisions and Protiviti with an opportunity to participate in worldwide remediation efforts to deal with the
financial crisis and its aftereffects.
We also are finding opportunity on the candidate side in current labor market conditions. The downturn has
displaced many outstanding professionals who would be welcome full-time additions to a companys team
under any economic conditions. We are, thus, provided with the opportunity to place experienced, sought-after
candidates who otherwise might not be available to clients.
LOOKING AHEAD
Economic and labor market conditions around the globe clearly will present challenges in 2009. However, this
environment will not stop us from doing what we do best: recruiting talented professionals to fill specialized
positions on a temporary or full-time basis. We have a long record of doing just that in North America and,
increasingly, all over the world.
The focus on current economic and labor market conditions obscures a compelling, longer-term demographic
story underlying our business outlook. Many baby boomers are fast approaching retirement age. Their exit from
the workforce could lead to shortages of experienced talent in the professions and industries we serve. The
growing number of retired professionals could provide us with a supply of workers who opt to consult from
time to time to supplement retirement incomes. This trend could accelerate in the current period of economic
uncertainty.
The Internet has become an integral part of our operations. We have invested in state-of-the-art technology
platforms, including those that emphasize video and multimedia. Recently developed projects with websites such
as LinkedIn and Facebook are putting us in touch with more candidates and clients. Our Internet presence has
meant increased efficiency, productivity and speed, as well as better matches and improved service levels.
Government officials have just begun to address regulatory reform of the nations financial institutions and
markets. Protiviti is well-positioned to assist clients in complying with regulations that may emerge in coming
months and years. There is evidence to suggest smaller companies, as a group, may be lagging larger businesses
in the effectiveness of their internal controls. At the same time, they may lack the internal capabilities to improve
internal controls. This provides near-term opportunities for Protiviti, working with Robert Half Management
Resources, to aid affected businesses in meeting newly enforced regulations and deadlines.
The proposed global conversion to International Financial Reporting Standards (IFRS) is a source of potential
revenue for Robert Half International, particularly our Protiviti unit. Most publicly traded companies now preparing
financial statements using U.S. GAAP will need help making the transition to the new standards. Early work
will involve determining the technical differences in standards as they apply to specific companies. That will
be followed by the bigger task of changing internal processes, controls and documentation needed to comply
with IFRS technical requirements. The Securities and Exchange Commission has issued a proposed roadmap
that could lead to the required use of IFRS for covered U.S. companies in 2014, or earlier in some cases. It is
noteworthy that the transition to IFRS in other countries has taken longer and been more complicated than
anticipated. That suggests companies can benefit from getting an early start on the conversion process.

At the heart of our business are people who are committed to helping companies find the talent they need to
succeed. That internal motivation is what allows us to provide excellent service to clients year after year, in
good and bad times.
We will no doubt continue to face economic uncertainty as 2009 progresses. But we believe our strong
financial and competitive position, as well as the skills, experience and dedication of all of our employees, will,
as they always have, help us emerge from this period a stronger company.
We would like to thank our field and corporate management teams for their leadership, and our board of
directors for their counsel. And, finally, we wish to express our appreciation to you, our stockholders, for your
ongoing support and loyalty.
Respectfully submitted,
Harold M. Messmer, Jr.
Chairman and
Chief Executive Officer
March 6, 2009
M. Keith Waddell
Vice Chairman, President and
Chief Financial Officer
March 6, 2009