Report to Stockholders
In my reports to you during the past five years,
I have emphasized the strategic steps we
have taken to strengthen our financial and
operational performance. The execution of
our strategy to balance near-term profitability
with long-term growth has positioned the
company to capitalize on strong energy prices
to generate a growing stream of income
and cash flow, and secure new investment
opportunities. As a result, our performance
in 2006 again ranked among the strongest in
the oil and gas industry as demonstrated by
a series of key metrics that we believe best
measure comparative financial performance.
Market Performance
After adjusting for a two-for-one stock split in August
2006, Occidentals year-end closing stock price of
$48.83 per share was the highest year-end stock
price in the companys history and 22 percent higher
than the previous year-end record of $39.94 set in
2005. Our 2006 total return to stockholders, based
on stock price appreciation plus dividend reinvestment,
was 24 percent. Over the past five years,
Occidentals total return to stockholders of 315
percent substantially outperformed the total returns
of 195 percent recorded by the Standard & Poors
500 Oil and Gas Exploration & Production Index and
125 percent by the Standard & Poors 500 Oil and
Gas Integrated Index.
Financial Performance
Consolidated net income was nearly $4.2 billion
compared to the $5.3 billion we earned in 2005.
Core income, which excludes special items, rose
to an all-time high of $4.3 billion. Cash flow from
operations was approximately $6.4 billion. In addition,
we received total proceeds of $1 billion from the sale
of non-core properties that were part of Vintage
Petroleum, which we acquired early in the year.
We also received $250 million from the sale of 10
million shares of our investment in Lyondell Chemical
Company. We used $3 billion of our cash flow to
fund our 2006 capital program, used $2.5 billion for
acquisitions and $1.5 billion to repurchase 30.6
million shares of Occidental common stock. We
also paid off $895 million of debt, including debt
assumed as part of the Vintage acquisition, and we
paid dividends totaling $645 million. At year-end
2006, Occidental had approximately $1.6 billion of
cash and short-term investments on hand.
We also increased the annual dividend rate by 24
percent, which was the fifth increase since 2002.
The board of directors will continue to evaluate the
dividend policy in keeping with our commitment to
generate top quartile total returns for our stockholders.
During 2006, our return on capital employed was
21 percent, and the three-year average from 2004
through 2006 was 25 percent. Our return on equity
in 2006 was 24 percent, and the three-year average
was 30 percent. During the same three-year period,
our stockholders equity grew by 142 percent
from $7.9 billion to $19.2 billion.
Op erations Performance
For the third consecutive year, oil and gas segment income set a
new record. Oil and gas earnings of $7.239 billion were 21 percent
higher than the $5.968 billion this segment earned in 2005.
The improvement was mainly the result of higher
crude oil prices and combined oil and gas volumes.
Oil and gas income on a barrel of oil equivalent
(BOE) basisafter taxes and before interest
expensewas $20.51, which we believe will
place Occidental in the top quartile among large
capitalization oil and gas industry peers for the
eighth consecutive year.
Worldwide oil and natural gas production averaged
601,000 BOE per day in 2006, which is 14 percent
higher than the 2005 average production of 526,000
BOE per day. Our oil and gas production for the
fourth quarter 2006 averaged a quarterly record high
of 616,000 BOE per day, up 5 percent from the third
quarter and 13 percent from the fourth quarter 2005.
Last year, we continued adding proved oil and gas
reserves at a pace well ahead of production. In
2006, we produced approximately 219 million BOE
while adding 511 million BOE of proved reserves
from all sources. At year-end 2006, Occidentals
total proved reserves from all sources rose to 2.9
billion BOE. The United States accounted for 72
percent of our total proved reserves followed by the
Middle East with 18 percent, Latin America with 9
percent and 1 percent for other operations.
United States
Combined United States oil and natural gas
production, which accounted for 61 percent
of total worldwide production, increased to an
average of 367,000 BOE per day, a 6-percent
increase compared to 2005. The largest operation
is in the Permian Basin of west Texas and southeastern
New Mexico and accounted for an average
of 199,000 BOE per day, or 33 percent of our
daily worldwide production.
Production from California averaged about 129,000
BOE per day, which was 21 percent of the worldwide
total.
The assets we acquired from Vintage Petroleum in
California, Argentina and Yemen were an excellent
strategic fit with our existing core operations. The
September acquisition of oil and gas producing
properties from Plains Petroleum further strengthened
Occidentals operations in California and the
Permian Basin. These 2006 acquisitions are part of
our business strategy to achieve profitable growth
by focusing on large, long-lived assets in our core
geographic areas.
Middle East/North Africa
Occidentals net 2006 production from our Middle
East/North Africa operations increased by 15 percent,
compared in 2005 to an average of 118,000 BOE
per day, and accounted for 20 percent of our total
worldwide production.
The primary factor accounting for the increase was
a full years worth of production from our historic
contract areas in the prolific Sirte Basin in Libya,
compared to four months of production in 2005.
Occidentals net production from Libya averaged
23,000 BOE per day in 2006, compared to an
average of 8,000 BOE per day in 2005. We continue
to hold discussions with Libyan officials about the
potential to increase production from these and
other producing fields through the application of
innovative enhanced recovery techniques.
Our success in winning nine new exploration blocks
in the EPSA-IV oil and gas licensing round in Libya
in January 2005 has been followed by the most
extensive seismic data acquisition program in the companys history. Occidental is the operator and
holds a 90-percent exploration working interest in
onshore Blocks 106 and 124 in the Sirte Basin,
Blocks 131 and 163 in the Murzuk Basin and Block
59 in the Cyrenaica Basin. Occidental also holds a
35-percent exploration working interest in offshore
Blocks 35, 36, 52 and 53. Occidentals total
exploration and production acreage in Libya
encompasses an area of approximately 130,000
square kilometers, making Occidental the largest
net holder of oil and gas acreage in the country.
Occidental and its partners have thus far accumulated
seismic data covering 20,000 kilometers. Plans are
in place to gather data spanning an additional 6,000
kilometers. Our ongoing analysis of the seismic data
has already led to the identification of a number of
attractive prospects and we have just begun an
extensive drilling program.
Since July 2005, when the Sultanate of Oman
awarded Occidental a 45-percent working interest
in a contract to operate one of the largest oil fields
in Oman, work in the Mukhaizna field has been
moving forward according to plan. Occidental and
its partners plan to implement a large-scale steam
flood to increase production to approximately
150,000 barrels per day within the next few years
and to recover approximately 1 billion barrels of oil
over the life of the project. During 2006, Occidental
drilled a total of 40 wells in the Mukhaizna field, and
is on track to reach its objectives.
The Dolphin Project, the premier transborder natural
gas project in the Middle East, made significant
progress in 2006. Construction was completed on
all the projects upstream and downstream components
by year-end except the giant gas processing
plant located at Ras Laffan in Qatar. The project
involves the initial development of approximately two
billion cubic feet per day of natural gas from Qatars
giant North Field. The processed gas will be
transported from Ras Laffan through a 48-inch
diameter, 260-mile long pipeline to supply power
and water desalination markets in the United Arab
Emirates and Oman. Occidental has a 24.5-percent
interest in this mega project.
Qatar remains our largest operation in the Middle
East/North Africa. Our net production in Qatar of
43,000 barrels per day in 2006 accounted for more
than one-third of our total production for the region.
Latin America
Our production in Latin America averaged approximately
76,000 BOE per day, which made up 13
percent of our total worldwide production.
The acquisition of Vintage Petroleums operations
in Argentina contributed significantly to Occidentals
2006 Latin American production. Occidentals share
of production from the Argentina operations
averaged approximately 39,000 BOE per day, on
an annualized basis, for the 11 months the company
held the properties. The integration of the former
Vintage assets into our operations has largely been
completed. We are excited by the large number of
opportunities to increase production in Argentina,
and we are moving forward with plans for the
accelerated development of these assets.
In Colombia, we completed our evaluation of
a multiphase enhanced recovery project in the
La Cira Infantas field and we are moving forward
with plans for the full development of the field in
cooperation with Ecopetrol, Colombias state oil
company. At full development, this project has the
potential to recover approximately 80 million net
barrels of reserves for Occidental, with net production
increasing to 20,000 BOE per day in 2010.
Occidentals production in Latin America was
adversely impacted by the seizure of the companys
operations in Ecuador. Occidental has filed a claim
against Ecuador with an agency of the World Bank
in Washington, DC seeking redress for the illegal
confiscation of our assets without compensation.
Our chemical segment core earnings of $901 million
were the highest level achieved in more than a decade
and exceeded our 2005 results by 16 percent.
Demand for our major products remained strong
across most sectors of the economy, including
construction, for much of the year. Sales volumes also
were sustained by the need to rebuild downstream
inventories following the hurricanes in late 2005 and
concerns about the potential for similar supply
disruptions in 2006. In addition, we continued to
benefit from the operation of assets acquired from
Vulcan Materials Company in mid-2005 and have
realized substantial synergies after the first full year
following the integration of these assets. Overall
chemical performance was driven by generally strong
demand that resulted in healthy margins for our
key products.
Consistent with our focus on continued improvement
in our assets and on health, environment and safety,
OxyChem began a project to convert its membrane
cell chlor-alkali plant at Taft, Louisiana to potassium
hydroxide. The project, when complete in early 2008,
will not only improve our cost position, but also
will eliminate the last of our mercury cell production
facilities in the United States.
Clean, efficient and reliable energy supplies are
critical to the growth and development of the global
economy. Occidental is committed to finding and
developing the oil and natural gas necessary to fuel
economic growth while upholding high standards of
social responsibility, protecting the environment and
safeguarding the health and safety of our employees,
our neighbors and our customers.
Safety is a top priority for Occidental. In 2006, we
maintained our excellent safety record. Our 2006
Employee Injury and Illness Incidence Rate (IIR
recordable injuries and illnesses per 100 worker
years) was 0.47, the same as our 2005 rate. We are
proud that we have been able to maintain consistently
low IIR rates while simultaneously meeting the
challenge of integrating two significant oil and gas
acquisitions into our operations. We are working
diligently to implement enhanced safety and
environmental standards into these new operations.
Incidents are thoroughly investigated and Occidentals
safety programs evolve to meet the changing needs
of our business as we grow. Occidental also maintains
a strong health, environment and safety risk management
and mitigation program, with results regularly
reviewed by senior management.
Building on our history of strong environmental
stewardship, Occidental Oil and Gas Corporation
joined the new U.S. EPA-sponsored Natural Gas
Star International program. Participation will provide
a recognized forum to document and present the
significant reductions in methane emissions that
have been achieved in Occidentals international
assets, primarily by capturing natural gas for sale.
Occidental Oil and Gas was recognized by U.S.
EPA as its International Partner of the Year for the
Natural Gas Star program.
We recognize that an important part of our effort to
find and develop new oil and natural gas resources
is evaluating how these activities impact local
communities. Maximizing the social, environmental
and safety benefits of our activities while minimizing
any negative impacts is our goal. In 2004, Occidentals
Board of Directors adopted a comprehensive
Human Rights Policy to restate and reinforce the
companys commitment to respect and promote
human rights. Since then, we have made excellent
progress in advancing the policy. More than 2,000
employees, including most of the employees from our
new operations in Argentina, Colombia, Libya and
Oman have participated in human rights training
courses. The training is required for all Occidental
managers, security employees and contractors
and new employees in our international operations.
The policy also requires social impact assessments
in new work areas in non-U.S. locations. So far,
assessments have been initiated in Oman, Libya,
Yemen, Colombia and Argentina. In Colombia,
Occidental collaborated with Ecopetrol and
International Alert, a London-based Human Rights
group, to introduce a new, comprehensive
risk matrix, which has proven to be helpful in
evaluating potential impacts of oil field operations
on local communities.
In addition, our oil and gas business units are
incorporating auditable social responsibility
standards into the companys Health, Environment
and Safety Management Systems to help ensure
our programs are fully integrated into our daily
business practices.
Occidental recognizes that responsible social,
environmental and safety practices are critical to
strong financial performance, and we strive to be
a socially responsible corporate citizen.
Looking Ahead
While we are pleased with our progress in 2006, we are focused
on the future to make sure that we grow stronger in 2007 and
beyond. World energy demand is growing rapidly. With very little
excess capacity, energy prices are likely to remain high and
volatile. These factors will continue to drive investment across
the energy sector. We plan to grow by building on our strengths
and participating in new investment opportunities. One of our
most important priorities is to add another core growth area to
keep our new project inventory full of high return projects.
Another priority is continued growth in our existing
core areas. We plan to aggressively pursue the
exploitation of properties we acquired in California
and Argentina in 2006 from Vintage and Plains
through a combination of primary drilling and EOR
projects. We expect to continue with our consolidation
initiatives in Texas and California. In addition,
we have a large inventory of EOR projects in the
Permian Basin of Texas and New Mexico where we
expect to offset moderate decline rates and keep
production stable.
We expect the Dolphin Project to become fully
operational around mid-year 2007, with gross
production ramping up to 2 billion cubic feet per
day by year-end.
We expect to drill between 14 and 16 exploration
wells in Libya this year, with 10 to 12 wells onshore
and the remaining four offshore. This focused effort
is the most extensive exploration initiative in
Occidentals history and we are excited by the
quality of the prospects.
Also, we are working on a number of new, high
potential projects in the Middle East and North
Africa. We expect to announce success in winning
additional projects this year.
We are confident we can keep our combined oil and
gas production growing at a sustainable annual rate
of 5 to 8 percent through 2010. Growth will come from
the companys Argentina assets, the giant Dolphin
gas project in Qatar and the United Arab Emirates
and the Mukhaizna EOR project in Oman and Libya.
This production outlook does not depend on new
exploration success, new EOR projects or new
acquisitions. However, we clearly hope that success
in these areas will further enhance our outlook for
sustained growth.
In January 2007, we sold our 50-percent interest in
the Vanyoganneft joint venture in western Siberia for
approximately $485 million. We recorded a gain of
approximately $400 million in the first quarter of
2007. This transaction reduces our risk profile and
provides additional cash to support our initiatives to
enhance stockholder value in 2007 and beyond.
Maintaining financial discipline and flexibility remains
a key part of our strategy. We will continue to look
for opportunities to use our free cash flow to
enhance stockholder value by maintaining a strong
balance sheet, expanding our share repurchase
program and investing for growth. We expect capital
expenditures to rise by approximately $400 million in
2007, from $3 billion in 2006 to $3.4 billion in 2007,
for an increase of 13 percent.
We will assess the potential of using the excess
cash from operations to fund new projects,
repurchase additional shares and make strategic
acquisitions. Our decisions will be driven by our
commitment to maximize value for our stockholders.
We believe our employees are second-to-none.
They are the most critical factor in the successful
execution of our growth strategy. As we strive
to achieve higher levels of performance, we are
broadening our recruiting efforts to attract the most
highly skilled and dedicated people.
We are committed to achieving superior financial
and operational results, while adhering to the highest
standards of ethical behavior, maintaining a safe and
healthy work place and serving as a responsible
steward of the environment. We are focused on the
fundamentals of the business, including optimizing
profits and free cash flow per BOE, keeping finding
and development costs competitive, growing oil and
gas reserves at a rate well in excess of production
and maintaining financial discipline. Focusing
on these fundamentals should keep our returns on
equity and capital employed in the top quartileand
ultimately generate top quartile total returns for our
stockholders. This formula, which has served us well
in the past, will continue to drive our performance in
the future.
Under the stewardship of our board of directors
and strong management team, we believe we have
established a solid foundation for future growth and
profitability. Our stockholders have high expectations,
and we are committed to meeting those expectations.
Dr. Ray R. Irani
Chairman, President and Chief Executive Officer