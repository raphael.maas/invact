Dear Shareholders,
We have been a public company since 1970, and throughout this time our focus has remained consistent: To drive
greater profitability for shareholders by maintaining an efficient operating structure to provide customers with a
compelling assortment of name brands and quality private brands at a modest markup. This unique blend of value,
combined with convenient locations, helps our customers balance their budgets and provide for their families.
While 2013 was more challenging than we originally expected, as our customers managed high unemployment levels
and higher taxes, our teams reacted quickly to manage a difficult operating environment. In fiscal 2013:
 Net sales increased 11.4% to a record $10.4 billion;
 Comparable stores sales increased 3%;
 Operating profit was a record $688 million, an increase of 3.6% over fiscal 2012; and
 Earnings per diluted share increased 7.0% to a record $3.83.
In addition, in 2013 we maintained our commitment to invest in the business for long-term sustainable growth, while
also returning excess capital to shareholders. In fiscal 2013, we:
 Opened 500 new stores, finishing the year with 7,916 stores;
 Renovated, relocated or expanded 830 stores;
 Opened our 11th distribution center in St. George, Utah;
 Repurchased 1.2 million shares for $75 million; and
 Increased our regular quarterly dividend by 23.8% and paid $108.3 million in dividends to shareholders.
Over the past two years, our customers have increasingly relied on Family Dollar for their basic needs, and weve
moved quickly to meet their demand. In 2012, we added about 1,000 new consumables items to our assortment, and in
2013 we launched a partnership with McLane Company, a highly successful supply chain services company, to
improve our refrigerated and frozen food supply chain. This multi-year, exclusive strategic distribution partnership
enables us to provide our customers with an improved assortment, guarantees weekly deliveries to our stores to keep
our coolers better in-stock, and provides an advantage in a very competitive retail environment.
When customers visit a Family Dollar store, our goal is to deliver a pleasant shopping experience. Through our
comprehensive renovation program, we are re-branding the chain and creating a warmer, more inviting shopping
environment, while also raising our customer service standards. At the end of fiscal 2013 about 60% of the chain
reflected a much improved shopping experience, and we expect to complete the chain over the next few years.
Our commitment to Family Dollar Team Members to be a compelling place to work is just as important as our
commitment to our customers. Building our bench of talent is critical to supporting our long-term growth objectives,
while ensuring that we provide our customers with a compelling shopping experience. We continue to invest in our
Team Members, and we are improving our training, on-boarding, and leadership development processes. As a result of
these efforts, in 2013, about 80% of our store managers were internal promotions.
There is no doubt 2013 was a challenging year against the backdrop of a slow economic recovery, but our teams stayed
focused on delivering great values for our customers and driving returns to shareholders. We accomplished a lot over
the past year, and we are well-positioned to drive even stronger results in 2014. While the environment remains
challenging and uncertain for our core customer, we are maintaining our unwavering commitment to meet their needs.
We will deliver a great assortment, at a great price, in a convenient shopping trip. This has been our goal since opening
our first Family Dollar in 1959, and I believe this positions us well for continued success in 2014 and beyond.
Sincerely,
Howard R. Levine
Family Dollar Chairman and CEO