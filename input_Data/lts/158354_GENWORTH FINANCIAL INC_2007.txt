Message from the Chairman

I'm pleased you've been introduced to Tralee
Neville and Jason Macdonald, Eeva Kaartinen,
Dr. and Mrs. Robert Poole, Al Gibbons, Tyone
and Kimora Savage, Dr. William Clovis and Mark
Sheehan in this Annual Report. Their stories are
unique, but they share one thing  they inspire us.

                                               and strong revenue growth enabled us to
   at's why our , Genworth employees
                                               outperform competitors substantially,
work hard with our distribution partners
                                               achieving  million in operating
to help people like these, and more than
                                               earnings and a full year mortgage insurance
 million others, achieve financial security
                                               loss ratio that stood at less than half of
at key stages of life.
                                               what the rest of the industry on average
                                               experienced in . But there are things
You can read detailed information about
                                               we would have done differently in selected
how that work translates into business
                                               geographic and product exposures and we
results in the -K report that follows.
                                               will learn from these.
Here, I'd like to share my thoughts on our
 performance, as well as our vision,
                                               Looking at our other two segments, which
our strategy and our future.
                                               represent the majority of our earnings, we
                                               delivered strong core growth in several
OUR  PERFORMANCE
                                               key product lines in , including our
During a year marked by turmoil in the
                                               international lines  payment protection and
U.S. housing industry and disruption in
                                               mortgage insurance  wealth management,
the global financial markets, we delivered
                                               retirement income and universal life
 net operating income of . billion
                                               insurance. is growth reflects the diversity
and net operating earnings per diluted
                                               and strength of our business portfolio. We
share of .  up over the prior year but
                                               have established positions across multiple
below our original target. While our overall
                                               growth markets, with good momentum,
performance across our businesses was
                                               and we continue to target the  to 
positive, we were disappointed by the
                                               percent operating return on equity goal we
year-over-year earnings decline in our U.S.
                                               established for the  to  timeframe.
Mortgage Insurance segment. In this
segment, our disciplined risk management




                                               
                                               Being distributor preferred  collaborating
OUR VISION AND STRATEGY
                                               with our distribution partners around
Moving forward, we will maintain our
                                               the world to offer innovative products,
vision of being a leader in providing
                                               specialized services, education and
solutions that help people achieve financial
                                               technology that provide them with value at
security at key stages of life. We want
                                               every part of the business relationship. We
to help people achieve homeownership
                                               share our global expertise and local market
through the use of mortgage insurance;
                                               knowledge to help our distributors grow.
create life security by providing life
                                               To advance understanding of key issues
insurance, payment protection coverage
                                               affecting our markets and customers, we
and wellness programs; build savings and
                                               work in partnership with thought leaders
wealth through our wealth management
                                               in business, government and academia.
offerings and financial advisory services;
                                               Our studies on mortgage trends in
and establish retirement security through
                                               Canada, Australia and other countries, as
retirement income products and long
                                               well as forums such as our Retirement and
term care insurance.
                                               Long Term Care symposia, bring together
                                               opinion leaders to share ideas and develop
To fulfill this vision, we outlined four
                                               common solutions in response to some of
strategic goals last year and have made
                                               the most important issues we face today.
good progress toward them:

                                               Leveraging capital markets leadership 
Becoming consumer wanted  working to
                                               using a combination of traditional
meet consumers' needs today while helping
                                               insurance disciplines and sound capital
them protect against future risk. We're
                                               markets-based techniques to enhance
developing a deeper understanding of
                                               risk management, capital efficiency
consumers' needs through research and
                                               and support new business models. For
market segmentation, while also creating
                                               example, we issued the industry's first
innovative financial security solutions.
                                               securitization that efficiently financed
For example, our Cornerstone Advantage
                                               statutory term life insurance reserves.
product helps more people secure their
                                               We also were the first to do the same
future independence by making long term
                                               for universal life insurance reserves and
care insurance more simple and affordable.
                                               have continued to innovate in this area
   is, coupled with our wellness and care
                                               during the past four years.
coordination services, helps create real
solutions for people's long term care needs.
                                               Being people driven  fostering an
Ultimately, we want to provide offerings
                                               environment that welcomes different
that consumers seek out and want 
                                               backgrounds, capitalizes on diverse
challenging the notion that insurance must
                                               talents, invests in the capabilities of
be sold and not bought  and enabling our
                                               our people and engages them in
distribution partners to meet the needs of
                                               improving our business every day.
their customers.



                                    
Our Genworth Development Center offers             are demonstrating a preference for the
thousands of online and in-person courses          safety and security of fixed-rate mortgages
to support employees' growth. We know              with mortgage insurance and as loan
our people drive our success so we want            standards become more conservative. In
to support their success.                          addition, our existing business portfolio
                                                   will benefit from substantial reinsurance
                                                   protection. As we move ahead, we'll
Our progress toward these strategic
                                                   continue the important work we began in
initiatives positions us well for the future.
                                                    to help ensure that the new business
In the United States, our Retirement
                                                   we originate performs well. And, we'll
and Protection segment now represents
                                                   keep managing risk through key actions
half of our total operating earnings and
                                                   including tightening underwriting
is deepening its broad product and
                                                   standards, expanding geographic
service capabilities while sharpening its
                                                   constraints and increasing prices.
relationships with key distributors. Our
International segment reached  percent          . Grow our Wealth Management and
of total operating earnings and remains           Retirement Income businesses. We'll
focused on payment protection and                 continue to focus on product innovation
mortgage insurance while selectively              and enhanced service offerings with
exploring new opportunities. And our U.S.         risk management disciplines. We'll
Mortgage Insurance segment is operating           also selectively pursue acquisitions
through a very difficult U.S. residential         as we capitalize on emerging growth
real estate market while positioning itself       opportunities in these areas.
to take advantage of a shifting market and
                                                . Maintain the responsible growth of our
create a better profile for the future.
                                                  International platforms. We'll apply
                                                  our product expertise, consumer insights
OUR FUTURE
                                                  and local market knowledge to deepen
We've made good progress, but we have
                                                  mortgage insurance and payment
more work to do. We're confident we
                                                  protection penetration in established
have the right vision and strategy, a solid
                                                  markets. We'll selectively approach
financial foundation and sound risk
                                                  new markets and assess opportunities
disciplines to help us take on the challenges
                                                  to extend our retirement income and
in today's markets, while seizing
                                                  wealth management solutions to
opportunities. As we execute our strategy,
                                                  targeted countries. For each, we will
we'll focus on five key areas in .
                                                  put a priority on growing prudently,
We'll continue to:
                                                  with disciplined risk management and
                                                  strong controllership.
     Navigate the storm in U.S. Mortgage
1.
                                                . Strengthen our Long Term Care and Life
     Insurance. We see the potential for
                                                  Insurance businesses. In Long Term Care
     significant profitable revenue growth
                                                  Insurance, we will grow our new block
     in the segment, as people increasingly



                                                
                                                   " ink it possible"SM is the spirit of
   of business profitably through additional
   products and distribution expansion              Genworth. We believe great things can
   leveraging our independent distributors,         happen when you think differently, and
   career sales operations and partnership          ask "what if?" and "why not?" We want
   with AARP. At the same time, work is             to turn opportunities into reality for
   underway to improve the performance              people like those you've read about in
   of our old block of business. Within our         this report. When I think about the
   Life Insurance business, we will expand          future, I'm optimistic and excited about
   our position in universal life, while            all that's possible for Genworth  the
   concentrating our term life business on          consumers we serve, our business partners,
   the most attractive consumer segments            our shareholders, our employees and
   and distribution channels.                       our communities. ank you for being
                                                    a part of our journey.
. Focus on strong capital management. We'll
  emphasize extracting underperforming
  capital and redeploying it to higher return
  areas or returning it to our shareholders.
  And we'll maintain our cost and
                                                    Sincerely,
  productivity disciplines.

   e foundation of our efforts is our clear
vision and our shared commitment with
our business partners to bring financial
security to more people.
                                                    Michael D. Fraizer
THINK IT POSSIBLE S M
                                                    Chairman, President and
Every day, our employees are driven by              Chief Executive Officer
this vision, demonstrating our values of            March 
ingenuity, clarity, performance, heart and
initiative. And their passion to make a
difference inspires us to get involved with
the communities in which we live and
work  from volunteering with local
children's programs to finding ways to
improve the environment. rough their
ideas and inspiration, our employees are
helping people around the world achieve
their hopes and dreams, and bringing real
value to our business partners. Simply put,
our employees "think it possible"  and by
doing this, make possibilities become realities.



                                    
