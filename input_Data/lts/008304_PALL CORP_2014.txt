To Our Shareholders:
Pall reported very solid results for fiscal 2014. For the full year, we delivered on both
revenue and EPS despite foreign exchange and end-market headwinds.

In the midst of significant macroeconomic uncertainty,
we delivered fiscal 2014 results based on
solid sales and operations execution. Beyond the
results for the year, we made significant progress
against all of our multi-year objectives. We also
continued to invest in innovation that will translate
to growth for the future.
We generated reasonable top line growth given
the industrial end market environment, strong
incremental margins and solid free cash flow. We
are very proud of our team for this accomplishment.
Innovation has been reinvigorated at Pall with
increasing new product development, patent
applications and invention disclosures.
Weve also made significant progress in operations.
A culture of continuous improvement
has taken hold from the factory floor to customer
service, sales and beyond. Weve also made
improvements in strategic sourcing, supply chain,
product engineering, and order and demand
management.
SG&A was down 140 basis points in fiscal 2014
and is down nearly 250 basis points since fiscal
2012. Were positioned to continue to generate
solid year-over-year performance based on reasonable
growth assumptions but coupled with our
continued concentration on working more efficiently
and leveraging our global footprint more
effectively.
Our capital deployment priorities have been very
clearinternal investment for organic growth and
M&A are the top priorities.
Our deployment of cash has significantly increased
over the past several years. In fact, fiscal 2014
represents the highest annual M&A spend in the
past decade.
In fiscal 2014 we continued to grow and enhance
our portfolio though a series of strategic acquisitions
including Medistad, SoloHill, ATMI Life
Sciences and FSI. We welcome all of our new
team members to the Pall family.
While we are pleased with what we have accomplished,
we still have significant room to improve in
meeting the high expectations of our customers
and our shareholders. Given the progress in fiscal
2014, and a stronger team than ever before, we
are very optimistic about 2015 and beyond.

Lawrence D. Kingsley
Chairman &
Chief Executive Officer
Akhil Johri
Chief Financial Officer
