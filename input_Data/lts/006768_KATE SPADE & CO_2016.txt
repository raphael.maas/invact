Dear Fellow Shareholders:
Three years ago, we established Kate Spade & Company as a standalone business, and at the end of 2016,
we reached nearly $1.4 billion in net sales. We delivered solid financial results and gained additional market
share last year, as well as made significant progress on our growth initiatives, despite the backdrop of a
consistently challenging retail environment.
As a result of our associates hard work, we have become a powerful, global, multi-channel lifestyle brand,
with a more than $2 billion retail footprint as we continue on our path to becoming a $4 billion brand at
retail.
Continuing to deliver industry-leading results
In 2016, we continued our top-line expansion, with net sales of $1.38 billion, a 14 percent increase versus
2015 on a comparable basis, and comparable direct-to-consumer sales growth of 9 percent, which continued
to be industry-leading. In addition, we generated full-year adjusted EBITDA of $261 million, a 29 percent
increase compared to 2015, representing an adjusted EBITDA margin of 18.9 percent, a 220 basis point
increase year-over-year.
We remained intently focused on building our differentiated business model. Our six points of differentiation
 our lifestyle brand, partnered approach to expansion, channel-agnostic strategy, controlled points of
distribution, focus on direct-to-consumer sales and protection of our quality of sale  remain core to our
success.
Focusing on thoughtful category expansion, while increasing brand recognition and engagement in
our key markets around the world
We continued to make progress along our two axes of growth  product category and geographic expansion.
In 2016, we introduced several new categories to our casual ready-to-wear classifications, including the launch
of our Broome Street label, our athleisure collaboration, outerwear and sleepwear, and strengthened our
handbag portfolio in both our full-price and off-price channels. We also expanded our watch assortment to
include wearable products, licensed our existing fragrance business, and supported our home category with
a successful pop-up shop in New York City.
At the same time, we advanced along our geographic expansion axis of growth, strategically expanding our
global footprint throughout the U.S., Europe, Greater China and the rest of Asia, and opening 52 net new
owned and partner-operated stores during 2016. In the U.K., we opened our Regent Street location, which
helps strengthen brand awareness and build brand equity, particularly with European consumers.
Looking ahead
In February 2017, we announced that our management team, together with the Board of Directors, is
conducting a process to explore and evaluate strategic alternatives to further enhance shareholder value.
We remain committed to acting in the best interest of the Company and our stakeholders.
Thank you for your continued support.
Sincerely,
Craig A. Leavitt