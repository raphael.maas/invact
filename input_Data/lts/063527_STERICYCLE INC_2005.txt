2005 was another solid year for Stericycle. We continued to set new
financial records by building our core business, growing across
new geographies, expanding our services and driving for operational
efficiencies while at the same time significantly strengthening our
balance sheet. Stericycle is positioned for continued revenue and
profit growth in 2006 and beyond.
Revenues in 2005 grew to $609.5 million, an 18.1% increase over
2004. Gross margins for 2005 were 44.0% versus 44.2% in 2004.
Operating income rose 15.6% to $168.4 million compared to $145.7
million in 2004. Operating margins for the year were 27.6% versus
28.2% in 2004. Under generally accepted accounting principles
(GAAP), after-tax net income decreased by 14.1% to $67.2 million
and diluted earnings per share decreased 12.4% to $1.48 per share.
Non-GAAP net income for 2005 was $94.0 million, a 15.2%
increase over non-GAAP net income of $81.6 million for 2004.
Non-GAAP diluted earnings per share for 2005 were $2.07 per
share, a 16.9% increase over non-GAAP diluted earnings per share
for 2004 of $1.77 per share. Excluded from our non-GAAP numbers
in 2005 are the costs related to the preliminary settlement of the
3CI class action litigation, the write-down of our note receivable
with our former South African joint venture, licensing legal
settlement, loan refinancing expenses and fixed asset impairment
expenses. Excluded from our non-GAAP numbers in 2004 are
expenses related to loan refinancing and fixed asset impairments.
ACCOMPLISHMENTS IN 2005
We continued to strengthen Stericycles industry leadership position as
the only national provider of integrated medical waste and healthcare
compliance services across North America. We successfully increased
the penetration of our new Steri-SafeSM OSHA compliance program
for small quantity generators throughout the United States, gained
further acceptance of our new Bio Systems sharps management
program in new domestic geographies, and continued to expand our
operations in both the United Kingdom and Mexico. In addition,
we significantly expanded our presence in the pharmaceutical services
business.
Domestic Growth: Our small quantity generator business revenues
grew approximately 9% as a result of our focused direct selling efforts
and our innovative Steri-SafeSM OSHA compliance program. We
now have approximately 95,000 Steri-SafeSM subscribers, up from
approximately 87,000 a year ago. We successfully continued the
roll out of our Bio Systems sharps management service into new
geographies, adding more than 250 new accounts in 2005.
Domestically, we integrated nine acquisitions of medical waste
businesses into our existing collection route, transfer station and
treatment plant infrastructure. We also strengthened our position
in providing reverse logistics and compliance services to the
pharmaceutical industry supply chain by both organic growth and
acquisitions. In 2005, we acquired two leading trade return and
recall service providers to pharmaceutical manufacturers, Universal
Solutions International, Inc. and NNC Group, LLC, and are well
positioned for future growth.
International Growth: We strengthened our position internationally
through the growth of our businesses in the United
Kingdom, Mexico and Canada.
Cash Flow Generation: We continued to generate strong free
cash flow from operations, which was used to fund growth and
improve our balance sheet. During 2005, we invested $26.3 million
of the $94.3 million in cash generated from operations into our
infrastructure. We expanded our non-incineration treatment network
and supported the growth of Bio Systems. In addition, we used
$139.7 million for acquisitions and expanded our revolving credit
facility to $550.0 million. Finally, we repurchased $60.7 million in
stock on the open market.
PRIORITIES FOR 2006
By building on Stericycles industry leadership position in 2005, we
are confident that we have established the operating platform needed
to drive future growth and explore new frontiers for our business.
We have the following priorities for 2006:
Domestic Growth: Our focus will be on our Steri-SafeSM and
Bio Systems service offerings. Our marketing efforts to small quantity
generators will concentrate on selling our Steri-SafeSM OSHA compliance
services and infectious waste management services. Our primary
focus in marketing to large quantity generators will be
extending the momentum of the Bio Systems sharps management
program. We will focus on creating shareholder value from the
acquired pharmaceutical services businesses by creating best-in-class
customer service, organic growth and meeting customer needs
with additional service offerings.
International Growth: We will remain focused on pursuing
attractive international market opportunities directed to providing
value to our shareholders over the course of the next several years.
Profit Growth: We are committed to continuing our track
record of improving our operating margins. We will seek to further
improve our collection route densities, reduce our long haul transportation
costs, reduce our plant operating costs and improve
efficiency in the pharmaceutical services operations. Our culture of
continuous improvement encourages the sharing of best practices
and productivity improvement ideas across our entire organization.
Service Innovation and Environmental Leadership:
During 2006, we will continue fulfilling our twin commitments of
being responsive to our customers needs and being an environmental
leader by offering services suited for both our large and small
customers. Our innovative Steri-SafeSM OSHA compliance services
and infection control and compliance products continue to help
customers enjoy a safer, more compliant workplace in a cost effective
manner. Other innovative outsourcing programs such as our Bio
Systems sharps management service offers significant environmental
benefits by reducing waste volume and conserving valuable natural
resources. As more and more healthcare providers switched during
2005 to our Bio Systems service, thousands of pounds of plastic
and cardboard that had been discarded was instead eliminated from
the waste stream, providing a significant environmental benefit.
  
We are very excited and confident about our future. Stericycle is a
clear leader in providing medical waste management, OSHA compliance
services and pharmaceutical services. We will focus on the
many growth opportunities our industry leadership position affords
us and will continue to refine the efficiency of our operations,
while maintaining our strong focus on safety and regulatory compliance.
We thank you for your support.

Jack W. Schuler
Chairman

Mark C. Miller
President and CEO