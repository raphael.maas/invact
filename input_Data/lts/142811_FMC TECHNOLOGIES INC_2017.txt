A MESSAGE TO
OUR SHAREHOLDERS

FMC�s crop protection transaction with
DuPont marked an important milestone in
our journey. We completed one of the most
transformative acquisitions in our company�s
history on November 1, 2017, when FMC
acquired a significant part of the DuPont
Crop Protection business and also sold FMC
Health and Nutrition to DuPont. Earlier in
the year, we announced our intention to
unlock the full potential of FMC Lithium by
separating it into a publicly traded company
in 2018.
FMC AGRICULTURAL SOLUTIONS
On March 31, 2017, FMC agreed to acquire a significant
portion of DuPont�s Crop Protection business, which was
being divested to comply with European Commission
requirements for DuPont to merge with The Dow Chemical
Company. When our transaction closed, FMC acquired
the majority of DuPont�s industry-leading insecticides and
herbicides, patented technologies, exceptional discovery
research capabilities and a global manufacturing network.
The crop protection industry has undergone changes during
the last few years that have led to a series of mega mergers
and consolidations. When these mergers are completed,
the agricultural industry will be served by two types of crop
protection companies�technology-focused innovators
and generics. FMC Agricultural Solutions has been a
highly successful business, delivering strong earnings and
operating margins. The DuPont transaction elevates our
company to a tier-one agchem leader with scale, global
reach and a broader portfolio of differentiated products.
The acquisition included:
� 12 active ingredients (AIs), including some of the
industry�s most successful AIs: Rynaxypyr� insect control,
Cyazypyr� insect control, and Indoxacarb insecticides
� 14 manufacturing sites
� 15 R&D sites around the world, including the Stine
Research Center in Delaware
� 15 active ingredients currently under development
� 1.8 million synthetic compounds
We now have a larger global footprint and a well-balanced
regional presence. In fact, we are almost equally balanced
across North America, Latin America, Europe/Middle East/
Africa, and Asia Pacific, ensuring business performance that
is less susceptible to market swings in any given region.
Our ag team is excited about the opportunities ahead.
FMC Agricultural Solutions is now one of the fastestgrowing crop protection companies in the world,
well-positioned to deliver strong financial performance.
FMC LITHIUM
The global lithium industry is growing at an unprecedented
rate, driven mostly by the ongoing adoption of hybrid and
electric vehicles (EVs) powered by lithium ion batteries.
The EV market is predicted to triple in the next three
years, becoming the largest consumer of lithium materials.
We made several strategic decisions during the last few
years to focus FMC Lithium on downstream, higher-value
products. We convert most of our lithium carbonate and
chloride production into high-purity materials, including
lithium hydroxide used in EV batteries, and butyllithium
and lithium metals for specialty applications. 

2017 was a year of significant growth for this business.
We expanded capacity in multiple locations, including a
new lithium hydroxide plant in China that is producing
9,000 metric tons per year to meet accelerating demand
for FMC�s products. We announced additional production
investments that will add another 12,000 metric tons
per year of lithium hydroxide capacity. To feed these
downstream products, we announced plans to more than
double lithium carbonate production at our Argentina site,
from 18,000 metric tons per year to more than 40,000
metric tons per year by 2022, with incremental production
starting as early as 2020. In addition, debottlenecking
projects at our major facilities are contributing to shortterm capacity increases.
In March 2017, we announced our intention to separate
FMC Lithium into a publicly traded company by the end of
2018. As an independent company, FMC Lithium will have a
focused investor base and strong balance sheet, ensuring it
has the financial capacity to pursue its growth plans and be
a leading force in this critical industry.
We expect 2018 will be another year of strong financial
performance for our Lithium business, positioning it for a
successful separation in the second half of the year.
FMC HEALTH AND NUTRITION
We sold our Health and Nutrition business to DuPont in
November as part of the crop protection transaction. Health
and Nutrition, which had been in the FMC portfolio for
more than four decades, delivered strong performance over
the years. We believe it will thrive within the larger DuPont
Nutrition & Health business, giving it additional resources
to grow in new and different ways. We wish all our former
Health and Nutrition colleagues the best of luck.
GROWING SAFELY AND SUSTAINABLY
FMC�s goal is zero injuries, and in 2017 we took another
step closer with an injury rate of 0.18. This represents an 18
percent year-over-year reduction, and a nearly 75 percent
reduction compared to our injury rate in 2010. In fact, our
company attained the lowest full-year injury rate since FMC
began keeping incident rate records.
Our employees are committed to operating safely and
responsibly every day, and their efforts have had a positive
impact across the company. In 2017, the American
Chemistry Council (ACC) recognized FMC�s achievements
in environment, health, safety and sustainability with the
�Responsible Care� Company of the Year� award. It is our
third consecutive honor from ACC, following an �Initiative of
the Year� award in 2016 for a new approach to manage
process safety risks, and recognition in 2015 for TH!NK.
SAFE.�, our global safety awareness and education program.

Sustainability is a fundamental element of how we operate,
innovate and serve our customers. We have made meaningful
progress toward our long-range sustainability goals that
address important global challenges:
� Dedicated 82 percent of our R&D investment to develop
sustainably advantaged products.
Goal: 80% by 2020
� Reduced three of four operations metrics, including
energy, greenhouse gas (GHG) emissions and waste
disposal intensities, down from the base year of 2013 by
14 percent, 21 percent and 16 percent, respectively. All
of our facilities are working to lower their intensities for
water, energy, GHG emissions and waste disposal.
Goal: 15 percent intensity reductions for energy,
GHG and waste disposal, and 20 percent intensity
reduction for water, by 2025
� Achieved 97 on our Community Engagement Index,
which measures key interactions between manufacturing
sites and their local communities in the areas of safety,
operations transparency, leadership and partnership.
Goal: achieve 100 on the index by 2020

Our sustainability goals will be updated in 2018 to reflect
the acquisition of DuPont Crop Protection products and
production sites.
In October 2017, we launched an external Sustainability
Advisory Council to help guide us on issues, trends and best
practices that advance our strategy. The council includes
representatives from academia, NGOs, industry and the
investor community.
Finally, we strive for a workplace that is progressive, inclusive
and respectful. FMC Diversity and Inclusion has made
great strides in awareness programs, education initiatives,
networking events, new external affiliations and launching
several new affinity groups. These include SPECTRUM LGBTQ
affinity group, New Generations for young professionals,
The Bridge multicultural affinity group and the Women in
Engineering affinity group.
THE NEW FMC
In the second half of 2018, we will create two leading
companies in their respective markets. FMC is the fifth largest
crop protection company in the world, and our Lithium business
will become the only publicly listed pure-play lithium producer
with a full product line of primary and specialty products.
Each company will have the power to grow and deliver
exceptional performance.
Our 7,000 employees are passionate about winning,
committed to our transformation and focused on serving
customers. All of us look forward to another exciting year at
the new FMC.
Pierre R. Brondeau
President, Chief Executive Officer and Chairman of the Board
FMC Corporation