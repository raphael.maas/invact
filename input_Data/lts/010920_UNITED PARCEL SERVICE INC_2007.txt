The year 2007 was particularly memorable for UPS. We celebrated 100 years of service,
negotiated an historic labor contract, and developed a new financial policy 
announced in January 2008  aimed at enhancing shareowner value.

On August 28, we marked the centennial anniversary
of our founding. We celebrated our 100th year of service
in our birthplace, Seattle, Washington, by recognizing
the people who made it possible  our founders, our
valued customers, and current and retired employees.
Shortly thereafter, we reached a labor agreement with
the International Brotherhood of Teamsters, covering
more than 240,000 UPS employees, 10 months ahead
of the July 2008 expiration of our current contract.
The new contract extends through July 2013, ensuring
service continuity for our customers.
The labor agreement satisfactorily resolved the issues
we identified as significant when negotiations began.
It does so at a manageable cost to UPS, while providing
us the flexibilities we need to remain competitive in the
marketplace. In short, we believe this is a good contract
for our employees, our customers and our shareowners.
UPSs new financial policy will enable the company to
migrate to a more efficient capital structure. After
study ing our options for some time, we determined
we could significantly increase the debt component
of the balance sheet and enhance shareowner value
by reducing the companys cost of capital.
We intend to manage our balance sheet to a target ratio
within a range of 50 to 60 percent of funds from operations
to total debt. At the end of 2007, we were at
75 percent. The new policy permits us to increase
investments in the business, undertake larger share
repurchases and pursue growth opportunities.
In line with this new policy, the Board of Directors
increased stock repurchase authorization to $10 billion.
We intend to complete the share repurchases by the end
of 2009. Timing will depend on market conditions.
UPS has a long-standing commitment to a very strong
balance sheet and that will not change. We are putting
our balance sheet strength to work to deploy capital
more efficiently for the benefit of our shareowners,
while maintaining a great deal of financial flexibility.
INVESTING IN THE FUTURE
In addition to these noteworthy events, in 2007 UPS:
 Delivered record revenue, operating profit and diluted
earnings per share, when adjusted for the impact of
several one-time charges
 Maintained our industry-leading adjusted operating
margin
 Invested in the companys future through new services
and technology, infrastructure expansion and aircraft
 Returned significant amounts of cash to our shareowners:
$1.7 billion in dividends and $2.6 billion
to repurchase 35.9 million shares of stock
GLOBAL SMALL PACKAGE NETWORK
Worldwide, UPS delivered a record number of packages 
3.97 billion. In the United States, slower GDP growth
in 2007 than in recent years restrained the pace of
volume growth. Even so, we experienced the highest
volume in history, and pricing remained firm. Revenue
increased 1.7 percent to $31.0 billion. However, the
U.S. Package segment reported a $1.5 billion loss for
the year, as a result of a $6.1 billion pre-tax payment
as part of the new labor agreement to withdraw
approx imately 45,000 UPS Teamster employees from
their pension plan and set up a new, jointly trusteed
plan. Without this payment and other one-time charges,
operating profit for the U.S. domestic operation would
have been $4.8 billion.
In a slow-growth economic environment, cost control
is essential, and UPS people did an excellent job in this
area. Workers compensation expense was down significantly,
a consequence of reducing work-related injuries
by almost 50 percent since 2002 to 1.6 per 100,000
hours, a win-win for our employees well-being and
for the bottom line.
Internationally, UPS experienced 10.4 percent export
volume growth, more than twice the rate of overall
economic expansion. Our international operations
reported the highest revenue and operating profit ever.
Revenue was up 13.1 percent to $10.3 billion with
operating profit improving 7.1 percent to $1.8 billion.
Operating margin was very strong at 17.8 percent.
In 2007, we laid the groundwork
for future growth with investments
in our worldwide air operations.
We began construction on
our Shanghai air hub, scheduled
for completion in 2008, the first
in China to be operated by a U.S.
carrier. It will link cities in China
to UPSs international network,
and provide direct service to the
Americas, Europe and other countries in Asia.
Additionally, we put in place another around-the-world
flight to connect the fast-growing Asian and European
trade lanes to each other and to the United States.
Lastly, UPS received the authority to operate six daily
flights between the United States and Nagoya, Japan,
in addition to our daily flights to Tokyo and Osaka.
SUPPLY CHAIN AND FREIGHT TURNAROUND
UPSs supply chain and freight capabilities are integral
to our vision of synchronized, transportation-focused
solutions that help move our customers forward. To
that end, we were very encouraged by the significant
performance improvement of our Supply Chain and
Freight segment, which posted a 5.3 percent revenue
gain and a $276 million increase in operating profit.
The Supply Chain operations, namely Forwarding and
Logistics, capitalized on initiatives that enhanced revenue
management and customer service, while reducing
operating cost. In the Forwarding business, international
air freight forwarding made notable progress.
Ocean freight forwarding will be a priority in 2008.
Logistics streamlined its operations and posted signifi-
cant profit gains on moderate revenue growth. In 2008,
its focus will be on developing solutions for the healthcare
and technology sectors.
UPS Freight experienced a very good year in 2007 with
revenue, profit and shipment growth. This was especially
noteworthy given the U.S. trucking industrys highly
competitive operating environment. The UPS brand
strengths of reliability and technology associated with
our small package business are attracting customers to
our freight services. We intend to continue leveraging
our small package customer base through cross-selling
the full complement of UPS services.
TRENDS AND GROWTH
OPPORTUNITIES
UPS is a major player in world
commerce; we enable it and we
benefit from it. Long-term industry
fundamentals are favorable for our
business and, in fact, play to UPS
strengths. These include expanding
global trade, direct-to-consumer
shipping, and outsourcing supply chain management.
Increasingly, supply chain strategy means that research
and development, production, component-making, final
assembly, marketing and distribution are located where
the best resources are available, and where the most
value can be added to the end product. Todays supply
chain leaders are focused on time-in-trade  ensuring
that each component of the value chain arrives at precisely
the right location, at the right time, and at the most
competitive cost  in short, synchronized commerce.
In todays global environment, our market opportunities
are significant. UPS participates in a $225 billion
global arena that includes small package shipping,
domestic less-than-truckload freight and global air
freight. We have about a 20 percent share of this highly
competitive space, in which there is no leading player.
Additionally, the ocean freight forwarding sector in
which we compete is a $60 billion industry, and the
outsourced logistics market is estimated at almost
$190 billion. Therefore, growth opportunities abound.
WHAT WE SEE FOR 2008
Economic uncertainty in the U.S. likely will make 2008
more challenging than 2007. However, UPS has a long
history of growth in many different economic environments.
Strength in the International Package and the
Supply Chain and Freight segments should mitigate what
we expect will be only modest gains in the U.S. Package
segment. We will be vigilant about operating efficiently,
while keeping a sharp eye on execution. For the year,
we anticipate achieving earnings per share in the range
of $4.30 to $4.50.
At UPS, we are committed to being a company that
people want to do business with, want to work for,
and want to invest in. These people  our customers,
our employees and our shareowners  have a tremendous
opportunity to move forward with us as we capitalize
on global opportunities.
FAREWELL TO A VISIONARY
At the close of 2007, Mike Eskew, our Chairman and
Chief Executive Officer for six years, retired. In Mikes
35-year career with UPS, he made significant contributions
to the company. He helped set up UPSs foray
into international operations and was instrumental
in one of the fastest startups of a large airline in history.
Mike also led the companys expansion into services
complementary to small package delivery  supply chain
management, international freight forwarding and lessthan-
truckload freight shipment in the United States.
Under Mikes leadership UPS realized substantial growth,
and we appreciate his insight and dedication to the company.
He leaves a strong UPS, and I am both honored
and humbled to be his successor.

D. Scott Davis
Chairman and
Chief Executive Officer