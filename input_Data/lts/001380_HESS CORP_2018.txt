Our company enters 2019 with significant momentum

investment phase in 2019 to a free cash flow generation

following continued strong strategic execution and

phase beginning in 2020 with the startup of the Liza

operational performance in 2018. As a result, we are

Phase 1 development offshore Guyana, which is

uniquely positioned to deliver increasing financial returns,

expected to produce up to 120,000 gross barrels of

visible and low risk production growth and accelerating

oil per day utilizing the Liza Destiny floating production,

free cash flow well into the next decade.

storage and offloading (FPSO) vessel. This important
milestone will be followed by our Bakken asset growing

We have built a balanced, focused portfolio with
Guyana and the Bakken as our growth engines and
the deepwater Gulf of Mexico and the Gulf of Thailand
as our cash engines. Our portfolio is positioned to
deliver approximately 20 percent compound annual
cash flow growth and more than 10 percent
compound annual production growth through 2025
at a $65 per barrel Brent oil price, with a portfolio
breakeven of less than $40 per barrel Brent by 2025.
Our strategic priorities are clear. First, we will invest only
in high return, low cost opportunities. In December 2018,
we announced a 2019 Exploration and Production (E&P)
capital and exploratory budget of $2.9 billion. We expect
to maintain capital expenditures of approximately $3 billion
per year through 2025, with plans to allocate about
75 percent to developments in Guyana and the Bakken �
two of the best investments in the oil and gas industry.

to 200,000 net barrels of oil equivalent per day by 2021,
and then startup of the Liza Phase 2 development
offshore Guyana by mid 2022, which will use a second
FPSO designed to produce up to 220,000 gross barrels
of oil per day. A third FPSO is expected to begin
production at the Payara development as early as 2023,
with an additional FPSO planned in Guyana in 2024 and
2025. As our portfolio generates increasing free cash
flow, we will prioritize the return of capital to shareholders
through dividends and opportunistic share repurchases.
A key driver of our strategy is Guyana, where Hess
has a 30 percent interest in the Stabroek Block and
ExxonMobil is the operator. In February 2019, we
announced discoveries at the Tilapia-1 and Haimara-1
wells offshore Guyana, bringing the total number of
discoveries on the Stabroek Block to 12. In March
2019, the estimate of gross discovered recoverable

Second, we will continue to ensure that we have the

resources for the block was increased to more than

financial capacity to fund our world class investment

5.5 billion barrels of oil equivalent, with multibillion

opportunities and maintain an investment grade credit

barrels of additional exploration potential remaining on

rating. We entered 2019 with $2.7 billion of cash on the

the block. The growing resource base on the Stabroek

balance sheet, 95,000 barrels of oil per day hedged in

Block underpins the potential for at least five FPSO

2019 with $60 per barrel West Texas Intermediate (WTI)

vessels producing more than 750,000 gross barrels of

put options, and the spending flexibility to reduce our

oil per day by 2025.

capital program by up to $1 billion should oil prices
move lower in future years.
Third, we will remain focused on growing free cash flow

In the Bakken, Hess is a top tier operator with a 15 year
inventory of high return drilling locations. Our transition
to plug and perf completions from our previous 60 stage

in a disciplined and reliable manner. Our company is

sliding sleeve design is expected to increase the net

at an exciting inflection point, transitioning from an

present value of the asset by approximately $1 billion.
Bakken net production is expected to grow approximately

20 percent per year to 200,000 barrels of oil equivalent

in overseeing Hess� sustainability practices, working

per day by 2021, generating approximately $1 billion of

alongside senior management to evaluate various

annual free cash flow post 2020 at $60 per barrel WTI.

sustainability risks and global scenarios in making
strategic decisions. We are committed to transparency,

Turning to our 2018 financial results, our adjusted net
loss was $176 million, compared with an adjusted net
loss of $1.4 billion in 2017. Cash flow from operations,

and our strategy and reporting are closely aligned with
the recommendations of the Task Force on ClimateRelated Financial Disclosures.

before changes in working capital, was $2.1 billion, up
from $1.7 billion the prior year. In 2018, we delivered

We have tested the robustness of Hess� portfolio under

proved reserve additions of 172 million net barrels of oil

the supply and demand scenarios of the International

equivalent, representing an organic replacement rate of

Energy Agency (IEA), including the ambitious greenhouse

166 percent at a finding and development cost of just

gas (GHG) emission reductions assumed within the IEA�s

under $12 per barrel of oil equivalent. The majority of

Sustainable Development Scenario. Our strategy is

these additions were in the Bakken. Proved reserves at

consistent with the energy transition needed to achieve

the end of the year stood at 1.19 billion barrels of oil

the Sustainable Development Scenario, in which oil and

equivalent, and our reserve life was 11.5 years.

gas will continue to be essential to meeting the world�s
growing energy demand. Our current asset portfolio

Pro forma for asset sales and Libya, full year production
was 248,000 net barrels of oil equivalent per day in
2018 � 10 percent higher than the pro forma 224,000

is resilient, and our pipeline of forward investments
provides strong financial returns under the Sustainable
Development Scenario.

net barrels of oil equivalent per day produced in 2017.
In the Bakken, 2018 net production averaged 117,000

Our business planning includes actions we will undertake

barrels of oil equivalent per day, up from 105,000 barrels

to continue reducing our carbon footprint in keeping with

of oil equivalent per day the prior year.

SUSTAINABILITY
We see sustainability as fundamental to our long term
strategy and performance, supporting our purpose to
be the world�s most trusted energy partner. We are
proud to have been recognized once again in 2018 by

the findings of the U.N. Intergovernmental Panel on
Climate Change and the aim of the Paris Agreement to
limit global average temperature rise to well below 2�C.
Our Board of Directors and senior leadership have set
aggressive targets for GHG emission reductions, and
over the past 10 years, our company has reduced our
equity GHG emissions by approximately 63 percent.

a number of third-party organizations for the quality of
our environmental, social and governance performance

COMMITMENT TO SHAREHOLDERS

and disclosure.

We will continue to execute our strategy that has
positioned our company to deliver long term value and

Climate change is a significant global challenge that
requires governments, businesses and civil society to
work together on cost-effective policies. We believe
climate risks can and should be addressed while also
providing the safe, affordable and reliable energy

increasing returns to shareholders. We are proud of our
employees for their many accomplishments in 2018 and
grateful for the counsel and guidance of our Directors.
Thank you, our shareholders, for your continued support
and interest in our company.

necessary to ensure human welfare and global economic
development in the context of the United Nations (U.N.)
Sustainable Development Goals.
Our company is committed to helping meet the world�s

James H. Quigley

John B. Hess

Chairman of the Board

Chief Executive Officer


April 2019