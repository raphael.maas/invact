To Our Shareholders:
Fiscal Year 2012 was a period of continued, profitable
growth at HARMAN. We posted our eleventh
straight quarter of year-on-year improvement at
both top and bottom line, we booked multi-year
strategic business awards with leading customers,
and we continued to advance the agendas of
innovation and operational excellence that will
make HARMAN a stronger, more competitive
company year after year.
Net sales in fiscal year 2012 were $4.4 billion, an
increase of 16 percent from the prior year. Sales
increased in all three divisions and the Company
continued to gain share in selected markets.
Operating income in fiscal year 2012 was $300
million, an increase of 58 percent compared to the
prior year. Our cash and short-term investments
balance stood at $820 million at year end, and our
total liquidity of $1.4 billion gives us the continued
flexibility to meet our business obligations as we
pursue strategic opportunities for growth.
All three HARMAN divisions posted sales growth
during the year, fueled by dozens of new product
innovations, continued emerging markets penetration,
and aggressive brand marketing. The Infotainment
Division increased net sales by 15 percent and more
than doubled operating income. The Lifestyle
Division increased net sales by 22 percent and grew
operating income by one-third. The Professional
Division increased net sales by six percent while
strengthening its competitive position with new
production capacity in China.
HARMANs achievements during fiscal year 2012
are closely aligned with the Companys defined
four-pillar strategy to grow our smart infotainment
solutions business, increase penetration of our
branded audio systems, pursue aggressive growth
in the emerging markets, and drive continued
improvement in our cost and capital structure.
As the year began, HARMAN announced a realignment
of its division structure  in order to recognize
the convergence of consumer electronic devices
across multiple markets, better leverage the public
mind share fueled by our premium brands, and optimize
reporting transparency for our stakeholders.
We partitioned our large Infotainment business as a
separate division to underscore the dramatic sales
and profitability improvements we are making in
this space, driven by HARMANs quick-to-market
scalable systems approach. We have also energized
the brand awareness that drives every HARMAN
business with new customer ambassadors including
Sir Paul McCartney, Jennifer Lopez, Tim McGraw,
Maroon 5, Chinas Liu Huan, Indias A. R. Rahman,
and others.
Our Infotainment business achieved several significant
wins during the year, including a $2 billion
multi-year award from BMW, a $400 million extension
of our relationship with Volkswagen Group, and
more than $500 million in new emerging markets
business from domestic automakers such as Chinas
BAIC, Changan and Geely brands, and Indias Tata
Motors. To meet the growing demand for entry-level
embedded infotainment systems, we also launched
a new scalable infotainment platform targeting
entry- and mid-segment vehicles.
Our Lifestyle division continued to support current
market trends with new product introductions
geared to the soaring popularity of smart phones,
tablet computers, and home theater entertainment.
We launched new branded audio systems for leading
automakers including BMW, Ferrari, Hyundai, Kia,
Mercedes, MINI, smart and SsangYong. Leveraging
the synergies between personal multimedia and
in-car infotainment, our Aha Radio brand rolled out
a new cloud-based service that will delight our
loyal customers with thousands of online music
stations, news, audio books, personalized traffic
reports, and audio-based social media interaction.
Five additional premium automakers embraced
the robust Aha platform.

Our Professional division continued to support
talented performers such as Lady Antebellum, Neil
Diamond, Jimmy Buffet, and Lenny Kravitz on tour,
and completed more than 20 major stadium and
venue installations around the world  from Staples
Center in Los Angeles to the Peoples Great Hall in
Beijing. We also provided premium audio systems
for such landmark events as the 2012 London
Olympics, Super Bowl XLVI, and the 54th annual
GRAMMY Awards where HARMAN again served
as official sound partner. The Professional divisions
innovative new IDX Information Delivery System for
large public spaces was installed in eight international
airports and railway centers.
Our sales performance in the emerging markets was
particularly dramatic, with steadily-increasing BRIC
country sales led by a 42 percent increase in China.
We have now added nearly 3,800 employees in
best-cost countries  highlighted during fiscal year
2012 by the opening of HARMANs largest new
manufacturing and R&D facility in Dandong, China,
a new engineering center in Chengdu, and our
second Chinese flagship store in Beijing. We will
continue to add resources at our multiple development
facilities in India and China, and we are
strengthening our resources in Russia and Brazil as
we pursue such milestone opportunities as the 2014
Winter Olympics, the 2014 FIFA World Cup, and
2016 Summer Olympics.
Across these many achievements, cutting-edge
HARMAN innovation remains a common catalyst
for growth. We added some 700 new patents and
filings during fiscal year 2012, bringing our invention
portfolio to more than 4,400. We earned distinguished
supplier awards from Ferrari and Toyota during
the year, and formed new partnerships with other
leading innovators including Sierra Wireless, Texas
Instruments, Broadcom, NXP and Freescale.
This commitment to innovation continues to
differentiate HARMAN from its competitors. Our
energy-efficient GreenEdge audio and infotainment
systems deliver premium performance at twice the
power and weight efficiency of traditional systems.
We have advanced the HARMAN Connected Car
vision with such innovations as real-time smart
phone integration via Near Field Communications,
a Situational Human Machine Interface that
provides drivers with highly personalized in-car
data, Augmented Navigation that blends live video
with GPS-based map displays for greater clarity,
and a revolutionary Parking Assistant that mixes live
camera views from the vehicle with virtual reality
modeling of the surrounding space.
We remain sharply focused on eliminating potential
driver distractions as vehicles and their occupants
become more connected with the world around
them. Millions of HARMAN customers already
control their in-car infotainment systems and various
vehicle functions via simple voice commands, and
others enjoy access to hands-free, eyes-free
infotainment that converts diverse Web or message
content to streaming audio. We recently took yet
another step toward multi-sensory human interface
as we demonstrated gesture-based controls that
will let drivers manage in-car systems with simple
hand movements.
With an eye to further nurturing our long-term
business model, HARMAN published its first
Sustainability Report during fiscal year 2012 
dedicated to the core principles of generating shareholder
value and promoting safe, sound enjoyment;
innovating to improve our environmental performance;
and making sound investments in our people
and communities. We continue to take definitive
steps in each area.
Recognizing that HARMANs talented people are
integral to our long-term success, we continue
to empower our team through innovative training
programs at every level, and we link a portion
of every key employees compensation to the
achievement of specific personal and group goals.
We strengthened our top-level talent pool during
the fiscal year with the addition of Samsung
Electronics veteran I. P. Park as Chief Technology
Officer, and the appointment of former Tyco
International Chief Financial Officer Frank Sklarsky
to the HARMAN Board of Directors. Our seasoned,
multinational leadership and governance team

remains a differentiating HARMAN strength,
and we have built a robust talent bench with
which to seize future opportunities.
We continue to reach out to the communities where
we live and work through a variety of educational
and sponsorship programs focused on our passion
for education, music and the arts. We delivered on a
key environmental stewardship target by achieving
a five percent reduction in energy use and greenhouse
gas emissions at several key facilities during
the second half of the fiscal year, and we are
extending this program and its proven principles
to HARMAN facilities worldwide.
Beyond our steadily improving financial performance,
we took several additional steps to increase
shareholder value during fiscal year 2012  doubling
our annual dividend and launching a $200 million
share repurchase program. Two major ratings
agencies upgraded Harmans outlook midway through
the fiscal year, and both Standard and Poors and
Moodys have raised the Companys corporate credit
rating to investment grade as this report goes
to press.
I am very proud of HARMANs management team
and the thousands of talented employees worldwide
who have embraced our culture of continuous
improvement. Our team is energized to carry this
performance into fiscal year 2013, and we look
forward to the continued support of our customers,
employees, and shareholders toward cementing
HARMANs position as the clear global leader in
audio and infotainment.
Best regards,
Dinesh C. Paliwal
Chairman, President and
Chief Executive Officer