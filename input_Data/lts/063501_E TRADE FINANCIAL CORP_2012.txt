To Our Shareholders

As I reflect back on 2012, it was a year of significant progress for E*TRADE
in which a number of important accomplishments were achieved. Although
the year was marked by a high level of disengagement by retail investors
as macroeconomic and geopolitical uncertainties led to a loss of overall
confidence, we made important progress against our long-term Strategic and
Capital Plan which was submitted to our regulators in the second quarter.
Core to our execution of the plan are four key areas of focus:
 First: expand the brokerage business  This is the heart of everything we are doing. With plenty of
distractions in todays world, we cannot lose focus of our overarching purpose, marked by our goals
to expand market share and to increase our share of our existing customers investable assets.
 Second: strengthen the overall financial position of the Company  This speaks to both the balance
sheet and the income statement, and will always be a focus, but is particularly important today,
given the meaningful value that can be unlocked though the execution of our Strategic Plan.
 Third: focus on cost reductions and efficiencies  This obviously ties in directly with the second area
I just highlighted, but it is a heightened area of focus as we work to fundamentally change the way
in which we assess our expense base on all levels.
 And fourth: build out our Enterprise Risk Management capabilities  This needs to be appropriate
for our size and complexity, mindful of the current regulatory environment.
I would like to share with you a select group of achievements during 2012 with respect to each of
these key areas of focus.
Expand the brokerage business
The heart of E*TRADE is the brokerage business. During a year characterized by global
macroeconomic uncertainty, domestic political uncertainty, and an ever-approaching fiscal cliff, we
are proud of our successes in the core brokerage business. Net new accounts for the year reached
the highest absolute level since 2008, and we retained more of those accounts as evidenced by
our record-low brokerage account attrition rate during 2012. Similarly, we recorded $10.4 billion in
net new brokerage assets during the year  a 7% growth rate, and the highest level since we began
tracking the metric in 2007. However, with waning investor confidence and volatility at a five-year
low, our average trades per day declined 12 percent during the year.
Expanding brand awareness around our suite of retirement and investing offerings is key to
decreasing our reliance on trading, and to expanding our relationship with our existing customers,
as we aim to consolidate more of their investable assets at E*TRADE.

To that end, we continued to expand our retirement and investing product and service offering.
During the year, we placed Chartered Retirement Planning CounselorsSM in nearly every branch,
complementing our call center capability. We launched OneStop Rollover, which simplifies what was
traditionally a tedious process of moving a 401(k) to E*TRADE, and allows customers access to a
professionally-managed portfolio at E*TRADE.
Education also remains an important component of our overall value proposition. During 2012, we
more than tripled our customer interactions with nearly 3.5 million interactions on our education
platforms, comprising live events, and live and on-demand videos and web seminars, including those
available via our mobile platforms. We also saw an increase in the options-related interactions on our
education platform. This speaks to an evolving and more sophisticated customer base, which is also
evident in that nearly one-quarter of our trades this year were in options.
Mobile continues to be a crucial way to serve our customer base, as retail investors are increasingly
diminishing their use of desktop computing. We introduced a new E*TRADE Mobile platform for
Windows Phone, bringing our total mobile platforms to six. Within these six platforms, we made
several upgrades and enhancements, including the introduction of options and mutual fund trading,
mobile check deposit, voice recognition software, and barcode scanning features. Mobile adoption
rates continue to improve as we saw 6.5% of all customer trades in 2012 executed through our
mobile platforms.
Giving our customers capabilities, knowledge and advice when and where they need it has not
gone unnoticed. The hard work and commitment of our employees earned us the #1 ranking in
Kiplingers annual best of the online brokers survey  the only firm to receive 5 star ratings in both
the Customer Service and Investment Choice categories. Among the other third party recognition and
awards received in 2012, our customer offering was recognized by SmartMoney  receiving top marks
in the trading tools and research categories  and by Barrons  earning top marks in portfolio analysis
and research. Both firms also recognized our customer service as one of the best in the industry.
Strengthen the financial position of the Company
In addition to growing our core business, we also made significant strides in strengthening the
financial position of the Company through execution of our Strategic and Capital Plan. Principally, we
refinanced our most expensive corporate debt, replacing it with the lowest coupons the Company
has ever issued. This effectively reduces the corporate cash interest expense by more than one-third
and extends our nearest debt maturity to 2016.
Furthermore, we took tangible steps to improving our Tier 1 Leverage ratio  our most constraining
ratio and also an important barometer in gauging our success against the Strategic and Capital
Plan we submitted to regulators. We view deleveraging to be the most practical and shareholderfriendly
approach to improving this ratio, and to that end, we completed a total of $4.9 billion in
deleveraging actions in 2012  at the low end of our targeted $5 to $10 billion. Considering actions
taken in 2013 to date, along with those planned for the future, we expect to ultimately settle toward
the high end of this range.
Progress on improving the quality of our balance sheet continued, as our legacy loan portfolio is
down 68 percent from its peak size. Along with the reduction in the size of this portfolio, these loans
are becoming more seasoned and total delinquencies have declined substantially, down 32 percent
in 2012.

Focus on cost reductions and efficiencies
Our third area of focus relates to reducing our costs and managing our business more efficiently. We
turned our attention away from the macroeconomic environment and factors outside of our control
and took a closer view of ourselves. Early in the year we set a target to identify $50 million in cost
reductions. I am proud to say that following our in-depth analysis, we have identified $110 million in
cost reductions, all of which we expect to be implemented by the end of 2013.
Build out Enterprise Risk Management capabilities
The Companys last key area of focus relates to the build out of our Enterprise Risk Management
capabilities, to create a structure that is appropriate for our size and complexity. We made significant
progress on this front during 2012, and are well on our way to implementing a best-in-class structure.
While risk management is the responsibility of every employee at E*TRADE, it really starts with
the appropriate tone at the top, which I believe we have set through internal communications and
focused actions. We are enhancing our risk management structure to focus on specific areas  for
example, operational risk management, capital planning, and regulatory relations. We are bolstering
areas like vendor management and stress testing; we have built out our teams of compliance
personnel and internal auditors, and utilized external consultants; and we began engaging in systems
upgrades.
Looking ahead: 2013
It is clear that these are pivotal times at E*TRADE. I am proud of what our team accomplished
in 2012 and look forward to what is to come in 2013. With our new CEO Paul Idzik at the helm, I
have every confidence that the Company will continue to seamlessly move forward with respect to
executing on our Strategic and Capital Plan.
Sincerely,
Frank J. Petrilli
Chairman of the Board