United States Steel Corporation entered 2016 facing a great deal of uncertainty and significant headwinds.
 Conditions in the energy tubular markets continued to deteriorate through much of the year.
 Global steel overcapacity was continuing to grow.
 Imported steel products, many of which were unfairly traded, were still coming to the United States and the
European Union at extremely elevated levels.
 Spot steel prices were still near their 13-year lows.
In 2015, we had just completed one of the most challenging years in our history, and 2016 was looking equally difficult at
the start.
Our company has experienced many challenges in recent years, and 2016 was no exception. However, what we
experienced in 2015 taught us invaluable lessons. We continued to face difficult choices as we worked to improve our
cost structure, streamline our operations and increase our customer focus. Some of those choices involved facility
shutdowns, idlings and layoffs. We recognize the impact these decisions can have on our employees and the
communities where we operate. However, we remained focused on the long-term health and stability of our company,
and achieving those conditions can require difficult steps along the way, but will ultimately result in sustainable benefits for
all of our stakeholders. Transformation is never easy, even in the best of circumstances.
Now Id like to turn to safety, which remains our #1 core value. In three different months in 2016, our entire company
worked without an injury requiring days away from work. In addition, numerous operating facilities set or are in the midst
of record injury-free streaks. From a safety perspective, we also continued to outperform peers in both steel and
manufacturing  in some cases by a significant margin. Employee engagement played an important role and resulted in
strong performances in key leading indicators such as employee-initiated safety conversations and the identification and
elimination of hazards. While these are all positive developments, we did experience serious safety incidents at our
facilities last year, including fatalities. Key lagging indicators also did not show year-over-year improvement. This
performance is not acceptable to me or any member of our leadership team. Together, we are committed to reversing
these aspects of our safety performance in 2017 because our commitment to zero injuries is unwavering. Safety is too
important to just be good enough  we are striving for greatness.
The primary reason for our progress to date in both our safety and corporate transformation journeys  our employees.
They remained focused on the things within our control, and they used Carnegie Way principles and methodologies to
address them. Their enthusiasm, creativity, resiliency and tenacity continue to amaze and inspire us every day. Last
year, employees at all levels continued working through our multi-level Carnegie Way training experience, which enabled
them to complete projects that were more complex and delivered more value to our stakeholders. Together, our focused
approach generated $745 million in Carnegie Way benefits in 2016. This is the third consecutive year weve delivered
sizable improvements to our business, which reinforces the scope of what we are trying to accomplish and the success
weve been able to achieve. Overall, our employees are embracing change and positively impacting our cultural, financial
and operational flexibility and effectiveness. We would not be where we are today without their commitment to the
journey were on together, and for that I am grateful.
Last year, we placed significant emphasis on improving our financial flexibility, increasing our customer focus and
reducing costs. I am pleased to report that we made noteworthy progress in all of these areas  progress that has us
well-positioned to take advantage of better market conditions in 2017.
Throughout 2016, we used a disciplined approach to continue strengthening our balance sheet. We made considerable
improvements to our working capital. We also completed opportunistic capital market transactions, including a successful
debt refinancing and an equity raise. These actions improved our debt maturity profile and increased our cash on hand,
positioning us to be able to take more aggressive actions to invest in our assets and grow our business.
Included in these investments is our asset revitalization plan that will be a key area of focus for our Flat-Rolled segment in
2017. Rooted firmly in The Carnegie Way, our comprehensive multi-year asset revitalization efforts will help us achieve
multiple goals. The projects involved will target improvements in safety, product quality and capabilities, asset reliability,
delivery and cost, all of which will enhance our profitability and competitiveness. Its important to note that while this kind
of investment will have an increased pace and focus in 2017, its not new to us. We have already made significant
progress in all of these areas, particularly in driving costs down at all of our steelmaking and raw materials operations.
We are now striving to reach higher and more sustainable performance levels. In addition, strategic investments in our 
2
people, processes and facilities will allow us to keep pace with the increasing needs and expectations of our customers
while differentiating ourselves from our competitors.
The full alignment in 2016 of our Flat-Rolled Commercial Entities and the manufacturing facilities that serve their
customers played an important role in many customer-focused improvements last year. Bringing all of the people
responsible for specific market segments together in one structure has created more focused and effective accountability
among team members. Together, they are getting closer to our customers and delivering solutions that create value for
both of us. Our customers are noticing the changes were making. Their message to us in return: We like what were
seeing  keep it up.
Our approach to transformation and our Carnegie Way methodologies were also the foundation for the bold and
aggressive actions we have taken to combat unfair trade and global overcapacity in recent years. In 2016, we built on the
previous years wins in the areas of legislative action and enforcement. Last year, we successfully completed three trade
cases in the United States that began in 2015. The U.S. Department of Commerce also opened an investigation into
whether cold-rolled and corrosion-resistant products made in Vietnam from Chinese substrate were circumventing
antidumping and countervailing duties already in place against Chinese steel. We also continued to support two cases
launched by the European Commission in 2015. In addition, we provided vocal support for U.S. and EU leaders as they
joined a growing chorus of voices urging countries with significant overcapacity to deal with the problem in a meaningful
way. Finally, we filed a groundbreaking Section 337 trade case with the U.S. International Trade Commission against
Chinese steel companies and distributors. While unfair trade is still an uphill challenge, its a very important issue we are
more than willing to confront head on until we achieve the level playing field we are entitled to under the law.
Our recent work on trade is one example of how innovation is taking hold and enabling our employees to create value.
Great companies recognize that transformation journeys require innovation, and last year The Carnegie Way continued to
fuel the innovative spirit that has long been a hallmark of our company. Were doing things in fundamentally different
ways that are making our plants, our people and our company better. Much of that work involves a deeper dependency
on data-driven decisions, helped in part by the substantial completion of our Enterprise Resource Planning (ERP)
deployment in the United States. With centralized data and uniform tools for analyzing it, we are seeing the benefits that
come with more standardization and better process control in offices and operating facilities across our company.
Of course a major focus of our innovation efforts remains our customers. Last year, we reorganized our research and
development team to put more emphasis on solution development. That combined with our concentration on product
quality and technical collaboration has proven to customers across markets that their voices are being heard and their
needs are being addressed. We also created an innovation team with a very specific purpose: cultivate relationships with
third parties in the public, private, governmental and educational sectors whose resources could help us develop
breakthrough opportunities for our business and for the materials we produce. Were encouraged by our early results and
believe there is significant value we can gain from these efforts.
In closing, I want to thank all of our stakeholders for their continued support during another challenging year. Were only
beginning to glimpse the true and sustainable power of what weve accomplished so far in our transformation, and thats
thanks to the contributions of our many stakeholders. Our customers, suppliers, employees, unions, lenders and
stockholders all believe  as I do  that the progress we have made in our Carnegie Way journey has positioned us to
deliver better results. Despite the challenges weve faced over the last two years, we know weve created a solid
foundation upon which we can build something special  something we can all be proud of. We look forward to doing
just that in the journey ahead.
Sincerely,
Mario Longhi
Chief Executive Officer