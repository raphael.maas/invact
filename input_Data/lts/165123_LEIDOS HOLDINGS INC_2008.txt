Message to Stockholders

As we enter our 40th year, our company is
stronger than ever. Our success is a testament to
our employees and their dedication to solving
our customers most critical problems.

Building for Continued Growth. Through the efforts of our employees,
in fiscal year 2008, we accelerated organic growth and expanded
operating margin, while building a strong foundation for continued
growth. Moreover, revenues for fiscal 2008 were $8.94 billion, up 11
percent from fiscal 2007; operating income for fiscal 2008 was $666 million,
up 16 percent over the previous fiscal year. These results were achieved
while laying the foundation for continued growth in future years, as we
continue to win larger programs. During the year, we won seventeen $100
million plus contracts with an expected value above $8 billion. One of
the more significant contracts we won last year involves managing the
supply chain for chemicals and packaged petroleum, oils, and lubricants
for the Department of Defense. Under an important command, control,
communications, computers, and intelligence program we won in fiscal
2008, we are expanding our support of the Space and Naval Warfare
Systems Command. Our customers are increasingly recognizing the
capability Science Applications International Corporation has in systems
engineering and integration. So, while we achieved terrific results in fiscal
2008, we also were successful in winning contracts that give us confidence
for continued growth in the future.
Strategic Acquisitions. During fiscal 2008, we completed key strategic
acquisitions to expand our opportunities in the areas of energy,
infrastructure, and the environment. We acquired Benham Investment
Holdings, LLC, an engineering and life-cycle technology implementation
firm that offers a full range of capabilities in consulting, engineering,
architecture, and design/build, including specialized expertise in energy
management, alternative fuels, process engineering, and industrial manufacturing. We also acquired Scicom
Technologies Private Limited, headquartered
in New Delhi, India, which provides on-site and
offshore hydrocarbon exploration product
development services and technology
consulting. Additionally, we effectively sold our
joint venture interest in AMSEC LLC and received
in exchange the technology and engineering
business divisions of AMSEC that we integrated
into our company following the transaction.
New Chief
Operating Officer.
Larry Prior became
our new chief
operating officer,
effective October
1, 2007. He brings
the right experience
and great leadership
ability for this key job,
qualities we saw in
his successful leadership in his previous position
as president of our Intelligence, Security, and
Technology Group. Larry brings exceptional
focus on business fundamentals and execution.
Larry now is leading our efforts to transition
our company to more robust financial and
procurement systems, as well as leading
Project Alignment.
Project Alignment. Across the enterprise,
we are taking a number of steps to improve
our performance and competitiveness, while
clarifying our employees responsibilities
and providing rewarding career paths. To
make these changes, we have undertaken an
initiative called Project Alignment. This is a
major, multiyear effort to bring together many
of the functions of human resources, finance,
information technology, and other functional
areas into a shared-services organization for the
entire enterprise.
Enduring Culture and Values. While our initial
public offering in October 2006 ushered in a
new era for us, marking a major change in our
capital structure, our entrepreneurial culture
and our values remained strong. Our financial
results and our prospects for the future show
that our culture is alive and well. Our company
was recently recognized by the Ethisphere
Institute for having one of the top overall ethics
programs among government contractors.
We were honored to receive this recognition,
but we also understand that we must remain
vigilant to sustain our ethical standards and
our entrepreneurial spirit in all of our business
activities. Providing solutions to our customers
toughest problems and dealing with matters of
global importance are our historical legacy and
will be our future.
1
Ken Dahlberg
CEO and Chairman of the Board