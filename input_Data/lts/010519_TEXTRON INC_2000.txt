2000 was a defining year for Textron, as we proved our ability to manage 
through turbulent times. We drove organic growth by improving performance 
of our market leading businesses and investing in innovation. We also 
strengthened our portfolio with the addition of 11 new companies. And, we 
continued to apply the rigorous principles of Textron Quality Management 
(TQM) to improve our operating performance and margins. 
These actions helped our Company offset the impact of the year's challenging operating environment, which included a slowdown in both the U.S. 
and European economies. Our record performance in 2000 speaks for itself: 
Revenues rose to a record $13.1 billion, a 10 percent increase, marking 
our fifth consecutive year of double-digit revenue growth. 
Earnings per share before special charges grew 15 percent this year, 
marking our 11th year of consistent earnings growth and our eighth 
year of double-digit increases. 
Segment profit margins improved by 70 basis points to 10.8 percent as 
a result of our aggressive cost reduction efforts under TQM. 
We continued to maintain a strong balance sheet, with debt-to-capital 
of 32 percent and free cash flow of $463 million, both within our 
target range. 
Our businesses made great strides in key areas during the year: 
In our Aircraft segment, Bell Helicopter and Cessna Aircraft continued 
to define their markets and exceed customer expectations -- evidenced 
by a record backlog of $8.1 billion. 
Our Automotive business achieved record results, improving segment 
margins by 60 basis points despite a rapid decline in North American 
automotive production at the end of the year. In addition, the segment 
was honored with numerous quality and innovation awards. We won 
$829 million in new business, fueling continued growth for the future. 
Within Fastening Systems, we've begun to see positive quarter-over-quarter 
margin trends. During the last 12 months we've significantly strengthened 
our management team to lead this business to higher operating efficiency 
and growth. 
In our Industrial Products segment, our Data-Signal-Voice (DSV) business 
grew to over $150 million in annualized sales as a result of acquisition 
and internal investment. This group is emerging as a strong player in this 
niche of the broader telecommunications market. And within our Golf 
and Turf business, we leveraged our brand strength to penetrate new, 
nontraditional markets with products such as EZGO's new rough terrain 
vehicle which serves the rural sportsman and agriculture markets. 
At Textron Financial, we achieved our 22nd consecutive year of net income 
growth by focusing on high growth, niche markets and maintaining 
industry setting standards for credit quality in our portfolio. 


Despite our outstanding 15 percent earnings growth, we faced a major disappointment in 
2000 as our stock price fell 39 percent by year end. We believe that, at today's levels, Textron 
stock represents a real value. In fact, during 2000 we invested $358 million to repurchase 
approximately 6.6 million shares. 
Meeting the challenge of an increasingly competitive business environment, we initiated 
a Companywide restructuring program in 2000 to optimize operating efficiencies within our 
Automotive, Fastening Systems and Industrial Products segments. We began to consolidate 
manufacturing facilities, rationalize product lines, divest noncore units and outsource non 
core production. When the restructuring is complete, it will generate annual savings of 
approximately $100 million to $120 million. 
A New Strategic Framework to Drive Growth 
For the past 11 years, Textron has distinguished itself 
through a profound commitment to consistent growth. 
Indeed, we have fulfilled this commitment unfailingly, 
reporting 45 consecutive quarters of increased earnings. 
However, it is clear that consistent growth alone is 
no longer sufficient to make Textron a convincing and 
compelling investment. As a result, we are committed 
to transforming Textron using a new strategic framework 
aimed at delivering compelling growth. We will accomplish this by creating a portfolio of powerful businesses 
and brands, and by fostering enterprise excellence -- 
with return on invested capital (ROIC) as our compass 
for guiding the way. 
Creating a Portfolio of Powerful Businesses and Brands 
Consistent growth has been a Textron hallmark over the 
past decade and we intend to continue that tradition. Under the tenets of our new strategic 
framework, we will create a simpler, more focused portfolio of strong businesses with powerful 
brands. Textron businesses will operate only in attractive industries -- industries that are growing 
faster than GDP levels; where competition is based on value and innovation rather than cost; 
and where leading players have the potential to earn high returns on capital. Increasingly, 
industry attractiveness will be a key criterion in our portfolio decisions. Our reinvigorated 
portfolio will be recognized for its substantial brand equity as measured by indices such as 
market share, competitive differentiation and margins. 
Many of the businesses in the Textron portfolio are powerful brands and already demonstrate 
these characteristics. Bell Helicopter is just one example. Here, the strength of our brand has 
allowed us to capture leading market share as we invest in tilt-rotor technology. And at Cessna 
Aircraft, we continue to differentiate ourselves with the fastest business jet in the world and 
a record delivery of three new models in 2000. EZGO and Greenlee have become their customers' first choice by consistently delivering leading-edge products and technologies. And the 
list continues. We are focused on transitioning all of our businesses into strong, power brand 
positions to deliver improved growth across our portfolio. 
Achieving Enterprise Excellence 
Another strategic imperative is to leverage the potential of the Textron enterprise. Throughout 
our history, our primary focus has been on optimizing each business individually. We have 
launched a new initiative called ``Enterprise Excellence,'' which enables us to tap our collective 
potential to gain greater leverage in areas such as e-business, supply chain management, 
shared services and global Internet infrastructure. These new efforts will foster organic 
growth while dramatically reducing costs. 
E-business offers tremendous enterprise wide opportunity for Textron. During the year, we 
engaged in a range of Internet related alliances aimed at fostering organic growth and reducing 
costs. For example, we began to implement universal B2B procurement processes for the 
purchase of goods and services worldwide. We created Asset Control Corporation, the industry's 
most comprehensive B2B marketplace specializing in the online resale of surplus industrial 
equipment, excess inventory and commercial real estate. We also recruited a new senior 
executive to oversee all aspects of our enterprise wide supply chain management program. 
Using ROIC as Our Key Performance Metric 
We believe that ROIC is the best indicator of how well a company deploys shareholder resources. 
As such, we have adopted ROIC as our primary financial measure of growth and value for our 
shareholders. It now guides every investment choice. To reflect the importance now placed on 
ROIC, we have more directly linked executive compensation to our ability to improve this target. 
Our goal is to achieve ROIC of at least 400 basis points greater than our weighted average 
cost of capital. During 2000, we increased ROIC to 13.1 percent from 12.6 percent in 1999 -- 
a respectable improvement but still 190 basis points from our goal. Other key financial objectives 
through 2005 include: 
Organic revenue growth of approximately five percent annually; 
Segment profit margins in excess of 13 percent; and 
Earnings per share growth averaging 10 percent per year. 
A-World-Class Management-Team Leading-Textron's Transformation 
In 2000, we fortified our management team with the appointments of Ted French, our new 
Chief Financial Officer and Terry O'Donnell, our new General Counsel. Complementing our 
external recruitment efforts, we also filled several key positions by tapping our own internal 
talent pool. 
Looking forward, our new strategic framework will require widespread transformation at 
Textron. Accordingly, we have formed a Transformation Leadership Team consisting of Textron's 
top 14 leaders and a number of other key executives. This team is charged with leading the 
transformation process and translating our new strategic framework into compelling, 
sustainable performance. 
A Case for Compelling Growth 
Building on our fundamental strengths, Textron's future has an expanding horizon. We are an 
experienced industry leader with an increasingly powerful portfolio of businesses. We have a 
clear strategic framework that will deliver better value for Textron and our shareowners, and 
we have the right leadership team in place to execute it. Further, our strong balance sheet 
provides the flexibility to make opportunistic investment decisions. All of this supports our 
well-deserved reputation for excellence, illustrated by our rankings among Industry Week's 
"World's 100 Best Managed Companies," and Fortune Magazine's "Global Most Admired 
Companies" in 2000. 
We believe that our new strategic framework is built on a solid foundation established 
through a decade of excellent performance. Moreover, our strong performance this past year 
has prepared Textron to face economic uncertainties and implement the necessary changes 
to become an even stronger company in the future. These accomplishments, combined with 
our managerial commitment to succeed, will unlock the power of the Textron enterprise. As 
we turn the key and open the door to a new Textron in 2001, I would like to extend my warm 
thanks to our shareowners, employees and customers for your continued loyalty and support. 
