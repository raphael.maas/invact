Dear Fellow Shareholders,
The saying goes when one door closes, another opens. In regards to Murphys performance throughout 2003 it
is an apt description. As production declined from our legacy assets in Western Canada and the U.K. sector of the
North Sea, new production ramped up in Malaysia and the Gulf of Mexico. And now, as we continue to develop our
new discoveries, we are in position to refocus our energies as a blue chip international frontier exploration
company that grows through the drill bit, a role that comes naturally to a company with
wildcatter roots.
This year, although good to Murphy, has proven quite trying for the world in general.
Uncertainty about world oil supply and the reaction of OPEC to the liberation of Iraq
and a declining U.S. dollar value kept commodity prices artificially inflated. Despite
such turbulence in our global landscape, we recorded encouraging results in terms
of increased earnings and exploration successes with good long-term potential.
Our Results By The Numbers
Our 2003 net income grew 164 percent to $294.2 million on revenues of $5.3 billion
and our earnings per diluted share for the year were $3.17. The Companys income
from exploration and production operations was $326.2 million, up 103 percent from
2002 levels. Although our refining and marketing operations had disappointing results,
suffering an $11.2 million loss in 2003, the segments financial results were a
significant improvement over 2002.
Our worldwide crude oil and condensate sales prices averaged $25.27 per barrel
during 2003, compared to $23.59 per barrel for 2002. Our total crude oil, condensate
and gas liquids production was an average 83,452 barrels per day in 2003 compared to
76,370 barrels per day in 2002, with the net increase due primarily to production from our
West Patricia field in Malaysia, and from Medusa and Habanero, our two Gulf of Mexico
deepwater fields that came on stream late in the year. Our North American natural gas
sales prices averaged $4.83 per thousand cubic feet during 2003, compared to $2.94 per
thousand cubic feet during 2002. Natural gas sales volumes declined from 297 million
cubic feet per day in 2002 to 215 million cubic feet per day in 2003, primarily due to lower
production from our Ladyfern field in Western Canada.

Funding Our Growth
Our capital expenditure program for 2003 included high
impact development projects in the Gulf of Mexico and Malaysia,
the Syncrude expansion program in Canada, and continued
exploration with a four-well program to define the potential of our Malaysian deepwater blocks. In addition to
the $763 million of capital spending allocated to exploration and development worldwide, approximately $215 million
was earmarked for downstream, including the completion of the Clean Fuels Project and a capacity upgrade at our
Meraux, Louisiana, refinery, and the continued build-out of the Murphy USA program at Wal-Mart Supercenters.
By year-end, 623 Murphy USA stations were in operation.
With the anticipated sharp decline of the once prolific Ladyfern field in Western Canada and the need for asset
reallocation to fund ongoing frontier drilling and development programs throughout 2004, we announced plans to
divest most of our conventional oil and gas properties in Western Canada. Continued growth in those maturing
basins would have required substantial reinvestment at a time when our program outside North America is growing
at a dynamic pace. Accordingly, we decided to capture and redeploy some of that value. This is consistent with
our previous actions of disposing of high cost and less profitable mature properties, which included our interests
in the Ship Shoal Block 113 field in the Gulf of Mexico Continental Shelf and the Ninian and Columba fields in the
U.K. North Sea.
Building Our Bench Strength Inside and Out
To better reflect our capability as a frontier exploration and production company and a downstream refiner and
marketer, we revised our organizational structure and added two key board members in early 2003. Our U.S. and
international exploration and production operations were separated into two operating segments: Murphy Exploration
& Production Company  USA, led by John Higgins, President; and Murphy Exploration & Production Company 
International, led by David Wood, President. These two E&P subsidiaries, along with our Canadian subsidiary,
Murphy Oil Company Ltd., with Harvey Doerr at the helm as President, now form the legs of a three-prong global
E&P strategy that focuses on the Gulf of Mexico deepwater, Malaysia and West Africa (Congo), and Canada. Our
worldwide downstream operations, consisting primarily of refining assets and retail marketing stations in the U.S.
and U.K., are now headed by Mike Hulse, President. These are talented, intelligent and highly motivated executives
who are responsible for our greatest successes in the past couple of years, and who are tasked with the future of
our Company.

Added to our Board of Directors were: Frank Blue, a 28-year veteran of the oil industry with international
experience in Southeast Asia and an esteemed attorney formerly with Fulbright & Jaworski in Houston, Texas;
and Ivar Ramberg, Executive Officer of Ramberg Consulting AS of Lysaker, Norway, and formerly President and
CEO of Norsk Hydro Canada.
The Lure of Frontier Exploration
Frontier exploration is in our blood. Underpinned by ongoing production from such core legacy assets as
Terra Nova, Hibernia, Syncrude and Schiehallion, we can commit to focused exploration in high impact areas,
specifically deepwater Gulf of Mexico and Malaysia. Our various drilling programs are further detailed in the
discussion following this letter, but the potential in Malaysia deserves special note. We currently have interests in
more than 14 million gross acres in three hydrocarbon provinces offshore Malaysia. Our Kikeh discovery in Block K
will create value for Murphy for many years to come. Not only is our working interest 80 percent on Block K, but we
are also operator. First production from Kikeh is projected for 2007.
Significant Downstream Momentum
Our downstream business is driven by our Wal-Mart program. In 2003, we opened 117 new Murphy USA stations
and are planning for approximately 160 new builds for 2004. We have already earned more than one percent of the
national market share in retail fuel sales, a significant achievement considering that this is only our fifth program year.
Our success with Murphy USA has driven the expansion of our Meraux refinerys crude processing capacity to
125,000 barrels per day. We have also brought on line new processing equipment to manufacture clean fuels.
Living Up To Our Legacy
I sincerely believe that the reason we enjoyed such positive results for 2003 is because we earned it, and because
we do not fit the mold of the typical independent exploration and production company.
First of all, our business model is two-fold with both upstream and downstream segments. Our growth through the
drill bit is balanced by our retail assets. Second, we are calculated risk-takers who choose to participate in underexplored
provinces with potential for sizable discoveries. Third, we are culturally and fiscally conservative and have
a debt to total capitalization ratio that is one of the best in our industry. Fourth, we never forget that we are working
for our shareholders and we continually strive to create value for them. We are wildcatters at heart, but we are
well-prepared, accountable and hard-working.
It has been an eventful year for Murphy Oil Corporation and we are looking at 2004 and beyond from a positive
perspective  with growing production, good exploratory prospects and an expanding retail market share. Although
the future is always uncertain, we are confident that we have the fiscal strength, the operational tools and a deep
pool of talent to meet whatever it brings our way.
Claiborne P. Deming
President and Chief Executive Officer
February 17, 2004
El Dorado, Arkansas