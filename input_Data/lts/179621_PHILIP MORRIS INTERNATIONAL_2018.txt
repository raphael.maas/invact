Dear Shareholder,


In 2018, we achieved robust results from our combustible tobacco
portfolio and nearly doubled our heated tobacco unit (HTU) in-market
sales volume, driven by growth in all IQOS markets. We fell short of
our full-year net revenue growth target provided in February 2018,
which was almost entirely attributable to lower-than-anticipated IQOS
consumer acquisition in Japan and related distributor HTU inventory
adjustments. This was a disappointment in an otherwise robust financial
and strategic performance across the business.
The net revenue growth shortfall contributed to an overall decline
in our share price, which was otherwise pressured by broad market
concerns surrounding the industry and the consumer staples sector.
While we recognize that the market is the ultimate judge, we find it
difficult to understand the share price impact of certain developments
in the industry last year, particularly those that were very U.S.-centered
and, arguably, less relevant to our international business.
Entering 2019, we believe that PMI has laid the foundation for
stronger performance, thanks to significant investments in product
portfolio development and organizational capabilities, including a stateof-the-art digital infrastructure to fuel our expansion. The underlying
strength of our combustible product portfolio remains intact, and our
smoke-free products are catalysts for accelerating substantial overall
business growth.
2018 vs. 2017 Results
Total cigarette and heated tobacco unit shipment volume of 781.7
billion units decreased by 2.1%, primarily reflecting the net impact of
estimated distributor inventory movements, principally related to HTUs
in Japan. Excluding these inventory movements, total shipment volume
was flat, comparing favorably to the 1.6% decline for the total industry,
excluding China and the U.S. This represented our best annual volume
performance since 2012.
We grew total cigarette and HTU market share by 0.5 percentage
points, reaching 28.4% of the international market, excluding China and
the U.S., driven primarily by the strong growth of our heated tobacco
brands. Underlining the strength of our combined portfolio, share grew
in all six of our Regions.
Importantly, our 27.4% share of the international cigarette category
was flat, demonstrating our success in maintaining cigarette market
leadership while transitioning our portfolio to a smoke-free future.
Despite over-indexed out-switching to IQOS and the sizable volume
contraction in Saudi Arabia during the first half of the year, Marlboro�s
share of the international cigarette category was also flat at 9.7%.(1)
Net revenues of $29.6 billion increased by 3.1%, or by 3.4%
excluding currency, reflecting RRP volume growth, mainly driven by
IQOS in our European Union and Eastern Europe Regions, as well as
our duty-free business, coupled with higher pricing for our combustible
tobacco portfolio across all Regions. The inventory adjustment in
Japan adversely impacted total ex-currency net revenue growth by
approximately 1.2 points. The move to highly inflationary accounting
in Argentina negatively impacted our currency-neutral net revenue
growth by a further 0.6 points.
Operating income of $11.4 billion was down by 1.8%, or up by 0.1%
excluding currency. Operating income margin decreased by 1.3 points,
excluding currency, primarily reflecting net incremental investment in
IQOS of approximately $600 million.
Adjusted diluted EPS of $5.10 increased by 8.1%, mainly reflecting
a lower effective tax rate and net interest expense stemming from U.S.
corporate tax reform, partly offset by currency. Excluding currency,
adjusted diluted EPS increased by 10.4%.
Operating cash flow of $9.5 billion increased by $0.6 billion
or by 6.4%, principally driven by higher net earnings, partly offset
by currency. Excluding currency, operating cash flow increased by
8.9%. Capital expenditures of $1.4 billion primarily reflected further
investment behind heated tobacco unit production capacity expansion.

In June, the Board of Directors approved a 6.5% increase in the
quarterly dividend to an annualized rate of $4.56 per share, reflecting
its confidence in the growth outlook of the business, underpinned by
the potential of our smoke-free products. The increase underscored the
Board�s commitment to generously reward shareholders over time.
Financing costs continued to decrease in 2018, primarily reflecting
ongoing efforts to optimize our capital structure following the U.S. tax
reform. Overall net interest expense was down by over 27% vs. 2017,
with a 2.5% weighted-average-all-in-financing cost of total debt.
Fiscal and Regulatory Environment
Our exceptional combustible tobacco pricing variance of 7.6% in 2018
exceeded our annual average of approximately 6.4% for the period
2008 to 2017. The increase primarily reflected a largely rational
cigarette excise tax environment and particularly strong pricing in
Canada, Germany, Indonesia, the Philippines and Russia. Importantly,
HTUs continue to be subject to excise tax classifications and structures
that preserve a favorable differentiation to combustible tobacco
products.
We continued to advocate for comprehensive risk-proportionate
regulation for smoke-free products, believing that public health
objectives regarding smoking can be met more rapidly and sustainably
by fully incorporating such products into existing tobacco control
policies. While widespread political endorsement has yet to emerge, a
number of countries have joined the U.S. and the U.K. in recognizing
better alternatives to cigarettes as important elements of policy.
Combustible Tobacco Portfolio
Our combustible tobacco portfolio has provided the resources
for investing in our vision of a smoke-free future and the ultimate
transformation of our business. Until we achieve our vision, we remain
committed to maintaining a leading share in the international cigarette
category and are managing our portfolio accordingly.
In this regard, we are focusing our combustible product innovation
strategy on fewer, more impactful initiatives that can be deployed
globally and swiftly. This strategy resulted in a 44% success rate
for new products in 2018. Furthermore, we continued portfolio
consolidation and simplification through portfolio morphing and the
reduction of low-volume brands. Thanks to these efforts, our top-six
international cigarette brands represented approximately 73% of total
cigarette volume in 2018, up from approximately 62% in 2013.
Reduced-Risk Product Commercialization
The year 2018 marked another meaningful step forward in our journey
to replace cigarettes with smoke-free alternatives. With IQOS available
in 44 markets as of year-end, our heated tobacco portfolio is now the
twelfth-largest international tobacco brand, excluding China and the
U.S. The number of legal age smokers worldwide who stopped smoking
and switched to IQOS(1) increased by 1.9 million to reach an estimated
6.6 million, with total IQOS users(2) reaching 9.6 million. In fact, IQOS
grew its user base in all launch markets, including significant growth
in our EU Region and Russia. This growth in the IQOS user base drove
a near-doubling of our global HTU in-market sales volume, which
reached approximately 44 billion units, versus 23 billion in 2017.
The most important product milestone in 2018 was our successful
global launch of the IQOS 3 and IQOS 3 MULTI devices beginning
in Japan, Korea and Russia. IQOS 3 features consumer-centric
enhancements, and IQOS 3 MULTI addresses the need of many
consumers for consecutive use of consumables.
A key focus in 2018 was speed and effectiveness in identifying
and addressing pain points along the IQOS consumer journey. This
was particularly important in Japan, where the slowdown in share
growth, compared to prior years, reflected lower penetration beyond
the innovator and early adopter consumer segments, as well as natural
experimentation by some IQOS consumers with new competitive offerings - all factors that led to a reduction in daily HTU consumption.

The global launch of IQOS 3 played an important role in addressing
these issues. However, we also implemented initiatives in Japan
to address the needs of more conservative adult smokers. These
included a simplified registration process, deployment of more targeted
communications to increase the understanding and relevance of IQOS
benefits, and the introduction of HEETS consumables at a mainstream
price point.
Outside Japan, we recorded sequential HTU quarterly share growth
across virtually all IQOS launch markets. The standout performers
included Russia, which reached a national market share in the fourth
quarter of 1.8%, despite IQOS being present only in approximately 25%
of the country by weighted tobacco volume, as well the EU Region,
which reached a fourth quarter Regional share of 1.7%, supported by
sequential share growth in all 24 launch markets. Notable examples
of strong national share performance in the fourth quarter were the
Czech Republic (4.2%), Greece (6.6%), Italy (3.3%), Poland (1.5%) and
Portugal (4.4%). These were particularly encouraging results given
that the marketing focus in these countries remained within select key
cities where the shares are above the national averages. This is equally
true of Japan and Russia where in Tokyo and Moscow, for example,
offtake share in the fourth quarter of 2018 reached 18.8% and 7.5%,
respectively.
Fourth-quarter share for HEETS in Korea increased by 1.1 points
sequentially to reach 8.5%, favorably impacted by estimated trade
inventory movements ahead of the change in health warnings on
heated tobacco products in late December.
Notably, IQOS continues to have high conversion rates across all
markets � with converted/predominant(3) rates ranging from 70%
to 90%.
We remain confident that IQOS will be launched in the U.S. The
two-day hearing before the U.S. Food & Drug Administration�s (FDA�s)
Tobacco Product Scientific Advisory Committee (TPSAC) in January
2018 was a very significant milestone in the history of tobacco control.
The hearing showed vividly that a tobacco company that has done its
homework can and should contribute to the regulatory process. Also
encouraging were comments that well-known, independent experts
submitted in support of our Pre-Market Tobacco Authorization (PMTA)
and Modified Risk Tobacco Product (MRTP) applications.
Following the TPSAC meeting, we responded to many FDA queries
related to the applications. We also submitted to the FDA our sixmonth Exposure Response Study (see below) and A/J Mouse Lung
Cancer Study, which completed our submission of the MRTP scientific
package.
With respect to other RRP platforms, we made further product
development and commercialization progress. In the U.K., we launched
the next generation of our Platform 4 e-vapor IQOS MESH product,
which employs a new proprietary aerosolization engine, and initial
results were promising. We concluded the small-scale city test of our
Platform 2 heated tobacco product, TEEPS, in the Dominican Republic
and are enhancing the platform based on our learnings. Finally, we
also advanced the development of our Platform 3 nicotine-containing
product and will initiate a clinical product use study.
The foundation of RRP commercialization efforts is PMI�s
�innovation machine,� which includes unparalleled expertise in critical
areas, such as technology, engineering, product development, quality
assurance, scientific substantiation, design and user experience, brand
retail, human insights and behavioral research, digital solutions and
experience marketing, and responsible business practices. This allows
us not only to respond to consumer needs, but also anticipate them
and proactively create solutions.

Scientific Assessment, Engagement and
Research & Development
Our scientific assessment program, outlined at www.pmiscience.com,
made substantial progress last year across all four RRP platforms.
Most notably, for IQOS, our six-plus-six-month Exposure Response
Study, which included nearly 1,000 participants, was completed at the
end of 2017, and the study report and results for the first six-month
term were submitted to the FDA in the second quarter of 2018. The
study was designed to answer important questions about the effects of
IQOS in actual use by adult smokers who switch to it. Results showed
that all eight clinical-risk endpoints improved in smokers who switched
to IQOS by the end of the six-month study and that the majority of
these endpoints (five of the eight) showed statistically significant
differences between smokers who switched to IQOS compared with
those who continued smoking.
The communication of our science remains paramount. In 2018, we
published 41 peer-reviewed scientific articles and presented at more
than 100 scientific and medical conferences.
Regarding research and development, we increased our RRP-related
intellectual property portfolio. Development activities are evidenced by
the approximately 4,600 patents granted worldwide as at the end of
2018, with approximately 6,300 additional patent applications pending.
As competition in the RRP space intensifies, patent generation and
defense have become increasingly critical. We substantially enhanced
our related capabilities in 2018 and were successful in addressing
numerous infringements.
Manufacturing & Supply Chain
Following IQOS-related supply limitations in 2017, first on heated
tobacco units and then on devices, we were able to fully address
market demand by the end of the first quarter 2018. We added
a second global device supplier and additional manufacturers of
components. We also increased heated tobacco unit production
capacity, thanks to the installation of new production lines as well as
greater efficiency and lower wastage.
In 2018, we laid the foundations for the transformation of our
broader value chain, focusing on delivering financial value with the
agility required by our dynamic business and developing the capabilities
critical to future success. This has already led to major cost efficiencies:
lower tobacco leaf costs through a better sourcing footprint, factory
headcount reductions through manning and shift-pattern changes, a
simplified combustible tobacco portfolio, and improved metrics for
material waste, tobacco yield and labor productivity.
The Organization
The transformation of our organization to deliver on our RRP ambitions
is a major undertaking. It requires unprecedented consumer focus,
a vastly higher degree of project complexity, and expertise in life
sciences, consumer electronics, real-time communication, own retail
and customer care.
In 2018, we established Global Communications as a separate
function to reinvigorate outreach regarding our vision of a smoke-free
future. Our overarching objective is to be present at some of the most
important milestone moments where public opinion and policy come
together. Through participation in major global events, we opened a
dialogue with world leaders about a shared agenda of harm reduction
and global health.
A critical 2018 organizational initiative was the design of an
operating model aligned with the entire IQOS consumer journey -
from awareness to advocacy. This new consumer-centric organization
model, piloted in a selected market, is being rolled out, implementing
real-time consumer feedback measurement systems, new roles and
accountabilities, new organizational structures and ways of working to
drive consumer focus. We measure the quality of consumer experience
across all channels at each stage of the journey. Our significant
investment in digital infrastructure and related capabilities is central to
this greater focus on the consumer.
As we transform the company, we continue to focus on the entire
operating cost base, targeting over $1 billion in annualized cost
efficiencies by 2021. Key initiatives include the shift to a project-based
organization model and a new zero-based budgeting program. To
support net revenue growth, some of the related cost savings will
be reinvested.
We remain committed to building an inclusive culture globally and
enabling a diverse mix of talented women and men to achieve their
full potential. To this end, an ongoing priority has been to improve our
gender balance. Last year, we increased the worldwide representation
of women in management to 35% and are on target to reach our goal
of 40% by 2022. We continue to aim to recruit an equal number of
women and men and to make sure that all have equal opportunities
to develop and advance. In 2018, women accounted for 40% at a
managerial level and 47% at lower levels. In addition, 38% of those
promoted were women.
We continue to benefit from the tremendous experience of our
Board of Directors, whose relationship with management is governed
by transparency, openness, trust and collaboration. In 2018, the Board
welcomed Lisa Hook, whose expertise in technology will be a valuable
resource to support PMI�s business transformation.
We were deeply saddened by the passing of Board member Sergio
Marchionne last year and former Board member Harold Brown earlier
this year; Mr. Brown had retired from the Board in October 2018.
Both had served as Directors with distinction since our transition to a
public company in March 2008. They brought invaluable business and
strategic insights to PMI�s Board, and we benefited tremendously from
their dedicated service. They will be sorely missed.
The Year Ahead
The underlying strength of our combustible tobacco business remains
intact, and there is the unprecedented opportunity for reducedrisk products to accelerate business growth and generously reward
shareholders. Our transformation to a smoke-free future allows us
to play a pivotal role in improving the lives of adult smokers, while
securing the long-term future of our company and the sustainability of
our earnings and dividend growth for years to come.
Seizing this opportunity requires us to complete the current stage
of our internal transformation and continuously improve thereafter to
deliver excellence to consumers. Although the task is enormous, we
are confident that the talented and dedicated people of PMI will rise
to the occasion.
Andr� Calantzopoulos,
Chief Executive Officer
March 8, 2019
Louis C. Camilleri,
Chairman of the Board