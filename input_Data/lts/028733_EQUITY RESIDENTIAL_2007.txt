
I am pleased to report that we delivered solid financial performance in 2007. We
knew that the apartment industry's impressive gains in 2006 would be difficult to
beat, yet we had high expectations as the year began, with good job and economic
growth across our core markets. As we all now know, after a strong start the economy
cooled and job growth slowed across the country. Nevertheless, our team of property
management professionals delivered same-store revenue growth of 4.3%, same-store
expense growth of only 2.1% and same-store net operating income (NOI) growth of
5.6%. Our FFO (funds from operations, the REIT equivalent of earnings per share) was
$2.39 per share, above the high end of our expectations.
This strong operating performance allowed us to reward our shareholders with a
4.3% dividend increase for a current annual distribution of $1.93 per share.



Technology is transforming the management and operations of our business. Our
state-of-the-art operating system, which we began to implement in 2006, has put
impressive tools for maximizing revenue, controlling expenses and relaying crucial
real-time information at our managers' fingertips. Now fully seasoned, the system will
deliver tangible results in 2008, through more strategic pricing, streamlined purchasing
and opportunities for increased customer loyalty.
The management "dashboard" of real-time activity means property personnel are able
to respond to resident requests quickly and more efficiently. Our maintenance teams
are now rewarded for hitting specific benchmarks that correspond to key criteria
like great move-in experiences and efficient resolution of service requests. Having
immediate access to everything from occupancy and unit availability to renewal
conversions and make-ready status gives our managers the information they need to
optimize on-site activities and maximize operating cash flow.
One area we're particularly excited about is how we communicate with our residents
and, more importantly, how they communicate with us. Our residents can now go
online to pay rent, renew leases, request service or just tell us what's on their minds
at our website. We've implemented an online surveying tool that helps track each
resident's experience, providing data to compute customer loyalty scores for each
property. The information we gather also allows us to target our services to the specific
wants and needs of each resident.
We also have upgraded our proprietary website, equityapartments.com. Generating
30,000 unique visitors each day, the site is one of our best sources for new leases. As a
result, we have reduced leasing and administration costs by 27% in the last two years.
In 2002, we made the strategic decision to concentrate our capital investments in
fewer, higher-quality assets located in metropolitan areas we believed would deliver
the best long term returns to our shareholders. Since then we have significantly
transformed our portfolio from a peak of some 225,000 apartment units in more than
50 markets across the country to our current portfolio of 150,000 apartment units in
20 core markets. Each of those markets is expected to significantly outperform national
norms in job and household growth for many years to come.
We continued to execute this strategy in 2007 by selling 73 properties in markets like
Houston, Milwaukee, Minneapolis and Nashville, for approximately $1.9 billion. We
reinvested these proceeds into exceptional assets in New York City, Seattle, Southern
California, Northern California and Florida.
At this point we have nearly completed our shift to high-barrier, high-growth markets,
with only $2 billion of assets remaining to be sold. We will continue to dispose of assets
this year, provided we can achieve pricing that meets our expectations. Thanks in part
to liquidity available to buyers from Fannie Mae and Freddie Mac, we continue to find
strong demand for our assets despite what is happening in other real estate segments.
Our team is in the market every day, valuing assets for both purchase and sale. Up to
this point, we have not seen a dramatic change in values or capitalization rates. As long
as this is the case, we will continue to execute our strategy to complete the portfolio
transformation on your behalf.



One of the most important competencies we've established over the past five years
is in developing assets from the ground up. Not only has development been hugely
successful in creating value for our shareholders, but it has played a key role in our
portfolio transformation process. In 2007, we broke ground on $573 million in assets
and stabilized three great properties  in Los Angeles, Washington, D.C. and California's
Inland Empire  at a yield of 7%. In 2008, we expect to continue with selective
opportunities in our development pipeline. As always, we will act prudently, this
year exploring joint ventures where third-party capital can be used to share funding
obligations. This will allow us to fund the development pipeline while maintaining
our very important liquidity position  keeping our powder dry, so to speak, in very
uncertain times.



We look at our condominium conversion business in much the same way as new
development. Our experience and expertise allow us to make significant profits for our
shareholders when the market is right. We have structured this business to expand or
contract our activities in response to market conditions, including retaining a team of
experts in the "nuts and bolts" of the business. While condominium conversions remain
a viable strategy long term, we will reduce our investments in this business in 2008,
yet remain prepared to take advantage of opportunities that will likely arise as markets
continue to evolve.
Stock market performance was disappointing for real estate companies in 2007, with
the long trend of positive capital flows to the real estate sector reversed for much of
the year. Equity Residential experienced total shareholder return of -24.7% for 2007.
During the year we saw a major disparity between what we knew to be the true value
of our real estate assets, as reflected by actual property acquisitions and dispositions,
and the implied value being assigned these assets by the public markets through our
common share price.
As a result, our Board of Trustees authorized us to buy back our stock.
We acquired approximately $1.2 billion of common shares during 2007. Property
dispositions of approximately $1.9 billion were sold at capitalization rates and prices
per unit and square foot that supported this buyback decision. We continue to believe
strongly in the underlying value of our portfolio, which consists of assets in some of
the country's most coveted locales, including the New York metro area, San Francisco,
Southern California and Seattle. And while we have approximately $469 million
available under our share buyback program, maintaining valuable liquidity at a very
uncertain time in the capital markets would likely take precedent over further share
repurchase.



Equity Residential has always thrived on opportunity, with access to capital being the
key to seizing opportunity. There is currently widespread uncertainty in the capital
markets, with liquidity a significant issue in most quarters. I am pleased to report that
we acted swiftly in 2007 to strengthen our balance sheet when credit markets began
to act unpredictably. We increased availability under, and extended the term of, our
$1.5 billion unsecured credit facility early in the year. In addition, we issued $1 billion
of unsecured debt at very favorable terms before the credit meltdown began in the
summer. Later in the year we completed a $300 million secured financing and a $500
million unsecured term loan.
Most recently, in anticipation of debt maturities towards the latter half of 2008, we
closed a $500 million secured loan. As a result, we have pre-funded all of our debt
obligations for 2008 and should finish the year with more than $1 billion available
on our credit facility. It is far better to take advantage of the market than have the
market take advantage of you.



The organizational success we have achieved comes only with the efforts of very
talented people. And our future success will require both great leaders and outstanding
people working with them, ready to step up when opportunity knocks. At the end of
2007, Gerry Spector, our longtime Chief Operating Officer, elected to retire. Not only
was Gerry instrumental in our success but he is a wonderful guy and a great friend
to us all. We will miss our daily interaction with him. Fortunately, he didn't go very far
since he remains on our Board of Trustees as Vice Chairman and will continue to be
an excellent sounding board for our senior management.
Gerry's retirement was part of our very important succession planning process.
We were fortunate to have two highly experienced and longtime Equity executives
willing to relocate to our Chicago headquarters to work closely with Gerry for several
years in anticipation of this changing of the guard. Fred Tuomi and David Santee are
doing a terrific job running all aspects of property management, operations, revenue
management, marketing and IT. During the year we also saw our CFO, Donna Brandin,
leave us. We thank Donna for her contributions and are very pleased that Mark Parrell,
formerly Senior VP and Treasurer, answered the call to become our CFO.



Our mission  to be America's choice for apartment living by being uncompromising in
delivering on our commitments to our shareholders, our residents and our employees 
is a durable one. It reminds us everyday what we need to do and why we need to do it.
Our outlook for 2008 remains positive and our expectations are high. Despite ever-
changing economic conditions and a great deal of market volatility, we expect our
financial strength, excellent portfolio of assets, improved operational tools and wealth
of seasoned, experienced real estate professionals to drive Equity Residential's success.
Today, the fundamentals of our business remain in good shape despite an uncertain
economic picture. Even with slowing job growth, our properties are currently 95%
occupied. Residents are staying in our units longer due to problems in the single
family home market. More renter households will be created as the single family home
ownership rate continues to decline. There is very little new apartment supply opening
in our core markets. And the demographic picture is very favorable for apartments.
Nearly four million members of the "Echo Boom" generation will turn 18 years of age
every year for the next decade, increasing the population of young people with a
very high propensity to rent apartments.
In short, we have the best people, the best systems, terrific assets in very desirable
locales and loyal residents. We are excited about our future and we will perform.


My best to you,




David J. Neithercut
President and Chief Executive Officer
April 11, 2008
