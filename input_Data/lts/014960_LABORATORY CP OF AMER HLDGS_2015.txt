Dear Fellow Stockholders,
The Este Lauder Companies delivered another year of strong fi nancial performance in
fi scal 2015, demonstrating the power of our business model, the diversity of our portfolio
and the strength of our execution. Our results were fueled by our multiple engines of growth
across brands, demographics, geographies and channels, signifying our proven resiliency
and agility, even against the backdrop of challenging market conditions in some areas.
We are continuing to build on our beautiful momentum by leveraging our multiple engines
of growth, combined with our brand building capabilities and our unrivaled creativity and
innovation, all of which have enabled us to deliver consistent, sustainable results for stockholders.
In fi scal 2015, we generated adjusted net sales of $11.0 billion, adjusted net earnings of
$1.18 billion and adjusted diluted earnings per share of $3.05. The strengthening of the
dollar relative to other currencies had an adverse effect on our results. In constant currency,
net sales rose 6 percent and diluted earnings per share increased 12 percent.* Cash fl ow
from operations increased 27 percent to $1.9 billion, providing ample resources to invest in
our business, while returning substantial cash to our stockholders. During the fi scal year,
we raised our dividend rate by 20 percent and repurchased nearly $1 billion of our
outstanding shares.
We are keenly focused on long-term value creation as we proactively manage our business
to remain well-positioned to generate impressive near-term results. Our 10-year Compass, a
vital long-range planning tool, helps us to anticipate emerging trends and direct our
investments appropriately. Consumer shopping behaviors and habits are undergoing a
profound transformation, driven by changing technology and distribution, the increasing
infl uence of the Millennial demographic and the continued spending power of the Ageless
consumer. Through skillful planning and strategic decision making, we are well-positioned
in this dynamic environment.
MAINTAINING CONSISTENCY IN
A DYNAMIC GLOBAL ENVIRONMENT
In fiscal 2015, we sustained our momentum by proactively managing our approach to successfully
anticipate and navigate through global uncertainties, including significant movement in key
currencies and slower consumer spending in China.
We have been able to nimbly and quickly shift our resources to the brands, regions and channels
that are demonstrating the best opportunities and strongest momentum. This agility helped us to
grow net sales at a pace higher than global prestige beauty. With one-third of our brands, markets
and channels growing at double-digit rates, the rest simply needed to grow overall in line with the
industry for us to outperform.
During the year, we benefitted from, and increased our investment in, several high potential areas of
our business. These included our fast-growing online and mobile offerings, which are a critical part
of our broader channel strategy. Our Online metrics are truly impressive: nearly all of our brands
experienced strong double-digit growth in this channel, with the largest contributions from MAC,
Este Lauder and Clinique. We also continued to see strong double-digit growth Online across all
regions, including our most developed countries, the United States and the United Kingdom.
Additionally, we continued to expand strategically in emerging markets. For example, we capitalized
on the global popularity of MAC, using freestanding MAC stores as an entry point into countries
with less developed prestige retail. This strategy delivered excellent results, contributing to a 71 percent
increase in MACs business at retail in Brazil in fiscal 2015, despite the economic downturn in that region.
OUR COMPASS
Our areas of focus and deployment of capital for sustainable growth are based on a rigorous strategic
process that is informed by our Compass. The Compass provides a vital source of consumer and
market trends and helps identify the fastest-growing areas in prestige beauty that will provide the
greatest opportunities.
Through the development of our Compass, we saw early on the mega trends that led to our
investments in direct-to-consumer and specialty-multi retail capabilities. It has also helped us spot
key product trends, such as growing consumer interest in subcategories like facial masks. This
resulted in the successful introductions of a range of masks at Origins and the expansion of this
subcategory at La Mer, where we doubled the size of our mask business during fiscal 2015. Our
recent acquisition of GLAMGLOW, the popular Hollywood skin care brand focused on fast-acting
skin care masks, gives us the opportunity to lead in this growing subcategory. In addition, artisanal
and luxury fragrances with a focus on craftsmanship and quality are anticipated to grow faster than
the rest of the fragrance category, and we are well-positioned to lead this trend through our other
recent acquisitions.
The Compass also helps us decide where to allocate capital by category, market and brand. For
example, we made the strategic decisions to move into China, now over 6 percent of our total sales,
and accelerate MAC and Jo Malone London in markets such as Latin America and Asia based on
findings included in our Compass. These decisions are generating strong and profitable growth.
The Compass keeps our boat in the winds of growth, enabling us to focus on delivering strong nearterm
results, while also laying the groundwork for sustainable, profitable growth over time.
POWERFUL GROWTH IN MAKEUP
AND LUXURY BRANDS
Many of our growth engines contributed to our strong performance in fiscal 2015, reflecting the
depth and breadth of our business, and the strength of our balanced portfolio, which enables us to
remain agile and nimble.
We are the global leader in prestige makeup, which continues to be one of our most compelling
opportunities. Our brands were well-positioned to benefit from the exceptional growth of the
category, where our sales increased 10 percent in constant currency during the year, adjusted for the
accelerated sales orders, led by MACs continued extraordinary performance.
We saw strong growth in lipstick sales across our brands, with products such as Este Lauders Pure Color
Lip Envy, Tom Ford Beautys Lips & Boys collection and MACs Ruby Woo lipstick leading this trend.
Our most aspirational luxury brands were also standout performers this year, with sales from Tom Ford,
La Mer and Jo Malone London showing strong growth. La Mer, one of our luxury skin care brands, grew
retail sales by double-digit rates in fiscal 2015. La Mers Treatment Lotion is the most popular SKU in
Asia in our distribution, with new marketing assets driving sales and recruiting new consumers.
GROWTH IN HERITAGE MARKETS:
THE UNITED STATES AND THE UNITED KINGDOM
We have generated growth in our heritage markets through our ability to be agile and nimble and, in
fact, approach them like emerging markets: by identifying and leveraging our strengths in the fastest
areas of growth. Using our proprietary consumer insights and cutting edge market research, we are
strategically deploying our brands, and tailoring product assortments and communications to fit local
tastes and preferences in cities and neighborhoods with ethnic diversity.
In the United States, which accounts for approximately 37 percent of our sales, we continued to
build our business in fiscal 2015 by concentrating our resources on the most populous and
prosperous states, including California, Texas and Florida, with products, brands and distribution
strategies tailored to the unique characteristics of each market. Further, we are investing in and
testing various omnichannel offerings to improve service and increase loyalty in the United States
and other established markets.
Our team in the United Kingdom, our second largest market, is producing remarkable results, with
net sales in constant currency growing double-digits, by directing our resources and attention to
secondary cities and brands and products with the greatest potential. As one example, our team
recognized an opportunity to leverage the convergence of hospitality and art in retail, and launched
a Smashbox shop in Box Park Shoreditch, a pop-up mall with fashion and lifestyle stores and events
in East London, to better reach consumers directly and provide them with an immersive experience.
In fiscal 2015, the talented teams at our heritage Este Lauder and Clinique brands continued to work
hard to reignite growth, beginning new, exciting programs to achieve this objective over the next
year. Both brands delivered breakthrough launches that filled white space opportunities for us. For
example, in summer 2015, the Este Lauder brand introduced New Dimension, a revolutionary
franchise of transformative skin care and makeup products that create visible definition for a
beautiful, more contoured looking face.
Este Lauder is continuing its efforts to modernize its image by reaching out to a wider demographic,
including the Millennial generation. Initiatives include collaborating with Millennial influencers
around the world, producing branded content across their social media platforms and introducing
Kendall Jenner as a new spokesmodel. Kendall is the number one model across social media, and
she is helping us to introduce Este Lauder to a whole new generation of consumers.
Clinique is also attracting new consumers through fun and fresh product offerings in makeup.
Clinique introduced Beyond Perfecting Foundation and Concealer, a new product with an innovative
applicator in the strong subcategory of foundation, where the brand leads in the United States. Also
in the makeup category, Clinique introduced Clinique Pop Lip Colour and Primer, a fun and colorful
draw for consumers, just as the popularity of lip products exploded.
GLOBAL GROWTH:
CONTINUED SUCCESS IN EMERGING MARKETS
We see significant, continuing opportunity in emerging markets, which are expected to comprise
approximately 25 percent of the industrys global retail sales within the next decade. Geographic
diversity and balance is an important part of our strategy to minimize volatility and enhance the
consistency of our performance. We are engaging local consumers by emphasizing products and
services with the greatest local appeal. Our insightful marketing encourages brand loyalty among a
new generation of aspirational consumers across these countries.
Emerging markets contributed significantly to our performance, with sales in fiscal 2015 representing
approximately 14 percent of our total business.
We continue to view China as a significant long-term growth opportunity and an important building
block of our strategy. We are investing significant resources to reach Chinese women who, on
average, use seven products in the morning and another seven at night in their beauty routines
many more than most consumers in the Western world. We are focused on becoming an even more
important part of the beauty rituals of an increasing number of Chinese consumers by launching
more brands in China and expanding distribution, with the potential of growing our presence from
100 cities currently to more than 300 over time.
Our growth in emerging markets is balanced across geographies. Sales in emerging markets,
excluding China, rose 26 percent in constant currency, led by Turkey, Brazil and South Africa.
Combined, our presence across these and other emerging markets represents an even larger portion
of our business than China itself. These countries represent wellsprings of opportunity as economic
improvements increase the purchasing power of a rising middle class.
We recognize that many consumers from emerging markets are introduced to our brands while
traveling or abroad. Travel Retail continues to be an important cornerstone of our strategy to reach
these consumers, particularly as destinations and traffic patterns shift. This year, we accelerated our
growth in certain limited-distribution brands in Travel Retail and will continue to add distribution
points and invest in this channel to help fuel our future growth.
CREATING THE NEXT GENERATION
OF ICONIC BRANDS
We have a successful history of collaborating with talented, inspirational founders of prestige beauty
lines to build their brands into game-changing global enterprises. By preserving what is special and
unique about these brands, while leveraging our global distribution, creative resources and
operational expertise, we are able to help these entrepreneurs realize their vision and create new
avenues of growth for our Company.
Our track record is spectacular: MAC, Bobbi Brown, La Mer and Jo Malone Londonall significant
value-creating acquisitions from the 1990sare helping to power our momentum today. For
example, since we acquired Bobbi Brown in 1995 and completed our acquisition of MAC in 1998,
sales of each brand has risen at a compound annual rate of nearly 25 percent.
We are extremely excited about the potential of the new brands we welcomed into our portfolio in
fiscal 2015. Each one strengthens our presence in subcategories poised for strong growth with
established brand equity and loyal consumers. GLAMGLOW increases our skin care offerings,
particularly in masks that help give skin a gorgeous, spotlight-ready glow, while Editions de Parfum
Frdric Malle brings us a collection of elegant, sophisticated ultraluxury fragrances with European
provenance. Le Labo adds artisanal fragrance and lifestyle offerings with an emphasis on fine
craftsmanship and personalization, and RODIN olio lusso provides our Company with another
authentic offering in the fast-growing luxury facial oil subcategory.
These new additions are important investments in our future and serve to enhance our already
diverse portfolio of powerful brands.
ADAPTING THE WAY
WE COMMUNICATE AND ENGAGE
Consumer behaviors and shopping preferences have reached a fascinating tipping point, and we are
well-equipped to respond and deliver in this dynamic landscape. One of the largest and most
significant forces transforming our industry is the rise of the Millennial consumer. Born between 1980
and 2000, Millennials are now the largest demographic group not only in the history of the United
States, but in the world. Their spending power in the United States alone is expected to reach nearly
$3.4 trillion by 2018.
These savvy consumers are widely considered to be the first digital natives, and expect a seamless
shopping experience across the brick and mortar, online and mobile worlds. For them, lines between
information, communication, entertainment, commerce and socializing do not exist. They instantly
share their perspectives and seek advice not only from friends, but from an ever changing and
expanding base of experts and style leaders on subjects they are interested in, particularly when it
comes to beauty. As a result, they are continually experimenting and searching for new experiences,
while also seeking out authentic brand stories and pursuing individualized brands. Our ability to
meet their desires for new experiences and rapid innovation provides an important advantage in
establishing strong connections with the Millennial consumer.
In many ways, the Millennial generationand their shopping habits and preferencesis changing
the expectations and behaviors of other generations. The very same dynamics that drive a Millennials
approach to the worldtechnology, interconnectedness, social engagementare shaping how we
all think and live, so our Company is focused on delivering High-Touch experiences across all generations.
For example, to meet the needs of our important Ageless consumer, we are emphasizing her bestloved
products and brands, and innovating to meet her evolving needs with the same High-Touch
approach to service in the channels where she shops. By 2017, approximately half of the United
States population will be 50 years or older and will control 70 percent of the nations disposable
income. Globally, the spending power of the Ageless group will reach $15 trillion by 2020. Women
in this category, particularly in our heritage markets, are willing to indulge themselves, which opens a
number of opportunities for engagement across our brands. We are innovating to meet their needs
by developing products tailored perfectly for them.
OUTLOOK: BUILDING ON
OUR BEAUTIFUL MOMENTUM
Global prestige beauty is dynamic, highly competitive and undergoing significant changes. We
delivered strong results in fiscal 2015 and demonstrated that our strategy is working precisely because
we anticipated many of the opportunities stemming from this environment. We have proven that we
are able to successfully navigate global challenges and leverage our strengths in the fastest-growing
areas. And, importantly, we believe we still have a great deal of runway for future growth.
There is enormous untapped potential around the globe and across our diverse brand portfolio, and
we are using our Compass to identify the areas of greatest opportunity. We are deepening our presence
in emerging markets, where disposable income among women is increasing. In heritage markets, we
continue to revitalize and accelerate our growth, leveraging new tools to allocate our resources
more effectively. We are continuing to innovate and add to our portfolio of exceptional brands.
Combined, these efforts position The Este Lauder Companies for long-term, sustainable growth
and increasing profitability. We are focused on continual improvement in the way we do business to
further increase efficiency and agility. In sum, we believe we are well-positioned to continue the
beautiful momentum we have created.
This is truly an exciting time to be a major player in global prestige beauty. The industry is forecast to
grow again by 4 to 5 percent over the next few years. Our goal remains to grow at least one
percentage point faster to deliver topline growth between 6 and 8 percent annually in constant
currency. In order to achieve this, we are actively investing in the fastest-growing areas of our
industry. We expect to leverage our sales growth through greater productivity and cost savings and
efficiencies from our Strategic Modernization Initiative program. We expect to reinvest some of the
savings in capabilities and growth drivers, including digital, R&D, retail store expansion and
information technology, while maintaining our goal of double-digit EPS growth in constant currency.
Improvement in our inventory management is expected to drive growth in free cash flow at a faster
rate than earnings, giving us more flexibility for acquisitions and returning cash to stockholders
through dividends and share repurchases.
Our outstanding performance this year, and over time, is supported by the following foundational
assets of our company:
First, the Lauder family. We are a public company built upon the strong foundation of family heritage
and values. The Lauder family members dedication to preserving the legacy of our Company
underlines our deep commitment to ensuring long-term sustainability and growth, and I am proud
and privileged to partner with them to continue to build this great organization.
Second, our people. Our success is the direct result of the talented team we have assembled and
with whom I look forward to working every day. Our management team defines what it means to be
world class and is supported by our extremely capable and talented employees around the globe.
We are committed to the continuous development of all our employees at the Company, and
encourage leadership from every chair.
I also thank our Board of Directors for their wise counsel and stewardship of our Company, which
have been essential to setting and maintaining our strategic course.
And importantly, I am grateful to you, our valued stockholders, for your continued support and
confidence.
I believe in the sustainability of the beautiful momentum that The Este Lauder Companies
demonstrated in fiscal 2015 and over the past several years. It is a true honor to lead this fantastic
company, and I look forward to working with all of the great people associated with it to continue
our progress.
