To the Shareholders and Employees
of The Boeing Company
Inspired by our 2016 centennial,
we have a bold vision for the
futurean invigorated sense
of what we can accomplish as
one team, One Boeing. Our
collective and abiding passion
for innovation, integrity and
technical excellencealong with
our financial strengthis driving
exciting new breakthroughs and
expanding opportunities for us
and our customers to connect,
protect, explore and inspire the
world. Its our turn to imagine
what comes next and bring it to
lifeand we are up to the calling.
2016 in Review
Thanks to the hard work, dedication and ingenuity
of more than 148,000 employees and our U.S.
and global partners, Boeing extended its broad
aerospace market leadership in 2016 by delivering
on customer commitments, enhancing the safety,
quality and affordability of our work, and investing in
our people, products and services while returning
significant value to shareholders.
During the year, we generated strong revenue, solid
core operating performance and record cash flows.
We also redoubled our focus on profitable long-term
growth strategies, disciplined execution of our production
and development programs, expanding our
services business, and accelerating further quality
and productivity gains across the enterprise.
Total company revenue in 2016 was $94.6 billion,
driven by strong commercial airplane deliveries, services
growth and solid performance in our defense,
space and security business. We also won $76 billion
in net new orders for our market-leading products
and services, reinforcing our strong companywide
backlog of $473 billion.
Core earnings per share of $7.24* for the year
reflected solid core operating performance companywide,
offset by the reclassification of costs for two
787 Dreamliner flight-test airplanes, the impact of
decisions on 747 production rates, and higher costs
on the KC-46 tanker and CST-100 development
programs. Core operating earnings were nearly
$5.5 billion*, and operating cash flow increased to a
company-record $10.5 billion.
Following through on our balanced cash deployment
commitment, we repurchased 55 million shares of
Boeing stock for $7 billion and paid $2.8 billion in
dividends in 2016. In December, as a further sign
of confidence in our long-term outlook, the Boeing
board of directors authorized a new $14 billion share
repurchase program and increased the quarterly
dividend by 30 percent.
Boeing Commercial Airplanes generated $65 billion
of revenue in 2016 and led the industry in
deliveries for the fifth consecutive year with 748 airplanes.
Booking 668 net new airplane orders worth
$49 billion sustained a robust backlog of more than
5,700 airplanes valued at $416 billionmore than
seven years of production at current rates. Full-year
operating margins were 4.8 percent.
In the high-value twin-aisle airplane segment, we
extended our market and technology lead with key
milestones and steady progress on the 787 and
777 programs. We delivered a record 137 787s from
our production centers in Everett, Wash., and North
Charleston, S.C., including the 500th Dreamliner
overall, making the 787 the fastest twin-aisle jet in
history to hit that mark. Nearly 50 customers now
operate the 787 Dreamliner, and its game-changing
combination of range, size and fuel efficiency has
opened more than 130 new nonstop city-pairs that
previously were unserved by nonstop flights. The 787
stands on a strong foundation for long-term production
with a backlog of 700 orders and solid ongoing
demand. Flight testing of the 787-10, our third and
largest Dreamliner model, will begin early this year,
with first delivery in 2018.
Our next new twin-aisle airplane, the 777X, remains
on track to enter service in 2020. In 2016, we opened
new production facilities for the program in Everett,
Wash., and St. Louis, Mo., and began manufacturing
components for pre-production testing. With a current
backlog of 340 orders and commitments from
seven customers, including a major win at Singapore
Airlines announced in February 2017, the 777X is
off to a very strong start, and customer interest
remains high.
The 737 family continues to perform well in the highly
competitive single-aisle market, with net new orders
and deliveries of 550 and 490 airplanes, respectively,
in 2016. Total program backlog is robust at more than
4,400 airplanes. Development and flight testing of
the new 737 MAX has exceeded expectations and
allowed us to accelerate the planned first delivery of
this fuel-efficient new airplane by the second quarter
of 2017. Last year, we built the first eight 737 MAX
production aircraft and completed service-ready
testing with launch customer Southwest Airlines.
With more than 3,600 orders from 83 customers
around the world, the MAX is the fastest-selling
commercial airplane in Boeing history, and we
continue to see capacity for additional orders as
large 737 operators announce their fleet expansion
and replacement plans.
Despite a softening of near-term widebody demand
that emerged in 2016 and our subsequent decisions
to de-risk the 747 program and 777X transition with
corresponding rate reductions, we remain strongly
positive about our commercial airplanes business.
We are on track to steadily increase production over
the next five years to more than 900 airplanes annually,
based on previously announced rate increases.
The 737 production rate will rise from the current 42
airplanes per month to 47 in the third quarter of 2017,
followed by 52 per month in 2018 and 57 per month
in 2019. On the strength of its commercial freighter
and military tanker backlog, 767 production is slated
to increase to 2.5 per month from 2 per month in the
fourth quarter of 2017. 787 Dreamliner production
increased to 12 per month in 2016, with plans to go
to 14 per month later this decade.
At planned production rates for the current 777,
including going from seven to five per month in
August 2017, delivery slots are nearly sold out for this
year, and we are about 90 percent sold out for 2018
and 2019 when we begin phasing in 777X production.
On the 747 program, while we are encouraged
by a modest improvement in the air cargo market and
a 2016 order from UPS for 14 747-8 freighters with
options for an additional 14 airplanes, the program
will remain at 0.5 per month.
Overall, we continue to project strong demand in the
global commercial airplane market, with customers
expected to buy more than 39,600 airplanes over the
next 20 years, including 9,000 widebodies spurred
by a replacement cycle set to begin around 2020.
This unprecedented outlook, underpinned by our
near-record order backlog, planned production rate
increases and forecasted traffic growth, positions us
uniquely among our competitors and peers for significant
and sustained growth in the coming years.
Boeing Defense, Space & Security delivered
another solid year of performance in 2016, scoring
healthy revenue and margins, winning important new
orders and achieving significant program milestones.
Revenues were $29.5 billion, driven by strong deliveries
and services growth. Operating margins were
10.2 percent. In total, we delivered seven satellites,
178 military aircraft and 23,052 weapons systems
(a 46 percent increase over 2015). We also booked
$26.2 billion in new orders, maintaining a healthy
backlog of $57 billion, 37 percent of which is from
international customers.
Key defense-related accomplishments in 2016
included:
Completion of key KC-46A tanker refueling tests
that retired substantial technical risk and cleared
the way for receiving U.S. Air Force contracts for
19 airplanes.
Winning approval for significant international
orders for F-15 and F/A-18 fighter jets, and contracts
for P-8 maritime patrol aircraft and AH-64
and CH-47 helicopters.
First flight of the T-X trainer aircraft.
Rollout of the new Echo Voyager unmanned,
undersea vehicle.
We also extended our leadership in space with these
accomplishments:
Welding of Space Launch Systems liquid
hydrogen tank, the largest section of the rocket
that one day will carry astronauts to Mars.
Initial assembly and test of the first CST-100
Starliner flight-test spacecraft.
Launch of seven satellites.
Twelve successful launches by our United Launch
Alliance joint venture.
Solid demand for our diverse portfolio of products and
services and an unwavering focus on affordability
and productivity have positioned our defense, space
and security business for continued success as we
compete for a growing share of a $2.2 trillion market
over the next decade. With strong customer support
and global defense budgets that are trending
upward, we are making targeted investments in areas
such as commercial derivatives, rotorcraft, satellites,
services, human space exploration and autonomous
systems. These investments are helping us compete
for future programs such as the T-X trainer, JSTARS
recapitalization and numerous advanced weapons
and unmanned systems.
Boeing Global Services is the name of a new
business unit we announced in November 2016
to be formed from core capabilities within our
Commercial Aviation Services and Global Services
& Support groups, as well as various Boeing subsidiaries,
including Aviall and Jeppesen. It will begin
fully operating in the third quarter of 2017 and be
dedicated to providing more agile, cost-effective and
streamlined after-market support and services. It will
further advance our growth ambitions in a market
that is expected to total $2.5 trillion over the next 10
years in areas such as spare parts, modifications,
upgrades, and data analytics and other informationbased
services.
Setting Our Sights Higher
With an eye toward the future, we refreshed our
company vision in 2016 to express it in a simpler
and more compelling way for our people. We added
clear enterprise strategies and a more inspirational
purpose and mission: To connect, protect, explore
and inspire the world through aerospace innovation.
This is what drives us to do our very best for the passengers
flying aboard our commercial airplanes, the
men and women in uniform who rely on our products
and services to complete their missions safely and
the astronauts operating at the edge of space, and to
attract and retain the best team and talent the world
has to offer.
Yet, in our second century, we aspire not only to be
the best in aerospace, but also an enduring global
industrial championa top performer in every
aspect of our business, delivering superior value to
our customers, employees, shareholders, communities
and partners. This bold goal requires that we
setand achievehigher expectations for our work
and recognize that we now compete with companies
in other sectors for capital, influence, talent and
positioning in a business environment that is getting
tougher, more complex and more global every day.
These realitiesand the aggressive existing and
emerging competition we face in the aerospace
industrydemand that we relentlessly drive productivity
and affordability to win in our markets and fuel
our investment in the products and services that will
drive future growth.
To be considered among the worlds best-performing
companies, looking ahead to 2025, we aim to be
number one in commercial airplanes with differentiated
products and demand-driven rates; lead in the key
defense, space and security growth markets we are
targeting; and be top in services, growing our business
significantly with a focus on lifecycle opportunities
across multiple areas. And, we must consistently
deliver top-quartile business performance.
Our three enterprise strategies to achieve these goals
are to continue leveraging the unique competitive
advantage we have in operating as One Boeing,
build strength on our historic strengths to deliver on
our existing plans and commitments, and to stretch
beyond those plans by sharpening and accelerating
our pace of progress on key enterprise growth and
productivity efforts, such as engaging our supply
chain to reduce costs and improve quality, breaking
the cost curve on our development programs,
implementing advanced manufacturing and design
technologies, and creating global scale and depth
by growing our presence and partnerships in key
markets around the world.
Foundational to this effort to accelerate company
performance is further strengthening our culture by
improving the way we work together in support of
our customers and one another. We must adhere
to enduring Boeing values that begin with integrity,
embrace diversity and inclusion, and fuel mutual trust
and respect. We are making measurable progress
in areas such as simplifying our strategies, clarifying
roles and responsibilities of teams and individuals, and
improving our processes for developing, assessing
and rewarding our peoplelevers we believe will
unleash accelerated growth and productivity.
Our Communities and Environment
Community and environmental stewardship are longheld,
core values. We aspire to lead in both areas.
In 2016, Boeing and its employees and retirees
invested $163 million to help improve lives and build
better communities worldwide. We stepped up our
efforts to inspire the next generation of innovators,
dreamers and doers through science, technology,
engineering and mathfocused giving, volunteering
and mentoring. And we strengthened our commitment
to military veteransmore than 20,000 of
whom we proudly call our teammatesand military
families with new programs and expanded outreach,
including co-sponsoring the 2017 Warrior Games for
wounded and injured veterans and service members.
Examples of our commitment to the environment
begin with the 30,000 Boeing employees who chose
alternative commuting methods in 2016, reducing
vehicle emissions, noise and road congestion. Our
ecoDemonstrator program has tested more than
50 innovative technologies designed to improve the
environmental efficiency and performance of Boeing
products. And we continue to work with universities
and other organizations to advance sustainable aviation
products and technologies, like biofuel-powered
flight and electric propulsion. Also, despite increasing
aircraft production and expanding operations, we
continue to target zero growth in greenhouse gas
emissions, water use, hazardous waste (normalized
to revenue) and solid waste sent to landfills.
A Bold Vision and Bright Future
Our goals and aspirations are bold, but they are
achievable within a company that has dared to dream
big and deliver on those dreams for more than 100
years. The challengingand meaningfulwork
ahead of us begins in 2017 as we focus on ramping
up commercial airplane production profitably, continuing
to strengthen our defense, space and security
business, launching our integrated services business,
consistently delivering on our development programs,
driving world-class levels of productivity and performance
and developing the best team and talent in
the industry.
These and other actionsundertaken with an unwavering
commitment to excellence and the utmost
integrity in how we do our workwill position Boeing
for continued market leadership, sustained top- and
bottom-line growth and increased value for our
customers, employees, shareholders, communities
and partners.
It is my great privilege to lead the people of Boeing
who strive every day around the world to carry forward
our founders vision to forever and always build
something better. Few companies have achieved as
much or are positioned as well for continued success
as Boeing. I am confident that we have the right
people, products, services and strategies to realize
our bold vision and bright futuretogether.
Dennis A. Muilenburg
Chairman, President and CEO