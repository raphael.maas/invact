DEAR FELLOW SHAREHOLDERS


Although oil prices declined precipitously in
the fourth quarter of 2014 and the lower oil price
environment has continued into 2015, we have
unique flexibility in our business model to adjust
our capital spending in times of uncertainty
while maintaining our strong financial position.
As a result, in November 2014, we took a decisive
and proactive step by significantly reducing our
projected 2015 capital spending to $550 million,
or half of our combined 2014 capital spending
of $1.1 billion.
	 We are able to do this because of the
unique production and cash flow profile of our
oil and natural gas assets, which are almost
all either current or future carbon dioxide
enhanced oil recovery (�CO2 EOR�) projects.
As we demonstrated in 2014 by balancing our
capital expenditures and dividends with our
cash flow from operations, we are committed
to strong financial discipline, and we believe
that we can fund our 2015 capital program
and dividends with projected cash flow
from operations.
	 During this period of reduced capital
spending, we are leveraging the talents of our
dedicated workforce to enhance our current
asset base. At the end of 2014, we assembled
multi-disciplinary innovation and improvement
teams among existing company personnel and
commenced detailed evaluations of our fields and
operational performance to identify and improve
our operating efficiency and reduce costs. I am
pleased to report that these evaluations are going
well. The innovation and improvement teams
have presented many potentially rewarding ideas,
and we are starting to evaluate and prioritize
these ideas to work toward implementation.
In addition to our innovation and improvement
teams, we continue to explore ways to reduce
costs and increase efficiencies in everything
we do, and I expect to see additional
improvements in our cost control initiatives
as we move forward.
	 On the operational front, we view 2014 as
a year we can build upon. There were several
bright spots for Denbury as we delivered
average daily production of 74,432 barrels of oil
equivalent (�BOE�) per day in 2014. We increased
our average tertiary oil production to a new
record level of 41,079 barrels per day (�Bbls/d�)
in 2014, a 7% increase from average tertiary oil
production in 2013, primarily due to continued
field development and expansion of facilities in
our existing CO2 floods at Hastings, Heidelberg,
Oyster Bayou, Tinsley, and Bell Creek fields.
Considering our reduced capital spending plan
for 2015, we expect to maintain production
relatively flat with 2014 levels.
	 Denbury�s total estimated proved oil and
natural gas reserves at December 31, 2014, were
438 million BOE (�MMBOE�), of which 83% was
crude oil, condensate, and natural gas liquids,
77% was proved developed, and 49% was
attributable to Denbury�s CO2 EOR operations.
The net reduction of total proved reserves of
30 MMBOE during 2014 was primarily the result
of 2014 production. In 2014, we did not commence
any new floods, although additional phases of
existing CO2 floods were implemented, moving
proved undeveloped (�PUD�) reserves into proved
developed reserves, lowering our PUD percentage
from 38% of the total reserves at December 31,
2013 to only 23% at December 31, 2014. In future
years we plan to initiate a number of new CO2
floods, including at Webster, Conroe and Cedar
Creek Anticline (�CCA�) fields.
	 In the Gulf Coast region, Hastings, Oyster
Bayou and Heidelberg fields continue to show
solid production growth and improved reservoir
response. Tinsley Field production continues
to perform strongly, although we believe
production is at or near its peak. Additionally,
we are reviewing our mature tertiary fields with
our innovation and improvement teams and
are optimistic that we can mitigate some of the
production declines at those fields.

In the Rocky Mountain region, our CCA 2014
annual production was up 14% year-over-year,
primarily due to improved drilling efforts as
well as waterflood performance in mature areas.
Bell Creek Field tertiary production increased to
over 1,800 Bbls/d at December 31, 2014 compared
to 300 Bbls/d at the end of 2013, and we expect
production at this field to continue to grow
in 2015. Hartzog Draw Field production was
up slightly for the full year as a result of our
successful completion of five wells in 2014.
	 Our CO2 supply and transportation
operations continue to operate strongly, but we
continue to look for potential optimization areas.
In the Gulf Coast region, we used an average of
835 million cubic feet per day (�MMcf/d�) of CO2
(including CO2 captured from industrial sources)
for our tertiary activities during 2014. Thus far in
2015, we have completed the only well planned
at Jackson Dome this year and are awaiting
completion and flow tests, but early indications
show that this well will be productive. In addition,
our industrial source supply is expected to get a
boost from the gasification and carbon capture
systems at Mississippi Power�s Kemper County
Power Plant in the next 12 to 18 months.
	 In the Rocky Mountain region, we used an
average of 69 MMcf/d of CO2 during 2014 from our
combined sources at LaBarge and Lost Cabin. Our
innovation and improvement teams have been
looking at multiple options to solve the issues at
the Riley Ridge gas processing facility, including
determining solutions for the sulfur deposition
in the gas supply wells (together, �Riley Ridge�);
however, due to such issues, we do not currently
expect natural gas or helium production at
Riley Ridge to resume until 2016. We continue to
believe Riley Ridge will be the anchor source of
CO2 for our Rocky Mountain fields in the future.
	 As CO2 EOR is increasingly being viewed as
a complementary long-term strategy to reduce
carbon emissions from various current and
proposed industrial facilities, we continue to have
ongoing discussions regarding the transport or
purchase of CO2 volumes from existing industrial
plants of various types. Currently we are utilizing
over 2.5 million metric tons of CO2 annually from
industrial sources for our CO2 EOR operations
that may have otherwise been released into the
atmosphere. Based on information from the
EPA�s Greenhouse Gas Equivalencies Calculator,
this amount equals the annual greenhouse gas
emissions from over 500,000 passenger vehicles.
Our CO2 EOR process provides an economical and
technically feasible method to develop otherwise
stranded oil reserves with the added benefit of
associated CO2 storage.
	 On the financial front, we generated
approximately $107 million of adjusted cash
flow from operations in excess of our capital


expenditures and dividend payments in 2014,
demonstrating our commitment to strong
financial discipline, as both capital and lease
operating expenses came in under budget for
the year. We are seeing continued improvement
from our focus in 2014 on reducing costs, as
evidenced by four consecutive quarters of a drop
in lease operating expenses per BOE (excluding
Delhi Field remediation costs, insurance
reimbursements and unplanned Riley Ridge
workovers). Excluding those nonrecurring items,
fourth quarter operating costs averaged $22.64
per BOE, 14% lower than in the fourth quarter of
2013. In addition, we expect to receive significant
incremental cash flow from our hedges in place
for 2015 if the current lower oil price environment
persists for the remainder of this year.
	 In April 2014, we issued $1.25 billion of
5�% Senior Subordinated Notes due 2022
to repurchase and redeem our 8�% Senior
Subordinated Notes due 2020 and to pay down
approximately $150 million of outstanding
borrowings on our bank credit facility. In
December 2014, we amended and restated our
bank credit facility, which provides for aggregate
lender commitments of $1.6 billion and an
extended termination date of the facility from
May 2016 to December 2019. Together, these
transactions move our long-term debt maturities
further into the future while also allowing us to
lock in attractive rates and reduce our
out-of-pocket interest costs.
	 We declared quarterly cash dividends of
$0.0625 per common share during each quarter
of 2014, with aggregate dividends of $87.0 million,
or $0.25 per common share, paid during the year
ended December 31, 2014. As a result of the oil
price declines, in January 2015 we announced the
decision to keep our dividend payment flat for
the first quarter of 2015 at the rate of $0.0625
per common share, rather than increasing it as
originally planned.
	 On the share repurchase front, we bought
back a total of 12.4 million shares of Denbury
common stock for $200.4 million during the first
quarter of 2014. In November 2014, we announced
that our share repurchase program was being
suspended in order to protect our financial health
LETTER TO SHAREHOLDERS
and preserve liquidity amid a period of declining
oil prices and overall oil price uncertainty.
	 As we strive to make improvements
throughout our entire company, we are
cognizant of opportunities to better the lives
of our employees, our environment and our
communities. We believe that operating a
sustainable and ethical company is essential to
being a responsible corporate citizen, and have
detailed our efforts to earn this distinction in our
2014 Corporate Responsibility Report. This report
illustrates our commitment to these principles
and to transparency with our stakeholders
regarding our economic, environmental and
social performance. We encourage you to review
our report and provide us feedback so that we can
continue to address matters that are important to
our stakeholders.
	 We believe Denbury�s business model
is an excellent example of how to combine
technology, economics and science to take a
proven, safe process to a new level. We believe
our investments, our experience and our acquired
knowledge give us a strategic and competitive
advantage, and we look forward to continued
leadership in this arena for many years. We have
built a strong team of dedicated employees with
the skills and expertise to pursue our strategy,
and our results are directly attributable to their
efforts. We believe this dedication, paired with
the support of our shareholders, will allow us
to come out of this difficult economic period
stronger and more prepared to deliver in 2015
and beyond.
Sincerely,
Phil Rykhoek
President and
Chief Executive Officer
March 27, 2015