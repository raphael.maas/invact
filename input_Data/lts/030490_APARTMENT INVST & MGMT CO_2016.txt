March 2, 2017
My Fellow Shareholders,
On behalf of Apartment Investment and Management Company and the entire Aimco team, I am pleased to
provide to you our 2016 Annual Report on Form 10-K as filed with the Securities and Exchange Commission;
our Corporate Citizenship Report; and the Proxy Statement for the Annual Meeting of Shareholders to be held in
Denver on April 25, 2017.
2016 was another solid year for Aimco and its shareholders. The steady execution of our strategy led to progress
in our five areas of strategic focus: Property Operations; Redevelopment and Development; Portfolio
Management; Balance Sheet; and Culture. I am pleased to share with you the following highlights:
1. Property Operations:
During 2016, Keith Kimmel, head of Property Operations, and his team in the field led by Kevin Mosher
and Didi Meredith, focused on revenue growth driven by customer satisfaction and resident retention, and
also on cost control. In our conventional same store portfolio, residents gave our site teams customer
satisfaction scores averaging better than four stars in each of the four quarters, supporting revenue growth of
4.7%. Our focus on efficient operations through centralization of administrative tasks, exploitation of
economies of scale, and investment in more durable materials and more efficient equipment helped limit
expense growth to 1.4%. These operating results led to year-over-year net operating income growth of 6.2%
and Adjusted Funds from Operations (�AFFO�) growth of 5%.
In 2017, we anticipate a more difficult year for the apartment business, with a maturing economic cycle,
government policy uncertainties, and, in many markets, excessive new supply. For Aimco, about one quarter
of our portfolio is invested in communities at the �A� price point in submarkets where competitive new
supply is more than 2% of existing stock. However, our portfolio, which is diversified in both geography
and price point, was designed with these predictable challenges in mind. Our operating teams will continue
to emphasize customer selection; measured customer satisfaction; high retention rates; cost discipline;
regular investment in the physical condition of our communities; and an engaged and collaborative team.
2. Redevelopment and Development:
During 2016, we invested $183 million in Redevelopment and Development activities. Under the leadership
of Patti Fielding and her lieutenant Wes Powell, we continued our phased redevelopments in Center City
Philadelphia. At Park Towne Place, we completed construction of the South and East Towers and have
completed lease-up of the South Tower and 70% of the East Tower. Based on these results, we commenced
construction of the North Tower late last year. At The Sterling, 88% of the apartment homes were complete
and 92% of these were leased.
We achieved NOI stabilization at three previously completed redevelopments in Coastal California: Lincoln
Place in Venice; Ocean House on Prospect in La Jolla; and Preserve at Marin in Corte Madera. On a
combined basis, redevelopment of these communities created approximately $170 million of value, a
premium of 30% on Aimco�s one-half billion dollars of redevelopment spending. We also started
redevelopments at four communities: Bay Parc Plaza in Miami; Saybrook Pointe in San Jose, California;
Yorktown in Lombard, a suburb west of Chicago; and a second phase of redevelopment at The Palazzo at
Park La Brea, in Mid-Wilshire, Los Angeles.
In 2016, under the leadership of John Bezzant, we completed development of One Canal in Boston. Thanks
to the quality of the property and an excellent lease-up by Keith and his team, 86% of the apartment homes
were occupied at year end, a pace several months ahead of underwriting.
In 2017, we plan to invest between $100 and $200 million in redevelopment and development, as well as
$70 to 90 million in other property upgrades.
3. Portfolio Management:
Under the careful eye of John Bezzant, chairman of our Investment Committee, our portfolio continues to
improve. In our conventional portfolio, monthly revenues per apartment home were up 8% year-over year to
$1,978. This rate of growth reflects the impact of market rent growth, and more significantly, the impact of
portfolio management activities. During 2016, we sold eight apartment communities with about 3,300
apartment homes for gross proceeds of $529 million. These communities had average revenues per
apartment home 28% below the average revenues of our retained portfolio. We reinvested the proceeds from
these sales in redevelopment and development, acquisitions and property upgrades, with substantially higher
expected free cash flow returns than those expected from the communities sold.
During the year, we closed the $320 million purchase of Indigo in Redwood City, California. Here too,
leasing is well ahead of schedule and 77% of the apartment homes were occupied by year-end at rents
consistent with underwriting.
In 2017, we plan to continue to upgrade our portfolio through our redevelopment and development activities
funded in part from the sale of apartment communities with lower expected returns. With these activities
and expected market rent growth, we forecast average revenues per apartment home to increase by 4% to
approximately $2,050 by year-end. While our outlook for 2017 does not include acquisitions, we will
continue to look for opportunities consistent with our paired trade discipline.
4. Balance Sheet:
Thanks to the leadership of Paul Beldin, our Chief Financial Officer, and Patti Fielding, our Treasurer, we
enjoy a safe and flexible balance sheet, with ample liquidity. Our leverage, as measured by the ratio of Debt
plus Preferred Equity to trailing twelve month EBITDA, was 6.7x at the end of 2016, 10 basis points lower
year-over-year. We expect this ratio to continue its decline in the next few years from earnings growth,
especially as apartment homes now being redeveloped are completed and as we complete the lease-up of
One Canal and Indigo. During the year, we restructured our bank line to extend its maturity to 2022 and to
lower its costs. We ended the year with approximately $700 million of liquidity, including cash, restricted
cash, and our largely unused bank line. We held unencumbered apartment communities with a value greater
than $1.6 billion, providing us additional financial flexibility.
5. Culture:
Aimco�s successes are driven by the hard work and dedication of a talented team. In 2016, Aimco was
recognized for our high-performing and collaborative work environment when the Denver Post named
Aimco a Top Place to Work in Colorado for the fourth consecutive year. Aimco was one of only three
mid-size companies to be named a Top Work Place for the past four consecutive years. I thank Jennifer
Johnson, head of human resources, and her team for helping make culture and team engagement a priority
for Aimco.
Philanthropy is a significant part of the Aimco culture and our team members take especial pride in giving
back to the communities in which we live and work. In 2016, Miles Cortez and Patti Shwayder led Aimco
Cares, through which teammates volunteered in a national community service week, supported numerous
veterans causes, and directed donations to nonprofit organizations of their choosing through Aimco�s Give
with Gusto program that provides a financial match for hours volunteered by our team members.
Shareholder accountability is yet another bedrock of the Aimco culture. Throughout 2016, senior leaders
held more than 300 meetings with investors or potential investors, working to communicate the Aimco story
and, importantly, asking what we can do better. In particular, Lisa Cohn, our General Counsel, meets
regularly with our largest shareholders holding about two-thirds of our shares to review matters of
governance and other policy concerns.
We begin 2017 with the goal to build upon 2016, and to strive to be �the best owner and operator of apartment
communities, inspired by a talented team committed to exceptional customer service, strong financial
performance, and outstanding corporate citizenship.�
Aimco is guided by its capable and collaborative Board of Directors. My colleagues on the board � Tom
Keltner, Lanny Martin, Bob Miller, Kathleen Nelson, Mike Stein, and Nina Tran � are fully informed on the
Aimco business and are extraordinary resources to management, in matters large and small. I am grateful for
their thoughtfulness, judgment, and help. We work together to be transparent to our shareholders and good
stewards for the capital entrusted to us. On behalf of them and the entire Aimco team, thank you for your
investment. We are working hard to keep it safe and to make it more valuable.
Sincerely,
Terry Considine
Chairman and CEO