to our stakeholders

At Dr Pepper Snapple Group, we are executing
on the priorities we established more than two
                                                     
years ago to grow our vibrant business with flavor.
Our strategy is working. In 2009, our first full
                                                      
year as a stand-alone company, we grew volume
and dollar share in carbonated soft drinks (CSDs)
                                                     
and juices in the U.S., Canada and Mexico amid
a challenging economic environment. We also           
delivered solid top- and bottom-line results while
                                                      
strengthening our internal capabilities. This
is allowing us to take advantage of the growth
prospects that exist for our flavor portfolio.          
Our priorities are simple. 1) Build and enhance
our leading brands. 2) Pursue profitable
channels, packages and categories. 3) Leverage
our integrated business model. 4) Strengthen
our route to market. 5) Improve our operating
efficiency. We made steady progress
against these priorities in 2009
and achieved the following:
   Outperformed the U.S. CSD
    category in volume growth
    by a margin of nearly
    7 percentage points
   Became the only major
    beverage company to grow
    U.S. CSD dollar share in
    each of the last five years
   Declared our first-ever
    dividend and announced a
    share repurchase program
   Reached an agreement
    to expand availability of
    Dr Pepper to all McDonald's 
    restaurants in the U.S.
   Completed the biggest makeover
    of Snapple in its 37-year history
   Achieved national distribution
    for Crush in the U.S.
   Added more than 200 new routes
    in Mexico
   Increased volume and market
    share of Dr Pepper in Canada
                           
                             Against a challenging economic backdrop, Dr Pepper Snapple continues to create shareholder value,
                             as shown in our relative price performance vs. the S&P 500 Index in 2009.


                             Our accomplishments in 2009 are just a                              A&W grew low single digits, while Sunkist
                             taste of those to come, and we are energized                        soda declined high single digits. Crush
                             by the knowledge that our journey is just                           volume more than doubled, adding 48
                             beginning. The actions we have taken this                           million cases in 2009 through expanded
                             year will build a foundation for long-term                          third-party distribution in the U.S. and the
                             growth that will sustain our company well                           launch of Crush value offerings in Mexico.
                             into the future.
                                                                                                 Our leadership in the juice aisle continued
                                                                                                 on the strength of Hawaiian Punch and
                             The Flavor of Growth
                                                                                                 Mott's, with volume gains of 14 percent
                             Macroeconomic conditions provided a
                                                                                                 and 8 percent, respectively. Our premium-
                             challenging backdrop for the beverage
                                                                                                 priced products continued to be negatively
                             industry in 2009. Sales of liquid refreshment
                                                                                                 impacted as consumers shifted to value
                             beverages (LRBs) declined for the second
                                                                                                 offerings. Snapple volume was down
                             consecutive year, while consumer spending
                                                                                                 11 percent for the year, but sales trends
                             remained weak and shoppers continued
                                                                                                 are strengthening and the brand improved
                             to gravitate toward value. Despite these
                                                                                                 sequentially for the last three quarters
                             challenges, Dr Pepper Snapple Group
                                                                                                 of 2009.
                             focused on finding new ways to win, and
                             the result was strong business performance,                         In Mexico, we grew our share of flavored
                             as we were the only major beverage company                          CSDs. The restage of Peafiel flavors and
                             to increase our share of LRBs in 2009.                              expanded distribution for the brand resulted
                                                                                                 in a mid single-digit increase in Peafiel
                             Net sales increased 2 percent on a currency-
                                                                                                 volume, while Squirt declined high single
                             neutral basis and excluding the loss of a
                                                                                                 digits. We also grew share of Clamato
                             licensed brand that we no longer distribute.
                                                                                                 and flavored CSDs in Canada, particularly
                             Driving the top-line improvement were price
                                                                                                 Dr Pepper, where expanded programming
                             increases and 4 percent sales volume growth,
                                                                                                 and trial contributed to double-digit
                             partially offset by negative mix from higher
                                                                                                 volume growth for the brand.
                             sales of CSD concentrates and value juices.
                                                                                                 Segment operating profit on a comparable
                             Strong performance across multiple brands
                                                                                                 basis increased 17 percent on the strength
                             contributed to the volume growth, with
                                                                                                 of the sales gain and lower packaging,
                             CSDs up 4 percent and our non-carbonated
                                                                                                 ingredient and transportation costs.
                             beverages up 2 percent. Dr Pepper volume
                                                                                                 Excluding certain items, we earned $1.97
                             increased 2 percent, largely driven by Diet
                                                                                                 per diluted share, an increase of more than
                             Dr Pepper and the launch of Dr Pepper
                                                                                                 6 percent compared to 2008.
                             Cherry. Among our Core 4 brands, Canada
                             Dry was up mid single digits and 7UP and






                                                                     And while other companies pulled back
... enabled us to accelerate our debt
                                                                     on advertising and marketing expenses
repayment and begin to deploy excess
                                                                     in 2009, we stepped up our spending to
cash in shareholder-friendly ways,
                                                                     position our brands for long-term growth.
including the declaration of our first-
ever dividend and the announcement
of a share repurchase program.
                                                      
                             Creating Value for Stockholders                      Leadership Transition
                             In 2009 we generated $865 million of cash            As we announced last October, John Stewart,





                             from operating activities. Our strong, stable        our chief financial officer, will soon retire.
                             cash flow allowed us to repay approximately          Without question, Dr Pepper Snapple Group
                             $550 million in long-term debt while                 would not be where it is today without John's
                             continuing to invest in growth opportunities.        talent, dedication and exemplary work ethic.
                             We also began deploying excess cash in               He played a critical role in our successful
                             shareholder-friendly ways, including declaring       separation from Cadbury Schweppes and led
                             our first-ever dividend of $0.15 per share on        significant improvements in our systems and
                             the company's common stock and announcing            financial controls. He also built a talented and
                             plans to repurchase up to $200 million of            highly effective finance and IT organization.
                             our outstanding common stock over the                We're grateful for John's many contributions
                             next three years.                                    to our business.
                             More recently, we completed the licensing            Succeeding John as chief financial officer
                             of certain brands to PepsiCo, Inc. following         is Martin Ellen, who will join us from Snap-on
                             its acquisitions of The Pepsi Bottling Group,        Incorporated on April 1. Marty has a strong
                             Inc. and PepsiAmericas, Inc. As part of the          background in finance as well as expertise
                             transaction, DPS received a one-time cash            in strategy and operations and will play an
                             payment of $900 million before taxes and             important role in taking our business to the
                             other related fees and expenses. Having              next level of financial and operating success.
                             used a portion of these proceeds to further
                                                                                  Growing with Flavor in 2010
                             reduce our debt obligations, our total
                             outstanding debt now stands at $2.55                 Although the economy and consumer
                             billion, in line with our target capital structure   spending are not expected to pick up until
                             of approximately 2.25 times total debt to            later this year, we remain confident in our
                             EBITDA after certain adjustments. Moreover,          powerful brand portfolio, our ability to capture
                             our board authorized the repurchase of an            new sales and distribution wins, our continued
                             additional $800 million of our outstanding           focus on cost control and our dedicated
                             common stock, bringing our total share               employees. Combined, they provide the
                             repurchase authorization to $1 billion.              platform to seize opportunities and deliver
                                                                                  another year of solid financial results in 2010.
                             Less than two years after going public, we
                             have achieved our target capital structure.          Sincerely,
                             Combined with our focus on growing the
                             business organically, we are now committed
                             to returning excess cash to shareholders
                             over time.

                             Empowering Our People                                Wayne R. Sanders
                             Over the past two years, our people strategy         CHAIRMAN OF THE BOARD
                             has centered on aligning and mobilizing our
                             19,000 employees around our business
                             strategy. The value of these efforts is reflected
                             in our strong 2009 financial performance as
                             well as third-party survey results that show
                             our team leaders' level of engagement has            Larry D. Young
                             improved significantly in the last two years         PRESIDENT & CHIEF EXECUTIVE OFFICER
                             and is now in a league with other high-
                             performing U.S. companies.                           March 2, 2010

