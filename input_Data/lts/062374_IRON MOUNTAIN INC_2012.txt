TO OUR STOCKHOLDERS:

Our results in 2012 reflect our
continued focus on maintaining the
steady, long-term growth of our core
business, driving strong returns on
invested capital and preserving the
durability of our cash flow stream 
all key elements of our goal to deliver
sustainable value.
We wrapped up another year of solid
financial performance, with total
revenues, margins and adjusted earnings
all in line with expectations. Our
storage rental growth of more than
4% in constant currency reflected
both healthy increases in our international
business and stable growth
in our North American business,
and offset a modest decline in total
service revenues. In addition, we
maintained our strong balance sheet,
achieved further improvements in
capital efficiency, maintained strong
return on invested capital and made
significant distributions to our stockholders
consistent with our capital
allocation strategy.
These strong fundamentals are what
attracted me to the opportunity to
lead Iron Mountain. I was impressed
by the companys established history
of steady growth and strong cash
flow generation, high return opportunities
in international markets and
unique culture. Many companies
experience fluctuations in revenues
as global economies rise and fall, but
Iron Mountain has a unique ability
to sustain its durable growth rate
throughout business cycles due to
the attractive underlying stability of
the storage business.
My near-term focus upon joining
the company in January 2013 was
to spend a good deal of time with
customers, investors and our associates
around the globe. These interactions
reinforced for me the opportunity
Iron Mountain has in three areas:
tapping the unvended records market,
expanding into additional emerging
markets and leveraging the brand
through adjacent services. Be assured
that as we pursue these opportunities,
we will continue to view them
through our capital allocation filter,
which focuses on maximizing cash
flow, generating strong returns and
supporting consistent growth.
The first of these opportunities
relates to the amount of unvended
records that remain in developed
markets in both the private and
public sectors. We have organized
our marketing efforts into vertical
segments such as healthcare, financial
services, legal and government
and more closely aligned our sales
and account management groups to
address this opportunity. It is early
in the process, but we are making
progress and receiving positive feedback
from customers who look to us

to provide insight into industry
best practices. This sector focus
combined with Iron Mountains
strong relationships should, in time,
allow us to store more records for
our customers and provide highervalue-
added services.
Additionally, Iron Mountain has an
internationally recognized brand,
yet only about a quarter of our
revenues today are generated
outside North America. Overall,
emerging markets are still early in
the records outsourcing process,
providing significant untapped
potential for continued growth in
our international business. This
expansion is a great fit with my
background, having spent the last
27 years running multinational
businesses from outside the United
States. To be clear, international
expansion is not about putting
pins on the map. We will pursue
entry into additional markets with
an aggressive, yet disciplined
approach that is consistent with
our return on invested capital
focus, and where we believe we
can develop industry-leading businesses
over time.
As a result of our efforts to
improve operating efficiencies
and capacity utilization, we are on
track to achieve our goal of 25%
international margins by the end of
2013. We manage our international
operations as a portfolio, where
margins range from very high in
markets where we have leadership
positions to emerging markets
we have only recently entered
that have the potential to achieve
attractive results over time.
Overall, our international business
is strong, with good growth opportunities,
and we expect to maintain
solid margins and returns as we
continue to expand.
Lastly, we will look to invest where
we can build on the trust our
customers have in Iron Mountain
for secure, reliable enterprise
storage, but in areas that are
adjacent to our core operational
expertise. One example
is our underground wholesale
data center, where customers
approached us seeking secure
space for their data center operations.
We have substantial space
to further expand capacity in this
data center, and we continue to

evaluate this and other opportunities
that are consistent with our
core business.
The combination of our durable
leadership positions in developed
markets, emerging market
opportunities and potential for
business adjacencies provides a
solid foundation from which we
will continue to drive sustainable
value, and I am confident in our
ability to build on this platform.
Beyond these opportunities to
sustain the durability of our
business, I was attracted by the
nature of the organization. It is
rare that one has the chance to
lead a company with such a strong
global franchise, extensive operational
expertise and reputation
for quality. These characteristics
are why customers trust us with
their critical documents and what
enable us to innovate and do
more to improve the efficiency of
their records management. I have
also found the culture to be very
collaborative, with an overarching
objective of driving returns
through smart, disciplined capital
allocation and a focus on longterm
sustainable growth.
Iron Mountain is a great business
that drives excellent returns.
During the year, we made
steady progress on preparations
to convert to a Real Estate
Investment Trust, or REIT, which
aligns well with our investment
strategy and can enhance our
strong returns. It enables investors
to participate in growth
from our international business
and allows flexibility to invest in

closely related businesses. It also
enhances stockholder value by
providing certainty with respect
to our cash dividend, which we
anticipate will increase from
current levels. If we are successful
with our planned conversion to a
REIT, we expect our dividends to
grow in line with free cash flow.
Moving additional international
operations into the REIT structure
and expanding our service businesses
would also support dividend
growth over time. We will measure
the success of our efforts by relative
stockholder return  including
both stock price appreciation and
dividends. Building on the strong
fundamentals of our business, the
REIT structure can enhance our
ability to deliver value.
We have invested in people, process
and preparation to accelerate our
ability to be ready for REIT conversion
at the beginning of 2014.
Work is underway to integrate and
upgrade information systems to
support portfolio performance
reporting and to organize our business
structures to align with REIT
requirements. We see strong parallels
between Iron Mountain and
certain REIT sector characteristics.
In terms of the nature of our business,
we believe Iron Mountain
is most like self-storage, but with
an important difference: we are
enterprise storage serving 94% of
the FORTUNE 1000, as opposed to
consumer self-storage, which has
a very different credit profile. We
also compare favorably in terms of
attractive cash flow characteristics,
low volatility and low risk. We
look forward to elaborating on the
specifics of these and other property
portfolio metrics, as well as our
thoughts on Iron Mountains valuation
as a REIT, in future reports.
In closing, we are executing against
our business strategy and are well
positioned to deliver on our financial
objectives in 2013. Our core
storage rental business is strong,
we have meaningful opportunities
available to us to extend the
durable storage annuity, and we
are making good progress toward
delivering the REIT.
I would like to take this opportunity
to thank Richard Reese
for his many contributions to
Iron Mountain. Our stockholders
have come to know his candid,
thoughtful style, and I believe the
culture of the company is due to
his influence and leadership over
the past 30 years. I have truly
appreciated his insights in the time
I have gotten to work with him, and
I know you all join me in thanking
him and wishing him well.
As we continue our work to deliver
sustainable value, we appreciate the
commitment of our employees, the
loyalty of our customers and the
encouragement from our investors.
William L. Meaney,
Chief Executive Officer
With confidence and appreciation for your continued support,