Dear Fellow Shareholders:

As we began 2016, oil prices were trading at 30-year lows, natural gas prices
were at a 17-year low�trading below $2.00 per MMBtu, and an unusually
warm winter was quickly heading toward an early spring. The industry is
navigating similar circumstances now. And as we look back at 2016, we look
at a year that brought new opportunities to Range Resources, with a renewed
emphasis on the importance of steady, measured progress.
Year after year, our forward momentum is tied to our strategy: Range is
focused on a long-term plan to prudently grow production volumes while
employing capital discipline to achieve one of the lowest cost structures
in the industry, all in order to generate per share growth on a debt adjusted
basis. We seek to expand our margins through cost improvements, capital
efficiencies and improved price realizations; and to consistently build
and high grade our drilling inventory. It is our goal to maintain a strong,
simple financial position, to be good stewards of the environment and to
operate safely. It is by following this strategy that we have established a track
record of growth at low cost, and now we have the best inventory of projects
in our Company�s history.
During the first half of 2016 we closed two important asset sales: non-operated
properties in Bradford County, Pennsylvania; and assets in Blaine, Canadian
and Kingfisher Counties in Central Oklahoma. These sales followed our
successful late 2015 Nora properties divestiture in Virginia and together, the
three assets sales totaled $1.1 billion in cash for Range.
In September 2016, we completed our merger with Memorial Resource
Development (MRD): providing Range with strategic positioning in both the
Appalachian and Gulf Coast regions, greater marketing capabilities and
opportunities, and adding beneficial exposure to growing natural gas demand.
In conjunction with the merger, we completed a note tender offer and exchange,
better positioning us to finance our future growth. Furthermore, the net
effect of our early-2016 asset sales coupled with the purchase of MRD served
to significantly improve Range�s balance sheet, while enhancing our drilling
inventory, improving our margins and netback pricing, and providing both
state and basin diversity.
We saw another solid year of reserve growth in 2016, with Range replacing
292% of production from drilling activities with drill bit development costs
of $0.34 per mcfe when considering pricing and performance revisions.
Positive performance revisions continued in 2016 as we extended laterals,
improved targeting and drove efficiencies throughout our developed leasehold
and infrastructure. The strong reserve additions from drilling activity were
driven primarily by our development in the Marcellus, as our acquisition of
North Louisiana assets closed in late 2016. Future development costs for
proven undeveloped locations are estimated to be $0.42 per mcfe, which is
outstanding and should improve our unhedged recycle ratio to approximately
3x. We monitor our recycle ratio because it is an important measure of our
overall profitability. Importantly, Range added 1.65 Tcfe of reserves, excluding
acquisitions, reflecting our large inventory of low-risk, high-return projects
in the Marcellus shale and in North Louisiana.
Well performance in North Louisiana in 2016 was in line with our acquisition
economics and reserve estimates recorded a slight performance increase,
while drilling added 79 Bcfe of reserves post-acquisition. Looking forward,
we see capital efficiencies continuing as we drive down well costs while
optimizing targeting to improve recoveries. Importantly though, as conditions
change, we will have the ability to move capital to either the North Louisiana
or Appalachian basins, providing us greater flexibility as a company.
Additional new transportation and marketing arrangements that came on
line in 2016 included the Gulf Markets Expansion in early October. As a result,
we are now able to move an added 150,000 Mmbtu per day of Range�s gas to
the Gulf Coast enabling us to realize better netback pricing. We also saw a
significant uplift in pricing on our Marcellus condensate during 2016. Overall,
we have seen noteworthy netback pricing improvements for all three of our
products: natural gas, natural gas liquids and condensate. We expect these
positive results to continue in 2017.
The most impactful change in netback pricing in 2017 is from an expected
improvement in our natural gas differential. The reasons for the improvements
are twofold: first, in 2017 we will have a full year of access to transportation
on projects like the Gulf Markets Expansion. Second, we will have a full year
of our North Louisiana gas production, which receives very near NYMEX
pricing. In addition, a full year of North Louisiana production positively
impacts our projections for condensate pricing, as do our new condensate
sales in Pennsylvania.
We expect to see continued uplift with regard to our NGLs in 2017 due
to three primary drivers. First, we will have a full year of transportation
on the Mariner East pipeline, which became fully operational last May. We
are now shipping approximately 20,000 barrels of ethane per day to Norway
and Scotland. We are also transporting 20,000 barrels of propane per day
to Marcus Hook, where it is then being exported to international markets,
or sold locally when conditions favorably impact prices. Second, in
2017 we will realize a full year of North Louisiana NGLs which are well
located and receive favorable pricing. Finally, propane and butane pricing has
improved relative to West Texas Intermediate (WTI) due to increased exports
and better alignment between supply and demand.
We are also expecting our cash costs to remain low. The net effect is that our
margins are projected to show an increase in 2017 versus 2016. That
improvement, coupled with our low development costs, will result in Range
having one of the best recycle ratios in our business for either an oil company
or a natural gas company.
Looking beyond this year, there is considerable demand for natural gas coming
from LNG exports, Mexican exports, power generation and industrial use. In
total, by 2020, about 14 Bcfpd of additional natural gas demand is projected
to occur. In addition to the roughly 14 Bcfpd of additional demand, it takes
more than 6 Bcfpd per year to offset the base industry decline. In aggregate
that is 14 Bcfpd of demand plus about 24 Bcfpd of base decline, which is a
total of about 38 Bcfpd of new gas that is required by the end of 2020.
As we continue to assess our position in North Louisiana, the team has made
significant progress early on, markedly lowering the cost to drill and complete
a well in Terryville: from $8.7 million to $7.7 million. This positively impacts
the economics of a well and the lower capital cost expands the inventory of
wells to drill, including in the Lower Red and potentially the Pink horizons.

The team is also now keeping the wells within a tighter zone. This technique
significantly improved the performance of our Marcellus wells, and has done
so in other plays as well. The bottom line is the team is drilling wells at a
significantly lower cost, while keeping them within zone, within a tighter
target window � and we are optimistic about the impact that these
improvements will have on both capital efficiency and production results.
In the Marcellus, lateral lengths continue to increase and we are projecting
an increase in our laterals from about 6,500 feet in 2016 to over 8,000 feet
in 2017. We have also updated and posted to our website revised economic
estimates for the dry, wet and super-rich areas. An increase in NGL pricing,
coupled with the longer laterals, has led to improved economic projections
versus 2016, especially in the wet and super-rich areas.
We see 2017 shaping up to be a year of steady improvement for Range; building
on the team�s successes in 2016. We are now seeing the advantages of a
diversified marketing portfolio, as prices are expected to improve for all of
our products in 2017, driving higher margins and a peer-leading recycle ratio.
Range remains one of the few companies in the industry with a multi-decade
inventory of high-quality wells. We have a deep bench of stacked pays in
both Pennsylvania and North Louisiana. And we have the optionality of
drilling dry or wet, which is important as we consider the recovery of NGL
pricing. Moving forward, we will maintain an unwavering focus on operating
safely, protecting the environment and meeting or exceeding all operational
goals for Range.
As we take a final look back at 2016, I want to thank our Board of Directors.
The steady leadership and guidance provided by our Board is an important
component of our Company. We are gratified to welcome Dr. Steve Palko and
Bob Innamorati to our Board. Steve joined the Board in late 2016 and brings
with him decades of experience in the oil and gas industry, including his role
as the co-founder of XTO Energy Inc., where he served as President and ViceChairman from 1986 to 2005. Steve is currently an Associate Professor at
Texas Christian University. Bob brings over five decades of investment
experience and knowledge to our Board. He served as a member of the Board
of Directors of Memorial Production Partners GP LLC from 2012 to 2014
and Memorial Resource Development Corp. from 2014 to 2016, where he
also served as Chairman of the Audit Committee. An American patriot, Bob
is also a former member of the U.S. Secret Service and a veteran of the United
States Marine Corps Reserves.
Finally, we extend our thanks to a team of employees whose innovation,
passion, creative spirit and work ethic are the very foundation upon
which our Company continues to build. And we thank you, our loyal
shareholders, for your continued faith in Range Resources and the exciting
future that lies ahead.
JEFFREY L. VENTURA
Chairman, President & Chief Executive Officer