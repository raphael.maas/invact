DEAR SHAREHOLDERS :

Its been a year since I reported to you about Mercks Plan to Win  our blueprint for returning Merck
to the industry-leading position it once enjoyed and which we are committed to reclaim.
I am pleased to report that the results of the past 12 months confirm that we are off to a strong start
in executing our plan. During 2006:
- We successfully launched five new vaccines and medicines  Gardasil, Januvia, Zostavax, Zolinza
and RotaTeq  that are breaking new ground in the fight against diseases such as cancer and diabetes;
- We advanced promising products in our pipeline at every phase of development and across our
therapeutic areas, while continuing to fundamentally change the way we discover, develop, manufacture
and market our medicines and vaccines; and
- We achieved impressive sales growth in such products as Singulair, and joint venture products
Vytorin and Zetia.
As I look back over the past year, I believe we have gathered the momentum needed to meet the
performance goal we established in December 2005: delivering double-digit compound annual
earnings-per-share growth, excluding charges and one-time items, by 2010.
And as I look at the year ahead, I am confident that we are poised to build on the success of 2006.
In 2007, we expect to file three New Drug Applications with the U.S. Food and Drug Administration (FDA).
They include raltegravir (MK-0518), a promising first-in-class treatment for HIV infection; gaboxadol, a novel
compound to treat insomnia; and a compound that combines Mercks own extended-release (ER) niacin with
laropiprant. This last compound, also known as MK-0524A, is expected to further help patients manage
cholesterol by decreasing LDL cholesterol, increasing HDL cholesterol and lowering triglycerides.
These filings are in addition to the three products already under review at the FDA: Janumet, a medicine
for the treatment of type 2 diabetes; Emend For Injection (MK-0517), an intravenous therapy to help reduce

the nausea and vomiting experienced by many cancer patients receiving chemotherapy; and Arcoxia, our
medicine for reducing the pain caused by osteoarthritis.
These products are just some of the results of our efforts to improve productivity at all stages of our
pipeline. We have increased the productivity of our early-stage pipeline, generating a steady progression
of promising compounds into our later-stage pipeline, while maintaining our high scientific standards.
And we are significantly reducing the time it takes to move a product through every phase of development.
In late development, we have already exceeded our prior cycle-time reduction goal by achieving more
than a 10-month reduction by the end of 2006, and are on track to make it a 12-month reduction by
the end of 2007.
The strength of our pipeline has been further enhanced by our continued commitment to identify and
enter into strategic acquisitions and alliances that complement our internal research and development efforts.
In 2006, we signed 53 key agreements, including the acquisitions of three leading biotech companies.
And we are leveraging these transactions to further strengthen and speed drug discovery at Merck.
In this report, you will read the real-life stories of patients and our five newest medicines and vaccines 
products that will make an immeasurable difference in the lives of millions of people around the world.
We also expect that these products will make a substantial contribution to Mercks future success.
The launches of our five new products are evidence of our scientific excellence. But they also clearly
indicate that our commitment to create and execute a new commercial model is on target.
We launched these products in the United States without any increase in our sales force, redirecting
1,500 of our sales representatives to our new vaccines. In addition, for more than two years, weve been
able to hold the line on the size of our sales force by using many new techniques for reaching our customers
in ways that meet their needs and the demands on their time.

This past year also has seen us lay the groundwork for creating the lean and flexible cost structure
we committed to last year. By the end of 2006, we had already achieved $1 billion of our goal to save
$1.2 billion in procurement Company-wide by 2008.
In addition, we already have eliminated 4,800 of the 7,000 positions we committed to eliminate by
the end of 2008 and have closed, sold or ceased operations at three manufacturing facilities. We are in
the process of doing the same with two other manufacturing facilities we said wed close or sell by the
end of 2008.
We are also restructuring our manufacturing network to make it more competitive, leaner and
flexible enough to meet future market needs as quickly as those needs change. That includes achieving
a $767 million reduction in inventory since year-end 2003, including a $226 million reduction in 2006,
even while launching five new products.
And while achieving operating efficiencies includes direct spending reductions, it also includes executing
broader efforts, across the Company, to increase productivity. That is why we are deploying Lean/Six Sigma
principles throughout Merck.
Furthermore, as the pace of development in our labs increases, we are making sure that our manufacturing
capability is also keeping pace. This way, once a product approval is received, we have the flexibility in our
facilities to begin delivering that product to our customers more quickly than ever before.
All of these actions to make Merck leaner and more flexible have put us on track to achieve our
$4.5 to $5.0 billion long-term savings goal through 2010.
Of course, even as we change, one thing remains the same. We are still a company whose mission is to
discover and develop novel medicines and vaccines that address unmet medical needs, with a commitment
to get those products to the people who need them.

That long-standing commitment was broadened in 2006 with the creation of Mercks Vaccine
Patient Assistance Program. This new program, which complements our 50-year-old Patient Assistance
Program, will provide Mercks adult vaccines (such as Gardasil and Zostavax) free of charge to uninsured
U.S. adults age 19 or over who cannot afford them.
And, as you will read elsewhere in this report, we have entered into a partnership with the Nicaraguan
government to provide our new vaccine RotaTeq at no charge to every newborn child in that country for
the next three years. We are also working with the global health nonprofit PATH and the Bill & Melinda
Gates Foundation to develop HPV vaccination programs that will facilitate the introduction of Gardasil
in some of the most impoverished nations.
The Merck Company Foundation, which marks its 50th anniversary in 2007, has played an important
role in the Companys efforts to enhance the health and well-being of people around the world. Since its
inception, the Foundation has contributed more than $480 million to support important initiatives that,
for example, support and enhance Botswanas national response to HIV/AIDS, address the complex and
growing problem of pediatric asthma in the United States, and improve science education for U.S. students.
This year were also celebrating another significant milestone in the Companys history of helping
patients gain access to our medicines  20 years of the Mectizan Donation Program. Over the last two
decades, Merck has donated close to 500 million doses of Mectizan for the treatment of river blindness,
preventing an estimated 40,000 cases of blindness annually in the developing world. It is the largest
ongoing medical donation program and longest-standing public-private partnership of its kind  one
were proud to have helped establish and to continue to support.
The people of Merck have worked hard and worked smart to bring about the successes of 2006 and
to set the stage for continued success in 2007 and beyond. Two individuals who have helped provide

enormous leadership and vision as we work to regain our leadership role, Larry Bossidy and Bill Bowen,
are retiring this year from our Board of Directors. I want to thank them for their commitment and
outstanding leadership on the Board.
Also retiring this year is Judy Lewent, Mercks chief financial officer for the past 17 years. Judys business
and financial acumen, keen strategic thinking and analytical skills have made her an invaluable advisor and
colleague. She will be greatly missed. Judy intends to retire in July, and we have begun a search for her
replacement to ensure an orderly transition.
The contributions of all three of these leaders have helped to ensure that we are on a sound course
to become a market leader once again.
I am convinced, not just by the success of 2006, but also by the energy and enthusiasm I see throughout the
Company, that the changes we are implementing will allow Merck to reach its high-performance potential. And
once we do, Merck will again occupy the unquestioned position of scientific and market leadership we
know it should.
SINCERELY,
DICK CLARK
CHIEF EXECUTIVE OFFICER AND PRESIDENT
