Dear Shareholders, Associates, Business Partners, and Friends:
These are highly volatile and unprecedented economic times with banks in a tailspin, retailers struggling to
survive, and consumers holding on to their money. We initiated our turnaround strategy back in 2007 on the
eve of the economic downturn. While our performance has yet to reward our shareholders, we remain diligently
focused on two goals  navigating the company through todays treacherous markets and taking brand-right
actions that strengthen the long-term saliency and earnings power of our portfolio.
The Beginning of a Turnaround
Recalling the past two years, our business model was already frayed in advance of the current marketplace meltdown
resulting in 2007 earnings that were significantly off target. To reverse this, we had to embark upon major change.
Seeking to recreate the magic and momentum that inspired great apparel and accessories mega-brands, including
our own heritage Liz Claiborne brand, we initiated an aggressive turnaround plan built around a principle we call
brand-centricity.
Our plan incorporated the key elements, or best practices, of successful businesses not only in our industry,
but also from across many sectors. These principles include:
 Communicating a clear vision  adopting a set of manageable, balanced priorities
 Aligning resources with our priorities and towards attainable profit pools 
carefully managing expenses to fund growth
 Attracting and rewarding top talent
 Integrating the multiple activities of a given brand under one roof with each brand
operating as a true profit/loss center
The choices we made werent elective. Not only did we reach a point where our earnings were unsustainable,
we lost our focus and commitment to these principles.
Thankfully, the era of acquisitions at Liz Claiborne Inc. bore fruit  we had a core set of brands with real power
and potential just waiting to be harnessed.
If you followed our story since, you know that we have narrowed our portfolio, embarked upon a massive
restructuring and cost reduction, accelerated our owned-retail mix, and promoted or hired the best talent
with the right skill sets for our brands. Fortunately, much of this was done just in advance of the global
recession hitting.
Operating in an Unprecedented Recession
While the actions we took early in 2007 provided much needed flexibility to our operating model, we did not go
into this recession with an advantaged balance sheet. Like many other companies, we were significantly leveraged,
a fact that has clearly played into our actions and plans in the past year. 
As the recession has deepened, we have carefully been controlling the controllables  cash, cost, and debt
reduction  while growing the relevance and appeal of our brands. Our strategy of building powerful lifestyle
brands through a brand-centric, decentralized approach remains intact with the added mandates of de-levering
and focusing on cash generation and capital efficiency. Our operating flexibility has been significantly increased
by the tangible benefits of our actions:
 The elimination of approximately 2,700 positions, including a de-layering of management
 Closure of six distribution centers
 Office space consolidation
 Discretionary spending reductions or eliminations
 Company-wide freeze of merit increases in 2009
 Capital efficient, revenue generating partnership with Elizabeth Arden for effective
management of the cosmetics business and associated brand line extensions
 Exclusive, company-wide buying agency agreement with Li & Fung, a world class
product sourcing agency
The progress weve made in implementing our turnaround principles can hardly be judged in our earnings story.
But, in time, that will change.
That said, the lack of visibility into the rest of 2009 and beyond makes it very hard to provide earnings guidance
for the coming year. Our plan calls for continued retail traffic declines and sales that mirror the trend in the fourth
quarter  minus the effect of the Christmas push in late December  and we are assuming these levels will persist
throughout much of the year.
I believe our internal projections and models are realistic  we are not banking on improved consumer traffic in
our stores or in the stores of our department store customers. Rather, we see managing liquidity and maximizing
our availability under our new bank credit facility as job one, with our operating priorities for 2009 as follows:
 Cash flow and liquidity. These enterprise-wide objectives are shared by virtually
every employee and our incentive systems and goals will reflect this focus.
 Brand execution. In this climate, our brands are using market research more than ever 
listening to and observing their consumers and getting very creative about their offerings,
in-store service, and marketing approaches.
 Cost management. This is not just cost reduction, but a commitment to ruthless management
of discretionary spending categories as well as additional cost-out in some areas.
Adaptive Execution
Financial management is key, but equally important is finding a way  through all the economic turmoil  to ensure
our brands are even more relevant to our consumers while staying the course on each brands core, long-term
positioning. We do this first and foremost by focusing on great product  special, exciting, new, and smart from a
value perspective. And not just with the much anticipated, highly touted re-launch of the Isaac Mizrahi designed
Liz Claiborne New York collection, but in every brand. 
At Liz Claiborne Inc., brand execution centers on five concepts:
 Assortment and mix shifts  meaning productivity at retail
 Price point recalibration  rethinking opening price points and their penetration in our lines
 Inventory management  planning conservatively and incenting teams to manage inventory turns
 Traffic generating promotions and value offers
 Capital light growth via strategic partnerships  domestically and internationally
Another important component of our focus on brand execution is the recently announced Li & Fung partnership.
Beginning in the second quarter of 2009, Li & Fung will be our exclusive agent for sourcing on all brands, on all
product lines, except jewelry.
Having worked with Li & Fung on Mexx and a few other projects, we quickly came to see this first-rate organization
as the best practice  the type we were trying to emulate when restructuring our sourcing enterprise. Rather than
recreate what exists already, we chose to access their best-in-class systems, management and talent, tapping into an
even broader base of regions around the world for compelling product, enhanced going-in margin, and improved
speed-to-market models.
The Call-to-Action
With the management team nearly in place, we are fully committed to navigating through the challenges of both
the turnaround and an extraordinarily difficult market. We will continue to creatively build these businesses with
one eye on earnings and one eye on critical balance sheet metrics.
We have a tremendous group of employees across the company and around the world, and every one of them is
engaged in executing this strategy. We constantly update our thinking as a management team, and as a Board
of Directors, and we are prepared to do what it takes to break through  ultimately delivering capital efficient,
profitable growth that rewards our stakeholders.
Sincerely yours,
William L. McComb
Chief Executive Officer