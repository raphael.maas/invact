LETTER TO SHAREHOLDERS FOR 2012
Kinder Morgan Management, LLC (NYSE: KMR) is a limited partner in and manages and
controls the business and affairs of Kinder Morgan Energy Partners (NYSE: KMP), one of the
largest publicly traded pipeline limited partnerships in the United States. KMP is an energy
transportation and storage company and owns an investment in or operates approximately 46,000
miles of pipelines and 180 terminals in North America. Our pipelines transport natural gas,
gasoline, crude oil, CO2 and other products, and our terminals store petroleum products and
chemicals and handle such products as ethanol, coal, petroleum coke and steel.
KMRs success is dependent upon our operation and management of KMP and KMPs resulting
performance. In 2012, KMP declared total cash distributions of $4.98 per limited partner unit, an
8 percent increase from $4.60 per unit the previous year. KMR, like KMP, declared $4.98 per
share for 2012. Since its formation in May of 2001, KMR has delivered an average annual return
to shareholders of 15 percent (assumes that distributions are reinvested).
Looking ahead, KMR, like KMP, expects to pay a distribution of $5.28 per share for 2013,
which would be a 6 percent increase over the 2012 distribution. We believe there are
exceptional growth opportunities at KMP, as there is a need to build additional midstream
infrastructure to move or store oil, gas and liquids from the prolific shale plays in the United
States and the oilsands in Alberta, along with increasing demand for export coal and CO2. We
currently have identified approximately $11 billion in expansion and joint venture investments at
KMP that we have, or are confident that we will soon have, under contract. We are also pursuing
customer commitments for many more projects.
Distributions to KMR shareholders, which are paid in the form of additional shares, are based on
the amount of the cash distributions paid to KMP unitholders. Because distributions of additional
KMR shares are made proportionately to all of our shareholders, distributions will not be
includable in the gross income of our shareholders for federal income tax purposes and no Form
1099 will be issued.
We invite you to review the Form 10-K for both KMR and KMP, and to read my annual letter to
KMP unitholders for more information. These materials are available on our web site at
www.kindermorgan.com. As always, thank you for your ongoing support. We believe the best is
yet to come!
Sincerely,
Richard D. Kinder
Chairman and CEO