DEAR SHAREHOLDER 
THE MOST MOMENTOUS EVENT
OF 2004 FOR PSEG WAS THE MERGER AGREEMENT WITH
EXELON CORPORATION. THIS STRATEGIC STEP SPECIFICALLY
ADDRESSES KEY CHALLENGES FACING OUR COMPANY, AND
PROVIDES A SOLID BASE ON WHICH TO BUILD THE NATIONS
PREMIER UTILITY AND POWER GENERATION BUSINESS.

We also entered into a nuclear operating services contract
with Exelon as a key but separate part of our collaboration.
Under the contract, which went into effect on January 17, 2005,
Exelon began providing management services for plant
operations at our Salem and Hope Creek generating stations.
Exelon is a leader in nuclear energy operations. We expect
that the application of Exelons proven management model
to the Salem and Hope Creek stations will produce quality
results at these facilities.
Our company is fortunate to have a large base of low-cost nuclear
generating capacity, especially in the current environment of
high natural gas prices. As the Salem and Hope Creek plants
improve toward Exelons standard of excellence, with resulting
higher capacity factors, they will make a meaningfully greater
contribution to earnings. This will also help ensure that the
people of New Jersey and neighboring regions will continue to
benefit by the abundant source of safe, clean, low-cost energy
provided by nuclear power.
Just as the nuclear operating services contract will help us
achieve critical operational goals, the merger agreement with
Exelon will advance our broad strategic interests. It will enhance
our ability to deliver on our longstanding commitments to
customers, communities, investors and employees. We will be
combining with a company that shares the same commitments
to the highest standards of corporate responsibility, integrity
and performance.
The merger offers a range of compelling advantages. Exelon
Electric & Gasthe combined company resulting from the
mergershould have a large, balanced portfolio in the nations
most robust wholesale energy market, extending from the
Mississippi to the Atlantic. Exelon Electric & Gas will bring
together a range of strengths, such as Exelons in nuclear
generation and PSEGs in transmission and distribution
operations as well as energy auctions.
This combination should produce benefits exceeding what
either company could accomplish on a stand-alone basis.
It provides the scale and balance to compete more effectively
in todays volatile and uncertain energy markets. Change is
a fact of life in these markets and in the wider policy environment.
The merger with Exelon will enable us to spread risk
and expand opportunities across a larger multi-state region.
It will improve prospects for consistent profitability and growth,
with lower risk.

The merger has a complementary focus on safe, reliable, lowcost
utility service, in keeping with our strong and continuing
commitment to customers and communities. It will enable
us to deploy a greater range of human talent and resources to
improve further the quality and efficiency of customer service.
Moreover, PSE&G will continue to build on its 101-year reputation
for service excellence and community support. PSE&Gs
headquarters will remain in Newark, as will the headquarters
of the combined companys generation business.

Substantial synergies should result from the merger. Excluding the
one-time costs to achieve these benefits, the merged company
expects to realize synergies of approximately $400 million pre-tax
in the first full year after closing, growing to $500 million by the
second year. Approximately 70 percent of the synergies will come
from unregulated businesses and 30 percent from the regulated
utilities. Savings are expected from a variety of sources, including
operational efficiencies, improved supply-chain sourcing and
workforce reductions of approximately 5 percent of a consolidated
workforce of 28,000 employees. We will seek to minimize
the impact on employees through attrition and retirements, and
many will find new opportunities as part of a larger company.
The merger also addresses the key issue of leadership succession
at PSEG. The new and larger organization envisaged under our
merger agreement will have as its CEO John Rowe, Exelons
chairman, president and CEO, one of the most experienced and
successful executives in our industry. John understands how to
accomplish the vision we share of the ideal business model for
the utility industry. It will be my privilege to work together with
John and the associates of both organizations to realize this
vision, and thereafter to serve in a non-executive capacity as
chairman of the board until my planned retirement in March 2007.
Finally, the merger provides a stronger foundation for continuing
to reward shareholders with attractive long-term total returns
through a combination of dividends and earnings growth.
Our management philosophy has always been guided by a longterm
approach to creating shareholder value. Over the past five
years PSEG delivered a total shareholder return of over 93 percent,
outperforming themajor utility industry averages and considerably
outpacing the return of the S&P 500.

Dividends have long been a key way that PSEG delivers shareholder
value: PSEG has paid annual dividends for 97 consecutive
years. In 2004, we increased our dividend modestly, from $2.16
to $2.20 per share. Our Board of Directors recently approved
a further one-cent increase in the quarterly dividend, raising
the annual indicated dividend rate to $2.24 per share. Looking
ahead, the merger agreement provides that our shareholders
will be kept whole with respect to the dividend payout.
Employees across the PSEG family of companies deserve recognition
for a milestone accomplishment in 2004: They enjoyed
their safest year in the 101-year history of our firm. Their performance
builds on nearly a decade of impressive safety results.
Exelon is also committed to safe work practices, and we will
continue to drive for a safer work environment for the benefit
of our employees.

Turning to business results, 2004 was particularly challenging
for PSEG Power, our wholesale domestic energy business.
Competition intensified. Markets were especially soft for
new gas-fired generation, including our two Midwest plants.
Fuel and energy prices moved sharply higher, but for the most
part after PSEG Power had entered into new supply contracts
early in the year. However, PSEG Power should benefit from
this upward price movement over the long term, as its current
contracts roll off.
While energy markets were difficult in 2004, PSEG Power also
faced serious operational challenges during the year. There
were extended outages at several of its generating stations,
including our critically important Salem and Hope Creek nuclear
units. We used the outages to make necessary improvements
in plant operations, with an unrelenting focus on safety as the
bedrock of a long-term improvement effort. The outages led to
higher power-replacement costs and were coupled with greater
operational and maintenance expenses.
Largely as a result of PSEG Powers performance shortfalls, our
2004 earnings from continuing operations of $3.03 per share
were substantially below 2003 earnings of $3.72 per share.
On the positive side in 2004, our company continued to derive
strength and stability from PSE&G, our large New Jersey
utility. PSE&G continued to excel in providing safe and reliable
service at costs that are among the lowest for any utility in its
region. In 2004 for the third consecutive year, PSE&G won the
Reliability One award for top electric-system reliability among
Mid-Atlantic utilities.

PSE&G benefited from the $159.5 million electric distribution
rate case that became effective in August 2003, reflecting our
substantial investments in New Jerseys utility infrastructure.
Even with the new rates, there has been an extraordinary degree
of rate stability over many years for residential customers.
PSEG Energy Holdings, our business with a mix of international
and domestic energy operations and investments, produced
strong cash flow and earnings from existing operations and
investments as well as selective asset sales. Its 2004 results
were consistent with our strategy of focusing this business on
profitability, cash generation and risk reduction. Overall, this
business realized over $300 million from asset sales in 2004.
PSEG Globalthe operational arm of PSEG Energy Holdings
sold several overseas facilities, including its entire equity interest
in several power plants in China and a power plant in Tunisia,
and sold part of its interest in a distribution company in Peru.
PSEG Global also rounded out its ownership of two 1,000-
megawatt generating plants in Texas by acquiring at minimal
cost the 50 percent interest of its partner in these facilities.
PSEG Resourcesthe passive investment arm of PSEG Energy
Holdingscontinued to produce solid earnings and cash flow,
as it has since its establishment in 1985. The overall credit
quality of its investments was further enhanced by the early
termination of its lease on the Collins facility in Illinois.
A key area of accomplishment in 2004 involved our continued
steps to maintain a solid financial position. We further
strengthened our balance sheet. Our liquidity has remained
strong. We increased the capacity and duration of our credit
facilities while only tapping them to a limited extent. And we
have only a limited number of debt maturities on the horizon.
2005 will be a critical transition year in which we will be
working intensively with Exelon to prepare for a successful
merger. The Boards of Directors of PSEG and Exelon unanimously
approved the merger agreement, and recommended
that shareholders from both companies approve it. From time
to time, you will be receiving more information about the
merger. We expect that the shareholder meeting to vote
on the transaction will occur in the second quarter of 2005.
We hope to complete the transaction by the first quarter
of 2006, after required regulatory approvals are obtained.
As committed as we are to combining with Exelon, we
recognize that a merger agreement in our industry is subject
to many layers of approval. We are very aware that we must
keep ourselves positioned with a viable stand-alone strategy.
Turning to key objectives for the PSEG family of companies
in 2005:
PSEG Powers number-one goal is to attain consistently excellent
plant operations, with safety first, as the key to capturing
opportunities in the marketplace. Our nuclear services operating
contract with Exelon should materially assist us in reaching
this objective. Another important goal for PSEG Power will
be to continue using successful marketing strategiesin
New Jersey and other marketsto lock in prices for most
of its anticipated output.
PSE&Gs main objective in 2005 will be to continue doing what
it does so well: The essence of its business is to provide safe,
reliable service for its customers at reasonable costs. PSE&Gs
fundamental mission remains unchanged in serving customers
and communities across New Jersey.
PSEG Energy Holdings goal in 2005 will be to continue its
focus on increasing the efficiency and returns of its existing
assets and seeking to opportunistically monetize investments
that may no longer be a strategic fit.
We also will continue to focus on reducing debt, maintaining and
enhancing credit quality, and preserving substantial liquidity.
Risk management will remain a key goal across our businesses.
5 YEAR CUMULATIVE COMPARATIVE RETURNS
AS OF DECEMBER 31, 2004
PSEGs total return for the last five years has outpaced
three major market indices. This chart shows the
value on December 31 of each year of $100 invested
on December 31, 1999. The value assumes reinvested
dividends.

OVER THE PAST FIVE YEARS PSEG DELIVERED
A TOTAL SHAREHOLDER RETURN OF OVER
93 PERCENT, OUTPERFORMING THE MAJOR
UTILITY INDUSTRY AVERAGES AND CONSIDERABLY
OUTPACING THE RETURN OF THE S&P 500.

In 2005, we expect our earnings per share to improve to the range
of $3.15 to $3.35 per share. And we believe the longer-term
picture offers brighter growth prospects. Macroeconomic
developments strongly point toward the continuation of a higher
energy-pricing environment than that prevailing a few years
ago. While relatively few of PSEG Powers long-term supply
contracts will expire in 2005, a much larger percentage will roll
off in 2006 and subsequent years, providing a correspondingly
larger profit opportunity.
As we move toward creating the premier utility of the future,
I want to emphasize that our work carries forward the proud
traditions of PSEG.
PSEG has built its reputation over the years by keeping its
commitmentsto customers, employees, communities and
the environment as well as the investors who put their trust
in us. These commitments continue to guide our organization
and employees.
In 2004, our company undertook significant new philanthropic
initiatives. Most notably, the PSEG Foundation committed
$5 millionthe largest single gift in our companys history
to help build a new world-class Childrens Specialized Hospital
in New Jersey.

A REMINDER FOR OUR SHAREHOLDERS:
Common and preferred utility dividends are now generally taxable at
reduced ratesno higher than 15 percentfollowing the passage of
federal tax legislation in 2003. You may wish to consult a tax advisor
for further information about the tax treatment afforded to dividends.
ACTUAL 2003 ACTUAL 2004 INDICATED 2005
$2.16 $2.20 $2.24
2005 WILL BE A CRITICAL YEAR IN WHICH WE
WILL BE WORKING INTENSIVELY WITH EXELON
TO PREPARE FOR A SUCCESSFUL MERGER.

Once again in 2004, our employees demonstrated the meaning
of commitmentboth in their dedication on the job and through
their many volunteer activities. They were outstanding in
caring for people in needboth in New Jersey and elsewhere.
When Florida suffered four destructive storms in September
2004, PSE&G employee volunteer crews did extraordinary
work in the largest mutual-aid power restoration effort in our
companys history.
The following pages in this report provide examples of our
employees putting their talents and skills to work on behalf
of countless people, and PSEGs continuing focus on the
essentials of success in our industry. You can be assured
of our unrelenting commitment to excellence, as we continue
striving to serve you.