To Our Stockholders

We took important steps to improve financial
performance in 2003, and I am encouraged by our
progress. We reduced debt, improved insurance underwriting
margins, entered promising new markets and
recruited new executive talent. Investment income rose
sharply, and we divested non-core businesses. As we
continue to enhance the productivity of our global
resources, I expect profit margins to grow and earnings
per share to increase.
Aon has a broad client and policyholder base developed
over many years. It is the foundation of our
valuable franchise. Each day, our employees create and
deliver risk management, workforce productivity and
insurance solutions. They exchange ideas through a
vast knowledge management network, giving clients
timely, world-class innovation at the local level. These
strengths, coupled with favorable long-term trends,
will be the catalysts for Aons profitable growth.
While economic and premium rate cycles are
inevitable, we have built resources around our vision
of long-term client needs:
 Risk management is more crucial to corporate business
planning and operations.
 Insurance broker markets are expanding as
countries open their borders and advice becomes
more valuable.
 Globalization has been driving companies to pursue
worldwide growth strategies that require parallel risk
management and consulting resources.
 Workforce productivity is defining the winners in the
marketplace.
 Health and financial security concerns are paramount
to consumers worldwide.
We intend to capitalize on these fundamental longterm
trends.
>> Risk and Insurance Brokerage Services
We achieved healthy revenue growth in our risk and
insurance brokerage segment in 2003. Our U.S. reinsurance
brokerage business had a particularly good
year as more insurance companies sought our services.
We also had strong results in our international retail
brokerage and our affinity business, partly driven by our
client segmentation strategy, which markets specialty
resources to particular industry and market groups.

Revenues also benefited from increasing premium
rates in certain lines of insurance during 2003. So
far in 2004, premium rates are still increasing in some
sectors  such as directors and officers liability,
professional liability and medical malpractice  but
more moderately. For other lines of insurance, such as
property, premium rates are declining on average.
Insurance carriers though are being pressured to
support premium rate levels on several fronts. Interest
rates are at historically low levels. Rating agencies are
requiring more capital. Industry claim reserve deficiencies
are estimated in the tens of billions of dollars.
Property, people and systems are more concentrated 
creating larger and more complicated insurance portfolios
that insurers must carefully manage through
reinsurance. These factors, together with the longterm
growth trends noted above, should support
favorable growth for the foreseeable future. In fact,
we see growth opportunities in most of our businesses
given the expanding complexities of risk management.
Aons primary challenge in the risk and insurance
brokerage segment has been profit margin expansion.
To improve our profitability, we are taking steps in the
following areas:
 Portions of our lower-margin claims services business
were sold in early 2004, and we are evaluating
strategic options for the remaining units.
 We modified U.S. employee benefit programs, and
compensation plans worldwide are being aligned
more closely with bottom-line profitability.
 Strategic sourcing opportunities are being pursued
more forcefully with vendors, suppliers and landlords,
and we will gradually consolidate office space in cities
where we have duplicate facilities.
 Staff-level support functions are being standardized
and more thoroughly streamlined.
 We are refining our client segmentation strategy to
more effectively market our services to targeted client
sectors. Global practice groups specialize by industry,
product and functional expertise. Strategic account
managers focus on the largest accounts.

 Technology and process management spending is
being leveraged across more business units.
 Account profitability analysis is being used more
extensively.
 A country management model is being implemented
in the United States across insurance brokerage and
consulting practices, similar to successful models we
use abroad. It will foster greater teamwork across
business units, and marketing and distribution will
be done more profitably.
 Aon Specialty Product Network (ASPN), which markets
wholesale brokerage and other Aon services to more
than 28,000 independent agents and brokers, will
further penetrate this important client segment.
Many of these actions are designed to improve not only
our brokerage segment margins but also consulting and
insurance underwriting profitability.
International Opportunities
We take great pride in having been named the first
foreign insurance broker to earn a license in China.
In February of this year, I had the pleasure of traveling
to Shanghai to meet with our joint venture partner,
China National Cereals, Oils & Foodstuffs Import &
Export Corp. (COFCO), as well as current and prospective
clients. As Chinas economy expands, so does our
growth potential. We received a similar honor by being
named the first foreign broker in India through a joint
venture with Global Insurance Services. Our license
permits us to offer services to indigenous companies,
as well as multinational companies conducting business
in India. In July 2003, Aon also formed a joint
venture, Aon Rus LLC, with Russian-based LLC
Corporation Risk. Through our offices in Moscow and
St. Petersburg, we offer a full range of insurance and
reinsurance brokerage services.
We have structured our business platform to take
advantage of long-term growth trends, such as the
opportunities in China, India and Russia. Our deep
industry and product resources, coupled with our proprietary
global distribution network, will continue to
be a major growth engine. As we become steadily
more productive, I am confident that revenue growth
will drive margin improvement.
>> Human Capital Consulting
As with our risk management business, we are among
the leaders in the human capital consulting industry
where we deliver a comprehensive set of workforce
productivity solutions. Services range from employee

benefits and human resource outsourcing, to compensation,
management and communication consulting.
In 2003, our margins were compressed by an increase
in lower margin outsourcing business. However, we
expect margins to improve as we realize further cost
efficiencies from the large AT&T outsourcing contract
that began in mid-2002.
Economic growth within the U.S. in the latter
half of 2003 was the strongest in more than a
decade. While job growth has lagged this increase,
Alan Greenspan, head of the U.S. Federal Reserve
Bank, expects domestic companies to begin hiring
more aggressively. Aon would be a beneficiary of
increased hiring, both in our consulting and insurance
brokerage businesses.
Clients are facing tough challenges as healthcare
expenses continue to escalate. Lower interest and
discount rates are exacerbating these problems as
they drive up pension obligations, expenses and underfunding.
While these costs need to be contained, clients
also need to attract, retain and motivate the most
productive workers. Our healthcare and pension benefit
experts consult with clients to balance the conflicting
demands of containing costs and maintaining talent.
Workforce productivity will remain a distinguishing
factor for successful companies, and we expect
client demand for our expertise and resources to
grow over time.
>> Insurance Underwriting
Our back-to-basics strategy in the accident and
health insurance underwriting business has produced
positive results. Margins in this group have improved
as we exit less profitable and less predictable lines, a
process we expect to complete next year. While this
will constrain premium growth in the near-term, we
are targeting continued margin improvement.
We will focus on our competitive strengths  a welltrained
sales force of 7,600 exclusive career agents, a
strong Combined Insurance Company brand name, solid
claims paying ratings, and innovative insurance products.
We recruited a new finance head for this business in
2003 to reinforce our commitment to profit discipline.
To enhance the management team, we also
appointed a new chief underwriter/actuary in our
warranty and select property & casualty insurance
underwriting business. Growth opportunities for our
automobile and consumer product warranties
are influenced by economic trends and buyers who
are more receptive to purchasing warranties

We increased select commercial P&C premiums in
2003, largely from managing underwriting operations
within the brokerage segment. We anticipate lower
growth in 2004 as we resume our traditional practice
of placing the majority of these premiums with independent
insurers.
Underwriting segment financial performance
showed progress in key areas that were targeted at
the beginning of the year, and we look for further
improvement in 2004. We strengthened reserves for a
run-off book of business, National Program Services, Inc.,
in 2003.
>> Summary of 2003 Consolidated Financial
Results and Highlights
Total revenues grew 11% in 2003 to $9.8 billion,
driven principally by risk and insurance brokerage
services revenues that rose more than $700 million.
Net income from continuing operations increased
36% to $663 million, and earnings per share grew
22% to $2.08.
Significant contributors to improved profitability
were insurance underwriting, risk and insurance brokerage
services and equity-related investment income,
principally from our holdings of Endurance Specialty
(NYSE: ENH). We also benefited from foreign exchange
through our extensive international operations that
collectively had strengthening currencies relative to the
U.S. dollar. In addition, we completed the settlement of
our World Trade Center property insurance claim for
$200 million. We sold our automobile finance services
unit eliminating this non-core business.
As anticipated, pension expense and the cash
funding of our benefit plans increased in 2003 due
principally to lower discount rate and asset return
assumptions. While we expect pension costs to rise in
2004, the year-to-year increase will be much lower.
Cash flows improved significantly, and we paid down
debt in addition to pre-funding our pension plans.
Our key leverage ratio (debt plus preferred stock to
total capital) was 33% at year-end, a significant
improvement from 40% the year before.
Our parent company credit ratings and the claims
paying ratings of our insurance company subsidiaries
remain solid. Total assets rose 7% to $27 billion at
year-end and stockholders equity grew 15% to
$4.5 billion.

>> Milestones
George A. Schaefer, an Aon director for 13 years, has
reached the mandatory retirement age and will be
leaving our board in May 2004. We are indebted to
George for his valuable contributions, and we wish
him all the best for the future.
We recruited two new executive vice presidents in
2003. David P. Bolger, our chief financial and administrative
officer, oversees all corporate finance functions,
as well as human resources, information technology,
property management, strategic sourcing and corporate
communications. Earlier in his career, Dave held
senior level finance and management positions in large
financial institutions.
D. Cameron Findlay joined us in August 2003 as
general counsel and is responsible for compliance
and legal affairs globally. He served as U.S. Deputy
Secretary of Labor before starting his new position.
Prior to working in the Labor Department, Cam was a
partner with Sidley Austin Brown & Wood.
Dave and Cam have made important contributions,
and we are fortunate to have them on our executive
management team.
More than 120 employees ran in the New York
City and Chicago marathons as part of Aons
Running to Remember campaign. With every step,
they raised contributions for the children of our
associates lost in the World Trade Center. One of our
colleagues, Greg Donovan, flew all the way from
Australia, was the first team member to cross the
finish line, and raised the largest single donation 
$71,000. This individual and team effort was a
poignant milestone to mark the passing of our
colleagues and the charitable spirit of our employees
around the globe. They have donated countless
hours every year to teach children, paint schools,
support the elderly and help those in need. We are
proud of their accomplishments and their devotion
to their communities.

>> Outlook
We took important steps in 2003 to improve our
financial performance and are building on that
progress in 2004. Our employees are extremely
talented, and they have fostered strong relationships
with our clients and markets through their dedication.
Their commitment is the key factor that makes Aon
such a formidable competitor.
Broadening our client relationships is a major goal
for 2004. Through an increasingly disciplined client
segmentation strategy  supported by senior-level
sponsors  we will efficiently market and deliver more
services to existing clients.
Our second major goal is to increase operational
efficiency. In the U.S., for example, we will use standardization
to streamline staff-related services and
consider the outsourcing of information technology
infrastructure. In addition, since our overriding objective
is improved profitability, we are implementing
stricter guidelines and accountability that will foster
better financial discipline.
I am confident that we will improve stockholder
returns through these actions. Our employees are
enthusiastic, management is committed, and we have
the necessary resources to win in the marketplace.
Sincerely,
Patrick G. Ryan
Chairman and Chief Executive Officer