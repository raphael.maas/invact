Dear Fellow Shareholders:

Im pleased to report that ICE once again achieved
record revenues and earnings in 2009. Amid
significant headwinds in the global financial markets,
these results required dedication and focus.
The year was characterized by the development of innovative risk
management services and our ability to deliver growth in our core
business. This is the strategic vision we established a decade
ago  and what we strive to do through both good times and
challenging times.
The last two years have presented many opportunities for ICE
to lead. Because of our long-standing conviction that central
counterparty clearing would become a cornerstone of over-thecounter
(OTC) risk management, we are uniquely positioned
to anticipate the emerging needs of our customers. By working
closely with regulators and industry participants, and by executing
consistently, we have helped restore stability and confidence in
derivatives markets while also producing superior financial results.
A Record Year in Review
Understanding that growth is what our shareholders expect, we
begin every year with a clear strategic and operational plan to
ensure success, regardless of market conditions. And we again
delivered record operating and financial returns for 2009, despite
ongoing contraction in the U.S. economy. ICEs futures exchanges
established annual volume records, with combined average daily
volume crossing one million contracts for the first time. Average
daily commissions in ICEs OTC energy markets reached record
levels and we meaningfully expanded our service offerings in
credit derivatives. Revenues increased 22% to $995 million,
operating income was $513 million and net income attributable to
ICE was $316 million  each measure a record.
Our results were driven by continued demand from our customers
for an expanded range of risk management products and
services in the many markets we serve. ICEs results  and our
outperformance relative to the exchange sector  are a function
of our unique positioning among futures and OTC markets,
including diversification across market participants, products and
geographies. As detailed below, our early recognition of the vital
role played by central counterparty clearing has already produced
tremendous value for ICE, our customers and shareholders.
Commodities have long been recognized as the building blocks
of development and economic growth. I continue to believe that
the potential of the global commodities asset class remains underappreciated.
First, the use of commodity derivatives  those we list
in our markets  as hedging and risk management tools continues
to grow rapidly. In particular, as emerging economies demand and
consume an increasing proportion of the worlds commodities, I
have no doubt that the demand for access to markets  including
trading, market data and clearing  will continue to grow as well.
This is a thesis we have held for several years, and our results
have proven its validity thus far.

Another driver of growth is automation, including electronic
trading, processing and clearing, which is relatively new to
commodity markets, particularly in the vast OTC markets. By
way of comparison, equities began trading electronically in the
early 1980s, while commodity markets were only electronified in
the late 1990s  and then only in selected products. Oil futures
did not trade on a fully electronic venue until 2005, when ICE
transitioned ICE Futures Europe from floor to screen trading. Since
that time, volume in our energy markets has grown four-fold. Our
agricultural and financial futures volumes have nearly doubled
since transitioning to electronic trading just two years ago. Further,
trade automation in the OTC markets is at a very nascent stage.
Significant Operational Achievements
As more customers than ever relied upon ICE in 2009, our markets
achieved healthy growth. At ICE Futures Europe, volume rose 8%
over the prior year. Contract volume in Brent crude and gasoil
futures, as well as coal, emissions and natural gas futures,
reached record levels, driven by growing demand for these
global benchmarks as key hedging tools. At ICE Futures
U.S., record volume in the U.S. Dollar Index and the
first full year of trading in the Russell Index futures
drove strong performance, while agricultural
futures volumes remained more subdued by
credit market conditions in the agricultural
sector.

We also had our best year yet in our OTC energy markets, with
average daily commissions up 8% year-over-year to $1.2 million.
As a result of the launch of ICE Clear Europe in November 2008,
we began realizing the financial and operational benefits of inhouse
clearing in our energy markets. We introduced more than
200 cleared energy swaps in 2009, nearly tripling the number
of contracts previously available. As market participants and
policymakers continue to absorb the lessons of the financial
crisis  recognizing the benefits of contract standardization,
transparency and risk mutualization that clearing offers 
cleared volume has increased to 96% of ICEs total OTC energy
contract volume in 2009, from just 84% in 2006.

I would like to take a moment to note that the performance of our
exchange business could not have been accomplished without
the sophisticated technology infrastructure weve developed.
ICE has established industry-leading performance, flexibility and
reliability in our trading platform and clearing systems. Based
on its unparalled accomplishments in our sector, I believe our
technology team is second to none.
After acquiring Creditex in August 2008, ICE took a leadership
role in working with industry participants, regulators and
legislators globally to develop centralized clearing for credit
default swaps (CDS). In March 2009, ICE launched ICE Trust,
which became the first clearing house to clear transactions
in the $26 trillion credit derivatives marketplace. ICE Clear
Europe initiated clearing services for European CDS in July.
Through February 2010, ICE cleared $6.3 trillion in gross
notional value, resulting in combined open interest of $514
billion. To date, gross notional value outstanding for cleared
contracts has been reduced by over 90%, representing a
meaningful reduction of systemic risk in the global CDS
market. And despite being less than one year old, our CDS
clearing business contributed over $30 million in revenues in
its first nine months of operation in 2009.

We believe our comprehensive clearing infrastructure  spanning
asset classes and geographies  is a significant competitive
advantage. Derivatives clearing cannot be approached with a onesize-
fits-all solution; it is not a custodial or bookkeeping business.
It recognizes the interrelatedness of markets and member firms to
account for risks broadly, while balancing the security and efficiency
of capital. It requires a highly specialized infrastructure, including
domain knowledge, tested quantitative risk models, intellectual
property, technology, and industry and regulatory relationships.
Development of that infrastructure has been a function of
ICEs core capabilities: customer and market responsiveness,
innovation, global presence and entrepreneurial capital.
Finding Opportunities in Change
As entrepreneurs, we take calculated risks and make investments
to reach new markets  rather than waiting for markets to come
to us. This has allowed us to penetrate OTC markets more
deeply and to grow, even during contractions in the business
cycle.
Given todays complex business and political climate, the need
for increased regulatory oversight in virtually all realms of
financial services around the world is clear. Its the right thing
for our customers, shareholders, and citizens globally, who play
a role in and are affected by financial markets. ICE remains
active in the regulatory dialogue and we support thoughtful, welldesigned
policies that foster  but do not constrain  vibrant
markets with widespread participation. Toward this end, weve
worked hard to help educate policy makers on the functioning
of markets and the essential role markets play, including their
ability to detect problems and send signals that corrections are
needed. In fact, markets themselves are today being viewed
as a solution to many problems weve seen in the past, with
influential policy leaders expressing support for the increased
use of electronic trading platforms and clearing houses. These
capabilities are at the heart of our business model.
As ICE approaches its ten-year anniversary, Id like to take a
moment to reflect on the unique business weve established in our
companys first decade. Weve integrated centuries-old exchanges
and clearing houses and combined them with modern technology,
innovation, customer service and robust financial management
practices to effectively serve market participants around the
world. Equipped with an unrivaled market infrastructure, optimism
and an entrepreneurial corporate culture, we approach the new
decade with confidence in our ability to transform challenges into
opportunities, and to create value for our shareholders.
Thank you for your continued interest in ICE.
Jeffrey C. Sprecher
Chairman and Chief Executive Officer
April 5, 2010
