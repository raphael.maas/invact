From the executive chairman

strategy
for success

Our long-term strategy has enabled us to
establish Oxy as a strong performer in our
industry  globally respected, financially
sound, with an enviable balance sheet and
great promise for generating future value
for our stockholders.

Our top priority is to create value for
our stockholders by focusing on total
shareholder return. Achievement of
this goal demands that we bring sound
judgment and stewardship to our
operations and assets. Near term, we must
focus on achieving healthy annual returns.
In planning longer term, we must identify,
and invest in, assets and projects that will
create growth.
In 2012, we experienced disappointing
stock performance, resulting from several
factors. Rising costs, primarily in our oil and
gas operations, contributed to lower
margins. In this regard, we fell short of
expectations in 2012. To remedy this, we
are taking aggressive steps to improve
operational efficiencies without impacting
overall production growth or compromising
our standards regarding health, safety
and the environment. These actions are
designed to improve profitability and total
shareholder return in 2013 and beyond.
Reflecting their confidence in this strategy,
the Board of Directors in February 2013
approved a dividend increase of 18.5
percent, to $2.56 per share. It was the
twelfth dividend increase in 11 years.
Throughout 2012, we invested in assets
and projects that we believe will
strengthen our companys performance
for years to come.
In the United States, we continue to
invest in oil and gas properties with a
focus on growing our oil production. We
acquired properties in some of the most
prolific oil basins in the continental United
States  in the Permian Basin in West
Texas and New Mexico; in California; and
in the Williston Basin in North Dakota. In
addition, we are increasing our gas
processing and oil pipeline transportation
capacity in key producing areas of West
Texas, and building a membrane cell
chlor-alkali plant in Tennessee.
In the Middle East, we are investing
significant capital along with the Abu Dhabi
National Oil Company (ADNOC) in the
Al Hosn Gas Project, which is expected
to become a strong contributor to Oxys
cash flow once it becomes operational
in late 2014.
Our longstanding Middle East operations
continue to be highly successful. In Qatar,
we are making good progress on the fourth
phase of field development of Idd El Shargi
North Dome, and we have received
approval for ongoing field development
of Idd El Shargi South Dome and Al Rayyan,
including additional drilling through 2013.
At the Mukhaizna Field in Oman, where
we have a world-class steam flood for
enhanced oil recovery, average gross
daily production in 2012 was more than
15 times the production rate of September
2005, when Oxy assumed operations.
Our long-term success would not be
sustainable without an unwavering

commitment to social responsibility:
maintaining high ethical standards, and
conducting our business in a manner that
safeguards and benefits our employees,
our neighbors and the environment.
We help strengthen local economies
where we operate by creating jobs and
ensuring our workers are provided the
best possible training. We also contribute
significant tax revenues, and purchase a
wide range of local goods and services.
All these activities play a significant role in
supporting and enhancing the communities
where we operate, and where our
employees live.
At every Oxy business location, safety is
our foremost priority. Our 2012 worldwide
employee Injury and Illness Incidence Rate
(IIR) of 0.33 was the best in our history  and
significantly better than the 2011 all-U.S.
industry average IIR of 3.5.
Our commitment to environmental
stewardship ranges from cost-effectively
reducing emissions at our plants and
facilities to supporting biodiversity and
habitat conservation programs on our
properties. We are proud to have received
recognition from government agencies
and the Wildlife Habitat Council for our
environmental performance.
In addition, a number of prominent
publications have ranked Oxy among the
national and global leaders in corporate
citizenship. In 2012, we were recognized
by Fortune, whose Worlds Most Admired
Companies survey ranks Oxy No. 1 in the
Mining, Crude-Oil Production category;
Barrons, which includes Oxy on its list of
the Worlds Most Respected Companies;
and Corporate Responsibility Magazine,
which lists Oxy among the 100 Best
Corporate Citizens.
It is the people of Oxy who make us a
strong, growing company with an exciting
future. I want to thank our talented and
hard-working employees and management
team for their daily dedication to excellence,
and my fellow members of the Board of
Directors for their invaluable advice
and counsel.
Our stakeholders around the world count
on Oxy to deliver top performance and
added value. We work hard every day in
every way to meet those expectations, and
we look forward to a successful 2013.
Ray R. Irani
Executive Chairman