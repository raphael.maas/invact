To Our Shareholders:
The rapid pace of change in technology and consumer behavior is leading to unprecedented shifts in the financial services industry. With change comes opportunity, and our employees responded last year, driving strong company performance and positioning us for continued success.
I am pleased to share Discover's 2018 results with you and proud of the people who achieved them. Throughout  my 20 years with the company­ and now as CEO-I have been honored to work with and represent employees who are united by shared values and who are committed to making a difference every day for our customers, owners, the community and one another.
In this Annual Report, you will see the results of these efforts, which culminated in increased growth and profitability for Discover. You also will hear from a few of the employees who contributed to our success through their focus on the customer and on developing innovative products and user-friendly technology. They represent the more than 16,000 employees who helped Discover deliver on our Mission of helping people achieve a brighter financial future.

A Look at Our Performance

 
Through our efforts in 2018, Discover achieved 7% growth in total loans. We funded those loans, in part, with the help of a 13% increase in deposits. We also brought on 11% more new card accounts and drove a 12% lift in total network volume. Consistent with our expectations, we experienced higher charge-offs due to supply-driven  credit normalization and continued loan growth. Discover's net charge-off rate for the year was 3.06%. All of this resulted in strong profitability as we grew revenues, controlled expenses and remained focused on risk management.
Diluted earnings per share were $7.79 and return on equity was 25%.
We repurchased 8% of our shares and increased dividends for the eighth consecutive year, returning significant capital to our shareholders.
 










Roger C. Hochschild Chief  Executive Officer and  President


DISC	 
 





A Look at Our Performance (continued)
I am also pleased to share that in addition to driving strong results in 2018, we continued to launch new and enhanced products and features that are expected to help Discover continue to grow in 2019 and beyond.

Discover Card
Credit card rewards are a very important factor when consumers are deciding which card to apply for or use, and Discover has long been a leader when it comes to rewards.
In 2018, we continued to enhance our rewards features, including announcing
a full year of rewards categories for our popular 5% Cashback Bonus program so card members can maximize the value of the program, and introducing a Rewards Tracker so they can see cash back earned and redeemed, and Pay with Cashback Bonus on PayPal to give them yet another way to redeem rewards.
We also continued to enrich the customer experience, which is anchored by our 100% U.5.-based customer service . As part of our strategy to communicate with
customers via whatever channel they prefer, we are transitioning from "chat" to in-app messaging, which makes it easier for them to initiate and continue conversations with us at their convenience .The improvements we made to the award-winning Discover Mobile App in 2018-including a more user-friendly homepage and use of artificial intelligence-helped us earn recognition in 2018 as a Digital Edge 50 Award winner .
In addition to enhancing credit card features and customer service, we launched two new products that add to the depth and breadth of the Discover product suite:

	The Discover it Business Card offers the same great features as our other credit cards, along with unlimited 1.5% cash back on purchases and free tools to help small business owners manage their finances.

	Our Identity Theft Protection product for cardmembers monitors three credit bureaus and other data sources for potential identity threats while providing more features and value than competing products.

With the help of these and other enhancements, and our continued focus on service and value, we grew card loans 8% and new card accounts 11% while decreasing acquisition costs per account by 6%.
 







Samika Mendoza, Team Leader, Customer Care Center, Phoenix

When Samika Mendoza came to Discover in 2015 after several years in the service industry, it was an eye-opener. "I was blown away by the focus on the customer and being trained on how to give great service."

Samika manages a team of customer service agents. "The question I always ask my team: 'Are you doing the right thing for the customer?"' That includes sharing all the information they need to make informed decisions.

"At Discover, we pay attention to every detail and show customers that we care. For example, a government employee called during the shutdown because he was stressed about paying his bill. When one of the agents on my team thanked him for his service, he broke down in tears because he felt appreciated. Then they talked about how Discover could help. The message was: 'We have your back."'

It's no wonder Discover was ranked highest for credit card customer satisfaction in the U.S. by J.D. Power for the fourth time in 2018.








Con sumer Banking
Beyond credit cards, Discover offers a suite of deposit and lending products intended to help people manage their finances and achieve a brighter financial future.
	Deposits: In 2018, we celebrated a milestone when we surpassed $40 billion in consumer deposits, marking a tenfold increase over the past decade. The most significant product development in 2018 was the addition of new features to our broad market checking product, which pays an industry-leading 1% cash back on up to $3,000 in qualifying debit card purchases each month. Discover Cash back Debit has a new eye-catching vertical card design, a redesigned mobile app, and acceptance at 60,000 no-fee ATMs in the United States.

	Student Loans: We achieved substantial growth in our private student loan business over the past few years by engaging with students and parents earlier in the decision-making process, offering features like no fees and Rewards for Good Grades, and simplifying the application process. In 2018, we launched an Interest Only product, and allowed co-signers to pre-qualify with no impact to their Credit Bureau report. Despite intense competition, we continued to gain market share over the past year, with more than 13% growth in originations.
 






Stephen Goates, Department Manager - Deposits, Lake Park, Utah

Stephen Goates was a college student in need of a part-time job when he started at Discover's Customer Care Center in Lake Park, Utah, in 2002. Today, he plays a key role as part of a team that offers distinctive customer service for new and existing deposits products.

"As we listened to our customers, we realized that many prefer digital banking and regularly use a debit card. So we asked ourselves, 'How can we provide them with a really great deposit product that helps them avoid debt and save for the future?"'

The team came up with Discover Cashback Debit, a new checking product that rewards customers with 1% cash back on their debit purchases.
To promote saving, customers can have their rewards automatically transferred into an online savings account.

"We hoped it would bring in new customers and lead to a relationship that would continue to grow." Indeed it has-about 50% of the applicants are new to Discover.








	Personal Loans: In 2018, we upgraded our capabilities to improve credit quality and fraud monitoring. We also worked to simplify the application process and offer value-added features like our free Credit Scorecard. These efforts better position us for future success and long-term profitability.
	Home Equity Loans: Over the past five years, we grew originations and gained efficiencies by focusing on simplicity, innovation and lean processes. Last year, we more than doubled funded loans while lowering processing costs by more than 30%. We are outpacing the market and are now one of the five largest originators of closed-end second mortgages.

Payment Services
In the world of digital payments, the keys to success are acceptance when and where there is demand, and via any channel of choice, using state-of-the-art technology. That has been the focus as we work with our network alliance partners and Diners Club and PULSE Debit Network issuers.
 
	Network Partners: With the goal to increase our international presence, we expanded on existing acquiring relationships with JCB International and First Data in Hong Kong. We also signed new strategic alliance agreements with Prosa,
a leading domestic payments network in Mexico, and with NCCC Global Cards in Taiwan. Today, our global payments acceptance footprint includes nearly 45  million merchant locations.

	Diners Club International: Last year, we continued to expand the Diners Club brand, signing two new issuers-Elo in Brazil and PPCBank in Cambodia. With more than 120 licensees, we achieved 7% transaction volume growth over the past year.

	PULSE: Discover's debit network posted another strong year in 2018, signing 155 new or renewed agreements with issuers and growing dollar volume by 14%. To improve the customer experience and deliver additional value to network participants, PULSE launched a new disputes system and a new fraud detection platform.








Alejandro Orestano, Senior Relationship Manager, Payment Services, Riverwoods, IL

When Alejandro Orestano joined Discover five years ago, our payments acceptance footprint in Mexico had room to grow. As part of the Discover Global Network team, he works to develop new network partnerships.

"It takes a great deal of time to build these relationships. We start by figuring out what the client needs, and then by sharing Discover's unique solutions and perspectives on the industry, we help them expand their business."

In 2018, Alejandro helped seal the deal on a strategic alliance agreement with Prosa, a leading domestic payments network in Mexico, which is an important tourist destination.

"Once the program is implemented, Discover will be accepted at more than 400,000 additional point-of-sale locations and 24,000 ATMs across Mexico, improving the experience when our cardmembers travel."

It's a win-win for both sides.
 
Recogn ition of Our Effo rts
In a year marked by strong financial results and accomplishments, Discover received a number of important awards and honors.
In 2018, Discover was ranked "Highest in Customer Satisfaction with Credit Card Companies" by J.D. Power for the fourth time in five years. Continuing a 22-year streak, we also were named #1 for brand loyalty in the Credit Card category, according to Brand Keys.
More recently, our Discover it and Discover it Secured cards were included on NerdWallet's annual Best-of Awards list for 2019. And our Discover it Card and Discover it Chrome Card for Students were ranked among the best rewards cards by Kiplinger's Personal Finance in 2018.

Our People
Behind these great products, services and awards are our employees. They are the key to our success, and we are committed to ensuring that their experience at Discover is exceptional.
In 2018, we gave employees more than one million hours of training and development to help them grow their skills and advance in their careers. We also provided nearly 1,000 employees with tuition assistance to help them further their education at 113 universities so that they can realize their dreams. And we promoted more than 5,000 employees, providing them with new challenges and more opportunities to play a bigger role in-and benefit from-the company's success.
In addition, we launched a number of new or improved employee benefits. These included a full ride to select online bachelor's degree programs, doubled paid parental leave for the primary caregiver following the birth or adoption of a child, a higher 401(k) matching contribution, access to discounts and savings at thousands of retailers and, most recently, a program for child and elder care.
The net effect of these investments is that our employees feel supported and respected. Year after year, their voice in external surveys has resulted in Discover being recognized as a "top place to work" in each of our locations. We have also been recognized for our efforts to make Discover a welcoming and inclusive place to work.

Giving Back is at Our Core
Volunteerism and a commitment to helping others are an important part of the Discover culture. Last year, our employees made a real difference by volunteering more than 66,000 hours of their time to support the work of local non-profit organizations across
 
the country. Discover employees taught financial literacy to nearly 8,000 children, filled thousands of school backpacks with supplies, built homes and playgrounds, conducted clothing and food drives, and packed 140,000 meals for the hungry.
In addition to their time, employees donated money to charities through our You Care,
We Share program, taking advantage of a match from Discover to make their giving go further. In the aftermath of Hurricane Michael, Discover worked with employees and cardmembers to raise money through a matching campaign of Cashback Bonus and direct donations, giving $2 million to the American Red Cross for disaster relief. Discover also worked directly with customers who were affected by natural disasters in the last year to waive fees and ease their financial burden.
As a company, our charitable giving aligns with our Mission to help people achieve a brighter financial future. Through our Pathway to Financial Success program in 2018, we invested more than $5 million to bring free financial education curricula and resources to middle and high schools across the country, as well as community colleges in the areas where our employees live and work.







Ying Zhe, Director - Application Development, Business Technology, Riverwoods, IL

In digital banking, technology is essential to ensuring customers have fast and easy access to products and services. That's why Ying Zhe and her team are building a modern acquisitions platform to enhance Discover's credit card application experience across all channels. Using the latest design techniques and automation capabilities, they are creating a system that adapts and evolves based on customers' needs.

"Applying for a Discover card is most likely a customer's first interaction with us, so it's really important that we create a best-in-class system that provides a great experience."

Working side-by-side with product owners, her technology team is leveraging the new platform to significantly reduce friction, and using machine learning models for underwriting, both of which will allow Discover to provide cards to more people.

"I truly believe right now is the best time to be in technology. I am excited to be part of an organization that takes advantage of technology innovations to create great products and experiences for our customers."
 







Our Path in 2019 and Beyo nd
As we look to the future, Discover is focused on growth in key areas: total loans, consumer deposits and partnerships to expand our global network.
We are making significant investments in new capabilities such as data analytics and machine learning, digital payments and the upgrade of technologies as we continue to be a prudent lender, manage risks and control our expenses.
I am confident in our ability to succeed because:

	We have the right model-direct  banking with  100% U.5.-based customer service, and proprietary global credit and debit networks.

	We have the right credit, lending and savings products and features-products that add value and features that fill the needs of today's customers.

	And, most importantly, we have the right people-employees who are committed to providing our customers with an industry-leading experience!

I want to thank our Board of Directors for their dedication and support, and our more than 16,000 employees for working hard, contributing to our ongoing success and putting our customers first every day.


 

Roger Hochschild
Chief Executive Officer and President March 21, 2019
