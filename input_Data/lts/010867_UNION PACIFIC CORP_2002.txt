In 2002, Union Pacific raised the bar  celebrating our 140th
anniversary with a record performance. We remained true to
our corporate vision and, as a result, we had a tremendous year.

Our annual report will detail the
accomplishments of 2002, but let
me share some of the highlights:
Commitments Made and Kept
Our targets for 2002 were to
achieve 1 to 3 percent revenue
growth, a rail-operating ratio below
80 percent, free cash flow of $350
million to $450 million, and
improved returns. Im pleased to
report Union Pacific met every goal.
 Revenues grew 4 percent to a
record $12.5 billion, with solid
increases at both the railroad
and Overnite.
 Excluding Overnite, the company
lowered its operating ratio to
79.8 percent  the first year
weve been below 80 percent
since the SP merger.
 Free cash flow after dividends
totaled $528 million.
 Return on total capital improved.
These achievements resulted in
record net income of $1.34 billion
or $5.05 diluted earnings per share.
Even excluding land sales to the
Utah Transit Authority and Santa
Clara Valley Transportation
Authority, as well as various tax
adjustments, diluted earnings per
share rose by 14 percent.We are
building value for our shareholders
by using increased profits to
reduce debt levels and raise our
dividend by 15 percent.
The stock market was turbulent
in 2002, buffeted by the sluggish
economy and several inexcusable
instances of corporate fraud. Still,
Union Pacifics stock price
increased for the third straight year
 rising 5 percent from $57 to nearly
$60 per share, compared to a 23
percent decrease in the S&P 500.
Defining the Yield Strategy
Union Pacifics revenue growth
has outpaced the nations Gross
Domestic Product (GDP) every
year since 1999. The key has been
the implementation of our Yield
Strategy. The strategys major
components are simple:
 provide premium service for a
premium price,
 achieve a mix of business that
maximizes profitability,
 leverage volume across the system,
 increase productivity by working
smarter, and
 maximize asset utilization.
In all, our yield strategy is leveraging
greater value and improved
returns from our superior rail
franchise.
Service Reliability and Efficiency
Service reliability and efficiency
are the linchpins of the yield
strategy. Through a combination
of new product development,
corridor initiatives and alliances,
were building service reliability
in an effort to capture market
share from trucks. The economies
of rail beat truck almost every
time. Our challenge is to provide
truck-like service and reliability.
We continually work to improve
reliability.Were narrowing the
service gap and customers are
rewarding us with their business.
Some of the statistics we use to
evaluate our service reached
record levels for the newUnion
Pacific in 2002: Service Delivery

Index, Train Plan Compliance,
Customer Satisfaction Index and
To/From Industry Compliance.
Our record service performance
was coupled with record business
volumes  transporting 6.1 million
vehicles, 1.5 billion board feet of
lumber, 241 million tons of coal,
32.3 million tons of rock, and nearly
1.6 million international steamship
containers.And, in keeping with
our great history of productivity
improvements, these volumes
were moved with fewer resources.
During a time of increased carloadings,
cycle-time improvements
actually enabled a 7,000-car
reduction in freight cars on line.
Quality Focus
Were building our reputation
as a quality service provider.
During 2002, we became a threetime
winner of Toyotas
Presidents Logistics Award and
DaimlerChryslers Gold Award.
UPS Autogistics named us
Carrier of the Year for our service
to Ford, and we won GMs Mark
of Excellence Award and Supplier
of the Year Award. In early 2003,
GM recognized Union Pacific as
one of only eight suppliers, worldwide,
for outstanding service.
Through our Cost of Quality
program, we proactively monitor
the cost of not doing things right
the first time (failure costs).We
work daily to eliminate these
unnecessary expenses. Using
quality and process improvements,
we identified failure cost
savings of $133 million in 2002 
furthering our belief that it costs
less to run a quality company. And,
our quality efforts were recognized
by our selection as a finalist for
the Malcolm Baldrige National
Quality Award for the second
straight year.
Corporate Responsibility
Not only are we raising the bar
operationally, Union Pacific is
also setting itself apart in the corporate
governance arena. Over
the past several years, weve
adopted corporate governance
best practices and we will continue
to examine our policies to
promote effective governance.
We are setting ourselves apart
as a corporate leader  building
communities in a small way
through our Principals
Partnership program. This new
program, focused on secondary
education leaders working in our
service territory, has already
received an overwhelming
response. As we continue to roll
out the program across our system,
were reinforcing a vital link to our
communities and to our future.
World-Class Employees
Ive been privileged to work for
Union Pacific for 43 years. I enjoy
meeting with employees and discussing
our companys future. This
year,members of our senior team
visited every service unit, talking to
employees about their jobs and
their ideas.We saw firsthand the
building pride within the UP
family. As a company, our bench
strength has never been greater.
Weve promoted talented individuals
from within and recruited
great folks from other industries to
provide fresh perspectives.
Overnite Corporation
Overnite also had a tremendous
2002 despite tough market conditions.
Leveraging its expanded
service area, Overnite is building
networks. Its accessing new markets
with great success  attaining
revenue and net income levels
that havent been seen since 1994.
The year also marked the end of
the Teamsters three-year strike
against Overnite and a competitors
forced closure provided new
business opportunities.
Looking Ahead
Union Pacifics future has never
been brighter. Our unparalleled
franchise cannot be replicated,
and our yield strategy will help us
achieve continued success.
Undoubtedly, 2003 will have
challenges. The economic picture
remains cloudy, threats of war
continue to loom and investor
confidence remains shaken.We
cannot control these factors, but
we can control our focus  margin
improvement, increased returns
and strong free cash flow.
We will build on our strong
tradition of total quality
management, recognizing that
quality processes are vital to the
cycles of improvement making
Union Pacific a better company,
one where:
Customers want to do business,
Employees are proud to work, and
Shareholder value is created.

Dick Davidson, Chairman and Chief Executive Officer
