                                 A MESSAGE FROM THE PRESIDENT AND CEO
                                      ONE TEAM, ONE PLAN, ONE GOAL...                    ONE TEAM
                                                                                              When I arrived at Ford I was extremely
                                      ONE YEAR                                           impressed by the talent and dedication of its
                                            Last year in this report I introduced you    people. Since then, we have worked hard to
                                      to the principles, practices and priorities Ford   unleash their full potential by building a real
                                      Motor Company began operating under after          team and creating an atmosphere of trust.
                                      I joined the team in September 2006. I would
                                                                                               We began with the senior leadership
                                      like to take this opportunity to update you
                                                                                         team, streamlining positions and moving
                                      on the progress we made in our first full year
                                                                                         several people up to head unified global
                                      and our plans for the future.
                                                                                         organizations. For example, all of our product
                                           Although a financial statement is just a      development operations worldwide now
                                      snapshot in time, the numbers we achieved          report to one person, Group Vice President of
                                      in 2007 are encouraging. Our overall net loss      Global Product Development Derrick Kuzak.
                                      of $2.7 billion represented an improvement         Our purchasing, manufacturing, quality,
                                      of nearly $10 billion over 2006. Excluding         communications and several other functions
                                      special items, we achieved a pre-tax profit        also were given a global structure to make
                                      from continuing operations of $126 million. All    better use of the company's worldwide assets.
                                      of our Automotive operations were profitable
                                                                                              We added one of the most talented
                                      outside of North America, excluding special
                                                                                         marketing executives in the industry, Jim
                                      items, as was our Financial Services sector,
                                                                                         Farley, as group vice president of Marketing
                                      which earned a pre-tax profit of $1.2 billion.
                                                                                         and Communications. And we created
                                            Our worldwide Automotive revenue,            the industry's first senior executive for
                                      excluding special items, was $155.8 billion in     sustainability, naming Sue Cischke as
                                      2007, compared with $143.3 billion in 2006.        our senior vice president of Sustainability,
                                      Automotive gross cash (including cash and          Environment and Safety Engineering.
                                      cash equivalents, net marketable securities,
                                                                                              I meet with our senior leadership team
                                      loaned securities and short-term Voluntary
                                                                                         once a week in a half-day session to review
                                      Employee Beneficiary Association (VEBA)
                                                                                         progress toward our goals and address any
                                      assets) at December 31, 2007 was $34.6
                                                                                         roadblocks. Individuals are held accountable
                                      billion, an increase of $700 million from the
                                                                                         for delivering results, but we work together as
   previous year-end.
                                                                                         a global team to find the best solutions. Even

                                          As pleased as I am by these results, I am      though we are a relatively new team, we are

                                     even more excited about the progress we are         already performing and operating at a high

                                     making behind the scenes in the key areas           level, and we continue to improve.

                                     that I outlined last year.
                                                             The best example of what I mean when
                                                        I talk about working together to make Ford a
                                     ONE FORD
                                                            vibrant and successful company is our new
                                          In last year's report, I described our
                                                             agreement with the United Auto Workers.
                                     efforts to fully leverage the tremendous
                                                                       In November 2007, we signed a four-year
                                     worldwide resources of Ford. We offer a full        national labor contract with our UAW partners
                                     line of products in a fiercely competitive global   that significantly improves our competitiveness
                                     industry. To achieve profitable growth we need      going forward. This landmark agreement will
                                     to take advantage of every potential economy        enable us to invest in the new products that
                                     of scale and best practice we can find. That        are a key element of our global plan.
                                     means operating as one team around the
                                     world, with one plan and one goal  which           ONE PLAN
                                     is what we did in 2007.
                                                                                              Our worldwide team is focused on
                                         We came together as a global team,              four key priorities: aggressively restructuring
                                     working with mutual respect and trust. We           to operate profitably at the current demand
                                     looked at the difficult challenges we faced         and changing model mix, accelerating the
                                     and created a plan that dealt with the reality      development of new products that our
                                     of our situation. We set a goal that would          customers want and value, funding our plan
                                     create value for our extended family of             and improving our balance sheet, and elevating
                                     customers, employees, suppliers, dealers            to a new level of performance our commitment
                                     and shareholders around the world.                  to working together with all of our partners.
                                         And then we got to work.                             These are the same priorities I outlined in
                                                                                         this report last year, and they will remain our
                                                                                         priorities for the foreseeable future.

                                                                                      
                                                                                      Autocar magazine's annual
      The highlights of our efforts in support of our plan in
                                                                 "Car Company of the Year" award.
      2007 included:


       match demand and improve productivity.
                                                                 performance on the Euro NCAP Top 10 list, giving
                                                                 Ford Europe the highest number of vehicles in the
       mix and exchange, excluding special items).
                                                                 top 10 for adult occupant protection.

       best in the business and earned us a number of
                                                                 year-over-year.
       strong endorsements from third parties such as
       Consumer Reports and J.D. Power and Associates.
                                                                 launched operations at our new assembly plant in
                                                                 Nanjing to produce small-car models for both Ford
       Focus, Mustang Bullitt, Mercury Sable and Ford
                                                                 and Mazda.
       F-Series Super Duty, which kept us on track to
                                                                    The progress we made in 2007 validates our
       achieve our plan of making 70 percent of our
                                                                plan and encourages us to continue working toward
       North American lineup new or significantly
                                                                our goal.
       upgraded by the end of 2008.

                                                                ONE GOAL
       utility vehicles led the fastest-growing market
                                                                    Our goal has not changed  we want to build
       segment with a sales gain of 62 percent.
                                                                products that are immensely desirable so that we can
                                                                grow profitably. As we work toward that goal, you will
       and first convertible ever to earn five stars  the      see increasing numbers of new product introductions
       highest possible safety rating  in all categories       from us around the world.
       from the National Highway Traffic and Safety
                                                                     In North America alone we will have six major
                                                                launches this year, putting us over the top of our goal
       Sable also earned five-star rating crash-test ratings
                                                                to deliver a 70 percent new showroom. This wave
       from NHTSA, as well as Top Safety Pick ratings
                                                                of new products includes the Ford Flex crossover,
       from the Insurance Institute for Highway Safety.
                                                                hybrid options for the Ford Fusion and Mercury Milan,
                                                                the new flagship Lincoln MKS sedan, and the new



                                                                                      A MESSAGE FROM THE PRESIDENT AND CEO
                                   F-150 will join the F-Series family, which has      industry as well as a slowing U.S. economy.
                                   31 consecutive years of truck leadership.           To help us deliver on our commitments in this
                                   Other new product launches in 2008 include          difficult external environment, we will be taking
                                   the Ford Focus, Kuga crossover and Fiesta in        further cost reduction actions in North America.
                                   Europe, Ford Mondeo in Asia and Ford Falcon         In addition, we will continue to accelerate the
                                   in Australia.                                       flow of new products and adjust production to
                                                                                       the changing business environment.
                                        As we go forward, the mix of vehicle
                                   models we offer will be much closer to where             At the conclusion of my message last
                                   the market is going. For example, small cars        year I said that the results of our efforts would
                                   have reached nearly 45 percent of total industry    be more readily apparent in time. Our early
                                   sales worldwide, a level never before achieved.     results are now in, and they are promising.
                                   We are positioning ourselves to take advantage      In the months ahead you will see more of
                                   of this trend with Ford Fiesta, an exiting new      the building blocks of the new Ford Motor
                                   small car we will introduce in Europe, Asia,        Company start to emerge. As they do, I hope
                                   South Africa, Australia and the Americas            you will share in the excitement we feel as
                                   between 2008 and 2010.                              we work together to create a dynamic global
                                                                                       enterprise growing profitably around the world.
                                          Importantly, the new vehicles we
                                   introduce around the world will offer improved           One team, one plan, one goal  one Ford.
                                   fuel economy and reduced environmental
                                   impact. In the short term, we will feature
                                   hybrids, clean diesel engines, flexible-fuel
                                   ethanol vehicles and a turbocharged direct-
                                                                                       Alan Mulally
                                   injection system we call EcoBoost. EcoBoost
                                                                                       President and Chief Executive Officer
                                   provides up to 20 percent better fuel economy
                                                                                       March 6, 2008
                                   and 15 percent fewer CO2 emissions, as well
                                   as superior performance. It will be available
                                   in half a million Ford, Lincoln and Mercury
                                   vehicles annually in North America during the
                                   next five years.
                                         For the longer term, we are exploring
                                   the potential of plug-in hybrids, fuel cells,
                                   hydrogen internal combustion engines and
                                   other advanced technologies in laboratories
                                   and test fleets around the world. These
                                   actions are not only the right and responsible
                                   thing to do, they also will give our customers
                                   the kind of products they want and value.

                                   LOOKING FORWARD
                                         In many ways 2008 will be like 2007 
                                   a year of transition. Our automotive operations
                                   will continue to improve, but will face the
                                   ongoing challenges of our highly competitive



