A MESSAGE
FROM THE CHAIRMAN AND CEO

Time Warner had an outstanding year in 2013, and as we look back on
it, I first want to acknowledge the inspiring work of our employees
and our creative partners. What we all share is a dedication to light
up the world with the best storytelling across technological frontiers and
geographic boundaries. At the same time, our company has an unwavering
commitment to deliver strong financial returns to our shareholders, and
were really pleased not just with the progress weve made so far but also
with how weve been investing to position our company for the future.
As the preceding pages highlight, weve been following a clear strategy
we established several years ago to become the worlds leading video
content company, with an emphasis on television. And its been working,
both financially and creatively. Turner Broadcasting, Home Box Office, and
Warner Bros. all just completed the most profitable year in their histories.
Our strategy is our guide, but its our people, brands, and leading scale
that attract the best talent with the best ideas, and then help creators and
journalists realize their visions and share them with the world.
From a creative perspective, we continue to set new standards 
exemplified by Warner Bros. 2014 Academy Award-winning films Gravity,
The Great Gatsby, and Her. What all these films have in common is not only
pioneering use of (and, in the case of Her, exploration of) technology, but
also a powerful narrative core that audiences connected with deeply and
emotionally. With 10 Oscars  led by seven for Gravity, including Best
Director for Alfonso Cuarn  Warner Bros. films received more Academy
Awards than all the other major studios combined. Financially, Warner Bros.
also had its best theatrical year on record and it ranked #1 at the domestic,
international, and worldwide box offices, with its films grossing over $5
billion at the worldwide box office. It was also the fifth straight year that
Warner Bros. films grossed over $4 billion at the worldwide box office, an
industry record.
Technology is not only making our films, television programming, and
journalism more vibrant and easily accessible to audiences, but has also
helped enable an unprecedented era of quality and choice. Warner Bros. has
been a leader of the TV renaissance, with more than 60 shows in production
across broadcast and cable networks for the 2013-2014 television season

 producing a lineup that includes hugely popular
hits across all genres, such as The Big Bang Theory,
The Voice, The Ellen DeGeneres Show, and The Vampire
Diaries. As weve increased our investments in
content over the last several years, weve seen
increasing demand for high-quality programming not
just in the United States, but around the world, where
we are expanding local production and launching
new local-language versions of proven favorites, like
Gossip Girl in Mexico and The O.C. in Turkey. Its no
coincidence that many of our acquisitions in recent
years bolster these international efforts, including
Warner Bros. most recently announced proposed
acquisition: the international business
of the Amsterdam-based television
production and distribution company
Eyeworks Group.
Deep engagement with audiences
is what we aspire to achieve with every
show at our television businesses 
through networks like HBO, TNT, TBS,
CNN, and Cartoon Network and Warner
Bros. TV Studio. Among premium
television and subscription videoon-
demand services, HBO remains
in a league of its own  it won more
Primetime Emmy Awards in 2013 than
any network for the 12th consecutive
year  and four out of the five mostwatched
shows in HBOs history
are current series on the network. These include
True Detective, which debuted this winter as the
most-watched freshman series in HBOs history,
and Game of Thrones, whose season 4 premiere was
the most-watched show since The Sopranos finale.
At Turner, we have the industrys leading portfolio of
basic cable networks, which continued to set the pace
for the industry in 2013 by investing in programming
that resonated with viewers and advertisers. For
example, TBS ended the year as ad-supported
cables #1 network in primetime among adults 18-49,
with its compelling mix of original programs such as
Conan, Cougar Town, and Ground Floor, acquired hits

like The Big Bang Theory, and a lineup of sports that
includes the NCAA Mens Basketball Championship
Tournament (in partnership with CBS). TNT continued
to prove itself the home of ad-supported cables
best-loved original dramas with such programs as
Rizzoli & Isles, Falling Skies, and Perception and had
more top 15 originals in primetime than any other
ad-supported network in 2013.
An important testament to these networks
popularity and stature was the successful renewal of
carriage agreements between Turner and seven of the
top 10 distributors of video content over the past few
years, including five in 2013. All of those deals were
consistent with our expectation that
Turner will show accelerated growth
in domestic subscription revenue over
the next few years  which is both an
important driver of our growth and
validation of our strategy.
As we look ahead, we have much
to be excited about. This year we will
introduce such promising shows as The
Leftovers, from Lost co-creator Damon
Lindelof, on HBO and produced by
Warner Bros.; and The Last Ship, from
blockbuster producer Michael Bay,
on TNT. Warner Bros. has an exciting
lineup of films for the next few years,
including the final Hobbit installment,
which comes out later in 2014, and
a new film series written by J.K. Rowling that will
introduce additional characters from the magical
realm of Harry Potter, as well as a sequel to the hugely
popular The LEGO Movie. Were also excited about
the forthcoming follow-up to Man of Steel featuring
Superman and Batman, which is scheduled for release
in 2016. In television were also digging deeper into
the rich DC Entertainment library, with programs such
as Gotham and The Flash following on the success
of Arrow.
To satisfy fast-evolving consumer demands while
building value for our shareholders, we continue to
aggressively enhance our digital efforts, which include

TV Everywhere products like HBO GO and Turners suite
of apps that now offer live streaming as well as rich
on-demand libraries, popular online brands like TMZ
and Turners Bleacher Report, and video games such
as Batman: Arkham Origins and our LEGO-branded
franchise. Were also pleased with the progress weve
made in finding new ways to reach consumers with
our content through such channels as electronic sellthrough,
subscription video-on-demand services, and
our support of the UltraViolet standard for storing and
accessing home video purchases.
As I write this, were on track to complete the spinoff
of Time Inc. into an independent, publicly traded
company during the second quarter of 2014. Last
year, Time Inc. again proved itself the clear leader
in its industry, with brands reaching nearly half of
all U.S. adults each month and increasing its leading
share of overall domestic magazine advertising. We
have every confidence that Time Inc. will benefit from
its structure as an independent company and further
unlock value for our shareholders.
For the next phase in our journey, we intend to
move our global headquarters to a new location in
Manhattans Hudson Yards development beginning in
late 2018. The move will consolidate seven existing
locations in New York, including the local operations
of HBO, Turner, and Warner Bros., as well as our
corporate team. The move will generate significant
financial savings, but it will also bring untold cultural
and creative benefits as we become an even more
focused company with about 90% of our expected
2014 profits coming from the television ecosystem. I
am also pleased that, as of January 1, we have the next

generation of CEOs in place at each of our operating
divisions, completing a transition process that began
a couple of years ago, which further positions our
businesses for continued success.
I began by acknowledging the inspiring contributions
of our employees hard work, and want to come
back to a few important things that set Time Warner
apart. One is that we can offer the finest creators and
journalists the biggest platforms and a home without
an agenda, ideological or otherwise. Another is that
we take our responsibilities as corporate citizens
seriously and care about the communities in which we
live and work  Im always humbled and impressed
by how many of our employees volunteer their time
to charitable causes and participate in efforts such
as Time Warners Impact program. Also, we believe
deeply in a diverse workplace, not just as a cultural
good but as a business imperative, one that leads to
developing new voices whose stories can resound
throughout an increasingly multicultural world.
We are proud of all our progress so far and
the returns weve been able to provide to you, our
shareholders. And while we still have plenty of work
ahead, we are confident and excited about what is to
come. On behalf of all of Time Warners employees, I
want to thank you sincerely for the faith you place in us.
Jeff Bewkes
Chairman and CEO
April 17, 2014