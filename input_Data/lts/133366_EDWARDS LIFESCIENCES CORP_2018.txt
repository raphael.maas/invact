A Letter to Our Shareholders
We are fortunate to have celebrated our 60th anniversary in 2018. Looking
back, things were very different in the world in 1958: The hula-hoop was
invented, the first credit card was introduced and President Dwight D.
Eisenhower became the first American elected official to be broadcast on
color television. At the same time, a retired hydraulics engineer named Miles
�Lowell� Edwards approached a young cardiac surgeon, Dr. Albert Starr, with
an idea. As a child, Edwards had rheumatic fever, which
he knew could lead to heart problems. Because he knew
pumps and circulation so well, he believed he could
create an artificial heart. Dr. Starr told Edwards they
should focus first on an artificial heart valve, since that
was the greater need. Two years later, the first Starr-Edwards valve was designed,
tested and successfully implanted in a patient, which led to the formation of
Edwards Laboratories. Our company was created with a trusted partnership
between an engineer and a physician, and it�s a legacy we continue today.
Innovative Strategic Focus
While the company has changed dramatically
since 1958, our strategic thrust remains. It is built
on three pillars, starting with our dedication to
innovating for patients with structural heart disease
and the critically ill. We imagine a world where we
can help patients by enabling them to live longer
and experience a higher quality of life � all based on
cost-effective therapies. This is the opportunity and
the foundation of our strategy.
Today, we are an intensely focused company.
We choose not to be as big and diversified as other
companies; rather, we are dedicated to structural
heart and critical care medicine. This focus means
that we have a deeper understanding of the disease,
as well as the patient, which results in unmatched
expertise and unique capabilities for addressing the
challenges they present. Innovating in structural
heart disease is demanding, but critically important
because the disease burden is heavy and growing. 

Our focus allows us to exercise discipline and prioritize
investments in future therapies.
The second strategic pillar is pioneering breakthrough
innovations. We strive to boldly innovate and
anticipate the future needs of clinicians and patients.
We also know that simply having a promising idea is
insufficient for driving value. Truly adding value means
a commitment to delivering strong evidence of value
to patients, clinicians and the healthcare system.
Our third pillar is leadership. When you are the leader,
you learn faster and you have the chance to build
strong partnerships. These include partnerships with
leading clinicians around the world who are also
driven to transform practice. It allows us to have the
important conversations with regulators and payers to
ensure patients have access to breakthrough therapies.
Strong Global Performance
I am pleased that the execution of our strategy
has created value over the long term. For more
than 10 years, our company has delivered adjusted
double-digit sales growth. Our 2018 results continued
this impressive decade of performance as we met or
exceeded expectations. Total adjusted sales grew
10 percent to $3.8 billion and adjusted diluted
earnings per share rose 24 percent over last year.
This year, we invested more than half of the U.S. Tax
Reform benefit. We utilized the savings to hire
new employees, accelerate R&D initiatives and
contribute more to employee retirement accounts
while also growing earnings.
Impactful Team and Culture
Our 13,000 passionately engaged employees,
executive team members and Board of Directors
embody our patients-first culture and are vital to
the success of our organization. This is why we
strategically focus on our talent by providing ongoing
training and development, and foster an inclusive
culture. We believe the company is stronger when it
incorporates diverse thoughts, perspectives, cultures
and experiences. This variety is especially important to
stimulate our innovation and it allows us to extend our
reach and understanding of the world marketplace.
At Edwards, we believe good corporate citizenship �
helping patients exercise their voices to improve the
healthcare system, encouraging diverse perspectives
and cultivating a welcoming workplace where all
employees thrive � is a must. Our aspirations inspire
us daily to act as responsible corporate citizens, while
integrating our sustainability initiatives into our
business strategy. This integration enables us to deliver
on our sustainability commitments, while driving
further innovation and growth. And, it strengthens
the trust of our customers, employees, investors,
partners and the communities we serve fighting
structural heart disease.
Additionally, our employees remain active supporters
of the communities where they live and work, and
we continue to aspire for 100 percent of employee
participation in charitable activities each year.
Combined with the cumulative grants from the
Edwards Lifesciences Foundation totaling nearly
$60 million, we are having a positive impact globally. 

In 2014, we embarked on a bold initiative to make
a significant impact on an issue we know best. We
wanted to mobilize our resources and community to
accomplish one common goal: to impact the global
burden of heart valve disease by educating, screening
and treating one million underserved people by 2020.
I�m pleased to share that our initiative, Every Heartbeat
Matters, met and surpassed this goal in 2018. So, we
have decided to expand our goal to 1.5 million people
by 2020, and we are building the strategy for what is
next, so we can continue to help those in need.
Transforming Patient Care
We are fortunate that nearly all of our technologies
are in a leading position globally. We believe this
leadership is a result of the high value associated with
the safety, quality, training and collaboration among
the clinical community that goes into our technologies.
As we look ahead, we expect 2019 to be another
exciting year of continued growth and investment,
as well as advancement of transcatheter aortic valve
replacement. We also believe now is the time to
transform patient care through transcatheter
therapies for mitral or tricuspid valve disease. Through
a newly-formed team, we are progressing rapidly on
the clinical front, and conducting feasibility studies and
clinical trials that are expected to lead to numerous
commercial introductions over the next several years.
Additionally, through our transformational
investments in innovation, our core surgical
structural heart and critical care businesses are
growing and prospering, and we expect to extend
our leadership across the portfolio in 2019.
Looking to the Future
I am humbled by our success over the past 60 years.
While our legacy is remarkable, we remain focused
on the future and have bold plans for what is ahead.
We look forward to continuing our efforts to partner
closely in a community dedicated to transforming
care. It is extremely rewarding and it motivates and
excites our team every day because, for all of our
successes, there are still immense numbers of
patients in need and challenges for us to solve.
We always seek to make possible what was once
thought to be impossible, all the while focusing on
the patients whose hope inspires us to push the
boundaries and improve the practice of medicine.
Thank you very much for supporting Edwards as we
endeavor to improve the quality of life for patients
around the world.
Michael A. Mussallem
Chairman & Chief Executive Officer