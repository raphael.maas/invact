Fellow shareholders:
American Capital modestly increased its net asset value (NAV) per share by 6% to $18.97 as of year-end. We did
this despite the headwinds of a decrease in value in our asset management business (American Capital Asset
Management, or ACAM) and at our majority owned operating companies (Operating Companies). Our NAV
growth in 2013 primarily resulted from net unrealized appreciation at European Capital and accretion from our
continued stock repurchases. We made important progress in each of our business lines during the year, and we
are hopeful that, given the quality of our investments and assuming the economy cooperates, we will experience
further growth in our NAV in 2014.
For the year, we produced Net Operating Income (NOI) before income taxes of $0.76 per diluted share, or $232 million, a 4% return on equity. Net appreciation at European Capital offset net depreciation in all other business lines.
So, our net earnings totaled $0.61 per diluted share, or $184 million, a 3% return on equity.
In 2013, our Private Finance business in the U.S. experienced significant realizations and achieved exit values consistent with our prior quarters determination of fair value. Also, despite the highly competitive environment, we made
attractive investments in several new One Stop Buyouts and Sponsor Finance opportunities and several investments
in our existing Operating Companies. In addition, we introduced a new strategy in the fourth quarter of retaining
senior debt in our One Stop Buyouts, which we believe will increase NOI and transparency and demonstrate, over
time as we implement this strategy in new One Stop Buyouts, the earnings power of our Operating Companies.
For the year, we had $1.2 billion in realizations from portfolio exits, not including $98 million of net repayment of
European Capitals revolving credit facility, and we originated $1.1 billion of new investments, including $271 million of investments in ACAM for fund development, which we believe will help us develop new funds and generate
attractive returns for our shareholders.
European Capital generated a 19% net earnings return on equity, with net earnings of 144 million, and it exited a
considerable number of investments, realizing substantial liquidity consistent with our prior quarters determination of fair value. We used some of this liquidity to pay down debt, allowing us the freedom to return $178 million
of capital to American Capital, even as we actively made new debt investments.
At ACAM, despite the decrease in book value of our two mortgage Real Estate Investment Trusts (REITs) under
management, we had $19 billion of earning assets under management (EAUM) as of year end, the same as we had
as of the end of 2012. Including assets acquired with leverage, our assets under management totaled $93 billion at
year end. Revenue from ACAM increased over 2012, because during most of 2013, EAUM was materially higher than
during 2012. Also, we raised two new Collateralized Loan Obligation funds (CLOs) and prepared for the initial public
offering (IPO) of American Capital Senior Floating, Ltd. (ACSF), which successfully occurred in early January 2014. It
is now trading on NASDAQ under the symbol ACSF. We hope to add more funds under management in 2014.
Our stock repurchases continue to be an important enhancement to shareholder value and NAV. In 2013, we purchased 40 million of our shares for $561 million at an average price of $13.90 per share, which was $0.66 per share
accretive to our NAV per share. Our Board of Directors has extended the buyback program through December 2014.
From the inception of this program in the third quarter of 2011 through the end of 2013, we had purchased almost
$1.1 billion of our shares, representing 27% of shares outstanding as of June 30, 2011. This was $1.75 per share accretive to our NAV per share.With a goal to return our debt obligations to investment grade, we further solidified the right side of our balance sheet in 2013, refinancing our secured facility, tapping the unsecured market and repaying our last, on-balance sheet securitization. In August, we repaid $150 million of our existing Senior Term Loan and refinanced the
remaining $450 million at more favorable terms, significantly lowering our borrowing costs. More importantly, in
September, we reentered the unsecured debt market, issuing $350 million of private 6.5%, five-year Senior Notes.
Repaying our final on-balance sheet securitization, or Business Loan Trust, was another important milestone, as all
ten of these facilities, totaling $3.5 billion, have now been repaid in full.
As we continued to strengthen our financial profile and diversify our funding sources, S&P upgraded our ratings to
BB- from B+. Given the prospects for increasing our cash flows from ACAM and our initiative to increase cash flows
from our Operating Companies, which is discussed more below, we intend to manage our business with a goal of
becoming an investment grade issuer.
We have a very strong and diversified business, which is delivering attractive returns for our shareholders, and we
have many opportunities that we can capitalize on in 2014 and beyond. We expect to continue to make attractive new investments in Operating Companies, through our new One Stop Buyout strategy, and also make new
investments in our Sponsor Finance business. We believe the mortgage REIT environment has stabilized and are
guardedly optimistic that ACAM will grow its funds under management in 2014. Lastly, we have other fund strategies in the works at ACAM, and we would expect some of them to come to fruition in 2014. We are very excited
about our future and believe we are in an excellent position to enhance shareholder value in 2014 and beyond. All
of us at American Capital thank you for your continued support. Lets now review 2013 in more detail.
Business Lines
We describe below our five business lines and some of the opportunities we have to continue providing strong
returns for our shareholders. Five of our largest Portfolio Companies are described later in this letter.Operating Companies (One Stop Buyouts)1
Through our One Stop Buyouts we acquire our Operating Companies and work with them to enhance their performance and grow their company.
As of December 31, 2013, the fair value of investments in our Operating Companies totaled $2.3 billion. We have
invested $11.9 billion in Operating Companies since our IPO, generating a 12% internal rate of return (IRR) on a
blend of realized and unrealized senior debt, mezzanine debt and equity investments. Of this amount, we have
exited $10.9 billion of Operating Company investments, generating a 15% IRR on a blend of senior debt, mezzanine
debt and equity investments and 25% IRR on exited equity investments.
In 2013, our Operating Companies had an aggregate of $3.2 billion in revenues, $657 million of Adjusted EBITDA2
and over 70,000 employees. In 2013, American Capital earned $145 million of interest, dividend and fee income
from our Operating Companies.
The first set of bars in the chart below shows the IRRs of the $11.9 billion of senior debt, mezzanine debt and equity
investments, both existing and exited, which weve made in our Operating Companies since our IPO (in calculating the IRR for our existing investments, we have used their December 31, 2013, fair value). The second set of bars
shows the IRRs of the $10.9 billion of investments in Operating Companies that we have exited. We work hard to
exit our investments when we can maximize returns on our equity investments. We have produced a 12% IRR on
all investments and a 15% IRR on our exited investments, on a blend of senior debt, mezzanine debt and equity.In 2013, we received $530 million from investment realizations at 34 Operating Companies. Also, we committed
$503 million to new investments in Operating Companies in 2013, mostly in the fourth quarter as we invested in
the combination of Cambridge Major Laboratories (CML) and AAIPharma and their recapitalization. As you may
recall, in the fourth quarter of 2012, we invested $212 million in the One Stop Buyout of CML, a leading global
provider of complex chemistry-based outsourcing services, serving a broad base of pharmaceutical, biotech and
generic drug companies.
AAIPharma is a leading provider of analytical and formulation services supporting all phases of drug development.
The combination created an industry leading Contract Development and Manufacturing Organization or CDMO.
We believe that the combination allows for drugs to be designed more efficiently from early stage development to
manufacturing. American Capitals strength in facilitating organizational growth and integration will benefit the
combined entity.
We are very pleased with this acquisition, and we have committed a net $458 million (including amounts already
invested) to this business, making it one of our largest operating companies. CMLs combination with AAIPharma
expands American Capitals growing portfolio of healthcare products and services companies.
As usual, American Capital provided all of the funding for the transaction, but this time we did not syndicate the
senior debt, as we have in past One Stop Buyouts. This new approach allows American Capital to earn interest income on senior loans to our Operating Companies, and, because we are the senior lender, the loans need
not have the usual prohibition against the payment of cash dividends to shareholders. Thus, our Operating
Companies can pay cash dividends to American Capital and their other shareholders as they produce cumulative
earnings and profits.
We generally intend to use this approach going forward on companies where we own at least 80% of the common equity. We believe this approach provides our shareholders more transparency into the performance of our
operating companies as we invest in new One Stop Buyouts. So, in the future, we plan not only to inform you of
the dividends we receive from American Capital Asset Management, but we plan to report interest income and
dividends from other 80% or greater American Capital owned operating companies.
Also in the first quarter of 2013, we committed $26.5 million to the Special Situations Groups One Stop Buyout
of Service Experts, LLC, a leading provider of service, maintenance, repair and installation of HVAC systems for the
residential and light commercial markets. American Capitals investment took the form of preferred and common
equity and a revolving credit facility. Headquartered in Plano, TX, Service Experts owns and operates 108 branch
locations in the United States and Canada with approximately $385 million in annual sales. Service Experts offers a
complete suite of custom heating and air-conditioning products and services, including inspections, maintenance,
sales, equipment installations, warranties and service plans, as well as a full line of indoor air quality and energy
saving green solutions.
As part of the transaction, American Capital partnered with Scott J. Boxer, President and Chief Operating Officer
of Service Experts from 2003 to 2010, who returned from retirement as the newly appointed President and CEO
of Service Experts. We urge you to consider Service Experts when you next are in need of heating and airconditioning products or services.Our Special Situations Group has invested $820 million in 13 companies and has produced a 19% annual return
since its inception on a blend of senior debt, mezzanine debt and equity.
Sponsor Finance and Direct Business
Our Sponsor Finance and Direct Business lends to and co-invests in the equity of U.S. middle market buyouts
sponsored by other private equity firms and also makes direct investments in middle market companies. We
valued these investments at $801 million as of December 31, 2013, and the $9.5 billion we have invested in these
transactions since our IPO has generated an 8% IRR on a blend of realized and unrealized senior debt, mezzanine
debt and equity co-investments. During 2013, our Sponsor Finance Team sourced new investment opportunities
by working closely with some of the best private equity firms. We invested $258 million in 16 Sponsor Finance
private equity buyouts last year and expect to do more in 2014. In 2013, American Capital recognized $131 million
of interest, dividend and fee income from our Sponsor Finance companies.
Asset Management Business
American Capital Asset Management develops, capitalizes and manages alternative investment funds, including
real estate, private equity and senior loan funds.
We valued our investment in ACAM at $870 million as of December 31, 2013. In 2013, American Capital earned
$133 million of dividend, fee and interest income from ACAM, a 29% increase over 2012. The increase was the result
of EAUM during the year being materially higher than during 2012. We expect a lower level of fee income in 2014,
primarily due to EAUM as of December 31, 2013 being lower than average EAUM in 2013 as a result of declines in
the book value of American Capital Agency (AGNC) and American Capital Mortgage (MTGE), the two mortgage
REITs under ACAM management.
We have other managed vehicles in the works, some of which we hope to raise in 2014. That fact, coupled with
our belief that the mortgage REIT market has stabilized, makes us guardedly optimistic that we will grow ACAMs
funds under management during 2014.
ACAM now manages seven private and three public funds with $12.7 billion of EAUM and $87 billion of total thirdparty AUM, as described below:
American Capital Agency Corp. (NASDAQ: AGNC)
AGNC invests primarily in mortgage backed securities issued by Fannie Mae and Freddie Mac. 2013 was a challenging year for AGNC, as the agency mortgage-backed securities market was one of the worst performing sectors
across the broader fixed income markets. Because AGNC was trading at a discount to book value during portions
of 2013, it executed on its stock repurchase program. Since commencing the buyback program in the fourth
quarter of 2012, the company has purchased approximately 43 million shares of AGNC common stock for total
consideration of approximately $934 million. As of December 31, 2013, AGNC had $76 billion in total assets.
For managing AGNC, ACAM receives a 1.25% annual fee on AGNCs adjusted book value (excludes unrealized
appreciation and depreciation), which was $9.3 billion as of December 31, 2013.
To learn more about AGNC, please visit its website at www.AGNC.com.American Capital Mortgage Investment Corp. (NASDAQ: MTGE)
Capitalizing on the success of AGNC, we sponsored the IPO of a hybrid mortgage REIT, MTGE, in August 2011. In
addition to the Freddie Mac and Fannie Mae securities in which AGNC invests, MTGE also invests in a broader array
of mortgage backed securities and other real estate related assets. Given MTGE was trading at a discount to book
value during 2013, it executed on its stock repurchase program. Since commencing the buyback program in the
fourth quarter of 2012, the company has purchased approximately 7.9 million shares of MTGE common stock for
total consideration of approximately $161 million. As of December 31, 2013, MTGE had $8.4 billion in total assets.
MTGE acquired a licensed mortgage servicer in Q4, which should expand its mortgage investment opportunities.
For managing MTGE, ACAM receives an annual fee totaling 1.50% of MTGEs adjusted book value (excludes unrealized appreciation and depreciation), which was $1.2 billion as of December 31, 2013.
To learn more about MTGE, please visit its website at www.MTGE.com.
American Capital Senior Floating, Ltd. (NASDAQ: ACSF)
In January 2014, we sponsored the $150 million IPO of ACSF, a business development company (BDC) managed
by ACAMs Leveraged Finance Group, which invests primarily in senior secured floating rate loans. The members of
the senior investment team of ACAMs Leveraged Finance Group have an average of 18 years of experience investing in this asset class and worked together at American Capital for the last eight years, managing approximately
$1.8 billion of leveraged loans.
At least 70% of ACSFs assets are expected to be invested in broadly syndicated first and second lien floating rate
loans. We also expect to invest opportunistically in debt and equity tranches of collateralized loan obligations,
which we expect will total less than 20% of total assets.
For managing ACSF, ACAM will receive an annual fee totaling 0.80% of ACSFs total assets (excluding cash and cash
equivalents and net unrealized appreciation or depreciation). We believe this senior floating rate BDC is an attractive vehicle for investors, particularly in a rising rate environment. Furthermore, ACAM is managing ACSF at what
we believe is a very investor friendly fee structure, with no carried interest and perhaps the lowest management
fees among all BDCs.
To learn more about American Capital Senior Floating, Ltd., please visit its website at www.ACSF.com.
American Capital Energy Infrastructure (ACEI)
In November 2012, we announced the hiring of Paul Hanrahan, the former CEO of AES Corporation, a Fortune 150
global power company, to lead and expand our Energy and Infrastructure Group, which targets investments in
global energy infrastructure assets.
In 2013, we filled out Pauls team with three senior investment professionals and appointed two senior advisors.
We also made our first two investment commitments.
In December, we committed $32 million with an option t o invest an additional $100 million in Azura Power Holdings
Ltd. (Azura), the company responsible for developing the Azura-Edo power project in Edo State, Nigeria. With
a goal of becoming the leading power development company in West Africa, Azura will utilize ACEIs investment to fund the first and second phases of the power project, pursue its greenfield development pipeline and future
acquisitions, expand its team, and grow its construction and operational capabilities.
The Azura-Edo power project is a proposed 450 MW open cycle gas turbine power station being developed near
Benin City in Edo State and represents the first phase of a 1,000 MW power plant facility. Our first commitment is
a good example of the type of investment in high growth platforms in the energy infrastructure space that we
are targeting.
Also in December, ACEI partnered with a veteran management team to create BMR Energy LLC (BMR), a new
energy company focused on developing and investing in power and related energy infrastructure throughout
Central America and the Caribbean.
ACEI committed $25 million to BMRs first project, which is a 34 MW wind farm in Malvern, St. Elizabeth Parish,
Jamaica. In September 2013, the Office of Utility Regulation in Jamaica selected BMR to build, own, and operate
the wind project, which is expected to be operational in 2015.
To learn more about American Capital Energy Infrastructure, please visit its website at www.ACEI.com.
Other Funds
In addition, ACAM manages two private equity funds with a combined $727 million of assets, as well as European
Capital, our wholly-owned European fund, with $1.2 billion in total assets.
European Capital
European Capital invests in operating companies and makes sponsor finance investments in Europe. In 2013,
European Capital had net earnings of 144 million, which resulted in a 19% net earnings return on equity. As of
December 31, 2013, we valued our wholly-owned equity investment in European Capital at $841 million, a 15%
discount to European Capitals NAV.
In 2013, our investment in European Capital appreciated $316 million, a 25% return on the fair value of our equity
investment. In the year, European Capital repaid the $98 million of principal and $17 million of accrued paymentin-kind (PIK) interest on its debt to American Capital, reducing the ACAS revolver to zero as of December 31, 2013.
In addition, European Capital paid $178 million in dividends in 2013 to American Capital, which were accounted for
as a return of capital.
To learn more about European Capital, please visit its website at www.EuropeanCapital.com.
Structured Products
Our Structured Products business invests in CLOs of broadly syndicated senior floating rate commercial loans and
investment and non-investment grade tranches of commercial mortgage backed securities (CMBS). In 2013, we
invested $75 million in Structured Products, of which $70 million was invested in CLOs. Our Structured Products
investments produced $72 million in interest in 2013. However, many of our CLO equity investments are maturing and will likely repay entirely within the next several years. This may cause our Structured Products revenue
to decline, offset to the extent we make new investments in CLO equity. Our Structured Products depreciated
$41 million in 2013, little of which was due to declining credit quality.
8 Amer i can Capital
Potential Sources of Growth in Our NAV
Since the United States GDP turned positive in Q3 2009, our NAV per share has grown at a 19% annualized rate.
While the future is never certain, we believe we can continue to drive growth in our NAV per share through a variety of endeavors discussed below.
Potential for Appreciation of Our Equity Investments
American Capital has $1.5 billion of investments in equity assets, excluding our investments in European Capital
and ACAM. If the U.S. economy continues to recover, these investments have the potential for appreciation at
equity rates of return, as we help our portfolio companies grow organically and through add-on acquisitions and
as we help them improve their operational performance.
Continue to Grow Our Asset Management Business
We expect to expand our existing funds under management and raise new funds to manage. We also will continue to leverage our existing cost structure as we grow our EAUM. If successful, this should drive growth in our
NAV per share.
New Investment Opportunities
Though the current volume of mergers and acquisitions is dramatically below the peak seen in 2007, we believe
there are opportunities to originate unitranche, senior debt, mezzanine debt and equity investments that should
allow us to grow our NAV per share.
Appreciation in Our Investment in European Capital
The $841 million fair value of our equity investment in European Capital is a $151 million discount from European
Capitals own NAV. While we cannot project future market discounts, we believe this discount will decrease over time.
In addition, European Capital has $503 million of equity assets at fair value that could be expected to appreciate
if the European economy improves.
Finally, European Capitals balance sheet reflects $39 million of bond yield discount to the value of its performing
debt investments. If those loans are repaid in full, this discount will be reclaimed.
Accretion Due to Stock Repurchases
Under our Stock Repurchase and Dividend Program, we may purchase shares if we are trading below book value
and pay a dividend if we are trading above book value, and as such, any stock purchases will be accretive to NAV
per share.
Retained Earnings
As a taxable corporation, we are able to retain future ordinary and capital income without paying taxes to the
extent we can utilize our net operating and capital loss carry forwards.
We plan to keep you posted on developments in these areas as they occur. Please visit our website at
www.AmericanCapital.com and sign up for our quarterly newsletter and press releases on the lower left of
the homepage.Looking Ahead
With our new One Stop Buyout strategy for acquiring Operating Companies, an excellent pipeline of Sponsor
Finance investments, reduced leverage, the growth opportunities within our Asset Management Business, potential growth in our European Capital investment and further opportunities for accretive stock repurchases, we feel
we are well positioned for the balance of 2014 and beyond to continue to grow our NAV per share.
Sincerely,
Malon Wilkus
Chairman and Chief Executive Officer
Gordon OBrien
President, Specialty Finance and Operations
Samuel Flax
Executive Vice President & General Counsel
John Erickson
President, Structured Finance & Chief Financial Officer
Ira Wagner
President, European Private Finance