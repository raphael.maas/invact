Letter To Our Shareholders
In 2017, GameStop set out to deliver on strategic priorities
designed to drive growth and deliver returns for our
shareholders. Thanks to the passion and hard work of our
associates around the world and their ability to navigate
industry changes, we drove annual sales growth of 7% and
delivered adjusted earnings per diluted share of $3.34.
Throughout the year and particularly during the very
important holiday period, we continued to demonstrate
that our family of retail brands � GameStop, EB Games,
Micromania, ThinkGeek, Zing Pop Culture, Spring Mobile
AT&T and Simply Mac � are the preferred destinations for
gaming, collectibles and consumer electronics merchandise
at a great value.
A few financial highlights from the year:
Video Games:
� Video game hardware sales grew 28%, driven by
innovative new consoles like Nintendo Switch, the PS4
Pro and Xbox One X.
� Video game software sales increased 4%, driven by the
strong performance of new titles.
Pop Culture:
� Our global collectibles business grew 29%, further
demonstrating that this business can be an accelerating
contributor to our overall profitability.
Global Omni-Channel:
� Global omni-channel sales grew 49% in 2017, driven
by investments we�ve made to improve the customer
experience and our integrated channels.
While we delivered solid results in 2017, generating
$325 million in free cash flow and paying $155 million to
shareholders in dividends, there are still many areas of our
business that can be improved to further drive shareholder
value. More specifically, we hold ourselves accountable for
improving the performance in the Technology Brands and
our pre-owned video game category. We believe we can
do better.
We are committed to finding ways to maximize profitability
going forward. A key aspect to delivering on this goal is to
take a pause on acquiring new businesses in the near-term.
We can drive growth and improve profitability by focusing
on the fundamentals of managing the three businesses
that we already have � Video Games, Collectibles and
Technology Brands. Once we improve the operating
fundamentals of each and achieve the performance
level we know they can attain, we will explore additional
opportunities for growth to drive shareholder value. For
2018, we believe we are in position to once again deliver
strong cash flows and earnings and that our near-term
focus on improving the fundamentals of our businesses
can position the company for long-term success.
GameStop has long been committed to giving back to the
communities we serve. As we look to 2018, we are evolving
our corporate giving efforts through the introduction
of our new GameStop Gives charity program, which will
focus on using the power of gaming for good. Through
partnering with charitable organizations like Make-AWish, Autism Speaks, Ronald McDonald House, Starlight
Children�s Foundation, the Children�s Miracle Network and
the Call of Duty Endowment, we believe gaming can and
will empower the lives of our customers and their families.
On behalf of our associates around the world, I would like
to thank all of our stakeholders for their continued support
of GameStop.
Sincerely,
Daniel A. DeMatteo
Executive Chairman and
Interim Chief Executive Officer