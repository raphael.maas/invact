Message to Shareholders

Your Company achieved record results in 2008 despite the severe
economic downturn. And, the steps weve been taking to further
improve our performance should help us deal with current
conditions and capitalize on future opportunities.
Key accomplishments included:
 Record earnings of $4.41 per share of common stock
 Near-record of $2.2 billion in cash generated from operations
 Record output of 82.4 million megawatt-hours (MWH) from our
generating plants
 Continued improvement in transmission and distribution reliability
 Stipulated agreements in Ohio on a rate plan that would
provide our customers with greater price certainty and our
utilities with better opportunities to recover their costs
Also, we paid an increased dividend of $2.20 per share and
exceeded the top end of our earnings guidance  notable
achievements in a challenging year.
These and other accomplishments underscore our focus on the
fundamentals, as well as our ongoing dedication to

Meeting the Challenges
of 2009 and Beyond
Our nation is in the midst of widespread
economic turmoil. This is creating a number
of challenges for our industry and our
Company  from lower electricity sales to
higher pension and financing costs.
We also expect more stringent environmental
requirements over the next few
years, which could include new mandates
for carbon emissions designed to address
the issue of global climate change.
Were responding to these challenges
by making targeted reductions in capital
and operating expenditures throughout
the Company that will total more than
$600 million in 2009 alone.
For example, weve delayed completion
of the Fremont natural gas plant to better
reflect current and projected power
supply needs. And, were reducing our
near-term capital requirements through
an adjusted construction schedule for
the air quality compliance project at our
Sammis Plant without affecting the completion
deadline of December 31, 2010.
In addition, weve put into place a
new organizational structure that will
ensure more appropriate staffing levels
and greater flexibility in meeting future
challenges.

Maximizing the Value of Our
Generating Fleet
The record performance of our nuclear
fleet contributed to our total generation
record of 82.4 million MWH. With recent
uprates and a strong focus on operational
excellence, our nuclear plants
safely produced 32.2 million MWH,
surpassing the previous record set in
2007 by nearly 6 percent. Also, the
nuclear fleets 92.6 percent capability
factor  the amount of electricity generated
compared with the amount that
could be generated at full power for
the year  set a new Company record.
Our fossil plants also contributed to the
record total by producing 50.2 million
MWH  a significant accomplishment
considering scheduled outages earlier
in the year at two of our largest
generating units.
To achieve cost-effective, incremental
growth of our generating capacity, were
installing advanced turbine technology
and other state-of-the-art equipment
at our plants. We added more than
50 megawatts (MW) of nuclear capacity
through a turbine upgrade on Unit 2
at the Beaver Valley Power Station and
through technology improvements at our
Davis-Besse Nuclear Power Station.
Also, we expect to add 100 MW of
capacity to our Sammis Plant by 2010
with the completion of turbine upgrades
on units 6 and 7. Over the last three
years, weve improved our generating
capability by nearly 500 MW through
such uprates  at a fraction of the
cost of a new power plant.
To further enhance our fossil operations,
we made a strategic investment in
a coal mine in Montana that is expected
to improve plant output and environmental
performance. Our $125 million equity
investment in Signal Peak Energy
should produce a fuel supply that offers
the benefits of cleaner-burning western
coal but with a higher heat value. Based
on tests conducted in 2008, we expect
the use of this higher-quality coal to add
up to 170 MW of capacity to our fossil
fleet while producing lower emissions
of sulfur dioxide than our current mix
of coals. Deliveries of Signal Peak coal
are scheduled for later this year.
Transitioning to
Competitive Markets
We remain focused on managing the
transition to competitive markets for
electricity in Ohio and Pennsylvania.
The Ohio Legislature enacted a new
electric restructuring law last May.
Throughout the legislative process,
we aggressively represented the interests
of our shareholders, employees
and customers. The new law provides
two options for pricing electricity in 2009
and beyond  through a rate plan that
can set the total price for electricity, or a
competitive bidding process that determines
the price of generation only.
Last month, we reached agreements
with key stakeholders  including organizations
representing schools, hospitals
and industrial and residential customers
 on a comprehensive rate plan for our
Ohio utilities. Under the plan, current
base distribution rates would be frozen
through December 31, 2011, and a
competitive bidding process would set
generation prices for customers who
do not choose an alternative electricity
supplier. If approved, the plan is expected
to offer greater price certainty for our
customers and more appropriate cost
recovery for our utilities.
In Pennsylvania, all electric distribution
companies are required to secure
generation from competitive markets
by 2011. A new law passed in 2008
outlines the procurement process and
sets targets for energy efficiency and
conservation. Customers in our Penn
Power service area transitioned to the
competitive generation market in 2007.
And, later this year, we expect to begin
arranging for generation that will be
available to customers of our Met-Ed
and Penelec companies in 2011.

To help customers prepare for this
transition, Met-Ed and Penelec introduced
a Voluntary Pre-payment Plan.
It gives customers the opportunity to
smooth out the impact of expected
generation price increases by making
modest, interest-earning pre-payments
during the period before rate caps expire.
Customers of Jersey Central Power
& Light have been receiving their
generation supply through competitive
markets since 2003, with prices set
through periodic auctions.
In addition, we are currently developing
compliance plans for energy efficiency
and conservation mandates in all three
states. We intend to introduce programs
later this year that will help us meet
these requirements while providing
for recovery of related costs.
Enhancing Service to Customers
We continue to make strategic
investments in our transmission and
distribution system that are designed
to achieve cost-effective improvements
in the reliability of our service.
For example, were investing in
technologies to improve system maintenance
and reduce the number and
length of outages. These include
advanced weather sensors near our
substations to protect equipment;
acoustical and temperature-sensitive
devices that better predict equipment
failures; and digital relays that provide
real-time data on system conditions.
These and other efforts have helped
reduce the average time customers
have been without power in each of
the last four years, for a total improvement
of nearly 40 percent. In 2008,
the average was just over two hours 
approaching top quartile for our
industry. And, the reliability of our
transmission system is among the
industrys best.
Weve also improved the effectiveness
of our storm response efforts. Following
a hurricane-strength windstorm in
September that interrupted electric
service to more than one million
customers in northern Ohio and western
Pennsylvania, we responded with our
largest-ever power restoration effort. It
involved about 4,000 line, forestry and
service workers from FirstEnergy companies
and other utilities in the region.
Our Contact Center representatives
handled nearly 640,000 customer calls,
and despite severe and widespread
damage to our system, we restored
service within 48 hours to nearly
90 percent of customers affected
by the storm.
Along with hundreds of letters from
customers thanking our crews, the
Edison Electric Institute (EEI) honored
us with its Emergency Recovery Award
in recognition of our outstanding service
restoration efforts following this storm.
Also, for the third consecutive year, we
received the EEI Emergency Assistance
Award, which acknowledged the work of
some 300 FirstEnergy employees who
assisted utilities in Louisiana to restore
service safely and efficiently in the
aftermath of Hurricane Gustav.
Working Safely
In 2008, we attained near top-decile
safety performance in our industry
with an OSHA-recordable rate of 0.97,
representing less than one recordable
incident per 200,000 hours worked.
Employees at 12 of our facilities had no
recordable incidents last year, and our
Davis-Besse employees have worked
more than 9 million hours without a losttime
accident.
Despite these solid results, we wont
be satisfied until we have no safetyrelated
incidents. As part of our efforts
to improve safety performance and
accident prevention, our training
emphasizes that employees are personally
accountable for their safety and

responsible for full compliance with our
safety programs and practices. Key to
these efforts is the participation of union
leaders who are taking an active role in
safety assessments and other program
components. With the consistent reinforcement
of safety practices, we are
working to achieve an accident-free
workplace throughout our organization.
Protecting the Environment
We are committed to operating our
facilities in an environmentally sound
manner. Toward that end, 76 percent of
the electricity we generated in 2008
came from low- or non-emitting sources.
And, 40 percent of our output  from
nuclear, wind and hydro sources  was
carbon-free. As a result, emission rates
for our power plants remain significantly
lower than the regional average, providing
us with a competitive advantage
when meeting increasingly stringent
environmental requirements.
In addition, later this year we expect
to begin placing new environmental
controls into service at our Sammis
Plant. This massive construction project
is approximately 85 percent complete,
and we expect to have new controls
on all seven units by the end of 2010.
Were further improving our environmental
performance by adding new contracts
for wind energy. We recently entered
into long-term agreements to purchase
62.5 MW of wind energy generated in
western Pennsylvania and 99 MW
of wind power from a facility in central
Illinois. With 376 MW of wind generation
under contract, were one of the largest
providers of wind energy in the region.
Also, we are incorporating technologies
across our system that help save energy
and protect the environment. At our
new West Akron Campus, a number
of environmental features  including
recycled building materials, droughtresistant
landscaping and high-efficiency
lighting with motion sensor controls 
should help us achieve Leadership
in Energy and Environmental Design
(LEED) certification for the facility.
Building on Our Progress
Certainly, the economic crisis is
troubling, but as I look back on the
history of our Company, Im reminded
of the many times we have successfully
overcome difficult challenges.
I am confident we will do so again by
remaining focused on the fundamentals
and continuing to deliver on our
financial and operational goals.
I would like to acknowledge the efforts
of our dedicated employees to make
FirstEnergy one of our industrys top
performers, and I thank you for your
continued support.
Sincerely,
Anthony J. Alexander
President and Chief Executive Officer
March 20, 2009