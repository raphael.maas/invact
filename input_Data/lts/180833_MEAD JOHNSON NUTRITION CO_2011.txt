MessAge FROM the
ChieF EXeCUtiVe OFFiCeR

tO OUR shARehOLdeRs,
COLLeA gUes AND BUsiness PAR tneRs:
In 2011, Mead Johnson Nutrition delivered a third consecutive
year of record results, including exceptional sales and
earnings growth of 14 percent and 15 percent, respectively,
reported on a non-GAAP, constant-dollar basis.
Against a backdrop of continuing economic challenges in
many parts of the world, and given the significant effort
required to complete the infrastructure necessary to operate
as a fully independent public company, we are particularly
pleased with our superior marketplace performance.
Mead Johnson is operating in an attractive category, with
powerful brands, a leading product portfolio and pipeline,
an expanding global footprint and world-class employees.
We are constantly building upon our outstanding reputation
for quality, nutritional science and innovation. As a result,
we are not making routine progress or simply taking steps
in the right direction � rather, we are hitting our stride as a
dynamic, independent and fast-growing global company.
DELIVERING GROWTH
In addition to being strong, our 2011 sales growth was
also of high quality � with three-quarters of the increase
attributable to volume and the balance to price. The
strength of our performance extended across both our
key product categories, with double-digit growth for our
infant formulas and for our growing-up milks. Both of our
geographic segments contributed to the sales increase.
North America and Europe recorded a 3 percent constantdollar gain, while our sales across our larger business in
Asia and Latin America grew by 22 percent, excluding
foreign exchange.
More than a dozen markets achieved double-digit sales
growth in the Asia/Latin America segment, which now
accounts for two-thirds of our global sales. China/Hong
Kong led the way with a 40 percent constant-dollar
increase, crossing the $1-billion mark in sales to become
our top global market.
We continued to expand our network of Chinese cities with
full sales and marketing resources, adding another 48 in
2011. But while ongoing expansion through this disciplined
Step-Change program is an important element of our longterm growth strategy in China, the large majority of 2011
growth came from increased market share and higher
consumption in established cities, demonstrating the
excellence of our execution with consumers, health care
professionals and retailers.
Among our other Asia markets, Thailand, Malaysia, Singapore,
Vietnam and India also recorded notable sales gains.
Our Latin America region is targeting $1 billion in sales by
2015, and its performance in 2011 was a strong step toward
that ambition. The large majority of our markets in the
region recorded double-digit increases in sales, with Peru
moving onto the company�s Top Ten market list, joining
Mexico and Brazil.
Given its demographics and projections for its economy,
Brazil offers especially exciting long-term prospects for
Mead Johnson. We are making significant investments in
the market, as we build an infant formula business on top
of our established Sustagen brand that leads the children�s
nutritional supplement category.
In North America and Europe, the economic and demographic environment remained especially challenging in 2011.
We were pleased to record growth in our U.S. business, even
though the market as a whole experienced a third consecutive
year of declining births and reduced formula consumption.
Our U.S. team was able to deliver modest, but important,
increases in both sales and market share in 2011 � due to
successful product innovations and opportunities created by
a competitor�s 2010 recall.
Across Europe, we successfully completed the complex
transition to a new distribution model and strengthened
our operations, including the implementation of new SAP
business systems.
In addition to comparing ourselves against prior year
performance, another measure of success is how our
results compare to those of our competitors. In 2011, we
grew our sales, in the markets where we compete, at the
highest rate of the five major global companies in our
industry, and thereby grew market share. We are proud
of our global leadership position, as the top-ranked
competitor across our markets in infant nutrition and
number two in children�s nutrition.
STRENGTHENING OUR FOUND ATIONS
We know that delivering continued high performance in the
future requires investment in the present. Last year included
a number of key initiatives that will enable Mead Johnson
to strengthen its foundations and will support sustainable,
profitable growth.
Our Mead Johnson Value of �uncompromising commitment
to scientific rigor and product quality� serves as a key support
to our company Mission �to nourish the world�s children for 

the best start in life.� Consistent with that commitment,
we have been stepping-up our investment in research and
development, including an 18 percent increase from 2010
to 2011. Beyond the various new product and packaging
projects that populate the company�s innovation pipeline,
the most visible result of this investment is the ongoing
expansion of the Mead Johnson Pediatric Nutrition Institute
(MJPNI). We began 2011 with the dedication of a new MJPNI
and technology center in Mexico and celebrated the grand
opening of the latest MJPNI facility in China in November.
These centers increase the company�s ability to develop
nutritional science, deliver new products and benefits, and
strengthen relationships with key scientific and regulatory
stakeholders around the world.
Another important milestone in 2011 was the completion
of our global SAP implementation and establishment of
an independent shared services function. Symbolically, it
marked the cutting of the last tie to Bristol-Myers Squibb
(BMS) � the final step on the road to complete separation
and independence that began with our Initial Public Offering
three years ago. On a practical level, it eliminated duplicated
costs for systems and services previously paid to BMS.
These systems will serve as a cornerstone for our efforts to
drive global operational efficiencies and will contribute to a
targeted reduction in general and administrative spending
from 8.5 percent of sales in 2011 to 6.5 percent by 2016.
We have been in this business for 106 years,
and we fully intend to be in it for generations
to come � with the most trusted and
respected brands in the industry.
Importantly, we continued to invest in business infrastructure
and capacity to support long-term growth. In December, we
announced the largest-ever capital investment in company
history � a $300-million spray dryer and technology
center to be built in Singapore. This expansion of our
manufacturing footprint and related realignment of our
regional operations is needed to support and manage the
future growth of our fast-growing Asian region. The new
manufacturing plant will be operational by mid-2014.
ALIGNING AND LEVERA GING TALENT
Recognizing the growing size and complexity of our business,
as well as the significant opportunities that lie ahead, we must
ensure the proper balance between focusing on our long-term
strategy and our more immediate operating performance.
As President & CEO, I continue to lead the overall Mead
Johnson enterprise, but in order to enable me to focus proper
attention on long-range strategic initiatives and important
engagements with external stakeholders, I established the
new role of Chief Operating Officer and named Kasper
Jakobsen to the position. Kasper, who most recently served
as the head of our Americas business and previously led our
Asia region, now has responsibility for managing our global
operations on a day-to-day basis.
Additionally, I appointed Charles Urbain, previously President
of Asia and Europe, to the new role of Senior Vice President,
Stakeholder Relations and Chief Development Officer in
recognition of the importance of business development
to complement our superior organic growth. Evidence
of our commitment to such strategic investments came
with the recent announcement of our joint venture with
SanCor � Argentina�s leading dairy producer. Developing
this strong infant and children�s nutrition business in
Argentina and surrounding markets will further enhance our
fast-growing business in Latin America.
PROTECTING AND ENHANCING THE BRAND
This past year offered a vivid reminder of the sensitivity of
the category in which we operate and how easily brands
can be negatively impacted. We saw how a U.S. competitor
was punished in the marketplace as a result of a product
recall. Then, near year end, we found ourselves having to
defend the reputation of one of our U.S. products � not
because of an actual safety or quality issue, but due to a
marketplace overreaction to a health investigation that was
magnified by inaccurate media reporting and speculation.
Although the quality and safety of Mead Johnson products
were fully validated � with officials actually complimenting
the rigor of our quality and safety processes and
programs � there was still a negative reputational impact
on our brand, given the abundance of caution that parents
use when caring for their precious infants. We know that
our business depends on the trust of parents, retailers,
healthcare professionals and government authorities; which
is why we not only hold ourselves to the highest standards
of safety and quality, but also engage in a passionate pursuit
of continuous improvement in all we do. We have been in
this business for 106 years, and we fully intend to be in it for
generations to come � with the most trusted and respected
brands in the industry.
Beyond quality products, integrity and performance,
we also recognize that a company�s corporate social
responsibility, or CSR, activities also influence its brand
equity and reputation.
Last year at this time, we communicated our overall CSR focus
and strategy; and in 2011, we took steps to bring it to life. Most
notably, we launched a signature charitable program � A
Child�s Best Start � dedicated to providing the best nutritional
start in life to the world�s most fragile and vulnerable children,
with an emphasis on those without parents.

In 2012, you will also see Mead Johnson strengthen its
commitment to protecting the environment. Following
careful review and study, Mead Johnson�s Executive Committee approved some ambitious, long-term environmental
and sustainability goals for the company at the end of 2011,
which we are publicly unveiling in this report and will followup with periodic updates on our progress.
VIEWING THE ROAD AHEAD
Last year marked the end of a series of activities and
investments that were necessary for Mead Johnson to
establish itself, and operate, as a fully independent public
company. As a result, we left 2011 and entered 2012 in full stride
and more focused than ever. With the ability to concentrate all
our efforts and resources on executing our growth strategies
and capitalizing on identified marketplace opportunities, the
future for Mead Johnson appears especially bright.
The contributions of our valued business partners and the
support of our shareholders are important to our success;
but above all, I appreciate and acknowledge the exceptional
efforts of our Mead Johnson colleagues around the globe,
who delivered such strong performance in 2011 and who
are committed to driving sustainable, profitable growth in
2012 � and beyond.
Stephen W. Golsby
President and Chief Executive Officer

MessAge FROM THE
ChAiRMAn OF the BOARd

Over the past three years, Mead Johnson has undergone
a successful transformation from an operating division of
Bristol-Myers Squibb to a fully independent and dynamic
public company. I have been able to follow the process from
a unique dual perspective � as the now retired CEO of the
former parent company and as the current Chairman of
the Mead Johnson Board. From both views, the company�s
performance has been nothing short of spectacular.
First and foremost, Mead Johnson has been able to carve
out its own unique identity, with an inspiring Mission and
Vision. It has enhanced its reputation as a trusted nutritional
expert, a successful marketplace competitor, a strong
business partner, a smart investment, and an attractive and
rewarding place to work.
While delivering strong financial performance and growing
market share, Mead Johnson has been investing for the
future, notably in its research and development capabilities.
Since 2010, the company has been steadily expanding its
worldwide network of scientific experts, state-of-the-art
technology centers, academic partnerships and research
collaborations within the framework of the Mead Johnson
Pediatric Nutrition Institute (MJPNI).
In November, the Board of Directors had the chance to visit
the company�s largest market � China/Hong Kong � and
participate in the opening of the new MJPNI center in
Guangzhou. This research and development facility, focusing
on nutrition for Chinese babies and children, will complement
our global R&D efforts. The dedication ceremony observed
traditional and colorful Chinese cultural customs and was
attended by employees, regional government representatives,
business partners and the media. We also had the opportunity
to visit leading Chinese hospitals and retail distribution outlets
for the �Enfa� brand while in the country. For me, the highlight
of the trip was having a private Board dinner with our talented
and dedicated local management and sales management
team at the Canton Tower.
Indeed, what gives me the most confidence in Mead Johnson�s
ability to deliver on its long-term strategic plan is the quality of
its people, such as those I met in China. It starts with the talent
and hard work of Mead Johnson�s worldwide employee team
and is enabled by the outstanding leadership and vision of
CEO Steve Golsby and the other members of the company�s
Executive Committee. I am also proud to lead an exceptional
Board of Directors, whose expertise, insight and counsel have
helped enable the company to flourish.
Although a strong foundation has been laid and much has
already been accomplished, the Mead Johnson narrative is
still being written. We are looking forward to the company�s
future long-term success.
James M. Cornelius
Chairman of the Board