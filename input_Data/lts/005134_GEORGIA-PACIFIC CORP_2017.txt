The financial year 2017-2018 (FY2018) was marked by competitive markets, appreciating Asian
currencies, rising interest rates, component shortages and rising material costs. Despite these
unfavorable market conditions, the Group maintained its strategy of brand building, technology
and product innovation, factory automation and distribution network expansion to compete in this
challenging environment.
Results and Dividend
Revenue for FY2018 increased by 6.0% to S$1,099.7
million, compared to S$1,037.6 million reported for the
financial year ended 31 March 2017 (FY2017).
During FY2018, rising component prices, especially
the rapid price increase of some metals used in
batteries manufacturing from July 2017, combined
with escalating labour costs in China led to a 1.2%
gross profit reduction despite the sales growth. During
FY2018, Asian currencies including Chinese Renminbi,
Malaysian Ringgit and Singapore dollar appreciated
rapidly against US dollar. As a result, the Group reported
a S$11.4 million exchange loss for FY2018 compared to
a S$9.2 million exchange gain reported in FY2017.
During the year, the Group rationalized its brandbuilding strategy for the Batteries Business in
some markets which contributed a 1.1% reduction
in distribution costs. The Group also continued to
consolidate smaller plants into larger factories and
disposed of some excess machineries and properties in
China, which contributed to a higher other operating
income of S$44.5 million for FY2018 compared to
S$22.3 million in FY2017. In addition, the Batteries
Business stepped up its efforts in managing collection
from the distributors in China and as a result doubtful
debt provision decreased by S$3.6 million.
In August 2017, the Group announced a strategic
plan to streamline its corporate structure by making a
voluntary conditional cash offer to acquire outstanding
ordinary shares of GP Batteries International Limited
(GP Batteries). GP Batteries became a wholly-owned
subsidiary of the Group in December 2017 and was
delisted from the Singapore Exchange Securities Trading
Limited on 27 December 2017.
For FY2018, profit before taxation was S$56.9
million, 2.9% higher than last year. Profit after taxation
attributable to equity holders increased by 24.5%
to S$23.2 million when compared to S$18.7 million
reported in FY2017.
The Board has recommended a final dividend of 1.75
Singapore cents per share. Together with the interim
dividend of 1.25 Singapore cents, the total dividend per
share for FY2018 was 3.00 Singapore cents, compared
to the total dividend of 2.75 Singapore cents last year.
Business Highlights
Batteries
The market was highly competitive in FY2018. Revenue
increased by 8.6% in FY2018 when compared to
FY2017. Revenue for the GP branded business,
the OEM batteries business and the industrial sales
business all recorded growth. However, higher labour
costs and sharply rising metal prices significantly eroded
the gross margin for the business. During the financial
year, the appreciation of Chinese Renminbi, Malaysian
Ringgit and Singapore dollar also caused significant
exchange loss to the Batteries Business.
The management continued its strategy to consolidate
smaller production facilities into the larger and more
automated factories. As a result, the management
disposed of some excess production machineries and
industrial properties. Both the expanded factories in 
Malaysia and the new factory in Vietnam started to
contribute to the revenue in FY2018.
The Group will continue to invest into building the GP
brand and its global distribution network. Investments
in building a global e-commerce platform will continue
to receive high priority.
With the delisting of GP Batteries from the Singapore
Exchange Securities Trading Limited, the Group will have
a simpler and more flexible organizational structure for
long-term development.
Electronics and Acoustics
The Acoustics Business recorded a 16.8% revenue
growth while the Electronics Manufacturing Business
recorded a 10.5% revenue decline in FY2018 when
compared to FY2017. The investments in branding and
distribution have started contributing to the growth
of the Acoustics Business. KEFs new media products
have received very positive response from the market.
The LS50 Wireless speaker is broadly recognized in
the market as an innovative product that leads in priceperformance ratio and size-performance ratio. The
Electronics Manufacturing Business has been facing
keen competition, component supply shortages and
rapidly increasing component prices. New products are
well received by customers and are expected to bring
new impetus to the Electronics Business in the financial
year 2018-2019.
Automotive Wire Harness
The revenue of the Automotive Wire Harness Business
declined by 7.0% in FY2018 when compared to FY2017,
mainly caused by the sales decline of a key product
reaching the end of its product life cycle. Sales to the US
market declined by 23.0% while sales in the domestic
China market increased by 30.5%. As the result of the
escalating costs in China, the Wire Harness Business
in the US market has been increasingly challenged by
competition from Mexico and the management has
shifted more focus to developing its business in China,
the largest automotive market in the world. As a growth
strategy, the management is investing into wire harness
technologies for modern automotive electronic control
systems and electric vehicles.
Other Industrial Investments
This business segment includes the Groups investment
in Meiloon Industrial Co., Ltd. (Meiloon) and Linkz
Industries Limited (Linkz). In FY2018, Linkz reported
revenue growth and contributed higher profit. Time
Interconnect Technology Limited, a subsidiary of Linkz,
made a successful initial public offering on the main
board of The Stock Exchange of Hong Kong Limited.
This new development strengthened the financial
position of Linkz. In FY2018, Meiloon also contributed
a higher profit despite a slight decrease in revenue.
Outlook
The markets in the US and Europe are expected to
strengthen while the market in China is expected to
remain strong. However, keen competition, volatilities
of the Asian currencies and rising production costs
will affect the performance of some of the Groups
businesses. Shortages of electronic components,
fluctuating metal prices, rising interest rates and
protectionist attitude in certain countries also cast
uncertainties for the new financial year.
The Group will continue to build our brands and
distribution network. The Group recognizes the strong
potential of e-marketing and e-commerce trading
platforms and will develop the Groups capabilities in
these new sales and marketing channels.
T h e G r o u p w i l l a l s o c o n t i n u e t o i nve s t i n n e w
t e c h n o l o g i e s , p r o d u c t i n n ov a t i o n a n d f a c t o r y
automation to further improve efficiency, productivity
and competitiveness.
Vote of thanks
On behalf of the Board, I would like to thank our staff,
our management team and my fellow directors for their
commitment and devotion during the year. I also thank
our shareholders, customers, suppliers and partners for
their continuous support.
Last but not least, I would like to extend our special
thanks to Dr Andrew Chuang who will retire as an
Executive Director on 1 July 2018 after serving the
Board for almost 23 years.
Victor LO Chung Wing
Chairman and Chief Executive Officer
20 June 2018