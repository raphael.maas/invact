Dear Valued Stockholders,
In 2013, Pepco Holdings, Inc. (PHI) focused on the key business metrics designed
to measure our progress toward enhancing value for our stockholders and
customers. I am pleased to report that we have made improvements in each of
these areas, increasing our Power Delivery earnings by approximately 23 percent,
growing our rate base by 10 percent, achieving one of our best reliability
performances ever, and continuing customer satisfaction on an upward trend.
STRATEGIC VIEW
After a decade of evolution, PHI has become what it set out to be  a regulated utility
company with a robust rate base growth plan focused on strengthening the reliability
of our transmission and distribution infrastructure. To achieve this strategic focus, over
the past several years we have taken some significant steps, including selling our
generating operations, winding down our retail energy supply business and liquidating certain financial investments.
We have emerged better positioned to continue enhancing stockholder and customer value. We have a stronger
balance sheet, a manageable financing plan and a solid business platform. In addition, as a recognized leader in smart
grid deployment, we are well prepared to further evolve PHI by leveraging exciting opportunities provided by new
energy-related technologies and the decentralization of generation sources

FINANCIAL PERFORMANCE
In 2013, our core power delivery business performed well with earnings of $289 million compared to $235 million
in 2012. Pepco Energy Services earnings improved to $3 million in 2013 as compared to a loss of $8 million in
2012. We believe the prospects of our energy services business are positive, with Pepco Energy Services signing
$66 million in energy efficiency contracts in 2013 as compared to $9 million inithe prior year. In addition, W.A.
Chester signed $111 million in underground transmission construction contracts in 2013 as compared to $47
million in 2012.
GAAP earnings from continuing operations were $110 million compared to $218 million in 2012, as detailed
below. Challenges to our 2013 earnings performance included interest associated with changes in the assessment
of corporate tax benefits related to our cross-border energy lease investments resulting in a charge to earnings of
$66 million; the establishment of valuation allowances related to certain deferred tax assets of $101 million; and
impairment charges related to Pepco Energy Services long-lived assets of $3 million, as compared to impairment
charges of $7 million in 2012. Excluding these items, our adjusted net income from continuing operations in 2013
was $280 million or $1.14 per share, compared to $225 million or $0.98 per share in 2012.
REGULATORY INITIATIVES
In 2013, we were very active in the regulatory arena, filing
six distribution base rate cases that requested recovery of
the investments we have made in our system. The four cases
we have concluded provided for total increases of more than
$75 million in annual revenues. We are awaiting final
outcomes in our District of Columbia and Delaware rate
cases, where we have requested a total increase of $83
million in annual revenues.
In 2014, we plan to file new electric distribution rate cases
in our jurisdictions as we continue to seek alignment
between our cost recovery and our investment in utility
infrastructure, which is currently about $1.2 billion annually.
Because filing multiple rate cases each year is expensive and
time consuming, we have been exploring alternative
ratemaking mechanisms for recovering our investment in a
timelier manner.
For example, in our two Maryland jurisdictions, the public
service commission approved a grid resiliency charge for
investments made to improve performance on selected
electric feeder lines on an accelerated schedule. Similarly, in
the District of Columbia, legislation has been passed that will
enable the Public Service Commission of the District of
Columbia to approve a special surcharge to recover
investments associated with an upcoming power line
undergrounding project. And in Delaware, we filed a
forward-looking rate plan that, if approved, will move the
company toward a more efficient and performance-based
ratemaking framework.
Clearly much work remains, but we are making progress.

RELIABILITY IMPROVEMENTS
Our commitment to improving the reliability of our electric system continues. Over the next five years, we plan to
invest $5.8 billion in utility infrastructure. Of that total, approximately $2.3 billion will be spent to improve
distribution reliability and approximately $600 million to improve transmission reliability. The remaining investments
will be related to load growth, customer requirements, gas construction and supporting infrastructure needs.
Our performance metrics show that our investments are having a significant positive impact. The past two years have
been the best reliability performing years in PHI's reporting history. Through the implementation of reliability
enhancement initiatives, our electric distribution system is now considerably
more resilient. I also am proud that our District of Columbia service area is
expected to rank among the best nationally in terms of the average
frequency of system outages in 2013. In addition, we met or exceeded the
reliability standards in all of our regulatory jurisdictions, with the exception
of our Delmarva Power Maryland service area, which experienced several
storms that were not excludable for reporting purposes.
We expect our reliability performance in the District of Columbia to
improve even further after we complete the $1 billion undergrounding
project that was approved by legislation passed by the Council of the
District of Columbia and is now subject to the 30-day Congressional
review period. A power line undergrounding task force, formed by
District Mayor Vincent Gray and co-led by Pepco and the city,
recommended a multi-year program to underground up to 60 highvoltage
distribution feeder lines that historically have been most impacted
by severe storms and overhead related outages.
In a unique public/private partnership to improve grid resiliency, we
anticipate funding for the project to be obtained through a combination
of traditional utility financing, District government revenue bonds and District Department of Transportation capital
improvement funds.
CUSTOMER SATISFACTION IMPROVEMENTS
Turning to customer satisfaction, we ended 2013 with continued progress as indicated by annual surveys conducted on
our behalf. In particular, customer satisfaction in the Pepco region improved significantly, rising 19 points in the fall of
2013 compared to the summer of 2012. In the Atlantic City Electric region, overall satisfaction also rose substantially,
moving the utility into the second quartile in peer benchmarking. For the same time period, overall customer
satisfaction remained stable in the Delmarva Power region.
To further enhance customer satisfaction, we are offering a number of new energy management options. This past
summer, we launched our inaugural Peak Energy Savings Credit program in Delaware and in portions of our Maryland
service area, made possible by our deployment of smart meters in those jurisdictions. Through this program,
participating customers collectively saved $4.5 million and 3.5 million kilowatt-hours of energy, by reducing their
electricity usage on selected days of high energy demand. These are tangible benefits, and we hope to expand the
program to our other jurisdictions in the near future as we complete the installation of remaining smart meters and
gain additional program approvals.
In another major customer satisfaction initiative, we are in the process of developing a new customer relationship
management and billing system called SolutionOne. Our two existing billing systems will be replaced with SolutionOne,
which will serve all of our utility customers. This new system will offer multiple benefits by integrating with other
internal and external PHI systems. During 2014, we will focus on system testing before launching SolutionOne in 2015.

In addition to serving our customers better, I am proud that PHI is a
recognized leader in environmental sustainability. In the most recent report by
the internationally respected Carbon Disclosure Project, PHI was ranked
among the top three S&P 500 utilities for carbon management and
performance. In addition, PHI was recognized for environmental leadership
by GreenBiz Group and Trucost, which identified us as a 2014 Natural Capital
Leadera company that used natural resources most efficiently to generate
revenue over the past year compared to sector peers. These awards attest to
our commitment to lead by example, as we work to improve the
sustainability of the communities we serve.
EMPLOYEE SAFETY
Tragically, despite achieving in 2013 the lowest number of recordable injuries since we formed PHI, two members
of our PHI family died in on-the-job accidents since we published this report last year. At all levels of our company,
we are committed to working safely and are focused on the fundamentals of management leadership and
employee engagement around safety to ensure each employee returns home safely every day.
BOARD AND EXECUTIVE TRANSITIONS
Our stockholders benefit significantly from an engaged, dedicated and driven Board of Directors. In accordance
with the Board retirement provisions of our Bylaws, Frank Heintz, George MacCormack, Frank Ross and Pauline
Schneider will not seek reelection at our annual meeting of stockholders in May 2014. We are extremely grateful
for their combined 40 plus years of outstanding service to PHI. In particular, I want to recognize Mr. Heintz, who
has served as our Lead Independent Director for the past four years. His support and wise counsel have been
invaluable to me personally and our leadership team. Although we will miss the contributions of these four
individuals, our Board succession process has provided for a smooth transition.
In January of this year, I notified our Board of my plan to retire in 2015. I will step down as President and CEO near
the end of 2014, following the selection of a successor, and remain as Executive Chairman of the Board until the
2015 annual meeting of stockholders. I am confident that the Board will select a talented and proven leader to
continue our focus on executing a regulated utility strategy, improving reliability and customer satisfaction,
enhancing stockholder value and ensuring our employees go home safely every day.
Serving as the CEO of PHI has been a unique and tremendous honor. The achievements that I have shared in this
letter are the work of PHIs greatest assetsits peopleand I am very proud of them. I want to thank our
dedicated employees for their focus on safety and accountability, our deep and talented management team for
their expertise and leadership, and our esteemed Board of Directors for their valuable guidance. We are aligned to
our strategy and focused on our key results. I am confident in our future.
As PHI continues on its journey, you can be assured that the Board of Directors is committed to delivering
increasing stockholder value, in part through our commitment to your dividend. PHIs future is bright. I thank you
for your continued support and trust in PHI.
Regards,
Joseph M. Rigby
Chairman of the Board, President and Chief Executive Officer
March 25, 2014