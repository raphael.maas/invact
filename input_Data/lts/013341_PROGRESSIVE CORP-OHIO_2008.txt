Letter to Shareholders
Wow! Followed by the claim, I say it louder, was an effective
interjection in the debut television commercial of our revamped
2008 advertising efforts, but might equally be used with a less
positive tone as a descriptor for the business year in the U.S.
2008 for Progressive had plenty of Wow!, some very positive
and encouraging, and some just flat out disappointing.
Reporting a net loss, the first in 26 years, is unquestionably a
tough pill to swallow and calls for a clear, open, and objective
diagnostic. My tendency is to focus on the actions we most
directly control and as such Ill lead off with a high level review
of operating results and environment before getting to our
investment and capital story which, this year, takes on unprecedented importance.
Writing this time last year, I summarized a prospective view of
the auto insurance rate environment as becoming more normal.
A view formed after observing, and participating in, the prior year
and a half of widespread rate reductions and the emergence of
an increasing rate need to match inflation in claims cost.
The year started off essentially true to that script. By June the
nation was focused on many issues, gas prices being high on
the list for most. The implication for our business was a sudden
and dramatic reduction in claims frequency. While the change
in consumer behavior, which led to fewer miles driven, fewer
exposures, and fewer accidents, was a societal positive, it added
a level of complexity to those who must price for future conditions, with little precedent for guidance.
Falling frequency and explanatory theories have been a
topic in these pages and for many industry commentators
throughout the current decade, but the sudden turn in 2008
was an entirely different event. The graph makes the point better than my words.
By later in the year, gas prices were less than half their peaks
and one might assume with perfect elasticity of demand all would
be as it once was. But, the country was now in an economic
tailspin and, at a minimum, consumer uncertainty prevailed.
Consumer behavior did not show perfect elasticity and the return to frequency levels of even the first quarter was dampened.
Modeling certain response variables important to us against
external data, such as unemployment rates, is critical, but challenging statistically. Rates of uninsured drivers, changes in
valuation of vehicles and parts, fleet aging dynamics, bad debt
propensity, medical treatment patterns, and the like are all
subject to change in the current environment. Our models, while
interesting, will never substitute for our ability to observe and
respond quickly. Our internal response is to assume change is
at an all-time high, be nimble in responding, reduce the time to
implement rate change, and assume that with any change we
may need to iterate quickly to match the environment.
Profit
Our 94.6 combined ratio for the year is testament to our responsiveness and respect for the conditions. The result is well in-line
with our target of a 96, and needs no qualifying statements regarding prior year development or storm adjustments.42 of our
established 50 states, which includes the District of Columbia,
were profitable for the year as were 8 of our largest 10 states,
including New York auto, which was a definite concern in 2007. Duplicating this profit margin next year will be very pleasing, but will
take incredible vigilance. In my view, thats Progressive at its best.
I distinguish established states to make special mention of our
entry into Massachusetts, which occurred on May1st. Progressive
is now available in every state of the nation. Additional commentary on Massachusetts is included in the Operations Summary.
Growth
Growth, by our standards, has been stalled for some time. Measured by premiums written that statement is undeniable; our 1,3,
and 5 year written premium growth has been (1)%, (1)%, and 3%,
respectively. Corresponding industry growth has been less than
1% for the three year period, 1% for five, and an estimated .5%
for the past year. As noted earlier a prolonged period of negative price adjustments is part of the explanation. In 2008 we saw
our average auto premiums on our new Agency business applications increase slightly over the prior year for the first time in 2
years, while the similar measure in our Direct business still lags
the prior year by 6%.
A healthier measure and our preferred one is the number of
customers served. Unfortunately there is no consistently reliable
comparative measure for the industry. Our results here are
considerably more encouraging with continuous increases in
numbers of policies in force. We ended the year up 3%, which translates to 350,000 additional policies spread across our product offerings. A closer look will show these additions have come
in our Direct auto offering, which has been and continues to
be a referendum on changing consumer shopping behavior. Our
9% growth in policies, and 4% increase in written premiums,
coupled with attractive improvements in expected customer
tenure, are top echelon results in this environment and forecast
to me even better days ahead.
Our industry leading special lines group, serving motorcyclists,
motor home and small boat owners, also gained a disproportionate share of our new customers in 2008. Some of these products
represent more discretionary consumer spending and, in the near
term, we may struggle to see significant market growth in units.
Similarly our Commercial Auto business group turned in a
very respectable year, but ended the year with about the
same number of policies as it started the year. The more recent
trends in growth and retention reflect the depressed outlook
for contractors and tradesmen.
Our biggest challenge continues to be growing our agent
business. While increasing our acceptance for agents more
preferred and commercial customers, we have lost some momentum in our traditional strength niche for agentsnonstandard
auto. Agents are critical to our success and our focus is, as it
should be, on providing products that continuously allow them
to win with Progressive. The Operations Summary in this
report will provide more details on specifics of new product
and service developments for agents. Our goal is a constant
and always worth reinforcingTo grow as quickly as possible,
constrained only by our profitability objective and ability to
provide high-quality customer service. That said, we must aggressively meet all competitive challenges through increased
segmentation and innovation.
Investment and Capital
A pretax underwriting income of some $735 million was, with
the above qualifications, an acceptable result and highlights
the quality of earnings we are capable of even in times when
more draconian changes may be inflicted on other parts of the
economy. This very premise is the basis for our long-standing
investment and capital management policy to maintain a liquid,
diversified, and high-quality investment portfolio. In short, our
primary investment goal is to ensure we never constrain our
ability to write as much insurance as we can. 2008 has exposed
us to market conditions that require us to do some additional
soul searching on our investment philosophy. Our intent remains unchanged. However our accepted views of liquidity,
quality, and diversification have all been severely challenged.
The accounting for invested assets in
2008 was, for me, an academic version of
water torture. Assessments of impairment
done in one quarter may require further
impairment in the next. Reliable market
pricing, in times of high volatility, added
a new level of challenge and diligence.
We have handled our accounting obligations with the openness and transparency
we believe characterizes Progressive.
Many years ago a colleague said,Embarrassment is just a timing difference,
an expression for which I and others
maintain great affinity, and believe reflects
in all our disclosures.
Our monthly reporting made our capital
position consistently available, however
balance sheet versus income statement
presentation depended on the timing of
impairment assessments. Experience
gives rise to knowledge and in this case
we believe we can provide additional
benefit to readers by adding comprehensive income and derived comprehensive
income per share to our monthly disclosure. Without reducing the importance
of net income, it should provide our
owners an additional, and at times more
consistent, all-in economic data point.
During the year we recognized net realized
losses, including other-than-temporary
impairment losses on the portfolio, of
some $1.4 billion (I had trouble typing
that), or about 10% of invested assets,
all culminating in a net loss of $70 million
for the year.
So, what do we know now? And, what
can we take away from this experience?
This may well be an unrepeated event, but
the lessons for many should be invaluable and the tuition has been paid.
We have codified lessons learned
based on what we thought were our
intents and expected outcomes versus
what we now know was actually possible. For example I reported last year that
our direct exposure to sub-prime related
instruments was small. However we also
had exposure to the largest banks and
financial institutions that had such risk.
It is clearer to us now that our indirect risk
was far greater than the direct risk we
avoided, and yet we fell short in anticipating the impact in the same way we
normally expect of ourselves.
Similarly, we allowed our concentration
guidelines to permit us to favor Government Sponsored Entities, such as Fannie
Mae and Freddie Mac, operating under
failed expectations of just what Government Sponsorship would mean. Detailing
the specifics is perhaps less important
than the recognition that, regardless of
the environment, there are opportunities
to improve what we do. The extremes of
outcomes that were far beyond those
seen or imagined in the economy forced
us to adopt a mantra of Imagine the
Unimaginable.Only then could we break
with thinking constrained by norms that
no longer applied. Breaking with thinking that defines the norm is, in large part,
what characterizes Progressive and has
been the spirit that has given rise to innovations such as concierge claims service,
comparative rating, pet injury coverage,
and usage-based insurance. So, for our
2008 report, it seemed apropos for the
art to reflect the notion of Imagine the
Unimaginable.
The investment results and market valuations clearly eroded our capital position,
raising reasonable questions about the
need for replacement capital. I used the
third quarter letter accompanying the
10-Q to provide insight into how we think
about capital and, in effect, constructed our capital position using a regulatory required layer, extreme
contingency reserve layer, and an excess layer. While the construct was for illustrative purposes, it is in fact an excellent model
of our capital husbandry.
While the loss of capital was clear, for some it was less clear
that the loss was contained at the excess layer, a layer largely
held at the holding company and outside the insurance subsidiaries. Dividends are made out of and, as necessary, into the
insurance subsidiaries to ensure they meet appropriate capital
requirements. We ended the year in a similar position to my
summary comments in the third quarter letterseveral hundred
million dollars above the sum of the regulatory and extreme contingency layer. While some return in investment valuations would
be welcome, we are not depending on any in the short term. We
have taken significant steps to restructure the portfolio and, more
specifically, the deployment of any new money to considerably
lower risk investments for now. While yields will match the risk,
our ability to generate operating income is the protected asset
and drives our prioritization. The prioritization is accentuated
when one considers that our operating cash flow has now been
positive for 100 consecutive quarters.
An Update
I said in my opening that there were some positive and encouraging Wows! And if youre ready for a change of pace I would
like to share a few.
In closing this letter last year I said, Our opportunities are
clear and exciting and include and listed five bullet points.
I repeat them here as my sub-headings along with some illustrative examples of meaningful progress made during the year. A
broad-based agenda such as this is never done, but I think you
will see we had some very real progress.
Building a stronger brand and communicating it well We let
Flo loose on consumers in 2008 as part of our television
advertising, in a campaign that by all accounts has trumped
our prior efforts. I report that much less based upon feelings
and anecdotes and more from the measurement analytics that
support all our actions.
I reported last year that we challenged ourselves to produce
advertising that has thematic continuity, makes a real breakthrough in interest, and has a strong call to action. The consumerfamiliar setting of a superstore provides a constancy of identity
and foundation for delivery of our many messages. While auto
insurance doesnt come in a box or service in a can, building on
the store-based metaphor provides tangibility over mystery,
enhances approachability, and invites trial for many consumers.
Less analytical assessments can be derived by consumer requests for their policy to in fact be delivered in a boxa request
we were initially not well equipped to handle.
A key objective is to have our efforts in marketing and brandbuilding match our assessed competency in other technical
skillsand sooner is always better. Adding the experience
that Larry Bloomenkranz brings as our Chief Marketing Officer
during the year is just one step of many that we have taken to
make this happen.
Advertising, however, is but a part of our marketing and branding efforts. Our mid-year Investor Relations meeting used
multiple consumer profiles to highlight internal marketing constructs that segment consumer behaviors. Simply stated,Know
your TargetAct with Purpose, while not a breakthrough in
marketing thought process, it is a very powerful notion when
implemented well in our product and service organizations.
Early in 2008 we announced we would be the title sponsor for
the Automotive X PRIZE, now known as the Progressive Automotive X PRIZE. This worldwide competition, with a prize purse
of $10 million, is designed to inspire a new generation of viable,
super fuel-efficient vehicles that are affordable and meet market needs for capability, safety, and performance.
Following in the footsteps of the British Governments Longitude prize in1714 and the Orteig prize won by Charles Lindbergh
in1927, the X PRIZE Foundation has already changed the worlds
views of what is possible in space flight, and has active plans to
do similar things in genetics. There can be little question that our
fossil fuel dependence is an issue that needs meaningful leadership. Employees and I are proud to associate the Progressive
brand with a relevant and bold effort to help move innovations
forward even faster on this challengewe think it fits perfectly.
We measure effectiveness of all such actions in economic terms
as best we can and we will have little to report until 2010, the year
the contestants will offer their solutions. If media interest and
consumer response to date are indicative, these will be dollars
well spent on something that also feels very right. For updates on
this effort and worldwide entries see progressiveautoxprize.org.
Building on our retention gains and providing continuity of coverage throughout a customer's lifetime Retention of customers
has been a significant focus for some years now, and at times the
slow speed of change in certain measures has posed doubt in our
belief that we could effect more dramatic change. 2008 confirmed
our confidence in the magnitude of change that is possible.
In a moderately increasing rate climate, we continued to see
an increase in customer retention with both our Agency and
Direct auto policy life expectancy measures increasing 11%.
Improvement in our special lines products was numerically less
at around 1% but, as a group, they remain our longest-tenured
customers. Commercial customer measures ended the year
slightly lower than where they started, after some initial gains,
reflecting the somewhat shocking numbers of trucks being taken
out of service and the general economic pull back.
Good luck or hard work? Either way when the sensitivity of one
month of policy life extension is easily valued at over one billion
dollars of lifetime premium, the result is bankable. The answer,
without doubt, is in fact hard work over a prolonged period of time.15
My words likely dont do justice to the
degree of change this represents and,
while not an objective, it is of some note to
see favorable positioning or improvement
in external measures of customer satisfaction. To be fair, our self assessment is
that we still have many opportunities to
further improve customer experiences
and permanently eliminate all too frequent
lapses in quality. I suggest with some satisfaction and confidence that our momentum here is contagious and our objective
of making Progressive what we call a
destination auto insurer is very real.
Maintaining a focus on operating at a
lower cost than competitors, while providing remarkable service Creating expense advantages that are sustainable
generally require doing things differently,
not just better. In 2008, our Claims and IT
organizations made substantial progress
on redesigning how they do what they do
while operating at lower costs.
For many years we have been able to report each year as our best ever in claims
quality. This year is no different. The preservation of claims quality is a must, along
with the equally encouraging progress on
customer satisfaction. However, skills
and tasks can be rearranged to optimize
even a well-performing process and, while
our guiding principles remain unchanged,
we see opportunities. Our concierge centers have provided invaluable insights
and surfaced opportunities to apply those
insights more broadly, and as such are
central to the redesigned process architecture. Proof will be in our future loss
adjustment expenses.
Reduced claims frequency, while at first
a welcome sign, poses significant challenges in maintaining the balance between
appropriately sizing the claims organization, and preserving the talent and experience required to support growth and
any return to prior frequency levels. Claims
management has met that challenge with
creative solutions that are economically
satisfactory, consistent with our workplace culture objectives, and advance
the redesign process even faster. The
supply and demand of claims and claims
response will likely remain a significant
management challenge throughout 2009.
Our dependence on, and self-assessed
strength in, technology has been reported
on frequently by me and remains true. The
challenge imposed and actively accepted
in 2008 within our technology organization
is the age-old desire to do more with current resource levels, while doing the right
things well:Smart choices, well executed.
In a vibrant business culture, demand
for technology resources may well outstrip supply. In 2008 our business and
technology groups have reassessed
just about every aspect of what we do
and how we do itto make the smart
choices, well executed by line more
operationally reflect business priority and
intensity. I have every expectation these
efforts will result in increased leverage of
technology costs and greater accountability to market priorities.
Creating more responsive product and
service offerings for the consumers we
currently do not reach Progressive Home
Advantage, our offering which combines
a Progressive auto policy with a homeowners or renters policy underwritten
by Homesite Insurance, expanded consistent with our expectations in 2008 and
now serves a growing number of our
customers, many of whom have added
the offering to their existing auto policy.
We provide this important option to customers in 46 states for Direct buyers and
in 33 states for Agency customers. The
We have used this report and other forums to highlight our
intensity into what we have called friendly fire incidents, or rough
product edges, along with our actions to address them. We have
continuously embraced the concepts and analytics of total quality management, and of more recent times adopted, and now have
become somewhat recognized for, the depth of application of the
Net Promoter Score. What has really happened is that we have
matured from tactically addressing just retention measures or
customer experience improvements on multiple fronts into a more
deliberate customer care culture that is embraced, managed,
and advanced at every level of the organization by those who
correctly see themselves as brand ambassadors and disciples.
My words likely dont do justice to the
degree of change this represents and,
while not an objective, it is of some note to
see favorable positioning or improvement
in external measures of customer satisfaction. To be fair, our self assessment is
that we still have many opportunities to
further improve customer experiences
and permanently eliminate all too frequent
lapses in quality. I suggest with some satisfaction and confidence that our momentum here is contagious and our objective
of making Progressive what we call a
destination auto insurer is very real.
Maintaining a focus on operating at a
lower cost than competitors, while providing remarkable service Creating expense advantages that are sustainable
generally require doing things differently,
not just better. In 2008, our Claims and IT
organizations made substantial progress
on redesigning how they do what they do
while operating at lower costs.
For many years we have been able to report each year as our best ever in claims
quality. This year is no different. The preservation of claims quality is a must, along
with the equally encouraging progress on
customer satisfaction. However, skills
and tasks can be rearranged to optimize
even a well-performing process and, while
our guiding principles remain unchanged,
we see opportunities. Our concierge centers have provided invaluable insights
and surfaced opportunities to apply those
insights more broadly, and as such are
central to the redesigned process architecture. Proof will be in our future loss
adjustment expenses.
Reduced claims frequency, while at first
a welcome sign, poses significant challenges in maintaining the balance between
appropriately sizing the claims organization, and preserving the talent and experience required to support growth and
any return to prior frequency levels. Claims
management has met that challenge with
creative solutions that are economically
satisfactory, consistent with our workplace culture objectives, and advance
the redesign process even faster. The
supply and demand of claims and claims
response will likely remain a significant
management challenge throughout 2009.
Our dependence on, and self-assessed
strength in, technology has been reported
on frequently by me and remains true. The
challenge imposed and actively accepted
in 2008 within our technology organization
is the age-old desire to do more with current resource levels, while doing the right
things well:Smart choices, well executed.
In a vibrant business culture, demand
for technology resources may well outstrip supply. In 2008 our business and
technology groups have reassessed
just about every aspect of what we do
and how we do itto make the smart
choices, well executed by line more
operationally reflect business priority and
intensity. I have every expectation these
efforts will result in increased leverage of
technology costs and greater accountability to market priorities.
Creating more responsive product and
service offerings for the consumers we
currently do not reach Progressive Home
Advantage, our offering which combines
a Progressive auto policy with a homeowners or renters policy underwritten
by Homesite Insurance, expanded consistent with our expectations in 2008 and
now serves a growing number of our
customers, many of whom have added
the offering to their existing auto policy.
We provide this important option to customers in 46 states for Direct buyers and
in 33 states for Agency customers. The objective is simple: reach new customers
who we might not have been reaching and
retain them and existing customers longer.
So far, so good, and in 2009 we will make
it available through additional agents.
We have placed additional emphasis
on understanding the needs of our multipolicy households, especially those with
special lines products in addition to their
auto. We clearly have more opportunity
and have set challenging internal goals
for multi-policy penetration in 2009 based
on achieved results to date.
Our usage-based insurance offering,
now called MyRate,
SM
expanded within and
across states in 2008. Notwithstanding
the continuous challenges posed by such
a new concept, the opportunity to reach
a set of customers that have significantly
different usage patterns than traditionally
underwritten products would recognize
is clear, and welcomed. Consumer acceptance is good. For us MyRate is slowly
moving from feeling like a new and different product to becoming a very viable
consumer option that speaks to a specific consumer segment.
Continuing to be innovative in all we do
The depth of this notion is not easily captured by a single example, but one may
serve well to amplify the thought and
share a recent product introduction.
Price is a clear factor in a consumers
purchasing decision and many efforts,
including our own, have focused on providing consumers with increased access
to price comparisons between providers.
This year we introduced a new concept
we call Name Your Price,
which invites
the consumer to participate in the quoting process by telling us just how much
they would like to spend on car insurance.
We start the quoting conversation in
a place determined by the customer.
Then we use combinations of coverages,
limits, and deductibles to meet or approximate their needs. Involving the consumer
in this way is both respectful of their preferences and involves them more actively
in the process.
Early results are very encouraging, but
even more important is the opportunity to
use technology to engage the consumer
in a meaningful and different way ensuring they get what they want, at a price
they can manage.
Looking Forward
We remain continuously motivated by our aspiration of becoming Consumers #1 Choice for Auto Insurance and, while our
numerical progress in 2008 did not match our aspirations, some
of our actions set an interesting stage for the future.
Nothing we have achieved has been without the efforts of
so many and, our single most important initiative especially in
current times, is ensuring Progressive is a Great Place to Work.
Creating an environment where our people enjoy working hard,
are motivated to do their best, can grow constantly, and that
others want to join is a never-ending challenge. Our people and
culture are what makes us special.
Equally important is our appreciation for the customers we
are privileged to serve, the agents and brokers who choose to
represent us, and shareholders who support what we are doing.
Glenn M. Renwick
President and Chief Executive Officer
