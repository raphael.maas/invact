To Our Shareholders
At Grainger, we help professionals keep their operations
running and their people safe. Our customers trust and
rely on us every day to provide the right products, services
and solutions, when and where they need them. They are
the backbone of economies around the world and the
reason we exist as a business.
With more than two million customers of
all shapes and sizes across multiple countries,
our relationships are a testament to our
understanding of facilities maintenance
professionals. While their industries vary,
their needs dont. All businesses and
institutions today must maintain productive,
safe and efficient operations. To do this,
customers need access to a broad product
offering, fast delivery and Grainger team
members who understand their business and
are committed to their success. Time is money
for these professionals; when motors break
or the lights go out, their businesses stop.
Our commitment is to help them get their
jobs done every time.
We continue to operate in an increasingly
complex and unpredictable global economic
environment. This creates opportunities for us
to gain market share by helping our customers
simplify their business using a trusted and
reliable partner  Grainger.
2013 HIGHLIGHTS
A solid year and more opportunity ahead
With record sales and earnings in 2013, Grainger
is a healthy and financially strong company.
 Sales for the year were $9.4 billion, an increase
of 5 percent versus 2012. Reported earnings
per share were $11.13, up 17 percent; on
an adjusted basis, earnings per share were
$11.52, up 10 percent.We had $0.39 per share
of impairment and restructuring charges that
were a result of underperformance in some of
our smaller businesses.
 Our continued productivity initiatives allowed
us to generate $190 million in savings, of
which $132 million was reinvested in growth
and infrastructure programs.
 In 2013, cash flow from operations was
$986 million, enabling us to fund capital
expenditures of $272 million and $154 million
in acquisitions.We returned $693 million in
cash to shareholders, including $255 million
in dividends and repurchased 1.7 million
shares of stock.
Even with these results, we have more work to
do. While I was pleased with how our largest
businesses performed this past year, we have
other areas that are notmeeting our expectations.
We are taking the appropriate steps to improve
the growth and profitability of several of our
smaller businesses.
Competing through two models
In the highly fragmentedmaintenance, repair
and operating (MRO)market, there is a large
amount of share to be gained, and we are well
positioned to take advantage of the opportunity.
We know that while our customers needs are
universal, how customers buyMRO supplies
differs significantly based on the size of their
business. These behaviors are driving how we
think about our business and have helped us
bring greater focus to how we serve customers
of different sizes.
Driven to lower their total cost of ownership,
large customers tend to have more complex
needs and require services more customized to
how they operate. They want a broad product
and service offering available through the
phone, branch, sales representative and
eCommerce channels. To meet this need, we
run our multichannel business model in all
geographies except Japan.
On the other end of the spectrum, many
small customers tend to use fewer services,mix
business and personal purchases and are often
willing to spend more time shopping, often
on the web.We serve these customers through
our online business in Japan,MonotaRO, and
our growing Zoro Tools business in the United
States. I am excited about the great success
we are seeing with both of these businesses.
In 2013, sales forMonotaRO grew nearly
20 percent in local currency, and revenue
for Zoro Tools grew more than 150 percent.
Scale matters
In the distribution business, scale matters.
It provides the opportunity to drive higher
levels of customer service, more productivity
and ultimately more growth. In 2013, we
continued to invest in our supply chain and
systems infrastructure to create economies
of scale and competitive advantage.
 Ramped up operations at our new
Minooka, Ill., distribution center. This
Leadership in Energy and Environmental
Design (LEED) Commercial Interiors (CI)
Platinum facility features the worlds largest
goods-to-person installation, an automated
system that increases productivity and
maximizes storage space by stocking and
dispensing more than 500,000 products.
 Made progress on the construction of a new
500,000 square-foot distribution center in
the Toronto area, which will be two and a
half times larger than the current one.
 Transitioned to a new web platform in the
United States to offer a consistent purchase
experience for customers across multiple
purchase channels, launched a Spanish
language website and introduced innovative
mobile solutions including a new iPad app.
 Continued our work to design and build more
scalable, responsive and flexible information
systems in Canada andMexico.
 Launched an initiative to improve our fastener
offering and drive a better cost position globally
by leveraging the expertise of our European
fastener business, Fabory.
Growth through service
Graingers differentiating factor has always been
service.When we invest in things that matter to
our customers, create value for their business
and deliver it better than anyone else, we gain
more of our customers purchase volume.
In 2013, we continued to make investments in
several capabilities that help our customers take
cost out and become more productive. These
capabilities have proven returns and continue
to be drivers of market share gain globally.
 Added more than 200 new sales
representatives in North America to help
customers find solutions for theirMRO needs
and reduce their total costs. Relationships
matter in this business. In general, sales to
customers with a sales representative grow
at twice the rate of customers who are not
covered by a salesperson.
 Bolstered our inventory management
services across the Americas and Europe,
making it easier for customers to lower
inventory and reduce labor costs in their
business. Installations for vendor- and
customer-managed inventory and vending
machines totaled approximately 60,000 in
2013. Sales to customers with a KeepStock
installation also grow at twice the rate of
non-KeepStock customers.
 Surpassed $3 billion in eCommerce sales in
2013, representing 33 percent of total company
sales. eCommerce is an integrated part of our
multichannel business. Revenue through this
channel grew 15 percent last year, and it is
the most profitable channel in the business.
 Added more than 300,000 new products
to Grainger.com, bringing the total number
of products online to more than 1.2 million.
In Canada, AcklandsGrainger announced
the addition of 200,000 products to its
online offering. A broader product line
enables customers to increase productivity
by consolidating their supplier base.
 Gained momentum with recent strategic
acquisitions in the United States. We
view acquisitions as a vehicle to accelerate
organic growth in key end markets. The
acquisitions of both E&R Industrial Sales, Inc.
and Safety Solutions, Inc. have helped us
broaden our offering to manufacturing
customers on the plant floor.
People make the difference
Embedded in this business is the belief
that engaged, hard-working and talented
people ultimately provide great service and
make Grainger a better company. We have
attracted and developed a terrific mix of people
who have spent much of their career with
Grainger and are experts on this industry.
They work shoulder-to-shoulder with new
teammembers who have come from other
industries and bring different experiences.
This is a powerful combination.
Grainger team members not only make a
difference for our customers, they extend
that commitment to service into our local
communities. In 2013, the company continued
our long-standing relationship with the
American Red Cross, completing a $3 million
grant to help launch Volunteer Connection,
an online volunteer management system that
enables the Red Cross to more efficiently
recruit and deploy volunteers across the
country. Through our ongoing support of
the Ready When the Time Comes program,
Grainger volunteers helped the Canadian
and American Red Cross respond to multiple
disasters this past year, including flooding in
Alberta, Canada, and storms in theMidwestern
United States. Im very proud of Graingers
legacy of helping communities in need.
As part of our commitment to environmental
sustainability, we continue to improve our
capabilities to measure, report and manage
our energy use. Over the past year, weve
made investments to improve energy efficiency
at several of our distribution centers and
continued our commitment to building LEED
certified facilities. In 2013, Grainger also
expanded the reporting of our carbon footprint
through the Carbon Disclosure Project beyond
our core U.S. business to include two of our
international businesses, AcklandsGrainger
and GraingerMexico.
Investing for the long term
Grainger is a strong company with the
confidence to invest in our business for the long
term. Across the company, we will continue to
add to the capabilities and services that help
customers manage their business and drive
growth for Grainger. At the same time, we
will further strengthen our infrastructure
through new distribution centers and updated
information technology systems to add capacity
and improve customer service.
We know that the investments we are making
help drive growth and shareholder value. In the
past five years in particular, we have seen some
of our largest market share gains and some of
our biggest increases in shareholder value.We
are committed and disciplined to find ways to
invest in this business and continue this trend.
Together with our team members, suppliers
and other businesses partners, Grainger will
continue to advance its leadership position in
theMRO market by serving the professional
customer better than anyone else.
Helping to guide Grainger for 24 years has
been JohnMcCarter, a long-standing member
of our Board of Directors and trusted adviser.
As John leaves the Board this year, we thank
him for his knowledgeable perspective and
dedicated service.
I am confident and excited about where
Grainger is headed.We know our customers,
what is important to them and how their
business runs. The opportunities in the years
ahead are energizing. This is a great industry
and we have a proven strategy to win.
James T. Ryan
Chairman of the Board, President and
Chief Executive Officer
February 27, 2014