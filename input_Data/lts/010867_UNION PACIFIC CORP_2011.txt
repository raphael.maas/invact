Fellow Shareholders:
Our 2011 results demonstrated the capabilities of the men and women of Union Pacific and the strength
of our unique and diverse franchise, contributing to the safest and most profitable year in Union Pacifics
150-year history. Both top and bottom line results achieved historic milestones and generated record
earnings of $6.72 per share and a return on invested capital of 12.4 percent. We rewarded shareholders
with improved financial returns, including a 58 percent increase in the quarterly dividend per share and
more than $1.4 billion in share repurchases. UPs stock price reached new highs in 2011, increasing 14
percent and outpacing the S&P 500 by 14 points as well.
We accomplished this while overcoming many challenges: global financial market turmoil, ongoing
economic uncertainty, historic flooding in the Midwest and extreme drought conditions in the South.
Despite these challenges, we maintained efficient network operations, met our customer commitments,
and continued operating a safe and productive railroad.
As volumes increased, we maintained excellent customer service and achieved many new safety records.
With Total Safety Culture flourishing across our company, employee injuries hit a record low in 2011,
capping a decade of significant improvement. Customers also rewarded us with record satisfaction
ratings, clearly valuing our service offerings and the efficiencies we provide as part of their total supply
chain.
Capital investments play a critical role in meeting the long-term demand for transportation. In 2011, we
invested a record $3.2 billion across our network. Over half was spent on replacing and hardening our
infrastructure to further enhance safety and reliability. The balance was invested to support business
growth. Projects large and small, in all regions where we operate, will enable us to serve our customers
well in the years ahead. This level of investment is supported by the strong financial returns we generated
in 2011.
Public officials are increasingly aware that a healthy, efficient rail network is critical to our countrys
growing need for a strong transportation infrastructure. With public spending for infrastructure under
pressure, the investments we are making will help our customers and our country compete in an
increasingly global economy. We work hard to make sure that policy makers and regulators understand
the relationship between financial results and investment so that new regulation does not hinder future
opportunity for our customers.
As we focus our efforts on 2012 and beyond, we see continued prospects for growth from our existing
customers, along with new, emerging market opportunities. Union Pacific plays a vital role in the global
supply chain. International trade currently represents one third of our revenue base, and it is growing. An
increasing U.S. population base will stimulate long-term demand for most of the goods we carry. As the
only railroad to serve all six major gateways to Mexico, we are in an excellent position to benefit from
expanding U.S. trade with Mexico.
With the development of new and improved horizontal drilling techniques, the U.S. has seen a dramatic
expansion of drilling activity for crude oil and natural gas at various shale formations around the country.
Were extending our energy reach by providing an efficient rail-based supply chain to support the
ongoing expansion of this business.
Last year was a historic milestone that set the stage for 2012, which marks 150 years of history for Union
Pacific. The generations who came before us helped build a nation, and today Union Pacific continues to
play a critical role in helping our country grow and prosper. Were making significant capital investments,
generating increasing financial returns, providing fuel efficient and environmentally responsible
transportation, and creating solid, well-paying jobs. The bar has been raised for Union Pacific in 2012 and
our prospects have never been better. We pay tribute to our 45,000 employees, whose innovation and
teamwork will move us forward to even greater accomplishments in 2012.
Chairman, President and
Chief Executive Officer
