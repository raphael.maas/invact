...
today
Annual Report 2006
4
NEWS CORPORA
TION
F
iscal 2006 was a year of change, challenge
and achievement for News Corporation.
We accelerated the transfor
mation of our
company fr
om a traditional media company into a
major digital player
. And we did so while maintain
-
ing
the competitive edge that has made our mor
e
established businesses the leaders in their fields.
Full year r
esults once again speak for themselves
 and mor
e loudly than ever
. Operating income
rose to r
ecor
d levels at nearly all segments of our
company
, including Dir
ect Br
oadcast Satellite
Television, Cable Network Pr
ogramming, T
elevision
,
Filmed Enter
tainment, Magazines and Inser
ts,
and Book Publishing. Ear
nings per shar
e fr
om
continuing operations r
ose by 26 per
cent to 87
cents, its highest level ever
.
Chief Ex
ecutiv
e Offic
er's Lett
er
imagine
opportunity
I could not be pr
ouder of our accomplishments
this past year and over the last several r
ecor
d-br
eak
-
ing years. Y
et I am even mor
e impr
essed by the ener
-
gy and intelligence with which our employees and
senior management have grasped the oppor
tunities
of the digital r
evolution  a r
evolution that holds the
promise of changing our world as fundamentally as
the Agricultural and Industrial Revolutions. Digital
technologies  br
oadband, mobility
, storage and
wir
eless  play to our company
s historical str
engths:
offering consumers the widest possible ar
ray of
quality content, deliver
ed in ways they pr
efer
.
News Corporatio
ns cor
e mission has always
been to pr
ovide as many consumers as possible
with the highest quality content thr
ough the
most convenient distribution channels. That was 6
NEWS CORPORA
TION
Now that we have the traf
fic, we ar
e thinking
creatively and aggr
essively about how to translate
that traf
fic into rising pr
ofits. Revenues fr
om
MySpace alone have nearly doubled ever
y four
months over the past year
. And others ar
e noticing.
Earlier this summer
, after the fiscal year
-end,
we announced a landmark deal with Google to
provide sear
ch functionality to most of our Inter
net
sites  most impor
tantly MySpace. Google will
also become the exclusive text-based adver
tising
provider and has the right of first r
efusal on
all r
emnant display adver
tising. W
ith at least
$900 million committed to us over four years, this
agr
eement mor
e than pays for the MySpace
acquisition.
Mor
e impor
tantly
, it allies us with
one of the gr
eat companies of the digital age,
while signifying our ability to monetize our traf
fic
in ways that make sense for our audience.
This deal is but one way we ar
e generating
revenues fr
om our Inter
net businesses. In just the
past few months, we have sharply incr
eased the
number and quality of FOR
TUNE 100 companies
buying display ads on our sites, ther
eby r
eaping
the r
ewar
ds of higher ad rates. As we focus on
growing r
evenues, we ar
e also intent on impr
oving
the user experience for our 120 million-plus users
worldwide. MySpace
s of
ferings have expanded
mor
e in the past thr
ee months than in the pr
evious
four years since its cr
eation. W
eve added str
eam
-
ing video, special comedy clips, fr
ee classified
ads and instant messaging. In a major deal with
Bur
ger King, we made available on MySpace
episodes of hit shows like
24
. MySpace is a major
driver of traf
fic to the so-called Inter
net 2.0 sites
 pictur
e-hosting sites like Photobucket and video
repositories like Y
ouT
ube. Ther
e is no r
eason why
we can
t replicate that functionality and r
etain
that traf
fic  and its attendant r
evenue.
Ther
e is mor
e to this r
evolution than just the
Inter
net, however
. The br
oader ter
m digital better
captur
es the scope of the tr
emendous change and
tremendous possibilities that lie ahead. No less
impor
tant than online media ar
e mobility and
storage, and we ar
e active on both these fr
onts.
Earlier this year we launched Mobizzo, a website
that makes available digital content for cell
phones and other mobile devices. This is a first
for any major media company
, and it follows the
success of our intr
oduction of the mobisode,
a shor
t, downloadable show based on popular
characters and franchises. W
e ar
e making available
music, ar
t, games, video clips and other content 
much of it based on popular Fox brands such as
Family Guy
,
American Dad
,
Napoleon Dynamite
and
Ice Age
 that can be pur
chased and downloaded
dir
ectly to a mobile device.
With r
espect to storage, we have made some
exciting new deals and intr
oduced some innova
-
tive new pr
oducts. W
e completed an agr
eement
with Apple to make television content fr
om FOX,
FX, SPEED, FUEL TV and the T
wentieth Centur
y
Fox T
elevision librar
y available at the iT
unes
Music Stor
e. Customers can pur
chase and down
-
load our content onto their computers or iPods.
The pr
ecise business model for sustained pr
of-
itability fr
om our digital investments is still
uncer
tain at this point. Consequently
, in some
ways, we ar
e embarking on a period of trial and
error. But we ar
e confident that, as the most
innovative and fearless media company in the
world today
, we
can devise a model that delivers
a first class digital
experience for our customers
and significant
revenues for our company
.
To the casual obser
ver
, News Corporation
s
entr
y into digital media may seem to be a depar
-
tur
e fr
om our past business models and practices.
But in tr
uth ther
e is fundamental continuity
to our appr
oach. W
e have always sought new
markets and new avenues for distribution. Our
company launched Australia
s first national paper
and America
s first new television network in
40 years. W
e helped pioneer the TV DVD market.
We upended a decade-and-a-half of r
eceived
wisdom about what works in the cable news mar
-
ket. Now we ar
e bringing that same innovative,
entr
epr
eneurial spirit to the Inter
net.
We have always gr
own this company by intelli
-
gently managing a mix of businesses in various
stages of gr
owth and development. Established
businesses pr
oduce modest gr
owth yet sizable 
and r
eliable  cash flows. Businesses in the middle
stage ar
e the primar
y gr
owth drivers of the com
-
pany
, delivering str
ong pr
ofit gr
owth. And our
youngest ef
for
ts ar
e being nur
tur
ed and developed
by the cash generated by our matur
e businesses,
to allow them to find their footing and r
ealize
their potential as the company
s futur
e gr
owth
drivers.
The impor
tance of our so-called traditional
media businesses cannot be overstated however 
both as solid per
for
mers for our company and as
sour
ces of cash flow to build up our next generation
of pr
oper
ties. And on both scor
es, our established
businesses per
for
med spectacularly this past year
.
Recor
d operating income at our Filmed
Enter
tainment segment was driven by four factors.
First, Fox scor
ed some genuine mega-hits at
this year
s box of
fice.
Ice Age: The Meltdown
has,
to date, gr
ossed nearly $650 million worldwide.
X
-Men: The Last Stand
scor
ed the str
ongest Memorial
Day opening in box of
fice histor
y and is now ANNUAL REPORT
7
appr
oaching $450 million in worldwide gr
oss.
The
Devil W
ears Prada
 a gr
eat example of counter
-pr
o-
gramming  has so far r
etur
ned $100 million on a
relatively small investment. Last summer
s
Fantastic
Four
 our newest franchise  did another $330
million at the worldwide box of
fice. And
Walk the
Line
was a critical and commer
cial success, winning
thr
ee Golden Globes and an Oscar and ear
ning
nearly $200 million on a low pr
od
uction cost.
Second, home enter
tainment per
for
mance of
recent films r
emained r
obust.
Robots
,
Hide and Seek
,
Mr
. and Mrs. Smith
,
Star W
ars Episode III: Revenge
of the Sith
, and also some of the films mentioned
above sold well in both the DVD and pay-televi
-
sion markets. In addition, some of our most
successful titles fr
om prior years continue to sell
str
ongly
, notably
Sideways
,
Napoleon Dynamite
,
Alien v
. Pr
edator
and
I, Robot
.
Thir
d, T
wentieth
Centur
y Fox
Television
remains
one of the lar
gest and most successful pr
oducers of
primetime TV in the business. W
e launched thr
ee
new hit shows: NBC
s
My Name is Earl
, the number
one new comedy of the season, and FOX
s
Prison
Break
and CBS
s
The Unit
, this season
s top two new
dramas. And lastly
, home enter
tainment sales for
our TV content continued to gr
ow
, led by str
ong
sales of
The Simpsons
,
Family Guy
and
24
.
Broadcast television needed a big year to top
last year
s recor
d shattering ratings per
for
mance 
and it deliver
ed. For the second year in a r
ow
, the
FOX network won the ratings race among 18-49
year olds. Once again,
American Idol
led the way
, but
once again, FOX showed r
eal depth acr
oss a range
of pr
ogramming.
Idol
shatter
ed pr
ecedent and
expectation, gr
owing its audience by 10 per
cent
in
its fifth year on the air
. This cultural phenomenon
has the legs to last many mor
e years, and the
brand str
ength to launch many mor
e shows on its
popularity
. And it has alr
eady helped launch two
big hits.
24
, now in its fifth year
, posted its best
ever per
for
mance. And
House
, last year
s welcome
surprise, became this year
s genuine hit, winning
its time slot for the season.
In the coming seasons, FOX will r
emain the
premier spor
ts network on U.S. television. W
e
renewed our contracts with Major League Baseball,
NFL football and NASCAR all at attractive rates 
and, in the case
of baseball, at a lower annual cost
than the pr
evious deal. W
e also inked a new deal to
broadcast the college football Bowl Championship
Series. Spor
ting
events ar
e  along with live news
 as close to DVR-pr
oof as pr
ogramming gets.
And the enor
mous ratings of the W
orld Series
and the BCS of
fer unmatched pr
omotional
oppor
tunities for
our network lineup.
Our owned-and-operated stations gr
oup  com
-
prising 35 stations in the U.S.  logged r
ecor
d
market shar
e in the 4th quar
ter on the str
ength of
network ratings and the popularity of local news.
Yet we also moved aggr
essively to keep ahead of the
demands of the market and impr
ove their per
-
for
mance in futur
e years. W
e worked with station
managers to launch new websites and ar
e leveraging
those sites to pr
omote films and network shows
by making clips available for download by their
viewers. W
er
e impr
oving local news coverage by
helping local newsgathering teams intr
oduce
proven techniques pioneer
ed by FOX News. And
weve worked to build up a str
onger lineup of
syndicated pr
ogramming.
In Asia, ST
AR saw operating income and r
ev-
enues rise on the str
ength of adver
tising sales and
subscriber gr
owth. In India, enhanced weekend
programming at ST
AR PLUS and new pr
ogramming
launched at ST
AR ONE boosted ratings. And shor
tly
after the end of the fiscal year
, we invested in the
launch of a new
dir
ect-to-home satellite ser
vice
called T
ata Sky that will capitalize on the rapidly
rising middle class in that countr
y.
One of the most inter
esting developments for
our br
oadcast television business this year was the
announcement that our 10 non-FOX af
filiated
stations will get a new face this fall as we launch
MyNetworkTV
. For
med quickly in the wake of the
WB/UPN mer
ger
, MyNetworkTV will bring the
telenovela, the most popular television for
mat in
the world, to the U.S. for the first time. Pr
oduced
by
Twentieth T
elevision, MyNetworkTVs original
new
shows will r
un six nights per week in 13-week
seasons without r
epeats. Our owned-and-operated
stations for
merly par
t of the UPN network r
each
nearly
a quar
ter of the U.S. market. T
o r
each
the other 75 per
cent, we have af
filiated with an
additional 157 stations.
Our Cable Network Pr
ogramming segment
continued to gr
ow at a tor
rid pace, solidifying its
position as one of the principal gr
owth drivers of
our company
. The success of these businesses is
emblematic of our br
oader strategy
. The elements
are simple to understand, if not always easy to
implement. First, be willing to ignor
e or even take
on conventional wisdom. Second, invest wisely
and early in a new business. Thir
d, be patient as
the new ef
for
t finds its footing. Four
th, enjoy the
growth and pr
ofitability as the business matur
es,
but always be thinking about and building the
next generation of new channel of
ferings.
Our cable businesses ar
e today wher
e we hope
and expect our Inter
net businesses to be in the near
futur
e. The FOX News Channel set another r
ecor
d
for operating income and r
evenue gr
owth, and has
now held the number one position in cable news
for 18 straight quar
ters. High ratings have driven up dver
tising r
evenues and the expiration  beginning
this October  of our early deals with cable and
satellite pr
oviders will shor
tly allow us to r
enew our
car
riage agr
eements at substantially higher rates.
Meanwhile, FX
s mix of original pr
ogramming,
compelling syndicated content and first-rate
theatrical films continue to lead the enter
tainment
cable market among 18-49 year olds. It is now
one of the top five basic cable channels in the
U.S., driven by hits like
Rescue Me
,
The Shield
,
and
Nip/T
uck
.
Spanning 49 of the countr
ys top 50 media
markets, Fox Spor
ts Networks 21 owned-or
-af
filiat
-
ed r
egional spor
ts networks today r
each mor
e than
85 million homes and continue to thrive on the
strength of original local pr
ogramming, established
national shows and str
ong team and community
relationships. W
ith the acquisition of T
urner South
earlier this year
, FSN added
8 million
subscribers
in six U.S. states.
Even within this segment, our thr
eefold strategy
applies. If FX, Fox Spor
ts and FOX News
are the
established businesses, then
SPEED
and
the National
Geographic Channel
are the next
generation 
except, in this case, the next genera
tion is alr
eady
profitable and gr
owing r
obustly
. Our thir
d gener
-
ation of channels  Fox Reality
, FUEL TV
, Fox
Soccer Channel and Fox Spor
ts en
Espaol  ar
e
being built into the gr
owth drivers
of the futur
e.
Turning to our satellite platfor
ms,
SKY
Italia
was pr
ofitable for the first time last year and is
now our company
s fastest gr
owing asset. Its br
oad
appeal and wide channel of
ferings have pushed
subscriber levels to mor
e than 3.8 million with
the lowest chur
n  below 10 per
cent  compar
ed
to DIRECTV and BSkyB. In fact, the subscriber
base
has gr
own ever
y quar
ter since the ser
vice
s
launch, and the Italian market  with high rates
of TV viewership yet the lowest per
centage of pay
TV penetration in Eur
ope  holds huge potential
for futur
e gr
owth. Because a lar
ge shar
e of the
costs ar
e alr
eady fixed, nearly all of this gr
owth
will translate dir
ectly into pr
ofits which we con
-
ser
vatively estimate will r
each into the hundr
eds
of millions in coming years.
At DIRECTV
, revenues gr
ew by 15 per
cent,
driven by higher r
evenues per subscriber and
nearly 850,000 net new subscribers. The company
is intr
oducing new exclusive content that will
fur
ther dif
fer
entiate this ser
vice fr
om its cable
rivals. DIRECTV gr
eatly expanded it
s flagship
NFL
SUNDA
Y TICKET
package and launched the new
music show CD USA. Even mor
e exciting, DIRECTV
signed a deal with Fox to of
fer FOX network and
FX original content on demand  and, in an indus
-
try first, in some cases
befor
e
the shows air
. Set to
launch next year
, this is yet another exciting option
customers can
t get thr
ough conventional cable.
BSkyB, meanwhile, has topped 8 million sub
-
scribers and is aggr
essively branching out into new
ser
vices. The company tur
ned competitors heads
this year by announcing that it will begin of
fering
free br
oadband
to its
customers. And DIRECTV
is
looking to follow suit, first with br
oadband video
ser
vice 2,000-titles str
ong and later with a full
broadband of
fering for all customers.
Our print businesses  the historic hear
t of
this company  continue to deliver value for our
company and shar
eholders. I have ar
gued that it
is much too soon to pr
onounce the death of print
media. Y
et r
eal challenges lie ahead. T
raditional
sour
ces of income like classified adver
tising ar
e
under attack. Mor
e ominously
, although cir
cula
-
tion r
emains str
ong at most of our pr
oper
ties, it
is declining thr
oughout the industr
y and sur
vey
after sur
vey shows that newspapers ar
e less
integral to people
s lives with each passing year
.
Younger consumers pr
efer alter
native means
of getting the news, such as r
eading the online
versions of newspapers for fr
ee.
Yet the hunger for news and infor
mation  for
content  is not fading. It is intensifying. And
as a content pr
ovider
, we ar
e well positioned to
capitalize on that hunger
, pr
ovided we ar
e smar
t
about r
eaching younger consumers in the ways
they pr
efer to be r
eached. Right now our print
businesses have mor
e total r
eaders than they ever
have, thanks to the Inter
net. All of this is to say
that, while in a cer
tain sense our digital and
Inter
net ef
for
ts ar
e specific and par
t of a defined
segment of our company
, in the br
oader sense our
effor
t to r
edefine this company for the digital age
is being and must be applied to ever
y segment.
This year
s results at the Newspaper segment
wer
e mixed. A soft adver
tising market and invest
-
ments in new color printing plants in the U.K.
combined to cause a decr
ease in operating income
overall. Y
et r
evenues incr
eased  par
ticularly at
The Sun
,
The T
imes
and
The Sunday T
imes
 and
our papers still generate enor
mous cash flow for
our company
.
The T
imes
of London launched
an American edition, on sale in the New Y
ork
metr
opolitan ar
ea and soon to expand elsewher
e,
fur
thering its inter
national r
each and influence.
In Australia, operating income was up on str
ong
adver
tising sales and higher cir
culation r
evenues
as a r
esult of our acquisition of QPL.
HarperCollins income r
ose on the success
of several blockbuster titles, including
The Purpose
Driven Life
,
YOU: The Owner
s Manual
and
Freakonomics
. And income r
ose at our Magazines
and Inser
ts segment on incr
eased demand for
in-stor
e marketing pr
oducts ANNUAL REPORT
9
Rupert Mur
doch,
Chairman and Chief Ex
ecutiv
e Offic
er
ANNUAL REPORT
9
Rupert Mur
doch,
Chairman and Chief Ex
ecutiv
e Offic
er
Thr
ee final points: First, thanks to our impr
oved
cash flow and r
obust balance sheet, we wer
e able
to extend and double our stock r
epur
chase pr
o-
gram
from $3 billion to $6 billion. As the first
phase wound down, with mor
e than $2.6 billion
repur
chased, it became clear to the Boar
d that
our stock is still significantly under
valued and
thus a ter
rific investment. The extension of this
program r
epr
esents our fundamental confidence
in the str
ength and
strategic dir
ection of this
company
.
Finally
, I was pr
oud and pleased to pr
eside
over our company
s first major
management
confer
ence in eight years. Imagining the Futur
e,
held in Pebble Beach, Califor
nia, was
a success in ever
y way
. W
e asked for and
received bold new ideas fr
om some of
the world
s most insightful individuals.
We hear
d fr
om political leaders such as
Bill Clinton, T
ony Blair
, Shimon Per
es,
John McCain, Al Gor
e and Newt Gingrich;
technology innovators including Craig
Venter and V
inod Khosla; enter
tainers
like Bono; and business figur
es, including
Lor
d John Br
owne and T
erry Semel. They
all gave us a lot to think about. Mor
e than
250 of our most senior managers left not
only r
efr
eshed, but challenged and excited
about the futur
e of our company
.
This has been a year of tr
emendous
change for News Corporation  one of
our most adventur
ous in r
ecent memor
y.
In the course of 50 years, we have
invented and r
einvented this company to
keep up with  and in our best moments,
stay ahead of  the times. W
e haven
t
always calculated
cor
rectly and we won
t
get ever
ything right
going for
war
d. But
our track r
ecor
d speaks for itself. And our
willingness to think big and boldly has
built a media company like no other in
the world.
To some in the traditional media
business,
these ar
e the most str
essful of
times.
But to us, these ar
e gr
eat times 
the pr
elude
to a new golden age of media.
Technology
is liberating us fr
om old
constraints, lower
ing key costs, easing
access to new customers
and markets
and multiplying the choices we can
offer. For a content company
, what could
be better?
one of Eur
ope
s gr
eat statesmen in this new centu
-
ry. Any company would be lucky to have him.
Our company
, with its global inter
ests and global
reach, can sur
ely benefit fr
om his exper
tise and
counsel. As U.S. Secr
etar
y of Education, Rod Paige
presided over the development and implementa
-
tion of the No Child Left Behind Act, the most
significant
federal education r
efor
m in a genera
-
tion. As
Superintendent of the Houston, T
exas
schools, he put in place tough standar
ds and
insisted on
system-wide accountability
. In both
jobs he had the pleasur
e of seeing his ef
for
ts bear
fruit as test
scor
es r
ose and student achievement
climbed. His skill as a manager will make his
counsel
invaluable, and his deep knowledge of
education will be an asset for a company whose
lifeblood is the literacy of its consumers.
Second, I am pleased to announce two new
additions to the News Corporation Boar
d of
Dir
ectors. Jos Mara Aznar
, as the Pr
esident
of Spain, enacted a bold ar
ray of r
efor
ms that
catapulted the Spanish economy to the for
efr
ont
of
Eur
ope. His courage as a leader in the war on
ter
ror
and the ef
for
t to liberate Iraq made him y
, with its global inter
ests and global
reach, can sur
ely benefit fr
om his exper
tise and
counsel. As U.S. Secr
etar
y of Education, Rod Paige
presided over the development and implementa
-
tion of the No Child Left Behind Act, the most
significant
federal education r
efor
m in a genera
-
tion. As
Superintendent of the Houston, T
exas
schools, he put in place tough standar
ds and
insisted on
system-wide accountability
. In both
jobs he had the pleasur
e of seeing his ef
for
ts bear
fruit as test
scor
es r
ose and student achievement
climbed. His skill as a manager will make his
counsel
invaluable, and his deep knowledge of
education will be an asset for a company whose
lifeblood is the literacy of its consumers Finally
, I was pr
oud and pleased to pr
eside
over our company
s first major
management
confer
ence in eight years. Imagining the Future held in Pebble Beach, Califor
nia, was
a success in ever
y way
. W
e asked for and
received bold new ideas fr
om some of
the world
s most insightful individuals.
We hear
d fr
om political leaders such as
Bill Clinton, T
ony Blair
, Shimon Per
es,
John McCain, Al Gor
e and Newt Gingrich;
technology innovators including Craig
Venter and V
inod Khosla; enter
tainers
like Bono; and business figur
es, including
Lor
d John Br
owne and T
erry Semel. They
all gave us a lot to think about. Mor
e than
250 of our most senior managers left not
only r
efr
eshed, but challenged and excited
about the futur
e of our company
.
This has been a year of tr
emendous
change for News Corporation  one of
our most adventur
ous in r
ecent memor
y.
In the course of 50 years, we have
invented and r
einvented this company to
keep up with  and in our best moments,
stay ahead of  the times. W
e haven
t
always calculated
cor
rectly and we won
t
get ever
ything right
going for
war
d. But
our track r
ecor
d speaks for itself. And our
willingness to think big and boldly has
built a media company like no other in
the world.
To some in the traditional media
business,
these ar
e the most str
essful of
times.
But to us, these ar
e gr
eat times 
the pr
elude
to a new golden age of media.
Technology
is liberating us fr
om old
constraints, lower
ing key costs, easing
access to new customers
and markets
and multiplying the choices we can
offer. For a content company
, what could
be better? 
