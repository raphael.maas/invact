DEAR
CUSTOMERS, EMPLOYEES
AND SHAREHOLDERS,

For 114 years, our focus has been on our customers.
We have been most successful when we view our business
through their eyes. In todays rapidly changing retail
landscape, this approach has never been more important,
so our strategy remains squarely focused on serving
customers on their terms. Knowing customers increasingly
desire an experience thats both personalized and convenient,
we continue to make investments that further integrate our
store and online experience to enable our customers to shop
seamlessly any way they choose.

We are uniquely positioned to serve customers
through full-price, o-price, stores and online.
Each oers a way for customers to shop with
us, and collectively they represent a significant
growth opportunity. The real success lies in
how our business works together to better
serve customers and help us attract and retain
customers, now and in the future. We see a
meaningful opportunity in this synergy through
a focus on service, product and capabilities.
FULLPRICE: NORDSTROM, NORDSTROM.COM
AND TRUNK CLUB
2014 marked an important milestone in
our companys history as we opened our
first international store in Calgary, Alberta,
Canada. We were humbled as more than
2,000 customers welcomed us when we
opened our doors. In the first six months,
we outperformed our expectations and are
extremely pleased with the eorts of our team.
Their successful planning, preparation and
navigation of many complex issues for several
years helped us put our best foot forward.
Its only one market though. We still have a
lot to learn about serving Canadians and will
continue learning from each community as we
expand further. On March 6, 2015, we opened
a store in Ottawa, Ontario, and we will open
another in Vancouver, British Columbia, on
September 18. Both stores are in terrific retail
locations, and we look forward to serving more
customers in Canada in the coming years.
In 2014, we also opened new stores in the
United States at The Woodlands Mall in
The Woodlands, Texas, and St. Johns Town
Center in Jacksonville, Florida. Both these
stores and those in Canada opened with our
new store design, reflecting an energized
customer experience that will allow us to
evolve with our customers in the years to come.
In January 2015, the Retail Design Institute
recognized Nordstrom The Woodlands
with First Place for a New or Remodeled
Department Store and Innovation in Store
Planning. We appreciate their recognition, but
the true measure of success will come from our
customers experiences in our new stores.
In addition to our two new stores in Canada
this year, well also open stores in Puerto Rico,
Milwaukee and a second store in Minneapolis,
which has long been a terrific market for us.
We are also mindful of serving customers
better through product, speed and
convenience. Expanded merchandise selection
and growing customer adoption of mobile
shopping are two factors contributing to the
continued growth of Nordstrom.com. First,
weve expanded Nordstrom.com selection by
three times from three years ago, so customers
have greater choice of their favorite brands.
Second, a great mobile experience is a vital
link to many customers, and we are working
hard behind the scenes to improve this
experience. We have enhanced our mobile
app with localized inventory and personalized
homepage features, and more features are
coming this year.
We are also opening a fulfillment center
in Elizabethtown, Pennsylvania, in 2015
that, when combined with our fulfillment
center in Cedar Rapids, Iowa, and our
Nordstromrack.com/HauteLook fulfillment
center in San Bernardino, California,
will greatly increase our capabilities.
These fulfillment centers will allow us
to be within two-day ground delivery of
approximately half the population of the
United States, which will help improve
delivery times for customers and help us
meet their rising expectations.
Finally, in 2014, we acquired Trunk Club,
a high-growth personalized mens clothing
business based on a service model that is highly
complementary to our own. We believe Trunk
Club is a natural extension of our business,
and together we will continue to evolve and
bring together the online and o?ine worlds
to deliver a great shopping experience.
OFFPRICE: NORDSTROM RACK,
NORDSTROMRACK.COM AND HAUTELOOK
We opened a record 27 new Nordstrom Rack
stores, ending 2014 with 167 stores and on
track to meet our long-term growth plans
of 300 stores by 2020. Customers continue
to respond favorably to the treasure-hunt
experience that defines Nordstrom Rack
stores. As we expand in many markets for the
first time, we hope to continue delivering a
great experience, as this business represents
a terrific opportunity for us to attract new
customers. Last year, Nordstrom Rack
was our biggest source of new customers,
attracting nearly 4 million. Also, a year ago,
we began accepting returns of HauteLook
and Nordstromrack.com merchandise at any
Nordstrom Rack store. This drove nearly
1 million trips to Nordstrom Rack stores in
2014. The Nordstrom Rack customer also
tends to be younger than our full-line customer,
and there is a meaningful opportunity for these
customers to begin shopping our full-price
channels as well. We plan to open 27 more
Nordstrom Racks in 2015 across the U.S.
In addition to our new stores, we improved
our online/o-price capabilities with the
launch of Nordstromrack.com. Combined
with HauteLook, the integrated ecommerce
site oers a consistent merchandise selection
as well as flash sales in a single web or mobile
experience, providing customers a wide
range of merchandise with one easy-to-use,
shared checkout. Since the launch last spring,
weve more than doubled the selection at
Nordstromrack.com. We will continue to work
on ways to further integrate our business to
improve our customer experience.
INCREASING RELEVANCE
We know ultimately customers come to
Nordstrom for great merchandise. They
continue to respond to fresh, relevant
brands. Last year, we were the exclusive
retail partner for the global launch of
Sarah Jessica Parkers SJP line of shoes and
launched Charlotte Tilbury in Beauty. We
increased the number of full-line stores with
Topshop to 53 and launched Kate Moss for
Topshop, which helped us rapidly grow the
number of Topshop customers, including a
younger customer who in many cases is new
to Nordstrom. By the end of 2015, we plan to
have Topshop in more than 80 stores.
This March, we were excited to begin carrying
Madewell, representing a new partnership
with J.Crew. Our initial launch was on
Nordstrom.com and in 15 of our stores in our
t.b.d. department. This is a terrific example
of our continued focus to bring great fashion
brands to customers at accessible price points.
Finally, Nordstrom Rewards has been a
successful program enabling us to deepen
our engagement with customers. In 2014,
we added more than 1 million new Rewards
accounts, a 15% increase from the previous
year. We want to give customers more choices
with our loyalty program, and our goal is to
provide an integrated multi-tender program in
all stores and online later this year. We know
our Rewards members are many of our most
loyal and best customers. So growing these
relationships by oering programs that
appeal to more customers will be beneficial
in the long term.
CONCLUSION
Our strategy is based on the customer and will
remain so. Customers expectations of speed,
convenience, personalization and mobile are
increasing. As we continue on our journey, we
recognize its imperative for us to invest for the
future and find ways to make our stores more
convenient and our online experience richer.
We believe we are well positioned to deliver a
great experience for our customersno matter
how they choose to shop with Nordstrom.

Blake W. Nordstrom
President, Nordstrom, Inc.

Peter E. Nordstrom
President of Merchandising, Nordstrom, Inc.

Erik B. Nordstrom
President of Nordstrom.com, Nordstrom, Inc.


A NOTE FROM OUR
CHAIRMAN

Our top priority at Nordstrom is to improve
our customers experience, continuously.
This priority challenges us each day and is the
basis for our long-term strategy to provide
all Nordstrom customers with a best-in-class
experience centered on service, product and
capabilities, whenever and wherever they want.
This commitment to Nordstrom customers is,
in itself, the foundation of the deep and shared
commitment of the Board of Directors and
management to our shareholdersto enhance
shareholder value over the long run through
capital allocation discipline and to hold
ourselves accountable for financial results.
About a decade ago, we began investing
strategically in the companys multi-channel
capabilities across stores and online to meet
the changing expectations of our customers.
Investing early in technology, merchandising
and stores, as well as integrating the in-store and
online experience, helped position the company
to achieve exceptional growth and superior
returns. For the last several years, we further
accelerated our level of investment to fuel
growth across all our businessesfull-price,
o-price, stores and onlineto help Nordstrom
deliver strong results this year and support
profitable growth for years to come. We expect
these investments to drive long-term
shareholder value.
As the Board of Directors looks back on the
many accomplishments of 2014 and ahead to the
future, we support our executive management
team led by Blake, Pete and Erik Nordstrom.
We view 2014 to be a watershed moment in our
history, with the successful entry into Canada,
the expansion of the Rack business through store
growth, the launch of Nordstromrack.com and
the acquisition of Trunk Club. These milestones
are the outcome of our strategy, which is squarely
focused on serving customers on their terms and
delivering the Nordstrom experience they expect
from us. More importantly, our forward-thinking
strategy puts us on track to achieve our goal of
top-quartile shareholder return, driven by high
single-digit sales growth and mid-teens return
on invested capital. We believe this will lead us to
be a $20 billion business by 2020, with superior
returns, creating significant shareholder value.

The hard work and performance of the team have
not gone unnoticed. Fortune magazine recently
published their annual ranking of the Worlds
Most Admired Companies, listing Nordstrom at
#14. This is the seventh consecutive year weve
appeared in the Top 50. Additionally, Nordstrom
was ranked number one among all companies
for quality of product/services. Within our peer
group of general merchandisers, we ranked
number one overall as well as for innovation,
people management, use of corporate assets,
quality of management, long-term investment
value and quality of products/services. These
accolades reflect the vision, high achievement
and values we believe the company stands for,
and the dedication and hard work of more than
67,000 Nordstrom employees.
This past February, we were fortunate to add
Shellye Archambeau to the Board of Directors.
As the chief executive o?cer of MetricStream,
Inc., we believe Shellye enhances an already
strong Board that is deeply committed to the
highest standards of ethics, accountability
and governance.
As we look to the future, we know that change
will be a constant and that the Nordstrom strategy
must evolve with customers expectations,
as must the means by which we enhance
shareholder value. However, most importantly,
the core principles upon which Nordstrom was
built will not change. Your Board of Directors
and your management team are committed
to maintaining the companys unmatched
competitive spirit and unyielding commitment
to serve and value our customers, employees and
shareholders. Be assured that Nordstrom and
its wonderful employees are well positioned to
deliver the best customer experience possible
in ever-innovative ways, creating value for our
shareholders in the years to come.
On behalf of the entire Board of Directors, thank
you for your continued support of Nordstrom.

Enrique Hernandez, Jr.
Chairman


