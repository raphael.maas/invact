Our 2007 results clearly demonstrate the advantage of our
geographic diversification, the spread of customers we
serve, the breadth of our products and brands as well as the
essential nature of the productivity solutions we provide to
professional users. In 2007, we continued to strengthen
the foundations of our business. We expanded in our core
business and we extended into new opportunities. We served
more customers in more categories across more geography
over 130 countriesthan ever before.
Customer service improved as we re-shaped our supply chain,
invigorated our manufacturing processes and delivered strong
improvements in complete and on-time order fill rates. We
renewed our commitment to delivering customer-relevant
innovations and more rapid development of those solutions.
Were particularly proud of the speed and tenacity with which
our associates created and adopted the changes necessary
to serve ever-evolving customer needs. Belief in these
fundamentals is creating a strong foundation on which we
can achieve future growth.
While there is still much work and opportunity ahead,
were encouraged by the progress reflected in our
performance. Net sales grew 15.7% year over year, driven
by strong sales in every key segment. Operating earnings
increased substantially, almost doubling, from $162.8
million to $324.8 million. In 2007, each of our associates
accounted for 11.3% more sales than the prior year.
Snap-on became a public company in 1939. Since that
time69 years agoSnap-on has paid uninterrupted
and unreduced quarterly dividends to its shareholders.
Snap-on is one of the very few public companies who can
make such a claim. In 2007, the Snap-on Board increased
the quarterly dividend 11.1% to $0.30.
Despite the many improvements made in 2007, were far
from satisfied. We have a mindset that brings a new
opportunity for improvement every day. Were continuing to
deploy a capability we call Rapid Continuous Improvement
(RCI), a structured set of tools and processes that enable us
to eliminate waste while making improvements in safety,
quality, delivery and cost. In 2007, we broadened our use of
RCI beyond manufacturing, extending it to administrative
and other functions. RCI was part of the growing business
suite of business processes we successfully deployed in
2007 to integrate our 2006 acquisition of ProQuest.
Our manufacturing system plays an important role in
delivering world-class products to a broad range of
customers. Their demand for our products has grown.
Weve continued to replace old technology with the
latest systems and equipment. Weve also expanded our
manufacturing footprint into emerging markets across the
globe. We intend to focus on further efforts to expand our
flexibility and capacity.
Our commitment to safety is unwavering. We improved
our safety record 53% in 2007; this comes on top of a 63%
improvement in 2006. Our overall safety record is now
significantly better than the U.S. national average for
manufacturing. We recognize the importance of keeping
the Snap-on family safe, so we are continuing our emphasis
on safety as we move forward.
Our legendary Snap-on brand grew even stronger in
2007. It stands for people across the world that deliver
value and are passionate about customer satisfaction.
It also represents innovative products that mark the user
as a professional and make work easier. In 2007, we
introduced more new Snap-on products and increased
focus on brand discipline. Customers rewarded us with
their business and their preference for Snap-on.
Whether measured by purchases from automotive
technicians, partnerships with the largest aerospace
and aviation companies in the world, the confidence
of OEM manufacturers, or the purchases of industrial
market leaders, the Snap-on brand is stronger than ever.
In selected world markets, our Bahco brand grew
significantly, expanding its position with a strong
reputation for ergonomic design and user relevance.
The strength of that broad brand gives our corporation
significant power to expand throughout the world,
especially in the emerging markets of Asia-Pacific and
Eastern Europe.
In 2007, we also built on our long-established Blue Point
brand. We sharpened the positioning of Blue Point and
expanded its product assortment, serving additional
purchase occasions for existing customers, and used it
to reach new customers.
Since 2006, weve been overhauling our supply chain
to create a market driven replenishment system. We
redesigned the system, positioning high-volume items
closer to the customer. We moved product assignments
between plants to improve our efficiency, responsiveness
and cost. In 2007, we continued these efforts, delivering
positive results and expanding our focus to improve
integration of our outside suppliers into this system.
Further, we addressed a broader range of customers,
improving order fill rates and customer service for a
breadth of global customers.
Historically, Snap-on grew through serial acquisition with
minimal integration. The result was a series of cultures
that did not work effectively together. Today, a growing
common culture fosters collaboration across the
enterprise. Snap-on associates worldwide created our
ongoing missionto deliver the most valued productivity
solutions in the world. This is a lofty objective, but one we
believe is important to keep our sights on a future state
vision, while driving to deliver short-term results.
In the Snap-on Tools Group, we continue to focus on
enhancing our franchise proposition and improving the
performance of our franchisees. Their success is our success.
In 2006, we transformed our field support system for
franchisees. In 2007, the approach matured. We increased
franchisee training, business planning sessions, technology
support, sales programs and related activities. These efforts
delivered marked improvements in franchisee satisfaction
and performance. The net is a win for our customers, the
company, our franchisees and our shareholders.
Net sales in the Snap-on Tools Group were up 8.1% in
2007. Operating earnings were up 65.5% year over year,
excluding the impact of a 2006 franchisee litigation
settlement charge.
In 2006, we re-launched our Snap-on warehouse
distribution program designed to serve the specialized
product needs of our franchisees and customers. In 2007,
the program accelerated and delivered strong results
with continual input from our franchisees on product
assortment and customer service.
Again this year, the National Franchise Advisory Council
was instrumental in collaborating to solve the most difficult
challenges facing our franchisees and our company. We
thank them for their dedication to their businesses and the
constituents they serve.
Our Commercial and Industrial Group again made
significant progress in 2007. Net sales were up 13.3% and
operating earnings were up 23.8%. Growth came across a
broad range of markets and customers. Natural resources
and energy markets, along with government, aerospace
and aviation were particularly strong. Our continued
focus on delivering integrated solutions, including tool
control systems and mobile tool cribs, were important in
capitalizing on these growth opportunities.
In undercar equipment, we focused on improving
manufacturing speed, flexibility and cost through RCI.
At the same time, we grew our business worldwide
through technology development, product innovation
and sales efforts targeted at key customers. In power
tools, we launched some of our most successful products
ever, driven by innovation, focused marketing and
integrated sales efforts. In education, we made significant
progress in creating career-long customers. For instance,
we partnered with Gateway Technical Institute, Kenosha, WI,
in opening the Horizon Center. The new facility is a
state-of-the-art technical school featuring Snap-on
Certification in diagnostics for instructors nationwide
and for a growing base of future automotive technicians.
In Europe, we achieved an ambitious manufacturing
footprint transformation ahead of schedule. We look
forward to increased capacity with the new footprint that
will help meet rapidly growing demand. To keep increasing
that demand, our product development team continued
to create new innovations to fuel the growth of our Bahco
brand with a broad range of customers.
Snap-on continued to invest into the emerging markets
of Asia-Pacific and Eastern Europe and our capabilities to
serve those regions increased again in 2007. We completed
our second plant in Kunshan, China, on schedule. Its
already helping to fuel our ongoing growth. We also
continue to expand our sales capabilities, adding a
substantial number of distributors and sales offices to our
on-the-ground network throughout the emerging regions.
In our Diagnostics and Information Group, 2007 was
another exciting and productive year. Sales increased
28.5% to over $650 million and operating earnings were
up 66.4%.
The integration of the Snap-on Business Solutions
business, acquired in November 2006, helped the
company to successfully build closer relationships
with key OEM customers and strengthen Snap-ons
position as a provider of productivity solutions. New
product and program offerings, coupled with a renewed
focus and dedication to customer delivery, were rewarded
with solid sales gains. Integrating our RCI capabilities
and other Snap-on business processes into the Business
Solutions organization drove operating cost leverage and
increased its effectiveness.
The Snap-on diagnostics equipment and software
business, as well as our Mitchell1 information and shop
management business, grew through the development and
launch of new hardware and software products targeted at
a broader and growing customer base. In addition, we had
significant success in attracting new key national account
customers and driving growth in customer retention
management system sales. The introduction of the new
Solus PRO scan tool resulted in solid sales and continued
expansion of diagnostic software sales. Beyond sales
expansion, focus on RCI initiatives across Diagnostics
and Information achieved strong cost reduction results
and continued improvement in inventory management.
In the years ahead, these initiatives will continue to drive
performance throughout the corporation.
After five years of dedicated service, Lars Nyberg resigned
from our Board to concentrate on new responsibilities as
CEO of TeliaSonera. We welcomed Jim Holden and his
extensive, 27-year automotive background to the Board
in 2007. We thank all of our dedicated Board members
for their continued and invaluable support and guidance.
Finally, we thank our associates, our franchisees, our
business partners and our shareholders for their support
and dedication.

Jack D. Michaels
Chairman of the Board

Nicholas T. Pinchuk
President & Chief Executive Officer