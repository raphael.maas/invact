Im delighted to be the bearer of good newsweve had
an outstanding year!
In an uncertain economic environment, our revenues
increased steadily throughout the year. How did we do it?
Many factors contributed to our success. But the single
overriding reason is that our products are fabulous.
Quite simply, Autodesk makes terrific products that contribute tangible
value to our customers with significant productivity increases and quick
return on investment. Creatingand continuously improvinggreat
products is the foundation that supports our success in every area. As we
discussed in last years report, fiscal 2003 marked a concentrated focus on
product development. Fiscal 2004 witnessed the payoff of that effort.
Our customers voices are at the heart of our product development. This
year we incorporated their feedback as never before, seeking their input in
focus groups, beta programs, user group meetings and face-to-face visits
at customer sites. And we expanded these events to locations around the
world, gathering feedback that reflects the interests of our European
and Asian customers.
Our planned release cycle for AutoCAD-based productsformerly 18 to
24 monthsis now 12 months. The shortened cycle provides customers
with more frequent gains in functionality and productivity, while reducing
the revenue volatility that we have experienced in the past. By releasing
new products and retiring others each year, were decreasing the impact
of product cycles on our financial performance. Also contributing to the
predictability of revenues is the continued momentum of our Subscription
program. Customers are increasingly embracing the added value of this
offering, which enhances their productivity and return on investment.
Our financial performance demonstrates that our strategy is working.
Our customers like what they see.
Our financial goals for fiscal 2004
were to drive revenue growth,
increase revenue predictability and
improve profitability. In a year
overshadowed by an economy in
transition, we met our goals, and
Autodesk flourished. Our strong performance
was driven by continued
strength in revenue from new users,
a significant increase in upgrades
and the ongoing success of the
companys Subscription program.
In summary, net revenue for fiscal 2004 was
$952 million, an increase of 15 percent compared
with $825 million in fiscal 2003. Net
income for fiscal 2004 was $120 million, or
$1.04 per diluted share, including a
$3 million restructuring charge and the
positive effects of tax settlements totaling
$27 million. Net income in fiscal 2003 was
$32 million, or $0.28 per diluted share,
including $26 million in restructuring
charges and the positive effect of tax benefits
of $4 million. Our operating margin
increased 8 percentage points over fiscal
2003 to 11 percent.
Our revenue growth was driven by strong
new releases of all major products during
the year, putting us in a good position when
the worldwide economy began to recover.
As our customers saw new strength in their
businesses, they began spending again, first
upgrading existing licenses and then buying
new ones. In fact, our upgrade revenue
grew 88 percent over fiscal 2003.
With the economic recovery as a backdrop,
we saw growth in all geographies and all
divisions in fiscal 2004. The Americas experienced
strong revenue growth, increasing
9 percent over fiscal 2003 to $410 million.
Europe, the Middle East and Africa (EMEA)
had an outstanding year, with revenue
growing 28 percent year over year, to
$337 million. We saw improvement in most
major economies in EMEA, and Germany
appeared to have halted its economic
decline. Asia Pacific revenues followed suit,
increasing 9 percent year over year to
$205 million, despite a midyear slowdown
due to SARS. Were happy to report particular
strength in Japan, where the economy
has been picking up since the middle of the
year. Foreign exchange rates also provided
substantial benefit during the year.
Divisional Review
Our business is organized into two
segments: the Design Solutions
Group, encompassing Autodesks
core competency of design technology
applications, and Discreet, our
media and entertainment group.
DESIGN SOLUTIONS GROUP
Revenues for our Design Solutions Group
(DSG)comprising the Platform Technology
Division, Manufacturing Solutions Division,
Infrastructure Solutions Division and
Building Solutions Divisionreached
$812 million this year, an increase of 17 percent
over fiscal 2003. DSG revenues reached
85 percent of total company revenues, on
the strength of customers upgrading to
the newest products. Revenues from our
Subscription program reached 14 percent
of DSG revenues for the year, marking
substantial progress toward our goal of
subscriptions amounting to 25 to 30 percent
of total DSG revenues.
The Platform Technology Division (PTD)
showed solid growth, with revenues of
$477 million, an increase of 20 percent compared
with last year. AutoCAD revenues
increased 26 percent over the previous year,
with strong growth in new licenses and
upgrades.
The Manufacturing Solutions Division (MSD)
posted revenues of $140 million, an increase
of 17 percent compared with last year.
Once again, Autodesk Inventor Series was
the best-selling 3D manufacturing design
software in the marketplace.
The Infrastructure Solutions Division (ISD)
also turned in an excellent performance.
Revenues increased 12 percent over the
previous year to $115 million. ISD saw particular
strength in the civil engineering
market, offering the only civil engineering
solutions with tools for every task of the
project lifecycle.
The Building Solutions Division (BSD)
finished the year strongly with a 9 percent
increase in revenues to $80 million, despite
continued slow spending in commercial
construction worldwide.
DISCREET
Discreet, our entertainment group, made
an impressive comeback with a 9 percent
revenue increase over fiscal 2003 to
$140 million. Discreets powerful products,
coupled with the return of spending in the
critical advertising market, drove strong
performance.
IMPROVING PROFITABILITY
This year we took a hard look at our cost
structure and saw room for significant
improvement. We benchmarked our cost
structure against that of other companies in
our industry, using the results to implement
productivity initiatives and set short- and
long-term productivity targets throughout
the company. We are already reaping the
benefits of these actionsour operating
margin has increased 8 percentage points
over fiscal 2003 to 11 percentand we are
committed to making substantial improvements
in this area in the coming year.
STRONG BALANCE SHEET
Autodesk is well positioned financially. Our
strong sales generated significant cash flow
this year. In fact, in 2004 we generated
$220 million in operating cash flow. We used
$13 million for our regular quarterly dividend,
$178 million for our share repurchase
program and $5 million related to mergers
and acquisitions in growth initiatives. Yet
with all those programs, we still ended the
year with a strong balance sheet: we have
$530 million in cash and marketable securities
and no debt.
Great products. Increased predictability
of revenues. Dedication
to profitability. Every product
enhancement, every new release,
every decision we made reflects our
strategic commitment to better
products, more predictable revenues
and increased profitability.
Our product cycles are now the
shortest theyve ever been, with
new versions of all our products
released each fiscal year. The
enhanced release cycles worked in
tandem with our Subscription
model to provide an increasingly
predictable flow of revenues.
Notable highlights of this landmark year
include the following:
 Launch of the strongest suite of products
in our history, including the AutoCAD 2004
family of products, Autodesk Inventor 8,
Autodesk Inventor Professional 8, Autodesk
Revit 6, 3ds max 6 and lustre, our new system
for digital grading and color correction
 Great strides for 3D in the Manufacturing
Solutions Division, and even greater potential
ahead, with less than 20 percent of our
customers using 3D at this point
 Significant advances toward our goal of
offering a full suite of lifecycle management
solutions in each of our key design
industries
 Launch of our Subscription program in the
Asia Pacific market, completing our last
phase of worldwide availability (subscription
constitutes 12 percent of our total
revenues for fiscal 2004)
 Opening of the China Development Center
in Shanghai, a major strategic initiative that
allows us to develop and launch products
within the local context of this vast market
 Terrific rebound in the Discreet business,
with a strong fourth-quarter showing
 Stepped-up customer feedback programs,
drawing on an unprecedented number of
customers to develop next-generation
product releases
We continue making the customer the center
of all our decision making. Furthering
initiatives begun last year, weve vertically
aligned our sales and professional services
organizations with our customers industries
and with our own product divisions. Our
customers needs are the driving force
behind the lifecycle management vision that
is a vital component of our success strategy.

Discreet
Discreet serves customers who create highvalue,
design-intensive content such as
effects-rich feature films, premier programming,
television commercials and interactive
gaming. We also help them simplify their
processes by providing integrated workflow
solutions that go beyond the creation of
digital media assets to include storage,
management and distribution.
Our customers recognize the importance
of digital workflow in driving their own
efficiency and profitability, and Discreet provides
the award-winning technology to do
that. Discreets inferno, flame, fire and lustre
systems are the industrys leading technologies,
enabling visual artists to create content
once and use it anywhere, from start to
finish. 3ds maxDiscreets industry-leading
modeling, animation and rendering software
is used by the worlds top 3D artists
and designers to create eye-catching visual
effects, cutting-edge games and unique
design visualizations.

Design Solutions Group
Our DSG customers can never get enough
performance. Performance improvements
deliver increased productivity and profitability
to users, and give them a competitive
advantage. In fiscal 2004, we shipped new
releases for our entire family of AutoCAD
productsone of our most successful
releases everas well as all other major
products, including Autodesk Inventor Series
and Autodesk AutoCAD Revit Series. We also
released several new strategic products during
the year, such as Autodesk Inventor
Professional and AutoCAD Electrical and the
important new Autodesk Vault functionality.
And customers responded enthusiastically
to them all.
Platform Technology Division: In our continuing
commitment to providing even better
software, we listened to our customers feedback
and suggestions to create product
releases with improved functionality, such as
smaller file sizes, quicker file opening and
closing, faster design creation and easier file
sharing. And customer feedback has already
provided the road map for upcoming
releases over the next three to four years.
Manufacturing Solutions Division: In fiscal
2004, we strengthened our position as
the industry leader in this critical market.
Autodesk Inventor Series was the bestselling
3D manufacturing software for the
third straight year. To extend its power, we
released Autodesk Inventor Professional,
which provides additional functionality.
Less than 20 percent of our manufacturing
customers currently use 3D, so there is a significant
growth opportunity for this division.
Infrastructure Solutions Division: ISD saw
growth in all geographies and all markets
transportation, civil engineering and infrastructure
managementand record growth
in all sectors of government as budgets
were increasingly redeployed toward public
safety and first-responder projects. As promised
last year, fiscal 2004 saw new releases of
virtually all ISD products, including Autodesk
Map, Autodesk Land Desktop, Autodesk
Civil Design, Autodesk Survey, Autodesk
Raster Design and the new and strategically
important Autodesk Civil 3D.
Building Solutions Division: Todays fast-track
construction projects demand speed and
depend on the contributions of people all
around the world. In tandem with the
launch of AutoCAD 2004, BSD released a
strong new version of Autodesk
Architectural Desktop, providing full support
for importing, exporting and linking files.
Our strategy of moving building customers
to the building information model was
enhanced in 2004 with the introduction of
Autodesk AutoCAD Revit Series, which, like
Autodesk Inventor Series, enables customers
to migrate to 3D at their own pace.
In fiscal 2004, Autodesk products
continued to stand at the crossroads
of technology and creativitybut
we werent standing still. Any idea
that our customers can imagine can
be designed, built or manufactured
using Autodesk products. And to
take Autodesk products, and
Autodesk, to the next level, we set
the following key growth initiatives
for fiscal 2004 and beyond:
 Stronger Design Tools: As we improve the
performance and functionality of current
products with each new release, we are
also developing products that address
industry-specific needs.
 Expanded Lifecycle Management: The
effective management and tracking of
complex project data throughout the project
cycle is an area of strategic importance
to customers in all our markets and a longterm
initiative for us. Were investing in this
initiative across all divisions. During fiscal
2004, we introduced Autodesk Vault, a data
management solution that enables fast,
accurate sharing of design data among
engineering and manufacturing teams.
 Greater Geographic Coverage: Geographically
based initiatives are a key element
of our growth strategy. Our new China
Development Center is the most significant
example, providing a deeper understanding
of local markets while developing
products to specifically address the
Chinese market.
In my letter last year, I said I believed that Autodesk had never been so
well positioned for future growth. That was true then, and it continues to
hold true as we embark on fiscal year 2005. Our product line has never
been better. Last year, we anticipated the release of powerful new versions
of all our major products. We delivered on that expectation, and in
fiscal 2005 we have significant new releases of all major products coming
out at the same time as last year, or earlier. In fact, as you read this report,
we have already started shipping the AutoCAD 2005 family of products.
We also plan to introduce several significant
new products, including the important new
release of Autodesk Civil 3D for our infrastructure
customers and new Linux-based
releases for our media customers. We have
just begun our restructuring efforts and
remain firmly committed to improving
profitability. Were on track to perform
and execute. In short, I have never been
more enthusiastic as we head into a new
fiscal year. My optimism is based on
several key factors:
 Our strategy is working: Autodesk products
are the worldwide design standard. The
move to 3D represents enormous revenue
growth opportunity with significantly
higher average selling prices.
 Our product portfolio has never been
stronger. Our planned annual product
release cycle focuses on thematic releases
that provide significant improvements in
functionality and integration.
 Our Subscription program continues to be
successful and is an integral component in
our strategy for improving the predictability
of our business model.
 Our move into lifecycle management is
creating new and exciting opportunities
for us. Well continue improving and delivering
design creation, management and
collaboration solutions that offer our
customers significant improvements to
their critical business processes.
The worldwide economy is showing
improvement, with the United States and
Japan looking particularly strong. We have a
great growth strategy thats bearing fruit
and another fantastic product year ahead of
us. Were moving to a stronger and more
profitable business model. We have good
reason for optimism as we head into a new
fiscal year.
Fiscal 2004 was a great year. Thank you.
Thank you to our investors, employees,
partners and customers for your contributions
and support. We couldnt have done it
without you. And I hope youll join us as we
continue to seek new opportunities to
deliver the innovation and leadership our
customers count on.
CAROL BARTZ
Chairman, Chief Executive Officer and President
