Letter to Shareholders

2008 was a year beset by major challenges. While the insurance/reinsurance industry experienced one of its costliest
years on record for natural catastrophes, the deepening global
financial market crisis resulted in an even greater destruction
of value, causing some companies to seek out costly financing
to shore up weakened balance sheets.
 In the face of these events, Everest�s core operations performed quite well, generating an underwriting combined ratio
of 95.6% and an operating return on average equity of 10.5%
for the year. Our liquidity�a crucial commodity of late�
remains robust, with the combination of our strong operating
cash flows, maturing investments, liquid assets, and available
credit line capacity being well in excess of our business needs.
More importantly though, our balance sheet remains strong
with $13.7 billion in invested assets�largely comprised of
high quality fixed income securities�and approximately $5
billion in shareholders� equity.
 This speaks volumes to the fundamental strength of
Everest�s franchise, best represented by the quality of our people, the relationships we have developed, and our overarching
strategy that focuses our activities on bottom line results and
the creation of long-term value for our shareholders.
With this strategy in mind, we had pulled back our writings
in 2008 as market conditions in many lines continued to deteriorate and returns diminished. The U.S. casualty markets, in
particular, had become increasingly difficult, as pricing and
terms and conditions softened. The international markets,
though, remained fairly stable providing us with growth and diversification opportunities. As a result, we have grown our international business to a larger proportion of our underwriting
portfolio. This business has been very profitable and therefore
is contributing a growing portion of our overall return. We will
discuss each of our business segments later in this letter.
 As we look out to 2009, we see a changing landscape.
We were very pleased with our January 1st renewals, particularly in the shorter tail lines, and anticipate continued firming
for both the insurance and reinsurance markets as the year
progresses. The confluence of events in 2008 has resulted in
diminished capacity for many insurers and, therefore, potentially greater demand for reinsurance support. The significant
catastrophe losses seen in the year heightened risk awareness
(again) signaling the need for greater returns in catastrophe
exposed regions.
 Unlike years past when new (and sometimes na�ve)
capacity rushed into the market to grab share and forestall
potential improvement, we believe that 2009 will be different,
with the professional reinsurers reaping the full benefit of these
positive changes. Flight to quality has become an increasing
reality that will inure to the benefit of the better run and better
rated companies. Everest has been and should continue to be
a major beneficiary of this trend due to its strong ratings from
each of the rating agencies.

Risk Management and Today
The events of 2008 highlight the need for a strong risk management framework. While the management of risk is fundamental to our business, to be successful we must understand
and manage the interplay of various, seemingly uncorrelated,
risks across the organization.
 At Everest, we have developed a risk management system
that encompasses clearly defined risk tolerance levels, management guidelines, and monitoring tools. While models play
an important part in our risk management processes, equally
important is the experience of our team and the judgment they
bring in assessing risk accumulations and correlations. We
have learned that while models are good tools, they are only as
good as the quality of the underlying data and assumptions
that go into them. An over-reliance on models, without the
exercise of sound informed judgment, has proven to be a
flawed strategy leading to disastrous consequences.
 Our risk management approach, which is built on more
than thirty years of experience, focuses on diversification of
risk with our �willing to lose� tolerance levels determined jointly
by our senior management team and our Board of Directors,
taking into consideration market conditions, return expectations and the key objectives of the organization. Our flat hierarchy and open communications ensure a free flow of relevant
information both up and down the ranks so that everyone is
clear on both the expectations and the results.
 Our investment strategy is generally conservative as we
recognize the substantial risk already embedded in the liability
side of our balance sheet. Our approach is to strategically
diversify the portfolio by asset class, industry and name to
maximize expected return within defined tolerance levels of
market value volatility. We expanded our investment in public
equity securities over the four years leading up to 2008. This
was done with a full understanding of the increased risk we
were assuming. We did have a sizable exposure to public equities in the beginning of 2008, but as market values declined,
we largely eliminated our equity exposure, which should reduce
volatility in the value of our portfolio going forward.
 On the fixed income side, which represented about 85%
of our invested assets going into 2008, we had restricted our
investments to higher credit quality securities and largely
avoided complex instruments such as collateralized debt and
loan obligations. We did not stretch for yield, and we kept our
portfolio duration short.
 Everest�s performance in 2008 clearly points to the
strength of our risk management framework. While some of the
largest and most respected companies in our industry were
hard hit by the financial market crisis, our strategy to maintain
a conservative, highly diverse, balanced investment portfolio
allowed us to come through this crisis comparatively well. We
did incur losses and we did have impairments with our investment losses about evenly split between equities and fixed
income. Most of the decline in the value of our fixed income
portfolio related to increased spreads brought about by the
repricing of risk rather than outsized exposures to risky assets
or financial instruments. Our total pre-tax return on investments for 2008 was negative 3%, considering both realized
and unrealized losses on investments and the investment 

income earned on the portfolio. In view of the financial market
dynamics, we believe this performance is quite acceptable.
We ended the year with total cash and invested assets of
$13.7 billion, with 93% allocated to investment-grade fixed
income securities.
 We exercise the same level of discipline in the construction of our underwriting portfolio. Each contract is evaluated in
the context of prescribed underwriting guidelines and return
expectations always with an eye towards risk accumulation.
Our long history has provided us with valuable data and tools
for measuring pricing and loss cost trends against current conditions, enabling our experienced underwriting team to make
sound decisions. We maintain all the traditional catastrophe
models for pricing and monitoring catastrophe risk and we
carefully control our region-by-region exposures. Having learned
a few lessons from the hurricanes of 2005, we dramatically
reduced our aggregate exposures to offshore energy, which
served us well with Hurricanes Ike and Gustav in 2008. Our
loss from these events, net of reinstatement premiums,
amounted to $257 million, well within our expected annual
average catastrophe losses.
 The strength of our risk management practices is exemplified by our long-term performance and the quality of our
balance sheet which is also reflected in the strong ratings that
we maintain with each of the rating agencies: A+ with A.M.
Best and Standard and Poor�s; and Aa3 with Moody�s.
Exciting Changes at Everest
Since our initial public offering in 1995, we have made many
changes to the organization; all with a focus on strengthening
our franchise, serving the needs of our clients, and optimizing
our returns. Starting out as a U.S. based reinsurance company
with less than $900 million in shareholders� equity, we have
grown to be a global leader in our industry, providing both
insurance and reinsurance products, with close to $5 billion in
shareholders� equity. Over that time, we have delivered average operating returns on revenue of almost 14% and average
operating returns on equity of almost 12%. Everest�s shareholders have also enjoyed, on average, almost an 11.5%
annual return on their investment since the Company�s initial
public offering, including stock appreciation and dividends,
while a similar investment in the S&P 500 only yielded a return
of just over 5% for the same period.
 We are proud of these accomplishments but recognize
the need to keep evolving in an environment that is ever changing. Toward that end, we have added several new members to
our management team who will help us shape the future of
Everest. Ralph Jones, a respected industry veteran, joined us 


in late 2008 and assumed the role of President and Chief
Operating Officer. His initial focus is on the insurance operations of the Company, and later you will gain some insights into
his plans for its future direction.
 In addition to strengthening our management team, we
have established operations in Brazil and Ireland. Following
the opening of the Brazilian market, we immediately applied
for and were granted a license to operate as an admitted carrier there. With thirty years of experience supporting the
Brazilian state reinsurer, we are well positioned to capitalize on
this important and growing territory and expect that this will
further strengthen our leadership position in the Latin and
South American regions.
 In Ireland, we established a subsidiary holding company
and a reinsurance company. This will headquarter our new
European platform. While we are recognized leaders in most
important markets worldwide, we continue to seek a stronger
foothold in Europe. We have historically been doing business
in Europe through a branch office of our Bermuda operating
company. An Irish subsidiary company provides us with �passporting� throughout the European Union, which means that as
an EU licensed company, Everest will be able to write business
in any other EU country without further licensing. We expect
Ireland to become a very important part of our corporate structure going forward.
 Despite these organizational changes, the essence of
Everest�s culture will remain part of its fabric. Nurtured since
the beginning, it is a culture that strives for above-average performance and sustained financial strength.
Capital Management and Balance Sheet Strength
We cannot emphasize these two areas enough. Throughout
2006 and 2007, the investment community continually challenged our conservative view of capital management. While we
engaged in share buybacks and increased our dividends to
return excess capital to shareholders, we approached this task
cautiously, recognizing our responsibility to maintain capital
flexibility in the event of a large loss.
 Our exposure to investment market volatility actually
exceeds our exposure to catastrophe losses and while we
might have envisioned our �excess capital� to be a safeguard
against a natural catastrophe, the 2008 financial market crisis
certainly emphasizes the benefits of maintaining a capital
redundancy. Today, despite the combined events of 2008, we
have substantial capital flexibility and believe we are well positioned to take advantage of any potential market opportunity
that may arise in the foreseeable future.
 In the following sections, we discuss some of the opportunities that we see in each of our markets.
Business Review�Reinsurance
Everest offers a broad array of property and casualty reinsurance products to primary insurance companies around the
world through operations in the U.S., Bermuda, Canada,
London, Europe, Asia, Latin America, South America, Africa
and the Middle East. The Company ranks among the top ten
global reinsurance groups, as measured by premium, and last
year wrote almost $3 billion of reinsurance business.

 In recent years, increasing levels of competition, particularly in the U.S., were putting pressure on pricing and causing
primary insurers to retain more of their business. We maintained our underwriting discipline in the face of this softening
market environment and as a result, our premium base was
shrinking. But the worldwide financial crisis, record level catastrophe losses, and significant loss of industry capital halted
this erosion, and we began to see the first signs of a positive
shift in market behavior.
 U.S. Property Reinsurance, which is written out of both
our U.S. and Bermuda offices, represents 23% of our reinsurance book. This market saw meaningful improvement during
the January 2009 renewal cycle, particularly catastrophe
exposed covers. These improvements came in the form of
both pricing and terms. Capacity was tight and many large
national programs could not be fully placed. Catastrophe
excess and retrocessional covers both saw double digit
increases in their rates and attachment points moved up.
Overall, this translated into a much better portfolio with higher
premium and lower exposure. We expect that by mid-year
when the Florida and Gulf market covers generally renew, reinsurance capacity will be further constrained, which should
lead to further improvements in the risk/return equation.
 U.S. Casualty Reinsurance, also written out of both the
U.S. and Bermuda offices, has been under significant pressure the last couple of years and now represents just 13% of
our total reinsurance premium. The impact of both rate
declines and higher ceding company retentions had impacted
this book but the January renewals showed some stabilization.
And specific types of risks, such as financial institution directors and officers (D&O) covers, showed, not surprisingly,
marked improvements in rate. Financial strength ratings are
becoming increasingly important for casualty business and
Everest has fared well in this regard. The end result was only a
modest increase in expiring premium, but the full effect of the
financial crisis has yet to be reflected in this market, and we
anticipate additional opportunities as the year progresses.
 International Reinsurance has become an increasingly
larger proportion of our reinsurance book and now represents
55% of this business. With offices around the world, we have
established leadership positions in most of the important global
markets. Our long-term presence in these markets coupled
with our financial strength ratings, with increased emphasis
being placed on the latter, has continued to foster growth in
these markets. The profitability of this business has been quite
strong. Rates have been generally stable in these markets with
London being the one exception, although the recent January
renewals did show signs of improvement. We would expect this
business to continue to grow as a result of our new offices,
market dislocations created by some of our competitors, and
the flight to quality trend evident in all reinsurance markets.
 Specialty Reinsurance, which includes accident and
health, marine, aviation, and surety business, accounts for the
remaining 9% of our total reinsurance premium. Accident and
health and aviation business has been declining over the last
number of years while marine and surety have remained relatively stable. A look at the January renewals showed accident
and health stabilizing and no significant changes in pricing,
capacity or retention in the aviation and surety markets. The
big story for the January renewals was marine which saw double digit increases. We, along with many of our competitors,
further limited our exposures to the Gulf of Mexico and as a
result there is significant stress in that region. Energy business, both onshore and offshore, largely renews mid-year, and
we expect there could be some attractive opportunities in
this area.
 Clearly, we are in the midst of positive change, and we
expect things to only strengthen as the year progresses.
Everest is well positioned to benefit from this trend given its
strong ratings, high quality balance sheet, seasoned staff, and
broad reach with locations around the world.
Business Review�Insurance
Our insurance operation is not insubstantial with almost $800
million in premium, 400 employees and 7 offices spread
throughout the United States. Our main focus is on specialty
casualty niche programs, which we access through managing
general agents. We are well regarded in this arena and have
grown to be one of the leading writers of program business in
the U.S., providing coverage to architects and engineers, security guards, public entities, residential �brownstone� buildings,
and other specialized risks.
 Increasingly, we are writing business directly through our
own offices, again with our focus being on specialty casualty
business where underwriting expertise is critical for success.
Given the expertise necessary to write this type of business, it
is generally less price-sensitive, and, therefore, pricing is more
stable. In 2008, we formed a unit to provide environmentalrelated insurance products and services for contractors, professional environmental consultants, and waste facilities. This
was a product that we had been underwriting�quite profitably�for more than ten years through a managing agent that
was recently acquired.
 This year, we have formed a unit consisting of industry
respected underwriters that will provide directors and officers
and errors and omissions coverage to financial institutions. 
This market has undergone dramatic change as a result of the
financial market meltdown with pricing and terms the firmest
they have ever been. We were fortunate to acquire the expertise that this team offers to augment the talent that already
exists within our group.
 We expect an ongoing shift in our business model towards
underwriting more business directly through the retail and
wholesale channels. As our operation matures, we are seeing
some opportunity for growth from program business but much
of what we have been seeing of late does not meet our criteria.
Since our approach to program business had differed from
many of our competitors, the expansion of our underwriting
strategy has been seamless. Our infrastructure is already in
place with full service underwriting, claims, actuarial, and
accounting staff. We will continue to write program business
but expect that a growing proportion of this book will be written directly.
 We will also seek out acquisition opportunities to propel
growth, but we will be very selective as it is critical that any
acquired entity share a common philosophy of selective and
disciplined underwriting with a focus on profitability. Since
implementing our insurance strategy in 2002, the business we
have written has produced good results with an average annual
combined ratio of 96%.
 As we look ahead, we see positive market momentum
and expect increased opportunities will emerge for us to grow
our business successfully. Our franchise value and rating
strength are key elements to achieving this goal.
 In closing, we thank you for your continued support. We
are excited by the opportunities we are seeing in 2009 and
believe we are positioning the Company to reap these rewards.
As always, we are mindful of the responsibility we have to you,
our shareholders, to produce a reasonable return and safeguard the Company�s capital against untoward risk.
 Thank you, as well, to our Board of Directors for their
guidance and oversight and to our employees whose hard
work and dedication make all this possible.
Sincerely,
Joseph V. Taranto
Chairman and Chief Executive Officer
Thomas J. Gallagher
Vice Chairman and Chief Underwriting Officer
Ralph E. Jones III
President and Chief Operating Officer