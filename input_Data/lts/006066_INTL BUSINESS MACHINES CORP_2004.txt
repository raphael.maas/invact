
chairmans letter
Now its a year later, and I want to tell you about the progress
we have made. IBM today is stronger and more focused than
it has been in years. The path we set for ourselves several
years ago is yielding resultsin terms of an improved competitive
position, an enhanced capacity to innovate, and a greater
ability to deliver results to our clients and to you, our owners.
Our task now comes down to execution. At a recent
meeting with IBMs senior leaders, I said that 2005 is the year
of small s and big E: less focus on strategic development,
maximum push on execution. This is the appropriate emphasis
for our company today, because most of the major strategic
pieces are now in place for IBM to become the leader of a
rapidly changing information technology industry.
A good year
I want to explain to you what we did last year to turn strategy
and vision into results. Your company turned in another good
year in 2004.We continued to execute our business plan
effectively, producing share gains in key markets, increasing
revenue and growing both earnings and earnings per share.
Our results from continuing operations saw record
revenue of $96.3 billion, an increase of 8 percent; earnings
of $8.4 billion, an increase of 11 percent; and diluted earnings
per share of $4.94, an increase of 14 percent.
One of the strengths of our business model, from a
financial point of view, is the amount of cash we generate.
After committing $5.7 billion to R&D in 2004, we had
Dear IBM Investor: Last year, I told you that IBM was in the process of
becoming a very different company. I said that we had achieved a new
degree of clarity about our business modelinnovation for the enterprise
and that this was driving change in every aspect of the corporation. And I
described the deeper level of this reexaminationour collective work to
redefine IBMs core values.
Samuel J. Palmisano
CHAIRMAN, PRESIDENT AND
CHIEF EXECUTIVE OFFICER
$12.9 billion in cash available for investment and distribution
to shareholders. Of that, $3.7 billion went for net capital
expenditures and $1.7 billion for acquisitions that strengthened
our capabilities.
We were able to return a record $8.3 billion to investors
$7.1 billion through share repurchase and $1.2 billion through
dividends. We ended the year in a strong cash position,
with $10.6 billion, including marketable securities. In 2004,
our return on invested capital increased to 29 percent ,
excluding our Global Financing business and a one-time
pension settlement charge.