Its a bold claim  Quite possibly the busiest chemical company on the planet.
Lets consider the facts. In 2010, LyondellBasell completed its restructuring, secured
$8.9 billion in exit financing, and established itself as a public company with a
successful listing on the New York Stock Exchange (NYSE). New, world class facilities
began production in Germany, China and Thailand. A renewed commitment to
operating excellence enabled our company to set production records at 60 percent of
our plants around the globe. While achieving these results, we improved our safety
performance and continued to implement disciplined cost controls.
2010 EBITDA totaled $4 billion, significantly more than anticipated and $1.8 billion greater than 2009. Led by a new
Board of Directors and new management team, both chosen for their proven abilities and industry experience, our
employees worked to create a culture that champions and rewards high performance. Even so, we know good is not
great. Our sights are set on becoming a truly great and enduring enterprise.
Looking ahead, we see a bright future. We enjoy an advantaged ethane feedstock position in the U.S. and have taken
steps to increase our ethane use and availability. We have improvements underway at locations like the Houston refinery
and are debottlenecking other assets to give us an opportunity to grow our capacity.
Our balance sheet is strong, and we are on track to optimize our financial structure for the long run. We have
demonstrated a significant ability to generate cash and earnings and have regained trade credit. We repaid $1.2 billion
in debt in the fourth quarter and still our liquidity remained at $6.1 billion with a year-end cash balance of $4.2 billion.
Last year we also restructured numerous sales contracts, enhanced our operating flexibility, and improved energy
efficiency. We invested in our internal capabilitiesmoving from multiple systems to a single global SAP system.
We have streamlined our workforce and continue to optimize to improve efficiency.
So, how are we positioned for the future? We believe our efforts will continue to be rewarded in a strengthening market.
The solid foundation weve built, when combined with our people, assets and technology is leverage for the future.
Our aim in 2011 will be to build upon and accelerate our successes in a few key focus areas including:
 Intensifying our focus on safety, reliability and costs,
 Completing major turnarounds at the Houston refinery and one of our Channelview olefins plants,
 Building on our 2010 SAP system consolidation, and
 Further optimizing our capital structure.
We sincerely appreciate the dedication and contributions of our employees. Their hard work in 2010 gives us
tremendous cause for optimism about the coming year. We pledge to continue to work together to maintain our focus
on operating excellence and to strengthen our portfolio for the benefit of you, our owners and stakeholders.
Thank you for your continued support of LyondellBasell. The best is still ahead.
James L. Gallogly, Chief Executive Officer