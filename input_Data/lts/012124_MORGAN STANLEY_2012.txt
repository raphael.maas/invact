dear fellow shareholders,
During 2012, we passed several important milestones along this journey, arriving at what
we have described as a pivot point: With a sharpened business strategy, a fortified balance
sheet and strong capital levels, we are poised to do a better job than ever of serving our
clients and delivering attractive returns to you, our shareholders.
Along the way, we have continually returned to the principles that guided this Firm during
its 77-year history  and most recently, we revisited three fundamental questions: What
do we do? How do we do it? With what result? We asked Firm leaders around the world
to reflect on these questions, and the result was a fresh articulation of both our long-term
strategy and our core values. The people of Morgan Stanley have embraced this process
with their customary creativity and intelligence, and it has energized our company.
We have been on a multi-year journey at Morgan Stanley. Beginning with the financial
and economic upheavals of the preceding decade, this journey has involved a thorough
reassessment of our mission  and subsequent actions to put our Firm on a path
to more consistent, sustainable profitability

our strategy
Organizations that last over time  in Morgan Stanleys case, nearly 78 years  have an
essence that gets them through the inevitable and sometimes violent cycles of our markets.
Growing organically from our origins during the Great Depression of the 1930s, we
underwrote stock and bond offerings for companies that would help lead the world out
of economic stagnation.
During 2012, we took the time to rearticulate our strategy and to lay the groundwork
for continued success in a world that is rife with change. Today, Morgan Stanley advises,
originates, trades, manages and distributes capital for clients, adhering to a standard of
excellence that is evidenced by the leading positions that our businesses hold in the global
marketplace. We do this in a manner that is consistent with our values and whenever
possible, we draw from the capabilities that reside across our organization to deliver to
our clients more than just one part of the Firm. We know that if we execute consistently
on this strategy, we will create value for our shareholders and continue to attract and
retain the best people in the industry.

investing in our businesses
To support this strategy, we must continuously invest in our businesses. Surveying the
last few years, I am proud of the work we have done to lay a strong foundation for Morgan
Stanleys future.
Our Investment Banking franchise perennially ranks at or near the top of the global
league tables in advising on mergers and acquisitions, and underwriting equity and initial
public offerings  and 2012 was no exception. One area of particular note was our debt
underwriting practice, where investments we made over several years led to higher
market share and wallet share over the full year. More broadly, we advise governments
in every region of the world, and we have assisted a number of European nations to manage
the challenges of the euro currency crisis by raising
capital and otherwise helping them restructure their
sovereign debt obligations.
Equity Sales and Trading remains one of the top
franchises of its kind in the industry, offering our
clients expertise across a broad range of products, in
markets all over the world. Within Fixed Income and
Commodities Sales and Trading, we are concentrating
our efforts on areas of growth, including interest rate
and foreign exchange products. We have built a leading
electronic trading platform, where we witnessed
significant growth in volumes during 2012. As regulations
pertaining to derivatives and other parts of our
sales and trading business unfold over the next 12 to
18 months, we expect trading markets to be more client-centric, less dependent on balance
sheets, and heavily electronic. We have planned and invested with these outcomes in mind,
and we are confident that our businesses will lead the way.

In Global Wealth Management, we are utilizing a growing deposit base to build out banking
and lending services that are tailored to our affluent clients. We also seek to enhance,
expand and improve the client experience in other key areas: our advisory business,
where we have been the industry leader in managed accounts for 40 years; our capital
markets group, which provides unparalleled access to products and content; and our
technology platform, the foundation of our financial advisors efforts to deliver superior
advice and service.
Over the past several years, Morgan Stanley
Investment Management has set its sights
on improving its distribution capabilities,
and this effort has helped drive steady
inflows from investors around the world.
MSIM increased assets under management
by 18 percent in 2012, and delivered strong investment performance across a broad range
of asset classes  access to which is essential for meeting the diversification needs of
institutional and individual investors alike.
progress in 2012 and beyond
In 2012 specifically, we achieved clarity around the cost and timetable for acquiring full
ownership of our wealth management joint venture, now doing business as Morgan Stanley
Wealth Management. This would increase the percentage of profits that our shareholders
realize from this business  one of the largest of its kind in the world  and deepen the
opportunities we have throughout the Firm to collaborate on behalf of clients.
The benefits to Morgan Stanley of owning a wealth management business of scale are
unmistakable: sizeable and stable revenues, a substantial deposit base, low relative capital
usage, and a growing demand for professional advice. With the Smith Barney integration
completed, we look forward to strong and growing profit margins in this business.

Another important accomplishment, where we have moved ahead of schedule, has been
a reduction in risk-weighted assets, or RWAs, on our balance sheet. These RWAs, which
primarily support our Fixed Income and Commodities businesses, have come down more
than 40 percent from a peak of approximately $500 billion, and we ended 2012 a year
ahead on targeted reductions. We will reduce these further in 2013 and beyond.
Why is this important? By reducing RWAs, we are freeing up capital that can earn more
attractive returns elsewhere in our business or ultimately be returned to shareholders.
And we are doing this in a way that does not sacrifice our vision for an appropriately
sized Fixed Income business  one that makes
efficient and profitable use of capital as it serves
our clients. Our Fixed Income platform is an
essential complement to our other institutional
franchises, Investment Banking and Equity Sales
and Trading, and as such, it is an important part
of our future.
Equally critical has been our resolve to reduce
expenses. Net of investments we have made
in our businesses, expenses declined by $500
million in 2012, and we intend to achieve
further reductions this year and next. After
reducing our headcount by more than 6,000 since the end of 2011, we believe our Firm is
sized appropriately for the operating and regulatory environment as we currently see it.
While we sometimes wish that markets were faster to recognize our progress, we are
pleased that equity investors appear to have taken notice of our momentum, particularly
in the second half of 2012. Our credit spreads have also tightened dramatically with
broader acknowledgment of our long-term strategy and our overall financial strength.
We are operating with confidence about our mission within the global capital markets,
and conviction in the strategy we have built to carry it out.

our plan to drive higher returns
As successful as our businesses have been, we are just scratching the surface of the
opportunity we have to align them more closely, benefiting our clients and growing
revenues in the process. We have more than 35 initiatives underway to increase collaboration
between our institutional and wealth
management businesses. These opportunities
are unique to Morgan Stanley and should yield
accelerating results over the next several years.
One major focus is a more coordinated and
systematized approach to senior relationship
management, as existing and potential business
partners of wealth management are often top
clients of our institutional businesses.
Looking forward, we are doing a number of things to drive returns higher. We have articulated
a clear and detailed plan for meeting and exceeding our cost of capital. It includes
the aforementioned acquisition of 100 percent of the wealth management business and
continued reductions of RWAs; achieving wealth management profit margin targets

through expense management, and exceeding them through revenue growth; controlling
expenses across the Firm in 2013, 2014 and beyond; and growing earnings through
collaborative opportunities that are specific to Morgan Stanley. We are highly confident
in our ability to execute on this plan.
A final word about the values that inform everything we do: We have a commitment to
putting clients first, leading with exceptional ideas, doing the right thing and giving back.
This commitment honors both our history and our aspirations for the future. Every day,
in all of our offices across the world, we expect our 57,000 employees to live these values.
That is what great organizations do.
Thank you for your investment in Morgan Stanley. I am excited about our future and
confident in our ability to deliver lasting value to our shareholders.
james p. gorman
chairman and chief executive officer
march 21, 2013