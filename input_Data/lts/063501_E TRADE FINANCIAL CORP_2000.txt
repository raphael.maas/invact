                                                                                                       LETTER TO SHAREOWNERS




You Saw the Future of Money--And it Looked a Lot Like Us. We understand what
people want: value, ease-of-use, and a simplified financial life. And we're delivering that to
them by continuing to create a new kind of company. The kind that sets standards for an entire
industry. It's a leadership strategy of vision, culture, technology, brand, and innovation.

E*TRADE Is No Longer Just an Online Brokerage Firm. In a very active twelve months, we have
transformed E *TRADE into a broad and deep financial services hub. Our customers can now do everything from
check their stocks in the morning to get cash from one of our 9,600 ATMs in the afternoon to transfer money
from their bank account to their brokerage account at midnight. Soon, they'll be able to look to us for advice,
plan for their children's education and trust us to manage their 401(k) plans.
   That convenience and broad consumer experience is what makes us different today from a year ago. But
our core values haven't changed and never will--and that's what makes E *TRADE so successful. Through the
democratization of financial services, we're disrupting and disintermediating an inherently inefficient industry.
We're still in the vanguard of a lifestyle revolution. We're still giving the power of the few to the many. We're still
creating the future of financial services.

Let's Do the Numbers. Look at the top and bottom lines:
   $1.4 billion in net revenues for fiscal 2000, up 104 percent from last year, reflecting the continuing diversification



   of our revenue stream. We're one of just five Internet companies with net revenues in excess of $1 billion.
   Customer assets of $66 billion, up 114 percent for the year, while the Nasdaq grew just 34 percent. We



   continue to be trusted with record asset levels as we develop insightful and sophisticated tools, products, access
   and content to help consumers simplify their financial lives.
   $475 million in earnings before marketing from ongoing operations, more than double that of 1999, reflecting



   the inherent profitability, power and diversity of our business model.
   Annual revenue per customer of $547. We're getting more value from our customers than virtually any other



   e-commerce business.


   We're still in the vanguard of a lifestyle revolution. We're still giving the power of the few
                                to the many. We're still creating

                                                the future of financial services.


   Gross margins of 62 percent, up 700 basis points for the year, improving a record that is already one of the



   industry's finest.
   Over 3 million customer accounts. We nearly doubled our customer base in the last 12 months. And while we



   were at it, we achieved a low per customer acquisition cost of just $263 for the year.
   Profitability, profitability, profitability. We've now had three consecutive quarters of profitability from ongoing



   operations. In late 1998 we took a moratorium on profitability, to build our blue-chip brand franchise and invest
   in our technology infrastructure and international businesses. In early 2000, the success of our investments was
   clear as we returned to profitability 12 months ahead of plan.

We're sharply focused on the growth of the business, and we're doing it in two key ways: by leveraging our innova-
tive products and services to expand the lifetime value of our current customer base, and continuing to grow the
customer franchise we've built.

You Can Take it to the Bank. E *TRADE BankSM is the cornerstone of that strategy, and it's showing incredi-
ble momentum. We closed the acquisition of Telebank at the beginning of the calendar year, and launched it with
the E *TRADE brand name just three months later. The immediate success of the rebranded Bank clearly under-
scores the power of the E *TRADE brand. Since the launch of the E *TRADE name, the Bank is booking new
accounts at double the rate it was a year ago. In fact, E *TRADE Bank is now the 19th largest federally chartered
savings bank in the U.S.
Let's look at some of the Bank's accomplishments:
   E *TRADE Bank surpassed $4.6 billion in deposits and $9 billion in total assets--over 100 percent growth in



   the past year.
   E *TRADE Bank contributed 52 percent of total interest income, and 25 percent of gross revenues.



   E *TRADE Bank more than doubled its account and deposit base from a year ago, adding a record 65,000 net



   new accounts in the fourth quarter alone, and over $700 million in deposits.
   Our cross-selling between banking and brokerage is accelerating, and at $60, the cost to bring an existing broker-



   age customer to the Bank is a fraction of the cost to acquire an account in the marketplace. The cross-sell of
   banking to our brokerage customers is an incredibly valuable proposition for E *TRADE, doubling the annual
   revenue and assets we gain from these customers.




    It's a Small World After All. We've always said that this business represented a huge global opportunity, and we
    continue to drive our growth worldwide. With branded sites in nine countries outside the U.S., we are strengthening
    E *TRADE's position as the only truly global brand franchise in the industry. Growth is strong--as is brand
    recognition. The UK, Sweden, Canada and Australia all delivered triple digit growth in accounts and assets for
    the year. Both Canada and the UK received #1 ratings from Gomez Advisors. Korea alone executed over half a
    million trades during the fourth quarter, and it launched only seven months ago.
      With the consolidation of E *TRADE Germany, we not only own 100 percent of the business in the largest
    market in Europe, we now have a banking license in Germany, and are set to launch banking and brokerage services
    there in the coming year.
       We recently launched E *TRADE South Africa, where we have a strong first-mover advantage. Online financial
    services is an emerging industry in South Africa, and E *TRADE is in on the ground floor.
       In September, E *TRADE Japan completed its initial public offering on the Nasdaq Japan Exchange, initially
    valued at $1.2 billion. We took this opportunity to monetize a portion of our international investment, selling
    20 percent of our equity stake in E *TRADE Japan for $80 million. We continue to hold a 32 percent stake in
    E *TRADE Japan.
       The acquisition of VERSUS Technologies gives E *TRADE the expertise to create an electronic trading platform
    for our more than 650 institutional customers worldwide. This revolutionary platform, which we plan to start
    rolling out in 2001, will connect investors, institutions, investment dealers, exchanges and alternative liquidity pools
    to one another around the world.


    Shop for Clothes, Stop at E*TRADE. We're placing E *TRADE in the center of consumers' lives--where
    they live, work and play. There are still 10 million consumers who invest with an offline broker but track their
    investments and conduct their own research online. E * TRADE is engaged in a number of initiatives to capture this
    massive category, all focused on deepening our relationship with the consumer.
      The addition of our new ATM network is the springboard that will propel E *TRADE Bank's next growth phase,
    as it also accelerates our bank and brokerage cross-selling efforts. As we build out the network, we're creating a
    value-added service for our customers while placing the E *TRADE brand on every corner.




continue to drive our growth worldwide.


         The deep relationship with the consumer takes root as we penetrate the fabric of consumers' lives with E *TRADE
      Zones in SuperTarget stores, which first launched this year in Roswell, Georgia. The Zone features ATMs, customer
      service representatives, and a full service E *TRADE financial kiosk offering brokerage and banking capabilities,
      electronic transfer of funds, streaming media and much more.
         Beyond the E *TRADE Zone is another retail access channel. Our Business Solutions Group is poised to reach
      the 1.3 million employees of our corporate customers who have over $130 billion in unrealized assets in vested and
      unvested options. These customers are given access to an E *TRADE account when they exercise their options, and
      we're giving them the products and service to keep them here.
         The launch of the E *TRADE "Virtual Credit Union" will make the E *TRADE brand part of corporate infra-
      structures throughout the country. Top-tier Fortune 500 companies will be able to offer their employees packaged
      E *TRADE services, including bank and brokerage, through their company Intranet. We plan to install E *TRADE
      branded ATMs in the companies' corporate headquarters and major facilities, connecting people to their finances in
      the simplest and most convenient way possible.

      Advice--The Way You Want It. One of the prime movers of our asset gathering strategy is something no one
      thought an online brokerage service could provide--a first-class advisory service. Through our partnerships and
      alliances, we're fully integrating the advisory component into new and existing products. eAdvisor, our joint venture
      with Ernst & Young, will offer an integrated electronic advice product and will give our customers access to a
      nationwide network of over 900 financial planners, filling a huge pent up demand in the electronic financial
      services marketplace.
          We're also developing a platform from which individuals--with as little as $100,000 of investable assets--can
      evaluate, select, access and monitor money managers on an ongoing basis via the Internet. In addition, we plan
      to introduce a product that will enable customers to create personal portfolios of securities. And we're developing a
      turnkey 401(k) solution for our corporate customers.
         According to Jupiter Research, online assets are expected to grow from $1.5 trillion today to $5.4 trillion by 2005.
      With our asset gathering products we expect to capture a share of this growth and garner over $1 billion dollars of
      additional annual revenues in the next five years.




                                                                                                                                
                                       We're creating a value-added service




       It's all about helping our customers achieve their financial goals. Until the Internet came along, the consumer
    really did not have an advocate in the financial services industry. E *TRADE is that advocate.


    You Connect with Us, We Connect with You. The Internet--and the ways people reach it--continues to
    grow. We're coming through on our pledge of anytime, from anywhere, on any platform. We see wireless as an
    essential growth market for the empowered individual. By 2005, 61 percent of the U.S. population will be using
    wireless Internet-capable devices. That's 175 million consumers. And that's why E *TRADE now has the widest
    wireless reach of any financial services provider.
       Our continuing drive to offer the highest quality, top performance products and tools to enable our customers'
    success continually gets accolades from industry rating services and the media. Our Web site has been rated Number
    One by leading research and evaluation services such as Gomez Advisors and Lafferty.
      Success in this business is about much more than simple investment. It's about defining a whole new business
    model for our industry, one that changes the fundamental way we interact with consumers.
       This new business model that E *TRADE is spearheading was created in response to a fundamental demand
    that went unmet in the industry. People want value, ease-of-use and simplicity in their financial lives. And E*TRADE
    has built a blue-chip brand defined by meeting that demand.

       If you want to create a superior, integrated customer experience, you'd better be quick about it. And we are. Our
    


       average phone answer time for trading during market hours has been dramatically reduced.
       If you want to enhance your critical customer relationships, you'd better have answers to all their questions. And
    


       we do. With customer service that's constantly offering new technologies and tools while increasing the lifetime
       value of our customers.
       If you want to give people total access to their money, you'd better be able to come through when they need you.
    


       And we deliver. Our E *TRADE Everywhere strategy is providing the technology that lets customers connect to
       all their finances wherever and whenever they want.
       If you want to stay a leader, you'd better set new standards for the future. And we have. With new products and
    


       services that continue to set the standards for the industry--even while we're raising the bar.




6
for our customers
                  while placing the E *TRADE brand on every corner.




         Our customers want more--and we're delivering it. A brand is only as strong as the products and service that
      support it. Our unmistakable global brand is one of E * TRADE's greatest assets, and it will continue to be a power-
      ful weapon in our arsenal as we grow the business.
          Our new positioning--"E *TRADE. It's Your Money."--and the ad campaign that goes with it capture
      E *TRADE's evolution into a fully integrated financial services company, offering consumers the breadth, depth
      and personalization of banking, brokerage and investment planning services to take control of their financial lives.
         Our brand differentiation has long defined E *TRADE, and with this step, it will continue to do so in the future.
      E *TRADE defined itself in the industry by offering consumers a way to save money. But that's no longer enough.
      Today's consumers are demanding service that empowers success, and enables them to make money. The E * TRADE
      brand is expanding to encompass that as we become the central provider in our customers' financial lives.
          We've made a lot of smart strategic moves over the past year, and we now have the pieces in place, and the
      products in the pipeline, to launch us into the next phase of growth. We think that the innovative products and
      initiatives that are slated for fiscal 2001 are really going to make this model hum, and we've put an incredibly
      talented leadership team and culture in place to do it.



                                                                                                 Christos M. Cotsakos
                                                                         Chairman of the Board, Chief Executive Officer,
                                                                              and Shareowner of E*TRADE Group, Inc.

