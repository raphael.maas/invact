To my fellow stockholders,
Thank you. Yes, thank you to our valued clients, and thank you to our long-term stockholders.
Why begin my annual opportunity to speak directly with  stockholders with a thank you? It's simple. Every success we achieved in 2017 is just a reflection of the trust millions of clients  and stockholders have in us. And their trust led to records in virtually every key area of measurement in 2017:
� Active brokerage accounts
� Revenue
� Net new assets
� Net income
� Client assets
� Stock price
Are we proud? Yes, of course. But far more, we are grateful and humbled. We are grateful for the trust our clients and stockholders place in us. We are humbled by your response to our "Through Clients' Eyes" strategy and the execution of our "Virtuous Cycle" approach to building the business. Again, thank you!

We are committed to continuing to build the type of company that treats all constituencies-our clients, our stockholders, our employees, our vendors and partners, our regulators, and our communities-with honor and respect.
This is my tenth opportunity to have the honor of writing to you. As in the past, my goal is to keep this letter clear, direct, and free of jargon, corporate speak, and trendy buzzwords. The litmus test for my letter is whether it reads as if I were corresponding with a business partner who has been out of touch for the past year. And as always, please let me know if I've achieved this goal.

The world we operate in is changing faster than ever before. The traditional expectations for price versus quality tradeoffs are breaking down all across our economy. On a recent Saturday, in fact, I ordered a jacket online for a lower price than it was offered for

at a local mall. The package containing my new jacket was sitting in my driveway by dinner that evening-a dinner from an outstanding local steakhouse that was also delivered as fresh and warm as if my family had been sitting in the actual restaurant.
This is the business environment that Schwab operates in...and is succeeding in. At Schwab, we have embraced- and even championed-the concept of "no trade-offs" for investors-an approach that contributed to our recordbreaking results.
Let's discuss three of the factors changing the world of investing today and how our strategy positions Schwab to lead the change and win in the market as a result.
1 The concept of "beating the market" has given way to a client focus on finan
planning, asset allocation, tax efficiency, and low-cost investing.
2 Fiduciary-standard advice, fee transparency, and low fees are a fundament expectation for most clients.
3 Scale is playing an increasingly large role in determining the "winners," as c related to cybersecurity, compliance, and regulatory oversight challenge su scale firms' ability to compete effectively.


For many years, experienced investors have understood just how difficult it is to "beat" or "outperform" the market on a consistent basis. Most industry studies show that very few professionals can generate market-beating returns over time, and it's almost impossible for any investor to identify who those professionals might be in advance.
Today, many wise investors put their emphasis on proper planning, asset allocation, tax efficiency, and keeping costs as low as possible-all factors that have driven success over time and all within every investor's control. In my opinion, no firm in the industry is doing more to support these essential ingredients of smart investing than Schwab.
In 2017, we completed 142,000 financial planning conversations with clients. Along with the independent investment advisors we

serve, we provided asset allocation and advisory services to clients with $1.7 trillion in assets, and we did so at a value and cost that left more money in the pockets of those clients. After all, it is the investors' money to begin with, and the less we take in fees, the more remains to grow and compound.
Who would have thought over 40 years ago that one day Schwab would be recognized by J.D. Power as "Highest in Investor Satisfaction With Full Service Brokerage Firms" in both 2016 and 2017?1  And who would have thought that our comparable advisory services and index funds would often have lower fees than a firm like Vanguard?
That is no trade-offs!


The word "fiduciary" sounds complex...but it really isn't. A fiduciary assesses what's best for another and puts that person's interests ahead of its own.
This is no different from when you visit a physician, and you expect advice tailored to your health needs and not influenced by the last medical equipment salesperson who spoke to your doctor. Similarly, when you speak with an attorney, you expect advice offered exclusively in your best interest.


And when you retain Schwab for fee-based investment advice, or one of the thousands of independent registered investment advisors who custody their client assets at Schwab, you should be able to count on the same level of objectivity, transparency, and professionalism.
A simple fact is: when you pay Schwab a fee for our advisory services, we provide that advice in a fiduciary capacity, placing your interests ahead of ours.
Why does this matter so much? Because investors have realized that:
* "Receiving advice in their interest" is very different from "being sold a product," and

* Understanding the exact fees and expenses being paid is not only critical to minimizing those fees and expenses, but the easiest way to keep more of their money invested.
It is no surprise that two of the fastest growing parts of Schwab are the advisory services we provide directly to investors, and the custody and related services we provide to support the growth of independent investment advisors.
Quality advice from a professional putting your interests ahead of their own...transparency as to exactly what you are paying for this advice...and a goal of keeping those costs as low as possible. And our unique Satisfaction Guarantee, which ensures that if for any reason, any client, no matter how large or small, is dissatisfied with the service they receive from Schwab, we will return any eligible commissions, fees, or advisory program fees.2
That is no trade-offs!



The world we operate in is not only changing rapidly, it is increasingly complex. It is fraught with the threat of cyberattack, and it demands risk management and oversight as never before.
And yet, at the same time, investors expect and deserve outstanding service and world-class value. How can investment services firms deliver on these seemingly conflicting objectives? One critical factor is scale. With client assets approaching $3.5 trillion, Schwab has enormous economies of scale advantages that we leverage for the benefit of investors.
Spreading these important costs over almost $3.5 trillion in assets allows us to deliver on our commitments, and to do so while operating with more efficiency than any of our publicly traded competitors-by a wide margin. Operating with greater efficiency means that we can deliver on our commitments, charge our clients less, and yet still achieve outstanding results for our stockholders.
In 2017, we chose to share further scale benefits with our clients by aggressively reducing pricing in a variety of areas ranging from online equity commissions to index mutual funds to purchased money market funds. The annual value of fees we reduced put

nearly $400 million back in the pockets of our clients-that's between 4% and 5% of our total revenue. In the "Virtuous Cycle," we share the benefits of our growth with our valued clients!
That is no trade-offs!


We believe investable wealth in the United States exceeds $30 trillion. So even though we are now serving almost $3.5 trillion in client assets, we are far from maximizing our growth potential. The opportunities to serve millions more investors and gain their trust to invest with Schwab are enormous.
Today, we are better positioned than ever to capture a growing share of the market by continuing to build on our "no trade-offs" approach-delivering quality service and advice, offering transparent and low prices, and leveraging our scale to operate efficiently and profitably.
And as we transition into 2018 and strive to pursue this growth, we will operate with several clear priorities:
� Ensuring we continue to build client trust by making every decision
"Through Clients' Eyes,"

� Optimizing our earnings power while continuing to invest in our clients and our people, and

� Investing in the business for the long haul, with a focus on growth, infrastructure, and risk management.

We believe your company has reached a point where we should be investing not only to support the growth we are achieving now, but also to "clear the path" for the growth we're going to achieve over the next decade and beyond. You'll see us working on both those fronts in 2018.


As I mentioned in the opening of this letter, this is my tenth opportunity to share with you my thoughts, dreams, challenges, and ambitions for your company.
The past decade has been a remarkable one. From the depths of the financial crisis and the resulting zero interest rate policies enacted by the Federal Reserve, to the highs of record stock market levels and consumer confidence-one thing has remained unchanged: our "Through Clients' Eyes" strategy.
Every day we ask ourselves how we can use our intimate knowledge of investing, saving, and the markets to better serve our clients, to offer them better value...to serve them the way we would want to be served.
This simple approach consistent with the Golden Rule comes to life in our "Virtuous Cycle." Treating clients as we would want to be treated has fueled our tremendous growth in recent years. That growth has helped us build scale and grow profits, and, importantly, it has also allowed us to share the benefits of that increasing scale with our clients in the form of great service, expanded investing solutions, and lower pricing.
So I will close where I started-with a sincere thank you. Thank you to our valued clients, and thank you to our longterm stockholders.
Your trust and confidence in Schwab have helped us build a remarkable company, one whose future has never been brighter!

Warmly,

Walt Bettinger March 2, 2018

