Dear Fellow Shareholders:
We are fortunate to be in
the business of education.
It gives us a noble purpose
as an organization: to improve
the lives of our students, to open
up opportunities and every day
to unlock the full potential of
thousands of people. It is why we
are passionate about what we do.

Our excellent results in fiscal 2008 reflected our steadfast
focus on executing our strategic plan. This plan outlines
an ambitious set of goals:
 Achieve the full potential of DeVry University and its
Keller Graduate School of Management;
 Grow through continued diversification:
 Across vertical curriculum areas, especially healthcare;
 Throughout the horizontal levels of education; and
 Geographically across the globe;
 Build the infrastructure to support this growth,
including online capabilities, information technology,
finance and human resources.
We expand on these priorities in the pages that follow.
Excellence in Academic Outcomes
Our success is predicated on our students success.
Our investments in academic quality were manifested in
outstanding outcomes. DeVry University exceeded its
90/40 employment outcome targetour goal of 90
percent of our graduates employed in their field of study
within six months of graduation at an average salary of
$40,000. Our students did better than that: 92 percent
were employed in their field at an average salary of
over $44,000. We see this as a particularly noteworthy
outcome in light of the current tight labor market.
At Ross University School of Medicine, our U.S. Medical
Licensing Exam Step I results are now on par with U.S.
schools, and at Chamberlain College of Nursing, our
students results on the National Council Licensure
Examination for registered nurses improved as well.
Chamberlain students achieved an 80+ percent first-time
pass rate in our Associate of Science in Nursing
programs, and a pass rate of over 90 percent in our
Bachelor of Science in Nursing programin the top 10
percent of all nursing programs in the U.S.
Strong Growth, Margin Expansion and Value Creation
By keeping our focus on student success, the numbers
tend to take care of themselves. And in 2008, the numbers
were very good:
 DeVry reported record revenue of $1,092 million, up 17
percent versus last year, and net income up 65 percent,
to $125.5 million, another record.

All of our business segments delivered doubledigit
revenue and operating income growth. DeVry
University segment revenue was up 15.5 percent versus
last year and operating income, excluding discrete
items, increased 265 percent as a result of enrollment
growth, operating leverage and disciplined expense
management. In the medical and healthcare segment,
comprising Ross University and Chamberlain College
of Nursing, revenue was up 23.8 percent and operating
income up 11.2 percent.
 The professional and training segment, led by Becker
Professional Review, delivered another record year
with revenue of $81.1 million, up 19.4 percent and
operating income of $33.8 million, up 31.4 percent
over fiscal 2007.
 Our enrollments grew across the board. Total
undergraduate student enrollment in Summer 2008
at DeVry University was up more than 12 percent
over fiscal 2007. Chamberlain doubled its summer
enrollments to 2,176 and Ross University May
enrollments grew almost 8 percent, beating our
goals despite capacity constraints.
In fiscal 2008, we made significant progress in enrollment
growth and student outcomes, and the response by
investors testifies to that fact. In the past year, the overall
market value of DeVry increased by $1.4 billion, or 58
percent. This follows a 55 percent increase in fiscal 2007.
Progress on Our Strategic Plan
In addition to our organic growth, we also made
progress in our plan to grow and diversify across vertical
markets, throughout the levels of education, and in new
geographic regions. For example, we made substantial
progress in our plan to establish Chamberlain as a
national college of nursing, successfully launching two
new campuses, in Illinois and Arizona. Further, we
completed two significant acquisitions, Advanced
Academics (AAI), in October 2007, and U.S. Education,
in September 2008.
The acquisition of AAI opened an important new
growth area for ussecondary education. AAI operates
traditional high school classroom programs and online
course instruction specifically designed for secondary
education; it also operates virtual high schools in
partnership with school districts and charter schools in
six states. Since its inception in 2000, AAI has delivered
online programs to more than 40,000 students in more
than 200 school districts.
As part of our strategic plan, we identified the career
college and allied health market as a promising growth
and diversification opportunity for DeVry. After a
thorough process of identifying the right partner, we
acquired U.S. Education, a 17 campus, 8,700 student
network of career colleges in seven western states. In
addition to establishing a presence in allied health, this
transaction will enable synergies in marketing, online
course expansion and campus co-location.
Fiscal 2009 and Beyond
Fiscal 2008 was a great year. Our outlook for fiscal 2009
is strong, as we build upon the momentum we created
this year. Education is a growing, vital industry. Within
this overall environment of growth, DeVry has abundant
opportunitiesto diversify our educational programs,
to expand margins, and to strengthen our team.
All this would not be possible without the passionate
and dedicated people of DeVryour professors,
operational staff, technology experts and everyone else
who strive to make this a great organization each and
every day. It is this team that dedicates itself to helping
our most important constituentsour studentsrealize
their full potential.
Sincerely yours,
Daniel Hamburger
President and Chief Executive Officer