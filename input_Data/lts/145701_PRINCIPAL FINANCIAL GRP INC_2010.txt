to our
shareholders
In 2010, we were ready. Ready to help people and
businesses rebuild. Ready to connect them to advisors.
And ready to help them think again about achieving
their financial dreams.
Just as we have for more than 130 years, the Principal
Financial Group (The Principal) remained focused
on listening to our customers, serving their needs and
executing our strategy. As a result of these efforts, we
achieved growth across our businesses.
n We achieved record assets under management,
ending 2010 at $319 billion.
n Our 2010 operating earnings of $845 million were
up 15 percent over 2009.
n Book value per share* finished the year
at a record $27.82.
And today, The Principal is at its strongest financial
position in its history.
Given the challenges of the past few years, were proud
of these accomplishments. And we are clearly well
positioned for additional success going forward.
Think again: Strategy
At The Principal, our strategy remains unchanged,
and our opportunities are growing.
The three core trends that have driven our strategy over
the last decade are stronger than ever. And they all
relate to one thing: an era of personal responsibility.
Think again about the impact of the following three trends:
1. Aging populations  Millions of people around the
world are living longer. And they need their savings to last.
2. Global competition for employees  Employers
need top-notch benefit plans to attract and retain the
best employees, yet there are real limits on what they
can spend.
3. Financially-constrained governments  As national
debts grow in countries around the world, so does the
strain on government-provided retirement programs.
As individuals take more ownership of their financial
securitythe basis for their dreamsThe Principal is
there to help.
Think again: Growth opportunities
I am very optimistic not only about 2011, but also about
the next 15 years and beyond. We have a clear view
of where we want to be and how were going to get
there. It all revolves around four pillars of growth.
And everything we do is focused on at least one of them.
First: Solutions for growing businesses. Running a
small business is hard work. Thats why The Principal
has spent the last seven decades focused on making it
easier for business owners to offer employee benefits
through solutions like eBenefits EdgeSM.
Kate, a human resources director for one of our
specialty benefits clients, told us this automated benefits
management platform has cut the time she spends on
benefits administration by 70 percent.
We have the broad mix of employer-sponsored benefits
that business owners and their employees are looking
for. And we can package them any way they want.
In the United States, 6 million small to medium-sized
businesses represent 99.8 percent of total businesses and
65-70 percent of total job growth. Its a vast and active
market. And The Principal is committed to helping these
businesses rebuild and move forward.
Second: The broader financial services opportunity
focused on employees, business owners and individuals.
Phil, owner of Advanced Time Management in
Michigan, had a dream. Rebuild his business that was
hit hard by the recession, so his employees and family
would be financially secure.
Many people have that dream. But Phils was more
immediate because he was suffering from ALS. Prior
to his illness, he had worked with his advisor, or as he
called her, his angel, to build a business transition
plan that would ensure his employees were taken
care of. Phil died in May 2010. But not before he
had the peace of mind of knowing his family, business
and employees were secure.
Today, at the same time individuals are taking on
greater responsibility for their financial futures, financial
services have gotten more complex. People need more
personalized assistance.
Thats where our relationships with growing businesses
and advisors come in handy. In 2010 our employees sat
down with more than 53,000 individuals right in their
workplaces to talk about what savings and protection
solutions they need to rebuild.
Third: The growth of global asset management. Were
already a recognized leader in global asset management,
growing unaffiliated assets under management at a
compound annual growth rate of 30 percent.*
Additional growth will come from emerging markets
and Sovereign Wealth Funds.
Were well-positioned to capture this opportunity.
Today, we have more than 400 investment professionals
around the world and asset management clients in
more than 50 countries. We have a multi-boutique
strategy that enables us to provide diverse investment
capabilities through our network of specialized
affiliates. Were managing assets for 10 of the 25
largest pension funds in the world, including the Social
Security Fund of China.
Fourth: Global mutual funds and retirement. There is
a massive demographic change happening. Each minute,
40 new families enter the middle class in emerging
countries like China, India and Brazil. They, too, dream
of financial security.
How do we know? Because we have been in these
countries and seven others for more than a decade.
We speak the language, know the culture and
understand their dreams.
Working with some of the best local partners in these
countries, we export our retirement saving and asset
management expertise to the places that need it most.
Think again: Business mix
Ours is a hybrid business model with purposeful
diversification. What does that mean for investors? It
means we have the right mix of asset-based and riskbased
businesses combined with best-in-class product
solutions that are meaningful to our clients and advisors.
This leads to:
n Above-market growth
n Higher return on equity
n Greater free cash flow
We remain disciplined in managing expenses and
investing in the highest growth areas. As a result, we
made the difficult decision to exit the medical insurance
business in September 2010. This action will enable us
to invest in other areas of the company that have greater
growth potential. I am proud (but not at all surprised)
of how employees reacted to this decisionand how
committed theyve been to their clients and advisors
during the transition.
Think again. dream again.
Everyone has a dream. The Principal believes that with
the right financial edge, any dream can be made possible.
Weve been helping our clients with their financial
goals and dreams for more than 130 years. 2011 will
be no different.
Visit us at www.principal.com/dreamagain.
We look forward to seeing you there.
Its time to dream again. With The Principal.
On behalf of everyone at the Principal Financial Group,
thank you for your continued investment in us. And
please be sure to cast your vote in time for the Annual
Meeting of Shareholders, May 17, 2011.
Larry Zimpleman
Chairman, President and Chief Executive Officer