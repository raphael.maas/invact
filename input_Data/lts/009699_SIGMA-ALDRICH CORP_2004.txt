The future of Life Science and High
Technology determines both the improvement
in the quality of life for mankind and the
destiny of Sigma-Aldrich. As the world leader
in supplying research chemicals and a major
player in fine chemicals, there are tremendous
opportunities ahead for our Company.
2004
This was a year where we again faced
very tough market conditions. Our results
were much better than 2003, due in part
to currency benefits.
We saw modest real growth in sales after
taking currency benefits into account. The
weaker U.S. dollar helped boost our underlying
performance to a reported sales gain of 8.6%
with sales exceeding $1.4 billion. Currency rate
changes provided 5.3 percentage points of the
reported sales growth. Net income was $233
million, with diluted net income per share up by
24.6% to $3.34, with strong contributions from
currency and a much lower tax rate in 2004.
Our three business units (% total sales),
Scientific Research (60%), Biotechnology
(>20%) and Fine Chemicals (<20%) all
contributed to the sales increase, with
reported growth of 9% for Scientific
Research, 6% for Biotechnology and 12%
for Fine Chemicals. They were impacted by
soft academic and pharmaceutical markets.
Geographically (% sales) we saw performance
improve in the U.S. (40%) from 2003.
Currency adjusted growth in Europe (40%)
and International (20%) combined was
largely consistent with the prior year increase.
Our star performers were in the Far East and
South America.
Pretax profits grew faster than sales,
as costs for new sales and marketing
initiatives were more than offset by yet
another outstanding performance in
process improvement.
During the year, we continued to focus
on our balance sheet, managing working
capital very tightly. This, and our higher
profit level, improved return on equity,
which again exceeded our 20% goal.
The ultimate measure and recognition of
a companys performance is its stock price.
Again, our stock price ended the year above
the previous year. Since the current management
team took over five years ago, weve
provided a 15% compound annual return to
shareholders, outperforming the declines in
all major market indices. Our aim remains to
Thrill Our Shareholders.
2004 also saw the implementation of our
important quality initiative and maintenance
of our tremendous safety record.
Overall these results were, of course, the
outcome of the efforts of our dedicated
employees worldwide.
Growth
The key to the ongoing success of our
Company is internal growth. This entails
continuing to build on our present strong
position in the markets we serve. Today we are
the worlds largest supplier of research biochemicals,
organic chemicals and kits. Were
also a major supplier of fine chemicals and
have moved into the top 10 worldwide with
our most recent acquisition of JRH Biosciences.
Overall, we offer more chemical products
(90,000) and have unmatched manufacturing
capabilities, producing more chemicals (40,000)
than any other company. We also supply
30,000 equipment products. This breadth is a
tremendous competitive advantage as its
almost impossible for any company to replicate
our unrivaled scientific knowledge:
Scientific Research
The worlds leading supplier of Life Science
and High Technology research chemicals.
Biotechnology
The strongest collection
of biotechnology capabilities.
Fine Chemicals
The broadest organic chemical and
biochemical technology base.
Furthermore, were the most profitable
company in our market sectors and have a
very strong balance sheet, providing the
muscle to make ongoing investments
and acquisitions.
In everything we do, our aim is simply to
Delight Our Customers, with the broadest
range of highest quality products, supported
by the most extensive library of scientific
information and backed by the best service.
This fundamental approach is encompassed
in our key strategic initiatives:
Leadership in Life Science
and High Technology
New research products are our future
lifeblood. And overall we launched over
5,000 new biochemical and organic products
in 2004. A major thrust continues to be in
Biotechnology. Here we are establishing a
significant presence in cell signaling,
genomics and proteomics, the key to all
forms of life. Our Fine Chemicals business
again landed major orders, reflecting the
capabilities and quality of our production
plants. We also increased investment in
intellectual property, another key to growth,
with the efforts of our strengthened
Business Development group. At the same
time, weve been stepping up our activity
level in R&D!
On the marketing side, we focused
even more clearly on our areas of product
excellence. And increased feet on the
street made our sales organization more
effective. We expect all these investments to
pay off again in 2005.
Obsession With Service
We continually strive to give the best
customer service worldwide. Sigma-Aldrich
is truly multinational, with over 60% of our
business in 2004 outside the U.S. Having
our own presence with over 6,100 dedicated
employees in 35 countries is a tremendous
service resource. And this support is much
appreciated by our customers who, in many
independent surveys, again rank us at the
top overall in servicing them.
To maintain and improve service, we
continued to make major investments. In
2004, we spent $70 million to upgrade
facilities in several locations and improve
our SAP computer software system. A
major investment of $30 million was for
our new Milwaukee administration, packaging,
warehousing, R&D and production facility
scheduled for completion in mid-2005. The
total cost will be $72 million with previous
state and local funding providing for about
half the outlay.
During 2004, we smoothly implemented
our computer software system SAP at
additional international locations. It is fully
integrated with our Web site  sigmaaldrich.
com, where we continue to make
major improvements. The $272 million in
internet-based sales in 2004 were well
above the previous years $207 million
level. And we expect further rapid growth
due to the ease of ordering, an increasing
wealth of informational content and faster
search capabilities. Our award-winning site
is certainly a competitive advantage.
Passion For Process
Improvement
A worldwide commitment to process
improvement has become an integral part
of our culture during the past 5 years.
Process improvement plays an essential role
in reducing costs while at the same time
eliminating quality and service problems and
increasing demand by adding new products
and services of high value.
2004 was another great year with savings
exceeding $15 million, which were key to
maintaining our margins! Based on the
profitability of the Company, our employees
understand that $1 saved is equivalent to
almost $5 of sales.
Acquisitions
2004 was the most active period for
acquisitions in the history of our Company.
So, you may well ask, has anything changed
in our approach? The answer is a definite
NO! In fact, over the last couple of years,
we reviewed many candidates, but sellers
expectations were always well above market
values, making it difficult for us to achieve
the desired return. However, during 2004
we were able to acquire some very exciting
businesses that met our stringent investment
criteria. In the second quarter of the year,
we purchased two companies, Ultrafine
(Manchester, U.K.), a leading supplier of
contract manufacturing service for drug
development, and Tetrionics (Madison,
Wisconsin, U.S.), a producer of high potency
and cytotoxic active pharmaceutical ingredients,
with combined annual sales of about
$30 million. These acquisitions considerably
strengthen the position of our Fine Chemical
business in the pharmaceutical sector.
We also made a strategic move in February
2005 by acquiring JRH Biosciences based in
Lenexa, Kansas, U.S., a major industrial supplier
of cell culture products for the pharmaceutical
and biotechnology industry, for $370 million
 our largest deal ever. Combined with our
own business in this sector, we will become
the #2 player in the fast-growing cell
culture market.
Finally, in February 2005, we also
announced the acquisition of the Proligo
Group, a global supplier of key genomics
research tools. Together with our own business,
we are now the clear market leader for
such genomic products. And, combined
with our new joint project with the Broad
Institute, Cambridge, Massachusetts, U.S.,
for the production of viruses capable of
knocking down gene functionality, we will
have the opportunity to become a leading
player in the explosive growth sector of
gene silencing. We also made many other
smaller investments that strengthen our
position in Life Science and High Technology.
Leveraging Economic Drivers
Life Science and High Technology are
among the key economic drivers for both
countries and companies worldwide.
Therefore, we expect expenditures for
chemical research and the use of specialty
chemicals to continue to increase. Life
Science, in particular, stands on the threshold
of great discoveries. The understanding
of genetics will revolutionize the diagnosis
and treatment of diseases and play a
major role in feeding the worlds growing
population. And industry will also continue
to demand High Technology chemical
products in the 21st century as the pace
of development accelerates. Our challenge
is simply to take advantage of all these
tremendous opportunities. We believe
we are uniquely positioned, being a very
financially sound Company with unrivaled
scientific knowledge.
The key to our future success is, of course,
based on the efforts and commitment of our
dedicated employees. During 2004 our senior
management structure was strengthened
with the promotion of Jai Nagarkatti to
President and Chief Operating Officer of
Sigma-Aldrich Corporation. All our employees
aim to Unleash Our Talents in making
Sigma-Aldrich an even greater company.
The Board of Directors wishes to express
appreciation to their retiring colleague, Jerry
Sandweiss, who has served the Company as
a Director since its formation in 1975. His
wise counsel will be sorely missed.
Finally, we wish to thank our customers,
our employees and you, our shareholders,
for your support and continued confidence
in Sigma-Aldrich. The future of Life Science
and High Technology bodes well for
Sigma-Aldrich.

Jai Nagarkatti
President and Chief Operating Officer

David R. Harvey
Chairman and Chief Executive Officer