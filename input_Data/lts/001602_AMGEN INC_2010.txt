Dear Stockholders,
2010 was a year of challenges met and promises delivered for Amgens
patients, stockholders, and staff. We delivered an important, innovative bone
health therapy, denosumab, in the form of two novel medicines: Prolia
,
approved in Europe, the United States, and several other countries for
postmenopausal osteoporosis, and XGEVA
, approved in the United States
for the prevention of skeletal-related events in patients with bone metastases
from solid tumors. We grew revenues 3 percent, adjusted earnings per
share* 6 percent, and generated nearly $6 billion in operating cash fiow,
despite continuing global economic turmoil and a $200 million adverse
impact to revenues from U.S. healthcare reform and reimbursement
headwinds. In sum, it was a superb operational year.
Developing Denosumab: Amgen at Its Best
The story of the development of denosumab is an excellent example of
Amgen at its best. After 15 years of extraordinary effort from molecule to
medicine, nearly $1.5 billion of investment, millions of staff hours, and
complex clinical trials with thousands of patients around the world, Amgen
delivered two medicines for patients that are seen by many as one of the
biggest therapeutic opportunities in our industry. We could not be prouder
of our ability to serve patients with these Amgen-discovered, -developed,
and -delivered therapeutics.
For their outstanding contribution and unfailing dedication to the innovative 
discovery and development of denosumab, we recognized two senior 
scientists, David Lacey and Scott Simonet, with our most prestigious internal 
scientific award, the George B. Rathmann Award for Scientific Excellence 
(see article at left). 
Other 2010 Accomplishments 
We advanced our pipeline significantly in 2010 with 13 international approvals, 
37 marketing applications filed, four new molecules in clinical trials, and eight 
new cross-functional product teams formed that have responsibility for the 
global design and implementation of a new products development and 
commercialization strategy. We garnered a number of prestigious scientific 
awards (see Awards and Recognition box on page 3). 
We continued to expand globally, providing our vital medicines for more 
patients around the world. We maintained very high compliance standards 
and performance in an increasingly challenging environment, and ended 
the year with strong momentum and a powerful base from which to grow 
revenues and earnings again.
Leadership Changes
During the year, our management team was strengthened by naming 
Bob Bradway as our president and chief operating officer and Jon Peacock 
as our new executive vice president and chief financial officer. George Morrow, 
executive vice president, began his transition to retirement from Amgen. 
George is one of the most respected executives in our industry, and his 
contributions to building the modern Amgen were immense. We look 
forward to his continued consultation and friendship.
Delivering Growth in 2011
Delivering financially, delivering the best pipeline, and capturing growth 
opportunities are the goals we have set for 2011. Beyond the successful 
launches of Prolia
 (denosumab) and XGEVA 
(denosumab)our most 
important growth drivers this yearwe will sustain and enhance the value of our 
existing $15 billion business. We will focus on growing our existing products, 
such as Neulasta 
(pegfilgrastim), Sensipar 
(cinacalcet), Nplate 
(romiplostim), 
and Vectibix 
(panitumumab), and invest in them to capture the opportunities. 
In 2011, we will grow our investment in R&D with additional funding in 
discovery research, as well as our pivotal phase 3 trials, such as AMG 386 
for the treatment of advanced ovarian cancer and ganitumab (previously 
known as AMG 479), used in combination with existing therapeutics for 
pancreatic cancer. We are excited about the potential of our pipeline in 
a number of therapeutic areas, including oncology, infiammation, bone 
disease, and cardiovascular disease. 
We also expect to achieve important regulatory goals this year. For example, 
we reported landmark clinical trial data for XGEVA
 in December, showing 
significant improvement in bone-metastasis-free survival in men with prostate 
cancer, thereby adding to the potential of XGEVA
 to benefit patients.
We expect to ?le for approval of a new indication in prostate cancer in
the United States in the first half of 2011.
Innovation from Inside and Out to Drive Growth
We will also use our considerable financial strength to acquire or license
innovative molecules that address important unmet medical needs. The
BioVex acquisition, which we announced in January 2011 and expect
to close in the first quarter of 2011, supports this strategy by further
strengthening our late-stage oncology pipeline. BioVexs lead product
candidate, OncoVEX
GM-CSF
, is a novel oncolytic virus in phase 3 testing that
has demonstrated encouraging antitumor activity in clinical studies for the
treatment of metastatic melanoma and head and neck cancer. We are very
impressed with the science behind this innovative therapeutic and believe
it holds substantial promise to help patients with malignant disease.
With a focus on growth in 2011, we are accelerating our entry into
faster-growing markets such as Brazil, Turkey, Mexico, and Russia. In
2010, we conducted business in 50 countries, and our aim is to operate in
75 countries by 2015. This means many more patients could access our
medicines, and we could add meaningful incremental revenue by that time.
On Course with Our Mission to Serve Patients
One of our greatest rewards is helping patients. So when we learned
through media interviews that professional golfer Phil Mickelson is using
Enbrel
 (etanercept) to treat his psoriatic arthritis, we were gratified he felt
it was making a positive difference in his life. Having discovered that Phil
is on ENBREL, we are now partnering with him to help people learn more
about psoriatic arthritis and other infiammatory diseases as well as the
treatments available for them.
Our mission to serve patients and our aspiration to be the best human
therapeutics company are what drive and sustain us. Continuing to deliver
against this mission and aspiration by growing our company, products, and
pipeline is the best way we know to serve society, stockholders, and staff.
In 2011, we have the energy, resources, and opportunities to deliver, and
we are very optimistic about our prospects.
