TO OUR SHAREHOLDERS:

As 2013 came to a close, I reflected on our bank. We work hard to take care of our customers,
strive to deliver value to our shareholders, and treat employees with the dignity and respect
they deserve. By providing a higher level of banking, we raise the expectations of what
a bank can be.
We understand that some of our customers work hard to build their dreams while
others have already worked a lifetime to achieve them. Yet all of our customers demand
one thing: the same unwavering commitment to excellence that defines their own lives.
This is something we have understood for 164 years as a relationship-focused, Main
Street bank. It is ingrained in our culture.
Through our transparency and our commitment to strong corporate governance and corporate social responsibility,
we strive daily to earn your trust. Thats because integrity and trustworthiness are the cornerstones upon which successful
companies are built.
Our employees exemplify those beliefs as well  both on the job and in the communities where they live and work. Their
commitment to service excellence and the values we as a bank hold dear are the ties that bind us together.
So, with this understanding of who and what we are as a bank, I am pleased to share with you our 2013 results,
as well as my thoughts on how I believe we are prepared for 2014 and beyond.

We reported 2013 net income of $541 million, or $2.85
per share, compared to $521 million, or $2.67 per share,
in 2012. The 7 percent increase in earnings per share
reflected continued strong credit quality, tight expense
control and customer-driven fee income growth.
Average total loans in 2013 increased $1.1 billion,
or 3 percent, to $44.4 billion, primarily reflecting an
increase of $1.7 billion, or 7 percent, in commercial
loans, partially offset by a decrease of $686 million, or
6 percent, in combined commercial mortgage and real
estate construction loans. The increase in commercial
loans was primarily driven by increases in National
Dealer Services, general Middle Market and Energy,
partially offset by decreases in Mortgage Banker Finance
and Corporate Banking.
Average total deposits in 2013 increased $2.2
billion, or 4 percent, to $51.7 billion, with increases in all
geographic markets, and with increases of $1.4 billion,
or 7 percent, in noninterest-bearing deposits and $803
million, or 3 percent, in interest-bearing deposits.
Net interest income of $1.7 billion decreased by
$56 million, or 3 percent, and noninterest income of $826
million increased $8 million, or 1 percent. The increase in
noninterest income reflected an increase of $13 million in
customer-driven fee income, partially offset by a decrease
of $5 million in noncustomer-driven categories

Credit quality remained strong in 2013. As a result,
the provision for credit losses declined $33 million to $46
million in 2013. Net credit-related charge-offs decreased
$97 million to $73 million.
We continued to carefully manage expenses in 2013.
Noninterest expenses decreased $35 million, or 2 percent
in 2013, compared to a year earlier.
Our capital position continues to be strong. On
January 22, 2013, and January 21, 2014, the board of
directors increased the quarterly cash dividend for common
stock 13 percent and 12 percent, respectively, to 17 cents
and then 19 cents per share. The dividend increases reflect
our strong capital position and solid financial performance.
We repurchased 7.4 million shares in 2013 under our
share repurchase program. Combined with dividends, we
returned 76 percent of 2013 net income to shareholders.
In January 2014, we filed our capital plan with the
Federal Reserve, our first as a full CCAR bank. CCAR
stands for the Comprehensive Capital Analysis and Review,
an annual exercise for bank holding companies with $50
billion or more of total assets. The Federal Reserve will
release its summary results in March 2014.
With respect to stock performance, the market value
of our stock increased 57 percent in 2013, outperforming
both the S&P 500 Index and the Keefe Bank Index, which
were up 30 percent and 35 percent, respectively.
The book value per share of Comerica stock increased 6
percent in 2013, compared to 2012, and was up 13 percent
from 2011.
At year-end 2013, we had $45.5 billion in total loans
and $53.3 billion in total deposits. We also ended the year
with $65.2 billion in total assets, and with 136 banking
centers in Texas, 105 in California, 214 in Michigan, 18 in
Arizona, and nine in Florida.
Regional banks such as Comerica are in that sweet
spot: big enough to be able to get things done efficiently
and still offer a wide array of products and services, and
small enough to be able to deliver personalized care to
customers and react quickly to changing market conditions.
As a country, in 2013 we were still working our way
out of the previous recession. We have been in a prolonged
low-rate environment, with slow job growth, recovering
housing and auto industries, and with resilient consumer
spending. Business optimism remained impaired in 2013,
due to economic uncertainties. These uncertainties tended
to keep many businesses on the sidelines during the year,
particularly when it came to making long-term investments
and to hiring, as well. Customers in this environment have
been de-leveraging and building cash, while also being
cautiously optimistic.
Within our Business Bank, we continue to position
ourselves as our customers trusted financial advisor as
they navigate the economic environment. We build deep,
enduring relationships with our business customers.
With our in-depth knowledge of our customers and their
industries, we offer solutions that meet their distinct
financial needs.
At December 31, 2013, more than 70 percent of
Business Bank loans are within the Middle Market
category, which we define as companies with revenues
generally between $20 million and $500 million. This is
our bread and butter, as we know how to serve the
credit and non-credit needs of manufacturing and service
companies and many others that make up this middle
market. Within this segment, we have considerable
expertise, with products and services to meet the specific
needs of auto dealers, energy businesses, technology and
life sciences companies, environmental services
companies, entertainment firms, and international
companies, among others. The long tenure and industry
expertise of our colleagues, combined with the right
products and services, allow Comerica to differentiate
itself in Middle Market Banking.
We were the recipients of 16 Greenwich Excellence
Awards in Middle Market Banking in 2013 in such
categories as Overall Satisfaction, Relationship Manager
Capability, Likelihood to Recommend, and International
Service, and within Treasury Management Services, in the
categories of Overall Satisfaction, Accuracy of Operations,
Customer Service, Sales Specialist Performance, and
Product Capabilities.
We continue to be dedicated to driving fee-income
growth by emphasizing the importance of cross selling.
Broader and deeper customer relationships result in more
loyal and profitable customers. We have a wide array of
products that are designed to meet the requirements of
our customers.
We see commercial and government cards as key
growth drivers for payment revenues. According to the
July 2013 edition of the Nilson Report, Comerica is one of
the leading issuers of commercial cards and the
number-one issuer of prepaid cards, the latter fueled by
reloadable debit cards for various state and federal
government benefit programs. As such, we are positioned
to capture increased card volumes.
We recently introduced a new, integrated payables
platform along with other enhancements for our
commercial customers and are already seeing the
benefits of these investments, as business customers are
placing more value on technology capabilities, especially
in payments, analytics, fraud control, and mobile access.
We are leveraging technology to provide the products our
customers desire, while helping them to grow their
bottom line.

The Retail Bank is somewhat unique, as we do not employ
a mass-market retail strategy. Rather, our banking
centers focus on the segments we serve exceedingly well:
middle market and small businesses, entrepreneurs,
affluent individuals, and retail customers. Our banking
centers are more geared to sales and service, as opposed
to transaction processing. Thats because of our strong
relationship focus.
In addition to banking centers, Retail Bank delivery
channels also include ATMs, as well as Web and mobile
banking. With Comerica Web Banking, customers can
access Comericas secure Internet banking services
anywhere they have Internet access. With Comerica
Mobile Banking, customers can access their accounts
anywhere, anytime  right from their mobile phone.
In August 2013, Comerica Bank introduced simplified
checking account guides to make it easier for consumers
to compare personal checking accounts at Comerica
Bank with other banks.
We have been increasing our Small Business
customer relationships over the past several years and
expect these relationships will continue to grow as the
economy improves. Small businesses turn to Comerica
because we understand them and are with them through
all phases of an economic cycle.
We were the recipients of seven Greenwich
Excellence Awards for Small Business Banking in 2013 in
such categories as Financial Stability and Relationship
Manager Capability, and within Treasury Management
Services, in the categories of Overall Satisfaction,
Customer Service, and Accuracy of Operations.
Wealth Management provides us with the ability to
leverage Comericas existing customer base by bringing
investment management and fiduciary solutions to our
Business Bank and Retail Bank customers. Our target
customers include business owners, corporate executives,
multi-generational wealth, and institutions such as
municipalities, hospitals, foundations and corporations.
Specifically, within Wealth Management we target
individuals with more than $1 million, and institutional
investors with over $10 million in assets.
Within Wealth Management is our Professional Trust
Alliance, which we established almost 20 years ago. We
have agreements with third-party broker-dealers to provide
trust administration, custodial services, and investment
monitoring for their clients. This business has been a
significant contributor to increasing our fiduciary income.
We have more than doubled the assets under management
related to these alliances over the past four years. We have
18 offices throughout the nation dedicated to serving and
building this business.

We primarily cover the major metropolitan areas in the three
states  Texas, California, and Michigan  where we have
abundant opportunities to pursue the types of businesses
we serve. These metropolitan areas are some of the largest,
fastest growing in the country. Our footprint places us
in seven of the 10 largest cities in the U.S., namely Los
Angeles, Houston, Phoenix, San Antonio, San Diego, Dallas,
and San Jose, as well as in other large cities such as Austin,
San Francisco, Fort Worth, and Detroit.
Not only are Texas and California the largest
economies in the nation based on GDP, but they are among
the fastest growing, as well.
Comerica has had a presence in Texas for 25 years.
As you know, we relocated our corporate headquarters to
Texas in 2007, with the strategy of leveraging our position
as the largest U.S. commercial bank headquartered in the
state. In 2011, we strengthened our competitive position
with the acquisition of Sterling Bancshares, Inc., which
significantly increased our presence in Houston, while
also adding the San Antonio and Kerrville regions. Our
strategy in Texas is working, despite the highly competitive
environment. Period-end loans and deposits in Texas in
2013 were up 1 percent and 10 percent, respectively,
from 2012.
California is a state where we have operated for
nearly 30 years. It serves as the headquarters of our
Technology & Life Sciences and Entertainment businesses.
And we have more relationships with auto dealers in
California than in any other state in the nation. In 2013,
California period-end loans were up 7 percent while
deposits were down 4 percent. The decline in deposits
was primarily in our business that serves title and escrow
companies whose balances fluctuate with mortgage
volume.
We have been serving the Michigan market since
our banks founding in 1849. Our focus on Michigan is on
maintaining our leadership position in the state, where
we have strengthened our market share to nearly 15
percent, ranking us No.2, based on the latest FDIC deposit
market share survey. In 2013, Michigan period-end loans
were down 1 percent as commercial mortgages continued
to amortize, while deposits were up 2 percent.

Comerica has an unwavering commitment to the
communities it serves. Comericas longevity  164 years
 is a testament to its strong relationship focus,
conservative principles, and people  the nearly 9,000
colleagues who serve as our ambassadors in the
community.
In 2013, Comerica contributed more than $8 million
to not-for-profit organizations within our markets, and our
employees raised more than $2.1 million for the United
Way and Black United Fund. Our employees also donated
their personal time, more than 72,000 hours, as a
demonstration of their commitment to the communities
we serve.
In Texas, we continued to build on the success of
our community outreach and educational effort, which we
call Shred Day DFW. In 2013, we shredded and recycled
more than 122 tons of paper documents and, in the
process, helped collect more than 7,000 pounds of food
for the North Texas Food Bank.
And in order to help our Houston neighbors better
protect themselves from identity theft, we hosted our first
annual Shred Day Houston this past April. That event
resulted in the collection, destruction, and recycling of
more than 51 tons of paper and the donation of nearly
7,300 pounds of food for the Houston Food Bank.
We continue to build grassroots support in
Michigans entrepreneurial community through our
sponsorship of the Comerica Hatch Detroit Contest. This
is the second year weve provided the $50,000 prize for
the winning idea for a new retail business in Detroit.
Detroits Cass Tech High School baseball and
softball teams and the Holland High School softball team
each received $10,000 to improve their programs as the
2013 winners of the Comerica Grand Slam Grant. This
annual grant competition attracts entrants across the
state and builds goodwill in the communities we serve.
Community goodwill was also generated through
our Home of the Brave National Anthem Facebook contest,
which enabled Detroit firefighter Martin Rucker to receive
a standing ovation at Comerica Park for his performance
of the National Anthem and a $10,000 grant to the Detroit
Firefighters Benevolent Fund.


In Northern California, the Hartnell College
Foundation in Salinas, California, recognized Comerica
Bank as a Distinguished Honoree for support of its
Womens Education Leadership Initiative.
In Southern California, our Retail team received
recognition from the Los Angeles Board of Education for
its continued support of LAUSDs homeless students
assistance program. Since 2010, our 31 Los Angeles-area
banking centers have collected and donated more than
110,000 school supplies, including new backpacks,
pencils, pens, and soap, all given by bank customers and
employees, to the more than 15,000 students who lack a
fixed, regular, and adequate residence within its school
districts boundaries.
Comerica Bank donated over 60 iPad minis to
low-income students in northern and southern California
in 2013, including at Aeolian Elementary School, to
increase hands-on learning opportunities in reading and
math. The others were presented to deserving high school
seniors, most of whom are the first generation in their
families to attend college.
Comerica continued to be recognized for its
commitment to diversity. The bank earned a rating of 90
percent on the Human Rights Campaign (HRC) 2014
Corporate Equality Index (CEI). And, for the fifth
consecutive year, Comerica Bank has been named by
LATINA Style Magazine as being among the 50 Best
Companies for Latinas to Work for in the U.S. Comerica
Bank also has been ranked #3 among the Diversity, Inc.s
Top 10 Regional Companies for Diversity in 2013.
Hispanic Business ranked Comerica Bank #1 among the
Top 25 Companies for Supplier Diversity in 2013. And
Comerica has been named to LATINO Magazines 2013
LATINO 100 list, the first annual listing of the top 100
companies providing the most opportunities for Latinos
in such areas as education, hiring, workforce diversity,
minority business development, governance, and
philanthropy.
Comerica continues to maintain a leadership
position in sustainability in our industry. In 2013,
Comerica achieved a position in the Carbon Disclosure
Project (CDP) Performance Leadership Index, recognizing
our ongoing progress in reducing our emissions and
reducing resource consumption. Our CDP score of 94 of
100 was the highest score ever attained by Comerica,
placing us among the top performers in the S&P 500 and
reflecting our ongoing commitment to creating long-term,
sustainable value to our stakeholders. In addition to our
ongoing listing on the FTSE4Good Index, Comerica was
included in the Thomson Reuters Corporate Responsibility
Indices (CRI) in both the Environmental and Governance
categories. Regarding our ongoing efforts to green our
supply chain, we were recognized by Office Depot with its
Leadership in Greener Purchasing award for the second
time in three years.
In closing, we believe we are prepared for the future.
In 2013, our asset-sensitive balance sheet remained
well positioned for rising interest rates. We are
allocating resources to our faster-growing markets
and industries where we have expertise. And we
believe our geographic footprint is well situated to
contribute to our long-term growth.
We have demonstrated through the cycle that we
can carefully manage expenses, successfully offsetting
the headwinds from higher regulatory costs. We believe
we have the capacity to grow without adding significant
expense as a result of the efficiency advances we have
made and will continue to make.
Finally, our conservative, consistent approach
to banking, including credit management, investment
strategy, and capital position, has earned the confidence
of our customers and investors and positioned us for
growth.
LOOKING AHEAD
Sincerely,
RALPH W. BABB JR.

COMMITMENT TO COMMUNITY,
DIVERSIT Y AND SUSTAINABILITY

FOCUS ON GROWTH MARKETS

RETAIL BANK AND
WEALTH MANAGEMENT

THE BUSINESS BANK

2013 FINANCIAL RESULTS