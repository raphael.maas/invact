Nothing says more about VF  who we are and what we
stand for  than our brands. Thats why this report opens
with our portfolio of brands, some of the best known in
the world.
World events and a recessionary economy have challenged us in ways
that were impossible to predict. However, by doing what we do best 
ensuring that our brands consistently meet the expectations of consumers
and retailers  we will weather these challenges successfully, just as we
have for the past 102 years. History has shown that a diverse portfolio of
powerful brands is the best defense in turbulent times. With names like
Wrangler, Lee, The North Face, JanSport , Eastpak , Vanity Fair , Lily of
France and Healthtex , VF is well armed indeed and, given our strong
balance sheet and cash flow, we are in an excellent position to further
strengthen our portfolio of brands through acquisitions. We will continue
to seek to add brands and companies in high return market segments that
complement our core businesses and enable us to reach new consumers
and international markets.
Our foundation of leading brands is backed by other formidable strengths.
First, our motivated and experienced people, all united by a common
vision and drive to excel, are focused on building the best, most responsive
apparel company in the world. Next, our financial position is
extremely strong. Weve reduced costs, our inventories are in excellent
shape and our cash flow is healthy. Finally, weve added new tools and
technology that enable us to react faster to changing trends, better serve
our customers and more effectively control our inventories.
2001 REVIEW
To say that 2001 was a difficult year would be an understatement.
A progressive worsening of the economy, slipping consumer confidence
and a slowdown in apparel spending  due in part to the terrorist attacks
on our country and their aftermath  rendered an already challenging
environment more so.
In 2001, sales were $5.5 billion, down 4% from 2000 levels, reflecting
generally weak retail sales of apparel. Excluding unusual items, earnings
per share were $2.68, compared with $2.92 in 2000. Actions taken in the
fourth quarter to reduce costs and improve our future profitability
necessitated a charge to 2001 earnings of $236.8 million, equal to $1.53
per share. Reflecting this charge, we reported earnings per share of $1.19
in 2001.
Nevertheless, despite the challenging environment, we accomplished
what we set out to do in 2001. We wanted to improve profitability in our
workwear business; efficiently integrate the acquisitions we made in
2000  namely, The North Face, Eastpak, H.I.S, Chic and Gitano brands; reap
the benefits of cost reduction initiatives; and continue our share repurchase
program. We also set out to reduce inventories by $100 million by
year-end  an ambitious undertaking, given the difficult sales environment.
Looking specifically at these key areas, I am pleased to report that we
made good progress in each. We substantially improved the profitability
of our workwear business, with margins well above 2000 levels. The
outdoor and jeans brands acquired in 2000 were successfully integrated
and added $.09 to earnings per share in 2001. We also reduced inventories
by more than $200 million in 2001. This contributed to cash flow
from operations of $685 million, the second highest level achieved in our
Companys history. Finally, we repurchased four million shares during the
year. In October, our Board of Directors authorized the repurchase of an
additional 10 million shares. We plan to continue to repurchase our
shares this year as well.
Despite great volatility in global equity markets during 2001, VFs share
price rose 8%, compared with a 13% decline in the S&P 500 index. In
addition, 2001 marked the 29th consecutive year of increased dividend
payments to our shareholders.
CHANGING WITH THE TIMES
There is no doubt that our industry is undergoing fundamental changes.
The weak economy has created excess retail and manufacturing capacity.
Apparel prices have continued to drop, putting pressure on suppliers to
reduce costs. Consumers are demanding more and more value in the
products they purchase  and theyre getting it.
Against this backdrop, VF has undertaken a strategic repositioning to
address current market dynamics and improve our return on capital 
a key driver of shareholder value. Specifically, we took action to exit
underperforming businesses, including our private label knitwear business,
our Jantzen swimwear business and a small specialty workwear
business, which in total accounted for approximately $305 million in
sales. We also reduced our Companys cost structure and adjusted
capacity. By closing certain plants and distribution centers, reducing our
workforce and moving to lower cost manufacturing locations, we expect
to reduce costs by approximately $100 million this year and $130 million
annually thereafter.
At the same time, we remain committed to fueling growth in those
businesses that enjoy attractive returns, including our jeanswear,
intimate apparel and outdoor brands. We plan to increase our brand
investment in these areas in 2002.
THE RIGHT TOOLS IN PLACE
VF is committed to doing what it takes to maintain the strength and vitality
of our Company. Most importantly, our people are skilled at managing
change and have consistently turned challenges into opportunities.
First and foremost, we remain dedicated to keeping our brands relevant
to consumers. Styles and fashion trends may change, but our unrelenting
focus on understanding and meeting consumer needs will not. Each of
our brands uses in-depth consumer knowledge to maintain a continuous
flow of innovative products and fresh marketing ideas. From The North
Faces MET5 jacket to Vanity Fairs Illumination bra to Playwears Kidproof
clothing, our brands continue to deliver outstanding products at great prices.
With jeans, bras, outerwear, shirts, uniforms and backpacks, VFs product
mix is one of the most diverse in the industry. Effectively delivering these
products to a wide variety of customers and retail locations around the
world requires exceptional supply chain management. We are forging
ahead with the continued development of the most flexible, cost efficient
and responsive supply chain in the industry. Five years ago, the percentage
of our products sourced or manufactured offshore and sold in the
U.S. was 57%. At the end of this year, that percentage should be approximately
85%. This shift in manufacturing is allowing us to build more
value into our products for consumers.
New technology and advanced business processes, which we have long
viewed as competitive advantages, are also helping us deliver value at
the lowest possible cost. The ability to use technology to control inventories,
maximize the productivity of products on the retail floor and provide
real time information throughout our supply chain is more critical today
than ever before. But technology itself is not a panacea  its leveraging
effect can only be realized when combined with changes in key
business processes.
For example, weve achieved tremendous success with our Retail Floor
Space Management process, which enables us to determine the optimal
product mix for individual stores, replenish goods rapidly and efficiently, and
keep the right products in stock with minimal inventory. Higher sales on
less inventory is a winning formula for success in todays retail environment.
Over the past several years, weve also invested heavily in new forecasting
and planning tools. They have helped us better align production with
current demand and keep our inventories lean.
FOCUS ON PROFITABILITY AND IMPROVED RETURNS
Over the past two years, weve worked steadily to put the pieces in place
for long-term, sustainable growth and improved profitability. Our goal is
to achieve a return on capital of 17%. To achieve this, we have targeted
long-term sales growth of 6% annually and operating margins of 14%. We
also continue to target a debt to capital ratio below 40% and a dividend
payout rate of 30%. Well be active managers of our brand portfolio,
investing in our core brands and searching for new ones. And we will
continue to ensure that our associates have the tools and support they
need to be successful.
Looking at our performance in 2001, I am confident that our portfolio of
powerful brands, motivated people, adaptive technology and advanced
business processes uniquely position us for growth and increased
shareholder value in a dynamic and constantly changing market.

Mackey J.McDonald
Chairman, President
and Chief Executive Officer