TO FELLOW OWNERS AND FRIENDS:
I am pleased to report 2012 was a year of significant accomplishment, and through the dedication of our
more than 11,000 colleagues, we achieved three of the six long-term financial goals that we established in 2010.
We are focused on executing our strategic plan to position Huntington for consistent long-term profitable growth,
demonstrated this past year by the 12% increase in tangible book value per common share to $5.78. Our Fair
Play approach to banking, coupled with our effective Optimal Customer Relationship (OCR) cross-sell process,
allowed us to meaningfully grow the balance sheet, continue to dramatically increase the number of customer
relationships, and positioned us significantly closer to our goal of being THE Bank of the Midwest. We remain
diligent with respect to maintaining our aggregate moderate-to-low risk profile. We continue to invest in our risk
infrastructure, and for the third year in a row, we decreased nonaccrual loans by at least 25%, while finishing the
year with our provision for credit losses at the low end of our long-term expected range.
During 2012, we were able to return more of our capital to shareholders. The full year declared dividend
increased by 60% to $0.16 per common share. Dividends, coupled with $148 million in share repurchases,
equated to the Company paying out just under 50% of its net income available to common shares. This represents
a significant increase from the 12% of net income paid out in 2011. Our capital levels remain healthy, and as we
look forward, our top capital priorities are to grow the core franchise and support the dividend.
The continued improvement in profitability, efficiency, credit, and capital demonstrated how Huntington
colleagues have risen to the challenges of the current banking environment. Expectations for each member of the
Huntington team remain high, as their execution of our long-term strategy is paramount to our future success. Let
me begin by offering a progress report on several key areas of our strategy. I will then provide a recap of 2012
performance and our expectations for 2013.
Strategies Overcoming Economic Headwinds
The banking industry has been severely affected by a struggling economic recovery, the prolonged low level
of interest rates, and a regulatory environment with unprecedented changes and new requirements. Our strategies
uniquely provide opportunities to counter many of these industry headwinds directly.
We continue to see positive trends within our Midwest markets relative to the broader United States.
Nevertheless, broad based customer sentiment began to change in late 2012, due to increased concerns regarding
the U.S. economy. While some businesses are hesitant to invest given the current uncertainty in the economy, we
believe our differentiated approach to banking, combined with investing in our franchise through enhanced
convenience, products and services, will drive growth and improvement of our long-term profitability.
The low interest rate environment, coupled with a relatively flat yield curve experienced for the last several
years, is a direct reflection of the uncertainty and volatility of the national and global economy. It also put
unrelenting pressure on our net interest margin (NIM). We were able to expand our NIM by three basis points
through our focus on our relationship-based sales approach. This approach, coupled with our deeply embedded
risk process, resulted in higher quality and more consistent revenue. An example of this can be seen within our
commercial loan portfolio, where loan yields declined only five basis points from fourth quarter of 2011 to fourth
quarter 2012, as we made a conscious decision to forego some growth due to highly competitive pricing. Equally
important are our funding costs. The 12% growth in consumer households and 9% growth in commercial
relationships experienced in 2012 start with each customers primary checking account. With each new
relationship, we improve the quality of our deposits and the Huntington franchise.
Near historically-low interest rates negatively impacted many parts of our business. However, the
investments we have made over the last two years added over $50 million of pre-tax income during 2012, and
several areas experienced record performance. Our mortgage group had a record year while assisting over 60,000

customers with new residential mortgages and consumer loans. Automobile Finance, where we have an over 50-
year legacy and distinct expertise, originated a record $4 billion of automobile loans and made great progress
with our expansion into the New England and northern Midwest markets. Small businesses are a critical priority
for us. We were once again the No. 1 Small Business Administration lender across our six-state footprint and
ranked Highest in Customer Satisfaction with Small Business Banking according to J.D. Power and Associates.
Our Fair Play banking philosophy implemented in 2010 aligns with the spirit of many of the new
consumer banking regulations. Other banks continue to add fees and reduce convenience, while we believe the
path to long-term relationships with our customers is through a superior level of service built around simple
products that are easy to understand. As shown by being named one of the Best Banks in America 2012 by
MONEY Magazine and the nearly 10% increase in the percentage of both consumer and commercial customers
with 4+ products and services over the past two years, customers agree. Looking forward, we believe these deep
relationships will assist us over the next several years, as we navigate a considerable amount of new regulation
that has yet to be finalized and implemented. Whether it is the final Basel III capital and liquidity standards, new
rules set forth by the Consumer Financial Protection Bureau, or moving from the Federal Reserve Board Capital
Plan Review (CapPR) to its Comprehensive Capital Analysis and Review (CCAR), we will continue to adapt and
drive our financial performance consistent with our long-term strategies.
2012 Performance Recap
Net income increased 18% to the record level of $641.0 million and earnings per common share increased to
$0.71, or 20% growth from the prior year and above our long-term goal of 6%-8%. Profitability, as measured by
our return on average assets (ROA) and return on average tangible common equity, increased to 1.15% and
13.5%, respectively. Since we established our long-term goals in 2010, 2012 was the first year that the ROA was
within our targeted range of 1.10%-1.35%.
Fully-taxable equivalent total revenue increased $204.1 million, or 8%, from 2011 as noninterest income
increased $117.2 million, or 12%, and net interest income increased $86.8 million, or 5%, from the prior year.
The increase in noninterest income was driven by a $107.7 million, or 129%, increase in mortgage banking
income, a $26.2 million, or 82%, increase in gain on sale of loans, an $18.7 million, or 8%, increase in service
charges on deposit accounts, an $11.6 million, or 32%, increase in capital market fees, and an $11.2 million
bargain purchase gain related to the acquisition of Fidelity Bank. These positive impacts were offset partially by
a $29.4 million, or 26%, decrease in electronic banking income, which was negatively impacted by over $55
million from the Durbin amendment, and a $16.0 million, or 11%, decrease in other income, reflecting a $16.5
million, or 62%, decrease in automobile operating lease income. We continue to make progress in shifting the
overall mix of total revenue towards our goal of 40% as noninterest income was 38.8% of total revenue, up from
37.4% in 2011.
Net interest income increased $86.8 million, or 5%, in 2012. This reflected a $2.1 billion, or 4%, increase in
average earning assets and a 3 basis point increase in the NIM to 3.41%. The increase in the NIM reflected the
positive impact of a 29 basis point decline in total deposit costs that were offset partially by a 24 basis point
decline in the yield on earnings assets and a 2 basis point decrease related to non-deposit funding and other items.
Average noninterest bearing deposits increased $3.5 billion, or 41%, and represented 27% of total deposits.
The $2.1 billion, or 4%, increase in average earning assets was driven by the $1.9 billion, or 10%, increase
in average total commercial loans and $0.8 billion increase in average loans held for sale. Those were offset
partially by a $0.6 billion, or 3%, decrease in average consumer loans including a $1.4 billion, or 23%, decrease
in automobile loans, reflecting $2.5 billion of automobile loans sold throughout the year.
For the year, average total core deposits increased $3.1 billion, or 8%, reflecting a $3.8 billion, or 27%,
increase in total demand deposits and a $0.6 billion, or 4%, increase in money market deposits. These were offset
partially by the $1.5 billion, or 19%, decrease in core certificates of deposit. Through our strategic focus on
growing consumer households and commercial relationships by earning their primary checking (demand deposit)
accounts, we continue to improve our overall funding mix.
Noninterest expense increased $107.4 million, or 6%, in 2012. This included a $95.7 million, or 11%,
increase in personnel costs primarily reflecting the addition of 561 full-time equivalent employees, as well as
higher incentive based compensation and a $10.4 million, or 11%, increase in equipment primarily reflecting the
implementation of strategic initiatives, including opening 37, or 6%, net new branches. These increases were
offset partially by a $9.3 million, or 12%, decrease in deposit and other insurance expense. The full year 2012
included $14 million of noninterest expense related to the Fidelity acquisition, which closed on March 30, 2012.
Our efficiency ratio (noninterest expense as a percentage of total revenue) modestly improved to 63.4%, but
remains above our long-term goal of mid 50%s. The investments we have made and the actions we are taking
provide us with a path to an efficiency ratio below 60%. However, for the Company to reach its long-term goal,
we will need to experience a more normal interest rate environment.
Since turning the corner on credit in 2011, our provision for credit losses has been at the low end of our
long-term expectations. In 2012, the provision for credit losses decreased another $26.7 million, or 15%, to
$147.4 million reflecting a $94.6 million, or 22%, decrease in net charge-offs (NCOs) to $342.5 million, or
0.85% of average total loans and leases. Of this years NCOs, $34.6 million related to regulatory guidance
requiring consumer loans discharged under Chapter 7 bankruptcy to be charged down to collateral value.
Approximately 90% continue to make payments as scheduled. Importantly, nonperforming assets declined by
$144.5 million, or 25%, to $445.8 million and were at the lowest level since 2006.
Our tangible common equity ratio improved 46 basis points to 8.76%, and our Tier 1 common risk-based
capital ratio improved 48 basis points to 10.48% from December 31, 2011. For perspective, these are the highest
common equity based capital ratios for the Company in the last 20 years. The regulatory Tier 1 risk-based capital
ratio of 12.02% at December 31, 2012 remained over $2.9 billion above the well-capitalized regulatory
thresholds but was down from 12.11%, at December 31, 2011. This decline reflected capital actions taken
throughout the year, including the redemption of $230 million of trust preferred securities and the repurchase of
23.3 million common shares at an average price of $6.36 per share.
The full detail of our financial performance is discussed in the Management Discussion and Analysis
section found later in the attached SEC Form 10-K. Please take the opportunity to read this. It provides additional
insights and discussion related to our 2012 financial performance.
2013 Expectations
As we have done since early 2010, we will continue executing our core strategy, growing market share and
increasing the number of products cross sold through selective investments in initiatives to grow long-term
profitability. We will remain disciplined in our growth and pricing of loans and deposits and are encouraged by
the three basis points of net interest margin expansion during 2012. We continue to expect credit quality to
improve and net charge-offs should approach the higher end of our normalized levels of 35 to 55 basis points of
total loans by the end of 2013. We will stay focused on increasing customer cross-sell ratios and delivering
positive operating leverage.
Over the course of 2013, net interest income is expected to show modest improvement from the 2012 level.
We anticipate an increase in total loans, excluding the impact of any future loan securitizations, although we
expect growth will be more heavily weighted to the second half of the year. However, those benefits to net
interest income from higher balances are expected mostly to be offset by downward NIM pressure. NIM is not
expected to fall below the mid 3.30%s due to continued deposit repricing and mix shift opportunities, while
maintaining a disciplined approach to loan pricing.

Excluding potential future automobile loan securitizations, we anticipate the increase in total loans will
outpace growth in total deposits modestly. This reflects our continued focus on the overall cost of funds as well
as the continued shift towards low- and no-cost demand deposits and money market deposit accounts.
Noninterest income over the course of the year, excluding the impact of any automobile loan sales, any net
MSR impact, and typical first quarter seasonality, is expected to be relatively stable. The anticipated slowdown
in mortgage banking activity is expected to be offset by continued growth in new customers, increased
contribution from higher cross-sell, and the continued maturation of our previous strategic investments.
Noninterest expense continued to run at levels above our long-term expectations relative to revenue. In
response to changes in our economic outlook, we have moderated the pace and size of our planned investments in
order to drive positive operating leverage again in 2013.
Commitment to Our Shareholders
We are committed to delivering long-term value to our shareholders. At Huntington, we believe that
balancing doing the right thing for our shareholders, colleagues, customers, and communities is the path to
developing a robust franchise that can produce the stable returns needed to create long-term value. Two examples
of that balance are 1) how our $4 billion commitment to small business lending in the Midwest has helped drive
the recovery of the local economy and also assisted Huntington in growing average commercial and industrial
loans by 16% over 2011; and 2) how the investments in our colleague wellness program lead to lower cost of
insurance and improved productivity through lower colleague turnover and higher colleague satisfaction.
The prudent deployment of capital is one of the most fundamental undertakings by the Board and executive
leadership. As I discussed above, we took actions in 2012 to increase the amount of capital returned to
shareholders. As we continue to generate capital, we will be prudent in its allocation. First, we will invest in our
core strategy, albeit at a slower pace than in 2012. Second, we will provide our shareholders a dividend. With the
regulators targeting a dividend payout ratio of up to 30%, as our income grows, then so should our dividend.
Last, we continue to evaluate other capital actions including share repurchases and mergers and acquisitions. As
we have shown over the last several years, we will continue to maintain a high level of discipline when
considering mergers and acquisitions.
Last year, I wrote about how we took steps to align executives interests with long-term shareholders
interests by increasing common stock ownership by requiring that 50% of net shares received by senior
leadership through incentive plans be held until retirement. To further align these interests, in 2012, we
implemented performance share units as 50% of the long-term incentive program for key senior executives, as
well as extended our market leading ownership guidelines and hold until retirement requirements deeper within
the Companys management structure.
I want to thank David Lauer, who is retiring from the Board effective at the annual meeting, for his
dedicated service and invaluable counsel to Huntington. David has been a leader in many critical areas, serving
not only as the chairman of the audit committee, but also as the lead of several other subcommittees over his
tenure. Through his referrals and support of our business initiatives, he has been an extraordinary friend and
ambassador for the Bank across the greater Columbus area. We are grateful for his service and extend our warm
wishes to him.
In addition to the personal pride I have for what the Huntington team has accomplished over the last several
years for our shareholders, I am especially proud of our colleagues for their deep commitment to our
communities. One example is Pelotonia, the annual bike tour that raises funds for the James Cancer Hospital and
Solove Research Institute, which continues to be a rallying point for Huntingtons colleagues. Since the inaugural
event in 2009, we have raised over $6.1 million towards curing cancer, and in 2012, more than 1,300 of our
approximately 11,000 colleagues rode in or volunteered for the event.

Looking back at 2012, it was a very good year as we made significant progress toward all of our long-term
financial targets. We enter 2013 a much stronger Company with a sound strategy and good momentum in a
number of key areas. This will be another challenging year given the continued sluggish economic growth and a
very low and flat interest rate curve. However, we remain confident in our strategies and resolve to continue
delivering strong financial results. We thank you  our shareholders, customers, and colleagues  for your
continued support.
Stephen D. Steinour
Chairman, President and Chief Executive Officer
March 4, 2013
