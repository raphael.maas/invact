Letter to shareholders

The Science of Collaboration
continues to propel our
business success

Collaboration and hard work.
These are the signatures of FMC
Corporation that helped drive our
strong performance in 2012. We
delivered another year of record
sales and earnings, despite the
challenges that faced the
chemical industry.
As we pass the midpoint on our
Vision 2015 journey, Im pleased to
report that we are tracking to meet
or exceed our original targets.
This is due in large part to the
enthusiastic embrace of our vision
by a dedicated global workforce,
supported by world-class
collaboration both internally and
externally. We call this the Science
of Collaboration  a unique culture
that propels our progress.
We concluded 2012 with
impressive results:
 Sales of $3.7 billion were up 11
percent from 2011, with adjusted
operating profit up 16 percent.
 Adjusted earnings per diluted
share grew 16 per cent to $3.48.
 Return on invested capital
was 22.9 percent for the year,
well above our Vision 2015
threshold.
 We delivered a total shareholder
return of 37 per cent, a premium
performance in the chemical
industry.
2012 HIGHLIGHTS
Agricultural Products Group
completed its ninth consecutive
year of record earnings by
strengthening its technology
portfolio and gr owing its global
presence.
Specialty Chemicals Group
had a mixed year , with the
BioPolymer business unit delivering
its eighth consecutive year of
record ear nings while continuing
to build its new natural colors
platform and pectin business.
But the solid performance
in BioPolymer was offset by
operational challenges in the
Lithium business.
Industrial Chemicals Group
increased both revenue and
earnings over 2011, and launched
a new business unit  FMC
Environmental Solutions  that
leverages our technologies in the
environmental remediation and
pollution prevention markets.
Our increasing exposure to
rapidly developing economies is
an important Vision 2015 growth
driver for FMC. In 2012, 49 percent
of our sales wer e tied to these
faster growing regions of the world
compared to 46 per cent in 2011.
In addition to our strong organic
growth, we have focused on

external transactions that add
product lines or technologies,
offer access to new markets
or provide a foundation for
future expansion. In 2012, we
completed seven external growth
transactions across the company.
These included two company
acquisitions, one joint venture, one
global licensing and IP agreement,
and three joint development and
commercialization pacts.
In the midst of this dynamic
year, two of our highly valued
senior executives retired during
the fourth quarter of 2012: Kim
Foster, acknowledged as one of
the best CFOs in the industry;
and Milton Steele, our innovative,
customer-focused president of
FMC Agricultural Products. We are
incredibly grateful for their 34 and
35 years of service, respectively.
FMC will miss their insightful,
inspired leadership, but we are
fortunate to have exceptionally
strong leaders in place who are
building on the high-performance
legacies of their predecessors.
THiNK. SAFE.
The first responsibility of every
leader and employee at FMC is to
achieve an injury-free workplace.
In 2012 we launched our Think.
Safe. initiative to remind everyone
that nothing is more central to our
success and sustainability than
the safety of our people. We made
good progress with fewer injuries in
2012 compared to the year before.
In fact, we recorded our lowest
injury rate in over a decade. Zero
injuries is our ultimate goal, and
we remain diligent in our resolve to
create a robust safety culture.

PROGRESS AS ONE FMC
Vision 2015 is about growth,
which comes not only from strong
business strategies but also from
capturing and leveraging the value
of common ownership. As you will
read in this report, we are working
more effectively as One FMC by
adopting best-in-class strategies
in several areas, including
procurement, capital deployment,
engineering, safety, external growth
and sustainability. We are also
implementing new manufacturing
excellence standards that are
improving operational efficiencies
across all of our businesses.
Collectively, these actions are
helping us build a str onger and
larger company for the future.
We are proud of FMCs record
performance in 2012. Every day ,
5,700 employees ar ound the world
demonstrate their commitment
to delighting customers with the
right chemistry and exceptional
service  fueled by the Science of
Collaboration.
Pierre R. Brondeau
President, Chief Executive Officer
and Chairman of the Board