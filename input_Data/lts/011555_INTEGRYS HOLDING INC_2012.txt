Fellow Shareholders:
You might think were all about energy.
And certainly, energy is the commodity
we provide. But the truth is, were all
about people. Our daily work is an
investment in the livelihood of our
customers, the safety of our employees,
the support of our communities, and
the success of our shareholders. With
so much at stake, we put a priority on
people. Its an investment thats worth
the energy!
As an energy company, were
focused on improving reliability and
reducing risk by making infrastructure
investments that are good for all of our
stakeholders. As a team, we accomplish
a lot.
The Year In Review
At our regulated utilities, we led
the way with a proposal for the first
commercial installation of Regenerative
Activated Coke Technology (ReACT) in
the United States. This new technology
for coal-fired electric power plants
simultaneously reduces multiple
pollutants using only a fraction of
the water that conventional wet
scrubbers demand. Wisconsin Public
Service (WPS) is seeking Public Service
Commission of Wisconsin (PSCW)
approval to design, construct, install,
and operate this new emission
control system on our 325-megawatt
Weston 3 generation facility. We expect
the $250 million ReACT project to be
operational in 2016. Our customers
and the communities we serve will reap
environmental benefits of cleaner air
while this prudent rate base investment
delivers a return for our shareholders.
In parts of WPSs service territory where
there is a higher risk of outages due to
weather related events, were planning
a 5-year System Modernization and
Reliability Project (SMRP). This project
is focused on converting areas of
overhead electric distribution system to
underground where feasible, as well as
upgrading portions of the system with
advanced technology. Weve initiated
the approval process with the PSCW
to convert about 200 miles of electric
distribution line each year, benefitting
about 5,000 customers annually, to
improve electric service reliability. We
expect this $200 million project to be
completed in 2019. Were investing to
ensure our customers continue to have
reliable electric power when and where
they need it.
Weve also announced our intent to
purchase the Fox Energy Center, a
593-megawatt combined-cycle natural
gas-fired plant located just south of
Green Bay, Wisconsin. The $390 million
acquisition gives WPS a more balanced
electric generation portfolio that
includes coal, natural gas, hydroelectric,
wind, and other renewable sources. This
acquisition also enhances supply and
pricing options, allowing us to better

manage our generation assets and the
overall cost of energy for our customers
benefit. And our shareholders will see
a return on this prudent infrastructure
investment.
In Chicago, were upgrading the
Peoples Gas natural gas distribution
system by replacing cast- and
ductile-iron pipe with state-of-the-art
polyethylene technology. During the
first two years of this program, weve
installed nearly 300 miles of new main,
retired 140 miles of cast iron main,
and set more than 42,000 meters for
customers across the city. In 2013 well
continue these prudent efforts, staying
on track to upgrade and modernize
around 2,000 miles of the natural gas
distribution system in Chicago over the
next two decades.
By making these infrastructure
investments, were able to reduce risk
in our distribution systems, increase
efficiency, and bring long-term
environmental and service benefits to
our customers.
Our nonregulated subsidiaries also
saw successes during 2012. Integrys
Energy Services was awarded the right

to serve electric customers in the city of
Chicago under the largest aggregation
program in the nation. This is in
addition to supplying electric service
to 45 other Illinois municipalities.
For more than a year, our employees
worked diligently and collaboratively
to learn from our experiences in other
aggregation efforts to be ready for
this opportunity. As a result, our Illinois
electric aggregation customers are
collectively saving millions of dollars in
energy costs.
At Integrys Transportation Fuels (ITF),
2012 was a year of integration and
growth as we worked with existing
and potential customers to create and
execute development opportunities.
We achieved growth in our targeted
segments of trucking, transit, and refuse
hauling through the construction of
12 new compressed natural gas (CNG)
fueling stations. Five station upgrades
were also completed, and over
$5 million of manufactured equipment
was sold. By year-end, we were
providing operations and maintenance
service for 52 stations nationwide. Our
2012 fuel sales exceeded 35 million
CNG gasoline gallon equivalents
through stations we own or operate.
This includes our sales to joint venture
partners, such as Paper Transport, Inc.
and AMP Americas, along with other
industry-leading customers.
Our joint venture with AMP Americas
is part of the foundation for further
growth of our CNG business. With a
focus on building a network of CNG
fueling stations along the I-65 and
I-75 corridors to support AMP Americas
clients, this has the potential to be
attractive to other fleets that use the
same route.
ITF came together as one team with
a collective direction, conquering
the typical challenges of business
integration. We impressed the market
with CNG fueling station designs,
equipment performance, and operations
support  demonstrating what it means
to be a team with character.
One of our key corporate initiatives
during 2012 was safety. Employees
embraced their work with safety in mind,
reducing the number of injuries, which
in turn provided safety performance
improvements and cost savings. Im
pleased that compared to 2011, we
reduced our injury rate by 29% and our
severity rate by an impressive 30%. This
means that more of our employees are
returning home to their families without
injury every dayand that is our most
important measure of safety.
We also put a strong emphasis on
training. At Peoples Gas, we partnered
with the State of Illinois, City Colleges
of Chicago  Dawson Technical
Institute, the Utility Workers Union of
America, and our own Local 18007 to
form the Gas Sector Utility Workers
Training Program. This successful
program trains potential natural gas
operations employees and targets
veterans and displaced workers.
Students receive training on industry
standards, hands-on instruction,
and general education during a
concentrated six-month program and a
one-month field internship with Peoples
Gas. Three groups of graduates have
provided immediate support to Peoples
Gas and helped bridge resource
needs to support our infrastructure
improvement program. This ongoing
initiative will provide employment
opportunities in Illinois and help
replenish our skilled workforce as we
face retirements in the coming years.
Every year I am heartened to see how
employees from all levels of the company
embrace our corporate citizenship
strategy by demonstrating support for
their communities. I am proud to lead
such dedicated people. You can learn
more in our 2012 Corporate Citizenship
Report titled A Priority on People...Its
Worth The Energy!
Financially, we had a strong year. Our
utilities performed well despite one of
the warmest years on record. At the
same time, our nonregulated business
grew contracted sales volumes in a very
competitive marketplace. Together, we
established the groundwork for future
growth in our utilities with the pending
Fox Energy Center acquisition, progress
on our pipeline replacement program
in Chicago, and investments in system
modernization and environmental
controls. We were also able to take
advantage of favorable market
conditions to refinance debt and lower
our interest costs.
Recognizing the importance of our
dividend to many of our investors, we
maintained our quarterly dividend of
68 cents per share. Weve now paid
a dividend for 72 consecutive years
without any decreases, which places us
among a rare group of companies to
achieve this. We are confident that our
business plan will continue to sustain
this quarterly dividend level going
forward.
Our 2012 total return (reinvested
dividends and capital appreciation)
outperformed the S&P Utilities Index
(1.35% vs. 1.29%). This is further
evidence that execution of our business
plan is on track and delivering results.
Whats Ahead in 2013
The importance of creating long-term
value for our shareholders, customers,
employees, and communities hasnt
changedits a strategic priority for
us as we provide our customers with
the best value in energy and related
services.
Were focused on providing safe,
reliable, and affordable energy to our
customers, while delivering 4% to
6% growth in diluted earnings per
share  adjusted on an average
annualized basis (with 2011 as the base

year) through 2015 for our shareholders.
Were committed to keeping our
values of integrity, innovation, safety,
collaboration, respect for employees,
service to customers, value for owners,
and support for communities at the
forefront of everything we do. And we
know that placing a priority on people is
essential to getting our job doneand
thats why its worth the energy!
The Right Leadership
Last year, Jim Kemerling retired from
the Board after 25 years of outstanding
service. Holly Keller Koeppel was
elected to fill the vacant position, and
Al Budney was selected as lead director.
Larry Borgard, our President and Chief
Operating Officer  Utilities, completed
a year-long term as chairman of the
American Gas Association. Larry shared
his 28 years of diverse utility experience
with others in the industry and
represented the natural gas industry
on significant policy issues in venues
around the world.
Diane Ford, Vice President and
Corporate Controller, and Barth Wolf,
Vice President, Chief Legal Officer and
Secretary, announced their retirement
early in 2012. Their departure last fall,
with a combined 61 years of experience,
left big shoes to fill. But our succession
planning process ensured we had
talent available within, and we tapped
Linda Kallas as Vice President and
Corporate Controller, and Jodi Caro as
Vice President, General Counsel and
Secretary.
Joe OLeary will be retiring in 2013.
We thank Joe for his dedication to
Integrys and for his many contributions,
which have been instrumental in our
success. Other executive team changes
in 2013 include Jim Schott being
appointed Vice President and Chief
Financial Officer, Phil Mikulsky named
Executive Vice President  Corporate
Initiatives and Chief Security Officer,
Mark Radtke named Executive Vice
President  Shared Services and Chief
Strategy Officer, and Bill Laakso named
Vice President  Human Resources and
Corporate Communications.
At Integrys, we want to ensure our
leaders are prepared to make a
difference. Thats why we created our
Leadership Development Program. Since
its inception in 2001, the number of
graduates now exceeds 1,000. Effective
leadership enhances individuals,
teams, and the organization as a whole.
Leadership makes a difference.
Placing a priority on people and giving
them room to grow and do great things
for our customers, the communities we
serve, and our shareholders really is
worth the energy!
Thank you for the continued confidence
youve placed in us.
Sincerely,
Charles A. Schrock
Chairman, President and Chief
Executive Officer
February 28, 2013
A Priority on PeopleIts Worth
The Energy!