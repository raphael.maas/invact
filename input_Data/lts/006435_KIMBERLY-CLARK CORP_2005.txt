Dear Shareholders:
In 2005, we again delivered on our top- and bottom-line commitments in a challenging business
environment. We accomplished this in part through our core strategy of leveraging customer, shopper
and user insights to help bring the best health and hygiene innovations to market. It was also a year
characterized by positive change as we consolidated our North American and European consumer
products businesses to allow us to streamline decision-making, increase our focus on partnering with
customers and introduce new products even more rapidly than before.
Perhaps most importantly, we launched a series of targeted growth investments as part of our
ongoing effort to build competitive advantage and ensure long-term success. Specifically, we are
focused on the following:
Strengthening our leadership position in baby and child care, adult care and family care;
Building on positions of regional strength in feminine care, particularly in the Americas and
parts of Asia;
Accelerating growth in developing and emerging (D&E) markets with an emphasis on BRICIT
(Brazil, Russia, India, China, Indonesia and Turkey) countries;
Extending the K-C Professional (KCP) portfolio in higher-margin segments such as the
workplace, safety and do-it-yourself markets; and
Expanding core Health Care products globally and adding higher-margin products to this
businesss portfolio.
SUPPORTING GROWTH WITH INCREASED R&D AND MARKETING SPENDING, STRATEGIC COST REDUCTIONS
In support of these business initiatives, we are increasing our strategic marketing investment as a
percentage of sales and boosting research and development spending. In fact, we invested about
$90 million in incremental marketing and R&D expenses in 2005. Doing so allows us to leverage
product innovation more effectively and to improve brand equity and market share. Annual R&D
spending should rise to more than $400 million by 2009, a 50 percent increase compared to 2004.
Our investment will be focused on brand expansion, exploration of new categories and the addition of
more health-oriented products. We are aggressively funding disruptive technologies, reducing product
development cycle time and vigorously pursuing external ventures and alliances. Additionally, we are
refining our customer management capabilities, tools and processes to foster greater collaboration
and ensure our role as an indispensable partner to our retailers. Our leadership in radio frequency
identification (RFID) technology, which has the potential to revolutionize the supply chain, and the
innovation summits we hold regularly with customers are two notable examples of the progress weve
made in these areas.
The strategic cost reductions announced in July 2005 as part of our competitive improvement
initiatives will help fund the growth investments. In conjunction with these efforts, we plan to sell or
close approximately 20 facilities, streamline four plants and expand seven others. Clearly, these were
tough decisions to make because they will result in a net reduction of 6,000 jobsroughly 10 percent
of K-Cs workforcebut they are necessary in an increasingly competitive environment. We have
made good progress to date implementing our plans and will continue to move rapidly in 2006 to
streamline both manufacturing and administrative operations, primarily in North America and Europe.
In total, we expect to incur $625$775 million in after-tax charges through 2008 for these initiatives,
but they should yield annual pretax savings that will increase to $300$350 million by 2009. Savings
should begin to ramp up in 2006, with $80$100 million targeted for the year. That amount is in
addition to at least $150 million in FORCE (Focused On Reducing Costs Everywhere) savings we
anticipate in 2006.
Weve also made important progress in improving the sustainability of our products and processes. In
2005, we completed the second phase of a 10-year sustainability effort that has yielded significant
reductions in energy and fresh water usage along with a number of other environmental benefits.
Such efforts are ongoing, and we have established even more ambitious goals for the next five years.
MEETING OUR FINANCIAL GOALS IN A CHALLENGING ENVIRONMENT
Now, let me share some details on Kimberly-Clarks 2005 financial performance. Our teams continued
to execute well, delivering on the objectives of our Global Business Plan launched in 2003. Revenues
increased by 5 percent, primarily based on a 3 percent increase in sales volumes and a 1 percent rise
in prices. Product introductions across all business segments, as well as double-digit sales gains in D&E
markets, drove the top-line improvement. In BRICIT countries, sales jumped more than 30 percent.
Earnings per share before unusual items for 2005 rose 6 percent, in line with both our annual target
of mid-to-high single-digit growth and our original guidance for the year. Revenue growth, rigorous
cost-reduction efforts and repurchases of KMB shares helped us meet our EPS goal in the face of
significant cost increases. Indeed, cost inflation totaled approximately $400 million in 2005more
than double the estimate in our planwith prices for oil-based raw materials, energy and distribution
all up substantially. Productivity gains, along with product specification improvements, helped K-C save
nearly $210 million in 2005, offsetting some of the inflation. In addition, pricing improved slightly,
principally in the North American consumer tissue business. Unfortunately, the higher costs eroded
much of these benefits. All told, operating profit before unusual items increased just 1 percent and
operating margins declined for the year.
Meanwhile, return on invested capital (ROIC), a key measure of how we put your money to work,
exceeded our annual improvement target, rising by 70 basis points. We spent $710 million, about
4.5 percent of sales, on capital projects. Investments were targeted primarily at cost savings initiatives
and product improvements. Cash flow remained strong, although down from 2004s record level. That
allowed us to return $2.3 billion to shareholders through dividends, which increased 12.5 percent, and
through share repurchases totaling $1.5 billion.
INNOVATIVE PRODUCTS YIELD VOLUME GAINS
Below are some of the highlights from each of our three global business segments:
In Consumer Tissue, our North American business delivered solid revenue and operating profit growth
due to higher sales volumes and prices, with further price increases set for 2006. Thanks in part to
the successful North American launch of Scott Extra Soft, overall volumes for Scott bathroom tissue
rose at a double-digit rate. The Scott Extra Soft tissue line extension is just one example of our ability
to transform consumer insights into solutions. In this case, we filled the No. 1 unmet need in the
bathroom tissue category for a softer, value-oriented product. K-Cs proprietary uncreped through-air
dried (UCTAD) technologywhich creates softer, bulkier sheets using less fiberhelped make this
possible. This highly efficient technology continues to be a cornerstone of our tissue growth plans.
Kleenex Anti-Viral facial tissue, introduced in 2004, continues to perform well in North America and
is now available in every region. In the United Kingdom, Andrex bathroom tissue reached a 35 percent
share of the market, its highest level in more than a decade. Moreover, we are beginning to see some
price relief in Europe as 2006 gets under way. Asia has been a particularly bright spot among
developing marketsdelivering double-digit volume growthwith Australia, South Korea and Taiwan
leading the way.
In Personal Care, 2005 saw the rollout of a number of product introductions and improvements,
including a stretchier waistband for Huggies Super-Flex diapers in Europe and Huggies Supreme
diapers in North America. We benefited from continued strong growth in our incontinence, wipes and
child care businesses. The latter delivered another year of record global volumes, with Pull-Ups
training pants with wetness liner a key driver of growth in North America. Our new line of Huggies
baby toiletries has also been well-received, and we recently launched Huggies Cleanteam toiletries for
toddlers. We continued to achieve double-digit volume growth in adult care in North America for our
Poise and Depend brands through line extensions and packaging changes gleaned from shopper and
user insights.
Thanks to our product innovations, European diaper volumes rose for the first time in several years as
we continued to execute our turnaround plan for the business. In Latin America, our multi-tiered product
strategy led to double-digit volume growth in infant care. Among other success stories outside North
America, our introduction of value-tier training pants in a number of Asian and Latin American markets
in 2005 increased our overall D&E training pant sales by more than 40 percent. In Israel, we achieved
the No. 1 market share position in feminine care following the 2005 launch of the Kotex brand.
In Business-to-Business (B2B), KCP achieved solid revenue gains due to a focus on higher-margin
products and to brisk business outside North America. Building on its successful wiper business, KCP
expanded its workplace portfolio with products such as a new version of WypAll X80 towels and
moved further into the do-it-yourself, safety and scientific sales channels. Im particularly proud of the
work done by the KCP washroom team in North America over the past two and a half years to improve
margins and returns. As a result, that business, originally identified as fix in our Global Business
Plan, is now solidly positioned as sustain.
In our Health Care business, our products are enjoying continued strong demand outside North
America. We are seeing success in Europe and in targeted markets in Asia as well. This business is
rapidly moving beyond a protection model centered largely on gloves, gowns and face masks into one
that offers higher-order medical devices and solutions. Microcuff endotracheal tube technology and
the patient warming system acquired from Medivance are two excellent examples. Our enterprise
focus on improving speed to market is evident in health care where weve prioritized our resources
behind fewer, more significant products than in the past. Doing so will allow us to get those with the
highest potential into the hands of health care professionals more quickly. You will see many of these
new products as 2006 unfolds.
BUILDING LONG-TERM COMPETITIVE ADVANTAGE
In 2006, we expect to make further progress under our Global Business Plan. The top line should
continue to benefit from new and improved products and strong results in D&E markets. We are also
confident that we will generate additional cost savings in 2006 and beyond. While we anticipate
continued inflationary pressures, we are intent on delivering sustainable bottom-line growth. We also
remain focused on increasing cash flow from operations and further improving ROIC.
We expect capital spending to be higher than in 2005 but within our long-term target range of
56 percent of sales, as we ramp up our investments in growth opportunities. Based on our
confidence that cash flow will remain strong, we have announced a 9 percent dividend increase and
plans to repurchase approximately 3 percent of our outstanding shares.
Before closing, I want to recognize the contributions of B2B Group President Dudley Lehman, who will
retire in late 2006. Dudley has been with Kimberly-Clark for 30 years, and his contributions have been
invaluable. As former head of our North American Infant and Child Care business, he was instrumental
in the development of Huggies as a truly global brand and in the launch of the Pull-Ups, GoodNites
and Little Swimmers brands. Dudley will be missed.
Finally, I want to thank our employees worldwide for their accomplishments. Building on a solid base,
were making positive changesinvesting in the best growth opportunities, improving our brands,
reducing our cost structure and ultimately creating further competitive advantage. Were also going
to market with innovations based on a deep understanding of our customers, shoppers and users. Im
convinced these efforts will translate into improved returns for you, our shareholders. Now I invite you
to turn the page and read more about how were bringing our strategies to life.
Sincerely,
Thomas J. Falk
Chairman of the Board and Chief Executive Officer
February 24, 2006