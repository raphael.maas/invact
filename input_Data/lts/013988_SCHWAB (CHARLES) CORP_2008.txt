DIFFERENT                                                and

DISCIPLINED
                                                



Those of you who are familiar with Schwab's recent history     our operating scale and expense discipline into profit
are aware that our business model has evolved substantially    margin and earnings growth, and then leveraging capital
over the years as we've developed a more comprehensive         discipline into even higher earnings per share growth.
set of products and services to help investors achieve their   Part of the formula's power is that it works across a broad
financial goals. By       , we earned less than percent of     range of conditions  even when economic growth is
our revenues from client trading activity, down from           slowing and rates are moving toward cyclical lows. In
percent in      . At the same time, asset management and       extraordinary circumstances, however, just mechanically
administration fees tied to our mutual fund and advice         following the formula when factors beyond our control
offerings had risen to about percent of total net revenues,    are pushing revenues down significantly might not be in
and net interest revenue relating to our cash management       our clients' or stockholders' best interests.
and banking offerings totaled about a third.
                                                               For example, clinging to a particular expectation for
We've certainly built a more diverse and stable revenue        profitability or EPS growth might lead to expense cuts so
stream compared to the trading-dependent Schwab of old.        severe that our client service capabilities are impaired, or to
If, however, someone wanted to test our current business       capital levels that provide a thinner-than-desired cushion
model by throwing us into the toughest operating               against potential stresses on the company's balance sheet.
environment imaginable, it's hard to see how they could
have done better than what we actually faced in         :a     With a percent drop in the broad equity indices during
faltering economy weighing on our clients' willingness               and a decline in the Fed Funds rate from      percent
and ability to invest in the capital markets; declining        to essentially zero, it was clear we needed to act. Our
equity values pressuring our asset-based revenues; and         philosophy of taking the long view in building value for
falling interest rates squeezing the spread revenue we         clients and stockholders meant that our path was also clear:
earn on cash balances.                                         balance the more aggressive expense management needed
                                                               to offset declining revenues against the ongoing investment
In my letter to you last year, I discussed how our baseline    needed to drive future growth. We also recognized the need
formula for financial management focuses on leveraging         to work with the other elements of the formula under our
                                               




control  a relentless focus on our clients and their needs,      environments, nor do we think you'd want us to impose
as well as careful risk and capital management  in order to      one. Instead, we believe the ongoing application of
deliver the strongest possible near-term performance while        judgment in determining and maintaining the right mix
protecting our longer term opportunities.                         of operating costs, investments for the future, and current
                                                                  profitability will enable us to maximize the company's
Did our balancing act succeed in       ? As Walt has shared       long-term value to clients, stockholders, and employees.
with you in the preceding CEO letter, by sticking to our
strategy and putting the needs of our clients first and           There are, however, certain areas where you can expect
foremost, we were rewarded with another year of strong            a more dogmatic approach from us in running Schwab.
asset inflows as clients remained engaged in managing their       First, the company's struggles following the Internet bubble
financial affairs.                                                provided a firm reminder that we cannot solve revenue or
                                                                  profitability challenges by imposing on the goodwill and
This success with clients, in turn, helped asset management       loyalty of our clients. Our pricing, client service capabilities,
and administration fees stay comparable to           levels       and product offerings must remain competitive through
and net interest revenue to rise by percent despite the           all environments, and we'll just have to make the tradeoffs
deteriorating environment, while trading revenue actually         necessary to ensure that happens.
rose by percent. With overall net revenues up by percent
and minimal impact from the ongoing mortgage and credit           In addition, we intend to sustain a conservative approach
market meltdowns, we were largely able to concentrate             to managing financial risk at Schwab. Our philosophy is
on sustaining our momentum, and we even increased                 straightforward  we're not trying to eliminate risk across
our marketing investment by approximately percent.                our businesses, but we do expect to identify and manage
Our ongoing efforts to improve operating efficiency and           it on a continuing basis. Schwab exists to help investors
leverage the scale in our support enterprises enabled us to       become financially fit, and we intend to earn our keep
trim overall expenses by percent, which in turn led to a          by serving that need, not by artificially manipulating the
     percent pre-tax profit margin and a percent increase         balance sheet, wandering into activities where we lack core
in income from continuing operations to          billion, both    competencies or clear client need, or chasing after higher
records, as well as a percent return on equity.                   returns on our investments without regard for credit quality.

As we think about the outlook for               , we have to      We may very well have more tough choices to make before
acknowledge that, as of today, there's no positive catalyst in    the operating environment improves, but the old expression
sight for the economy or those two big external influences        "That's why it's called management" comes to mind 
on our revenues  short-term rates and equity valuations.         if all we had to do was set and then rigidly follow a
Given that these drivers are starting out dramatically lower      formula, you'd rightfully be asking what the Schwab
than a year ago, we believe this is a case where even more        executive team was doing with the rest of our time.
aggressive measures are called for if we wish to maintain         Whatever           and beyond holds in store for us, I can
critical investments for the future and still deliver the solid   assure you that this team is dedicated to coming to work
financial performance our owners expect. As a result, we          every day with the goal of finding a better way to serve
currently plan to cut our total expenses by to percent            clients, and then turning that success into standout financial
between         and        . This reduction will come from        performance. We hope to earn your continued support.
lower levels of staffing, professional services, development
projects, and certain aspects of our marketing efforts.
Including these expense cuts  but excluding any related
charges  we'd expect to achieve a pre-tax margin of at least
   percent for         if equity valuations and short-term        Joe Martinetto
rates are consistent with or above year-end            levels.    March

I should note there's no bright line here in terms
of minimum performance expectations in tough
