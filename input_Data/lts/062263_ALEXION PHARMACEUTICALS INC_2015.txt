TO OUR SHAREHOLDERS
It is a privilege to report on a transformative year in
Alexions history. In 2015, we grew our complement
franchise, built a premier metabolic franchise with global
approvals of two new therapies, and advanced the most
robust rare disease pipeline in biotech to accelerate
our growth. Amidst great transformation, one thing has
remained unchangedour singular focus on serving
patients with rare and devastating diseases.
I am often asked what sets Alexion apart from others
in our industry. The answeras clear to me today as
when I joined the company 10 years agois that Alexion
sees what others do not. More than two decades ago,
we saw the untapped potential of complement inhibition
to address the needs of patients suffering from
devastating, ultra-rare complement-mediated disorders.
Today, Alexion is the global leader in complement biology
with Soliris
 (eculizumab) at the foundation of a growing
complement franchise. In 2015, we continued to serve
an increasing number of patients with paroxysmal
nocturnal hemoglobinuria (PNH) and atypical hemolytic
uremic syndrome (aHUS) with Soliris across our
50-country operating platform.
We also strengthened our global leadership in rare
diseases with the acquisition of Synageva BioPharma
Corp., expanding our pipeline and further establishing
our metabolic franchise. Our two highly innovative
enzyme replacement therapies both received regulatory
approval in 2015Strensiq
 (asfotase alfa) for patients
with HPP and Kanuma (sebelipase alfa) for patients
with lysosomal acid lipase deficiency (LAL-D). Both HPP
and LAL-D are devastating ultra-rare disorders in which
the youngest patients face early mortality, often before
their first birthday. In both diseases, we saw an
opportunity to overcome myths and misperceptions
and to reach more patients through targeted,
scientifically-based disease awareness programs and
diagnostic pathways. We are now in the early stages of
launching Strensiq and Kanuma in initial countries, and
are working with urgency to serve patients with these
two devastating diseases.
As Alexion expanded its commercial portfolio in 2015,
we advanced the most robust rare disease pipeline
in biotech, with 10 programs in clinical development
across our complement and metabolic portfolios, and
30 preclinical programs. Notably, we progressed three
ongoing registration programs for eculizumab in severe
and ultra-rare complement-mediated disorders, and
presented initial patient data for three highly innovative
moleculesALXN1210, ALXN1007, and SBC-103.
Lastly, 2015 was a year of strong financial
performance. Total revenues were $2.604 billion,
representing 21 percent revenue growth and reflecting
29 percent volume growth compared to 2014. Looking
ahead, we continue to see the majority of opportunity
to serve patients with PNH and aHUS ahead of us, with
Strensiq and Kanuma further supporting our growth.
I am proud of what we achieved in 2015, but our work is
far from done. We are at the strongest point in Alexions
history, with an unprecedented opportunity to serve
patients. As we undertake this work, we are delighted to
have returned to New Haven, where Alexion was founded
in 1992. Id like to recognize our principal founder and
current Chairman of the Board, Dr. Lenny Bell, and thank
him for 23 years of exceptional leadership as our CEO
to build Alexion into the company we are today.
Thank you, too, to the patients and families whom
we proudly serve, the researchers and physicians who
share our singular drive to transform patients lives,
and the shareholders who support our mission. I would
especially like to recognize our employees3,000
strong todayfor having the courage to find answers,
change the world, and create a legacy.
Sincerely,
David Hallal, chief executive officer