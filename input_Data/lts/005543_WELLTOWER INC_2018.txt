Letter To Shareholders

Dear Shareholders:

In 2015, we took the bold step of changing our name to Welltower, marking our journey from a portfolio of real estate properties to a health care infrastructure platform that emphasizes wellness and connectivity across the continuum of care. We are working toward effecting change in health care and believe that our platform can play a pivotal role in making change happen.

Today, our society is challenged to manage the needs of the worlds aging population. Health systems must work with leading post-acute and seniors housing operators to achieve the mandates of population health models being implemented in the U.S. and across the world. These mandates hold health systems responsible for patient outcomes and aim to make costs manageable.

We are already seeing an increase in Alzheimers disease and associated dementias. The cost of providing care to people with dementias is enormous and unmanageable over time from a treatment and infrastructure perspective. Seniors housing is a wellness model that provides a compelling system for maintaining mobility, nutrition and cognitive engagement. Meanwhile, post-acute care and outpatient medical solutions support emergent technologies that provide better treatment at lower cost and a superior patient experience.

Thats why we see dramatic opportunities for growth in seniors housing and post-acute care, where we have built a network of partnerships with the best national and regional operators in the U.S., U.K. and Canada. Were in a great position to transform health care infrastructure.

Were in a great position to transform health care infrastructure.
Since our inception, youve been able to count on this company to execute progressive strategies that define or redefine our industry and create superior long-term value for our shareholders. Our accomplishments in 2015 represent another milestone that I believe will be remembered for many years to come.

Our financial results in 2015 show that our work is paying off. We have the right assets and the right partners in the right markets: New York, London, Toronto, Los Angeles, Boston, San Francisco and Washington D.C.cities with higher incomes, aging populations and significant barriers to entry. Equally important, we have been disciplined in our capital allocation to avoid smaller, over-supplied markets and higher-risk investments.

The quality of our real estate stands out more than ever as a material competitive advantage
In a more volatile environment, the quality of our real estate stands out more than ever as a material competitive advantage, and I am equally confident these advantages lead to superior upside for our shareholders. Our performance this year attests to the solid fundamentals, resilient demand for our properties and an exceptionally strong investment strategy that drives growth:

Welltower generated annualized earnings growth of 6% to $4.38 normalized FFO (funds from operations) per diluted share.*
Growth was positively impacted by full- year average total same store cash NOI (net operating income) growth of 3.1%. Resilient demand for our properties, improved occupancy rates and rental rate increases in the face of competitionunderscore the quality of our real estate.*
We completed $4.8 billion of new investments, and 77% were with existing seniors housing, post-acute care and health system partners. This demonstrates the power of our relationship network.
Equally important, we disposed of $1.2 billion of non-strategic assetsincluding our last remaining U.S. hospital propertyas we proactively recycle capital to best position our portfolio in an evolving health care industry.
Our balance sheet at the end of 2015 is in excellent shape. We have reduced leverage significantly over the past three years. As of December 31, our net debt to undepreciated book capitalization was 39.9% and our net debt to adjusted EBITDA stood at 5.6 times.*
We are successfully aligning with long-term capital partners to enhance our financial flexibility. Im proud of our new joint venture with the Canada Pension Plan Investment Board (CPPIB), one of the worlds premier institutional investors. CPPIB decided to make its first investments in U.S. outpatient medical and seniors housing real estate with Welltower, which I believe speaks volumes about the quality of our company. Additionally, we grew our partnership with PSP Investments, another large Canadian pension fund, which owns our operating partner Revera. Since closing our $1 billion investment with Revera in 2013, we have completed nearly $1 billion in follow-on, pro-rata investments.
Our Board of Directors declared a cash dividend for the quarter ended December 31, 2015 of $0.86 per share, a 4.2% increase compared to the same period last year. On February 22, 2016, we paid our 179th consecutive quarterly cash dividend.
Our growth during the past five yearswith more than a 300% increase in gross real estate investments totaling over $31 billion  has laid a successful foundation for our continued success.

We are poised to generate another year of solid financial results and dividend growth in 2016. Economic uncertainty has led to volatility in the capital markets. These periods often lead to separation among companies. We are in a strong positionwith our world-class portfolio, deep partnerships and the best capitalized balance sheet in the industryto take advantage of opportunities while protecting our downside.

Over the medium-term, we see targeted opportunities to go deeper into strategic markets. We have unmatched local scale in our seniors housing business, strong partnerships to develop the next generation of post-acute care solutions, and enormous opportunity to grow our outpatient medical business with the worlds best health systems.

I cannot be more excited about what lies ahead for Welltower.
No one in our industry has a better platform than Welltower for creating long-term shareholder value. At the same time, we are making a tremendous difference in the world. I am proud to lead a seasoned management team and employees who take pride in our mission to promote wellness. I cannot be more excited about what lies ahead for Welltower.

Sincerely,


Thomas J. DeRosa CEO, Welltower Inc.