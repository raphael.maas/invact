Dear fellow shareholders:
As I reflect on 2012, three words come to mind  execute,
execute, execute. We continued to focus on doing what
we do best  helping our clients take action to achieve their
fi nancial dreams. We exercised discipline. We managed risk.
And as a result, we achieved outstanding growth and success.
understanding our clients. Serving their needs.
Since 1879, the Principal Financial Group has been investing
clients hard-earned dollars to help them achieve fi nancial
success and protection for their loved ones. We have always
invested for the long-term  delivering on our promises
through the best and worst of times.
2012 was no exception. Our Investment Management Plus
strategy, supported by strong execution, continued to build
momentum across our businesses and deliver strong fi nancial
results.
 Despite continued macroeconomic pressures, adjusted
total company operating earnings grew 7 percent in 2012.
 Earnings per share rose 12 percent.
 Net income grew 25 percent.
These results  in yet another year of market uncertainty
 underscore the soundness of our strategy, the synergy of
our businesses, the strength of our global partnerships and
the huge opportunities available in exploding middle class
markets.
The most meaningful proof of our performance is in the
stories of real people and businesses whose dreams weve
helped achieve.
I hear these stories every day. I talk to financial advisors
who work hard on their clients behalf. I read letters from
clients who aspired and achieved. I listen to calls from
baby boomers preparing to retire.
I understand the relief a business owner feels when her
succession plan is in place. And Im inspired by the millions
of families in emerging markets who fi nally have the
opportunity to dream, to plan and to save.
Our work on behalf of the clients and advisors who rely on
us delivers unparalleled long-term value  not only for them,
but also for the shareholders who invest in us.
Achieving strong organic growth.
We ended 2012 with record assets under management 
exceeding $400 billion and refl ecting $30 billion in total net
cash fl ows.
 Full Service Accumulation had record sales of $11.5 billion
in 2012 and record full-year net cash fl ows of $7 billion.
 Driven by strong investment performance, Principal Funds
rose to 19th in market share for advisor-sold funds and
11th in net cash fl ows, based on rankings by Strategic
Insight.
 Both Principal Global Investors and Principal International
closed the year with record assets under management.
 U.S. Insurance Solutions delivered growth in both
Life Insurance and Specialty Benefi ts.
Contributing to these growth results is our unique and
extensive U.S. distribution network  including proprietary
agents, independent fi nancial advisors and consultants,
banks, and local market wholesale and service teams.
We also work with some of the leading wealth management
fi rms, such as Edward Jones, UBS and Merrill Lynch. They
choose The Principal because were a single source for a
broad array of proven retirement, investment and insurance
solutions. We share their commitment to make a difference
in customers lives. And were focused on helping them
achieve success.
In emerging international markets, we have enviable, longterm
relationships with several of the worlds largest and
most respected fi nancial entities. In China, we partner with
China Construction Bank, the second-largest commercial
bank in the world. In Brazil, our partnership with Banco do
Brasil, the largest bank in Latin America, extends until 2033.
In key markets in Southeast Asia, we partner with CIMB
Group, one of that regions leading universal banking groups.
And in India, we have a partnership with Punjab National
Bank, the largest national bank in India. By building strong
local relationships, these partnerships  and others  enable
us to speak the language, share the dreams and serve the
unique needs of each market and culture.
expanding our global footprint.
Just two years ago, the worlds population surpassed
7 billion. Since 2010, The Principal has completed six
strategic transactions that give us strong competitive
positions in markets that comprise half that global
population. Whats even more compelling is that, every
minute, record numbers of people are entering the middle
class in these emerging markets.
We continued to expand our global footprint in 2012. Two
signifi cant Latin America acquisitions solidifi ed our position
as a retirement leader in this regions fast-growing emerging
markets.
 In April, we acquired Claritas, a Brazilian retail mutual
fund and asset management company. Claritas
complements our existing pension leadership position in
Brazil, where BrasilPrev, our joint venture with Banco do
Brasil, is the fastest-growing pension company.
 In October, we announced our intent to acquire Cuprum, a
premier mandatory and voluntary pension provider in
Chile. The transaction closed in February 2013, further
positioning The Principal as a retirement leader in that
market.
Staying the course. rewarding investors.
Our Investment Management Plus strategy has been in
place for more than a decade. This strategy not only
remains unchanged, it has proven itself to be more relevant
and appropriate than ever. As a result of that strategy, we
have achieved our goal of becoming a global investment
management leader. And we are positioned for greater
growth and success in the years ahead.
None of these accomplishments would be possible without
the skills, experience and dedication of our employees
around the world. And we are committed to providing a
working environment that supports their growth and success.
 We are No. 1 in the largest employer category of
Pensions & Investments Best Places to Work in Money
Management.
 For the 11th consecutive year, IDGs Computerworld
named us to its List of 100 Best Places to Work in
Information Technology.
 For the 10th straight year, we were named a Top 50
Company by the National Association of Female
Executives.
On behalf of the board of directors, Id like to thank our
shareholders for your continued support and confi dence
going forward. As our business model continues to generate
more fee-based operating earnings, we are able to extend a
greater share of earnings back to shareholders, leading to
increased long-term value for them.
To that end, The Principal moved to a quarterly dividend
in 2012. Additionally, the total dividend in 2012 increased
11 percent over the previous year. Over time, we expect to
increase our common stock dividend payout ratio, which
was about 30 percent at the close of 2012.
Finally, please be sure to cast your vote in time for the
Annual Meeting of Shareholders on May 21, 2013.
Larry Zimpleman
chairman, President and
chief executive Offi cer