Dear Fellow Shareholders

The year 2008 was a successful one for Murphy Oil Corporation with record net income
of $1.74 billion ($9.06 per share), helped in no small part by record high oil prices during
the first half of the year. New earnings milestones were reached in both our Upstream
and Downstream businesses and as a result, strengthened an already sound balance
sheet. We exited 2008 with a debt to capital employed ratio of 14%, a reduction from
23% the previous year. The rapid collapse of world oil prices in the second half of 2008,
a wobbly world economy and uncertainty about the timing of future economic recovery
do not point to the likelihood of us duplicating the same level of earnings in 2009.
However, there will be interesting opportunities for a financially sound company like ours;
our aim is to position ourselves to benefit.
The oil and gas business has a long history of being cyclical; I recall several of these
unannounced downturns in my thirty-plus year career. In this setting, our conservative,
disciplined outlook, solid balance sheet, quality assets and a view to the horizon will, as
in past downturns, stand us in good stead as we look forward to new value-enhancing
opportunities for our Company.
Exploration and Production Production during 2008 grew 25% over the previous year
and averaged 127,500 barrels of oil equivalent per day. The primary catalyst was the
production ramp-up at Kikeh (80%) which started flowing oil in August 2007. The field
continues to perform well and reached its multi-year plateau rate during December
2008 as planned. Natural gas production from Kikeh began in December 2008 and is
contracted to supply a third party methanol plant onshore. We expect to recoup our
Kikeh investment at some point during the first half of 2009. For years to come, this
field will be an important contributor to a very robust and rising production portfolio.
Near term meaningful sources of additional production growth will come from
projects including: Tupper (100%), Thunder Hawk (37.5%), Azurite (50%), Sarawak
natural gas (85%), and Kakap (14%). With the exception of Kakap, which is scheduled
to commence production in 2012, the remainder of these fields will contribute to
production in 2009 and are all operated by Murphy.
Tupper, our lead-off entry into a North American natural gas resource play,
commenced production in December 2008, barely 16 months after first investment.
To date we have accumulated land holdings totaling 131 net sections (84,000 acres) in
British Columbia as we target the Montney formation. Wells thus far have flowed at
rates within our expected range. With our very competitive cost structure, we expect
to receive benefit for many years to come as our Tupper program moves forward.
Later in 2009, a new wave of production begins. Thunder Hawk, located in
Mississippi Canyon Block 734 in the Gulf of Mexico, is slated to start producing oil and
natural gas late in the second quarter. Azurite, located in the Mer Profonde Sud (MPS)
Block offshore Republic of the Congo, is our first oil development in the region and
is also scheduled to begin producing late in the second quarter. Sarawak natural gas
production in Malaysia is expected to commence in the third quarter with initial gross
volumes of up to 250 million cubic feet per day. In addition to the first three dedicated
fields, planned exploitation of nearby existing discoveries should keep this development
producing well into the future.

Exploration results on balance for 2008 were disappointing. We made two tie-back
sized natural gas discoveries in the Eastern Gulf of Mexico: Diamond (62.5%) and
Dalmatian (50%). For the remainder of 2009, we will slow the pace of exploration as
we look to hold our capital spending to within the level of cash flow while relooking
at the risks and costs of our program. We still have exposure to individually significant
upside in the five exploration wells planned for the balance of the year. The first of these
will be an oil and natural gas prospect named Samurai in Green Canyon Block 432. We
own a 33.33% working interest in this nonoperated well that is scheduled to spud later
in the first quarter. An Eastern Gulf of Mexico well targeting natural gas is also planned.
We will drill one deepwater well in Malaysia and the remaining two wells will target oil
offshore Republic of the Congo in our MPS block.
In a continuing effort to improve our prospects, offshore acreage was acquired during
2008 in Indonesia in the Semai II Block (33.33%) as well as in the Browse Basin of
Australia where we picked up Block WA-423-P (70%). We will likely farm down our
interest in the new Australian block to 40%. We operate both areas and plan to shoot 3D
seismic before drilling. In South America, on our Suriname acreage acquired during 2007,
3D seismic has been shot and is currently being processed ahead of potential 2010 drilling.
Refining and Marketing Downstream operations contributed nicely to the bottom line
once again in 2008. United Kingdom refining led in the first half of the year while
United States retail marketing performed exceptionally well during the second half
of the year. Net income of $313.8 million set an all-time downstream record. My sense
is that many are starting to realize the type of earnings potential that, given the right
market conditions, can be realized from the asset mix we now have in place.
As you recall at the end of 2007, we purchased the remaining 70% interest in the
Milford Haven, Wales refinery and now own and operate this asset outright. As
anticipated, blending this facility into our overall refining mix provided us with broader
coverage to the often differing markets on opposite sides of the Atlantic. Also, we have
now been able to capture a low cost plant expansion and optimization opportunity that
will be implemented by early 2010. While we remain long on gasoline in the U.K., we
expanded our retail network during the year not only in England and Wales, but also
in Scotland, where we are testing the market for the first time.
In the United States, our refineries at Meraux and Superior went through planned
turnarounds and were margin challenged for a good portion of 2008. Our retail business
in the United States performed exceptionally well on multiple fronts. Not only were
earnings healthy, but in an environment where overall gasoline demand in the U.S. was
weakening our network of stations grew market share, collectively selling over 2.5% of
total gasoline volumes in the country, and with year-on-year margin growth for non-fuel
sales. We ended 2008 with 1,025 retail outlets. Of these sites, 992 are Murphy USA
sites located on Walmart Supercenter parking lots and 33 are our new larger,
independently located Murphy Express convenience stores.

In Closing We have a multi-faceted oil and gas business with worldwide scope.
Collectively our operations provide a balance of risk and reward which in the current
environment, and the foreseeable future, serves us well. In the past, exploration has
been one key value driver for us. While costs are high and prices low, it makes sense,
absent stellar results, to moderate that program but still keep an important exposure
to its upsides. Adding other growth initiatives, such as joint ventures or selected
acquisitions, makes sense. Our simple goal is to add reserves in the most cost effective
way possible. Our downstream business has carved out its own niche and, while not
collectively large, has components that can deliver attractive returns. As we saw late
last year, this return often occurs out of cycle with the upstream business and thus
lends support to an integrated model.
My predecessor, Claiborne Deming, who worked long and tirelessly to make this
company an even greater success, often remarked you do your best work in times of
adversity  he is right on for the times we are in. We have a great company with a long
history of success through many cycles, great people who take pride in our collective
accomplishments as well as a great opportunity to move forward and reshape our
company to achieve even higher levels of success in the future. I am honored to be
in my position looking out to the horizon, working with my colleagues to enhance the
value for all our shareholders. I thank you for your support.
David M. Wood
President and Chief Executive Officer
February 16, 2009
El Dorado, Arkansas