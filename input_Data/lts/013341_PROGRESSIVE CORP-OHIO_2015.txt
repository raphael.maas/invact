LETTER
TO
SHAREHOLDERS

Marketers aspire to coin catch phrases that capture great emotion and expand brand influence into welcomed but untargeted domains Wheres the beef? Reality suggests they defy an engineered approach and resonate or not
much like a viral video.
In some small way we may have created a contender in 2015 with Sprinkles are for Winners, a pithy ending to one of our commercials where the character Jamie has been unable to present Progressive as the lowest price option to a customer, recognizing an alternative was for the moment lower. As Flo consoles his disappointed state, but recognizes the transparency of our approach, hes offered ice cream; when sprinkles are requested the reality sets in that certain spoils do in fact go to the victor and hence Sprinkles are for Winners took on a life of its own. Used in everything from song lyrics to sports team mantras, we found an accidental host of untargeted but welcome extensions. Equally welcome is the opportunity to describe Progressives business year as deserving of Sprinkles, and Ill enjoy breaking that down in some detail in this letter, along with our efforts and outlook to ensure the accolade remains relevant for a long time to come.

HIGHLIGHT REEL
We added $1.9 billion in net premiums written to top
$20 billion in 2015a 10% growth rate over 2014, with net premiums earned just short of that mark. Operating profitability reflected in our combined ratio was a very solid
92.5, comfortably ahead of our 96 target and comparable
to last years performance. Together these results produced
a pretax operating income of $1.5 billion, about 6% ahead
of similarly strong results last year. Investment returns were less notable on an absolute basis, but within our design parameters represent a considerably stronger relative per-
formanceice cream, perhaps no sprinkles. We added
$455 million of interest and dividend income, up 11% from last year, $113 million in realized gains, half of last years, and a less exciting $324 million unrealized loss for the year, reflecting about a 1.6% total return on our $21 billion portfolio.
Combined, these results produced net income for the
year of $1.3 billion, or $2.15 per share, both comparable to 2014, and represent a return on shareholders equity of 17.2%. The unrealized loss, in part, pressured our more preferred
measure of comprehensive income and we lagged last year by some 50 cents per share on that basis.
The $20 billion threshold, deservedly so, had special mean-
ing within the company and for many we re-experienced
the same pride when passing both the $1 and $10 billion marks and now immediately set our sights on the next significant round number. To be fair, the growth this year, while very solid from year-over-year organic expansion, was aided by the inclusion of nine months of the property premium from ASIthe controlling interest acquisition
we closed in April.
Ive stated before that we would welcome an improved investment environment with interest rates and valuations more comfortably matching our longer-term investment income preference, and perhaps a little less volatility for good measure, but by design we are not dependent on it and our commitment to an investment strategy that supports and enhances the primary asset of the operating company remains central to our long-term thinking.

Gainshare, our best measure of companywide operating performance, valued in a range between 0 and 2, completed the year at 1.6, the highest in some time, and for me a fair and accurate reflection of the year with notable strengthening
in the second half and, even more importantly, carrying attractive momentum forward into 2016. Gainshare is the basis for variable compensation
for Progressive people and shareholders. Our variable dividend formulation met all criteria required to be declared and based on our post-tax underwriting income and the Gainshare score, we declared
a dividend of 89 cents per share,
or an approximate yield of 2.8% at year end.
We enter 2016 with a strong,
well-structured capital position,
bolstered by the $400 million debt offering we completed earlier in the year. Our debt-to-total capital ratio ended the year around 27%, below our self-imposed guideline of not to exceed 30% for any extended period of time and thus preserving significant
debt capacity should we need or choose to use it. Our
capital management philosophy remains consistent with
our long-standing view that capital in excess of regulatory requirements and any contingencies we can envision is available for share repurchases, acquisitions, or shareholder dividends. Our share repurchases during the year were somewhat modest, at 7.3 million shares, in part due to a stronger performance of our stock. The ARX transaction, discussed in greater detail in last years letter, consumed
$877 million, and the shareholder dividend for 2015 performance happily returned $519 million to our shareholders.

TALE OF THE TAPE
Now we know the aggregate results worked out well, it is worth a moment to dissect our primary product offerings and provide a little color commentary.
With no great joy, at this time last year, I characterized our Agency auto growth as flat-out disappointing and suggested it had our full attention and that our commitment to winning in the channel should not be underestimated. More so after such a statement, its rewarding when planned actions and results align to tell a more favorable story. Our Agency business found a higher gear in the second half of the year and has sustained momentum through year-end with expectations of continuing into 2016. What changed? We executed on a multi-point agenda to address opportunities, much of which we outlined in our Investor meeting in
May. The details are not for now, but headlines include a significant advance in product design as we continually use our best data and knowledge to meet competitive market demands and a demonstrably improved bundling option
for agents as we deepen
our relationship with our ASI property solution. Weve reversed the erosion in policy counts wed not enjoyed reporting during the year, while advancing premium growth. Our new business inflow developed during the course of the year into some of the strongest we have seen in recent times, suggesting that
in the incredibly price efficient agency market our product is attractive, and our loss results provide support that the rate level is for now sustainablea considerably stronger positioning we look forward to building upon.
While not yet a massive contribution to the Agency results, strategically a highlight of 2015 was the market introduction of Platinum. Platinum, in its simplest form, is the integration at the market level of ASI and Progressives product lines. The multi-provider approach to homeowners that works so well for us in our Direct business is simply not an option within the Agency channel, and with ASI we have the perfect combination. The product provides agents a single offering, with compensation and coordinated policy periods, along with other features, that reflect the needs and desires of agents that we are now very well suited to address. The product
is by design focused on those agents who have the target
customers and are prepared to accept the proposition that
ASI and Progressive, with this introduction and what is sure
to follow, will be a must have bundled offering in their
agency. Our excitement is obvious for the growth potential,
and the engagement we can have with agents in our development
of the Destination Era, outlined more fully in this
communication last year. However, perhaps most importantly
for those of us who enjoy the science of rating, this will
provide a data set of customers for whom we have had limited
prior auto history and thus presents an incredible opportunity
as we do what we do bestuse that data to refine and
improve our offering over the years ahead. Early results are
just that, but very encouraging would be a fair assessment.
This paints a much brighter picture in the Agency space
and deservedly so, but direction and early responses need
to be supported with continued execution, higher customer
retention, and consistent economic performance. All the
pieces look to be in place to offer even greater value in a
channel we enjoy serving.
Building on a strong 2014, our Direct channel had what
might be described in sports terms as back-to-back winning
years. With low double-digit premium growth rates, and just
short of double-digit unit growth, there is little to be upset
aboutnot suggesting that outside the scope of this letter we
dont have our wish list. Taking advantage of market opportunities
to grow also means we will run our combined ratio,
reflecting new business loss costs and front loaded acquisition
costs, much closer to our acceptable maximum and the com-
bination of a 95.1 combined ratio with 11% written premium
growth proved to be a strong combination of profit and
growth for the year and one I would readily sign-up for again.
Our Direct to the consumer
delivery channel has been built,
in part, on skills developed
from the Agency business
that is at our core. We have,
however, developed
considerable expertise in
marketing creative and
placement, consumer
experience and
presentation, along
with the economics of
acquisition and
customer life
expectancy.
Less by any
designed
outcome,
but more a
reflection
of consumer
choice and
technology, our
Direct channel and Agency
channel for auto are now about
equally balanced. Metrics of
premium and units will produce
slightly different allocations, but an equal
split of the auto business is a reasonable
way to think, and were confident we can
continue to grow in both distributions. Our
Special Lines and Commercial Lines businesses
are considerably more agent favored, but are well
positioned for any similar trends.
Special Lines had a great year with loss costs slightly
lower than expectation, absent a major storm or hurricane
to cause havoc with our boats or even recreational
vehicles. Given our significant market share in motorcycle
and the other products within this grouping, we dont expect
outsized growth and enjoyed the 3% or so growth in the
product line and look forward to any effects of a recovering
economy and reduced gas prices to fuel additional discretionary
toy purchases in future years.

Our Commercial Lines business gets the award for
outsized performance this year. Strong mid-teen growth
and a combined ratio of 84 certainly is a rare combination
and contributed significantly to our aggregate results for
the year. Now just north of $2 billion in written premium,
Commercial is fully 10% of the enterprise and, at that
premium, were expecting when industrywide results are
tabulated that we may be adding commercial auto to
the list of products with a #1 market share designation.
While the story leading to these results has
played out over several
years, the essential message is when results are
not what you want, act with purpose
and ensure profitability before growth.
The Commercial group did exactly that and
by the time we were operating with rate
levels that were more reflective
of market conditions, we found that
others having less comfort with
their rates were limiting their market
involvement and our extraordinary
results followed. Market dynamics
and competition are our constant gravity,
and we expect these results to
become more consistent with our longterm
targets over time, but appreciate
when opportunity knocks, to expand our
influence in this important part of our business.

?egardless of the specific product, our organizational imperative is to continuously improve our ability to segment customers into smaller homogeneous groups and advance our ability to best match expected loss costs to those groupings. Over time those groupings have become more and more complex and may now be defined, in part, by driving behaviors, such as hard braking, or technology preferences for policy quoting or servicing. The ability to
do this well is at the heart of any success we may have had and made possible with people who enjoy doing so and finding relationships in data that could provide yet marginal improvements to an age old concept. Data is our central asset, and our ability to analyze data, now
in some cases in massive quantities, and find correlations and causal relationships, is the Progressive statistics factory I often describe to those outside our industry.
Consumer sensitivity to changes in their insurance premium is for many very high. Consistent with our desire to keep customers a long time, we want to be sure that any rate change is needed, surgical, and, where possible, smaller in magnitude, even to the point of several smaller changes over time, which are appreciated more than a singular adjustment. As illustrative examples of our commitment to advancing rating science and achieving that objective, we have developed significant ability, by ingesting weather
data from hundreds of monitoring stations around the U.S. several times an hour, to help recognize the difference between an observed weather influence and an expected weather influence in an attempt to ensure we do not let a more obvious rationale for elevated loss costs disguise a more substantial trend that needs to be addressed, or equally important resist reacting to a false positive for rate need. An accurate historical perspective has come
into play more than once in this El Nio year. Similarly, with a longer-term trend toward lower frequency, intermittent increases in frequency trends need to be explained in some detail to respond appropriately. This year we experienced much more favorable pricing for gasoline and numerous sources confirmed increased mileage resulted. We were in
a position to use a signif-icant sample of our Snapshot users to more accurately understand the types of mileage increases and associated frequency, effectively in real-time versus delayed and aggregated reporting sources. The specifics here are less the point to be made and much more so the value of data and an ability to have that data be converted into a commer-cial advantage. We dont intend to stop looking for opportunities anytime soon and
expect to carry that forward into our Destination
Era product line up.
Progressive and ASIs Technology, Claims, Marketing,
and Customer Relationship Management groups all had very successful years and, while their specific accomplishments deserve greater recognition, suffice it to say that each group is focusing on the types of issues that will advance the company versus fixing in-place processes, and thats a foundation that makes all we expect to achieve possible. Our expense ratio of 20% is an attractive position relative to the industry and in no small way contributes to our success, without restricting the ability of each group to create experiences that foster long-term promoters with confidence in us. Accepting that, we see opportunities for yet greater leverage, in large part through longer customer relationships.

WHERE TO FROM HERE?
Forces of change are all around us, and almost any industry will have challenges and opportunities advanced usually through technology and consumer behavior. Our industry will not be immune and, while some will have longer enactment periods, they will present real and exciting opportunities for those prepared to embrace them.
Media consumption, and a shift to more addressable media in forums other than network television, will both attract more of our acquisition budget and provide far greater ability to analyze yield and spending effectiveness for those suited to the newer models. Advancing our brand, more so to the right audiences, is critical to the future we see for Progressive in the Destination Era and were excited by the prospects.
Car ownership or temporary vehicle use options,
headlined by names like Zipcar to Uber, will challenge
the foundations of driver-to-car matching, which, while
not perfect in current schemas, is not as widely variant as we
must accept as possible in the future. We enjoy embracing
change and the potential for new products and will look for
ways to avoid putting square pegs in rounds holes, rather
taking opportunities available for us to lead in designing
solutions more responsive to contemporary realities.
Vehicle technology is certain to challenge the notion of
driving weve held for the last hundred years or so and,
as such, we must position Progressive to be advantaged
in the product shifts and opportunities that will surely
follow. Recognizing vehicles as moving IP addresses
and being in the data flow was an early and
remains an ongoing objective, and our
Snapshot offering is exactly that. We
remain very excited by our Snapshot
program and have even higher
hopes in 2016 of reaching a greater
proportion of our policyholder
base, with the introduction of a
smartphone application capable
of meeting the data integrity
standards we have been seeking
for several years. Long-term
accident frequency reduction
has been a fact of life for our
industry for the last 30 plus years,
with great societal value. The
outlook with current and expected
vehicle technology features, up to
and including autonomous features, is
for continued reductions. We must be
intensely focused on our segmentation science
and assigning pricing accurately during what
will be a long period of mixed-mode technology
vehicles on the road. Disruptive change we believe
will play well to our strengths for a long time to come.

Accepting change can be a catalyst for Progressive to
grow; its equally rational to accept that premium per vehicle
could decline, potentially materiallyinterestingly enough
that has not been the case to date with claims costs more than
offsetting the frequency declines over many years; however,
Im only prepared to accept that outcome as one of several
that seem possible. I do, however,
sense that new insurance products will develop alongside the changes in vehicle technology (who might have envisioned a sizable market in cell phone insurance 15 years ago or the need for cyber insurance?). Progressive will be true to its name in managing change to our immediate advantage and positioning for longer term shifts as they eventuate.

A feature of this report and last years letter
was the evolution of Progressive through
eras described as Manufacturing/
Wholesaling, to Retail, and now to the
consumer centric Destination Era. The
substance of the Destination Era gained
additional and significant institutional
energy in 2015 with every area of the
company examining in greater
depth the operating and systems
implications needed to execute
in that model with the same
intensity weve attained in
our core products. Now,
with bonafide solutions
to meet consumer needs
beyond the initial auto
insurance product, we have positioned Progressive to
meaningfully address the
lifetime needs of customers.
They can now look to us for
protection products including all
property needs, umbrella, flood, classic
car, special event, travel, pet, life, ID
protection, and more. Historically, we didnt
have these assets and thus were less well posi-
tioned to keep the continuity of relationship that so
often starts with an auto insurance purchase, but develops
into a broader product array. Simple models are just that,
but the customer label of ?obinsons for us is reflective of the customer with home and auto insurance, and often a selection of one or more other products suited to their life style. ?eferenced internally as Auto+, our nomenclature
is a fitting designation for the positioning and one were finding can also start with a non-auto relationship.

Weve reported that we under index on ?obinsons and over index, relative to our aggregate market share, on those in life stages leading toward a more ?obinson-like status. Graduating the future ?obinsons, we believe, is highly rational given our advantaged position of a brand that has shown appeal to new buyers, the positive first product experience we provide, and now a product line-up that encourages continuity of relationship as their needs expand. Again a simple model, but highly reflective of our
positioning, is to think: Acquire, Anchor, Bundle, and Extend. Acquireenhance the brand
appeal in every way we do business; Anchora great first product
experience often through auto; Bundlebe responsive to individual life lanes and add product needs seamlessly; Extendan optimal state for both the customer and Progressive. By retaining customers for decades and attracting those ?obinsons who find our proposition compelling, implies for us a significant addition to our addressable market, perhaps as much as 50% for just the auto product, and more so with
our entry into the property market and recognition of the Commercial Lines analogue to the Destination Era. Auto and Property are the lions share of the personal lines property and casualty insurance premium base, but other products are important complements when needed for long-term retention. Our focus on auto insurance and the skills we have honed will certainly be central to our business model for as far forward as anyone can reasonably see, but our business model is without question evolving toward being a contemporary solution for our customers with a wider aperture and greater diversification of insurance products.

Our product requirements to execute effectively in the Destination Era are expansive and we dont intend to manufacture them all, but rather rent both the capital and expertise from those who are leaders in their field. In some cases a single product provider will be the desired solutionASI in the Agency channel for Property is a perfect example, along with our classic car solution for Direct customers with Hagerty Insurance, in our minds the leader in the
spaceor we may seek diversification within a product line to meet the broadest range of situations, such as the line-up
of homeowners providers in the Direct channel. We have marketing opportunities through our agents, our online presence, and a greatly expanded in-house agency to offer customers greater access to the products they seek and have opportunities for presentation, relationship building, and just-in-time marketing, all expanding our repertoire
of customer experiences. We will take indemnity risk and
use our capital where it makes sense to do so and receive commission income on other products where thats more appealing. As time and experience develops, we may offer other combinations that are also mutually appealing. Noted last year and worth reinforcing, Progressive is becoming
a consumer company with auto insurance at its core and
the currency of success is customer life expectancy (an appropriate restatement of the earlier positioning of policy life expectancy).
The times they are a-changinnot quite Sprinkles
are for Winners, but a Dylan lyric no one would readily rejectand Progressives adaption to those changes
embodied in our Destination Eras positioning provides for
us a gamut of challenges, but equally, for me, the feeling
of never being more optimistic about what lies ahead.
(A feeling Ive been lucky enough to experience more than
a few times in Progressives evolution as a company).
OUR PEOPLE AND CULTURE
The artistic theme visually complementing this years report
is continuity and while its selection was primarily to amplify the positioning of Progressive as a continuous and contemporary solution to a range of consumer insurance needs over time, it applies equally well to the continuity of the cultural foundation of Progressive.
Our Vision, Values, and Objectives are the bedrock of the Progressive culture. The Vision and Mission affirm our benefit to society and drive our aspiration to be consumers number one choice for auto and other insurance needs.
Our Values unambiguously guide behavior. Demanding Objectives attract and retain special people who enjoy working hard, performing well, being rewarded competitively, and growing constantly.
Our people, culture, and aspirations are what make us special.
Nothing we have achieved has been without the efforts of so manyProgressive and ASI people, our agents, customers, and shareholders.
To those who make Progressive, progressiveThank you; definitely add sprinkles!

Glenn M. Renwick
President and Chief Executive Officer
