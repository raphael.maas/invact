TO OUR SHAREHOLDERS AND FRIENDS:
As we anticipated, 2013 was a year
of two distinct halves: In the first
half, we continued to position the
business for success as the U.S.
construction economy moved
toward recovery. In the second half,
the construction market recovery
accelerated, and we delivered.

While pricing was solid throughout 2013,
aggregates
volumes increased by 8 percent in
the second half of the year, after declining by
1 percent in the first half. And our gross profit from
aggregates increased by 27 percent in the second
half, after increasing by 4 percent in the first half,
demonstrating both the accelerating recovery of
our markets and the powerful earnings leverage
in our business.
For the year, earnings per share from continuing
operations were $0.16, an improvement of $0.58
from 2012; Adjusted EBITDA  an important
measure of our cash generating capabilities 
increased by $57 million, or 14 percent; and gross
profit margin increased by 230 basis points.
Importantly, the profitability of our aggregates
business as measured by cash gross profit per ton
was 30 percent higher than at the prior peak
level of shipments, setting the stage for stronger
earnings leverage in this cycle.
Throughout our business, Vulcan is reaping
the benefits of our operational excellence,
our unmatched geographical footprint and the
spirited performance of our people.
We continue to focus on what matters  controlling
costs and exceeding our customers expectations
while operating safely and in a socially responsible
manner. In fact, 2013 marked a record year for
us in both safety and environmental performance.
Thanks to decisive actions taken over the past
several years by our employees  the best in the
industry  Vulcan is positioned for better performance
in any phase of the economic cycle and
especially in a recovering construction economy.
The significant and sustainable reductions in our
cost structure over the past few years have been
designed to allow our operations to participate fully
in this recovery as it happens, at higher margins 
and to bring with it additional improvements in
unit profitability and significant earnings growth.

And we are not standing still. Throughout the year,
and into early 2014, we made significant progress
on our long-term strategy to drive growth, profitability
and shareholder value by being the leading
aggregates producer in the faster-growing regions
and urban markets of the United States.
IMPROVED MARKET CONDITIONS
Demand for our products continues to benefit from
recovery in private construction activity, particularly
residential construction, in most of our key markets.
We realized strong volume growth across all of our
segments in the second half of 2013. Growth in
residential construction activity, and its traditional
follow-on impact on private nonresidential construction,
continues to underpin our expectations
for future volume growth and earnings improvement.
Pricing in our aggregates business also
continues to benefit from an improving demand
outlook. We are realizing price improvements
across most of our markets.
As you can see by the accompanying charts, the
outlook for our business continues to improve, and
we have delivered on the commitments we made.
CORE AGGREGATES STRATEGY
We have continued to make strong progress on
our core aggregates strategy. In February 2012, we
made a commitment to enhance profitability, divest
non-strategic assets and reduce debt. We launched
a Profit Enhancement Plan to improve run-rate
profitability
by $100 million and a Planned Asset
Sales program to generate at least $500 million
from the sale of non-strategic assets.
I am proud to say that we have exceeded those
targets. Since announcing the plan, we have
increased Adjusted EBITDA by $116 million and
have completed actions that have generated more
than $1 billion from the sale of non-strategic assets
and from future production sales agreements. We
also have reduced debt by almost $800 million.
In our most recent strategic initiative, we sold our
cement and concrete businesses in the Florida
area to Cementos Argos for gross cash proceeds of
$720 million. We retain all of our aggregates operations
in Florida, and through a long-term supply
agreement continue providing aggregates at market
prices to the divested concrete facilities.
Divesting these non-strategic cement and concrete
assets at a full and fair valuation enhances our
financial strength and advances our strategic
focus as the leading aggregates producer in the
fastest-growing regions of the United States.
The transaction closed in early March of this year.
Concurrent with this announcement, we initiated
a tender offer to purchase $500 million of outstanding
debt. This tender offer will reduce
annual pretax interest expense by approximately
$32 million and increase annual net earnings by
approximately $0.15 per diluted share.
With the growing recovery, and backed by the
measures we have taken throughout the prolonged
downturn, we have embarked on a course of strategic,
measured growth that we believe will benefit
our shareholders for many years to come.

We remain deeply committed to building shareholder
value through the execution of our core
aggregates strategy. We further strengthened our
aggregates business in 2013 through the acquisition
of reserves and quarries in growing markets in
California, Georgia, Texas and Virginia.
For example, on the West Coast, we exercised an
option in December to purchase land, previously
operated under a quarry lease, containing
136 million tons of aggregates reserves serving
attractive San Diego markets. This purchase eliminates
a $7 million per year lease payment, while
allowing us to defer a significant portion of the cash
taxes from the Argos transaction.
We also began mine development at our Azusa Rock
Quarry, east of Los Angeles, California  the culmination
of years of cooperative work with the City
of Azusa, regulatory officials and local stakeholders.
This strategically located source of reserves in the
San Gabriel Valley contains over 100 million tons
of quality granite aggregates, allowing us to supply
critical infrastructure projects and commercial
development in Greater Los Angeles for decades
to come. The Azusa Rock Quarry also will generate
millions of yards of overburden material that
will be used to complete reclamation of a 90-acre
former sand and gravel mine we own in nearby
Irwindale, for large-scale commercial development.
These initiatives are emblematic of our long-term
strategy of expanding our aggregates operations
and securing reserves in the fastest-growing regions

of the United States, while also building upon our
significant and valuable land holdings in ways that
benefit our shareholders, our employees and our
host communities.
OUTLOOK FOR 2014
Our positive momentum should continue this year.
Aggregates demand is expected to grow in each of
our end markets in 2014, driven primarily by private
construction activity and large infrastructure and
private industrial projects. Due to our focus on the
fastest-growing regions, we expect that aggregates
volume growth in Vulcans markets will exceed
national averages.
Overall, we expect our aggregates volumes will
increase 4 to 7 percent in 2014, compared to 2013,
and that pricing will increase by 3 to 5 percent. This
outlook  combined with expected growth and
earnings improvement from our non-aggregates
businesses; stable selling, administrative and general
costs; and significantly reduced interest expense
from our debt reduction efforts  positions us to
deliver strong results in 2014.
A GREAT FOUNDATION FOR GROWTH
We are capitalizing on the growing market recovery,
and are fully prepared to meet growing demand.
Our ability to convert incremental sales to cash flow
and profits is exceptionally strong. We have reduced
our debt and improved our credit metrics substantially,
giving us greater flexibility to invest for
growth and return capital to shareholders. With
this in mind, we are pleased to report that our dividend
increase announcement in February begins
the process of restoring a meaningful dividend,
based on our improving operating earnings and
cash flows, strengthening balance sheet and the
favorable outlook for our business. Throughout
this period, we have continued to demonstrate
industry-leading operational expertise and unit
profitability.
Vulcans fundamental strengths are intact. We
benefit from an outstanding workforce, strong
and loyal customers, the best production locations
and reserves, and superior aggregates cash gross
profit per ton.
Our employees have delivered superior results in
our aggregates business despite external economic
challenges. Vulcan people are the very best in the
business. Their focus and efforts position Vulcan
for superior performance and results.
Thank you for your continued interest and support.
We see a great future ahead.
Donald M. James
Chairman and Chief Executive Officer
March 14, 2014