                                                                   Fellow Shareholders
                                                                   and Employees


                                                                   2008. We stepped back and tried to clearly understand the
2004 was a very good year for Clorox.
> Sales grew 4 percent to $4.3 billion.                            kind of company we are, where we excel, and how we will win
> Eight of the 10 business units grew volume.                      in the future. I want to share some of our conclusions with you.
> We generated more than $100 million in cost savings.
> We achieved a top-tier operating margin at just over             What Is Clorox?
  20 percent of sales.                                             With our fairly broad portfolio of brands and categories, I am
> Earnings grew 15 percent to $2.56 per diluted share.             often asked, "What is Clorox?"
> We earned 14.5 percent return on our invested capital.
                                                                   In my view, Clorox is simply the best company in our business
                                                                   at building big market-share brands in midsized categories...
2004 was also a very good year for Clorox shareholders.
                                                                   brands that make people's lives easier, healthier and better.
> Total shareholder return grew 29 percent.
  T

                                                                   Let me break that down further, because it contains some
Importantly, the year played out much as we anticipated. We
                                                                   important ideas.
needed to improve our consistency and predictability -- and
we did, delivering against both the year's business and finan-     First of all, market share is a very important scorecard. It's a
cial plans.                                                        measure of how consumers vote when they choose among
                                                                   competing products. And it is an essential key to profitability.
As I look back over the year, I'm very proud of what Clorox peo-
                                                                   With customer consolidation and consumer fragmentation
ple accomplished. Their focus on a few key priorities drove our
                                                                   today, the rewards go to brands with big shares in their cate-
strong performance. Everyone did a great job.
                                                                   gories. Most of our business today is in big-share brands like
So, how does the future look? During 2004, we engaged in a         Clorox  bleach, Kingsford  charcoal, Brita  water filtration,
major effort to develop our strategic plans and goals through      Glad bags and wraps and many of our other brands.




 Our business is also focused on midsized categories. Typically,
                                                    .T               We have brands like Clorox disinfecting wipes, which kill germs
 these are not huge, global categories with large global com-        to help make life healthier...like Glad  Press 'n Seal TM, which
 petitors. They tend to have a more regional geographic profile,     makes protecting food easier...and Hidden Valley with its great
                                                                                                               V
 for example, the United States, and generally have only one or      Ranch flavor, which makes life just a little better. Our focus on
 two major competitors. They are a sufficient size to support our
                                    uf                               delivering these kinds of benefits with brands that have big
 business model. At the same time, they are not so unique or         shares in midsized categories...that's what Clorox is all about.
 focused that they're niche categories and too small for us. And
 in those few areas where we participate in somewhat larger          Our Portfolio -- "Creating the Right to Win"
 categories, our goal is to stake out strong positions in smaller,     e are also all about winning...with consumers...with cus-
 defendable segments, such as Hidden Valley  in the Ranch           tomers...with shareholders. Every business needs to have a
 segment of the salad dressing category.                             "right to win." What does that mean? It means owning a
                                                                     strong, advantaged market position that delivers and sustains
 We succeed because we bring big-company capabilities to
                                                                     exceptional returns. We expect to be the No. 1 or a strong No. 2
 these midsized categories in areas like technology, innovation,
                                                                     brand in each and every business. Weak brand positions that
 consumer marketing, retail customer capability and strong cost-
                                                                     don't have the potential to win in the marketplace will not have
 structure management. The results are leading shares and con-
                                                                     a place in our business.
 sistent returns.
                                                                     As a part of our strategy work this past year, we evaluated our
 How do we compete in our eight major categories? We focus
                                                                     portfolio of brands, businesses and countries. That includes
 on building brands that make people's lives easier, healthier
                                                                     our business in the United States and our businesses in Latin
 and better.
                                                                     America, Asia-Pacific and other geographies. Each one needed





 to justify its place in our portfolio and why it is winning or how   Now, there is also a small part of our portfolio that we will opti-
 it can win in its competitive environment. That review con-          mize for value. That means we will manage these businesses
 firmed our belief in the strength of this portfolio. Nearly all of   for cash or we could potentially exit them.
 the businesses in our portfolio around the world have or can
                                                                      The bottom line...we have a strong portfolio. We will stratify
 create the "right to win."
                                                                      our investments more aggressively and we will focus on our
 Importantly, that does not mean that we are going to invest          "right to win" in each business.
 equally in each business. We aren't. We have made a con-
 scious choice to invest to create the "right to win" in some         Winning With Corporate Capabilities -- "The 3Cs and 3Ps"
 areas where we believe that investment will yield solid results      On its own, the strength of the portfolio is not enough. A great
 in growth and profitability for us and our shareholders over the     deal of our work over the past year focused on identifying areas
                                                                                                                           f
 next several years. The Glad products business is an example        where we have or can create "corporate capabilities" that give
 where we are investing heavily to introduce new "gamechang-          us further ability to win in our categories.
 ers" that we believe will generate long-term profitable growth
                                                                      We've identified six key strategic capabilities. We call them the
 and a stronger market position.
                                                                      3Cs and the 3Ps. The 3Cs (consumer, customer and cost) are
 There are other businesses that are equally focused on winning,      our core business capabilities. The 3Ps (people, process and
 but where we are consciously restricting our investment...some-      partnerships) are enabling capabilities. In other words, they help
 times because we already have strong defendable positions...         us deliver our business strategies. Here's a quick overview:
 and sometimes because other brands and businesses simply
 of
 offer better returns or opportunities.




                                                                        Over the past decades our success with customers has pri-
 Consumers -- "Begin and End With The Consumer"
 Identifying and satisfying consumer needs with brands and              marily been built on the strength of our No. 1 and our strong
 products is at the heart of everything we do. We simply need           No. 2 leadership brand positions. That is still true, but it's no
 to be better in this area than the competitors in our categories.      longer sufficient.
 Ultimately, we believe this is about having world-class con-
                                                                        We must also provide our customers with an integrated port-
 sumer insights -- our own unique capability to deeply under-
                                                                        folio of high-impact, value-added, supply and demand-build-
 stand our consumers. We then deploy those insights to
                                                                        ing services...like category management, creative customer
 develop consumer-preferred new products, to build our retail
                                                                        comarketing programs, expertise in supply chain and financial
 customers' categories with our brands, to better target con-
                                                                        analysis and consumer insights unique to a retailer's shoppers.
 sumer segments and to communicate with consumers. We're
 devoting a good portion of this annual report to our consumer          That's really what our customer strategy is all about: pairing our
 ef
 efforts, including insights, innovation and brand building. After      leadership brands and the right set of our innovative ideas and
 all, Clorox begins and ends with the consumer.                         services with our customer's strategies so that we both grow.


 Customers -- "Grow Our Customers' Categories With                      Cost -- "Cut Costs and Enhance Margins"
 Our Brands"                                                            Cutting costs has been a critical focus at Clorox for several
 Our customers, that is, our retail partners, play a critical role in   years. Through our strategy to cut costs everywhere (CCE),
 helping us reach the consumer. Simply said, our business is            we've had some remarkable results. In fiscal 2003 and 2004
 good when our customers' businesses are good.                          alone, Clorox employees generated more than $275 million in
                                                                        cost savings.




                                                                       People -- "Create the Right to Win Through Clorox People"
Some people have asked whether there are any further cost-
saving opportunities. Absolutely. There are still substantial          Clorox has great people. They're passionate, smart, hard work-
cost savings in our future. But we're taking our program to the        ing and dedicated. Staying at the top of our game means con-
next level. We will still cut costs everywhere; but we are now         tinually raising the bar of performance. People development is a
also keenly focused on driving productivity and enhancing              top priority.
margins as an important part of our efforts. We will now call
                                                                       One key effort will be to develop leaders by using what we call
                                                                            
the strategy cut costs and enhance margins (CCEM).
                                                                       the Clorox Leadership Model. We are convinced this will improve
We've identified numerous projects that cut across the bound-          our leadership capabilities and our bench strength over time.
aries of our businesses. Examples include capturing the real           Over the next several years, we will ensure that we have the
value of our Project Delta enterprise resource systems imple-          strong leaders and the diverse company culture that we need to
mentation through more efficient processes and decision-mak-
                     e ef                                              drive top-tier performance...to have the "right to win."
ing tools. We are also standardizing best-in-class manufacturing
tools and processes to eliminate waste in our supply chain and         Process -- "Deliver Better Results Faster Through
drive productivity gains throughout manufacturing operations.          Process Discipline"
That is a big opportunity. We've prioritized and sequenced these       When you get down to basics, there are four things we do as
and other major projects so we can deliver consistent cost sav-        a company. We build brands with consumers. We build busi-
ings and margin improvements over the next several years.              ness with customers. We create new and better products.
                                                                       And we ensure the supply of our products. These are our
                                                                       major processes.




 With the successful completion of the Project Delta systems         invented here" culture to one that is constantly on the prowl for
 implementation, we're now establishing new and higher per-          ideas and technology from other sources. We now have close
 formance standards in these major processes along with clear        partnerships with strategic suppliers, universities and even folks
 metrics and accountability for delivering enhanced results.         who compete with us in other categories. We developed a
 We're also working hard to be sure that we have real-time           "breathable" food storage bag for freshness with Dow. We
 actionable information. Ultimately, this is about having a seam-    licensed the Teflon trademark from DuPont to communicate the
                                                                                eT
 less organization -- one that works effectively across the          nonstick properties of new Clorox  bathroom cleaners. We
 boundaries of businesses and functions. Process discipline is       formed a Glad products joint venture with Procter & Gamble to
 a meaningful enabler of improved performance.                       access new plastics technology.

                                                                     But we will move even further in the partnering arena. We are
 Partnerships -- "Leverage Partnerships to Drive Growth
                                                                     pursuing new relationships to ensure that our cost structure
 and Scale"
                                                                     matches or beats our competitors in every category, whether
 One of the questions I sometimes get asked is how Clorox can
                                                                     it's production, distribution, procurement, marketing or any
 compete in a business where some of our competitors are
                                                                     other area. We believe teaming up with the right partners in a
 larger. Over the past several years, Clorox has been building
                                                                     variety of ways is a key to creating the "right to win" in our
 an important partnering capability. In other words, leveraging
                                                                     businesses over the long term.
 outside relationships that can help us grow and give us what I
 call "virtual scale."
                                                                     The Path Forward
 Clorox Research and Development is an excellent example.            So the strategy work really comes down to three things. First,
 Over the past several years, the function has transformed a "not    we have confidence in our portfolio of brands and businesses,






and we will aggressively segment and allocate investments to             And at the end of the day, we expect to deliver top-third total
the best opportunities. Second, we will compete increasingly             shareholder return in our peer group...because ultimately, the
on corporate capabilities and we will focus on the diligent exe-         strategy is all about performance.
cution of our consumer, customer, cost, people, process and
                                                                         I'm proud of the results Clorox people delivered in fiscal 2004,
partnership choices. Third, we expect to deliver top-tier finan-
                                                                         and we are excited about our future. I look forward to report-
cial performance versus our peers.
                                                                         ing on our progress.
So, what does Clorox look like in 2008? (1)
                                                                         Sincerely,
> We expect sales to grow 3 percent to 5 percent each year,
  excluding acquisitions.
> We expect to have solid expansion of both our gross mar-
  gins and operating margins.
> We expect to deliver earnings-per-share growth at a rate
  higher than operating profit growth.
> We expect that return on invested capital should increase to
  a range of 16 to 17 percent by 2008.
                                                                         Gerald E. Johnston
> We will continuously improve our consistency and pre-
                                                                         President and Chief Executive Officer
                                                                                                        f
  dictability.
                                                                         August 30, 2004
> We will improve our corporate capabilities to sustain our right
  to win.
> We will look at sensible extensions of our portfolio and our geo-
                                       
  graphic presence to ensure continuing growth beyond 2008.

                                                                     