DEAR FELLOW SHAREHULDER,
Im happy to report that Compuware delivered a strong 17 percent year-over-year increase in Earnings Per
Share in fiscal 2009, despite this year's economic pressures. Our targeted cost-cutting initiative reduced
Compuware's expense run-rate by more than $179 million this year. And our fundamentals remain outstanding,
as Compuware finished the year with no debt and with operating cash flow of approximately $232 million. We
delivered these operating achievements while substantially transforming our business and positioning it for
future growth.
At the beginning of fiscal 2009, we launched our Compuware 2.0 initiative. This transformational effort
created a foundation for accelerated growth by positioning Compuware to become best in the world in select
market categories. Through this initiative, we are transitioning our Professional Services business to a leaner,
higher-margin practice and have divested our noncore Quality Solutions business. We have established the
right strategy; implemented a new and highly focused operations leadership team; strengthened processes and
metrics; and focused our people and our capital on differentiated solutions that will lead the markets where we
participate, particularly in the area of application performance. Through these efforts, we have put Compuware
in a position where our potential for success has never been greater.
Moving forward, our Compuware 2.0 transformation will continue based on five fundamental principles:
- ValueWe do things that are important, strategic and differentiated enough
to produce economic value for customers and drive revenue for Compuware.
' FocusTo deliver maximum value to our customersand growth for our
shareholders and employeeswe focus where we are best in the world.
~ AlignmentBy clearly aligning all of our resources behind a compelling goal,
we have created a more leveraged and efficient attack on our target markets.
- ReferenceabilityCompuware will produce undeniable and public
success with referenceable customers.
- SimplicityWe will scale our business by organizing the company to deliver
simple, complete solutions that solve strategic business and technology problems.
These Compuware 2.0 principles provide a robust framework for our growth. This year, we will capitalize on that
framework to extend our global leadership in providing flawless application performance through our Business
Service Delivery (BSD) approach. By providing a detailed perspective on both the progress of new projects that
enable business growth and the health of existing operations, Compuware BSD solutions give Cl0s and business
leaders the confidence that applications will work well and deliver value, whatever the technologies in use. This
value proposition increases the strategic relevance of our company, allowing us to sell higher and more broadly
into our customer base. It also positions us in large, highgrowth markets where key analysts recognize our
leadership positions in areas such as enduser experience monitoring.




Each of our core lines of business plays a key role in this long-term strategy. Our mainframe business
remains best-in-class, and we continue to invest in these solutions to increase our competitive advantage.
We will capitalize on the strength of our mainframe position to build market leadership in our Vantage and
Changepoint distributed solutions. In particular, I expect our focus on optimizing endtoend application
performance will pay off exponentially. Heres why.
Our customers face tremendous challenges in getting value from the investments they make in applications.
When IT commits to delivering a service, it is doing more than just implementing and managing technology.
IT makes a promise: Bring more speed, agility and innovation to the business, cut costs by automating
processes, improve the customer experience and foster greater satisfaction and loyalty.
There is a fundamental shift occurring in computing architectures, and to uphold these promises, many
IT organizations adopt disruptive technologies like web services, virtualization and cloud computing. These
technologies add complexity and tax already scarce IT resources.
Overcoming these challenges requires an endtoend perspective on application performance, and Compuware
is unmatched in this category. To further expand our reach in delivering these solutions, weve also established
a structured strategic partners program. This organization will cement global partnerships with systems
integrators, consultants and IT solution providers to jointly meet the business needs of our customers.
Covisint continues to gain market share and has established a leadership position in secure healthcare
collaboration. Our partnership with the American Medical Association represents an inflection point in
Covisints growth curve. With healthcare stimulus spending and IT investments at an all-time high, Covisint's
ability to deliver access to critical yet dispersed healthcare information in a secure, single sign-on collaboration
environment will produce explosive growth in the years to come. Covisint is positioned well to help physicians
and state governments get their fair share of the $19 billion federal government healthcare stimulus package.
Supplementing the success l expect in our core solutions, the dramatic transformation of our Professional
Services business will continue this year. By focusing on a consistent set of offerings where we see high
demand and have differentiated competencyareas like QA; Governance; Requirements Management;
Business Intelligence and Data Management; Architecture and Development; and Operations Management
we will increase the scalability and profitability of this organization.
Our accomplishments this fiscal year give me great optimism for Fiscal 2010. We have reinforced our strong
commitment to value, established a focused strategy and aligned the organization to achieve that strategy.
Through reference customers and a simpler business model, we are creating a pull in the marketplace for
Compuwares BSD solutions. I expect our differentiated ability to optimize application performance will drive
breakout growth in the years ahead, to the benefit of our shareholders, customers and employees.
Sincerely,
 
Peter Karmanos, Jr.
Chairman and CEO
Compuware Corporation



