TO OUR
SHAREHOLDERS

The Company�s strong
performance in 2018
continued to demonstrate
our steadfast commitment
to building the franchise
for the long-term, and
nurturing a culture
defined by our guiding
principles to: deliver
outstanding results, care
for customers, support our
communities and succeed
together.
The success of this strategy
is evident; over the past
ten years People�s United
has more than doubled total assets to $48 billion,
increased net income at an average annual rate of 13
percent, and maintained exceptional credit quality,
while deepening our presence across the Northeast
and expanding national businesses. Our Company�s
culture is the foundation upon which these results were
achieved and reminds us that a successful approach
with our customers, communities and colleagues is one
that reflectsthe values we embrace.
Delivering Outstanding Results
In 2018, we further positioned the franchise
for sustainable growth, with investments in
revenue-producing initiatives and talent, as well
as enhancements to our digital capabilities and
technology infrastructure. While making these
important investments, we continued to strengthen
the profitability of the Company. Full-year net income
increased 39 percent from a year ago to $468 million,
the highest in the Company�s history. In addition,
net income per common share of $1.29 increased for
the eighth consecutive year. Our commitments to
effectively control costs, maintain a conservative and
well-defined underwriting philosophy and a diversified
business mix, helped the Company achieve improved
returns despite experiencing subdued loan growth in
certain segments. As such, we were pleased to report
a return on average assets of 1.04 percent, and a return
on average tangible common equity of 14.3 percent,
increases of 25 basis points and 330 basis points,
respectively, from the prior year.
We also remain focused on balancing organic growth
with thoughtful acquisitions. Since 2008, we have
acquired and integrated eight bank franchises,
four specialty finance companies, two insurance
brokerages and a wealth management firm. We 

recently welcomed First Connecticut Bancorp, Vend
Lease and VAR Technology into the People�s United
family. The addition of First Connecticut bolsters our
well-established presence in Central Connecticut
and Western Massachusetts. Vend Lease and VAR
Technology are both leading equipment financing
businesses in their industries and further strengthen
our nationwide network of specialty finance experts.
We also expect to close the acquisition of BSB Bancorp
early in the second quarter of 2019. The BSB Bancorp
transaction will deepen our presence in the Greater
Boston area, particularly in the suburbs west of the city,
which are attractive banking markets.
Our prudent capital management has enabled us to
grow the business organically and invest strategically in
the franchise, while also providing a consistent return of
capital to you, our shareholders. In 2018, we were very
proud to announce the Company�s 25th consecutive
annual common dividend increase.
Caring for Customers
At People�s United, customer relationships define our
brand and span generations. These long-standing
relationships are the result of our diverse product and
service offerings, expert guidance, tailored solutions
and, increasingly, the integration of digital capabilities
with our Branch Banker expertise. In 2018, the Company
embarked on a strategic initiative to adopt a suite of
online and mobile banking technologies across Retail,
Commercial and Wealth Management business lines,
to create a more personalized customer experience.
We believe by evolving how the Company interfaces
with customers, we can create even closer customer
connections.
In recognition of the customer relationships we�ve
developed, and the collective success we have achieved
in helping our customers grow their businesses,
Greenwich Associates granted People�s United seven
awards in 2018, including three National and three
Northeast awards for Excellence in Middle Market
and Small Business Banking. We were the only New
England-based bank to receive a Greenwich Best
Brand � Trust award, as rated by customers for our
support of small businesses across the Northeast.
Supporting Our Communities
At People�s United, community giving has long-been an
integral part of our culture. Since inception of our two
foundations, People�s United Community Foundation
and People�s United Community Foundation of Eastern
Massachusetts, more than $32.5 million has been
awarded acrossthe Northeast.

As we have grown the franchise, we continue to
expand our community partnerships. In 2018, these
partnerships increased by nearly 20 percent as our
foundations provided grants to over 600 non-profits.
These grants, totaling more than $3.8 million, supported
important community programs focusing on three key
areas: youth, community development and affordable
housing. People�s United also provided nearly $5.3
million in charitable donations, sponsorships, and
employee contributions, which included more than
31,000 employee volunteer hours, generating an
economic impact approaching $1 million.
People�s United continues to be a strong supporter of
economic and community development, and a provider
of extensive lending, investment and banking services
in our markets. In 2018, we provided $130 million in
equity investments in affordable housing and SBA
loans. In total, donations, investments, and employee
contributions through the Bank and its Foundations
resulted in an impact of $139 million for the year.
In 2018, we also established an important ten-year
partnership with Quinnipiac University to support two
prominent centers in the School of Business, the Center
for Innovation and Entrepreneurship and the Center
for Women and Business. Together, we look forward
to leveraging many synergies to hold events, and help
educate and deliver value to students, faculty and the
entire community for years to come.
Succeeding Together
At People�s United, employees are among our most
valuable assets and are the driving force behind our
success. We are committed to educating and providing
employees with the tools they need to achieve their
professional goals and deliver an extraordinary
customer experience.
We continued to advance the skills of our managers
through our leadership development programs. These
programs focus on coaching and mentoring managers
to become effective team leaders and drive measurable
growth. We also introduced The Learning Connection,
an online portal with on-demand courses on business
and professional development topics, including content
from our partners at Quinnipiac University.
The Company�s Women in Leadership Group continued
to expand, growing to more than 1,300 members across
ten chapters since launching in 2015. The mission of
the Group is to promote diversity and inclusion by
welcoming all People�s United employees to participate
in building strong relationships � through women and
for women � to spark collaboration, foster relationships,
support personal and professional development
and encourage volunteerism. In 2018, we hosted
educational events for employees, clients and prospects
that focused on women and investing. Partnering
with Gerstein Fisher and People�s United Advisors, we
delivered seminars covering the basics of investing and
what differentiates women in their decision making.
We were also named to Forbes annual list of �America�s
Best Employers� for 2018. As we continue to build a
company where the best employees want to work and
establish rewarding careers, this honor is a reflection
of the company values that have been instilled in our
brand for more than 176 years, and carry throughout
the Bank at all levels.
In Conclusion
Our approach to forging deep relationships with
customers, supporting communities, and investing in
employees, continues to differentiate People�s United
not only in the markets we serve, but also nationally.
We were recently recognized by Barron�s as one of the
top 100 most sustainable U.S. companies. People�s
United was ranked #5 among financial institutions and
#41 overall based on assessments of more than 200
performance indicators in five categories: shareholders,
employees, customers, planet and community. We are
very honored to receive this noteworthy recognition
and it is a testament to our unwavering commitment
to the Company�s guiding principles.
Looking forward, we expect the upcoming year to
be similar to 2018 in terms of economic and political
uncertainty. In particular, uncertainty around Federal
Reserve monetary policy and the ultimate impact on
the yield curve will likely be the prevailing themes for
banks this year. However, our strategy of building the
business for the long-term, driven by a deeply rooted
culture will further enable us to move the Company
forward and deliver outstanding results, regardless of
these uncertainties. As such, we remain confident in our
ability to deliver shareholder value in the years ahead.
Thank you for your continued support.
Sincerely,
Chairman and Chief Executive Officer
People�s United Financial, Inc.
March 1, 2019