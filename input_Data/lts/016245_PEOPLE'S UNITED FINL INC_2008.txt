Dear Shareholder:
 The year 2008 was a positive, significant and evolutionary 
one for Peoples United Financial. Our acquisition of
the Chittenden Corporation and its six bank brands on 
January 1, 2008 positioned us decisively as the premier 
New England-based regional bank franchise. In stark 
contrast, the year has proven to be one of the worst ever 
for the American banking sector and, in fact, for financial 
services worldwide. A shockingly large number of previous 
icons of the financial system did not survive the year. 
A Bank to Bank On
 Our performance throughout 2008 reflected our 
fortress balance sheet and strong asset quality. It also
highlighted our strategic decision to value the security 
of our excess capital over the potential to increase 
short-term earnings at the risk of future capital loss. 
In the wake of the economic and financial sector turmoil 
throughout 2008 and into the early part of this year, 
our fortress balance sheet and asset quality are more 
distinctive than ever.
 The past year was also one in which Barrons magazine, 
in an article featuring Peoples United (August 25), hailed 
us as a bank to bank on. In the article, Barrons noted 
that the company is so well-capitalized that its hunting 
for acquisitions, as other lenders are watching their 
share prices and franchises shrink.
Business Growth and Strong Asset Quality
 Our principal accomplishments in 2008 included: 
substantial progress in integrating Chittenden; steady 
growth of strategic assets, specifically, commercial 
and home equity loans; and the preservation of our 
strong asset quality.  
 We achieved and exceeded cost efficiencies associated 
with the acquisition that we spelled out earlier in the 
year and consolidated the six northern New England 
banks charters under that of Peoples United Bank 
on January 1, 2009 as part of our efforts to reduce 
complexity and risk while, at the same time, lowering 
costs. These banks continue to operate within their 
respective markets, under their own names, as divisions 
of Peoples United Bank.
 Our focus on exceeding customers expectations led 
to continued business growth in 2008, particularly in our 
commercial and consumer loan portfolios. Indeed, we are 
appreciative of our many new  as well as long-standing  
customers to whom we provided financing last year. 
Asset quality remained high, with full-year 2008 
charge-offs fiat compared to 2007 at 10 basis points, 
a level dramatically lower than our peer group. Though 
clearly facing a challenging environment, we still find 
that there are many good loans to be made and we 
certainly have the capital with which to make them.  
 Another significant development in 2008, and 
a fitting recognition of your companys performance, 
was the addition of Peoples United to the S&P 500 index 
in November. I would also note that we decided not to 
participate last year in the Treasurys Capital Purchase 
Plan, part of the Troubled Asset Relief Program (TARP).  
We continue to feel that it was the correct choice for us 
and do not expect to participate in the future. In short, 
given our extremely well-capitalized position and strong 
asset quality, the cost-benefit trade-off of participating 
makes no sense for us.
2008 Financial Performance
 Looking at our overall financial picture for the year 
ended December 31, 2008, net income totaled $139.5 
million, or $0.42 per share, compared to $150.7 million, or 
$0.52 per share, for 2007. Included in the 2008 results 
are one-time items mostly tied to our acquisition of 
Chittenden, the net impact of which reduced 2008 net 
income by $33.2 million, or $0.10 per share. Results 
for 2007 included a $60.0 million contribution to The Peoples United Community Foundation, which had the 
effect of reducing net income in 2007 by $39.6 million, 
or $0.13 per share. Excluding the effect of these items 
from the respective years results, earnings would have 
been $172.7 million, or $0.52 per share, in 2008, compared to $190.3 million, or $0.65 per share, in 2007.
 The total return on our stock of 3.4% for 2008  
a year when most banks experienced significant declines 
in stock value  reflected the strong performance of 
Peoples United relative to its peers. In fact, our peer 
group, as represented by the KBW Bank Index, declined 
46% in 2008. We were the second highest-performing 
bank in the index for total annual return, including dividend 
reinvestment, during 2008, and are number one for 
three- and five-year total returns. It is also worth noting 
that we maintained our dividend throughout the year, 
while many of our peers cut theirs sharply, or eliminated 
it altogether.
 As we have said before, we continue to weigh 
all of our options to determine the most attractive 
use of capital to deliver long-term shareholder value. 
At this time, we still feel the best use of our capital is 
to have the patience to find and execute a well-priced 
acquisition that will enhance the long-term profitability 
of the company. We do not feel under pressure, 
therefore, to immediately deploy the excess capital 
in either a poorly-priced transaction or an aggressive 
stock buyback.  
 At the same time, it bears noting that the significant 
asset sensitivity of our balance sheet, amplified by the 
$2.5 billion in excess capital invested in short-term 
instruments, puts pressure on current earnings, particularly 
with interest rates at historic lows. We recognize that 
the ongoing performance of the core bank, as well as 
how effectively we ultimately deploy our excess capital, 
are critical to our long-term value proposition.
Looking Forward: 2009
 As we look forward to the remainder of 2009, 
we will continue to pursue prudent growth while remaining 
keenly aware of the major drivers that will infiuence our 
performance this year. The most significant of these, 
given our highly asset-sensitive balance sheet, is the 
interest rate environment.  
 Credit quality will certainly be the watchword for 
the financial services sector in 2009. We continue to 
feel that the quality of our loan portfolio remains high 
and we do not currently see any systemic issues across 
our portfolios. Given the current depressed state of the 
economy, however, we also do not feel we will necessarily 
be permanently immune to declines in asset quality.
 Loan growth will remain principally driven by 
commercial lending. This growth will be partially funded 
by a decline in our residential mortgage portfolio as 
our customers continue to pay down their outstanding 
balances and we continue our strategy of selling the 
mortgages we originate.
 Fee income is another area that will likely reflect 
the broader economic environment in 2009. Ongoing 
weakness in the equity markets has already had a 
negative impact on the level of assets under management 
in our Wealth Management area. We intend to maintain 
a keen strategic focus on this business.
 In closing, while we are sure to face challenges 
in the months to come, we find ourselves in an enviable 
position relative to our peers. At a time when capital 
and asset quality remain strong determinants of 
shareholder value, we continue to out-perform on both 
fronts. Our ability to consistently do so will, we believe, 
prove beneficial to our customers, communities, and, 
of course, you, our shareholder. We thank you, as 
always, for your continued support.
Sincerely, 
Philip R. Sherringham
President and Chief Executive Officer