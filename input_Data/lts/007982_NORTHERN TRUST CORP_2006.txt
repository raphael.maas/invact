Not only did Northern Trust 
achieve record financial results, 
we laid a foundation in a number 
of key strategic areas for substantial 
growth in 2007 and beyond. 

Clients are at the center of everything 
we do. Since 1889, Northern Trust has 
distinguished itself by our commitment 
to exceptional client service and the 
importance of building and maintaining 
strong, long-lasting relationships. Our 
clients' needs drive every product and service decision 
we make and the location of every office we open. 
This client centered focus has been integral to Northern 
Trust's dramatic growth over the years. By remaining true 
to our focused business strategy and to the principles upon 
which we were founded - a commitment to unquestioned 
integrity and honesty, a dedication to excellence in client 
service, a continuous search for innovative solutions and 
a passion to improve the communities in which we do 
business -- Northern Trust has evolved into a dynamic 
global financial services enterprise. 
RECORD PERFORMANCE 
In 2006, we achieved record net income per share of $3.00, 
compared with $2.64 in 2005, an increase of 14 percent. Net 
income grew 14 percent to $665.4 million, compared with 
$584.4 million earned in the previous year. 
Total revenues increased 14 percent to $3.06 billion, with 
trust, investment and other servicing fees, foreign exchange 
trading profits, and net interest income 
exhibiting double-digit growth for the 
year. The Corporation maintained 
its excellent financial condition and 
increased the quarterly cash dividend per 
common share by 8.7 percent to 25 cents 
per share for a new annual rate of $1.00. 
This marked the 110 th
consecutive year of dividends paid. 
KEY ACCOMPLISHMENTS 
2006 was a very significant year for our business. Not only 
did Northern Trust achieve record financial results, we laid a 
foundation in a number of key strategic areas for substantial 
growth in 2007 and beyond. 
In Personal Financial Services, working with Northern Trust 
Global Investments, we completed numerous initiatives 
that enable us to provide blended investment solutions to our 
clients -- bringing to the market the best of Northern Trust's 
investment capabilities along with investment products from 
outside managers. Since the middle of 2006 when this new 
platform was completed, our new business has grown 
substantially. We are very optimistic that 2007 will continue 
this positive momentum. 
We also saw the strong acceptance of Wealth Passport, 
our industry leading technology platform that was developed 
just one year ago for the highly specialized needs of substantial 

families and the family offices and advisors who serve 
them. Already, more than 450 Wealth Management family 
members and family offices utilize Wealth Passport. 
In Corporate and Institutional Services, we completed 
the integration of Baring Asset Management's Financial 
Services Group hedge fund, property, and other fund 
administration in the U.K. and Ireland. We are the largest 
administrator of offshore private equity funds in Europe 
and have the largest number of funds administered in 
Dublin. We also completed at year end the migration of 
Insight Investment Management (Global) Limited's back 
and middle office investment operation to our strategic 
platform -- nearly $200 billion in assets. This outstanding 
success positions Northern Trust well for future out 
sourcing opportunities. 
We experienced dramatic growth in the Asia-Pacific region 
as we celebrated our 10year anniversary in Singapore. We 
were selected to provide global custody, investment mandate 
compliance monitoring, and performance measurement 
services to China's multibillion dollar National Social 
Security Fund -- the first institutional retirement fund in 
China to invest overseas. On January 23, 2007, Northern 
Trust announced that it was chosen to act as custodian for 
the New Zealand Superannuation Fund, which is expected 
to grow from approximately US $8 billion to approximately 
US $76 billion by 2025. 
Through our five offices in Asia - 
Singapore, Hong Kong, Japan, China, 
and Bangalore, India - we continue to 
expand Northern Trust's reach into new 
markets such as New Zealand, which 
provides a strong foundation for our 
future growth in the Asia-Pacific region. 
Our operations center in Bangalore 
opened in 2006 and supports not only 
our rapidly growing international 
businesses in the Asia-Pacific region and 
worldwide, but also supplements our global operating 
centers in North America and Europe. As of December 31, 
2006, we had more than 270 Northern Trust employees 
in Bangalore. 
Around the world, clients drive our business and guide our 
expansion strategy. We continuously adjust our client service 
models to more completely surround clients with the high 
quality service they expect and to help them manage the 
complex financial decisions they must make. As a trusted 
advisor, Northern Trust provides clients peace of mind and 
the freedom to focus on what really matters to them. 
THE OPPORTUNITIES AHEAD 
On behalf of the Board of Directors, we want to express 
our gratitude to our shareholders for their investment and 
continued confidence in our organization. 
On November 14, 2006, Northern Trust 
Corporation announced that Nicholas 
D. Chabraja, chairman and chief executive 
officer of General Dynamics Corporation, 
was elected to our Board of Directors, 
effective January 1, 2007. Mr. Chabraja's 
expertise and perspective further strengthen 
our distinguished Board. Also, we would 
like to recognize Duane L. Burnham who 
will retire from the Board of Directors 
in April 2007, after 10 years of dedicated service. His 
commitment as a Board and Board Committee member 
has been invaluable to the organization. 
As we enter 2007, Northern Trust remains a highly focused, 
global financial services firm. With an experienced staff 
of outstanding professionals, a strong balance sheet, and 
a growing U.S. and international presence, we are well 
positioned to build upon our proven strategies and to 
capitalize on the many opportunities ahead.