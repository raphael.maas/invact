Key performed well in
2004, earning $954 million,
or $2.30 per diluted
common share. That
result compares with
$903 million, or $2.12
per diluted common share, in 2003.
The total return of our shares, which
includes price appreciation and dividend
payments, was more than 20
percent in 2004. In contrast, the total
return of shares making up the Standard
& Poors 500 Banks Index was approximately
14 percent, while that of shares
making up the Standard & Poors 500
Index was about 11 percent.
Substantially improved asset quality
was the main reason for our stronger
performance. Nonperforming loans fell
to $316 million in the fourth quarter,
their ninth consecutive quarterly
decline. Net loan charge-offs as a
percentage of average loans in 2004
fell to 0.67 percent, their lowest level
since 2000.
We also continued to be disciplined
in our spending. Our costs rose less
than 3 percent in 2004.
Such results demonstrate that our
work to reposition Key is paying off
for shareholders. Over the past several
years, we have been successfully
transforming the company from a
traditional provider of bank products
to an organization that builds lasting
client relationships by delivering tailored
financial solutions.
I am more convinced than ever that
this transformation will produce
meaningful levels of revenue growth,
allowing us to overcome our companys
greatest remaining challenge.
My optimism stems from mounting
evidence that Key, indeed, is winning
with clients, as I will describe later in
this letter.
BUSINESS GROUP RESULTS
Consumer Banking
Consumer Banking earned $375 million
for the year, down 9 percent from
$412 million in 2003. The reduction
reflected the impact of Keys sale of its
broker-originated home equity loans and
the placement of its indirect automobile
loans into a held-for-sale category.
Excluding the impact of these decisions,
the group earned $452 million
for the year, nearly 10 percent higher
than the prior years result. Stronger
asset quality and aggressive cost management
drove the improvement.
The group also experienced modest
increases in loan and deposit balances,
the result of Group President Jack
Kopnisky and his teams emphasis on
profitable growth. To promote it, they
strengthened Consumer Bankings sales
capabilities, enhanced its service quality
and expanded its franchise.
Licensing 250 employees to sell
investment products is an example of
the groups many sales-related actions.
Another is work done to simplify its
consumer-checking product line: offering
four types of accounts instead of seven
will make selling easier. In addition,
nearly 40 percent of the training delivered
to the groups employees focused
on building their sales skills.
Further, new, more sophisticated
marketing techniques improved
Consumer Bankings sales efficiency.
For instance, the group fine-tuned the
content, timing and destination of
direct-mail offers  and improved the
follow-up on offers sent. Where tested,
that initiative reduced the banks cost to
acquire new clients by two-thirds.
Similarly, the group implemented a
variety of service enhancements. It
opened or reintroduced drive-up windows
at 34 KeyCenters, a number that
will continue to grow. In addition, the
group extended service hours at 389
KeyCenters. It tailored offerings to
local communities, adding mortgage
originators and check-cashing services.
Consumer Banking also modified its
recruiting practices, hiring more people
with successful track records at retailers
known for their service orientation.
Finally, Key bought EverTrust
Financial Group Inc., of Everett,
Washington, as well as 10 branch
offices and the deposits of Sterling Bank
& Trust FSB, of Southfield, Michigan.
The acquisitions complemented the
groups organic growth and built its
presence in these attractive communities,
all of which are within or abut the companys
current banking footprint.
Corporate and Investment Banking
Corporate and Investment Banking
earned $486 million for the year, up
nearly 36 percent from $358 million
in 2003. Improved asset quality helped
this group as well, as did solid growth
in noninterest income.
Client demand was especially strong
for many of the groups investment
banking and capital market solutions.
Corporate and Investment Banking,
for example, led or co-led 56 deals for
equity capital markets clients  its best
year ever. The dollar volume of those
deals moved the group to 14th place
from 25th among providers of domestic
equity capital services.
The groups ability to offer a full
menu of financing solutions helped it
offset modest demand for commercial
and industrial (C&I) loans.
Group President Tom Bunn and his
team also took several actions to
promote profitable growth in the year
ahead. Especially important was expansion
 through acquisitions and organic
growth  and efforts to improve brand
awareness.
Late in 2004, the business acquired
American Expresss equipment leasing
unit. The deal makes Key Equipment
Finance one of the nations leading
small ticket vendor leasing companies.
It also acquired American Capital
Resources, Inc., a specialty originator
and servicer of commercial mortgages,
further enhancing the groups Real
Estate Capital business.
Many of the groups national business
lines established or expanded their presence
in attractive locations throughout
the country, such as Miami, Dallas and
Los Angeles. They also hired dozens
of seasoned professionals  a testament
to Keys reputation as a highly desirable
place to work.
Finally, the group rebranded several
divisions in 2004, using either a
KeyBanc Capital Markets or KeyBank
name. The move is intended to reinforce
among clients the breadth of
Corporate and Investment Bankings
offerings and capitalize on the strength
of the companys brand.
Investment Management Services
Investment Management Services
earned $112 million for the year, up
more than 38 percent from $81 million
in 2003. Healthier financial markets
and Keys strong client focus were
among the reasons.
McDonald Financial Group (MFG),
headed by newly appointed Group
President Robert Yank Heisler,
enjoyed a 15 percent increase in the
number of high-net-worth clients it
serves, and a 14 percent increase in
average revenue per client.
Behind this strong performance was
MFGs ongoing drive to offer integrated
banking, investments and trust services.
Steady increases in the percentage of
affluent households having a favorable
image of MFG validate the appeal of its
unique approach.
That approach makes available to
each client a team of experts, led by a
financial advisor. To perfect it, MFG
trained more than 700 professionals to
use a sophisticated sales process. It promotes
the systematic exploration with
affluent individuals of issues that typically
affect them, such as insurance and estate
planning. In addition to stronger sales, its
consistent use pushed up client satisfaction
scores to 8.80, on average  ever
closer to MFGs goal of a perfect 10.
Assets under management at Victory
Capital Management rose 8 percent
during the year. Strong demand for the
lines offerings by institutional clients,
such as pension and union funds,
accounted for much of the increase.
Victorys management team, headed
by Group President Rick Buoncore,
continued building the lines deep
research capabilities and its distribution
network. They also finalized plans to
further rationalize the business product
set and to hire world-class talent to fill
pivotal positions.
Victorys aim is to concentrate on
selected equity styles and work to make
each emulate the success enjoyed by its
flagship offering, the Diversified Stock
Fund. That product received in 2004
from Lipper, a respected fund-rating
agency, its top award for lifetime performance,
from among more than 800
large-cap core funds.
CORPORATE PRIORITIES
One reason our business groups are
winning with clients is that theyre
focused on one set of corporate priorities.
The priorities support Keys drive
to grow profitable revenue by building
relationships with clients.
Growing revenue by building a sales
and service culture tops our priority
list. As a result, we are seeing increases
in important measures, such as the
number of new clients we attract, and
our ability to retain clients and to build
deeper relationships with them.
The average revenue generated by
our relationships with Corporate
Banking clients rose 3 percent, for
instance, while average core deposits,
an important indicator of our ability to
establish and deepen relationships, grew
approximately 5 percent corporatewide
during the year.
Such results stem from the growing
ability of our businesses to demonstrate
to clients the benefits of our relationship
orientation.
For example, we now act more
quickly to contact people who move to
a community served by our KeyCenters.
By calling them within one week of
their arrival, we now convert twice as
many into clients as we once did.
We also have been moving more
rapidly to identify and retain at risk
clients, such as those whose account
balances have fallen recently by a large
amount, suggesting their imminent
departure from Key. By acting quickly, we
were able to improve our retention rate
of selected Consumer Banking clients.
To deepen relationships, especially
those with commercial clients, Key has
been expanding the use of relationship
reviews by relationship managers (RMs).
RMs prepare carefully for the reviews,
and then discuss with clients ideas about
how, not whether, Key can help them.
Results have been impressive: Among
participating small-business clients, a
segment for which reviews were initiated
in 2004, revenues rose 30 percent.
Expanding through acquisition is
another corporate priority, which we
acted on in 2004, as I noted earlier.
Of special interest to us are banks
that can build share in geographic
markets where we already have a
presence. We also are interested in
organizations that can enhance our
equipment leasing, commercial real
estate and asset management
businesses. All of our 2004 acquisitions
reflect these areas of interest.
At the same time, we continue to
rationalize our business mix. By divesting
businesses with unfavorable risk
and reward characteristics, we free up
capital that can be reinvested for more
profitable purposes.
Good examples in 2004 were our
decisions, mentioned earlier, to exit the
brokered home equity and indirect automobile
businesses. Our departure from
these nonrelationship businesses, both of
which produced low risk-adjusted
returns, allows us to reallocate resources
to higher-return activities that support
our relationship strategy.
There are several new faces on Keys
management team. We welcomed in
2004 Stephen E. Yates as our chief
information officer, Charles S. Hyle as
our chief credit and organizational risk
officer, and Timothy J. King as our
Retail Banking president. These highly
qualified leaders significantly enhance
Keys performance potential.
Making sure that newcomers and
veterans alike row in the same direction
is essential. For that reason,
seasoning our leadership talent is
another priority.
Representative actions include insisting
that job rotations occur more often,
and vigorously applying our pay-forperformance
system to reward behaviors
consistent with Keys drive to grow
profitable revenue.
In addition, Key will continue its
strong expense discipline. Of course,
being frugal is not an end in itself; I
expect Keys managers to reinvest savings
wisely, to ensure a more profitable
future.
Finally, most companies stumble due
to poor follow-through rather than poor
planning. For that reason, perfecting
our ability to carry out our strategy,
or performing flawlessly, is another
corporate priority. We are paying
especially close attention to completing
our work to restore our asset quality,
improving the performance of our Retail
Banking and Commercial Banking
businesses and continuously improving
our relationship management practices
(see Keys Relationship Model, page 5).
Moreover, we are committed to
moving decision-making closer to our
clients. To that end, we are empowering
local credit officers in our banking
footprint, broadening the responsibilities
of our district presidents and
expanding teamwork among local
product experts.
STRONG FOUNDATION
Another reason our business groups
are succeeding is because they receive
outstanding support internally from
seasoned employees in disciplines
ranging from software engineering to
brand management.
These professionals made it possible,
for instance, for Key to begin settling
checks exclusively with digital images 
a first in our industry. Not only is our
imaging technology saving Key millions
of dollars annually, it also is allowing us
to provide electronic delivery of other
services our clients want.
Many of those services are delivered
through Key.com, our award-winning
internet site. The ability to pay bills,
view check images and receive statements
online appeals to many of our
clients  and is among the many reasons
they choose to do business with us.
Such work explains why Key has
been listed, for six years running,
among the nations largest and most
innovative users of information technology
by Information Week, in its
annual 500 ranking.
Our business groups also benefited
from the expertise in our marketing
organization, which was recognized by
The American Business Awards.SM
Among its many successes in 2004 was
the launch of new advertisements in
selected markets that reinforce qualities
that differentiate our company.
Keys employees are proud of these
and other achievements. The results of
our recent Employee Survey revealed
their willingness to go the extra mile
to ensure the companys success.
Further, nearly three quarters of them
own company stock and they are
increasingly recommending the companys
solutions to their friends and
families.
BOARD CHANGES
I would like to acknowledge the
contributions of Dennis W. Sullivan,
who will retire from the Board at this
years annual shareholders meeting.
Mr. Sullivan, retired executive vice president
of Parker Hannifin Corporation,
has served Key ably as a director since
1993. We will miss his participation and
wish him all the best.
I am pleased that our directors in
January 2005 increased Keys dividend
for the 40th consecutive year, an enviable
record in any industry.
A LOOK AHEAD
We are focused on improving our
performance in 2005 by growing
revenue, managing expenses and
enhancing asset quality.
Growing confidence in the power of
our relationship model only reinforces
my conviction that we will succeed 
this year, and beyond. 