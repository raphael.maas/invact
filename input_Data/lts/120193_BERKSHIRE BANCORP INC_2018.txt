Dear Shareholders, Customers and Employees:
Berkshire Bank is defined by our commitment to providing our customers and our
communities with the scale and resources of a big bank, but with local decisionmaking. This is a model that engages our employees, supports our communities,
and creates enduring returns for our investors.
Our 2018 results reflect that commitment, as we hit key earnings and operational
targets and achieved record return on assets. All are important accomplishments
during our first full year of operating above the $10 billion asset mark. Additional
highlights include:
� Improved profitability
� Full year earnings per share increased by 65% to $2.29
� Non-GAAP adjusted earnings per share increased by 18%
to $2.71
� Revenue grew 12% driven by increased product and
service offerings to our Northeast markets
� Return on assets improved by 61% to 0.90%
� Return on equity improved by 54% to 6.8%
� Non-GAAP adjusted return on tangible common equity
improved by 14% to 13.5%
� Fully integrated the operations of Commerce Bank
and Trust Company
� Announced a definitive agreement to acquire SI Financial
Group in Connecticut

BUILDING A 21ST CENTURY
COMMUNITY BANK
We are the third largest regional bank headquartered in New
England, and the largest based in Boston. Our franchise
stretches from Boston to Syracuse and from Southern Vermont
to New Jersey and Philadelphia and will soon expand to
Eastern Connecticut and Rhode Island. This scale creates great
opportunity for both outstanding customer service and new
relationship acquisitions.
In 2018, we continued the expansion of our footprint with the
seamless systems conversion and integration of Commerce
Bank and Trust Company, including their 16 branches in Central
and Eastern Massachusetts. This conversion allowed us to
secure the leading deposit position in Worcester, New England�s
second largest city, and successfully cross the $10 billion asset
threshold. We built on the success of the Commerce acquisition
and our Boston corporate headquarters move by expanding our
regional teams, creating opportunities to build both awareness
and enduring relationships. Together, our Boston and Worcester
teams increased our market share in Eastern Massachusetts,
which is now the largest regional market we serve.
In December, we announced a definitive agreement to acquire SI
Financial Group, with 23 branches serving Eastern Connecticut
and Rhode Island. This contiguous market expansion is a natural
fit for our company, allowing us to bring big bank resources, local
responsiveness and our distinctive culture to strengthen the
capabilities and offerings of the SI Financial team.
Our sustained focus on the banking relationships that fuel small
business growth continues to be an effective strategy. Our SBA
lending team at 44 Business Capital generated record volume
and revenue in 2018, and moved into the top 30 SBA producers
nationally, both by volume and dollar value of SBA loans. In
addition to our success with 44 Business Capital, we integrated
our SBA teams with our Firestone business equipment
lending operation and our Commercial lending teams to drive
synergies and revenue growth across our footprint. Our small
business lending outpaced prior year performance locally and
nationally, while helping main street businesses create jobs and
improve the economic vitality of the communities in which we
serve and operate. 

We also saw strong activity across all of our banking regions and business lines,
including retail, commercial, wealth, insurance, and in our specialty businesses.
A 21st century bank provides innovative solutions through multiple delivery
channels, reaching customers where they are. We opened two new branches in
Connecticut and New York featuring our distinctive branch layout and MyTellerSM
Interactive Teller Machines (ITM), which combines state-of-the-art virtual
technology with the human touch of traditional teller service and extended
service hours. Additionally, we expanded our team of MyBankers� who offer one
point of contact and exceptional service at no cost, for any customer interested
in a committed banking relationship. Our team of MyBankers� expands our
geographic reach and service availability with personalized concierge level
responsiveness. More information on this service and on our other offerings can
be found on our new berkshirebank.com website.
ENHANCING OUR CULTURE
Strong financial performance is only one component of a successful
corporation. A business that operates responsibly, invests in its people and
communities, and creates a best-in-class workplace will inevitably mitigate risk
and deliver higher returns.
Our internal culture has been and always will be a priority. It is the foundation
of a 21st century community bank. When our people thrive, our company
thrives. At the start of 2018, we implemented a new hourly minimum wage of
$15, and during the year, implemented the new Massachusetts Equal Pay Act
requirements. These measures help to ensure that women and men who have
similar experience and do comparable work receive the same wage. While
raising our minimum wage and working towards gender pay equity are the start,
we must have a workforce that reflects the communities we serve. We grew
quickly from a small community bank in Western Massachusetts to a full-service
financial institution with operations in six states and select national markets.
Now we must settle in and focus on creating a culture that mirrors our evolution.
Berkshire Bank has built on our longstanding legacy of responsible
business practices by naming a senior leader for corporate responsibility
and culture. Together with our Board, we will collaboratively enhance
our social responsibility programs and our employee performance. Our
inaugural corporate social responsibility report (available on our investor
relations website) details the meaningful impact these efforts have had on
our community. We also partnered with a diversity consultant to advise
management on creating a culture of belonging. These collective efforts,
combined with disciplined expense management and our improving
profitability, will contribute to our success moving forward.



FOSTERING SUSTAINABLE COMMUNITIES &
RESPONSIBLE BUSINESS PRACTICES
We understand the important connection between the vitality of our communities
and the success of our business. We seek to contribute to our communities through
employee volunteerism and the Berkshire Bank Foundation. In 2018, our XTEAM�
Employee Volunteer Program achieved 100 percent employee participation
for the third straight year, providing over 40,000 hours of service through 342
volunteer events. In addition, Berkshire Bank and its Foundation provided $4.2
million in contributions to organizations engaged in important work throughout
our footprint. As a result, Berkshire Bank was honored by the United Nations with
the IMPACT2030 Innovation Award for its XTEAM� Employee Volunteer Program,
a Gold Halo Award from Engage For Good as the Top North American employee
volunteer program, and PR Daily�s Corporate Social Responsibility Award.
SHAREHOLDER FOCUS
Delivering returns for our shareholders is of the utmost importance. Berkshire
Bank�s book value per share gained 4% during the year. We increased our
quarterly cash dividend by 5% in January 2018, and then increased it by another
5% in January 2019. In June, Berkshire�s stock was added to the S&P SmallCap
600� index, widening our visibility. With our larger size, we implemented debt
ratings from a recognized credit rating agency, earning an investment-grade
rating on our senior debt.
We have a lot to be proud of, and as I look forward, I am enthusiastic about the
opportunities we have around the footprint. We are dedicated to pursuing
efficiencies and deploying our capital and liquidity to drive profitability and
strengthen our enterprise. I am grateful to our Board and our leadership team
for their dedication to our mission. We all thank our customers, employees, and
shareholders for their continuing support.
Berkshire Hills Bancorp, Inc.�BHLB | berkshirebank.com 4
Sincerely,
Richard M. Marotta
Chief Executive Officer