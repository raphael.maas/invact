T O O U R S H A R E H O L D E R S ;
FISCAL YEAR 2008 MARKED OUR 32ND YEAR IN
BUSINESS, 22ND YEAR AS A PUBLIC COMPANY,
AND ANOTHER YEAR OF COMPANY-WIDE PROGRESS
AND SOLID FINANCIAL RESULTS WITH REVENUE,
EARNINGS PER SHARE, AND OPERATING CASH
FLOW REACHING RECORD HIGHS.

Despite the global credit crisis, the weakening
U.S. economy, and the related effects on our
financial institution customers, our conservative
business principles, recurring revenue, long-term
contracts, large customer base, and broad
product offering have enabled us to continue
to generate solid financial results.
As we enter a new fiscal year we continue to
focus on four primary growth drivers:
� Increase market share by aggressively earning
new traditional and nontraditional clients and cross
selling additional products and services to our
existing clients.
� Add new products and services that enable
financial institutions to capitalize on business
opportunities and resolve specific operational
issues.
� Increase recurring revenue by optimizing
outsourcing opportunities, transaction-based
processing fees, and ongoing software maintenance
and support fees.
� Pursue disciplined acquisitions that
complement our internal growth and continue our
focused diversification.
Jack Henry & Associates has prospered in a
competitive business environment by narrowly
focusing on our core competency � providing the
technology financial institutions need to attract
and serve customers, aggressively compete,
control operating costs, and improve operating
efficiencies.
During the fiscal year ended June 30, 2008 total
revenue increased to a record $742.9 million,
representing an increase of 11 percent over last
year�s revenue. Net income was $104.2 million or
$1.16 per diluted share, as compared to net income
of $104.7 million or $1.14 per diluted share reported
in fiscal year 2007. We generated strong cash flow
from operating activities of $181.0 million, return on
assets was 10.3 percent, and return on equity was
17.4 percent.
Our revenue mix for the year consisted of $73.6
million in software license fees or 10 percent of
total revenue, $580.3 million in support and services
or 78 percent of total revenue, and $89.0 million in
hardware sales or 12 percent of total revenue.
Recurring revenue, which provides the financial
stability to support our ongoing growth, was
approximately 70 percent in fiscal year 2008,
compared to 66 and 64 percent in fiscal years 2007
and 2006, respectively.
Backlog, which consists of contracted sales of
products and services that were not delivered by
fiscal year-end, reached $257.4 million, an eight
percent increase over the $239.3 million reported
last year.

ACQUISITIONS
During fiscal year 2008 we acquired two companies,
marking our 16th and 17th acquisitions since we
adopted our focused diversification strategy.
Both acquisitions support our strategy to acquire
companies with innovative solutions we can cross
sell to our core bank and credit union customers,
and that expand the products our ProfitStars
division can sell to virtually any financial services
organization regardless of core processing platform.
GLADIATOR TECHNOLOGY SERVICES, INC.
On July 1, 2007 we acquired Gladiator Technology
Services, a leading provider of technology security
monitoring solutions and related consulting services
that enable financial institutions to safeguard
their information and transaction processing
infrastructures and comply with the related
regulatory requirements. These monitoring
services include network intrusion prevention,
firewalls, server intrusion prevention and event
logging, risk and vulnerability assessments,
compliance and regulatory solutions, e-mail filtering
and encryption services, and the development of
information security, network, and Internet policies.
These security services support more than 500
financial institutions including numerous Jack
Henry & Associates customers.
AUDIOTEL CORPORATION
On October 1, 2007 we acquired AudioTel
Corporation, a provider of remittance, merchant
capture, check imaging, document imaging and
management, telephone and Internet banking
solutions, and Web site design and hosting services.
AudioTel supports more than 1,000 financial
institutions with these back-office and retail banking solutions and has consistently been on the
forefront of the payment processing industry. In
addition to the synergies we identified among our
respective product lines, this acquisition adds more
products and services to our ProfitStars division
than any other previous acquisition.
H I G H L I G H T S
Despite the challenging business environment, we
continued to generate solid financial performance
during fiscal year 2008. We generated industryleading organic revenue growth of 9 percent and
industry-leading profitability with a 22 percent
operating margin. We also experienced significant
growth in our payments business which now
represents 18 percent of the company�s total fiscal
year revenue and an increase of 29 percent over
fiscal year 2007. Each of our three divisions also
realized important milestones and capitalized
on their respective growth opportunities during
the year.
JACK HENRY BANKING
As a leading provider of core information and
transaction processing solutions for banks, Jack
Henry Banking continued to capitalize on the broad
appeal of our products and services. Our core
banking systems, which have replaced every major
competitive solution marketed today, enabled our
ongoing expansion in two key markets � recently
chartered de novo banks and mid-tier banks. We
added 30 de novo banks to our customer roster
during the fiscal year and our technology platforms
now support approximately 20 percent of mid-tier
banks with assets ranging from $1 billion to $50
billion. During fiscal year 2008, we added our
largest in-house customer based on asset size and
completed our largest outsourcing transaction ever.
We also continued to capitalize on the outsourcing
trend with 27 of our in-house bank customers
electing to migrate to our outsourcing offering.

SYMITAR
As a leading provider of core information and
transaction processing solutions for credit unions of
all asset sizes and as the first technology partner to
replace every major competitive core processing
solution, Symitar continued to capitalize on its
market acceptance and signed its 500th credit
union to the Episys core platform. Symitar
continued its dominance among credits unions
with more than $25 million in assets and now
serves more than 30 percent of the credit unions
that have more than a billion dollars in assets �
more than twice the market share of its closest
competitor. Symitar also continues to enjoy the
highest customer retention rate of any major core
data processor in the credit union space.
PROFITSTARS
As a leading provider of specialized products and
services that diverse financial services organizations
and businesses outside the financial industry use
to generate revenue, increase security, and reduce
operational risks and costs, ProfitStars continued to
capture new market share during fiscal year 2008.
More than 600 non-core customers contracted for
ProfitStars� solutions, representing an 18 percent
increase over fiscal year 2007 signings. More than
4,500 individual contracts with varied revenue
implications were signed which represents a 63
percent increase over the prior fiscal year. ProfitStars continued to experience tremendous growth
with its Remote Deposit Capture solution.
Approximately 800 banks and more than 14,000
of their merchant customers now use this system
to electronically deposit the checks received as
payment for goods and services. The related
transaction volumes also increased 271 percent
over fiscal year 2007 volumes. ProfitStars� diverse
customer base now consists of approximately 1,300
Jack Henry Banking and Symitar core customers,
and more than 6,900 non-core customers, including
the top 15 domestic banks.
We recognize that our customers, associates,
and shareholders are responsible for our success
� past, present, and future � so we would like to
formally thank our customers for their business,
loyalty, and guidance; our associates for their
commitment to our customers and mission;
and our shareholders for their confidence in our
company.
JACK PRIM
Chief Executive Officer
TONY WORMINGTON
President
KEVIN WILLIAMS
Chief Financial Officer & Treasurer