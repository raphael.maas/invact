Last year was a watershed year for
BlackRock. We closed the State Street
Research (SSR) acquisition, adding
scope and scale to our activities.
We attracted over $50 billion of net new business,
the most in any single year since forming the
Company in 1988. We crossed the $100 million
mark in revenue from BlackRock Solutions, our
investment systems and advisory products, and we
exceeded $1 billion in total revenue for the first
time. For the year, we generated $4.03 of adjusted
earnings per diluted share(1), up 50% versus the
prior year and 24% compounded annually over
the past 5 years.
At year-end 2005, assets under management were
$453 billion and risk management assets exceeded
$3 trillion, reflecting robust growth in our investment
products and in BlackRock Solutions. Our new
business efforts fueled strong growth across both
geographic regions and client segments. During
the year, we reached the $100 billion mark in assets
managed for non-U.S. investors and in assets
managed for insurance companies worldwide.
Our flagship fixed income and cash management
products remained competitive, and we achieved
strong investment results in the majority of our
equity and alternatives investment products. We
closed the year with strong new business momentum.
This year promises to be even more significant.
On February 15, 2006, we announced an agreement
to combine BlackRock and Merrill Lynch
Investment Managers (MLIM), forming one of the
worlds largest asset managers with approximately
$1 trillion in assets managed on behalf of individual
and institutional investors globally. Our motivation
for undertaking this transaction is straightforward:
to enhance our ability to serve our clients by
broadening and deepening our global capabilities

April 2006
and to position the firm to capitalize on the key
macro trends that are defining investor needs and
shaping the future of asset management.
Globalization has far-reaching implications for all
market participants. In one sense, globalization
points to the world getting bigger. That is, the
global capital markets have grown explosively, with
over $136 trillion of financial instruments outstanding
at year-end 2004, up from $12 trillion in
1980. Even more telling  global bonds surpassed
bank deposits as the largest segment of the market.
This sharp shift was driven by the ongoing
disintermediation of traditional portfolio lenders,
which was in turn driven by innovative product
development and securitization-friendly regulatory
and tax regimes, particularly in the United States,
Europe and Australia.
Paradoxically, globalization also points to the
world getting smaller or, to borrow from author
Thomas Friedman, the world getting flatter. This
phenomenon has been aided in no small measure
by technological advances that facilitate capital flows,
information sharing and having a global workforce.
As an investment manager, having a global
perspective is no longer optional. Understanding
global economies, global industries, global value
drivers, and global capital flows is vital, whether
you are investing solely in U.S. dollar assets or in
markets throughout the world.
Global capital flows are changing the dynamics of
fiscal and monetary policy, defining investment
challenges and shaping the global capital markets.
Between 1985 and 2003, institutional assets in
OECD countries grew nearly eight-fold, four times
faster than GDP growth. Capital expenditures, on
the other hand, dropped precipitously, as institutions
shifted focus from reinvesting in their businesses


to investing in the capital markets. The resulting
flood of global liquidity has had a pronounced
effect on our markets and illustrates the importance
of understanding global capital flows.
Since June 2004, the Federal Reserve has raised
the discount rate 15 times. Typically, the long
tightening cycle and the soaring U.S. deficit would
induce higher long-term rates and slow the economy.
Until recently, however, long rates actually fell and
the economy sustained its momentum. The culprit
for this apparent conundrum was strong foreign
buying of U.S. debt securities.
Foreign demand was driven by several factors.
Increasing oil prices generated greater petrodollars.
Central bank reserves, particularly in China
where the currency is effectively pegged to the
U.S. dollar, increased sharply. U.S. yields
remained attractive relative to rates in other key
markets. Lastly, the U.S. dollar remains the
worlds preferred store of wealth, in part because
there is no debt market as large and as liquid as the
U.S. Treasury and Agency market. The flow of
capital from non-U.S. investors kept rates low,
effectively making up for our countrys anemic
personal savings rate and funding Americas enormous
and growing deficit.
Persistent low rates are not necessarily a good
thing. Investors are driven to reach for yield and,
in so doing, take on increasing amounts of risk.
The global search for yield and alpha has induced
explosive growth in alternative investments.
Hedge funds alone have grown to over $1.5 trillion
in assets. Perhaps inevitably, average returns have
come down  a sign of too many people and too
much money chasing too few strategies. As returns
decline, investors are induced to ratchet risk up even
further, notwithstanding the fact that many lack
the necessary risk management tools and discipline.
At the same time, regulators and shareholders are
demanding greater focus on risk management.
Institutional investors are responding in various
ways. An increasing number of companies are
freezing their defined benefit plans, capping their
future pension obligations. In addition, plan
sponsors are increasingly interested in liabilitydriven
investing. LDI is not a new concept  banks
have long focused on managing their asset/liability
gap, while insurance companies focus on managing
their surplus risk. Nonetheless, it demonstrates
an increasing focus on risk management that
is likely to continue to gain appeal as rates rise.
The separation of manufacturing and distribution
is another significant trend in our industry. The
adoption of open architecture among brokerage
firms and other financial advisors set the stage for
this trend years ago. A series of regulatory issues,
beginning with the Wall Street research settlement
and extending through the mutual fund scandals,
increased concern about real or perceived conflicts
of interest. Re-regulation has increased the cost of
doing business and the need for scale, particularly
in mutual funds. These issues and the inherent
cultural conflicts have caused large distribution
companies to question the merits of maintaining
proprietary manufacturing. As financial services
companies focus increasingly on their core competencies,
the trend of separating manufacturing
and distribution will continue.
No strategic discussion would be complete without
comment on the broader retirement landscape.
The demographics  aging of the population, the
impending retirement of the baby boomers, and
longer life expectancies (and, as a result, longer
retirements)  are of obvious importance to the
future flow of funds. The concomitant shifting of
the retirement burden from employers to employees
is at least as important. When a company freezes
its defined benefit plan, or a government extends
the retirement age for its social security scheme,
the effect is to shift the burden for retirement
savings and planning to the individual. The
potential social and economic consequences are
enormous, and the opportunity for innovation in
our industry is considerable.
All of these trends have important strategic implications
for BlackRocks business. Our most critical
imperative is to continue investing in our core
competencies of investment management and risk
management. We must have a global investment
platform that encompasses local manufacturing and
benefits from information sharing across the capital
structure. To best serve our clients, we need a deep
understanding of local regulatory, legal and tax
considerations, and the resources to support clients
in their home markets. We have to intelligently use
technology as an effective tool to promote a unified
platform, seamless communication, efficient business
7
catalyst for accelerating the business model changes
we began last year, overlaying a regional matrix on
our functional organizational structure, in response
to the demands of globalization. While the
magnitude of the integration presents challenges
and will take time, we are confident that we will
achieve a unified platform and that we will be able
to mobilize the deep capital markets, risk management
and investment processing expertise of the
combined organization to achieve our mission:
helping our clients solve problems.
The commitment and dedication of our employees
will always be the most vital ingredient in our performance
as a firm. Most mergers in our industry
fail because the parties fail to integrate cultures.
SSR succeeded because we were willing to make
the hard decisions early, transition quickly to a single
technology platform and, most importantly,
establish a common vision and weave a common
culture for the firm. Our objectives for integrating
BlackRock and MLIM are identical; achieving
them will require the contribution and commitment
of many professionals. Among our employees
greatest responsibilities in the year ahead will be
reaching out to each other in order to establish one
culture informed by an understanding of our
respective histories and defined by shared values
and a common vision for our collective future.
As I hope this letter conveys, we remain squarely
focused on achieving competitive investment
performance, helping clients solve problems,
enhancing our processes, and fostering a vibrant
culture that will enable us to continue to attract,
develop and retain the intellectual capital that
most significantly differentiates BlackRock. I am
grateful for the guidance of our Board of Directors
and, most importantly, for the trust our clients
place in us. We remain committed to continually
earning that trust and, through those efforts, to
creating meaningful and enduring shareholder
value.
Sincerely,
Laurence D. Fink
Chairman and Chief Executive Officer
processes and effective operational risk management.
We have to recognize that globalization requires
new organizational paradigms. With substantial
portions of our employee and client bases outside
the United States, a purely functional model will
no longer suffice. Most importantly, as we evolve
our business model, we have to preserve what is
most valuable about our culture and sustain our
ability to attract and retain talented professionals.
We carefully consider our business opportunities in
the context of these strategic imperatives. We have
consistently reinvested in our business and have
pursued incremental capabilities organically and
through targeted hiring. We have also considered
a variety of mergers and acquisitions over the
years, although we approached each situation with
a great deal of skepticism. Mergers in investment
management are notoriously difficult, and we are
not an easy partner. We insist on full integration
and a unified operating platform. We have highly
valued client relationships that we are not willing
to impair, and strong business momentum that we
are not willing to dilute. We have a vibrant culture
that is core to our franchise and that we are unwilling
to compromise. We have to be convinced
that we can create substantially greater shareholder
value together than BlackRock could create alone.
SSR met these criteria, and we believe that acquisition
has been a resounding success. We concluded
in early 2006 that MLIM met our criteria and that
a combination of our firms would better position
both of us to capitalize on trends in our industry.
Like BlackRock, MLIM fosters a culture that
emphasizes investment performance first and
foremost. In addition, MLIM has tremendously
talented professionals worldwide, and its capabilities
are largely complementary to BlackRocks, including
an established retail presence in Europe and
broader international efforts that would have taken
us years to build. The combined firm will have
substantial scale in mutual funds, which will be vital
to serving retirement needs, and expanded scope in
equities and alternatives that, with continued
strong performance, will be of significant interest
to our clients.
On a pro forma basis, we will have employees in
18 countries, including investment professionals in
the U.S., the U.K., Japan and Australia, and clients
in 50 countries. The transaction will serve as a