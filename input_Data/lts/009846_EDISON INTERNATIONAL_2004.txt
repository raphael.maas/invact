

Dear Fellow Shareholders:
                                  It is with pride and satisfaction that I report
                                  to you another year of strong performance by
                                  Edison International. The total return on your
                                  investment in us during 2004 was 51 percent.
                                  This spring, the value of your shares rose to an
                                  all-time high.
                                  In recent years the Edison International story has been one of
                                  rebuilding, as we survived first the California power crisis and
                                  then the collapse of the U.S. wholesale power market. Initially,
                                  our increased value was largely attributable to the success of
                                  our recovery efforts. In 2004, we turned an important corner as
further value increases were driven principally by the favorable post-recovery longer-term outlook.
On behalf of all the employees of Edison International, I thank you and all the other shareholders
who remained confident in us through the uncertain times. It gives us great pleasure to reward
your confidence.
Last fall, our senior management team and I presented to investors and our employees a forward-
looking, multi-year strategic plan called Building the Future. Its rationale is simple but powerful:
We intend to capture the above-average growth inherent in our existing operations. We like the
opportunities squarely in front of us.
Edison International has one unifying focus  providing the electrical energy that enhances
people's lives. We generate it from diverse fuels, including natural gas, coal, nuclear, hydroelectric
and renewables. We transmit it over long distances. We buy and sell it wholesale. And we
distribute it to retail customers who are its end users.
In Southern California Edison (SCE), we have a strong utility serving a dynamic region where
both average energy usage per customer and the population are growing. Serving our customers
by making the large capital investments needed for our utility electric system should also result
                                          



in substantial growth in the utility's earnings           Investments in our utility wires business,
base over the next five years.                            excluding generation, make up $9 billion of
                                                          the $11 billion in capital spending projected
Our independent power and infrastructure
                                                          in our Building the Future strategic plan.
finance businesses make up almost one-third
                                                          This represents a near doubling of our wires
of our assets. They are well positioned to
                                                          business investments over the previous six
compete in wholesale power markets and in
                                                          years. We are also stepping up investments in
the development of renewable energy. Our
                                                          utility power generation. As a result, in 2004
independent power business includes a large
                                                          alone, we increased our total utility capital
base of low-cost coal generation plants which
                                                          expenditures by about 70 percent over the
provide good margins today and offer substan-
                                                          previous year.
tial additional upside potential.
                                                          Significantly, the California Public Utilities
If we execute our plan well in these business
                                                          Commission (CPUC) last year endorsed our
lines and continue to secure sound regulation
                                                          plan for increased infrastructure investment
in California, we should be able to produce
                                                          and provided rate support for these invest-
healthy earnings and dividend growth through
                                                          ments through the end of 2005. This was a
the next five years.
                                                          critically important step forward.
Investments to Serve Our Utility Customers
                                                          Successful International Asset Sale
In Southern California, two factors have
                                                          In the fall of 2003, following a sharp down-
converged to create a need for substantially
                                                          ward revision in the overall credit ratings of
increased investment in our utility wires
                                                          companies within the independent power
business. First, our customer base and per
                                                          industry, we decided that the sale of our large,
capita usage are growing, requiring extensive
                                                          valuable portfolio of international power plants
construction and expansion. Second, much of
                                                          would likely strengthen our independent
the electrical system was built in the decades
                                                          power business.
following World War II. Those decades-old
elements of that infrastructure are aging                 The sales are now substantially complete 
and should be systematically replaced and                 only one plant remains to be sold  and have
modernized.                                               brought in approximately $2.4 billion after
                                                          taxes and costs. The completion of these sales,
                                                          along with cash from operations and plant





decommissioning, allowed us in the last year            to provide additional capacity for the summer
to pay off $1.4 billion in debt within our              of 2006, when supplies are forecast to be tight.
independent power business line and to
                                                        Award-winning Conservation Programs. During
increase cash on its books to slightly in
                                                        the past three years, Southern California
excess of $2 billion.
                                                        Edison has conserved more energy through its
Today, our independent power business owns              energy-efficiency programs than any utility in
approximately 7,700 megawatts of low-cost               the U.S. Our programs have helped customers
coal plants in Illinois and Pennsylvania and            reduce their energy use by more than one
slightly more than 1,000 megawatts of natural           billion kilowatt-hours during that time, or
gas plants operating predominantly in                   enough power to supply 140,000 average
California. Although challenges lie ahead,              homes for one year.
this business offers shareholders both current
                                                        Customer Refunds. We are continuing to work
value and substantial potential for the future.
                                                        with California and federal officials to seek
                                                        refunds from suppliers who overcharged our
Other Highlights
                                                        customers during the power crisis. Through
New Renewable Energy Investments. We began              early 2005, we have reached settlements with
making investments in renewable energy                  five suppliers totaling more than $400 million,
projects in the mid-1990s, but had to put               most of which goes to our customers.
that initiative on hold during the power crisis.
In 2004 we started again within Edison                  Strong Financial Results
Capital to grow that business. If conditions
                                                        When we serve our customers well, our share-
remain favorable, we propose to make invest-
                                                        holders benefit. In 2004 our earnings were
ments in excess of $1 billion in renewable
                                                        $2.81 per share, an increase of 12 percent over
energy over the next five years.
                                                        the previous year. Our common stock value
New Generation for California. Early in 2004,           increased 46 percent. We also paid off all the
federal and state utility regulators approved           outstanding debt  approximately $1.5 billion
SCE's proposal for the construction of                   at the Edison International parent level.
Mountainview, a large natural gas generating            Edison International and Southern California
facility. It is scheduled to be complete in time        Edison are now both rated investment grade
                                                        by Moody's and Standard and Poor's, reducing
                                                        our borrowing costs. Finally, at the end of




2004 we were able to increase our dividend by          position, to enforce rigorously our century-old
25%, to an annualized rate of $1.00 per share.         commitment to both service and integrity.

Key SCE Generation Issues                              Building the Future

Two issues I mentioned in last year's letter           Our principal objectives for the next years
deserve brief updates. SCE's Mohave and San            are established. Our challenge is to meet the
Onofre generating plants provide cost-effective        targets established in our Building the Future
sources of power and fuel diversity in a state         strategic plan. Four initiatives will be particu-
heavily reliant on natural gas for its power           larly important in the coming year:
generation. However, continued operation of
                                                       First, our efforts to reduce costs and improve
the Mohave plant beyond 2005 is in question.
                                                       efficiency will continue. In our non-utility
We are working diligently to resolve difficult
                                                       operations, that means further debt reduction,
outstanding issues of coal and water supply in
                                                       as well as reductions in general and adminis-
an effort to extend that plant's operating life.
                                                       trative expenses. At SCE, our infrastructure
At our San Onofre nuclear plant, four steam
                                                       improvement plan will require a level of
generators must be replaced at an estimated
                                                       skilled field work that surpasses anything in
total cost of $680 million to permit continued
                                                       our history. We recently launched a multi-year
safe and reliable operation. We filed for
                                                       productivity effort to drive more work through
advance authorization from the CPUC early
                                                       our system while keeping costs down. This
last year, and expect a decision in 2005.
                                                       will be a major emphasis.
A Significant Disappointment                           Second, we will seek continuing regulatory
                                                       support for our electric infrastructure improve-
During 2004, we discovered serious problems
                                                       ment plan, in the form of three-year forward
with data SCE had submitted to the CPUC
                                                       rate recovery that builds on the rate-making
to support potential incentive awards for cus-
                                                       precedent set by the CPUC last year. The
tomer satisfaction and safety. We advised the
                                                       only good thing that may have come from
CPUC of these issues, conducted a thorough
                                                       the power crisis was the consensus it created
investigation and submitted the results to the
                                                       about the importance of maintaining a reliable
commission. In addition to firm disciplinary
                                                       electric system. California's recent experience
action for those who violated company policy,
                                                       has been a harsh teacher.
we launched a strengthened ethics and compli-
ance program, including a new officer-level




Third, we will solidify our independent power           done countless times before, our employees
business, expand our renewable energy invest-           answered the call, found means to overcome
ments, and carefully evaluate our prospects for         large unexpected obstacles, and sacrificed their
growth in this sector. One opportunity that             own holiday celebrations to restore reliable
may present itself is in California, where it           power delivery.
is likely that a significant portion of much-
                                                        I believe this same spirit sustained our com-
needed new generation will be developed
                                                        pany these last few years during back-to-back
through competitive processes.
                                                        crises as serious as any in our 118-year history.
Finally, although East Coast and Midwest                When other companies might have failed, the
power markets served by our fleet of low-cost           men and women of Edison International rose
coal plants remain oversupplied with genera-            to the occasion and helped engineer a remark-
tion, we make solid margins because wholesale           able turnaround.
power prices are often set by more expensive
                                                        We still face worthy challenges but, once
natural gas plants. When economic growth
                                                        again, we are on a path to sustainable growth.
restores a better balance of supply and demand
                                                        We have a solid five-year strategic plan and a
in these markets, our plants should also earn
                                                        balanced business mix, with talented people
capacity values  the price paid to generators
                                                        and deep, well-tested experience in both
for on-call access to their supply. We are
                                                        regulated and competitive markets. We are
focused on optimizing these values as we
                                                        positioned well to take advantage of the large
operate and sell the output of these plants.
                                                        opportunities that lie ahead.
Concluding Thoughts                                     Sincerely,
I have had the privilege of serving in senior
roles at Edison International for more than
20 years, and I still marvel at the inventiveness
and drive of our employees when conditions
are at their worst.
                                                        John E. Bryson
During the 2004 holiday season, a series of             Chairman of the Board,
storms moved across Southern California,                President and Chief Executive Officer
causing widespread damage to our utility
                                                        March 21, 2005
electric system and disrupting service to hun-
dreds of thousands of customers. As they have
