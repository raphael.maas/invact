A Message
from David Seaton
to our Valued Shareholders

Throughout 2012, Fluor celebrated 100 years of service to our clients, often reflecting on the companys
historic strengths and values that made such a significant achievement possible. We also focused on the steps
we need to take to position ourselves for the next century of industry leadership.
We learned a great deal about our company and our management commitments as we brought our history and
culture into perspective, and we sharpened our focus on creating growth strategies for the future. Fluor is a strong
and resilient company, and we fully understand the imperative to deliver shareholder value.
We know that we have to earn your trust every day, and delivering profitable growth gives us the
opportunity to do that. Notwithstanding the unexpected charge relating to the adverse arbitration ruling on the
Greater Gabbard Wind Farm project, I am pleased to report that Fluors fundamentals remain strong and our financial
performance was significant. During the year, we booked new awards totaling $27.1 billion, and our ending backlog
stood at a sizeable $38.2 billion. Fluors revenue grew to a record $27.6 billion, and net earnings attributable to
Fluor were $456 million, or $2.71 per share.
Fluors balance sheet remains strong, with year-end cash and securities of $2.6 billion. In 2012, we returned
$518 million in cash to shareholders through dividends and the repurchase of 7.4 million shares, and still maintain one
of the healthiest cash positions in the industry. We expect to continue to return cash to shareholders during 2013.
Our shareholders and clients can have confidence in the underlying strength of Fluor. Our substantial
financial position, global diversification, expansive client relationships, and the expertise that comes from a century of
experience give Fluor resilience in the face of big challenges. This is the legacy of Fluor, one that allows us to provide
customers with best-in-class services and the best people in the industry. The strength and depth of our company,
and our continuous efforts to grow the next generation of industry leaders, position Fluor as an employer of choice,
giving us another strategic and competitive advantage.
These defining characteristics will prove especially valuable in 2013 and beyond, as large industrial projects that
address unprecedented population needs and energy demands become reality.
We anticipate significant long-term growth in the capital spending trends of our major customers, which
will fuel growth opportunities for Fluor. Throughout 2012, we took a number of important steps to position Fluor for
this next growth cycle, focusing on the strategic imperatives of optimizing costs, building and improving relationships, and
growing our businesses and services.
For example, in 2012, Fluor signed a three-year strategic global engineering, procurement, construction and
construction management agreement with Dow Chemical Company. This illustrates our ability to strategically partner
and position the company as a valued resource for clients on the execution of their capital project plans. Fluor was
also awarded a five-year enterprise framework agreement for engineering and project management services for
Shells projects in Europe, Africa and the Middle East. Our partnering arrangement with BASF in Asia and Europe
was strengthened with an additional services agreement in 2012, covering all of North America. Fluor currently
has multiple active projects under the BASF partnering agreements, and the Dow alliance has already resulted in a
contract for a propylene production project in Freeport, Texas. Strategic partnerships of this nature position Fluor
for substantial future global opportunities.

Significant new awards across all five business segments were won during 2012, and noteworthy
milestones were achieved during the year, solidifying Fluors strong position in key markets. Fluors Oil & Gas
business secured $12.6 billion in new work. A Fluor-led joint venture was awarded a contract by Tengizchevroil, LLP,
for its Wellhead Pressure Management Project in Kazakhstan, and two contracts were awarded to support Reliance
Industries next phase of refinery and petrochemicals growth in India. Other noteworthy awards included additional
scope and incremental releases for a major oil sands expansion project in Canada. In addition, the number of
front-end engineering and design (FEED) projects awarded to Fluor in 2012 signals a resurgence in oil and gas
markets, where Fluor is well positioned.
Fluors Industrial & Infrastructure group booked new awards of $9.5 billion in 2012. This group celebrated the
early opening of the new I-495 Express Lanes in Virginia, bringing welcome relief to one of the most congested
highway systems in the United States. Another Fluor-led joint venture was named by the Texas Department of
Transportation as the preferred bidder for the design, construction and capital maintenance of a major road project in
Dallas, Texas. Fluor won a number of important mining and metals contracts, despite a slowing demand for
commodities, and the company continues to see long-term opportunities to maintain our leadership position in this
market. For example, Fluor was awarded significant new scope from Barrick Gold Corporation at the Pascua Lama
mining project located on the Argentina/Chile border, arguably one of the most significant and challenging mining
projects currently underway in the world.
Our Government business remained strong, strengthening existing relationships and expanding our portfolio of
service-related work. During 2012, the U.S. Department of Energy extended Fluors contract at the Savannah River
Site and selected a Fluor joint venture company as the prime contractor for the decontamination and decommissioning
of the Portsmouth Gaseous Diffusion Plant in Ohio. A Fluor team was selected by the U.S. Army to participate in its
massive EAGLE logistics program, which will allow Fluor to compete for future task orders. The U.S. Department of
Defense also selected a Fluor joint venture to perform base operations support at seven arsenals in Illinois.
Fluors Global Services team pursued a number of new long-term contracts with major industrial customers,
booking $904 million in new awards and renewals during the year. In addition, key acquisitions, along with joint
ventures established during the year, will further strengthen Fluors growth potential.
Further advancing our position as a leader in the solar power engineering and construction industry, LS Power
Group awarded Fluor an engineering, procurement and construction contract, as well as a separate contract for
ongoing operations and maintenance services, for a 170-megawatt solar photovoltaic facility in Southern California.
Also during the year, regulatory permits were received by Fluor client LCRA, clearing the way for Fluor to proceed
with plans to build a new natural gas-fired power plant in Horseshoe Bay, Texas.

Fluors global reach and project expertise provide a strong foundation for the future and bring new
opportunities for success with our clients. Growth in emerging markets, infrastructure needs in mature markets,
and improving economic conditions hold great promise for Fluor across all of our business segments. I believe that
meeting these growing demands will require new thinking and changes to the way we approach our markets and
serve our customers.
Fluor is working to provide a more vertically integrated approach to executing work, which should drive more
value to our clients and increase our share of a projects profitability. To that end, we will seek opportunities to make
strategic acquisitions or establish joint venture companies that enhance the companys ability to provide modular
construction and fabrication. We will also expand upon our heritage as a builder by increasing direct-hire construction
where it makes the best sense for our businesses and our customers.
During 2012, Fluor formed a joint venture with AG&P, a Philippines-based company that specializes in modular
engineering and fabrication services. We made this investment to expand our capabilities into a full range of
integrated engineering, fabrication, module assembly and construction services.
We also formed a new joint venture with one of Brazils largest construction companies, which will link that
organizations growing presence in Brazil with Fluors project execution leadership. AMECO, Fluors integrated
equipment and tool solutions company, acquired ServiTrade, a Mozambique-based construction equipment rental
and project services company. This gives Fluor an early entry in this high-growth region of Africa.
Investments like these position us to provide improved schedule, delivery and cost certainty to our clients.
But that is only part of the equation. We also know that Fluor plays an important, and often essential, role in
helping shape the competitiveness of our clients. Customers are searching for a trusted partner with mutual benefits
and shared risks. Meeting clients needs for reduced capital costs and accelerated schedules is key to successful
long-term business relationships.
Our business and financial performance is the result of extraordinary commitment and capability at every level
of the company. I am immensely grateful to Fluors over 40,000 employees for the dedication they bring to work
each and every day. Id like to acknowledge that after over a decade of exceptional service in leading Fluors finance
organization, Chief Financial Officer D. Michael Steuert retired during 2012. Biggs C. Porter joined the company as
Fluors new Chief Financial Officer, bringing a wealth of financial leadership experience. And to Fluors Board of
Directors, for their ongoing guidance and confidence in the company, I offer my sincere thanks. Joining our
Boards ten other independent directors in 2012 was Armando J. Olivera, retired president and chief executive
officer of Florida Power & Light Company.
When I reflect upon where Fluor stands today, and the remarkable achievements that distinguish us as
an industry leader, I am also respectful of the vast opportunity that is before us. As we step into the first decade
of our next hundred years, we are focused on sustainable growth  adapting to market changes and client
needs, investing in our people, being selective in the projects we pursue, and building trusted relationships
with our stakeholders.
DAVID T. SEATON
Chairman and Chief
Executive Officer
Fluor Corporation
March 8, 2013