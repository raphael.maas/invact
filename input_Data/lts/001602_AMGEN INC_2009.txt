Dear Stockholders,
In 2009, Amgen delivered vital medicines to
patients as we weathered the most challenging
economic environment in our 30-year history.
We managed our business with ? scal discipline,
generated more than $6 billion in operating cash
? ow, and advanced our pipeline meaningfully.
This is a challenging time for investors, but we
feel good that in a time of particular uncertainty
in our industry, we have outperformed many
of our major competitors and more than held
our own compared to the S&P 500 Index
performance. However, we are not satis? ed
with these results and hope, as denosumab
and our pipeline develop, to do even better.
We predict in our revenue guidance for 2010
a return to top-line growth.* We also anticipate
continuing our strong adjusted earnings per
share** performance with the prospect of growing
adjusted earnings per share** meaningfully this
year.* In fact, we have grown adjusted earnings
per share** at an average rate of approximately
16 percent per year during the last ?ve years

The European regulatory authorities 
recommended Prolia
 (denosumab) for 
approval in Europe last December, and we 
announced in January that we have submitted 
the information the U.S. Food and Drug 
Administration requested in the Complete 
Response letter for Prolia
 in the treatment 
of postmenopausal osteoporosisa very 
important step on the way to approval. We 
expect Prolia
 approval this year in the 
United States, Europe, and other regions. 
In the United States, we have a world-class 
sales force hired and trained; we have a 
collaboration in place with GlaxoSmithKline 
to commercialize Prolia
 in markets outside 
the United States and Japan; and we are 
ready to launch worldwide. This is exciting 
for patients, and for all of us at Amgen. We 
are also enthusiastic about denosumabs 
potential to help people with cancer. In several 
common cancers, including prostate and 
breast cancers, tumor cells may spread to 
bone, causing fractures or other painful and 
dangerous complications (known as skeletalrelated events, or SREs). In pivotal studies, 
denosumab demonstrated superiority over 
zoledronic acid, the standard of care, in 
delaying SREs in breast and prostate cancer 
and non-inferiority to the standard of care in 
solid tumor/multiple myeloma. Data from these 
studies will form the basis of our submissions 
to regulatory agencies this year for an SRE 
indication. This year, we also anticipate results 
from an important study of denosumabs ability 
to prevent bone metastases in prostate cancer 
patients. Denosumab is perhaps the most 
promising product opportunity in the industry 
right now, and it was included in TIMEs list of 
Top 10 Medical Breakthroughs of 2009. 
Delivering ? nancially and making Prolia
available to patients are important goals we 
have set this year. Another important goal is 
to advance the pipeline beyond denosumab. 
Late last year we were recognized by Scrip
as having the best overall pipeline in the 
business. We owe it to patients everywhere 
to deliver on that promise. Our pipeline 
programs will continue to progress in 2010. 
In addition to denosumab in oncology, we 
look forward to additional data and ? lings 
for Vectibix
 (panitumumab) and phase 3 
decisions on a number of therapeutic oncology 
programs. We are also focused on earlier 
programs in new therapeutic areas for Amgen 
such as diabetes, asthma, and cardiovascular 
disease. Over the past decade we have made 
a sustained investment in research and
development. It has been a long and dif? cult
road, but returns on that investment are now
in our sights for the decade ahead. Driving
innovation will continue to mean looking
outside as well as insideas we did last year
with Cytokinetics in heart failure and Array
BioPharma in type 2 diabetes. Investing in our
pipeline and pursuing growth through strategic
partnerships and business development are
cornerstones of our strategy.
Another goal we have set this year is to
expand internationally. Over the past decade,
we have expanded our international presence
from 18 to nearly 50 countries. In so doing,
our international product sales have grown
from 10 percent to 22 percent of total product
sales. We are committed to aggressively
continue this expansion into underserved
and fast-growing markets which many see
as among the most attractive opportunities
in our sector.
Amgen operates in a dif? cult environment.
Discovering and developing innovative new
medicines is hard work. Our U.S. healthcare
system also needs reform. The percent of GDP
spent on healthcare grows inexorably and is
the highest in the world. Yet our outcomes
are no better, and in many cases, worse than
other societies and tens of millions of our fellow
citizens cannot afford insurance or care. Our
national attempt to begin to address some
of these issues was disappointing in process
and result, but we must keep trying. Amgen
will remain constructively engaged to help
address our national healthcare system
challenges and assure America remains
a place where innovation is encouraged,
protected, and rewarded.
After 30 years, Amgen remains as committed
as ever to our mission to serve patients by
developing innovative medicines to address
serious unmet medical needs. We continue
to believe that delivering against our mission
is also the very best way to serve our
stockholders. I am excited by our prospects
and Amgen is well prepared to deal with
these challenging times.
