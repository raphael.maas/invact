Dear Shareholders,
Associates and Customers:

Its an exciting time for Walmart. From the U.S. to the U.K., from
Mexico to China, and across all the markets we serve, retail is
changing in fundamental ways. Our future is bright because
were increasing our investments in associates, stores and
e-commerce capabilities to prepare for the way customers will
want to shop with us in this new era of retail.
Each week, we serve close to 260 million customers in our
stores, in 27 countries, and through our websites globally. While
language and culture may differ, remarkable similarities exist
globally in what customers expect from a retailer. Whether its a
young mom in Toronto or a retired couple in Phoenix, customers
everywhere want to save money and save time. They want
to shop on their terms in a manner thats easy and convenient.
They seek broad choices in assortment. And, regardless of how
they shop, in stores or on their mobile device, they expect a great
price and experience. At Walmart, our enterprise strategy guides
how we fulfill those expectations and deliver on our customer
proposition. Well drive sales growth by executing well, in stores
and e-commerce, every time we serve customers.
Delivering a solid financial performance
Im encouraged that Walmarts fiscal 2015 revenue grew
by more than $9 billion to nearly $486 billion and earnings
per share were $4.99, a nearly 3 percent increase from the
prior year. But, we have higher expectations. Our priority
is to run great stores, clubs and e-commerce everywhere
we operate to grow the business.
Walmart U.S. delivered net sales of $288 billion, a more
than 3 percent increase, and improved its sales and
operating income trends each consecutive quarter
during the year. Im pleased by the positive comp sales
growth, especially the strong performance from
Neighborhood Markets, but were not satisfied. The
Walmart U.S. team is implementing a broad
range of initiatives focused on strengthening
our assortment (especially the fresh offering),
driving the integration of e-commerce with our
stores, and improving the customer experience.
For example, in February, we announced a
$1 billion investment in our U.S. hourly associates
to provide higher wages, more training and
increased opportunities to build a career with
Walmart. These are strategic investments in our
people to reignite the sense of ownership they have in our stores
and foster an improved customer experience to drive sales growth.
Walmart International produced solid constant currency sales and
operating income growth. On a constant currency basis, net sales
surpassed $141 billion, while operating income increased to more
than $6 billion. Im pleased that were running better stores in our
International markets. Operations in Canada, Mexico and China
continue to improve, leading to stronger sales and profitability.
The U.K. market has become fiercely competitive, and in Brazil, we
continue to work on improving performance. Across International,
our commitment to a compelling fresh food offering and innovations
in e-commerce, like grocery home shopping, will be important
growth drivers for the future.
The emphasis of the Sams Club team on making membership
more rewarding helped drive net sales of $58 billion and an
increase of more than 10 percent in membership income.
Members appreciate the value-added benefits offered by Sams
Club like Plus Cash Rewards and the suite of comprehensive
business member services. The team is focused on bringing
merchandise excitement and newness to drive sales. In addition,
Sams Club continues to strengthen digital integration with clubs
through initiatives like Club Pickup.
Our 22 percent growth in global e-commerce sales surpassed
the overall market and was supported by enhancements to our
technology, assortment and supply chain. The investments
in our global technology platform provide a foundation that
strengthens usability and conversion across our e-commerce
websites and mobile apps. Were also investing in more fulfillment
centers around the world to enable faster delivery of merchandise
to customers. Each of our business segments continues to
increase the integration of e-commerce and mobile assets with
our stores and clubs. For example, were testing Click & Collect
pickup points in many of our key markets. Asda already has
Click & Collect capabilities in all stores.
Investing in customer relevance
As we invest to expand our global e-commerce capabilities and
build more stores, we keep customer expectations at the forefront.
The type of store format or fulfillment center we build, the location
of where we put a club, or the functionality of a website are all
predicated on how we can better serve our customers. And, as
we make these choices, were striving to balance sales growth
and profitability. Were being thoughtful with our investments,
ensuring we have the infrastructure in place for sustainable
growth. Walmarts strong balance sheet and robust cash flow
provide a solid foundation to support these investments. While
we grow, we remain focused on expense management and EDLC.
When we operate and grow efficiently, well generate increased
value for shareholders.
Engaged associates fuel our success
Highly motivated and engaged associates are essential to providing
customers with excellent service. And, its only through associates
who are merchant-minded that well continue to connect customers
with new items that they want and need. Although technology
has transformed our business, retail is still a people business.
Walmart has always provided a ladder of opportunity  one that
today is available to our 2.2 million associates globally. Regardless
of your background, Walmart will give you the opportunity to
grow a career as far as your ability and hard work will take you. I
am one of many leaders in our company who benefited from this
opportunity to begin as an hourly associate and grow into roles
with increased responsibility.
Talent is the essential enabler to reach our objectives. Im excited
by the new initiatives weve put in place around the world to
better train and equip our associates for success. For example, the
steps weve taken in the U.S., China and Mexico to strengthen
compensation structures and increase training opportunities give
associates more ownership and accountability, so they can react
faster to customers needs. Adding new talent is also important as
we work to grow digital retail and fully align our organization with
a changing retail environment. Some of the brightest minds in retail
are joining Walmart because they know this is an organization thats
embracing innovation to deliver a better future for customers.
Committed to a better world
Were not only thinking differently about retail, were thinking differently
about the world. Walmart is a powerful change agent, and were
committed to global responsibility initiatives that make our world
better. Im proud of our work to advance environmental sustainability,
to support womens economic empowerment, and to offer healthier
food choices for our customers. We continue to look for more ways
to lead and have an even greater impact on the communities that
we serve. Well also remain steadfast in our commitment to compliance,
ethics and doing business the right way. Im pleased with the
enhancements weve made, including better technology, to
strengthen these organizations and build world-class compliance.
My career at Walmart began more than three decades ago, and Ive
never been more excited about our future than I am today. Walmart
has a great purpose  to save people money so they can live better.
Were embracing change so we can deliver that promise more effectively.
As I look back over this past year, weve made great strides
towards our goals. We know where our customers expectations
are going, and were ideally positioned to deliver for them. Walmart
has great assets and capabilities, but theres more we must do. Were
continuing to build a Walmart that excels globally at the integration
of digital and physical retail, providing our customers with a seamless
experience to shop whenever, wherever and however they want.
Its a great opportunity. Im excited about the next steps in our journey.
Sincerely,