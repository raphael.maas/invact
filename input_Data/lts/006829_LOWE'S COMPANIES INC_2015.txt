Letter to Shareholders,
Its easy to explain what we do and how we do it.
We sell home improvement goods and services
through our stores, our website, our contact centers,
at job sites and even right in peoples homes.
But over the past few years, weve taken time to
evaluate why we do these things. Why do we sell
home improvement goods and services? Why are
we transforming to an omni-channel home
improvement company? Through this evaluation
we uncovered a purpose thats authentic to who
we are as an organization and a reflection of what
we do every single day.

Our purpose is to help people love where they live
And, as were evolving from a company with a purpose to
a company driven by purpose, we remain focused on our
ability to meet customers on their terms, provide solutions
for their projects, and be their trusted partner in
home improvement. Moreover, we seek to differentiate
ourselves through better customer experiences that
make us the project authority. We do this by continuing to
leverage our distinctive strengths in customer experience
design to inspire customers and guide them through
completion of their home improvement projects.
We continue to enhance our relevance to customers
through omni-channel retailing, where all of our channels
work together seamlessly so that we support customers
at every step of their home improvement journey and
build greater affinity for the Lowes brand. We began rolling
out technology to better support our omni-channel
strategy to a couple of test markets in late 2015 and continue
deploying the technology across the chain.
As we transform to a home improvement company, we
know that there is more than one path to serving customers,
so we are growing our capabilities and our reach all
with the intent of helping people love where they live. By
leveraging our portfolio of home improvement businesses,
we are well positioned to adapt to what customers
expect, now and in the future, and to generate value
for shareholders for years to come. While the size and
scope of what we do continues to evolve, just as it has
since we were founded in 1946, the passion and pride of
our people remains constant.
I believe our steadfast commitment to customers is evident
in the solid results we achieved in 2015. Total sales
grew 5.1% to $59.1 billion, driven by comparable sales
growth of 4.8%, with all regions and product categories
achieving positive comps.

With a keen focus on productivity and profitability, we
leveraged this continued top-line growth to deliver an
increase in Adjusted Net Earnings1 of 14% and Adjusted
Diluted Earnings Per Share1 of 21%.
While our purpose empowers us to focus on the thing
that matters mostthe customerit also fuels substantial
returns for our shareholders. Our capital allocation
priorities align with the commitments weve made to both
customers and shareholders. From the solid cash flow
we generate, we first invest to maintain and grow our
businesses. Next, we target a dividend payout ratio of
35%, and our dividend has grown every year since going
public in 1961. Then, we use remaining funds to repurchase
shares.
Our fundamental purpose ensures that all the effort
weve put into transformational change will last well into
the future, because were driven to build deep, lasting
relationships with customers. Id like to thank our
employees, a group of more than 270,000 people, who
embrace our purpose to help people love where they live,
who dedicate themselves to serving customers and who
have committed to evolving with changing customer needs

Robert A. Niblock
Chairman of the Board, President and Chief Executive Officer

IN-STORE
 More than 1,855 convenient retail store
locations
 Provides access to knowledgeable
associates and immediate fulfillment
of purchases
CONTACT CENTER
 Three contact centers, in
Wilkesboro, NC, Albuquerque,
NM, and Indianapolis, IN
 Direct phone sales and customer
support, including coordinating
project deliveries and facilitating
repair services
ONLINE
 24/7 shopping experience through
websites and mobile applications
 Offers product information and
customer ratings and reviews, as
well as buying guides and how-to
videos and information
ON-SITE
 Specialists meet with Pro customers
in their workplace or at
job sites
 Specialists meet customers in their
home to sell interior and exterior
projects, offering consultations
and project management services
through project completion
CUSTOMER CENTRIC APPROACH