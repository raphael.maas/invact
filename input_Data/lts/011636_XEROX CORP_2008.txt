Dear fellow shareholders:

It will come as no surprise to you that Xerox shareholders
have been negatively impacted by the economic crisis that
has ricocheted around the world in recent months. Through
three quarters of 2008, we were on the path to another year
of solid performance. That trajectory changed dramatically
in the fourth quarter.
One example will make the point. Our developing markets
organization was on track to deliver another year of doubledigit
revenue growth. Through the first three quarters of
2008, revenue was up 17 percent. Starting in mid-November,
the bottom fell out with breathtaking speed, resulting in
a fourth-quarter revenue decline of 14 percent in our
developing markets.

Some of the sudden shift was due to a rapid decline in the
Russian and Eastern European economies. But 11 points
of the 14-point decline was due to major  some would say
wild  shifts in currency in several developing markets.

Typically, we manage changes in currency through pricing,
but this currency decline was so swift and so significant that
pricing couldnt catch up. I point this out not as an excuse, but
as an illustration of the roller-coaster nature of this economy.

Although we are hardly immune from the recessionary turmoil,
we are better positioned than most to navigate through it.
Our value proposition is supported by the strength of our
financial position and the resiliency of our recurring revenue
stream that is driven by installs of Xerox technology and
multi-year contracts for Xerox services. More than 70 percent
of our revenue and 80 percent of our cash flow is generated
from our annuity-based business model, making for a solid
and reliable asset, especially in tough economies. Last year,
our annuity delivered $12.9 billion in revenue  up 4 percent
from 2007  and helped us generate more than $1.7 billion
in adjusted cash from core operations*  or $1.36 per share in
adjusted free cash flow.

Given our cash flow, healthy cash balance  $1.2 billion at the
end of 2008  and a $2 billion credit facility, we remain quite
confident in our financial position and have no need to access
the capital markets in the foreseeable future. Its certainly an
advantaged position in this economy.
Although we are keenly aware that there is much we cannot
control, the wonderful people I am privileged to lead at Xerox
are focused with passion and grit on those things we can
control. This attitude and focus helped us turn in credible
performance in a very difficult year:
 Total revenue for 2008 was $17.6 billion  thats an increase
of $380 million, or 2 percent, over 2007.
 Full-year net income was $230 million including a litigation
charge. Excluding this and certain other charges, adjusted
net income was $985 million.*
 We generated $939 million of operating cash flow. Adjusted
cash from core operations for the year was $1.7 billion.*
 Through the 5 percent of our revenue invested in innovation,
we continued to expand our portfolio of document
management technology and services  already the
broadest in the industry and in our history.
 And we continued to expand distribution, bringing the Xerox
brand to more businesses of any size all around the world.
The proof points are everywhere. The highly respected Gartner
organization lists us as a Magic Quadrant market leader in
managed print services, as well as for printers and multifunction
systems that print, copy, fax and scan all in one device. Our new
offerings last year garnered some 230 awards from industry
groups and media around the world. I could go on, but you get
the point. Third parties are validating our progress and our
leadership in virtually every aspect of our business.
Thats all looking in the rear-view mirror. I dont have to be
a psychic to know that you have little patience for that 
especially in these turbulent times. Neither do I. Ive met with
many investors in recent months and I keep hearing three
questions asked over and over:
 What are you doing to confront the recession right now?
 What are you doing to make sure you come out of this crisis
with a full head of steam?
 Why should I continue to invest in Xerox?
Fair enough. Let me answer each of those questions as
candidly and succinctly as I can.

Actions to Minimize Impact of the Recession
We are doing everything possible to reduce cost  everything
that doesnt mortgage our future. We took a $349 million
restructuring charge in the fourth quarter of 2008 that has
resulted in the elimination of about 3,400 jobs. We expect that
the restructuring will deliver $200 million in savings this year.
2008 bonuses have been scaled back substantially and people
on bonus plans will receive no salary increases this year.
Expenses like training and travel have been significantly cut
back. External hiring has to be approved by me personally.
Weve accelerated the consolidation of manufacturing
facilities around the world, stopped the production of
products made in very low volume, and consolidated
operations wherever we can. Weve realigned our support
operations  including human resources, training, finance
and marketing  to achieve better synergy at lower cost.
Weve streamlined product development and engineering by
combining two product development organizations into one.
So we are taking a long list of actions aimed at getting our
expenses aligned with the realities of faltering economies in
just about every corner of the globe. Our investments over
the years in Lean Six Sigma have given us a set of tools and
processes that simplify and reduce the cost of managing our
global operations. For us, its not an event, but a way of life.
The management team Im privileged to lead is a seasoned
group that knows all about belt-tightening and is eager to
deliver the best possible shareholder value that conditions
allow. You place a lot of trust in us. Its something we take
very seriously.
Emerging with a Full Head of Steam
At the same time, we are not mortgaging our future. We are
continuing to invest for growth and we are continuing to stick
to the fundamentals  starting with listening to our customers.
One of the things Im proudest of is the customer-centric
culture weve developed at Xerox. We have a proven track
record that demonstrates that our customers increasingly see
us not as a vendor, but as a partner.
Our combination of document services, affordable and
innovative color technology, and broader distribution to
businesses of any size positions us exceptionally well to attack
a $132 billion market. Heres how:
We provide document services that help businesses work
faster and smarter with lower costs. Today businesses rely
on us to help reduce their infrastructure costs by optimizing
how they manage their document technology and handle
enterprise-wide printing needs. Also core to our document
services offerings is our expertise in converting paper to digital
and helping customers seamlessly track, edit, share and save
documents in any form.
In a world where 15.2 trillion pages are printed each year
and $650 billion in productivity is lost each year due to the
overload of information, our ability to simplify the way work
gets done helps customers save up to 30 percent on their
document management costs. Thats a figure that is getting
more and more attention as we speak to our customers these
days, many of whom are looking for ways to contain cost
and reduce paper. That may seem counter-intuitive coming
from a company known for putting marks on pages, but
its actually a sweet spot for us, especially in what we call
document-intensive industries like legal, healthcare, financial
services and education.
Increasingly were partnering with IT services companies
like CSC and HCL Technologies to integrate our strength in
document management with their capabilities in managing
enterprise-wide IT systems. And, we are the preferred global
imaging partner for IBM Managed Business Process Services,
a unit of IBM Global Technology Services. Under our worldwide
agreement, were scanning and imaging millions of
documents for IBM and their customers.
We provide affordable color printing systems that cut
through the clutter with high-impact communications. Xerox
continues to benefit from making color printing affordable
for businesses of any size. Revenue from color now represents
more than 41 percent of our total revenue.
The billions of pages printed on Xerox color systems grew
24 percent last year, and total significantly more than any of
our competitors. In fact, according to estimates by InfoTrends,
a leading independent research firm, Xeroxs DocuColor,
iGen3 and iGen4 presses accounted for approximately
one-half of the total worldwide color pages printed by
high-speed digital systems.
With the broadest portfolio of color printing systems on the
market, we continue to create new business opportunities in
production printing through color technology and systems
such as the Xerox iGen4 Press and Xerox 700, both of which
launched in 2008.
This year well shake up the industry even more with the
introduction of a color multifunction system that breaks down
the cost barrier of color printing in the office through exclusive
solid ink technology, which not only lessens the impact on a
business bottom line but also on the environment.
We have more distribution channels to bring Xeroxs
technology and services to more businesses of any size,
anywhere around the world. For decades, weve had the
largest and best direct sales force in our industry. Currently
its over 7,500 strong  one of our crown jewels and an asset
that gives us a competitive advantage. Thats backed up by
Global Imaging Systems, our wholly-owned and growing
network of U.S. dealers, as well as an equally impressive
partnership with agents, concessionaires, resellers and more 
giving Xerox the largest, broadest and most professional
distribution network in our industry.
By acquiring nine office technology dealers in the last two
years and launching 19 products in 2008 designed for small
and mid-size businesses (SMB), we have significantly increased
our penetration in the SMB marketplace. In fact, the number
of installs for our desktop printers and multifunction systems
grew 10 percent last year, largely due to more channels selling
the Xerox brand.
Bottom line: yes, we are feeling the impact of the recession;
yes, we are moving aggressively to reduce costs, generate
cash and weather the storm; but no, we are neither giving
up on 2009 nor mortgaging our future by compromising
on investments that will give us momentum as we come out
of the economic downturn.
The Case for Xerox as an Investment
We firmly believe that we will navigate through this set of
challenges and emerge stronger than ever. I say that for
several reasons.
We continue to enhance our leadership position in document
technology. Last year, Xerox inventors earned more than
600 U.S. utility patents. We currently hold more than 8,900
active patents in the U.S. and, together with our research
partner Fuji Xerox, we have received over 55,000 worldwide
patents in our history.
Our research leadership yields the best and broadest set of
offerings in our industry. Over the past three years, we have
launched more than 80 products  including 29 new products
last year with about the same number expected this year.
Our distribution and services capacity is also the best and
broadest in our industry. Through our vast and growing
network of direct sales, agents, resellers, Global Imaging
partners and distributors, we do business in over 160 countries.
This is a huge competitive advantage, as customers depend
on us for global account management and increasingly
want to move documents around the world with speed, ease
and security.
Our strategic bets in the marketplace are paying off in
areas like color and document services. Color pages now
represent 18 percent of total pages printed on Xerox
technology, up from 12 percent in 2007. We lead the industry
in the number of color pages printed. In services, we generated
$3.5 billion in annuity revenue last year, a year-over-year
increase of 3 percent.
Our recurring revenue stream represents more than 70
percent of our total revenue. That gives us some cushion
in challenging economic times and helps fuel the $1.7 billion
in cash that we expect to generate this year from core
operations.
Our leadership team is battle-tested and results-driven.
They are an unusual mix of Xerox veterans, new leaders who
have recently emerged from the ranks and people who joined
us from other leading companies. They are all driven by one
goal  the success of Xerox measured by the value we deliver
to our shareholders.
Last but hardly least is the quality and dedication of our
workforce. Together with our technology and our distribution,
they provide the knowledge and the passion to bring value to
our customers. When I visit customers, its our people they
want to talk about. Customers sing the praises of Xerox people
and want to talk about their focus on solving problems, their
can-do attitude, and their desire to do whatever it takes to get
the job done. At the end of the day, Xerox people spell the
difference between failure and success.
So we are feeling good about where we are. There is a fair
amount of disruption in our industry  some of it brought on
by the economy but much of it brought on by Xerox. Over the
past few years, we have upped the ante considerably on the
technology we have brought to market. We have both built
and acquired new document services offerings. And we have
expanded our distribution.
We are proud of but not content with the competitive
advantage we have created in our industry. We know its a
never-ending battle but its one were eager to wage. We feel
the same way about corporate responsibility. Even in the worst
of times, we continue to make appropriate investments in
the communities in which we work and live. We continue to
be a leader in diversity in all its forms. We continue to fight
for a sustainable world and a greener planet.
We dont have our heads in the sand about the challenges
we face, yet we believe we are better positioned than most
to meet the obstacles that 2009 will certainly bring our way.
There are likely to be winners and losers when the dust finally
settles. We feel confident we will be a winner.

Anne M. Mulcahy
Chairman and Chief Executive Officer









