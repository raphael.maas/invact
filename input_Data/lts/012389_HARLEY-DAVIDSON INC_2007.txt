DEAR FELLOW SHAREHOLDERS:
2007 WAS A LOT LIKE THAT MOTORCYCLE TRIP YOU TAKE IN EARLY SUMMER IN THE MOUNTAINS  THE ONE WHERE THE FORECAST
IS FOR BLUE SKIES AND DRY ROADS AND, ALL OF A SUDDEN AT 9,500 FEET OF ELEVATION, YOU FIND YOURSELF IN THE MIDDLE OF A
SNOWSTORM. YOU KNOW THAT WITH CAUTION AND PERSEVERANCE YOULL REACH YOUR DESTINATION. BUT IN THE MEANTIME, ITS
SURE TOUGH TO GET MUCH TRACTION AND IT MAKES FOR SOME UNCOMFORTABLE RIDING!

Harley-Davidson had a challenging year
in 2007 as growing troubles in the U.S.
economy increasingly impacted major discretionary
purchases like motorcycles.
In that environment, Harley-Davidson
full-year revenue and earnings declined
year-over-year, as did the number of new
Harley-Davidson motorcycles our U.S.
dealers sold at retail. Even so, except for
2006, it was the biggest year in our history
for revenue, earnings and worldwide retail
motorcycle sales. Revenue for the year was
$5.73 billion, compared to $5.80 billion in
2006 and $5.34 billion in 2005. Dealers sold
337,774 new Harley-Davidson motorcycles
worldwide in 2007, compared to 343,981 in
2006 and 317,169 in 2005. Diluted earnings
per share were $3.74, compared to $3.93 in
2006 and $3.41 in 2005.
A major factor affecting revenue and
earnings in 2007 was our decision to reduce
fourth-quar ter motorcycle shipments, in
keeping with our commitment to ship fewer
Harley-Davidson motorcycles than we anticipated
dealers would sell at retail worldwide.
For the full year 2007, we shipped 330,619
Harley-Davidson motorcycles, which was
5.3% less than the prior year.
Our decision to reduce shipments underscores
the priority we place on brand strength.
Our strong brand is the backbone of our
business and the value proposition of our
motorcycles. We will continue to be careful
guardians of the brand by managing the
relationship between supply and demand
and managing the business for the long term.
Turning to Harley-Davidson Financial
Services, we all know what a challenging
year 2007 was in the consumer credit
industry. However, HDFS is a premier financial
services company with a reputation
for a disciplined and prudent approach to
lending, and for the full year, HDFS realized
a 0.7% increase in operating income compared
to 2006. HDFS also grew its market
share for loans for new Harley-Davidson
motorcycles in the U.S. to 55% in 2007,
up from 49% in 2006. HDFS continues
to serve a broad range of customers,
lending across all credit tiers and appropriately
balancing for risk and reward.
And although HDFS was not immune to
some of the pressures in the credit
markets in 2007, its results demonstrate its
understanding of how to effectively serve the
needs of dealers and their retail customers.
INTERNATIONAL STRENGTH A major bright
spot in 2007 was the continued success of
Harley-Davidson on the international stage,
where dealers retail sales grew 13.7%
compared to the prior year. Our international
strength is the result of a well-orchestrated,
strategic approach we embarked on a number
of years ago to grow Harley-Davidsons
international retail motorcycle sales faster
than U.S. sales. Our international strategy has
resulted in sound investments in marketing
and products that are attuned to local customer
preferences, improved distribution at
the wholesale level, dealer network strength,
and assuming ownership and management
of business operations in an increasing
number of key markets.
Perhaps more than anything, our international
growth is a sign of the incredible
strength of the Harley-Davidson brand
abroadand the ability of the brand to
transcend cultures and languages. As youll
see from the examples in this annual report,
Harley-Davidson consistently delivers
culturally -relevant experiences in the 72
international countries in which we sell our
motorcycles. Our international employees 
the vast majority of whom are local market
nationalsand our dealers understand the
challenges and opportunities and how
to fine tune our approach to the cultural
expectations of each market.
Going forward, we believe international
markets will continue to be an area of
strength and we are continuing to refine our
business and marketing strategies for
Europe, Asia, Australia, Latin America and
Africa to leverage those opportunities.
THE ROAD AHEAD Looking ahead, we
anticipate that weakness in the U.S. economy
will continue to make 2008 challenging
for Harley-Davidson and we have again
committed to ship fewer Harley-Davidson
motorcycles than we expect dealers to sell
worldwide. At this time, we believe this is
the right course for the brand, our business,
for dealers and for customers.
At the same time, we continue to devote
major resources to drive our business forward,
investing heavily in marketing, product
development and our people. We believe
there will be solid opportunities to grow when
the U.S. economy rebounds and we intend
to be well-positioned to take advantage of
those opportunities.
CUSTOMER FOCUS Customer relationships,
market -defining products and extraordinary
customer experiences are the not-so secrets
to our success. We intend to keep it that
way through a continued focus on these
strengths, connecting with core customers,
crossover prospects and non-riders like never
before, and staying ten steps ahead of our
competitors. From unmatched events like
our upcoming 105th Anniversary Celebration
or an exclusive Nightster motorcycle
launch par ty for young adults to our Dark
Custom family of motorcycles (check them
out at www.Harley-Davidson.com), we continue
to make our brand more relevant to
more people.
LEADERSHIP FOCUS Harley-Davidson has
achieved its success over the years through
the talent and dedication of our employees,
growing the Company from less than $2 billion
in revenue a decade ago to nearly $6 billion
in 2007. Today, more than ever, having capable
leaders is crucial to our success.
We are currently in the process of
transforming our internal culture to maximize
the talent and contributions of our existing
workforce, making leadership development
a key priority. And were bringing in new
people for the fresh perspectives and
capabilities they can provide.
I want to make sure that our peopleand
especially our leaders at all levels of the
organizationare well-prepared to meet the
challenges we face and to make the most of
Harley-Davidsons many business opportunities.
My goal is to make sure that we are
world class when it comes to leadership,
just as were world class when it comes to
our motorcycles.
ORGANIZATIONAL STRUCTURE To make
Harley-Davidson more agile and adept
going forward, we made major changes
in our organizational structure in 2007.
Our new, more streamlined structure promotes
faster decision making, greater
responsiveness to rapid change, and clearer
accountability and responsibility for results.
Karl Eberle now serves in the newlycreated
position of Senior Vice President,
Manufacturing, responsible for companywide
manufacturing strategy and operations.
Ron Hutchinson is now in the newly-created
position of Senior Vice President, Product
Development, responsible for engineering,
materials management and Parts & Accessories.
They bring 47 combined years of
experience at Harley-Davidson to these
key operational areas. And mid-year,
Mark-Hans Richer joined the organization in
the newly-established role of Senior Vice
President and Chief Marketing Officer, with
overall responsibility for the brand and motorcycle
product planning, and for U.S. marketing
and customer relationships.
2008AN EXCITING YEAR Im expecting a
challenging year in 2008 but its also going
to be an incredible year.
Anticipation is building for the opening of
the Harley-Davidson Museum this summer.
The Museum is much more than our collection
of memorabilia and bikes. It will be an
exciting destination for connecting, sharing a
road story or a meal, celebrating and fueling
Harley-Davidson dreams!
This summer, H.O.G. celebrates its 25th
anniversary. So does Buell Motorcycle Company.
Our police motorcycles business marks
its 100th anniversary. Buell is also pumping
out lots of excitement and reaching new
customers with the launch of its all-new,
liquid-cooled 1125R motorcycle, delivering
a true balance of street and track performance
in the superbike category.
What else is going on? Way more than I
can cover in this letter! Weve doubled the
size of our demo motorcycle fleet, bringing
the Harley-Davidson experience to more
riders than ever. Our event teams will be
putting on more and better consumer events
around the globe. And in July, well have
a major injection of adrenaline with the
introduction of the 2009 motorcycles.
A few weeks later we celebrate our
great future, not to mention 105 years of
riding proud, with the biggest anniversary
ride in company history and four days of
celebration in Milwaukee. With the entertainment
and activities weve got in store,
our 105th Anniversary Celebration will
deepen the passion of current customers
and ignite the dreams of new ones like only
Harley-Davidson can.
I cant think of another consumer products
company that can lay claim to that level of
customer activity and excitement in one year.
So even though the economy has hit a
rough patch, I am very enthusiastic about our
future. With the most passionate employees,
customers and dealers anywhere, our
powerful brand and our great motorcycles, I
believe there is outstanding opportunity for
Harley-Davidson throughout the world for
years to come.
JAMES ZIEMER,
President and Chief Executive Officer,
Harley-Davidson, Inc.