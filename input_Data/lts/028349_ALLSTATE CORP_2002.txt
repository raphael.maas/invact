2
Edward M. Liddy
Chairman, President and CEO
The Allstate Corporation
On behalf of our Allstate employees
and dedicated agencies, I am pleased to
report that your company had a very good
year in 2002. We significantly improved
our financial results, while some of our
competitors continued to struggle. This
has enabled us to advance our strategy
of helping to protect and secure the
financial future of our customers, and
enhanced the value of your shares.
Just as important, we continued to
transform Allstate by getting better and
bigger in the personal lines insurance
market and by broadening our financial
services offerings. By doing so, we were
able to give our customers more ways to
protect and grow their assets. Allstates
successful implementation of our
business plan should provide for more
predictable and sustained growth, create
additional opportunities for our employees
and agencies and ultimately translate
into a reliable and rewarding picture for
our investors.
In a time of corporate mistrust, you
should feel confident that your board
worked diligently over the past year
to further strengthen our corporate
governance processes. These processes
were recently recognized by a leading
investor services group as being among
the very best in corporate America.
With the 10-year anniversary of our
initial public offering only months away,
Allstate is well-positioned for both
accelerated growth and sustained
profitability. Over the past decade, we
have continuously refined our strategy to
meet the ever-changing expectations of
the marketplace.
We have broadened our business so
that, as we reach out in new directions,
our journey will take us both further and
faster in our quest to reassure you, our
shareholders, that Allstate has the right
strategy at the right time. Our performance
in 2002 represents another
milestone in achieving those goals that
we set for ourselves and well endeavor to
build on those accomplishments in 2003.
Message from the Chairman
At the core of our company are only two
things: people and capital. With the right
people and sound stewardship of our
financial resources, we can confidently
deliver on our promise - to give our customers
peace of mind by protecting their
property and their lives, and to help them
achieve financial security.
Having the assurance of one of the
industrys strongest balance sheets helps
give us, and our customers, that confidence.
But more importantly, Im fortunate
to work with the finest employees and
agencies in the business, and to have the
support and good sense of the best management
team in the insurance industry.
Together, we have fashioned a strategy
that worked very well in 2002, and has us
readied for the future.
Im honored to lead the work ahead.

3
2002 Results
As usual, numbers help tell the story:
 In our Allstate brand standard auto
insurance, our largest single product,
Premiums written2 rose 5.9 percent
last year and loss ratios moderated
compared with 2001.
 The improvement was even more
dramatic in our Allstate brand homeowners
product, where Premiums
written were up 18 percent. As
promised, were on track to bring profitability
to targeted levels by mid-2003.
 Our Ivantage independent agency
business increased Premiums
written by 4.5 percent and cut its
Underwriting loss3 approximately in
half. Were on schedule to achieve
targeted levels of profitability.
 In financial services, we performed
well despite one of the most difficult
market environments in memory.
Overall Property-Liability Premiums
written rose 5.8 percent and Operating
income jumped 54.8 percent, driven
largely by decisive pricing and risk management
actions. Premiums and
deposits4 in our Allstate Financial business
rose 11.6 percent and Operating
income gained 5.5 percent.
For the company as a whole,
Operating income increased 39.1 percent,
from $1.49 billion to $2.08 billion.
A solid performance.
These accomplishments helped drive
up the value of your Allstate stock during
Allstate today has a window of opportunity we
can use to strengthen and consolidate our
competitive position. Were determined to seize
that advantage. Execution will be the key.
2002. Total return for the year increased
12.3 percent over 2001, compared with an
average loss of 22 percent among companies
in the Standard & Poors 500 (S&P), as
well as outperforming all insurance indices
that S&P tracks. Your dividend increased
for the ninth straight year, while we completed
one share repurchase program and
authorized another in early 2003.
Of course, not everything was perfect.
We had our challenges: Declining interest
rates reduced our investment income
and, as a result of deteriorating capital
markets, losses on securities in our
investment portfolio cut into Net income.
Elsewhere, regulation and other factors
slowed opportunities for growth in
key states such as California, Texas and
Florida. Issues like medical inflation and
mold claims, as well as adverse trends in
environmental and asbestos litigation,
caused us to strengthen reserves.
In all of these areas, we acted quickly
to confront concerns. We did the same
with respect to rate increases in auto and
homeowners. Because we were ahead
of the industry curve, we are hitting
profitability targets faster and gaining
greater strategic flexibility than many of
our competitors for 2003 and beyond.
Other trendlines are pointed in
similarly positive directions:
 In our protection business, the focus
of our customer base is shifting. Were
increasing the number of more profitable
customers while decreasing the
number of less profitable ones.
 In financial services, our mix of business
likewise is expanding from a
primary reliance on life insurance
products toward more asset accumulation
products across a range of
distribution channels.
 We continue to refine our organization
to reflect the companys business
strategy. In September 2002, we
joined our Allstate property and casualty
and Ivantage businesses to
leverage their resources. We intend to
strengthen and deepen the relationships
with agencies and customers in
both channels.
 Our protection claims organization
successfully completed a national
consolidation of claims offices, substantially
reducing overhead costs
while allowing us to focus our people
and resources on front-line customer
service and severity control.
This progress reflects the effort and
expertise of everyone in the organization.
Change is never easy, even when its
working. But what weve accomplished
has created real momentum. Allstate
today has a window of opportunity we
can use to strengthen and consolidate
our competitive position.
Were determined to seize that
advantage. Execution will be the key.
Underlying our better, bigger and
broader strategy are seven priorities that
will help make Allstate the company customers
prefer to help bring alive their
version of the American Dream.


Our focus is clear. Allstate does not want
to be all things to all people in financial
services. We want to be more things to
more people. So we dont plan on being
a player in the consumer finance or brokerage
businesses, for example. Instead
we focus on areas adjacent to our existing
business  asset protection, asset
accumulation and life insurance and savings.
Within our overall customer base,
we likewise focus on an underserved
segment of the financial services marketplace.
Households earning between
$30,000 and $100,000 a year have an
aggregate income of $3.1 trillion. Like
most Americans, they want to build
assets for a home, for college education
and especially for retirement.
Our protection business gives Allstate
a great place to start  because of our
brand name and large customer base.
Also, because insuring assets like homes
and cars is the first step toward financial
security. But to build on that base for the
future, we must make the most of our
existing customer relationships.
Thats where our Allstate agencies
play a critical role. They have answered
the challenge and broadened their
expertise. More than half have studied,
tested and are licensed to sell a broad
range of financial products. We call them
Personal Financial Representatives. More
than a third also regularly partner with
Allstates Exclusive Financial Specialists
(EFS)  more than 1,000 professionals
who focus solely on life insurance and
financial, rather than property/casualty,
products.
The strategy is already working.
New sales of financial products by
Allstate exclusive agencies5 reached
$1.6 billion in 2002  more than in the
three previous years combined.
In addition to life insurance, annuities
and mutual funds, we offer insured
savings accounts, certificates of deposit,
insured money-market accounts and
other savings and lending products
through our Allstate Bank. Deposits have
grown rapidly, reaching $610 million in
less than 18 months. Its one more way
were reaching customers with new
solutions that fit their financial futures.


Allstates local presence is a long-standing
strategic advantage, and it will be just
as important in the future. But running
an agency is harder than it used to be.
Competition is keener, costs keep rising
and the industry keeps changing.
Weve made some changes too. In
recent years, for example, almost all
Allstate employee agencies have made
the shift to independent contractors.
Likewise, we asked our agencies to
become educated on new products,
new processes, new technology. To be
competitive, all these changes were
essential. But they still were wrenching.
So now, more than ever, our job at
Allstate is to help make the work of our
agencies easier, not harder. And to give
them more ways to win.
One way we can help is by providing
more opportunities to sell more products
to more customers. Our expansion into
financial services, for example, is broadening
their revenue base.
Well also help by expanding local
agency advertising and marketing
programs in 2003. Well make more technology
enhancements available, based
upon guidelines and feedback from
agencies themselves. And, by linking
compensation to strategies that
produce more profit for the agency
and for the company, well help
motivate and encourage agencies
to build their businesses.
Were working in other ways,
too. Because so many agencies
are active as volunteer leaders in
their communities, were increasing
financial and other support for local
community efforts.
All this represents progress. But
we need to do much more. Nationally
the trend is toward larger agencies
with more support staff. So, for
agencies, finding and training more
employees, organizing businesses for
greatest efficiencies, and financing
growth initiatives all are issues we
must help them address. By listening
harder to them, and working harder
with them, well help create more
opportunities for them.


1. Meeting Customer Needs
Putting the customer first is too often an empty promise. Mistrust in
the marketplace is real, and it creates problems for many companies.
But it also creates opportunities for those who can consistently meet and
exceed expectations. Allstate is working to be that kind of company.
In the recent era of rapid growth and mass
marketing, many businesses believed the
key to success was pushing product.
Many companies still do. But its a strategy
out of sync with todays realities.
We know the only way for Allstate
to win in a crowded and competitive
marketplace is to understand and meet
customer needs, one-on-one, better than
anyone else in the business. So were
constantly studying what matters most to
our customers  what attracts them, satisfies
them and keeps them coming back.
For example, research shows that
Allstate customers closely associate our
traditional protection products with other
financial services. But to help make that
connection a reality, we have to offer a
spectrum of financial solutions  from
protecting their current assets with
products like auto and homeowners
insurance to providing for their future
financial security with savings and
investment opportunities.
And were constantly assessing
market trends to introduce new products.
In 2002, for example, we redesigned
some long-term care and life insurance
policies and introduced important new
annuity features for customers.
We also offer alternatives in terms of
accessibility. Were available to customers
through Allstates exclusive agency
network, the Internet and our toll-free
telephone number. Moreover, customers
can also buy Allstate products through
independent agents, banks and financial
services firms.
To succeed, we must pull together our
various products and ways to interact
with customers in ways that help us
understand and anticipate customers
current and future needs. One tool
were testing is Enterprise Customer
Relationship Management (ECRM).
Its a technology that helps us better
understand our customer needs,
especially in response to significant
life events. It gives our agencies, our
Web site and our telephone centers a
better ability to interact with our customers
in a more personal manner.
By tapping into true needs rather
than simply pushing product, we
intend to draw on the reservoir of
consumer goodwill that we know
exists. Customers want to be satisfied,
to be delighted, to be loyal. Our job is
to help make that possible.

2. Help Middle America Achieve
Financial Security
But who are our customers? They are teachers, firefighters, professionals
from all walks of life. Mostly they are middle-income Americans who
want and need better protection and retirement planning. Many have
been ignored or intimidated by traditional financial services companies.
And with relationships with one out of six American households, Allstate
is in a unique position to help them achieve financial security.
Steinbacher residence, Marysville, Wash.  Commercial tradesman Jason Steinbacher and wife Andrea
(with newborn daughter Malia) think they will move to a larger home soon, and they intend to seek out their agent
Bruce Pleasant when they start planning for Malias education costs.


3. Improve Agency Relationships
Meeting customers needs and helping them achieve financial
security is everyones job at Allstate, but the people customers see
most  the faces and voices of our company in the community 
are Allstate agencies and EFSs.
The Parthenon, Nashville, Tenn.  Dennis Smith was among the first agents to secure his Series 6/63 licenses. Smith also has partnered with Allstate
to develop new ways for agencies to manage their businesses more efficiently.


4. Deepen Relationships With
Our Financial Partners
Allstate exclusive agencies serve one of the biggest customer bases
in the business, with a presence in 14 million American households.
But the other 90 million-plus U.S. households need financial services too.
To be a leader, we must be able to serve consumers who prefer to do
their financial business through other channels.
Allstate reaches these consumers in a
variety of ways. Approximately 80,000
independent agents and financial planners
sell Allstate products under the
Lincoln Benefit Life name. We also provide
Allstate Financial products through a
wide range of blue-chip financial services
firms, including many of the largest
national and regional investment advisory
firms and banks, as well as through independent
insurance agents and
brokerages.
Diversified distribution is good
for growth. In 2002, for example, total
Premiums and deposits in the bank
channel rose 38 percent, easily surpassing
projections. Today, Allstate ranks
third in life insurance, sixth in fixed annuity
and eighth in variable annuity sales
through banks.
Were also diversified in terms of
products we can offer customers  from
traditional life insurance to mutual funds
to annuities and 529 college savings
plans. And were constantly adding to
our portfolio based on feedback from
consumers and producers. Product
development at Allstate today is faster
and more focused.
One example is the Allstate
Treasury-Linked Annuity, which combines
the safety and security of a fixed
annuity with the opportunity for growth,
based on the five-year U.S. Treasury yield.
Based on extensive investor research
and developed by a cross-functional
team of Allstate Financial and Allstate
Investments professionals, the Allstate
Treasury-Linked Annuity was launched
in February 2002. It recorded $762 million
5. Simplify Doing Business
With Allstate
Reaching customers through Allstate agencies or other distribution channels
is only the beginning. To succeed, we need to nurture customer
relationships. Every interaction is an opportunity to serve, satisfy and retain
their loyalty. So we need to provide real solutions and support  no matter
what, where or when.
in sales in just nine months. Some
of its features are so unique weve even
applied for a patent.
Another growing distribution
channel focuses on workplace marketing.
Allstates Workplace Division sells
life insurance, disability and supplemental
insurance coverage through
independent agencies to employees at
more than 20,000 small and mid-sized
businesses across the country.
All told, these non-proprietary channels
accounted for 80.2 percent of the
total Premiums and deposits generated
by the Allstate Financial business unit
in 2002. Our aim is to continually
strengthen these relationships by
offering more products and generating
more revenue with our successful
producer-partners.
Our most important interactions with
customers involve claims. So were constantly
seeking innovative ways to make
the claims experience faster, better and
more attuned to customer needs.
For example, we expanded our
innovative Sterling Autobody Centers,
which offer customers the option of having
Allstate-owned facilities repair their


a wealth of services and information,
including paying a bill, reporting a claim,
purchasing a policy, viewing policy
information or learning about investment-
planning basics.
The response has been positive.
Registration for our online Customer
Care Center more than doubled during
2002. Nearly nine of 10 visitors were
either very or completely satisfied with
the overall experience.
Like our Web presence, 1-800-Allstate
is available to customers 24 hours a day,
seven days a week. For example, our tele-
Allstate Customer Information Center, Charlotte, N.C.  Allstate CICs provide extensive upfront and ongoing training to elevate
and maintain strong customer service skills. (Instructor Charlotte Tucker pictured.)
autos. Compared with traditional
alternatives, Sterling Autobody
Centers have substantially lower
repair times. Plans call for continued
expansion in 2003 and beyond.
Beyond claims, customers have
more routine interactions such as
policy changes or bill payments. And
they want to do them on their time,
on their terms. We try to make that
as easy as possible.
So were continually expanding our
online presence at allstate.com. Most
American customers can choose from
phone centers answered more than
50,000 calls for agencies that were
closed the day after Thanksgiving
last year.
Our Customer Information Centers
(CIC) offer all the services available
on our Web site and more. All told,
1-800-Allstate handled nearly 10 million
calls last year from claims reports
to agent referrals.
Its already a powerful partnership.
Allstate.com and 1-800-Allstate help
handle many routine service tasks,
freeing agencies to focus more on
building relationships and satisfying
customers. They also bring new
customers into agencies. Our focus
will be to continue to leverage this
relationship with Allstate agencies
and Exclusive Financial Specialists.


6. Achieve Profitable Growth
Focusing on customers first is the surest way to build our business.
But as we expand, we must keep a close eye on our bottom line and
our capital as well.
to total policies in force. The same is true
when we sell more policies to the same
number of customers.
Its also important to remember that
the impact of SRM is cumulative. Today,
customers priced through SRM represent
about 25 percent of our total
business. Within five years, they will
account for two-thirds, and the cumulative
effects will be significant. One
example: High-lifetime-value customers
are renewing their policies today at a rate
1 percent to 2 percent higher than other
customers. Over 10 years, that could
mean substantially fewer defections.
Moving forward, we know profitability,
growth and acceptable returns can live
together, even when the need arises to
adapt to different regulatory and market
conditions. In 1998, for example, Allstate
created a separate property/casualty
company in New Jersey to deal with
profitability issues. Today, Allstate
New Jersey is profitable and growing.
Were currently looking at this and
other options in states where we face
similar profitability challenges.
Finally, generating profitable
growth requires that we be highly
visible in the marketplace. Thats
why late last year we dramatically
increased advertising and marketing
spending, with a focus on encouraging
current single-line customers to
become multi-line customers. Well
build on that marketing momentum in
2003 and supplement national efforts
with local plans in key markets.
It all starts with our Strategic Risk
Management (SRM) tool that integrates
underwriting, pricing and marketing
to help attract and retain the right customers
with the right products at the
right price. In particular, SRM helps us
attract and keep high-lifetime-value customers
 the kind who are more likely to
renew their policies and buy more products
from Allstate.
In one 22-state sample, for instance,
the share of new standard auto customers
in high-value segments increased from
28 percent to 43 percent. Retention rates
in those segments are higher. And those
customers buy more, too. In the same
study, cross-sell results were 51 percent
more in higher-value segments.
The long-term benefits are obvious:
When we lose fewer customers, we add


7. Maintain Our Financial Strength
Supporting growth  in our customer base, in our distribution networks and
in our product portfolio  is the financial strength that has made us a stable
company for decades. When we tell our customers, our investors and our
employees, Youre In Good Hands with Allstate, the commitment carries
with it an awesome responsibility. While some companies today find their
resources and reputations declining, financial strength ensures that Allstate
will be able to keep its promises.
Strength comes from strategy and
focus. Since our public offering, we sold
non-core businesses, including large
commercial accounts, reinsurance and
mortgage insurance. That channeled our
energies and resources to markets where
we had the best chance of winning, while
limiting our catastrophe exposure and
shielding our asset base against multibillion-
dollar losses.
Financial strength comes from
sound governance, transparency and a
commitment to integrity. Our board is
recognized as one of the most independent
in America. In recent years weve
increased the transparency of our financial
reporting. Whats more, we operate
in one of the most regulated industries
anywhere. Our principles, accounting
and otherwise, are sound. So when I sign
our financial statements every quarter,
I do it with a steady hand.
Strength also comes from sound
investment decisions that help improve
returns for shareholders. Allstate
Investments, LLC, an Allstate subsidiary,
manages our $91 billion portfolio. We
believe in being diversified and valueoriented.
While almost no one emerged
unscathed from economic downturns in
recent years  Allstate fared far better
than most. In 2002, for example, our
investment writedowns, while substantial,
amounted to only one half of 1 percent of
the average portfolio for the year. And
while the major rating agencies downgraded
the life operations of many other
companies in the industry, our ratings
from A.M. Best Company, Moodys
Investors Service and Standard & Poors
remained unchanged at the same high
levels that we have consistently enjoyed
over the past five years.
Finally, financial strength comes from
always being conscious of costs. In 2002,
our Property-Liability expense ratio
declined again, from 23.9 percent to
23.3 percent. The reduction reflected
premium increases and companywide
efforts by individuals at all levels. Six
Sigma initiatives implemented in Allstate
Financial and a more rigorous procurement
governance process also yielded
incremental savings.
Staying true to these priorities while
making prudent moves to ensure that our
reserves are sufficient to address liquidity
requirements will help keep us financially
strong far into the future.

Looking Ahead
Clearly, we have a full plate of initiatives going forward. Our agenda is
ambitious, yet achievable. And our confidence in the future is bolstered by
lessons from the recent past.
In the near decade since becoming
a public company, much has changed at
Allstate. But several characteristics have
remained consistent:
 We have an unwavering commitment
to integrity. Our word is our bond.
 Our approach to change has been
calculated, not haphazard. We built
on our core strengths.
 We place a premium on execution.
We did what we said wed do.
 We demonstrated a passion for
success. Our employees have
been able to embrace, and to
implement change.
These attributes will continue to
serve us well. In many ways, Allstate
today represents the best of both worlds.
As a company with roots that go back
more than 70 years, we have financial
strength and one of Americas finest
brands. Having functioned on our own
as a public company for nearly a decade,
were becoming nimble enough to
respond to changing markets and
customer preferences.
The combination makes us a
formidable competitor. Well draw on
all of these resources as we expand our
operations and our opportunities in
the years ahead.
Sincerely,
Edward M. Liddy
Chairman, President and CEO