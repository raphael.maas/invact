                                                        TO OUR SHAREHOLDERS:


                                                        In addition to celebrating Bard's 100th anniversary, 2007 will
                                                        be remembered for another milestone: we surpassed $2 billion
                                                        in annual revenue for the first time. We also marked five
                                                        consecutive years of meeting or exceeding our adjusted
                                                        earnings per share growth objective of 14%. This success is
                                                        the result of good execution of our plan, the dedication of our
                                                        employees to serve patients and the unwavering support
                                                        and confidence of our valued shareholders.
                                                        As we begin our second century, the world's demographic
                                                        landscape is vastly different from the days of our founder,
                                                        Charles Russell Bard. Our worldwide population is enjoying
                                                        a longevity boom, and Bard is uniquely positioned to play a
                                                        pivotal role in tackling many of the inevitable issues associated
                                                        with the aging body. As people live longer and pursue more
                                                        active lifestyles, there is a mounting demand for innovative
                                                        medical devices that will not only extend their lives, but
                                                        preserve their health and quality of life. With our diverse
                                                        product portfolio, Bard stands ready to meet this growing
                                                        demand for life-enhancing and life-prolonging treatments.
                                                        Our technologies in high-growth segments of health care 
                                                        infection control, hernia repair, oncology, electrophysiology,
                                                        obesity therapy, continence management, pelvic floor
                                                        reconstruction and dialysis, to name a few  represent
                                                        opportunities in areas where patients are demanding better
                                                        treatment options.
     
                                                        Bard's sound financial position allows it to invest in new
        
                                                        technologies to improve patient outcomes and help achieve
    
                                                        consistent, long-term growth. While our overall business
                                                        strategy remains unchanged, we expect the volume and
                                                        velocity in each of our strategic investment areas  research
                                                        and development (R&D) and clinical work, business
                                                        development, and sales force expansion  to continue
                                                        to increase. The execution of this strategy has made
                                                        the following results possible:
                                                        2007 Financial Highlights
                                                         Net sales growth: 11% as reported and 9% in
                                                          constant currency
                                                         Net income from continuing operations: $406.4 million
                                                          as reported; $404.2 million (up 15%) excluding items
                                                          that impact the comparability of results between periods
                                                          as identified in financial highlights on page 1
                                                         EPS from continuing operations: $3.84 as reported;
                                                          $3.82 (up 16%) excluding items that impact the
                                                          comparability of results between periods as identified
                                                          in financial highlights on page 1
                                                         Stock price: up 14% over 2006 close




2
Bard's tradition of excellence has been shaped by a legacy              of highly flexible, fracture-resistant stents. Upon Food and
of exceptional leaders. In celebrating our centennial year, we          Drug Administration (FDA) approval for the treatment of
would like to pay tribute to the most recent trio of chairmen           blockages in the superficial femoral artery (SFA), we expect
who guided us from the early 1970s through the first years              that the LIFESTENT product will add significant strategic value
of the new century: Robert H. McCaffrey, who joined Bard                to our portfolio of non-coronary stent products. The LIFESTENT
in 1976 and retired in 1988; George T. Maloney, who began               system, together with our FLAIRTM arteriovenous access stent
his career with Bard as a sales representative in 1959 and              graft and ELUMINEXXTM iliac stent  both also pending FDA
retired in 1993; and William H. Longfield, who joined in 1989           approval  will give Bard one of the broadest product offerings
and retired in 2003. Their steady guidance through recent               for peripheral vascular stenting in the industry. We anticipate
decades positioned Bard well for a new century of success               FDA approval for these products in the near future.
in the medical device industry. We thank them for their
                                                                        Other notable achievements in business development in 2007
leadership and vision and the strong foundation they have
                                                                        include: obtaining a license from Genzyme Corporation to
established for the current management team.
                                                                        market and manufacture the SEPRAMESH hernia repair product
We cannot deliver strong financial performances year after              line  which accelerates Bard's entry into the absorbable barrier
year without the commitment and loyalty of our employees                mesh market; purchasing from A.M.I. GmbH the unique
worldwide who share Bard's values. To commemorate our                   PERMASORBTM resorbable fixation device used in ventral hernia
centennial in 2007, our employees volunteered to perform                repair; and acquiring from Inrad, Inc., the ULTRACLIP breast
100 Acts of Kindness throughout the year. Thanks to an                  tissue marker used in ultrasound-guided breast biopsies. Each
outpouring of efforts to benefit their local communities all over       of these product lines creates exciting growth opportunities
the world, our employees tallied nearly 250 Acts of Kindness,           in 2008 and beyond, and we are grateful for the commitment
ranging from holiday food drives and walks to raise money               and diligence exhibited by our business development team
for cancer research, to fund raising for a children's hospice           in their pursuit of new growth platforms for Bard.
and making critical repairs to a rural elementary school in
                                                                        Along with our emphasis on acquisitions, we continue to devote
Mexico. We are proud of their enthusiasm and generosity,
                                                                        significant resources to internal research and development.
both within and outside of the workplace.
                                                                        In 2007, we invested $136 million in R&D, including purchased
                                                                        R&D. We generated 333 patentable ideas and filed 264 patent
Business Development Review
                                                                        applications and had 71 patents issued. These efforts enhanced
In 2007, we continued to improve our business development
                                                                        our pipeline with technologies that will help Bard maintain its
execution as a core component of Bard's product leadership
                                                                        position as a leading innovator in the medical device industry.
strategy. Robert L. Mellen, Vice President, Strategic Planning
and Business Development, and our divisional business                   The application of our infection control coating technology to
development directors (pictured on page 4) have been building           address the deadly threat of ventilator-associated pneumonia
and refining our processes, resources and expertise over                (VAP) provides an excellent example of the foresight and
the last several years. Bob and his team identify and pursue            diligent efforts of Bard's R&D engineers. Nearly 10%1 of patients
new technologies and companies that meet our well-defined               intubated with an endotracheal tube for more than 24 hours
criteria. In addition to providing clinical and economic value,         develop VAP, which has a 50%2 mortality rate. In late 2007,
these opportunities are targeted to:                                    after completion of the largest and most extensive clinical
                                                                        trial in the company's history, the FDA approved Bard's claims
 generate a market leadership position;
                                                                        that our new, proprietary AGENTOTM I.C. endotracheal tube
 deliver sustainable double-digit revenue growth;
                                                                        reduces VAP by 36% in the first 24 hours after intubation,
 compete in fast-growing markets; and
                                                                        and 49% over the first 10 days of intubation.The technology's
 benefit from intellectual property protection
                                                                        proprietary coating inhibits bacteria from colonizing the tube
  or other distinct competitive advantages.
                                                                        and infecting the patient's lungs.
We strive to ensure that each new addition to the Bard
product family contributes to our goals. In 2007, we generated
$250 million in revenue from business development activities
completed over the past five years.                                associated
                                                                    

                                                                                       A Review of Our Businesses
                                                                                            Vascular Business
                                                                                            As a leader in electrophysiology catheter technology for the
                                                                                            last 50 years, Bard's R&D efforts hold promise for treating
                                                                                            atrial fibrillation (A-fib), a complex heart condition afflicting
                                                                                            millions of people around the world. A-fib treatment represents
                                                                                            a worldwide business opportunity of $900 million and is
                                                                                            growing at a rate of 13% annually. In 2007, our controlled
                                                                                            European rollout of the BARD HD mesh ablation catheter
                                                                                            showed very positive clinical performance. In 2008, we will
                                                                                            continue to analyze this data in anticipation of entry into the
                                                                                            larger U.S. market. Bard's mesh ablation system, combined with
                                                                                            our diagnostic catheters and electrophysiology lab systems,
                                                                                            position Bard for future leadership in the diagnosis and
                                                                                            treatment of electrophysiology disorders.
                                                                                            Bard's endovascular business, which participates in a $1.8 billion
                                                                                            market growing 8% annually, includes our high pressure and
                                                                                            large diameter percutaneous transluminal angioplasty (PTA)
                                                                                            catheters. The recent launch of the DORADO catheter family
                                                                                            has the potential to improve our leadership position in the
                                                                                            standard PTA catheter segment. In 2008, we plan to expand our
                                                                                            market-leading high pressure line with a new line of specialty
                                                                                            PTA catheters designed for the nephrology and dialysis center
                                                                                            market. Our G2 vena cava filter line was a strong growth driver
                     in 2007, and recently received FDA clearance as a removable

                                                                                            filter in the United States. Later this year, we anticipate the

                                                                                            clearance and launch of the G2 EXPRESSTM filter, which will
         give clinicians the option of retrieving the filter with either

                                                                                            our RECOVERY CONE retrieval system or a snare catheter.

                        Urology Business
                                                                                            In keeping with Bard's emphasis on growth markets, we
In this annual report, we have highlighted three innovative                                 have successfully expanded our urology business beyond
technologies developed internally or acquired through                                       urine drainage products to include faster growing products
business development efforts. They include:                                                 in the areas of infection control, continence management
                                                                                            and catheter stabilization.
POWERPORT Implantable Port: This implantable port
designed by Bard Access Systems sets a new standard for                                     The treatment of hospital-acquired infections (HAIs) costs U.S.
power injection devices for cancer patients, eliminating the                                hospitals more than $500 million annually and significantly
need for repeated needlesticks associated with conventional                                 increases a patient's length of stay. With the recently announced
intravenous therapy (see page 6).                                                           elimination of Medicare reimbursement to hospitals for the
ULTRACLIP Tissue Marker: Just three millimeters in length,                                 treatment of HAIs, our infection control products are well-
these tiny tissue markers sold by Bard Peripheral Vascular                                  positioned as a means to control costs and improve patient
are improving the diagnosis and treatment of breast cancer                                  outcomes. Hospital-acquired urinary tract infections (UTIs)
by helping doctors mark and later recall sites through                                      are particularly troublesome because they endanger patient
ultrasound-guided biopsies (see page 8).                                                    health, strain hospital staff and budgets and help foster the
                                                                                            growth of antibiotic-resistant bacteria. By effectively combating
AVAULTA PLUSTM BioSynthetic Support System: Bard's
                                                                                            UTIs, Bard's infection control Foley catheter has achieved
Urological Division configured this new product for the
                                                                                            annual double-digit revenue growth since its launch more
anatomical needs of the pelvic floor area of the body,
                                                                                            than 12 years ago. In late 2007, we initiated a clinical study
providing surgeons with a less invasive option than
                                                                                            of our next generation infection control Foley catheter, based
traditional open surgery for pelvic floor procedures
                                                                                            on technology similar to our new proprietary AGENTOTM I.C.
(see page 10).
                                                                                            endotracheal tube, and anticipate launching this product in 2009.


Since acquiring the STATLOCK catheter stabilization device          Board of Directors and Organizational Changes
in 2006, growth rates in this product line continue to exceed
                                                                     Bard is fortunate to have the guidance of a highly distinguished
our expectations. In 2008, we plan to launch additional
                                                                     Board of Directors that brings a broad range of experience and
configurations for the STATLOCK stabilization device and
                                                                     knowledge. The Board's support and counsel are invaluable.
to add sales resources to capitalize on the global market
                                                                     We are also fortunate to have an exceptional management
opportunity. Our advances in surgical continence and pelvic
                                                                     team whose efforts help advance our business plan. In 2007,
floor reconstruction products have helped increase Bard's share
                                                                     we bid farewell to two long-term executives who have made
in this growing $400 million global market. In addition to our
                                                                     many contributions to our organization over the years:
ALIGN urethral support system, we augmented our AVAULTA
                                                                     James R. Kelleher, President, Asia, Americas, Australia &
biosynthetic support system line in 2007 with the launch of
                                                                     Canada, and Charles P. Grom, Vice President and Controller.
AVAULTA PLUSTM and AVAULTA SOLOTM support systems for anterior
                                                                     We thank them for their dedication and service and wish
and posterior pelvic floor repair.These products are designed for
                                                                     them well in retirement.
strength, flexibility and ease of placement  helping thousands
                                                                     Outlook for 2008
of women resume and maintain a better quality of life.
                                                                     While creating shareholder value clearly starts with top-line
Oncology Business
                                                                     revenue growth, achieving this growth comes from our ability
Bard pioneered the peripherally inserted central catheter (PICC)
                                                                     to successfully execute our business strategy. As we often say,
and today it is our single largest product line. In late 2007,
                                                                     "Great strategy is more about great execution than great
we launched our POWERPICC SOLOTM catheter  a significant
                                                                     thinking." Looking ahead to our next century, we remain
advancement in specialty venous access technology.
                                                                     convinced that our fundamental approach is sound. We will
Conventional PICC catheters must be flushed daily with a
                                                                     continue to increase our strategic investments in acquisitions
saline-heparin solution to prevent clotting and thrombosis.
                                                                     and R&D, focusing on innovative products in high-growth
Our innovative proximal valve design reduces the need for
                                                                     areas. We will strive for continuous improvement on all fronts
flushing to once per week, with saline only  reducing risk,
                                                                     to help ensure that our execution is as sound as our strategy.
cost and inconvenience in clinical and home-based settings.
As we continue to advance specialty venous technology,               You, our shareholders, are vital to our success as well.
we also anticipate the launch of several new implantable             We thank you for your loyalty and confidence, and for
POWERPORT devices with new catheter configurations in 2008.          supporting our vision.
In early 2008, we upgraded our proprietary SHERLOCK catheter        Sincerely,
tip location system to facilitate its use with Bard's SITE-RITE
bedside ultrasound guidance system for specialty venous
access catheter placement. These two systems, used together,
allow for quicker, easier and more precise placement of catheters.
Surgical Specialties Business
In 2007, our Davol subsidiary broadened its product offering in
the soft tissue hernia repair market  estimated at $585 million     Timothy M. Ring                 John H. Weiland
 with the addition of the SEPRAMESH IP absorbable barrier
                                                                     Chairman and                    President and
hernia repair product line. We've begun the process of
                                                                     Chief Executive Officer         Chief Operating Officer
combining the unique SEPRA anti-adhesion technology with
our diverse line of procedure-specific meshes and composites.
                                                                     February 25, 2008
Looking forward, we expect to offer multiple new products
in our soft tissue repair line using technologies like the
SEPRA or tyrosine coatings, as well as new configurations of
our COLLAMEND porcine dermal collagen product. The hernia
fixation market  $125 million and growing 10% annually 
is another key area where resorbable products are making
inroads. Initial demand for our PERMASORBTM resorbable
fixation device, acquired in mid-2007, has been strong.




                                                                                                                                    5
