Dear Colgate
Shareholder

Q. Please comment on the Companys 2012 global
business results.
A. We are delighted to have delivered another year
of strong performance in 2012. Net sales grew 2.0%
to an all-time record level, and global unit volume
from continuing businesses grew 3.5%, led by strong
growth in emerging markets. We achieved our profit
goals, with diluted earnings per share increasing 7%,
despite an intense competitive environment, volatile
foreign currency exchange and challenging macroeconomic
conditions worldwide.
All of the Companys fundamentals are strong and
getting stronger. Gross profit margin, operating profit
margin and net income as a percentage of sales all
increased versus year ago.
Advertising spending behind the Companys
brands rose 3%, leading to market share gains
across many of our core categories. Notably, Colgates
global market shares in toothpaste, manual
toothbrushes, mouthwash, bar soaps, body wash,
deodorants and fabric conditioners all increased during
the year.
We also maintained our strong balance sheet and
cash flow, which, along with the Companys positive
growth momentum, led the Board of Directors
to authorize a 7% increase in the quarterly dividend,
effective in the second quarter of 2012.
Q. How will the 2012 Restructuring Program announced
in October 2012 contribute to growth and
profitability? When do the projected savings begin?
A. Colgates 2012 Restructuring Program is a

four-year Global Growth and Efficiency Program
designed to produce significant benefits in the
Companys long-term business performance. The
programs initiatives focus on three areas:
u Expanding our use of commercial hubs, that is,
clustering single-country subsidiaries into more
efficient regional units;
u Extending our business-supporting shared
service centers to all regions and expanding their
scope beyond accounting and finance to streamline
additional global functions; and
u Further optimizing our global supply chain and
facilities.
Aftertax savings of approximately $30 million to
$40 million are expected to begin in the latter half of
2013. By the fourth year of the program, we expect
annual aftertax savings to reach approximately $275
million to $325 million. Reinvestment will focus on
new product innovation and brand building, enabling
technology and analytics, digital engagement and
driving growth in emerging markets.
The cumulative charges for the program are projected
to be between $775 million and $875 million
aftertax, with approximately $185 million to $220
million aftertax in 2013.
We are living in a fast-changing world with many
challenges, including slowing economies in many
countries. This program will help us to move forward
from our current position of strength to continue to
deliver sustained, profitable growth over the long
term.
Q. With challenging macroeconomic conditions in
many parts of the world, how can Colgate continue
to deliver such strong results?
A. Accelerating growth and staying ahead of the
competition will require being smarter and faster in
everything we do. We have chosen to undertake
this Global Growth and Efficiency Program now
when our business is strong in order to capitalize on
our growth momentum. One of the key objectives is
to become even stronger on the ground through the
expansion of proven global and regional commercial
capabilities, such as the use of commercial hubs
and shared service centers.
Commercial hubbing drives smarter and faster
decision-making by strengthening the resources
available to the smaller operations in the region.
This allows the local operations to focus more on
building market share, professional recommendations
and retailer engagement to drive growth.
Hubbing also improves cost structure by reducing
redundancies.
Simplifying and standardizing how work gets
done will also lead to being smarter and faster on
the ground. In Europe, our financial shared service
center in Warsaw, Poland, has significantly reduced
structural costs for the region by first lifting and
shifting transactional work to a central location,
then standardizing those processes. We have also
seen an increase in the speed and value of services
provided to subsidiaries.
Together with the expansion of these already
successful programs, Colgate people remain
sharply focused on the Companys four strategic
initiatives: Engaging to Build Our Brands, Innovation
for Growth, Effectiveness and Efficiency, and Leading
to Win. As outlined in this report, these initiatives
continue to power Colgates global growth.
Q. Todays commodity and cost environment is
increasingly volatile. What is Colgate doing to
ensure its long history of gross margin expansion
continues?
A. Mindful of the historical cost run-ups weve seen
in the past couple of years, we continue to be very
focused on generating savings throughout all areas
of the business to fund Colgates growth. Our traditional Funding The Growth cost-savings program, a
collection of small initiatives globally, has generated
between $400 million and $700 million of pretax
savings annually over the last several years and that
pace is expected to continue. Another area we are
increasingly focused on is making pricing more of a
strategic competence. Its all about balancing profitable
growth with the health of our brands.
We are building pricing excellence into our culture
in several ways. We have developed global guidelines
with performance measures against those
guidelines that are reviewed regularly throughout
the year. We have also developed a standardized
training program, which includes interactive workshops
to share ideas and best practices, and have
launched a new pricing web site accessible throughout
the Colgate world. These efforts, we believe,
have helped us manage the right balance globally
between unit volume growth and higher pricing,
which drives gross margin expansion.
Q. Please discuss how Colgate is using enhanced
technology and advanced business analytics to drive
growth.
A. Advanced information technology has clearly
increased the amount and quality of data available
for companies like Colgate and makes analytics that
are aligned with key business priorities an essential
competency in order to stay ahead of the competition.
We rely on a whole range of tools from the
fundamentals of basic analytics, such as reporting
of market data, pricing and new product tracking,
to more advanced business analytics, everything
from marketing mix modeling and shelf assortment
optimization to shopper loyalty data.
In Mexico and Brazil, for example, after an
analysis of our marketing mix, we reworked the way
we spend our traditional media money. Specifically,
media spending was shifted to productive print and
digital vehicles, and we changed the scheduling and
lengths of our television commercials. As a result,
we doubled our rate of return on investment.
As we make these analytics a global strategic
and tactical core competency, we are becoming
more and more successful in driving profitable topline
growth.
Q. How is Colgate meeting consumers desire for
more meaningful innovation?
A. Todays consumer is very discerning and wants
value, and value doesnt necessarily mean low price.
It means offering innovative products that are incremental,
breakthrough and transformational.
Weve discussed before the importance of our
nine consumer innovation centers, located close to
consumers in different parts of the world. At these
centers, marketing and consumer insight professionals
identify opportunities based on insights into
consumer behavior and work closely with scientists
to ensure we have the right technology in place.
Another important element of our innovation strategy
is our use of external partnerships to complement
our strong internal capability.
Our success in delivering meaningful innovation
is evident in several recent introductions. After a
very successful launch in the United States, we are
taking our breakthrough whitening brand, Colgate
Optic White, around the world, offering a full whitening
regimen including toothpaste, toothbrush and
mouthwash.
With the launch of Colgate ProClinical in European
markets this past October, we entered the
$1 billion-plus rechargeable toothbrush category, a
segment that we have not competed in before. The
product combines intelligent technology and design
for superior oral health and works in a unique way.
The speed and direction of the bristle movement
adjusts automatically depending on the orientation of

the brush, giving users a strong yet gentle clean.
Q. What are the Companys expectations for the
Hills business going forward?
A. We are projecting a return to volume growth for
the Hills business in 2013, driven by a very full
pipeline of innovation, including the relaunch of our
Science Diet wellness line and of Ideal Balance, our
naturals offering, and the launch of Prescription Diet
Metabolic Advanced Weight Solution.
We have made a major upgrade in our Science
Diet business in the United States with a new
formulation, which contains natural ingredients with
high quality animal protein as the first ingredient and
which has significant taste improvement. We also
have improved packaging and clear selling communications
for our consultants at retail. Together with
our new integrated marketing campaign, What Vets
Feed Their Pets, the refreshed offering has already
gained strong customer support.
Ideal Balance represents Hills entry into the naturals
pet food segment. It takes advantage of Hills
expertise in pet nutrition to provide natural ingredients
in perfect nutritional balance. The relaunch,
which is continuing to build momentum, will reposition
Ideal Balance as an authentic natural proposition
while maintaining the strength of Hills scientific
credentials. We are expanding into all key segments:
dry, wet and treats and removing wheat and soy
from all products.
These activities and other innovations, supported
by increased marketing investment, together with
growing veterinary endorsement levels worldwide,
are expected to build growth momentum in the Hills
business as we progress through the year and beyond.
Q. Please provide an update on the Companys
progress toward its sustainability goals.
A. Colgate has committed to a 2011 to 2015 sustainability
strategy with focused, measurable goals
around three areas: People, Performance and
Planet. We are making great progress on those
goals by integrating sustainability into our everyday
operations.
In 2012, for example, Colgate began evaluating
new products using a Product Sustainability
Scorecard to drive improvement across the product
life cycle, and it was estimated that approximately
30% of Colgates packaging materials globally are
from recycled sources. Additionally, we continue to
expand the reach of Colgates Bright Smiles, Bright
Futures oral health education program and of our
global hand washing campaign to more children and
their families around the world.
More on our sustainability strategy and progress
in this area appears on page 21 of this report.
Q. What is the Companys outlook for 2013?
A. Looking forward, we expect our growth momentum
to continue in 2013. Our new product pipeline is
very full around the world and we are pleased that
our worldwide restructuring program is on track and
proceeding smoothly.
We have proven global strategies that have consistently
delivered strong results despite competitive
and macroeconomic challenges, and a world-class
team around the world aligned behind clear goals.
As we move ahead together, I wish to thank all
Colgate people worldwide for their personal commitment
to achieving our goals with the highest ethical
standards, and express appreciation for the support
of our customers, suppliers, shareholders and directors.
Ian Cook
Chairman, President and Chief Executive Officer

