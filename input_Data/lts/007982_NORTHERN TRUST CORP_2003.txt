letter to Shareholders

significant challenges as 2003
began, with a continuing down-
turn in global equity markets and
the U.S. economy still struggling.
But even this sort of climate presents opportunities
for an organization whose clearly defined, consis-
tently executed strategy centers on clients. We used
2003 to sharpen both our strategic focus and our
client service model, to make targeted investments
in capabilities, markets and facilities and to assure
our clients of our unwavering commitment to
meeting their needs. By the fourth quarter, we
were able to report a 35 percent increase over the
prior year in both net income and earnings per
share, as the economy gained strength and markets
improved dramatically. We are pleased with the
momentum we have established as we enter 2004.
For the full year 2003, Northern Trust
reported net income per share of $1.80, compared
with $1.97 reported in 2002. Net income was
$404.8 million, compared with $447.1 million
earned last year. This resulted in a return on average
common equity of 13.8 percent.
Trust assets under administration rose 43
percent to a record $2.2 trillion, and assets under
management increased 58 percent to a record
$478.6 billion at year end. Managed assets at year-
end 2003 included $75 billion in index assets
acquired earlier in the year from Deutsche Bank
AG. After three consecutive years of decline, equity
markets rebounded in the second half of the year
with the S&P increasing 26.4 percent for the year.
Total revenue from continuing operations
was $2.14 billion, up 1 percent from the previous
year. Credit quality as of year end was strong, with
net chargeoffs for the year totaling $13.8 million.
In June 2003, we announced a number 
of steps to reduce expenses, improve efficiencies
and position the company for profitable growth
resulting in a reduction to Northern Trusts expense
base by $75 million annually over the following
12 months. As of January 2004, we had completed
almost 90 percent of the total annual target of $75
million and will complete the remaining amount
by June 2004. Throughout the Corporation, we
aggressively manage expenses while maintaining
an exceptional level of client service. Careful
expense management allows us to focus resources
in growth areas where Northern Trust has a lead-
ership position and a clear competitive advantage.
The Corporation declared a quarterly cash
dividend of 19 cents per share, marking the 107th
year of consecutive dividends paid.
The price of Northern Trust Corporation
stock increased 32 percent in 2003 from $35.05
at year-end 2002 to $46.28 at the end of 2003.
Northern Trust stocks compound annual growth
rate for the 10-year period ending December 31,
2003, was 16.7 percent, compared with 13.6 percent
for the KBW50 Bank Index and 9.1 percent for
the S&P 500. The Management Discussion and
Analysis beginning on page 29 includes more
detailed financial results.
a   va lu e d   b ra n d
Amid the many challenges and changes that
occurred last year, Northern Trusts focused business
strategy remained constant. Northern Trust is a
leading provider of global financial solutions for
the investment management, asset and fund
administration, fiduciary and banking needs of
corporations, institutions and affluent individuals.
Our primary focus is on the administration, custody and management of clients assets in two specific
marketsaffluent individuals and institutions.
These services are delivered to private clients
through our Personal Financial Services (PFS)
business unit and to institutional and corporate
clients through Corporate and
Institutional Services (C&IS).
These businesses are supported
by our world-class investment
organization, Northern Trust
Global Investments (NTGI),
and a leading-edge technology
unit, Worldwide Operations
and Technology (WWOT).
Since opening for busi-
ness in 1889, Northern Trust 
has delivered high-touch trust,
asset management and private
banking services to affluent
individuals and families and
has done so under the same
brand namea significant
accomplishment in this era of mergers and acqui-
sitions. PFS serves private banking and personal
trust clients through our national network of 82
distinctive offices in 15 states. In 2003, Northern
Trust opened offices in New York City and Stamford,
Connecticut, and in Atlanta, Georgia, with the
acquisition of Legacy South, Inc., a private wealth
management firm. Plans call for continuation of
expansion on the East Coast with the opening 
of an office in Boston, Massachusetts, in 2004, as
we extend Northern Trusts reach as the Wealth
Specialist that can most effectively meet the com-
plex needs of high-net-worth clients. When the
Boston office is opened, Northern Trust will have a
presence in 16 states and will have reached the goal
of being within close proximity to 40 percent to 45
percent of the nations millionaire population. No
other provider has this kind of reach in attractive,
affluent growth markets.
Northern Trust, which already enjoys a strong
market reputation, intends to continue to grow our
PFS business by offering clients in both existing
and new markets a broad spectrum of sophisticated
services delivered by respected, experienced pro-
fessionals. Through three PFS
segmentsPrivate Banking
and Personal Trust, Wealth
Advisory Services, and Wealth
ManagementNorthern
Trust has the ability to reach 
a full range of clients.
C&IS has achieved a
top-tier position in the industry
by serving selected markets
worldwide through a strong
team of professionals who create
value for clients by offering
innovative financial, credit and
investment solutions for their
complex needs. In 2003, C&IS
added record new business,
grew market share and expanded into additional
markets. C&IS serves public and private retirement
plans, foundations, endowments, insurance
companies, global fund managers and other asset
pools, such as governmental entities. C&IS,
which contributes approximately one-half of the
Corporations revenues, provides asset servicing, fund
administration, investment management and advi-
sory services to institutional investors worldwide.
Northern Trust is one of the top 10 custodi-
ans worldwide in terms of assets under custody.
Of a total of nearly $2 trillion in assets under
administration in C&IS at year end, global custody
assets accounted for $751 billion. Over the last
decade, global custody assets grew at a compound
annual rate of 31 percent.
C&IS has a strong foundation of global
growth with clients in 38 countries and services
offered in 100 markets. We have experienced especially strong growth in Europe and the Asia-
Pacific region. Our international business is now
one of Northern Trusts fastest-growing areas, and
we have a demonstrated track record for success 
in this arena.
NTGI is a world-class diversified asset 
manager with $478.6 billion under management at
year-end 2003, an increase of 58 percent over the
previous year, including $75 billion in index assets
acquired in 2003. This allows NTGI to offer clients
a full range of leading global index products and
brings Northern Trust significantly greater scale
and additional capabilities.
To grow its business, NTGI continues to
focus on five keys to success: investments, branding,
product management, distribution and client service.
In April 2003, Northern Trust expanded 
its international presence by opening an office in
Tokyo. This is an important part of Northern Trusts
plan to further develop as a global investment firm.
Since the opening, NTGI has been awarded several
significant mandates for passive management from some of the most prominent institutions in Japan,
including the Government Pension Investment
Fund. Seeking to expand outside the U.S., NTGI
has established joint ventures and other relationships
with major European financial organizations, such
as Helaba Invest KAG in Germany, Mediolanum in
Italy, Groupama in France and Scottish Equitable
in Scotland.
Through an integrated, single-technology
platform, WWOT effectively supports PFS, C&IS
and NTGI with creative solutions for the complex
needs of both institutional and individual clients
worldwide. Northern Trust is committed to
remaining a technology leader. In 2003, industry
publication ComputerWorld recognized Northern
Trust as one of the Best Places to Work in
Information Technology. Going forward, WWOT
will continue to work to increase productivity,
decrease cost and risk while expanding Northern
Trusts business continuity efforts. Our business
continuity planning allows the company to anticipate
events that could affect critical functions and to
develop plans to respond quickly and effectively 
to events such as earthquakes and power outages.
To support business continuity efforts, in 2003
Northern Trust opened a data center in the western
suburbs of Chicago. This permanent workspace
allows some of Northern Trusts mission-critical
functions, such as wire transfer and securities pro-
cessing, to be done dually at our Canal operations
center and at the west suburban site.
In 2003, Northern Trusts Passport products
continued to be well received as WWOT partnered
with the business units to enhance and refine this
suite of online financial services for institutional
and personal clients and internal partners.

Although the financial services industry is highly
dependent on technology, financial institutions are
ultimately judged by the traditional fundamentals
of good reputation and trust. This has never been
more true than during the past year that saw an
unprecedented number of corporate scandals. In this
unsettled atmosphere, we were pleased to have been
recognized by a number of independent sources as
both a safe and ethical companyqualities that we
have emphasized throughout our history.
In October 2003, Global Finance magazine
named Northern Trust one of the Worlds Safest
Banks. Crains Chicago Business selected Northern
Trust as one of Chicagos icons, citing a legacy of
loyalty to the organizations historically conserva-
tive principles and the culture of caring, charitable
giving and civic participation An article in
U.S. Banker entitled Character is Foundation 
of Firms Reputation states that Northern Trust
Corporation received the highest reputation rating
in a survey of 600 executives and financial analysts.
And for the third consecutive year, Northern Trust
was named one of the 100 Best Corporate Citizens
by Business Ethics magazine.

When economic conditions are unfavorable, as
they were last year, it is especially important that
organizations continue to actively support the
communities they serve. In 2003, Northern Trusts
charitable giving was more than $9.5 million in 
cash contributions; we donate approximately 
1.5 percent of pre-tax profits to charities each year.
One important aspect of our giving relates to the
United Way/Crusade of Mercy. Approximately 
76 percent of the Chicago-area staff contributed 
a total of $1.5 million, and the Corporations 
contribution was $750,000bringing the total to 
$2.3 million, which was donated to agencies serving
more than 2 million people in the Chicago area.
Northern Trusts community support is
not only financial. Equally important is the time
and energy Northern Trust partners across the
organization devote to a wide range of volunteer activities. Volunteerism is an integral part of
Northern Trusts culture, and last year, our partners
worldwide volunteered more than 180,000 hours
to community service.
Through a number of lending and commu-
nity reinvestment programs, Northern Trust
strengthens communities where we do business.
The Northern Trust Company has maintained
an outstanding Community Reinvestment Act
rating from the Federal Reserve Bank of Chicago
for the last four years.

Stephen B. Timbers, Vice Chairman and President
of Northern Trust Global Investments (NTGI) and
a member of the Management Committee, retired
in February 2004. Steve joined Northern Trust in
1998 and, since that time, has introduced successful
strategies that have led to NTGI becoming a premier
asset management company. Terence J. Toth,
Executive Vice President and Global Head of
Northern Trust Quantitative Management,
Securities Lending, Transition Management and
Commission Recapture, was named President of
NTGI and became a member of the Management
Committee. Terry joined Northern Trust in 1982.
In November 2003, Mark Stevens, Vice
Chairman, retired after 24 years with the company.
At the time of his retirement, he was responsible for
coordinating strategic corporate expansion activities.
Also in January 2004, Steven L. Fradkin,
Executive Vice President and Head of Northern
Trusts Finance Group, was named Chief Financial
Officer (CFO) of the Corporation and became a
member of the Management Committee. The CFO
position previously was held by Vice Chairman
Perry R. Pero, who will continue as Vice Chairman
and as a member of the Management Committee.
Peter L. Rossiter, Executive Vice President,
previously Northern Trusts General Counsel 
and most recently head of the Corporate Risk
Management area, left Northern Trust in
February 2004 to return to the practice of law.
Perry has added Peter Rossiters responsibilities 
to his current responsibilities as Chairman of the
Asset and Liability Policy Committee and head of
credit risk management.
We are fortunate to have worked with such
talented professionals and thank them for their
contributions to Northern Trusts success. We are
equally fortunate to have an outstanding team of
seasoned professionals, who in their new positions
will contribute importantly to Northern Trusts
continued success.

I would be remiss if I did not acknowledge the
exceptional efforts of Northern Trust people across
the Corporation who worked in partnership to
meet the many challenges of a difficult year. They
were called upon to embrace some major changes
while at the same time maintaining the highest
standard of service to our clients. They met the
challenges, and I thank each of them for their
extraordinary performance.
To each of our Directors, I want to express
gratitude for the time, effort and wise counsel they bring
to the important mission of corporate governance.
On behalf of the entire Corporation, I thank
our loyal clients and shareholders for their continued
support and belief in our organization.
As equity markets stabilize and the economy
strengthens, we at Northern Trust look forward
with the confidence that we are well positioned for
continued success in the year ahead.

Chairman and Chief Executive Officer
February 17, 2004