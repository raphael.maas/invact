Dear Fellow Shareholder,


Most people measure the completion of a merger by the
execution of a contract or the actual closing. In the case of the
combination of Raymond James and Morgan Keegan, many in
the home office would probably measure it by Morgan Keegan�s
move to our processing platform on February 18. This is a
reasonable conclusion as so much work by personnel on both
sides was necessary to accomplish that task. Many associates in
all parts of the firm, especially Information Technology,
Operations, Capital Markets and the Private Client Group,
worked long hours, traveled extensively and suffered through
dealing with the numerous challenges that arose during the
process before the exhilaration associated with an almost
seamless integration. The event itself was remarkable in light of
the almost interminable issues encountered by other firms in
past integrations.
But that�s really not the end. The training, development of
enhancements for new members of our family, who joined
from Morgan Keegan, and the continuing efforts by everyone
to acculturate to the �New Raymond James,� as well as the
extraordinary expenses of the integration, persisted until after
year-end, when our CEO, Paul Reilly, declared the integration
essentially over. We must also remember that mergers aren�t all
about working hard and then celebrating. We had a number of
very good associates that were severed from the recently
combined entity because some of our businesses overlapped.
That certainly was management�s most difficult task to effect a
successful combination. However, we all know that it�s really
never totally over. It�s management�s duty to earn the respect
and allegiance of all our associates and to continue to improve
our systems every day.
Bolstered by the inclusion of Morgan Keegan for a full year,
as contrasted to six months last year, net revenues rose to a new
record of $4.5 billion in 2013, up 18% from last year. Non-interest
expenses, which were inflated by a host of non-recurring
acquisition related expenses, were up 17%. Consequently, net
income grew 24% to a record $367 million. Net income per diluted
share increased 17% from $2.20 to $2.58. On a non-GAAP basis (1),
2013 net income, adjusted for acquisition and other nonrecurring expenses of $80 million, was $419 million, representing
an increase of 25% from last year. Accordingly, non-GAAP
diluted(1) earnings per share increased from $2.51 to $2.95. On
a GAAP basis, the pretax operating margin on net revenues was 

12.6% and a healthy 14.4% on a non-GAAP basis(1). The aftertax rate of return on average equity was 10.6% (12.0% nonGAAP(1)). Shareholders� equity increased to $3.66 billion, or
$26.40 per share, on September 30, 2013. The tangible book
value per share (a non-GAAP measure (1)) was $23.86.
Although annual comparisons of segment results are also
impacted by the addition of six more months of Morgan Keegan
revenues and pretax income, record results in all four core
segments all contributed to the material increase in revenues
and pretax income. The Private Client Group produced $2.93
billion in revenues, representing an 18% increase over last year
and a $230 million contribution to pretax income, a 7% increase,
which, like last year, was depressed by the high costs of the
integration. At year-end, our financial advisor count was down
by 13 from last year, or 0.2%, as we experienced minor attrition
related to the merger. Private Client Group assets under
administration increased by 9.5% to $403 billion ($425 billion
of total assets under administration) during 2013. As the
demands of the merger have abated, the recruiting run rate
has flourished.
The Capital Markets segment generated a revenue increase of
15% to $945 million. The pretax contribution was $102 million,
up 35%. In light of the low interest rate�s impact on institutional
commissions and trading profits in the Fixed Income sector,
a 30% decline to a still healthy contribution of half of Capital
Markets pretax profits wasn�t surprising. However, Equity
Capital Markets more than compensated for the shortfall by
increasing its pretax profit contribution by 365%. Net revenues
increased 21% to $480 million as mergers and acquisitions fees
rode the waves of increased activity fueled by high levels of
corporate liquidity.
Asset Management Group revenues grew 23% to $293 million
as financial assets under management increased 31% to $56
billion. The segment�s contribution to pretax income grew
43% to $96.3 million. Obviously, results in this sector correlate
well with market appreciation and net new sales, which have
continued to augment the favorable market gains.
More than offsetting the effects of lower gross interest rates,
Raymond James Bank�s net revenues grew 3% to $347 million.
Despite a trend to lower net interest margins during the year,
Raymond James Bank�s pretax income grew 11% to $268
million, as loans grew $830 million, or 10%, during the fiscal
year. In addition, loan quality improved and generated net
credits to the loan loss provision, which benefited net income.
Unfortunately, net spreads may continue to decline as the demand
for good quality loans is robust, causing further declines in
net spreads. Our objective is to offset that decline with net
loan growth.

Notwithstanding the demands of the merger, business still
had to be conducted daily. As a result, there were significant
accomplishments, awards and events, some of which are
recorded below:
� In December, Raymond James introduced the Investor
Access mobile site application, which provides clients
with complimentary, secure access to their Raymond James
brokerage account information while they are �on the go.�
� Raymond James received a ranking of second in
REP. magazine�s annual Broker Report Card competition.
Financial advisors rated us 9.1 on a 10 point scale.
� In December, 11 Raymond James advisors were recognized
by Bank Investment Consultant as members of its list of top
advisors in 2012.
� In the March quarter Raymond James recognized
$65 million in gains from the sale of Albion Medical
Holdings, Inc. (unadjusted for the elimination of
non-controlling interests and taxes) in Raymond James
Capital�s merchant banking fund, which inflated revenues
and profits due to consolidation. That extraordinary
event successfully completed the sale of investments in the
Raymond James Capital Partners� fund for its investors and
increased Raymond James� pretax profits by $22.7 million
for the year after eliminating the interests of our other
investor partners.
 � In February, 21 Raymond James financial advisors were
recognized by Barron�s for being among the nation�s 1,000
top financial advisors, up from 19 last year.
 � For the third consecutive time, we were named to the
Fortune World�s Most Admired Companies list, ranking
fifth in the securities/asset managers category. We were
the first securities firm on the list.
 � In March, Raymond James received the Bank Insurance
and Securities Association (BISA) Technology Award
for the firm�s Goal Planning & Monitoring software.
 � In March, Vin Campagnoli was promoted to
chief information officer.
 � For the second consecutive year, Raymond James was
named the top real estate investment bank by Global
Finance on its 2013 World�s Best Investment Banks list.
 � In April, our Equity Capital Markets segment announced
the formation of the Institutional Strategic Options Desk,
which is housed in our institutional equities office in
New York City.
 � Raymond James was recognized by Bloomberg News
in April as the best brokerage firm as measured by the
risk-adjusted return to shareholders since 2009 among
nine U.S. brokerage firms, banks and advisory firms.
 � In April, the Financial Times featured 24 Raymond James
financial advisors on its inaugural FT 400 list of top advisors
in the United States.
 � Four of our female financial advisors were included in
Barron�s 2013 list of the Top Women Financial Advisors.
 � Chet Helck, our Global Private Client Group CEO,
was listed among Investment Advisor magazine�s 25 most
influential persons in our industry in 2013 as a result of
his service to our industry as chairman of the Securities
Industry and Financial Markets Association.
 � In July, Peter Moores, the CEO of Raymond James
Investment Services, our private client broker/dealer in
the United Kingdom, became our UK manager, which
added UK Capital Markets oversight to his role to coordinate
all of our activities there.
 � Our new Denver Information Center was completed and
has begun processing data. It will become the principal
IT processing center in 2014.
 � In consonance with its long-term record of outstanding
equities research, the Raymond James Research team
received 17 awards in the 2013 Financial Times/Starmine
Analyst Awards, ranking the firm second among all
broker/dealers. Starmine measures results by the returns
on buy/sell recommendations and the accuracy of
earnings estimates.

Although Raymond James� financial results combined with the
accomplishments mentioned on the previous page constitute
an outstanding year, the outlook for 2014 and beyond is even
more exciting. We have a larger, energized team of talented
associates to drive future growth. Our financial condition has
never been better. In fact, our improved earnings power
motivated our board of directors to increase our dividend rate
by $0.08 per annum. Furthermore, all of our segments are
performing well with an array of internal growth opportunities.
While the market�s recovery of over 140% since the lows in
March 2009 does pose a higher degree of market risk, the
economy�s slow climb out of the depths of the market decline
appears to be picking up speed, which could engender an
extended rally. Frankly, we are enthusiastic about the prospects
of capitalizing on the hard work invested over the last year.
Best wishes for a happy, healthy and prosperous New Year!
Thomas A. James
Executive Chairman
Paul C. Reilly
CEO
December 13, 2013