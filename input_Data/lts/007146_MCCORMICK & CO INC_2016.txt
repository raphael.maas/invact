It is an honor to send you my first shareholder
letter as CEO. 2016 was an excellent year for
McCormick. We delivered breakthrough innovation,
cutting-edge digital marketing campaigns,
value-enhancing acquisitions and substantial
cost savings. Importantly, actions like non-GMO
labeling and a transition to more organic products
are keeping our brands on-trend and top of
mind for consumers. Simply put, McCormick is
winning flavor. I am also proud to let you know
that this year marked our 31st consecutive year
of increased quarterly dividends. Looking ahead,
our steadfast focus on growth, performance and
people will continue to drive McCormicks strategy
and momentum forward.
We Are Delivering Record Financial Results
McCormick achieved record financial results in 2016, meeting
each of our key financial targets. We delivered strong growth
in sales and profit, and saw a fifth consecutive year of record
cash flow. Highlights include:
? Net sales rose 3%, driven by increases in our base business,
new products, expanded distribution and acquisitions.
Excluding the impact of unfavorable currency rates, the
growth rate was nearly 6%. Our consumer segment led this
increase with sales up 4%, while industrial segment sales
were comparable to 2015. Excluding the impact of unfavorable
currency rates, we grew consumer segment sales 6%
and industrial segment sales 4%, with increases in each of
our three regions.
? Operating income of $641 million compared to $548 million
in 2015 due in part to higher sales and a favorable business
mix. In addition, we achieved $109 million of cost savings
and used a portion of these cost savings as fuel for a $12
million increase in brand marketing and a $7 million
increase in acquisition-related transaction expenses.
Special charges lowered operating income by $16 million in
2016 and $66 million in 2015. Excluding the impact of special
charges and unfavorable currency rates, we grew
adjusted operating income by 9%.
? Our earnings per share increased to $3.69, compared to
$3.11 in 2015. This result exceeded our initial 2016 goal due
in part to the strong operating income performance and a
favorable tax rate. Excluding a $0.09 impact from special
charges in 2016 and a $0.37 impact in 2015, we grew
adjusted earnings per share 9% to $3.78. This increase
includes the impact of unfavorable currency rates.
? We continue to generate strong cash flow from our operations
and reached a new high of $658 million in 2016.
Long-term, our goals in constant currency are to grow sales
4% to 6%, operating income 7% to 9% and earnings per share
9% to 11%. We performed well and achieved sales and
adjusted operating income growth toward the top end of
these goals in 2016. We are executing against strategies to
continue this strong performance in 2017 and beyond. For
those invested in McCormick stock, total shareholder return
exceeded the broader market in 2016 and in the past 5-, 10-
and 20-year periods.
McCormick Is Driving Growth in 2017 and Beyond
At McCormick, our focus is on flavor. In an environment where
consumers are seeking simple ingredients, fresh food and
new tastes, flavor is an advantaged category. In the U.S.,
while convenience, health and price are all important, when
consumers choose what to eat, taste ranks #1. Globally, retail
sales of packaged spices and seasoning are $11 billion and
projected to grow at a 5% compound annual growth rate
for the next five years. We are well-positioned for growth
with a leading share in this market of approximately 20%;
nearly four times the size of our next largest competitor.
? With our teams on-track to meet our growth targets, we
were able to return $461 million of cash flow to our shareholders
through dividends and share repurchases, finance
two acquisitions and expand our manufacturing capacity to
accommodate our growth. At the end of the year, our Board
of Directors authorized a 9% increase in the quarterly
dividend, making 2016 the 31st consecutive year of dividend
increases!
We are driving sales through increases in our base business,
product innovation and acquisitions. This has led to a 5-year
average sales growth rate that is at the upper end of our
long-term, constant currency goal of 4% to 6%. In 2016, we
made great progress with each of our three avenues for
growth; expanding our base business, innovating new products
and making value-enhancing acquisitions. Our Board
and management team continue to be closely aligned on our
go-forward plan, and we are diligently executing our strategy
to continue this progress in the coming years.
We are growing McCormicks base business and increasing
the appeal of our brands.
In particular:
? Consumers are seeking transparency in their food. This
past year we added non-GMO to the label for approximately
70% of our everyday spices and seasonings in the
U.S., making us the largest brand of spices and seasonings
bearing the non-GMO label in this market.
? We are the #1 organic brand of spices and seasonings in
the U.S., following the conversion of 75% of our premium
gourmet line to organic. This has been a welcomed move
with consumers and retail customersfour of the top ten
U.S. retailers expanded their range of our gourmet products.
? Also in the U.S., we introduced organic versions of our most
popular recipe mixes and new organic Kitchen Basics stocks.
? In 2017, we are planning to move toward clean label on
all of our Zatarains rice mixes, with no artificial flavors or
dyes and no MSG added.
Enhanced brand marketing is a key growth driver of our base
business. We measured our returns on this investment and
they are exceeding benchmarks for the food industry. As
a result:
? We have increased our brand marketing by 35% in the past
five years and plan to increase further in 2017.
? In 2016, we introduced a new marketing campaign in
North America that emphasizes the flavor and quality of
McCormicks spices and seasonings. In the U.S., we increased
millennial household penetration 2 percentage points
with this powerful message.
? Globally, we are shifting toward more digital marketing
which ended 2016 at 46% of our total advertising, double
the percentage from five years ago. We are targeting millennial
audiences in particular through a variety of digital
media, social platforms and new content approaches,
such as how to cooking videos.
Continued distribution expansion is another important aspect
of our base business growth strategy.
? For our industrial segment, we excel in customer intimacy
and as a trusted partner are supporting the geographic
growth of multi-national packaged food companies and
quick service restaurants.
? We are reaching an increasing network of restaurants with
our branded foodservice products, and in 2016, had particularly
strong sales growth in the U.S. and China.
? In 2016, we began
construction on a
new, larger facility
in Shanghai to
accommodate
our
growth in China for
both our consumer
and industrial segments.
In addition, we established a manufacturing facility
in Dubai and acquired a construction site for a new
regional manufacturing facility in Thailand to service
Southeast Asia.
McCormick is driving growth through innovation.
In addition to growing our base business, our strategy of
accelerating scalable and differentiated new products has
demonstrated its effectiveness.
In our industrial segment, we have a particularly high rate of
innovation. We are shifting our industrial segment portfolio
toward more value-added products and, together with our
greater scale and efficiency, we have increased adjusted
operating income as a percentage of sales to 10.0% from
7.5% five years ago.
? Recent product development wins include seasonings for
snack chips and crackers, beverage flavors, sandwich
sauces and burger seasonings. With our focus on flavorful,
healthy eating, we are an ideal partner for industrial customers
working to improve the health profile of their
products with actions such as reducing sodium or eliminating
artificial flavors and colors.
? An increasing number of our new product briefs in the
industrial segment have health and wellness attributes. In
2016, this percentage in the U.S. was approximately 50%,
up from 40% in 2014.
? We are developing advanced flavor delivery capabilities
that include technology like FlavorCell, a proprietary
encapsulation system.
? Our innovation and customer intimacy earned us global
flavor supplier status with four large packaged food companies
in the last two years.
For our consumer segment, a great example of scalable and
differentiated innovation is the expanding use of liquid pouch
packaging, which provides consumers with another way to
enjoy the flavor of fresh. We have introduced skillet sauces,
slow cooker sauces and most recently, baking sauces across
North America and squeeze pouch condiments in China.
In 2016, we expanded our popular Grill Mates brand to
single-use liquid marinade that exceeded our sales projections
by nearly 20%.
Our latest breakthrough innovation is herb grinders. Herb
grinders are a unique product line that provides a convenient
alternative to fresh herbs for use in cooking, serving and at
the table. These products were awarded the top consumer
product innovation of the year by the U.S. Grocery
Manufacturers Association and recognized in France by consumers
as a new product of the year.
? Our herb grinders are bringing in new consumers with
approximately 30% of sales incremental to the dried herb
category in the U.S.
? We see broad appeal for these products and by the end of
2016 won distribution in six other countries, including
Canada, U.K. and France.
Innovation for our consumer segment in 2016 also included:
? In the U.S. we developed Lawrys Casero seasoning blends
that deliver authentic Mexican and Latino flavors, as well
as Kitchen Basics bone broth that provides a simple way to
add flavor and protein.
? Across Europe, we expanded our variety of recipe mixes
and in France, added new ingredients and value-added
packaging to our Vahin brand dessert items.
? We are expanding our recipe mix varieties in China and
iconic Aeroplane brand dessert mixes in Australia.
Overall, our innovation strategy has delivered results. In
2016, 9% of our sales came from new products launched in
the past three years.
Successfully integrating valuable acquisitions remains at
the forefront of our strategy.
We expect acquisitions to account for about a third of our sales
increase. We continue to seek businesses that expand our
portfolio of flavors, build scale and make financial sense. Our
acquisition pipeline includes both bolt-on and larger assets.
? We were pleased to acquire Gourmet Garden this past
year. Based in Australia and with exports to markets
around the world, Gourmet Garden is a leader in chilled
packaged herbs. Nearly two-thirds of sales are in the U.S.
and this product is merchandised in the high-traffic produce
aisle. Gourmet Garden is a great addition to our portfolio
of flavors and we plan to grow this business globally
through increased household penetration, expanded distribution
in current and new markets and leveraging our
marketing expertise.
? Toward the end of the year, we announced an agreement
to acquire Giotti, a leading flavor supplier in Europe. Based
in Italy, Giotti expands our value-added flavor solutions
across Europe and provides additional expertise in flavoring
health and nutrition products.
Our Comprehensive Continuous Improvement program
is driving margin improvement at McCormick.
Along with our strategies to grow sales, we are working to
increase operating income margin by driving operating
income growth at a faster rate than sales. In 2016, we
increased adjusted operating income as a percentage of
sales by 60 basis points from the year-ago period. Our
Comprehensive Continuous Improvement
(CCI) program, is
driving margin improvement at McCormick. Since 2009, we
have been improving productivity throughout the company
and recently increased our resources to step-up our efforts.
These efforts are paying off.
Early in 2016, we set a goal to achieve $400 million of cost
savings through greater productivity over the next four
years. We are well on our way to achieving this goal with a
record $109 million of cost savings delivered in the first year.
In addition to driving margins, these cost savings are our fuel
for growth, funding increases in brand marketing and additional
sales initiatives.
We Have the Right Team to Win Business
Employees throughout the company are the backbone of our
success. On behalf of the executive team, I would like to thank
them for their dedicated efforts and accomplishments. The
foundation of our culture began with C.P. McCormick and I
am proud to say we still engage all employees through our
Multiple Management philosophy of encouraging participation
and inclusion.
Our people are the reason McCormick is a great place to work.
In order to compete effectively and attract the best talent we
are strengthening our winning ways of working with faster
decision making, more personal accountability and actionable
insights. In this regard, we have taken a number of steps
that include developing tools to help achieve strategic goals
that cascade to the individual employee and provide a direct
link to compensation. We have also increased global resources
behind innovation, analytics, digital marketing, our CCI program
and business development.
inclusion, and now have seven employee ambassador groups
underway. The largest of these is the Womens International
Networkit currently has three chapters spanning several
countries.
This past year we also saw a number of changes to our management
structure and are confident that we have the right
team in place to deliver sustained growth in 2017.
? Alan Wilson, who served as CEO for eight years through
February 1, 2016 and since then as Executive Chairman,
will retire January 31, 2017. On this date, he will remain a
member of McCormicks Board of Directors, but will end his
role as Chairman. We thank Alan for his outstanding leadership,
an enviable track record of financial performance and
delivering significant shareholder return. I was honored to
have been named McCormicks new Chairman of the Board.
? Gordon Stetz stepped down from his roles of Executive
Vice President & CFO and Board member and retired in
December 2016. (Please see our tribute to Gordon on page 9.)
? Mike Smith was named Executive Vice President & CFO
effective September 1, 2016. Mike brings extensive experience
in financial roles at the company that span both our
consumer and industrial segments.
McCormick Is Poised for Success
I would like to express how proud I am of what we accomplished
in 2016. Our business and products are on trend with
todays consumers and we are well-positioned for continued
momentum. Our leaders and employees are executing on
effective growth strategiesstrategies designed to win with
our customers and consumers. As part of this strategy we are
stepping up our performance and I am confident McCormick
is on track for a very successful 2017. As we enter the new
year, I look forward to continued progress as we focus on
growth, performance and people to build the value of your
investment in McCormick.
Thank you for your interest in McCormick.
Sincerely,
Lawrence E. Kurzius
President and Chief Executive Officer