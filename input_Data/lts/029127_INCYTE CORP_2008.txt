Dear Shareholder:
The past year was extremely difficult with respect to the capital markets. Incyte�s valuation
was no exception. Despite this, we advanced our key programs and prioritized in a clear
manner those which we believe we can afford to develop. As such, our focus in 2009 will
be to initiate pivotal trials in myelofibrosis with our lead product candidate, the JAK1/2
inhibitor, INCB18424, while continuing clinical development of this compound in the other
myeloproliferative disorders, polycythemia vera and essential thrombocythemia. We also
intend to continue development of the topical formulation of INCB18424 in psoriasis and
take a second JAK1/2 inhibitor, INCB28050, into a dose-ranging Phase II trial in rheumatoid
arthritis. A Phase IIb trial in type 2 diabetes with our 11beta-HSD1 inhibitor, INCB13739,
will complete this summer while the Phase II program in breast cancer patients for our
sheddase inhibitor, INCB7839, continues.
Although we ended 2008 with enough cash to advance our lead drug candidates, an
important objective in 2009 is to secure additional funds through the establishment of
partnerships for a number of programs. We are receiving serious interest in them from
other pharmaceutical companies and I am optimistic that partnerships represent a realistic
source of capital.
I am also confident our focus on programs that can create the greatest near-term value and
on partnering certain of them is prudent and should expedite our transition from a pure
discovery company to one that can successfully develop and commercialize important new
medicines.
Summary of 2008 Achievements
JAK inhibitors are a new class of drugs we believe will be useful in treating a variety of
cancers and chronic inflammatory conditions. Our most advanced JAK1/2 inhibitor,
INCB18424, is being developed as a treatment for myeloproliferative disorders (MPDs), a
closely related group of hematological malignancies that include myelofibrosis (MF),
polycythemia vera (PV) and essential thrombocythemia (ET). MPDs represent a highly
concentrated market with significant unmet medical needs which we believe we can
effectively reach on our own in the U.S. and possibly Europe. With pivotal Phase III trials in 
MF expected to begin this year, and the possibility for an expedited review and approval
process, this program represents our nearest-term commercial opportunity.
Myeloproliferative disorders affect
approximately 200,000 people in the
United States and a larger number in
Europe. These disorders tend to be
treated by hematologists and oncologists
who could be efficiently reached through
our own sales and marketing efforts. We
have already established strong
relationships with many key opinion
leaders in these specialties and have
conducted initial market research among
them confirming the clinical and
commercial potential of INCB18424 in this
disease area.
A topical formulation of INCB18424 achieved positive clinical results in our early Phase IIa
trials involving 42 mild to moderate psoriasis patients. A Phase IIb three-month doseranging trial involving over 200 patients should complete this summer and the results will be
used to determine the future development and partnering goals for topical INCB18424.
INCB28050, our lead oral JAK inhibitor compound for systemic inflammatory indications,
has completed single- and multiple-dose Phase I studies in healthy volunteers. In a 28-day
Phase I drug-interaction study in patients with rheumatoid arthritis, INCB28050 was safe
and well-tolerated and showed impressive efficacy. We expect to begin the Phase II
development program in May of this year. INCB28050 has therapeutic potential in multiple
indications including rheumatoid arthritis as well as other types of inflammatory arthritis,
inflammatory bowel disease such as Crohn�s disease, dry eye and another ocular
inflammatory disease, anterior uveitis. Because of these multiple opportunities, and
because of our objective to remain competitive by advancing these indications in parallel as
opposed to in series, we are seeking to partner this program. Based on the clinical results
seen with INCB28050 and other JAK inhibitors in development, it is clear that these oral
compounds have the potential to be equally efficacious if not superior to the highly effective
anti-TNF biologics.
Our 11beta-HSD1 inhibitor, INCB13739, which is being developed for type 2 diabetes,
achieved positive Phase IIa trial results in 2008. As I mentioned above, the double-blinded,
placebo-controlled, dose-ranging, three-month Phase IIb clinical trial is now completely
enrolled with results expected this summer. As has been our objective since we began this
program, if these results are positive, we intend to secure a partner for INCB13739.
Our sheddase inhibitor, INCB7839, which is being developed for breast cancer is in an
ongoing Phase II trial in combination with Herceptin�. We have seen encouraging early
results in a well-defined subset of breast cancer patients and similar results in additional
patients would define a clear and potentially rapid path forward for regulatory approval.
Key Accomplishments in the Myeloproliferative
Disorders Program
? Submitted a special protocol assessment to secure
agreement to begin pivotal Phase III trials in the
first half of 2009
? Orphan status granted in the U.S. and Europe for
MF, including primary MF, post PV-MF and post
ET-MF
? Presented positive Phase II results at several
scientific meetings demonstrating that INCB18424
treatment results in rapid and durable clinical
benefits in MF patients
? Initiated an open-label multiple-dose Phase II trial
to determine the safety and efficacy of INCB18424
in patients with advanced PV and ET, which could
expand our market opportunity in MPDs 
In 2008, we filed investigational new drug applications (INDs) for two new oncology
programs involving, respectively, oral inhibitors of c-MET and indoleamine 2, 3-
dioxygenase. Both INDs have been cleared by the FDA. We intend to initiate clinical trials
for these compounds once we secure additional funding from one or more corporate
partners.
We have several other programs that we believe warrant further development, including our
CCR2 inhibitor for multiple sclerosis, our CCR5 inhibitors for HIV and our HM74 agonist for
type 2 diabetes. These are now outside our core focus in oncology and inflammation and
we are looking to out-license these programs.
Last year�s appointment of Pat Andrews as Incyte�s executive vice president and chief
commercial officer reflected our decision to begin building the capabilities and infrastructure
to commercialize our first product. Pat�s pharmaceutical industry experience and expertise
in launching and marketing new oncology therapies and in business development are
especially valuable to us as we advance INCB18424 into registration studies and expand
our partnering activities.
Finally, I want to express my gratitude to Matthew Emmens for his service to Incyte as a
member of our board of directors. We wish him well in his new position at Vertex
Pharmaceuticals.
In closing, I want to thank our employees for their many contributions to our goal to
discover, develop and commercialize important new medicines. I have great confidence
that their ongoing efforts and hard work in 2009 and beyond will yield substantial value to all
of our key stakeholders.
Sincerely,
Paul A. Friedman, M.D.
President and Chief Executive Officer
April 2009 