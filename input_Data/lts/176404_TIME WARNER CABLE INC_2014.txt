Dear Shareholders:

I begin my second letter to you as Chairman and CEO of Time Warner Cable in
the same way I ended my firstwith a renewed commitment to the two primary
objectives that have guided my leadership of this company over the past
17 months: maximizing shareholder value and delivering great customer
experiences. Thanks to 55,000 dedicated and resilient employees, I am pleased
to report, the company today is far stronger than at the beginning of 2014. We
have great operating momentum and are extremely well positioned for the future.
Reflecting on 2014
We began 2014 with our historic agreement to merge with Comcast Corporation.
The transaction recognized Time Warner Cables unique value and, we believe,
would have accelerated our business goals, stimulated innovation and benefited
our customers and shareholders. Ultimately, it became clear the proposed merger
could not gain regulatory approval, and, on April 24, 2015, we mutually agreed with
Comcast to terminate our merger agreement.
Throughout the year, even while we engaged in integration planning and sought
regulatory approvals, we remained steadfastly committed to executing on our
operating plan so that we would be positioned well for life as a stand-alone
company in the event the merger could not be consummated. I am extremely
proud of the way the TWC team performed during what was, without question,
a fluid and challenging environment.
In last years letter, I promised that during 2014 we would revitalize our
residential services business by improving the reliability of our network
and enhancing customer service, differentiating our products in ways that
matter to our customers, reinventing the Time Warner Cable experience by
launching TWC Maxx, and growing our subscriber base through better
customer acquisition and retention. I committed to growing business services
toward our goal of $5 billion in annual revenue by 2018. And I said we would
capitalize on the mid-term elections to generate record revenue in our
advertising business.
I am pleased to say that our team delivered on all counts. In fact, the team
surpassed my loftiest expectations. We ended 2014 ahead of plan in almost
every operational category.

Getting Stronger in 2015
The tremendous momentum we built in 2014 has continued, and increased, into
2015. In fact, the first quarter of 2015 was our best subscriber quarter ever. We
added the most customer relationships ever, the most triple plays ever and the
most voice customers ever. And, notably, for the first time since the first quarter
of 2009, we added video subscribers. At the same time, we improved the overall
quality of our subscriber base...with more customers taking higher tiers of service,
paying us more for those services and remaining with us longer.
The business services team continues to execute well from a proven playbook.
For the 15th consecutive quarter, business services revenue grew by more than
$100 million year over year. And even as we invest in future sales growth, business
services profit margins are improving and making an even greater contribution to
our bottom line.
As we look forward to the rest of 2015, we will continue to strive to deliver even
better customer service, make investments to improve the reliability of our
network, enhance training and tools for our customer service, sales and retention
agents, make our products better, deploy new and more capable equipment in
our customers homes, and roll out TWC Maxx...which, beyond increasing
speeds, has become for us a new way of doing business to transform the
customer experience.
In business services, well continue to expand our network, automate
and standardize our processes, augment our product portfolio and
improve productivity.
In summary, and put simply, I feel great about the overall health and momentum of
our company.
To my Time Warner Cable colleagues, who performed so admirably in a year filled
with great uncertainty, I again thank you. I am extremely appreciative of the way
you have come together to make us a stronger and better company. I couldnt be
more proud to lead our organization.
To our customers, we remain committed to providing you with the service you
expect and to offering you compelling products at a price that delivers great value.
And finally, to our shareholders, thank you for your continued support and
confidence in us as we move this great company forward.
Sincerely,
Robert D. Marcus
Chairman and Chief Executive Officer
