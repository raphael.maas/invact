Letter to Stockholders
The last several letters to stockholders described a multi-year period
of strong senior housing and health care fundamentals, attractive
investment opportunities, excellent access to reasonably priced capital
and resultant strong Company performance. An ironic result of the
sector strength was the infusion of large amounts of capital during 2006
and 2007 that drove asset prices up and narrowed investment spreads.
During this period, we made a major commitment to investing across
the full spectrum of senior housing and health care properties. This
process was dramatically accelerated in late 2006 through our merger
with Windrose Medical Properties Trust. Our full-service capabilities
were further enhanced through Windroses development platform and
the 2007 acquisition of a national medical office building property
management company.
In late summer 2007, the credit crunch began to impact
market liquidity. In late 2007 and throughout 2008, the economy
entered a serious recession. Unlike many other sectors, REITs, and
health care REITs particularly, continued to have sporadic access to the
equity markets. As the year progressed, winners and losers emerged in
the equity markets, largely based upon liquidity considerations. Since
that time, stock prices within the health care REIT sector have fallen,
albeit less significantly than in most other sectors. The unsecured debt
market essentially closed for REITs. At this writing, the debt markets
have improved somewhat but remain extremely expensive.
In 2008, our focus gradually shifted from investment to capital
preservation. In 2009, we are funding ongoing development projects
and preserving cash. New investments will be few, and will only be
made if they are compelling and funding is clearly identified. At the
same time, we will continue to strengthen existing operator, system
and developer relationships and to cultivate new relationships.
We must plan for a prolonged period of economic and market turmoil. However, we will
position ourselves to take advantage of investment opportunities once the capital markets reopen.
2008 Highlights
Before attempting to describe our goals and plans for 2009 in these uncertain times, I will review
2008 highlights:
1. Company Performance. In one of the toughest years on record both for REITs and the
broader equity markets, we are pleased to have generated a positive 0.5% total return for our
stockholders.
2. Investments. We completed gross investments of $1.2 billion in 2008, of which 85% related to
our preferred investments in combination senior housing facilities and customer-focused medical
facilities.
3. Portfolio Performance. Payment coverage at the property level stood at 1.96 to 1.0. We
continued to avoid concentration risk, as our top five operators comprised only 25% of the portfolio.
We believe both of these metrics are the best in our peer group.
4. Dividends. Our February 2009 dividend payment was our 151st consecutive payment. Our
normalized FFO and FAD payout ratios for 2008 were 80% and 85%, respectively.
5. Capital. Although the unsecured notes market was effectively closed in 2008, we did raise a
total of over $775 million of equity at attractive prices and generated $296 million in additional capital
from dispositions.
Health Care and Senior Housing Challenges
In my most recent stockholder letters, I have focused on the changes occurring in health care and
senior housing. The drivers of change include technological advances and pharmacological breakthroughs
that make non-invasive and minimally invasive procedures the focal point of the health
care delivery system. Another key driver is the primacy of the customer, who is demanding better
services and customer-friendly facilities. The customer is increasingly seeking an integrated
approach to lifestyle and health care needs with more focus on wellness and complementary
treatments.
Both of these factors in turn require dramatic change to health care and senior housing real
estate platforms. We have anticipated these needs by investing in high quality, customer-focused
facilities operated by strong management teams. Top teams focus on delivering excellent care costeffectively,
recognizing that profits naturally follow from this appropriate prioritization. Senior care and
health care are clearly areas in which doing the right thing produces the best results.
In the senior housing sector, seniors wish to live in facilities that offer multiple levels of
service. This allows residents to age in place, accessing the continuum of care in one location as
their housing and health care needs change. These facilities include combination facilities that offer
two or more levels of care, such as independent living, assisted living, skilled nursing and dementia.
They can also be larger continuing care retirement communities (CCRCs) that have a housing
component coupled with independent living, assisted living and, usually, skilled nursing. As facilities
and campuses become larger, more wellness, fitness and other services generally follow. A number
of studies have concluded that properties with multiple levels of service produce higher occupancy
rates and revenues.
While these combination properties and CCRCs have felt the pain of the economic downturn
and ravaged housing market, we believe these facilities will continue to fill, although at a somewhat
slower rate. Yet, even with the buy-in CCRC projects, approximately 40% of the units are
rental in nature and are generally filling in line with our expectations. Once the economy and housing
begin to rebound, we believe that these more modern, high quality, customer-friendly facilities will
have significant pricing power and will position themselves as key components of our portfolio.
(See pages 12 and 13 of this Annual Report for photographs of representative facilities.)
In the acute care and medical office building sectors, hospitals are moving away from older,
institutional platforms to facilities that optimize the patient experience and also provide integrated
care and wellness. We are investing in hospitals, medical facilities and medical office buildings that
reflect the changing health care arena. Health systems are increasingly locating satellite hospitals,
medical office buildings and other medical facilities in locations more accessible to their customers.
(See pages 14 and 15 of this Annual Report for photographs of representative facilities.)
As in senior housing, acute care facilities are not immune from the effect of the economic
downturn. Bad debts are up; elective surgeries are down. Regardless of those issues, we believe
that acute care facilities remain more resistant to economic problems than other property types
given the relatively inelastic demand for health care services.
Managing Risk in Tumultuous Times
As we navigate through these tough economic times, we will never let our guard down with regard
to possible operator or facility issues. Nor can we assume that the capital markets will necessarily
become more friendly in the near future. In a sense, all of what we do involves managing the risk
of our particular enterprise. In economic times like these, it seems appropriate to describe our risk
management techniques.
Portfolio Management
We are generally credited with having one of the most comprehensive portfolio management
programs in the REIT industry. Our approach has produced a portfolio that is the most diversified
and has the highest property level coverage within our peer group. Moreover, the private pay
component at year end stood at nearly 65%, which we believe over time appropriately balances
private pay and governmental revenue sources. Our portfolio management program begins with
strong industry research, origination and underwriting, and then rigorous monitoring of our portfolio
by our asset management group. Larger economic upheaval, as evidenced by todays recession
and housing disaster, can adversely affect even the strongest portfolio. While these exogenous
forces can be critical factors, the primary portfolio risk normally is operator execution.
To mitigate these risks, we have developed, and continuously refine, a comprehensive,
state-of-the-art management system that addresses origination, underwriting and monitoring.
All are informed by our long-term perspective on health care and senior housing that is
continuously being enhanced through active, in-depth industry research.
Origination
This function is divided between the senior housing and acute care groups led by Chuck Herman
and John Thomas, respectively. Our experienced senior housing marketing team is comprised of
Mike Stephen in Sarasota, Florida; Mercedes Kerr in Irvine, California; and Joe Weisenburger and
Steve Blazejewski in Toledo, Ohio. On the acute care side, we also have a seasoned marketing
team comprised of Will Roberson in Jupiter, Florida; Greg Schonert in Dallas, Texas; and Dan Loftus,
Steve Buckeridge, O.B. McCoin and Ryan Doyle in Nashville, Tennessee. We believe that this group,
together with senior management, has tremendous reach in the marketplace. We have been quite
successful in identifying quality operators, health systems and facilities and establishing long-term,
productive relationships that have produced a diverse, strong portfolio.
Underwriting
Once we have identified an opportunity, the underwriting process begins. Our 10 person underwriting
team, led by Scott Brinker, evaluates all investment opportunities. Their analysis has four
parts: the industry, the operator, the facility and the local market.
With respect to the industry, we have a long-term investment horizon and therefore seek to
invest in industries with favorable long-term fundamentals. We believe we are uniquely qualified to
make this determination because of our 38 years in the business and the scale of our existing
portfolio, which provides an invaluable resource to spot challenges and opportunities. We also
diversify our investments among operators, property types and regions to reduce concentration risk.
The operator is critical to our investment because it delivers health care services to the
residents, maintains our facility and produces the cash flow that is used to pay our rent. Our
networking within the industry is unparalleled and allows us to select operators who have
best-in-class reputations and capabilities. We also complete a comprehensive credit analysis to
evaluate the operators historical profitability, liquidity and capacity to manage our facility.
The facility is the primary collateral for our investment, so we perform a thorough evaluation
of its historical and projected financial performance. We use a variety of internal operating metrics
and benchmarks to measure the facilitys performance. When reviewing financial information, we
focus on the sources and uses of cash and emphasize the importance of long-term trends rather
than point-in-time measures. This analysis helps to ensure that we do not overpay for investments
and that we will receive our monthly rent payment. The physical plant is equally important to our
investment decision. Technological advances and consumer demands are changing the face of
health care real estate, and we seek to own a portfolio of assets that will increase in value as the
market evolves.
Health care is a local business with market-specific delivery models, referral sources,
competition and demographic forces. We seek to own facilities that fill a niche within the local
market. In general, this means that the facility is integral to the delivery of health care in the
local market, has a prime location in a growing market and is operated by a third party who is
well connected within the local health care market. We conduct a multi-day, on-site inspection of
each market to complete this assessment.
Asset Management
At closing, responsibility for monitoring an investment shifts seamlessly to the nine person asset
management team managed by Chris Simon. His teams primary objectives are to protect stockholder
capital and enhance stockholder value. Asset management employs a systematic review of
payment risk including the monthly review of financial statements, periodic review of obligor credit,
periodic property inspections and review of covenant compliance relating to licensure, real estate
taxes, letters of credit and other collateral. In monitoring our portfolio, our personnel use highly
effective, proprietary software to collect and analyze property, operator and industry-specific data.
Additionally, they conduct extensive research to ascertain industry trends and risks. Through these
asset management and research efforts, we are typically able to intervene at an early stage to address
payment risk, and in so doing, support both the collectability of revenue and the value of our investment.
Balance Sheet Management
In normal times, our conservative balance sheet management is largely assumed because it has been
our trademark for 38 years. In todays capital constrained marketplace, where cash is king and
balance sheet conservatism is imperative, this focus is valued even more highly. Our approach to
balance sheet management is really fairly simple:
1. Maintain cash and line of credit availability to minimize our near term dependence on the capital
markets.
2. Maintain moderate leverage, which for us suggests a debt to undepreciated book value of
approximately 45% or less.
3. Maximize our access to capital by maintaining our investment grade ratings and driving strong
equity multiples. Both require us to effectively communicate our vision and successful execution to
the capital markets. Among the health care REITs, we have the highest rating with Moodys Investors
Service at Baa2. Investment grade ratings require us to limit the amount of secured debt, maintain
high interest and fixed charge coverage ratios, maintain low FFO and FAD payout ratios and limit
development.
Growth in a Recessionary Market
Our growth has been in the top levels of health care REITs in each of the last five years. We continually
recycle assets, acquiring or developing desirable asset types and disposing of assets that do not have
the same long-term appeal in light of health care or senior housing trends. We are focusing on
combination facilities and CCRCs in the senior housing sector and more modern MOBs, hospitals
and other medical facilities in the acute care space. Conversely, we are disposing of stand-alone senior
housing facilities and small MOBs with limited medical services. In 2008, 85% of our investment
activity related to modern medical facilities and combination facilities, while 80% of our dispositions
were freestanding properties.
While we believe our growth platform is second to none, this is not a time for significant
additional investment commitments. We will finish our development funding, but will consider
additional investments very selectively and then only with an identified source of funding. If possible,
we will also accelerate dispositions to provide additional capital.
On the other hand, we have invested, and will continue to invest, in key people for our team,
and will continue to spend time and money in meeting with existing and prospective operators. We are
a key player in this space and want to maintain our visibility and leadership, especially in these times.
Once the capital markets reopen and stabilize, we believe that the combination of our bestin-
class access to capital and health care and senior housing leadership should lead to a period
of unprecedented investment opportunity. Cap rates are moving upward, marginal investors in our
space are gone, and our full-spectrum investing platform gives us a multitude of different ways to
invest successfully. In short, we are being conservative now as is mandated by these times. Yet, we
are positioning the Company for a period of great investment opportunities.
Changes to Our Intellectual Capital
Every year we experience changes in personnel as we attract new talent to the Company and some
colleagues decide to move on or reduce their time commitments due to their desire to commit to
other endeavors.
This year, my colleague Ray Braun has decided to move on to a third career. After serving
with him as a partner at a law firm and as a key executive at the Company, I will miss his experience
and intelligence, but understand the allure of taking on a new career challenge. Fortunately, Ray will
remain a consultant during 2009, transitioning his responsibilities and relationships to others and
continuing to train our talented young people. We truly thank him for his service and wish
him only the best.
Another colleague, Fred Farrar, must reduce his time commitment to the Company as he
needs to expend more time on his obligations at Klipsch Group, Inc., an international audio products
company. We appreciate Freds willingness to continue as a consultant to the Company, maintaining
his close relationships with developers and operators. His availability as a mentor to John Thomas,
our new head of acute care, will be invaluable to John and the Company.
John Thomas joined our Company on January 19, 2009, adding his extensive knowledge,
experience and relationships in the acute care sector. Before joining the Company, John was
President and Chief Development Officer of Cirrus Health, a Dallas-based owner and operator
of hospitals, ambulatory surgery centers and other health care facilities. His responsibilities included
all aspects of their development activities. He previously served as Senior Vice President/General
Counsel for Baylor Health Care System, Dallas, and General Counsel/Secretary for the St. Louis
division of the Sisters of Mercy Health System. We welcome John and look forward to his
successful efforts to take a top flight team with a full spectrum of capabilities to the next level.
Several other promotions have also been made:
Jeff Miller has been promoted to Executive Vice President-Operations and General Counsel.
He will essentially be chief of staff and will be responsible for virtually all aspects of the Companys
operations.
Scott Estes has been promoted to Executive Vice President and Chief Financial Officer due to
his significant, successful efforts to promote the Company in the capital markets.
Mike Crabtree has been promoted to Senior Vice President and Treasurer due to his key role
in managing our treasury functions, including pursuing alternative capital sources.
Scott Brinker has been promoted to Senior Vice President-Underwriting and Research in
recognition of his contribution to developing, refining and leading our comprehensive underwriting
system.
Mike Stephen has been promoted to Senior Vice President-Marketing in recognition of his
12 years of success with the Company in expanding our operator and facility base.
We are quite proud of all of our people and understand that in the last analysis intellectual
capital is our most important resource. We have built a great team that will change from time to time,
but will always provide leadership within health care and senior housing.
Conclusion
Once again, we thank you for your support and confidence. Know that we will manage your
Company appropriately in all cycles, understanding that, first and foremost, we are stewards of our
stockholders.
George L. Chapman
Chairman, Chief Executive Officer
and President
March 6, 2009