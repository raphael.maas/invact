On behalf of Meredith Corporation and our more
than 3,700 employees, we want to thank you for your
investment in our Company. As a shareholder, youve
entrusted us with your financial resources. Thats a
responsibility we take very seriously.
At Meredith, we are executing a strategic plan that
strengthens our connection with consumers and
delivers a superior return for our shareholders.
Fiscal 2016 was a year of strong growth in revenues
and cash flow. We generated record revenues of $1.65
billion, a 3 percent increase over fiscal 2015. Fiscal 2016
earnings per share (EPS) were $0.75 - which includes
special items primarily related to non-cash impairments -
compared to $3.02 in fiscal 2015. Excluding special items
in both years, we delivered EPS of $3.30, matching fiscal
2015 even though we recorded $31 million  or $0.42
per share  less of incremental, high-margin political
advertising revenues in fiscal 2016.
Several accomplishments stand out in fiscal 2016:
 We expanded our audiences across media platforms
and increased our reach to Millennial women.
 In our television portfolio, nine of our stations
ranked No. 1 or No. 2 in late news viewership,
and eight stations ranked No. 1 or No. 2 in
morning news, according to the May 2016 data
compiled by Nielsen.
 Readership across our magazine portfolio grew to
a record 127 million, according to the Spring 2016
GfK Mediamark Research & Intelligence Report.
 Traffic to our digital sites increased to more
than 80 million monthly unique visitors, according
to comScore.
 Our reach to Millennial women grew to 72
percent of U.S. female Millennials, a jump of
nearly 10 percentage points.
 Merediths multi-channel reach to American
women hit an all-time high of 102 million.
Additionally, Merediths database grew to 125
million American consumers.
 We grew non-political, magazine and digital
advertising revenues. Local Media Group
non-political advertising revenues increased in the
mid-single digits, and the groups digital advertising
revenues were up in the low-teens. National Media
Group magazine advertising revenues grew in the
low-single digits, and the groups digital advertising
revenues were up in the mid-teens.
To Our Shareholders
Meredith Corporation 2016 Annual Report | 1
 We increased revenues from businesses not
dependent on traditional advertising. Our Local
Media Group delivered growth in retransmission
consent fees and contribution by renewing
retransmission consent agreements with pay
television providers at higher rates. Our brand
licensing activities delivered record performance in
fiscal 2016 and are now ranked No. 2 in the world,
according to License! Global magazine.
 Finally, we continued to execute our Total
Shareholder Return strategy. We grew our dividend
for the 23rd straight year, increasing it in February by
8 percent to $1.98 per share on an annualized basis.
Weve paid an annual dividend for 69 straight years,
and its currently yielding approximately 3.5 percent.
We also strengthened our balance sheet by paying
down $100 million of debt. 
Our Plans for Fiscal 2017
As we look to fiscal year 2017, we anticipate generating
record earnings per share driven by a diverse range
of business activities. These include a robust political
advertising cycle, higher retransmission contribution and
strong digital advertising growth across the enterprise.
To achieve this goal, our Local Media Groups strategic
priorities consist of:
 Continuing to strengthen our local programming,
and monetizing audience gains through increased
advertising revenues. We continued to add news
hours across our portfolio, increasing them from 270
hours in 2005 to 660 hours today.
 Maximizing our opportunity for political advertising
revenues. While we are still early in the campaign
cycle, we anticipate delivering between $40 and $50
million of political advertising revenues. That would
represent a strong increase over the last presidential
election year.
 Monetizing our fast-growing digital platforms.
Were adding more video and focusing on monetizing
our mobile traffic, which grew nearly 40 percent in
fiscal 2016. Weve tripled digital advertising revenues
in our Local Media Group over the last six years, and
believe we have room for continued strong growth.
 Renegotiating favorable retransmission
agreements with cable, satellite and telecom
providers. About 40 percent of our subscribers
are up for renewal in fiscal 2017. Since most of our
major network affiliation agreement renewals are in
fiscal 2018, we expect to continue to deliver higher
retransmission contribution and margins.
In our National Media Group, we are focused on these
strategic priorities:
 Ensuring that our content remains relevant,
and that we grow our reach with the Millennial
generation. We now reach nearly 75 percent of
women in this important demographic, and expect
continued growth as many start families and buy
their first homes.
 Growing total advertising revenues and increasing
our share of market. In print, our share in our
competitive set is at an all-time historical high of 41
percent. Our digital strategy focuses on delivering
premium branded content, accumulating rich and
differentiated first-party data, and using advertising
technology to put the right message in front of the
right consumer at the right time.
2 | Meredith Corporation 2016 Annual Report
 Generating more profit from consumers. Our
major magazine circulation opportunity comes from
transitioning our nearly 30 million subscribers to an
auto-renewal business model, which is nearly twice
as profitable as traditional subscription sales. Today,
about 15 percent of our new subscription orders are
auto-renewal, so the potential benefit is great.
 Growing brand licensing and other non-advertising
sources of revenue. This business is anchored by our
relationship with Walmart, and we recently renewed
our licensing agreement for three more years. We
have more than 3,000 Better Homes and Gardens
(BHG) branded SKUs available at 4,000 Walmart stores
nationwide and at Walmart.com. We are expanding
this program to Mexico and China. Additionally,
we have a BHG-branded real estate program with
Realogy that now includes more than 250 offices and
10,000 agents. Finally, we continue to launch new
partnerships, including an EatingWell frozen food line;
a line of womens fitness apparel carrying our Shape
brand; and a line of Allrecipes-branded cookware.
Our Long-Term Vision
Looking to fiscal 2017 and beyond, we will continue to focus
on producing consistently strong free cash flow, driven by:
 A great group of television stations in large,
fast-growing markets.
 Trusted national brands with an unrivaled reach
to women, particularly Millennials.
 A profitable and growing digital business.
 A vibrant and growing licensing business based
on our very strong brands; and
 A strong and proven management team with a very
successful record of generating strong free cash flow
and growing shareholder value.
Our talented and creative employees play a vital role in
Merediths success. They include our inventive content
creators, innovative sales and marketing professionals,
dedicated support groups and committed management
team. Our workforce is the best in the media industry,
underscored by our 114-year track record of success.
In closing, we continue to be highly confident in the strength
and resilience of Merediths diversified business model. We
have a proven track record of developing our existing brands
and profitably integrating acquired properties. We have a
long history of prudent capital stewardship and an ongoing
commitment to Total Shareholder Return.
It is our mission and pledge to protect and grow the
value of your investment in Meredith over time.
Sincerely
Stephen M. Lacy
Chairman and
Chief Executive Officer
Mell Meredith Frazier
Vice Chairman
As part of Merediths executive development and succession planning process, the Meredith Board of Directors elected
Thomas H. Harty as President and Chief Operating Officer in August 2016. In his new role, Harty, 53, will oversee
Merediths National and Local Media Groups. He will continue to report to Meredith Chairman and CEO Stephen M. Lacy,
allowing Lacy, 62, to focus more on long-term strategy, business expansion and acquisition activities.
The Board also elected Jonathan B. Werther, 47, to succeed Harty as National Media Group President. Werther previously
served as President of Meredith Digital.
During his tenure at Meredith, Tom has led initiatives to strengthen our media brands and grow diversified revenue
sources such as digital, brand licensing and marketing services, said Lacy. Working together with Local Media Group
President Paul Karpowicz and Jon Werther, Im confident Tom can continue to grow Merediths position as one of
Americas leading media and marketing companies.