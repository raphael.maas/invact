Letter from the Chairman of the Board

The business environment is undergoing significant change
as the macroeconomic cycle, related monetary policy,
heightened geopolitical activity and evolving technology
impact financial markets. The pace of that change is
accelerating and clearly influences many companies, including
Moody�s, along with our employees and our customers.
Moody�s Board of Directors is committed to ensuring
that the company is well-positioned not only to weather
change but to thrive with it by taking advantage of inherent
new opportunities.
Central to our ability to prosper throughout cycles is staying
true to Moody�s roots as a purpose-driven organization. We
are focused on providing information, tools and solutions
to promote market transparency and fairness. Both are
necessary conditions for market confidence, which in turn
supports healthy financial markets over time.
In keeping with this purpose, Moody�s has continued to
increase our presence globally and to expand the scope of our
products and solutions to create new standards in financial
markets adjacent to credit markets.
Moody�s Investors Service (MIS) continues to defend its
core position as a leader in credit ratings and research. Its
ratings remain predictive, predictable and transparent, and
it provides timely and insightful research. At the same time,
MIS continues to pursue strategic growth opportunities,
benefitting from the tailwinds of long-term global economic
growth and continued disintermediation of credit in both
developed and developing economies. A representative
example of these efforts can be seen in Latin America, where
MIS recently invested in ICR Chile. With this investment, MIS
and its affiliated companies now assign domestic ratings in
seven countries within the region.
Within Moody�s Analytics (MA), Moody�s has remained
focused on the successful integration of Bureau van Dijk,
acquired in August 2017. Bureau van Dijk�s database of
financial information for over 300 million small and medium
size enterprises provides a tremendous platform for MA to
expand its customer base and product offerings. MA has
also increased its commercial real estate presence with the
acquisition of Reis, Inc. and added to its financial training 

solutions with the addition of Omega Performance. The
crucial nature of MA�s data and analytics allows for strong
retention of its existing customers as well as ongoing sales
to new customers. Further, MA�s use of emerging
technologies to deliver its products and solutions embeds
MA in its target markets.
Early in 2018, Moody�s launched a new corporate social
responsibility (CSR) strategy to build a closer bond to our
businesses. This strategy features four areas of activity:
empowering people with financial knowledge, activating an
environmentally sustainable future, helping young people
reach their potential and sharing our passion and purpose
with the world. A cornerstone of the strategy is Reshape
TomorrowTM, a global program focusing on empowering
entrepreneurs�especially women and members of
underserved communities�that helps provide them with
expertise and tools to successfully navigate the credit process.
In October, Moody�s announced six partner organizations and
programs that support our CSR objectives.
Moody�s also continues to focus on ensuring that we employ
best practices across all areas of management, especially
governance. To that end, I would like to welcome our new
directors: Vincent Forlenza and Gerrit Zalm, who joined
Moody�s board in 2018, and Th�r�se Esperdy, who joined in
2019. They have been strong additions who add unique and
insightful expertise.
In closing, I extend my appreciation to my fellow board
members, Moody�s executive management team and
Moody�s employees for their hard work and dedication
in 2018. I would also like to thank Moody�s shareholders,
many of whom have supported the company for numerous
years. Moody�s remains true to our core purpose, focused
strongly on execution, and we are well-positioned to adapt
to changing conditions and circumstances. I strongly believe
Moody�s will remain an essential component of the global
capital markets.
Henry A. McKinnell, Jr., Ph.D.
Chairman of the Board