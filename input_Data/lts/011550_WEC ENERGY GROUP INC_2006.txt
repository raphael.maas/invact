To Our Stockholders,

To paraphrase the lyrics from an old Frank Sinatra song
2006 was a very good year.
In fact, on nearly every meaningful measurefrom customer satisfaction
to earnings and cash flow, from operational performance to
progress on our construction planthis was the best year in the
110-year history of the company.
Record Financial Results
Earnings from continuing operations rose to a record $312 million
or $2.64 a share in 2006. Excluding gains and nonoperating items,
adjusted earnings for the year were $2.58 per share. This is an increase
of 6.6 percent as compared with earnings on the same basis in 2005.
Key factors driving this increase included a full year of operation of
our new Port Washington Generating Station, stronger margins in our
natural gas distribution business and full recovery of our fuel costs.
Im also pleased to report that the financial markets responded
positively to our progress and to the prospects of our long-term
business plan. Wisconsin Energy stock hit 34 new all-time closing
highs during the year. We ended 2006 at $47.46 a shareup
21.5 percent from year-end 2005. And our stock outperformed
virtually every relevant market index including the Dow Jones
Industrial Average, the S&P 500, the Russell 2000, the S&P Electric
Index and the Philadelphia Utility Index.
As the performance chart shows on page one, our total shareholder return
over the past five years has also been strongmaterially outpacing the
S&P 500 and our peer group of 30 utilities across the United States.
Our ratio of debt to total capital in the business remained essentially
unchanged at 59.5 percent as we closed 2006. This is a significant
accomplishment in light of the fact that
we invested nearly $930 million during
the year to improve the energy infrastructure
in Wisconsin and Michigans
Upper Peninsula.
As we turned the page on the new year,
our board of directors voted to raise
the dividend on our common stock
to a new quarterly rate of 25 cents
a sharean increase of 8.7 percent.
This marks the fourth consecutive year
that weve raised our dividend.
A Focus on Customer Satisfaction
The long-term success of our franchise
rests in our ability to satisfy customers
with reliable service and responsive
care. And Im pleased to report that we
continued to make significant progress
last year in raising the level of customer satisfaction
with our company and with the manner in which every
customer transaction was handled.
Independent surveys show an increase of nearly
8 percentage points in the number of customers who
were very satisfied during the fourth quarter of 2006
compared with the fourth quarter of 2005.
One way that we work to ensure customer satisfaction
is our We Care Call program. More than 500 employees
from across the company make We Care callsfollowing
up with customers after theres been an outage at
their home or business to determine that the service
restoration was completed to the customers satisfaction.
We Care calls are also made each day to customers
whove received other types of service from the company,
such as forestry work or
meter changes.
In all, we made more than
365,000 We Care calls
during 2006. And we were
honored by Chartwell, a
research organization that
studies best practices in
the utility industry, as the winner of the 2006 national
award for customer service.
Op erational Excellence
Of course, our We Care calls would be of little value if we
didnt have a reliable network and a fleet of power plants
that runs well. Last year, for the fourth time in the past
five years, we were named the most reliable utility in the
Midwest by an independent firm that analyzes outage
data from utilities across the United States.
In addition, we set production records at two of our
largest power plants. Unit 1 at Pleasant Prairie, one of
our major workhorse plants, set a new site record with
517 days of continuous operationby far the longest
run in the plants 20-plus-year history. And our Point
Beach Nuclear Plant produced more energy during
2006 than during any other calendar year in its more
than 30-year history.
Growing Demand for Energy
As temperatures neared the 100-
degree mark on the last day of July,
our customers set another all-time
demand record for electricity. At
6,505 megawatts, the new peak
demand was approximately 5 percent
higher than the previous record set in the summer of
2003underscoring the decision to build additional
power plant capacity in Wisconsin.
In our natural gas business, more records were set
in February 2007 as arctic air gripped the region.
We delivered more natural gas to our customers on
February 4 and again on February 5 than during any
other 24-hour periods in our history. The new delivery
record is 6.8 percent above the previous one-day mark
set in 2004.
Building for the Future
As many of you know, were engaged in the largest
construction program in our companys historyas we
work to improve and expand the energy infrastructure
of the region. The plan, which was approved by the
Public Service Commission of Wisconsin, calls for the
completion of four major new generating units between
the years 2005 and 2010with a total investment of
nearly $2.6 billion.
The first new unit at our Port Washington site was
placed into service in July of 2005on time and on
budget. With 545 megawatts of capacity, this new gasfired
unit certainly helped us keep the power flowing
during the days of high demand last summer.
Construction of the second unit at Port Washington is
moving along very well. At year end, the project was
about 30 percent completetracking on schedule and
on budget.
At our Oak Creek site, south of Milwaukee, were building
two new coal-fired units. Activity at the site began
on June 29, 2005, following a favorable ruling by the
Wisconsin Supreme Court. Since that time, weve moved
forward with engineering, procurement, site excavation,
foundations, structural steel, air quality control equipment,
the coal handling facilities and the cooling water tunnel.
At the end of 2006, the project stood at almost
25 percent complete. And we continue to be very
satisfied with the progress at the site.
We have received all the permits needed to build the
facilities at Oak Creek. One of these permits, which
will allow us to operate the water intake and discharge
system, is being challenged by an environmental group.
On March 5, the parties received a decision from the
Circuit Court in Dane County. The judge reaffirmedin
several important respectsthe decision by the state
Department of Natural Resources (DNR) to issue our
water permit.
However, the judge remanded certain elements of the
permit for further review and asked that they be made to
comport with all aspects of Riverkeeper II. Riverkeeper
II is a case involving the federal Environmental Protection
Agency that could affect power plants nationwide.
We will work with the DNR on this additional review
of the permit. And in the meantime, construction
continues at Oak Creek.
Our plan remains to have the first unit at Oak Creek
in service in the summer of 2009. The second unit is
scheduled to followone year laterin 2010.
An Expanded Role for Wind Power
Im also pleased to report that we recently received
Public Service Commission approval to build the largest
wind farm in Wisconsin. We call it our Blue Sky Green
Field wind project, and we expect construction to begin
this year. The wind farm will have a generating capacity
of up to 200 megawatts. This is a key step in helping
us meet the requirements of a new Wisconsin law that
calls for nearly 5 percent of our retail electricity sales to
come from renewable sources by the year 2010.
Selling Point Beach
Throughout the past year, we also conducted a formal
review of our options for the ownership and operation of
our Point Beach Nuclear Plant. Point Beach is one of
our most valuable assets. It has served our customers
well for more than three decades and has years of
productive capacity ahead.
We initiated this review in light of changing circumstances
across the nuclear power industry. Companies that
operate fleets of nuclear power plants clearly have a
cost and talent advantage compared to companies that
operate at only a single site.
After a thorough analysis and an extensive market test,
we announced in December an agreement to sell Point
Beach to FPL Energy, a subsidiary of FPL Group in Florida.
FPL will purchase the plant, equipment and inventories
for $998 million, subject to normal adjustments at
closing. Weve also agreed to purchase all the energy
produced at Point Beach for the
remaining life of the unitsat a
price comparable to our projected
cost of power had we continued
to own and operate the plant.
I believe this is a very positive
transaction for the company. Not
only will we receive the highest
price ever paid per unit of nuclear capacity in the United
States, but weve also been able to preserve the benefits
of the plant for our customers for years to come.
We hope to receive all federal and state approvals and
close this transaction by the end of August 2007.
Supp orting Wisconsins Economic Growth
In last years report, I mentioned that a number
of business and government leaders were working
together to craft an economic development initiative
for southeastern Wisconsin. In part, this effort was
designed to give our region some of the fundamental
tools needed to attract and support economic growth.
One of those tools was launched in late Novembera
ChooseMilwaukee.com web site and a high-tech resource
center that will give any business or industrial firm instant
information on site availability, demographics, job training,
taxesall the fundamental data a company might need
to decide whether it wants to expand or locate here.
Im pleased that the Wisconsin Energy headquarters
building was chosen as the home for the new resource
center. This is just one more way were helping to grow
the economy, expand the opportunity for jobs and
sustain the quality of life that we enjoy in this region.
a look to tHe Future
The path of progress was marked with achievement in
2006. Our business plan is sound. Our prospects are
bright. And, well continue to focus on growing the value
of this enterprise for our customers and stockholders.
Our management team truly appreciates your confidence
and support.

Sincerely,
Gale E. Klappa
Chairman, President and Chief Executive Officer






