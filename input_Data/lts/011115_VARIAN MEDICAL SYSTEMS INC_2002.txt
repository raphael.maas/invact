Welcome to 02 Varian, a magazine and
summary annual report highlighting the
achievements of Varian Medical Systems
in fiscal year 2002. We have a lot to talk
about. Fiscal 2002 was a highly
productive and successful
year for our company.

For fiscal year 2002, the company reported:
 A 13 percent increase in net orders to
$974 million
 A 13 percent increase in sales to
$873 million
 A 2 point boost in the gross margin
to 39 percent
 A 2 point increase in our return on sales
to 16.6 percent
 A 17 percent increase in our year-ending
backlog to a record $698 million
 A 31 percent increase in net earnings to
$93.6 million ($1.33 per diluted share) compared
to prior year pro-forma earnings(2)
We increased our year-end cash and marketable
securities by 36 percent to $299
million, even after spending $55 million to
repurchase 1.4 million shares of common
stock. The company generated operating
cash flow of $156 million for the year,
helped in part by a sharp reduction in days
sales outstanding, which stood at 80 days at
the end of the year. Shareholder equity was
$473 million at the end of the fiscal year,
up 20 percent from the prior year.
All three of our business segments
achieved success during the year, but in different
ways. The Oncology Systems unit
capitalized on a booming radiation oncology
market, which is well funded and supported
by reimbursement rates that are
favorable to Intensity Modulated Radiation
Therapy (IMRT), the most advanced form
of modern radiotherapy. Our X-Ray
Products group responded effectively to
tough economic conditions including difficult
challenges in Japan by improving factory
efficiency, enhancing product quality,
and introducing new products that opened
new market opportunities. The Ginzton
Technology Center put our emerging
BrachyTherapy business on its feet and
extended our technical capabilities. While
varied, these achievements come by virtue
of a characteristic that is shared throughout
our company  the ability to execute.
ONCOLOGY SYSTEMS
Oncology Systems boosted annual sales
and net orders by 18 percent over the
previous year and managed a 40 percent
increase in operating profits. This growth
came from market expansion and an
increase in our already significant market
leadership position.
As of the end of the fiscal year, we had
equipped many more radiation oncology
clinics for Varian SmartBeam IMRT,
which enables doctors to improve outcomes
by focusing higher doses of radiation more
precisely on tumors while reducing complications
by avoiding surrounding healthy
tissue. The number of clinics treating
patients with IMRT had jumped to 188 
more than double the number that were
offering this treatment last year. With ongoing
product enhancements, growing media
coverage, and stronger patient demand,
the pace of adoption remains rapid.
Our commitment to provide a best-inclass
product for every facet of radiotherapy
and to integrate these products into a
user-friendly, fast, reliable system has proven
to be a winning strategy. We consider ourselves
to be second to none in software for
treatment planning and management and
in image-guided three-dimensional radiotherapy
systems components.
At the annual meeting of the American
Society of Therapeutic Radiology and
Oncology (ASTRO), we introduced our
Acuity planning, simulation, and verification
system, which will make it substantially
easier for clinics to begin treating
patients with IMRT. We also gave customers
a glimpse of the future by displaying
a prototype Clinac linear accelerator
with an on-board imaging system, which
will enable clinics to track and adjust
for tumor motion during treatment. With
IMRT, this is becoming an essential capability.
Both Acuity and the on-board
imaging system feature a flat panel imager
and X-ray tube built by our X-Ray
Products business.
X-RAY PRODUCTS
Our X-Ray Products group overcame a
very tough first half and got back onto the
growth track in the second half, creating
new market opportunities while tightly
controlling costs to maintain profitability.
In the second half this group reached new
agreements with two major customers who
have committed to buying more tubes. Our
engineers developed several new products,
including X-ray tubes for CT scanning and
airport explosive detection systems, in record
time. These new products are already being
shipped to customers. The group also initiated
production of our new PaxScan 4030A
flat-panel X-ray imager, a significant entry
in what is expected to be a large market in
the future. With disciplined management,
X-Ray Products also cut the cost of product
failures and improved factory efficiency.
GINZTON TECHNOLOGY CENTER
The Ginzton Technology Center successfully
fulfilled its incubation mission with our
BrachyTherapy business. We acquired the
GammaMed business from MDS
Nordion, creating a $30 million annual
brachytherapy business with a strong global
presence. Our BrachyTherapy group also
introduced a new version of the VariSeed
software product, which enables doctors to
improve treatment precision through intraoperative
planning. The Ginzton group
passed several milestones in key research
projects. Engineers demonstrated the feasibility
of cone beam CT scanning on our
Clinac and Acuity products, real-time respiratory
gating, and the use of marker seeds
to track prostate tumor movements.
We reshaped the organization through
several key management appointments
designed to build upon our talent, consolidate
and streamline operations, accentuate
and share best practices, and better focus
resources into emerging enterprises.
Timothy E. Guertin, president of Oncology
Systems and a 25-year veteran of the business,
has been made executive vice president
of the company. We expect that the new
structure will serve us well as our orders
approach the $1 billion mark.
In 02 Varian, we highlight the technologies
that we expect will carry us forward in
our ongoing campaign to expand a highly
profitable business based on fighting cancer,
improving X-ray imaging, and enhancing
inspection and security capabilities. We
expect that you will find the information
herein as compelling as we do.
Before signing off, we must acknowledge
the fine work of our employees. They have
executed our strategies with a commitment
and passion that come from knowing that
we are making a difference for the better.
Their many achievements in fiscal 2002
have positioned our company for another
excellent year in fiscal 2003. We all look
forward to sharing our progress with you
and we thank you for your support.
Sincerely,

Richard M. Levy
President and CEO

Richard W. Vieser
Chairman of the Board