  Chairman and CEO Letter

  To our shareholders:
        2003 was a year of solid performance for AES. We                                  difficult market, these facilities sold at attractive prices,
  began the year with a number of ambitious goals. And                                    bringing in proceeds of $1.1 billion. The key, however,
  then, we met them. We strengthened our financial posi-                                  was not just selling businesses that could realize good
  tion and balance sheet well ahead of schedule. We                                       value in a difficult market. It was also avoiding the sale of
  restructured key elements of our business portfolio and                                 businesses that are essential to our business strategy.
  challenged our operating units to improve their perfor-                                 We met that test. Our asset sale program helped our
  mance dramatically. Now, while keeping an eye on the                                    short-term liquidity  and clearly affirmed the substantial
  lessons of the past, a reenergized AES is focused on a                                  market value of our global portfolio.
  promising future.                                                                              Several of our businesses required signif icant
        Our business is one of creating and managing valu-                                restructuring. Eletropaulo, our distribution company in
  able long-term assets. Yet in the past few years, AES's                                 So Paulo, Brazil, had a complex business structure and
  rapid growth left it with too much near-term debt, matur-                               a heavy shor t-term debt burden. The restructur ing
  ing faster than could be supported by operating cash                                    process was prolonged and volatile. However, the agree-
  flow. Our response was to raise $3.1 billion in debt and                                ment reached in the last days of 2003 preserves material
  equity, ex tending our debt matur ities more evenl y                                    value for AES and gives our Brazilian businesses a capi-
  through 2015. Overall, AES parent company debt was                                      tal structure more suited to their cash flow profile. Brazil's
  reduced by $1.2 billion last year. As a result, our publicly                            government demonstrated its commitment to the fair
  traded debt rallied to par or better by year-end. We are                                treatment of foreign investors, who will play a crucial role
  pleased with how quickly AES achieved these results.                                    in meeting the growing demand for power in Brazil.
  And we welcome the recent recognition of our improve-                                          Similarly, important progress was made in develop-
  ment by the rating agencies despite the continued tur-                                  ing a refinancing plan for Gener, our generation business
  moil facing much of the electric power industr y. We                                    in Chile. This is a solid business and the second-largest
  expect to continue to reduce debt at the parent level, as                               electricity generator in a country with bright long-term
  our debt progresses toward investment grade.                                            prospects. The equity injected into Gener will strengthen
        This rapid turnaround required tough decisions                                    its balance sheet and help it return to financial health.
  about our business portfolio. To improve our liquidity and                              The reinvigorated company is now poised to capitalize on
  reduce our debt load, we sold 14 facilities in Africa, the                              the growth in the Chilean electricity sector and to be an
  Middle East, Asia, Europe, and the US. Despite the                                      important contributor in our global portfolio.


                                                          beyond
* Parent debt includes consolidated recourse debt plus New York Secured Equity Linked Loan and Drax credit obligations paid off in 2003
** Includes previously announced call and repayment of securities totaling $231 million through March 15, 2004
Sales                                                         Plant                                                          
                                                  



       Our repositioning also demanded painful but nec-       achievements of AES people last year, we are confident
essary write-offs of past investments. These decisions        that industry-leading performance will be reached.
were made to establish a sound business portfolio on                In the generation business, for example, an impor-
which to build successfully in the future. For example, we    tant driver of per formance is high availability of our
wrote off our investment in Drax, a large merchant power      power plants. AES started the year with plant availability
plant in the United Kingdom. We originally had hoped to       of 85%, which is about average performance in the US.
refinance Drax in a way that maintained an ownership          So our people went to work to do better. We ended the
interest for AES. In the end, however, the right decision     year with plant availability of 88%, which moved us
was to turn over our ownership to the lenders when it         to top quartile performance. And we think we can still
became clear that no other scenario would produce             go further.
acceptable returns for us. This kind of strict, disciplined         One of the main drivers of per formance in the
decision-making is a critical dimension for us to maintain,   distribution business is reducing commercial losses.
particularly as we look toward renewed growth.                These losses result, for example, when customers con-
       Our portfolio restructuring is largely completed.      nect to our system without paying. Our distribution busi-
Our focus is now on improving operating performance.          nesses in El Salvador alone reduced commercial losses by
To do this, we have made a number of important changes        $2 million last year, so we know that dramatic improvement
in the past year. We have realigned our business leader-      is possible. Many of our non-US utilities still have room for
ship under two Chief Operating Officers, John Ruggirello      major improvement that could yield hundreds of millions
f or t he Ge ne rat ion grou p, a nd Joe B ra ndt f or t he   of dollars of additional revenue with no additional costs.
Integrated Utilities group. Additionally, new leaders have    We will seize every opportunity for such improvement.
been appointed to senior positions across the enterprise.           We are well along with the three-phase recovery
       We have carefully identif ied the key dr ivers of      plan we launched last year. The first phase, to stabilize
performance for our businesses and have set demanding         our finances, is complete. The second phase, to improve
goa ls. Success w il l mean that each AES business            operating performance, is well underway. Now, AES is
ope rates at top-qua r tile pe r formance by 2006 and         moving into the third phase, to begin growing again
top-decile performance by 2008. Given the impressive          through disciplined investments.
                                                                                                     





       Capitalizing on global investment opportunities will         should be a meaningful contributor to favorable stock
enhance future growth and value creation. In the develop-           price performance.
ment of these opportunities, we have consciously decided                  Thank you for the trust you have shown by investing
to refocus our efforts under new, dedicated leadership. In          in AES. We look for ward to earning your continued
Februar y 2004 we recruited one of our directors, Bob               support with a company that is reenergized and worthy
Hemphill, to rejoin AES to lead this effort. Bob and his very       of your continued trust.
ca pa ble de ve lopme nt te a m w i l l e va luate pote nt i a l
investments in a disciplined manner, identify the best oppor-             Sincerely,
tunities, and pursue these with our proven development
skills and responsible attention to the quality of our invest-
ment portfolio.
       This strategy will take maximum advantage of our
global footprint and resources: transferring knowledge
and best practices from country to country; scanning
markets around the world to identify the most attractive                  Richard Darman
investment opportunities; drawing on functional and                       Chairman of the Board
geographic expertise; and building upon demonstrated
capacities for both entrepreneurial creativity and opera-
tional excellence. The combination of these factors gives
us a distinct competitive advantage.
       AES is moving in the right direction. We expect
double-digit growth in earnings per share over the next
several years, driven by sales growth, per formance                       Paul Hanrahan
improve me nt, cont inued de bt reduct ion, a nd new                      President and CEO
investment opportunities. And we believe earnings and
cash flow growth above the broader market averages                        March 15, 2004
