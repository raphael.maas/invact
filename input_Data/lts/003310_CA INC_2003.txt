
For us, Fiscal 2003 was all about seeking higher standards and better execution

in everything we do. In a very challenging economic environment -- particularly in
the Information Technology (IT) sector -- Computer Associates (CA) is appealing

to customers who are increasingly focused and disciplined in their infrastructure

technology decisions. As a result, we must be as flexible as what the future will

demand of us in order to build long-term partnerships that deliver value and
continue to drive CA's success.


This past year, we appealed to customers by increasing our market presence in

three ways. First, we offer the software industry's most comprehensive and

innovative management solutions. Second, our business model makes it easier
for customers to license, install and run our software based on their own spe-

cific needs and their own specific environments. Finally, we foster a company

culture that puts customers at the center of everything we do. In short, we

demonstrate flexibility on behalf of our customers and are building value in CA.
                          The power of our approach was reflected in increased           On a more encouraging note, CA's enterprise manage-
                          revenues, increased number of overall license transac-         ment solutions, which provide compelling, long-term
                          tions and increased customer satisfaction. We grew             returns on investment, are enjoying stronger demand.
                          in some of the most important measures of financial            Also, the security challenge that corporations face
                                                  health. We increased fiscal 2003       continues to be a topic of great interest from the data
                                                  revenues 5 percent to $3.12 bil-       center to the boardroom. This provides a great oppor-
          We are fortunate for the seasoned
                                                  lion, narrowed our GAAP loss to        tunity for CA's highly competitive security offerings.
      management team that is carrying
                                                  $0.46 per share and generated
               our company forward as we
                                                                                         In response to shifting market dynamics and demands,
           strive to be                           $1.31 billion in cash from oper-
                             one of the most
                                                                                         we reorganized and redirected two key parts of our
                                                  ations, an increase of 5 percent
                flexible, efficient enterprise
                                                                                         business during the past six months. We did this to
                                                  over the previous year.
                          software companies.
                                                                                         help ensure that our professional services and indirect,
                          While we are encouraged by our progress, we                    or channel, operations are better prepared to meet
                          recognize the economic challenges we face in sectors           the opportunities and challenges that lie ahead. To
                          such as information management tools and storage               enhance our ability to execute in these areas, we
                          management solutions. Customers' demand for these              reorganized the work force and management teams.
                          offerings continues to be tempered, at least in the            We believe these improvements position CA to take
                          short term, by their own economic concerns and con-            advantage of what we hope is an improving economic
                          sequent budget constraints. Our role is to be realistic,       environment, while delivering more value to our
                          while being prepared for longer-term opportunities.            customers.



      Going forward, we believe our business model gives                      Corp. says that FlexSelect Licensing "promotes goodwill,
        us the flexibility to continue driving innovation in our                as clients view CA as more customer-centric in showing
        business and technology.                                                flexibility to their needs and a greater commitment to
                                                                                the relationship." Equity research firm FTN Midwest
                                                                                Research notes that "CA's new business model fits
       Putting customers first
        Over the past three years, we have refitted and retooled                right into today's tight IT spending environment. By
        nearly every aspect of our business to better serve our                 making its products easier to buy, CA not only is
        customers. In the past 12 months alone, we branded                      poised to gain market share, but will likely also win
        the way we sell our solutions as FlexSelect Licensing ,                 goodwill with its customers." We believe that our
                                                                   SM




        created a new organization -- CA Technology Services                    model will become the standard for our industry in
        -- to provide customers with exceptional technical                      the years ahead.
        expertise, and evolved our CustomerConnect familySM




                                                                                By combining our technical pre-sales and professional
        of continuous, web-based support.
                                                                                services organizations, CA Technology Services con-
        With FlexSelect Licensing, customers have access to                     nects our technical experts with potential customers
        CA's software solutions on affordable and flexible                      from the initial interaction through the life of the
        terms. After more than two years, customers are telling                 project. We are now able to deliver valuable product
        us they not only appreciate our unrivaled licensing                     and service breadth and depth, and respond quickly
        flexibility, but they also view it as a competitive edge                to new business opportunities that result from our
        for CA. Wall Street analyst firm CIBC World Markets                     closer relationships.


                                  On a tactical level, we now offer customers the flex-                                                       We continued to
                                                                                                          During the year,
                                  ibility to download and pay for software, receive                                                              seek out highly
                                                                                                      the CA Board of Directors
                                  technical assistance and manage their accounts all                                                              qualified
                                                                                                                 corporate
                                                                                               adopted state-of-the-art
                                  from their computers through CustomerConnect.                                                                    individuals
                                                                                                   governance principles
                                                                                                                                                    to serve, and
                                  All our efforts are aimed at making it easier for               designed to achieve strong Board
                                                                                                                                                   in May 2003,
                                  customers to do business with CA. Based on what                  oversight and alignment with
                                                                                                                                                   former EDS
                                  customers are telling us, our efforts are making a differ-           shareholder interests.
                                                                                                                                                 Vice Chairman,
                                  ence and helping CA stand out from the competition.
                                                                                                                                             Gary J. Fernandes,
                                                                                                                                      was elected to the Board.
                                  setting higher standard
                                  Throughout the year, we remained committed to setting            As we move forward, we are fortunate that our key
                                  the highest standards of quality in everything we do.            Board committees are guided by seasoned profes-
                                                                                                   sionals and recognized industry experts. Our Human
                                  In 2000, CA became the first enterprise software
                                                                                                   Resources and Compensation Committee is headed by
                                  company to achieve worldwide certification with ISO
                                                                                                   Lewis S. Ranieri; the Corporate Governance and Nom-
                                  9002:1994 for quality in more than 200 offices and
                                                                                                   inating Committee by Harvard University's renowned
                                  40 countries by the International Organization for
                                                                                                   corporate governance expert Jay W. Lorsch; and our
                                  Standardization (ISO). This was a tremendous
                                                                                                   Audit Committee by Walter P. Schuetze, former chief
                                  achievement for CA, and it was only the beginning. In
                                                                                                   accountant to the Securities and Exchange Commission.
                                  2003, CA achieved global ISO 9001:2000 certification
                                  for our quality management system used by all CA                 In addition, the Company set term limits for inde-
                                  operations worldwide.                                            pendent directors, adopted strict rules prohibiting the
                                                                                                   repricing of stock options (even though we have never
                                  Our pursuit of "gold-standard" quality is also apparent
                                                                                                   repriced options in our history), and took the lead in
                                  in our continued focus on corporate governance.
                                                                                                   the technology sector by announcing CA would begin
                                  During the year, Robert B. Lamm joined CA as Director            to expense stock options, effective April 1, 2003.
                                  of Corporate Governance, a new position responsible              Finally, by filing our Form 10-Q and Form 10-K reports
                                  for the ongoing development and implementation                   within hours of our earnings releases -- no simple task
                                  of corporate governance programs and policies. Also              -- CA has enhanced the timing of its financial disclo-
                                  during the year, the CA Board of Directors adopted               sures. Not only are we pursuing the gold standard
                                  state-of-the-art corporate governance principles                 in all that we do, in some instances we are clearly
                                  designed to achieve strong Board oversight and align-            defining the gold standard.
                                  ment with shareholder interests. The Board established
                                                                                                   On a separate note, we continue to cooperate fully
                                  a lead independent director position and elected Lewis
                                                                                                   with the previously reported investigations being con-
                                  S. Ranieri, founder of Hyperion Partners, to fill it.
                                                                                                   ducted by the government. We are hopeful that they
                                  Related to Board composition, it is with great sadness           will be resolved in a timely manner.
                                  that I note the passing, in January 2003, of distinguished
                                  Board member Thomas H. Wyman, former Chairman                    D E L I V E R I N G T H E " R I G H T " T EC H N O LO GY
                                                                                                   Looking ahead, we see a shift in the enterprise soft-
                                  and CEO of CBS Inc.
                                                                                                   ware business. For several decades, IT industry growth
                                                                                 truly
                                                             We will never tire of searching for
                                                             innovative solutions to changing
                                                             business demands.




has been technology driven, because each new inno-           In April 2003, CA announced a host of solutions
vation would spur a new round of buying.                     within our Unicenter brand to manage on-demand
                                                                                    



                                                             computing. We will expand our managing on-demand
With customers expecting a return from every IT dollar,
                                                             offerings to include our BrightStor and eTrustTM brands.
                                                                                                     

technology providers can no longer rely on buyers'
                                                             As the market evolves, we plan to apply our core
insatiable demand for the latest thing. New technology
                                                             enterprise management, security and storage strengths
will need to offer real solutions to real business
                                                             to pursue the next phase of on-demand computing.
problems. Successful IT companies will have to truly
understand a customer's business, determine what             In the near term, we continue to develop and deliver
solutions will help that customer operate more effec-        technologies that help customers run their businesses
tively and develop the "right" technologies that enable      effectively today.
the customer to succeed.
                                                             In closing, I want to thank the women and men of CA,
On-demand computing is a case in point. It's a vision        who have worked so hard to ensure our Company's
for computing in which businesses access computing           continued success, and offer a special thanks to our
capacity and power in the same way they do electricity.      leadership team, whose vision and determination are
This approach would solve three significant problems         helping us chart a course to future growth.
for customers -- responsiveness, inefficiency and cost.
                                                             In addition, I want to acknowledge the innumerable
Most businesses today are dealing with multiple plat-        contributions of our founder, Charles B. Wang, who
forms, multiple solutions from multiple vendors and a        retired last fall. We are fortunate to be able to build
host of challenges as they work to integrate legacy sys-     upon a strong foundation that he helped create. We
tems. The goal of on-demand computing is to hide that        will honor his legacy by continuing the rich tradition of
complexity and enable customers to quickly respond           community service in which he strongly believed.
to changing business requirements. Customers will
                                                             2004 is going to be an exciting year for CA and one of
ultimately benefit from systems that have the ability to
                                                             great opportunity. Given the tremendous dynamics of
recognize -- dynamically and automatically -- which
                                                             our industry, we will continue to be as flexible as what
resources are at capacity, scour the enterprise for avail-
                                                             the future will demand of us, in order to build value for
able resources and configure them to handle the task
                                                             our customers, our employees and our shareholders.
at hand. This would happen in a matter of hours -- or
even minutes -- in a reliable and repeatable manner          Thank you for your continued support.
with little chance for error.

We believe the key to on-demand computing is
management software.

Customers don't need to replace their IT infrastruc-
                                                             Sanjay Kumar
tures to benefit from on-demand computing. They
                                                             Chairman and Chief Executive Officer
need a way to better manage their current resources.
CA is well positioned to help them. For a quarter of a
century, we have applied our management capabilities
to meet an ever-escalating demand for integrated
solutions to manage IT needs.