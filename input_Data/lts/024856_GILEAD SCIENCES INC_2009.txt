TO OUR STOCKHOLDERS, EMPLOYES
AND FRIENDS,
The opening pages of this book introduce you to
Matt, Ana and Al. They are three of the nearly 4,000
employees of Gilead Sciences  4,000 individuals
with diverse roles and backgrounds who share a
singular focus
 and that focus is on doing what we can, every
day, to help other individuals around the world
who are living with unmet medical needs.
In 2009, we grew our business substantially, achieving
record revenues of more than $7 billion and
strengthening our product portfolio and pipeline
programs across four primary therapeutic areas.
I believe we were able to realize these achievements
because we view our work as more than a business.
We are a community of people, working in partnership
toward a shared vision.
The diseases we focus on affect tens of millions of
individuals around the world, and they represent
challenges for the physicians who treat them, for
the patients and their families who live with them,
and for our researchers and our collaborators who
work to find new ways to combat them.
Today, more than 33 million individuals around
the world are living with HIV/AIDS. The disease
remains one of Gileads primary areas of commercial
and research and development focus and the
cornerstone of our business. In the United States,
75 percent of HIV patients on treatment are now
receiving one of Gileads products. Our once-daily
fixed-dose tablet Atripla was the most-prescribed
HIV treatment regimen, and Truvada, taken as
part of combination therapy, remained the mostprescribed
HIV product during 2009. In Europe,
Truvada and Atripla became the number one and
two brands, respectively, in Germany, Italy, Spain
and the United Kingdom.
Even with this commercial success, we recognize
there is much more to be done. In the United States,
at least 20 percent of individuals living with HIV 
or approximately 250,000 people  are unaware
they have the disease. During 2009, support continued
to grow for routine testing to help diagnose
individuals living with HIV, prevent late diagnoses
that make the virus harder to treat and prevent
new infections.
In August, the U.S. Department of Veterans Affairs
began offering routine, voluntary HIV testing at
facilities nationwide. The October extension of the
Ryan White CARE Act established  for the first time 
a national goal to screen five million people each
year. These developments will help make HIV tests
and ultimately treatment more accessible. Gilead
is actively supporting programs to diagnose and link
to care individuals living with HIV in the hardest
hit and often most underserved communities. And
we are looking at all facets of HIV disease, including
prevention. Gilead is supporting ongoing trials
around the world to evaluate whether taking our
antiretroviral medicines could help protect at-risk
individuals from becoming infected with HIV, a
potentially groundbreaking HIV prevention strategy
called pre-exposure prophylaxis, or PrEP. The first
efficacy results for PrEP are expected in 2010.
Like HIV, chronic hepatitis B is a disease where many
individuals are not aware they are living with the
virus. Left untreated, chronic hepatitis B can lead to
cirrhosis and liver cancer. We are pursuing efforts
to expand access to information and care in communities
most affected by chronic hepatitis B  a
disease with a disproportionate impact on individuals
of Asian descent. We have demonstrated,
through long-term clinical trials, the potential of
Viread to lower viral load and thus decrease
damage to the liver. Viread is now recommended
as a first-line treatment for HBV in guidelines
issued by U.S. and European medical societies, and
continues to gain share in these markets as one
of the most frequently prescribed oral antivirals for
patients with chronic hepatitis B.
On the cardiovascular front, we expanded our portfolio
with the acquisition of CV Therapeutics in
April, which brought us Ranexa for the treatment
of chronic angina. We continued to drive uptake
of Letairis for pulmonary arterial hypertension
(PAH), a disease that affects approximately 200,000
patients worldwide. We also broadened our efforts
to support Phase IV studies of Letairis, and are
collaborating with GlaxoSmithKline on an international,
event-driven clinical trial, called AMBITION,
to study combination therapy versus monotherapy
in a first-line treatment setting for PAH.
In addition, we received conditional approval of
Cayston for the treatment of lung infections due to
Pseudomonas aeruginosa in patients with cystic
fibrosis (CF) in the European Union and Canada. Data
from our head-to-head study of Cayston versus
tobramycin, which would support full approval in
these territories, is expected toward the middle
part of this year. In early 2010, Cayston received approval
in the United States and Australia. Cayston
represents the first new inhaled antibiotic approved
for patients with CF in more than a decade.
Our first therapy in the category of respiratory disease
 Tamiflu  proved to be an important component
in the global fight against the H1N1 strain
of influenza during 2009. Tamiflu was developed by
Gilead and licensed to F. Hoffmann-La Roche, our
global commercialization partner.
Beyond our commercial products, we made significant
progress with our pipeline in 2009. Across
our research and development programs, hundreds
of Gilead scientists are working to create and develop
new compounds for the treatment of HIV,
liver disease and serious cardiovascular and respiratory
conditions.
For HIV, we have several compounds in late-stage
clinical trials and new combination treatments
on the horizon. In collaboration with Tibotec Pharmaceuticals,
we are developing a new once-daily
single-tablet regimen combining Truvada with
Tibotecs non-nucleoside reverse transcriptase inhibitor
candidate, rilpivirine. If approved, this product
would be the first alternative to Atripla with the
potential to offer an improved side effect profile.
We hope to file applications for regulatory approval
of this product in the second half of 2010. We are
also excited about a development-stage product that
combines our investigational HIV integrase inhibitor,
elvitegravir, and cobicistat (formerly GS 9350), our
investigational pharmacoenhancing or boosting
agent, with Truvada, creating a new once-daily,
fixed-dose regimen, known informally as the Quad.
Phase II studies of the Quad and of cobicistat as
a stand-alone agent have been completed, and
pending discussions with the U.S. Food and Drug
Administration, our goal is to initiate Phase III
studies by the middle of this year.
We continue to develop treatments for hepatitis C
and nonalcoholic steatohepatitis (NASH), including
our caspase inhibitor GS 9450, now being evaluated
in Phase II studies.
With the additional expertise and resources gained
from the CV Therapeutics acquisition, we have
greatly improved our ability to advance research
and development of treatments for serious cardiovascular
conditions with unmet needs. We are
evaluating the potential for Ranexa and next-generation
late sodium current inhibitors in a number
of additional indications and settings, including
for the treatment of patients with diastolic heart
failure. And, GS 9667, a partial A1 adenosine receptor
agonist, is being evaluated as a potential
treatment for diabetes.
The potential of our products  those available today
and the next generation in development  can and
will only be realized if patients have access to those
therapies.
First and foremost, we believe responsible pricing
is the best way to ensure broad access. We also
recognize that many patients face challenges affording
prescription medications. Through Advancing
Access, Atripla Patient Assistance, GileadSolutions
and the Cayston Access Program, we assist patients
in the United States who do not have insurance or
who need additional financial help to pay for our
medicines. We also assist patients who have insurance
coverage for prescription medications but are
unable to bear the full cost of their co-payments.
Worldwide, more than 1 million individuals received
one of Gileads four HIV medications during 2009.
More than half of these patients live in resourcelimited
parts of the world. To reach these patients,
we have partnered with 13 Indian generic companies
to develop generic versions of our HIV medications
and distribute them in 95 countries, including India.
Gilead provides charitable contributions to a broad
range of non-profit organizations. Through the
Gilead Foundation, we also support efforts worldwide
to expand access to disease education and
improve infrastructure and health services.
The Gilead team is a diverse group of people,
working in laboratories, offices and communities
around the world. We benefit from the guidance
and leadership of our Board of Directors, and in
2009 we welcomed Kevin Lofton, President and
CEO of Catholic Health Initiatives, and, in 2010,
Per Wold-Olsen, Retired President of the Human
Health Intercontinental Division of Merck & Co.,
to the Board.
To each of you  our stockholders, our employees
and our friends  I thank you for your continued
support and belief in our mission. You are a part
of the Gilead community  one built over the past
20 years and one from which we will continue to
grow and bring new medications to those in need.
Sincerely,
John C. Martin, PhD
Chairman and Chief Executive Officer