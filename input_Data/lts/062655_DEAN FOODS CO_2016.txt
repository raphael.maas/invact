Dear Fellow Stockholders:
There�s only one thing that excites me more than where Dean Foods is today, and that�s where
we�re headed. This dynamic company has many opportunities ahead of it, and I�m humbled and
excited to be the one privileged to lead Dean Foods into the future.
STRONG PROGRESS IN 2016 TO EXPAND INTO 2017
No one can deny our momentum in 2016. We ended the year with our stock trading near its 52-week high while
we continued to return dividends to our shareholders. We invested in the business and took important steps to
support our future success. In 2016, we:
NOW, AS WE MOVE INTO 2017, WE WILL BUILD ON THESE
SUCCESSES. IN 2017, YOU WILL SEE:
� Expansion of the DairyPure line. Our new clean-label sour
cream line was created to beat any competitor in taste and
texture and began to launch nationwide in January. DairyPure
sour cream represents another critical step for us as we venture
"6,+!?2&!*&)(?
� Continued category interruption with TruMoo. Meeting
the demands of consumers, TruMoo now features No GMO
Ingredients. New choices in our ingredients give parents and
school nutrition directors further peace of mind and another
good reason for choosing TruMoo.
� Renewed focus on ice cream. Buoyed by the Friendly�s
acquisition, we�re reimagining our ice cream business by
enhancing our recipes, improving our packaging, and introducing
"5 &1&+$+"4-/,!2 10+!?3,/0?"4&)))0,"5-+!1%"/" %
,#,2/"+?0+!6?")!/"*"/6)&+"01,/&+$1%"0""),3"!
brands to new areas of the country.
Invested in the
DairyPure� and
TruMoo� brands.
We supported our
power brands with
category-leading
advertising and built
strategic marketing
partnerships with the
Olympics, DreamWorks
Pictures and Oreo
cookies to engage
our consumers
and customers.
Launched new
products.
DairyPure Lactose
Free and Caribou�
& "! ,?""4"/"
two new entries into
the dairy case. Our
lactose-free milk marks
1%"?/01"51"+0&,+
of the DairyPure
brand into adjacent
and complementary
categories while
the Caribou launch
enabled us to compete
in the fast-growing iced
 ,?"" 1"$,/6?
Acquired Friendly�s�
Ice Cream.
As planned, the
acquisition of the
leading Friendly�s
brand grew our top
line and proved to be
accretive to earnings
while providing a solid
platform for growth in
a strategic category.
Developed a joint
venture with
Organic Valley�.
Known as �Organic
Valley Fresh�, � the joint
venture leverages the
strength of the Organic
Valley brand and its
high-quality organic
*&)(4&1%1%"0&$+&? +1
selling, manufacturing
and distribution
capabilities of
Dean Foods.
Continued our
strong Food
Service momentum.
This is a growing
channel in the food
and beverage industry
that is now nearly the
same size as retail.
We are among the
biggest suppliers to the
industry�s top players
and delivered solid
top-line growth in 2016.

PROGRESS ON THE DEAN FOODS STRATEGIC PLAN
Our team created a three-year strategic plan to set the direction for the
successful future of the company. It is a comprehensive strategy that
balances a solid growth agenda with a strong emphasis on productivity. Of
course, the primary objective is to deliver shareholder value as well as help
Dean Foods become more resistant to extreme commodity increases. We
put the plan in motion in 2016, and the successes I highlighted fall under
,+",#?3"01/1"$& -&))/0?"4,/("!%/!&+UUuU1,Build & Buy Strong
Brands as evidenced by our progress on DairyPure and TruMoo as well as
our acquisition of Friendly�s ice cream. We were able to Strengthen Our
Private Label Business with some key customer wins. Our relentless focus
on the Drive Operational Excellence pillar of our strategy continues to be
a top priority and provides the fuel to drive our plan. We made progress on
the Enhance Our Future Capabilities pillar with the addition of several
highly-skilled people, additional training for our leadership team and
investment in important technology systems. We were able to take initial
steps to Transform Go To Market with the addition of new manufacturing
and warehouse distribution capabilities. 

CONCLUSION
I see a bright future ahead for Dean Foods. We have
experienced leaders and hardworking local teams in place
to drive a strong strategic plan that is the perfect road map
to a successful future. Our products are on trend in today�s
rapidly evolving food culture and we really do have what
1,!6?0 ,+02*"/0/"),,(&+$#,/?%")1%6,?"/&+$01%1/"
locally sourced with uncluttered ingredient lines and clean
labels with ingredients that they can read and understand.
We have a powerful direct-to-store delivery system that
is second to none in the refrigerated space. Our plant
network enables us to be close to the farm and close to
the consumer so we can bring the freshest, �farm to table�
products to our consumers and retailers shelves every day.
And retailers and foodservice customers realize better
today than ever before that we can help them unlock the
power of Dean Foods to reach their full potential.
Thank you for your continued support and I look forward to
2017 and beyond.
Sincerely,
Ralph P. Scozzafava