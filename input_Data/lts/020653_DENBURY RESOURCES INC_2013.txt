Dear Fellow Shareholders:

During 2013, we started the transition of our
company from one focused purely on growth to one
that supplements both Growth & Income (dividends),
the theme of this years annual report. We are able
to do this because of the unique production and cash
flow profile of our assets, which are all either current
carbon dioxide enhanced oil recovery (CO2 EOR)
projects, future CO2 EOR projects or assets that
produce much of the CO2 that we use in our projects.
We anticipate that our unique capability among
oil and gas independents will enhance shareholder
value and returns in the coming years.
In preparation for this transition to Growth & Income,
over the last few years we made a series of tax efficient
acquisitions and dispositions that sharpened our
operational focus and made us a pure CO2 EOR play.

Since CO2 EOR is limited to areas with large CO2
quantities, our ownership of significant CO2 resources
and pipeline infrastructure needed to transport CO2
gives us a significant competitive advantage in the areas
in which we operate. We have chosen to utilize and
maximize our strategic advantage, and therefore have
made CO2 EOR our core strategy and business.
To enable the expansion of our strategy from
growth to Growth & Income, we modified our future
development plans and flattened out our anticipated
annual capital spending levels for the remainder of
the decade. This adjustment, combined with our view
that these changes would not significantly reduce our
anticipated oil and gas production and reserve growth
rates, allowed us to bring forward our free cash flow
by a few years. This, in turn, allowed us to accelerate
our objective of providing returns to our shareholders
through cash dividend payments.

With the declaration of our first-ever cash
dividend on January 28, 2014, we began the process
of distributing free cash flow generated from our
operations to shareholders. Our first quarterly
dividend of $0.0625 per common share, or a rate of
$0.25 per share on an annualized basis, was paid to
stockholders on March 25, 2014. Based on our current
financial projections and commodity price outlook,
we expect to grow our regular annual dividend rate to
between $0.50 per share and $0.60 per share in 2015 and
at a sustainable rate thereafter.
Of course, the expansion of our shareholder
return strategy to include both Growth & Income
is made possible by the many accomplishments of
our operations team over the last few years. Let me
touch on a few of these achievements over the past
twelve months:
 We delivered average production of 70,243 barrels
of oil equivalent per day in 2013, which was just
slightly above the mid-point of the estimated
range we presented in the prior year. Our tertiary
oil production increased by 9% between 2012 and
2013. Our non-tertiary production, after the closing
of our Cedar Creek Anticline acquisition in March
of 2013, was down only modestly from levels prior
to our Bakken area asset sale and exchange in late
2012. Our tertiary production growth in 2013 was
driven by our newest CO2 floods at Oyster Bayou
and Hastings fields in the Houston area, and we
anticipate additional growth at both of these
fields in 2014. Going forward, we expect to deliver
4% to 8% annual production growth through the
end of this decade without needing to acquire any
additional properties.

We delivered our first tertiary oil production and
proved reserves in the Rocky Mountain region.
Since establishing our position in the Rocky
Mountain region in 2010, our team has worked
diligently to initiate our first CO2 flood in the
region. The milestones we have attained since late
2012 include: completion of the 20-inch Greencore
Pipeline in Wyoming, our first CO2 pipeline in the
Rocky Mountain region; the first receipt, delivery,
and injection of CO2 into Bell Creek and Grieve
fields; the first tertiary oil production at Bell Creek
Field; and the completion of an interconnect
between a third partys CO2 pipeline and our
Greencore Pipeline, which allows us to transport
our CO2 volumes from ExxonMobils Shute Creek gas
processing plant to Bell Creek Field. With tertiary
production now established and growing in the
Rocky Mountain region, we look forward to the
continued expansion of our tertiary operations in
the region, at both Bell Creek Field and Grieve Field.

We increased our proved oil and natural gas
reserves to 468 million barrels of oil equivalent
(MMBOE) as of December 31, 2013, compared to
409 MMBOE at December 31, 2012. We added 85
MMBOE of estimated proved reserves during 2013,
including tertiary reserves of 34 MMBOE at Bell
Creek Field during the fourth quarter, non-tertiary
reserves of 42 MMBOE from the acquisition of
additional interests in CCA during the first quarter,
and 9 MMBOE of other additions or revisions. We
estimate our total proved and potential reserves
at December 31, 2013, were 1,250 MMBOE, including
an estimated 910 MMBOE associated with the
planned future CO2 EOR development of fields
we currently operate. We plan to convert these
potential reserves to proved reserves as we develop
these oil fields.
 We placed our Riley Ridge gas processing facility
into service. We acquired our initial interest in
the Riley Ridge gas processing facility and the
LaBarge Field in southwestern Wyoming in 2010
with the goal of making it our Jackson Dome of
the Rockies. LaBarge Field is estimated to hold
significantly more CO2 than Jackson Dome, but it
is mixed with other gases, including methane and
helium. With the startup of the plant and our sales
of both methane and helium, we will generate cash
flow on our investment, although the bigger prize
will be realized later this decade when we add CO2
separation equipment, connect this plant to our
existing CO2 pipelines, and make Riley Ridge our
anchor source of CO2 in the Rocky Mountain region.
 We purchased and used man-made CO2 in our
operations. Starting in late 2012, we began
purchasing and utilizing anthropogenic (manmade)
CO2 in our tertiary operations. In the Gulf
Coast region, we are currently receiving CO2 from
two plants and anticipate, adding a third source in
2014. We expect the new facility to be our largest
man-made source in the region, with other sources
expected throughout the remainder of this decade.
These projects illustrate our unique ability to use
and store captured CO2 that would otherwise be
released into the atmosphere.
While we had many accomplishments in 2013, we
did face several challenges, particularly at Delhi Field
in northern Louisiana. In June, a release of well fluids,
consisting of a mixture of carbon dioxide, saltwater,
natural gas and oil, was discovered and reported within
Delhi Field. We immediately took remedial action to
stop the release and contain and recover well fluids in
the affected area. We have determined that the release
originated from one or more wells in the affected
area of the field that we believe had previously been
improperly plugged and abandoned by a prior operator
of the field. While we completed our remediation
efforts during the fourth quarter of 2013, the halting of
CO2 injections into the directly impacted area reduced
the fields oil production and required significant
corporate resources. We have taken numerous steps to
mitigate the risk of something similar occurring in the
future, including a more thorough review of plugged
and abandoned wells, more stringent criteria for what
is an acceptable plugged and abandoned well, and
assignment of additional, dedicated staff and capital

resources to administer this program. I am confident
that the lessons learned and applied from the incident
will make Denbury a better company in the future.
On the financial front, we generated $1.36 billion
of cash flow from operations, more than enough to
fund the $1.14 billion we spent on oil and natural
gas development, CO2 supply, pipelines, and plant
capital expenditures. The excess cash flow was used to
partially fund our common stock repurchase program,
which our Board first authorized in 2011. We believe
our stock has been undervalued, even today, trading
below the net asset value of our proved oil and natural
gas reserves and at levels that completely ignore the
significant incremental value of our potential CO2 EOR
reserves. We have spent over $940 million through the
first quarter of 2014 to repurchase approximately
15% of the shares we had outstanding when we
initiated the program in 2011. Weve repurchased

between 3.5% and 4.2% of shares outstanding every
year starting in 2011 and have repurchased over
3% thus far in 2014, all while maintaining a solid
capital structure. The repurchases have improved
our per-share metrics and have been completed
at attractive prices that we believe make them very
accretive for our shareholders. Our repurchase
program remains in place with approximately
$220 million still authorized as of the date of this letter.
We intend to be opportunistic with this program.
In summary, it has been another eventful and
productive year at Denbury. We remain focused
on increasing shareholder value by optimizing the
development of our attractive asset base. We are aware
that we have had unfavorable operating and capital
cost trends in 2013 and recent years, and as a result are
implementing several internal initiatives that we expect
will result in meaningful cost reductions in the future.
We believe that we can make significant improvements
in our cost structure and reverse the recent negative
trends. We have excellent visibility on long-term oil

growth in our two core regions, our solid
balance sheet provides us tremendous financial
flexibility, and our workforce of highly technical,
dedicated, and motivated employees is focused on
executing our unique strategy.
We strongly believe in our strategy and its long-term
economic benefits and are committed to creating
value for our shareholders through a combination of
production and proved reserve growth, dividends and
share repurchases. We look forward to executing our
value-driven strategy in 2014 and beyond.

Sincerely,
Phil Rykhoek
President and Chief Executive Officer
March 28, 2014