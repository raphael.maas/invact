To Our Shareholders:
2017 was another excellent year for CBRE, marked by strong financial results and strategic gains across
our business. For the year:
 Revenue rose 9% to $14.2 billion, while fee revenue1 increased 8% to $9.4 billion.
 Adjusted EPS1 increased 18% to $2.71.
 Adjusted EBITDA1 rose 10% to more than $1.7 billion.
We have now recorded eight consecutive years of double-digit growth in adjusted EPS, and revenue and
adjusted EBITDA reached all-time highs. Our 18.2% adjusted EBITDA margin on fee revenue was above
the 17.5% to 18% target we had established at the beginning of the year.
As these results attest, we are keenly focused on capitalizing on enduring trends that support the longterm growth of our industry. These include the growing acceptance of outsourced commercial real estate
services, the increasing capital allocation to commercial real estate as an institutional asset class, and the
continuing consolidation of activity within our industry to the highest-quality globally diversified firms,
such as CBRE. In addition, technology holds more upside than downside for our sector, particularly for
firms with the wherewithal to invest wisely in digital capabilities.
In 2017, we made important strategic gains that will help to advance our ability to produce
differentiated client outcomes that other firms find increasingly difficult to replicate.
 We have focused considerable effort on driving market share gains by building the global
capabilities of our capital markets business and improving the connectivity between our outsourcing
and leasing businesses. This enabled our property sales and leasing businesses to meaningfully
outperform the broader market in 2017.
 In occupier outsourcing, our enhanced capabilities to self-perform high-quality technical services
around the world is a key differentiator that drove another year of double-digit revenue growth for
this business.
 In data and technology, we added significantly to our digital talent base through recruitment and
M&A. We have developed and are executing digital roadmaps for each of our lines of business
allowing them to introduce commercially-focused technologies that are enhancing client outcomes.
One example is our recently announced workplace experience service  CBRE 360  which is
supported by a variety of digital tools to meet the rapidly rising demand for experience-enhancing
occupancy strategies that boost employee morale and productivity.
 Through our Client Care program, we are gaining increased insight into the outcomes we are
producing for our clients. This insight, in turn, is helping us to improve those client outcomes,
resulting in higher client satisfaction and deepening our relationships with our largest clients. In
2012, we had one $100 million (revenue) client. By 2017, we had 17 such clients.
 We continued to execute a highly targeted M&A strategy with a focus on enhancing capabilities
rather than adding scale. In 2017, we made 11 acquisitions driven by close collaboration between
our Corporate Development team and our geographic and business line leaders around the world.
These acquisitions included companies operating in investment management, project management,
retail experience advisory services, occupier brokerage focused on major technology companies, as
well as two real estate software as a service companies.
 We added significantly to our talent base, with one of our best years for producer recruiting, and
strengthened the leadership team that is responsible for driving our continued growth.

 Creating a work environment where people of all backgrounds can thrive remains near the top of
our agenda. While we continue to have work to do in this area, our progress  particularly in
adding women to key leadership roles and to our Board  was recognized by both Forbes and
Fortune, which included CBRE among the top companies for diversity and inclusion.
 Similarly, our continued strong focus on mitigating the environmental impact in our own operations
and at our client properties earned us a place on the Dow Jones Sustainability Index for the fourth
straight year.
 Finally, we ended 2017 in the strongest financial position in the companys history. Our low
leverage, high liquidity and considerable cash flow position us to make further thoughtful investments
to that build on our market-leading position in our sector.
Credit for our performance in 2017 goes to our more than 80,000 people around the world. Their
determination to deliver great outcomes for our clients helps to drive the kind of robust performance you
have seen from CBRE consistently over the past several years.
We are indebted to you, our shareholders, for supporting us as we pursue and execute our strategy.
Everyone at CBRE works hard every day to earn and keep the confidence you place in us.
Sincerely,
Robert E. Sulentic
President & Chief Executive Officer
CBRE Group, Inc.