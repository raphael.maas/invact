Fellow Shareowners:
A
                                                                                                    which we completed and surpassed last
           few years ago, we raised the bar
                                                                                                    year, was even more challenging. The
           on Alcoa's stated Vision of being
                                                                                                    new goal will be equally tough, but we
            the best aluminum company
                                                                                                    have the talent, a proven Alcoa Business
in the world to that of aspiring to be the
                                                                                                    System (ABS), and the determination to
best company in the world. While we
                                                                                                    reach it. Upon its completion, we will
have much to be proud of as we enter a
                                                                                                    have eliminated more than $3.3 billion
new year, we know that we have more
                                                                                                    in costs, which will help offset the impact
to do... and that this goal is about more
                                                                                                    of tough global conditions we have
than delivering strong financial perform-
                                                                                                    experienced over the last three years and
ance. It is about sustained financial suc-
                                                                                                    benefit us when markets resume their
cess, while building for the future. And
                                                                                                    growth patterns.
it is about delivering on all of Alcoa's
                                                                                                         While we have made strides toward
seven Values.
                                                                                                    our ROC goal of permanent member-
      Despite what was a challenging
                                                                                                    ship in the first quintile of the S&P
business environment in many markets
                                                                                                    Industrials, we still have a long way to
across the globe, we did make significant
                                                                                                    go. Our fourth quarter 2003 ROC on
strides in 2003:
                                                                                                    an annualized basis was 7.6% versus
 Income from continuing operations
                                                                                                    a company ROC of 4.2% in 2002. The
   rose 117% to $1.034 billion, or $1.20
                                                 first quintile entry point, however, is
   per share, in 2003, and every segment
                                                                                                    currently around 16%.
   demonstrated higher profitability;
                                                                               This goal is not about the honor of membership in the first
 Our disciplined approach to capital allowed us to pay down
                                                                          quintile. It is about earning the right from shareowners, through
   more than $1 billion in debt, providing us with additional flexi-
                                                                          our performance and consistency, to continue to profitably
   bility in pursuing profitable growth opportunities;
                                                                          grow. It will provide us the opportunity to plan for the future,
 We surpassed our second three-year, cost-savings challenge by
                                                                          continue to offer challenging jobs, and pursue growth opportu-
   reaching $1.012 billion of annualized savings (the first, in
                                                                          nities for shareowners, for Alcoans, and other stakeholders.
   1998-2000, achieved $1.1 billion in savings), which helped
                                                                               All our efforts are based upon two precepts: deliver short-
   drive gross margins higher;
                                                                          term performance while positioning the company to be successful
 Revenues increased 6% to $21.5 billion; and
                                                                          for years to come. The steps we are implementing are improving
 We recorded the lowest lost workday rate in the Company's
                                                                          both short-term and long-term profitability... and laying the
   history.
                                                                          foundation for our continued leadership position for generations
      Reflecting this performance, in part, Alcoa's total share-
                                                                          to come. We are:
owner return for 2003, including dividend reinvestment, was
                                                                           Continuing to refine our portfolio of businesses;
70%. For comparative purposes, the Dow Jones Industrial
                                                                           Strengthening our asset base and improving its productivity;
Average, of which we are a component, returned 25% during
                                                                           Extending our global reach and repositioning our primary
the same period.
                                                                            businesses lower on the cost curve;
                                                                           Strengthening our connection to customers; and
Goals and Strategies for 2004 and Beyond
                                                                           Building on the transformation of our businesses  from
In 2004, we are committed to our financial goals:
                                                                            making products to delivering solutions... working across
1. Profitable Growth  We will continue the drive to profitably
                                                                            businesses in order to help our customers be successful within
    grow our revenues as well as to join the first quintile of the S&P
                                                                            their markets.
    Industrials measured in terms of Return on Capital (ROC).
2. Cost Savings  Financial fitness is key to our future. In sup-
                                                                         Portfolio Reconfiguration
   port of our profitable growth challenge, we have launched our
                                                                         Last year, we announced a program to divest a number of busi-
   third three-year, cost-savings challenge to eliminate an addi-
                                                                         nesses that either did not have the ability to grow in excess of
   tional $1.2 billion in costs by the end of 2006. When we set
                                                                         GDP or did not have the ability to deliver superior returns in sec-
   our first billion-dollar, cost-savings goal in 1998, it was diffi-
                                                                         tors where Alcoa maintains a sustainable competitive advantage.
   cult, but we achieved it. The second billion-dollar challenge,
                                                                                                                                             
     We have made solid progress on this program, as we have                                 fast-growing region. We are also installing a new foil production
sold the Latin America PET bottle business, our equity interest in                           line at Alcoa Shanghai Aluminum.
Latasa, a can producer based primarily in Brazil, and the packag-                                   In Canada, we are working with the government of Qubec
ing equipment business. We have announced the sale of the                                    to upgrade and expand our hydropowered smelting operations in
specialty chemicals business and expect to complete it in the first                          Baie-Comeau and Deschambault.
quarter, with the remainder of this divestiture program completed                                   In Brazil, in addition to restructuring our long-term partner-
by the end of the first half of 2004. In total, the program should                           ship with the Camargo Correa Group, we are engaged in several
generate proceeds in excess of $750 million,                                                                              hydropower projects that will increase our
which will be used to pay down debt.                                                                                      energy self-sufficiency and cost-competi-
                                                       
                                                       
     We added to our portfolio this year                                                                                  tiveness while meeting Alcoa and Brazilian
                                                       
with the acquisition of the Camargo                                                                                       standards for socially and environmentally
Correa Group's 40.9% interest in Alcoa's                                                                                  sound development. For example, in the
                                                    
South American operations, a position                                                                                     case of the Barra Grande hydropower facil-
                                                    
these partners held since 1984. The largest                                                                               ity, construction has begun and is expected
                                                    
subsidiary in these operations is Alcoa                                                                                   to be completed in 2006, with total
Aluminio, headquartered in So Paulo.                                                                                     installed capacity of 690 MW. Upon the
                                                    
Aluminio owns and operates mining,                                                                                        completion of this project, coupled with
                                                    
refining, power generation, smelting, and                                                                                 other hydropower investments we have
                                                         
aluminum fabrication facilities at various                                                                                made, we will provide more than 40% of
                                                           
locations in Brazil. Additional businesses                                                                                the energy required for our two smelters in
                                                            
include closures and flexible packaging.                                                                                  that country. And we have an additional
     We also expanded our aluminum                                                                                        355 MW under concession.
                                                      
                                                     
alliance with Kobe Steel Ltd. in Japan on                                                                                       During 2003, we signed a Memoran-
                                                      
the joint development of aluminum prod-                                                                                   dum of Understanding (MOU) with the
ucts for the automotive market. As part of this arrangement,                                 government of the Kingdom of Bahrain that paves the way for
we assumed complete control of aluminum rolling operations in                                Alcoa to acquire up to a 26% equity stake in Alba, a Bahrain com-
Australia, used in making beverage containers, foil, and sheet.                              pany that owns and operates a 512,000-mtpy aluminum smelter.
                                                                                             The MOU also covers a long-term alumina supply arrangement
Strengthening and Growing                                                                    for Alba. Alba is currently adding a fifth potline which, when
In 2003, we made significant strides in strengthening our asset                              completed in 2005, will bring its overall capacity to 819,000 mtpy
base in both our primary metals and alumina businesses,                                      and make it the largest prebake aluminum smelter in the world.
accelerating movement down the global cost curve, and laying                                 The MOU is designed to accelerate plans for an additional expan-
the foundation for our continued leadership position in the                                  sion, a sixth line with 307,000 mtpy of additional capacity.
industry.                                                                                                                       In addition, we have announced plans
     The year began with the finalization                                                                                 to conduct a feasibility study over the next
                                                     Surpassed Second $1 Billion
of agreements to build a new, state-of-the-                                                                               two years to explore building a smelter in
                                                     Cost-Savings Goal
art, greenfield smelter in Iceland. We plan to                                                                            Brunei Darussalam.
                                                     Quarterly Run Rate in Millions of Dollars
break ground on this 322,000-metric-tons-                                                                                       Alcoa World Alumina and Chemicals
per-year (mtpy) smelter  to be among                                                                                     (AWAC)  a global alliance between
                                                     Continue to                                                   
the most efficient in the world in terms of                                                                               Alcoa and Alumina Ltd, with Alcoa
                                                     reduce costs 
                                                                                                      
production and sustainability  in 2005                                                                                   holding 60%  completed the 250,000-
                                                     eliminate $1.2B
and expect production to begin in 2007.                                                                                   mtpy construction expansion of its
                                                     in costs on a
                                                     run-rate basis
     Our joint venture with the Aluminum                                                                                  Jamalco alumina refinery in Clarendon,
                                                                                           
                                                     by the end
Corporation of China, Ltd. (Chalco), at                                                                                   Jamaica ahead of schedule. AWAC also
                                                     of 2006
Pingguo, has been delayed pending final                                                                                   broke ground on a 250,000-mtpy alumina
government approvals. We expect agree-                                                                                    expansion at its Paranam alumina refinery
                                                                                      
                                                                                       
ments in this regard to be formalized in                                                                                  in Suriname. And we have begun an effi-
                                                                                                 

2004. This joint venture will enhance our                                                                                 ciency upgrade at AWAC's Pinjarra alumi-
position in the fastest-growing market in the                                                                             na refinery in Western Australia that will
aluminum industry within one of the most efficient  and grow-                               increase capacity there by 600,000 mtpy.
ing  alumina and aluminum production facilities in China.                                          As part of our strategy, we continue to explore promising
     Elsewhere in China, we are working toward completing a                                  opportunities across the world, including some in Russia. These
joint venture with China International Trust & Investment, our                               efforts are designed to lower Alcoa's costs in primary metals  in
equity partner in Bohai Aluminum, with the objective of                                      many cases replacing older, less competitive capacity with newer,
expanding soft-alloy extrusions and foil opportunities in this                               more efficient operations, while in other cases we are adding


capacity in order to take advantage of our already efficient opera-                          workday incident  and 36% of them did not have a single
tions. We will continue to expand our global footprint and build                             recordable injury. Regrettably, despite this performance, we
on the successful foundation of our upstream businesses.                                     experienced four fatalities. That is why we are increasing our
    In the consumer packaging and semi-manufactured areas we                                 emphasis on identifying difficult-to-predict, low-probability
concentrated on quickly integrating businesses we acquired last                              events that have potential for catastrophic consequences, and
year  such as Ivex Packaging into our packaging businesses and                              why we are analyzing the causes of human error and how to
Fairchild Fasteners into Alcoa Fastening Systems.                                            prevent them... as we strengthen our resolve toward achieving
                                                                                             the ultimate goal  zero incidents.
Strengthening Our Connection to Customers                                                            On the environmental front, our results were numerous...
Through Solutions                                                                            ranging from landfill waste reductions, to the use of less water,
In 2003, we continued to invest in productivity improvements                                 to the completion of our goal to plant one million trees five
in our Company through the disciplined and systematic deploy-                                years ahead of schedule, and the launching of a new program
ment of the Alcoa Business System, which begins with the                                     to begin planting ten million new trees by 2020, which can
customer. Through this process we created a clearer picture of                               absorb more than 250,000 metric tons of carbon dioxide per
what it means to be a customer of Alcoa. As you'll see in                                    year during their lifetime.
the following pages of this report, our Market Sector Lead                                           Perhaps most exciting is the attainment of our 25% reduction
Teams (MSLTs)  an extension of ABS  made substantial                                       in greenhouse-gas emissions from 1990 levels. When we defined
progress toward building a coordinated approach to markets                                   this goal in 2000, we sought to achieve this level of reduction by
and customers.                                                                               2010, but we have eliminated 13 million tons of greenhouse gases
       These Lead Teams work with Alcoa's business units to                                  per year, so we reached the goal six years ahead of schedule.
improve the response to customer needs                                                                                             In 2003, our connection to the
that span our organization and migrate                                                                                        communities in which we operate contin-
                                                     
                                                     
our value proposition from materials and                                                                                      ued to strengthen. Our sponsorship of
components to high-value, customer-                                                                                           the Smithsonian's National Air and Space
                                                     
centric engineered solutions.                                                                                                 Museum's centennial of flight exhibition on
                                                                                               
                                                           
       This means we apply technologies that                                                                                  the Wright Brothers was broadened beyond
                                                                 
                                                                                               
enable a customer to meet specific benefits                                                                                  Washington, DC, to include activities in
convenience, economy, safety, speed, etc.                                                                                    many of our aerospace communities. In
                                                                                 
                                                                             
for their end users. This elevates our value                                                                                  addition, our social commitment continued
                                                                                          
                                                                                                
to the customer well beyond the commodi-                                                                                      to expand beyond grant making. Employee
                                                                                                         
                                                                                                     
                                                                                                                      
ty and materials levels. For our customers,                                                                                   volunteerism grew to almost 200,000 hours
this is a huge benefit. For Alcoa, our chal-                                                                                  in 2003, including the capstone to the Alcoa
                                                    
lenge is twofold: first, we must understand                                                                                   Foundation's 50th anniversary celebration 
                                                         
the issues facing our customers; and then,                                                                                    a worldwide week of service focused on
working with our Research and                                                                                                 conservation and sustainability projects.
                                                     
                                                    
Development and application engineering
                                                                                             Our Vision is Challenging, Our Goals High,
staffs, as well as other businesses selling in the same market,
                                                                                             Our Strategy Clear
develop solutions and demonstrate their benefits to our customers.
                                                                                             While I am proud of all that our employees have achieved in
Safety, Sustainability, and Community                                                        2003, I am most proud that they share a focus on our Values,
At Alcoa, we begin business reviews with an update of our safety                             delivering annual results, and building for the future.
performance. We do this for two reasons. First, we take safety seri-                                 Our goals remain high, our strategy is clear. And everyday
ously, and we are proud of our progress. The second reason is to                             we seek to strengthen your Company, improve its agility, and
demonstrate to everyone what is possible through implementing a                              improve the bonds with our customers, while living our Values.
disciplined process. When we first set our sights at being the safest                        This focus has served us well in the past and has positioned
company in the world, many thought it was unrealistic. Whether it                            us for success in 2004 and beyond... as we strive to become the
is a financial goal (such as our ROC or cost-savings initiatives), an                        best company in the world.
environmental goal to reduce emissions, or the goal to have zero
safety incidents, the bottom line is when Alcoans put their minds
and talents against a given goal, we almost always achieve it.
       In Safety, we had our lowest lost workday and total record-
able injury rates in the Company's history. Our lost workday rate                            Alain J. P. Belda
improved to 0.12 from 0.15 in the previous year, and our total                               Chairman and Chief Executive Officer
recordable rate is 1.66, down from 2.22 in 2002. Approximately                               March 1, 2004
77% of our locations operated in 2003 without a single lost

                                                                                                                                                                      
