To Our Stockholders Electronics, that have been pervasive in our individual consumer lives, are now evolving to dominance in the industrial and automotive end-markets. This is great for Linear. We are now in a new electronics age in these markets. Industrial output that was a cottage industry in the 1800s evolved to the industrial revolution with the concentration of human labor in factories. Mechanization overtook many of the tasks previously done by workers and now this same mechanization is giving way to electronics implementation such as wireless transmission of sensor measurements, electronically activated valves, digital x-ray machines and all forms of robotics. In addition, smart manufacturing and energy efficiency are becoming prevalent in modern industrial factories.
This new dominance of electronics is even more evident in automobiles. Previous mechanical operations such as braking and steering can now be performed electronically. And valuable safety features such as collision avoidance, lane departure and parking assistance are now a reality. Stored, alternatively sourced energy now assists automotive acceleration, even in race cars. Soon battery efficiency will be more discussed than horsepower!
This new age of electronics in industrial and automotive markets comes with very high requirements for performance, quality, reliability and repeatability. Much of the electronics is analog, as signal clarity and power efficiency are as critical as digital processing power and data storage. Cell phones, computers and consumer electronic products can tolerate low levels of failure that are totally unacceptable in the industrial or automotive markets. These demands for innovation and reliability, basically analog excellence, are an ideal match for Linear Technologys skill set. Linear has been effective in enabling the new electronic age for the industrial and automotive markets.
Linear has historically been the leader in high performance analog. This year we won several product awards, including the prestigious
EE Times/EDN ACE product of the year award for our LTC6804 battery management system employed in hybrid and all electric vehicles. Success has required not only innovation and first to market unique solutions, but also vertically integrated manufacturing and the

highest levels of quality, reliability and support.
Linears objective has been analog excellence:
design novel, needed functionality; enlighten
the customer in employing this innovation;
and, deliver products on time, flawlessly
manufactured.
The efforts of Linear to bring more analog
innovation into the industrial and automotive
markets have been well timed. Within the
analog market, industrial and automotive
are the fastest growing and two of the three
largest end-markets. For the three calendar
year period 2010 through 2012, WSTS reported
that the compound annual growth rate (CAGR)
for analog ICs was 7%. However, within
analog, the CAGR for products sold into the
industrial market was 12.8% and 22.6% for
the automotive market. This trend favorably
impacted Linears sales. During this same
three calendar year period, Linears CAGR
was 12.5%; its industrial CAGR was 19.2%
and its automotive, 32.3%. These are also
large markets within analog. Industrial now
represents 20% of the overall analog market
and automotive 21%, surpassed only by the
communications end-market, which includes
cell phones and accounts for 22% of the overall
analog market. Within Linear, industrial is
42% of bookings in FY13, up from 35% in
2009, automotive is 17%, up from 8% in 2009
and communications is 21%, down from 29%
in 2009, with most of the reduction resulting
from exiting the cell phone handset market as it
requires few high performance analog solutions.
This transition over the past several years to
more emphasis on industrial and automotive
customers was a multi-discipline effort within
Linear. Products were designed that can
operate at lower power and higher voltages,
and perform in harsh environmental situations.
Linear innovations brought to market include:
Power Systems Management solutions that
provide control and monitoring of power usage,
voltages, sequencing, margining and fault
logging; low power ultra-precise SAR analogto-
digital converters that permit more accurate
product testing; Power over Ethernet++
solutions that enable the delivery of up to 90
watts of power through traditional Ethernet
cables; Micromodule solutions that combine
several analog functions in one integrated
circuit format; and Wireless Sensor Network
solutions that transmit sensor output from low
power sources and operate in rugged, harsh

ambient environments. Additional efforts were made in the manufacturing process to bring failure rates down to nearly 1 part per million (PPM). Finally, the sales force was directed towards customers with longer design-in cycles and commensurately longer product life.
Some of these changes in market and
customer focus also benefited our positioning
in other end-markets. Opportunities have arisen for us in server and solid state drives in the computer end-market. Within communications, networking and cellular infrastructure markets require complex, system level, analog solutions.
For fiscal 2013 this positive end-market positioning was somewhat offset by a choppy macroeconomic environment. Our sales tapered off in the middle of the fiscal year as concerns over the election and sequester in the USA, a change of government in China, sovereign debt issues in Europe and stagnation in Japan weighed on business confidence. However, sales improved in the third and fourth quarters
and throughout the year we maintained
high levels of profitability.
As a result our fiscal 2013 was similar to fiscal 2012. Revenues of $1.28 billion increased 1.2%, or $15.6 million from the
previous year. Net income of $406.9 million increased 2.2% or $8.8 million and was a
respectable 31.7% of sales. Diluted earnings
per share were $1.71, similar to the $1.70 reported in the prior year. Cash, cash equivalents and marketable securities increased by $321.7 million from the prior year after paying dividends of $241.3 million and purchasing $85.7 million of our common stock. As has been consistently demonstrated by our business model, this was good cash flow generation and return to stockholders in a relatively flat revenue year.
For the year the Company returned 25% of its revenue to stockholders in the form of dividends and common stock purchases.
In summary, this is an exciting time for Linear. Our major end-markets are undergoing significant changes as their products become, in many ways, predominantly electronic. The tough electronics problems are analog. We welcome the challenges. We are grateful to our employees for their resolve and talent; to customers who share their challenges with us; and to our stockholders who support our business strategies.
As always we continue to strive for analog excellence.
Thank you for your support.
Sincerely,
PAUL COGHLAN
Vice President, Finance and
Chief Financial Officer
LOTHAR MAIER
Chief Executive Officer
ROBERT H. SWANSON, JR.
Executive Chairman