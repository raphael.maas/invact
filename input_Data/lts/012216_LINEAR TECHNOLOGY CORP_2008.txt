TO OUR STOCKHOLDERS

Measure us accordingly. We ended our last years report to our stockholders
with those words. We had been altering our end-market emphasis to more
traditional diverse analog end-markets such as industrial, communications, auto
and military, while lowering our exposure to consumer products. In fi scal 2008
the results were good. We outgrew most of our competitors and we continued
our stellar fi nancial performance as we grew sales, earnings per share and cash
fl ow from operations. We increased
the weighting of our business in the
traditional analog end-markets. Although
we lowered our exposure to consumer
products, we nevertheless took advantage
of technological opportunities in these
consumer markets, such as the smart
3G handset area. In fi scal 2008, 72%
of our business was in traditional analog
end-markets.  is percentage has
increased from 60% in 2003 to 64%
in 2005, 70% in 2007 to the 72% now.

COMMITTED TO ANALOG EXCELLENCE
While our end-market direction emphasizes
traditional linear markets, our overall
strategy remains steadfastly committed to
analog excellence: excelling at the highest
levels of design innovation, customer
problem solving, factory execution, quality
and reliability, product timeliness and
corporate fi nancial performance.
2008 was a successful year in our
continuing pursuit of analog excellence.
In the product area we introduced many
new products in each of our primary product
groups. In the power area we introduced
the initial products in a new family of power
management ICs directed at portable
electronic products that require high current
(fast) battery charging, high power

efficiency and digital control in a small
package. In the lighting area we introduced
a LED driver controller providing up to
50 watts for LED headlamps in motor
vehicles. We also introduced a multiphase
boost controller that is critical to a new
generation of fuel injection systems. We
added to our Module products, which are
state of the art power systems with the
form factor and reliability of an integrated
circuit. In the signal conditioning product
group our new family of high speed fully
diff erential amplifi er drivers helps our
communications and instrumentation
customers to maximize the performance of
our high speed analog to digital converters.
Finally in the mixed signal area we
introduced the industrys fi rst high speed
serial interface analog to digital converter
for use in basestations and other highly
sensitive communication systems.
These new products add to a library of
innovative products developed over the
years to help customers solve their analog
problems while enhancing their own end
products. This interface with the customer
is critical to our success at Linear.  is fi scal
year we added signifi cantly to our worldwide
field sales team to expand our penetration
globally. This strengthened sales team
enables us to broaden our exposure into
the traditional analog end markets.
In fi scal 2008, our bookings were distributed
as follows: 32% in industrial; 34%
in communications (9% handsets, 10%
infrastructure, 15% networking); 12%
in computer; 10% in automotive; 7% in
high-end consumer; and 5% in space level
and military products. Relative to the
geographic distribution of our products;
30% were sold in North America; 18% in
Europe; 13% in Japan and 39% in Asia
Pacific outside of Japan, where, in addition
to indigenous business, a large amount of
manufacturing subcontracting takes place.
Manufacturing execution throughout our
factories has always been integral to our
success. We internally perform 95% of our
wafer fabrication, 80% of our assembly and
100% of our testing.This vertical integration
enables us to directly ensure outstanding
quality and reliability, short lead times
and high on-time delivery standards. Our

reputation for quality was further enhanced
last year when we received the Excellence
in Quality Award from Cisco.
FINANCIAL PERFORMANCE
The cumulative benefit of analog excellence
is reflected in our financial performance.
In a diffi cult macroeconomic climate we
grew revenues 8.5%, substantially more
than the overall market grew. This sales
growth, aided by an Accelerated Share
Repurchase (ASR) that we did at the end
of fi scal 2007, whereby we purchased 83.3
million shares, or approximately 27% of
our outstanding shares, enabled us to grow
EPS 23% over fiscal year 2007. We also
maintained our successful margin structure
by reporting 48.4% operating margin,
similar to last year. However, net income
decreased due to lower interest income and
higher interest expense on the funds used
to fi nance the ASR. In summary, fiscal 2008
revenues were a record $1.175 billion, net
income was $387.6 million, a 33% return
on sales, and diluted EPS was a record
$1.71, compared with $1.39 in fiscal 2007.
Once again the Company generated
positive cash fl ow from operations of
$530.3 million versus $478.0 million in
fi scal 2007. Cash fl ow from operations was
positive for the 89th consecutive quarter.
During the year the Company increased its
quarterly dividend payment by 17% from
18 per share to 21 per share. We initially

began paying a dividend in 1992 and have
increased it every year since.
Looking forward we see many opportunities
to continue our strategy and grow our
business. We were $1.2 billion out of a
$37 billion analog market in our fi scal 2008.
We serve roughly 30% of this market and
have approximately 10% market share of
this portion. Therefore, we have plenty of
headroom to execute our strategy.
THE TREND TOWARD GREEN
In this annual report we have highlighted
the global trend to become more energy
effi cient as an example of the opportunity
for us in our traditional analog markets.
High oil prices are driving both more effi cient
oil exploration and driving the automobile
industry for more hybrid and electric
vehicles. The electronic content of gas driven
vehicles is also increasing as mechanical
driven functions such as steering and
braking become electronic.
Cost management and environment
sensitivity is creating demand for lower
power consumption, alternate power
sources and pollution reduction. These
worldwide trends are impacting large scale
infrastructure projects and power and heat
intensive facilities, such as data server farms,
all of which require analog power management
and signal measuring products.

Power efficiency has become very much in
vogue. In the area of lighting alone, LEDs
for which we make drivers and controllers
consume less than 10% of the energy of
incandescent and halogen bulbs.  erefore
LEDs can drive the same light output
with one-tenth the energy in industrial,
medical and automotive applications such
as high speed scanners, surgical lighting
and automobile lighting.
In summary, at Linear our passion is analog
excellence in our products, our customer
support, our quality and our fi nancial results.
Fiscal 2008 was a good year and the
opportunity for analog excellence continues.
Once again, to our customers and our
stockholders, our goal is to be an excellent
supplier and an excellent investment.
Continue to measure us accordingly.

Sincerely,

ROBERT H. SWANSON, JR.
Executive Chairman

LOTHAR MAIER
Chief Executive Officer

PAUL COGHLAN
Vice President, Finance and
Chief Financial Offi cer