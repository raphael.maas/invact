Dear fellow shareholders,
I am very pleased to report . . . On second thought, lets just say Im kind of pleased to report . . . Well, to be frank, Im actually more relieved than anything else, to report our results for 2008. In an extraordinarily challenging economic environment, we are doing OK. Our numbers arent quite as good as last year, which was our best year ever (though theyre pretty close), but relative to what is being reported by the majority of other public companies, we are just fine.

HIGHLIGHTS  OF  2008 ( AS COMPARED TO 2007 )
	Revenue rose 12.2% to $330.2 million
	Funds from operations (FFO) decreased 2.2% to $185.5 million
	Core FFO increased 2.9% to $184.2 million
	Common stock dividends paid increased 7.6% to $169.7 million
	Portfolio occupancy was 97.0% at year-end
	Maintained a large and diverse portfolio of 2,348 properties located in 49 states occupied by 119 different retailers in 30 different retail industries
	Same store rents increased 1.1%
	108 new properties were acquired for $189.6 million
	Zero balance on our $355 million credit facility and $46.8 million in cash on hand
	No mortgages on any of our properties
	No debt maturities due until 2013

INVESTOR RETURNS
During 2008, we paid 12 monthly dividends on our common stock and increased the amount of the divi- dend five times. Dividends paid per share increased 6.5% and shareholders who owned our stock for the entire year received $1.662 per share in dividends during 2008, compared to $1.560 per share in 2007. Investors who have owned Realty Income for many years also benefited from the regular payment of dividends and dividend increases. For example,
 
shareholders who purchased shares ten years ago (12/31/98), now enjoy a current yield on the
original cost of their shares of 13.7% and have received back 103.2% of their original dollars invested because of consistent and increasing dividend payments (See The Magic of Rising Dividends Over Time table on the next page).
During 2008, the price of Realty Incomes shares decreased $3.87, or 14.3%, from $27.02 to $23.15. When you add to that the $1.662 in dividends we paid last year, that works out to a total return to share- holders of 8.2%. While this is disappointing, I think it is useful to look at our relative performance versus other real estate companies and the overall stock market. The total return, including dividends, on the Dow Jones Industrial average for 2008 was 31.9%, for

 


12
 




 

the Standard & Poors 500 index it was 37.0%, for the NASDAQ it was -40.5% and for the Equity REIT Index (other real estate companies) it was 37.7%. I think it is safe to say that we held up much better than most other public companies during 2008. However, I would also note that, while I actually know how to put stock gains and cash dividends in my pocket, I still havent figured out how to pay my bills with relative performance. As such, we prefer the stock price to go up along with the increases we have had in our dividends.
 
WHAT WE NEED TO TALK ABOUT THIS YEAR
Now that Ive gone over the highlights of 2008 and provided some perspective on our investment results for the year, lets turn to what, I believe, is of chief concern to our shareholders. That is, how the current credit crisis, rapidly decelerating economy, and the dramatic changes in the financial markets have impacted us this year and how these factors might affect us going forward.
 

	The Magic of Rising Dividends Over Time
(through December 31, 2008)

Yield on Cost	The Cumulative Dividend Effect	
	1,000 Shares
Purchase Date	Original
Investment	Original
Dividends(1)	Original
Yield	Current Yield
on Cost(2)	Dividends
Received to Date	% of Original
Investment Paid Back	
	10/18/94	$ 8,000.00	$	900.00	11.3%	21.3%	$ 17,022.25	212.8%	
	12/31/94	$ 8,562.50	$	900.00	10.5%	19.9%	$ 16,722.25	195.3%	
	12/31/95	$ 11,250.00	$	930.00	8.3%	15.1%	$ 15,809.75	140.5%	
	12/31/96	$ 11,937.50	$	945.00	7.9%	14.2%	$ 14,763.50	123.7%	
	12/31/97	$ 12,719.00	$	960.00	7.5%	13.4%	$ 13,817.25	108.6%	
	12/31/98	$ 12,437.50	$ 1,020.00	8.2%	13.7%	$ 12,834.75	103.2%	
	12/31/99	$ 10,312.50	$ 1,080.00	10.5%	16.5%	$ 11,792.25	114.3%	
	12/31/00	$ 12,437.50	$ 1,110.00	8.9%	13.7%	$ 10,701.00	86.0%	
	12/31/01	$ 14,700.00	$ 1,140.00	7.8%	11.6%	$ 9,579.75	65.2%	
	12/31/02	$ 17,500.00	$ 1,170.00	6.7%	9.7%	$ 8,428.50	48.2%	
	12/31/03	$ 20,000.00	$ 1,200.00	6.0%	8.5%	$ 7,247.25	36.2%	
	12/31/04	$ 25,290.00	$ 1,320.00	5.2%	6.7%	$ 6,006.00	23.7%	
	12/31/05	$ 21,620.00	$ 1,395.00	6.5%	7.9%	$ 4,659.75	21.6%	
	12/31/06	$ 27,700.00	$ 1,518.00	5.5%	6.1%	$ 3,222.50	11.6%	
	12/31/07	$ 27,020.00	$ 1,641.00	6.1%	6.3%	$ 1,662.25	6.2%	
	12/31/08	$ 23,150.00	$ 1,701.00	7.3%	7.3%		
	(1) Based on annualized dividend amount on purchase date
(2) Based on annualized dividend amount of $1.701 per share on 12/31/08		
		


13
 



 
Heres what we will cover:
1.	We will examine the current credit crisis, the economy, and its impact on your Company,
2.	We will review our operations during 2008,
3.	We will give our view of what we believe the future holds for your Company and,
4.	Most importantly, we will renew our commitment to the ideals of The Monthly Dividend Company and assure you, once again, that we are passionate about providing monthly dividends for all of us as shareholders.

   1 . The Credit Crisis and Realty Income
In last years Annual Report, we said, We believe that many companies (us included) have had a fairly easy operating environment in recent years. Interest rates have been historically low, which has allowed businesses, consumers and investors to finance their purchases of assets with higher levels of debt. This has led to increased values in the stock market, bond market, residential real estate, commercial real estate and almost all other asset classes. At the same time, we have seen historically low default rates on mortgages, bonds, consumer loans, and virtually all other types of credit. It has been our experience that all good runs eventually end, asset values get a bit overdone and, at some point, things slow down. We wouldnt be surprised if 2008 is a year that, at best, we all move towards a more normal operating environment, and, at worst, we face more challenging conditions throughout the economy. We believe we are prepared for such an environment and should be able to make progress in our operations in 2008.
Thats an understatement! As a conservative Company, we were becoming very concerned with what we were observing in the financial markets and where we thought the economy might be headed. Thinking that there might be substantial excesses in the overall market, we made a number of moves, over the past couple of years, that are serving us extremely well
 
today. Lets take a moment to review what we were thinking and what we did about the economic environment we thought might be coming.
Since our business is to provide capital to other businesses by buying their real estate and leasing it back to them, we are, in essence, a provider of credit. That means our competition is often other sources of capital, such as the debt markets, and we try to maintain an awareness of what is going on in those markets. From 2003 to 2006, we noticed that less creditworthy borrowers were being charged rates of interest very close to the interest rates being charged to the best borrowers, which was unusual. This encouraged the more risky borrowers to add a significant amount of debt to their operations. We observed this not only in the retail industry, but also throughout the overall economy. There seemed to be too much leverage (borrowed dollars) in almost all areas of the economy. Additionally, living in San Diego, we had a front row seat to view the incredible increases in the prices of residential real estate. We observed that a lot of the real estate being bought and sold in our community was being financed by nothing down, interest-only, sub-prime, limited documentation, and negative amortization mortgages. As a result, we came to believe that a housing market bubble was building very quickly. However, we also knew, from experience, that excesses in any market can last a long time and our ability to time the end of the cycle is limited.
In late 2006, we also came to believe we might be at the beginning of the end of the rapid rise in the housing market. Grants Interest Rate Advisor, a great publication to which I subscribe, noted that the cost for  an  institution to  insure against a  default in  a
$100 million residential mortgage bond was only about 40 basis points (4/10 of 1%), or $400,000. In a matter of a few weeks, the pricing for the insurance went to over 300+ basis points ($3 million), and by February it went to over 1,000 basis points ($10 million), which meant that one or more institutions in the market thought they
 



14
 



 
needed insurance on their residential mortgage bonds and were willing to pay up for that insurance. Grants very accurately interpreted these, and other events, as the end of the housing bubble, and I thought their analysis made sense. This also caused us to speculate that, if the residential mortgage bond market collapsed, the commercial mortgage bond market might not be far behind, and then the REIT bond market might soon follow. Eventually, the overall debt market could be negatively impacted and so it occurred to us that capital might become more difficult to obtain in the future.
Based on these events, in early 2007, we began to plan what we should be doing to protect the Company. At the time, we felt a potential risk could exist for us with Crest Net Lease, Inc. (Crest), our subsidiary that buys and sells properties to other investors. Most Crest buyers are investors who have sold properties for a gain and want to reinvest the proceeds in tax deferred exchanges. We thought that if financing became difficult for Crest buyers, lease rates might rise, prices might decline and, with a lot of properties held for sale, we might have a large inventory of properties that would be difficult to sell. Unlike the properties we hold for long term investment, if the value of the properties we hold for sale were to decline, the accounting rules would force us to write down the values against our earnings. That is a long-winded way to say we could have taken some hefty losses on what is really only a small part of our business, if the market slowed substantially. We had about $120 million in inventory in Crest at that time, and so we decided that we would stop buying any additional properties in Crest, sell the properties we had, and close down Crests operations. As a result, over the last two years, we have taken inventory from $120 million down to about
$6 million, effectively exiting that small part of our business, for now. This cost us $0.10 per share in funds from operations during 2008, but we believe this was the right action to take. Had we not undertaken these changes, we would feel a lot worse about the state of our business today.
 
By August 2007, the bond market had, for the most part, effectively closed. We thought we might be at the beginning of a very restrictive securities market, and so we decided (with our Boards strong support) to try to access the bond market and raise capital.
Over the last 10 years or so, whenever we issued bonds to raise capital, the demand for our bonds usually exceeded the amount we wanted to sell. We could have used this position to shave a little interest off of our borrowing costs, or reduced the covenant protections for our bond buyers and sold to a few more marginal buyers, which would have lessened the protection for all of our bond buyers. Instead, we sought the best quality holders of our bonds, kept the terms of the bonds very attractive and allocated the majority of the available bonds to these good buyers. Most of the buyers were large insurance companies that tended to buy bonds all the time and hold them to maturity. We thought that if we took care of them during the good times, they might be more likely to buy our bonds and give us fresh capital during tougher markets.
Thus, when we went to the market with our bonds in August 2007, (during a very difficult market) we were trying to see if we could raise $100 million. We went to these same insurance companies and were very gratified to learn that they did, indeed, remember we had taken care of them. We launched our bond offering in the morning and, by midday, had almost $1 billion in demand for the bonds. Ultimately, we were able to increase the transaction size to raise $550 million of 12-year, senior unsecured notes at a 6.75% interest rate. This very attractively priced capital has served us well during the last 18 months of this credit crisis, and we have been able to put all of it to work. Today, it would be quite difficult to do the same transaction and, if you could, the interest rate would likely be over 12.0%, or an additional
$29 million per year in interest. (Think of what that would mean to your bottom line if you were an investment grade company looking to refinance debt today.)



15
 



 
By February of 2008, we felt that, after a brief opening of the debt market in the fall, albeit at higher prices, the debt market was really starting to fall apart. As such, we felt much stronger about the fact that the property market would cool, which would result in property prices dropping. That led us to temporarily shut down our efforts to acquire properties for long- term investment because we felt that potential property purchases would be cheaper later. We continue to view potential acquisitions in this way, and I believe this has served us well. In addition, the selling off of Crests inventory and ceasing all new property acquisitions in Crest has also contributed to our current, fortunate position of being awash in liquidity in a market where liquidity is highly valued.
Another decision we made was to sell some assets out of our core portfolio to both increase our level of liquidity, and to reduce our restaurant exposure. In 2008, we sold 29 properties for $27.8 million at attractive lease rates, with the majority of the property sales occurring earlier in the year. Towards the end of 2008, and into 2009, it has becoming increasingly difficult to sell properties at attractive lease rates, so our timing turned out to be fortunate.
In March of last year, we also began to see limitations in the market for bank credit facilities. In light of this, we decided to restructure our credit facility early. Some banks, at the time, suggested that the interest we would have to pay would be higher than during the last few years and, if we just extended the existing agreement for a year, the pricing might be better in 2009. Feeling the economy and credit markets were further eroding, we elected to push ahead and we were able to obtain a new, three- year credit facility (plus two, one-year extension options) with borrowing capacity of up to $355 million (plus a $100 million expansion feature). We are grateful to our banks (Wells Fargo Bank, Bank of America, Regions Bank, The Bank of New York, Wachovia Bank, Raymond James Bank, and Chevy Chase Bank) for their
 
confidence in our business and continued support. Today, the bank credit facility market, for many companies, is very limited, or closed, and so we are quite relieved to have our new facility in place. Additionally, for the time being, we have also decided to maintain a zero balance on the credit facility so that we have maximum liquidity in this market (all $355 million if we need it).
Last September, we saw an opportunity to issue common stock to build cash, which was used to pay off the $100 million of notes we had coming due in November and the $20 million of notes due in January 2009. We quickly raised the capital by issuing common stock priced at $26.82 per share. Our success with this offering, and the subsequent repayment of debt, leaves us with no debt maturities whatsoever until 2013. This is likely one of our most favorable attributes in this environment. Again, we would like to thank Raymond James & Associates, UBS Securities LLC, Robert W. Baird & Co. Incorporated, Credit Suisse Securities (USA) LLC, J.P. Morgan Securities Inc., RBC Capital Markets Corporation, Stifel Nicolaus & Company Incorporated, Janney Montgomery Scott LLC and Morgan Keegan & Company, Inc. for helping us to raise this capital in a very difficult market.
Finally, over the last few years, with the real estate and securities markets becoming increasingly overheated, we have sought to acquire the more prof- itable stores of our retailers, since this is our primary protection and margin of safety in our real estate investments, should the retailers in our portfolio see their sales decline. That margin of safety is turning out to be absolutely invaluable, as almost all retailers today are suffering from a marked decline in consumer spending.
To say the least, all of these activities have significantly enhanced Realty Incomes position in this very difficult economy and credit crisis. Quite frankly, I dont know how I would sleep at night had we not
 



16
 



 
recognized what was going on and actually taken the steps we have taken to mitigate a good part of the impact on Realty Income. I am reminded of the Noah rule: It is not enough to know its going to rain, you actually have to build the ark!
With that said, seeing now the severity of the economic downturn, I wish we could have done even more to prepare the Company for this extraordinarily difficult economy and market. I do believe we did enough so that today we are well positioned financially, and we are certainly in a better position than many other companies. To summarize our strengths in this very difficult economy:
Balance Sheet: Our balance sheet is very strong
and we are rated by industry analysts as one of the top 10 real estate companies for balance sheet health. We have no mortgage debt on any of our 2,348 properties and have no near-term debt maturities or requirement to access the credit markets for several years.
Liquidity: We also have cash on hand, which is
where you want to be in this environment, and we have a zero balance on our $355 million credit facility, which further contributes to our liquidity. This provides us with a great deal of flexibility and the potential fire power we would need should compelling investment opportunities arise.
Occupancy: Our occupancy was 97%, at the end
of the year, and the vast majority of the retailers in our portfolio continue to pay us full rents and provide us with dependable lease revenue. Its important to note that these properties generate a great deal of cash ($330 million) from which to pay dividends.
Dividends: Weve paid 461 consecutive monthly
dividends, as of the end of the year and, during 2008, we increased the dividend five times and paid share- holders $1.662 in dividends per share.
In short, we are in pretty decent shape at the end of the year and are faring better than most other companies in our industry. In todays operating
 
environment, though, no public company seems to be immune to the economic turbulence. Lets take a look at how we fared during the year.

   2 . Operating and Financial
Performance Results for 2008
REAL ESTATE PORTFOLIO PERFORMANCE AND OUTLOOK
Our portfolio of net-leased retail properties continues to provide the lease revenue from which we pay monthly dividends. As of December 31, 2008, portfolio occupancy was 97.0% (yes, that surprises me too) with just 70 properties available for lease or sale. The weighted average remaining lease term for properties in our portfolio was approximately 11.9 years. In addition, our portfolio continues to be well diversified by individual retail chain, retail industry and by geographic region. At the end of December 2008, we owned 2,348 properties located in 49 states leased to 119 different retail chains doing business in 30 separate retail industries.
Same store rents on 1,772 properties under lease, for 2008, increased 1.1% to $258.7 million from
$255.9 million in 2007. To break down the same store rent increases during 2008, we had 23 industries with increases in rents, two with flat same store rents, and five with declining same store rents. The increase in same store rents in 2008 was a bit smaller than in 2007. That is not surprising given the state of the economy and the retail environment, and I think you should expect more moderate increases again in 2009. We also selectively sold 29 properties for
$27.8 million during 2008. These are properties that had been targeted for sale based on specific asset sale criteria. In general, Realty Incomes business model is to hold properties for the long-term cash that is generated to pay dividends. However, we like to sell properties out of the portfolio when we believe:
1. Reinvesting sale proceeds will generate higher returns, 2. Asset sales will enhance the credit quality of



17
 



 
our real estate portfolio, 3. Asset sales will extend our average remaining lease term, or 4. Specific tenant or industry concentration levels will be reduced.
With respect to lease expirations during 2008, the portfolio management group handled 168 lease expirations, of which 127 of the leases were the result of normal lease expiration and the remaining 41 were the result of properties that had to be re-leased as a result of tenant financial difficulties. Needless to say, this was a much tougher year to lease vacant space. I think our portfolio management group did a great job, that resulted in 97% occupancy at the end of the year, and my thanks go out to Dawn Nguyen, Elizabeth Cate, Kristin Ferrell, Janeen Bedard, and the rest of the staff, for the hustle they showed in 2008. Wisely, their boss, Richard Collins, mostly just tells them how great they are and tries to stay out of their way. Good job, Richard!
I think this solid portfolio performance is particularly noteworthy given the stress in the consumer retail market during the year. The downward spiral of consumer spending continues into 2009 and our portfolio management team is working overtime to proactively manage lease expirations and selectively prune out weaker performers from the portfolio. As we begin 2009, we continue to monitor the operations of our retailers, looking for the areas of weakness that may need to be addressed so that our real estate portfolio performs as well as possible. It is a tough market out there. I hope we get a little lucky again this year, in this part of the business.
Keeping the vast majority of the buildings leased, year after year, is the key to generating dependable lease revenue. I believe our ability to maintain consistently high occupancy has largely been the result of rigorous due diligence and careful initial under- writing. Since we rely on the retailers, with whom we do business, to make lease payments for 15 to 20 years, we know that finding the right tenants, and owning
 
their more profitable properties, is the key to keeping the buildings leased whenever the consumer reduces spending, or the retailer hits a tough patch. We work very hard on the initial underwritings on our properties and it is in times like these where the margin of safety we build into our underwriting process gets tested. Having the right properties really pays off. It is also pretty evident, by the way, when we miss something because we end up with vacant buildings. We inevitably make some mistakes and a market like this one does a good job of pointing these mistakes out to us. I suspect there may be a fair amount of pointing out to us in 2009.
As part of the due diligence process, and our ability to generate a margin of safety, before we pur- chase any property, we send our real estate research people to analyze every location. This provides us with a first-hand analysis of operations and a feel for the likelihood of long-term profitability in a particular trade area. Each site and its surrounding area is video-taped so that our Investment Committee can see the property while they listen to the analysts presentation on the area demographics, economics, property values, traffic flow and a host of other information. All of this data is carefully considered prior to moving forward with any real estate transaction.
The depth of our research and the strength of our underwriting processes do not mean, however, that weve found a recession-proof methodology for acquiring properties. Trust me on this one, folks! But it does mean that we make every attempt to underwrite our properties sensibly so as to moderate the degree to which our lease revenue is impacted during difficult financial times.
A key metric we use when determining whether or not a property makes economic sense is a propertys cash flow coverage ratio. The equation for this is: cash flow of the store we buy  rent. This metric helps us determine our margin of safety, or how bad things
 



18
 



 
have to get before the operations of a particular retail location wont be able to support rent payments. We have seen a few of our retail tenants come under financial pressure this past year, but, fortunately, the majority of the properties we owned were some of their most profitable locations, so our lease revenue has not been impacted as much as it might have been.
We also believe that the diversification of our retail tenant base, by both industry and the individual retail chain, provides us with another measure of protection. When all of these factors are combined, we believe that the odds of profitably managing our portfolio, during recessionary periods like this, are markedly enhanced.

Portfolio Occupancy
(as of the end of each year)	
2008	97.0%
2007	97.9%
2006	98.7%
2005	98.5%
2004	97.9%
2003	98.1%
2002	97.7%
2001	98.2%
2000	97.7%
1999	98.4%
1998	99.5%
1997	99.2%
1996	99.1%
1995	99.3%
1994	99.4%
19691993	>99.0%
 
PORTFOLIO ACQUISITIONS AND OUTLOOK
During  2008,  we   acquired  108   properties   for
$190 million, with an initial weighted average lease yield of 8.7%. The 108 new properties are located in 14 states and are 100% leased under net-lease agreements with an initial average lease length of
20.6	years. They are leased to eight different retail chains in seven separate industries.
The 2008 portfolio acquisitions were lower than in prior years. This is because we had decided early on in the year that, with the uncertainty in the commercial retail real estate market, it would be prudent for us to wait on the sidelines for most of the year. As we observed the market throughout the year, we saw property prices continue to decline and lease rates rise. We believed that by being patient we would very likely be rewarded with more attractively priced retail real estate in the future. We continue in that stance today.
The  retail consumer continues to  struggle as  I write this letter and the outlook for the near-term growth of the majority of our retailers appears challenging. Come to think of it, I think most of them would be very happy with just moderate declines in their sales, given the exodus of the US consumer from the market. Though the future may seem somewhat bleak for a lot of the retail chains out there today, we believe there are retail chains, with solid operating concepts, that will need to access capital in the near future. We think they may find that capital difficult to obtain through the traditional debt and equity markets. Some of these retailers may look to unlock the capital they have tied up in their real estate through a sale-leaseback transaction. Since our efforts have focused this past year on strengthening our balance sheet and increasing our liquidity, we believe we are in a very good financial position to capitalize on some of the more attractive opportunities down the road. But for



19
 



 
now, we are being patient and have our capital where we can keep an eye on it.

ACCESS TO CAPITAL
In an environment where access to capital is mostly non-existent for the majority of companies, we are pleased to report the following transactions:
	Just the other day, we paid off $20 million in 8.0% Senior Notes due January 2009. Prior to that, in November 2008, we retired $100 million in 8.25% Monthly Income Senior Notes due November 2008. Both of these notes were retired with cash on hand from operations and property sales, as well as from proceeds received from securities offerings.
	In September of 2008, we issued 2,925,000 shares of common stock, raising approximately
$74 million in net proceeds to fund the retirement of our senior notes and for other corporate purposes. Despite challenges in the stock market at the time of the offering, our shares were very well received by the firms that participated in the offering.
Again, thanks to our investment bankers and a bit of good fortune with the timing on this one.
	In May of 2008, we announced a new and expanded $355 million acquisition credit facility that replaced our $300 million credit facility, which was due to expire in October 2008. It was our belief, at that time, that the credit markets would become more difficult and perhaps might ultimately be closed to companies seeking to extend their credit lines. Thus, we elected to restructure our credit facility well ahead of its scheduled expiration date.
These capital market activities have reduced the amount of debt on our balance sheet and increased our liquidity. Balance sheet health and liquidity are the two areas that are most carefully scrutinized in the current investment environment.
 
AS WE END 2008
We are doing OK. Your Company continues to perform pretty well and begins 2009 with:
	A strong balance sheet with just 33% debt to total capitalization, no debt maturities until 2013 and no mortgages on any properties
	Good liquidity with ample cash on hand from operations and a zero balance on our $355 million credit facility
	Occupancy at the end of 2008 of 97%
	$330 million in lease revenue
	A solid dividend track record:
-	Dividends paid per common share during 2008 increased 6.5%
-	The amount of the monthly dividend was increased five times during 2008
-	Weve paid 461 consecutive monthly dividends through 2008
-	Weve paid over $1.5 billion in dividends since 1969.

   3 . What the Future Holds for Your Company Oh, ok, I know I have to take a shot at this. Let me be perfectly frank. We are in the midst of a terrible recession and a continuing, significant, credit crisis.





Standard & Poors 500	3.2%
10-Year Treasury	2.8%
1-year CD	1.7%
Realty Income*	7.4%

 



20
 



 
Job losses are mounting, the consumer has reduced spending and many retailers are struggling. The Government (thats you and me) is spending a huge amount of money trying to bring some sense of normalcy to the credit markets, capital markets and our banking system and, through that, attempting to stem the tide of a downward spiral in the economy. I believe it is going to take awhile to right our economic ship and I dont see an ebullient environment returning for quite awhile. The amount of debt that has permeated our economy, in recent years, will need to be reduced and that reduction will likely continue to inflict pain on our economy. In past reports, I have spoken about one of our core beliefs at Realty Income. It is that we own our properties under leases that typically last 15 to 20 years and seek to pay monthly dividends to our share- holders over many years.
As such, we take a long-term view of the business. We realize that, over the long terms of these leases, we are
 
likely to have periods of both economic growth and recession, easy access to capital and periods when the capital markets are closed, high inflation and low inflation, high interest rates and low interest rates, and periods of time when events that few expected would occur in our economy, suddenly do occur. We believe quite strongly that it is our job to operate the Company in a manner where we can accomplish our mission of paying monthly dividends in all of these environments. This is not to say that when the 100-year flood hits it wont be uncomfortable for us. We operate in the same economy as everyone else, and the current environment will certainly make our task much more difficult during 2009. However, we continue to believe that your Company is positioned to withstand this market and succeed in future years. Very thankfully, we are well capitalized, quite liquid, have high occupancy, and a well diversified portfolio. As such, I believe we should weather the storm better than many others.


























21
 



 
   4 . Monthly Dividends are our Passion! . . . Our Commitment and Point of View
I think it is important to reiterate both our commitment to providing monthly dividends, as well as our point of view concerning the payment of dividends to our shareholders. First of all, we are The Monthly Dividend Company! Weve been The Monthly Dividend Company ever since the Company was founded in 1969. The only reason for us to be in business is to pay monthly dividends to our shareholders by owning properties, holding them, collecting rent, and then using the cash flow to pay dividends. Were not in the business of trading real estate or maximizing long term growth at the expense of current cash flow. Our primary goal is to manage our real estate assets that generate the revenue from which we pay dividends and, secondarily, to increase the number of assets we own and their cash flow so that we can, hopefully, increase the amount of dividends we pay over time.
Sounds simple, perhaps even boring, but believe me, we are passionate about this. Since were all share- holders, weve experienced for ourselves the benefit of having that monthly dividend check show up in our mailbox, or deposited directly into our account. Producing these dividends over the long term is why we are here, period. We will continue to operate the Company for those shareholders that are seeking a monthly dividend.
We also think the demand for that income will continue to grow. As the first members of the Baby Boomer generation approach 65, the traditional age when one begins to examine retirement options, there is increasing interest in how and where to get income. While many Baby Boomers may not be able to retire completely from the workforce, they may decide to pursue other interests and need to replace salaried income in order to do so. For these and other reasons, we believe our focus on providing monthly income is good business, both because it fulfills an unmet
 
economic need, and because of the sheer number of people who will be looking for income in the coming years.

IN CLOSING
2008 was a challenging year, and I believe 2009 will be at least as challenging, and maybe more so. As we have said many times, we are committed to our objective of paying you monthly dividends that increase over time. Once again, we are grateful for the support of the thousands of loyal shareholders who, like us, have enjoyed years of monthly dividends. As always, there are no guarantees we will be successful in our efforts during 2009 and we recommend that all investors remain diversified and rely on us for only a portion of their income needs. We will do our best to operate your Company in a prudent fashion so that the monthly dividends just keep on coming.

Sincerely,


Tom A. Lewis
Chief Executive Officer
Vice Chairman of the Board of Directors
