To our shareholders:

For Williams, 2002 was a year of intense focus on
strengthening our finances. It was a year of challenges
and, indeed, a year of real progress in spite
of those challenges. Your company closed 2002 with
renewed confidence and a clear plan for the future.
Williams began efforts to strengthen our balance sheet as
2001 drew to a close with markets unsettled by Enrons
collapse. In January of 2002, we sold $1.1 billion of publicly
traded units. In March, we sold Kern River natural
gas pipeline system and issued $275 million in new
equity. By the end of the first quarter, wed paid or brought
onto our balance sheet $2.14 billion in obligations related
to our former telecommunications subsidiary and had
essentially prepared ourselves to deal with its bankruptcy,
which did occur in April.
The impact of the telecom-related obligations on our
finances was significant and the shock waves from Enron
continued to move through our industry, specifically in the
credit markets. California and other government authorities
launched high-profile allegations and investigations of
virtually all participants in the energy marketing and trading
sector, including Williams, creating uncertainty in
financial quarters.
It became clear that we were operating in a new business
environment with new rules. We quickly set out to align
our company with these new realities. We took significant
steps to improve our liquidity and to fund previously committed
expansions of our core businesses. Key to that
strategy was sales of non-core or under-performing assets,
an endeavor in which we executed effectively.
Then bad turned to worse. Power markets deteriorated
dramatically, turning our marketing and trading groups
strong earnings into losses. These losses, coupled with
increased telecom-related debt, caused rating agencies
to lower our companys credit ratings to non-investment
grade. Those actions triggered several hundred million
dollars in additional collateral requirements for our energy
marketing and trading contracts and other commodityrelated
businesses.
In conjunction with other events, this news virtually eliminated
Williams access to public capital markets. In late July,
with $800 million of maturing debt, we faced a liquidity crisis
that threatened the financial viability of our company.
Despite this series of events, the company completed a
$3.5 billion liquidity program that allowed us to remain
solvent and move forward. We learned some hard lessons
and made difficult decisions to divest a significant amount
of good assets managed by good people.
Now, as we rebuild our organization, it is clear that
Williams core natural gas businesses  exploration and
production, interstate pipelines, and midstream  remain
very good businesses, and we are confident that our company
has a brighter future.
Why are we confident? We are confident because of the
solid financial results our core businesses achieved in
2002, because of excellent progress we made in addressing
very important issues, and because of the success we
continue to have building upon Williams strengths  our
core values, our strong people, our sound businesses.
Higher 2002 Profits From Core Businesses
All of our core businesses achieved remarkable results in
the face of overwhelming challenges. This is a testament to
the spirit of our people, the quality of our assets and the
companys ability to work through adversity.
Our core businesses achieved $1.4 billion in segment
profit, compared with $978 million in 2001. A significant
factor in the overall 2002 results was a $625 million
segment loss in our energy marketing and risk management
business, although the fourth-quarter results from
that business significantly improved compared to the
previous two quarters of 2002. A detailed review of our
financial results by business segment is available in the
Form 10-K report that follows.
Solid Progress in 2002 on Key Issues
In addition to growing profits and cash flows in our core businesses,
we made solid progress in 2002 on other key issues.
We continued to improve liquidity. After resolving our
immediate liquidity crisis in July, we instituted an
enhanced cash-management process to monitor status and
progress. We made significant cost reductions, cutting quarterly
SG&A costs by $62 million (fourth-quarter 2002 vs.
fourth-quarter 2001), and we reduced our work force by a
quarter and our officer ranks by nearly two-thirds as a result
of asset sales, measures to improve efficiency and reduced
activity in the energy marketing and trading business.
In our energy marketing and trading business, we worked
diligently to reduce our portfolio risk and liquidity
requirements. Importantly, we were able to reach favorable
agreements with the state of California and related
parties before yearend, removing significant uncertainty
from our company while preserving the substantial value
in our California energy contracts.
A Clear Strategy for 2003 and Beyond
We have established a clear commercial strategy for the
future. We are concentrating on natural gas assets in key
growth markets where we enjoy the competitive advantages
of scale, a low-cost position and leadership.
Additionally, we will:
??Maintain adequate liquidity to execute the companys
business strategy. We expect to have sufficient liquidity
to meet debt-retirement obligations and continue to
operate our businesses safely and reliably. We will
maintain adequate liquidity through selling assets in a
timely manner, rigorously controlling costs and taking
advantage of renewed access to the capital markets to
smooth and manage our ongoing debt maturities.
??Complete asset sales. In the first quarter of 2003, we
sold our retail petroleum TravelCenters and Memphis
refinery operations, two assets that in 2002 we had
placed on the market, for $644 million. Other assets we
announced for sale during 2002 include our North Pole
refinery and related Alaska operations, ethanol and
Canadian midstream operations. We have reached an
agreement with a buyer for our ethanol business and
expect to complete all of these sales during 2003.
We announced in February of this year that we intend to
sell assets beyond those previously announced; expected
proceeds are $2.25 billion, net of related debt. We are
pursuing the sale of our general partnership position and
limited-partner investment in Williams Energy Partners.
In addition, we have targeted for sale our 6,000-mile Texas
Gas pipeline system. We also will pursue targeted asset
sales amounting to less than 20 percent of the value in
Exploration & Production and Midstream Gas & Liquids.
??Create greater financial flexibility to invest in core
businesses while continuing to reduce debt. We
expect to move from year-end debt of $13.9 billion in
2002 to a level approaching $9 billion by 2005. Just a
few weeks ago, our Northwest Pipeline Corporation
completed a $175 million debt offering of senior notes
due in 2010. We intend to use these proceeds to finance
Northwests capital projects. In the meantime, we may
issue new debt or refinance existing debt to proactively
manage liquidity and balance sheet improvements
within the context of our strategy. We are exploring
issuing additional debt on Transco of $100 million to
$125 million, and refinancing our Rocky Mountain
reserves for $300 million to $800 million.
??Narrow our business focus to a smaller portfolio of
domestic natural gas operations. Williams plans to
focus on assets within its Exploration & Production,
Midstream Gas & Liquids and Gas Pipeline businesses
that provide opportunities to grow operating
cash flow and earnings with limited-scale capital
investment. In 2003, we expect to invest approximately
$1 billion in core assets  all in our key markets  for
maintenance and to satisfy existing expansion commitments
to customers, along with some development
drilling of our natural gas reserves. The company
expects to invest approximately $500 million in these
businesses in 2004 and 2005 while generating cash
flow from operations of approximately $1 billion in
2004, increasing to approximately $1.5 billion in
2005. At this level of capital spending and cash flow,
we can properly capitalize and fund the businesses in
our narrowed portfolio.
??Continue progress toward sales of parts or all of the
energy marketing and trading business. The objective
is to reduce risk and liquidity requirements while realizing
the value of this portfolio. We received $67 million cash
from the sale of our Worthington, Ind., power plant and termination
of our power agreement with Hoosier Energy during
the first quarter of this year. Late in the first quarter, we
announced an agreement to sell a full-requirements power
contract in Georgia for $188 million cash. That sale also
reduces the potential credit impact on Williams.
??Continue to reduce our cost structure. This includes
reducing the size of our work force to align with the
smaller size of our asset base. Williams closed 2002 with
a work force of about 10,000 people. Divesting assets in
2003 and continuing efforts to rationalize costs will
reduce the work force size to less than 6,000 individuals.
Poised for Future Growth
Our core natural gas businesses are healthy and viable.
Our gas production wells, pipelines and midstream facilities
should continue to generate substantial free cash flow,
an important measure of our success as we execute our
business strategy. These businesses are poised for growth
in the years ahead.
We have a strategy that defines Williams as a balanced,
integrated natural gas company with world-class assets in
exploration and production, gas pipelines, and midstream.
Essentially, we will focus on our strongest, most attractive
assets in each of our core businesses.
Its obvious we are a different company than we were a year
ago, and we are managing our company differently. We are
actively managing cash, reducing costs to align with the
size and scope of our remaining businesses and allocating
capital with strict discipline. Additionally, we are managing
the company consistent with a set of balanced financial
performance measures focused on cash, return on investment
and earnings.
All of these changes at Williams are designed to strengthen
our financial condition and to position the company to prosper
under new market realities. The year 2003 will be about
execution for us, and we expect to emerge from this year
with a solid platform from which to meet our debt obligations
for 2004. We remain confident that we can build value
over the long term, and we look forward to your support.


Steve Malcolm
Chairman, President and Chief Executive Officer