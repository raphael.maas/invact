                            Letter to the shareholders:




                   Fiscal 2006
  financial results underscore the strength of our business model and represent what




                was another
    spectacular demonstration of effort on behalf of our dedicated employees. Our




            landmark year
with over $5.3 billion in sales and solid market share growth, provides a strong platform




              for GameStop
  to take full advantage of the exceptional opportunities in this fast-growing industry.
2006 was such a successful year for GameStop that we need             testament to the talent of our management team, but, through a
multiple headlines to communicate our performance: Sales              "Better Together" focus, resulted in best practices being applied
Exceeded $5 Billion and Grew by 72%, Net Earnings Increased by        across all stores and in a stronger GameStop better able to serve
57%, and Comparable Store Sales Growth Up 11.9%.                      our customers.

Sales rose on the strength of new video game software and next-
generation hardware consoles, and continued growth in the used        THE GAMESTOP BUSINESS MODEL
category. Net earnings for the year increased to $158.3 million,      What really makes GameStop unique is not only that we have
including $4.3 million of merger-related expenses and $3.8            combined new and used product into a unified business model,
million of debt retirement costs. In addition, our diluted earnings   but that we have refined the concept to drive incremental new
per share were $1.00, which includes $0.05 per share of merger-       sales, while also generating great value at lower price points on
related expenses and debt-retirement costs.                           used product.

Last year we retired $122 million of debt, including $100 million     The used business is a complex proposition. It is a "big thing" that
of notes related to the buyback initiative that we announced          has been built by incorporating many "little things" through the
in May of 2006. In February of 2007, the Board approved an            experience we have gained through over 10 years of refining the
additional $150 million for note repurchases. To date, we have        model. We have built custom systems, created unique operating
repurchased and retired an additional $106 million in debt under      metrics, trained our personnel to make the appropriate decisions
this program.                                                         on used product and constructed a first class refurbishing center
                                                                      that recycles damaged product for resale. Being successful in
On February 7, 2007, the Class B shareholders approved the plan       the used business means being even more successful in driving
to convert the Class B shares to Class A shares on a one-for-one      new title sales, while at the same time giving our customers a
basis, while on February 9, 2007, the Board of Directors, in order    better deal.
to make our stock more attractive to a wider range of investors,




       The breakthrough Wii remote is putting
     "physical" into game play, while making the
   experience less complex and more fun.

authorized a two-for-one stock split in the form of a stock           THE CYCLE
dividend. GameStop's stock price increased by 73% over the            There is no question in our mind that this group of emerging
course of the calendar year and closed at a split-adjusted price      consoles will create a completely new cycle: one that is deeper,
of $27.55. Since year end with the combining of the A & B shares      wider and longer than any before it.
and the stock split, the stock price has increased by just over
24% to $34.27 as of May 15, 2007.                                     From the power of emerging technology, where the Xbox 360
                                                                      and the PS3 systems deliver "teraflop" measured processing
In 2006 we opened 421 stores worldwide, with total capital            power, to the impact of innovation with Nintendo's Wii using
expenditures of $134 million, paid down $122 million in debt, and     gyroscopic controllers, video gaming has entered an amazing
still ended the year with a strong balance sheet and over $650        new cycle of expansive potential. Furthermore, we are
million in cash.                                                      experiencing the benefits of merging technologies: plug in the
                                                                      360 or PS3 to a High Definition television and the gaming impact
                                                                      is further energized; play DS Lite games with a friend through the
GameStop's 2006 performance is a reflection not only of the
success of our business model, but points to the increasing           built in Wi-Fi connections, and a solo game becomes a shared
importance of over 22,000 associates who know games, love             experience; hold a rock party with Guitar Hero, and instantly the
gaming, and every day serve our customers with a special              definition of a video game takes on a more expansive meaning.
passion for the category.
                                                                      Unique concepts, better graphics, better sound, better
                                                                      connectivity  better everything is not too bold a statement for
A SUCCESSFUL MERGER
In October of 2005, we closed on a merger of the two leading          the new generation of video game product.
video game specialists in the industry: GameStop and EB Games.
In 2006, we successfully achieved the worldwide integration           Not only is the technology more powerful, it is more innovating
                                                                      and engaging. The breakthrough Wii remote is putting "physical"
of 2,351 EB stores with 2,026 GameStop stores. The significant
synergies we envisioned were delivered on time and exceeded           into game play, while making the experience less complex and
our original forecasts. This massive undertaking is not only a        more fun. With new innovative game experiences hitting the
market like Guitar Hero, Dance Dance Revolution and Cooking
Mama, video gaming is redefining itself and creating a new             What about digital downloading?
customer base.                                                         We don't see digital downloading as a threat to our business; in
                                                                       fact, we see it as an incremental factor in making video gaming
While we are at the beginning of a cycle, it is a cycle with more      even more compelling.
powerful characteristics than ever before. It is really a cycle
within a cycle.                                                        Publishers are currently experimenting with micro transactions
                                                                       generally paid for with "points," often from point cards that we
                                                                       sell in our stores. Full game downloads, while not impossible,
For the first time ever, we have a vibrant seven-year-old system
with the PS2, as it continues to generate both hardware and            are generally impractical from a time standpoint, cumbersome
software sales consistently near the top of our bestseller             from a portability view and risky from a piracy perspective. The
lists. With seven viable hardware systems: four consoles (PS2,         data file sizes of next generation video games are becoming
PS3, Xbox 360 and Wii), two handheld units (DS Lite and PSP),          exponentially larger. It is forecast that we will soon see game
along with the PC, we have exceptional fuel for sales growth.          files as large as 25 gigabytes of memory; the larger the file the
Furthermore, the original Xbox, GameBoy and GameCube, all              more difficult to efficiently download. To put this into perspective,
represent platforms that are still very much alive in the used         Sony's Resistance: Fall of Man takes over 17 gigabytes of space
market. They continue to deliver engaging entertainment at             on a Blu-ray disc, while the average music CD is 60 megabytes
lower price points, and, in many instances, GameStop is the only       and a DVD movie is about 1 gigabyte in size.
retailer still stocking titles for these older systems.
                                                                       Will the used business continue to grow?
Incoming or outgoing, GameStop is well positioned to serve core,       Yes. We expect the used business to grow in parallel with the
casual and new gamers with multiple platforms at multiple price        healthy growth currently forecast for new software over the
points. With more purchasing options, the growing diversity of         next few years. As our customers trade in product that becomes
                                                                       currency for the purchase of new games, the GameStop trade
the market fits GameStop perfectly. Not only do we have more




video game product than any other retailer, but every store is         model drives incremental new title purchases, while at the same
staffed by "gamers" who have the experience and information            time builds our inventory of older product for a more budget
needed to help our customers make the right buying decisions.          oriented customer. With seven active systems, including the
When the choice comes down to Xbox 360 or PS3; a Wii or a PS2;         original Xbox, the most active of our trailing platforms, this is an
a DS Lite or a PSP, our associates can help. When the questions        outstanding growth period for both new and used product.
are, "Do we need an extra controller?" "Will I need more
memory?" or "Will the new hardware play my old games?," our            2006 was another exceptional year for GameStop. What is even
"gamers" have the answers.                                             more positive is that we are even better positioned for 2007 and
                                                                       beyond. We have more stores serving more markets than ever
                                                                       before to fully take advantage of the powerful sales trends in the
WHAT DOES THE FUTURE HOLD
As we meet with investors, there are a few recurring questions         business. With innovative consoles, more advanced technology,
that almost always are part of the conversation:                       increasing creativity from the software developers and a
                                                                       broadening of the demographic for video games, GameStop is
                                                                       poised to maximize all the opportunities for growth.
Can you continue your aggressive store growth?
In 2007 we will open between 500 and 550 new stores
worldwide. We believe that growth level will continue for              Thank you for your continued support. We are looking forward to
the foreseeable future for many reasons, but primarily three.          another outstanding year.
First, we are growing into an already expanding market for
video games with more consoles, more innovation and more
customers. Second, we engaged an independent demographic
firm to forecast the number of additional strip stores that the U.S.
market could support. They found that there was ample room
for continued expansion in the U.S. at roughly our current yearly      R. Richard Fontaine
growth rate for at least the next five years. Third, we are now        Chief Executive Officer and Chairman of the Board
doing business in 16 countries, and believe there are growth
opportunities in every one of them.
