2011 | shareholder letter

In a year that saw the worlds
population top seven billion people,
societies across the world are ever
more dependent upon laboratory
science to improve our health,
our food, and our environment. This
dependence on science shows no
signs of slowing down.
For Waters, 2011 saw a second
consecutive year of double-digit
sales growth and even faster earnings
growth. Successful new product
launches, healthy demand from our
customer base, and continuing growth
in emerging economies were key
factors in driving our success. It is
also important to note that 2011 was
a year of investment for Waters.
During the year, we continued to invest
in research and development across
all our major product lines, including
plans for a new mass spectrometry
center of excellence near Manchester,
U.K., and expanded field personnel to
support rapidly expanding customer
bases. We are confident that we have
a strong foundation to grow top and
bottom line results in 2012 while
maintaining our reputation for industryleading
customer service and support.
Our 2011 full-year financial results
were highlighted by 13% sales growth,
along with 18% growth in adjusted
earnings per share.* As mentioned,
2011 was a year of significant
investment, and yet we managed to
expand our operating margins and
generate record free cash flow**
about 25 cents of free cash flow for
every sales dollar generated. As has
been our strategy for several years,
the primary use of cash was our ongoing
share repurchase efforts. Over the
years, this program has reduced our
share count by a third, thereby
supporting EPS growth without adding
risk to future earnings growth.
Clearly, the biggest success for Waters
in liquid chromatography technology
in 2011 was the ACQUITY UPLC
H-Class introduced a year earlier.
Continuing the momentum of 2010,
H-Class shipment and revenue growth
rates rank among the highest in
company history for a new instrument.
Even with this rapid adoption of
H-Class, Waters continues to innovate
at the highest levels by further
strengthening the high-end capability of
UPLC. In 2011, Waters introduced and
shipped the ACQUITY UPLC I-Class, the
highest performing UPLC system ever
engineered, addressing the most
difficult separations and optimizing the
performance of any mass spectrometer.
Turning to Waters mass spectrometry,
2011 was marked by new systems
introductions addressing all levels of
customer sensitivity and performance
needs. From the development of the
research-grade SYNAPT G2-S to the
ultra-sensitive Xevo TQ-S system,
Waters has a full complement of mass
spectrometers for any application.

The year also saw the introduction
of Waters first workflow-specific
total system solutions tailored for
biopharmaceutical and bioanalysis
applications. Both new systems bring
together UPLC/MS characterization
technology driven by UNIFI Scientific
Information System, an industry-first
comprehensive software solution that
unites all aspects of respective analyses
and workflows for high-resolution
analytics. With our industry-leading
core competencies in separations,
detection, chemistries, and informatics,
Waters is uniquely positioned to
develop such systems that optimize
technology performance for dedicated
application areas. We look to continue
this strategy in 2012 with new
application-tailored systems that offer
workflow benefits.
While Waters continues to experience
strong support from virtually all our
target markets, we have been able to
further our industry leadership profile in
food safety through innovative publicprivate
partnerships. In September, the
U.S. Food and Drug Administration,
University of Maryland, their Joint
Institute for Food Safety and Applied
Nutrition and Waters jointly opened
the International Food Safety Training
Laboratory (www.ifstl.org), the worlds
only permanent food safety lab that
provides hands-on lab training
on detection methods and classroom
lessons on regulatory standards,
educating governments and food
exporters so they can ensure food
is safe before it reaches the table.
Waters also expanded its Centers
of Innovation Program in 2011,
recognizing analytical scientists
facilitating breakthroughs in health
and life science research, food safety,
environmental protection, sports
medicine and many other areas.
Scientists from the nearly 20 Centers
of Innovation, in partnership with
Waters, are using liquid chromatography
and mass spectrometry to unlock the
mysteries of science and take research
down new and exciting paths.
Not surprising, the TA division finished
2011 strong by completing a year of
profitable growth with yet another
double-digit sales growth, as customers
continue to show strong adoption of
new technology instruments and as
the division enjoyed balanced sales
growth across its major geographical
segments. In 2012 and beyond, TA
will expand its system offerings and
expand its applications footprint
into biological and high-temperature
material testing opportunities.
So while we are encouraged by the
outcomes of 2011, we feel that
our strong product positions and our
access to growing markets in Asia,
Latin America and Eastern Europe
position us well to weather economic
uncertainties and continue to grow our
business. Globally, we remain confident
that the pharmaceutical industry will
continue to require our products for
research and regulatory compliance
applications, and we are also confident
that our application-tailored system
strategy will allow us to both secure a
competitive advantage and expand our
business into new application areas.
Of course, none of our accomplishments
would be possible if not for the hard
work and dedication of Waters
employees around the world. Their
efforts are truly the driving force
behind the companys success.
On behalf of the 5,700 Waters
employees worldwide, we appreciate
your continued support.
Best Regards,
Douglas A. Berthiaume
Chairman, President and
Chief Executive Officer