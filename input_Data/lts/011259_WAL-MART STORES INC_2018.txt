Dear shareholders,
associates and customers:
Recently I was talking to Chad Daggett about his experience leading
our Sam’s Club in Bangor, Maine (he was recently promoted to a new
role). Chad’s club had a good year and we were recognizing him for his
servant leadership in the club and the community. He was quick to give
credit to his team and said, “If you treat people like they make a
difference, they will.”
In that moment, he captured what makes our company unique: it’s our people, our culture, our
purpose and our values that make the difference for our business and have enabled our success
since the start. And those are the things that will empower our success in the future.
We’ve had an exciting year at Walmart. We have the right strategy and we’re
making progress putting our unique assets to work to serve customers in
all the ways they want to shop. We are confident in our strategy because
customers are responding. We are confident in our ability to execute the
strategy because we have the culture, the talent and the resources not
only to deliver the current plan, but also to dream of new things and new
ways to transform our business. We will delight customers in ways they
might not have even thought of yet. We’re out to provide an easy, fast,
friendly and fun shopping experience for them whether they shop with
us in stores, online, via a mobile device, with their voice or with
virtual reality, augmented reality or whatever comes next.
This year we made great progress accelerating innovation to save
customers both money and time. In our stores, we’re digitizing
experiences to make it easier for customers to shop and more
efficient for associates to manage inventory and perform routine
tasks. Our eCommerce business continues to scale as we launch
new ways to enhance the customer experience. We’re moving
faster, getting stronger and we have good momentum
throughout the business.
Last fiscal year we reached more than $500 billion in revenue
for the first time as a company. Walmart U.S. delivered the
highest comp sales growth in nine years and U.S. eCommerce
sales grew 44 percent. Walmart International had 10 of 11 markets
post positive comp sales for the year and five of those markets
grew comp sales by more than 5 percent. Sam’s Club comp
sales continue to improve and the business is repositioned
to capitalize on future growth. We have a strong foundation
and great opportunities to better serve customers.
I’m excited about what the future holds. 

HERE’S MORE ABOUT WHAT WE’VE BEEN
UP TO: First, we know our customers’ lives are busier
than ever before, so our first objective is to make every
day easier for them. They rely on us for low prices and
they want us to save them time. We’re leveraging our
stores as a strategic advantage in new ways. This past
year, we nearly doubled the number of stores offering
online grocery pickup to more than 1,100 locations in
the U.S. and we’ll add another 1,000 locations in fiscal
2019. We’re expanding online grocery in Canada, Mexico
and China as well. We’re also broadening our delivery
capabilities in the U.S., China and other international
markets. For example, through our partnership with
JD Daojia in China, we offer grocery delivery in less
than one hour in over 160 stores in China. We launched
an express checkout experience in pharmacy, money
services, and returns at Walmart U.S. Sam’s Club Scan
& Go has been well-received by members and we’ve
introduced this capability in certain Walmart U.S.
stores as well as in Canada.
We launched free 2-day shipping and easy reorder on
Walmart.com and the response has been strong. Sam’s
Club also enhanced the value of the Plus membership with
the offer of free shipping. Our eCommerce assortment
has also improved. In the U.S., we’ve doubled the number
of items on Walmart.com over the past year. Jet.com
continues to complement Walmart.com nicely with
its appeal to higher-income, urban,
millennial customers. Acquisitions
like Bonobos bring unique,
private branded products to
our shopping experience,
and relationships with
partners like Lord & Taylor
enable us to offer brands
and experiences that
complement our own
assortment. We’re also
partnering with Google on
personalized voice shopping.
Outside the U.S., we launched
eCommerce marketplaces in Canada and Mexico so that
customers have access to more of the general merchandise
assortments they desire. Overall, it’s a lot easier to shop
with Walmart today than it used to be. And, we’re going
to keep getting better as we focus on serving customers
seamlessly across our apps, sites and stores.
Second, we’re changing how we work to become more
efficient. We’re equipping and empowering our associates
to be successful with better information, tools and
training. In stores, this means our associates are
spending more time driving sales and less time doing
repetitive tasks. We’ve opened training academies to
further develop the retail skills of our associates and
we’ve deployed new technology and apps to help them
improve in-stock levels and better manage price changes.
We’re also testing shelf-scanning technology in stores
and distribution centers to handle routine tasks. We
will compete with technology, but we will win with our
people. We’re creating a high-performance culture that
rewards achievements and fosters accountability while
remaining true to our core values. 

The investments we’re
making are critical
to winning with
customers as
we’re focused on
delivering results
and operating
with discipline.
An everyday low
cost mindset is
core to who we
are as a company
and it helps fuel the
productivity loop
enabling lower prices
for customers. We’re
laser-focused on improving
productivity throughout the
organization and leveraging expenses as
a company. We’re also being more strategic with capital
allocation to deliver strong efficient growth. We’re
prioritizing eCommerce, store remodels and customer
initiatives like online grocery over new store openings.
In addition, we’ve made strategic choices around our
portfolio including closing certain stores and clubs and
winding-down the first party eCommerce business in
Brazil. These decisions are positioning the business for
more efficient, sustainable growth in the future.
Finally, we want to be the most trusted retailer. We
believe in the concept of shared value where Walmart
operates for the benefit of not just customers, associates
and shareholders, but all stakeholders including suppliers,
communities and society in general. It’s important for
us to do things ethically and the right way. Customers
trust us for low prices on quality merchandise. They
also want the products they buy to be good for the
planet and the people who made them so we’ve invested
to promote the safety and dignity of the people who
make the products we sell. We’ve also implemented
leading technologies in our stores and supply chain to
promote the highest freshness, quality and safety in
the food we sell, while at the same time improving
the safety of our operations worldwide.
Many of our customers also value the role we play
in their communities. We provide good jobs for our
associates with opportunity to build a career with
Walmart. Earlier this year, we increased the starting
wage for our U.S. associates to $11 per hour and
expanded maternity and parental leave for salaried
and hourly associates. Families are a priority to us and
connecting with and caring for a new family member
is obviously important. We’re working hard to provide
opportunities for associates to be successful at Walmart
or wherever their careers take them.
We’re also ready to support the communities we serve
during times of crisis. I could not be more proud of
the resolve our associates displayed this past year in
responding to the devastating damage caused by the
hurricanes in Puerto Rico, Texas and Florida and the
earthquake in Mexico. The character of our people was
on full display. We believed they would make a difference
and they did not disappoint. In addition, we’re enhancing
the environmental sustainability of our operations
and we’re investing to make our supply chain more
transparent. It’s really all about being true to our
purpose: we save people money so they can live better.
Thanks for your continued interest in our company and
for investing in our future. We’re well-positioned to
win—we have the plan, we have the assets, and most
importantly, we have the people who are hungry to
make a difference and innovate for customers in new
ways. This is a great time to be at Walmart. I am excited
for the year and what we will accomplish.
Sincerely,
Doug McMillon
President and Chief Executive Officer
Walmart Inc.