Dear Shareholders:
2014 was an important year for TripAdvisor. Our global community of travelers contributed their reviews,
opinions, photos and tips at an impressive clip, helping us reach more than 200 million reviews and opinions.
Last year also represented the beginning of an exciting new chapter for our brand, as we added the ability for
users to book many aspects of their trips directly on TripAdvisor.
Our unique understanding of travelers
I want to thank all the TripAdvisor members who contributed more than 70 million reviews and opinions to our
site. Your devotion enriches the entire TripAdvisor community and reinforces our place as a pre-eminent brand
in travel and leisure. We ended the year with more than 200 million reviews and opinions in 28 languages on
more than 1.6 million listings of places to stay, 2.4 million restaurants, and 500,000 attractions around the globe
and content is continuing to grow at an accelerated pace. Our content attracts a massive global travel audience, as
more than two-and-a-half billion users visited TripAdvisor sites during the year. User-generated content from our
robust travel community is the lifeblood of our brand. We will continue to build on this asset in 2015, making it
even easier for more users to share their experiences.
Our content gives us a unique understanding of travelers, whose needs are nuanced, contextual, and episodic.
Last year we launched Just for You, a personalized, highly adaptive recommendation engine that delivers better
travel advice for every user. We want to be every users personalized travel guide, whether someone is looking
for a place to stay, a place to eat, or something to do while on a trip and today we believe we are only scratching
the surface of what is possible.
The next chapter: Book on TripAdvisor
In 2014, we also kicked off a major initiative that enables users to plan and book their perfect trip on
TripAdvisor. Here are some highlights:
 Hotels: As previewed in last years shareholder letter, in 2014 we introduced Instant Booking, an
exciting new feature that enables users to book directly on TripAdvisor. Users can move seamlessly
from room selection, to credit card entry, to booking confirmation, all without leaving the TripAdvisor
experience. This means less friction, no confusing hand-offs and a much more enjoyable hotel shopping
experience.
 Attractions: Another exciting development of the year was our acquisition of Viator  the global leader
in online tours and attractions bookings. Viator has listings in more than 1,500 destinations and in 10
different languages, and this acquisition allows our users to find and book the best things to do on their
trip. The combination of Viators transaction capabilities with TripAdvisors media assets gives us a
strong leadership position in what PhoCusWright estimates to be an $80 billion annualized market
opportunity. Whether you are interested in booking a balloon ride in Cappadocia or a Segway tour in
Barcelona, youll be able to check availability, see prices, and complete your booking, all on
TripAdvisor.
 Restaurants: In May we acquired LaFourchette (French for The Fork)  the leading mobile restaurant
reservation business in Europe  which helps restaurant owners manage tables and fill their restaurants
with both travelers and local diners. This acquisition has provided a nice jump-start to our restaurant
reservations business, and our product and engineering teams worked quickly to improve the restaurant
planning and booking experience on TripAdvisor. LaFourchette also became the foundation for our new
restaurant platform, The Fork, which now operates in 11 countries, has a network of more than 20,000
restaurant partners and has more than six million users per month.
 Vacation Rentals: Users can also book Vacation Rentals on TripAdvisor, and we continued to scale and
hone our transaction capabilities in 2014. We drove strong growth across all of our key metrics,
including inventory, traveler inquiries, bookings, and revenue. We ended the year with more than
650,000 vacation rental listings, driven by our free-to-list business. In 2015, we expect to see a
continued shift towards transactions, and we are focused on listings growth, owner engagement, user
awareness, and product enhancements to drive higher customer satisfaction and more transactions on
our platform.
The business of being a global travel leader
Our business continues to show healthy growth and our 2014 financial performance remained strong. Total
revenue grew 32% for the year, to more than $1.2 billion. Adjusted EBITDA grew 23% to $468 million.
In our 10-K filing, you will notice that we introduced two reportable segments  Hotel and Other. This additional
information gives more insight into how we are leveraging our core Hotel business to make investments in
attractions, restaurants and vacation rentals, the businesses that constitute our Other segment. Last year, our
Hotel segment revenue grew 26% with very solid 42% Adjusted EBITDA margins. Other segment revenue grew
141%, driven primarily by continued strong growth in vacation rentals as well as by our LaFourchette and Viator
acquisitions. Adjusted EBITDA margin for the Other segment was negative 4% as we absorbed these
acquisitions and began to aggressively invest in global growth. We are prioritizing revenue growth over Adjusted
EBITDA dollar growth for the time being and we believe Adjusted EBITDA margins could improve as these
businesses achieve greater global scale.
At $1.3 trillion in bookings, the global travel market is large, lucrative, and increasingly competitive. Our
investment philosophy is to prioritize user experience, partner satisfaction, top line growth, market share gains,
and EBITDA dollar growth and reinvest profit into promising growth areas. We will continue to make the
necessary investments and decisions to enhance our long-term growth prospects, grow our community and drive
more benefit to our users and our partners, even if it is at the expense of near-term profitability. As I have said
before, we are in this for the long term.
15 years and counting
For a while now, I have had two signs taped on my office door: Speed Wins and If it is worth doing, it is
worth measuring. I recently added a third quote  attributed by some to Charles Darwin  that reads: It is not
the strongest of the species that survives, nor the most intelligent that survives; it is the one that is the most
adaptable to change.
This past February marked TripAdvisors 15th anniversary, and as I reflect on what has been most important to
our business over that time, it boils down to our ability to adapt. We test, we fail, we learn, we adapt and we keep
moving forwardquickly. I am very grateful to work with an outstanding team who continuously delivers strong
business results and moves with speed and focus to find new ways to make TripAdvisor even more essential to
travelers around the world.
In closing, 2014 was an exciting year. I would like to thank TripAdvisors users, partners, employees and
shareholders for their continued support.
Happy Travels,
Stephen Kaufer
Co-founder, President and Chief Executive Officer
TripAdvisor, Inc.