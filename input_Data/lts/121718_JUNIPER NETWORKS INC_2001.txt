The past year has presented challenges for service providers, network 
operators, and for all suppliers of networking infrastructure selling into 
these markets. The industry began 2001 with early indicators of a 
pending downturn and ended with confirmation that it would indeed 
be a difficult period. Yet despite these obstacles, Juniper Networks 
more than doubled its customer base to over 500 service providers 
and carriers in 47 countries around the world, and ended the year with 
a 32% increase in revenues. 
Juniper Networks three fundamental goals as 
we began the year were to continue our investment 
in innovation, build our market footprint around the 
world in major accounts, and maintain our financial 
fundamentals. During 2001 the company expanded 
its investment in new and existing markets and met 
targets for extending our technology and product 
portfolio lead in multiple markets. We maintained 
our ability to meet schedules for product development to support investment in market development 
efforts that further expand our business. 
The Juniper Networks customer base today 
includes many significant new network operators 
in all major markets of the world, in mobile, cable, 
and the edge, as well as continued success with our 
initial backbone core network customers. We have 
maintained the financial internals used to measure 
the fundamental health of our business. Company 
margins remain intact at nearly 60%. Days Sales 
Outstanding are within the stated 5565 day range, 
operating income as a percent of revenue remains 
solid, and cash flow from operations has been 
positive and substantial throughout the year. 
Our commitment to innovation boosts 
trust and attracts new customers 
We are pleased to post positive sales 
growth for 2001, and to be able to take advantage 
of the differentiation this result makes possible in 
a difficult market. The focused execution of the 
company's strategy allowed us to deal with adverse 
business conditions by avoiding shortterm measures 
that would have hampered technology innovation 
and leadership. Our continued research, development and market expansion investments resulted in 
four major software releases and 37 new system 
interfaces shipped during the year. Our portfolio of 
bestinclass core and edge products now includes 
six Internet router platforms, consistent software 
features throughout our entire product portfolio, 
and 107 hardware interfaces that address a variety 
of access speeds and types. 
We also offer many new Internet Protocol (IP) 
service capabilities such as Virtual Private Networks 
(VPNs), that help service providers rapidly provision 
these new revenuegenerating services at scale. 
Juniper Networks established base of service 
provider customers, including both new carriers as 
well as many incumbent providers, provides the 
company with the installed base and the resulting 
credibility to execute an expanded strategy as we 
enter 2002. 

Juniper Networks creates new 
revenue opportunities with solutions 
for multiple markets 
Juniper Networks business strategy is to sell 
service providers the fundamental IP infrastructure 
solutions for profitably building the New Public 
Network---a multipurpose conduit for multiple data, 
voice, and video services based on the Internet 
Protocol (IP). Unlike legacy networks used by single 
users for one purpose, the New Public Network 
requires a broad array of core and edge routing 
technologies to provide multiple services on the 
same network. With the acquisition of Pacific 
Broadband Communications, Juniper Networks 
builds on its leadership position in core and edge 
infrastructure for service providers and carriers by 
offering cable multiservice operators products that 
integrate twoway, highspeed IP applications using 
the Internet with cable systems. We are also very 
pleased with the progress of our joint venture with 
Ericsson, which targets products and solutions for 
users of mobile data services who need the same 
highperformance capabilities in the mobile market. 
By leveraging our industryleading technology 
and established business relationships into new 
related markets, Juniper Networks is helping 
customers rapidly create new revenue streams 
and stimulate more demand for its leading IP 
routing products. However, executing this strategy 
requires more than a broad product portfolio. 
Success requires a company vision and business 
model in sync with our customers' requirements--- 
the service providers of the New Public Network. 
A vision matching the needs of service 
providers and their customers 
Juniper Networks vision of the New Public 
Network reflects the new way that enterprise 
customers are using IP network services. In an 
effort to trim operating costs, many businesses are 
focusing on their core competencies and outsourcing 
activities such as building and operating networks. 
Outsourcing allows customers to do what they do 
best---run their business---while network operators 
can do what they do best as well, which is to operate 
networks. The New Public Network is moving routing 
intelligence and service provisioning, along with the 
associated complexity, off the customers' premise 
equipment and into the network. 
Juniper Networks provides complete support 
for service providers' requirements for New Public 
Network technology. The company strategy aligns 
the respective business models of building value in 
the public network and making life easier on the customer premise. These shared goals lay the foundation 
for a mutually profitable business partnership. 
Company focus and execution remains 
the hallmark of our business 
Juniper Networks management and its more 
than 1,200 employees are excited to spearhead 
the company's vision of the New Public Network 
with dedication and continued focus. With new vital 
technology and products, expanded markets, and a 
clear commitment to our service provider customers, 
Juniper Networks is poised to leverage the global 
market for expansion and growth. 
I would like to thank our employees, 
customers, suppliers, our business partners, 
and our investors for their continued confidence 
and commitment to Juniper Networks. 

By leveraging our industry leading technology and established 
business relationships into new related markets, Juniper 
Networks is helping customers rapidly create new revenue 
streams and stimulate more demand for its leading IP 
routing products. 

