TO OUR
STOCKHOLDERS

Host Hotels & Resorts prides itself on being the premier lodging real estate investment company. Our strategy is to own the most geographically diverse portfolio of
iconic and irreplaceable hotels in the United States, utilizing our scale and investment
grade balance sheet to grow externally through smart acquisitions and organically
through operational improvement.
Today, we believe we own one of the world�s best lodging portfolios and maintain
a sharp focus on creating long-term value for stockholders. Our bright, innovative
employees thrive in a corporate culture that rewards creative thinking and hard
work. We value the communities we operate in and aim to be responsible corporate
FLWL]HQVDVDQLQWHJUDOSDUWRIRXUEXVLQHVV:HDUHQHYHUVDWLVCHGZLWK\HVWHUGD\�V
success and strive to be better tomorrow.
In 2017, we achieved a great deal on operational, transactional, and organizational
fronts, including:
3 We had solid revenue per available room (�RevPAR�) growth of 1.3% at our comparable hotels (on a constant US Dollar basis). The company ended the year with
comparable RevPAR of $180, the highest in its history.
3 2XURSHUDWLQJSURCWPDUJLQVGHFUHDVHGEDVLVSRLQWVSULPDULO\GXHWRLPSDLUment expense at one property, while comparable hotel EBITDA margins improved
10 basis points due to continued productivity improvements and cost savings,
green choice program rollouts, and in-room dining changes. These operating
improvements led to net income of $571 million and Adjusted EBITDAre of $1.51
billion. Diluted earnings per share was $0.76 and Adjusted FFO per diluted share
was $1.69.
3 :HVROGCYHKRWHOVLQFOXGLQJRQHLQHDUO\IRUQHDUO\PLOOLRQ7KHVH
included assets located in low-growth markets with high future capital requirePHQWV7KURXJKWKLVZHFRPSOHWHGRXUVWUDWHJLFH[LWIURPWKH3DFLCFUHJLRQZLWK
the sale of the Hilton Melbourne South Wharf and opportunistically sold the Key
%ULGJH0DUULRWWIRUDYHU\ORZFDSUDWHHYHQEHIRUHFRQVLGHULQJWKHVLJQLCFDQW
capital the asset required.
3 We acquired the iconic Don CeSar and Beach House Suites complex in St. Pete
Beach, Florida and the irreplaceable W Hollywood in California in separate transactions totaling approximately $430 million. These fantastic hotels are in markets
ZLWKDYDULHW\RIVWURQJGHPDQGJHQHUDWRUVDQGUHcHFWRXUVWUDWHJLFYLVLRQWR
continually acquire assets that enhance the value of the entire portfolio.
3 We invested $277 million in capital improvements at our properties and made
tremendous progress on creating value in our portfolio, most notably at The
Phoenician where we received approval for a new Planned Unit Development,
subject to customary appeals, enabling us to sell land zoned for residential
XQLWGHYHORSPHQWZKLFKZHDQWLFLSDWHZLOOQHWXVLQFUHPHQWDOSURCWLQ
and beyond.
3 We returned almost $630 million to our stockholders via a total 2017 dividend
of $0.85 per common share.

 Last but not least, we have a new senior team in place that is better aligned under
the streamlining of asset management and investments that we completed late in
2017. We have also listened to the investment community�s call for greater transSDUHQF\LQWRRXURSHUDWLRQVDQGVLJQLCFDQWO\HQKDQFHGRXUSRUWIROLRGLVFORVXUHV
which helps illustrate the depth of the value in our best in class hotel portfolio. You
FDQCQGWKHH[SDQGHGLQYHVWRULQIRUPDWLRQDWZZZKRVWKRWHOVFRP
:HZHUHRIIWRDEXV\VWDUWLQ'XULQJWKHCUVWTXDUWHURIZHSODFHG
a portfolio of three Hyatt hotels under contract for acquisition for $1 billion. With
this unique opportunity, we are executing on the external growth part of our strategy
to begin the year. The 301-room Andaz Maui, 454-room Hyatt Regency Coconut
Point, and the 668-room Grand Hyatt San Francisco are exactly the type of assets
we have been targeting: resort and large city center properties, segments where the
supply outlook for the next several years is anemic. They are also in markets where
ZHEHOLHYHWKHQHDUWHUPJURZWKLVVLJQLCFDQWO\VWURQJHUWKDQRXUEURDGHUSRUWIROLR
and the country as a whole.
We also made great progress on addressing our New York strategy, culminating
in the announcement of the W New York sale for $190 million, which we intend to
close sometime in the second quarter, subject to customary closing conditions.
For the remainder of 2018, we remain steadfast in our commitment to driving
long-term value to our stockholders. We will continue to focus on mining value from
our existing portfolio through utilization of our analytics capabilities, real estate
enhancement initiatives and capital investments. Our scale and diverse portfolio gives
us a wealth of property information that we can leverage to enhance the value of our
portfolio by driving improvements in operating performance. We also continue to
explore strategic acquisitions and dispositions as we look to take advantage of the
strength of our balance sheet throughout the lodging cycle. We believe our balance
sheet and scale provide a competitive advantage to pursue large, complex transactions.
We are very proud of and intend to maintain our investment grade rating, which we
believe to be one of our core strategic tenets in the volatile lodging business.
As we look forward, GDP growth, coupled with recent tax reform, is expected to
UHVXOWLQLQFUHDVHGFRUSRUDWHSURCWVDQGEXVLQHVVLQYHVWPHQWLQWKHFRPLQJ\HDU
This has historically correlated to strengthening business transient demand, leaving
us optimistic that we can continue to deliver value to our stock holders. We believe
Host Hotels & Resorts is the gold standard in the lodging industry and is wellpositioned for continued success. We appreciate your support and look forward to
continuing to serve you in the future.
RICHARD E. MARRIOTT
JAMES F. RISOLEO
3UHVLGHQW&KLHI([HFXWLYH2ICFHUDQG'LUHFWRU
March 15, 2018
