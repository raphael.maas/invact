Dear Fellow Shareholders:

Coal is necessary today. At the start of every day,
when I turn on the lights, I am relying on Central Appalachian
coal to make them work. I am also relying on my
neighbors, my friends and my fellow Massey members
who work in the Central Appalachian coalfields to produce
safely and efficiently the coal that our power plants require.
Millions of other Americans are likewise relying on these
men and women to do their jobs. In 2006, Central Appalachia
produced 236 million tons of coal. This coal provided
power for roughly 20% of the households and businesses
in the United States. It was also used to manufacture
70 million tons of steel and to fuel many of our domestic
industries. The coal that Massey members produce is not
a luxury or a lifestyle consumer product. The coal that our
members produce is fundamental to the modern way of
life in America.
Coal will be crucial tomorrow. The United States
consumes 25% of the worlds oil production but has only
2% of the worlds proved reserves of oil. As this math would
predict, our country has become ever more dependent on
imported oil. We are counting on the Middle East, Africa
and South America to provide a dependable supply of fuel
for the United States. Many of these oil-producing countries
are politically unstable. In some cases, our dollars are funding
regimes that are openly hostile toward the United States.
In the past decade, our country has also increasingly turned
to natural gas in the belief that production would remain
abundant and cheap. In much the same way as with oil,
domestic reserves and production of natural gas have fallen,
and the United States has looked overseas to import the
natural gas it uses. Similar to oil, the United States consumes
nearly 25% of the worlds natural gas production but has
only 3% of the worlds proved natural gas reserves.
As more people in more countries adopt modern conveniences
and compete for a larger share of the worlds finite oil and
gas resources, we at Massey believe the United States will
eventually turn to coal for more of its energy. Fortunately, the
United States has the worlds largest reserves of coal  coal
that can be converted to electricity, to motor fuel or to
pipeline gas.
An open and rational debate about the long-term merits
of carbon-based energy belongs on the nations agenda.
However, this discussion should not occur without addressing
the near-term dangers of our current energy policy. The
tangible costs of becoming more reliant on foreign energy
supplies have been made apparent to all Americans. Our
dependency on imported oil and gas is a national security
crisis for the United States today. This very real and immediate
threat is manageable with an appropriate national energy
policy that utilizes our domestic coal reserves. It would be
inappropriate to focus a national energy policy on climate
change forecasts at the expense of American jobs and our
national security interests.
Massey Energy can deliver. Since becoming
Chairman in 1992, I have worked to ensure that Massey is
well-positioned to generate returns over the long term in
Central Appalachia. Achieving this required our management

team not only to deliver results year over year, but also to
make difficult decisions that would take a decade or more
to prove out. Now those decisions have put us in a position
to deliver value to our shareholders and deliver the energy
that the United States will need in the coming decades.
Extensive reserves. With 2.2 billion tons of coal reserves in
Central Appalachia, Massey may be the only major producer
in the region that is capable of sustaining production in the
next decade without moving into high-cost reserves or making
large acquisitions.
Low legacy liabilities. Massey has avoided accumulating large
legacy liabilities that have plagued much of the industry.
Many of the legacy liabilities borne by the industry, such as
multi-employer pension plans, dont appear on balance
sheets and wont become apparent for years, but they are
real and they will eventually come due.
Well-capitalized infrastructure. We have invested $1.2 billion
in our infrastructure in just the past five years. These dollars
were spent to make our mines and processing plants efficient
and our equipment safe and productive. These investments
will continue paying off for years to come.
Best practices. Our safety, productivity and measurement
best practices have been developed from decades of coal
mining experience in Central Appalachia. We are continuously
enhancing these procedures and training our members in
their methodology. This is one reason we have been able to
incorporate more than 4,000 new hires in the past two years
and still achieve above-average safety and productivity rates.
Looking to 2007. We have many reasons to be optimistic
about 2007. Our turnover rate has improved significantly
so far this year, and we are seeing higher productivity from
our mines as a result. Our new dragline operation is in full
production, expanding our low-cost surface mining capabilities.
Our Aracoma mine that was idle and recovering from a mine
fire this time last year, has returned to production. Our new
Kepler mine will soon be producing coal, giving us access
to 44 million tons of low volatile, high-quality metallurgical
coal. Our new Mammoth 2-Gas seam mine, with strategic
river access, will begin production of high-quality steam coal
this summer.
I personally have several goals for the upcoming year. First,
to improve further on our already excellent safety record.
Second, to reduce our cash costs from the unacceptably high
levels we experienced in 2006. Third, to be disciplined users
of capital, deploying our free cash flow to strengthen our
balance sheet and to return value to our shareholders if we
dont find attractive investment opportunities. Finally, for
Massey to end 2007 in an even stronger strategic position than
it is today.
I would like to thank our customers, communities, members
and shareholders for their continued support as we work
together for future success.
Don L. Blankenship
Chairman, Chief Executive Officer and President