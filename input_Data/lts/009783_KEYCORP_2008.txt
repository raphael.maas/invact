Key sustained a loss from continuing operations of $1.468
billion, or $3.36 per common share, for 2008 on revenues of
$4.279 billion. As much as it pains me to report that stark
fact, it is important for everyone who has a stake in Key to
realize that our enterprise has weathered the most severe
market conditions for financial services firms in modern history.
Certainly, in my four decades in the industry, I have never
experienced any other period as perilous and destructive to
the credit markets and our overall national economy. What
encourages me in the midst of this international disruption is
my confidence that our management and Board have done,
and are doing, everything possible to preserve Keys core
strengths so that Key will emerge from this systemic crisis
in an advantageous position.
But not all the news was dismal for Key in 2008. Our
Community Banking businesses performed well, with growth
in deposits and loans across all four regions, as we effectively
communicated Keys financial strength and security to our
clients, and continued to invest in our branch teams, locations
and teller technology. Those investments are paying off:
BusinessWeek magazine recently named Key 11th on its listing
of the top 25 U.S. companies for customer service. Key joined
Lexus, Amazon.com and Nordstrom in the rankings.
Keys loss for the year was largely driven by three factors.
First, as a result of an adverse court decision, we had to take
a $1 billion after-tax charge in the second quarter involving a tax
dispute with the Internal Revenue Service on certain leveraged
leasing transactions. Second, we significantly increased loan
loss provisions as the year progressed. And finally, in the
fourth quarter, we reported an after-tax noncash accounting
charge of $420 million to mark down the goodwill value of
our National Banking unit. Importantly, our losses for 2008
were not related to so-called toxic assetssubprime
mortgage loans or trading in complex mortgage-backed
securitiesthat have been reported by other institutions
and heavily criticized by financial analysts and the media.
Right now, the age-old financial truism has never been more
valid or compelling: Capital is King. This is the principle
that guided us as our management team moved quickly to
strengthen Keys capital and funding positions when windows
of opportunity presented themselves as the economic slump
deepened in 2008, as well as in making the difficult decision to
reduce our dividend. In the aggregate, we raised over $8 billion
in equity and term debt during the year. Two separate equity
capital raises were executed: The first was a $1.74 billion
offering of common and preferred shares in June that was
oversubscribed by investors. Then, in November, we added
$2.5 billion in capital as a participant in the U.S. Treasurys
Capital Purchase Program. The strong capital position resulting
from these two actions enables us to make competitive loans
and invest in our businesses, and, generally speaking, provides
us the flexibility and confidence to manage prudently in unprecedented
market conditions.
Again this year, we present a portion of our annual review in
the form of a question-and-answer interview format based on
questions most frequently asked by individual and institutional
investors, analysts, employees, the news media and community
leaders. As always, this is intended to supplement the details
in Managements Discussion & Analysis and financial notes
provided in the subsequent pages of the report.
In closing my personal letter to you, I want to emphasize
three broad and essential points about our company and
business strategy:
 KeyCorp is well-equipped to withstand the economic
downturn. As mentioned earlier, we achieved a net increase
in deposits for the year. Deposits grew across our operating
regions, reflecting depositor confidence and trust in Key.
On the other side of the balance sheet, our loans were up
8 percent for the year. And, we are continuing to invest
carefully in our businesses.
 KeyCorps fundamental strength rests in its client relationship
strategy, fortified capital position, mix of businesses
and multi-region geographic reach.
 KeyCorps approach to the current economic turmoil is to
stay on our strategic course and prepare to use our strength
to advance the longer-term value of the enterprise. Our longstanding
approach to the financial services market centers
on building a portfolio of businesses, consistent with our
relationship approach, that has higher risk-adjusted returns.
This gives us staying power for today and positions us as a
strong competitor as the country regains its financial footing.
I believe that 2009 will be at least as challenging as 2008.
It is our goal to guide the company safely through this
unprecedented financial market turmoil so that we can be
well positioned to take advantage of the eventual recovery.
We never forget that it is your confidence and trust that underpins
everything Key is and will be, and I thank you for that.
Sincerely,
Henry L. Meyer III
Chairman and Chief Executive Officer