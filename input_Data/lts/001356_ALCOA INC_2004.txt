Fellow Shareowners:
                                            Alain Belda, Chairman and Chief Executive Officer




L
       ast year I wrote about how                  A favorable external business
       Alcoa's Vision of aspiring to be      environment contributed to our per-
        the best company in the world        formance. The global economy grew
is about delivering sustained financial      at its fastest pace (+4.4%) in the last
performance, while building for the          two decades. Aluminum consumption
future and delivering on all of Alcoa's      increased by almost 9%, and aluminum
seven Values. This is the goal that          prices were up 21%. Several of our
all 131,000 Alcoans across the globe         key markets  notably aerospace and
are striving for every day.                  commercial vehicles  moved to a
      In 2004  facing the headwinds         cyclical upturn after declining trends
                                                                                                  On our ROC goal, we again
of rising costs and currency challenges     over the past two to three years.
                                                                                             improved this year, but we have more
Alcoans delivered the highest annual               However, these favorable trends
                                                                                             work to do. Our 2004 ROC was
revenue and the second-highest prof-         were countered by a weakening
                                                                                             8.4%, which improved from 7.0% a
itability in the Company's history. Strong   U.S. dollar and significant cost issues,
                                                                                             year ago. However, the first quintile
cash flows allowed us to not only reduce     notably energy and input costs such
                                                                                             entry point is currently 18.5%, so we
debt by approximately $2 billion over        as resins, carbon-based materials, and
                                                                                             will continue to work to profitably
the last two years, but also to invest in    alloy materials. Currency and energy
                                                                                             grow and improve our results.
numerous growth projects around the          alone in 2004 negatively impacted our
                                                                                                  Our cost-savings program, as
world for the Company's future. And we       earnings by more than $300 million
                                                                                             previously mentioned, was held back
did all of this while we continued to live   compared to 2003.
                                                                                                            by rising costs in 2004.
the Values that are the core
                                   In 2004  facing the headwinds of rising costs and We expect this to contin-
of our Company, achiev-
                                                                                                            ue to be a challenge in
ing, for example, a Lost                currency challenges  Alcoans delivered                             2005. We project approx-
Workday rate of 0.09
                                  the highest annual revenue and the second-highest imately $600 million in
for the first time, while
                                                                                                            cost increases from ener-
                                          profitability in the Company's history.
improving our Total
                                                                                                            gy, labor, raw materials,
Recordable Rate for the
                                                                                                            and currencies. Our
18th year in a row.                                As strong as our 2004 financial
                                                                                             cost-savings plans for 2005 indicate
                                             results were, we are confident we can
                                                                                             we will be able to offset most, if not all,
                                             do better. The ultimate measure of
2004 Financial Results
                                                                                             of these increases. But this will require
Our 2004 financial results included:         success is growth in shareholder value
                                                                                             substantial effort in planning, focusing
                                             during the year and over a period. Our
 Income from continuing operations                                                          effort, and execution at all levels of
                                             2004 return was 15.7%, obviously a
  of $1.4 billion, a 33% increase vs.                                                        the organization and across our global
                                             disappointment. However, the combined
  2003, and our second straight year of                                                      businesses, which is why our structure
                                             2003/04 return is +44% compared to
  double-digit earnings growth;                                                              and culture are so vital.
                                             +31% for the S&P 500.
 Revenue of $23.5 billion, an 11%
                                                   Stock performance is a mix of past
  increase vs. 2003;                                                                         Building for the Future
                                             performance, future expectations, and
 A debt-to-capital ratio decline                                                            We have taken many steps to position
                                             shareholder options. The best answer to
  to 30.0%, the lowest in five years;                                                        and align your Company to capture
                                             improved shareowner returns is consis-
 Disciplined use of capital to fuel                                                         growth opportunities and expand 
                                             tent, transparent, and improved perfor-
  growth projects. 2004 capital spending                                                     both through new geographies as well
                                             mance. And this is what our plans are all
  was $1.1 billion, or 95% of deprecia-                                                      as expanding current assets.
                                             about. We remain committed to our key
  tion, with approximately one-third                                                              In late 2004, we realigned the
                                             financial goals:
  used on growth initiatives. In 2005,                                                       Company along six global platforms to
  we expect to invest approximately           Profitable Growth  Continuing the            better serve customers and increase the
  $2.5 billion on capital projects,             drive to profitably grow revenues            ability to capture efficiencies. We also
  with nearly two-thirds dedicated              as well as to join the first quintile of the centralized purchasing on a global basis
  to growth projects; and                       S&P Industrials, measured in terms of        to improve our costs and better use our
 Improving the Company's return on             ROC; and                                     leverage. As we moved to strengthen the
  capital (ROC) to 8.4%, up 140 basis         Cost Savings  Achieving $1.2 billion         global business structures, we also creat-
  points from last year.                        in cost savings by 2006. In 2005, this       ed strong regional leadership to ensure
                                                cost-savings initiative  our third three-   that we maintain optimum relationships.
                                                year program since 1998  will be
                                                used to help offset cost increases across
                                                the business and bolster profitability.
                                                                                                                                      1
      In addition, we combined                      Alcoa Aluminio in Brazil also                         Clarendon, we announced plans to fur-
functional groups such as ABS (Alcoa          broke ground on an expansion that will                      ther expand by at least an additional 1.5
Business System), Customer and                increase its share of the Alumar smelting                   million mtpy. This new expansion would
Marketing Services, EHS, People, and          operations in So Luis by 30%, or                           more than double the refinery's total
Quality to accelerate the culture changes     63,000 mtpy, bringing Alcoa Aluminio's                      capacity to at least 2.8 million mtpy. In
in order to achieve our Vision and            share of smelting capacity there to                         addition, Alcoa World Alumina and
emphasize the ABS philosophy that             262,000 mtpy, and Alcoa's share of                          Chemicals (AWAC) ownership in the
                                              overall output will grow from 54% to                        refinery will move from 50% to 70%.
"people are the linchpin of ABS."
      As you will see in the pages of         60%. The expansion was facilitated                                AWAC is also a lead partner in a
this report, we are expanding across          by completion of a new, 20-year                             project exploring the feasibility for devel-
our markets, using the tenets of ABS, to      hydropower agreement.                                       oping jointly with the Government of the
help our customers grow their businesses.           We signed a memorandum of                             Republic of Guinea a 1.5 million mtpy
We expect the combination of our              understanding (MOU) under which                             alumina refinery there. A detailed feasi-
global business structure, our functional     Alcoa and the Government of the                             bility study for the refinery is expected
resources, and our R&D expertise to           Republic of Trinidad and Tobago will                        to be completed by mid-2005, with final
continue to spur profitable organic           build a state-of-the-art, environmentally                   costs, investment decisions, and con-
growth and share gains across the world.      friendly aluminum smelter and related                       struction to begin shortly thereafter.
                                              facilities in Trinidad. The smelter will                          A 600,000 mtpy efficiency upgrade
                                              utilize power produced by Trinidad and                      of the Pinjarra, Australia, alumina
Upstream  Restarts and
                                              Tobago's vast natural gas fields, which                     refinery continues to be on-track for
Growth Projects
To take advantage of the strong alu-          will then be converted into low-cost                        completion by the end of 2005. And
minum market, we are restarting several       electricity for the smelter. We are now                     Suralco, owned by AWAC, completed
smelters. When complete, these restarts       working with the government to finalize                     a 250,000 mtpy expansion to its
will add 220,000 metric tons per year         the project plan.                                           Paranam alumina refinery in Suriname
(mtpy) of production in 2005, leaving                                                                     well ahead of schedule. This brings that
                                              
us with idle capacity of 361,000 mtpy.                                                                    facility's total capacity to approximately
                                              
Restart progress has been made at:                                                                        2.2 million mtpy.
1) the Aluminerie de Bcancour, Inc.                    
                                                                                        
                                              
(ABI) facility in Quebec, following the                                                                   Portfolio Realignment and
                                                                                        
end of a strike there. This strike resulted                                                               Expanding our Footprint
                                              
in a significant cost to the Company,                                                                     We continued to review and rebalance
                                                                                    
but was necessary to enable the long-                                                                     our portfolio to better focus on our core
term flexibility we need to operate                                                                       businesses, while also making progress
                                               
efficiently; 2) the Wenatchee smelter in                                                                  on acquisitions that will enhance our
                                               
Washington, USA, after the successful                                                                     competitive position.
                                                        
resolution of an issue regarding health-                                                                        In 2005 and for a few years there-
                                                       
care cost-sharing. The plant had been                                                                     after, one of our chief challenges will
idle since 2001, and the resolution                In Ghana, we have signed an                            be the successful and ongoing integration
of this issue helped save 400 jobs; and       MOU with the Government of the                              and investment in two fabricating facili-
3) the Massena East and West smelters         Republic of Ghana to develop an inte-                       ties that we purchased this year in
in New York, USA, where we are                grated aluminum industry that would                         Samara and Belaya Kalitva in the
now running at full capacity.                 include bauxite mining, alumina refin-                      Russian Federation. The addition of the
      In addition to restarts, we began       ing, aluminum production, and rail                          two Russian fabricating facilities  which
steps to expand our upstream operations       transportation infrastructure upgrades.                     feature cast house, flat-rolled products,
that will benefit the Company for years,      We have begun expedited feasibility                         extrusions, tubes, and forgings capabili-
if not decades, to come. For example,         studies that are expected to be complet-                    ties  will allow the Company to serve
we broke ground in Iceland on Alcoa           ed in 2006, at which time both parties                      both the growing Russian market and
Fjar al, the Company's first greenfield      will negotiate definitive agreements                        global customers in Europe, Asia, and
smelter in more than 20 years. This           as well as total investment costs. The                      the Americas, giving us substantial com-
hydropowered smelter, which is sched-         MOU calls for the initial restart of                        petitive and comparative advantages.
uled to come on line in 2007, will            120,000 mtpy at the jointly owned                                  We reached an agreement with
be among the most environmentally             200,000 mtpy Valco smelter in Tema,                         Fujikura Ltd. that paves the way for
friendly in the world.                        Ghana, which is currently idled.                            Alcoa to obtain full ownership of
      In Norway, we have agreed to build           Our alumina refining system is                         the Alcoa Fujikura (AFL) Automotive
a new anode plant in Mosjen, together        also expanding significantly with an                        business. In return, Fujikura will
with Elkem ASA, that will supply the          eye toward sustained, long-term growth.                     obtain complete ownership of the AFL
Alcoa Fjar al smelter in Iceland and         In all, we are working on expansions                        Telecommunications business. AFL
the Mosjen smelter in Norway, which is       that total more than 4 million mtpy. In                     Automotive is a large part of Alcoa's
50% held by Alcoa and Elkem.                  Jamaica, for example, following on the                      approximately $3 billion automotive
                                              heels of a 250,000 mtpy expansion of                        franchise, and this transaction increases
                                              the Jamalco alumina refinery in                             our position in this market.


                                              
     During the course of 2004, we
                                              
completed the portfolio restructuring
efforts announced in January 2003,                                                                                         
                                                                                                                                       
                                                    
including the sale of Packaging                                                                                                         
                                                   
                                                        
Equipment, Huck Automotive Fasteners,
                                                                                                                                            
                                                                 
Specialty Chemicals, the Russellville (AR)                                                                                                 
and St. Louis (MO) foil locations, and                                 
                                                                                                                                  
                                                                           
Itaipava. In addition, we completed                                                                       
                                                                                   
                                                                                                    
the sale of Integris Metals to Ryerson                                                                           
                                                                                                                            
                                                
Tull and transferred our interest in the
Juruti bauxite reserves that had been
acquired through the Reynolds acquisi-        workplace. We can do better, and we                                              resulting in more than $2 million being
tion to AWAC.                                 owe it to ourselves and our families to                                          contributed on behalf of Alcoans.
     In China, we received approval           move more rapidly toward creating a                                                   We also made great strides in
to form our joint venture with the            zero-incident workplace. In 2005, we                                             increasing the understanding of both
Aluminum Corporation of China, Ltd.,          will place further focus on this vital area.                                     external and internal audiences of our
(Chalco) at Pingguo and continue to                This year we maintained Alcoa's                                             sustainability activities across the globe,
move this project, as well as our Bohai       leadership position on climate change                                            including the online publication of five
rolling mill initiative, through various      by meeting and holding the goal of                                               regional reports to augment our corpo-
government approvals and negotiations.        reducing greenhouse gas emissions by                                             rate report on our social, economic,
Both of these projects will enhance our       25% from 1990 levels. We helped devel-                                           and environmental impacts. Our overall
position in the fastest-growing markets       op a global sustainability model with                                            sustainability efforts have been recog-
in the aluminum industry.                     the International Aluminium Institute.                                           nized as among the best in the world by
     Within other businesses of the           Under the most likely scenario in that                                           Innovest and Corporate Knights at a
Company we also continued to expand           model, the industry will become "green-                                          ceremony held at the World Economic
our footprint, opening facilities in new      house gas neutral" by 2017  the first                                           Forum in Davos, Switzerland.
geographies in order to serve customers       and perhaps only industry that will be
effectively and efficiently, including        able to make that claim. A presentation                                          Delivering Both Short- and Long-Term
operations in Romania (extrusions),           on the model received "Best Paper"                                               As we look into 2005, market conditions
Honduras (wire harnesses), and Egypt          honors at The Minerals, Metals &                                                 remain strong. Both the alumina and
(closures).                                   Materials Society (TMS) conference                                               primary aluminum markets continue to
                                              in Washington, D.C.                                                              be in a deficit where demand exceeds
Living our Values                                                                                                              supply, and key downstream markets
                                              SOX and Internal Audit  Regulatory
At Alcoa we live our Values every day.                                                                                         are recovering.
                                              requirements associated with Sarbanes-
They are the guide to how we operate                                                                                                While we will continue to experi-
                                              Oxley (SOX) represented one of the
our business. Our Values require us                                                                                            ence cost and currency pressures, we are
                                              most significant challenges faced in
to think and act not only on the present                                                                                       applying rigor toward sustained cost-
                                              2004. Responding to these, we devel-
challenges, but also with the legacy in                                                                                        savings programs to help offset external
                                              oped and implemented a comprehensive
mind that we leave for those that will                                                                                         market conditions. Your Company is
                                              set of controls through the use of self-
come after us... as well as the commit-                                                                                        actively pursuing growth opportunities 
                                              assessment documentation and testing.
ments made by those that came before                                                                                           both in operations and through helping
                                              In addition, we conducted over 140
us. We think of it in terms of a relay                                                                                         our customers succeed in the market-
                                              Finance and IT SOX reviews, as well as
race, where each one of us adds value                                                                                          place. And our focus is on delivering
                                              300 scheduled process/location audits.
today and long term for all shareowners,                                                                                       both short-term and long-term results.
                                              As of year-end, Alcoa was able to clearly
Alcoans, and the communities in which                                                                                               I am confident that Alcoa will
                                              demonstrate an effective internal control
we operate.                                                                                                                    achieve its goals. I can express this confi-
                                              system. To ensure compliance through-
                                                                                                                               dence knowing how Alcoans deliver
Environment, Health and Safety  Our          out the organization, we spent nearly
                                                                                                                               when we focus on the task at hand,
lost workday rate of 0.09 (injuries per       $10 million this year auditing and testing
                                                                                                                               using the tools at our disposal, and with
200,000 work hours) for 2004 was the          our controls. The result is an even
                                                                                                                               total commitment. Our personal and
best in our history. During the year, we      stronger control environment, with less
                                                                                                                               professional pride drives our talented
had 258 million hours worked with             rework and waste.
                                                                                                                               people across the globe as we strive to
116 incidents, and more than four out of
                                                                                                                               become the best company in the world.
                                              Alcoa Foundation, Community
five Alcoa locations did not have a single
                                              Framework, and Sustainability  Alcoa
lost workday. Despite that significant
                                              Foundation increased its 2004 payout by
progress, we suffered three fatalities: two
                                              14%, with grants totaling $17 million in
Alcoans and a contractor working on
                                              34 countries, up from nearly $15 million
our site. We continue to investigate these
                                              in 2003. Alcoa's employee engagement
incidents to root cause, as well as other
                                                                                                                               Alain J. P. Belda
                                              activities reached new heights in 2004.
incidents with significant fatality risk,
                                                                                                                               Chairman and Chief Executive Officer
                                              Our programs, designed to encourage
to eliminate or control them in our
                                                                                                                               February 18, 2005
                                              and reward active participation in com-
                                              munities, saw a 39% increase in 2004,
                                                                                                                                                                                 
