LETTER
TO STAKEHOLDERS
Two Decades of Excellence. Sustained.
2017 was an outstanding year for Ventas, upholding our two decade history
of excellence. Our superior business drove record revenues and cash flow.
Our balanced, high-quality portfolio of life science, outpatient, seniors housing
and other properties across the healthcare landscape delivered year-over-year
earnings growth. We invested in our future, expanded our attractive university-based
life science business, improved our financial strength, and profitably disposed of
almost $1 billion in assets.
In 2017, we also received numerous prestigious honors recognizing excellence
in our important environmental, social and governance (ESG) efforts. We are
proud of our inaugural inclusion on the Dow Jones Sustainability North America
Index, and our designation as a �Winning Company� on the 2020 Women on
Boards Gender Diversity Index. We were also named as one of Fortune�s �2018
World�s Most Admired Companies,� underscoring our industry leadership,
commitment to ESG values, exemplary stewardship and world-class team.
Even with all of our achievements, Ventas knows there is so much more opportunity
in the large, fragmented real estate healthcare space which continues to be
supported by compelling demographic trends. In our highly dynamic market,
the opportunities are immense and our position is strong.
In 2018, our 20th anniversary year, we remain relentlessly focused on delivering
value to all our stakeholders. Our strategic principles�a diversified portfolio
of superior properties across asset classes in our space, operated by leading
care providers, innovative premier institutions and partners; financial strength
and flexibility; a secure dividend supported by stable and growing cash flows;
and leadership built on a foundation of integrity�will continue to produce reliable
growth and income.
Ventas has consistently advanced and improved through cycles because we
know that every environment provides pathways to create value. Our cohesive and
skilled team stays consistently prepared, productive and ready to seize opportunities.
With two decades of proven strategic vision, exceptional foresight and innovation, and
intelligent, timely capital allocation decisions, we expect to build upon our successful
track record long into the future.
Thanks to your ongoing trust and support, Ventas stands ready for the next
twenty years.
DEBRA A. CAFARO
Chairman and Chief Executive Officer