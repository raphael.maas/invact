To Our Shareholders:
F5s business strategy and operating model enabled us
to deliver four quarters of sequential growth and solid
profi tability, along with a number of new products that
we believe will continue to drive our growth in fi scal 2009.
We managed to achieve these results in spite of an
obviously challenging global economic environment. 

Fiscal 2008 Highlights:
Following are some of the companys key accomplishments
during the past year:
 The fourth quarter of fi scal 2008 marked our 23rd
consecutive quarter of sequential revenue growth, driven
by the continuing strength of our core business.
 Annual revenue of $650 million was up 24 percent from
fi scal 2007.
 Revenue from our ARX line of fi le virtualization products,
which we acquired with Acopia in September 2007, was
within our target range of $25 million to $30 million for
the year.
 Gross margins remained at historic levels and operating
margins trended up throughout our year-long integration
of Acopia.
 We generated record cash fl ow of $194 million from
operations and ended the year with $451 million in cash
and investments, after purchasing Acopia for $210 million
in September 2007 and repurchasing $200 million of
F5 stock in fi scal 2008.
 In January 2008 we extended our technology lead with
the introduction of VIPRION, the industrys fi rst chassisbased controller for advanced application delivery, which
has gained solid traction and is opening new opportunities
as the high end of our BIG-IP product family.
 In July 2008, we launched two new entry-level products
(BIG-IP 1600 and BIG-IP 3600), the first in a series
of refreshed hardware platforms we plan to roll out in
fi scal 2009.
 During the year, we added SAP to the list of partners,
including Microsoft and Oracle, who have embraced our
Application Ready Solutions strategy.
 We continued to extend our market leadership, moving
farther up in vision and execution on Gartners Magic
Quadrant and capturing 60 percent of the Application
Delivery Controller/Advanced Platform market.
 We continued to improve our service delivery and
execution, achieving customer satisfaction ratings above
9 on a 10-point scale.
Our ability to achieve these results in a very diffi cult
economic climate refl ects our commitment to balance
revenue growth through investments in people
and technology with disciplined expense control and
improving profitability.

Balancing Growth and Profi tability
Throughout fi scal 2007 we accelerated our investments in sales, service, and product development, increasing our total
headcount, with the addition of Acopia, by nearly 50 percent. In fi scal 2008, we began to see the fi rst returns on our
investment in sales as more feet on the street helped drive our sequential revenue growth against increasing economic
headwinds. Our investment in service personnel not only paid off in robust service revenue growth during 2008, but
enabled us to achieve new levels of customer satisfaction. As a result of our investments in product development, we were
able to meet our targets for the delivery of VIPRION and the fi rst phase of our BIG-IP platform refresh and press forward
with the development of other products scheduled for release over the next 18 months. VIPRION has become a signifi cant
contributor to product revenue during the past three quarters, and demand for our new entry-level products outstripped
our internal expectations, offsetting slower sales of the products they were designed to replace.
In fi scal 2008, with the Acopia team on board and a larger organization in place, we scaled back recruiting and returned
to our practice of hiring behind revenue growth. Strategically, our focus in fi scal 2008 was on leveraging our expanded
sales, marketing, and service organizations to address growing opportunities in our core business and in the emerging fi le
virtualization market. Although we continued to fi ll needed positions across the organization, we adjusted the pace of hiring
each quarter in response to changing business conditions. As a result, we were able to maintain or improve our non-GAAP
operating margins, which increased from 24.9 percent in the fi rst quarter of 2008 to 26.7 percent in the fourth quarter.
Leveraging Our Partnerships
Partnerships have always been key to our success in penetrating large accounts and winning market share against major
competitors like Cisco. Industry partners such as Dell, Hewlett Packard, Nokia, and Ericsson resell our products and
are instrumental in helping us penetrate accounts where Cisco has a dominant presence. In fi scal 2008, we also made
progress in building relationships with several large systems integrators, including Hewlett Packard, EDS, IBM, and CSC,
who integrate our products into their enterprise solutions.
Since introducing iControl in 2001, we have built and maintained strong technology partnerships with Microsoft, Oracle,
Siebel, PeopleSoft, SAP, and other large software vendors who have adapted their applications to exploit the capabilities of
our products. For the past two years, we have worked closely with Microsoft, Oracle, and SAP to develop Application Ready
Solutions, which are confi gured and tested to optimize the performance, availability, and security of specifi c applications.
More recently, we have developed partnerships with VMware and Microsoft to integrate our products with their server
virtualization technology. For customers turning to server virtualization to lower their capital and operating costs, both our
ADC and fi le virtualization products offer signifi cant additional savings by reducing the number of physical and virtual servers,
the number of storage devices attached to them, and the cost and complexity of managing servers and storage.
Over the past several years, reference selling by our iControl partners has been a key competitive advantage, accounting for
more than half of quarterly sales. Within the past two years, sales of Application Ready Solutions in conjunction with our
focus on solution selling have boosted the number of $1 million accounts (customers that spend more than $1 million per year)
from fewer than 30 in fi scal 2006 to more than 75 in fi scal 2008.
All of our partnership programs are tailored to the specifi c strengths of our partners and aligned with their strategic initiatives.
The ability of our products to optimize the performance and scalability of their solutions underscores the power and fl exibility
of our leading-edge technology Extending Our Technology Leadership
F5s Application Delivery Controllers (ADCs) offer a range of features, functions and performance that is unmatched by
competing products. As the high end of our ADC product family, VIPRION delivers unprecedented throughput and
supports the full array of software modules currently available on TMOS. Since it was released in January, we have seen
solid demand for VIPRION, particularly among large telecommunications companies and internet content providers with
massive throughput requirements. We have also seen growing interest from enterprise customers for large-scale projects
such as data consolidation.
With the introduction of BIG-IP 1600 and 3600 in July, we delivered the fi rst phase of a complete BIG-IP platform refresh
that is scheduled to roll out through fi scal 2009. Incorporating dual-core processors, clustered multiprocessing, and
other technology developed for VIPRION, these entry-level products replace BIG-IP 1500 and 3400, delivering twice the
performance for roughly the same price. Both products include Fast Cache, IPv6 Gateway, Rate Shaping, SSL Offl oad,
and Compression as standard features. In addition, BIG-IP 3600 runs WebAccelerator and Application Security Manager,
software modules that were previously available only on our high-end products.
In late October, we introduced BIG-IP 6900, which replaces the BIG-IP 6400 and 6800 platforms and offers relative
performance and feature enhancements comparable to those of our new entry-level products. Also in October, we
introduced the ARX 4000, our fi rst 10-gigabit fi le virtualization product that can manage up to 2 billion fi les.
Within the next six months we plan to introduce a replacement platform for BIG-IP 8400 and 8800, which will be released
in conjunction with BIG-IP v.10, a new version of the BIG-IP software. In addition to a number of other advanced features,
v.10 will incorporate WAN Optimization as a software module, running on TMOS and tightly integrated with other functions
that make up our core application delivery solution.
Over the next 12 to 18 months, other planned hardware releases on our product roadmap include new high-end platforms
and a new blade for VIPRION, which will double the systems current performance. During fi scal 2009, we also plan to
introduce BIG-IP Secure Access Manager, which will replace our FirePass SSL VPN appliance. Available as a software module
on TMOS, Secure Access Manager will be tightly integrated with other BIG-IP modules including WAN Optimization,
Application Security Manager, and WebAccelerator.
As we roll out new products and add functionality to expand our addressable markets, we continue to widen our
competitive lead, consolidate our position in the data center, and increase awareness of the F5 brand. While the global
business environment has become even more challenging, we believe our products and solutions offer customers with
constrained budgets cost-effective ways to get more out of their existing data center and network infrastructure, trim
capital spending, and reduce operating costs. During fi scal 2009, we will continue to focus on leveraging our strengths
to increase our market share and grow our business profi tably.
On behalf of the Board, I want to thank our customers, partners, and the entire F5 team for your past and continuing
contributions to the companys successand to our shareholders, including F5 employees, for your ongoing confi dence
and support.
John McAdam
President & Chief Executive Officer
November 15, 2008