Our leadership is largely attributable to the outstanding locations
of most of our self-storage facilities. We have aggressively
identified, acquired and developed properties in major metropolitan
areas throughout the United States. We own a substantial
percentage of the higher-rent self-storage space in or near major
cities in 37 states; none of our competitors operates portfolios
that have comparable market penetration. We believe our 1,361
properties, and our intensive property management system,
enable us to compete effectively in a competitive industry.
Of equal importance is employing a unifying strategy to tie
together our operational strengths. Our goal during 2000 was to
further implement our Convenience Strategy, designed to satisfy
the customers specific storage needs while generating increasing
funds from operations.
OUR CONVENIENCE STRATEGY
In todays fast-paced environment, self-storage customers want
reliable, hassle-free solutions to address a range of storage problems:
from how to find and use clean, well-managed self-storage
to how to economically transport their belongings across town,
the state or the country. Finding solutions to storage problems
benefits both the customer and our company. Our many superior
property locations, clean, well-managed storage space, complementary
businesses addressing the entire storage decision and
quality customer service add value to the customers storage
experience and encourage customers to make their storage
decisions based upon factors in addition to price.
Through our Convenience Strategy, we encourage our
onsite property managers to be flexible, informed and customer
responsive, allowing them to see what effective customer service
means to the customer.
A changing, expanding population will present challenges
and opportunities as the new century unfolds. The possessions
that future generations store may differ from what the baby
boomer generation stored. Despite these emerging trends, we
believe we will continue to be positioned to capture storage
demand, increasing funds from operations.
America is a nation of consumers, and much of what
Americans buythe
quickly outdated
computer, the nowtoo-
small televisioninevitably ends up in self-storage. Indeed,
Americans are purchasing more entertainment and recreational
goods, including TVs, stereos, powerboats, motorcycles, jet skis,
all-terrain vehicles and so forth. The escalating American consumption
of goods and services suggests the rising affluence and
leisure time of a highly mobile population. According to the
U.S. Postal Service, recent population relocation figures indicate
22 million households move in the U.S. annually, with 14 percent
of those households renting storage space.
Our national call centers in California and Texas are
designed to acquire, assimilate and utilize information about the
services and products our customers want us to provide to solve
their storage problems.
Thanks to Public Storage employees, who generously
donated their time, and to the storage containers that we
provided, we again assisted the U.S. Marine Corps Reserve with
its annual Toys for Tots program. Over a quarter of a million toys
were distributed to children in California, Florida, Illinois and
Texas. Our commitment to communities is also reflected in the
support we provide for civic and social services, the arts and
education. For example, we donated computers and computer
peripherals to local
schools and charities.
To our customers,
shareholders and employees, we express our deepest gratitude
for your support.

Sincerely,
B.Wayne Hughes
Chairman of the Board and
Chief Executive Officer

Harvey Lenkin
President