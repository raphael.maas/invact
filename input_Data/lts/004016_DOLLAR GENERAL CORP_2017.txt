To Our Fellow Shareholders,
Customers, and Employees:
At Dollar General, our mission is Serving Others, and we
think our customers are best served when we focus on
what matters most to them. Our customers know they
can rely on us to deliver a unique combination of value
and convenience, and our goal is to do everything we can
to provide them with a great shopping experience.
Dollar General achieved meaningful milestones in
2017. We completed over 2,000 real estate projects,
celebrated the opening of our 14,000th store, began
shipping from our 15th distribution center and executed
nearly two billion customer transactions. All of this hard
work helped Dollar General deliver its 28th consecutive
year of same-store sales growth, and we believe that we
are well-positioned to capitalize on growth opportunities
over both the short and long term.
Highlights of 2017 Compared to 2016
 Net sales increased by 6.8% to $23.5 billion and samestore sales grew 2.7%. The 2016 53rd week negatively
impacted the fiscal year 2017 net sales growth rate by
approximately two percentage points.
 We reported net income of $1.54 billion, or $5.63 per
diluted share.
 Cash flows from operations were $1.8 billion, an
increase of 12.3%.
 We returned $863 million to our shareholders through
the combination of share repurchases and dividends.
 We opened a record 1,315 new stores, including the
acquisition of nearly 300 store sites from another retailer,
and remodeled or relocated an additional 764 locations.
We believe that our strong performance in 2017 reflects
the strategic investments we have made in our business
and our people. As we look ahead, we are building
momentum behind key strategic initiatives that we
believe will drive sales and profit growth in the future.
In 2018, we plan to invest in digital tools and resources
to drive traffic to our stores and provide our customers
with a more personalized and convenient in-store
shopping experience. This will allow us to leverage our
more than 14,500 stores to further enhance the value and
convenience proposition for our customers.
We also plan to begin implementing a bold, new and
expanded assortment in certain non-consumable
product categories. The new and differentiated
assortment at compelling prices is aimed at enhancing
the treasure-hunt experience for our customer, as well
as further complementing our strong and growing
consumables business.
In addition to these and other strategic initiatives, we
remain focused on delivering sustainable growth and
value to all of our stakeholders through our four key
operating priorities:
1. Driving profitable sales growth: We intend to
drive a balance of top and bottom line growth
by continuing to grow new customer trips, in
addition to capturing additional share with existing customers. We expect to continue to build on our
success with various merchandising and operations
initiatives, in addition to leveraging opportunities for
gross margin expansion, including shrink reduction,
distribution and transportation efficiencies, foreign
sourcing, private brand penetration and increased
non-consumable sales.
2. Capturing growth opportunities: Our flexible
real estate model is a strategic foundation for our
continued growth. It positions us well to invest in
new store growth, enter new markets, deliver new
formats and reinvest in our mature store base. In
2018, we expect to open 900 new stores, remodel
1,000 of our mature store locations and relocate
approximately 100 stores.
3. Leveraging our position as a low cost operator: We
remain committed to controlling expenses through
our clear and defined process. In 2018, we have
initiatives in place to focus on work simplification
and efficiency in the stores and at the store support
center. We believe that these efforts will help control
costs and focus spending on the customer and our
key strategic initiatives.
4. Investing in our people as a competitive advantage:
We know our employees are a competitive advantage,
and we have a track record of investment in
compensation and training to deliver on our ongoing
commitment to their continued development. In 2017,
we were proud to see the lowest level of store manager
turnover in the past five years, and are pleased to note
that over 10,000 of our current store managers were
promoted internally. This year, we are introducing an
expanded, paid parental leave policy and adoption
assistance benefit as well as other training efforts to
prepare employees for future success.
Dollar Generals mission to serve extends to the
communities we call home. In 2017, Dollar General and
our foundations continued the legacy of the Companys
founder J. L. Turner in helping to improve peoples quality
of life through literacy and basic education. Together
with the Dollar General Literacy Foundation, we donated
a total of $20 million to literacy organizations and other
important charitable causes.
I am grateful to our nearly 130,000 employees whose
incredible work this past year helped create value for
our customers and shareholders alike. We look forward
to capitalizing on our momentum in 2018 as we continue
to execute our strategy, and remain focused on serving
our customers with the value and convenience they know
and expect from Dollar General.
Thank you for your continued support.
Respectfully,
Todd J. Vasos
CHIEF EXECUTIVE OFFICER
April 12, 2018