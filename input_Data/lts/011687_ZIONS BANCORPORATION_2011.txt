To Our Shareholders
Zions Bancorporations performance in
2011 reflected a return to profitability in
the wake of the most severe financial and
economic downturn in 75 years. It was a
year in which we continued to substantially
strengthen the composition of our
assets, our capital levels and our liquidity.
And it was a year in which we continued
to grapple with a myriad of new regulatory
requirements  a phenomenon
which will continue to pose greater financial
burdens and risks in the years ahead.
In the aftermath of the financial crisis, we
are also witnessing enormous backlash
against the banking industry in general.
The Occupy Wall Street movement
was but one expression of the frustration
some in our society are feeling with
respect to an economy beset with continuing
high levels of unemployment and
underemployment, home prices that have
in many cases fallen below corresponding
amounts owed on mortgages, and
further risks posed by financial instability
in Europe, soaring public debt levels
here in the United States, and a federal
government which many consider to be
dysfunctional.
While the root causes of these problems
are varied and complex, banks have been
an easy target and have been vilified 
usually very unfairly  by our nations
President and other politicians, and by
many in the media, for the economic
malaise experienced in recent years. In
light of the reputational stress our industry
has experienced, Im especially proud
of the exceptional manner in which our
approximate 11,000 employees have
assisted our customers,
both consumers
and businesses, in weathering a severe
financial storm. The availability of credit
banks provide is the very life-blood of
the economy, and we have worked hard
to continue to sustain our customers and
to prospect for new clients during these
uncertain times.
Financial Results
Net income in 2011 was $324 million, as
compared to a net loss of $293 million
in 2010. Net earnings applicable to common
shareholders was $153 million or
$0.83 per share in 2011, up from a net
loss of $413 million or $(2.48) per share
in 2010. Preferred dividends totaled $170
million, an increase from $123 million in
2010 as a result of increased preferred
equity resulting from the conversion of
subordinated debt.
The largest factor contributing to the
improvement in earnings results was a

reduction in the provision for loan and
lease losses to $74 million in 2011 from
$852 million in 2010, reflecting substantial
improvement in the credit quality of
our loan portfolio. Net loans and leases
rose modestly, to $37.1 billion at the end
of 2011, up 1.1% from $36.7 billion at the
end of the prior year. At the same time,
we continued to experience exceptionally
strong growth in noninterest-bearing
deposits, which grew 18.0% to $16.1 billion
at the end of 2011, compared to $13.7
billion at the end of 2010. Total deposits
increased 4.7% to $42.9 billion at yearend
2011.
Historically low interest rate levels have
pressured net interest margins throughout
the industry, and we were no
exception. Zions Bancorporations core
net interest margin, calculated net of the
amortization of discount associated with
subordinated debt convertible into preferred
shares, and net of the additional
accretion of interest income on loans
acquired from the FDIC, was 3.99% in 2011,
down from 4.12% in 2010. We witnessed
a more competitive loan pricing environment
this past year, and we continued
to shrink our portfolio of higher-priced
 though higher-risk  construction
and land development loans. We continued
to experience significant growth
in noninterest-bearing demand deposits
in 2011, and at year-end 38% of our total
deposits were of this variety  giving us
one of the highest levels of such deposits
in the industry. This led to a substantial
increase in our holdings of highly liquid,
short term, low-margin assets.
Nevertheless, our margin remains among
the strongest in our peer group of U.S.
regional banks, and we believe we are
somewhat less susceptible to the risk
of large portions of our portfolio being
refinanced at much lower rates, given
that we have a much smaller portfolio of
residential mortgage loans on our books
than most banks, and we have almost no
mortgage-backed securities. Managing
our net interest margin during this period
of persistently low interest rates, while
not exposing ourselves to inordinate risk
when interest rates rise at a future date,
remains one of our top priorities.
Noninterest income rose 9.4% in 2011
to $482 million from $440 million in
2010. This was primarily due to a substantial
reduction in net impairment
losses on securities, which are offsets
to noninterest income. Net impairment
losses totaled $34 million in 2011, down
from $85 million the prior year. Service
charges and fees on deposit accounts
decreased $25 million in 2011, due in
part to less income from overdraft fees
on debit card and ATM transactions
as a result of new regulations. The socalled
Durbin-Amendment to the
Dodd-Frank Act became effective in the
fourth quarter of the year capping debit
card interchange fees, and reduced fee
income by approximately $8 million. We
expect the full year impact to be roughly
four times that amount in 2012, before
the effect of partially offsetting actions
to re-price retail deposit account rates,
minimum balances and fees.
Noninterest expense decreased 3.5%
or $60 million in 2011 from 2010. The
decrease resulted primarily from a $67
million reduction in expenses associated
with other real estate owned, a $10 million
reduction in credit related expenses,
and a $38 million reduction in FDIC premiums.
Salaries and employee benefits
increased $49 million or 5.9%, largely
as a result of higher incentive payments
attributable to improved credit and
operating results. We expect to achieve
further reductions in total operating
expenses in 2012.
Capital and Liquidity
We continued to strengthen both our
capital and our liquidity during 2011.
Total tangible equity increased 6.4% to
$5.9 billion from $5.5 billion at the end
of 2010 as a result of increased retained
earnings and preferred equity due to subordinated
debt conversions. Regulatory
capital ratios strengthened significantly
during the year, as our Tier 1 leverage
ratio increased from 12.6% to 13.4%; Tier
1 risk-based capital increased from 14.8%
to 16.1%; and Total risk-based capital
increased from 17.2% to 18.1%. Our Tier
1 common ratio rose to 9.6% from 9.0%
a year ago. These stronger capital ratios
have been achieved concurrently with a
material reduction of credit risk in our
balance sheet while maintaining one of
the strongest loan loss reserve coverage
ratios among large banks throughout the
industry.
Liquidity continued to strengthen as
well. At year-end 2011, net cash and

short-term liquid investments totaled
19.6% of tangible assets, as compared
to 16.0% at the end of 2010. We have
increased our liquidity in part as a result
of our expectation that upon the scheduled
expiration of the FDICs Transaction
Account Guarantee program at year-end
2012, we will experience a reduction in a
portion of our demand deposits which
have benefited from full FDIC insurance
coverage under this program.
Credit Quality
The Company experienced a great deal of
improvement in the quality of its assets in
2011. Nonaccrual loans decreased 40.5%
from $1.5 billion to $910 million, and other
real estate owned decreased 48.9% from
$300 million to $153 million. The ratio of
nonperforming lending-related assets to
net loans and leases and other real estate
owned dropped to 2.8% from 4.9% the
previous year.
Net loan losses improved significantly
in 2011, dropping 54% from $983 million
in 2010 to $456 million in 2011. We
expect substantial further improvement
in the year ahead. Through the course of
the most recent credit cycle  the most
severe in several generations  Zions
outperformed its large-bank peers by
nearly 25% as measured by net chargedoff
loans as a percentage of total
average loans: over the past four years,
net charged-off loans as a percentage
of average loans averaged 1.9% annually,
compared to an asset-weighted average
of 2.5% for all banks with over $20 billion
in assets (excluding failed institutions
and trust banks). This result was achieved
despite having a land development and
construction loan portfolio that, at its
peak, was nearly twice the proportionate
size of our regional peers, and significant
exposure to arguably the two most
severely impacted markets in the nation,
as measured by real property price
declines: Arizona and Nevada.
We continued to reduce our land development
and construction loan exposure
this past year, with outstanding loans
decreasing 33% from $3.8 billion at yearend
2010 to $2.6 billion at the end of
2011. This portfolio, which exhibited the
highest loss content among all of our
loan product types in recent years, has
been reduced a total of $5.7 billion or
69% from peak levels in the first quarter
of 2008.
During 2011 we also strengthened a number
of elements of our credit process
throughout the Company. We added
additional credit officers in our subsidiary
banks and at the parent Company;
we strengthened concentration limits
and governance, and underwriting processes;
and we enhanced credit policies
and expanded our credit training program.
As in past downturns, we made a
real effort to learn and adjust, with the
expectation that the experiences of this
cycle will leave us stronger and better
prepared for the next down cycle.
Regulator y Environment
Banks are operating in the harshest political
and regulatory environment ever

experienced. It could have been argued
that commercial banking was already
among the most highly regulated industries
in America before the financial
crisis. That is certainly the case now, and
is getting worse. One prominent law firm,
Davis Polk, reports that of the approximately
400 rulemakings required under
the Dodd-Frank Wall Street Reform
and Consumer Protection Act, only 86
have been finalized; another 155 have
been proposed, and 159 have yet to be
addressed.
Paradoxically, at a time of economic
stagnation when many are urging banks
to increase the availability of credit, the
costly and complicated task of ensuring
compliance with this unprecedented
wave of new and proposed rules and
regulatory guidance makes it more difficult
than ever to respond to customers
needs and to create innovative products
and services. Regulatory complexity
and compliance have become among
the greatest risks in our business, and
threaten the industrys ability to attract
both financial capital and talented people
in the years ahead.
Under the Dodd-Frank Act, Zions
Bancorporation is deemed to be a
Systemically Important Financial
Institution, subject to a number of new
requirements. Among these new requirements
are a heightened standard for
integrating risk management and the
determination of the Companys risk
appetite. Both are accomplished in part
by utilizing empirical stress tests to
forecast expected levels of loss under
severe economic scenarios. We have
devoted significant resources to this
task in recent quarters, culminating most
recently with our participation in the
Federal Reserve Board Systems 2012
Capital Plan Review (CapPR) process. In
accordance with this process, we submitted
projections for the nine quarters
ending December 2013 indicating the
net effect on earnings, credit losses and
capital of our forecasted results under
baseline and severely stressed scenarios
formulated by both the Company
and the Federal Reserve Board System.
The stressed scenarios incorporate the
possibility of sizeable and relatively
immediate further declines in property
values, employment and gross domestic
product, among other factors. The results
of this and future stress tests required
under the Dodd-Frank Act, and the regulators
assessment of them, together with
our financial performance and the capital
plans we submit in conjunction with the
tests, will determine our ability to engage
in future capital actions, including dividends
and share buybacks.
Maintaining a Strong Reputation
in Challenging Times
In this most challenging operating environment,
I am especially proud of the
manner in which the leaders and all of
our associates in our subsidiary banks
and other operating companies have
remained close to our customers and
responded to their circumstances and
needs. Our customer relationships are
fundamentally important to our ability
to create value  both for our customers
and for our shareholders.

In May, 2011, American Banker  our
industrys leading daily trade publication
 and The Reputation Institute published
the findings of a nationwide consumer
survey of reputational strength, using the
most prominent brand name owned by
each of the nations largest financial services
companies. The Zions Bank brand
ranked second in the nation  behind
Harris Bank (a Chicago-based unit of
Bank of Montreal Corporation) and
ahead of Charles Schwab  in overall
reputation, and first in terms of products
and services.
We continued to fare exceptionally well in
surveys conducted by the industrys
leading
relationship market share research
firm, Greenwich Associates. Among over
750 banks mentioned by businesses surveyed,
Zions Bancorporation was one
of only 15 banks to receive Excellent
marks in Overall Satisfaction among
middle market businesses, and one of
only 21 banks named as being Excellent
in Overall Satisfaction among small businesses.
Notably, Zions also received
Excellent marks in Relationship Manager
Performance by both groups, one of
only 14 such banks so honored by small
businesses, and one of only six banks
recognized in this category
by middle
market firms across the United States.
Amegy Bank of Texas was recognized in
the most recent Greenwich Associates
surveys as having the largest market
share of lead banking relationships
among middle market firms (those with
revenues between $10 million and $100
million) in the Houston market  the
sixth largest market in the United States
 a first for the bank. Zions Bank was
recognized as Best Bank in Utahs Best
of State competition for the seventh consecutive
year; as the leading provider
of U.S. Small Business Administration
(SBA) 7(a) loans in both Utah and
Idaho for the eighteenth and tenth consecutive
years, respectively; and as the
nations leading provider of SBA 504
loans to small businesses. California Bank
& Trust was recognized as San Diegos
Best Bank by a readers poll conducted
by San Diego Union-Tribune newspaper.
And National Bank of Arizona was lauded
for the fifth year in a row as Arizonas #1
Bank and Best of the Best by Ranking
Arizona magazine.
I am also especially proud of the fact that
Zions Bancorporation was recognized as
having one of the top teams of women
in the banking industry by American
Banker magazine  one of only four
banks so recognized. We have significant
numbers of both women and minorities
serving in critical senior roles throughout
the Company. Their contributions to our
success are greatly appreciated.
In short, at a time when many banks
reputations have been taking a beating,
our customers continue to recognize the
special
combination of a tremendous
set of products and services delivered
by banks focused on their local communities,
led by experienced local
management teams who are involved in
those communities.
While the economy remains sluggish, and
challenges abound, I expect that 2012
will be a year of continued improvement
and stronger fundamental profitability
for Zions Bancorporation. We appreciate
your loyal support, and encourage you
to continue to refer your friends and
business
associates to us. We would love
to serve them!
Respectfully,
Harris H. Simmons
Chairman, President and CEO
February 13, 2012