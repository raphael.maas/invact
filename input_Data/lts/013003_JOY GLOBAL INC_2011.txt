Dear shareholders

 I am pleased to report that in fiscal 2011 Joy Global again
demonstrated that it is an operationally and financially efficient
business, and that it is well positioned to capture the long-term
growth that continues to derive from the industrialization of the
emerging markets. We consider growth, operational efficiency and
incremental profitability to be the cornerstones of our business,
and during 2011 we leveraged growth with efficiency to deliver
exceptional profitability.
Despite macroeconomic concerns, our order bookings in 2011
increased to record levels as our mining customers deployed
record levels of capital expenditures on organic growth projects
during 2011. Much of this was devoted to making up lost time, as
our customers accelerated the completion of projects that were
delayed or deferred in late 2008 and early 2009. In addition, they
continued to allocate a high percentage of their free cash flow to
new organic growth projects based on their positive longer term
outlook for energy, metals and minerals

A significant reduction in the time it takes us to build our
machines allowed us to maintain competitive delivery times
even with record bookings. It also increased the speed
of translating bookings into revenues, the latter of which
were also a record for the year. During 2011, we realized
additional benefits from Operational Excellence through
production efficiencies, and this allowed us to fund increased
investments in Engineering and in Sales and Marketing.
The Sales and Marketing investments were for expansion
of our aftermarket service capability in core markets and
for aftermarket infrastructure in emerging markets. The
Engineering investment was used to extend the range of
our longwall systems into lower and higher seams, for entry
development systems that increase the rate of advance with
lower manning levels, to expand our solutions for surface
loading, and to develop technologies that can improve the
productivity and safety of underground hard rock mining. These
are not just increased expenses but real investments that will
grow revenues and increase the profitability of our business
in future years. Even with these additional investments, our
relentless focus on operating leverage allowed us to exceed our
long-term expectation for incremental profitability. This enabled
us to increase our operating margin by over 100 basis points
to 21 percent of sales, even when including all of the costs
related to our acquisition activities during the year.

These are not just increased expenses
but real investments that will grow
revenues and increase the profitability
of our business in future years.
A by-product of our Operational Excellence program is increased
realizable capacity within existing rooflines. Although significant,
these increases are not sufficient to keep pace with the strong
outlook for our markets, and therefore we also continued to
build new factories in 2011. We are currently underway on our
fourth factory in China and our third in Tianjin. This factory will
build underground equipment for both China and our global
markets, and will double our footprint in China. This should meet
growth projections through 2013, and we are planning further
additions beyond that.

We are very selective in the acquisitions we pursue,
and do so specifically to add products that are highly
complementary to our core business and which enhance
the value we deliver to our customers. LeTourneaus large
wheel loaders expand our solutions for surface loading,
and International Mining Machinery, or IMM, gives us
access to an additional major sector of the China market.
Both acquisitions have been complex, but our team
continues to keep them on track. To get the large wheel
loaders into our product portfolio, we had to acquire the
entirety of LeTourneau Technologies, Inc. and secondly sell
off their oil and gas equipment segment. We did this very
effectively, completing the secondary transaction in just
four months. We finished the year with a majority ownership
of IMM and will tender for the remaining shares early in
2012. Our exceptional Balance Sheet has facilitated these
transactions, and we will be able to complete them while
maintaining our investment grade credit rating.

In fact, our business model is based on the premise that
a strong aftermarket business supports industry leading
machine reliability and creates a strong customer preference
for our equipment. I am pleased to report that we significantly
strengthened our aftermarket business in 2011. We started a
multi-year program to build field service centers in new and
emerging markets and to upgrade and expand our facilities
in traditional markets. By the end of our fiscal 2011, we
completed the expansion of our Mesa, Arizona service center
and we are nearing completion of a new service center in
Antofagasta, Chile. We also approved and started construction
Joy Mining Machinery Offices, Warrendale, Pennsylvania
of new service centers in Durgapur, India; the Kuzbass region
of Russia; the Hunter Valley of Australia; and the Minnesota
iron ore range in the United States.
These service centers will provide essential support to our
Life Cycle Management programs as we continue to translate
more of our aftermarket revenues from transactions to
programs. Our Cost per Ton programs and Maintenance
and Repair Contracts provide us with higher capture rates
and greater stability, and they consistently deliver better
machine reliability for our customers. The next step in Life

Cycle Management is our Smart Services, which delivers a
range of services that further improves machine reliability.
These services include remote condition monitoring, remote
diagnostics, integrated consumption modeling and parts
planning, and linked processes to increase the mean time
between failure. Based on the proven success we have had
with our initial implementation in South Africa, we are getting
strong support to deploy Smart Services in Australia, the
United States, China, and other key markets. As a result,
all of our new and upgraded service centers will be enabled
to deliver Smart Services.
As this report indicates, fiscal 2011 was a very busy and
productive year for Joy Global. Although we delivered record
results, that is only a part of the story. Operational Excellence
was a significant contributor to these results, and the drive for
improved efficiencies and incremental profitability has become
an integral part of our culture. We also made significant efforts
and investments that continue to improve our operational
performance, enhance our service support and develop
technologies that work for our customers, and thus are helping
us define the future of mining. In my view, this is the most
sustainable basis for improving shareholder value.

