2010 marked an important year for Developers Diversified
Realty. It was a year of execution, reflection and discovery.
After navigating the peak of economic uncertainty and stabilizing the portfolio, this past year provided the opportunity to
reflect upon lessons learned and formulate a long-term strategic plan to ensure this company would never again face the
challenges it did in 2008 and 2009. As a result, the management team, employees and board of directors have made a
collective commitment to operate this company with significantly less financial risk going forward, with a keen focus on
emphasizing our operating platform and providing industryleading transparency and disclosure.
A significant priority in 2010 was to reestablish market credibility
that was lost in prior years. As a management team, we understood the only way to achieve this objective was to deliver on
our stated goals. I am proud to report one year later that we met
or exceeded the key operational and deleveraging metrics of
our 2010 guidance while strengthening the foundation of our
enterprise. Our intent was to ensure preparedness in order to
navigate future micro and macroeconomic market cycles and
reaffirm certainty of execution among investors and retailers.
Execution defines success
As Thomas Edison said, Vision without execution is hallucination. The execution of our strategic initiatives was contingent
upon a culture and operating platform that was able to meet
market demands and drive outperformance. During 2010, our
execution in portfolio operations resulted in same store net
operating income growth of 1.1 percent, exceeding expectations of flat to slightly positive; our execution in leasing resulted
in a 110 basis point improvement in our portfolio leased rate,
exceeding expectations of a 100 basis point improvement; our
execution in ancillary income resulted in a 22 percent increase
in ancillary revenues, exceeding expectations of 17 percent;
our execution in portfolio management resulted in $250 million
of asset sales, at our share, exceeding expectations by $100
million; our execution in the capital markets resulted in $2.9
billion of capital raised and nearly $900 million of debt reduction. Simply put, execution defined our success.
People and platform matter
Though this list of accomplishments is encouraging, it would not
be possible without the right people and a strong operating platform. Those who study open-air retail real estate often ascribe the
value of a portfolio based on simple demographic analyses and
market position; however, many operators within the business
own the same or very similar product in the same or very similar
markets. While we, along with our most highly-regarded peers,
have successfully bounced off the bottom in terms of occupancy
and same store net operating income, some are still searching
for the floor. Why? People and platform matter.
Our assets do not operate themselves; rather, they require a
significant and consistent level of active management and
industry engagement. A high-quality operating platform, over
time, can grow net operating income and increase occupancy
while fundamentals remain challenging. A high-quality operating platform is able to attract talent and drive organizational
advancement amid market dislocations. And, most importantly,
a high-quality operating platform does not rely upon financial
engineering as a means to cloud inferior portfolio performance. 
We will continue to invest in our operating platform and promote
a corporate culture that supports excellence in execution and
management because we believe this philosophy will continue
to attract leading industry talent, reward long-term shareholders and further enhance the quality and performance of our
assets individually and our company collectively.
Moving into the future
Looking forward to 2011, the economic environment is showing
signs of improvement and the appetite for risk in our industry is
clearly growing. Rest assured, as a unified management team,
we will remain prudent in our operational execution and capital
allocation plans while maintaining our singular focus on creating
long-term value for our shareholders. We understand our responsibility as a steward of shareholder capital to act in a disciplined
and strategic manner, and we will continue in this effort with a
heightened sense of respect for the market in which we operate.
This annual report is designed to introduce our senior management team. The word team is not advanced by accident. These
individuals are seasoned professionals committed to building
credibility and respect through humility, discipline and outperformance. I am proud of their accomplishments and appreciate
their leadership, professionalism and loyalty.
With humble appreciation
In closing I would like to thank you for your confidence and
support of our company, enabling the progress we have made
thus far. While we are encouraged by the prospects for the
retail real estate industry overall, we remain committed to
meeting your expectations of becoming a prime organization
and enhancing asset value through the disciplined execution of prudent balance sheet management and operational excellence. I am excited to be working alongside talented individuals committed to delivering consistent results and compelling
returns for our shareholders.
Lastly, I wish to thank our board of directors for their steady guidance and all my fellow employees for their tireless efforts and
seemingly endless enthusiasm to make our company great. I
am extremely proud to work with these individuals and I am very
grateful to you, our shareholders, for the opportunity to do so.
All my best for a successful 2011,
Daniel B. Hurwitz
President & Chief Executive Officer