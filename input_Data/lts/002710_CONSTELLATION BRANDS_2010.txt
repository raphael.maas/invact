    Chief Executive Officer's letter
    Growing, naturally.                                          Fertile soil
    If I could sum up fiscal 2010 in four key messages,          Although the stormy economic climate continued
    they would be: focused strategy; tightened and               to provide challenging growing conditions for our
    aligned international business leadership; targeted and      business, the wine category remains healthy and ripe for
    disciplined actions; and significant accomplishments.        expansion. While the on-premise channel is tempering
                                                                 the overall growth rate, the grocery, mass merchant and
    With a new vision, mission and set of strategic
                                                                 club channels are growing at faster rates. Additionally,
    imperatives in place, Constellation set out with steadfast
                                                                 the newest generation of "millennial" wine drinkers
    determination in fiscal 2010 to execute against our
                                                                 represents an exciting and underdeveloped market of
    organic growth strategy. Our aim now is to cultivate
                                                                 high-growth potential that we are only just beginning
    the considerable strengths and opportunities that lie
                                                                 to cultivate.
    within our powerful portfolio, our operations, our team
    of talented people and the growing wine category as a        Even during the economic downturn, the premium
    whole. The wine category has historically grown above        category, where wine sells for greater than $5 a bottle
    consumer product categories and this continues to be         at retail, is growing at mid- to high-single-digit rates in
    the case, even in a bad economy. Our fiscal 2010 actions     the U.S. Eleven of our brands in the U.S., and a total
    were taken to better leverage our position in this natural   of 21 internationally, sold more than one million cases
    growth category.                                             in calendar year 2009. Seven of our wine brands were
                                                                 in the "Top 50" U.S. wine brands sold in calendar year
    Strong roots                                                 2009 and 16 were in the "Top 100." SVEDKA Vodka
    Continuing the work started in fiscal 2009, we escalated     exceeded three million cases, increased sales by an
    our efforts to reduce debt and generate free cash flow      astounding 40% versus last year and is currently the
    ending the year ahead of our expectations in both areas.     fastest-growing major U.S. spirits brand. Black Velvet
    Actions such as the sale of our value spirits business       sold more than two million cases. Crown Imports
    and the divestiture of our Gaymer Cider Company              maintained the leading market share position in the
    business, coupled with our strong free cash flow,            imported beer category in the U.S. with six of the "Top
    helped us reduce debt by nearly $600 million while           25" import brands  Corona remains #1 and Modelo
    advancing our efforts to premiumize the portfolio.           Especial is close behind at #3, achieving exceptional
    Our Board of Directors authorized the repurchase             double-digit market growth in calendar year 2009.
    of up to $300 million of Constellation's stock which
                                                                 This was a particularly challenging year for the U.S.
    we have implemented as an accelerated stock
                                                                 beer industry, in general, with volumes for our Crown
    buyback transaction.
                                                                 Imports joint venture impacted by the economy,
    We also made considerable headway in our efforts to          particularly in the convenience and on-premise market
    improve operations and organizational effectiveness          channels. To address this challenge, Crown realigned the
    throughout the company. Our global cost reduction            sales team and implemented new and creative marketing
    program, supply chain initiatives and tightened controls     and advertising campaigns to help drive market share
    on SG&A spending significantly improved our cost             gains for the imported beer portfolio.
    structure. Select portfolio rationalization efforts
                                                                 Seeds of change
    and consolidation of our global footprint served to
    enhance productivity and create greater efficiencies and     From a strategic perspective, we integrated our U.S.
    alignment across our operations. This was particularly       sales and marketing structure into a single organization.
    evident in the U.K. and Australia where we began             This resulted in synergy benefits and improved
    to realign our businesses there to be consistent with        collaboration with our trade partners. In concert
    the realities of those markets. By working to merge          with this effort, we also launched our U.S. distributor
    operations and combine infrastructure, we plan to better     consolidation effort. Five select distributors now have
    align strategy, increase efficiencies and significantly      the exclusive rights to sell Constellation's portfolio of
    reduce SG&A costs. We are increasing our focus on            wine and spirits in their respective markets in 22 states
    cash generation and are striving to lower working             currently representing approximately 60% of our total
    capital and minimize capital expenditures. Additionally,     U.S. wine and spirits volume.
    we are consolidating winemaking operations and selling
                                                                 In fiscal 2010, Constellation also took significant steps
    assets to optimize asset utilization.
                                                                 forward to make it easier for consumers to recognize
    To strengthen and unify our core business capabilities       and select our brands whether in a restaurant, online
    in key areas such as supply chain, global procurement,       or at the store. We stepped up our priority brand
    customer service, and information management and             building efforts with investments in promotions,
    analysis, we launched an intensive company-wide              advertising, strategic sponsorships, new packaging,
    technology enhancement effort designed to create the         online marketing and targeted line extensions of our
    infrastructure necessary to identify and seize               high-margin brands. Vincor Canada played a starring
    business opportunities faster and more effectively           role in the 2010 Winter Olympic Games as its "Official
    than ever before.

Wine Supplier"  achieving unprecedented
global exposure for our Canadian wine
brands such as Inniskillin, Jackson-Triggs
and Sumac Ridge. In March, SVEDKA
Vodka launched its first national television
advertising campaign. Blackstone Sonoma
Reserve, Nobilo and Monkey Bay all
introduced new labels and packaging to
better represent their premium status.
And all of our companies extended their
brand portfolios with several new premium
product introductions.

Bountiful fruits
With one of the most powerful portfolios
in the industry, Constellation's brands and
wineries gained impressive national and
international recognition in fiscal 2010.
To name just a few achievements ... nearly
100 of our current release wines have been
awarded 90 points or above by leading wine
industry publications. Nine of our beer,
wine and spirits brands were awarded 2009
"Hot Brand Awards." Five of our brands
achieved 2010 "Growth Brand Awards"
from the Beverage Information Group and
five of our brands were featured on the 2010
"Power 100 List."
The next few pages provide additional fiscal
2010 highlights showcasing the measurable
progress we have made to position
Constellation for profitable, sustainable
organic growth. We are enthusiastic and
optimistic about our future prospects and
remain committed to growing from within.
By continuing to leverage the unmatched
strengths of our people, our portfolio and
our operations, we will create value for our
shareholders while we elevate life for our
loyal and growing base of core consumers.

Sincerely,



Rob Sands
President and
Chief Executive Officer



                            