TO OUR STOCKHOLDERS:
Fiscal year 2010 was a year of significant challenges and opportunity for Microchip brought on by the global economic
conditions throughout the world. What started out as a gloomy year filled with the uncertainty of the global economic crisis
turned out to be a year of significant sequential revenue growth and profitability for Microchip. For the first time in
Microchips history we achieved double-digit percentage sequential revenue growth in every quarter during a fiscal year, and
in the fourth quarter of fiscal 2010 we achieved all time records for revenue, non-GAAP gross margin and non-GAAP
earnings per share. We firmly believe that the manner in which we handled the 2009 recession has positioned us
exceptionally well to gain market share in all of our strategic product lines in the future. As you are well aware, last fiscal
year (fiscal 2009) was the most difficult year in the history of Microchip due to the global financial crisis. Our decision not
to lay off any of our employees during the recession allowed us to keep our new product development activities on track,
resulting in a record number of new product introductions in fiscal 2010. Additionally, our customer support activities were
in full force, allowing our sales team to help our customers design their way out of the recession using Microchips
innovative products. Our actions during the recession have made Microchip stronger, and positioned us nicely for the future.
A summary of some of Microchips key accomplishments during fiscal year 2010 include:
? We were one of the few semiconductor companies that remained profitable throughout the global financial crisis. In
fiscal 2010, our net sales were up 4.9% over fiscal 2009 and our net income was $216.0 million, which was 22.8% of
sales.
? Microchip continued to generate high levels of cash from its operations which allowed us to grow our cash and
investment balance during fiscal 2010 by $90.8 million, even after paying out a record annual dividend of $249.6
million. The quarterly cash dividend payment we made in the fourth quarter of fiscal 2010 was a record 34.1 cents per
share.
? We introduced approximately 150 microcontrollers, analog products and memory products. Today we have over 700
microcontroller products and over 600 analog products in our portfolio.
? Based on Semiconductor Industry Association (SIA) data, we gained market share in all of our target markets -- 8-bit and
16-bit microcontrollers, and analog. We are continuing to expand our 32-bit microcontroller product offering with
outstanding market acceptance. This business is growing at a rapid pace, albeit from a small base.
? Microchips analog products attained new highs in revenue in fiscal 2010 and ended the year with an annual revenue run
rate of over $125 million (by annualizing the fourth quarter results). Over the past two years, Microchip was the fastest
growing diversified analog company in the semiconductor industry.
? Microchip shipped 160,243 development systems in fiscal 2010, a 17.4% increase over the previous fiscal year. Every
quarter in fiscal 2010 set a new record for quarterly development tools shipped. We believe development tool shipments
are an excellent leading indicator of the future success of Microchips products as a development tool is often the first
item a customer needs to purchase when beginning their design process with one of our microcontroller products.
? During fiscal 2010, we announced our agreement to acquire Silicon Storage Technology, Inc. (SST) and we closed this
acquisition on April 8, 2010. Through this acquisition Microchip gained access to SSTs SuperFlash technology and
extensive patent portfolio, which are critical building blocks for advanced microcontrollers. We expect that the SST
acquisition will enhance our ability to customize technology variants, thereby adding an advantage over competing
technologies. Since completing the acquisition, we have already made significant progress in rationalizing SSTs
business and expect that the acquisition will significantly add to Microchips value proposition to both its customers and
stockholders.

? During fiscal 2010, we acquired ZeroG Wireless, Inc. (ZeroG), an innovator in low-power embedded Wi-Fi
solutions. ZeroG is a developer of Wi-Fi-certified transceivers and FCC-certified modules which further strengthen
Microchips wireless offerings by enabling embedded designers to easily connect to this ubiquitous networking protocol
with any 8-, 16- or 32-bit PIC microcontroller.
? During fiscal 2010, Microchip successfully integrated and expanded the market position of R&E International, a leading
provider of analog products addressing the safety and security markets. We acquired R&E International at the end of
fiscal 2009.
? Microchip won the 2010 Arizona Business Leadership Award, and was named to the Phoenix Business Journals Best
Places to Work in the Valley for the third consecutive year. During fiscal 2010, I had the distinct honor of receiving the
Executive of the Year award in the annual EE times ACE Awards, and I was included in the Phoenix Business Journals
annual list of Most Admired CEOs. While these awards were bestowed on me, they were possible only because of the
hard work and extraordinary results created by the global Microchip team and as such, belong to them.
? Microchips products also gained numerous honors for product excellence in fiscal 2010. The PIC24F16KA eXtreme
Low Power PIC microcontrollers won the prestigious EDN Innovation Award in the Microcontroller category, and the
MCP651/2/5 operational amplifiers were finalists in the Analog Signal Path category. Both of these products were also
named finalists in the Design News Golden Mousetrap Awards.
As we begin fiscal year 2011, there remain plenty of challenges in the global economy. However, we believe we have
positioned Microchip to gain market share and deliver outstanding financial performance in fiscal 2011. I want to express
my sincere appreciation to our customers, stockholders and employees for your ongoing support of Microchip.
Steve Sanghi
President and CEO
Microchip Technology Incorporated