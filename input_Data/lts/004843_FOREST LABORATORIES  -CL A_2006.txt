Everyone knows that a succession of quality new
products is the requisite for a successful business  any
business, but especially the pharmaceutical business.
The challenge for Forest has always been how to acquire
products when the traditional method in our industry,
developing novel molecules through long, costly and
arduous research, has historically not been affordable or
available to us.
And so we have had to rely on acquiring products
from other companies, anywhere from their conception
to their approval, and that has required creating and
excelling in three facilities. First, developing a demonstrably superior marketing and sales
organization, capable of reaching as many physicians as necessary, regardless of specialties,
to successfully sell virtually any pharmaceutical product. Second, establishing close and
quality partnerships with companies that have desirable products and seek precisely that
marketing and sales facility in order to enable them to participate in the worlds largest
market. And third, creating a scientific development and regulatory capacity that can take
products, some even at the earliest stages of their development, and then complete their
development and obtain the necessary regulatory approvals.

That program, which has matured over a number of years, has enabled us to establish
an array of intimate, multileveled partnerships that in their longevity, productivity and
congeniality are perhaps our most precious asset. That program reached a crescendo in the
six months from November 2005 to April 2006, in which we concluded five major
partnership transactions.
While that will obviously be a difficult pace to maintain, it confirms our particular
desirability as a development and marketing partner  and validates our business strategy.
Of course, we continue to maintain a relentless pursuit of additional quality opportunities.
We have to have many of them because we recognize that there is risk with every
opportunity until the product is approved, more risk with early stage products but still risk
even with late stage opportunities.
Two of our most recent transactions were with Gedeon Richter Ltd., the largest
pharmaceutical company in Hungary and one of the major pharmaceutical companies in
Eastern Europe. Gedeon Richter was already our partner for a novel antipsychotic, RGH-
188, which we licensed in November 2004, and which is presently in Phase I and which
we expect will enter Phase II later this year. Gedeon Richter specializes in central nervous
system products which makes it a particularly suitable partner for us because of our
extensive experience in this area. The two additional products acquired last November
include a product for neuropathic pain which has just completed two small Phase II studies,
one in the United States, and one in the United Kingdom. We are preparing that product
for a more definitive Phase II study. The other compound is a preclinical product for the
treatment of anxiety and possibly depression which should enter Phase I next year.

In January of this year we entered into an agreement with Mylan Laboratories for
nebivolol, a beta blocker for hypertension which already has an Approvable Letter from
the FDA and which we hope to launch in the first half of our next fiscal year. It was
originally developed by Johnson & Johnson. We believe it has several important features
which distinguish it from other beta blockers. It is highly cardioselective for the beta-1
receptor which means that it is likely to be better tolerated by patients. It also has a
vasodilatory effect, which is unusual for beta blockers, which results in less resistance to
blood flow in the circulatory system. Vasodilation, combined with the heart rate lowering
effect of beta-1 inhibition, is how nebivolol effectively lowers blood pressure. This
vasodilation may be the result of nebivolols effect to stimulate endogenous production of
nitric oxide, which is a known vasodilator.
In addition, in clinical studies patients levels of fatigue and sexual dysfunction were
similar to placebo, and nebivolol had comparable efficacy for African-Americans as for
Caucasians. Fatigue, sexual dysfunction and lack of effectiveness in African-Americans are
typical characteristics of the available beta blockers. And, unlike most beta blockers,
nebivolol has a neutral effect on metabolic parameters such as glucose and lipids, which
may have important clinical implications for hypertensive subpopulations, such as those
with high cholesterol or diabetes. The market for beta blockers is a large and growing market,
with several generics already available, but we believe nebivolols special advantages will
make it the preferred product for many patients.

In February we entered into an agreement with Replidyne, Inc. for a novel antibiotic,
faropenem, for respiratory and skin infections, for which the New Drug Application (NDA)
was filed on December 20, 2005. Because faropenem is a novel synthetic antibiotic, there
has been less opportunity for pathogens to develop resistance to faropenem. It also appears
to have a lower incidence of gastrointestinal problems than some of the most widely used
antibiotics. Resistance to antibiotics is a serious concern as the infectious agents cleverly
mutate to avoid one after another of our cleverly designed mutations of our antibiotics.
It is therefore necessary to continue developing novel antibiotics. Faropenems ability to
frustrate one of the most prevalent mechanisms bacteria have used to avoid other
antibiotics (production of beta-lactamases) could be a useful clinical advantage. Obviously,
it would be presumptuous to assume that faropenem will not eventually succumb to the
evolutionary ingenuity that has enabled pathogens to survive man and nature. The antibiotic
market is of course a very large market with a number of products available, but it is a
growing market in terms of prescriptions and we expect faropenem, with its unique
features, to obtain a significant share. There are some additional studies with faropenem
presently being conducted for additional indications, including pediatric use.
And finally, in April of this year, we concluded a transaction with Almirall Prodesfarma,
S. A., a leading Spanish pharmaceutical company for LAS-34273, a muscarinic antagonist
for the treatment of chronic obstructive pulmonary disease (COPD), a condition which
affects 12,000,000 people in the United States. LAS-34273 is an inhaled product developed
in a highly advanced multidose powder device. The product will shortly enter Phase III in
the United States. There are a very limited number of products that can effectively treat this
serious illness and we believe LAS-34273 alone and possibly in combination with other
drugs, both in the same device, will become a major product for the treatment of COPD.

Of the five recent product acquisitions, two are already filed with the FDA, one is just
about to enter Phase III, one has finished Phase I and is being prepared to enter Phase II,
and another is likely to enter Phase I next year.
All of those products complement an existing product pipeline which includes
milnacipran, for fibromyalgia, presently in Phase III, and desmoteplase, for stroke, which is
also in Phase IIb/III, and several products that are already in or will be entering Phase II this
fiscal year, including oglemilast, for asthma and COPD, RGH-188 for schizophrenia, and
Memantine and Neramexane, for several CNS indications. And we also have several life cycle
management programs being studied clinically this year for our principal marketed products.
We are also intrigued by the prospect of developing novel compounds for targets
identified by us. We have in-house the expertise to identify and to administer such
programs, although for the time being some of the earlier stages of molecular development
may have to be performed through other collaborators. We expect to commence a few
such projects this year. We will direct and supervise the chemistry, pharmacology and
toxicology, and then develop the compounds once they are ready to enter Phase I. These
are obviously longer term projects but we think it is time for Forest to become involved in
the earliest stages of drug development now that we have the means, the knowledge, and
an already in place repertory of products in all stages of development. We think that
initiative confirms our long term objective of supplementing our partnership programs
with original research.

Almost all the products described above have exclusivity protection for years beyond
the products we are currently marketing. At the same time, our presently promoted products
are protected beyond the expiration of this decade. We clearly recognize the necessity to
augment our profits in the near term and to replace and surpass our current products
when their exclusivity expires.
In the meantime, however, we have four currently promoted products, Lexapro,
Namenda, Benicar (promoted together with Sankyo) and Campral, all of which are growing
steadily in market share at the same time that their respective markets are also growing.
Campral is a modest product compared to our other promoted products, but it can be very
useful for those patients who seek to take advantage of its benefits to deal with alcohol
addiction. That presages increased sales and earnings for Forest, even if moderated from
time to time by milestone payments due for new product opportunities, particularly for
late stage products which justifiably require larger upfront payments, which we must
be prepared to pay. We anticipate that it will not be long until our sales and profits reach
the level when such milestone payments, even if congregated in short periods, can fit
unobtrusively in our R&D budget. The actual R&D costs for our recently acquired products,
excluding initial milestones, already fits comfortably in our ongoing R&D budget calculated
as a percentage of sales.
We believe the Companys performance this last year was a model of surmounting the
loss of a major product  a problem that all pharmaceutical companies continually face.
Our exclusivity on Celexa expired in October 2004 in our prior fiscal year at which time
multiple generic versions were launched. And yet, despite a loss of a product that was
annualizing at $1,000,000,000 prior to generic entry, our actual sales loss was only

$258,000,000, and our profits decreased 15.5% compared to the previous year primarily
as a result of the growth of our remaining products, and our earnings per share decreased
only 8% as a result of our share repurchase program. And going forward, we anticipate
earnings growth in fiscal 2007 over the prior year.
The way in which a company is managed cannot always be consistent with the objectives
of all classes of investors. Some investors have short-term objectives, sometimes very
short-term  in and out within the same day; some are longer-term, some for many years;
some investors look only for events that will cause our shares to suddenly rise, or suddenly
fall. Some move around and follow current fashions. Right now the pharmaceutical industry
is not in fashion, and what happens to one or a few companies in our industry is often
generalized to the entire industry. Others may see companies or industries out of fashion
as a buying opportunity. We manage Forest, as we always have, to achieve the greatest
continuing growth in sales and earnings with the right products in the best product areas,
and to build and maintain the best management and operational group of executives and
employees. And so we have excellent marketed products, the best in their category, and all
still growing, and we have an enviable pipeline of products which, by the time of their
approval within the next several years, we hope will continue to assure continuing growth
even as our present marketed products exclusivity ultimately expire. Of course we have
no assurance that every product will be successful, but we are assured that we will be
adding a flow of additional products to continually expand that pipeline to support the
continued growth of our earnings. Growth is obviously important to shareholders.
But it is fundamentally important to employees. It is important to retain and motivate our
existing employees, and enable us to recruit a steady source of new employees to enable
our growth to flourish.

I cant help commenting again on the appalling defects in our legal system that are
virtually unique to the United States. There is a large prosperous plaintiffs bar that
contributes very little useful benefit to anyone except themselves by bringing an avalanche
of spurious lawsuits that impose incalculable expense on almost all businesses in this
country. At its zenith that hurricane of litigation has virtually obliterated companies and
whole industries. Does anyone really believe that all or even most of those plaintiffs in
whose name class actions are brought, or the thousands who sue individually, really
suffered the alleged consequences of exposure to or the use of the various products that
are the subject of the thousands of lawsuits that have been commenced. While we have
only a very limited number of product liability suits against us, they invariably miserably
fail the straight face test, cases that it is hard to believe that any lawyer or plaintiff could
seriously bring and the same is true of suits against many businesses all over the country.
Imagine what could be accomplished if the billions of dollars and millions of man hours
that these litigations waste were used productively. It would certainly pay for better healthcare
for Americans. It might even eliminate our national debt.
Of course, there are legitimate grievances that our fraud and tort laws were designed
to alleviate, and they should properly be used for that purpose. It is the feeding frenzy of
baseless lawsuits that burdens business, the court system and ultimately the consumers
who end up paying for it one way or another.

And then there are the patent challenges encouraged by the Hatch Waxman law which
rewards a generic company which successfully challenges a patent on a branded product.
The law was well intentioned  to encourage challenges to some of the frivolous patents
that branded companies have sometimes used to extend their exclusivity. But it has resulted
in challenges to every patent in sight regardless of the merits and imposed another vast
legal expense in the system. One of those cases is the challenge to our Lexapro patent,
which was tried in March. We continue to have confidence in the validity of our patent.
And, of course, the media barrage against pharmaceutical companies has continued
during this last year. It is in the nature of media to relish uncovering scandal and vice, and
if the real thing isnt available, second best is to exaggerate or invent altogether the
shocking stories that attract readers. Certainly the real thing is sometimes there and the
medias stridency is often necessary to obtain warranted attention. But dont drugs do
something else other than cause side effects? Are we living longer with less pain and less
depression and mood disorders through remarkably swift evolution, or is it the result
of the deciphering of human illnesses and the development of useful remedies by
pharmaceutical companies? We are fascinated by man strolling on the moon, but, in fact,
the discovery of penicillin and the technology to produce it and the discovery of drugs to
alleviate pain, hypertension, schizophrenia, depression, respiratory disease and so many
other afflictions, are at least as brilliant and vastly more important achievements. Other
achievements, also stunning and wonderful, are being developed around us all the time by
scientists and research laboratories and by pharmaceutical companies.

Of course there are abuses and excesses and mistakes by pharmaceutical companies as
there are by every industry and every arm of government because we are all programmed
to be imperfect and so everything we do has something wrong with it. Maybe it is because
all of us who are flawed are busy correcting everyone elses flaws that we are able to achieve
the balance that enables us to function together in a democratic civilized society. But
within that society the pharmaceutical industry, with all its own flaws, at the most basic
level, makes perhaps the most significant contribution to human welfare and survival.
I was asked recently by a large investor: Tell me, who were the people responsible for
Forest obtaining the latest product acquisitions, because it was such a major strategic
achievement. The only correct response is that we achieve those opportunities because
of what Forest is as a totality, because of our past and current achievements in marketing
and science, and because of our reputation for integrity and fair dealings with our
partners, and based on the wide range of personal contacts that take place during the
entire identification, negotiation and diligent review of each transaction before it can be
consummated. All these are what makes each additional step forward possible and assures
our continuing progress. And all of this is the accomplishment of all of our employees 
the ones upfront and the ones behind; the ones who turn our commitments, our promises
and hopes into reality. They make Forest an alluring and reliable partner. It is all our
employees who make us what we are; they are why we are successful, why we are able to
devise and execute the strategies and solve the endless array of problems that every
business has. And so, as I have said so many times, we are deeply indebted to all of our
employees for where we are and the promise in our future.

Forest was incorporated in 1956  just 50 years ago.
None of the present management, products or personnel
were around at that time. Forest was certainly a very
different company then. The history of its founding by a
survivor of a Nazi concentration camp, its subsequent
financial travails and its ultimate success is a long and at
times poignant story. But what I have tried to make clear
in this letter is what Forest is today, and what we expect
it to be in the future  a future that all of us at Forest
are rededicated to on this fiftieth anniversary of the
Companys founding.

Howard Solomon
Chairman & Chief Executive Officer
Kenneth E. Goodman
President & Chief Operating Officer