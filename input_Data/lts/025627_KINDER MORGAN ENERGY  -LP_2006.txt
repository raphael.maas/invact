                                                  Our vision was to
                                                                       build a different type
     Kinder Morgan Energy Partners (KMP) celebrated its                of energy company by
10-year anniversary in February of 2007. It has been, and              utilizing the master limited
continues to be, an exciting ride! We have grown KMP from              partnership structure
a company with a few small assets and an enterprise value              as a growth vehicle,
of about $325 million in 1997 into one of the largest energy           something that had really
transportation and storage players in the industry with an             never been done before.
enterprise value of more than $16 billion.                             We committed to being
     I'm proud of KMP's superb growth and the exemplary                lean and frugal, focusing
portfolio of assets that we have assembled over the past decade,       on being a low-cost asset
which has enabled us to deliver strong returns to you  an             operator and generating
average annual return of 30 percent to unitholders. Our future         maximum returns to our
growth opportunities also appear to be exceptional. Sources            unitholders. That philosophy still holds true, as we continue
of supply for energy products in North America continue                to allocate capital in a disciplined way, eliminate perks and
to shift, which means there is a need for additional energy            needless corporate overhead expenses, and relentlessly pursue
infrastructure. That is our forte  building and operating             both financial and operational excellence.
midstream energy assets  and we plan to invest approximately                Over the past decade, we have been fortunate to acquire
$6.5 billion in new infrastructure and expansion projects at           some premier energy assets that have produced outstanding
KMP over the next four years alone.                                    growth at KMP due to both the markets in which they operate
     In keeping with our tradition of watching your dollars,           and the expansion opportunities that they have presented.
my letter is relatively short. If you would like more detailed         Combined, we have invested more than $11 billion in
information, I encourage you to review the attached Form 10-K,         acquisitions and expansions through 2006. We have also
along with the annual investor conference presentation and our         invested substantial capital in maintenance, repairs and integrity
news releases which are posted on our web site,                        management programs to ensure our assets operate safely.
www.kindermorgan.com.                                                        KMP now owns an interest in or operates approximately
                                                                       26,000 miles of pipelines and over 150 terminals. In the United
                                                              States, we are the largest independent transporter of refined
                                                                       petroleum products, a major transporter and storage operator
                                                                       of natural gas, the largest transporter and marketer of carbon
      KMP was formed Feb. 14, 1997, when a group of investors
                                                                       dioxide for enhanced oil recovery projects, and the largest
headed by Bill Morgan and me acquired the general partner of
                                                                       independent terminal operator.
Enron Liquids Pipeline, L.P., a small master limited partnership
                                                                             So, how have investors fared over the past 10 years? A
which began in August of 1992. We developed a strategy that
                                                                       $1,000 investment in KMP units on Jan. 7, 1997, (the day
the company still follows today: focus on stable, fee-based
                                                                       before the announcement that investors, including myself, were
assets which are core to the energy infrastructure of growing
                                                                       acquiring the general partner) purchased 36.36 units. KMP has
markets; increase utilization of assets while controlling costs;
                                                                       had two, two-for-one unit splits since then, so an investor who
leverage economies of scale from incremental acquisitions and
                                                                       purchased then and held all of his units would now own 145.45
expansions; and maximize the benefit of a unique financial
                                                                       units. Since Jan. 7, 1997, the investor would have received over
structure that fits with this strategy.
                                                                       three times his initial investment ($3,192) in distributions. For
                                                                       2007, the investor would expect to receive $500 in distributions
                        another 50 percent of his original investment  based
                                                               $3.26
                                     $3.13
                                                                       on KMP's 2007 budgeted distribution per unit of $3.44.
                                               $2.87                   Additionally, the investor would still own the units, which
                                                                       would now be worth over seven times his original investment
                                                                       ($7,332) based on the closing price of $50.41 on Jan. 31, 2007.


                                                                       2006
                                                                            Overall KMP had a good year, producing total segment
                                                                       earnings before depletion, depreciation and amortization
                                                                       (DD&A) of $1.9 billion. While this represented 8 percent
                                                                       cumulative growth compared to 2005, it fell short of our
                                                                       2006 published budget of 12 percent growth. The natural
                                                                       gas pipelines and terminals segments produced outstanding
                                                                       results, but earnings were less than expected from the products
                                                                       pipelines and CO2 segments.
                                                                            segment volumes increased by 1.6 percent and revenues were up
                                              By the end of
                                                                            5.5 percent over 2005.
                                        the year, we increased
                                                                                  This business segment continues to offer numerous growth
                                        KMP's quarterly
                                                                            opportunities, serving seven of the 10 fastest growing markets
                                        cash distribution per
                                                                            in the United States. In 2006, we increased pipeline capacity
                                        common unit to $0.83
                                                                            for gasoline, jet fuel and diesel into Arizona by completing
                                        ($3.32 annualized),
                                                                            a $210 million expansion of our East Line, which delivers
                                        a 4 percent increase
                                                                            fuel from El Paso, Texas, to Tucson and Phoenix, Ariz. An
                                        over the 2005 fourth
                                                                            additional $145 million expansion of the East Line is expected
                                        quarter cash distribution
                                                                            to be completed in the fourth quarter of 2007. We have also
                                        per unit of $0.80. This
                                                                            approved spending over $400 million to expand and upgrade
                                        was the 28th distribution
                                                                            the CALNEV pipeline system to meet growing demand
                                        increase in 10 years.
                                                                            in Las Vegas, Nev. The majority of this expansion involves
                                        In total, KMP declared
                                                                            construction of a new pipeline and is subject to permitting and
2006 cash distributions of $3.26 per unit compared to
                                                                            the receipt of regulatory approvals authorizing rates that make
$3.13 per unit for 2005, but below our published budget
                                                                            the project economically viable. Additionally, we entered into
of $3.28.
                                                                            an agreement with affiliates of BP to increase our ownership
      Because KMP failed to hit its 2006 budget target, we had
                                                                            stake in the Cochin pipeline system from almost 50 percent
no obligation to fund the employee bonus plan. However,
                                                                            to 100 percent. The transaction is subject to standard closing
the board of directors of Kinder Morgan, Inc. (KMI), the
                                                                            conditions and is expected to be completed in the first quarter
owner of the general partner of KMP, determined that it
                                                                            of 2007. Pending board approval, we also intend to acquire the
was in both companies' long-term interests to fund a partial
                                                                            Trans Mountain pipeline system from KMI during the second
payout of KMP's bonuses through a reduction in the general
                                                                                            quarter this year. Trans Mountain transports
partner's incentive payment. Thus, KMI waived
                                                                2007 BUDGET ED              crude oil and refined products from Edmonton,
approximately $20 million of its incentive payment
                                                         DIS TRIBUT ABLE CAS H FLOW
                                                                                            Alberta, to marketing terminals and refineries in
for the fourth quarter, which equates to KMP's
                                                                                            Vancouver, British Columbia, and
bonus payout for 2006 and represents about
                                                                                               Washington state.
75 percent of its full budgeted bonus payout.
                                                                                                    The Natural Gas Pipelines segment
Four of our senior corporate officers, at their             Natural Gas      CO2
                                                             Pipelines                         generated 2006 earnings before DD&A of
suggestion, received no bonus payout for 2006.                               28%
                                                                25%                            $554.9 million, up 11 percent from 2005
My salary remains $1 per year, and I continue
                                                                                               and significantly ahead of our published
to receive no bonuses, stock options or
                                                           Terminals                           budget. Growth for the year was driven
restricted stock.                                                       Products
                                                              20%       Pipelines            by another outstanding performance
      During 2006, we invested approximately
                                                                           27%             from the Texas Intrastate Pipeline Group,
$900 million in capital expansions and
                                                                                         which experienced improved sales margins on
acquisitions, and we continued to make significant
                                                                                  renewal and incremental contracts and higher value
progress on large projects that will spearhead future
                                                                            from storage activities. This segment also benefited from an
growth. Additionally, we outperformed the industry averages
                                                                            increase in transport volumes, along with good results from the
in virtually all environmental, health and safety categories, as
                                                                            KMIGT and TransColorado pipelines, and the Casper Douglas
detailed on our web site. Believing that transparency drives
                                                                            and Red Cedar gathering and processing businesses.
accountability, we also continue to publish our annual budget
                                                                                  While we have pipeline and storage expansions planned
on our web site, so investors and the public are able to track
                                                                            across this entire segment, the big news is three large natural gas
both our financial and operational performance.

                                                                                                          K M P: 30 % A NNU AL RE TU RN
BUS IN ES S S EGMENT S                                                                                                                                            $1,346
                                                                                                        KMP
      Our portfolio of primarily fee-based assets, which are
                                                                                                        MLP Index
essential to our country's energy infrastructure, continues to                                          S&P 500
produce strong, reliable cash flow. The following is a summary
of how our business segments performed in 2006 and their
future growth prospects.                                                                                                                                            $515
      The Products Pipelines segment produced 2006 earnings
before DD&A of $501.9 million, down slightly from 2005
                                                                                                                                                                     $225
and well below our published budget. The majority of this
shortfall was due to environmental expenses and rate reductions
that went into effect on the Pacific system for the SFPP rate
case. The segment also was impacted by lower than expected                   Returns calculated on a daily basis through Dec. 29, 2006, assuming dividends/distributions rein-
                                                                             vested in index/stock/unit, except MLP Index calculated on a monthly basis. Start date 12/31/1996.
results from Plantation Pipe Line. Excluding Plantation, 2006
pipeline projects that we continued to move forward in 2006.                          include developing a new CO2 source field and adding
Rockies Express is a $4.4 billion joint venture that will move                        infrastructure at the McElmo Dome and the Cortez Pipeline.
natural gas from the prolific producing basins in Wyoming and                         Additionally, we are exploring various CO2 opportunities
Colorado to eastern Ohio. The 1,663-mile project will be one                          in Canada, including capturing CO2 that would otherwise
of the largest of its kind ever constructed in North America                          be released to the atmosphere and transporting it for use in
and will transport up to 1.8 billion cubic feet per day (Bcf/d)                       enhanced oil recovery projects.
                                                                                            The Terminals segment produced 2006 earnings before
of natural gas. The first 328-mile leg of the project in the
                                                                                      DD&A of $396.9 million, a 25 percent increase over 2005 and
Rocky Mountains is in service and the full project is expected
                                                                                      ahead of our published budget. Growth was driven by internal
to be completed by June 2009, pending regulatory approvals.
                                                                                      expansions, incremental volumes and acquisitions.
The $1.2 billion Midcontinent Express Pipeline, another
                                                                                            Acquisitions in 2006 included a rail terminal in southern
joint venture, is an approximately 500-mile pipeline that will
                                                                                      California that is one of the largest ethanol unit train handling
have the capacity to transport about 1.4 Bcf/d of natural gas
                                                                                      facilities in the United States, a transload business located
from Bennington, Okla., to Butler, Ala. The pipeline will
                                                                                      primarily in the Midwest that will increase our steel handling
move gas out of the Midcontinent and Barnett Shale areas
                                                                                      capabilities, and a company with expertise and technology that
and the anticipated in-service date for the entire system is
                                                                                      will enable us to pursue opportunities in the growing sulfur
the end of February 2009. The $500 million Kinder Morgan
                                                                                      market.
Louisiana Pipeline will provide 3.2 Bcf/d of take-away capacity
                                                                                            We invested approximately $264 million in expansion
from the Cheniere Sabine Pass liquefied natural gas plant
                                                                                      projects in this segment in 2006 and plan to make an additional
under construction in Louisiana. The 137-mile pipeline will
                                                                                      $400 million in capital investments in 2007. The expansions
interconnect with various interstate and intrastate pipelines,
                                                                                      include building additional storage tanks for refined petroleum
and the entire project is expected to be in service in the second                     products, adding infrastructure to expand our coal operations
quarter of 2009. All of these projects are supported by long-term                     and increase our ethanol handling capabilities, and constructing
binding contracts with creditworthy shippers.                                         storage tanks and infrastructure to accommodate the growing
      The CO2 segment delivered 2006 earnings before DD&A                             biodiesel market. In addition, our first terminal facility in
of $490 million, up 4 percent from 2005 but well below our                            Canada, a fully-subscribed crude oil tank farm in Edmonton,
published budget. With the exception of the SACROC Unit,                              Alberta, is expected to come on-line in the fourth quarter this
however, this segment had a strong year. Results reflected                            year and will have storage capacity of nearly 2.2 million barrels.
stronger than expected oil production at the Yates Field and
record annual delivery volumes from the CO2 and Wink
                                                                                      OUT LOOK
pipelines. Average oil production year over year increased
by almost 8 percent at Yates to 26.1 thousand barrels per
                                                                                            As I say every year, like all companies, KMP has its share
day (MBbl/d), but declined by 4 percent at SACROC to
                                                                                      of challenges. Regulatory matters, CO2 crude oil production,
30.8 MBbl/d. This segment is one of the few areas where KMP
                                                                                      construction cost overruns, environmental issues, terrorism
is exposed to commodity price risk, but we mitigate that risk
                                                                                      and interest rates are among the risks that we must effectively
through a long-term hedging strategy designed to generate more
                                                                                      manage.
stable realized prices.
                                                                                            For 2007, we expect KMP to declare cash distributions of
      Looking ahead, we plan to invest approximately
                                                                                      $3.44 per unit, an increase of about 6 percent over 2006. We
$445 million in 2007 to further develop production at
                                                                                      expect to grow the distribution per unit by about 8 percent for
SACROC and Yates and to expand our CO2 operations in
                                                                                      the next several years beyond 2007. Combined, the growth in
southwest Colorado. The Colorado expansion activities will
                                                                                      distributions per unit and a yield in the 6 to 7 percent range on
                                                                                      the units equates to an expected annual return to unitholders in

                                                                                      the low to mid teens.
                              $1,893
                                
                                                                                            As noted throughout my letter, KMP has secured and
                                                        continues to pursue many growth opportunities, and we believe
 $1,618
                                                                                      the company is well positioned for the future. We also pledge
                                                                                      to continue to maintain a solid balance sheet and invest your
                                                           $1,343
                                        $1,261                                        money wisely. I would like to thank our dedicated employees for
                                                                     $1,143           successfully executing our vision and strategy, our customers for
              $1,065 $1,020
                                                                                      utilizing our assets and serving as the catalyst for us to expand
                                                                               $885
                                                  $873
                                                                                      and grow, and our investors for your longstanding support. I
                                                                                      truly believe the best is yet to come!




                                                                                      Richard D. Kinder
                                                                                      Chairman and CEO
 
