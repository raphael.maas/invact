Shareholder Letter

Dear Shareholders, Customers, Partners, and Employees:
Fiscal 2003 was a year of important and exciting innovations, reflected in many new Microsoft products that advance our mission of enabling people and businesses throughout the world to realize their full potential.

Despite challenging economic conditions, revenue increased by $3.82 billion to $32.19 billion, and operating income grew by $1.31 billion to $13.22 billion. Revenue in each of Microsofts seven business segments grew by 11 percent or more.

During the year, the companys senior leadership refined our strategy for achieving continued growth and success in the years ahead. To help you understand our approach to the possibilities before us, we would like to outline our business plan.

A challenging environment
Because Microsoft is making substantial investments today to enhance the power and value of an integrated computing platform, we are optimistic about the companys long-term growth opportunities. Like any business, however, we face challenges in the near- and mid-term. We are addressing them head-on. 

One of the most important challenges lies in serving business customers. Having recently invested in Y2K upgrades and the Internet, businesses want their future technology investments to do more and cost less. Many enterprises are migrating their server networks from proprietary hardware running the UNIX operating system to less expensive alternatives built on Intel microprocessors. Microsoft can benefit from this shift because we provide customers with comprehensive, end-to-end technology solutions and unmatched value. Last year, our server and tools revenue increased 16 percent.

Some organizations migrating from UNIX are considering noncommercial software such as Linux and OpenOffice. While the initial cost of acquiring a stripped-down, do-it-yourself operating system may seem attractive, a growing body of independent research shows that our integrated platform provides not only greater functionality but also lower total cost of ownership in most common business functions.

Today the Windows platform offers unmatched simplicity, security, choice of applications, and overall business value. Tomorrow the Windows advantage over the competition will continue to widen as we continue to invest in innovation.

Still, Linux and other noncommercial software present a challenge, and we are not complacent. We are working hard 
to ensure that our products and services continue to improve and meet customer demands for value. We are committed to exceed our customers expectations for product reliability, security, and engineering excellence.

In order to take computing to the next level for customers, our business plan identifies six things we will do: 

	Provide breakthrough, high-quality integrated innovation;
	Deliver best-in-class responsiveness to customers;
	Make our platform the best for developers;
	Deliver simple, high-value experiences and services;
	Ignite customer enthusiasm and tell the Microsoft story; and
	Build our talent pool and increase productivity.

Integrated innovation
We believe that customers will find enormous value in a truly integrated, affordable software platform that takes the complexity out of computing. We are positioning Microsoft to deliver on the promise of this vision. In FY04, the company plans to invest $6.8 billion in R&D innovation.


To continue to strengthen our value proposition, we are focused on the following objectives:

	Continue to advance the development of an end-to-end technology platform that seamlessly integrates hardware and software from the enterprise to the desktop to wireless mobile devices;
	Address security, manageability, and reliability;
	Provide high-value and easy-to-use technologies that support customer scenarios right out of the boxwith little or no customization;
	Continue to support and grow a worldwide ecosystem of complementary hardware, software, and services partners;
	Achieve strong integration between our products and services, and applications on other systems, through standards 
of interoperability and XML Web services; and
	Ensure that we protect and derive value from the intellectual property we create.

A great example of the kind of value we see in integrated innovation is Windows XP Tablet PC Edition. Released last fall, it combines all the power of Windows XP with true mobility and advanced ink and speech tools for a more dynamic and flexible computing experience. Today, there are 40 computer makers that offer versions of the Tablet PC, and independent software vendors have created exciting applications for it.

Another great example is Windows Server 2003, the flagship of our integrated platform for business, which helps organizations be more agile, productive, and secure. It incorporates important new security and reliability features, and advances in management, applications platform and information worker support, not to mention a #1 ranking in scalability.

Exchange Server 2003, released June 30, offers new levels of integration in server and client capabilities, enabling organizations to easily create and manage business communications both in the office and on the road. Microsoft Office 2003, to be released this fall along with new applications in the Office System, will fundamentally change how people communicate, collaborate, and manage their information.

As we look to the future, Longhorn, the next generation of Windows, is our big investment in driving a new wave in computing, including a new applications platform, new end-user functionality and services, and major advances in deployment, upgrading, management, reliability, security, and performance.

We are excited about opportunities to innovate for customers with a consistent approach in other areasin new products, services, and solutions, and in innovations to Windows and Office. An example is our new Microsoft Business Solutions CRM, which provides mid-sized businesses with powerful tools to build profitable customer relationships, improve sales, and deliver efficient service.

We see opportunities to transform how people use technology at home and on the move, with software for telephones, all the electronic devices in the living room, and personal wear, as well as games and educational software for consumers. Last year we launched many important new consumer products, including Windows Powered Smart Displays, Windows XP Media Center Edition, Windows Powered Smartphones, MSN 8, and Xbox Live.

Responsiveness to customers
Providing value to customers means not only building great products, but also listening carefully to customers, responding quickly, and being more transparent and accountable.

We responded to business customers concerns about our licensing programs with a new Software Assurance program, recently enhanced, that provides more assistance, more support and training options, more predictability, and more value 
than ever before.

We listened to customers concerns about product quality, and responded with a companywide drive for Trustworthy Computing, aimed at constant improvement in reliability, security, privacy, and business integrity. We have made significant progress, and this important work will continue long into the future.

We have intensified our focus on engineering excellence to help ensure that our products are not only more trustworthy but also more compatible, scalable, and manageable. We continue to enhance the capabilities of our products to automatically detect problems, perform diagnostics, apply known solutions, and prevent future problems. We have made progress in improving our product support operations, and we will help customers solve problems with more consistent documentation and richer Web services.

In short, we are committed to doing whatever it takes to ensure that our customers are highly satisfied with our products and services. Underscoring our commitment, we recently revised the incentive system for hundreds of our senior leaders so a significant part of their equity compensation depends on progress in the number and satisfaction of our customers.

Supporting developers
The popularity of Windows derives in large part from its support for the broadest array of applications. Microsoft has a long history of providing programmers with state-of-the-art tools and services that make it easy and rewarding to develop for our platform.

Our new Visual Studio .NET 2003 is the most advanced and complete set of software development tools, enabling developers to greatly improve their productivity with a single programming model for PC, mobile, and Web applications. It is the definitive tool set for building Web services with the .NET Framework.

We also are expanding efforts to build deeper relationships with developers. For example, we are pairing our developers 
with industry peers to help us better understand their needs and help them with their issues. And we are making Windows development more accessible to a wide range of programmers, including students, novices, hobbyists, and other nonprofessionals.

We are creating new opportunities for developers and independent software vendors (ISVs) by expanding the platform for their applications beyond Windows. We will help ISVs develop add-ons and applications for Office and other products, such as those from Microsoft Business Solutions.

We are proactively communicating with ISVs about our own application development plans so that they can choose their best opportunities, and we are extending our co-marketing and selling with ISVs. Helping ISVs find customers and vice versa can be a real advantage for us, our partners, and our customers.

Simplicity, value, understanding
One of our primary objectives in innovating the integrated platform is to provide simpler, more compelling, complete, and targeted customer experiences. We are adding more capabilities to existing products, and segmenting our product offerings to deliver the simplest, most valuable package of our technologies, at the right price, for specific customer sets such as small businesses, students, home users, and information workers at home.

The right mix of our technologies, offered as services in the right set of segmented offers, will dramatically enhance the value delivered to customers.

Many customers and partners have said they would appreciate our innovations more if they knew Microsoft and its people better. We are encouraging our developers to participate more in the communities around their products, and we are communicating more broadly about our work in important areas such as security, privacy, and rights management.

Executives, too, are spending even more time listening to customers not just to technology managers, but to IT users, developers, and even schoolkids.

We also have increased our advertising budget, communicating in ways that illuminate the amazing work we and our partners do, and explaining our mission to help people realize their potential.

Excellence in people and processes
As a company whose primary assets are intellectual property, we are constantly focused on attracting and retaining the people who create it, the top talent in our industry. We had great success last year as we expanded our recruitment and career-development efforts in core areas such as product development, marketing, finance, sales, and operations.

Because Microsofts success depends entirely on the great ideas of our employees, we are committed to continuing to provide a diverse workplace where people feel at home, have an opportunity to develop their skills, and find new and interesting opportunities.

In July, we took steps to enhance employee compensation. We are no longer issuing employee stock options, but instead granting stock awards that vest over several years. This will offer employees more stable equity compensation, and will better align employee and shareholder interests.

While we continue to invest for the future, we also continue to search for ways to reduce overhead and become more efficient. We recently named chief financial officers for each of our seven business segments to help further improve our financial management.

The path forward
We believe our business plan puts us on a sure path to achieve our goals. All our actions are aimed at putting the customer first. Doing so, we believe, is critical to our continued success.

Making technology easier and better for customers was our inspiration 20 years ago, when Microsoft announced its graphical user interface for personal computers. We called it Windows, and it helped revolutionize computing. Today, we are developing a range of integrated products that will again transform how people work, learn, communicate, and are entertained. We are totally focused on our mission of enabling people and businesses throughout the world to realize their full potential. We deeply appreciate your support for our efforts.


Bill Gates
Chairman and Chief Software Architect


Steven A. Ballmer
Chief Executive Officer 


