To Our Shareholders
As we look back at 2011, the one word that best defines our year is
transformation. While we worked throughout the year with our characteristic
and unwavering commitment to safety and operational integrity, we also acted
aggressively on our conviction that the pursuit of our vision of being the best offshore
driller required a shift in both strategic and tactical execution. So we worked with purpose
on many fronts to define our goals and put ourselves in a position that would ensure our
future success.
The Noble fleet is changingas it must to keep pace with customer demands
for higher-spec units. We began 2011 with six rigs under construction consisting
of two Bully-class and two Globetrotter-class drillships along with two JU3000N highspecification
jackups ordered late in 2010. During 2011, on the strength of our balance
sheet and backlog we placed new orders for four more ultra-deepwater drillships and four
additional JU3000N jackups, taking total newbuild project commitments at one point in
the year to 14. All of these projects are well priced to provide shareholders with excellent
returns for years to come.
With the departure from the shipyard of the Noble Bully I in September and the
Noble Bully II and Noble Globetrotter I in December we ended 2011 with 11 newbuild
shipyard projects in progress and three rigs in various stages of acceptance testing prior
to commencing contracts with Shell.
The remaining projects have what we believe to be very attractive delivery points
in late 2012, 2013 and 2014. Given our outlook and current assumptions regarding the
future of offshore drilling activity and customer requirements, we are confident in our
ability to secure contracts that support these investments. In fact, we view this capacity
as a distinct advantage going forward.
The geographic presence of Nobles fleet is expanding. In 2011, we successfully
moved into several new offshore regions as we looked to diversify our operations
geographically. For example, we recently opened an office in Anchorage, Alaska to support
our client with their planned Arctic drilling program, which is currently scheduled to
commence in 2012.
We also opened an office in Singapore as a starting point to support our expansion
into Southeast Asia, which will begin in earnest later this year with the arrival of the
Noble Clyde Boudreaux in Australia. We believe this broad region will continue to provide
tremendous opportunities for shallow and deepwater units, including our newbuild
drillships and jackups.
We also expanded our presence in Saudi Arabia during the fourth quarter of 2011 as
the jackups Noble Gene House and Noble Joe Beall commenced three-year contracts for
Saudi Aramco. We were recently awarded another three-year contract, bringing to five
the number of rigs committed to Saudi Aramco. Prior to 2010 Noble had no rigs working
for this customer.
Nobles culture of safety and operational integrity defines our approach to
every job. Nobles seven operating divisions completed 2011 with their second best safety
results in the Companys nine-decade-long history, yet it stopped short of our goal of zero
incidents. But with a growing work force there is much to do. Instilling our culture of
safety and operational integrity into each of the more than 1,300 new employees who
joined our ranks last year is our top priority and considered job one.
Underscoring all of our efforts is our long standing and ongoing commitment
to continually improve the safety leadership skills and enhance the competence of all
our rig based teams, while reinforcing their accountability for ensuring personal and
process safety. The goal of ensuring personal safety for our crews is inseparable from our
commitment to improving process safety management and is an integral part of further
advancing our overall safety culture.
From an operational integrity perspective, in 2011 we centralized the management of
our subsea teams and equipment, dramatically increased the amount of capital devoted to
the purchase of subsea spare components and more than doubled our subsea specialists to
more efficiently and effectively maintain, service, repair and operate our critical subsea
systems. These efforts are already allowing us to deliver an enhanced level of well control
management and have the potential to dramatically improve our downtime experience.
The drivers of our business are changingfor the better. The offshore drilling
industry continues to experience solid signs of recovery in both the shallow and deepwater
sectors. This recovery is occurring in multiple regions of the world and two fundamental
measures of our industrys health, utilization and backlog, are on the rise.
The stability of crude oil prices has resulted in increased confidence amongst our
customers and that confidence should lead to higher exploration and production spending.

Perhaps as important as stable oil prices is the rate of exploration success. Last year there
were 23 announced discoveries in an average water depth of 6,200 feet representing
the sixth consecutive year of 20 or more announced discoveries and among the deepest
average water depths ever reported.
These discoveries were registered in 12 different countries, an indication of the rapid
geographic expansion underway as global geologic prospects are better understood.
Successful exploration results are expected to lead to a greater mix of appraisal and
development drilling programs in the years ahead which will be another important
fundamental driver of future demand in the deepwater sector throughout the world.
Backlog levels support our growth plans and underscore our customers
confidence in Noble. Contract backlog has once again begun to expand. Since the end
of the third quarter when the measure stood at $12.8 billion, we have increased backlog
by $900 million with new contracts in Mexico, the North Sea, the Eastern Mediterranean,
Australia, and Africa bringing the total to $13.7 billion at year end. This backlog breaks
down into $11.7 billion for floaters and $2 billion for jackups.
Our backlog provides a base of revenues of at least $2.5 billion per year for each of the
next three years and does not reflect the significant growth potential from uncontracted
newbuilds, including our three ultra-deepwater drillships and six high-specification
jackups under construction, all scheduled for delivery before the end of 2014.
Notwithstanding this enviable position, the Company enjoys a comfortable balance
between contracted and available rig days. In our floating fleet, at January 31, 2012,
we had approximately 2,100 available rig days remaining this year, compared with
approximately 4,000 days for our jackup fleet. Given our view of the market potential this
year we believe these available operating days will prove valuable in 2012 and beyond
should dayrates continue to improve.
Our belief that people make the difference is unchanged. Since our founding
more than 90 years ago, Noble has recognized that the secret to our success is not just our
rigs, but the people who make them run. People are essential to our ability to compete and
without the talent and effort of our staff of more than 7,100 highly skilled and motivated
personnel, there would be no Noble.
In 2011, we continued to work hard to attract and retain the best
and brightest people whom we need to deliver value and grow our
business. We had a record year in terms of adding employees to our
ranks. Equally as important, we remain focused on the retention
of our talented team members who rightfully enjoy the reputation
as being among the best in the offshore drilling industry. Even in
this time of rapid growth in our industry, turnover among our top
tier rig employees remained near historic lows.
We have enhanced our ability to provide continuous training and attractive growth
opportunities for all of our team members, enabling them to pursue a career path that
best provides for their future and meets the Companys changing requirements. Notably,
technology plays an ever-increasing role in our business. For example, we can now
offer highly advanced simulator-based training wherein team members train to operate
equipment in a virtual environment. In 2011, we provided tens of thousands of hours of
training to our team, a number we expect to continue to increase in the coming years.
As we enter 2012, I believe a newly energized and responsive Noble is
emerging. Thanks to the tireless efforts of the Noble team, both shore-based and
offshore throughout the world, our goal of helping to meet our customers need for safe
and efficient drilling services with advanced drilling technology is on track and moving
ahead at an accelerated pace.
With our increasingly modern fleet, operated by our highly-trained, motivated and
experienced teams committed to operational integrity, I am excited about the Companys
prospects and confident in our ability to deliver service to our customers and value to our
shareholders in the years ahead. Thank you for your continued support.
David W. Williams
Chairman, President and
Chief Executive Officer