I think the first subject a companys report to
shareholders these days should deal with is integrity
in business. Management obviously needs wide discretion
in running a business, because it is most familiar with
the vast amount of information that goes into making
daily business decisions. But there can be little leeway
when it comes to integrity in dealing with investors
and employees. Management, even at its best, makes
its full share of mistakes, and sometimes investors will
lose money or employees lose their jobs because of
management errors. But whatever the tolerable range
is for quality of management performance, there is no
latitude that permits compromising integrity.
Just recently I discussed this subject with over 2,000 of our employees at
our launch meeting for LexaproTM, and I would like to repeat for shareholders
part of what I told them.
Business in the United States operates under an economic system, called the
capitalist system or the free market system. It is the system which, on the one
hand, has produced vast wealth and prosperity for our country, surpassing all
other systems and other countries in history. It has also facilitated abuses that
recently reached their nadir in the Enron disaster. That event, in turn, illuminated
abuses more pervasive than just that one company  abuses practiced by many
companies, sometimes facilitated by their accountants, lawyers and bankers 
abuses like earnings distortions, and claiming assets that do not exist and hiding
liabilities that do exist  abuses perhaps not as great in scale or ingenuity as
Enron, but abuses nevertheless that have harmed investors, employees and the
public, and in some cases irreparably harmed them. Curbing those abuses may
require some institutional or regulatory changes. But it will also require renewed
individual commitment to basic ethical standards.
The Enron affair illustrates how important it is, and how difficult it is, even in an
advanced political and economic system such as ours, to control greed that is
ingenious enough and that takes no account of any responsibility to anyone else.
Of course, we know that egotism and self-interest are part of the formula we
are all made of, and those instincts undoubtedly are in part responsible for the
survival of the human species through the rigors of natural selection. But it is
also true that civilization  that prized and glorious achievement that enables us
to live peacefully in vast communities  is founded on the systematic control of
our more animalistic instincts.
And that control is formalized in our laws, as well as in the habits of mutuality and
in the virtues that our parents, our religions and our own experiences have taught
us. We have learned that it works better for all of us when we live by the communitys
rules. We are free to try to change them, but as long as they are there they define
the ethical behavior the community expects and we have to live by them.
But laws and accounting rules are really crude, simplified generalizations to deal
with complex systems, and either we enter into their spirit if we are essentially
civilized, or try to twist our way around them if we are essentially uncivilized.
Most of us stay well within the boundaries, either because of caution or conscience.
And we have learned, or should have, that pushing against the boundaries is dangerous
business, because sometimes if what you are doing is essentially antisocial,
even though it looks technically legal, the boundary can move and ensnare you.
And that is what is happening today to some managements and their
professionals and bankers who pushed too hard against the laws intent.
And therefore, you should know that at Forest we stay safely within the boundaries.
We believe  and have proven  that you can develop a successful business and
stay within the boundaries of law and ethical behavior. That doesnt mean we
dont pursue our own self-interest, because of course we do, and it is perfectly
right that we should. There is nothing wrong with the profit motive; we pursue it
aggressively, and enjoy its rewards. But there is ample room within legal boundaries
for profit  even for high profit margins  so long as we are law abiding and
responsible to each other.
The problem sometimes starts with the shorthand that Wall Street uses to
evaluate public companies, which places pressure on managements to squeeze
into that shorthand or risk the analysts wrath. The passion to please Wall Street
and the formulaic way analysts are programmed can sometimes seduce
managements to compromise candor. But clearly the egregious cases that
are being identified these days are not just bending a little to accommodate an
imperfect standard; they were specifically designed to distort the operating
activity and value of the company.
The second subject a pharmaceutical company report this year should deal with
is healthcare in America. The beginning of wisdom on that subject is to accept
that healthcare in the United States is going to cost more and more, and that
this is wonderful, because it is the result of major advances in the understanding
of human biology and chemistry, in diagnostic and surgical procedures, in
medical treatments, and in the development of new and better drugs. All these
together have achieved benefits undreamed of as recently as a few decades ago.

Whatever the
tolerable range
is for quality
of management
performance,
there is no latitude
that permits
compromising
integrity.
And because of all these achievements, human longevity is prolonged and will
be prolonged more and more in the future, with the result that more and more
older people will require more and more medical care, including drugs, to remain
healthy and to have quality of life.
As for the drug part of the equation, the human body, after all, is an infinitely
complex chemical apparatus, and drugs are chemicals that can affect that
chemistry. If we knew enough we could make a human being out of chemicals.
But the bodys chemistry is shrouded in secrecy and we have, at best, unraveled
only the tiniest part and probably will never unmask all or even most of it.
It takes vast resources and brilliant and tedious efforts to discover how to
beneficially affect even the smallest part of our elaborate machinery.
Once we accept and understand the health bounty that is increasingly available,
and once we realize that we, the American people, want to enjoy its fruits,
we have to accept that it must cost more. We must realistically be prepared to
devote more and more of our national wealth to the enlargement of healthcare
benefits. We have to pay doctors more, not less, and hospitals more, not less,
and reward pharmaceutical development where appropriate. Short of the
national defense of our free, democratic society, there is no need deserving
greater revenue commitment than our health. And, of course, the government
has to provide for the cost of medical care where individuals are not otherwise
provided for, which means that all the rest of us have to contribute our share.
If we scrimp on physician and hospital and drug reimbursement we will impair
or deny the medical care every American should be receiving. The politicians
who keep inveighing against rising healthcare costs are flying in the face of
the increased need and increased opportunity.
Regarding the role of pharmaceutical companies in healthcare, it simply has to
be recognized that what we produce is more beneficial by far for the American
people than any other industry. We prolong life; we reduce pain; ultimately we
facilitate happiness that otherwise would be unattainable. But it is not easy.
It is highly risky, vastly expensive, and it requires scientists with years of training
and brilliant insight, and the commitment of enormous resources for years
before even the prospect of an adequate return. When, after all that effort and
time and risk and cost, it is finally well done, the reward is a higher profit
margin than a supermarket that anyone could open tomorrow. That is because
pharmaceutical creativity is protected for a finite period by our patent system
without which those prodigious efforts would not occur at all. That brief
exclusivity is compensation for the benefits achieved and the risk and cost
incurred to achieve them.
Of course there are overreaching business practices that some pharmaceutical
companies sometimes utilize, such as selling too hard, charging too much, or
taking advantage of consumer ignorance with overstated direct-to-consumer
advertising. And, of course, it is appropriate to criticize, and in a proper case,
to take action against such excesses but, at the same time, to realize that all

businesses have comparable excesses. And maybe that is the way our economic
system works because it is managed by a flawed species and not by saints  or,
as Winston Churchill said about democracy, it is the worst form of government
except for all the others. As an industry, we are no worse than any other industry,
including the media themselves, which sometimes feast on the pharmaceutical
industry if that flavor sells. In fact, we are probably more benign than many
others because our business practices  our claims and our marketing  are so
closely regulated by the government unlike most other businesses, including the
media which has a constitutional amendment to protect it.
So far as Forest is concerned, this last year again has been our best ever,
principally due to the blockbuster success of CelexaTM, which achieved sales of
$1,100,000,000 for the year, and sales of $313,000,000 in our fourth quarter.
Celexas market share had the greatest growth in the category last year increasing
from 14.2% to 17.0%. And the entire category continues to grow at 15% per
annum. Total sales of Celexa therefore grew last year by 52.3% over the prior year,
and the product has years before it faces generic competition. And yet, in fiscal
2003, we are going to stop promoting it. And the reason is that we have a much
better product, Lexapro, which we expect will increase our total overall market
share of the SSRI market even more. In fact, Lexapro has the potential  based
on its product virtues  to become the leading antidepressant in the U.S. market.
We will shortly be launching Lexapro, the S-enantiomer of Celexa. Some drugs
are racemates, meaning that they are made up of two enantiomers which are
mirror images of each other, like a left and right hand. For many purposes, the
differences between left and right may not be important. But left handed batters
and golfers and dentists have to stand in a different place and use different instruments.
For receptor binding, a left or right configuration can make a vast difference in
how and whether a molecule binds to receptors on the surface of a cell.
Whether that difference in receptor binding has a therapeutic significance
varies, and the only way to find out is to test it. In the case of Celexa, the
S-enantiomer has all the effect of inhibiting the reuptake of serotonin back
into the neuron, and the R-enantiomer has none of that effect. It is believed
that the R-enantiomer interferes with the action of the S-enantiomer at the
receptors that the S-enantiomer has to bind to in order to exert its therapeutic
effect, thereby inhibiting the clinical benefit the S-enantiomer could confer
if the R-enantiomer were not present. The R-enantiomer also binds to other
receptors that the S-enantiomer does not bind to or bind with the same avidity
which do not contribute to the therapeutic effect of Celexa and may account
for the reduced side effects found with Lexapro. The end result is that 10 mg.
of the S-enantiomer  Lexapro  has been shown to be as effective as 40 mg.
of the racemate  Celexa  and to have less of certain side effects. We believe
Lexapro is the most effective SSRI, the most consistent rapidly acting, best
tolerated and with the lowest proven therapeutic dose, and therefore a
significant step forward in the treatment of depression. In clinical studies

Once we accept
and understand the
health bounty that
is increasingly
available, and once
we realize that we,
the American people,
want to enjoy its
fruits, we have to
accept that it must
cost more.

Lexapro has consistently demonstrated separation from placebo as early as
one week, significantly sooner than other drugs in the category. Following the
expected launch of Lexapro for the treatment of depression, we plan to file
NDAs for additional clinical indications for Lexapro. At present, we and our
partner, H. Lundbeck, have completed positive placebo-controlled studies in
general anxiety disorder, panic disorder and social phobia. Additional studies
are currently underway to support future NDA submissions for these indications.
The central nervous system is an area in which we have become much adept, and
our next major entry may be memantine, for Alzheimers Disease  unfortunately
not a cure, but a product that can usefully ameliorate the symptoms of that
terrible disease for some significant period of time. It is an NMDA antagonist,
represents a completely new mode of action in dealing with Alzheimers Disease
and it has an excellent safety and tolerability profile. We will shortly be filing
the NDA for memantine with two pivotal studies, one of which we believe meets
the FDAs standards, and one of which, performed in Europe, does not have an
endpoint traditionally required by the FDA. We do not know whether the FDA will
accept that study, or whether it will seek advice from an Advisory Committee, or
whether, because of that study, it will not accept our filing for review. The FDA
will make that decision after we file the NDA. In the meantime, we are conducting
four additional studies, any one of which could be a pivotal study, and the earliest
of which could be submitted in early 2003. Memantine has just recently been
approved in Europe for Alzheimers Disease. We believe memantine will be approved
in the U.S. and become a major product for Forest. It appears that it can be used
with or without the cholinesterase inhibitors, the only other products presently
approved for Alzheimers Disease.
Another principal area in which we are particularly experienced is the cardiovascular
area. We have successfully marketed Tiazac, a diltiazem calcium channel blocker,
for several years and last year Tiazac sales reached $195,000,000. It appears
likely that a generic for Tiazac may gain FDA approval this year, in which case
we will also be marketing our own generic through our Inwood subsidiary.
However, we have two other major cardiovascular products, one that we have
just commenced marketing and one that we expect to be marketing next year.
The first is BenicarTM, an angiotensin receptor blocker for hypertension. Benicar
was developed by Sankyo, and is being marketed jointly by Sankyo and Forest in
the United States. The second product is lercanidipine, a new type of
dihydropyridine calcium channel blocker also for hypertension, which was
submitted to the FDA last October, and which we expect to be launched next
spring. Both products have strong competitive advantages in their respective
categories and we expect will achieve significant sales.
We believe Lexapro
is the most effective
SSRI, the most
consistent rapidly
acting, best
tolerated and with
the lowest proven
therapeutic dose.
Hypertension is an increasingly major disease in the U.S. People are living longer,
and research confirms the desirability of maintaining lower blood pressure. It is
also increasingly clear that blood pressure is controlled by a number of biological
factors, and that therefore therapy with multiple drugs targeting different mechanisms
is often necessary to achieve the desired treatment objectives which may not
be achieved by only one category of drugs. It is therefore pertinent and valuable
for Forest to be able to market two superior products with different modes of
action, each of which can be administered separately, but when appropriate, can
also be administered together. Forest is therefore exploring combining products
with different mechanisms in one dosage form, and has rights to combinations
being developed by its partners.
Also to be launched next year is acamprosate, for alcohol addiction, a drug
licensed from Lipha, the French subsidiary of Merck KGaA, for which the New
Drug Application has already been filed by Lipha, and which was reviewed by
an FDA Advisory Committee meeting last month which determined that the drug
was effective for the treatment of certain alcoholic patients. The drug will help
people who want to control their alcoholism to reduce their craving for alcohol.
The need is certainly enormous with estimates of fifteen to twenty million people
in the United States who have alcohol addiction problems. The marketing of
acamprosate will present novel challenges and require novel marketing technology.
We are looking forward to introducing this unusual product and the large benefits
it may confer on many people. In addition, our greatly improved flunisolide product
for asthma, and our combination oxycodone and ibuprofen product for pain are
both at the FDA and each has significant additional profit potential for us.
These are the most current products in our pipeline. We have several more products
in development. Dexloxiglumide, in Phase III, is being developed for irritable bowel
syndrome. Memantine, for neuropathic pain, is also in Phase III, with one pivotal
study already completed. Neramexane, in Phase II, is being explored for several
CNS indications. Taken all together, we have a remarkable pipeline, particularly
for a company our size.
Forests success in obtaining a continuing pipeline of products is based on the
appeal of its drug development and its sales and marketing strengths. Forest does
not yet create new molecules, principally because of the cost, the high risk of
failure and the long development time involved. But it is also because there is
such a large amount of innovative drug discovery and development activity being
conducted by foreign companies and by smaller companies whose products are
available for partnership with companies with Forests skills. Some drug discovery
companies need a partner to take their invention through regulatory approval

and then to market it. Some foreign companies with products from early stage
to already marketed in their home markets, need a partner to market their product
in the United States or, in the case of Sankyo, need a partner to co-market a
major product they cannot yet fully market on their own. For some products
and some companies, we are a uniquely desirable partner because of our record,
our size, our flexibility and our rapid decision making process. Our existing
partnerships are among our most valuable assets and have several times
produced multiple product opportunities.
Some questions have been raised recently about the productivity of the
pharmaceutical industrys research and development. And certainly, like painting
in the Renaissance and political genius when our country was founded, there
are always peak periods of creative achievement. But granted that each
pharmaceutical company has its own individual prospects, as a whole there
should be no concern about the research potential of the pharmaceutical industry
 if we look at a little longer time frame than the next quarter. Certainly the
need for new drugs has not perceptibly diminished. In fact, we know so little,
and have accomplished so little, against the total puzzle of human health, and
we have touched so few of the targets that control our biological functioning
that the opportunities for scientific intervention are barely less than they were
when Hippocrates practiced medicine. It is a fiction to think that we have
finished the easy ones and now we are slowed down because we have only
the harder ones. The early ones were not so easy; what is relatively easier is
improving on the breakthrough products. But breakthroughs have always been
hard. Decoding the genome will help to identify targets, but help very little in
affecting their functions. And the targets illuminated and their interconnections
in human functioning reach almost to infinity.
The apparent dearth of productive R&D may lie more in the ambition of research
objectives. The very size of the pharmaceutical leviathans require blockbuster
blockbusters to make a perceptible difference in their profit and loss, and there
are only so many targets that affect so many people so often that a successful
drug can have the requisite billions of dollars of potential sales. As the leviathan
becomes more bloated it has to feed on whales and it ignores the minnows,
and there are far fewer whales. Maybe it is not merging companies that are
needed but splitting them up instead as a means to unleash R&D productivity.
Every merger results in terminating research on smaller projects, and more effort
focused on fewer jackpots.
For us it is really quite different. Because we are so much smaller, we are delighted
to market a drug with a $200,000,000 potential provided its developmental and
promotional budget is in scale with its potential. For the leviathans, the overhead

For some products
and some companies,
we are a uniquely
desirable partner
because of our
record, our size,
our flexibility and
our rapid decision
making process. Our
existing partnerships
are among our most
valuable assets and
have several times
produced multiple
product opportunities.
to even look at such a drug would consume its potential. And so, although
we do not yet do our own discovery research, we are able to license
products, often from smaller companies, that do not interest the leviathans.
And sometimes precisely because of our size and flexibility we are simply a
more desirable partner even when competing with the leviathans. And
sometimes if we are clever or fortunate, some of those apparently smaller
products turn into very large products. At least five American companies
reviewed and rejected Celexa before we acquired the product. And so, because
our threshold is not so high, we have a full pipeline of promising drugs, with
enough to keep us very busy for several years, and hopefully to assure significant
continued growth as we carefully modulate the expenses of achieving
growth against the income that growth generates. And all the time, of
course, we are actively pursuing additional product opportunities.
We all know that success in any enterprise depends on having capable,
dedicated and serious employees who create and move all the little
pieces that shape strategies and enable those strategies to be realized.
Our executive strategies would be useless, without the thousands of
employees with their individual and collective wisdom, who help to
create corporate strategies. It is their tenacity and skill and hard, hard
work which enable Forest to obtain products, to develop them and to
market and sell them. It is therefore to all employees that we owe our
success. They have been working harder this past year than ever before
and the next year looks at least equally demanding. It is always difficult
in a rapidly growing company to have enough help and enough space,
but our employees continue nevertheless to do more with less assistance
and comfort. As we try our best to augment their support, they continue
to be the spark that enables us to move forward and we are all deeply in
their debt. Our congratulations and gratitude to all of them.
Sincerely,
Howard Solomon
Chairman and Chief Executive Officer