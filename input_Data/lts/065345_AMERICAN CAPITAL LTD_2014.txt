American Capital had a very successful 2014. We increased total Earning Assets Under Management
(EAUM) by 19%, with wholly-owned American Capital Asset Management (ACAM) raising five new
funds. We invested $1.9 billion in a highly diverse set of Senior Floating Rate Loans, plus we invested
$1.6 billion in Sponsor Finance, Structured Products, ACAM and fund development, all intended to
improve our net operating income, expand ACAM and advance our spin-off plans, which we describe
below. At year end, Senior Floating Rate Loans represented 22% of total assets, while equity in middle
market companies represented only 12% of total assets. As a result of our efforts in 2014, we increased
NAV per share by 8% and net earnings per diluted share by 154% to $1.55.More importantly, on November 5, 2014, we announced our intention to transfer most of our investment assets to two newly established Business Development Companies (BDCs) and to distribute
those BDCs to our shareholders, leaving the Company to operate primarily in the asset management
business. We previously noted that we may need regulatory relief from the SEC to complete the transaction. We have now concluded that regulatory relief likely will not be needed. We will, however,
still need to file proxy and registration statements with the SEC, which we expect to file in the third 
2 American Capital
calendar quarter of 2015. These documents will be subject to review and comment by the SEC, which
will affect the time frame in which the transaction can be completed, so we cannot give any assurance
as to if or when the transaction will be completed.
Once the spin-off has been accomplished, American Capital would focus on being an external asset
manager of permanent capital companies and other investment vehicles. Following the spin-off, we
would no longer be an investment company or subject to regulation under the Investment Company
Act of 1940. All employees would reside at American Capital and it would retain its net operating
losses (NOLs). It would manage the two new BDCs, American Capital Growth & Income (ACGO)
and American Capital Income (ACIN). As part of the transaction, American Capital would consolidate
operations and its remaining assets with ACAM and would manage the investment vehicles currently
managed by ACAM, as well as any new funds developed in the future.
American Capital Growth & Income would be a BDC with its investments focused on American Capital
One Stop Buyouts, leveraged senior floating rate loans and collateralized loan obligation (CLO)
equity. It is anticipated to be spun off tax free to our shareholders and to qualify as a Regulated
Investment Company (RIC) under Subchapter M of the Tax Code. It would target the payment of
market rate dividends. American Capital Income would be a BDC focused on lending to third-party
buyouts and would continue the lending business currently conducted by our Sponsor Finance group.
It is anticipated that its spin-off would be a taxable transaction for our shareholders, it would be a RIC
for tax purposes and it would target the payment of market rate dividends. Both BDCs are anticipated
to have market rate management and incentive fee agreements and provide for appropriate expense
reimbursements to American Capital.
Subsequent to the spin-off of these two new BDCs, we expect to be managing five public and ten
private funds, with total EAUM, including assets on American Capitals balance sheet, approximating $19.6 billion, based on asset levels as of December 31, 2014, of which 84% would be permanent
capital companies.Separation into three public companies allows each to focus its business strategies and objectives, providing greater transparency for investors. Investors can thus choose each company based on desired
investment objectives, return profile and risk tolerance. In addition, separate investment strategies
reduce perceived channel conflict between our American Capital One Stop Buyouts business and our
Sponsor Finance business. With three public companies, benchmarking to comparable public companies will be easier, allowing the market to value each company more accurately, potentially allowing
growth through capital raises. And, each company will be able to determine its own dividend policy.
Now lets discuss the details of this years results as we review our business lines.Business Lines
We describe below our six business lines and some of the opportunities we have to continue providing
strong returns for our shareholders.
American Capital One Stop Buyouts1
Through American Capital One Stop Buyouts we acquire or build operating companies and, with
the combined efforts of our Investment Team and Operations Team, work with them to enhance their
performance, implement add-on acquisitions and expand their operations. Over the past 17 years, we
have invested $12.8 billion in American Capital One Stop Buyouts in over 120 portfolio companies in
North America and have exited 84 of these, with proceeds totaling $12.9 billion. On these exits, we
have produced a 14% annual return on a blend of senior debt, subordinate debt and equity in these
companies, and a 20% annual return on the equity portion of the investments.
In 2014, we received $1.2 billion of realizations from 31 of these companies, almost half from selling
equity assets to American Capital Equity III, LP (ACE III), which ACAM now manages. American Capital
earned $194 million of interest, dividend and fee revenue in 2014 from these companies.Sponsor Finance
Our Sponsor Finance business lends to and co-invests in the equity of U.S. middle and large market
buyouts that are sponsored by other private equity firms and also makes direct investments in middle
market companies. We valued these investments at $1.1 billion as of December 31, 2014. Since inception, we have produced an 8% IRR on these debt and equity investments. During 2014, our Sponsor
Finance team sourced new investment opportunities by working closely with some of the best private
equity firms in the country. We invested $689 million in 24 Sponsor Finance Investments last year, a
167% increase over 2013, and we expect to do more in 2015. These new investments were originated
with an unlevered weighted average effective interest rate of 8.8% as of December 31, 2014. With leverage, we expect these investments will produce an attractive return on equity. In 2014, American Capital
recognized $56 million of interest, dividend and fee income from our Sponsor Finance Investments.
Senior Floating Rate Loans
In 2014, we began investing in Senior Floating Rate Loans to large market, U.S. based companies. Our
Senior Floating Rate Loan portfolio also includes a small amount of second lien floating rate loans. We
invested $1.9 billion in 230 portfolio companies with the average loan obligor representing 0.4% of
these loans and a 1.7% maximum issuer concentration. We also arranged two debt facilities totaling
$1.25 billion, which should allow leverage levels somewhat in excess of 3:1 debt to equity. Utilizing
this leverage should produce attractive risk adjusted returns on equity. These new investments were
originated with a weighted average 4.4% yield at cost as of December 31, 2014 and our two new debt
facilities have a funding cost of 2.2% as of December 31, 2014. In 2014, American Capital recognized
$33 million of interest income from our Senior Floating Rate Loans.
European Capital
European Capital invests in operating companies and makes sponsor finance investments in Europe.
In 2014, European Capital had net earnings of 120 million, which resulted in a 21% net earnings return
on equity. European Capital received 575 million of realizations from its investment portfolio in 2014,
a large portion of which came from the sale of Farrow and Ball, which was one of our most successful
buyouts. European Capital committed 166 million to new investments in 2014. Since inception, we
have produced a 6% IRR on European Capitals debt and equity investments. As of December 31, 2014,
European Capitals investment assets at fair value were 483 million.
American Capital has consolidated European Capitals financial results as of and for the three months
ended December 31, 2014, with its financial results for the same periods. In addition, prior to the Q4
consolidation of European Capital into American Capitals results, European Capital paid $265 million
in dividend distributions in 2014 to American Capital, which were accounted for as a return of capital.
To learn more about European Capital, please visit its website at www.EuropeanCapital.com.Structured Products
Our Structured Products business invests predominantly in CLOs of senior floating rate loans and in
non-investment grade tranches of commercial mortgage backed securities (CMBS). Since inception, we have produced a 16% IRR on our CLO investments. At the end of 2014, we had $560 million
invested in Structured Products, of which $525 million was in 54 CLOs. In 2014, we invested $512 million in Structured Products, of which $502 million was invested in CLOs. Our Structured Products
investments generated cash proceeds of $192 million, including $65 million of interest income, in 2014.
American Capital Asset Management
ACAM develops, capitalizes and manages investment funds, including real estate, private equity and
loan funds. We valued our investment in ACAM at $1.2 billion as of December 31, 2014.
At the end of 2014, ACAM had $14.5 billion in EAUM, a 15%, or $1.9 billion, increase from 2013. Including
levered assets, ACAM had $78.7 billion of Assets Under Management.
We raised five new funds in 2014, including:
 $150 million initial public offering (IPO) of American Capital Senior Floating, Ltd. (NASDAQ: ACSF)
in January,
 $1.1 billion committed to ACE III in September,
 $619 million ACAS CLO 2014-1, Ltd. in July,
 $411 million ACAS CLO 2014-2, Ltd. in November, and
 $165 million European Capital UK SME Debt fund in August.
At the end of 2014, ACAM managed ten private and three public funds.
In 2014, American Capital had revenues of $111 million from ACAM, comprised of dividend, fees, interest income and expense reimbursements, a 17% decrease from 2013. The decrease was the result of
our average EAUM during 2013 being materially higher than during 2014, due to share repurchases
and lower book value at AGNC and MTGE as well as increased costs at ACAM associated with fund
development activities.
We have other managed funds in development, some of which we hope to raise in 2015, including
a European debt fund, several CLOs, an international power fund, a European buyout fund, a North
American energy fund and a commercial mortgage REIT.American Capital Agency Corp. (NASDAQ: AGNC)
AGNC invests primarily in mortgage backed securities (MBS) issued by Fannie Mae and Freddie Mac.
AGNCs objective is to create long term value for shareholders through a combination of book value
growth and the payment of an attractive dividend. Its primary metric for measuring performance
is economic return, which is the sum of the change in the net book value per common share and
dividends declared per common share, divided by the beginning net book value per common share.
The recent relative outperformance of agency MBS helped drive AGNCs strong economic return of
18.5% in 2014. AGNCs active approach to portfolio management, which included repositioning its
asset portfolio, modifying the composition of its hedge portfolio and operating with a somewhat
larger duration gap during the first half of 2014, benefited AGNCs financial results during the year.
Importantly, AGNC generated this strong economic return for the year despite operating with relatively low leverage. As of December 31, 2014, AGNC had $67.8 billion in total assets.For managing AGNC, ACAM receives a 1.25% annual fee on AGNCs adjusted book value (excludes
unrealized appreciation and depreciation), which was $9.4 billion as of December 31, 2014.	To learn more about AGNC, please visit its website at www.AGNC.com.
American Capital Mortgage Investment Corp. (NASDAQ: MTGE)
Capitalizing on the success of AGNC, we sponsored the IPO of a hybrid mortgage REIT, MTGE, in August
2011. In addition to the Freddie Mac and Fannie Mae securities in which AGNC invests, MTGE also
invests in a broader array of MBS and other real estate related assets. The U.S. fixed income markets
performed well in 2014, a sharp contrast to the extreme interest rate volatility and price deterioration
experienced in 2013. Against this backdrop, both agency and non-agency MBS performed relatively
well during the year, driving MTGEs solid 14.1% economic return. Over the year, MTGE paid $2.60 of
dividends per common share to shareholders and increased book value by $0.44 per common share.
In addition, given the slight improvement in share price to book value ratio over the year, total stock
return was 23.3%, exceeding economic return by 9.2%. As of December 31, 2014, MTGE had $7.0 billion
in total assets.For managing MTGE, ACAM receives an annual fee totaling 1.50% of MTGEs adjusted book value
(excludes unrealized appreciation and depreciation), which was $1.2 billion as of December 31, 2014.To learn more about MTGE, please visit its website at www.MTGE.com.
American Capital Senior Floating, Ltd. (NASDAQ: ACSF)
In January 2014, we sponsored the $150 million IPO of ACSF, a BDC managed by ACAMs Leveraged
Finance Group, which invests primarily in senior floating rate loans to large U.S. based companies and,
to an extent, seeks to enhance those returns by opportunistically investing in CLOs. The members of
the senior investment team of ACAMs Leveraged Finance Group have an average of 19 years of experience investing in this asset class and have worked together at American Capital for the last nine years,
managing approximately $2.8 billion of leveraged loans as of December 31, 2014.
During ACSFs first year of operation following its IPO, it successfully deployed its capital, optimized
its portfolio, increased its yield and earned $1.09 per share net investment income. In turn, ACSF paid
$1.03 in dividends to its shareholders producing an approximate 7.0% annualized yield on its IPO price
of $15.For managing ACSF, ACAM receives an annual fee totaling 0.80% of ACSFs total assets (excluding cash
and cash equivalents and net unrealized appreciation or depreciation), which were $0.3 billion as of
December 31, 2014.
We believe this senior floating rate BDC is an attractive vehicle for investors, particularly in a rising rate
environment. Furthermore, ACAM is managing ACSF at what we believe is a very investor friendly fee
structure, with no carried interest and perhaps the lowest management fees among all BDCs.
To learn more about American Capital Senior Floating, Ltd., please visit its website at www.ACSF.com.
Other Funds
In addition to the funds above, as of December 31, 2014, ACAM manages four private equity funds with
a combined $1.5 billion of committed capital, including the committed capital in the European Capital
UK SME Debt Limited as of December 31, 2014. Two of these funds closed in 2014:
American Capital Equity III
In September 2014, we closed on a new private equity fund focused on investing in U.S. companies in
the lower middle market. The transaction further expands American Capitals asset management business and diversifies its investor base, adding new private equity limited partners. The investor group,
which was led by funds advised by Coller Capital, Goldman Sachs Asset Management and StepStone
Group, also includes select sovereign wealth funds, state retirement and pension systems, high net
worth family offices, superannuation funds and foundations, totaling 39 limited partners.
ACE III acquired a portfolio of equity investments from American Capital and an option to purchase our
equity investment in another portfolio company. ACE III investors committed an additional $445 million to fund the purchase of new control equity and equity-related investments in companies with
$5 to $25 million of earnings before interest, taxes, depreciation and amortization.
For managing ACE III, ACAM receives an annual management fee totaling 2.0% of ACE IIIs primary
ongoing commitments and 1.25% on secondary investments and a carried interest in the net profits of
ACE III, subject to certain performance hurdles.
European Capital UK SME Debt Limited
In September 2014, we closed a 100 million ($165 million) fund, European Capital UK SME Debt
Limited, to provide debt financing to small and medium sized enterprises in the UK. The fund will
invest in small and medium sized businesses with a turnover of up to 100 million. The British Business
Bank committed 50 million to the fund under the British Business Bank Investment Programme. The
remaining 50 million was committed by European Capital Limited and its affiliates.
For managing the European Capital UK SME Debt Limited, ACAM receives an annual management fee
totaling 1.5% and a 15% incentive fee, subject to certain performance hurdles.Looking Ahead
The transformation of our balance sheet and the $3.6 billion of new investments in 2014, coupled with
the growth of our asset management business, further our efforts toward improving our net operating
income, expanding ACAM and advancing our spin-off plans. We have several other public and private
funds under development, and we would expect some of them to be completed in 2015, which would
further strengthen and diversify our asset management platform. We are working on unlocking additional shareholder value through our spin-off, after which we expect to have five permanent capital
businesses and 10 finite-life funds under management, each with different investment strategies, providing growth opportunities across varied interest rate environments and business cycles.
We appreciate this opportunity to share the excitement we have about the future opportunities that
will come with the spin-off, and all of us at American Capital thank you for your continued support.
Sincerely,
Malon Wilkus
Chairman and Chief Executive Officer 