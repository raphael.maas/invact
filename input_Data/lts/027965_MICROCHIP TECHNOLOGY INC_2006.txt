to our shareholders

Microchip Technology continued
to deliver outstanding performance
throughout all segments of our
strong enterprise during fiscal
year 2006.
For the fiscal year ending March 31,
2006, Microchips net sales were
$927.9 million, increasing 9.6%
from net sales of $846.9 million
for the fi scal year ending March 31,
2005 and establishing a new
record level. Non-GAAP net
income for fiscal 2006 was
$273.0 million, an increase of
20.4% over non-GAAP net income
in the prior fiscal year of $226.8
million. We achieved record gross
margins and operating margins of
59.4% and 35.2%, respectively,
in fiscal 2006. Our balance sheet
continued to strengthen, and
we generated $450.4 million of
net cash (prior to our dividend
payments of $120.1 million and
stock buy-back activity of $3.3
million), driven by our sound
operating results and successful
business model.
Microchip increased its cash
dividend payment every quarter
during fiscal 2006, resulting
from strong net cash generation
throughout the fiscal year. This
allowed shareholders to benefit
even further from the Companys
solid performance. Microchips
total annual dividend payment in
fiscal 2006 was $0.57 per share,
an increase of 174% over the
annual dividend payment of
$0.208 per share in fiscal 2005.
Cash dividend payments to
shareholders in fiscal 2006
totaled $120.1 million.
Superior Financial
Performance
These outstanding financial results
place Microchip in a leadership
position when compared to most
other semiconductor manufacturers
in areas such as sales growth,
operating profit, gross margin,
operating margin, earnings per
share, cash generation, stock price
performance and dividend payment
and growth  which all lead to
Microchip producing significant
value to our shareholders.
Our sustained growth and
accomplishments are the result
of many factors, including a
successful business model that
has thrived in both the ups
and downs of the cyclical
semiconductor industry
and a unique company
culture that embraces
continuous improvement,
teamwork and employee
empowerment.
We consider our employees to
be Microchips greatest strength 
the critical ingredient that
continues to power Microchip
ahead in reaching ever new
highs across all segments of
the business.
In the fourth quarter of fi scal 2006,
we reached our long-term gross
margin target of a record 60%.
Microchip achieved the 60%
target earlier than anticipated
thanks to our richening product mix,
an aggressive conversion to Flash
and mixed-signal microcontrollers
by our customers, continued
strong execution in Fab 4 and
depreciation rolling off in our other
factories. Given our expectations
for continued performance gains,
we raised our long-term (two to
three years out) gross margin
guidance to a record 62%.
New Initiatives for
Additional Growth
In order to take higher levels of
direct control over demand
creation for our products, Microchip
undertook several key initiatives in
fiscal 2006.

The microchipDIRECT online
procurement site debuted,
expanding our e-commerce
services and customer support
functions. This comprehensive
online resource provides Microchips
customers worldwide with
additional features previously only
available through Microchip sales
offices, including credit lines,
credit-card payments, competitive
pricing from local sales teams,
and e-mail notification of orders,
deliveries and quote status.
Additional investments in fiscal
2006 enabled us to provide 24/7
technical support, hire additional
local field applications engineers,
establish 23 Regional Training
Centers worldwide and offer
direct production programming
services for our 8- and 16-bit
microcontrollers. Other technical
training included the annual
MASTERs Conference, which
was held in several worldwide
locations and attracted record
numbers of engineering customers
who learned more about our
products as well as new embedded
design techniques.
These demand creation initiatives
provide a multitude of resources
that our customer base can truly
appreciate, and we are confident
they can further enhance our
competitive advantage. Microchip
is committed to offering the best
possible service and technical
support for our 50,000+
customers around the globe.
Successful, Early
Conversion to
Lead (Pb) Free
One key business issue faced
by the worldwide semiconductor
industry in fiscal 2006 was the
European Union Restrictions on
Hazardous Substances (RoHS)
directive. This directive limits the
amount of lead (Pb) in electronic
equipment. It was scheduled to
go into effect on July 1, 2006,
and governs all electronic equipment
manufactured or sold in the
European Union member countries.

Understanding the potential
impact on our business and
Microchips commitment to
environmental responsibility,
a dedicated cross-functional
team began planning four years
ago for the necessary conversion
of our manufacturing activities.
Our employees executed this plan
flawlessly, allowing Microchip to
become one of the first semiconductor
companies to convert
product packaging to environmentally
friendly Pb-free plating.
This in turn enabled our
customers to eliminate Pb from
their manufacturing processes
well ahead of schedule.
Aggressive New
Product Development
During the fiscal year, our
employees carried out numerous
new product development
strategies in many important
areas, further expanding our
strong product portfolio of 8- and
16-bit microcontrollers, 16-bit
digital signal controllers, analog
ICs and serial EEPROMs.
Microchip aggressively pushed
into the 16-bit controller space
by announcing 49 new 16-bit
PIC microcontrollers and dsPIC
digital signal controllers. These
devices offer critical advantages
such as pinout compatibility,
software compatibility, peripheral
compatibility and common
development tools.
The PIC24 family of 16-bit
microcontrollers is comprised
of two series. The PIC24F offers
a cost-effective step up in performance,
memory and peripherals
for many applications that are
pushing the envelope of 8-bit
microcontroller capabilities.
For more demanding applications,
the PIC24H offers 40 MIPS
performance, more memory and
additional peripherals.
The new dsPIC33 family of 16-bit
digital signal controllers targets
embedded designers who need
high levels of performance,
memory, on-chip peripherals and
I/O without the complexity of
traditional digital signal processors.
All new 16-bit family members
range from 64 to 256 Kbytes of
self-programming Flash memory
and 64- to 100-pin packages.
Today, we have a total of 70
16-bit devices sampling or in
volume production that address
the $3.8 billion 16-bit controller
market space**. More products
are in design. We are pursuing
16-bit customer opportunities in
a wide range of high-performance
applications that are separate from
and incremental to those being
served by an 8-bit microcontroller
today. We believe we have the right
silicon and development tool
solutions in place to reach our
long-term goal of becoming the
leading microcontroller architecture
in the 16-bit market, duplicating
the leadership we achieved in
the 8-bit microcontroller space.
Looking at our 8-bit PIC
microcontrollers, in fiscal 2006
Microchip drove further
innovations to ensure the largest
segment of our product portfolio
continues to meet the evolving
design needs of our customers.
Many 8-bit devices were launched
across our entire 6- to 80-pin
families, offering additional
memory, faster speeds and richer
on-chip features based upon customer
demand. Microchip remains
steadfast in our commitment to
supporting 8-bit applications.
For the ultra-small 6-pin
microcontrollers, Microchip
added an analog-to-digital (A/D)
converter and doubled the
operating speed, which helps
further expand non-traditional
applications for digital
intelligence. New 8- to 64-pin
low-power Flash microcontrollers
debuted with richer on-chip
features such as advanced
analog and digital peripherals,
integrated KEELOQ technology
and on-chip LCD drivers.

Microchip announced 14 new
PIC18F 8-bit microcontrollers
that deliver 40 MHz at 3 volts
and 16 to 128 Kbytes of Flash
memory in 28-, 40-, 64- and 80-pin
packages. As the performance
levels among 8-, 16- and 32-bit
microcontrollers continue to blur
and more designs migrate to
lower voltages, embedded
engineers are looking for costeffective,
yet peripheral-rich,
8-bit microcontrollers with high
memory densities and pin counts
that help them preserve their
legacy 8-bit code and
development tool investments.
In certain applications, designers
are faced with ever-increasing
demands for processing power
and connectivity, while simultaneously
facing the challenge to
lower power consumption and
overall system costs. Microchip
introduced a new series of
PIC18F microcontrollers that
features an extra serial port
for expanded connectivity and
a faster A/D converter for
quicker measurement  all
at a 30% lower price than
the previous generation.
A leading indicator of the
continued acceptance of our
microcontrollers and digital signal
controllers is the number of
related development systems
shipments. Engineers utilize
these high-performance systems
to evaluate, compile, edit,
debug, program or emulate our
controllers. In fiscal 2006, our
development tools continued their
robust growth with 73,705 total
shipments, up from 53,311 total
shipments in fiscal 2005. Total
cumulative shipments now stand
at 432,016.
In fiscal 2006, our analog and
interface products enjoyed strong
market acceptance with sales up
16.6% over fiscal 2005. This high
demand outpaced the growth of
most of our analog competitors.
Microchip succeeds by offering
a broad line of analog devices,
including linear, mixed signal,
power management, thermal
management and battery
management products, that are
required in microcontroller-based
designs today.
Many analog innovations brought
to market this year target key
design opportunities with the
precision, performance and price
benefits ideally suited for
embedded designs. For example,
the MCP355X family of deltasigma
A/D converters offers
22-bit resolution with the lowest
power consumption in the industry.
It is also one of the smallest
in an 8-pin MSOP package.
Our serial EEPROM product line
continues to grow with a new I2C
family that has 1 Mbit of memory 
the highest memory density
available for such devices.
From our serial EEPROMs to
analog products to 8- and 16-bit
microcontrollers to 16-bit digital
signal controllers, Microchips
business is exceptionally strong
today. We are well positioned for
continued market share gains in
fiscal year 2007, thanks to the
tireless dedication and unlimited
contributions of our employees
worldwide. Working together,
our goal is to continue to outpace
the semiconductor industry.
With sincere appreciation to our
shareholders, customers and
employees for your continued
confidence in Microchip,
Steve Sanghi
President and CEO
Microchip Technology Incorporated