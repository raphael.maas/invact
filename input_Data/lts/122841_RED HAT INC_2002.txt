Dear Red Hat Shareholder:

Against a backdrop of negative industry activity, we proved a business could be built around open source software:
	Delivered profitable or break-even adjusted earnings and operating cash flows in every quarter of fiscal 2002.
	Improved gross margins to 65% by Q4.
	Ended the year with no long-term debt and $287 million in the bank.

Red Hat is improving the customer experience. Everywhere you look, our technology and services are leading the Red Hat Linux migration.

In the past year we expanded our engineering capabilities, opening a new engineering facility in Boston, Massachusetts.

We developed Red Hat Linux Advanced Server, raising Linux to true enterprise class.

We worked with Wall Street's top financial firms -- the most demanding proving grounds for corporate success.

We saw market-leading software vendors confirm the demand for Red Hat Linux by certifying their applications on it.

And we fought to advance open source and provide technology access in schools.

n a recent CNET survey of more than 2,200 IT professionals, Red Hat was ranked #2, ahead of companies like IBM and Sun, as the company most relevant to their business for the next five years. In addition,half of the respondents said that Linux had either a significant or major role in their plans.

The Linux market is in its infancy. Last year alone Linux shipments grew 57% from the year before, according to IDC. More than half of all Linux servers sold are Red Hat Linux.

To meet the growing opportunity,we've focused on our business -- improving operating profits and maintaining a strong balance sheet.

And through our unparalleled open source development expertise and true collaboration among customers, partners, and the community, we are leading Linux into the enterprise.
Red Hat Linux Advanced Server is the first enterprise-class Linux operating environment. We developed Advanced Server to provide reliability and performance in mission-critical enterprise infrastructures.Together with widespread support from market-leading software vendors, Red Hat Linux Advanced Server creates a definitive route of migration. It also creates a scalable, high-margin, recurring revenue stream for Red Hat.
Red Hat Expertise
	Red Hat Network is the first of our Red Hat Managed Services offerings. It's an integrated, technology-based system management and support mechanism for the Red Hat Linux platform.
	Our service professionals help Fortune 2000 customers migrate to Red Hat Linux from proprietary RISC/UNIX platforms to lower their costs and increase performance.
	We staff a global, 24x7 support organization, operating in 15 locations and 7 languages.
	Red Hat training expanded to more than 60 cities in the Americas, Europe and worldwide. Red Hat Certified Engineer (RHCE) continues to dominate as the standard for Linux training and certification programs--rated as the overall top IT certification for quality in a 2002 Fairfield Research/Certification Magazine study.
Today's companies are responsible for much more than providing a financial return.

If a company brings a positive financial returnbut neglects its responsibility to society -- whether by unethical business practices or mistreating customers in the name of competition -- the company will eventually suffer.

Without financial stability, the positive value to society will not last.

This balance is imperative to change an industry.

Returning Trust
With open source at our foundation, we are bringing trust back into the customer relationship.

The code we write is open so others can see it and learn from it.

So it can be collaboratively built and tested.

So it can be used by all developers to create new software.

So it can be returned to the community to help schools educate their students at a lower cost. We're helping make this possible not only with technology, but by bringing together advocates of open source in education through an online community we call Open Source Now.

Because history proves we progress faster when access to information is open. It's true in schools or software. When access is open, we all win.
