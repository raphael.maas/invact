Letter to Shareowners


Some of my most rewarding days
are those I spend with our farmer
customers. Of course, its enjoyable
to meet the people and see the faces we serve,
but its more than that. No one knows more
about agriculture. They are both customers
and trusted advisers, and in a year that changed our business
forever, theyve helped us reflect on what agriculture looks like.
There are many faces that shape our industry, and no matter
where you are in the world, they all play a part in helping meet
global demand in a sustainable way. The
farm family. The salesman. The scientist.
The agronomist. And as I look back on 2010
and think about those faces, I realize its been
an excellent year for agriculture.
A year of challenge and change for our
business, no doubt, but an excellent year for
agriculture. Farmers around the globe chose
next-generation technologies and employed
new agronomic practices  and they saw yield
improvements as a result. Our scientists continued to innovate,
discovering promising leads to populate our robust pipeline. And
the world continued to grow, and with it grew demand, furthering
the need for sustainable agriculture.
Thats our vision at Monsanto  to help produce more, conserve
more and improve lives. As I think back on this year with that vision
in mind, I feel very good about our business.

A Year of Great Change
The 2010 fiscal year brought
with it a good deal of change
for our business. We realigned
our company in light of increased
competition in the crop protection
market. We overhauled our
Roundup and other glyphosatebased
herbicides business 
refining pricing and our market
approach in a compressed
timeframe  and identified
new opportunities for weed
management moving forward.
We are now focused, as a
seeds-and-traits company, with
our crop protection business
serving in its rightful supportive role. Today this evolution is complete,
but it has not been easy. We had to make some very difficult decisions
along the way, which included resetting our financial goals and adjusting
the size of our workforce. We remain optimistic about our near- and
long-term prospects, not only for our business, but also within agricultures
ability to meet the needs of a growing global population. That optimism
is built on a solid foundation we set in 2010, the year in which we launched
Genuity SmartStax corn on 3 million acres and farmers planted 6 million
acres of Genuity Roundup Ready 2 Yield soybeans in the products second
year in the marketplace.
Our cotton business is strong, and this year we saw an increase in
planted U.S. acres.
Our vegetable business continues to grow and create innovative products
for consumers like the EverMild onion and Benefort broccoli.
This year we also saw the world continue to open to biotechnology.
We secured approvals for four products in Brazil and six key product
imports in the European Union. We also saw approvals for field trials
in five countries around the world: corn in Mexico, India, Vietnam
and Uganda, and cotton in Pakistan.
These accomplishments lay the foundation for the mid-teens earnings
growth we expect going forward.

Looking Ahead
Creating value for farmers around the world by developing and delivering
best-in-class seeds and traits is at the heart of our business. Our customers
are clear in what they want from Monsanto. They want us to continue
to invest in research and development (R&D) to help
bring innovative approaches to the farm. They want more
top-performing germplasm and trait choices across
geographies. They want a broad array of product choices
across all crops. And they want us to price our products
in a way that encourages trial and adoption.
Weve taken that feedback and used it to redefine
our strategy.
In the United States, our largest market, we continue to
see our broader seeds-and-traits portfolio offer benefits
to farmers. Respecting corn refuge requirements  the
amount of acreage on which farmers are required to plant products
without insect protection  is important, but it can also mean a lost
yield opportunity for farmers. Thats what makes Monsantos reducedrefuge
family of products so appealing. Our Genuity VT Triple PRO
corn reduces refuge from 50 percent to 20 percent in the Cotton Belt,
creating incremental whole-farm yield opportunity for farmers.

Genuity VT Double PRO corn offers
dual modes of action for greater insect
control and reduces refuge
requirements from 20 percent to
5 percent in the Corn Belt. And
our premier offering, Genuity
SmartStax, is our lead product in the
largest segment and offers 5 percent
refuge in the Corn Belt. We also are
working toward approval for Refugein-
the-Bag, or RIB.
The reality is this unique family of products offers multiple modes
of action and has us on track to offer the industrys first single-bag RIB
option, making refuge compliance more convenient for farmers.
Outside of the United States, corn in South America is a key driver for us.
Were poised to increase penetration of first-generation double-stack corn in
Argentina, where theres been an increase in acres as more growers adopt
the technology. This will serve as a building block for the technology

progression weve already seen in the
United States. The same is true for
Brazil, where stacked traits will create
new opportunities for farmers as
we prepare to introduce our secondgeneration
corn borer technology.
In Argentina, we are in our third
season of adoption for our double-stack
corn technology and have secured
regulatory approvals for our triple-stack
offering. In Brazil, our proven YieldGard
corn borer technology has been widely
adopted by farmers. In soybeans, Brazils
National Technical Biosafety Committee
approved the planting of Bt Roundup
Ready 2 Yield soybeans. This is a significant
step toward the commercialization
of our first biotechnology trait developed
for a non-U.S. market.
In India, weve seen strong adoption
of our insect-protected cotton technologies
 including Bollgard II. This is a
significant step as India has become one
of the worlds leading adopters of new
cotton technologies.
Beyond these commercial benefits,
we continue to see broad global
acceptance and recognition of technologys
role in meeting our worlds growing needs. Were pleased to see the value
this technology is bringing to 14 million farmers globally, 90 percent of
whom are resource-poor farmers in developing countries.
Beyond our current offerings, our R&D pipeline is expected to fuel growth
in our seeds-and-traits business. This was a record year for our industryleading
R&D engine as an unprecedented 11 projects advanced to the
next phase in the pipeline. Several key products, like drought-tolerant
corn and Genuity SmartStax RIB Complete, are now in latter pipeline
stages as they move to regulatory approvals.

Weve made some real changes to our portfolio and business approach,
and the positive feedback Im hearing from our customers tells me we are
on the right track. Theres still more to do. We have demonstrated agility
in the face of adversity, enhancing our portfolio and offering more product
choices at more price points. This year brought its challenges, yet just as
leaders do in all sectors, we are quickly evolving our business.
The seed business is a long-term business, and we are investing and
running it that way. Were regaining momentum both in our business and
in making agriculture more sustainable around the world. We have the
best people and the best tools, and were in the best industry in the world.
On behalf of the team, I thank you for your continued support.
We are committed to earning your investment, and that of the farmer,
for years to come.
Hugh Grant
Chairman, President and Chief Executive Officer
Oct. 27, 2010