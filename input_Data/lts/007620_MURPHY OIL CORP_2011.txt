Dear Fellow Shareholders
2
Two thousand eleven saw continuation of an overall sluggish global
economy with Euro zone sovereign debt issues, a Standard & Poors
downgrade of U.S. government debt and a tragic natural disaster in Japan all
adding further drag to any sign of economic recovery. However, oil markets
saw some strength on the back of Asian demand and shut-in of Libyan
production. The global benchmark crude, Dated Brent, ranged between
$95 and $115 per barrel for much of the year and showed strength above
U.S. West Texas Intermediate crude that was partially landlocked and in
oversupply. This differential, that at times was close to $30 per barrel,
closed near year-end 2011 to a more normal $10 as pipeline constraints
were being worked. We sold our two U.S. refineries just as this price
advantage waned.
While our international natural gas production benefited from strong
liquified natural gas market pricing, gas prices in North America continued
to fall throughout 2011 and recently dropped below $3 per million British
Thermal Units as warm winter temperatures further challenged the current oversupply picture.
Absent increased demand or new export opportunities, I expect North American natural gas prices to
be under continued pressure for the midterm. We are fortunate to have the opportunity and flexibility
to react quickly and redirect spending away from dry gas investments in North America.
Our diverse, oil-weighted global portfolio, complemented by our North American resource position,
provides the basis for strength and growth across our upstream business. We are almost two years
removed from the unfortunate and tragic events of the Macondo incident in the Gulf of Mexico
(GOM) and, while the pace of play has improved, new permits require higher levels of manpower
and much more time to complete. That coupled with fewer rigs makes for a much slower, and for
us a smaller, business. Over that same time frame, our onshore business has picked up the slack
and is on a rapidly growing trajectory.
Last year we established the framework and began executing on our strategy to reposition the
company. As mentioned, we closed on the sale of the two U.S. refineries at the end of the third
quarter and are now focused on the disposition of the U.K. refinery at Milford Haven, Wales. We are
also evaluating the potential to separate our retail business and to unlock its value, which we believe is
unrealized within the current corporate structure.
Net income for 2011 totaled $873 million ($4.49 per share), up slightly from 2010 as contributions from
Refining and Marketing, including discontinued operations, offset lower Upstream results. The lower
net income in our exploration and production segment was primarily due to an impairment charge taken
at our Azurite field, offshore Republic of the Congo. Our balance sheet remains strong as we ended the
year with a long-term debt-to-capital-employed ratio of 2.8%, providing ample capacity to act on valueadded
opportunities.

Exploration and Production
Annual production for 2011 averaged 179,400 barrels of oil equivalent per day with a weighting
of 58% to oil and 16% to oil-indexed natural gas. We were more negatively impacted by lower
production from the Kikeh field in Malaysia due to a sand/fines migration issue in some wells, lower
Azurite production in Republic of the Congo and less than budgeted wells drilled in the GOM due to
lack of permits than we were by positive results in the Eagle Ford Shale (EFS) area in south Texas and
Tupper West in British Columbia.
In 2012, we should see good year-on-year production growth with accelerated development at EFS
and the Seal heavy oil project in northern Alberta, complemented by stabilized production from Kikeh
following the successfully completed remediation program at the end of last year.
Activity in our North American resource program ramped up further in 2011. This oil-weighted
resource program now extends over 800,000 net acres with entry into a fifth play. We reorganized our
North American resource business as a single unit to bring expertise and complementary disciplines
from our successful Montney development program in Canada to the fast growing business in the
EFS. Heavy oil production growth at the Seal project is moving forward at an accelerated pace. We are
focused at Seal on additional development drilling, down spacing opportunities, multi-leg laterals and
advancing our Enhanced Oil Recovery projects related to polymer flooding and thermal stimulation as
we look to unlock the large resource in place there. In the southern Alberta Bakken play, we are in the
very early stages of evaluating the productive capability of our acreage having drilled six appraisal wells
in the play, with the most recent showing encouragement.
In addition to our North American onshore activity, in 2011 we sanctioned seven development projects
that will be important growth components starting in 2013. These oil developments include Patricia,
Serendah, South Acis, Permas and Endau in the Sarawak blocks and Siakap North in Block K, all in
Malaysia, and the Schiehallion field re-development project in the U.K. North Sea

Activity levels and results from our global exploration program were disappointing in 2011. We only
drilled four wildcat wells testing targets in Suriname, Indonesia and Brunei. The process of lining up
rigs and finalizing well schedules pushed some activity into this year, so I expect the 2012 program
will return to a more normal pace of testing 12 to 15 prospects. The year is off to a good start with
a gas discovery in Block H, offshore Sabah Malaysia, in close proximity to the Rotan gas discovery,
which is being evaluated for Floating LNG development. Impactful prospects will be drilled in the
Kurdistan region of Iraq, Republic of the Congo and Australia during the year.
Refining and Marketing
Consistent with our strategy to exit the refining business, we are now progressing on the divestiture
of the Milford Haven refinery in Wales.
The Downstream business had a very solid year turning in net income of $190 million, excluding
discontinued operations, with the U.S. Retail business contributing the second highest net income in
its history. Build out of our chain continued with the addition of 29 stations, bringing the total number
of retail outlets to 1,128 at year-end. Our U.S. retail chain broadened its partnership with Wal-Mart
and participated in the 10 cents per gallon Rollback program on gasoline prices from June through
December. Longer term we are excited about the opportunity to expand both within and outside our
current footprint with Wal-Mart.
In Renewable Energy, we completed the construction and upgrades to the Hereford, Texas
ethanol plant, which started up at the end of March, on schedule and within budget. Start-up and
commissioning went well and the plant has ramped up to its nameplate capacity of 105 million
gallons per year. Operations at our Hankinson, North Dakota ethanol plant continue to be steady
in the 120 million gallon per year range. These two plants have now been fully integrated into our
U.S. marketing business.
In Closing
The repositioning of our Company remains on track with the sale of the two U.S. refineries and
attention shifting to divestiture of the U.K. refinery. Recognizing the value of our retail business is
also in focus. The Upstream growth plan is in place with the sanction of key projects in Malaysia and
accelerated development of our oil-weighted North American resource portfolio. We will maintain an
active and impactful exploration program this year and continue to renew and replenish our prospect
inventory. Our plans are set and the team is in place to deliver on these growth initiatives.
Your support over the past year has been greatly appreciated and together we can look forward to a
Company better positioned for a bright future.
David M. Wood
President and Chief Executive Officer
February 10, 2012
El Dorado, Arkansas