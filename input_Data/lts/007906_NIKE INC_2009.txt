To Our Shareholders

Fiscal year 2009 is in the books. Over the last 12 months we had some big wins and equally big challenges. Its tempting to offer a long list, but instead Ill focus on three things that capture the scope and variety of what we do around here. Lets start with the biggest win of all  Beijing.

For three years we worked with athletes all over the world to design new products for nearly every competitive event. We created new technologies like Flywire and LunarFoam that continue to drive product innovation. We turned the Hyperdunk basketball shoe into one of the highest profile and most dramatic shoes in Olympic history. For two weeks in August we watched our products and athletes from all over the world compete and win on the biggest stage in sports. It was a great moment for Nike and for sports.

If there were any lingering doubts that China should be considered an emerging market, the Olympics responded with a thunderous, No! China is not an emerging market. It is an emerged market that combines power and potential critical to the future of any global company.

We sold our first shoes in China in 1984 when we placed 200 pairs in a 50-square-foot shop called The Friendship Store in Beijing. They sold out in 11 days. Today, nearly a year after the closing ceremony in the Birds Nest, the appetite for Nike products and athletes continues to grow. The brand is known and, more importantly, understood among many of the 500 million Chinese consumers under 25 years old. Thats a billion feet that were going after just as fast as we can.

Another big win came in October when the Nike Foundation launched the Girl Effect. In simple terms, it leverages the power that adolescent girls have to bring financial and social stability to the developing world. The response has been tremendous, not only with global leaders but more importantly on the ground in Liberia and other developing countries. I think its the most innovative solution yet to help break the cycle of intergenerational poverty around the world. And its just one example of the leadership Nike continues to show in corporate responsibility.

Were equally committed to leading our industry in climate change and sustainability. Were entering a new era of open-source collaboration that commits to sharing intellectual and patent property. Its the kind of behavioral change that can help lower carbon emissions, reduce waste, and close the loop on the resources required by product manufacturing.

We also undertook the most ambitious reorganization in the history of our company, which resulted in a significant charge associated with a reduction in force of more than 1,750 of our friends and teammates. We did it for all the right reasons  to build a leaner and stronger company, reduce costs and better leverage our resources against the greatest growth opportunities. It was a tough decision but the right decision. I wish I could say that made the layoffs easier for everybody. It didnt.

As for challenges, the biggest one was the global economy, and well continue to manage through it during fiscal year 2010. I think we can expect consumers and investors to remain cautious while economies reset around the world.

Despite these economic challenges, Nike delivered a solid year:

We generated $19.2 billion in revenue  up 3 percent year over year.
Earnings per share declined 19 percent for the year. Excluding non-comparable items earnings per share would have been up 10 percent.
The Nike brand generated $16.7 billion in revenue, another record performance, as footwear sales topped $10 billion and apparel sales topped $5 billion. Sales for the Jordan Brand exceeded $1 billion.
Converse and Hurley delivered record revenues as those brands continue to resonate with consumers in the U.S. and around the world. And while Cole Haan and Nike Golf faced very tough conditions in their market sectors, they continue to deliver innovative products and consumer experiences.
In FY09, we completed our first full year with Umbro as part of our portfolio. Financially, the business performed slightly better than we expected. Nevertheless, Umbro was significantly affected by the decline in global consumer and financial markets, reducing the value of our investment, which required us to take a charge for impairment of those assets. While were certainly disappointed in that result, we continue to view Umbro as a vehicle for long-term growth within the Nike portfolio.

On balance, we had a challenging year that we managed through with conviction and optimism. For many companies, success over the past 12 months was defined as simply surviving. As weve said in the past, we intend to do more than just survive. We plan to emerge from this downturn competitively stronger. We will do this by delivering appropriate financial performance while positioning Nike for sustainable, profitable growth over the long term.

That means well continue to do the things that keep us healthy and opportunistic:

Deliver compelling product,
Maintain the integrity of our brands while building even stronger relationships with consumers,
Strengthen and leverage our operational capability, and
Create compelling retail experiences  both in our owned retail and with wholesale partners.
We remain laser-focused on the highest potential opportunities:

Seven key markets  Brazil, China, India, Japan, Russia, U.K. and U.S.
Seven key performance and lifestyle categories  action sports, basketball, football, mens training, running, sportswear and womens training.
This category focus helps us connect in really meaningful ways with consumers. Overlay these consumer insights across our key markets and you get a very clear picture of what it takes to deliver relevant products and drive growth into the marketplace. Thats our growth strategy. Our job is to do that better than anyone else. And I certainly wouldnt bet against us.

While we see glimmers of economic recovery, we still have a challenging road ahead. We are well-positioned for the future, with our fundamentals as strong as theyve ever been  deep consumer connections, innovative and compelling product, powerful brands, operational excellence, and a strong balance sheet. Were moving forward with confidence in hand and opportunity in mind.

Thanks,

Mark Parker
President and Chief Executive Officer
NIKE, Inc.