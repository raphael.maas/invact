Creating the next chapter of our legendary legacy

2015 Letter to Shareholders

04/05/16

Dear Shareholders,



For 113 years, Harley-Davidson has given true individuals around the world a way to
powerfully express their own personal freedom. Their expression of freedom is our passion;
it's our inspiration. And it's why we create for them unmatched and unmistakable
motorcycles, accessories and apparel that offer unlimited potential for making a unique and
bold statement. The emotion and legend that is the Harley-Davidson brand crosses borders,
cultures and generations. It's more than a motorcycle; it's a way of life and a personal
expression for our millions of riders and fans that, for us, carries a responsibility we never
take for granted.




Riders with 2016 Electra Glide Ultra Classic and 2016 Road Glide Ultra



In May 2015, I became President and Chief Executive Officer of this great company, and I
could not be more honored and humbled. In my 22 years with Harley-Davidson, I've
personally heard countless stories of what our brand means to our customers. In so many
cases, their Harley-Davidson connection is far more than a product or experience; it's an
integral part of their lives. The same connection holds true for our dealers. I've seen and
worked with so many dealers who get up every day with the sole purpose of delivering
exceptional experiences. It's their life; it's who they are. As for our employees, I can't
imagine working alongside a better, more passionate and committed group of people who take
such pride in creating and building legendary Harley-Davidson motorcycles and bringing our
unique experience to every corner of the globe.



2015 Performance



Our long history has taught us to be agile and adaptable to changing circumstances and 2015
was one of those times. Last year served up one of the most competitive environments we've
seen in years. Our 2015 results are a blend of the enormity of the challenge and the
significance of our response. For the full year:

       Dealers worldwide sold 264,627 new Harley-Davidson motorcycles, a 1.3 percent decrease
       from 2014.
       Revenue decreased 3.7 percent to $6.0 billion.
       Gross margin increased 0.4 points to 36.8 percent.
       Net income decreased 10.9 percent to $752.2 million.
       Diluted earnings per share decreased 4.9 percent to $3.69.

While our 2015 results didn't fully meet our expectations, we were proud to deliver strong
value to our shareholders. Last year, we increased the dividend rate by 12.7 percent and
repurchased nearly 28 million discretionary shares of our stock, funded in part by a $750
million debt offering. Thanks to an all-out effort by our employees, suppliers and dealers, we
were able to mitigate the impact of a highly competitive market fueled primarily by currency
headwinds. We also made progress against our objectives, including demonstrating our appeal
and relevance to all customer segments globally. Here are just a few highlights:

       In August, we introduced our 2016 motorcycles, including the new Road Glide  Ultra, the
       refreshed Iron 883TM and Forty-Eight, and the high-octane S series cruisers  the Softail Slim
       S and the Fat Boy S. The S models are high-impact motorcycles developed to inspire
       customers and were instrumental in driving a mid-single digit increase at retail for our large
       cruisers in the fourth quarter.
       In the Asia Pacific region, we had our best year ever for retail motorcycle sales and our fifth
       consecutive year of growth.
       In Australia, we gained the number-one share position for on-road registrations of all
       displacements, thanks to the success of the Harley-Davidson Street 500 motorcycle.
       In Canada, we successfully transitioned to a direct distribution model, delivering a 12.3
       percent increase at retail during the fourth quarter.
       Over the last five years, sales of new Harley-Davidson motorcycles to U.S. outreach
       customers have grown at a compound annual growth rate of nearly 7 percent. In fact, in the
       U.S., we lead in the sale of motorcycles in every outreach segment  African Americans,
       Hispanics, women of all ages and young adults between 18 and 34.
       In the U.S., after three quarters of competitive pressure that included deep discounting, we
       held our ground on market share in the fourth quarter and saw favorable trends as our new
       products hit the marketplace.
       Through our rebranded Harley-Davidson Riding Academy program in the U.S., which features
       the Street 500 motorcycle, we increased the number of graduates, in part through our
       military Learn to Ride for Free initiative.
       In the EMEA region, we increased Touring sales and share by exposing riders to the distinct
       features and capabilities of our Project RUSHMORE motorcycles, including the King of All Test
       Rides campaign, which generated 100,000 test rides and delivered progress at retail as the
       year advanced.
       We continued to enhance and expand the dealer network, adding 40 new dealer points
       internationally, including China, India, Thailand, Vietnam, Germany, Poland and Mexico.
       We worked together with our employees at our York Vehicle Operations in York, Pa.,
       represented by The International Association of Machinists and Aerospace Workers Union, to
       reach a new 6 -year labor agreement. This agreement helps ensure the continued strength
       of our York manufacturing operations and our ability to respond to the needs of our
       customers globally.
       We continued to generate buzz and gain rider insights and feedback on our Project LiveWire
       electric motorcycle through the Project LiveWire Experience Tour, which continued to make
       its way across the globe with stops in Canada, Europe, Latin America and Asia Pacific.

We are proud of our efforts in 2015 to preserve key performance measures and build
foundational strengths for long-term success. We recognize there are challenges ahead, and
we are taking solid steps to assert our brand promise, enhance our company capabilities and
grow our leadership position and share in the markets we serve.




2016 Iron 883



The Road Ahead



In October, we announced our plans to build a stronger, deeper bond with our existing loyal
customers while creating powerful connections with new customers  those who already ride
and those who have not yet discovered the sport of motorcycling. We challenged ourselves to
free up resources  both financial and people  for more demand-driving focus and
investment in 2016 and beyond. These efforts underpin our long-term business strategy. Our
objectives are clear; they are aligning and motivating our entire company:
       Lead in every market  achieving and holding the number one share of the 601+cc
       motorcycle segment
       Grow the sport of motorcycling in the U.S., in part by growing the number of core customers
       and growing U.S. outreach at a faster rate
       Grow U.S. retail sales and grow international retail sales at a faster rate
       Grow revenue and grow earnings at a faster rate through 2020
       Outperform the S&P 500

To help achieve these objectives, we are substantially increasing our demand-driving
investments in marketing and product development, focused in four key areas:

       Increasing product and brand awareness globally
       Growing new ridership in the U.S.
       Increasing and enhancing brand access
       Accelerating the cadence and impact of new products

In 2016, we will increase our customer-focused marketing by 65 percent. This increased
investment will build awareness of our brand and our incredible products for new and current
riders around the world. Through the increased investment in our Riding Academy programs,
we will give more people reasons and opportunities to learn to ride. We also plan to increase
Riding Academy U.S. dealer points by roughly 20 percent to support our goal to grow the
number of U.S. graduates to 65,000 in 2016.



Our efforts to extend our reach through great new Harley-Davidson dealerships also
continues. Between 2008 and 2015, we added 176 dealers in international markets, bringing
the dreams of Harley-Davidson ownership closer to people in 95 countries across the globe.
Our plans are to further extend access to the brand internationally via 150-200 additional
dealerships by 2020.



Giving customers an unparalleled retail experience in all 1,435 independently-owned
dealerships worldwide is key to our success. Our dealers not only love what they do, but they
continue to invest and challenge themselves to be even better in delivering what we call
HDCX, the Harley-Davidson Customer Experience.



Finally, a good portion of our demand-driving investment is going to motorcycle product
development. This is the heart of what we do and we are committed to leading the industry
with the best motorcycles in the world. We will increase investment in product development
in 2016 by 35 percent over 2015. Given the step-up we made last year, this new investment
significantly increases new product development investment from 2014 levels.



We have set high expectations for ourselves in 2016 and beyond. We have loyal and
passionate customers who would be the envy of any business. We are confident our focused
strategy, passion for our brand and progress on key initiatives make us strong and well
positioned for the challenges of today's dynamic marketplace. We have exceptional
employees, dealers and suppliers who are aligned and committed to deliver dreams of
personal freedom for all those who desire to be part of this exceptional brand. Together we are
creating the next chapter of our legendary legacy.




Matt Levatich

President and Chief Executive Officer
Harley-Davidson, Inc.
