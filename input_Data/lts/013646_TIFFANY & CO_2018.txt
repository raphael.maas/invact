Dear Shareholder:
You are cordially invited to attend the Annual Meeting of Shareholders of Tiffany & Co. on Tuesday,
June 4, 2019. The meeting will be held at The Rubin Museum of Art, 150 West 17th Street (at Seventh
Avenue) in New York, and will begin at 2:00 p.m.
To attend the meeting, you will need to register online. To do so, please follow the instructions in the
Proxy Statement on page PS-9. When you arrive at the meeting, you will be asked to provide your
registration confirmation and photo identification. We appreciate your cooperation.
Your participation in the affairs of Tiffany & Co. is important. Therefore, please vote your shares in
advance regardless of whether or not you plan to attend the meeting. You can vote by accessing the
internet site to vote electronically, by completing and returning the enclosed proxy card by mail or
by calling the number listed on the card and following the prompts.
I am pleased to report that Tiffany achieved record net sales of $4.4 billion in 2018, and net earnings
were $586 million, or $4.75 per diluted share. However, the first and second halves of the year
were markedly different. In the first half, growth was strong across regions and product categories,
attributed to spending by both local customers and foreign tourists. Spending then softened in certain
regions and customer segments in the second half, which we believe can be at least partly attributed
to external pressures, uncertainties and distractions that may have affected customer spending.
Most important, you will likely recall that I made reference one year ago to Tiffany being at the
beginning of a long and exciting journey intended to deliver increasing excitement and relevance
to our customers, and greater returns for our shareholders. Of course, any journey can include
short-term pauses, but I continue to believe that we are in an excellent position to achieve success
over the long term. As such, we will stay focused on specific growth opportunities and not be
distracted by external challenges.
To that end, one year ago we established a set of six key strategic priorities developed to support
sustainable, long-term growth in global sales, operating margins and net earnings. Since then,
our entire organization has been focused on these priorities: (i) Amplifying an evolved brand message;
(ii) Renewing our product offerings and enhancing in-store presentations; (iii) Delivering an exciting
omnichannel experience; (iv) Strengthening our competitive position and leading in key markets;
(v) Cultivating a more efficient operating model; and (vi) Inspiring an aligned and agile organization
to win. In connection with those priorities, we also increased strategic investment spending to higher
levels that we believe are sustainable and necessary to strengthen our ability to achieve that
long-term growth.
All in all, I believe we made much progress against those strategic prior

All in all, I believe we made much progress against those strategic priorities in fiscal 2018.
A fresh modern marketing campaign, Believe In Dreams, embodied our new brand message,
�legendary beauty with freedom, wit and joy.�
We launched a beautiful and modern new jewelry collection, Tiffany Paper Flowers�
, our first such
collection in many years. Its debut in New York City in May was followed by regional rollouts in Europe,
Japan and Asia, including a major launch event in Shanghai. At the end of the year, we were also
excited to launch in North America a limited selection of Tiffany True, a new collection of diamond
rings and jewelry that perfectly represents modern love, and which will be launched globally
throughout 2019. And, of course, new design interpretations were added to some of our popular
collections, such as Tiffany T, Tiffany Keys, Tiffany HardWear and our classic Return to Tiffany� collection.
Our new Make It My Tiffany program highlighted all the ways our clients can personalize Tiffany jewelry
or a gift to make it even more special.
Our celebrated Blue Book collection of High Jewelry was enthusiastically received by our customers.
In addition, we made a significant strategic investment during the year to increase the inventory
levels and global availability of our High Jewelry assortment in key markets.
To support Tiffany�s ability to maintain a powerful and expanding assortment of extraordinary jewelry
offerings, over the past two decades we have invested in and developed a formidable global supply
chain. We benefit from the expertise of several thousand Tiffany professionals with diamond polishing
and manufacturing skills who are focused on ensuring availability of the highest quality diamonds
and jewelry to meet customers� expectations.
And to foster a more efficient pace of jewelry innovation, during 2018 we opened Tiffany�s first-ever
Jewelry Design and Innovation Workshop in New York City that enables us to have designers,
model-makers and engineers collaborate side by side and bring our designs to life.
Corporate social responsibility, including environmental sustainability, is a core element of our
brand. Related to that, we launched a new Diamond Source Initiative whereby customers can know
the geographical countries or region of origin of their individually registered diamonds, which is a point
of difference for Tiffany in the luxury jewelry market. And we progressed in the nurturing of talent,
diversity and inclusion for our employees throughout the organization.
I believe that our brand is increasingly recognized and desired, and Tiffany is investing to strengthen
its presence and competitive position and lead in key markets. During 2018, we expanded our
global store base, opening 10 stores across Asia-Pacific, the Americas, Japan and Europe, closing
four and relocating/renovating others. Creative visual merchandising is also playing an increasing
role in updating the interior look and feel of our existing stores.
We were also very excited to announce a major project intended to transform our important New York
City Flagship store into a 21st-century retail experience, with an anticipated completion by the end
of fiscal 2021. Tiffany owns that Flagship store, built in 1940, and we have developed unique plans
to expand the customer-facing space of that 10-story building, located at what is arguably the most
important intersection of luxury in the world. And The Blue Box Cafe� on the 4th floor of that store
continues to delight customers from New York and around the world, offering them a tasty experience
to enjoy breakfast (lunch or tea) at Tiffany�s.

We are committed to delivering a more relevant and interesting omnichannel experience to customers
globally, and internet and catalog sales rose strongly in the year, representing 7% of worldwide net
sales. We offered e-commerce enabled websites in 13 countries at year-end, and plan to add that
shopping capability to our website in China during 2019.
We moved boldly forward in 2018, and will continue to do so in the future, and believe that Tiffany�s
balance sheet provides us with the strength and flexibility to pursue our strategic priorities regardless
of short-term pressures. We finished the year with $855 million of cash and short-term investments,
and total short-term and long-term debt represented 32% of stockholders� equity.
During the year, our Board of Directors approved a 10% increase in the quarterly dividend rate,
representing the 17th increase in the past 16 years. Tiffany also returned capital to shareholders by
spending more than $400 million to repurchase approximately 3.5 million shares of its Common Stock.
In closing, it was an exciting and eventful year for Tiffany. I believe that our multitalented organization
is aligned with our goals and is becoming more agile to evolve where appropriate, and is focused
on winning! More than 14,000 employees around the world are contributing to Tiffany�s success and
I appreciate their skills and dedication.
With solid progress being made against our strategic priorities, I believe that the long-term outlook is
very promising for this amazing Company. We look forward to updating you on our progress.
Sincerely,
Alessandro Bogliolo
