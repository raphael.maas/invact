Agilents first year as an independent, public company was a
great success.
 We completed our separation from Hewlett-Packard (HP) 
probably the most complex such split ever done  on a very aggressive
schedule.
 We achieved outstanding financial and business results, including
excellent growth and profitability and the introduction of many
innovative products.
 We are faster and more customer-driven than ever before.
None of this could have happened without the heroic efforts of 47,000 talented
and energetic Agilent people around the world. They launched our company,
delivered strong results and built a foundation for reaching the ambitious goals
we share for Agilent.
Prior to Agilents initial public offering (IPO) of stock, we expected
to achieve net profit for 2000 of between 5 and 6 percent of revenue. At midyear
we revised that guidance to a little bit above 6 percent net profit. For
the full year, our net profit margin was 7.0 percent. This translates into net
earnings of $757 million, or $1.66 per share. Im very pleased that we met
the commitments we made on financial performance.
At the start of the year, we set four goals: complete the separation from
HP, accelerate growth, launch operational initiatives and transform the Agilent
culture. In this letter Ill review our progress in each of these areas, and Ill
spell out our priorities for 2001.
We completed the separation from HP: Thousands of Agilent and
HP people worked tirelessly to separate hundreds of information technology
(IT) systems and applications, more than 600 physical sites and an extensive
portfolio of patents and intellectual property  all of which had become deeply
intertwined over 60 years. On June 2, 2000, the two companies became
completely independent.
During 2000 we incurred two kinds of costs that we had planned for
and that will be lower in 2001. The first was for branding to establish Agilents
name and identity worldwide. This year we spent about $120 million on branding,
and were very pleased with the results to date. We will continue this
essential work to establish our identity in 2001, but at reduced spending levels.
We also spent approximately $250 million, as anticipated, in costs
related to operating as a stand-alone company. On our own, we have systems,
processes and governance activities that we previously shared with HP. As we
continue to tailor these functions to Agilents needs, we expect to start to
achieve savings by the end of 2001.
We achieved very strong growth: For the full year, Agilents orders
rose 35 percent and revenue increased 29 percent. This compares with 1999
order growth of 12 percent and revenue growth of 5 percent.
We achieved strong growth because we provide key enabling products
and technologies to customers who are revolutionizing communications and
electronics as well as the life sciences. Agilent provides best-in-class products
and services to industries that are growing rapidly.
In communications and electronics, our products are on the critical path
for companies that are delivering the most advanced optical networking,
wireless communications and Internet capabilities. During 2000, orders in our
businesses that serve communications and electronics grew 49 percent over
1999 while revenue increased 43 percent.
In life sciences, we provide tools that enable pharmaceutical and
biopharmaceutical companies, and other research organizations, to discover
the origins of disease and accelerate the development of new drugs based on
scientists increasing knowledge of genetics. The first-draft mapping of the
human genome, completed during 2000, ushered in the next phase of the life
sciences revolution in which Agilent products will help customers understand
more fundamentally how genes create diseases and how drugs can cure them.
This was a banner year for new products, and our growth was driven by
product introductions that won strong market acceptance. This years annual
report describes a good number of these major introductions. One key to our
ability to innovate is Agilent Laboratories, our central research facility, whose
goals are to address growth opportunities in our current businesses and to
create new businesses. This year we spent $1.3 billion, or 11.7 percent of net
revenue, on all our research and development across the company.
We launched operational initiatives: We undertook a number of
initiatives designed to align our systems, processes and cost structures to our
business requirements. We are tailoring our IT systems, internal processes,
manufacturing, sales and customer support programs to make Agilent even
faster, easier for customers to work with and more cost-effective.
We began to shape Agilents culture: One of our new companys
great strengths is our HP heritage, especially our foundation values: integrity;
trust and respect for individuals; teamwork; and the importance of making a
real contribution to customers and industries. These values remain crucial
to Agilent.
Were also putting increased emphasis on values that are essential to our
success going forward: focus, speed and accountability. During 2000 we took
a number of actions designed to strengthen these values and to encourage the
behaviors that bring them to life.
We implemented a pay-for-results program for our 800 top managers
and supervisors, a portion of whose compensation is now linked to Agilents
revenue and profit goals. We trained more than 6,000 managers around the
world on a new set of leadership expectations based on our values. Finally,
we rolled out a new compensation system, stock option program and employee
stock purchase plan in order to reward the individual accomplishments that
will help Agilent reach its goals.
Challenges We Addressed
We faced real challenges this year as well. Industrywide parts shortages meant
that our shipments to many of our customers couldnt keep pace with demand
in the third quarter. We worked very hard with suppliers and took extraordinary
measures within the company to address these shortages, and by the end of
the year we had made a lot of progress.
In our chemical analysis business, growth in traditional markets 
chemical and petrochemicalwas sluggish. We have been increasing our
focus in this business on exciting opportunities in life sciences, and the
response to our life sciences products introduced this year was extremely
good. We believe our life sciences business has a bright future as a provider
of enabling technologies to researchers who are developing drugs and
diagnostic techniques based on genetics.
Our healthcare solutions business had a disappointing year. This business
was affected by fundamental changes in the healthcare market, particularly
in the United States. Size and scale are essential to winning in healthcare,
since customers increasingly want to work with companies that offer a broad
product line.
Given this trend, we faced a decision: Should we make the investments
necessary to bring our healthcare business to the size needed to succeed?
After much analysis and thought, we decided that the best course of action
for our customers, shareholders and employees was to divest this business.
In November 2000 we announced an agreement under which Philips, a leader
in the healthcare market, will acquire our healthcare business, subject to
customary regulatory approvals and other closing conditions.
This was a difficult decision that was made easier because Philips is
the ideal company to acquire this business. There is a great fit between our
This was a banner year for
new products that won strong
market acceptance.
healthcare solutions and Philips product lines and geographic strengths.
Together, our healthcare business and Philips can offer the broad product line
that is essential to success in this market.
We overcame challenges in 2000 while making extraordinary progress
in creating Agilent as a high-growth, high-performance company.
Priorities for 2001
Since March 2, 1999  the day HP announced plans to create two new
companies from the existing HP  our vision has been to build a high-growth,
high-performance company for the long term.
To do that, we have to become even more customer-driven. In todays
fast-moving, intensely competitive markets, we are focused on customers,
Agilent and our individual businesses  in that order.
Our priorities for 2001 reflect our determination to meet, and exceed,
the commitments we make.
Customer satisfaction: Our goal is to distance ourselves from
competitors through superior quality, consistent on-time deliveries and ease
of doing business with Agilent.
Operational excellence: We will continue to execute our plans to
make Agilent faster, even more responsive and more cost-effective.
Growth: We want to grow faster than our best-in-class competitors
as well as enter new markets. To do this, we have ambitious new-product
programs in place across the company.
Our people: We will accelerate our work to develop a culture based
on focus, speed and accountability, in which people embrace change, lead
rather than manage and are rewarded for results.
Agilents first year as an independent company was the most exciting and
gratifying of my 34 years with HP and Agilent. We benefit enormously from
our heritage but are intently focused on the future.
We have extraordinary people, industry-leading customers and a deep
technology portfolio that we can apply to address opportunities and create
new businesses. Were determined to build on an outstanding first year in
ways that will delight our customers, employees and shareholders.