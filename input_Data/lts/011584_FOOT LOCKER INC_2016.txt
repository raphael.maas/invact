Dear Shareholders, 
I am very pleased to welcome you to Foot Locker, Inc.s 2016 Annual Report, which will guide you through our Companys many successes during the year just completed. I can sum up our outstanding 2016 performance in a single sentence, though: We sustained the strong momentum built over the last several years by generating another year of all-time best financial and operating results as an athletic footwear and apparel company. In todays challenging, fast-changing retail environment, this was an exceptional achievement, and one for which all credit must go to the incredibly talented team 
of associates our Company has around the world. Yet over the course of the year we also accomplished something less tangible but equally important: we clearly demonstrated that our Companys banners are at the center of sneaker culture. This is a critical attribute of our business that our customers and suppliers have understood for a long time, and in this report, I will describe what it means to be at the center of sneaker culture, and how we work hard every day to maintain this position with our customers. 
Being at the center of sneaker culture may be the single most critical strength we have developed as we strive to achieveour vision to be the leading global retailer of athletically inspired shoes and apparel. As many of you know, we started on our journey to achieve this vision seven years ago, and 
each year we have made tremendous progress. In fact, we have established our Company as a leader across multiple dimensions: in our global footprint; in our relationships with our outstanding suppliers; in the seamless connection of the channels through which we engage with our customers; in the diversity of our banners, families of business, and product categories; and, above all, in the unequalled team of associates whose passion it is to serve our customers every day. In last years report, I described these leadership attributes in some detail. This year, I will 
demonstrate how all these leadership qualities have fused together naturally to position ourbanners at the center of 
sneaker culture. 
Having a deep and constantly evolving understanding of the core customers of each of our banners is the most critical prerequisite for attaining this central position. True, each of our banners sells premium sneakers, and almost all of them also sell athletic apparel and accessories. But what may appear at first glance to be subtle differences between the assortments and experiences across our banners is, in fact,a key differentiator for us. We consistently create authentic and memorable experiences for our customers, startingwith the enthusiasm and talent of the store associates who interact with them; the high quality and freshness of the store environments into which we invite customers to shop and buy; extending to the exciting experiences we provide on our customers digital devices; and, most importantly, including the compelling, trend-right, and often exclusive assortments we provide from a variety of outstanding vendors. It is this powerful combination that we believe sets us apart in the athletic retail industry. 
In the pages that follow, we provide you with a high-level introduction to the core customers of each of our banners our muses. With these short introductions, we are only scratching the surface of what we know about each of these unique sneaker lovers. There are some common features to most of them. First, they tend to be young (teenaged to 20s). Second, they are connected to their world on mobile devices seemingly 24 hours a day; consequently, they are constant consumers of, as well as active participants on social media. While often focused on their local communities, this same social media connection also creates a very global outlookfor our customers, who get their style cues from a variety of sources, ranging from athletes (including both performance and increasingly visible off-court or lifestyle trends) to musicians to celebrities of various backgrounds. Finally, our male customers, especially, define their personal style through their sneakers, consistently making athletic footwear one of the most important purchases on their shopping lists. In addition to these common attributes, it is understanding the differences 
between these muses and what they are looking for, and tailoring relevant experiences for each of 
them, that is vital to staying at the forefront of our customers minds when they decide they 
need a fresh pair of kicks or the latest in athletically-inspired apparel. 
3 
TOTAL SALES EARNINGS PER SHARE 
$4.9 
$5.1 
$2.47 
$5.6 
$2.87 
(in billions)$7.2 
$6.1 $6.5 
$7.8 
$7.4 
$4.82 
$4.29 
$1.82 $1.10 
$0.54 
$3.58 
2009 2010 2011 2012 2013 2014 2015 2016 2009 2010 2011 2012 2013 2014 2015 2016 
We only have room in this report to highlight eleven of our muses. In fact, we have several more core constituents the parents of our Kids Foot Locker consumers, for example whom we count amongst our muses. Nonetheless, this introduction should help you appreciate the strong position we have built with our customers, which gives us the confidence that we can continue to deliver strong financial and operational results well into the future. 
2016 HIGHLIGHTS 
Before we talk more about the future, though, let me briefly review the past, including the very strong results of 2016, which was the second year spent executing the specific strategic framework and priorities shown below: 
Customer Engagement 
The power and relevance of these initiatives, as well as our teams outstanding execution of them, can be seen in the financial success we achieved in 2016, as shown in this brief list of highlights: 
		Sales totaled $7.8 billion, the most in our history as an athletic company; 
		A full-year comparable store sales gain of 4.3 percent, our seventh consecutive year of significant sales growth; 
		Operating income reached $1.0 billion, the first time our Company has attained this milestone; 
		Gross margin and operating expense rates both improved, as did our rate of Earnings Before Interest and Taxes, which reached a record 13.0 percent; 
		Earned net income of $4.82 per share, a 12 percent increase over 2015, and the seventh year in a row in which we have driven a double-digit percentage increase in earnings per share; 
		Opened 96 new stores, including several pinnacle retail experiences  highlighted by our new flagship Foot Locker and SIX:02 store on 34th Street in midtown Manhattan  and remodeled or relocated 218 other stores; 
		Increased sales per gross square foot to $515. 
People 
Core Business 
		Drive performance in the Core Business with compelling customer engagement 
		Expand our leading position in Kids 
		Aggressively pursue European expansion opportunities 
		Build our Apparel penetration and profitability 
		Build a more powerful Digital business with customer- focused channel connectivity 
		Deliver exceptional growth in Womens 
		Build on our industry-leading team by embracing the power of our People 
4 
Kids 
Europe 
Apparel 
Digital Womens 
PROGRESS ON STRATEGIC INITIATIVES 
Due to the outstanding skill and execution of our associates throughout the organization, we continued to make progress on each of the strategic initiatives outlined on the previous page. 
Our core business, which we define as selling mens footwear in the Foot Locker, Champs Sports, and Footaction banners in the United States, Canada, Australia, and New Zealand, continued to grow steadily in 2016. This progress, which built on the exceptionally strong results in 2015, was especially encouraging given a significant slowdown in the basketball category. In fact, it was the resilience of our core business 
in the face of pressure from basketball which probably best illustrates how important and successful the work was that we have done over the last several years to expand our leadership position in product categories such as lifestyle running, casual, and classic sneakers. 
Our kids business also posted very strong growth in 2016. Fortunately for us, kids today are growing increasinglysavvy about sneakers at ever younger ages. It is a passion sometimes handed down from their parents, who also grew up wearing sneakers, and sometimes the kids themselves push the parents into the fun and exciting world of sneaker culture. Either way, it is a natural market for us to pursue, which is one reason we opened 45 new Kids Foot Locker stores during the year, the majority of which were in the United States, with several in Europe and Canada, as well. Sales of childrens footwear also increased substantially in our adult banners. 
Our Foot Locker banner in Europe produced another strong year. Although sales did not increase as rapidly in 2016 as they had in recent years, productivity and profitability remained high, and Foot Locker Europe maintained positive momentum across mens, womens, and kids products, as well as across footwear, apparel, and accessories. Our two newer European banners, Runners Point and Sidestep, both of which are primarily based in Germany, struggled with a retail climate that saw significant declines in traffic there. Runners 
Point in particular was challenged by the ongoing consumer preference shift to lifestyle footwear from more performance-based product,which had traditionally been its 
strength. 
Our apparel business did very well in 2016, especially in terms of improved profitability. While sales momentum was mixed across our banners, our profit margins improved 
fairly consistently, as we focused our energy on premium, mostly branded assortments that complemented 
our strong footwearbusiness. We have long said that we believe apparel margins, which are lower than margins on footwear today, should be meaningfully higher than footwear margins, and we closed the gap by elevating our assortments, being on trend in key categories such as windwear and graphic tee shirts, and thereby lowering markdowns. 
The investments we have made in our digital businesses continue to pay off, with overall digital sales posting an 8.3 percent increase even as Eastbay, our single biggest online banner, had an off year with a mid-single digit sales decline. Our store banners online sales increased more than 20 percent, and the proportion of online sales to each banners total sales increased significantly in almost every case. Most importantly, the vast majority of sales in our store banners involve one or more digital interactions prior to sale, a sure indicator that the seamless integration of our sales channels, while still a work in progress, is making tremendous strides. 
Sneaker culture, and the appeal of athletic footwear and apparel, is clearly not just a male phenomenon. Our womens business accelerated significantly during 2016, led by womens footwear sold in our male-led banners. Our newest banner, SIX:02, also delivered strong sales growth, especially after the opening of our new flagship store on 34th Street in Manhattan. Our female customers have shown that they respond just as passionately to the latest athletically-inspired products, backed by the right style influencers and delivered in an exciting store environment. 
The accomplishments outlined above do not happen without the excellent teamwork and leadership of each one of Foot Locker, Inc.s people. I have said it before, and I will say it 
again, it is a very humbling experience being the Chief Executive Officer of such a talented 
and outstanding team. I want to thank each of our associates around the world for their valuable contributions, the 
sum of which has led to our Company producing several consecutive years of record financial and operating results 
 and claiming our place at the center of sneaker culture. 
5 



A BALANCED APPROACH TO CAPITAL ALLOCATION 
With the solid financial position created by our consistently strong operating performance, we were well-positioned in 2016 to elevate our investments in the future of our business, while also increasing the amount of cash returned directly 
to shareholders. We invested a total of $284 million into our stores, digital sites, logistics network, and other infrastructure. This level of capital expenditures represents a peak for us, given the substantial investment we made in two flagship properties in New York City, one on 34th Street and the other in Times Square. In April, we also moved two blocks west along 34th Street into our new corporate headquarters, which is a much more modern, energizing, and collaborative environment. 
In February, 2017, our Board of Directors approved a capital program for the coming year of $277 million, as we continueto lead the industry in the investments we are making in our store remodel and vendor shop-in-shop programs, our website and mobile capabilities, training programs and technology to enhance the effectiveness of our associates, and more pinnacle new stores in cities such as Chicago, Los Angeles, Toronto, Sydney, Melbourne, and Paris. 
While we believe these investments will help us achieve our 2020 financial objectives, at the same time we are also committedto returning cash to our shareholders, and this February our Board increased the Companys dividend 13 percent above the 2016 level, to a quarterly payout rate of 31 cents per share. This increase marks the seventh consecutive year with a dividend increase in the 10 percent range. 
We also spent $432 million to repurchase and retire almost seven million shares of our common stock in 2016, the most ever spent on share repurchases in the Companys history, and in February the Board authorized a new three-year $1.2 billion share repurchase program which we have already begun to execute. 
CONCLUSION 
As I mentioned at the beginning of this letter, the success of Foot Locker, Inc. in 2016  and the continued success we believe we can deliver as we step confidently into the future  truly rests in the hands of our global team of associates, who continue to do outstanding work to stay engaged with our customers. I want to thank them for the work that they have done, and the work I know they will do, to generate top quartile results such as we delivered in 2016. I also want to thank our 
Board of Directors, which has been invaluable in their support for me and the entire leadership team at Foot Locker, Inc. This year, I am very pleased to welcome two new members to our Board, Kim Underhill and Ulice Payne, Jr., both of whom I know will provide broad global leadership experience and unique insights to our Company. 
We also have one member of our Board, Nick DiPaolo, who is retiring this spring when his term as a director expires, and I want to recognize Nick with a special personal thanks. Not only has Nick been on our Board for 15 years, he served as Lead Director for several years, including during my transition to Chief Executive Officer. He then served in the capacity of Non- Executive Chairman of the Board as my mentor until I became Chairman myself last year. Nick has truly been an indispensable partner to me and the entire Company, and I want to express 
my sincere gratitude to him for his tremendous leadership and guidance. 
Finally, I want to thank you, our shareholders. In my interactions with you over the last few years, I am consistently impressed with the passion you share for our business and the trust you have in the ability of our team to execute our strategic initiatives to reach the ambitious goals we have set for ourselves. 
Together with our innovative suppliers, landlords, and other partners, I am excited about our direction and believe we have the foundation to achieve our vision of being the leading global retailer of athletically-inspired shoes and apparel. 
Richard A. JohnsonChairman and Chief Executive Officer 
