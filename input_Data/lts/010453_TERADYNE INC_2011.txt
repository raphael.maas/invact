Fellow Shareholders,

We posted another solid year in 2011 despite a slowdown in worldwide
semiconductor device growth and constant uncertainty in world markets.
Cyclical pullbacks in capital spending are not new to our industry and
we saw another one in the second half of the year. While it wasnt the
freefall of 2009, the test equipment sector did drop over 40% from the
peak to trough quarter in this cycle..dizzying for some industries, but
normal for ours.
Despite that correction we performed very well, posting another strong
year of operating profit and cash flow and registering our best back-toback
financial results in over ten years. That was accomplished through
strong System-on-a-Chip (SOC) performance, growth in memory test
and record numbers from our combined hard disk drive, defense and
commercial board test businesses.
In the last four years, weve moved the Company from one whose results were determined mostly by market swings to one that
delivers industry-leading performance independent of market gyrations. Three things have made this possible:
1. Financial discipline combined with a relentless focus on our core semiconductor test business, where we invest over $150 million
every year to extend our SOC test market leadership and grow our share in memory test;
2. Entry into new markets, like storage test, through home-grown innovations including the Neptune hard disk drive test system;
3. Strategic acquisitions that strengthen our core or extend our reach into high-growth adjacent markets.
When you combine these initiatives with the strongest financial model in
the industry, the results are impressive. As you can see in the adjacent
chart, this strategy has moved our top line into the $1.5 billion range, up
36% from the level we were at just three years ago when the worldwide
semiconductor test market was a comparable size. And our operating
profit rate has increased by a factor of 7 from 3% of revenues to 21%.
In 2008, we led the consolidation of the semiconductor test market with
the acquisitions of Eagle Test and Nextest and those business units have
proven themselves with segment leadership in analog test and strong
market share growth in memory test. Our storage test business has
delivered $300 million in revenues since its launch less than three years
ago and has become the industry standard for testing 2.5 inch mobile
and enterprise hard disk drives.
Our prospects for future growth have been greatly enhanced with the
acquisition of LitePoint late in 2011. LitePoint provides wireless test
solutions for the latest generation of mobile consumer products like
smart phones and tablets. That test market today is about $1 billion and
is growing at 8% to 10% per year. We expect that LitePoint will represent
at least 10% of Teradynes total sales in 2012 and will outpace the robust
wireless test market growth in the years ahead.

The acquisition of LitePoint reveals much about our vision of
the future. Mobile computing is the most powerful innovative
force in the electronics industry today. Test technology touches
the creative process at every step as products are envisioned,
designed and mass-produced. Our leadership at the IC test level
in semiconductor fabs and assembly lines around the world is
unquestioned. Now LitePoint extends our reach into the final
stage of test, verifying and calibrating the performance of wireless
products such as smart phones, tablets and the internet of things
that communicate without human involvement.
But LitePoint is only a piece of the larger Teradyne picture.one
which includes a zealous focus on customers, constant financial
discipline and prudent use of our hard-earned capital resources.
Last year I told you that we had finished our first fifty years on an exceptional note and we were eager to start the next fifty. 2011 was
a very good start...but we continue to look to the future. That future includes:
1. Steady market share growth in semiconductor test through superior test solutions;
2. Outpacing the wireless test market growth with LitePoints expanding product portfolio;
3. Strong over-the-cycle financial performance with each business unit pulling its weight;
4. Careful deployment of our capital.
Im extremely proud to be at the helm of Teradyne. I dont recall any time in the past where weve been as strong or have had
as much promise looking forward. Our customers count on us every day to solve their most critical test challenges. You, our
shareholders, can count on us too.

Michael A. Bradley
President & Chief Executive Officer
Teradyne, Inc.