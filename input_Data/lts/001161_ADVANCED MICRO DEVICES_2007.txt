To Our Stockholders:
There can be no doubt 2007 was a
tough year for AMD. Although we made solid progress as the year went on,
our overall financial results were unacceptable. Within this challenging year, however, there was much to have
confidence in. Two of the principal causes of our poor financial performanceloss of microprocessor market
share in the distribution channel and the delay in volume shipments of our QuadCore AMD OpteronTM
processorhave been successfully addressed. And as you will see throughout 2008, we set the stage in 2007 for
new product cycles across all our business units that could collectively create a multi-billion dollar market
opportunity for AMD.
Most significantly in 2007, we completed the integration ofATI Technologies, positioning AMD to pursue many
new and fastgrowing markets. We launched important new products and platforms across all our businesses,
producing top-line momentum. And we secured yet another global PC OEM, Toshiba, who we expect will be an
important partner as we work to continue growing our notebook processor business. Our Consumer Electronics
Group is pushing into new markets with partners like Samsung, who will use our AMD XilleonTM family of
panel processors to bring advanced video processing to the rapidly growing HDTV flat screen market. As
promised, we shipped hundreds of thousands of the industrys first and only native quad-core x86 processors into
llre market. And finally, we ended 2007 shipping a record number of microprocessor units, increasing 9 percent
yearover-year compared to 2006.*
2007 represented a critical juncture in AMDs evolution, and both the lessons and successes of the past year will
serve AMD well into the future. Looking forward into 2008 and beyond, I am confident 2007 will be
remembered as the year AMD showed the grit1 determination and stamina necessary to achieve our longstated
goal of bringing greater growth, competition and choice to our industry
2007 Corporate Results
Our progress in 2007 was highlighted by the improvement in our second half financial results. Average selling
prices for our microprocessor products improved in the second half of 2007 compared to the first half of 2007, as
our product mix improved with the introduction of our Quad7Core AMD Opteron processors for servers and
AMD PhenomTM 9000 series of quadxore processors for desktop PCs. Gross margin improved through each
quarter in 2007, and was 44 percent for the fourth quarter of 2007. Graphics net revenue in the second half of
2007 increased 30 percent compared to the first half of 2007. From the first half of 2007 to the second half of
2007, GPU unit shipments increased by 19 percent and average selling prices increased by 9 percent.
In 2007, we also undertook key measures to improve our financial position. We completed two convertible notes
offerings, raising $3.7 billion in proceeds. We used a portion of the proceeds to repay in full the amount
outstanding under the October 2006 term loan agreement with Morgan Stanley Senior Funding, Inc. In the fourth
quarter of 2007, we sold and issued 49 million shares of our common stock to a strategic investor in exchange for
net proceeds of $608 million.
For 2008, we have a primary goal: achieve operational profitability in all of our business units, led by topiline
growth. And we intend to do so by capitalizing on the vast opportunities within our reach.
2008: Quad-Core Delivered
In 2008, we plan to deliver quadxore processors in significant volumes to the server and desktop markets. We
expect the ramp of our updated QuadiCore AMD Opteron processors will begin late in the first quarter. All of
our strategic OEMs, along with our channel partners, are committed to delivering systems based on these
processors. And with hundreds of upgradeable dualicore AMD Opteron platforms already available on the
market, AMD has a significant opportunity for growth in this area.
The Quad7Core AMD Opteron processor represents a significant advancement in business computing, delivering
on customers demands for industryileading energy efficiency, optimal virtualization, balanced performance and 
superior investment protection. With these processors, AMD is enabling businessesibig and smallito increase
the productivity of their servers and workstations, save time and costs, and maximize their IT investments. The
updated QuadiCore AMD Opteron processor remains the most advanced x86 processor in the industry.
Quadicore technology will become more relevant in 2008 as computing users demand more immersive, high,
definition 3D graphics. Introduced in late 2007, our quadicore platform for highiperformance desktop PCs,
codenamed Spider, is powered by an AMD innovation tripleiplayithe AMD Phenom quadicore processor,
ATI RadeonTM HD graphics technology, and the AMD 778eries chipset. It delivers the computing muscle
necessary to run multiithreaded digital media applications and is capable of delivering the ultimate visual
experience.
In 2007, our Dresden facility excelled in guiding new manufacturing process technologies from the R&D stage to
highlycfficient, volume production. The first 45nm quadicore products are running on platforms both inside
AMD and at customers in trials. General availability is planned in the second half of the year.
2008: Graphics and Consumer Electronics Leadership
Visuals now define the computing experience for all of us, as evidenced by the insatiable craving for high,
definition televisions, fullifeatured multimedia smart phones, and graphicsiintensive computing devices. AMD is
relentless in its pursuit of multimedia excellence to ensure that we enjoy the ultimate visual experience across all
the screens of our lives.
In 2007, AMD continued to push the limits of graphics technology innovation. We launched a new graphics
architecture and new GPUs to favorable reviews and demonstrated our ability to deliver industryileading gaming
and multimedia visual performance. With our foundry partner TSMC, we also delivered the industrys first 55nm
GPU. This process technology shrink can translate into processors with equivalent performance, but lower power
requirements.In 2008, AMD plans to introduce new, successor ATI Radeon GPUs. These processors are being designed to
offer breakthroughs in 3D performanceiperrwattiperidollar and offer leading HD video quality.
In the consumer electronics business, AMD plans to expand its footprint in the digital television and handheld
marketsl Already this year, we have grown our portfolio of technologies targeted for LCD TVs, mobile phones
and handheld devices And we are prepared to take advantage of the mass conversion to digital TV with our
AMD Xilleon processors that are well suited for digital converter box makers.
2008: Platform Differentiation
In 2007, AMD began the transformation from a product provider into a solutions and platform provider, and we
remain the only company offering CPUs, chipsets and discrete GPUs. We plan to build on that momentum in
2008, and design our industryrleading energycfficient processing and graphics technologies into all of our
platforms.
In 2008, we plan to unveil a
tripleicore platform based on the new AMD 780 chipset for mainstream desktops,
known as Cartwheel. Cartwheel will represent an evolutionary leap in the highrdefmition and 3D graphics
capabilities of mainstream PCs.
We are set to unleash our nextigeneration platform for notebook computers, codenamed Puma", in the second
quarter of 2008. Puma is designed to deliver exceptional visual performance and energyrefficient mobility.
Already we are experiencing record customer acceptance for Puma, and we expect we will nearly double the
number of design wins at launch as compared to the AMD TurionTM 64 X2 launch.
Also critical to our platform strategy is the planned launch of our AMD Business Class solutions, fostering
greater development of AMD commercial desktop and notebook platforms. AMD Business Class desktop solutions are designed to enable greater productivity through increased application and graphics performance.
AMD Business Class users will appreciate the energyefficiency features that keep power consumption in check,
while IT managers can count on up to a 247month image stability period to minimize IT disruptions.
A Look to the Future: Raising Expectations
Our suocess over the years has bred higher expectations for AMD~from our customers, our partners, and you, our
stockholders. Our success has also increased the expectations for choice in our industry, and the customericentric
innovation that occurs when fair and open competition exists. AMDs strength is knowing what the customers want
and expect. Customers expect technology solutions that address real business problems, not just raw performance
benchmarks. Partners expect collaborative relationships that advance the agendas of all participants. Finally, we
know what you, our shareholders, expect. You expect reliable and consistent financial performance.
As we enter 2008, you and our customers are not alone in having great expectations ofAMD. We have great
expectations of ourselves and I have enormous confidence that those expectations will not go unmet in the years
to come.