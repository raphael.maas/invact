dear customers, 
employees and shareholders
Nordstrom is not a faceless company. It includes our salespeople, department managers, 
store managers, merchants, support personnel, and our board of directorsall working
together to achieve sustained improvement in our results by providing better service to our
customers. Ours is not a one-quarter, two-quarter, or even a 2003 story. Its a story focused 
on constantly striving to be better, supported by a well-established culture: competitive and
empowering, challenging and supportive, flexible in responding to market opportunities, 
and unyielding in working every day to warrant the trust of our customers, employees,
shareholders, and business partners. 
Our progress in the past year is measured in small but meaningful steps. We set out to 
improve service, drive top line sales, reduce expenses, and implement our perpetual inventory
system nationwide. Were pleased to report we made progress on all four fronts. For example,
comparable store sales, a reflection of customers voting with their hard earned dollars, grew 
1.4 percent in 2002. We also recognize that in this challenging economic environment in which a
number of retailers experienced negative comparable store sales, we made strides in regaining
lost market share. 
We continued to make progress in better managing expenses. Selling, general and
administrative expense, as a percentage of sales, showed improvement for the second
consecutive year. This expense had grown considerably in recent years relative to our growth 
in sales. The progress with expenses over the last two years, while modest, moves us in the
right direction. Opportunities remain to become more efficient and we intend to act on them,
while remaining committed to enhancing the customer experiencein our stores, through 
our catalogs and online.
We opened eight full-line stores in 2002, the most weve ever opened in a single year, 
in addition to four Nordstrom Racks and one Faonnable boutique. We also successfully
implemented a company-wide perpetual inventory system. While we believe that no
implementation is completely seamless when it comes to installing a roughly $200 million
information technology system, we were able to accomplish this within the established budget
and without a significant misstep. Although our perpetual inventory system is a wonderful tool,
it is not the all-encompassing answer. With this technology in place, the responsibility now lies
with Nordstrom employees, not the system, to make sure that ultimately our customers benefit
from our stores carrying more of the merchandise they are looking for. Our technology will
continue to improve in 2003 as we begin implementation of a new point-of-sale system, 
which will enable our salespeople to be more efficient in sales transactions with customers. 
Different trends and formats have created a stir within retail over the last few years, 
from specialty stores in one period, to e-commerce in another, to discounters and increased
promotions more recently. Our challenge in 2003 and beyond is to be the best Nordstrom 
we can be, which we believe is an increasingly attractive niche. While we have a platform 
that is viable in multiple channelsfull-line stores, Racks, Faonnable boutiques, 
catalogs, Internetwe want to act as one company providing customers with a consistent
Nordstrom experience.
Thank you for your support of this company. We look forward to continuing to move in the right
direction in 2003 and were eager to demonstrate through our actions and results why you
should continue to be associated with Nordstrom. 
Sincerely,
Blake W. Nordstrom
PRESIDENT