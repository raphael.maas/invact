Letter to Shareholders

The last three years presented unique challenges
to all businesses, and CarMax was no exception.
However, we weathered the economic storm and
have now produced two back-to-back years of record
results. In fiscal 2011, we are proud to report that
total revenues increased 20% to $8.98 billion, while
net earnings improved 35% to $380.9 million, or
$1.67 per diluted share.
Our results demonstrate what can be accomplished
when we challenge ourselves to take a crisis and
turn it into an opportunity. It was during this time of
economic upheaval that we launched Building a Better
CarMax, which includes focused programs to support
three primary initiatives: develop our associates and
provide them the tools for success, drive execution by
improving the car-buying experience for customers and
discover efficiencies by reducing waste.
Associate training and development are often early
casualties in a business downturn. CarMax took
a different approach. We chose to dramatically
increase our efforts in this area, including introducing
Professional Selling Principles training and renewing
our focus on having experienced mentors to assist
with new associate training. We believe these investments
laid the foundation for our improvements in
sales conversion in the last two years.
This past year, we introduced a comprehensive upgrade
to MaxCare, our customized extended service plan. The
redesigned MaxCare allows customers to build their
own plan, with choices on deductibles, coverage and
cost. Response to the new product has been favorable,
with a resulting increase in service plan sales.
In December, we launched our carmax.com mobile
website, which was specifically designed to improve
the experience of customers accessing our site
from web-enabled phones. This application was well
received, and it already accounts for approximately
10% of our website visits.
Fiscal 2011 was a record year for our wholesale auctions,
where we topped $1 billion in sales on a 33%
increase in units sold. During the year, we rolled out a
new and dramatically improved carmaxauctions.com website,
providing new tools and functionality to our wholesale
customers, including a more robust search engine,
vehicle photos and customer-driven inventory alerts.
Also in fiscal 2011, we continued our efforts to
increase reconditioning process consistency and eliminate
waste, while still maintaining overall vehicle quality.
In recent years, we estimate that we have achieved
a cumulative sustainable reduction in reconditioning
costs of approximately $250 per vehicle as a result of

these efforts, while simultaneously realizing a steady
improvement in vehicle quality scores. This represents
roughly $100 million of savings that are available to
optimize our sales and profitability.
During the year, we resumed geographic growth, opening
three stores in Augusta, Cincinnati and Dayton, all
of which are new markets for CarMax. We are actively
working to fill the real estate pipeline and build the
management bench necessary to support future
store growth. Currently, we expect to open five stores
in fiscal 2012 and between eight and ten stores in
fiscal 2013. We remain confident that we are well
positioned for growth. Despite being by far the largest
retailer of used cars in the U.S., we estimate our
share of the late-model used vehicle market is less
than 3%. With just 103 used car superstores at year
end, we were in markets representing less than half
the U.S. population. We believe geographic expansion
and additional fill-in opportunities in existing markets
should fuel our growth for years to come.
We closed fiscal 2011 with a healthy balance sheet
and the liquidity to support our growth plans. During
the year, CarMax Auto Finance took several steps to
mitigate the risk of future dislocations in the credit
markets, including establishing a second warehouse
facility with a staggered renewal date and increasing
the overall warehouse capacity. In addition, we took
steps that we believe give our third-party finance providers
an even better appreciation of the advantages
of our unique finance origination channel.
Tom Folliard
President and Chief Executive Officer
April 27, 2011

While pleased with our recent achievements, we
believe that we can continue to improve every aspect
of our business. As we look to the future, we are
excited about initiatives underway to make more of
the car-buying process available to customers online.
We recently implemented CarMax EasyShop in two
test stores. This initiative gives customers a fast and
flexible online alternative for some of the most timeconsuming
aspects of buying a car. Using CarMax
EasyShop, customers can put a car on hold, set up an
appointment, order and pay for a transfer of a vehicle
from another CarMax store, apply for financing, select
a MaxCare plan and get a jump on much of the
paperwork. We look forward to continuing to find
more ways to make car-buying the way it should be.
In closing, I want to again thank our associates,
customers and shareholders for their continuing support.
I cannot give enough credit to all the associates
who play pivotal roles in our success as we continue
Building a Better CarMax and who are the foundation
of our long-term performance. Their unwavering commitment
to customer service allowed us to not just
survive the recession but to emerge an even stronger
company. I am proud to be part of this winning team.