TO OUR SHAREHOLDERS,
Our objectives in this annual report remain the same as in the
past: to examine our annual results, review the progress made in
2005 towards accomplishing our tactical and strategic goals, and
to share our views about our future. Additionally, 2005 was our
tenth year as a public company, so I will also give my perspective
on what we have accomplished during the past decade.

In 2005, Iron Mountain performed as we expected and as we projected,
albeit in most cases at the high end of our forecasted ranges. We met or
exceeded all of our targets for revenues, operating income, OIBDA and
internal growth. Revenues increased by $261 million, or 14%, and internal
growth was 8%, at the upper end of our guidance. Our storage revenue
internal growth rate, a key driver of our business, continued to rise
throughout the year reaching 10% in the fourth quarter and 9% for the
year overall. Capital spending modestly exceeded our forecasts, driven
primarily by the higher than expected growth of storage revenues,
as storage is the most capital intensive service we provide. Operating
income before depreciation and amortization (OIBDA) rose 13% to $574
million and EPS rose 17% to $0.84 per diluted share. OIBDA margins ran
ahead of plan at 27.6%.
Our core North American businesses, comprised primarily of our physical
records management and data protection businesses in the U.S. and
Canada, performed very well in 2005. They exceeded both their revenue
and contribution targets while working toward a merger of the two
operations effective January 1, 2006. Storage revenues grew 9% overall
and 8% internally, driven primarily by increasing storage internal growth
rates in the paper business. By the end of the year we saw the highest
storage internal growth rates since 2002, reflecting increased sales
production and a more positive pricing environment. Core services in
North America strengthened in 2005 with the data protection business
growing through the technology change cycle that impacted its results
in 2004. As important is the improvement in the core service growth
rate of our paper business, which grew more in line with storage in 2005.
Secure Shredding grew more than 20% internally as we continued to
build our footprint through 10 acquisitions.
2005 was also a good year for our international operations, highlighted
by expansion and consolidation, strong revenue growth in Latin
America, and solid operational performance in Europe. Total revenues in

our international segment grew 15%, primarily through acquisitions. In
addition to the Pickfords acquisition in Australia/New Zealand and the
shredding acquisition in the UK, we completed fold-in acquisitions in
both France and Spain. We also purchased significant minority partner
positions in Latin America, giving us ownership of more than 78% of
our operations in that region. Our market position in Europe remains
unequaled and we are building a strong management team focused on
improving sales performance and driving higher margins through scale
and increased efficiency.
Perhaps the most dramatic changes we saw in 2005 were in our Digital
business. Having integrated Connected with our existing Digital operations,
acquired LiveVault and, effective January 1, 2006, added our
Intellectual Property Management (IPM) business to the group, we
brought all of our Digital businesses together under a common umbrella
to give them the freedom and flexibility to focus on developing and
selling the products and services necessitated by this rapidly growing,
ever-changing market. Our Digital business was contribution positive for
the first time in 2005, several quarters ahead of expectation. Including
IPM, our Digital revenues grew 125% in 2005, with an internal growth
rate of more than 40%. We continue to be a major player in the digital
archiving space and have established clear leadership in the data protection
market for remote servers and desktop/laptop computers.

After going public in 1996 and raising capital to actively
consolidate a fragmented industry, we embarked upon
a long-term growth plan to build global leadership in
a new, emerging market space: information protection
and storage. This growth plan was embodied
in a three phase strategy that we have executed
with discipline ever since. The first phase involved
establishing leadership and broad market access in
both of our core businesses: records management
and data protection, primarily through acquisitions.
In the second phase we invested in building a worldclass
selling organization to aggressively sell new
customers, converting previously unvended demand.
We are now focused on the third phase, which we
term the capitalization phase. In this phase, which
will run for a long time to come, we seek to expand on
our existing customer relationships to help customers
solve their information protection and storage problems
and increase our share of their expenditures in
these areas. Doing this means expanding our product
lines to solve new, more complex customer problems
and propagating these new services across our global
footprint. In addition, we are focusing significant
energy and investments internally to standardize our
global services, wring costs from our processes and
provide better quality services and support to our
customers and employees.
Our Company is a portfolio of related businesses and
service lines that operate on four continents. As such,
different parts of our business are at different stages
in their evolution along the three phase strategy.
However, the enterprise as a whole has moved solidly
into the third phase and is focused on internal
growth from existing and new customers and driving
margin, and therefore, returns on invested capital.
That said, we will continue to use all of the tools we
used during the first two phases, such as acquisitions
and sales force expansion, to build a larger, more
profitable, global enterprise that remains the leader
in information protection and storage services.
During 2005, we accomplished much towards
advancing our strategy. Late in 2004, we acquired
Connected Corporation, a technology partner of our
Digital business. We have now successfully integrated
Connected and our former Digital operations
into a unified organization that performed above
our expectations. The success of this acquisition
was important for market and financial performance

reasons, but also because it was our first acquisition
in the technology space. We leveraged that experience
late in 2005 and acquired another technology
partner, LiveVault Corporation. LiveVault owned the
core technology central to one of our fastest growing
service lines, Server Electronic Vaulting. By acquiring
LiveVault we bought out a future royalty stream,
which we believe would have grown significantly,
gained control of this core technology and added to
our organizational strength and intellectual property
portfolio. We also advanced our agenda for introducing
this service in Europe.
We augmented our growing business in Secure
Shredding in the U.S. and Canada through internal
startups in Latin America and an acquisition in the
United Kingdom. Late in 2005, we acquired the
largest Secure Shredding operator in the UK, Secure
Destruction Limited, which gave us national market
coverage, plant capacity and a professional staff.
Following the same model we developed in North
America, we are integrating the shredding business
into our Records Management operations to leverage
customer relationships and overhead.

In December, we made our first
acquisition in the Asia Pacific
region with the acquisition of
Pickfords Records Management.
Pickfords gives us a scale business
with a strong management
team and market presence
in every major city in Australia
and New Zealand. We now have
the footprint to immediately
compete for national contracts
and to offer Records Management
services across both countries
to our global customers. Pickfords
has an emerging Secure Shredding
business which we will continue
to develop and we look forward
to introducing all of our information
protection and storage
services to this market in the fullness
of time.
Expanding our footprint into Asia
and the Pacific Rim is an integral
part of our strategy to be where
our customers need us to be. Not
only are many of our customers
asking us for help in this part of
the world, but it is also a large
and growing market in its own
right. We have done significant
work to inventory the various
business development opportunities
in this part of the world and
we expect to take advantage of
many of those opportunities over
the next several years. It is likely
that we will use a combination
of joint venture arrangements,
minority ownership positions,
and technology and marketing
agreements to develop our presence
in this region. While we will
also use acquisitions, they are not
likely to play as significant a role
as they did in the build-out of the
Americas and Europe. As such, we
should require less capital to build
out our Asia Pacific operations.
In 2005, we invested $464 million
of total capital in projects to maintain
current operations, support
internal growth, develop new products,
improve business operations,
acquire 16 new businesses and
complete three partner buyouts in
Latin America. The mix of capital
investments is shifting. From the
beginning of 1996 through 2000,
84% of the capital we invested in
the business went to acquisitions.
That has dropped to 49% over the
last five years. The future will likely
see a continuation of investment
trending away from acquisitions

and into growth and efficiency initiatives such as
new product development, processes and systems to
drive future margin increases and building a stronger
platform for a substantially larger business. In 2005,
we generated $120 million of free cash flow before
acquisitions, which was enough to fund two-thirds of
our acquisition spending for the year.
We remain a leveraged business model. The highly
predictable cash flows derived from our largely recurring
revenue model allow us to comfortably support
significant financial leverage. Our business requires
considerable capital investment in real estate, racking
systems and IT storage systems for increased capacity
to support the growth of both physical and digital
storage revenues, which represent 57% of our consolidated
revenue stream. We believe that prudent
financial leverage applied to attractive return assets
drives return on shareholder equity. That said, our
consolidated leverage ratio, as defined by our senior
credit facility, was 4.3 at year end, which is the lowest
level it has been at any time during our 10 years as a
public company. We have ample liquidity and access
to sufficient capital to fund our strategy.
During the year, we faced several challenges as we
and our customers reacted to new laws surrounding
privacy and the handling of personal information. As
the industry leader and the custodian of considerable
amounts of personal data, we have the opportunity
and the responsibility to provide leadership to
our customers who are facing significant challenges
meeting these new regulations. We developed new
technologies and expanded our service options
for customers to help them address these issues.
Internally, we upgraded our processes, enhanced our
employee training and implemented data encryption
technologies for our own data. These and other
trends in regulations continue to challenge us now
and in the future. However, they also create tremendous
opportunities for those that understand them
and who can provide services to relieve increased
customer pain. 2005 challenged us but we are now
much stronger and are focused on the opportunities
that lie ahead in this area.
We continued to build and evolve our organization
to develop management bench strength and add to
our skill set in preparation for managing the larger
enterprise we fully expect to become. During the
year, I asked Bob Brennan, previously President of
North America, to accept the role of President and
Chief Operating Officer of the Company. This allows
me to focus more time on developing the business
and new products, while Bob focuses on building the
global organization and aligning it to take advantage
of the opportunities ahead. During the year, we

surveyed our employees around the world to understand their views
of the Company, with which they are investing their career. We will
continue this process in the future as we recognize the value of our
Iron Mountaineers to our collective success. We also introduced to our
organization seven core values that we believe form a common foundation
of the behavior we expect from one another. As our partners, you
will be interested in these values, which you can find on page 11 of this
report. We are a stronger organization today than a year ago and we will
continue to build on that strength.
We continued our journey to build a world-class selling organization
supported by a service delivery organization driven by total customer
satisfaction. We are fortunate to operate in large, trend-favored markets
that afford a fair return on capital. We remain committed to our two
original principles that bear repeating: (1) invest our shareholders
money as if it were our own and (2) do what is right for our customers
and employees. As always, we will continue to invest your money
with the goal of increasing OIBDA in relation to total capital invested.
Nothing in our business changes rapidly. However, we have established
our strategic position, acquired dominant market share and broadened
our business to offer a unique value to our customers. The majority,
but not all, of the capital investment required to build our platform is
behind us. Our returns currently reflect the returns derived from our
acquisition phase. We are biased for growth, meaning that our focus

is on driving long-term sustainable revenue growth,
potentially at the expense of near-term margins.
Future investment will be dominated, although not
exclusively, by success-based investments in revenue
growth and efficiency projects. We generate significant
free cash flow and expect that to grow steadily.
We can see the time coming when our free cash flow
will exceed our ability to reinvest in our current businesses.
Our future strategy remains one of returning
to our shareholders any cash in excess of our ability
to invest at attractive returns in our current market
space. We are not driven for size alone. Simply put,
we strive to invest the maximum amount of capital
at acceptable returns on total capital and apply
prudent financial leverage to enhance return on
shareholder equity.
As I said at the beginning, 2005 was our tenth year of
operations as a public company. As I reflect on the last
decade I am amazed at what this organization, with
the support of our shareholders and for the benefit
of our customers, has accomplished. In 1995, before
our IPO on February 1, 1996, we generated revenues
of $104 million from operations in 26 U.S. cities, with
over 90% derived from the storage and servicing
of paper documents. 10 years later we reported
revenues of $2.1 billion, a 21-fold increase, 72% from
the U.S. and 28% international, with operations in
166 cities in 26 countries on four continents. The
storage and servicing of paper documents still dominates
our business at 75% of total revenues as it has
grown steadily during the last 10 years. However, we
have broadened our business from that single service
to three major service lines: records management
services, data protection and recovery services, and
information destruction services, all with multiple
products and vertical solutions that encompass physical
and digital service delivery models. This period
saw unprecedented acquisition activity averaging 11/4
acquisitions per month over 10 years, spanning four
continents, and driving a total revenue compounded
annual growth rate of 35%.
Even with this high velocity of change, the organization
accreted OIBDA margins more than 200 bps after
reinvesting approximately 800 bps of operational
improvements and overhead leverage into increased
technology spending to support an emerging Digital

business and our customer facing
organizationssales, marketing
and account managementto
support internal growth. In 1995,
we had internal growth of 8%;
in 2005 we had internal growth
of 8%, on a 21 times larger base!
The exact match of the beginning
and ending points is mostly
coincidence, but our ability to
maintain a healthy internal
growth rate that averaged 10%
over this period, while creating
such a scale change, was all about
great execution by our organization
and a strategy that required
the willingness and the ability
to make the investments necessary
to accomplish our goals.
Almost any company can create
growth through acquisitions and
we understand how to do that.
However, our plan has been to
create strong internal growth
over a long period of time and it
is working. I cannot promise the
next 10 years will be as fast paced
as the last 10 years. However, the
strategy is in place, our market
position is established and the
team that brought you the last
10 years is still here, augmented
by new talent to help us execute
even more crisply. I will let you
know 10 years from now what I
think of our performance.
Iron Mountain is fortunate to
have a strong and active board
of directors. As a group they
hold a significant financial stake
in our company. The majority of
these holdings were created by
investing their own cash, not by
accrual of value from options
granted to them by other shareholders.
They meet the standards
and actions required for independence.
We believe those characteristics,
along with experience,
good judgment, integrity and the
willingness to share their opinion
are the keys to good governance.
We are also fortunate to have
nearly 16,000 dedicated
Mountaineers worldwide. The
knowledge and loyalty, developed
through longevity, of our
employees is a significant asset
of Iron Mountain. We appreciate
their service and we will
continue to work to reward them
for their performance. One sign
of their dedication was evidenced
by how our team responded to
our customers and employees
needs during Hurricane Katrina
in New Orleans. We rallied our
disaster recovery plans and
protected all of our customers
assets, took on new customer
challenges during the aftermath
of the storm and found, protected,
and supported our displaced
employees and their families with
a relief fund from employees and
the Company that totaled over $1
million of contributions. Another
reminder to me of the dedication
of our Mountaineers!
We continue to attract high
quality shareholders as our partners.
We appreciate your advice
and input. We will continue to
communicate with you in an
open and transparent manner as
we believe that you are our partners.
As such, I want to remind
you of an investment opportunity
for our partners that we created

a few years ago. I believe that individual education
is the long term way to improve our world.
To help our employees advance their families, we
have established a charitable foundation, the Iron
Mountain Scholarship Foundation. The Foundation
was originally funded with commitments from several
members of our board of directors and management
team. The Foundation provides tuition assistance
for children of our employees to attend college. It is
intended to assist those that need extra help to push
them over the line. Awarding of the scholarships is
managed by an independent, professional organization
and awardees are limited to employees that earn
below $60,000 per year. For those partners that have
earned exceptional returns from their Iron Mountain
investment, I ask that they consider joining us in
supporting our employees family development. If
you have interest in contributing, please contact me
at scholarship@ironmountain.com. I thank everyone
who has generously supported the Foundations work
thus far.
I have been fortunate to lead this exceptional company
for nearly 25 years. I have seen it grow from a small,
local company to a global leader. Its been a lot of fun,
but from my seat, the fun is just beginning. Thank you
for allowing me the privilege of leading your company
for the last 10 years.
With continued pride and enthusiasm,
C. Richard Reese
Chairman & Chief Executive Officer
April 3, 2006