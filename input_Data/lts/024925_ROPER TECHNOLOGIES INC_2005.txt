Ropers unwavering focus on markets
and customers contributed importantly
to our record financial performance in
2005. Net sales grew 50% to $1.45 billion,
operating cash flow grew 71% to
$281 million and earnings grew 63% to
$153 million. Our shareholders were
rewarded with significant appreciation
in our share price during the year.
We also raised our cash dividend for
the thirteenth straight year, and we
completed a 100% stock dividend.
For the Company and its shareholders,
2005 was a very good year.
Our performance reflects the successful
execution of all elements of our
growth strategy. Record sales resulted
from solid internal growth and the successful
integration of recent acquisitions.
Our success was demonstrated
by improvement in operating margins.
Our relentless emphasis on cash returns
enabled us to convert 184% of our
record net earnings into record operating
cash flow of $281 million.
These achievements reflect a management
philosophy based on simple
ideas that yield powerful results. Its a
philosophy that starts with positioning
the Company in advantageous markets,
identifying the best growth opportunities
within those markets and capturing
market share by creating valuable
customer solutions.
Market-Driven at Every Level
Careful selection of end-markets is
critical to our goal of producing sustainable
growth. We strive to avoid markets
with cyclical characteristics in favor of
those offering steady expansion and
multiple growth paths. Over the past
few years, our investment strategy has
positioned the Company in attractive
markets such as water, radio frequency,
energy and research/medical  all
notable for strong demand drivers and
profitable opportunities.
Our performance within our chosen
markets is enhanced by tools we use to
identify the most effective means to
grow. In some cases, this leads to investments
in developing new products or
capturing an advantage in technology
or application expertise. We also work
tirelessly to improve our channel access
to customers, extending our distribution
to enhance global reach and increasing
and improving our access to end-users
in order to get closer to our customers.
We invest in sales training to improve
our capabilities in serving customers.
Our progress on each of these initiatives
contributed to internal growth in 2005.
By getting closer to our customers,
we can develop application solutions
that deliver superior value by meeting
their critical needs. Our water metering
and RF tolling solutions, for example,
act as the cash register for our customers,
enabling them to generate the
revenues they need for operations or
infrastructure investments. Our energy
systems and instruments improve customer
process productivity and provide
necessary information for improving
quality and meeting regulatory requirements.
Our research tools facilitate
advanced research in leading laboratories
around the world, while our medical
products improve patient outcomes and
enhance provider productivity. Customer
solutions such as these demonstrate the
multidimensional nature of our marketbased
strategy.
Creating Growth through Acquisition
Our acquisitions have created considerable
value for our shareholders and
represent a core competency at Roper.
We have developed a rigorous acquisition
methodology steeped in diligence,
analysis and discipline that enables us
to recognize a good fit. It also enables
us to know when to walk away. Every
year we evaluate billions of dollars of
potential transactions, selecting only
those consistent with our growth and
cash generation requirements.
Our acquisitions are driven primarily
by growth potential, not simply cost
reduction. We focus on the market
potential of a business. We value
management continuity. We bring our
proven governance processes to drive
operational and integration successes.
Simply put, we buy a business in order
to grow it  a message that most
quality acquisition candidates find
immensely appealing.
Acquisitions have proven to be an
excellent means to expand into desirable
end-markets. The acquisitions of
Neptune in 2003 and TransCore in 2004
created substantial positions for us in
the large and growing water and radiofrequency
markets. Both businesses have
become platforms for further growth via
bolt-on acquisitions, such as Inovonics,
a provider of wireless technology used
primarily in security applications, which
we added to our RF segment in 2005.
The integration of this new segment
went well in 2005, with operating margins
expanding throughout the year
from 12.1% in the first quarter to 17.6%
in the fourth quarter.
During 2005, we also acquired CIVCO
and MEDTEC to establish Ropers first
platform in medical markets. Together
MEDTEC and CIVCO provide a wide
range of devices and consumables for
diagnostic and therapeutic imaging
applications, creating excellent future
growth potential. Not only is the medical
market stable and growing, but
these businesses also generate strong
recurring revenue and high margins,
with relatively low working capital
requirements. In other words, they fit
our criteria well.
New Segment Operating Structure
Sharpens Market Focus
In 2003, we created several operating
segments on the basis of common
competencies that relate to markets,
products and infrastructure in order to
further improve our performance. Our
businesses have benefited from this
structure in the form of reduced costs,
expanded product development and
enhanced customer access.
Having achieved many of the synergies
we initially envisioned, we now
plan to refine our operational structure
further in 2006 to reflect our strengthened
focus on markets. The net result
will be market-focused segments. This
realignment will result in even greater
collaboration among our businesses in
developing customer solutions and
improving distribution. The new structure
will unleash other opportunities.
For example, we will be consolidating
most of our Energy market businesses
into a new facility in 2006, giving us the
ability to increase our capabilities and
reduce noncore costs. An operational
structure even more closely aligned with
markets should further drive our growth.
Financial Discipline Yields
Strong Results
Details in our 2005 performance underscore
the improved operating focus that
we have pursued. Ropers cash conversion
story is particularly compelling. In
2005, EBITDA reached $335 million, a
64% increase over 2004 and more than
triple the $95 million recorded in 2003.
EBITDA margins expanded 210 basis
points year-over-year to 23.1%.
Another aspect of our cash story is
the dramatic improvement in working
capital velocity. At year-end, net working
capital dropped to an all-time low of
13.8% of annualized fourth quarter sales.
This level is 620 basis points lower than
the same metric in 2003. Even with the
substantial growth realized in 2005,
inventory reductions contributed nearly
$10 million to our operating cash flow.
We continue to make solid progress
toward our goal of achieving an investment
grade rating. Even after investing
$330 million in acquisitions in 2005, we
finished the year with our revolver largely
undrawn, a modest debt-to-EBITDA ratio
of 2.7 and interest coverage at nearly
8 times. In conjunction with our growing
cash flow, we have ample capacity to
fund investment and acquisitions.
Count on More in 2006
As we start 2006, we are building
on our strong momentum. We are
continuing to expand our market leadership
through market share gains in
water and technology rollouts in RF.
We are excited about our new medical
growth platform, while our energyrelated
businesses continue to capitalize
on growing global demand.
We are well positioned to execute
our growth strategy. The Company has
successfully maintained the nimbleness
of its small-cap past, while enjoying
the scale benefits of its mid-cap size
today. Our market-driven strategy complements
our focus on cash generation
to create shareholder value. We have
shown how bringing simple ideas to our
businesses empowers them to create
powerful results. For all these reasons,
and the contributions from our dedicated
employees, we will continue to
create value for our shareholders.

Sincerely,
Brian D. Jellison
Chairman, President and
Chief Executive Officer