Dear Fellow Shareholders,
We entered fiscal 2010 with more uncertainty than we have seen in the
last several years, including the political and economic environment
in the United States, the new administration�s healthcare reform
proposals and our SYSTEM 1� Sterile Processing System
regulatory matters. As we close the year, we have greater clarity
on many fronts, and are pleased with our ability to perform well
during this challenging year. All in all, we have much to be
pleased with as we look back on fiscal 2010.
From a sales perspective, we ended fiscal 2010 with
total revenues down 3% and strong capital equipment
backlog. As we think about the highlights of the year,
one was clearly the performance of our consumables
across the business. Healthcare consumables grew 7%
for the year, driven by growth in new products, such as
Prolystica� Ultra Concentrate Cleaning Chemistries and
Class 6 indicators, as well as the benefit of increased
hand hygiene usage due to the H1N1 pandemic. On
the Life Sciences side, consumables grew 13%, again
impacted by H1N1, as well as volume increases and
some price recaptures during the year.
While it was a more challenging year for capital
equipment, we did have pockets of strength,
particularly for newer product lines like Vision� Washers,
V-PRO� 1 sterilizers, surgical tables and LED lights.
Shifting to profitability, we generated even more
efficiencies than we anticipated which led to a 270
basis point improvement in operating margins. We are
particularly pleased with the progress being made outside
of North America, as we achieved profitability (excluding
restructuring charges) in all major geographic regions during
fiscal 2010. On the bottom line, our earnings per diluted share
were a record $2.16, or 16% better than last year. These results
were achieved even though we saw a decline of approximately
$20 million in revenue for the year in SYSTEM 1 capital equipment,
consumables and service.

Fiscal 2010 was also a record year for free cash flow, which we achieved through improvements in every facet of
the business: earnings, capital spending and working capital. Our balance sheet remains strong, ending the year
in a net cash position and with debt-to-total capital just under 22%.
Moving Forward
As you know, just after the end of the fiscal year, we reached an agreement with the FDA on the Transition Plan for
our SYSTEM 1 Customers and received clearance on our SYSTEM 1E� Liquid Chemical Sterilant Processing System.
We believe SYSTEM 1E is the most compatible single product on the market to replace SYSTEM 1. It is indicated for
liquid chemical sterilization of cleaned, immersible, and reusable critical and semi-critical heat sensitive medical devices,
which is the most common application for SYSTEM 1. While it has only been a short time since SYSTEM 1E was cleared
and the Transition Plan (including the Rebate Program) was announced, the initial feedback we are getting from
Customers has been encouraging.
As we think about fiscal 2011, our expectations are clearly being impacted by the transition away from SYSTEM 1.
However, we are not a one-product Company. We have worked diligently to develop products and services across
our broad offering to meet the needs of the perioperative environment and healthcare providers in general, whether
they are hospitals, surgery centers, drug or medical device companies or others. Our people are focused on exactly
what they should be: our Customers. We are working every day to create more solutions to meet their most pressing
needs and continue to earn their trust and respect.
We believe we have the freshest product line in our history, and we intend to continue to innovate. We will also
improve our efficiencies as we strive to become a Lean organization. And, we will remain disciplined in not allowing the
inefficiencies we have eliminated to creep back into our Company as volume improves.
These past few months have been extraordinary for everyone at STERIS, but the Customer focus and commitment of
our people has been truly first class. I appreciate all their hard work and dedication and the results they have achieved.
I want to thank our entire Board of Directors for their continued counsel, support and leadership. A special recognition
to Robert Fields, who resigned from the Board in March, for his service over the last two years. We wish you the best in
your endeavors.
On behalf of everyone at STERIS, we thank you, our fellow shareholders, for your ongoing support.
Walter M Rosebrough, Jr.
President and Chief Executive Officer
June 2010