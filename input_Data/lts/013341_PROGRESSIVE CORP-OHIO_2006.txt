Letter to Shareholders
An Auto Insurance Commercial Every Fourteen Seconds!
This was an interesting factoid gleaned recently during the course of evaluating advertising agencies.
Without validating the claim, directionally it captures
one of the most observable macro changes in our industry over the past few years. In 2006, few consumers
escaped an auto insurers message as just about every
conceivable impression space has been used to capture consumers interest. Why now? Whats changed? 
Three years ago, I wrote that 2003 would be the second year of 
underwriting profitability in the auto insurance industry for the past
25. Since then, the industry has gone on to four consecutive years 
of underwriting profitability. Based on where we start 2007, the odds
look good for a five-peat (I hope thats not trademarked!). With 
favorable underwriting results and a stable outlook, the competition
for consumers has increased dramatically. With consumers in 
demand, price pressure was sure to follow. While simple characterizations about pricing dont capture individual market and segment
level detail, a second notable macro change is that the industrywide earned premium for 2006 may well be lower than 2005, a first
in at least 25 years.
Under these market conditions 2006 was a good, not great, year for
Progressive. Our calendar-year underwriting profit margin remained 
exceptionally strong at 13.3%, still far outstripping our long-term 
target of 4%. Combined with investment returns for the year, net 
income was up 18% over last year to $1.65 billion, or up 21% to $2.10
per share. Our return on shareholders equity was 25.3%, consistent
with our five-year average. Less exciting was the slow growth in 
premiums, which on a written and earned basis grew about 1% and
3%, respectively. Our growth in policies, my preferred measure, tells
much the same story, with our Personal Lines book increasing 3%
and Commercial Auto about 7%. Ive said many times that our 
culture thrives on profitable growth, and while understandable 
given market conditions, these growth numbers and trends do not
meet our expectations.
The well-documented reduction in claims
frequency over what is now several years is
clearly the underlying driver of industrywide 
favorable results and limited upward pressure on premiums. At this time last year, I 
reported that our view continued to be that
we would slowly return to more normal operating margins by allowing expected increases
in severity, and potentially frequency, to absorb the margin in excess of our target rather
than immediately price it away. That forecast, at least in the months that followed,
proved to be incorrect. Since no significant
change in frequency or notable acceleration
in severity appeared to emerge, our view of
the future and our game plan needed reevaluation. At the annual investors meeting, 
I announced that our efforts to fully explain
and calibrate frequency reductions, while interesting, had been inconclusive. Obvious
contributors of decreased frequency include
vehicle safety, road design, driver education,
more vehicles per household, and gas price
fluctuations, but the amalgamation of these
factors does not lend itself easily to a reliable
forecast. What is more apparent to us now
is that these drivers were, in many cases,
structural changes and distinguishable from
the observable insurance cycle. The prospect
for future advances, especially in vehicle
safety, suggest that even further reductions
in frequency and injury severity are plausible, though difficult to forecast. 
We also assessed that failing to respond to
stepped-up advertising efforts and the increased potential for our customers to search
for lower prices in the marketplace when we
could profitably meet or beat competitive 
offers, was no longer the correct strategy.
While rates for a majority of consumers were
relatively stable, marketing messaging was
at an all-time high. Unlike other industries,
our marketing efforts are unlikely to create
increased overall demand, rather the expected
outcome is more about moving around the
available pool of consumers. Conscious that
not all price reductions result in good tradeoffs, we challenged ourselves to assess our
market pricing relative to our goal of a 96
combined ratio and to make trade-offs that
were acceptable and smart. Accordingly, we
placed increased emphasis on competitive
pricing for our current customers to ensure
their likelihood of staying with us. 
This reassessment of frequency and severity trends in our business means we will
play-it-as-we-see-it and, more importantly,
we are prepared to react quickly when we see
a change. Our personal auto policy periods
are short, providing greater flexibility to price
correctly. Our controls and analytic review of
profitability by sub-segments of our book are
robust. We believe our ability to recognize
trends is better than our ability to predict
them, and our product management, technology and operational groups are all capable
of reacting quickly. We are at our best when
the market conditions require nimbleness.
Each of our product lines has acted on the 
reassessed game plan and a review is provided in the Operations Summary section
provided later in this report. 
Long before 2006 started, we recognized that growth, and the attendant issues in managing growth, would not be the primary focus for
the year but rather we would give maximum effort to key initiatives
we believed would shape the company for the future and, in some
cases, provide a distinct competitive advantage. We concentrated
on claims, developing a marketing culture and focusing on long-term
customer satisfaction that will lead to increased retention.
Claims Our emphasis on quality claims handling and superior 
customer experiences continues to meet our expectations, and 
I never tire of reporting that year-after-year our assessments on 
both measures exceed prior highs. Going forward, our emphasis 
will be to ensure quality and service levels are maintained in balance with a cost structure that contributes to, and strengthens, our 
market competitiveness. 
Our biggest strategic contribution to facilitating this balance took
a huge step forward in 2006 with the opening of 29 new concierge
claims centers. We now operate 53 such claims centers nationally 
in 41 metropolitan markets. Long-term followers of Progressive 
know that this initiative has gone from concept, to market test, to a
20 operation burn-in period to, now, full-scale deployment. Each
phase tested our thesis that this concept is capable of creating a 
really meaningful and valued change in the consumer experience,
while reducing the frictional costs associated with claims handling,
all while leveraging our scale to economic and competitive advantage.
I could not be more pleased that we have now reached a market penetration where we can declare and advertise that this is our primary
approach to damage assessment and facilitation of repair in a growing number of markets. We will incorporate this distinctive offering
as part of our ongoing advertising and brand communication.
Marketing Culture I have said that as a business we are good at three
things that really matter: allocating costs between consumers in ways
that best match their expected costs; managing the claims and administrative costs that must ultimately be allocated; and providing
superior consumer experiences. In 2006, we added the need to become equally good at marketing as we are in other skill areas to the
things-that-really-matter list. We have good reason to be pleased
with how the Progressive brand has developed in the last decade or
so, but as with all things that show opportunity for improvement, we
took stock of the relevance of our message and actions in todays
marketplace. In doing so, we saw opportunities to advance in many
areas ranging from market intelligence gathering, to consumer feedback, to consistency of brand communication, to retailing of product
features, to a more effective use of our skills and talents. In the latter
part of 2006, we dedicated resources to take a pragmatic look at all
marketing and brand activities, and to confirm or amend as necessary
the foundation of our brand and consumer promise. A few highlights
are important to share. 
Perhaps the most significant finding is the powerful association
consumers and agents have with the Progressive name. Our 2004
introduction of our Agency Business brand, Drive Insurance from
Progressive had, for some, de-emphasized Progressive and created
unintended separation from our claims service. In response, in 
February 2007, we announced that we repositioned the Progressive
name in the name of all products we sell, allowing agents to more 
effectively leverage the power of the Progressive name as our single
brand name. Progressive Drive Insurance is the name of the private
passenger auto product sold through independent agents. This 
preserves the agent-specific product identity for our private passenger auto product that we desire, while increasing the emphasis on
the Progressive name. Progressive Direct is the name used for the 
private passenger auto product sold directly by the company online
and by phone. Progressive Commercial, Progressive Motorcycle,
Progressive RV, Progressive Boat, etc. are the names of our non-private
passenger auto products sold by agents and directly. A change of this
nature was not something we expected so soon after market introduction of the Drive name, but with an increasing focus on our
concierge claims service and other aspects of the Progressive brand,
we wanted all customers to associate with the Progressive name and
have our agents offer to their customers one more strong reason to
buy Progressive.
The images in this report are those of real customers, reinforcing
that a marketing culture for us starts with respect for the customer
and an increased appreciation of the relevance of our product and
what works and what doesnt. 
Its about you. And its about time.
SM is our new advertising tagline,
but its much more than that; its a positioning we will challenge 
ourselves to live up to.
Our work around reaffirming and challenging our brand promise
to consumers includes ensuring that all our internal pricing and customer experience actions are consistent with the brand. Predictably,
we have opportunities to improve. Pleasantly, we confirmed that we
offer distinctive benefits that are truly valued by consumers, and
greater opportunity to ensure we present our full story. With the 
assistance of new advertising and creative resources, we expect to
advance quickly on closing identified gaps.
Retention I have reported on retention activities and their importance
for some time, and we have had some successes at improving internal processes and eliminating what last year I called friendly fire.
That said, we have yet to make substantial progress in moving our
retention measures and now realize additional root cause issues exist.
To quantify how we are doing on customer focus and retention 
efforts, we rolled out a companywide deployment of a Net Promoter
Score in 2006. Underlying this concept is our belief that the strength
of response to a single question, How likely is it that you would 
recommend insurance from Progressive to a friend or colleague?, can
provide substantial amounts of information about those consumers
who identify themselves as promoters, detractors or simply indifferent. A referral, or willingness to refer, speaks volumes about an 
individuals attitude toward the company. When further sliced and
diced to every possible consumer grouping, and correlated with the
multitude of customer experiences we provide, this knowledge
trumps the array of inconsistent and often annoying consumer 
surveys that preceded it. Moreover, it has created a companywide
customer measure that has quickly taken on the importance of other
key cultural imperatives. We no longer debate the academic pros and
cons of internal and external survey data. Rather, we are committed
to maximizing the benefits from this singular approach. From what
we have seen so far, this is more than an interesting cosmetic measure; this is something that contributes to a marketing culture.
It is not an unfair characterization to suggest that Progressives
culture and business practices, in part, reflect our beginnings 
and history as an acquirer of new customers with relatively short
tenure. Transforming our focus to build on that acquisition culture
and more effectively blend it with the necessary changes to retain
customers for more of their insurable lives is proving to be more 
than just a subtle change. By our best estimates, Progressive has 
acquired more new customers per year than any auto insurer in 
recent years. Marketing to our existing customers, done well by 
many of our competitors, is an area of exponential internal attention, which in combination with our acquisition skill, provides for very 
exciting growth potential. 
We believe we get many benefits by being a pure-play in auto insurance and have no plans to deviate from that course, but we are
also aware of consumer needs for other products and the retention
differences that come as a result of having multiple relationships with
the same customer. During the year, we introduced a Progressiveunderwritten Personal Umbrella product in our Agency Business and
entered into a joint marketing relationship with Homesite Insurance
Group for Agency and Direct Business customers interested in a
homeowners policy. The economic contribution of these ventures to
date is far too small for comment, but both represent additional 
efforts to strengthen our ability to meet long-term customer needs.
Our Gainshare measure, highlighted last year as our way to calibrate the business gain made in any calendar year, for 2007 will reflect
our internal emphasis on retention and the customer. The general 
construct remains the same, but two important tuning changes have
been made. Growth, and our stated goal to profitably grow as fast as
possible, will be calibrated for Gainshare in terms of number of policies rather than in just dollar terms a simple change intended to
reinforce the importance of every customer interaction. We will also
begin to disaggregate new and renewal policy populations and focus
on the growth of each, with a weighting between the two reflective
of the importance of the cultural shift toward increased retention of
current customers. The loss of a current customer replaced with a
new customer, while a numerical offset, is not a brand or economic
offset we are happy with. 
In about the last 20-some years, Progressive has doubled in size
five times each doubling more challenging than the last. During
2006, we blueprinted the plan for our next doubling incorporating
many of the issues touched on above. While we are cognizant of our
size, market conditions and the strength of competition, we are 
confident a Progressive of twice our current size can be a very real
outcome with quality execution on our challenges.
INVESTMENT AND CAPITAL MANAGEMENT
Our investment portfolio made a greater 
contribution to our results this year with a
total return of 7.4%, up considerably from the
4.0% produced in 2005. Investment income
was up a healthy 21% for the year. Our common stock portfolio generated a return of
16.3%, largely tracking the general market.
We continued to maintain a high-quality, relatively short-duration fixed-income portfolio.
As the market price for risk declined during
the year, we further reduced our exposure,
ending the year with a weighted average
credit quality of AA+, up from AA at the end
of 2005. Fixed-income returns were strong
on an absolute basis at 5.9% and very strong
for the level of risk assumed.
Early in the year, shareholders approved
an authorization to increase the number of
shares outstanding and the Board of Directors subsequently approved a 4:1 stock split,
which was effective in May. Based on our
long-standing and continuing position on
capital management  to repurchase shares
when our capital balances, view of the future
and the stocks price make it attractive we
repurchased 39.1 million shares during the
year, or a little over 6% of the outstanding
balance at the start of the year. The average
repurchase price was just under $25, on a
split-adjusted basis.
We ended the year in a very strong capital position, with no constraints on any 
business opportunities, and a debt-to-total
capital ratio below 15%. Our capital strategy
preference is to maximize operating leverage (i.e.,ratio of net premiums written 
to statutory surplus), while maintaining 
relatively low financial leverage, and we 
continue to expend considerable effort to
assess capital needs under a variety of 
operating and external contingencies. We
believe we have opportunities to extend 
our operating leverage and will continue to
manage our capital to that objective.
As part of our capital planning process, we
announced last year that we would introduce
a variable annual dividend based on our
Gainshare factor. During 2006, we published
the year-to-date Gainshare factor in our
monthly reporting to provide shareholders
some familiarity with the measure and its
relative volatility. We closed the year with a
factor of 1.18.
The Board of Directors has established
that the 2007 variable dividend will be based
on 20% of after-tax underwriting profit 
multiplied by the companywide Gainshare
factor for 2007 and paid in early February
2008. Based on similar parameters and the
1.18 factor of 2006, if the dividend policy had
been in effect for the year, the dividend would
have been about $.39 per share. We will continue to publish the year-to-date Gainshare
factor and full details of underwriting performance rather than provide any guidance
on dividend expectations.
Forward-looking statements and earnings guidance are not something anyone would associate with Progressive and we think for good 
reason. However, this letter to all shareholders serves as a good forum
to tell our story as we see it the results, the opportunities and, most
importantly, the organizations sense of optimism for the future. 
We are continuously motivated by our aspiration of becoming 
Consumers #1 Choice for Auto Insurance throughout their insurable
lives. While our growth in 2006 did not match our aspirations, the
positive introspection that has resulted at all levels is exactly the 
attitude that is expected. Our Core Value of Excellence continuously
doing better than we have done before is both demanding and a
challenge that Progressive people love to embrace. 
We tend to celebrate our successes quickly and then move on to
our opportunities. As summarized in this report, we have several 
opportunity areas with tremendous upside potential. We enter 2007
with some exciting changes, an emphasis on capitalizing on claims
strategies that have been developed over the past several years, a 
renewed focus on marketing and brand development, and an 
increased commitment to achieve meaningful progress toward the
cultural shift necessary to make longer tenure for our customers 
possible and desirable. 
Everything we have achieved is a result of the efforts of the nearly
28,000 Progressive people whom we sincerely thank for our business results and for continuing to make Progressive an environment
where people enjoy working hard and are motivated to do their best
work as well as an environment others want to join. 
Equally important is our appreciation for the customers we are
privileged to serve, the agents and brokers who choose to represent
us and shareholders who support what we are doing.
Glenn M. Renwick
President and Chief Executive Officer