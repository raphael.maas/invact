IF ANY YEAR PROVES THE VALUE
OF WHAT ECOLAB OFFERS, 2014 DID.


                                        A LETTER FROM ECOLAB'S CHAIRMAN AND CHIEF EXECUTIVE OFFICER



                                                                      Brazil and much of Europe. We succeeded by staying focused
                                                                      on serving our customers. Our model links our technology and
                                                                      service expertise with training and information to solve problems
                                                                      in a comprehensive and cost effective way. Our ongoing ability to
                                                                      deliver the best results at the lowest total cost to the customer,
                                                                      and reduce their labor, energy, waste and water use, continues to
                                                                      make us a valuable partner.

                                                                      We had very good performance across the metrics that matter
                                                                      the most to us. We had strong sales, margin improvement, cost
                                                                      efficiencies, capital returns and earnings growth, as well as
                                                                      very good results in safety, business integration, innovation,
                                                                      infrastructure efficiencies and talent development.



If any year proves the value of what Ecolab offers, 2014 did. Our
                                                                         OUR ONGOING ABILITY TO DELIVER THE BEST
ability to help ensure clean water, safe food, abundant energy and
healthy environments has only become more relevant in a world
                                                                         RESULTS AT THE LOWEST TOTAL COST TO THE
where concerns about water and energy availability, environmental        CUSTOMER, AND REDUCE THEIR LABOR, ENERGY,
sustainability and public health are increasing.                         WASTE AND WATER USE, CONTINUES TO MAKE
Once again, our team made the difference. They were a powerful           US A VALUABLE PARTNER.
and positive force behind the scenes in every industry we serve,
in every region in the world, partnering with our customers to
solve their most pressing challenges. Through the dedication of all   DOING MORE FOR OUR CUSTOMERS
Ecolab associates, our company achieved outstanding results in        Throughout our company, we increasingly operated as One Ecolab,
2014. We won significant new business, had a record year for new      leveraging the talent, insights, capabilities and innovation within
product launches and accelerated sales. As a result, we leave 2014    our businesses to do more for our customers. When we acquired
with solid momentum.                                                  Nalco, we gained 3D TRASARTM, innovative technology that
                                                                      continuously monitors to detect water quality issues, enabling
STRONG PERFORMANCE IN 2014                                            our customers to use water more efficiently and dramatically
Net sales of $14.3 billion were up 8 percent and adjusted earnings    reduce their water footprints. Last year, we combined 3D TRASAR
per share rose 18 percent to $4.18 (reported diluted earnings per     with our clean-in-place expertise to improve our offering to
share were up 24 percent to $3.93). Excluding acquisitions, sales     food and beverage producers. This enables better cleaning and
grew by 5 percent.                                                    increased production capacity while reducing water and energy
We achieved these results while managing in an uneven world,          consumption. We also added solid chemistry to 3D TRASAR
moving ahead through a seemingly record number of major               for increased safety and sustainability benefits in our
geopolitical events and softness in key markets such as China,        institutional markets.




4   ECOLAB ANNUAL REPORT 2014
                                                                                                                ADJUSTED
                                                               NET SALES             UP                     DILUTED EARNINGS UP
                                                                                     8%                                      18%
                                                              $14.3                                             PER SHARE

                                                               BILLION                                          $4.18


Doing more with less has become essential in our times. Nowhere       The strong dollar presents a challenge because it will negatively
is that more true than with water. Fresh water is increasingly        impact translation of our non-U.S. business results. Since we
scarce and projections show demand significantly outstripping         principally produce where we sell, it does not create a competitive
supply in the years to come. Our core strength in helping             challenge. However, it will likely have a real impact as we translate
our customers reduce, reuse and recycle water is becoming             profits, and we are working to offset that through a continued focus
increasingly important. We are working hard to reduce the amount      on cost effectiveness everywhere we compete. We are confident
of water used in every aspect of our customers' operations (as well   we will manage the year effectively and achieve another year of
as our own) and we see strong interest in water-saving solutions      superior growth.
across the board. Water stewardship continues to be a guiding
                                                                      Importantly, we will accomplish this while continuing to invest in the
principle in our research and development work.
                                                                      foundations for future growth, including improving the information
Because of the investments we've made in accelerating our             systems that support our business teams, developing more robust
business, strengthening our capabilities and increasing our           innovation pipelines, implementing a more efficient and effective
efficiencies, we enter 2015 in a good position, with significant      supply chain, developing our talent and continuing to build a safety
opportunity in every market we serve. Our business is balanced        culture. We continue to take the long view as we chart our course.
by geography, by market and by customer segment. This business
                                                                      Ecolab has a great track record of performance. That track record
balance will be especially important as we move into 2015, which
                                                                      comes from remembering that our job is to manage both for today
like all years, presents its own set of challenges. This year, the
                                                                      and for the future. Our ability to provide our customers with the
primary external factors that will impact our company are the
                                                                      right technology and the right service while also enhancing our
sharp decline in oil prices and the sharp increase in the value of
                                                                      capabilities for the future may not seem like a complicated formula,
the U.S. dollar.
                                                                      but it is a successful one. And it is one we plan to continue to follow,
                                                                      because we believe it is the best way of ensuring we remain a
                                                                      company that our customers, our team, our shareholders and our
   WE ENTER 2015 IN A GOOD POSITION, WITH                             communities can count on, today and in the years to come.
   SIGNIFICANT OPPORTUNITY IN EVERY MARKET
   WE SERVE.
                                                                      Sincerely,


PREPARED FOR 2015 CHALLENGES
Lower oil prices have both positive and negative effects for us,
and the ultimate impact on our business will be manageable.
On the downside, we expect slower growth in our Global Energy
segment. On the upside, many of the raw materials we buy will be      Douglas M. Baker, Jr.
less expensive, which will reduce our product, transportation and     Chairman and Chief Executive Officer
distribution costs. Consumers will have more money to spend, and
a healthier consumer economy creates more demand for many
of our products and services, particularly in the foodservice and
hospitality sectors.



                                                                                                                   ECOLAB ANNUAL REPORT 2014   5
