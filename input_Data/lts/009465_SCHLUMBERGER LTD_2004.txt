The year 2004 set new records for Schlumberger Oilfield Services and for WesternGeco as sustained high
prices for oil and gas gave our customers the confidence to increase their investment in new exploration and
production programs. The year also marked our transition to becoming a pure oilfield services business as we
completed the divesture of our non-core assets.
Oilfield Services reached new revenue and pre-tax operating income records of $10.24 billion and $1.8 billion
respectively. Activity increased in almost all areas of the world and for all technologies with the highest
geographical growth rates being recorded in the Canada; India; Mexico; Brunei, Malaysia and the Philippines;
and the Eastern Mediterranean and East Africa GeoMarket* regions. Russia also experienced strong growth
despite the absence of any activity for Yuganskneftegas in the last quarter of the year.
Demand for technology was robust everywhere but particular strength was seen for the Drilling &
Measurements PowerDrive* family of rotary-steerable tools and the Wireline ABC* Analysis Behind Casing
and PressureXpress* services. Well Services introduced a number of new technologies that included the
PowerCLEAN* system, an integrated solution for optimizing production through well bore fill removal using
coiled-tubing. Growth in production services technologies from the Well Completions & Productivity segment
was strong, and increasing acceptance of the Schlumberger Information Solutions Petrel suite of seismicto-
simulation products was marked by impressive revenue growth during the year. Integrated Project
Management activity continued to grow strongly, with operations beginning on significant new projects in
Colombia, Romania and Asia.
WesternGeco made excellent progress and ended the year with a pre-tax profit of $124 million that reversed
the $20 million loss of 2003. Increased overall activity and growing market uptake of Q-Technology* coupled
with hard work on reducing the number of unprofitable operations and adoption of a highly disciplined
approach to the multi-client business contributed to these results. Q* revenue more than doubled in 2004
and interest from both new and repeat customers remained strong. Time-lapse towed-marine survey data
was delivered to a customer in Norway only eleven days after acquisition due to improvements introduced
through the Q-Xpress* processing chain. Building on the Q-Land* experience gained in 2003 in North Africa,
the system was extensively tested in the Middle East in 2004 with very satisfactory results.
Progress in Russia was marked by a number of important initiatives. Early in the year we acquired the
Siberian Geophysical Company, SGK, from our joint-venture partner and we completed the acquisition of our
initial 26% stake in PetroAlliance, an independent Russian oilfield services company. We opened a Technology
Hub on the campus of the Gubkin State University for Oil and Gas and we began engineering and manufacturing
activity in Novosibirsk to enhance our position in this crucial future market.
During the year we also took several steps to ensure the continuing future growth of Schlumberger in the
current up cycle. We decided to move our research center from Ridgefield, Connecticut to Cambridge,
Massachusetts. A site has been selected and the move will be completed in the summer of 2006. This will
place our principal research laboratory close to one of the most vibrant scientific communities in the world,
just as we have done with our other major laboratories in Cambridge, UK and in Moscow, in the Russian
Federation.
One key element in sustaining the growth and accelerating pace of new technology introduction will be the
continued quality of our workforce worldwide. To enhance this, we recently took a close look at the future of
our training organization. As part of the response, we opened the first of a new generation of training facilities
during the year. The new center, located in Europe, is dedicated to the training of Drilling & Measurements
and Well Completions & Productivity personnel. We also broke ground for a new center in the Middle East,
and during the next 12 months will begin work on a similar facility in Russias Tyumen Province. These new
centers will provide training facilities for multiple technology services.
The year also saw us make significant progress against our financial objectives. As a result of the divestiture
program we were able to reduce our net debt of from $4.2 billion to $1.5 billiona level we consider consistent
with the long-term capital structure of the company. In July the Board of Directors approved a stock
repurchase plan of 15 million shares and by year-end we had repurchased 5.15 million shares at an average
cost of $62.20 per share. Oilfield Services after-tax return on sales for the year rose to 14.2%, up from 13.5%
in 2003. Our return on capital employed rose to 19.1% at the end of 2004significantly higher than the
corresponding 9% in 2003. And on January 20th, 2005 the Board of Directors approved a 12% increase in the
annual dividend, the first increase in nine years. The mixture of the stock repurchase plan and the dividend
level now in place match our strategy of maintaining sufficient cash in the business to fuel growth while
returning any excess to our shareholders.
Last year I reported on the results of our efforts to ensure that service quality, safety, health and the
environment remain a top priority for the company. I am pleased to report that despite the rapid increase
in our activity our safety and driving record continued to improve in 2004. However, unlike 2003 where we
experienced no occupational auto fatalities, we regrettably suffered one in 2004. In a year when the total
miles driven increased by almost 20% to more than 183 million, a single fatality, while not acceptable,
represents a remarkable performance.
Last June I remarked that, absent a drop in demand caused by an economic recession, I consider us to be
entering the most favorable business climate since the 1970s. Subsequent events have yielded nothing to
change that view. I am not of the opinion that our business has ceased to be cyclical, but a sustained period
of investment in oil and gas will be necessary to meet the rapid growth in demand from China, India and the
other countries of the developing world as well as to replace inevitable production declines that are taking
place in the reservoirs bought on stream in the last long up cycle of the 1970s. Our global workforce, our
technology leadership and our geographic reach position us uniquely to grow in this environment.
Once again, I would like to thank our customers and employees for their support in what are exciting
times for our industry.

Andrew Gould
Chairman and Chief Executive Officer
