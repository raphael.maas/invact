In the face of heavy catastrophe losses and continued low interest rates in 2012, Chubb was still able to produce net income of $1.5 billion or $5.69 per share. Operating income, which we define as net income excluding realized investment gains and losses after tax, was $1.4 billion ($5.23 per share). For all three of our business units, it was a year of strong underlying performance as we focused on margin improvement through rate increases and disciplined underwriting.
Catastrophes are part of our business, but in the last two years their impact has been significant. In 2012, we sustained a record $1.1 billion impact before tax of catastrophes ($2.73 per share after tax), with Storm Sandy accounting for $882 million of the total. This exceeded the $1.0 billion impact in 2011, a year of multiple severe catastrophes on three continents. Although the median impact of catastrophes on our combined loss and expense ratio over the past ten years has been approximately 4 percentage points, the impact was 9.6 points in 2012 and 8.9 points in 2011.
Storm Sandy, which had the largest financial impact on Chubb of any single catastrophe in the companys history, struck areas where we have significant market presence  the states of New York, New Jersey and Connecticut

 with devastating wind and water damage and lengthy power outages. Our losses from Sandy, which were about evenly divided between our commercial and personal lines businesses, resulted in by far the largest number of claims from a single event in Chubbs history. Our celebrated claim service was once again well received by our customers, with nearly 98% of personal lines claimants rewarding us with the highest rating possible on the survey form: highly satisfied.
Net written premiums grew 1% in 2012 to $11.9 billion, reflecting our continued emphasis on profitability rather than the pursuit of growth for growths sake. We remained focused on improving renewal rates in all of our business units and on being more selective in the new business we write.
Our worldwide combined loss and expense ratio was 95.3% in both 2012 and 2011. Excluding the impact of catastrophes, the combined ratio improved to 85.7% in 2012 from 86.4% a year earlier. This improvement resulted in part from favorable loss experience excluding catastrophes. While catastrophe losses were substantial, we had fewer losses from

non-catastrophe weather-related claims in our personal and commercial property lines.
Global Reach
For more than 130 years, Chubb has provided property & casualty insurance to businesses and individuals around the world. In addition to our long-standing operations in the United States, Chubb has offices in 25 countries outside the United States. In 2012, net premiums written outside the United States accounted for 26% of our total.
Net premiums written in the United States grew 2%. Premiums written outside the United States, expressed in U.S. dollars, declined 3%, reflecting the impact of the stronger U.S. dollar relative to several currencies in which we wrote business. In local currencies, premiums written outside the United States grew 1%.
Our insurance operations both in the United States and outside the United States contributed to Chubbs profitable results in 2012. The combined ratio was 96.6% in the United States and 91.5% outside the United States.
Our international operations provide us with a measure of diversification through exposure to economic and insurance cycles that vary from those in the United

States. Our global presence also provides us with growth opportunities not available to domestic-only competitors. For example, a German multinational corporation prefers to do business with us because we can provide both standard commercial and unique specialty products for not only its
domestic operations but also its operations and facilities around the world. Likewise, high-net-worth families with multiple residences in different countries appreciate our ability to provide seamless coverage for all their homes and valuable articles.

Rewarding Shareholders
Chubb shareholders fared well in 2012. Book value per share increased 8%, and our return on shareholders equity was 9.9%. Total shareholder return was 11%, including market price appreciation and reinvested dividends. Over the longer term, an investment in Chubb has substantially outperformed both the broad market average as well as our peer company index:
 Over the past five years, Chubbs compound average annual total return to shareholders was 9.5%, which is 7.8 percentage points better than the S&P 500 and 8.8 points better than the S&P Property & Casualty Insurance Index.
 Over the past ten years, the compound average annual total return to Chubb shareholders was 13.9%, which is 6.8 percentage points better than the S&P 500 and 9.0 points better than the S&P Property & Casualty Insurance Index.
In 2012, our Board of Directors increased the common stock dividend for the 30th consecutive year. We also
repurchased 13.1 million shares of our common stock. In total, during 2012 we returned $1.4 billion to shareholders
in the form of dividends and share repurchases.
From the time our share repurchases began in December 2005 through the end of 2012, we have bought back 47% of the shares that were outstanding when the buybacks began. During that period, we have returned a total of $13.9 billion to shareholders, through $10.6 billion
of share repurchases and $3.3 billion of dividends.
In January 2013, our Board of Directors authorized
a new $1.3 billion share repurchase program. Subject to
market and other conditions, we expect to complete the
new program by the end of January 2014, demonstrating
our continuing commitment to return excess capital to
our shareholders.
Financial Strength and Our Conservative Investment Philosophy
Financial conservatism continues to be a hallmark of Chubb. This has been recognized in the high ratings for financial strength that Chubbs insurance companies have received from rating agencies. Our financial strength gives us a competitive advantage with our customers and independent

agents and brokers, because it is the underpinning of our ability to pay every covered loss fairly and promptly. At
year end 2012, our consolidated investment portfolio totaled $44.2 billion. About 86% of our portfolio was invested in fixed maturity securities with an average credit rating of Aa. Shareholders equity was $15.8 billion. At year end, our debt-to-capital ratio was 18.4%, well within the acceptable range for our ratings.
Our quest for higher insurance rates and increased underwriting income is being driven in part by the low interest rate environment that exists around the globe. As central banks and governments continue to induce artificially low interest rates in an attempt to jump-start their local economies following the financial crisis, the proceeds of our maturing investments are being reinvested at lower yields. This is putting downward pressure on our investment income and making it more difficult to generate adequate overall returns on our business.
Nevertheless, we do not intend to boost investment yields by increasing the risk profile of our portfolio. We believe that our conservative investment strategy has served us well though a variety of market cycles.
Operating Performance
Our three strategic business units performed well in 2012.
Chubb Personal Insurance (CPI), which accounted for 35% of Chubbs total net written premiums in 2012, serves primarily the high-net-worth market with the broad coverages of our Masterpiece policies. We insure fine homes, luxury and collector cars, yachts, jewelry, art and antiques, and we provide personal liability coverage, both primary and excess, to protect policyholders assets. CPI also includes our growing global accident business.
In 2012, CPIs net written premiums increased 4% to $4.1 billion, rising both in the United States and outside the United States. CPIs growth was due in large part to new business, strong retention of existing business, higher renewal rates and higher insured exposures upon renewal.
CPIs results in 2012 were greatly affected by catastrophes, particularly Storm Sandy. CPIs combined ratio was 94.4%, including a 13.7 percentage point impact of catastrophes. In 2011, CPIs combined ratio was 98.3%, including a 13.1 point impact of catastrophes. Excluding the impact of catastrophes, CPIs combined ratio improved substantially to 80.7% in 2012 from 85.2% in 2011. Average renewal rates in 2012 for our personal lines increased slightly in the United States and were relatively unchanged outside

the United States. CPI continued to retain a high percentage of its customers both inside and outside the United States.
Chubb Commercial Insurance (CCI), which accounted for 43% of Chubbs total net written premiums in 2012, offers a broad range of standard commercial insurance products. CCI focuses on specific industry segments and niches, and much of its customer base consists of mid-sized commercial entities. It provides such lines as property, marine, general liability, commercial auto, workers compensation, excess/umbrella and multiple peril insurance.
In 2012, CCIs net written premiums worldwide were up 2% to $5.2 billion, as a result of growth in the United States driven by improved pricing. Due in part to the negative impact of foreign currency translation, premiums written outside the United States declined modestly in 2012. In particular, growth in Europe was restrained by the fact that the post-recession recovery and rate environment of commercial insurance markets there have lagged the
United States.
As was the case with CPI, Storm Sandy and other catastrophes significantly affected CCI results. CCIs combined ratio was 99.0%, including an 11.4 percentage point impact of catastrophes. In 2011, CCIs combined ratio was 99.3%, including a 10.5 point impact of catastrophes. Excluding the impact of catastrophes, CCIs combined ratio improved to 87.6% in 2012 from 88.8% in 2011.
CCIs average renewal rates in the United States increased significantly in 2012 after rising modestly in 2011. We achieved increases in 2012 in all major lines. Average renewal rates outside the United States also increased slightly in 2012. Renewal premium retention remained strong.
Chubb Specialty Insurance (CSI), which accounted for 22% of Chubbs total net written premiums in 2012, provides a wide variety of specialized Professional Liability (PL) insurance products for publicly traded and privately held companies, financial institutions, professional firms, and healthcare and not-for-profit organizations. Professional Liabilitys lines include directors & officers, errors & omissions, employment practices liability, fiduciary, and commercial and financial fidelity. CSI also provides
Surety products.

In 2012, CSIs net written premiums decreased 6% to $2.6 billion. Premiums declined modestly both in the United States and outside the United States. CSIs combined ratio was 91.3% compared to 85.1% in 2011.
Professional Liability premiums were 5% lower in 2012, as growth was constrained by the continuing effects of the economic downturn in recent years and our focus on profitability rather than premium volume in what remains a highly competitive marketplace. PLs combined ratio
was 96.7%.
We were pleased that the improvement in the overall PL rate environment that we began to see in 2011 continued in 2012. Average 2012 renewal rates for PL were up significantly in the United States and up slightly outside the United States. PL renewal rates have increased each quarter for five consecutive quarters after having declined in six of the previous seven years. Renewal premium retention remained strong.
For our Surety lines, net written premiums in 2012 were down 11%, and the combined ratio was 51.4%. Premiums in the United States declined due to the impact of weak economic conditions on our customers. Premiums outside the United States increased modestly, driven by growth in Latin America.
Rate Environment
Operating margins in the property & casualty insurance industry have been under pressure from the effects of lower investment yields, rising loss costs and significant catastrophe losses. These factors underscore the need for rate increases. Pricing conditions in the standard commercial insurance business began to improve in late 2011, and this trend continued in 2012. Professional Liability rate increases in the United States lagged those of standard commercial but accelerated in the second half of 2012, as did rates in United States personal lines. Unfortunately, rate improvements outside the United States have lagged those in the United States.
Chubb remained focused on obtaining adequate rates for the business we write, securing mid- to high-single-digit rate increases on commercial and PL renewal accounts in 2012 in the United States. We continue to emphasize underwriting discipline and the importance of rate adequacy and profitability for both our renewal and new business. Even in lines where we might believe we have achieved rate adequacy at a moment in time, continued rate increases are

usually necessary to maintain adequacy, given average annual loss cost trends as well as the lower returns being generated on our investment portfolio.
The Outlook for 2013
We expect our net written premiums to be modestly higher in 2013 than in 2012, assuming that average currency exchange rates relative to the United States dollar remain at year-end 2012 levels. Although the global insurance market will likely remain highly competitive in 2013, we expect our performance to be favorably affected by the higher rates we achieved in 2012 being reflected in our earned premiums, as well as continued rate increases in 2013.
Overall, we expect to see an improvement in our underlying margins, as earned rate increases should exceed long-term loss trends looking at the business as a whole. Whether this winds up being reflected in our bottom line will, of course, depend on our actual loss experience in the year. In 2013, we expect that non-catastrophe weather-related losses in our personal and commercial property lines will revert to more normalized levels from the low levels we experienced in 2012. Improvement in our underwriting results will be partially offset by continued pressure on investment income from low interest rates. On balance, however, assuming a more typical impact of catastrophes,
we expect significantly improved operating earnings in 2013.

We are optimistic about Chubbs prospects
going forward:
 We expect to see continued rate increases in 2013 in the United States and hope to see some improvement in rates outside the United States.
 There are signs that the United States economy is improving slowly, and we are hopeful that the European economy will begin to stabilize and improve over time. Economic growth usually results in higher insurable exposures and increased demand for our products and services.
 We will continue to adhere to our underwriting strategy that has been successful over time, focusing on niche products and geographic areas, as well as disciplined risk selection.
 We will continue to emphasize profitability over growth, and remain committed to obtaining adequate pricing on both new and renewal business.
 Chubb continues to be the insurer of choice for many of our independent agents and brokers who recommend Chubb to their best clients because of our broad coverages, financial strength and unparalleled claim service.
I am grateful to our customers, employees, agents and brokers for your role in making 2012 another successful year for Chubb.
John D. Finnegan
Chairman, President and Chief Executive Officer
February 28, 2013