                                                                                                                                                             1
                                                                    President's Letter to Shareholders
                    I will start this letter by stating the obvious: 2009             We should see the benefits of this hard work well into the future.
                    was a very difficult year for Fastenal. The traditional
                    measures of sales growth, earnings growth, return     In January 2009, our leadership team spent a lot of time communicating
                    on assets, and most other financial measures          with employees to prepare them for the difficult period ahead. We held
                    would indicate our team underperformed. However,      meetings, wrote articles, and produced some internal video messages
                    these do not tell the entire story. I believe we did  to inform everyone in the company about the upcoming challenges.
                    a good job in the face of a difficult situation, and IFirst, we reassured everyone that we would do everything we could to
                    will attempt to explain why in this letter.           avoid personel reductions. Everyone was also told that, because our
                                                                          pay is performance-based, many of us would make less in 2009 than
For the year, our sales declined 17.5% and our total revenue came         we had in 2008, so we encouraged everyone to plan accordingly. We
in at $1.9 billion. The monthly sales pattern looks like a reverse bell   announced there would be a hiring freeze for support personnel  if
curve, with our year-over-year average daily sales falling from -8.5%     someone left the group, everyone would have to pull together to cover
in January to -22.9% in July. Since July, our average daily sales have    the workload. We also talked about employee benefits, reminding
improved each month, and we ended December down 8.6%.                     everyone that our 401(k) profit sharing program is based on achieving
                                                                                              certain financial targets which would be difficult to
The challenges were apparent heading into the "The fact that we have more attain in 2009. The general message was clear: Most
year. We saw the world economy start to weaken                                                people would probably have to work harder for less
                                                        customers buying from us is pay, but we would do everything we could to protect
in October 2008, turn down sharply in November,
                                                        a very good indication that, jobs and benefits for the future. The response from
and continue to worsen in December. Our historic
trend is for our average daily sales to decline 10.0                                          the team was overwhelmingly positive, and in my
                                                          although our sales were
to 11.0% from September to December due to                                                    opinion, employee morale has been very good all
                                                          down, we still managed
normal seasonal changes in our business. In 2008,                                             year. I'm not saying people enjoy this economic
                                                         to grow market share. We
we saw a 23.3% drop during that period, the most                                              situation, but I believe that when you speak openly
dramatic change we've ever seen in such a short                                               and honestly with people, they'll usually rise to the
                                                         should see the benefits of
span. The trend continued into the new year as                                                occasion. And that's exactly what our team did.
                                                           this hard work well into
our average daily sales declined each month until
                                                                  the future."
April, when we bottomed at $7,249,000 per day, a                                              I believe these open meetings had a lot to do with
27.9% decline from the high in September 2008.                                                the good job we did in controlling our operating
(In comparison, from September 2007 to April 2008 our average             expenses in 2009. One thing we learned from the slowdown is how
daily sales grew by 4.4%.) We continued to see weakness from              hard it is to reduce spending. We have been in a growth mode for
April through July, but we seemed to be bouncing off the bottom.          the last 42 years, and it's not easy to change the mindsets of 12,000+
Then, in the July/August time frame, our sequential trends began to       people in a short time. It took a few months to slow our expense
improve. While our historical sales trend would predict a seasonal        growth, but in the second quarter we showed an 11.2% reduction in our
drop in average daily sales of 6.2% from July to December, our daily      operating expenses on a year-over-year basis. For the year, expenses
average dropped only 3.5% during this period.                             were down 12.6%.

We continued to add customers during 2009; however, our existing                      Our net earnings for 2009 were $184.4 million, a 34.1% decrease
customers purchased less due to the recession. During 2009, the                       compared to our 2008 earnings of $279.7 million. The two major
average customer purchased 11.9% less than in 2008. The average                       factors that contributed to the drop in earnings were lower sales
invoice amount in 2009 fell from a high of $200 in January to a low of                dollars and a decline in our gross margin. Our reported gross margin
$163 in July. Since July, we've seen a steady improvement and ended                   for 2009 was 50.9%, a decline of 3.6% from the 52.8% reported
December with an average invoice amount of $189. The fact that we                     in 2008.
have more customers buying from us is a very good indication that,
although our sales were down, we still managed to grow market share.




Three main factors contributed to the drop in gross margin. The first      around $650 million, and we based our inventory purchases on this
was the deflation in steel prices triggered by the world economic          expectation. Our actual sales came in at $489 million, which meant
slowdown. This affected our fastener product line. Normally we stock       that we had excess inventory for several months. Everyone worked
nine to ten months worth of inventory on import fasteners, so when         together to analyze the information and determine what needed to be
business slowed so quickly, the time needed to sell the inventory          done in order to reduce our excess inventory without causing service
lengthened. At the same time, many customers requested lower prices,       problems for our customers. I'm pleased to report that by the end
as they were also under pressure to reduce costs. It is not our normal     of the year we were able to reduce our inventory by 9.9%, or $55.8
business practice to lower prices before lower cost inventory arrives;     million, compared to year-end 2008.
however, we understood the importance of retaining our customers,
so in many cases we passed along price concessions to help maintain        Because of the positive trends in our accounts receivable and inventory,
the business relationship. Another factor that contributed to the drop     along with reduced demand for working capital due to lower sales,
in margin was the increased pressure from our competitors during the       we are currently in a very strong cash position. In 2009, we paid out
first half of the year. It was somewhat of a free-for-all in the market,   aggregate dividends of $0.72 per share, or $106.9 million. We also
with everyone trying to grab business to survive. By the middle of         repurchased 1.1 million shares of our company stock. Our investment
the year, this pressure subsided, and business became more normal          in capital expenditures came in at $52.5 million, a reduction of 44.9%
in the second half of the year. The third factor that affected margin      from the $95.3 million we spent in 2008. After these expenditures, we
was the rebate programs and volume incentives we receive from our          managed to increase our total cash and marketable securities at year
suppliers. Over time we've negotiated rebate and volume incentive          end by $107.9 million, ending the year with no debt, $164.9 million
programs with our suppliers that are usually based on growth. At           in cash and cash equivalents, and another $30.6 million in marketable
the beginning of the year, our product managers                                                securities.
did a good job of renegotiating these programs              "Because of the positive
to adjust for the slower economy, but at that time                                             In 2007, we introduced our Pathway to Profit
                                                              trends in our accounts
we did not foresee such a severe drop in revenue.                                              strategic growth plan. This plan calls for us to
                                                           receivable and inventory,
So despite our efforts, many of the programs did                                               balance our growth investments between opening
not work out as they had in the past. All three of       along with reduced demand new stores and adding additional sales people in
these factors are timing issues which we believe                                               our existing stores. Through this balanced growth
                                                             for working capital due
are behind us at this time. This is why we expect                                              plan, we projected that we could grow our average
                                                              to lower sales, we are
an improvement in gross margin for 2010.                                                       store size from approximately $80,000 to $125,000
                                                                                               in monthly revenue over a five-year period ending
                                                           currently in a very strong
Our team did a good job of managing assets in the                                              in 2012, while at the same time increasing our pre-
                                                                  cash position."
last 12 months. Early in the year I was concerned                                              tax profit by one percentage point each year  from
about how our accounts receivable would track in                                               18.0% to 23.0% of sales. Because of the economic
the slow economy. We did see an increase in customer bankruptcies,         setback we encountered in 2009, we believe the time required to
which caused an increase in our bad debt expense. But due to some          achieve our goals for the Pathway to Profit strategy will be extended
changes in the way we mechanically bill our customers  and a lot          by another 24 to 30 months. That said, we believe one of the reasons
of hard work by the accounts receivable team  the overall accounts        we were able to reduce our operating expenses so quickly in 2009
receivable numbers, on a day's outstanding basis, dropped on a year-       was that we had implemented this plan in 2007 and had fewer stores
end comparison.                                                            and a more variable cost structure. This experience, along with other
                                                                           things we have learned, has strengthened our conviction that Pathway
As indicated earlier, the sudden drop in sales caught us in a high         to Profit is a good long-term growth strategy for Fastenal.
inventory position. A large percentage of our fastener products are
purchased five to six months in advance due to the longer lead times       In 2008, we recognized the need to provide cost-effective vending
of these products. Before the market slowed in the fourth quarter of       solutions for our customers. The vending systems that were on
2008, we expected that revenue in the first quarter of 2009 would be       the market at the time were quite expensive and only cost-effective


for large customers with very high volume. We believed if we could           In December, we acquired Holo-Krome from Danaher Corporation.
develop a more affordable solution we could expand the market to             Located in West Hartford, Connecticut, Holo-Krome is a leading
smaller and medium sized customers. Our team researched various              manufacturer of socket head cap screws, and the acquisition
solutions and found a manufacturer to work with to help design our           greatly increases our capacity to make high-quality domestically
SmartStore vending program. The SmartStore machines provide                  produced fasteners. To that end, we have also worked on expanding
a customer's employees with 24-hour access to the products they              the capabilities of our cold heading facility in Rockford, Illinois.
need (e.g., safety supplies or general maintenance                                               The investments in new equipment and facilities
items) using their employee badge. They also give                                                over the last several years at our Rockford plant
                                                              "International expansion
our customers complete control and accountability                                                enabled us to produce a special flange bolt that
over the material they use. It required time to                                                  was required by a large customer. The parts had to
                                                              continues to be a strong
develop this program and train our people, but                                                   be domestically produced, and they were working
                                                                 focus for the future.
as the learning curve rose there was tremendous                                                  on a very short lead time. We were able to make
                                                               We currently operate in
growth in the number of customers who signed up                                                  the part and meet the lead time the customer
over the course of the year.                                                                     needed, which led to several more large orders
                                                              Canada, Mexico, China,
                                                                                                 for similar parts from that customer. With the
We also made great strides with our Internet The Netherlands, Singapore additions of West Hartford and Malaysia, we now
                                                              and as of 2009, Hungary
development in 2009. One of the highlights was the                                               have six manufacturing locations totaling more
launch of our new website in April 2009, which has                                               than 400,000 square feet and employing more
                                                                    and Malaysia."
garnered very good feedback from our customers.                                                  than 400 people dedicated to making parts for
The challenge for our team is to design a system that                                            our customers. In 2009, our manufacturing team
combines the product search capabilities of the web with our high-quality    produced and shipped more than 45,000 orders. The manufacturing
local service. This fits into our plan to give customers a range of choices  team works very hard to run lean operations. This allows them to
regarding how they purchase product  over the phone, at the local store,    offer low minimum order quantities and fast lead times.
as part of a vendor-managed inventory program, through our vending
machines, or via the web  and have it all serviced and delivered by a local The product development team continued to work hard on developing
sales person.                                                                Fastenal's exclusive brands. We now have 12 exclusive brands covering
                                                                             most of our product areas. We have found that many customers are
International expansion continues to be a strong focus for the future.       very interested in exclusive branded products if you can deliver both
We currently operate in Canada, Mexico, China, The Netherlands,              quality and a competitive price. With this in mind, the product team
Singapore  and as of 2009, Hungary and Malaysia. The businesses             has developed relationships with manufacturers that can produce
in Canada and Mexico are very similar to the store-based model we            high-quality product and still sell it at a competitive price, creating a
use in the United States. The businesses in the other countries are          win-win situation for both our customers and our sales people.
set up to sell to larger multinational customers, many of which we're
already doing business with in other parts of the world. This strategy       In December 2008, we moved into our new distribution center in
has worked well, and that is why we opened operations in two more            Denton, Texas. This facility incorporates cutting-edge distribution
countries in 2009. Our operation in Malaysia is a sales operation, but       technology designed to reduce both cost and shipping errors. It took
it also includes a manufacturing facility. Many of our customers in          five to six months to learn the new system, and now we are starting
that area are in the gas and oil business and have a need for specialty      to see the benefits of this investment. We also added a similar system
bolts and studs, which we're now able to produce locally, providing          to our existing distribution center in Indianapolis. This project was
fast delivery on high-quality parts. Our plan is to continue to invest in    started in 2008 and came on-line in the second and third quarters
these and other countries in the future to drive above average growth        of 2009. The facility in Indianapolis is much larger than the one in
over the next several years.                                                 Denton but incorporates similar systems and technologies. Although
                                                                             it's early in the start-up, we are pleased with the systems' speed
                                                                             and the early results on shipping accuracy. We are also seeing the



gains in labor efficiency we had anticipated. Our main goal in this                                 Another area we invested
facility is to shorten the time it takes to process orders during the day                           in this year is our National
so we can load the trucks earlier and provide next-day service to a                                 Accounts team. Because of
high percentage of our customers. More than 70% of our customers                                    the slow economy, many
operate within 750 miles of this facility, which is the distance a                                  companies were looking for
semi-truck can cover between 5:00 p.m. and 7:00 a.m. We are not                                     ways to save money and
able to cover this entire service area overnight today, but our people                              were more willing to look
understand the goal and, utilizing our new technology, are working                                  at new suppliers. In light of
hard to achieve it.                                                                                 this situation, we had our
                                                                                                    National Accounts leads add sales people where they felt they needed
                                                                                                    them, and we saw the benefits throughout the year.

                                                                                                    As you read this letter, you will notice a consistent theme: Fastenal
                                                                                                    people are what make it happen. That's why we continued to invest in
                                                                                                    our people despite the tough economy. Even though we reduced our
                                                                                                    training budget, the Fastenal School of Business maintained a busy
                                                                                                    training schedule in 2009. To reduce cost, most of the training was
                                                                                                    done in the field. This was more difficult for our instructors, but it was
                                                                                                    the only way to achieve their goals while staying on budget. We also
                                                                                                    made investments to improve our hiring practices. As we have grown,
                                                                                                    it has become harder to identify the best talent to hire, so our human
                                                                                                    resources team spent much of 2009 working to design and provide
                                                                                                    better hiring tools for our field people. We believe the best place to
                                                                                                    make the hiring decision is close to the source, and these new tools
                                                                                                    should allow our local leaders to make better decisions.

                                                                                                    In closing, I want to say that even though 2009 was not a good year by
                                                                                                    most measures, I am proud of the performance of our team. I believe
   "As you read this letter, you will notice a consistent
                                                                                                    if we continue to foster an entrepreneurial environment that allows
   theme: Fastenal people are what make it happen."                                                 our people to make decisions that benefit our customers, then we will
                                                                                                    continue to create greater opportunities for our team and be successful
Our district and regional leaders focused much of their attention on                                well into the future. This success should also create long-term benefits
working with underperforming stores during the year. Using our                                      for all of our stakeholders.
internal benchmarking, they identified the stores that were performing
in the bottom quartile and worked with them to develop a plan to
improve their performance. Even with the number of stores we have, it
                                                                                                    Thank you for your continued support.
is still difficult to quickly identify what is holding a store back. So our
district managers analyze every aspect of the business, from personnel
to pricing, and try to implement the necessary changes to improve
their performance. We are very focused on improving the performance
of these locations so they are in a better position to take advantage of
                                                                                                    Willard D. Oberton
the economy as it recovers.
                                                                                                    President & CEO


