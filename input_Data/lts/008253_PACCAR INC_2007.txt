to our shareholders
PaCCaR had an excellent year in 2007 due to its global diversification,  
superior product quality, technology-led process efficiency and record results from 
aftermarket parts and financial services.  Customers benefited from PaCCaRs record 
$682 million of capital investments and research and development, which enhanced 
manufacturing capability, developed innovative aftermarket support programs and 
accelerated new product introduction.  PaCCaR delivered 133,900 trucks to its 
customers around the world and sold $2.3 billion of aftermarket parts.  Record truck 
deliveries in Europe, Mexico and australia were partially offset by a weak truck 
market in the U.S. and Canada.  PaCCaR Financial Services generated $3.9 billion of 
new loan and lease volume.
Net income of $1.23 billion on revenues of $15.2 billion was the second highest 
in the companys 102-year history.  PaCCaR issued a 50 percent stock dividend during 
the year and declared cash dividends of $1.65 per share, including a special dividend 
of $1.00.  Regular quarterly cash dividends have increased over 470 percent in the last  
10 years.
The U.S. and Canadian Class 8 truck market declined
45 percent in 2007 from the previous year due to
transport companies pulling forward vehicle purchases
in 2006 to avoid more costly 2007 EPA emissioncompliant engines and a slower economy, resulting from
the housing and automotive industry downturn.		The
Class 8 truck market in North America, including Mexico,
was 207,300 vehicles, compared to 348,000 the prior
year.		The European heavy truck market in 2007 was a
record 340,000 vehicles, compared to 309,000 in 2006,
due to strong economic growth in the European Union,
including the economies of Central Europe.	
	 PACCAR continued to set the standard for financial
performance for capital-goods companies worldwide.		
After-tax return on beginning shareholder equity (ROE)
was 27.5 percent in 2007, compared to 38.3 percent in
2006.		The companys 2007 after-tax return on revenues
was 8.1 percent.		Profits were driven by strong European
truck sales, global parts sales and new finance contracts
for 60,000 units.		PACCARs outstanding financial
performance has enabled the company to distribute over
$3.0 billion in dividends and triple shareholder equity to
$5.0 billion during the last ten years.		PACCARs average
annual total shareholder return was 22.7 percent over
the last decade, versus 5.9 percent for the Standard &
Poors 500 Index.
INVESTING FOR THE FUTURE  PACCARs excellent
profits, sparkling balance sheet, and intense focus on
quality, technology and productivity enhancements have
enabled the company to consistently invest in products
and processes during all phases of the business cycle.		
Productivity, efficiency and capacity improvements continue to be implemented in all manufacturing and 
parts distribution facilities.  Many PACCAR facilities 
established new records during the year in terms of 
quality metrics, inventory turns and assembly hours.  
PACCAR is recognized as one of the leading technology 
companies in the world, and innovation continues to be 
a cornerstone of its success.  PACCAR has integrated 
new technology to profitably support its business, as 
well as its dealers, customers and suppliers. 
Capital investments were a record $427 million in 
2007.  An exciting multi-year initiative was launched 
with the commencement of construction for PACCARs 
$400 million engine assembly plant in Mississippi, which 
builds upon its legacy as a premier engine manufacturer.  
Other major capital projects during the year included 
completion of a $70 million engine research and 
development center in Eindhoven, the Netherlands; 
opening of a parts distribution center (PDC) in 
Oklahoma City; beginning construction of a new parts 
distribution center in Budapest, Hungary; and 
completion of a 30 percent capacity expansion at 
Kenworths Chillicothe, Ohio facility.
PACCAR is judiciously examining business 
opportunities in Asia, with its primary focus on China 
and India.  The company has sold transportation 
equipment in Asia since 1908.  The rapidly developing 
highway systems in China and India will increase intracountry commerce, resulting in demand for reliable 
high-quality commercial vehicles.  PACCAR opened a 
purchasing and component sales office in Shanghai in 
2007, complementing its Beijing sales office, and plans 
to open a purchasing and sales office in India in 2008.
SIX SIGMA  Six Sigma is integrated into all business 
activities at PACCAR and has been adopted at 180 of the 
companys suppliers and many of the companys dealers.  
Its statistical methodology is critical in the development 
of new product designs, customer service and 
manufacturing processes.  Since inception, Six Sigma has 
delivered over $1 billion in cumulative savings in all 
facets of the company.  In addition, High Impact 
Kaizen Events (HIKE) leverage Six Sigma methods with 
production flow improvement concepts.  The HIKE 
projects conducted in 2007 were instrumental in 
delivering improved performance across the company.  
More than 11,000 employees have been trained in Six 
Sigma and 7,000 projects have been implemented since 
its inception.  Six Sigma, in conjunction with Supplier 
Quality and Development, has been vital to improving 
logistics performance and component quality by the 
companys suppliers.
INFORMATION TECHNOLOGY  PACCARs 
Information Technology Division (ITD) and its 740 
innovative employees are an important competitive asset 
for the company.  PACCARs use of information 
technology is centered on developing and integrating 
software and hardware that will enhance the quality and 
efficiency of all products and operations throughout  
the company, including the seamless integration of 
suppliers, dealers and customers.  In 2007, ITD provided 
innovative advancements in GPS systems, new 
manufacturing software and infrastructure capacity 
upgrades and installed over 1,800 new personal 
computers.  Over 16,000 dealers, customers, suppliers 
and employees have experienced the companys 
technology center, which highlights automated finance 
applications, sales and service kiosks, tablet PCs and 
Radio Frequency Identification (RFID).  New features 
include an electronic leasing and finance office and an 
electronic service analyst.
TRUCKS  U.S. and Canadian Class 8 retail sales in 
2007 were 175,800 units, and the Mexican market 
totaled 31,500.  The European Union (EU) heavy truck 
registrations were 340,000 units.
PACCARs Class 8 retail sales in the U.S. and Canada 
achieved a record market share of 26.4 percent in 2007 
compared to 25.3 percent the prior year.  DAF achieved 
13.9 percent share in the 15+ tonne truck market in 
Europe.  Industry Class 6 and 7 truck registrations in the 
U.S. and Canada numbered 85,000 units, a 21 percent 
decrease from the previous year.  In the EU, the 6- to 
15-tonne market was 84,000 units, down 4 percent from 
2006.  PACCARs North American and European market 
shares in the medium-duty truck segment were excellent, 
as the company delivered nearly 25,000 medium-duty 
trucks and tractors in 2007.
A tremendous team effort by the companys 
purchasing, materials and production personnel ensured 
improved product quality and manufacturing efficiency 
during challenging market conditions.  The negative 
effect of high commodity prices was partially offset by PACCARs excellent long-term supplier partnerships, 
which enabled production and efficiency improvements.
PACCARs product quality continued to be recognized 
as the industry leader in 2007.  Kenworth and Peterbilt 
dominated customer satisfaction awards in the Class 6,  
7 and 8 markets and the DAF XF105 was the 2007 
International Truck of the Year.
Over 60 percent of PACCARs revenue was generated 
outside the United States, and the company realized 
excellent synergies globally in product development, sales 
and finance activities, purchasing and manufacturing.  
DAF Trucks achieved record truck production, sales,  
and profits and excellent market share.  DAFs strong 
backlog and growth in Western and Central Europe has 
been generated by its modern product range, extensive 
dealer network and superior aftermarket support.
Leyland Trucks is the United Kingdoms leading truck 
manufacturer.  Leyland expanded its innovative bodybuilding program that delivers custom-bodied vehicles 
to customers.  It also began production of the complete 
range of DAF XF and CF vehicles.
PACCAR Mexico (KENMEX) had another record 
profit year as the Mexican economy grew and truck 
fleets were renewed.  KENMEX recorded gains in plant 
efficiencies as production reached an all-time high.  
KENMEX is increasing the size of its aftermarket parts 
distribution center by over 60 percent to further 
enhance customer service and meet growing demand.
PACCAR Australia achieved record profits and sales 
in 2007, supported by the highest production level in the 
companys history.  The introduction of new Kenworth 
models and expansion of the DAF product range in 
Australia combined for a 22.7 percent heavy-duty 
market share in 2007.  Aftermarket parts sales delivered 
another year of record performance.
PACCAR International exports trucks and parts to 
over 100 countries and had a record year due to strong 
sales buoyed by natural resource exploration globally.
AFTERMARKET CUSTOMER SERVICES  PACCAR 
Parts had an outstanding year in 2007 as it earned its 
15th consecutive year of record profits.  With sales of 
$2.3 billion, PACCAR Parts is the primary source for 
aftermarket parts for PACCAR products and supplies 
parts for other truck brands to PACCARs dealer 
networks around the world.  Over five million heavyduty trucks are operating in North America and Europe, 
and the average age of these vehicles is estimated to be 
over six years.  This large vehicle parc will create 
excellent demand for parts and service and moderate 
the cyclicality of truck sales.
PACCAR Parts is adding new distribution centers  
and expanding current facilities to enhance logistics 
performance to dealers and customers.  PACCAR Parts 
continues to lead the industry with technology that 
offers competitive advantages at PACCAR dealerships.  
Managed Dealer Inventory (MDI) is now installed at 
over 1,000 PACCAR dealers worldwide, including South 
America.  PACCAR Parts enhanced its Connect program, 
a software solution for customer fleet-maintenance 
management.  The program is a Web-based application 
providing fleets the tools to reduce their vehicle 
operating costs.
FINANCIAL SERVICES  The PACCAR Financial 
Services (PFS) group of companies has operations 
covering three continents and 18 countries.  The global 
breadth of PFS, as well as its industry-leading funding 
structure and responsive credit application processes, 
enabled the portfolio to grow to more than 169,000 
trucks and trailers, with total assets exceeding $10.7 
billion and pretax profit at a record level of $284 million.
PACCAR Financial Corp.s (PFC) conservative 
business approach, coupled with PACCARs superb S&P 
credit rating of AA- and the strength of the dealer 
network, enabled PFC to earn excellent results in 2007 
despite turbulent financial markets worldwide.  PFC 
offers a comprehensive array of finance, lease and 
insurance products.  PFC is the preferred funding source 
in North America for Peterbilt and Kenworth trucks, 
financing 23.3 percent of dealer sales in the U.S. and 
Canada in 2007.
PACCAR Financial Europe (PFE) completed its sixth 
year of operations and increased assets and profits as it 
served DAF dealers in 14 Western and Central European 
countries.  PFE provides wholesale and retail financing 
for DAF dealers and customers and finances almost 21 
percent of DAFs vehicle sales.
PACCAR Leasing (PacLease) earned its 14th 
consecutive year of record operating profits and delivered 
4,600 new PACCAR vehicles in 2007.  The PacLease fleet 
grew to over 32,000 vehicles as 20 percent of the U.S. and Canadian Class 6-8 market chose full-service leasing to 
satisfy their equipment needs.  PacLease substantially 
strengthened its market presence in 2007, increasing the 
global network to 328 outlets, and represents one of the 
largest full-service truck rental and leasing operations in 
North America.
PacLease acquired TCH Leasing, a leading 
independent truck leasing company in Germany.  This 
strategic investment provides the foundation to grow 
PacLease throughout the European Union.
ENVIRONMENTAL LEADERSHIP  PACCAR is a global 
environmental leader.  A significant accomplishment was 
earning ISO 14001 environmental certification at all 
PACCAR manufacturing facilities in Europe and North 
America.  PACCAR plans to introduce medium-duty 
hybrid-electric vehicles in mid-year 2008, which can 
achieve up to a 30 percent fuel economy improvement.  
Kenworth and Peterbilt launched proprietary technology 
that can increase fuel economy 8 percent by eliminating 
the need for customers engines idling at night.  Kenworth 
and Peterbilt earned the prestigious EPA SmartWay 
designation for designing environmentally friendly 
products.  PACCAR employees are environmentally 
conscious and utilize van pools, car pools and bus passes 
for 30 percent of their business commuting.  
A LOOK AHEAD  PACCARs 21,800 employees 
enabled the company to distinguish itself as a global 
leader in the technology, capital goods, financial services 
and aftermarket parts businesses.  Superior product 
quality, technological innovation and balanced global 
diversification are three key operating characteristics 
that define PACCARs business philosophy.  The 
company continues to take aggressive steps to manage 
production rates and operating costs, consistent with its 
goal of achieving profitable market share growth.
In the next five years, PACCAR plans to significantly 
increase its capital investments and related research and 
development in order to design and launch a new range 
of vehicles, increase global production capacity and 
develop a new family of industry-leading PACCAR 
engines.  The higher research and development expenses 
may dampen earnings in the short term, but are 
expected to generate superior results in the long term.
PACCARs excellent balance sheet ensures that the 
company is well positioned to continually invest in all 
facets of its business, strengthening its competitive 
advantage.
Other fundamental elements contributing to the 
exciting prospects of this vibrant, dynamic company are 
geographic diversification, modern manufacturing and 
parts distribution facilities, leading-edge and innovative 
information technology, conservative and comprehensive 
financial services, responsive suppliers, enthusiastic 
employees and the best distribution network in the 
industry.
PACCAR and its employees are firmly committed to 
strong, quality growth and are proud of producing  
69 consecutive years of net profit.  The embedded 
principles of integrity, quality and consistency of 
purpose continue to define the course in PACCARs 
operations.  PACCAR has successfully evolved as a leader 
in several industries since its founding in 1905.  The 
proven business strategy  delivering technologically 
advanced, premium products and an extensive array of 
tailored aftermarket customer services  enables PACCAR 
to pragmatically approach growth opportunities, such as 
Asia and financial services, with a long-term focus.  The 
strength of the business foundation provides a platform 
to examine growth opportunities in complementary 
business segments worldwide.  PACCAR is enhancing its 
stellar reputation as a leading technology company in 
the capital goods and financial service marketplace.

Chair man and Chief Executive Officer 
