Dear Shareholders:
Sometimes it is a stumble that puts you into the right position. And while that is no
argument for generally stumbling about, our imperfect start to 2012 positioned us
for a very good year of progress.
As we entered 2012, we knew that patience, both inside and outside the company,
was wearing thin. We had yet to deliver on the promise that the strategy we unveiled
at our 2011 investor conference would translate into better performance. Our level
of strategic investment was still planned to be high, increasing nerves and giving
credence to skeptics. At the same time, our Fresh Start business review process
and our re-underwriting efforts were due to show up in the results and we were
determined to make that true.
While we still believed better results were at hand, it was getting lonely.
Happily, 2012 proved to be the pivotal year that we had forecast it to be. But it sure
didnt start that way.
The first signs of trouble emerged when it became clear that a number of our
businesses  seven of our 34 in fact  experienced a spate of unusual large losses
heading into the 2011 year end. Given the variability across our portfolio, you always
expect some degree of challenge, but you dont expect many businesses to all raise
their hands at once. Suddenly, it was clear that trust in our orderly improvement plan
was going to be severely tested.
And then to compound the stumble, our fourth quarter 2011 earnings call was
poorly received, as in an effort to provide clarity, we dwelt on the challenged
businesses and emphasized spending plans and modest share buybacks that in the
words of one analyst, presented a perfect bear case for XLs stock.
Little did we know at the time, but this sequence of events perfectly positioned us
for what came next.

There were three benefits: First, our confidence switched to urgency. While we have
maintained great confidence in our strategy, the danger in confidence is that it can lead
to complacency. Our reaction was anything but complacent and we initiated a tighter top
level focus on the underwriting actions in challenged business units.
Second, we re-examined the pace of strategic investments relative to expanding
margins  balancing investment in newer, often slower, growth lines, with proper scaling
of our infrastructure and appropriate capital buffer allocations. This combined for what
was, in our mind, exactly the right value creation message.
Third, the need to correct the effects of the Q4 2011 earnings call led to our better
explaining how our plan would, we believed, allow for margin expansion in 2012, and
continued top-line growth and expanding margins in 2013.
From then forward, 2012 was focused and relatively straightforward: Maintain discipline
and drive delivery of the revised plan. While we still have much more to do  a point we
make at every opportunity  we are now seeing the tangible proof of our progress.
In 2012, quarter by quarter, our discipline was well rewarded. We delivered solid
earnings and growth in book value including a nearly 12 point increase in our operating
margin versus 2011 and just over an 11 point decrease in our overall combined ratio for
the same period. That these strong results came despite the broad and well publicized
impacts of Superstorm Sandy, speaks to the degree and quality of our re-underwriting,
as well as the continuing excellence of our Enterprise Risk Management (ERM) efforts.

For the full year, Property and Casualty (P&C) operations combined ratio, excluding Prior Year Development (PYD) and the impact of natural catastrophe losses, was 93.7%, compared to 98.5% in 2011.2 We finished the year with an all-in Return on Equity (ROE) of 6.2%. This is a far better result than our 0.9% 2011 ROE but, make no mistake, is not a result with which we are satisfied. Our ROE target, as we have stated since the implementation of our strategy, is for double-digit returns.
The area we and outside audiences looked to most for proof of progress in expanding margins was in our Insurance segment results. Here, we think the best indicator of the segments very real improvement is the reduction in the accident year, ex-CAT loss ratio, which for the full-year of 2012 was 66.6%. This is a 6.4 point improvement over 2011, and our best full-year Insurance loss ratio performance since 2007.
Also significantly, by the end of 2012, the accident year ex-PYD and ex-CAT combined ratio of the seven businesses we previously described as challenged was 99.3%. This is versus 113.0% in 2011 and as compared to 98.3% for the rest of the performing Insurance segment portfolio in 2012. Said another way, this group was brought closer to the level of the rest of the Insurance segment and given the significant progress made in rehabbing these businesses, we have eliminated the challenged label. But, we are diligently spreading to all of our business the accountability mindset and practices that led to this improvement.

As Insurance improved throughout the year, XLs Reinsurance segment continued
its excellent record. Reinsurance, even after taking the brunt of our Sandy losses,
produced an all in full year combined ratio of 86.9%. Excluding the impact of both
natural catastrophe losses and PYD, this was a combined ratio of 83.1%. An excellent
underwriting result.
In our investment portfolio we continued to play defense, being highly selective in our
asset allocation decisions  particularly in relation to Europe where, in our opinion,
significant issues remain. As a result, our portfolio performed well in 2012, generating
a total return of 6.7% in aggregate  clearly helped by strong recovery in the market
values of some of the assets that were our problem areas in recent years, the decline in
interest rates in our core markets as well as credit spread tightening across almost all
asset classes.
For the full year, we reported an operating profit of $614 million or $1.98 per share,
compared to $89 million or 28 cents per share in 2011. We also continued to act on the
compelling economics of buying back our ordinary shares  about $500 million in shares
that we associate with 2012  $402 million during the calendar year itself, and another
$98 million in January 2013 after the uncertainty around the losses caused by Sandy
forced a pause in our purchase schedule. And as the year progressed, confidence inside
of XL translated to confidence outside of XL. We ended 2012 with share price growth of
27% and close to 19% growth in our tangible book value per share. Our progress has also
continued to be noted by rating agencies and needless to say, we felt their revisiting of
XL was well deserved.

Now, dont get me wrong  we did not achieve this all on our lonesome. We had some help from broader trends.
It was, across the industry, a year of relatively low large losses  aside, of course, from Sandy. And while Sandy was a huge event, for us as well as for the industry, our early display of awareness of the likely industry loss and the subsequent explanations of our particular loss were calmly received. But, for XL, there is a deeper truth about Sandy. It was the re-underwriting of our U.S. property exposure by the new talent we had recruited in 2011 (aided by some strong partnering from our ERM folks) that prevented us from having an out-sized Sandy loss. We finished 2012, as we finished 2011, with catastrophe losses as a percent of our shareholders equity below the median and average of our peer group.
Fortuity no doubt helped in other aspects of our results as well. For example, while we are increasingly confident of the improvements in our International P&C business, it is still too early to tell just what percentage of its improved results is luck versus design. Good fortune is welcome in the risk business, but it takes time to sort it from intended results. And while uneven, pricing for risk continues to improve and was generally better across the industry in 2012 than in 2011.
Though, regardless of the trends, by the end of 2012, overall it was clear that our own actions were part of the larger XL story.
How do we keep it going? We believe that through our efforts, we have generated four winds at our back that will continue to be earned out in expanding margins and quality growth. We also have a fifth wind coming through the aforementioned pricing environment. But, we will emphasize over and over, none of our progress continues if we dont relentlessly execute. And execute. And execute some more. This is a business for grinders and we are busy grinding away.
So, what are these four self-generated winds at our back?
1. Our re-underwriting continues to take hold and isnt done working its way into our results.
These efforts had uneven start dates across our businesses and took different lengths of time to implement depending on such factors as the nature of the fix, the renewal pattern of the block of business, market receptivity and the analysis of the efficacy of the actions taken. These will take some time to earn out. And while there is always volatility in higher severity lines like ours, by the last half of 2012, the results of this re-underwriting clearly started to show up. We know we are on the right track. We are confident that this re-underwriting expands our margins going forward. And we know we have more opportunities to improve.

2. We are continually refined by new leaders.
The leadership that we have either recruited or promoted from within XL continues to
make their imprint on the company. This improvement in our leadership talent gives
us even more insight to drive better performance whether through new products,
additional underwriting actions or strategic growth.
Just a few of the recent names in this group: Matt OMalley, North America
Environmental; Stephen Ashwell, Crisis Management; Marcus Gonzales, International
Casualty Reinsurance; Ken Riegler, Primary Casualty; Richard DeSimone, North America
Marine; Lorraine Seib, Excess Casualty; Richard Maxwell, Political Risk and Trade Credit.
And what gets us really excited is that these leaders are only now hitting their stride.
These folks have been leading their business  as a collection of discrete businesses
within the way we recently started managing the Group  for only a couple of years at
most. We think that they are just getting to the best part.
3. We are still remixing our book and better allocating
our capital.
Through the selective addition of new teams and lines, we have for some time been
remixing XLs book of business toward lower loss ratio lines and lines of business that
are more effective given the historically low investment yields available. This gives us
better choices for allocating our capital  more capital behind those businesses where
the margins are improving or viewed as likely to improve. Our book still has a severity tilt
to it but we are getting toward a broader portfolio overall and one that we think gets us a
greater likelihood of a lower overall loss ratio.
And, again, the impact of these activities will be increasing throughout 2013 (and
beyond) as these new lines become a greater part of the firm and as the capital allocation
continues its impact.

4. Our strategic investments are now increasing operating leverage.
For some time, we have been investing in XLs systems, processes and technology. For some time, we have been carrying a higher expense load. Now, were beginning to see the benefits of those investments in terms of operating leverage and by the end of 2012 we delivered growth without bearing the same kinds of expense margins that we had seen before.
A few critical steps come to mind: Our global underwriting platform is now up, supporting businesses in North America and will continue to be rolled out to the company throughout the year. The Reinsurance segment introduced XCat, its new portfolio optimization and pricing tool for its Bermuda Property Catastrophe portfolio. We have now implemented 11 pricing tools in our XLAPS framework and are utilizing multiple new predictive models to better price and select risks. And our global claims system, which was fully introduced to all of our world-wide offices this year, has earned numerous awards and recognition and makes working together all the easier for our clients and brokers, creating deeper relationships.
We are not done investing but we can clearly begin to enjoy the benefit of our work. These systems are crucial for operating margin expansion and the ability to flexibly add new lines of business while creating the pipeline for incorporating new analytics.
And then the fifth wind, one being felt across the sector: better pricing.
For the first time in many years, some lines of business were experiencing the compounding effects of rate added to prior year rate gains. And other areas, where rate has been hard to come by, finally showed some overdue progress by the end of 2012  our International P&C business comes to mind. These trends are welcome. But while we are in a much better position from a pricing perspective today, we remain convinced that further rate is needed broadly, particularly on long tail lines of business.
On a less positive note, as we enter 2013 we are especially aware that reinsurance pricing trends have not been as strong as on the primary side, and we will retain our pricing discipline regardless of the effect on the top line

Now? Execute. Execute. Execute.
2013 should be the first time since this leadership team assembled that all five of these
winds will be helping at the same time. But lets be clear: none of the benefits of the huge
effort expended to get in this position come to fruition if we go on vacation. We must
maintain the urgency that was hard won by our stumbles.
Our goal remains to get back to a double digit ROE. To be clear, driving a better ROE is
best achieved by expanding our core operating margins, which all of the previous factors
Ive discussed enable. No other action makes a comparable difference. This grinding
work will not stop. And as we get our operating earnings moving in the right direction,
the goal on which we will focus more and more is the ROE we produce.
One other important thought. As a strategy is put in place and as all the different projects
are unleashed to try and make that strategy take hold, theres often a lot of confusion and
a lot of doubt. There are often higher expenses that cloud whether or not you are getting
the job done. And not everything you try works. It is an inherently messy process.
But one thing has shown through all of this effort: our colleagues deeply believe in
this firm and its purpose. We are bound together by a common belief that what we are
doing is important, relevant work. In a rapidly changing world of risk, XLs tradition of
underwriting-centric innovation and disciplined risk-taking is clearly going to be more
relevant to our clients, and thereby to the world at large.
And this makes it especially gratifying to be able to see the bottom line impact of our
strategy taking hold. We feel like this is just the beginning of something great.
From here, then, it is in essence more of the same: A relentless grind towards executing
on high underwriting standards, of innovating our products and processes while driving
our few remaining strategic projects towards their completion and the realization of their
benefits.
XL is not yet the company we dream of. But, one year later, XL is a company in which our
colleagues, clients, partners and investors can have confidence is well positioned.
Best,
Mike