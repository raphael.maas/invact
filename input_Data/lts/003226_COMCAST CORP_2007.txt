

Dear Fellow Shareholders,
Over the last few years we have
successfully transformed Comcast
from a classic cable television
company into an innovative and
diversified multiproduct entertainment
and communications provider. Our
strategy is to utilize one fiber-rich
network to deliver a growing number
of best-in-class services to millions
of customers.





Our 2007 results reflect that transformation. We reported double-digit growth in revenue
and operating cash flow (OCF)(a), generated significant free cash flow (FCF)(b) and added
more new product subscriptions than in any previous year. Despite this solid operating and
financial performance, our results did not quite meet our expectations -- or yours -- and our
stock price declined 35% after a banner year in 2006.

We are aggressively responding to the challenges we faced in the second half of 2007,
including intensifying competition and a weakening economy. In addition, we have
refined our capital allocation strategy and significantly accelerated our return of capital to
shareholders. We are confident that we are taking the right steps to maximize the growth
potential of our business and to improve shareholder returns, in the near term and over time.

In order to accomplish this, our focus is on growing the business profitably -- including
revenue, operating income and free cash flow -- while also delivering the best products and
customer experiences. To that end, at the beginning of 2008 we laid out an exciting new
product road map for Comcast's future. This next-generation strategy is based on leveraging
our existing state-of-the-art, fiber-rich network to create new products and services that are
differentiated and future innovations that will keep Comcast ahead.

2007 Financial and Operating Results
Our Chief Operating Officer, Steve Burke, and his team delivered outstanding results once
again in 2007. Consolidated revenue increased 24% to $30.9 billion, operating cash flow
increased 25% to $11.8 billion and operating income increased 21% to $5.6 billion. We
generated free cash flow of $2.3 billion. These strong consolidated results were driven
by our cable business(c). Cable reported organic, double-digit growth in revenue (up 11%
to $29.4 billion) and operating cash flow (up 13% to nearly $12 billion), and significant
growth in new products, or RGUs(d) (six million additions, up 19%). Growth in new products
continued to successfully diversify our revenue. In 2007, 40% of our total revenue came
from services other than our traditional cable television service. And these new services
are also the drivers of growth, representing 64% of our organic revenue(e) growth last year.
Products like Comcast Digital Cable, Comcast High-Speed Internet and Comcast Digital
Voice are far from mature and, together with additional new businesses, should help drive
our growth for many years to come.

As a result, our outlook remains confident and optimistic, even in an environment of intense
competition and economic uncertainty. That is why we utilized our balance sheet strength
to make meaningful commitments to our shareholders. We repurchased another $3.1 billion
of our stock in 2007, expect to repurchase $6.9 billion of our stock by the end of 2009 and
initiated a regular dividend.




Our company has many significant opportunities that give us confidence in our continued
long-term growth and superior financial performance. We believe that executing on
these opportunities will be critical to driving shareholder returns over time. Some of the
highlights are:

Our two-way, fiber-rich network is our core asset and reaches more than 48 million homes
nationwide. It is a flexible and increasingly open IP infrastructure that is highly reliable and
scalable. With our network rebuilds complete, we are deploying capital to maximize the
capabilities of this infrastructure. Ongoing investments to recapture analog capacity, in
switched digital video, node splits, new compression technologies, tru2way (formerly "Open
Cable") and Wideband (or DOCSIS 3.0) are making our network faster and more efficient
and reducing our cost of service delivery. These investments give us the ability to innovate
relentlessly so that our products remain highly differentiated in the market. They also open
the door to a profitable array of new products, services and businesses.

Today, our video, high-speed Internet and digital voice products are all competitive, and
each has an exciting product development road map that will benefit from significant
technology advances in the next few years. These three products are available now to
more than 42 million homes -- far more than any of our competitors. To capitalize on this
advantage, we are entering 2008 with many new initiatives, several of which are already
in the marketplace and starting to show solid results. We're expanding the range of our
product offerings and promotions beyond Triple Play to better meet customer expectations
and to attract new customers. We are also refocusing our marketing efforts to ensure that
we get our message out more powerfully.

Cable television is at the heart of our business. Our video product has been enhanced
greatly in recent years by the success of our On Demand platform and by the ever-
expanding breadth of digital and high-definition (HD) choices we offer. That means nearly
300 HD choices today, growing to over 1,000 HD choices by the end of this year. Next,
we are working on a new architecture that will let us offer more than 6,000 movies On
Demand to our customers, with 3,000 of them in HD, every month. This new architecture
paves the way for our ultimate vision of what On Demand can be, which we call Project
Infinity: our mission to give our customers dramatically more content choices delivered
instantaneously to their TVs. Our investment in On Demand, our commitment to Project
Infinity and our determination to offer more HD choices than anyone give us powerful points
of differentiation. We are striving to deliver the most personalized TV viewing experience
anywhere, today and in the future.





We ended 2007 as the nation's largest provider of residential high-speed Internet services.
Our product holds the premium position in the market because we offer more features,
more unique content and more speed. In the coming year, we will begin to deploy Wideband
capability to millions of homes, with speeds of 50 to 100 Mbps and greater. Wideband
represents a quantum leap in speed for consumers and a whole new platform for innovation
in the years ahead.

Comcast Digital Voice, in only its first three years, has generated terrific results. We are
already the fourth-largest residential phone provider in the country, serving more than four
million customers. This strong momentum and a brand new all-IP network will allow us to
begin to offer new integrated features in the second half of this year. The growth prospects
for our phone business are very exciting, since even with our rapid growth to date we only
penetrate 10% of our markets.

Our newest product opportunity lies in the small and medium-sized business market
(Business Services). During 2007, we put in place a strong foundation to serve the more
than five million small and medium-sized businesses in our service areas. We are just now
beginning a nationwide effort to market our services to potential commercial customers. Our
goal is to build a new, highly profitable, multi-billion-dollar revenue business over the next
several years.

Business services, digital voice and high-speed Internet are all clear examples of how we
have built successful businesses upon our core video offering and fiber-rich infrastructure.
Comcast knows how to innovate and deliver new products and services -- and how to
continue to drive reliable and profitable growth. To learn more about our products and
technology innovations, as well as other great growth opportunities in converged products,
online content (including Fancast) and interactive advertising, please visit other sections of
this website.

Underpinning all our competitive strengths is our ability to listen and respond to our
customers. We have an unwavering commitment to deliver the best customer experience,
end-to-end, from the first phone call through the installation and beyond. In 2007 we
undertook many technical initiatives to improve reliability and quality of service, and we're
starting to see the fruits of these critical efforts. Our customer service -- and the entire
customer experience -- has to reach new levels of excellence, and that will continue to be a
major focus for us in 2008 and beyond.





Capital Allocation and Return of Capital
With regard to capital allocation, we have three clear but interdependent goals related to
capital -- and we must carefully balance these goals as we focus on creating shareholder
value. They are: (1) investing in the operating and strategic needs of our business, (2)
preserving our strong balance sheet and (3) returning capital to our shareholders.

Even as our businesses become increasingly competitive, we will continue to grow and
innovate. Approximately 70% of our capital expenditures are related to growth in new
products and services. We believe that our capital investment delivers strong financial
returns and contributes to our competitive advantage. In the coming year, we will expand
our transparency to investors on capital allocation as we amplify our focus on returns on
investment and our commitment to growth in free cash flow.

As for strategic investments or acquisitions, our focus is squarely on growing our core
businesses and, in so doing, enhancing our competitive advantages. Should opportunities
arise, we will remain highly disciplined in ensuring that all transactions have strong financial
returns and are clearly aligned with our strategy. We have a very long track record of creating
substantial value in our investments and we will continue to evaluate attractive acquisition
and investment opportunities to complement our businesses.

At the end of 2007, we had total debt of $31 billion and a leverage ratio of 2.7x, giving
us a solid credit profile and strong investment-grade ratings. We believe it is prudent to
maintain our balance sheet strength so that we can continue to maximize our competitive
position while ensuring ready access to capital, particularly in this difficult economic and
competitive environment.

We have been, and will remain, aggressive buyers of our stock. Since we started our
buyback program in 2003, we have dedicated more than 100% of our free cash flow
to repurchase $10.5 billion of our stock, reducing our shares outstanding by more than
15%. And we expect to buy back the full $6.9 billion of stock remaining under our existing
authorization by the end of 2009. In addition, we are instituting a meaningful quarterly
dividend. Factoring in the buyback and dividend, our plan equates to over $8.4 billion over
the next two years and represents a total return to shareholders of $16.4 billion from 2005
through 2009. Our ability to return this significant amount of capital to our shareholders
is only possible because of the confidence we have in our business, its opportunities for
growth and its ability to generate meaningful free cash flow.




All of us at Comcast want to thank and bid a fond farewell to our co-CFO, John Alchin. John
has been a great friend and a true gentleman and has served Comcast with distinction for
the past 18 years. My father, Ralph, and I personally want to thank John for helping us build
Comcast over so many years. He will be sorely missed, not just by us but surely by many of
you. John played an integral part in the search that led to our hiring of Michael Angelakis,
our new chief financial officer. Michael brings a fresh and disciplined perspective and we are
thrilled to have him as a part of our team.

Finally, I want to thank each of our 100,000 employees for their hard work and dedication in
2007. We are competing and operating in a new environment -- and our team is playing to
win. We have a refined and focused strategy, great products and many new opportunities to
deliver sustainable growth and build value for many years to come.

It is an honor to help lead this amazing company. Thank you for your continued support.

Sincerely,




Brian L. Roberts
Chairman and Chief Executive Officer
Comcast Corporation
April 2, 2008


