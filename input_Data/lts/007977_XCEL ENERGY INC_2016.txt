Dear Fellow Shareholders:
Xcel Energy delivered excellent results in 2016
 financially, strategically and operationally.
Our performance continues the long tradition
of delivering value for our shareholders and
positions us for continued success in 2017
and beyond.
I am proud to work for this company and with
such dedicated and talented employees. The
phrase Always Delivering is one that rings
true throughout Xcel Energy, encapsulating
the important role we play powering millions
of business and residential customers every
day. It also reflects our deep commitment
to providing outstanding service to our
customers. We deliver safe, clean, reliable
energy at a competitive price. We respond to
our customers needs with new products and
solutions to help manage their energy use. We
quickly restore power when storms strike our
communities and damage the energy grid. And
we leverage technology to create efficiencies
and keep costs in check.
One measure of our success is financial
performance, and we delivered again in 2016,
continuing to provide strong shareholder
value. Xcel Energy delivered 2016 GAAP
and ongoing earnings of $2.21 per share,
compared with GAAP earnings of $1.94
per share, and ongoing earnings of $2.09
per share, in 2015, which marks the 12th
consecutive year we have met or exceeded
our ongoing earnings guidance.
Xcel Energy also increased your dividend 6.3
percent in 2016, marking the 13th consecutive
year of dividend growth. We maintained our
dividend growth guidance in the 5 to 7 percent
range, reflecting the ongoing confidence we
have in our ability to deliver for you.
Total shareholder return is another way we
measure performance; we posted a 17.1
percent return in 2016. Our three-year total
shareholder return is 62 percent, which
compares favorably to the overall utility sector.
With another successful year behind us and
strong momentum in place, we initiated 2017
earnings guidance of $2.25 to $2.35 per share.
As we kept close focus on the companys
financial performance, Xcel Energy united
around a set of key priorities that matter most
to our stakeholders.
Delivering Long-Term Growth
During 2016, we continued to execute our
steel for fuel growth strategy, which locks
in long-term fuel savings for our customers
by building and owning wind farms at a time
when tax credits make this a significant value.
The approach takes advantage of the windrich
resources that are available in our service
territory and provides billions of dollars in fuel
savings, which offset the capital costs to build
the new wind generation and accompanying
transmission to bring renewable energy to
the marketplace.
Steel for fuel offers impressive economic and
environmental benefits that appeal to our
customers and shareholders and strengthens
our position as a low-cost energy provider. It is
a prudent way to reduce our carbon footprint
and transform our energy supply mix from fossil
fuels without raising prices for customers,
while simultaneously providing growth
opportunities for the company.
We made tremendous strides in 2016. Just
15 months after construction began, our
Courtenay Wind Farm in North Dakota became
fully operational. It is a testament to our ability
to successfully develop and construct wind
projects. In Colorado, we gained approval for
the Rush Creek Wind Project, a 600 megawatt
wind farm  one of the largest in the state 
that will break ground this year and will go into
service in 2018.
Xcel Energy entered into a supplier agreement
with Vestas, one of the largest wind turbine
manufacturers in the world. The partnership
ensures we have access to the steel needed
to fulfill our wind commitments and provides
additional tax credits for our customers. We
also announced plans to add 1,550 megawatts
of wind in the Upper Midwest and propose to
own approximately two-thirds of that capacity.
In addition, we are pursuing the potential
to add more than 1,000 megawatts of wind
power in Texas and New Mexico.
Over the next five years we are pursuing
several capital investment projects  including
a significant amount of large-scale renewables
 that would grow our rate base by 5.5
percent. To enhance reliability, we will continue
to invest in the electrical grid and be vigilant in
our efforts to protect it from cyber and physical
threats. An example is our Advanced Grid
Intelligence and Security proposal in Colorado
that will upgrade our communications platform,
improve security and reliability, and leverage
smart meters to provide customers more
choices in how they manage their energy use.
Our investments play a key role in driving
economic development through good jobs,
tax base and lease payments to land owners.
They also contribute growth opportunities for
you, our valued shareholders.
Engaging Stakeholders
We took our stakeholder engagement
efforts to new levels in 2016, resulting in
ground-breaking agreements in Colorado and
Minnesota. The company
is poised to implement one
of the states first multiyear
electric rate plans in
Minnesota and is testing
updated pricing designs
in Colorado. Through an
industry-leading resource plan
approved in Minnesota, Xcel
Energy will more than double its wind and
solar resources while retiring two coal-fueled
units, which would result in a 63 percent
carbon-free energy mix to the region by 2030.
Xcel Energy will launch a new customer
option, Renewable*Connect, in Minnesota
and Colorado to provide up to 100 percent
renewable energy that is certified. And finally,
a wide-reaching agreement was secured with
22 stakeholder groups in Colorado that will
expand the companys rooftop and community
solar offerings and position the company
for ongoing stakeholder collaboration. The
agreement is one of the largest of its kind in
the states history.
Operational Excellence
and Industry Leadership
Fundamental to our business is providing
reliable service for our customers. We
continue to deliver on that promise, meeting
our energy reliability goals and delivering
industry-leading storm response when
customers need us the most. Xcel Energy was
recognized by the Edison Electric Institute
for our emergency response and power
restoration after a massive winter storm
struck communities in Texas and New Mexico
and interrupted service to tens of thousands
of our customers in December of 2015.
The Edison Electric Institute is an industry
association that represents all U.S. investorowned
electric companies, which collectively
serve 220 million Americans.
Our public safety commitment is a
responsibility we take seriously. Our work
is especially visible as we upgrade natural
gas infrastructure and make repairs to our
power grid in the communities we serve.
Perhaps less visible, but just as important, is
our behind-the-scenes work as we employ
multi-faceted efforts to protect the electrical
grid from physical and cyber attacks. In 2016,
I was honored to be appointed to the National
Infrastructure Advisory Council, a group of
government, business and industry leaders
convened to advise the U.S. President and
government agencies on policies and strategies
that help to ensure our nations critical
infrastructure is secure.
Our employees and customers take pride
in Xcel Energys long-standing wind energy
leadership, and that continued in 2016 when
we were named the nations No. 1 utility wind
energy provider for the 12th consecutive year
by the American Wind Energy Association.
This achievement is a component of our efforts
to significantly reduce our carbon footprint by
increasing our large-scale renewable portfolio,
repowering existing facilities with more
carbon-friendly natural gas and maintaining
our nuclear, hydro and biomass operations.
Always Delivering
It is what we do: 24 hours a day, seven days
a week, 365 days a year.
We are proud to power the lives of millions
of people and to give back to the communities
we serve. Our record-setting United Way
campaign raised more than $3 million and
brought thousands of volunteer hours to
nonprofits throughout our service territory.
We are unwavering in our commitment to
partner with stakeholders to build value 
whether it is delivering renewable energy,
expanding customer choices or making it
easier than ever to do business with us.
We know that our business continues to evolve
and are well-positioned to deliver long-term
value regardless of the challenges triggered
by the rapid pace of change.
Once again, we appreciate the trust you place
in Xcel Energy. We dont take it for granted as
we strive to deliver exceptional value for you
today and tomorrow. With your partnership,
our future is indeed very bright.
Sincerely,
Ben Fowke
Chairman, President and
Chief Executive Officer