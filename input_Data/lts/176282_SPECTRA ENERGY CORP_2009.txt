The year 2009 would have been a tough one to fully and accurately predict given the level of economic upheaval
we saw play out. Ten percent unemployment negative economic growth and volatile financing costs affected
virtually every business in every sector  and the customers and communities we serve across North America.
The good news for Spectra Energy investors is that we embarked on 2009 fully aware of the economic challenges
we faced as a company. With a realistic view of the path ahead, we committed to you, our investors, that not
only would we overcome those economic challenges, but we would also position your company to seize the
inevitable opportunities that would arise. One year later, Im pleased to report that we have fulfilled that
commitment  and then some.
We took a number of steps to ensure we remained the company of choice for investors, customers,
communities, government, regulators and our employees. Those steps are detailed throughout this report.
While they demonstrate Spectra Energys ability to address current realities head-on and tackle
whats now, they also underscore our capacity to prepare for and focus on whats next.
Not only did we outperform the stock market during 2009, but we also secured firm
contracts with new and existing customers for expansions in all our business segments
and markets for 2011, 2012 and 2013. This will help ensure that investors are
positioned to benefit from earnings growth and a healthy, secure dividend.
Lets take a look now at how were delivering.
The right resource
As the theme of our annual report  Right Here, Right Now  suggests, natural
gas is positioned to be the 21st-century fuel of choice. Its clean, abundant, domestic,
reliable and efficient. Our industry produces jobs  good jobs  in cities, towns and
communities across the continent at a time when more jobs are sorely needed.
Equally, theres no sustainable solution to our combined economic, energy and
environmental challenges that can be credibly put forth that would not require
a significant and growing role for natural gas. To suggest otherwise risks turning
two of the largest issues we face in coming decades  sustainable economic
growth and energy security  into a political football, which will do a disservice
to this generation and those to follow.
So while were working diligently every day to deliver on our 2010 financial and growth commitments, were
also looking to secure the rightful and necessary position of natural gas in our future economy. Governments
and regulators in both the U.S. and Canada will hear about the fuel of choice from the advisor of choice 
Spectra Energy. This report includes a separate feature devoted to the benefits of natural gas, which I encourage
you to read and share.
The right growth strategy
In 2009, Spectra Energy delivered 10 new expansion projects on time and on budget  projects that will deliver
excellent returns, earnings and cash flow for many years.
Moreover, since our 2007 launch, we have placed into service 42 fee-based expansion projects totaling more than
$3 billion. These projects will contribute annual earnings before interest and taxes of $380 million, for a combined
return from these investments of greater than 12 percent.
The projects were delivering are hitting the mark in many respects: Theyre situated in the heart of high-growth
markets and adjacent to the most productive supply basins in North America. Theyre real projects, contracted
with creditworthy customers who look to Spectra Energy as their supplier of choice. Theyre cost-effective, lowrisk,
bolt-on expansions to our existing assets that we can size and time to meet our customers evolving needs.
This year, well place $900 million of projects into service  again, projects that will deliver returns on capital
employed in excess of 12 percent. Were also securing and executing opportunities that go beyond 2012 and
well into the future. Opportunities like TEAM 2013 our New Jersey-New York expansion additional expansions
in Western Canadas Horn River and Montney regions power conversions in the Southeast U.S. and Ontario
and a slate of excellent projects at DCP Midstream, our field services business jointly owned with ConocoPhillips.
Combined, we expect these new projects and our existing profile to provide annual EPS growth of 8 to 10 percent
on average through 2012. Well use our master limited partnership, Spectra Energy Partners, to take advantage
of attractive, value-adding acquisition opportunities as they present themselves.
The right place
Location is mission-critical to the natural gas infrastructure business, and Spectra Energy occupies prime real estate
across North America. The enviable positioning of our infrastructure assets would be nearly impossible to replicate
today  and our investors benefit from that premier footprint.
Were ideally connected to both conventional supply basins and prolific new unconventional reserves like the
Marcellus, Barnett, Haynesville, Horn River, Montney, Eagle Ford and Cordova Embayment plays  reserves that
are redefining our continental energy outlook. And our respected businesses in both the U.S. and Canada serve
four of the five fastest-growing demand markets in North America.
The right foundation
Spectra Energy is well positioned financially, with an investment-grade balance sheet, strong cash flow, ample
liquidity and excellent access to capital. Those essential strengths comprise a firm foundation that we relied upon
during last years unrelenting series of economic twists and turns.
Early in the year, we took proactive steps to bolster our financial flexibility, and weve been diligent in maintaining
more than adequate liquidity to fund our operations, capital expansion program and dividend.
We know how important a healthy, reliable dividend is to investors, particularly during times of economic uncertainty.
The fact that 80 percent of our earnings come from solid, fee-based businesses independent of either commodity
prices or throughput further supports our ability to deliver on our dividend commitment. We are fortunate to have
within our portfolio DCP Midstream, whose dependable cash flow continues to assist in the financing of our
expansion efforts, as evident in the $100 million of distributions we received from the business in 2009.
The right people
More than 5,000 Spectra Energy employees work on your behalf every day  delivering excellence and results in
every facet of the business, from day-to-day operations to new business development to community involvement
and strategic planning for the long term. This high-performing team is motivated to excel, innovate and lead, and

its members flourish within a culture of inclusion, accountability and opportunity  the culture of a company
committed to being the employer of choice. Youll hear directly from members of our team in the pages that
follow, and I expect youll be as impressed as I am by their contributions to our shared success.
The right values
Our values, which you see listed in our Charter, guide our actions and shape our decisions. While we view our
values as an internal compass, we also realize that living those values becomes apparent to those we serve 
and helps us in our quest to be the partner of choice to our communities.
On the sustainability front, Spectra Energy was once again named to the Dow Jones Sustainability Index, and
we were recognized as the top energy company on the Carbon Disclosure Projects 2009 competitive listing.
We received the U.S. Environmental Protection Agencys Natural Gas STAR Program Partner of the Year Award,
recognizing improved operational efficiency and reduced emissions.
Our focus on respect, diversity and inclusion was recognized by both the Human Rights Campaigns Corporate
Equality Index and the Anti-Defamation League, which for the second year in a row honored Spectra Energy as
a Community of RespectTM .
We also were proud to be ranked the most admired pipeline company on FORTUNE magazines annual list of the
worlds most admired companies.
We did a lot of things right in 2009, but we did not deliver well on the safety front and our aim of achieving a
zero injury and incident culture. Sadly, Spectra Energy and DCP Midstream each suffered the loss of an employee
at work during the year. We honor them by learning from these events, continually sharpening our safety focus
and implementing new policies and procedures to improve in this most vital area.
The right focus  becoming the company of choice
In the fall of 2008, we set a path for 2009 through 2012 that would put us on a trajectory to be the company of
choice and to be leading our sector by 2012 in terms of three key focus areas  safety and reliability, customer
responsiveness, and profitability. It is an ambitious goal and one that we are confident in meeting and being
measured against.
As always, I would like to thank our directors for their counsel and guidance throughout the year, and for the
able chairmanship of Bill Esrey. My thanks to you for your investment and interest in this great company and its
bright future.
I am proudly reminded each day of the commitment to excellence that our employees demand of each other,
the management team and themselves. It is a commitment that you can invest in with confidence. Right here,
right now  Spectra Energy.

Gregory L. Ebel, president and chief executive officer