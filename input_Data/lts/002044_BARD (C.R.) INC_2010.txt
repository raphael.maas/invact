TO OUR SHAREHOLDERS:

In 2010, we continued to achieve solid results in a
challenging environment, generating 7% net sales
growth in constant currency, up from 6% in 2009.
Bard continued to drive revenue growth through
innovation, providing clinicians with differentiated
products to better meet the needs of their patients.



  We reached record levels of organic research and                Our 2010 acquisition of the CROSSERTM CTO Recanaliza-
development (R&D) in 2010. In all, we invested over $180         tion Catheter (see page 8), which uses high-frequency
million in R&D and participated in a record 37 clinical trials   vibration to break through chronic total occlusions
while navigating a regulatory approval process that has          (CTOs), has further broadened our success and reach
become increasingly difficult. We focused on operational         into treatments for vascular disease in the leg. The
excellence, developing an even more innovative and               addition of this device to our portfolio is beginning to
                                                                 drive incremental LIFESTENT vascular stent sales as
diverse portfolio, launching 37 new products and
expanding in emerging markets like China and Brazil.             physicians cross CTOs and then seek a stent solution
    We also achieved record performance in business              to support the reopened artery.
                                                                    Building on the success of our VACORA vacuum-
development, gross margin and sales-force deployment,
all key components of our growth strategy. We delivered          assisted biopsy platform, at the end of 2009 we launched
strong year-over-year adjusted earnings per share growth         the next-generation vacuum-assisted device for ultra-
                                                                 sound-guided procedures. This product, the FINESSE
of 10% while investing in and building for our future.
    While our industry continues to feel the residual effects    ULTRA Breast Biopsy System, is our first single-insertion,
of a global economy that has been slow to pull itself out        multiple-sample device (see page 10).
of a recession, we have taken a long-term view. Our results         Early in the third quarter of 2010, we seized upon an
indicate that we remain on the right strategic path.             opportunity to leverage our existing success in the breast
    Following are some of the 2010 highlights from our           biopsy market and expand into the stereotactic X-ray
four major product categories.                                   segment through the acquisition of SenoRx, Inc. SenoRx
                                                                 was a complementary match that more than doubled the
                                                                 size of our biopsy franchise. In addition to the ENCOR
VASCULAR
Sales of the LIFESTENT vascular stent have more than            vacuum-assisted breast biopsy system, the transaction
                                                                 brought the CONTURA breast brachytherapy catheter, tissue
doubled in the past two years. We continue to enjoy the
benefit of having the only stent indicated for use in the        markers and other related devices into our portfolio.
                                                                    Our line of ULTRACLIP breast tissue markers has shown
superficial femoral artery in the U.S. market. While we
anticipate increased competition in this area in late 2011,      strong growth since we acquired the technology in 2007,
we believe our strong clinical data, including one-year          and we recently augmented the line with the launch of
follow-up results that were published in June and two-           our new VISICLIPTM family of higher-visibility tissue markers.
and three-year follow-up results expected to be published
this year, will help us maintain our leadership position.        UROLOGY
This position will be further enhanced with the planned          With the leading position in the U.S. Foley catheter
launches of new LIFESTENT vascular stent offerings in late      market, Bard felt the impact of reduced hospital patient
2011 and in 2012.                                                volumes in 2010. Nevertheless, our Bardex I.C. Infection
                                                                 Control Foley catheter line, with BACTI-GUARD* silver
    A year ago, Bard entered the specialty percutaneous
                                                                 coating and BARD hydrogel, is clinically proven to reduce
transluminal angioplasty (PTA) market with the
acquisition of the VASCUTRAKTM catheter family. In early         the occurrence of urinary tract infections in catheterized
2010, we entered the small-vessel market with our own            patients. We believe that as hospitals increase their focus
ULTRAVERSE PTA Balloon Dilatation Catheters. The                on preventing hospital-acquired infections to reduce
small-vessel segment continues to be the fastest growth          overall treatment costs, this line is well-positioned to
driver in PTA, and will be further strengthened by               return to strong growth.
                                                                    Our STATLOCK catheter stabilization line saw double-
favorable reimbursement rates for below-the-knee
procedures scheduled to begin in 2011.                           digit growth in 2010 despite similar headwinds.


                                                                                                                               3
                                                                            ONCOLOGY
                                                                            At the heart of Bard's product leadership strategy is
                                                                            our focus on providing products with positive clinical
                                                                            outcomes and reduced procedural costs. Prime examples
                                                                            include our peripherally inserted central catheter (PICC)
                                                                            and implanted port product lines.
                                                                                In PICCs, growth has been driven primarily by
                                                                            upgrades to our SHERLOCK Tip Location System (TLS)
                                                                            and maximum barrier PICC kits. Catheter tip-placement
                                                                            and tip-confirmation technology represents a significant
                                                                            opportunity in this franchise. In November, we acquired
                                                                            the SAPIENSTM electrocardiogram-based catheter tip-
                                                                            confirmation system (TCS), which is used as an alternative
                                                                            to chest X-ray and fluoroscopy for central venous cath-
                                                                            eter tip-placement confirmation in adult patients. Based
WORLD-CLASS QUALITY                                                         on a several-hundred-patient clinical study conducted in
Bard's goal is to do more than just comply with FDA regulations. Instead,
                                                                            Europe, the product received 510(k) concurrence in the
we aim to create a total quality system that stands as the benchmark for
                                                                            United States for guidance and positioning of central
the medical device industry. Our Quality Task Force is one of the largest
                                                                            venous catheters such as PICCs, implantable ports and
and most important collaborative efforts in Bard's history, encompassing
hundreds of employees from a variety of functional areas working in teams
                                                                            hemodialysis catheters. We launched the SAPIENSTM TCS
headed by the leaders pictured above. (L-R): Todd C. Schermerhorn,
                                                                            system in the first quarter of 2011, and late in the year
Senior Vice President and Chief Financial Officer; John H. Weiland,
                                                                            we expect to launch our next-generation system called
President and Chief Operating Officer; Gary D. Dolch, PhD, Senior Vice
President, Quality, Regulatory and Medical Affairs; John A. DeFord, PhD,    SHERLOCK 3CGTM TLS.
Senior Vice President, Science, Technology and Clinical Affairs;
                                                                                In our estimation, the penetration of ports into the
Timothy P. Collins, Group Vice President; Bronwen K. Kelly, Vice
                                                                            eligible U.S. chemotherapy market is less than 60%.
President, Human Resources; Jim C. Beasley, Group Vice President.
                                                                            We believe the best way to capitalize on this potential
                                                                            is through clinical and customer education, and by
    Early in 2010, we launched the AJUST single-incision                   advancing port technology to improve clinical outcomes
sling system in the United States. On the strength of                       and facilitate ease of use. The POWERPORT VUE implanted
its innovative, two-way post-placement adjustability                        port, launched in the third quarter of 2010, includes
and positive clinical data from Europe--where it was                        a silicone-encapsulated body that speeds up and
introduced earlier--it has bolstered our position in the                    simplifies the placement of sutures anywhere around
market for devices that treat stress urinary incontinence                   the base of the port. Since identification of the power
in women.                                                                   injection capability of these devices is critical, the
    Our DIGNICARE Stool Management System (SMS)                            POWERPORT VUE implanted port introduces our new
maintained a strong growth profile in 2010. We                              TRINITYTM CT marker to provide the highest-visibility
received 510(k) concurrence from the U.S. Food and                          marker currently available in the market.
Drug Administration (FDA) in late December for our                              Growth in our vascular-access ultrasound product
next-generation product in this space, the DIGNISHIELDTM                    line was led by our SITE~RITE VISION ultrasound
SMS, which incorporates new material to reduce odor                         system, launched early in the second quarter of 2010.
and an improved collection and containment system                           The SITE~RITE VISION system gives clinicians more tools
to reduce the likelihood of contamination.                                  to optimize and manage ultrasound images, including a


4
color-flow Doppler display and a linear probe with             function since 1996. The Information Technology Solutions
controls that enable common image adjustments from             group is now headed by Patrick D. Roche, who joined Bard
within the sterile field.                                      from PricewaterhouseCoopers LLP.
   We've seen healthy growth in our dialysis access
business, reflecting the success of our recently launched      LOOKING AHEAD TO 2011
EQUISTREAM and POWER-TRIALYSIS catheter lines.               Product leadership remains a strategic priority for Bard.
                                                               Today, 82% of our sales are generated by products that
                                                               are number one or number two in the market. We strive
SURGICAL SPECIALTIES
Bard offers a comprehensive portfolio of natural tissue        to offer clinical solutions with the best value in terms
products in the soft-tissue repair market. Our growth          of outcomes and cost. By increasing our investments in
in this franchise reflects the success of the XENMATRIX       research and development, quality control, and sales-force
implant in the treatment of complex hernias, and the           size and effectiveness, we're turning market challenges
success of our ALLOMAX product line, particularly in breast   into a competitive advantage.
reconstruction (see page 6), a new market segment for             To sustain this advantage, we count upon the commit-
Bard. Whether allograft or xenograft in origin, our implants   ment and dedication of almost 12,000 employees around
leave healthy collagen intact as the scaffold to strengthen    the world. We know we speak for all of them when we
and promote the patient's natural healing. We anticipate       thank you, our shareholders, for your ongoing support
further expansion of the market opportunity for natural        and confidence.
tissue as this technology continues to develop and
demonstrate improved clinical performance.                     Sincerely,
    Bard has long been a leader in synthetic hernia
products. In the third quarter of 2010, we launched the
VENTRALIGHTTM ST mesh for laparoscopic ventral hernia
repairs. With a 50% weight reduction, improved ease of
use and the proven performance of the SEPRAMESH IP
resorbable barrier technology, this product has been
well-received by customers. Our plans for 2011 include
the launch of several new synthetic ventral repair meshes
                                                               Timothy M. Ring
that incorporate the SEPRA resorbable barrier.
                                                               Chairman and Chief Executive Officer
    Sales of the innovative SORBAFIXTM resorbable anchor
fixation device continue to grow at a healthy rate. We're
on schedule to launch an enhanced delivery system for
this and our PERMAFIXTM permanent anchor fixation device
in the first half of 2011, which should continue to drive
growth in fixation products.

MANAGEMENT AND THE BOARD OF DIRECTORS
                                                               John H. Weiland
Bard's experienced Board of Directors remained
                                                               President and Chief Operating Officer
unchanged in 2010, providing the steadfast guidance
and wise counsel to which we have become accustomed.
We bid farewell to Vincent J. Gurnari Jr., who is retiring
after leading Bard's global information technology             February 28, 2011


                                                                                                                        5
