Dear Customers, Employees and Shareholders,


salespeople and customers. For this relationship to flourish, our customers must
believe we are sincere in our desire to make their shopping experience as enjoyable
and rewarding as possible. And our folks on front lines must feel that they are
empowered to not only meet, but exceed our customers expectations. 
Over the past 12 months, we have made significant progress in our goal to regain the
trust and goodwill of these two key groups. As weve increased our focus on the front
lines, we have also reviewed many of our operating procedures and practices to make
sure our time and energy are well spent  all while building upon the core values
that define our culture and differentiate our position in the marketplace.
Ive highlighted some of our more noteworthy accomplishments below.
 Weve worked to clarify the offering in each of our lifestyle departments, making it
easier for customers to find the items that appeal to them, while providing more
balance to our overall merchandise mix.
 Weve improved on getting the right item, at the right time, at the right price in each
of these departments, which is helping to drive volume.
 Weve finished testing and begun implementation of our Perpetual Inventory
system, a vital merchandising tool that will provide us with information to make
smarter decisions throughout the selling process, and better serve our customers.
 Weve streamlined back-of-the-house operations, saving valuable time and effort,
while also helping us achieve significant reductions in our overall costs.
There is no doubt that none of these things would have been possible without the 
focus and dedication of our entire team. Through their efforts, we believe we are
getting back on track regarding what it is that makes Nordstrom unique and special.
But we realize there is still more work to be done. Obviously, these are challenging
times, and consumers have many choices when it comes to spending their hardearned money. At Nordstrom, we need to make sure that we are providing real,
tangible reasons why they might choose to shop with us. We must continue to hone
our   l i stening  ski l l s ,  and maintain a  sense of  urgenc y  when  responding  to  our
customers needs. Im confident were doing just that.
Sincerely,
Blake W. Nordstrom
PRESIDENT