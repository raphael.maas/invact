Dear Fellow Shareholder:
I am proud of the performance of IntercontinentalExchange (ICE) in 2011,
especially in light of the uncertain regulatory and economic environment
in which it was achieved. We again delivered industry-leading results, and
we outperformed our major competitors in terms of growth, profitability
and returns. We are building a company that consistently creates value
for shareholders with innovative, customer-focused risk management
solutions  from central clearing of over-the-counter (OTC) swaps, to
developing one of the industrys highest performance trading platforms
for both futures and OTC markets. This capability to create value for
customers is a competitive advantage and the principal reason we have
produced seven consecutive years of record results  even during
extended periods of economic and regulatory uncertainty.
We know that past successes do not guaranty future results. As Peter
Drucker wrote, success creates new realities, and its own and different
problems. Successful companies, Drucker observed, never stop asking,
What is our business? That remains a central question before my
colleagues and me.
2011 was a challenging year for the exchange industry and the financial
services sector. The implementation of The Dodd-Frank Wall Street
Reform and Consumer Protection Act in the U.S. began to reach critical
mass. The bankruptcy of MF Global shook the confidence of market
participants who previously weathered larger industry shocks. Financial
markets endured dramatic swings as a result of uncertainty regarding
European sovereign debt, the U.S. debt ceiling and the banking crisis.
And a wave of global consolidation among exchange operators largely
dissipated following opposition from policymakers and shareholders. All

of these developments had a major impact on our industry  and will
continue to influence our sector in 2012.
Our business is to drive results
For ICE, 2011 was another year of record financial and operating results.
Earnings grew 28% to a record $510 million on $1.3 billion in revenues,
and operating income increased 22% to a record $793 million. Futures
volume across our exchanges was a record 381 million contracts, up
16% from 2010, and OTC energy volume grew 25% to a record 417
million contracts, with 97% of these contracts cleared. We launched over
260 new OTC energy contracts and more than 30 new futures and
options contracts. Our credit default swaps (CDS) clearing houses in
2011 reached $26 trillion in gross notional cleared, and we added more
than 80 new instruments, including four Latin American sovereigns  the

first cleared sovereign CDS in our industry  during a muted credit
trading environment.
Volume at ICE Futures Europe was a record 269 million contracts in
2011, up 24% from 2010. Average daily volume (ADV) increased 24%
from 2010 to 1.1 million contracts. Volume growth was led by ICE Brent
crude and Gasoil futures and options, up 34% and 26%, respectively, with
continued adoption of Brent as the light, sweet crude oil benchmark, and
Gasoil, as the benchmark for middle distillates.
Volume in both EU emissions and UK natural gas futures and options
reached 14 million contracts for the first time in 2011, increasing 23% and
50%, respectively, as our utilities strategy gained traction. Growth in oil
options volume  up 450% in 2011 to 3.4 million contracts for Brent,
WTI and Gasoil  demonstrates our progress in extending options
capabilities by deploying the new technologies in which we have invested
over recent years.
Volume at ICE Futures U.S. was flat from 2010. With strong price
moves in the first half of the year and a challenging global commodities
trade finance environment rooted in the banking crisis, volume in our
benchmark soft commodities futures and options declined 10% from
2010. Despite generally restrained equity market conditions, volume
in our largest equity derivatives contract, the Russell 2000, grew 10%
to 44 million contracts. Volume in our currencies contracts grew 21%
to a record 9.1 million contracts, as more customers recognized the
unique value proposition of the U.S. Dollar Index. Addressing continued
client demand for our currency products, we introduced more than a
dozen new currency pairs. Volume at ICE Futures Canada, which lists the
global benchmark canola contract, increased 10% to a record 4.8 million
contracts during 2011.
In ICEs OTC energy markets, volume was 417 million contracts, up 25%
from 2010. Amid low volatility and range-bound natural gas prices, our
OTC markets continued to grow as we experienced sustained demand
for new products. Since the launch of ICE Clear Europe in November
2008, we have introduced more than 500 OTC contracts for clearing.
Our business is the evolution of the markets we serve
Commodities are the building blocks of global economic growth, and
we continue to believe that emerging economies will be a source of
demand for commodities and associated risk management practices.
In 2011, nearly half of ICEs revenues came from outside the U.S. A
December 2011 Wall Street Journal story titled As China Goes, So Go
Commodities, reported that China accounts for 60% of the growth in oil
demand, though it accounts for just 11% of global oil consumption, and
that its natural gas imports are projected to increase ten-fold by 2020, to
a level greater than any other country.
ICEs strong performance in 2011  from Brent and Gasoil to the USDX
and canola  largely reflected this developing economy thesis. But we
also know that fundamentals change. In the absence of constantly asking
What is our business? todays global benchmark can be tomorrows
porkbelly contract.
In 2011, we took a number of important steps to ensure that our markets
and risk management capabilities stay ahead of global developments.
In February, we listed U.S. thermal coal futures contracts and in April
we introduced clearing for wet and dry freight swaps. In September,
we introduced the ICE Low Sulphur Gasoil contract in anticipation of
changing long-term hedging requirements in the global distillate market,
particularly continental Europes increased adoption of low-sulphur diesel.
In December, we launched ICE Brent NX, to align the benchmark crude

futures contract with changes in the physical oil markets. And in January
2012, we launched new milling wheat, durum wheat and barley futures
contracts following the end of the Canadian Wheat Boards monopoly
for sales and marketing.
Along with these contract introductions and refinements, we continued
to improve our technology and service offerings. With the addition of a full
range of options-trading capabilities on WebICE, our agricultural options
trading went from 90% open outcry to nearly 50% electronic in a matter
of months. Following Creditexs March expansion of electronic CDS
trade execution, around 95% of index trades are electronically transacted.
The introduction of ICE Links new interdealer workflow enabled sameday
clearing of CDS trades: 85% of trades are cleared in less than a
minute and around half are cleared within two to three seconds. Also
in 2011, we completed a three-year transition from outsourced clearing
technologies to our own software, which integrates a number of critical
components. Implementation of these state-of-the-art clearing systems
increases our ability to introduce new products, serve new markets and
improve capital efficiencies for our customers.
Our business is the capital we invest
As stewards of shareholder resources, our goal is to earn a long-term
return on invested capital in excess of our cost of capital. This objective
guides every decision we make, from new product introductions, to
technology implementation, to direct operating and infrastructure
investments. We succeeded in this in 2011 and raised our return on
invested capital to an industry-leading 19%.
In July, ICE and four Brazilian partners launched BRIX, an electric power
marketplace in Brazil, with ICE providing the trading platform and contract
design. In its first five months, trading volume on BRIX reached one million
MWh on nearly 1,000 trades, and included participants representing over
70 multinational, private and government-controlled companies. Also in
July, ICE acquired a 12.4% stake in Cetip, S.A., Brazils leading operator of
registration and custodial services for securities, fixed-income bonds and
OTC derivatives.
In 2011, ICE increased its borrowing capacity by entering new credit
facilities at very attractive rates, providing future flexibility for M&A,
share repurchases and continued investment in organic growth. We
also repurchased $175 million of common stock at an average price of
$112.94.
Our business is finding opportunity in uncertainty
Our industry continues to face uncertainty in 2012. Many important
Dodd-Frank regulations have yet to be finalized. Financial reform legislation
in Europe, which is being written in 2012, will necessitate further change

in our industry. With the MF Global bankruptcy unresolved, customer
confidence in the futures market model was shaken. And even when
the European debt and banking crises are resolved, other sources of
uncertainty  likely those we cant anticipate  are almost certain to
emerge. Simply put, our job is to simplify business for our customers
 to conquer complexity for those who depend on markets and risk
management services  especially in uncertain times.
Our business is the team and culture we have established
Everyone at ICE either serves a customer directly or supports someone
who does, and we never forget that customers are our ultimate source
of value creation. We also never forget that we are custodians of
shareholder resources  you have entrusted us with your capital, and we
have an obligation to use it prudently. This focus on productivity increases
the rate of return we produce on invested capital and is an important
gauge of value creation.
Our culture remains entrepreneurial. We invest efficiently. We take
calculated risks. We innovate and evolve to reach new markets  and
we take advantage of opportunities that exist amid change.
ICE has established a strong strategic position, industry-leading technology,
a diverse global reach, and an extensive track record of innovation and
results. We are very optimistic about our ability to continue to create
value. But we are not satisfied  because we never stop asking, What
is our business?
Jeffrey C. Sprecher
Chairman and Chief Executive Officer
March 23, 2012