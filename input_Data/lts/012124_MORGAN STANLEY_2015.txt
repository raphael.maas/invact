DEAR FELLOW SHAREHOLDERS,
The close of2014 marked.five years for this management team and is an excellent
opportunity to reflect on where we have been and where we are headed. These five
years have seen significant progress in reshaping Morgan Stanley for the operating and
regulatory environments we live in today. We have put the difficulties of the financial
crisis largely behind us, and completed a multi-year transformation of our business mix
that has led to improved earnings consistency and balance sheet strength.
Rather than take the approach of a typical annual letter to shareholders and simply
recount the preceding year, I would like to use this letter to focus on our strategy for
the next five years. 2014 demonstrated that the strategic decisions we made are
beginning to bear fruit. The next five years will be about enabling this Firm to operate
at its historical best-in-class level through a rigorous focus on clients, culture and talent.
REALIZING THE BENEFITS OF STRATEGIC INITIATIVES
AND THE PATH TO HIGHER RETURNS
Our diversified business model combines a world-class institutional franchise - our
investment banking and institutional sales and trading businesses - with a leading
wealth management organization and a strong and growing investment management
business. This is a balanced set of businesses that complement each other through
business cycles, and serve a wide-ranging set of clients including governments,
institutions and individuals. With a strong culture, an enhanced risk management
framework, and a fortified balance sheet with durable capital levels, we have built a
model that can deliver consistent earnings and higher shareholder returns and is
fully aligned with the new regulatory framework.
2014 demonstrated progress on that path,
and looking forward, we see a number of
opportunities and areas of focus that will
lead to returns in e..xcess of our cost of
capital. As we do at the beginning of each
year, we have laid out specific milestones
for 2015, so that investors can measure
us and hold us accountable as we work
toward sustained and higher returns.
In our Wealth Management business, we
see ongoing upside through additional
margin improvement. In 2014, the fullyear
profit margin increased to 20% from
18% in the prior year. We have targeted
further growth in 2015 and have a clear path to achieve a profit margin of 22% to 25% by
the end of the year. Last year, we reached $2 trillion in client assets and saw a continued
trend toward fee-based managed accounts. This trend plays to the strength of our
advisory-based model and generally translates into deeper client relationships and even
more stable revenues.
Over the longer term, we will benefit from our focus on high net worth and ultra high net
worth clients who have the broadest and most complex financial planning and investing
requirements - consistent with Morgan Stanley's strengths.
Another key driver of higher returns will be the continued execution ofour US bank
strategy in both our Wealth Management and Institutional Securities businesses.
Combined across both of these businesses, we are targeting US bank assets of up to
$180 billion by the end of2016 from $151 billion at the end of 2014, driven by the
deployment of our growing deposit base.
W INNING IN THE M ARKETPLACE
20' HG IGH S
NO., In Barron's Top 100 Financial
Advisors, with 31 Ustings
In managed accounts, with
$783 biWon in assets'
In Equity Sales and Trading revenue
wallet share>
Underwriter of global initial
pubUc offerings'
NO. 2 Advisor on global announced and
completed mergers and acquisitions'
Underwriter of global equity offerings'
NO. 4 Underwriter of US dollar investmentgrade
debt offerings>
In Institutional Securities, the deposit base
enables us to support clients through expanded
corporate lending via relationship and event loans.
Within Wealth Management, we have a significant
opportunity to continue balanced loan growth
given our sizable, underpenetrated client base.
With no bricks and mortar, an embedded client
base and the required infrastructure investments
behind us, the incremental margin on lending
products is particularly high.
Furthermore, we are developing a broader
suite of cash management solutions, which
will help us attract more of our clients'
deposits. Given the magnitude of the client
base, even a modest increase in our share of
client deposits holds significant upside and
positions us well to fund continued, prudent
loan growth. These opportunities from our US bank strategy should yield accelerating
results over the next several years.
Our world-class Investment Banking franchise consistently ranks in the top three of
the global league tables in advising on mergers and acquisitions and underwriting initial
public offerings, and 2014 was no exception. Equity Sales & Trading had the highest
revenue wallet share globally this year and offers our clients expertise across a broad
range of products in markets all over the world. Our leadership and footprint position us
well to benefit from a strengthening economy and to increase our market shares.
Over the past few years, we have
fundamentally resized and reshaped
our Fixed Income and Commodities
Sales and Trading business recognizing
regulatory changes that have impacted
the industry structurally. At the same time,
cyclical challenges continued to impact
performance across the financial sector
this past year. The question we needed to
answer was: How do we size a fixed income
business to serve our clients while using
capital efficiently and profitably? To that
end, we are continuing to implement a three-fold plan to drive returns to greater than our
cost of equity in this business, while offering a suite of products and services to our clients.
The first part is to optimize the commodities business by reducing our exposure
to physical oil commodities, and as such, we are committed to selling our global oil
merchanting business, having already sold TransMontaigne last year. The second is
to centralize the management of resources across all the fixed income businesses so
that there is strategic resource allocation across the division for expenses, technology,
capital and balance sheet. The third is the continued reduction of risk-weighted assets
(RWAs), and we are on track to achieve our 2015 year-end target ofless than $180 billion
These RW As, excluding lending, have come down nearly 50% from $370 billion in
2011 to $188 billion at the end of 2014. The resulting client-focused business should
become more capital efficient and earn returns in excess of its cost of capital, while
being consistent with the regulatory environment.
In Investment Management we want to take advantage of the long-term growth trend
in professionally managed investable assets. Assets under management and supervision
grew 7% last year, and we are concentrating on growing them further by expanding
product offerings, improving distribution capabilities, and maintaining strong investment
performance, all of which should drive steady inflows from global investors and higher
returns on equity for the business.
Besides growing the top line across all of our businesses, we will also benefit from a lower
expense base. Reflecting the stability of the franchise, in 2014 we made strategic changes
to our deferred compensation structure that include reducing the overhang of prior year
deferrals. This will provide operating leverage in a higher revenue environment and a
reduced liability in a lower revenue environment. We have set explicit compensation
ratio targets across each of our business segments, and have laid out an overall target
expense ratio for the Firm. Further, as the market has recognized the improvements to our
business mix and balance sheet, our bond spreads have contracted dramatically over the
past few years. This reduces our funding costs as we refinance older, more expensive debt,
providing a tailwind for the future.
Last year, we doubled our share repurchase program and dividend. Last month, we
received approval from the Federal Reserve Bank for a $3.1 billion share buyback and
the ability to increase our dividend. We will continue to prudently increase our return
of capital to shareholders over time, subject to regulatory approval, increasing both our
share repurchase program and our
dividend, with greater returns of capital
supported by the increased proportion of
our revenue and earnings coming from
more stable businesses. The key drivers
of increased returns to shareholders over
time will be our increasingly consistent
earnings, our strong capital ratios, and
finally, a strategy that is consistent with
today's regulatory framework.
LOOKING AHEAD
FORTIFYING CAPITAL AND
ENHANCING LIQUIDITY
2014 HIGHLIGHTS
 Common equity tier 1 capital ratio
advanced (transitional) of 12.6% as of 4Q
2014, with $65 billion in common equity
 $193 billion global liquidity reserve as
of 4Q 2014
The quantum of regulatory change in the industry during the last five years has been
greater than in the preceding fifty. Adjusting to these changes has been enormously time
consuming and resource intensive.
Having adapted our business to this new regulatory framework, we are well positioned
for the future. We are now focused on building market share, increasing earnings and
improving returns. Our businesses are more aligned than ever to better advise and
transact for our clients, and we see a clear path to success in a world where financial
institutions are highly regulated.
With the proper business model in place, Morgan Stanley's success is in the hands of
our people. We have a highly talented workforce, and a strong culture that continually
reinforces our focus on serving our clients and taking the right kind of risk. The strength
of our culture carried us through the difficult periods of the last five years - ensuring
our people stayed focused on delivering the best of the Firm to clients, even as we were
working through a significant reshaping of our businesses. Going forward, a priority for
the Firm will be to continue evidencing our culture in everything we do. We believe a
strong, cohesive culture focused on putting clients first and rigorous evaluation of risk
is a critical component of ongoing success of the business. Taking risk is a core element
of our business. We take risk when we make markets, act as intermediaries, advise on
capital raisings and manage money. Our culture must complement and reinforce our
systems for ensuring that the risks we take are sensible, controlled and sized properly.
Our culture drives our employees, and our values inform everything they do:
We have a commitment to putting clients first, leading with exceptional ideas,
doing the right thing and giving back. These values honor both our history and
our aspirations for the future. I am proud of each of our nearly 56,000 global
employees for their hard work and commitment, who live these values every
day as individuals and honor our tradition of excellence.
Thank you for your investment in Morgan Stanley. I am optimistic about our
future and confident in our ability to deliver lasting value to our shareholders.

JAMES P. GORMAN
CHAIRMAN AND CHIEF EXECUTIVE OFFICER
A PRIL 2, 2015