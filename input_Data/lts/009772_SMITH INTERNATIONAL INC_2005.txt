Our industry is in the fourth year of an upcycle. Considering the current business climate and our exposure to
many of the fastest-growing market segments in the oilfield service sector, we expect to continue to generate
some of the highest revenue and earnings growth in our industry.
Our business expansion is influenced not only by our growth-focused product portfolio, but also by our
favorable geographic presence. Approximately two-thirds of our oilfield revenues are generated in areas outside
of the United States, a market that, historically, has tended to be more susceptible to short-term swings in
commodity prices. Therefore, our business mix provides a certain degree of downside protection in a volatile
energy-price environment. More importantly, as more drilling activity shifts into the Eastern Hemisphere markets,
we have the people and the resources in place to capitalize on this growth. We have also been successful
expanding our product mix, either through internal development or acquisitions, to respond to changing industry
needs. We believe this creates sizable growth opportunities for Smith. For example, weve expanded our business
portfolio to provide products and services needed to optimize production in mature fields, or brownfield
development, and to comply with ever-tightening environmental requirements. We believe demand for these
product initiatives will expand at rates in excess of the rig count and could serve to accelerate our growth in the
years ahead.
Growth-Promoting Market Fundamentals
Globally, the industry is experiencing significant expansion in drilling activity. With supply and demand factors
tighter than we have faced in a number of years, commodity prices are being sustained at higher levels. We are
also seeing record energy demand, driven primarily by global economic expansion and industrial development
in Asia and other emerging markets. Furthermore, while worldwide energy consumption is at an all-time high,
the supply overhang is diminishing at an
accelerated rate, impacted by both high depletion
rates of established oil and gas provinces and
decades of underinvestment by exploration and
production companies. This has created a market
climate that is prompting our customers to find,
develop and produce assets in challenging markets
around the world  and Smith is well-positioned
to assist in these efforts.
Exposure To Fast-Growing Product Markets
Mature fields create substantial growth
opportunities for Smith. According to industry
analysts, there hasnt been a significant
hydrocarbon discovery in the last two decades
with production potential in excess of one million barrels of oil per day. In fact, the majority of todays
production is from existing, mature fields that are in decline. In order to increase production in these fields,
operators need better re-entry methods, more efficient production chemicals
and advanced drilling technologies. This is exactly what we provide to our
global customer base and part of the reason we have been able to grow at
an accelerated rate.
Another expansion opportunity for us is environmental services, which
is driving increased demand for our unique portfolio of drilling waste
management technologies and solutions. Our solids-control and waste
management business has been building for more than ten years, and today
we offer the broadest array of environmental equipment and service
offerings in the market  enabling a custom-designed waste management
solution to meet specific customer needs. For the past four years, this drilling waste management business
has achieved a compound annual growth rate of nearly 40 percent and, with the recent acquisition of Epcon
Offshores produced water treatment technology, we see even more potential going forward.
The Year In Review By The Numbers
In 2005, Smith reported record revenues and earnings driven by increased global activity levels and an improved
operating environment, which resulted in favorable product pricing. Our total revenues increased 26 percent
over the prior year to $5.6 billion, significantly outpacing the 15 percent
improvement in global drilling activity. While higher North American
natural gas-directed drilling contributed a significant portion of the increase,
we also achieved strong growth in markets outside of North America,
particularly in the Middle East and the North Sea regions.
Higher revenue volumes drove record earnings and cash flows in 2005. Last
year, Smiths net income increased to $302.3 million, or $1.48 per diluted
share  roughly 50 percent above the operating earnings reported in the
prior year. Oilfield segment margins accelerated strongly in the last half of
2005, reaching 16.9 percent in the fourth quarter, evidencing increased
demand for our technology-based product and service offerings.
Throughout fiscal 2005, we also witnessed improved margins in our distribution segment as Wilson was
positively impacted by new contract awards and increased North American activity levels.
Higher profitability levels combined with limited capital investment drove a five percentage point improvement
in our return on stockholders equity  to 20.6 percent. These financial results helped place Smith on the
FORBES Platinum 400 List for the seventh time, ranking us among the best-performing companies in the
energy sector.
Focused on Optimizing Our Capital Mix and Shareholder Returns
We focus on growing revenue and profitability levels and remain committed to maintaining a strong balance
sheet and an optimal capital mix. With limited working capital investment, we were able to internally generate
the funds necessary to finance acquisitions and capital spending needs. We added approximately $135 million
of net debt last year, yet every dollar of this increase was distributed to our shareholders  either in the form of
dividends or share repurchases  in an attempt to improve our capital mix. During 2005, we dispersed more than
$150 million of cash to investors  translating into just over half of our earnings and a significant portion of our
cash flow.
In 2005, we bought back $118 million of our common stock and continue to believe share repurchases are
compelling from an economic standpoint. Reflecting this conviction, our Board of Directors authorized a new
Share Repurchase Program last October which allows for the buyback of up to 20 million shares of Smith stock.
Moreover, fiscal 2005 was the first time in over 20 years that we paid a shareholder dividend. We initiated a sixcent quarterly dividend program in February 2005, and in early 2006 our Board increased the quarterly dividend
to eight-cents per share.
b Return on Stockholders Equity
2003 2004 2005
10.8%
15.3%
20.6%
(in percent)
b 2004 percentage excludes the impact of a litigation-related charge.
c Peer Group consists of the seven largest publicly-traded oilfield service companies.
Maintaining Our Momentum
We continue to seek out and retain qualified people, local talent and industry experts to help grow our business.
As with any company, it is critical for us to have succession plans in place so we can maintain our momentum
when key individuals retire or leave the company. During
the past year, weve made several important management
appointments in the organization. Last April, Mike Pearce
was promoted to President of Smith Technologies taking
over for Roger Brown who retired after ten years of service
to the Company. And, in September 2005, Peter Pintar joined
Smith as Vice President, Corporate Strategy and Business
Development, to help in our merger and acquisition efforts
and identify strategic opportunities for growth  both organic
and inorganic. At the beginning of 2006, Bryan Dudman was
elected President of Smith Services to fill an opening created
by the retirement of Richard Werner at the end of last year.
And, more recently, we announced the appointment of Don McKenzie as President and Chief Executive Officer
of M-I SWACO which becomes effective upon the retirement of Loren Carroll in late April. We are confident
that these additions to our leadership will greatly assist the Company as we strive to continue our growth trend
in the years to come.
Dependability At Our Core
We hold fast to our value proposition, and our customers and shareholders can depend on us to do just that.
Customers can depend on Smith to provide leading edge product development that delivers significant value
with higher productivity, greater savings and increased safety and environmental compliance. They can also
depend on Smith to maintain a tight focus on our core product offerings and continue to do what we do best.
In closing, wed like to extend our sincere appreciation to Mr. Ben Bailar, a member of Smiths Board of
Directors, who will retire at this years Annual Meeting. Over his 13-year tenure, Ben has been an important
contributor to Smiths success  providing immeasurable support and wise counsel. He will be greatly missed.
In preparation for Bens retirement, we welcomed Mr. Robert Kelley to the Smith Board in July 2005.
We consider ourselves fortunate to attract an individual of Bobs skills and experience and look forward to
a long association with him.
5
Five-Year Comparative Total Return
2000 $0
$75
$150
$225
Smith
$201
Peer Group
$149
S&P 500
$103
2001 2002 2003 2004 2005
(assuming $100 investment on December 31, 2000)
c
Doug Rock Loren K. Carroll Margaret K. Dorman