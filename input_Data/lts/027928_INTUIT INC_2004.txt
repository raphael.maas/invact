
TO OUR
STOCKHOLDERS
Intuit delivered solid results in fiscal 2004, setting new records for revenue and pro forma* earnings per share. Equally important, were excited about our future prospects as we expand our offerings beyond the very successful one-size-fits-all approach weve had in the past. In fiscal 2005 and beyond, were taking a broader view of our markets and opportunities  executing Right for Me strategies across most of our businesses to deliver new offerings for customers and new growth for Intuit.
FISCAL 2004 FINANCIAL HIGHLIGHTS
- Revenue of $1.87 billion increased 13 percent from fiscal 2003, driven by strong growth in Intuits QuickBooks-Related and Consumer Tax segments.
- Pro forma operating income of $477 million increased 19 percent from fiscal 2003, driven by unit growth, mix
improvements and to a lesser extent price. Pro forma EPS of $1.67 per diluted share was up 20 percent year-over-year.
- GAAP net income of $317 million was down 8 percent, and GAAP EPS of $1.58 per diluted share decreased 3 percent year-over-year. Last years GAAP results included a one-time benefit from the sale of Intuits Japan subsidiary, which contributed $71 million, or $0.34 per share, to results.
- Intuit continued to improve profit margins substantially in 2004. The companys pro forma operating margin of
25.5 percent was up 130 basis points from fiscal 2003.
- Intuit spent nearly $610 million in fiscal 2004 to repurchase approximately 13.5 million shares of common stock
under its stock repurchase programs. Since we introduced our first repurchase program in May 2001, weve spent
approximately $1.7 billion to repurchase approximately 39 million shares. Intuits board has authorized a fourth
repurchase program for another $500 million as we remain committed to returning value to shareholders through
stock buybacks.
- Intuit had approximately $1.0 billion in cash and short-term investments, or approximately $5.36 per share, at the
end of fiscal 2004.
BUSINESS SEGMENT HIGHLIGHTS
Intuit is rigorous about managing its business portfolio, concentrating on products and services that meet the needs of small- and mid-size businesses, taxpayers and accountants. Within those three areas, we focus on businesses where we have a durable competitive advantage, where there are large, under-served opportunities and where we are or are on a path to beprofitable. In any given year, some of Intuits individual businesses do better than projected and some may do
less. The strength of our total portfolio of businesses enables us to more consistently deliver on our overall expectations for the year.

For example, in fiscal 2004, the QuickBooks-Related and Consumer Tax segmentsour two largest business segments had very solid performance. Intuit-Branded Small Business also had good growth, while Professional Tax grew, but not as strongly as we originally anticipated. Other Businesses performed within its growth guidance range. With that mix, we delivered within the range of total company revenue guidance for the year. Results from our business segments were:
- QuickBooks-Related revenue grew 18 percent over fiscal 2003 to $653.9 million.
- Intuit-Branded Small Business revenue increased 14 percent over fiscal 2003 to $272.4 million.
- Consumer Tax revenue grew 16 percent over the prior-year period to $490.0 million.
- Professional Tax revenue increased 3 percent over fiscal 2003 to $251.9 million.
- Revenue from Other Businesses, which includes Quicken and Canada, was up 4 percent year-over-year to $199.5 million.
A PROVEN GROWTH RECIPE
Intuit has accomplished a lot in recent years, producing an enviable track record of revenue and profit growth. In
fiscal 1999, Intuit had revenue of $801 million. In fiscal 2004, we had revenue of $1.87 billionan average annual
revenue growth rate of 19 percent. Pro forma diluted EPS was 45 cents a share in fiscal 1999. In fiscal 2004, it was $1.67, up an average of 31 percent per year. Intuits stock price is up 37 percent during that five-year period, a sharp contrast to declines in the NASDAQ, Dow Jones Industrial Average and S&P 500.
The secret to Intuits successboth over recent years and, I believe, in the futureis consistently executing the
following three-point growth recipe.
Choosing the Right Businesses As mentioned earlier, a key element of our growth recipe is to carefully choose our
businesses. As Intuit solves for revenue and profit growth, we continually review and, when necessary, adjust our
business portfolio. Weve made a number of acquisitions and dispositions in recent years to position us for stronger
performance. Weve announced two changes during the past 12 months:
- In October 2003, we acquired Innovative Merchant Solutions, which provides credit and debit card processing services for small businesses, for $116 million. The acquisition enabled us to expand the capabilities of our existing QuickBooks Merchant Account Service. We had strong performance from the combined merchant account business in fiscal 2004, with revenue up 35 percent over fiscal 2003.
- In August 2004, we announced our decision to sell Intuit Public Sector Solutions (IPSS), which provides software
for the government and nonprofit sector. IPSS had approximately $13 million in revenue in fiscal 2004, about flat
year-over-year and its third year of relatively flat growth.
Broadening Our Mind-set and Offerings 
Over the past 20 years, Intuit has done an outstanding job of penetrating the markets weve defined and served. To continue our growth and leadership, a key element of our strategy is to broaden our definition of the markets we serve. This will enable us to uncover additional customer problems we can solve. Well then solve those problems as weve always donecreating solutions so profound that people wont want to go back to the old way of doing things.
To do this, we must take a fresh look at what were really competing against. Many might think our QuickBooks and TurboTax offerings are competing primarily against other software providers. While thats partly true, the reality is that were competing against something much larger that offers much more opportunity for growthnon-consumption and alternative ways of doing things. For example, in small business, some of our biggest opportunities are to meet the needs of people who use basic spreadsheets or even paper and pencil to keep their books instead of QuickBooks. In tax, we see similar opportunities to serve people who manually prepare their returns or go to a franchise tax preparer rather than using TurboTax.


Were targeting new offerings for these unserved segments, some of which youll begin to see in fiscal 2005. One new product were particularly excited about is QuickBooks Simple Start, which is designed as an affordable tool that radically simplifies life for small business owners who are just getting started and would ordinarily keep their books manually.
At the same time, were innovating upwardadding new functions to meet more demanding customer needs and then pricing accordingly for those solutions.
This new mind-set and strategy is vastly different than the one-size-fits-all approach weve used in the past in QuickBooks and TurboTax and the two-sizes-fit-all product strategy we had in Pro Tax.
Applying Operational Rigor The third element of our strategy is to apply operational rigor and process excellence
to drive both better customer experiences as well as operating margin leverage. This is a critical element in running a high-performance organization and has enabled Intuit to improve its pro forma operating margin to 25.5 percent in
fiscal 2004.
EXECUTING RIGHT FOR ME STRATEGIES TO DRIVE FUTURE GROWTH
Im confident in Intuits future because the Right for Me approach were taking to all our businesses has already delivered strong results. Four years ago, many people thought our small business portfolio had reached a dead end. But we saw
opportunity. We challenged ourselves to think beyond a one-size-fits-all accounting product, and as a result, weve
uncovered a lot of additional customer needs that we could meet.
Since we launched our QuickBooks Right for Me strategy in December 2001, weve generated $288 million in cumulative revenue from new higher-end QuickBooks offerings and new offerings that attach to QuickBooks. A number of these
businesses, like Merchant Account Services and QuickBooks Point of Sale, have had great early success. Theyre growing nicely and were excited about their potential to have a longer-term impact on our growth rate as they become larger
in scale.
Im proud of the strong growth Intuit has delivered over the past five years. And while we expect revenue growth to
be in the 6 percent to 9 percent range for fiscal 2005, I believe were on our way back to double-digit revenue growth in the future.
In my mind, the question isnt whether Intuit is headed toward stronger growth, but when it will occur. We have great
assets. We have lots of opportunities for new growth across our small business and tax businesses. And we have a proven track record of executing our growth recipe.
Thanks to the talent and hard work of all Intuit employees, weve accomplished a lot in recent years. Were pleased with the progress weve made, but theres a lot more we can do. We continue to believe in a great future for Intuit. And we believe the best is yet to come.
Steve Bennett
President and Chief Executive Officer