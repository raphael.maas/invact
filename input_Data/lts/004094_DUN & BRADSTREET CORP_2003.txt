
To Our Shareholders
Fellow Shareholders,

We are pleased to report to you that the D&B team continues to make good progress in our
journey to transform D&B into a growth company.

As an indication of our continuing progress, in 2003, we:
    delivered core revenue growth before the effect of foreign exchange of 4%, at the upper end of


    our most recent guidance of 1-4%;
    delivered earnings per share (EPS) growth of 18%2 before non-core gains and charges, at the
    upper end of our guidance range of 16-18%; and
    generated 76% of our revenue over the Web by year-end, exceeding our goal of 70%.

In short, the D&B team once again met our commitments to our shareholders as we continued
on our journey. We feel good about our accomplishments. So do our customers and our team
members: both customer and employee satisfaction levels improved in 2003. These
improvements in customer and team member satisfaction, coupled with our consistent financial
performance and our record of meeting our commitments to our shareholders, make us, we
believe, a good company.

Being a good company is important, but it is not enough: we want to be a great company. We see
no reason to be satisfied with simply being good when we have the potential of being great. We
believe we can be a great company, and we intend to be.

Great can have a wide variety of meanings. Here is what great means to us at D&B. First, in terms
of revenue: it means 7-9% revenue growth, year after year, by 2007. Second, it means that our
customers recognize that we're focused on their success. Third, it means that everyone at D&B







personally believes that we are best-in-class at     One of the ways that we continue to leverage our
what we do, and acts like it day-in and day-out.     Brand is by helping our customers to understand
Finally, it means that we are continuing to          more fully the value of our proprietary
create shareholder value.                            DUNSRight TM quality process. DUNSRight TM
                                                     enables us to collect, aggregate, edit, and verify
                                                     data from thousands of sources daily. The output
Our "New" Aspiration
In order to generate shareholder value and a         of the DUNSRight TM process is insightful
consistent 7-9% level of revenue growth, we          information that our customers use to make
know that we need to be even more intensely          better, more informed business decisions (please
focused on our customers' success. We've             see "The DUNSRightTM Process" on p. 7).
always had a customer focus: in fact, "place
the interests of our customers first" is one of      We invested over a quarter of a billion dollars in
our core values. But now we are taking that          DUNSRightTM during 2003 to bring our customers
focus to a higher level, and our intensified         a level of insightful information that they can
focus on our customers' success is reflected in      find nowhere else. These investments in
our "new" aspiration:                                DUNSRightTM enable us to continually improve
                                                     the quality of the information we bring to our
                                                     customers, enhance the value propositions
       "To be the most trusted source of
                                                     we offer, and differentiate D&B from our
     business insight so our customers can
                                                     competitors. Our team members are
           decide with confidence."
                                                     communicating the power of DUNSRightTM to our
(please see more on our aspiration on p. 1)          customers every day so that our customers can
                                                     better understand how D&B can help them
Our intention to become a growth company has         grow their businesses and be more profitable. As
not changed. Our new aspiration continues our        a result, we believe that DUNSRightTM is gaining
emphasis on growth: it gives us direction about      traction in the marketplace, and generating an
the best way for us to achieve year over year        increasing number of competitive wins.
growth and create the most value for our
shareholders. Our Blueprint for Growth strategy      We are able to make strategic investments in
remains the roadmap for our journey, and three       DUNSRight TM and other growth initiatives
of our Blueprint components provide us with          because of Financial Flexibility. We view our
powerful competitive advantages that will help       financially flexible business model as the
us achieve our aspiration: our Brand, our Flexible   engine that fuels our growth. It enables us to
Business Model, and our Winning Culture.             view almost every dollar we spend as flexible.
                                                     This means that we consider very little of our
                                                     costs as fixed, and we continually evaluate
Our Competitive Advantages
Our Brand remains at the center of our               every investment to assure we are spending our
Blueprint, and at the core of our growth strategy.   money on the most impactful activities. We






            constantly seek to improve our performance in                         Our Customer Solution Sets
            terms of quality and cost and reallocate our                          Our competitive advantages drive revenue
            spending to maximize shareholder value. This                          generation in our Current Business and in
            attitude reflects an ongoing mindset at D&B,                          our E-Business customer solution sets. These
            not a one-time effort or an activity that takes                      
            place only in the budget planning process.                            DUNRight,TM help our customers to grow their
                                                                                  businesses and become more profitable. In
            Through our ongoing Financial Flexibility                             2003, each of our solution sets contributed to
            initiatives, over the past three years we have                        D&B's revenue results.
            reduced our expense base by approximately
            $70-80 million per year on an annualized                              Risk Management Solutions, our largest
            basis (before any reallocation of spending).                          solution set, help our customers to increase
            We expect our initiatives in 2004 to generate                         profitability and cash flow while managing
            an additional $70-80 million in annualized                            overall risk exposure. Our investments helped
                  us achieve 2% revenue growth in this area in
            savings in areas that will spur our long term                         2003, mitigating historical declines in our
            revenue growth, we return some of these                               traditional solutions, and driving 10% growth in
                     our value-added Risk Management Solutions.
            enhancing EPS.                                                        Our value-added solutions support automated
                                                                                  decision-making and portfolio management
            We also continue to invest in our team                                through the use of scoring and integrated
            members' leadership development as part of                            software solutions.
            building a Winning Culture. The foundation
            of our Winning Culture initiative is our                              In 2003, we targeted the expanding small
            Leadership Model, which focuses on creating                           business credit market more directly with our
            a team of great leaders. We emphasize                                 DUNSRightTM powered integrated solutions. Our
            leadership improvement for one reason: we                             Small Business Risk Insight (SBRI) initiative, for
            believe that superb leadership leads to superb                        example, speaks directly to customers operating
            execution which leads to superb results for                           in this market. SBRI is our data repository that
            our shareholders. Our passion for improving                           collects and consolidates lending performance
            our leadership and focusing on our customers'                         data from financial institutions on small
            success provides us with a competitive                                businesses. Members leverage D&B's powerful
            advantage in the marketplace. Over the past                           predictive tools to make more informed small
            three years, our team members' improved                               business financial credit decisions. During 2003,
            leadership has made D&B a good company:                               the number of financial institutions becoming
            we believe that our team members' ongoing                             members of SBRI more than doubled to 90, and
            commitment to becoming better leaders will                            the repository grew to include more than $60
            help us make D&B a great company.                                     billion in total outstanding transactions.




Sales & Marketing Solutions help our                    E-Business Solutions provide our customers
customers to obtain profitable new customers,           convenient access to our products over the Web.
and to generate inc reased revenue from                 In March of 2003, we added to our Web
existing customers. In the first quarter of 2003,       capability through our acquisition of Hoover's.
we faced a slowdown in sales of our traditional         Hoover's helps our customers drive revenue
products which we believe reflected                     growth by converting "prospects" to "customers"
customers' reluctance to conduct marketing              more quickly and at a lower total cost. Hoover's
campaigns in the then current market                    provides in-depth industry and market
environment. We recognized the challenge in             intelligence on more than 40,000 companies on
traditional product sales and addressed it              a subscription basis; primarily to sales,
through our investments and better                      marketing, and business development
leadership. As a result, we saw continued               professionals. Since the acquisition, our
improvement quarter by quarter during 2003.             investments have enabled us to more than
We also invested in our value-added Sales &             double the number of companies in Hoover's
Marketing Solutions, which generally include            database. Hoover's has become a significant
decision-making and customer information                contributor to growth, with revenue that
management solutions. Our value-added                   exceeded our expectations.
solutions grew by 18% and helped us achieve
1% revenue growth for Sales & Marketing                 Our focus on leveraging our Brand through
Solutions in 2003.                                      DUNSRight,TM our ongoing investments made
                                                        possible through Financial Flexibility, and
Supply Management Solutions enable our                  our team members' leadership provided us
customers to manage their suppliers more                with important competitive advantages in
effectively to generate savings. Our solutions          2003, and these competitive advantages
help our customers to identify 100% of what             drove the per formance results outlined
they spend throughout their entire                      above.
organizations worldwide. Using this insightful
information, customers can mitigate supply              Looking Ahead
chain risk and manage their spending and                To progress further toward consistent 7-9%
purchasing compliance more efficiently. They            revenue growth year-over-year by 2007, we
can also negotiate more favorable terms with            realize that we will need to intensify our focus
their suppliers based on an informed under-             on our customer-centered aspiration because,
standing of their total purchases not only with         put simply, our customers' success is our
each individual supplier but also with all of           success. To achieve this aspiration, the D&B
the supplier's related companies. In 2003,              team will focus on the following seven strategic
Supply Management Solutions revenue                     priorities which are derived from our Blueprint
grew 16%.                                               strategy:






                1. Build the D&B brand around DUNSRight,                                            Looking ahead, we feel even better about our
                                                              TM


                   the basis of our competitive advantage in                                        future. We are confident that by continuing to
                   the marketplace.                                                                 execute on our Blueprint we will achieve our
                2. Grow our Risk Management Solutions by                                            aspiration, and become a company that
                   3-5% annually.                                                                   consistently grows revenue and continues to
                3. Grow our Sales & Marketing Solutions by                                          create shareholder value  year after year. That
                   4-7% annually.                                                                   is our focus, that is where we are headed as a
                4. Grow our Supply Management Solutions by                                          company  from good to great.
                   25% annually.
                5. Build and/or acquire a 5th product line that
                   leverages DUNSRight TM and creates                                               Sincerely,
                   additional growth opportunities.
                6. Grow our E-Business Solutions by 35-50%
                   annually.
                7. Implement our financially flexible business                                      Allan Z. Loren
                   model.                                                                           Chairman and Chief Executive Officer

           Executing against each of these priorities will,
           we believe, enhance shareholder value, and
           move us forward as we transform D&B into a                                               Steven W. Alesio
           growth company.                                                                          President and Chief Operating Officer

           Looking back, we feel good about our progress
           thus far. We have focused on our Blueprint
           strategy and delivered on our commitments to
           our shareholders: we have made D&B a good
           company.


