FRONTIER COMMUNICATIONS is a dynamic company
with a robust network, quality service and exciting new
opportunities arising from dramatically increased scale.
We are positioned to offer our customers more choices for
great content, products and services. Our employees support
their local communities and are good corporate friends
and neighbors. Together, we are driving long-term value
to our shareholders.
I am proud of the company we have become. Once a rural
telephone company, Frontier is now among the nations top
communications services providers and an industry leader.
We have stayed the course, consistently executing a strategy
focused on our core capabilities and business.
We are committed to keeping and winning residential and
commercial customers through quality service, engaging
content and attractive pricing. Operational excellence
translates into profit, and our focus on fundamentals made
profitability a bright spot in 2015. Frontier maintained
industry-leading margins, with an adjusted operating cash
flow margin of more than 40 percent. In 2015 our adjusted
dividend payout ratio of 49 percent was superior to our peers.
Frontier seeks sustainable growth through our expanded
presence in growing markets and by enhancing our product
offerings across our entire footprint. Our acquisition of
broadband, video, voice and FiOS operations in California,
Texas and Florida transforms our company.
The transaction accelerates three critical objectives:
to increase our presence in growth states, to transform our
revenue mix, and to focus on core competencies. While this
disciplined approach has presented us with both enormous
opportunities and challenges, we are on the right path with
the right team.
In 2016, under the new Vantage brand, we are evolving
to become an integrated entertainment provider offering
customers simple ways to consume video however they want,
wherever they want. This report shares more information
about our exciting plan to deploy video in new markets.
I am confident Frontier Communications will continue to be
a forceful competitor in the increasingly digital future. We
have transformed our company from its modest beginnings
and will continue to pursue the right opportunities to deliver
value and achieve success.
Sincerely
Daniel J. McCarthy
President and Chief Executive Officer
