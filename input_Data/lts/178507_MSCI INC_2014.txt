MY FELLOW SHAREHOLDERS:
2014 was a strong year for MSCI. We reported revenue of $997 million, an increase of
9% compared to 2013. Earnings per diluted common share increased 33% from 2013 to
$2.43, which included the gain relating to the sale of Institutional Shareholder Services
Inc. (�ISS�). Notwithstanding the significant investments associated with the enhanced
investment program discussed below, earnings per diluted common share from
continuing operations increased by 4% to $1.70 in 2014 compared to $1.64 in 2013.
The mission of our company is to provide global institutional investors with objective
insights that help them make better investment decisions. We deliver an integrated view
of investment performance and risk across all major asset classes: equities, fixed income,
hedge funds and real estate. The extensive data that we produce and deliver to our clients
is enabled and enhanced by the cutting-edge analytical applications that we develop.
Our firm is in a unique and privileged position because we are at the epicenter of the
investment process worldwide, situated among the owners of assets, the managers of
assets and the traders of assets. To capitalize further on the important role we play in the
investment industry, and to take advantage of the significant changes occurring in our
markets, over the past 18 months we have made substantial investments in people and
technology to position our firm for future growth. We have strengthened our foundation
and augmented our product line, added capacity to our systems and established
important capabilities in areas such as marketing, client relationship management and
technology. While we have been building for the future, we have also done an outstanding
job of retaining clients� business and winning mandates.
Since the end of 2012, $115 million of a total increase of $151 million in our expense
base was dedicated to developing the capabilities I just outlined. Approximately $71
million of the $115 million was related to the acquisitions of IPD, our real estate
performance attribution service; InvestorForce, a leading provider of performance
reporting solutions to the investment consultant community; and most recently, GMI
Ratings, a leading provider of environmental, social and governance (�ESG�) ratings and
research to institutional investors. These acquisitions were designed to fill gaps in our
product and service portfolio, expand our product offerings to include new asset classes,
and accelerate capabilities, all within the three- to five-year window that we set for
acquisitions to be accretive to our financial results.
Another $44 million of the $115 million was invested to develop new and enhanced product
offerings, including new indexes, new factor models, new risk models, and data center
capacity; achieve geographic expansion; and further enhance the executive leadership
of our firm. We have also made significant investments in our sales, client service and
technology platform. These investments have driven near-term returns, principally in
the form of much higher retention rates. We expect the investments that we have made
in product development, sales, client service and technology will deliver returns over the
medium term as well, in the form of higher sales, Run Rate, and revenues.
To drive sales and strengthen our relationship with clients, we hired 109 net new sales and
client service people in 2013 and an additional 44 people in 2014. The training cycle of our
salespeople varies by product area. A salesperson in our equity index product line takes
about six months to train and become productive, whereas a salesperson in our analytics
product line might require a full year or more. The hires that we made in 2013, therefore, had
a negligible impact on our sales in 2013, but we did begin to see a positive impact in 2014. In
the last quarter of 2014, for example, we recorded total sales of $37 million, the highest level
of sales since the first quarter of 2011. Furthermore, subscription sales of $32 million in the
fourth quarter were the highest since the third quarter of 2010. Higher sales, combined with
some of our lowest levels of cancellations, have translated into stronger levels of net new 

business and very strong aggregate retention rates across all our product areas. As a result,
we ended 2014 with an aggregate retention rate of 93%, an increase of 150 basis points over
2013. This is an accomplishment that just a few years ago we thought would be unattainable.
We are pleased with the early returns of our enhanced investment program, which is now
largely complete. Over the past two years, $57 million of our revenue benefited directly
or indirectly from our investments. We are now able to wind down our spending growth to
more normalized levels and the focus going forward will be to ensure that we achieve the
expected returns for the investments we have made in each of our product areas.
INDEX, REAL ESTATE AND ESG PRODUCTS
Equity index, real estate and ESG revenues of $583 million in 2014 represented 58% of
our overall revenue. In 2014, we invested an incremental $9 million in our products,
including $6 million related to the acquisition of GMI Ratings. Other investments that we
made have significantly enhanced our MSCI ACWI Index subscription and asset-based
fee products and our factor indexes. These investments have allowed us to grow our
Run Rate, attract more client assets benchmarked to our indexes, bring on new clients,
better retain existing clients and license our indexes as the basis of new Exchange
Traded Funds, or ETFs. Over the past two years, $45 million of the revenue generated
by the index, real estate and ESG product areas directly or indirectly benefited from the
investments we have made.
We had 182 policy benchmark wins in 2014, up 8% from the prior year. New index
families launched increased 243% in 2014 compared to 2013. Revenues from licensing
our indexes as the basis of ETFs showed significant strength in 2014, and the success
of our licensing efforts is being driven by the major investments that we have made in
this area. The number of new ETFs linked to MSCI indexes grew 20% year-over-year in
2014. More than 40% of the cash inflows into ETFs linked to MSCI indexes were in ETFs
we licensed over the past two years.
Factor indexes are a very important future growth driver for the company. Barra, which
has been part of MSCI for over 10 years, was a pioneer in studying the effect of factors
on the performance and risk of equity portfolios, and we have leveraged this expertise to
create the leading franchise for factor indexes in the world. Almost half of new MSCIbased ETFs launched in 2014 were linked to MSCI factor indexes. In 2014, assets either
benchmarked to, or passively tracking, our factor indexes totaled $122 billion, up 69%
from the $72 billion we recorded at the end of 2013, reflecting the strong growth we are
seeing in this area.
PORTFOLIO MANAGEMENT ANALYTICS PRODUCTS
Equity portfolio management analytics revenues were $104 million in 2014, or 10% of
our overall revenue. In 2014, we invested an incremental $4 million into the business,
primarily focused on enhancing Barra Portfolio Manager, our client software application,
and significantly increasing the production of equity risk models.
There was a time when clients used generic country, regional and global risk models,
but clients are now looking for more specific views of performance and risk trade-offs
in different areas of the market. Demand for sector, small cap and emerging market
models has increased dramatically. To serve this demand, we launched 25 new risk
models in 2014, driving a strong increase in our Run Rate. I am very pleased with the way
MSCI�s research capabilities have contributed to the production of equity models that are
strongly differentiated from what our competitors are offering.
Over the past two years, $9 million in revenue generated by the portfolio management
analytics product area can be linked directly or indirectly to our investments and we are
confident that this product line is on track for continued growth.

RISK MANAGEMENT ANALYTICS PRODUCTS
Risk management analytics revenues were $310 million in 2014, or 31% of our overall
revenue. In 2014, we invested an incremental $7 million into this product area on several
projects focused on performance attribution, over-the-counter (OTC) margining and
mortgages and structured products. As a result of these investments, we have been able
to improve our Run Rate through higher retention. Over the past two years, $3 million in
revenue generated by risk management analytics products benefited directly or indirectly
from the investments that were made.
Our ability to process large and very complex portfolios is a core strength of the
business. In 2014, for example, we processed 31 billion securities on our risk analytics
platform, which represented an increase of 29% over the 24 billion securities processed
in 2013, most of which was in the risk management analytics product area.
CORPORATE DEVELOPMENTS
Turning to the firm as a whole, in 2014 we also took a number of actions to better
manage our capital for the benefit of our shareholders, enhance our competitive position
in the near and medium terms and set our strategic imperatives for 2015.
In the third quarter of 2014, we announced an enhanced capital return policy with a
commitment to return $1 billion in capital to investors by the end of 2016. We have
been disciplined in our approach to mergers and acquisitions and our focus now is on
realizing the full benefit of our enhanced investment program and returning excess
capital to our investors. I am pleased to report that we returned $420 million to investors
in 2014 through share buybacks and dividends. This included the cash proceeds
received from the sale of ISS, which we executed to focus on our core business. In the
third quarter of 2014, we also refinanced our senior secured term loan facility through
the issuance of $800 million aggregate principal amount of 5.25% senior notes due
November 15, 2024. The goal of the refinancing was to increase our financial flexibility,
take advantage of the current low interest rate environment, and decrease our exposure
to interest rate changes.
Over the past few months, we have taken steps to better align the firm�s organizational
structure with our clients� demand for more seamless and integrated relationships with
MSCI. Roughly 65% of our top 100 clients use products from the entire MSCI product
line and these clients represent 43% of our subscription Run Rate. Since we began our
enhanced investment program in 2013, the subscription Run Rate for this client set is
growing at a rate of about 9%, above our growth in subscription Run Rate of 7% for all
clients. This is why we are building a go-to-market approach that enables us to deliver
our full suite of products and services to our entire client base, which we expect will
result in higher revenues over time.
Toward this end, we have combined all of MSCI�s product lines under the newly formed
Product Group, which also has responsibility for the research that fuels product
development and maintenance. This integration will help us become smarter and more
efficient in the way we build and market products. Baer Pettit, who has successfully led
our index, real estate and ESG product lines, now heads up this group.

Within the new Product Group, we have also combined our portfolio management
analytics and risk management analytics product lines into one Analytics product line
under the leadership of Peter Zangari, who has successfully re-established the growth
engine for portfolio management analytics over the last three years. Peter and his team
will focus on generating more revenue and increased operating efficiencies.
In 2014, we strengthened our leadership team with two important additions. In April,
Scott Crum joined MSCI as Chief Human Resources Officer and in December we
brought in Laurent Seyer as Head of Client Coverage. Both executives bring a wealth of
experience that will be very valuable to MSCI as we continue to grow our business.
On behalf of our Board of Directors, I would like to welcome Wayne Edmunds, Rob Hale
and Wendy Lane as new Board members. Wayne Edmunds was most recently the Chief
Executive Officer of Invensys plc. Rob Hale is a Partner of ValueAct Capital, an investment
fund with over $18 billion of assets under management. Wendy Lane has been Chairman
of Lane Holdings, Inc., an investment firm, since 1992. We very much look forward to
benefiting from their collective experience and perspective.
With much of our infrastructure building behind us and the benefits of our enhanced
investment program gaining momentum, the next phase of our transformation can begin.
This year will be about sharpening our strategic focus to ensure that we are managing the
firm in a way that will continue to produce long-term value for our shareholders.
Our operating plans for 2015 are built around five strategic imperatives that we have
established for the firm:
1. Operate as one integrated firm, building upon our scale and core capabilities.
2. Become even more client centric, ensuring that we understand, prioritize and deliver
on the needs of our clients.
3. Energize our culture to increase our focus on high performance and full employee
engagement.
4. Establish technology as a business enabler that delivers our unique content to clients
through common platforms, architecture and tools across all products.
5. Produce sustainable revenue growth, expand operating margins and create value
for shareholders.
In summary, 2014 was a strong year for MSCI. In the coming quarters and years, we
expect a number of business drivers to enable us to achieve double-digit growth in 2016
and beyond.
Sincerely,
Henry A. Fernandez
Chairman, CEO & President