    Chief Executive's review
                                                                                




    In 2001, we grew our capacity by 8%, built on our positions in the key vacation markets of North America,
    the U.K. and Germany and delivered earnings growth of 3%.

    The events of 11 September 2001 had a significant impact on our business. Nevertheless, operating profit
    for 2001 was down by only 3% compared to 2000 and basic earnings per share were up by 3%. We expect
    that there will be an ongoing impact in 2002 from the events of 11 September. However, we have been able
    to take advantage of the inherent flexibility of our business and mitigate this impact by moving our ships, adjusting
    itineraries and taking other measures. In 2002, we will have a higher proportion of cruises leaving from North
    American homeports thereby reducing air travel for our North American customers.

    A key highlight in 2001 was the announcement in November of our proposed combination with Royal Caribbean
    Cruises. This combination will create a cruise company with well known brands that will operate in the major
    global vacation markets. The combined group will have one of the largest and most modern fleets of any major
    cruise company with over 40 ships, giving the combined group the flexibility to deploy ships strategically
    in response to demand around the world, enter and accelerate growth in newer markets and maximise the
    potential of our brands. We believe that the proposed combination will benefit our customers and shareholders.

    We believe that the combination is a natural strategic fit, given Royal Caribbean's strength in the important
    Caribbean trade and our strengths in the destination trades and our global reach. The combination is expected
    to bring cost efficiencies in the near term through achieving a more efficient operating cost structure and
    through rationalising some back-of-house systems and functions.

    The proposed combination is currently being reviewed by the U.K. and U.S. regulatory authorities. Completion
    is subject to shareholder and regulatory approval.

    Throughout the year we have made progress in modernising our fleet, differentiating our product, expanding
    our global reach and improving our cost structure.

    In May 2001, we introduced to the Princess Cruises' fleet Golden Princess, sistership to the popular and successful
    Grand Princess. We took delivery of Star Princess in early 2002 and expect delivery of Coral Princess later
    this year. These ship introductions, together with the withdrawal of Pacific Princess, and the transfer in 2002
    of Crown Princess and Ocean Princess to A'ROSA and P&O Cruises respectively, will mean our capacity
    growth in North America for 2002 will be 9%. These changes will improve the quality of our fleet and should
    increase our overall earning capability by improving both the revenue potential and cost structure of the fleet.

    In January 2001, we introduced a new platform for the Princess brand. The new brand positioning - Personal
    Choice Cruising - emphasises the many personal choices offered by the modern cruise experience that
    Princess provides. Throughout the year we have introduced product innovations, which enable customers to
    personalise their holiday experience. We have also continued to invest in our Alaska tour operations.






We also continued to build upon our positions in the key vacation markets of the U.K. and Germany.

During 2001, we announced that Oceana, currently trading as Ocean Princess, would be introduced to our
P&O Cruises' fleet in November 2002. Her introduction will coincide with the retirement of Victoria, whose
sale we announced in 2001. These changes will help improve the quality of our fleet and will increase our
capacity in this important sector by over 20%.

In Germany, we launched a new brand, A'ROSA, which will operate alongside our successful cruise product,
AIDA. The launch of A'ROSA enables us to expand our product offerings within the German cruise and
leisure industry. The Arkona will be withdrawn in February 2002. With the introduction of AIDAvita in May
2002, we are set for significant expansion in this fast growing sector.

Finally, we have successfully focused on improving our cost structure through modernising our fleet and
reducing overhead costs through office relocations and rationalising certain administrative systems such as I.T.,
reservations and other operations.

Cruising is an exciting growth industry underpinned by strong fundamentals. We have made strategic progress
throughout the year to strengthen our position within this industry and I believe that our Company is well placed
to take advantage of this exciting growth. The proposed combination with Royal Caribbean will bring additional
strategic and operational advantages both in the short and longer term, driving shareholder value.

I would like to thank our travel agent partners throughout the world for their support of the Company
throughout the last year. Their untiring efforts and commitment have been a major contributing factor to our
success. I would also like to thank all our staff for their commitment to providing our customers with
outstanding vacations in a safe and secure environment whilst operating as efficiently as possible. Their
response to the events of 11 September was magnificent.


