Letter to shareholders

Were the right company, in the
right place, at the right time.
Now is the time for North American natural gas and now is the time for
EOG Resources. Were the right company, in the right place, at the right time.
The North American supply of natural gas continues to decrease while demand for
fuel-efficient, clean burning natural gas is likely to grow. Our persistence and discipline
in concentrating EOGs exploration and development efforts largely on
North American natural gas are aligned to continue to help the United States
meet its energy needs.
On the supply side of the North American natural gas picture, we foresee a
perfect storm brewing: a disturbing, unprecedented convergence of market forces
related directly to the three longstanding bases in the North American natural gas
market grid  the U.S., Canada and Mexico. From 1994 through 2001, the U.S.
recorded flat natural gas production of approximately 52 billion cubic feet per day
(Bcfd), while natural gas demand increased to 62 Bcfd. In 2002, we estimate total
U.S. natural gas supply decreased 5 percent, the largest drop in 17 years. In 2003,
we estimate supply could fall another 1 to 3 percent to approximately 47.5 Bcfd.
In the past, natural gas from Canada provided a safety valve, flowing as
needed across the border to fill the 10 Bcfd gap. Those days may be gone, based
on steadily dwindling Canadian natural gas reserves and the increase in inherent
decline rates in mature basins. After reaching a peak in 2001, Canadian production
declined in 2002, the first time since 1986.
While Mexico was another source of U.S. supply in the past, it currently
imports 0.6 Bcfd of natural gas from the United States. By 2006, estimates predict
that figure will increase to at least 1 Bcfd. This perfect storm confluence of
events is expected to be a multi-year phenomenon that pushes natural gas prices
substantially higher than historic levels.
Drilling activity in North America has not yet responded to the recent
higher natural gas prices. Substantial new supplies of natural gas are not expected
to hit the U.S. market until 2007 at the earliest, when a Mackenzie Delta
pipeline from Canada is constructed, or relief is provided by increased liquified
natural gas (LNG) infrastructure and imports.

Demand for natural gas, on the other hand, has remained strong. Thus, from
a producers perspective, the North American natural gas market is the sweet
spot of the entire worldwide energy picture. This reinforces EOGs belief that
North America is an excellent place to be positioned for the foreseeable future.
Consistency remains our catchphrase
EOGs business and operations strategy for 2003 and beyond is consistent with
the game plan we have reiterated in prior years. We are heavily weighted
toward North American natural gas, which currently represents 73 percent of
our total production.
We find natural gas and oil the old-fashioned way  through drilling and
exploration.
With our consistent growth through the drillbit strategy, we have created a
prospect generating franchise  a bright, dedicated workforce whose passion is
finding new reserves of natural gas and oil. We continue to add to EOGs talent
pool, attracting more exceptional men and women to work in the nine decentralized
divisions. From time to time, EOG makes tactical acquisitions but has generally
steered away from the purchase of large concentrations of reserves through
major mergers and acquisitions. In 2002, EOG was the fifth most active U.S.
driller and the third most active driller in Canada.
One of the primary growth drivers of natural gas in prior years and again in
2002 was our ongoing shallow natural gas program in Canada. EOG drilled
approximately 1,100 wells in 2002 and the goal in 2003 is to repeat a high level
of activity with the same level of success.
Noteworthy in EOGs Corpus Christi Division was a 16,000-foot discovery
in the Wilcox Trend that hit a December 2002 exit rate of over 40 million cubic
feet per day (MMcfd), net. EOG also scored several discoveries in the Roleta
Trend. Additionally, EOG achieved significant recent success in the Frio Trend,
where we expect to drill 20 to 30 wells in 2003. In West Texas, EOG holds a
majority interest in a sizeable horizontal Devonian play where a typical well,

the George Weir 14 No. 1H, initially flowed approximately 6 MMcfd of natural
gas with 700 barrels per day of condensate. Eleven out of 12 Mesaverde
wells in the Denver Division proved successful, while in the Oklahoma City
Division, production increased from 65 to 85 MMcfd during 2002. Consistent
with our strategy, this was all through the drillbit.
Elsewhere, EOGs Trinidad operations picked up tremendous momentum in
2002, turning into a dynamic, high growth rate operation. It is now set up for
long-term production increases at a favorable after-tax rate of return. Highlights
of the year include the Parula natural gas discovery with net reserves of 250 billion
cubic feet equivalent (Bcfe) on the offshore SECC Block booked at year-end.
In addition, EOGs license on the 78,000-acre SECC Block was extended 25
years to 2029. EOG was awarded two new offshore blocks: the 90,000-acre
Lower Reserve L and the 96,000-acre Modified U(b). EOG is supplying
approximately 50 MMcfd, net of natural gas to the CNC Ammonia Plant that
commenced operation in 2002. EOG expects to supply a second ammonia plant,
Nitro 2000, with approximately 50 MMcfd, net of natural gas. It is targeted to
come on line in 2005. Because natural gas is the primary feedstock in the production
of ammonia, Trinidad production may displace U.S. and Canadian
ammonia plant output that could shut down due to high feedstock costs. Along
with LNG, this creates a tie between the natural gas markets in North America
and Trinidad.
Since becoming a wholly independent company in 1999, EOG has consistently
posted excellent reserve results. For the three-year period 2000 through
2002, we averaged 182 percent reserve replacement at a worldwide finding cost
of $1.17 per Mcfe.
2002 performance sets the stage
Although a shadow blanketed corporate America with regard to earnings efficacy
in 2002, a major U.S. investment house rated EOGs financials and reserve bookings
as the most conservative in our peer group. EOGs passion for clean, simple
financials has not changed. We have no goodwill on our balance sheet. We use
successful efforts accounting, the more conservative exploration and production

accounting methodology. We have avoided the massive write-downs that the
industry has recorded. External reviews of EOGs reserves have been within 5
percent of internal estimates for 15 consecutive years, according to the independent
reserve engineering firm of DeGolyer and MacNaughton. In addition, relative
to the industry, EOG has a low percentage of proved undeveloped reserves
on its books.
Also in 2002, EOG achieved full float stock liquidity after the last 10 percent
block held by a former shareholder was successfully placed in the market.
For the eighth consecutive year, EOG reduced its total outstanding share count,
from 115.1 million at year-end 2001 to 114.4 million at year-end 2002. EOG
has very strong debt coverage ratios, and compared with the industry, a conservative
year-end debt-to-total capitalization ratio of 40.6 percent. Because we
anticipated healthier prices in 2003, we purposely overspent cash flow in 2002.
This sets up EOG for operational success in 2003 and beyond.
For the three-year period, 2000 through 2002, EOGs stock ranked number 14
among the best performers of the Standard & Poors 500. For the five-year period
1998 through 2002, EOG posted a 24.6 percent return on equity. EOGs focus on
performance never wavers; our goal is to be a solid, responsible and profitable exploration
and production company with a consistent long-term strategy.
The best is yet to be
The creative talents, energies and enthusiasm of our 1,000-employee workforce
are in lockstep, right here, right now. With one of the most efficient and effective
organic growth machines in the business, EOG will continue to play a leadership
role in the industry, adding precious natural gas reserves efficiently and costeffectively,
year-in, year-out.

Mark G. Papa
Chairman and Chief Executive Officer
March 10, 2003
Edmund P. Segner, III
President and Chief of Staff