To the Shareholders
and Cast Members of
The Walt Disney Company:

It has been a very long time since our country has faced
the kind of economic turmoil we are currently experiencing,
and The Walt Disney Company, while strong, is certainly not
immune to the difficult market conditions that exist today.
With consumers spending more carefully and spending less,
and advertisers doing the same, our media, consumer products
and theme park businesses all face significant
business challenges.
I am confident our brands, products and people will pass
the test that lies before them, but our businesses are
all affected by an economic downdraft whose duration
remains uncertain.
We will continue to focus on what creates the most value
for our shareholders: delivering high-quality creative
content and experiences; balancing respect for our legacy
with the demand to be innovative; and maintaining the
integrity of our people and products. As stewards of our
great Company, we have reason to be optimistic. But we
must also be realistic, and it is this blend of realism and
optimism that is guiding us during this tumultuous time.
When signs of a weakening economy began to surface,
our senior management team put in place measures to
operate more efficiently and to invest even more prudently.
We adopted marketing and pric ing strategies designed
to keep our products as attractive as possible, while at
the same time underlining our commitment to quality. In
this tough climate, even the most forceful measures are
unlikely to compensate for the loss of business.
In fiscal 2008, despite an economy that weakened as the year
progressed, we delivered strong creative and financial results,
posting record revenue and earnings per share.
Highlighting our financial achievements, revenue hit an all-time
high of $37.8 billion, a 7 percent increase over the previous year.
Earnings per share, excluding certain items detailed in the
footnote on page one, were $2.27, up 18 percent from the
previous year.
As has been the case for many years, our financial success has
been largely due to our creative achievements and in 2008,
those achievements were numerous. I could mention many, but
instead Id like to highlight just three.
This past summer, DisneyPixar once again enthralled audiences
around the world with an original, unique and memorable film.
WallE was a commercial, creative and technological success.
Director and writer Andrew Stanton and his team created a film
for the ages and for all ages, and we are extremely proud of
their work.
As Americans turned out in record numbers to elect the 44th
president of the United States, ABC News, led by anchors
Charlie Gibson, Diane Sawyer and George Stephanopoulos,
brought the right mixture of excellent reporting and thoughtful
analysis to this memorable and historic election, capping
months of insightful, interesting and energetic coverage of the
American political process.
Thanks to our Imagineers, at our parks in California and
Florida we opened Toy Story Mania!, an engaging attraction
that combines the wonder and lovability of the Toy
Story characters with breakthrough technology. The result
is yet another theme park experience that distinguishes our
Company and causes people to say: What will they think of
next? or, Only Disney can do that.
Beyond these individual achievements, we continued to
create and support several key franchises. These are stories
and characters that can be leveraged across many of our
businesses, on many technological platforms, in many territories,
and over long periods of time.
While we continue to break new creative ground, our substantial
investment in such great Disney franchises as Cars,
Toy Story, Princesses, Pirates, Mickey Mouse, Winnie the
Pooh, High School Musical and Fairies continues to drive
strong returns, differentiates us from our competitors and
builds long-term shareholder value.
Not only do we possess a significant number of such franchises,
but our ability to make the most of their success is
unrivalled. This comes from a collection of great assets and
a commitment to manage them as a whole that is worth far
more than the sum of its parts. This defines Disney and, as
we have been saying, creates the Disney Difference.
We are proud of our businesses, but we balance that pride with
keen awareness of the challenges they face, whether secular
or cyclical. While we realize perfection is not something that
can be delivered all of the time, we at least embrace the value
of striving for perfection all of the time. This is true of all of
our businesses, whether it be ESPN, ABC, ABC Family, Disney
Channel, our Disney Parks and Resorts, Consumer Products,
Studio Entertainment or Interactive Media Group.
Strategically, we continue to adhere to priorities established a
few years ago. A commitment to high-quality creative work,
a persistent focus on mastering new technology and selective
investment in promising international markets are strategies that
have worked for us, and we believe they will continue to position
us well for the long term.
Moving forward, we will be taking a very pragmatic approach
to new investments across the Company, pressing ahead only
in those areas we believe offer the greatest opportunity for longterm
growth and returns. We will continue to invest in our creative
franchises and in developing our highly promising and fast
growing video game business. We are also going ahead with the
improvement and expansion of Disney's California Adventure at
Disneyland Resort, new Disney Vacation Club properties including
the Ko Olina project in Hawaii, and two new cruise ships that
will be setting sail in 2011 and 2012.
Given the environment, we will likely see some interesting acquisition
opportunities, and given the strength of our balance sheet,
many could be enticing. Rest assured, though, no matter how
inexpensive these opportunities may look, we will not relax our
standards in terms of the quality of the assets we seek to purchase,
their fit within our Company's businesses and core
strategies, or their prospects for delivering strong returns.
We are committed to upholding the excel lent reputation of the
Disney name through continuous refinement and expansion
of our social responsibility efforts. Last year, we took several
steps in this area that we believe will benefit our employees
and Cast Members, our Guests and consumers, our shareholders,
our business partners and the communities in which we
live and work.
We expanded globally our initiative to associate our brands
and characters with healthier foods, bringing fresh Disneybranded
fruits, vegetables and dairy products to families
in dozens of countries. Weve completed our first Companywide
greenhouse gas inventory and set reduction targets for
emissions in order to minimize the Companys impact on the
planet. And we celebrated the 25th anniversary of our Disney
VoluntEARS program with employees contributing a record
495,000 hours on a wide range of projects benefiting children
and caregivers around the world.
corporate
responsibility report, which will provide greater detail
and greater insight into how we approach crucial issues related
to the environment, community, workplace, products and our
responsibility to kids and families.
We are fortunate at Disney to have a very diverse board of
directors, whose broad interests and experiences are helping
to guide the Company in this complex environment. Im
especially grateful to John E. Pepper, Jr., our non-executive
Chairman of the Board, and the wisdom, integrity and generosity
of spirit that he brings to our Company.
We are also fortunate to have a truly outstanding and committed
group of people working for this Company. Im impressed
and amazed on a daily basis by the passion they put into making
sure our Guests and consumers not only have a special experience,
but are transported from their everyday lives to worlds
that could only be created by Disney.
So, on behalf of our employees and Cast Members, Id like to
thank you personally for your continued support. Its a real honor
to uphold the Disney legacy and to continue to create memorable
and magical experiences for kids of all ages. Together, we
look forward to doing our very best for you every day, everywhere
and in every way.

Robert A. Iger
President and Chief Executive Officer