Fiscal 2001 was Dardens best year ever. Building on our record financial
results in the prior year, this years outstanding performance reflects wide-ranging
and fundamental improvements in our businesses. It also reinforces our belief
that we have the right strategies to achieve our ultimate goal  being the best
company in casual dining, now and for generations.
 Revenues increased 9% to $4 billion. We combined continued same-restaurant sales growth with new restaurant growth at Red Lobster and Olive Garden, continued rollout of Bahama Breeze, and significantly accelerated our test of Smokey
Bones BBQ Sports Bar.
 Earnings after tax increased 14%, excluding an unusual non-operating gain in the prior year, to $197 million, our highest level ever.
 Earnings per share on a diluted basis rose 21% before the prior years unusual non-operating gain, to $1.59. This is above the 15%  20% annual long-term EPS growth range weve targeted and was achieved despite the softness in the U.S. economy for much of the second half of the year.
 Red Lobsters annual sales were a record $2.20 billion and its average sales per restaurant reached a record $3.4 million. Same-restaurant sales increased 5.9% for the year, and Red Lobster ended the year with 14 consecutive quarters of same-restaurant sales growth.
 Olive Garden also delivered new sales records. Total annual sales were $1.71 billion, and average sales per restaurant climbed to $3.6 million. Same-restaurant sales grew 7.2%, matching its 7.2% increase for the prior year. With gains in each quarter of the year, Olive Garden has now posted 27 consecutive quarters of same-restaurant sales growth.
 Bahama Breeze continued to provide guests award-winning culinary and beverage offerings with exceptional levels of service andhospitality. It opened an additional seven restaurants, and there are now 21 Bahama Breeze restaurants in 16 markets.
 Smokey Bones BBQ Sports Bar accelerated its test, opening seven new restaurants to bring the total number in operation to nine. The combination of great tasting barbecue in a relaxed sports bar atmosphere is proving to be broadly appealing. Each of the restaurants opened strongly and sustained high sales levels. We are now poised to begin expansion in fiscal 2002.
 Our strong cash flow and balance sheet enabled us to purchase 8.4 million shares of common stock in fiscal 2001.
 We continued to move forward in our efforts to make our Company more
diverse. Red Lobster, for example, was recognized as a top company for management diversity by People Report, a human resources firm that tracks casual dining restaurant workforce data. And for the third year in a row, Darden was named to Fortune magazines list of the top 50 companies for minorities.
 As always, we were actively involved in the communities in which we do business, contributing more than $8 million
to social service, environmental, educational and cultural efforts through corporate and restaurant donations, the Darden
Restaurants Foundation, and the Darden Environmental Trust. Our employees also devoted hundreds of thousands of volunteer
hours to community-based organizations and civic projects all over North America. With the excellent operating and financial performance weve enjoyed, theres new excitement at Darden about the kind of future we can create. Were excited because the casual dining industry is poised for tremendous growth and because Darden is strongly positioned to become the best in casual dining, now and for generations. 
We Have Reason to Be Excited
Casual dining is already a $47 billion industry, and its long-term prospects are excellent. I have written before about the powerful factors we believe will drive casual dining sales growth of six to eight percent a year over the next ten years. These growth drivers remain
firmly in place and include (1) the significant increase in the coming decade in the number of people between 45 and 65, the peak years
of casual dining usage; (2) the continued steady growth projected in the percentage of women in the workforce; and (3) the durability of
lifestyle changes that place a premium on the timesaving and social connection benefits of dining out. Darden is well prepared to capitalize on this growth opportunity because of the commitment we made four years ago to being brilliant with the basics. The food and service in our restaurants are better. The quality of the support provided to our restaurants is better. Marketing, communications, finance, purchasing, technology, human resources  were clearly better in virtually everything we do. While were proud of how much better the organization has become, our goal is to be the best in casual dining, now and for generations. With the platform of tremendous strength we now have, we are shifting our focus from a phase of getting better to one of becoming the best in a great and growing industry.
Thats what excites us. From Getting Better to Becoming the Best

Our Strategy
For the journey from getting better to becoming the best", Dardens strategy remains the same. We intend to:
 Keep Red Lobster and Olive Garden fresh and vibrant, enabling these well-established businesses to provide sustained same-restaurant sales growth and engage in steady new restaurant expansion.
 Expand Bahama Breeze and Smokey Bones BBQ Sports Bar significantly, while enhancing the unique characteristics guests find so appealing, to  turn these exciting, emerging brands into core Darden businesses.
 Acquire or develop attractive new restaurant concepts that are responsive to enduring consumer demand and help Darden realize its growth objectives. 

Our Strategic Building Blocks
The three strategic imperatives at the core of Dardens strategy also remain unchanged. Supported by our enduring values of trust,
respect, integrity, and a commitment to diversity, we intend to create an organization that is continuously focused on:
 Leadership development as a competitive advantage.
 Service and hospitality excellence.
 Culinary and beverage excellence.

While weve accomplished a great deal during the past several years to strengthen each of these strategic building blocks, becoming the best requires that we accelerate our progress. To achieve the increased strategic momentum the Company needs, we must embrace fundamental change. We must be more rigorous in identifying the critical things we do today and find ways to do them better. We must focus on identifying completely new things that must be done and ensure we do them with urgency. And, we must be more committed than ever to turning away from activities and approaches that have served us well in the past but may not be right for our future.

Fundamental change is our ticket to becoming the best  and we welcome it. We have a talented and experienced management team, the best Ive had the pleasure of working with during my entire career. This team appreciates how fortunate we are to have the opportunity to create an organization that is the very best at what it does. Each of us is committed to raising the bar on how we think and act to capitalize on that opportunity. The Essential Building Block: Leadership Development Leadership development, establishing and achieving the industrys very highest service and hospitality standards, and redefining culinary and beverage excellence in casual dining are the essential building blocks in our strategy to be the best. To make them a reality, we must have leaders who inspire in their teams a great passion and enthusiasm for people, restaurants, and dining. We have dedicated significant resources to building the leadership skills we need to continue growing Darden. This includes specifically tailored leadership assessment and development processes for leaders throughout the organization. From the frontline employees and managers in our restaurants, to the executive teams and other officers, our efforts will intensify. In addition to enhancing each team members functional skills, we are deepening their strategic knowledge of service and
hospitality, culinary and beverage, and leadership development. We are also reinforcing the long-held values we believe are necessary for success  trust, respect, integrity, and a commitment to mastering dynamic areas of opportunity. Two special areas of
opportunity that will continue to be focal points are diversity and technology. No one can be a truly effective leader in todays environment, or in the world well face tomorrow, without extraordinary commitment to these areas.

Diversity Leadership 
The business-building benefits of a deep and intuitive understanding of diversity are clear and compelling. We aim to create an environment that welcomes diversity, understands its implications, and translates that understanding into appropriate systematic practices
at every level of the organization. As we build our understanding of and commitment to diversity, well be able to better understand ourselves and our own leadership motivations, strengths, and opportunities. Well be better able to select, develop, and lead our teams. Well be able to speak to guests more effectively in our marketing. And, well be better able to deliver on the needs of individual guests in our restaurants across a wide range of groups  whether defined by racial, ethnic, gender,
age, regional, cultural, or other differences.

Technology Leadership
It is also clear that technology offers tremendous opportunities to directly benefit the guest, employee, and supplier experiences we
provide. Our leadership development objectives in this area are straightforward. We want leaders who have a sufficient understanding of technology to cut through the clutter and recognize the right new applications to pursue. We also want leaders who know
how to use any new capabilities to our maximum advantage, so we fully leverage the significant investments being made.

Summary
The greatest competitive edge our Company has is the quality of our employees, evidenced by the excellent job they do every day.
Bill Darden, the founder of Darden Restaurants, recognized that decades ago, and it remains just as true today. I want to thank our
more than 128,000 employees for accepting the challenge of being brilliant with the basics. I also want to thank them for accepting the
new challenge of shifting from getting better to becoming the best. I am fully confident we can be the best, now and for generations. Dardens operating success in fiscal 2001 is a testament to what we can do together when we make the commitment. 