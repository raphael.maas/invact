CHIEF EXECUTIVE OFFICERS LETTER TO SHAREHOLDERS
Dear Shareholders,
Fiscal 2013 was clearly a challenging year for our company as a result of economic headwinds in Europe and media attention
from voyage disruptions. However, we addressed those challenges head-on and made significant progress on a number of
strategic initiatives designed to better position our company for the future.
2013 Overview
Revenue yields declined 2.6 percent (constant dollars) driven by lower yields for our North America segment, due principally
to promotional pricing at Carnival Cruise Lines, as well as lower revenue yields in Europe resulting from the challenging
economic environment.
Excluding fuel, unit costs increased 4.6 percent (constant dollars) primarily due to voyage disruptions and related repair and
vessel enhancement costs as well as increased investment in advertising and emerging-market development.
Net income for 2013 decreased to $1.1 billion from $1.3 billion the prior year. However, our company continued to produce
strong cash from operations approaching $3 billion, funding our capital commitments and returning value to shareholders
through regular dividend distributions of $775 million and share repurchases of $100 million.
During 2013, we achieved a number of major milestones within our organization. Our flagship brand, Carnival Cruise Lines,
embarked on a number of strategic initiatives to accelerate its reputational recovery. The brand executed a travel agent
outreach program, called Carnival Conversations, which entailed a series of roadshows reaching thousands of agents across
the country to better align with our travel partners. They launched a creative new national advertising campaign, Moments
That Matter, featuring memorable vacation moments experienced by millions of guests as captured through their own
images. The brand also unveiled the Great Vacation Guarantee, an unprecedented hassle-free money-back guarantee. These
efforts had a powerful impact on the marketplace, as Carnival Cruise Lines experienced a significant improvement in brand
perception following the events in early 2013. The Carnival brand recovery has been faster than originally anticipated and our
team is working hard to maintain that momentum.
Also during 2013, Costa Cruises continued to experience a strong brand perception recovery, aided by the dramatic
parbuckling operation which righted Costa Concordia. The brand recovered more than half of the reputation it lost in 2012,
achieved a 4 percent improvement in revenue yields and returned to profitability, despite the sluggish economic environment
in Southern Europe.
We made significant progress on our Asia growth strategy, positioning us to capitalize on that very important emerging
market. We doubled our cruise presence in China and successfully launched an inaugural homeport in Japan. We also opened
five offices in China, a second office in Japan and offices in Korea, Taiwan, Hong Kong and Singapore. These additional
resources will support our 2014 Asian sourcing, which is again expected to double.
Also in 2013, through our two innovative new ships, as well as new technology and ship retirements, we have advanced our
fleet and improved efficiency. Royal Princess, the first of a new class of ships for Princess Cruises, captured worldwide
attention with a spectacular naming ceremony in England presided over by the ships godmother, Her Royal Highness the
Duchess of Cambridge. In addition, our German brand AIDA Cruises introduced AIDAstella, its seventh ship in just seven
years for the burgeoning German market. AIDAstella and Royal Princess are among the most efficient ships at sea today, both
from a unit cost and fuel efficiency standpoint, enabling them to produce returns significantly higher than our fleet average.
We also retired a smaller, less-profitable Costa vessel and announced construction of a new, more efficient Seabourn ship for
delivery in 2016 to replace three smaller original ships.
In addition, we announced many exciting new product features to take the guest experience to new heights. Carnival Cruise
Lines continued to roll out its Fun Ship 2.0 product enhancement program, transforming the onboard experience through
celebrity partnerships including comedian George Lopez, who serves as the brands director for comedy, and Food Network
personality and chef Guy Fieri, who developed a complimentary burger venue called Guys Burger Joint. Carnival Cruise
Lines has also undertaken a variety of brand-building initiatives including an exclusive partnership with Dr. Seuss Enterprises
to bring the beloved childrens brand and favorite characters to its fleet, and the Carnival LIVE Concert Series, which
brings the best in live music to the seas with a diverse roster of popular music artists including Jennifer Hudson, Lady
Antebellum and Jewel.
More broadly, across our worldwide fleet, new entertainment concepts have been introduced including a Las Vegasstyle
water and light show on-deck at Princess; Dancing with the Stars at Sea, a Holland America partnership with the hit TV
show in the U.S.; and Strictly Come Dancing, a P&O Cruises (UK) partnership with the BBC. Onboard AIDA, we brought
two of Germanys biggest TV shows to sea with Who Wants To Be a Millionaire and Voice of the Ocean.
P&O Cruises (Australia) introduced a new adventure park at sea, P&O Edge, allowing guests to jump, climb and race their
way to parts of the ship never seen before. Costa Cruises introduced a new slow style of cruising, Costa neoCollection, with
smaller ships sailing to less traveled destinations featuring longer voyages and overnight stopovers. We are looking forward to
Cunard celebrating Queen Mary 2s 10th year in service in May 2014, underscoring her role as the only passenger ship in the
world maintaining scheduled service across the Atlantic, and featuring two exclusive performances by Grammy Award
winning singer James Taylor

The level of quality, variety and innovation available throughout the fleet has never been greater and our guests are reaping
the benefit of truly exceptional vacation values. We are working to more broadly communicate this message through
strengthened engagement with our travel agent partners as well as stepped-up consumer and trade marketing efforts. We
recently introduced new advertising campaigns for P&O Cruises (UK) and Costa Cruises in continental Europe. Princess
Cruises launched, Come Back New, its first television advertising campaign for the North American market in more than a
decade. Also, Carnival Cruise Lines was the national cruise line advertiser of the Sochi 2014 Olympic Winter Games.
Sustainability
We recognize our reputation and success depend on having sustainable and transparent operations. Our commitments and
actions to keep our guests and crew safe, protect the environment, develop our workforce, strengthen our stakeholder
relationships and enhance the port communities our ships visit as well as the communities where we work are as vital as
maintaining our fiscal strength.
We continue to make significant strides toward reducing our environmental footprint. In 2013, our fuel consumption per unit
decreased by more than 5 percent for a cumulative 23 percent reduction since 2005. We also furthered our environmental
efforts through the successful testing of ground-breaking exhaust gas cleaning scrubber technology, which we plan to install
throughout the fleet. In addition to exceeding stricter air emissions standards, this technology will help mitigate higher fuel
costs, creating a true win-win scenario.
Vision
In my first eight months in this new role, I have affirmed what I knew at a visceral level as a board member  the inherent
strengths of this organization clearly lie with our people, a passionate and dedicated team worldwide; our core product, which
is clearly the best vacation value there is; and, of course, our scale.
We have over 100 ships carrying more than 10 million guests each year  twice that of the next largest player in the industry.
With 78 million passenger cruise days, we have a tremendous opportunity to accelerate earnings growth by utilizing our scale.
Operating our brands independently has been successful and led to our industry-leading position. Our brands will remain
independent at the guest interface and become increasingly distinct in the psychographics of the guests they source. Going
forward, we plan to change the focus of our efforts to take advantage of our scale. We will capitalize on this inherent
opportunity through greater communication, collaboration and cooperation across our 10 brands.
We have already announced a realignment of our leadership team. We have begun to change our work processes and our
incentive structure to facilitate those efforts. Our highly talented leadership team is energized by the opportunity and has
identified a number of areas to further use our scale. On the revenue side we are collaborating on onboard revenue initiatives
and revenue management practices. On the cost side we are increasing cooperation among our brands on procurement,
inventory management, information technology and port planning to achieve cost savings. We have begun to mine our
opportunities by sizing and prioritizing these and many other initiatives.
We will secure our future success though a willingness to invest for the long-term. We have eight innovative new ships
scheduled to enter service through 2016 and we continue to invest in existing ships to strengthen the leadership position of our
brands. We will invest in gaining an even deeper understanding of what drives consumer vacation decisions and onboard
enjoyment, which bodes well for attracting first-time cruisers and powerfully differentiating our brands relative to others. We
are ardently focused on delivering a customer experience that surpasses even the high levels we achieve today and, most
importantly, driving greater advocacy from our satisfied guests.
Carnival Corporation & plc enjoys an enviable position in todays competitive landscape. Our leadership position is firmly
rooted in our portfolio of distinct iconic brands. It is fueled by the efforts of our dedicated employees around the world and
countless travel agent partners. Most importantly, it is anchored in a consistent strategy of exceeding guest expectations. I
have great confidence in our ability to execute and deliver on the expectations we have set for ourselves, and thereby realize
our companys true earnings power, driving significant free cash flow and returning to double-digit returns on invested capital
over time.
I am honored and privileged to be leading the next stage of growth for Carnival Corporation & plc. I thank Micky and our
board of directors for entrusting me with this great enterprise and its rich legacy. I thank our employees for their dedication to
delivering great joyful vacation experiences each day and our travel agent partners who have helped us grow to the point
where almost half of all guests that cruise anywhere in the world cruise aboard one of our ships. And, of course, I especially
thank our shareholders and our more than 10 million annual guests for their continued support.
Arnold Donald
President and Chief Executive Officer
February 20, 2014