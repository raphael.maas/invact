To Our Stockholders

Fiscal 2010 was a very good year for us as we recovered quicker than we had
anticipated from the global recession. Our quarterly revenues grew sequentially over
the prior quarters by 14%, 9%, 21% and 18%, thereby surpassing our previous
quarterly sales record prior to the recession. We are now exceeding our revenue
and operating profit growth trend lines which began prior to the recession.

This has been an innovation led recovery. Prior to the recession, the seeds were planted
for cutting edge products across many end-markets. During the recession, companies
continued to invest in the development of these products. Their emergence recently has
led the recovery. Innovation in multi-protocol basestations, automotive electronics, energy
efficiency, tablet computers and many other products, a few of which are shown on the cover
of this annual report, has fueled market recovery across the industrial, telecommunications,
automotive and computer end-markets. Linear Technology has been at the forefront of
analog innovation, which has played an enabling role in the technological trends driving
these products. This innovation leadership has enabled us to be the fastest growing and
most profitable major analog supplier as demonstrated by our financial results.
Revenues for fiscal 2010 were $1,170 million, up from the $968.5 million reported for
fiscal year 2009. This growth was very broad based as we grew in each major geographical
areaNorth America, Europe, Japan and Asia Pacificand in most major end-markets
industrial, computer and automotive. Net income of $361.3 million was an increase of $72.1
million, or 25% over the $289.2 million reported for fiscal 2009. Diluted EPS was $1.58, an
increase of 23% over $1.28 in the prior fiscal year. Operating profit as a percent of sales was
48.2%, up from 42.5% last year and is nearly double that of our closest competitor.

Once again the Company generated positive cash flow from operations, this years
being $492.5 million versus $416.6 million in fiscal 2009. Cash flow from operations
was positive for the 97th consecutive quarter. During the year the Company reduced
the total outstanding principal of its Convertible debt from $1,405.6 million to $1,240.9 million
as it purchased $154.9 million of its 3.00% Convertible debt and $9.8 million of its
3.125% Convertible debt. Presently, the Company intends to retire all of its remaining
3.125% Convertible debt of $395.8 million in November 2010 when it is putable and
callable. During the year the Company increased its quarterly dividend from $0.22 per
share to $0.23 per share. This marks the 18th consecutive year, encompassing various
economic cycles, that the Company has increased the dividend since initially declaring
a dividend in 1992.
These industry leading financial results are the culmination of a several year focus on a
strategy to optimize our strengths and differentiate the Company from its competitors.
Our marketplace is the non-commodity traditional high-performance analog market.
In addressing this marketplace, we concentrate on inventing cutting edge products
which we market globally through a technical sales force. We have vertically integrated
manufacturing capabilities, which emphasize timeliness of delivery and the highest
levels of quality and reliability.
With regard to our marketplace, several years ago we fine tuned our strategic direction
to increase our presence in industrial and automotive markets; maintain our presence
in the computer and telecommunications infrastructure and networking markets;
and decrease our exposure in the commodity driven markets of consumer and cell
phone products. Accordingly, in fiscal 2010, the industrial end-market was 36% of our
bookings, automotive was 11% and military/satellite was 7%, up respectively from
32%, 5% and 3% five years ago. Telecommunications infrastructure and networking

was 22% of our bookings and computer was 15%, similar to 24% and 11% respectively,
five years ago. Finally, high-end consumer was 7% and cell phone was 2%, down
substantially from 9% and 11%.
Going forward we have an opportunity rich environment. The overall analog market is
forecasted to grow to roughly $45 billion in 2012. We serve approximately one-third of
this market and our areas of emphasis are projected to be the fastest growing.
This emphasis on high-performance analog has driven our product direction. We have
many unique creative products currently populating sales in this innovation driven
recovery in the global marketplace. The latest generation 4G basestations utilize
Linear products from many of our business units, including high speed low power A to
D converters, signal chain Module products and high frequency mixers, demodulators
and RMS power detectors. Automotive electronics employ our new battery management
systems for hybrid and electric vehicles and also use our drivers for LED lighting and
various other power products in both gas and electric vehicles. Energy efficiency is
particularly prevalent throughout industrial end-markets. Our high efficiency power
products with operation up to 100 volts are one example of solutions that mitigate
thermal challenges in a wide range of factory applications. Our digital power
management products are employed in server farms to monitor and control for
optimum performance. Tablet computer products have become very successful this
year using several of our latest generation switching power managers and battery
charging devices to optimize charging efficiency and run time.
Energy harvesting techniques employing alternative ambient energy sources such as
temperature changes, vibration, wind and solar present unique challenges in storing
and efficiently converting alternative energy. Our new energy harvesting products and
nano-power battery chargers have received wide acceptance in this developing market.

Our timely ability to innovate has been matched by our ability to deliver
products to our customers. Two important attributes have been distinguishing
factors to our success this year as there has been a rapid rebound from a
recessionary environment to marketplace growth and limits on the availability
of products. First, our comparatively low lead times and performance to
schedule have given us a competitive advantage and second, our outstanding
quality and reliability have distinguished us as a unique supplier, particularly in
the quality conscious automotive market.
In summary, we believe we are well positioned going forward. Analog
excellence has been our mantra from product design to marketing to
manufacturing to financial performance. We are a good fit for an innovation
rich marketplace. We have a very talented and energized workforce and
discerning customers and investors. We are grateful to all of them for their
efforts, their challenges and their loyalty. This innovation rich environment is
the optimum environment for us.

PAUL COGHLAN
Vice President, Finance and
Chief Financial Officer

ROBERT H. SWANSON, JR.
Executive Chairman

LOTHAR MAIER
Chief Executive Officer


