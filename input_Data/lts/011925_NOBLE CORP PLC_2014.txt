To Our Shareholders


In many ways it was a simpler time in 1921 when
Lloyd Noble put a single rig to work drilling for oil
and gas. Simpler, maybe, but no less challenging.

Others had similar plans and business aspirations, but few survived the
inevitable cycles that impact the drilling and energy industry. In 2015, the
company he founded stands strong and continues to be a leader in the offshore
drilling business by being focused on excellence�despite the challenging
market dynamics.
Throughout the decades as technology advances, drilling opportunities
expand and well designs have become more challenging. Noble has consistently
responded with innovative solutions, top-tier assets and team members who
are prepared to deliver operational performance across our fleet. At the same
time, Noble�s ability to manage through cycles and swings in demand has been
a hallmark of the Company for almost 95 years. The current cycle is no different,
and today we are well-equipped to execute our work, manage our business and
drive performance.
Our strategy for securing the future of our Company revolves around
investing in our employees, pursuing operational excellence and managing
our financial resources with foresight while maintaining flexibility. As with all
cyclical businesses, we believe there will be a time when stability and clarity
will return and rig demand and dayrates will respond. Noble�s time-proven track
record of managing our costs and spending in periods of growth as well as
industry decline will serve us well in the years ahead.
In light of the reduction in drilling activity, in 2015 you can expect Noble
to be focused on operating efficiencies and cost control while maintaining our
operational and environmental and safety standards. As a part of this effort, we
will concentrate on continued improvement in rig up-time, and in particular,
continuation of our subsea BOP relaibility improvement efforts. Also, we believe
that a safe operation is an efficient one and we will maintain our focus in this area.

By adhering to our strategy, we can continue to differentiate Noble from our
peers, retain our financial stability and maintain flexibility as we extend our
legacy of success into the future.
Noble�s achievements in 2014, including our spin-off of Paragon Offshore in
August, our stellar performance in nearing completion of our newbuild program
and record-setting well construction programs aboard our rigs, are a testament
to our talented team of professionals who share our vision and desire for
excellence. One of Noble�s strengths is our ability to attract, develop and retain
these highly professional industry leaders, which include both our offshore and
shorebase colleagues.
Consistent with our core value of Environmental Stewardship, members
of the Noble team are committed to the duty and responsibility to ensure that
day-to-day activities are in compliance with the letter and spirit of the law, our
Code of Business Conduct and Ethics, and our environmental, health and safety
policies and procedures.
There are no exceptions to our requirements to act safely and in a way that
protects human health and the environment in all of our activities. We recognize
and continuously reinforce that each of our team members play an important
role in maintaining and fostering Noble�s reputation for safety, protection of the
environment and honesty and integrity.
In no small measure, the dedication and effort of these team members made
our dramatic transformation over the past few years possible. Moreover, these
individuals are central to our strategy for success. In our view, our team and
their ability to deliver operational excellence will set us apart during this cyclical
downturn and situate us for the leadership position in the inevitable recovery of
the industry that will follow. 

Looking ahead, it is clear that we will face a challenging offshore drilling
market in 2015. Noble�s well-timed shipyard program, contracting success and
dedication to operational execution position us to perform well and continue
to deliver high levels of service to our customers and create value for our
shareholders even in this industry downturn.
Despite the current offshore rig supply imbalance, we believe it will be selfcorrecting, and global demand and production will certainly come back into
balance. There may be many days ahead with fluctuating crude prices, but the
long-term prospects for our industry are decidedly favorable. Demand for energy
can, and will, rise and today�s lower prices will play a role in the commodity�s
ultimate price recovery.
It would be wrong, however, to ignore the headwinds we face today. Dayrates
across the industry are down and for the near-term there are more rigs available
than current demand can accommodate. Some drillers, including Noble, are
stacking or retiring rigs. This is a normal stage of the offshore cycle. Some of
these rigs won�t work again as the cost of reactivation outweighs contracting
opportunities. Our decision to retire three rigs from the fleet is a part of that
process. Retiring these units also underscores our willingness to act decisively
to manage the current environment and our capital investment spending.
On the plus side, we have been here before and managing through cycles is
a Noble hallmark. The Noble fleet, one of the most modern and capable in the
industry, is well-contracted. For those rigs that do have exposure to the current
cycle�we expect to rely on all of our experience and renowned reputation
to compete intensely for the work that becomes available, and we are already
meeting with some success. Our focus on excellence equips us to deliver high
levels of performance that appeal all the more to customers when their budgets
are constrained. 

Our people, training and development programs are first-rate, and
improvements made in our operations, procurement, maintenance and planning
efforts are yielding solid benefits. The investments we have made in our fleet
in recent years will yield both a near and long-term competitive advantage.
Likewise, the health of our balance sheet and commitment to financial discipline
underpin our ability to weather the current drilling environment. In all, the
Noble team is well prepared for the challenge.
As we have done in the past, Noble will continue to define excellence in
offshore drilling. On behalf of the men and women of our team, I thank you for
your confidence and your investment in Noble.

David W. Williams
Chairman, President and
Chief Executive Officer 

