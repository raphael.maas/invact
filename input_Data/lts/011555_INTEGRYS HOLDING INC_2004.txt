                                                                               




         fellow
                                 DEAR



                                 SHAREHOLDERS



                                 "Energy to Believe In"


                                 Throughout life we find it necessary to believe in something. As children we believe that our
                                 fondest wish will come true. In our innocence, we delight in seeing only the desired outcome
                                 and none of the obstacles in our path. As children we trust that someone or something will
                                 know our heart's desires--even if we don't share our hopes and dreams with anyone--and
                                 our wishes will come true.

                                 As children, we believe the care and nurturing our parents provide will always be there for
                                 us. We believe that our teachers will teach us everything we need to know. We believe that
                                 if we work hard, we will have an honest chance to succeed as we grow older. Then we learn
                                 to believe in ourselves. And, finally, we learn to trust and believe in each other.



                                                                      Energy for
Belief also plays an equally important role in our industry.
Customers believe in our ability to supply their energy
                                                                      Environmental Stewardship
needs--to be there whenever they want light, heat, or
energy for a myriad of needs. They believe in our reliability
                                                                       We completed the sales and donation of the Peshtigo River
and our dependability in delivering energy in a safe and
                                                                       lands, transferring almost 12,000 acres of recreational
environmentally responsible manner and at competitive prices.
                                                                       lands for the future enjoyment of many generations.
Investors believe in our ability to manage our business well
                                                                       We signed an agreement to purchase 70 megawatts
and to provide them with reasonable returns and prudent risk
                                                                       of renewable energy, provided by wind generation, and
management. They believe we will continue to protect and grow
                                                                       collaborated with paper customers in using by-products
the value of their investments.
                                                                       as a renewable energy source.
Employees believe in their own abilities and the abilities of their
                                                                       We are building a highly efficient generating unit in
co-workers to get the job done, to do the right thing for our
                                                                       Weston 4, which will operate under stringent emission
customers and the communities we serve, and to work in an
                                                                       standards that are among the lowest in the nation for a
atmosphere that provides respect, opportunity, and safety for all.
                                                                       coal-fired unit and will make the plant one of the cleanest
                                                                       plants of its kind.
Communities and the public believe in our ability to make the
right decisions in energy matters, environmental stewardship, and

                                                                      Energy for Partnerships
community involvement. They have learned to trust us to be there
in their time of need and help resolve their immediate concerns.
                                                                       We signed a joint ownership agreement with Dairyland
Believing in someone or something first requires trust. A
                                                                       Power Cooperative for Weston 4. Joint ownership will reduce
big part of our job at WPS Resources is to build the trust
                                                                       the risk associated with ownership of such a large unit.
necessary for all stakeholders to believe in our willingness
and ability to contribute positively to today's society. That          We signed a long-term, comprehensive energy supply
contribution may be in the form of energy, community support,          agreement with Consolidated Water Power Company, a
employment opportunities, or just caring.                              subsidiary of Stora Enso North America, that will provide
                                                                       benefits for both organizations for many years to come.
Our employees have completed another year of building that
trust and enabling others to believe in us. They are continuing        We signed a long-term wholesale supply agreement with
to create energy to believe in. Let me illustrate a few examples       Wisconsin Public Power Inc., providing the communities
from 2004.                                                             they serve with part of their energy requirements.



Energy for the Future                                                 Energy for Innovative Products
   We began construction on Weston 4--the largest single               We developed portfolio optimization and risk mitigation
   electric generation project in our company's history.               capabilities for WPS Energy Services that allow us to
                                                                       provide new products and solutions for customers.
   We signed a contract with Calpine Corporation, doubling
   the amount of energy available to us from the Fox Energy            We welcomed Advantage Energy into the WPS Resources
   Center. This facility will add 470 megawatts to our                 family and began offering WPS Energy Services' products
   generating capacity for several years.                              to the New York retail markets.

   We completed a new facility housing our customer call               We offered new tariffs to our Wisconsin commercial and
   center. This facility will enable us to better serve our            industrial customers that provide options and enable them
   customers during adverse conditions.                                to better control their energy costs.




      "Customers believe in our ability
                        to supply their energy needs.
                                                    "

                                                                                                       We delivered a 13.2 percent total shareholder return 
                                                                                                       our investors who reinvested their $2.20 in dividends
     
                                                                                                       per share and held their WPS common stock from
         
                                                                                                       December 31, 2003, through December 31, 2004.


                                                                                                       The Wall Street Journal reported in their February 28, 2005,
                       
                                                                                                       edition that WPS Resources ranked 6th in shareholder
                      

                                                                                                       return over the past five years (2000 to 2004) out of a

                                                                                                       universe of 44 electric utilities.
     1,500                               100                          3,000

                                                                      2,500
     1,200                               80
                                                                                                       We reviewed and revised our corporate governance policies,
                                                                      2,000
                                                                                                       which led to top quartile scores by corporate governance
      900                                60
                                                                      1,500
                                                                                                       rating agencies.
                         $1,292.0


                                                                                                      Energy for Great
        0                                 0                              0

                                                                                                      Customer Relationships
     
                                                                                                       In customer satisfaction surveys done by us and others,
                                     WPS Energy Services, Inc.
                                                                                                       our customers have consistently given us high marks for
             Revenues                          Net Income                      Assets
                                                                                                       our performance.
             (Millions)                         (Millions)                     (Millions)
     4,000                               40                           1,200
     3,500                                35
                                                                      1,000                            We've worked hard on ensuring customer satisfaction, and
     3,000                               30
                                                                                                       this was once again proven when WPS Energy Services'
                                                                       800
  
                                                                                                       customers rated us 7th in overall customer satisfaction
     2,000                               20                            600
    

                                                                                                       and 6th by industrial customers in a 2004 national survey

                                                                                                       conducted by Mastio & Company. Since we first appeared in
                                                   
                                                                                                       the Mastio rankings in 1999, customers have consistently
        0                                 0                              0
                                                                                                       rated WPS Energy Services among the top 10 energy
        

                                                                                                       marketers in North America.

                                                                                                      Energy for Meeting


                                                                                                      Our Challenges

                          
                                                                                                       As part of our asset management strategy and in an 

                                          4                                                            to modify our risk profile for our customers, in 
                                                                                                       2003 we announced the pending sale of the Kewaunee
                                           2
        10                                                              50
                                                                                                       nuclear power plant. We've received all necessary
   
                                                                                                       approvals, except one. The Public Service Commission
                                          -2
                                                                                                       of Wisconsin initially rejected the sale, but granted our
                                                                                                       request for reconsideration.

                    "The energy within our organization
           is the energy you can believe in."
                                                                   The tone of an organization is set by senior management's
   As part of our asset management strategy and following
                                                                   actions and deeds much more than by their words. Our
   a shift in the markets in Pennsylvania, we announced the
                                                                   leaders must be those that our employees, the communities
   sale of our Sunbury generating facility. This sale has not
                                                                   we serve, the financial community, and our shareholders
   occurred as scheduled, but we are progressing with a sale
                                                                   believe will deliver the value they seek. So, in 2004, I made
   process using an investment banking advisor and expect
                                                                   a number of changes to my senior staff to bring to the forefront
   the sale to occur during 2005.
                                                                   strong models of trust and service.

Energy for Winning Strategies                                      This past year, we restructured the operations of WPS Energy
                                                                   Services and WPS Power Development to better capture
   We have developed a portfolio of regulated and                  synergies. Both companies are now under the leadership
   nonregulated businesses that have adhered to our core           of Mark Radtke as President.
   competencies of energy and energy-related activities.
                                                                   Phil Mikulsky is now Executive Vice President - Development for
   This combination has provided higher growth opportunities,
                                                                   the holding company and is responsible for our transmission team
   diversified our risk profile, and enabled us to provide
                                                                   and our expanded asset management and portfolio strategy.
   customers with more energy solutions.

                                                                   Tom Meinz is now Executive Vice President - Public Affairs.
   We are continuing to seek balanced growth of our
                                                                   Tom also serves as Chairman of the Board for Upper Peninsula
   businesses, placing greater emphasis on growth of
                                                                   Power Company, President for Wisconsin River Power Company,
   our regulated utility business while at the same time
                                                                   and Vice President for Wisconsin Valley Improvement Company.
   carefully growing our nonregulated subsidiaries.

                                                                   Wisconsin Public Service Corporation has two new Presidents
   We have maintained a conservative financial strategy
                                                                   in Charlie Schrock, President and Chief Operating Officer -
   and risk profile while growing the company and providing
                                                                   Generation, and Larry Borgard, President and Chief Operating
   investors with above-average returns.
                                                                   Officer - Energy Delivery.
   In the coming months, we will be taking steps to make our
                                                                   Upper Peninsula Power Company has a new Chief Executive
   operations work even more effectively and efficiently than
                                                                   Officer in Larry Borgard and a new President in Gary Erickson.
   they have in the past. We will be reaching for higher levels
   of competitive excellence through innovation and creativity.
                                                                   Bud Treml is now Senior Vice President - Human Resources
   Our goal is for every process in the company to be "best in
                                                                   for both WPS Resources and Wisconsin Public Service.
   class" in our industry. We expect that these efforts will
   result in higher standards of service for our customers,        Barbara Nick is now Vice President - Corporate Services for
   lower prices, and highly effective operations.                  WPS Resources and is leading our new initiative in technology
                                                                   and project management.

Energy for Leading the Future                                      These leadership changes are evidence of a wide-reaching
                                                                   effort throughout our companies to build the face of the future.
The challenges and opportunities we face, as individual
employees and as a business, are changing rapidly, and
                                                                   Long-Term Financial Goals
we are continually developing the management team and
the organization necessary for our continuing success.                 Grow our earnings per share from continuing operations
                                                                       at 6 to 8 percent on an average annualized basis.
Our focused succession planning process is one of the critical
initiatives that is helping us successfully manage our destiny         Achieve 15 to 25 percent of our earnings from our
in new environments. It is an ongoing assessment and                   nonregulated energy supply and services company.
development of our people, ensuring the necessary knowledge,
                                                                       Continue our moderate growth in the annual dividend paid.
skill, and ability to support our corporate strategy for growth.
It is the drive to create even more energy to believe in.
                                                                       Provide investors with a solid return on their investments.


Many other changes have occurred, as well, to place leaders
and their employees in a position where they can use their                 
strengths for the good of our customers and shareholders.
                                                                 4.00

We also have a number of new Vice Presidents and Assistant       3.50

Vice Presidents in other key areas:                              3.00

                                                                 2.50
     Brad Johnson, Vice President and Treasurer for
                                                                 2.00
     WPS Resources and Wisconsin Public Service
                                                                 1.50

     Bill Bourbonnais, Vice President - Transmission for         1.00


     WPS Resources                                               0.50


     Dave Harpole, Vice President - Energy Supply - Projects     0.00
                                                                        
     for Wisconsin Public Service

     Jim Schott, Vice President - Regulatory Affairs for
     Wisconsin Public Service                                                 
     Terry Jensky, Assistant Vice President - Energy Supply -    2.50

     Operations for Wisconsin Public Service
                                                                 2.00

     Howard Giesler, Assistant Vice President for
                                                                 1.50
     WPS Power Development

This new team has skill, knowledge, diversity, and drive         1.00

and is already creating even more energy to believe in.
A Company to Believe In                                          
Our company had a very successful 2004, and our success
is directly related to all of our employees. They are working
                                                                                      
to provide for our customers' current and future energy
requirements, protect our resources for future generations,
                                                                  350
and earn an even higher level of trust.
                                                                 300

We respect your trust and will never take it for granted.
                                                                  250
Thank you for believing in WPS Resources and choosing
                                                                 200
us for your investment. We're proud to provide you with
an energy company to believe in.                       

Sincerely,
                                                                 
Larry L. Weyers
                                                                
Chairman, President, and Chief Executive Officer                 
March 9, 2005


