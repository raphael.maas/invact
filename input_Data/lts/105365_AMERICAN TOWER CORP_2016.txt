To Our Shareholders
April 19, 2017
The mobile phone has emerged as an indispensable part of our daily lives. It has evolved from a simple
talk and text device to a multifunctional necessity capable of internet access, entertainment, travel,
health monitoring, document delivery and personal navigation, among countless other functions. As
these handsets and their capabilities have grown and evolved, so too have the mobile networks upon
which they rely.
American Tower provides the foundation of these mobile networks by developing, acquiring and
operating communications real estate and making it available for multitenant use. And, as one would
expect, as the capabilities of mobile devices have grown dramatically and steadily over the past two
decades, so too has our business. In 2016, we took important steps to expand and further diversify our
portfolio and position ourselves for long-term growth, including several strategic acquisitions and the
strengthening of our capital structure. Further, we grew our common stock dividend by approximately
20% and delivered another year of double-digit growth across key financial metrics.

After many years of disciplined, deliberate investment across a wide variety of markets at staggered
stages of wireless technology development, we are positioned as the only globally diversified
independent owner and operator of mission-critical communications real estate. We view this strategic
positioning as a crucial competitive advantage, particularly in an increasingly interconnected and mobile
world, and remain focused on maximizing the value of our existing asset base while continuing to seek
additional compelling investment opportunities. We expect these efforts will enable us to deliver strong
financial and operational performance for many years to come.
Leading wireless connectivity around
the globe
From our entry into Mexico in 1999 to our recent
acquisition of FPS Towers in France in February 2017,
we have expanded our global portfolio to encompass
approximately 147,000 sites in a total of 15 markets.
During 2016, we meaningfully enhanced the scale of our
business in a number of these markets, particularly through
the acquisition of a majority stake in Viom Networks
Limited in India. This transaction nearly quadrupled
the size of our portfolio in that country, positioning
American Tower as the largest independent tower
company in the most populous free-market democracy.
Importantly, with less than 2% of India�s population
connected to the internet via wired networks, mobile
devices are expected to be the sole method of accessing
the internet for most Indians.2 With just 8% of these
connections currently being made on a 4G network, the
mobile revolution, and anticipated explosion in demand for
mobile data that will accompany it, is just getting started.3
In fact, Indian smartphone subscriptions and total mobile
data traffic are projected to increase four-fold and
fifteen-fold, respectively, by 2021.4
 We expect this rapid
growth in mobile connectivity to result in significant
demand for our expanded real estate portfolio in India.

To help bridge the current mobile connectivity gap, the Indian government has established
the �Digital India� program in an effort to �transform India into an empowered society
and knowledge economy.�5
 At American Tower, we are proud to be key participants in this
initiative and expect to not only generate strong returns for our shareholders through our
Indian real estate, but to also help connect the unconnected by bringing mobile broadband
to the underserved populations who need it most.
As part of our contribution to the �Digital India� program, we have constructed 51 Digital
Village Squares across rural India, each of which includes a digital learning center and two
internet access kiosks in close proximity to local schools. As of year-end 2016, thousands
of children ages 6 to 14 have accessed the kiosks, many of them experiencing the world
of computing and the internet for the first time. We have also helped to certify nearly
400 people on how to use computers, the internet and various other electronic services in
partnership with the NIIT Foundation.
This training and newly available internet access is improving the lives of people in these
communities in very tangible ways. For example, a recent study conducted at a school with
a nearby Digital Village Square showed a 20% increase in students� computer literacy and
problem solving abilities attributed to educational content delivered there.6
 It is also our goal
to provide digital education to at least one adult family member in each of the households
served by these Digital Village Squares. To extend the reach of this important program, we
expect to increase the number of Digital Village Squares by nearly a third in 2017. 


Driving efficiencies
throughout the industry
As mobile connectivity continues to
expand and mobile usage increases
rapidly, our portfolio of physical capital,
including macro towers, small cell
systems and other communications real
estate, is well positioned to serve as a
key infrastructure solution underlying
this mobile revolution. Our real estate
assets have high structural capacity and
are in attractive locations to support
wireless network operators as they
further build out their mobile networks.
Meanwhile, our intellectual capital,
composed of the systems and processes
we have developed since our inception,
ensures the efficient management
of our asset base and seamless
integration of new portfolios in diverse
geographies. Finally, our organizational,
or human, capital, composed of the
highly skilled professionals we have
recruited and trained across all levels
of the Company, helps ensure we
continue to successfully manage our
business and adapt as required to
retain a leadership position within the
communications real estate industry.

Focused on delivering superior
results for our shareholders
The success of our long-term strategy is
demonstrated by the consistency of our
financial results. 2016, for example, was the
seventh consecutive year of simultaneous
double-digit growth in our Property Revenue,
Adjusted EBITDA and Consolidated AFFO per
Share. In addition to the strong sustained
growth in these key financial metrics, we have
increased our ROIC by approximately 90 basis
points since 2007. Further, we have more than
doubled our common stock dividend in just
the past five years, resulting in compelling total
shareholder return. Today, we believe we are
well positioned to continue to deliver attractive
growth rates and returns over the long-term.
A foundation of our financial capacity
to support this level of growth has been
our commitment to maintaining a strong,
investment-grade balance sheet and prudent
financial leverage, which was approximately
4.7x net debt to Adjusted EBITDA as of
year-end 2016. This financial strength,
paired with a proven, comprehensive capital
allocation strategy, will continue to play a
pivotal role in our future growth.

At American Tower, we continue to plan for and invest in the global proliferation of mobile
connectivity, driven by the belief that mobile technology is fundamentally transforming our world for
the better. Our comprehensive communications real estate portfolio, strong balance sheet, mutually
beneficial relationships with multinational carriers and commitment to help bring broadband internet to
billions of people across the globe have positioned us for long-term success. Our dedicated managers
and employees around the world, from Boston to S�o Paolo to Johannesburg to Paris to Delhi, are fully
prepared and tremendously motivated to deliver this success.
James D. Taiclet, Jr.
Chairman, President & Chief Executive Officer