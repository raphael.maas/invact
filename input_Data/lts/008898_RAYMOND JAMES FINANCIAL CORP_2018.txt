Ten years ago, we were in the midst of the worst financial crisis since the Great Depression.
Too many firms in our industry suffered from excessive leverage and overexposure to
illiquid assets, which caused many to fail and some to rely on government support through
the Troubled Asset Relief Program (TARP). Raymond James not only survived the financial
crisis but did so without a government lifeline. Our long-term success reflects our mission
of always putting the financial well-being of our clients first, which ultimately serves the
best interests of our shareholders, associates and communities.

That approach led to outstanding results in fiscal 2018. A decade
after the financial crisis we finished the fiscal year with our 123rd
consecutive quarter of profitability and record annual net revenues and
earnings. Record net revenues of $7.27 billion increased 14%, record
pre-tax income of $1.31 billion increased 42%, and record net income
of $856.7 million represented a 35% increase over fiscal 2017. Adjusted
net income of $964.8 million1
, which excludes the $105.3 million impact
of the Tax Cuts and Jobs Act, increased 26% compared to fiscal year
2017. Record financial results were driven by growth of client assets,
record investment banking revenues and the positive impact from
higher short-term interest rates. Client assets under administration
increased 14% during the year to $790.4 billion, another record,
lifted by equity market appreciation and the net addition of financial
advisors in the Private Client Group (PCG) segment. Our return on
average total equity for the year was 14.4%, very good given our strong
capital position throughout the year. Our shareholders� equity of $6.4
billion on September 30, 2018, increased 14% over September 2017,
resulting in a book value per share of $43.73.
During the fiscal year we increased our quarterly dividend twice,
moving from $0.22 to $0.30 per quarter. In addition, in November
2018 the quarterly dividend was increased to $0.34 per quarter,
resulting in 55% growth over a 12-month period. In May, our board
of directors also approved a $250 million securities repurchase
authorization. The firm repurchased nearly 3.17 million shares of
common stock for an average of $78.94 per share. With the prior
authorization fully utilized, the board approved an additional
authorization of $500 million in late November 2018. While we strive
to maintain ample levels of capital and liquidity, we are deliberate
about returning capital to our shareholders, as appropriate.
Turning to our segment results, the Private Client Group generated
record net revenues of $5.09 billion, an increase of 15% over fiscal
2017, and record pre-tax income of $576.1 million, a 54% increase
over 2017, which was negatively impacted by $130 million of
reserves associated with a legal settlement. Fiscal 2018 concluded
with records for PCG assets under administration of $755.7 billion
and PCG assets in fee-based accounts of $366.3 billion.
A significant driver of PCG results in 2018 was our ability to retain
and attract quality financial advisors who appreciate the unique
combination of a client-first focused culture along with robust
technology and product offerings. More than 7,800 advisors are
now affiliated with the firm. Fiscal year 2018 established a new
milestone for financial advisor recruiting with over $300 million
of trailing 12-month production joining the firm. In addition to
attracting experienced advisors, we continued investing in the future
by expanding our financial advisor training program, with over 250
associates starting the program during fiscal 2018.
That view to the future was also clear as we implemented an
important succession plan, with Chief Operating Officer Dennis
Zank announcing his retirement from that role and as leader of PCG.
We�re grateful for Dennis� 40 years of committed service to Raymond
James, where during his career he successfully led operations and
administration, and the Private Client Group. Most importantly,
he helped prepare the firm for continued success, including those
who took on his responsibilities, namely Raymond James Financial
Services (RJFS) President Scott Curtis, who we named head of PCG,
and Raymond James & Associates (RJA) President Tash Elwyn, who
expanded his role to become CEO of the RJA broker/dealer. In addition
to these changes, Jodi Perry replaced Scott Curtis as leader of the RJFS
Independent Contractor Division and was appointed to the Raymond
James Financial (RJF) Executive Committee, and Alex. Brown
President Haig Ariyan took on leadership of the firm�s Investment and
Wealth Solutions departments. I have utmost confidence in these
leaders and am pleased we have a strong team in place to continue
propelling our Private Client Group forward.
Notable for the firm and our industry this year was the reversal
of the Department of Labor�s fiduciary rule by a federal court of
appeals in June. While Raymond James has long advocated for a
uniform best interest standard, we as a firm and I personally were
outspoken against the DOL�s fiduciary rule and were pleased by
the outcome. We are encouraged by the SEC�s Regulation Best 

Interest proposal, which, once finalized, is expected to require the
same standard of care for all investors and account types. We will
implement any necessary changes with a consistent approach
to putting clients first, minimizing disruptions, and maintaining
flexibility and choice for advisors and clients.
Whatever the years ahead bring, I know with strong leadership in
place and a commitment to our advisors and their clients, PCG is well
positioned to continue its record of growth and success as we enter 2019.
Moving on to the Capital Markets segment, net revenues of $963.8
million and pre-tax income of $90.6 million declined 5% and 36%,
respectively, compared to fiscal 2017, in spite of record M&A results
that drove record total investment banking revenues of $440.8
million, up 11% over last year�s record. The strong M&A results
reflected a favorable market environment and the substantial
investments we made to strengthen our capabilities over the past
several years. The broader story for the segment was less positive.
Equity underwriting revenues declined 27% and institutional
commissions remained challenged in fiscal 2018, as the equity
business was negatively impacted by both structural and cyclical
factors, and a flattening yield curve hurt the fixed income business.
The Asset Management segment produced record net revenues of
$654.4 million and record pre-tax income of $235.3 million, increasing
34% and 37% over fiscal 2017, respectively, driven by financial assets
under management, which ended the fiscal year up 46% at a record
$140.9 billion. The increase in financial assets under management
reflected market appreciation and increased utilization of fee-based
accounts in PCG, with a large influx due to aspects of the now-defunct
DOL fiduciary rule, as well as interest in newer products such as
Freedom Foundations and environmental, social and governance
(ESG) portfolios, both of which offer professional management at
significantly lower asset levels than the segment�s typical offering.
Also meaningful was the addition of $27 billion of assets from the
acquisition of Scout Investments and Reams Asset Management
in November 2017. This acquisition, which significantly expanded
and diversified Carillon Tower Advisers� product offering, has been
successful to date with strong asset retention and a seamless
integration during the fiscal year.
Also of note for the year was RJF Executive Committee member
and Asset Management Group President Jeff Dowdle being named
as RJF Chief Administrative Officer, expanding his role over several
administrative areas of the firm.
For Raymond James Bank, record net revenues of $726.7 million
were up 23% and record pre-tax income of $491.8 million increased
20% over fiscal 2017. By continuing to focus on providing solutions
to clients in our PCG and Capital Markets segments, the bank�s loan
portfolio grew 15% to a record $19.5 billion during the year. Most
importantly, the bank�s credit metrics improved, with nonperforming
assets declining 36% and criticized loans declining 12%. Net interest
margin expanded 12 basis points to 3.22% in fiscal 2018, helped by
the increases in short-term interest rates, which more than offset the
growth of lower yielding assets such as residential mortgages to PCG
clients and the agency-backed securities portfolio.
Complementing the strong performance within our businesses, we also
achieved several other notable accomplishments during the fiscal year:
� Giving back to our communities is an integral aspect of our mission,
and we certainly exhibited that in fiscal 2018. Between associate
contributions and a company match, Raymond James raised more
than $6 million for communities across the country through its
annual United Way campaign. Additionally, our associates raised
more than $250,000 for the American Heart Association through
the 2017 Heart Walk, and during the firm�s annual Raymond James
Cares Month, more than 2,500 advisors and associates volunteered
over 7,250 hours to benefit 172 charitable organizations, reflecting
an 18% increase in the number of volunteer hours over 2017.
Raymond James also donated $150,000 to recovery efforts in the
areas most affected by Hurricane Michael and $250,000 to the
American Red Cross to support Hurricane Florence relief efforts.
� The firm appointed two new directors to its board of directors �
retail and consumer products veteran Anne Gates and information
technology executive Bob Dutkowsky � and both were named to
the Audit and Risk Committee. I am confident that Anne and Bob
share our firm�s values and will be excellent additions to our board
of directors.
� We also formed an Operating Committee � this committee is
comprised of executive leaders representing different business
units and diverse perspectives from across the firm and will work
with the Executive Committee to participate in decision-making
and setting strategy.
� We�re continuing our long-term focus on having a more inclusive
and diverse workforce, and received recognition for our efforts,
including the Bank Insurance & Securities Association 2018 BISA
Diversity Award, which annually recognizes successful diversity
efforts of organizations from the financial industry. InvestmentNews
also recognized the firm as a �Diversity Champion,� two of our
advisors � RJA�s Tony Barrett and RJFS� Joshua Charles � as �See
It Be It Role Models,� and EVP of Technology and Operations Bella
Allaire and financial advisor Sacha Millstone as �Women to Watch.�
� Raymond James was named to Fortune�s list of the World�s Most
Admired Companies and was also ranked 58th on Forbes� list of
America�s Best Employers.
� Chairman Emeritus Tom James was recognized by InvestmentNews
as one of two �Icons� of our industry, alongside Fidelity Investments
Chairman Emeritus Edward �Ned� Johnson III, on its 2017 list of
Icons & Innovators.
� Several Raymond James-affiliated advisors were recognized during
the year, including eight advisors named to Forbes� list of America�s
Top Women Advisors, 30 advisors named to the Financial Times �FT
400� list of top financial advisors, 55 advisors named to Barron�s Top
Advisors ranking, three advisors named to Barron�s list of the Top 

100 Women Financial Advisors, and 74 advisors named to Forbes� list
of America�s Top Next Generation Wealth Advisors.
� Raymond James earned numerous investment banking
awards during the year, including several awards from the M&A
Advisor, comprised of three �Deal of the Year� awards and five
�International Deal of the Year� awards.
In short, it was a superb year for Raymond James. We are entering
fiscal 2019 with records for our key revenue drivers, including client
assets under administration, financial assets under management,
the number of PCG financial advisors, and net loans at Raymond
James Bank, capping off a very strong run of growth in recent years.
However, we are very aware that maintaining our strong position
is reliant on continuing to look forward and being vigilant about
continuing to evolve our business for the needs of the future, while
remaining rooted in the values that have defined our firm for the
past 55-plus years.
That includes managing our business with a thoughtful, conservative
approach. Volatility in the S&P 500 index late in 2018 reminds us that
we are 10 years into one of the longest bull markets in history, and
we should always be prepared for an eventual prolonged downturn
in the equity markets. Furthermore, the industry is starting to
experience significant competition for client cash balances, which
could also create headwinds for our financial results, particularly
when short-term interest rates stop rising.
Nonetheless, I am confident about our ability to navigate these
potential challenges, growing our businesses with our clients at the
forefront while delivering superior returns to our shareholders over
the long term.
Thank you for your trust and confidence in Raymond James.
PRIVATE CLIENT GROUP
More than 7,800 financial advisors � affiliated as traditional
employees, independent contractors, independent registered
investment advisors or financial institution-based advisors �
provide financial planning, investment advisory and
securities transaction services.
CAPITAL MARKETS
Investment Banking, Public Finance, Institutional Sales and
Trading, and Syndicate serve corporate, institutional
nonprofit and municipal clients throughout North America
and Europe. The group also provides research on companies
globally, market-making in common stocks, and trading
primarily in municipal, government agency, mortgagebacked and corporate bonds. In addition, Raymond James
Tax Credit Funds provides resources to developers of
affordable housing and facilitates tax-incentivized
investments in communities through fund offerings.
ASSET MANAGEMENT
The Asset Management segment provides investment
advisory and related administrative services to Private
Client Group clients through the Asset Management
Services division and through Raymond James Trust, N.A.
The segment also provides investment advisory and asset
management services to individual and institutional
investors, including through third-party broker/dealers,
through Carillon Tower Advisers and its affiliates, which
also sponsors a family of mutual funds.
RAYMOND JAMES BANK
Raymond James Bank provides a comprehensive array of
personal and corporate banking services including
residential, securities-based and commercial lending
products, as well as FDIC-insured deposit accounts that
serve as one of the primary sweep options for client
brokerage accounts.
OTHER
The Other segment includes the firm�s private equity
activities, as well as certain corporate overhead costs of
Raymond James Financial, such as the interest cost on our
senior notes payable, and the acquisition and integration
costs associated with certain acquisitions.
Paul C. Reilly
Chairman and Chief Executive Officer
Raymond James Financial
December 14, 2018