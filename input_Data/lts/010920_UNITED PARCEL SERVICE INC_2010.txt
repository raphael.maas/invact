After more than two years of economic turmoil we saw signs of recovery in 2010.
Emerging from the recession as a stronger and more responsive organization, UPS was
uniquely positioned to benefit from the improving business conditions. The result was a
9.4 percent increase in revenue to $49.5 billion and adjusted* diluted earnings per share
soaring 54 percent. Our business strategy was driven by three tenets: creating value for
customers, transforming our business, and investing for growth.

During the year, UPS implemented a major restructuring plan
for its U.S. operations and completed several ambitious infrastructure
projects. We continued to expand into emerging
international markets as well as introduce industry leading
products to customers around the world.
The tremendous financial performance that UPS realized
during 2010 was a direct result of tough decisions made
during the recession and the superb execution by the UPS
team. We experienced balanced growth across all business
segments and achieved record profit levels in International
Package and Supply Chain and Freight.
Looking back, I am proud of our many accomplishments.
Here are some of the highlights from 2010:
Creating Value
Advancements in our industry-leading technology allowed
us to introduce new products and services in 2010 that
simplified and improved customers business processes.
 To help our customers better manage returns in the United
States, we now offer UPS Returns Flexible Access, a low
cost and high quality service that provides unmatched
alternatives to customers.
 We introduced UPS Smart Pickup, a service that saves
money by eliminating scheduled stops that are unnecessary
and routes unscheduled ones to the nearest driver.
 In April, we released the first-to-market mobile application
for Google AndroidTM smart phones, adding to our
comprehensive suite of applications for mobile devices.
 For our International customers, we introduced UPS
Import ControlSM, a solution that allows them to better
manage inbound shipments.
 We continue to develop new innovative service
offerings in the Ocean Freight market, and in 2010,
we introduced UPS Preferred LCL Ocean Freight. This
service offers less-than-container load Ocean Freight
customers improved shipment visibility and day-definite
delivery commitments up to 20 percent faster than
traditional services.
Transforming to Compete
In 2010, small package operations in the United States were
transformed to serve our customers more effectively and
ensure our future success. Now organized along strategic
market lineswith resources closer to customerswe are
focused on what is necessary to compete and win.
Technology continues to transform our superior global
network. For example, Telematics software has been
implemented in approximately one third of our U.S. package
car fleet. This technology not only increases productivity and
reduces cost, but it also improves safety.
Investing to Grow
In 2010, UPS completed significant infrastructure projects
that have strategically enhanced our global network.
And, we invested in technology that has further improved
operational efficiency.
 We completed the expansion of Worldport, our global
air hub, ahead of schedule and under budget. This
creates substantial network operational improvements
and allows UPS to fly larger, more fuel-efficient aircraft.
 We began operating our new intra-Asia air hub in
Shenzhen, China, allowing us to better serve our
customers by reducing time in transit for hundreds of
city pairs in the region.
We also developed strategic alliances in Asia to further
strengthen the reach of our global network and expand our
service offerings.
 New relationships were established with local partners
in the key emerging markets of Vietnam, Malaysia,
and Indonesia. These alliances allow us to expand the
coverage of our global network.
 We submitted an application for a domestic service license
in China. This is the next logical step in our strategy
for expansion in the Chinese marketplace.
These investments will enhance the service we give
customers, provide better access to international markets,
and improve efficiencies in the UPS network.
Recognition
Throughout the year, UPS was recognized for leadership in a
variety of areas.
 UPS was named most admired in its industry by
Fortune and one of the Top 10 most reputable companies
by Forbes magazines.
 United Way recognized UPS and its employees as
the first company ever to total donations of more than
$1 billion.
 We were honored when President Obama asked UPS
to serve on his Export Council, working with the
administration and business leaders to help achieve the
objective of doubling U.S. exports in the next five years.
 Our efforts in sustainability were recognized by
numerous organizations in 2010, including the Climate
Innovation Index, the Climate Counts Scorecard, and the
Dow Jones Sustainability Index.
Other Significant Events
 Last fall, UPS announced a new global communications
platform. Given the complexities of todays marketplace,
we designed our advertising campaign to spotlight the
power of logistics. Our experience and worldwide capabilities
make UPS uniquely qualified to provide solutions
to customers throughout their supply chains.
 In 2010, we opened our first Olympic Logistics Center
in London. As the Official Logistics and Express Delivery
Supporter of the London 2012 Olympic and Paralympic
Games, UPS has an excellent opportunity to showcase its
capabilities to the world.
Operations Review
Global Package Operations
Improving economies around the world and market gains
led to total package volume growth of 3.4 percent to
3.94 billion packages.
 In the United States, we experienced slow but steady
improvement as customers rebuilt and efficiently
managed their inventories. Strong pricing and effective
management of our network drove a 45 percent increase
in adjusted* operating profit.
 The strength of our International operating model was
evident as the average daily volume in that segment
jumped 14 percent over last year. Operating profits
climbed 39 percent to $1.9 billion, a new record.
During the year, U.S. and International operations showed remarkable
adaptability in the face of a challenging economic
environment. In the first quarter of the year, we announced a
significant restructuring of our U.S. operations. These changes
will improve operational efficiency and allow us to better
serve our customers with our go-to-market strategy.
The International segment showed the strength of our balanced
global network and portfolio. We experienced strong
growth in all regions of the world in 2010. In Europe, we saw
impressive growth throughout the year despite the difficult
economic environment.
Supply Chain and Freight
The Supply Chain and Freight segment saw revenue soar
16.5 percent, with total adjusted* operating profit in this
segment up 95 percent to $577 million, a new UPS record.
This developing segment experienced an improvement in
adjusted* operating margin to 6.7 percent in 2010, a 270
basis point jump over last year.
The freight forwarding business unit rebounded in 2010 as a
result of improved market conditions and increased demand.
Revenue management initiatives and more competitive
block-space agreements were key contributors to the success
of the unit.
Logistics saw solid gains as we managed supply chains
for an increasing number of customers. UPS continued to
expand third-party logistics services in the high tech and
healthcare industries.
UPS Freight experienced increases in tonnage and shipments
during the year, which allowed this unit to return to
profitability in 2010.
Financial Strength
UPS is proud of its strong balance sheet and ability to generate
superior free cash flow. In 2010, we made a decision to
take advantage of the record low interest rates available and
issued $2 billion in debt. The proceeds from this transaction
were used to make discretionary contributions to our pension
plans. This transaction was balance sheet neutral and a
great move for UPS employees and shareowners.
Our Community
It would have been easy to respond during the recession
with a heavy hand aimed solely at cutting costs and jobs.
Instead, UPS was prudent and conscious of the responsibilities
that we have to our more than 400,000 employees,
the thousands of communities in which we operate, the
customers we serve, and the shareowners who invest in us.
Our decision-making was led by principles that have served
us well for 103 years: a balance of economic prosperity,
social responsibility, and environmental stewardship.
For example, in 2010 The UPS Foundation invested more
than $95 million in charitable giving around the world,
continuing to place great emphasis on helping global relief
organizations improve their disaster response through more
efficient logistics. UPS employees also donated 1.5 million
hours of volunteer time.
Our strategy during the economic recovery remains
the same: focus on our customers and employees,
grow the business while remaining competitive, and
operating responsibly.
Outlook for 2011
I remain optimistic about the global economy in 2011 but
expect to see economic expansion develop faster in certain
regions than others, with UPS ready to take advantage.
Our extensive global network and unique product portfolio
provide UPS customers with the unmatched solutions for
growing their businesses.
In 2011, UPS expects to implement a more aggressive
share repurchase strategy, increasing to approximately
$2 billion. We will continue to invest in opportunities for
growth and plan to increase our capital expenditures to
$2.2 billion. In January 2011, we used cash to make an
additional $1.2 billion in accelerated pension contributions.
As a result, our plans are more than 100 percent funded.
And in February, we announced an 11 percent increase in
our dividend from $0.47 to $0.52 per share.
I look forward to another strong year of earnings growth
in 2011. As a result, UPS is expected to surpass previous
highs for earnings per share. The company anticipates
2011 diluted earnings per share to increase between 16
and 22 percent over 2010 adjusted* diluted earnings per
share to a range of $4.12 to $4.35.
Stronger Than Ever
It was hard to imagine in the midst of the great recession
in 2009 that we would be anticipating record earnings per
share in just two short years. I am proud to say that UPS has
emerged from the recession as a much stronger company.
With the best financial and competitive position in the
industry, we are a company with a rock-solid balance sheet,
strong earnings growth, and tremendous free cash flow.
Yes, 2010 was a great year for UPS, and I am confident that
2011 will be even better, as we further implement the three
tenets of our strategy. We will continue to create value
for our customers, transform to strengthen our leadership
position, and invest in key markets and new opportunities.

D. Scott Davis
Chairman and Chief Executive Officer