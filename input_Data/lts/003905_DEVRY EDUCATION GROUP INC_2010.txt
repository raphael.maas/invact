Fellow Shareholders, Students, Coworkers, and Friends,

we executed on our Achieve, Grow, and Build strategy and drove unprecedented
student success.
Achieve Superior Student Outcomes and
Unmatched Student Services
Among our academic successes this past year:
 DeVry University graduates continued to excel in the job market. For the
most recent period, 88 percent of DeVry University graduates in the active
job market
were employed in their fields within six months of graduation.
 Ross University School of Medicine students achieved a 93 percent pass rate*
on the United States Medical Licensing Exam, on par with the pass rate of
U.S. medical school students.
 Chamberlain College of Nursing Bachelor of Science in Nursing graduates
achieved a first time pass rate on the National Council Licensure Exam
(NCLEX) in excess of 90 percent in 2009, exceeding the national average for
all nursing schools.
 Eight out of nine eligible Carrington Colleges campuses qualified for Honor
Roll status during a reaffirmation of accreditation by the Accrediting Council
for Independent Colleges and Schools (ACICS).
 In 2009, nine out of 10 of the highest scoring students on the CPA (Certified
Public Accountant) exam, prepared for the exam through Becker Professional
Education.
These and many more positive results would not have been possible without
the motivation of our more than 120,000 degree-seeking students, and the hard
work and dedication of our 12,000 employees.
Grow and Diversify Our Educational Programs
In 2010, DeVrys overall revenues totaled more than $1.9 billion, a 31 percent
increase over 2009. Our net income grew by 69 percent, to nearly $280 million.
Our strong financial results this fiscal year enabled us to make investments of

$826 million in academics and educational
services. We invested an
additional $131 million in other academic
quality initiatives such as patient
simulators, classroom technology,
and the creation of student service
programs that help our students
graduate and find career success.
The Business, Technology, and
Management segment (DeVry University
and its Keller Graduate School
of Management) delivered revenue
growth of 28 percent over last year.
Our commitment to academic quality
led to double-digit enrollment growth,
strong retention rates, and operating
leverage. Online enrollments grew
24 percent.
As we continue to build capacity to
help meet U.S. needs for healthcare
professionals, revenue in our Medical
and Healthcare segment (Ross University,
Chamberlain College of Nursing,
and the Carrington Colleges Group)
increased 40 percent and net income
by 21 percent. Chamberlains enrollment
growth was especially strong,
as students took advantage of
Chamberlains new locations and
programs.
Apollo College and Western
Career College were renamed
Carrington College and Carrington
College California, respectively.
With ongoing challenges in the
financial services sector, results for
our Professional Education segment
(Becker Professional Education)
remained relatively flat this past year.
We expect to see improved results
as the economy recovers. In the meantime,
Becker continues to invest to
position itself for long-term growth.
For example, we made a major investment
in the redevelopment of our
CPA exam material that responds to
changes in the exam for 2011.
Our fourth segment, Other Educational
Services (Advanced Academics
and DeVry Brasil), delivered revenues
of $59.8 million. Advanced Academics
added 60 new school district partners,
helping more than 22,000 students
pursue their goal of graduating high
school. We successfully integrated
Fanor in Brazil, and brought Fanors
three schools together under the DeVry
Brasil name. Over the long term, we
see growth opportunities through
new programs, new locations, and the
development of an online platform to
serve DeVry Brasil students.
Build the Infrastructure
to Support Growth
and Quality
A key to sustainable growth and
quality student outcomes is building
the supporting infrastructure. This
includes investing in technology and
human resources.
The rollout of our new student
information system, Project DELTA,
will enable us to better and more
rapidly
respond to students needs
with world-class service.
Aspiring to be the Employer of
Choice in education, we invested in
attracting and retaining top caliber
talent with new leadership development
programs and Live Well, a
holistic employee wellness program.
We also strengthened our management
team. William Hughson,
formerly of DaVita Inc., a leading
provider of dialysis services, joined
DeVry as President of our growing
Medical and Healthcare Group.
In October 2010, Steven Riehs, previously
President of DeVry Online
Services, will lead a new organizational
structure that includes Advanced
Academics, Becker Professional
Education, and DeVry Brasil.
Thomas Vucinic, President of Becker
Professional Education, will retire
after 14 years of distinguished leadership
at the end of calendar 2010.
Under his direction, Becker has
diversified from its core CPA exam
reviews into CFA (Chartered Financial
Analyst), CPE (Continuing Professional
Education), and PMP (Project
Management Program) exam reviews.
John Roselli, currently Senior Vice
President of Business Development
& International for DeVry Inc., will
become Beckers President.
We also welcomed Darren Huston,
Corporate Vice President of Consumer
& Online at Microsoft, to DeVrys
Board of Directors, and Dr. Richard
Carmona, the 17th Surgeon General
of the United States, to the Board of
Trustees at Ross University.
After many years of dedicated
service,
Charles Bowsher, retired
Comptroller General of the United
States, and Robert McCormack, former
Under Secretary of the Navy, retired
from our Board of Directors. We are
indebted to Chuck and Mac for their
significant contributions, helping us
become the strong organization we
are today.
Meeting the Future 
One Student at a Time
It is projected that by 2018 more than
half of all jobs will be in the fields of
business, education and healthcare.*
DeVrys schools are well positioned
to educate the workforce of tomorrow.
Our diversified array of programs,
from technology to business,
healthcare, professional education,
K12, and international, will serve us
well in good times and in bad.
As we strive to meet societys educational
needs, we will continue to be
guided by one constant: the success
of the individual student. It is through
the fulfillment of that commitment
that we will Achieve, Grow, and Build
the DeVry of the future.
Thank you for your continued
support.
Daniel Hamburger
President and Chief Executive Officer