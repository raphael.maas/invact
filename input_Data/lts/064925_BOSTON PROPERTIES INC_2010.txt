
Throughout our history, Boston Properties has followed a strategy of combining high-quality, iconic assets in supply-constrained markets
populated with businesses that are focused on recruiting and retaining talented employees. This strategy has enabled us to become a premier
owner, manager, developer and acquirer of Class A office buildings in five regions in the United States where these conditions exist. Our assets
share a group of common traits that serve us very well in both strong markets and under more challenging economic conditions. We have
worked to lease our buildings with the strongest tenants in each particular submarket with the belief that these companies or institutions
will thrive and potentially grow even during slower economic conditions. Developing, owning and investing in the best assets gives us the
opportunity to attract tenants as well as service providers and employees that want to be associated with our company.
We have focused our capital and our attention on expanding our portfolio in a manner that is consistent with our existing activities. In 2010,
even in the face of limited job growth and absorption of office space in our markets, we were able to complete a significant number of
transactions involving our operating portfolio and make some new investments that will strengthen our regional portfolios, enhance our
future operating cash flow, provide opportunities for growth and solidify our balance sheet.
Over the last 18 months, we actively pursued the acquisition of a number of high-quality assets where high levels of leverage in the assets
capital structures combined with the sharp decline in operating fundamentals presented attractive repositioning and leasing opportunities as
we anticipate the markets recovery. While the means of acquiring assets was more diverse in 2010, each of our acquisitions fit squarely within
the overall quality and locational attributes of our portfolio in our existing regions. In total, Boston Properties made $1.5 billion of investment
commitments during the year. The highlights are as follows:
510 Madison Avenue is a 350,000 square foot newly constructed 30-story office building located in the heart of Manhattans Plaza District.
It is the only new building built in this area of midtown in the last decade. We expect it to achieve some of the highest rents in New York City and
be a building of choice for small financial service firms. Since the acquisition in September 2010, we have completed five leases totaling
43,000 square feet at rents significantly greater than our original expectations.
The John Hancock Tower is a 1.7 million square foot, 62-story office tower located in Bostons Back Bay market and is highlighted on the cover
of this annual report. The building features panoramic 360 degree views and is undergoing a major capital improvement program. Along with
our ownership of the 2.6 million square feet of space at the Prudential Center, we own 38% of the Class A Back Bay office market and are the
only owner of space above the 27th floor. The Back Bay has recently outperformed the overall Boston CBD market place and continues to be a
highly desirable location for many of the growing tenants in the Boston market.

Bay Colony Corporate Center is a four building, one million square foot Class A office
complex located in Waltham, Massachusetts. Bay Colony was once the premier asset in
the market until its owner experienced financial difficulty and was unable to invest
capital to retain tenants. We have begun a multi-year, multi-million dollar repositioning
and reinvestment program to bring the asset back to its former position in the market.
We now own more than 3.2 million square feet of space in 13 buildings in this submarket.
500 North Capitol is a 176,000 square foot building near the U.S. Capitol Building in
Washington, DC. We purchased a joint venture interest in the asset and reached a new
agreement with the existing partners in the building to allow for joint decision making.
The building will be redeveloped upon the vacancy by its sole tenant in 2011. Within six
months of taking ownership, our regional team was able to procure a 171,000 square foot
long-term lease with an international law firm and achieved approval to expand the
building by an additional floor to bring its total size to 231,000 square feet.
We bring a unique approach to working with existing and new tenants and their
brokerage representatives, whether it is in a leasing context or with ongoing property
management. In 2010, our regional operating teams completed more than 6.5 million
square feet of leasing, which represented 1.5 million square feet more than our previous
high which occurred in 2007. Our activity was dominated by Boston and Washington, DC,
with each leasing 2.4 million square feet and where many of our transactions involved
long-term commitments by new tenants on space that was to expire in 2011 and 2012.
Our leasing activity in New York City was limited by the lack of vacancy and near-term
availabilities across the New York portfolio. As we began 2011, the dialogue with tenants
interested in our development project at 250 West 55th Street has increased. Activity in
the San Francisco market lagged behind our other markets in 2010, but is now showing
signs of increased activity in the CBD and the suburban markets. Some of our more
noteworthy leasing transactions were as follows:
In Reston, Virginia, we won the competition for the 523,000 square foot lease for the
Defense Intelligence Agency consolidation from the Department of Defense. They
signed a 20-year lease on two buildings that were occupied by Lockheed Martin for
the past 20 years and that have 2011 and 2012 lease expirations. Also in Reston, we
completed an additional 475,000 square feet of leases to renew or replace tenants
with 2011 and 2012 expirations in our Reston Town Center portfolio.
In Boston, we completed a 306,000 square foot lease with Massachusetts Financial
Services (MFS) to replace a 207,000 square foot October 2011 lease expiration at
111 Huntington Avenue. This transaction required the relocation and long-term
extension of three other tenants totaling 185,000 square feet. In the Boston suburbs,
we completed a ten-year, 320,000 square foot renewal with Parametric Technology
Corporation in Needham, and in Chelmsford we signed a ten-year, 220,000 square foot
lease with Zoll Medical. In Cambridge, we completed major extensions and expansions
with Google, EMC and VMware totaling 221,000 square feet, and a new lease with
Microsoft for 113,000 square feet.
In New York City, we completed a 15-year, 190,000 square foot renewal and expansion
with Katz Communications at 125 West 55th Street, a 108,000 square foot lease with
the law firm Freshfields Bruckhaus Deringer at 601 Lexington Avenue and more than
605,000 square feet of leasing in other assets in 58 separate transactions.

As we began 2011, we completed the construction of two very significant new mixeduse
developments that will stabilize over the next 12 months and generate additional
long-term income. In January 2011, Wellington Management took occupancy of its
450,000 square foot premises at Atlantic Wharf in Boston, which is a 31-story tower.
We are now 79% leased on this 790,000 square foot office project and will be delivering
86 residential units this summer. In Washington, DC, the first tenant moved into
2200 Pennsylvania Avenue, our 460,000 square foot project which is now 85% leased.
The 50,000 square foot retail component and the 335 apartments will be opening this
spring. These two mixed-use projects represent $1 billion of new investment.
Our capital markets team was also very active during the year and we were able to
refinance and extend a number of secured and unsecured debt facilities in a very
favorable interest rate and credit environment. We completed two 10.5 year unsecured
bond deals totaling $1.55 billion. The first transaction in April had a coupon of 5.625%
and the second offering completed in November had a coupon of 4.125%. In conjunction
with these financings, we repaid $502 million of secured mortgages with maturities of
under one year and redeemed $700 million of our senior notes that mature in 2013 and
$236 million of our exchangeable senior notes that the holders have the right to cause
us to repurchase in 2012. In addition, we completed three ten-year financings and two
five-year loans to replace expiring secured loans on our joint venture properties
totaling $720 million. We ended the year with total consolidated debt of $7.8 billion, a
total consolidated market capitalization of $21.8 billion, available cash on our balance
sheet of $479 million and virtually full availability of our $1 billion line of credit.
Since becoming a public company in 1997, Boston Properties has produced an
annualized total return for its shareholders of 16%. In 2010, we had a very strong year,
achieving a total return to shareholders of 32%. Over the past five years, our total
return to shareholders has been 52%, ranking us first within a selective office REIT
peer group that we use in assessing performance, whose average total return to
shareholders for the same period was -0.49%. Over the same period, the total return
to shareholders for the entire NAREIT Equity REIT Index was 16%. We are pleased
to have paid $2.00 per share in regular dividends and are proud of our financial
performance as we continue to take actions that will position us for growth in the future.
Finally, we would like to acknowledge and thank each of our employees for their
contributions to our record leasing year, large-scale acquisitions, development
completions, capital markets activities and the maintenance of the highest quality
portfolio of office buildings. Our continued success is attributable to their expertise,
thoughtfulness and dedication to Boston Properties, as we are recognized as one of
the most successful office owners, developers and managers in the United States.