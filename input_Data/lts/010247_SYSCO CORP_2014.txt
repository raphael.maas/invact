
To Our Shareholders

Syscos transformational journey had a two-part storyline in fiscal 2014. One centered on how we remained focused on continuing to support the daily needs of our customers. The other was about continuing to ensure our customers and companys long-term success with the largest proposed merger in Syscos history.

On the business side, the year reflected a period of significant challenges, tremendous change, and solid progress for Sysco. Financial results in the first eight months were affected by a sluggish economic environment that included one of the harshest winters on record. Market conditions in the final four months improved as general economic conditions picked up. In the face of these challenges, our 50,000 associates continued to demonstrate their commitment to helping our 425,000 customers succeed.

During the year, Sysco sales grew 4.7 percent to a record $46.5 billion on a 3.4 percent increase in case volumes. Net earnings and adjusted net earnings(1) were $1 billion. Our gross profit of $8.2 billion was a 2.3 percent increase compared to the same period a year ago. Cash flow from operations was $1.5 billion. We meaningfully reduced our operating costs per case in the North American broadline business. We returned nearly $670 million in dividends to shareholders and increased our dividend for the 45th time in our 44-year history.

On Dec. 9, 2013, we announced our intent to merge with US Foods, the second-largest foodservice distributor in the United States, in an estimated $8.2 billion transaction. Pending completion of a Federal Trade Commission review, the proposed merger will provide substantial benefits to our customers and help us achieve more scale and efficiency in an evolving and highly competitive marketplace. We expect to achieve benefits totaling at least $600 million over a three- to four-year period.

While a substantial amount of effort went into the actions driving each storyline, our collective perseverance was rooted in our leadership teams and associates ability to achieve against our long-term, five-point strategy. In fiscal 2014, heres how we made progress on this strategy:
Profoundly enriching the experience of doing business with Sysco

In an environment of modest underlying industry growth, we focused on deepening our local and national customer relationships. We launched the Ingredients for Success sweepstakes promotion through our relationship with the Food Network and the show Restaurant: Impossible, resulting in the participation of 17,000 U.S. customers. The grand prize winner received $10,000 in Sysco credit and a visit to their restaurant from celebrity chef Robert Irvine. In June, Sysco hosted many customers at the FARE 2014 industry conference in Grapevine, Texas, where they received a high-touch experience of business seminars, food shows, cooking demonstrations and more. Our local sales teams also launched three nationwide customer blitzes, which provided a lift in new sales revenue and opened the doors to numerous new customer relationships.
Continuously improving productivity in all areas of the business

In the fiscal year, we made substantial progress in optimizing the areas of delivery routing, fleet, and equipment with the goal of gaining future efficiencies. In addition, we successfully deployed the SAP enterprise resource platform to five operating companies and added two more shortly after the years close, for a total of 12 companies now on the system. As we have made enhancements along the way, each implementation has been progressively smoother and the technology has performed well. Our focus in the next fiscal year is to implement an SAP software version upgrade and then to continue deploying additional functions and operating companies.
Expanding our portfolio of products and services by initiating a customer-centric innovation program

We continued to implement our category management initiative in the past year. We focused on enhancing our execution by developing and implementing new field-ready sales tools, enhancing our customer service in support of the category wave rollouts, and improving communication and coordination with our local sales teams. Nearly 100 product categories were activated, and we anticipate launching the remaining categories in scope by the end of fiscal 2015. On the Sysco Ventures front, more than 2,000 customers are now using our technology solutions, which utilize a suite of applications to interact with diners and that help our customers run their restaurants. And through our ongoing Customers 1st work across the business, we continue to receive valuable insights into our category rollouts and what our customers need to succeed.
Exploring, assessing and pursuing new businesses and markets

In addition to the proposed merger with US Foods, we continued to fill out our service footprint in the fiscal year with some strategic acquisitions. Among them was an agreement to acquire a 50-percent stake in Mayca Distribuidores S.A., a leading food distributor in Costa Rica. Domestically, we also launched a comprehensive effort to authentically serve fast-growing ethnic market segments, beginning with a focus on the Hispanic segment.
Developing and effectively integrating a comprehensive enterprise-wide talent management process

Results from our third Sysco Speaks survey indicate that our leadership team is making improvements in listening to and engaging with associates. The survey feedback ultimately allows us to develop and refine tools and business processes to help everyone better serve and support our customers. Additionally, with the anticipated completion of the proposed merger with US Foods, we envision leveraging and blending the best industry talent from both companies to partner in our customers success.

The year ahead will be a continuation of these transformational themes: growing the core business, streamlining operations, optimizing and managing our business costs, and beginning the integration of the proposed merger with US Foods. The fundamental elements for success are in place. While we have much work ahead of us, we believe that we are making the right strategic investments to strengthen Syscos leadership position in the industry and to be our customers most valued and trusted business partner.


Jackie Ward
Chairman of the Board

Bill DeLaney
President and Chief Executive Officer 