To Our Shareholders
Pattersons overall performance was very good in fiscal 2011 despite coping with the challenges presented
by a persistently sluggish economy. For the year ended April 30, 2011, consolidated sales rose 6% to
$3.4 billion, while net income also increased 6% to $225.4 million or $1.89 per diluted share. We were
particularly encouraged by the sales momentum that our businesses generated in the fourth quarter, which
makes us optimistic about Pattersons prospects as we enter our new fiscal year.
During the past year, we generated additional shareholder value by implementing an expanded capital
allocation program. We initiated a quarterly cash dividend of $0.10 per share in the fourth quarter of
fiscal 2010. Our board of directors voted to increase the dividend to $0.12 at its fourth quarter meeting
in fiscal 2011. Also at that time, the board replaced our existing share repurchase program with a new 25
million share, five-year buyback authorization. Using internally-generated cash, we repurchased 3.3 million
shares in fiscal 2011, returning almost $100 million to our shareholders. When you add this amount to
our dividend, we returned nearly $150 million to our shareholders in fiscal 2011. We believe our strong
earnings potential, financial position and cash flow provide us with the financial flexibility to continue
making substantial investments in our dental, veterinary and rehabilitation businesses, while enabling us to
support our dividend and share buyback programs.
Patterson Dental
Sales of Patterson Dental, our largest business, increased 3% in fiscal 2011 to $2.2 billion. Among our
three businesses, Patterson Dental was most affected by the sluggish economy. This was most clearly
evident in the fluctuating sales of basic and new-technology dental equipment during the year as many
dentists adopted a cautious stance toward making capital investments in their practices. However, sales of
consumable supplies strengthened as the year progressed, a key development since we view consumable
sales as a leading indicator of the overall dental market. This market strengthening, coupled with the
implementation of additional marketing programs, resulted in a strong rebound in sales of dental equipment and
software in the fourth quarter, paced by mid-teens sales growth of new-technology equipment, including CEREC
dental restorative systems and digital radiography products. As a result of improving market fundamentals and our
sustained marketing efforts, we believe Patterson Dental is rebuilding sales momentum in its dental equipment
business.
Patterson Dental is North Americas leading provider of dental equipment by a significant margin with an estimated
40% share of this key market. Our share of the market for new-technology equipment is even greater, particularly
in the area of CAD/CAM dentistry as embodied by the CEREC system. This is significant in terms of Patterson
Dentals long-term prospects, in view of the ongoing decline in the number of dentists per capita. Given the resulting
growth of patient case loads, dentists are facing a critical need for strengthening the productivity of their practices.
New-technology dental equipment enables dentists to do exactly thattreat more patients during the day, while
simultaneously improving clinical outcomes. Upgraded CEREC software that will become available later this summer
will make the system easier to use while offering improved productivity.
Dentistry, like many other fields, is undergoing a digital conversion, and as practitioners with limited office support,
dentists require an organization that can guide them to the best digital outcomes. The Patterson Technology
Center supports all aspects of the dental digital conversion, from software development and system integration to
maintenance. In support of our technology offerings, our new, 100,000-square-foot Patterson Technology Center
is slated to open in September. Meeting the technology needs of more than 80,000 customers today, the Patterson
Technology Center provides us with an unparalleled strategic advantage in the dental marketplace.
Patterson Medical
Sales of Patterson Medical, our rehabilitation supply and equipment business, rose 18% in fiscal 2011 to $504.7
million. Internally-generated sales, which attained planned levels, increased 4% for the year, while the June 2010
acquisitions of the rehabilitation businesses of DCC Healthcare accounted
for the balance of the units sales growth. Patterson Medicals results
in fiscal 2011 were affected by budgetary constraints imposed by the
British government on healthcare expenditures, but the impact of this
situation is lessening. Despite these austerity moves, Patterson Medicals
U.K.-based Homecraft operation continued to grow during the year.
During the past year, Patterson Medical was strongly focused on
integrating the DCC businesses into its Homecraft unit, which
virtually doubled the size of our U.K. operation. The DCC transaction
has significantly strengthened and expanded Patterson Medicals
international operations. The three acquired businesses rank among
the leaders in their respective markets, providing assistive living
products and rehabilitation equipment and supplies to hospitals,
physical and occupational therapists, long-term care facilities, dealers
and consumers in the U.K., continental Europe, Australia, New
Zealand and other international markets. In addition to expanding our
position in the U.K.s physical and occupational therapy markets, this
acquisition brought a stable of trusted and established brand names.
Through this acquisition, Patterson Medical also acquired a product
sourcing team in China.
For the past several years, Patterson Medical has been steadily
strengthening its value-added platform through its network of branch
offices, growing sales force and growing range of consumable supplies
and equipment. We are encouraged by Patterson Medicals fiscal 2011 performance and believe this unit is well
positioned, domestically and internationally, as an ongoing growth driver for our consolidated performance.
Webster Veterinary
Sales of Webster Veterinary, our companion-pet veterinary supply and equipment business, increased 5% in fiscal
2011 to $674.9 million. During the first nine months of the year, Websters sales were affected by previously reported
changes in the distribution arrangements for certain pharmaceuticals, which reduced Websters nine-month sales
growth by an estimated three to four percentage points. This situation had no material impact on fourth quarter
sales and none is expected going forward. Websters fourth quarter sales, which increased 14% from the year-earlier
period, benefited from strong demand for new combination products in the flea/tick and heartworm category. For
the fourth quarter and full year, Webster also posted continued strong sales of veterinary equipment and software.
Websters equipment business has been growing at solid rates, and we intend to continue investing in this relatively
new portion of Websters operation that has expanded the units full-service platform.
Technology is an increasingly important component of Websters full-service
capabilities. Intravet is a leading practice management software for veterinary
offices. Through ePetHealth, which was acquired in fiscal 2011, customers
of veterinarians have 24/7 access to their pets medical records as well as to
our Diagnostic Imaging Atlas, an advanced 3-D education tool that provides
detailed information about pet health issues and treatment options. Clients also
can place orders with their veterinarian for medications through ePetHealth,
which is being integrated into the home delivery service of VetSource, in
which Patterson holds a minority equity investment. ePetHealth also provides
veterinarians with a variety of marketing tools. In addition, through Websters
PURdigital product line, veterinary offices can be equipped with a PACS (picture
archiving and communications system) and fully integrated digital imaging
capabilities. We believe Websters expanding range of technology offerings
will help drive this units future growth by enhancing the profitability and
productivity of veterinary practices, forging stronger relationships between the
pet owner and veterinarian, and improving clinical outcomes.
Fiscal 2012 Outlook
The outlook for our three businesses is promising for the year ahead. We believe the North American dental
market is gradually strengthening as evidenced by the improving results of our consumable supply business.
Assuming the confidence of dental practitioners continues to strengthen, we believe the outlook for both basic
equipment and our range of technology offerings is favorable. We believe Patterson Medical is positioned to
continue gaining share from its competitors and take optimum advantage of favorable global market trends,
including aging populations in developed economies, todays more active lifestyles and less invasive treatment
protocols that frequently entail a rehabilitation regimen. The units growth also will be generated by continued
expansion of its sales force, further strengthening of its value-added business model and strategic acquisitions.
Demands by pet owners for more extensive services from their veterinarians will require capital investments by
the practitioner to meet this need. To capitalize on this opportunity, Webster will continue to focus on further
expanding its equipment and technology offerings, as well as its financing and service capabilities, in fiscal
2012. Targeted acquisitions also will remain a component of Websters overall growth strategy. Reflecting all of
these factors, we are optimistic about Pattersons overall prospects for fiscal 2012.
We also are confident about Pattersons longer-term future. Each of our three businesses has sustainable
competitive advantages and high barriers to new entrants. Our markets have compelling long-term growth
drivers. Our units are aggressively marketing their products and services. Our three businesses are staffed with
the best people in their respective markets. Finally, Patterson should continue generating substantial operating
cash flows, providing us with ample resources for supporting our various growth initiatives, dividend program
and share repurchasing.
As always, we extend our sincere thanks to our many outstanding employees for their dedicated efforts on
behalf of our customers. We also appreciate the continued support of our shareholders, customers and vendor
business partners.
Sincerely,
Scott P. Anderson
President and Chief Executive Officer