Fellow Shareholders,
Two years ago, in November 2007, we outlined significant and necessary changes to the companys
strategy. We adopted TSR as our primary financial metric, changed priorities for use of cash, adopted
role-based portfolio management and implemented more rigorous strategic planning. Our goals,
in sequential order, were to: i) divest low-performing businesses, ii) return more cash to investors,
iii) improve margins and returns on remaining assets, and iv) then begin to carefully and conservatively
grow the company at 4-5% annually. During 2008 we concentrated on the first two goals; for 2009 our
focus was largely on margin improvement.
In 2009, despite a 25% sales decline to $3.06 billion (caused by the weak economy), we expanded
both gross and EBIT margins. EPS from Continuing Operations increased slightly (from $.73 to $.74).
Our efforts generated 43% TSR for calendar year 2009, which ranked among the top 38% of
S&P 500 companies.
Looking more broadly, over the last two years we made significant strategic and tactical advances.
Consistent with our stated intentions, we:
 Generated cash of over $1.4 billion from operations and divestitures.
 Increased quarterly dividends by 44%, to $.26 per share.
 Purchased nearly 27 million shares, or 16%, of Leggetts outstanding stock.
 Reduced net debt to its lowest amount in over a decade.
 Implemented an annual, formal, rigorous strategic planning process at the business unit level.
 Reduced our Store Fixtures business unit to a more profitable base, one-half its former size.
 Introduced a new executive bonus plan based solely on 3-year TSR relative to peers.
 Achieved 32% cumulative TSR, which ranks within the top 4% of all S&P 500 companies.
More Cash to Shareholders
In late 2007 we announced our intent to: i) generate more cash by improving returns, divesting
certain assets, and diligently managing working capital, ii) reduce the amount of cash used for
acquisitions and capital spending, and iii) return more cash to shareholders in the form of
dividends and stock buybacks.
Since then, we have generated over $1.4 billion of cash from two sources: $1.0 billion of cash from
operations (for 2008 and 2009 combined) and over $420 million (after tax) from the divestiture of six
business units. Furthermore, as anticipated, weve reduced combined annual spending for acquisitions
and capital by over 50%.
As a result, we were able to increase quarterly dividends by 44%, from $.18 per share in 2007 to
$.26 currently. We also spent nearly $500 million to repurchase 16% of our stock during 2008 and
2009. This was accomplished while also reducing net debt to its lowest level in over a decade.

We plan to continue using a meaningful portion of excess cash flow (after funding dividends and capital)
to buy back our stock. For 2010, the level of excess cash flow depends heavily on the economy (and
resultant sales). We again have Board authorization to repurchase 10 million shares, but if the economy
remains lackluster we may not fully utilize that authorization. Though we could buy the full 10 million
shares with an increase to debt, our financial conservatism suggests we exercise patience and await
a stronger economy.
Improving Returns and Margins
During 2009, the company directed significant effort toward improving margins and returns. Gross
margin, in excess of 20% for 2009, was the highest recorded since 2000. EBIT margin improved
by 180 basis points (versus 2008) as a result of pricing discipline and significant, company-wide
cost reduction efforts. To attain this level of margin increase, while sales were declining 25%, is a
noteworthy accomplishment.
Further margin improvement must come from one of three areas. First, significant margin expansion will
require an increase in market demand and sales. Second, incremental margin improvement is possible
as our business units introduce new products, improve productivity, or raise prices. Finally, we could
consolidate facilities to increase plant utilization; however, we are purposely maintaining spare production
capacity (even though it depresses margins) because we expect market demand to ultimately improve.
Eventual Growth
For the near term, revenue growth is likely to be rather minimal (apart from possible broad economic
recovery). Over the longer term we believe consistent expansion of 4-5% per year is achievable.
Future growth requires that we successfully: i) develop and commercialize innovative new products,
and ii) identify and cultivate new higher-growth business platforms. We have recently commenced
efforts to enhance our capabilities in both of those areas.
Over the long term, we will use four levers to generate TSR: margin improvement, sales growth, dividends,
and stock buybacks. In 2008, dividends and stock buybacks largely drove our TSR. During 2009, we
benefited significantly from margin improvement. Within a few years we expect that modest annual sales
growth will also contribute to TSR. We believe this multi-faceted approach will enable us to achieve our
goal of consistently generating TSR that places Leggett & Platt in the upper one-third of the S&P 500.
For the first two years under our new strategy, TSR performance well exceeded our goal. Our aim is
to create a continual string of TSR successes, and we are fully committed to that prospect.
David S. Haffner 
President and CEO 
Karl G. Glassman
Executive VP and COO
February 28, 2010