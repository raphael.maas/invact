Dear Fellow Stakeholder,

In 2012, we harnessed the power of our company to fulfill
our promise to deliver consistent, superior total returns to
shareholders. Our success is the result of our outstanding
execution, commitment to stakeholders, sound strategy,
cohesive team and strong work ethic. These are the
values that have sustained excellence at Ventas for
more than a decade.
Last year, we continued our value-creating external growth,
while enhancing our infrastructure to integrate our scale and
complexity and position us for the future. We significantly
grew cash flow, improved our cost of capital, and actively
managed our diverse portfolio to increase revenue and
protect value.
Once again, we demonstrated the merits of building a
company with balance, diversification, scale and flexibility
to provide investors with growth and capital preservation,
essential components of our long-term consistent strategy.
Over ten years, our compound annual return to shareholders
exceeded 25 percent and normalized FFO per share and
compound annual dividend growth increased more than
10 percent per year. Our equity market capitalization
exceeded $20 billion in early 2013.
Strong demographic demand for our over 1,400 properties
propels our business. The 85+ cohort, many of whom will
move into our high-quality senior living communities, is
the fastest-growing segment of the U.S. population. Our
MOBs will benefit from both the 79 million Baby Boomers
who are turning Medicare-eligible and the 32 million newly
insured individuals to be covered under the Affordable Care
Act. In the future, most Americans will increasingly reside
and receive care in our consumer-focused senior living
communitieswhich do not rely on government funding
and our low-cost healthcare settings, as these properties
become part of the solution in our national healthcare
policy debate.
Momentum and market trends help us build strength and
sustain excellence. In a favorable environment when we
have the opportunity to make accretive investments and
access debt capital at historically low rates, we will act with
purpose and urgency. And we protect shareholder capital
with thoughtfully managed risk: 70 percent of our enterprise
value is composed of permanent equity capital, and our
debt maturities are staggered and manageable.
Excellence Achieved
I am proud of our many significant accomplishments during
2012. Total normalized FFO rose 44 percent to $1.1 billion,
and normalized FFO per diluted share increased 13 percent
to $3.80.
Our total shareholder return (TSR) for 2012 exceeded
22 percent, outperforming the U.S. REIT Index and the
S&P 500 Index in each of the trailing 1-, 3-, 5- and 10-year
periods. At year-end, we were the fourth largest Real Estate
Investment Trust (REIT), measured by equity value.
We performed flawlessly on our three pillars of excellence:
investing capital; managing our portfolio; and raising capital.
Our $2.7 billion in investments in high-quality private pay
assets, at nearly an 8 percent unlevered cash yield, was
exceptional. Our strategic investment in Atria Senior Living,
the fifth largest U.S. senior living care provider, gives us
a creative way to participate in Atrias growth and
industry evolution.
Asset management drove same-store operating cash flow
growth exceeding 4 percent and also recycled over
$400 million in capital through attractive asset sales and loan
repayments. Our market knowledge and industry expertise
were essential to our successful re-leasing or sale of all
89 licensed healthcare assets up for renewal in 2013. And
our capital markets team managed our liabilities prudently,
raising over $2.6 billion in debt capital at a weighted average
rate of just over 3 percent in 2012.
All of our efforts resulted in $1 billion in cash flow from
operations in 2012. Increasing cash flows lead to increased
investments in income-producing assets, generating
future growth potential and creating a virtuous cycle for
our continued success, including higher dividends for
shareholders. Last year, and again in early 2013, we raised
our annual dividend by 8 percent, to $2.68 per share
maintaining a safe secure payout ratio, with room for growth.

Excellence Pursued
I am excited about the future, challenging our organization to
raise the bar on our record of success and excellence.
Future success means we must consider ourselves a
world-class company. As we adapt to our more complex
enterprise and rapidly changing external environment, we
must also reinforce the principles and values that have
served us well for many yearspassion for excellence,
problem solving and execution, sound consistent strategy,
stability in management, teamwork, commitment to
protecting the firm and integrity.
We are managing our firm, assets and liabilities consistent
with our near-term view that economic growth will be slow
but positive and that low interest rates will persist, supported
by accommodative policy. Those factors favor cash flowing
businesses with hard assets and dividend paying stocks
like ours. But we must also be prepared for other scenarios,
including a recession or, more likely, a backdrop of higher
growth and rising interest rates.
On the liability side, we are taking steps to exploit the current
environment and protect against future changes. One
example is the recent issuance of our first 30-year bond at a
very attractive fixed rate. The long tenor protects us against
rising rates. However, if rates actually decline or our credit
spreads improve materially, we have the option to repay the
principal at par and refinance at lower rates. We value this
one-way option. We also are actively managing our ratio of
fixed rate debt to floating rate debt. While some floating rate
prepayable debt is appropriate and indeed desirable, we will
primarily finance our business with fixed rate debt to match
the long duration of our assets.
On the asset front, we were prescient in investing
$14 billion in our portfolio of high-quality seniors housing
and medical office building assets beginning in 2010. Our
flexible business model and powerful platform give us the
advantage of investing in different subsectors at early stages
in improving economic, capital markets and reimbursement
cycles. We will also consider increasing exposure in certain
existing asset classes, investing outside the U.S. and
even adding new asset classes to our mix. Our resources,
experience and scale allow us to invest wisely on the
return slope, while staying true to our rigorous investment
principles and within our areas of expertise.
We remain forward-thinking, value-creating capital allocators
positioned to grow in the highly dynamic, robust and
fragmented $1 trillion seniors housing and healthcare
real estate market. Assets will flow to the most efficient
ownerslike Ventaswho have the best cost of capital,
industry knowledge, skill and foresight to take advantage of
the ample opportunities.
Excellence. Sustained.
The history of Ventas is about achieving and sustaining
excellence. For this, we are indebted to the support,
experience and judgment of our dedicated Board of
Directors, who are actively engaged as we chart and
recalibrate our path to a secure and exciting future.
My colleagues in the Ventas senior leadership team and
across our hardworking employee base are second to
none. Our cohesive team executes our growth strategy,
protects the downside and continues to deliver outstanding
shareholder value. To all of you, I applaud your many
achievements and tireless efforts.
Finally, I value our investors enormously for supporting us
and trusting us to be faithful stewards of their capital. We
are driven to excel for you. We return your confidence by
creating exceptional value and delivering consistent, superior
shareholder returns. Our unwavering commitment to you
continues as we sustain the excellence you have come to
expect from us.

Debra A. Cafaro
Chairman and
Chief Executive Officer
March 20, 2013