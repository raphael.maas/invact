LETTER TO THE OWNERS, CUSTOMERS, TEAM MEMBERS AND
BUSINESS PARTNERS OF PULTEGROUP:
I am extremely pleased to report that PulteGroup’s 2017 financial results show significant, double-digit
percentage gains on both the top and bottom lines. Even more important, and consistent with our strategy, we
grew the business while continuing to generate high returns on equity and invested capital. The success we
realized this year, and over the past several years, was driven by a number of factors, including: investments
we have made to acquire well located land positions; outstanding consumer-inspired designed floor plans a
renewed focus on delivering a superior customer experience; and, our steadfast commitment to building
quality homes. Although impacting many different parts of our business, there is a common thread running
through each of these elements…having great people who are passionate about what they do.
Our founder, Bill Pulte, understood decades ago that floor plans are easily replicated, but that our employees
can never be copied and that their talent and integrity are vital to our sustained success. I couldn’t agree
more, which is why having PulteGroup be a great place to work isn’t just a platitude, it is a core objective. One
that we continue to advance as demonstrated by our 2017 employee engagement scores that, as measured
by Gallup, are now in the top 5% of all companies in the world. In a market where the battle for talent is
constant, having a supportive and engaging culture is vital to hiring and retaining the best people.
Our focus on employees is driven by more than just niceties. Having an engaged and genuinely devoted
workforce is the lynchpin to realizing our goal of delivering high quality homes and a great consumer
experience…every time. I believe success in these areas is one of the drivers supporting gains in
PulteGroup’s Net Promotor Scores (NPS). For those of you not familiar with NPS, it is a customer loyalty
metric calculated based on responses to a single question: How likely is it that you would recommend our
company to a friend or colleague? The good news is that our NPS score at the time of home delivery is 50,
which is defined as excellent and ranks us among some of the global leaders recognized for customer
experience. We will never stop working to improve our customer satisfaction, but these scores tell me that we
are doing a lot of things right.
A passionate group of people working tirelessly to deliver great homes is a strong foundation upon which to
successfully grow our operations. To that end, we made the decision in late 2013 to begin ramping up our
investment in the business to further capitalize on our strong team and strategic operating improvements.
Over the following four years, our annual land acquisition spend doubled to $1.1 billion in 2017. Today, we
are reaping the reward of that investment, as our homebuilding revenues have increased at a compound
annual growth rate of 11% since 2013, including an increase of 12% to $8.3 billion last year.
Even more impressive than the expansion in top line revenues is the significant gains realized in bottom-line
earnings. On a year-over-year basis, our adjusted 2017 earnings increased 32% to $2.19 per share.*
Converting 12% top line growth into a 32% expansion of EPS was accomplished through solid operational
performance, driven by significant SG&A leverage, and an 11% reduction in shares outstanding. Consistent
with our stated capital allocation priorities, the lower share count reflects our decision to return $1.5 billion to
investors through share repurchases over the last two years.
In a world where many believe growth is good, more growth is better, and hyper growth is fantastic, our focus
remains on building a business that can generate high returns and more consistent performance over the
housing cycle. Assembling and then maintaining all the pieces needed to sustain a more predictable business
is incredibly difficult, but that is the model we are working to establish. Our financial results demonstrate that
we are moving in the right direction as our returns on invested capital and equity continue to increase.
* See Reconciliation of Non-Gaap Financial Measures immediately following the Form 10-K Exhibits
2017 ANNUAL REPORT | PulteGroup, Inc. 1
LETTER TO THE OWNERS, CUSTOMERS, TEAM MEMBERS AND
BUSINESS PARTNERS OF PULTEGROUP:
Core to successfully creating a business that can generate high returns and consistent performance is how
we handle our largest asset: land. We continue to manage our land pipeline to increase asset efficiency while
lowering overall market risk. More specifically, we are reducing the years of land we own and targeting
development of smaller, faster-turning projects that we expect will generate high returns. Between 2014 and
2017, we approved over 600 new communities at an average size of 136 lots and 2.9 years of supply. This
investment discipline allowed us to end 2017 owning 4.2 years of lots
LETTER TO THE OWNERS, CUSTOMERS, TEAM MEMBERS AND
BUSINESS PARTNERS OF PULTEGROUP:
While we continue to have a positive view on future demand, we must remain disciplined in our business
practices. This includes how and where we invest in land, maximizing our production of build-to-order homes,
and continuing to drive production and overhead efficiencies. We have put the Company in an outstanding
market position heading into 2018, and I want to make sure we capitalize on the ensuing opportunities.
At the Intersection of Technology
It would be fair to say that the homebuilding industry is rarely viewed as a hotbed of technological innovation.
I believe this image is about to change. Not just change, but to be shattered, as innovative new ideas are
impacting every facet of our business. From sales, where numerous internet-based businesses are facilitating
real estate transactions, to construction, where new building processes and offsite manufacturing increasingly
fill the void created by scarce labor resources, the industry is evolving.
The home itself, of course, grows increasingly “smart” and connected allowing homeowners to control key
systems through the tap of a screen or a simple voice command. The computer controlled home of sci-fi
dreams is quickly becoming a reality, as are driverless cars which will impact how and where communities get
developed in the future.
While technology will continue to have a tremendous positive impact on homebuilding and homeownership, it
does add layers of complexity to our business. Given the rate of change, which will only be accelerating, I
believe we need to be actively engaged in the process rather than being forced to adapt down the road. From
marketing and sales to construction and financing, we are working to intelligently integrate innovation into our
production processes. Examples of this include our construction of an innovative Smart Neighborhood in
partnership with Georgia Power and the national rollout of our new Smart Home offering to consumers later in
2018. Our technology initiatives are still in their infancy, but the projects we are undertaking provide unique
opportunities to learn. Home-related technologies are advancing and multiplying at such a rapid pace and we
see the education provided by these pilot programs to be invaluable.
Business Summary
The gains realized in 2017 have helped put PulteGroup in a strong competitive and financial position. Our
operations are well positioned within their markets. We have the people, land and financial resources needed
to continue expanding our business. We are also operating in a favorable macro environment that should
support increasing housing demand for the next several years.
Our strong business platform reflects a lot of hard work by an incredibly talented team of people who are
passionately committed to our Company’s success. I saw the true character of this team when tragedy struck
in the form of Hurricanes Harvey and Irma. Hearts, hands, homes and wallets were opened without hesitation
and with only one question: “What else can I do to help?” Having seen this first hand, I am not surprised at the
strength of our employee engagement scores and the overwhelming feeling that our Company is a great
place to work.
2017 ANNUAL REPORT | PulteGroup, Inc. 3
LETTER TO THE OWNERS, CUSTOMERS, TEAM MEMBERS AND
BUSINESS PARTNERS OF PULTEGROUP:
The Passing of a Legend
As I write this letter, our Company founder and industry icon Bill Pulte passed away on March 7, 2018. Bill’s
founding of this Company is one of those amazing tales that has only grown richer with the passage of time.
An 18-year old Bill walks away from a college scholarship to chase a dream. On a plot of land donated by an
aunt, with a floor plan purchased from the newspaper and tools given as high school graduation gifts, Bill
builds his first home and in the process, launches a business that has spanned seven decades and delivered
over 700,000 homes.
Bill leaves behind countless family and friends whose lives were made better by knowing this talented,
successful and humble man who loved his God, his family and his Pulte family. A man who always said that
he never worked a day in his life because he so loved what he did. Bill’s legacy will survive for generations to
come, and may we all take to heart these simple ideals that guided his life. We will miss Bill’s colorful
sweaters and even more colorful personality, but he can rest assured that his spirit is threaded into the very
fabric of the Company that so proudly carries his name. We are committed to carrying his legacy forward for
at least another 70 years.
Sincerely,
Ryan Marshall
President and Chief Executive Officer