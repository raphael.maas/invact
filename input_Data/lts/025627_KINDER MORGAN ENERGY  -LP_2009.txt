MARCH 2010 LETTER TO UNITHOLDERS                                                 We plan to continue to execute the same successful strategy
                                                                           that has made KMP one of the largest and leading midstream
      2009 was another difficult year for many, and while we               energy companies in North America. In a nutshell, we will
certainly had our share of challenges at Kinder Morgan Energy              continue to focus on owning and operating primarily fee-based
Partners (NYSE: KMP), I'm delighted to report that we were able            assets that are core to the energy infrastructure of growing
to generate enough cash flow to cover our annual distribution              markets and that produce stable cash flow.
target of $4.20 per unit and end the year with excess coverage                   KMP is well positioned for the future. We completed
of $14 million. The $4.20 per unit distribution represents a               construction of and placed into service three large natural gas
4.5 percent increase over our 2008 distribution of $4.02 per               pipelines last year--Rockies Express, Midcontinent Express
unit. More importantly to you, our unitholders, KMP returned               and Kinder Morgan Louisiana. In 2010, we anticipate investing
45 percent to its limited partners in 2009 and has delivered a             approximately $1.5 billion to further grow the company. Since
compound annual return of 27 percent since KMP was founded                 KMP was formed in 1997, we have invested approximately
in early 1997.                                                             $20 billion in new-build projects, expansions and acquisitions.
      Our businesses were impacted by various economic                           Our large footprint of assets provides us with opportunities
headwinds during 2009, including lower refined products                    to expand and grow more cost effectively than smaller companies,
transportation volumes, decreased steel handling at our bulk               and we continue to stand poised and ready to capitalize on
terminals, lower crude oil prices and a difficult business                 emerging trends in the industry. For example, we anticipate more
environment for our Texas Intrastate Pipelines. We were able to            natural gas (the cleanest burning fossil fuel) will be used in the
overcome these headwinds by exceeding expectations at some of              future to meet energy demand and reduce carbon emissions. Our
our other assets, reducing internal costs and benefiting from lower        assets position us well to access, transport, store and treat more
interest rates. Also in 2009, we maintained a strong balance sheet         natural gas from various supply sources, including the emerging
and continued to have excellent access to the capital markets.             shale plays. Additionally, Renewable Fuels Standard mandates
      KMP has been described by some analysts and media                    are increasing the amount of ethanol and biodiesel that is being
as a resilient company that produces solid results in all types            used. In 2010, we expect to handle about 80 million barrels of
of market conditions. How have we accomplished that? First,                ethanol, or about 25 to 30 percent of all the ethanol handled in
we have a large, diversified portfolio of assets that produce              the United States. We are also well positioned to capitalize on our
tremendous amounts of stable cash flow. Second, we have an                 extensive crude oil reserves in the Permian Basin in West Texas,
                                                                           and if CO2 sequestration becomes more feasible we have the
experienced management team with a proven track record.
                                                                           expertise to play a major role in that arena.
And third, we have outstanding employees. Considering the
                                                                                 While the markets are currently better than they were a
challenges that we faced in 2009, what our 8,000 employees were
                                                                           year ago, we still face plenty of challenges in addition to these
able to accomplish was truly exceptional and I salute them for
                                                                           opportunities. Commodity prices are still volatile, the general
their commitment and tireless efforts.
                                                                           economy remains unstable and we face continued regulatory
                                                                           uncertainty. With approximately 28,000 miles of pipelines and
LOOKING AHEAD                                                              180 terminals we also face an ongoing concern of operating our
                                                                           assets in a safe, compliant and environmentally sound manner.
      In 2010, KMP expects to pay cash distributions of $4.40 per          Every year we spend hundreds of millions of dollars on integrity
unit, which would be a 4.8 percent increase over the 2009                  management programs and maintenance, and I'm pleased to
distribution of $4.20 per unit.                                            say that we continue to outperform the industry averages on
                                                                           environmental, health and safety measures. I invite you to
                                                                           visit www.kindermorgan.com to review KMP's operational

                                                                           performance, our annual budget, investor presentations and more.
 
                                                                                 While the future is always unknown, I pledge that we will
                                                              $4.02
                                                                           continue to do our best to operate our assets safely, satisfy our
                                                          $3.48
                                                                           customers and deliver an attractive return to our limited partners.
                                                     $3.26
                                                $3.13
                                                                           Thanks for your ongoing support. I still believe the best is yet
                                        $2.87
                                                                           to come!
                                   $2.63
                                                                           Sincerely,
                     $1.71
                                                                           Richard D. Kinder
                                                                           Chairman and CEO
                                                                           Kinder Morgan Energy Partners

