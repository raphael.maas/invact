In 2017, we continued to make significant progress toward achieving our vision to be the world’s premier timber, land, and forest products company. I’m proud of how our teams worked together to successfully complete our Plum Creek merger integration, improve financial performance across all our businesses, and furtherfocus our portfolio. I’m especially pleased with the progress we’ve made to build our leadership bench through focused people development.
Together, these actions position our company well to continue generating value for shareholders by fully capturing the benefit of improving markets, out-executing our competitors across all business lines, demonstrating disciplined capital allocation, and attracting and retaining the very best talent.
FINANCIAL PERFORMANCE
For the past tour years, our teams have been relentlessly
focused on driving improved operating and financial performance.
In 2017, we:
• Increased Adjusted EBIT[ by 31 percent to nearly $2.1 bOon
• Generated more than $1 billion c% Wood Products EBITDA, the most since 2004
• Grew Real Estate, Energy& Naturl Resoirces EBITI)8L by nearly 30 percent
• Earned a 55 percent premium to timberland value on our real estate sales
• Exceeded our $100 million merger cost synergy target by25 percent
• Eliminated $35 million of cts formerly allocated to our Cellulose Fibers business
We also captured nearly $140 million of operational eellence
improvements in 2017, including
• More than $65 million in Timberlands
• More than $70 million in Wood Products
We’re proud of the nearly $500 million in operational excellence improvements we’ve captured since 2014, but we know more opportunities remain. We look forward to continuing to improve our relative performance in each of our businesses.
FOCUSED PORTFOLIO
During the year we continued to simplify and optimize our portfolio. We divested our Uruguay operations, redeemed our ownership in the Twin Creeks joint venture, and sold 100,000 acres of Southern timberlands for collective proceeds of more than $700 million.
CAPITAL ALLOCATION
Our first priority for capital allocation is returning cash to shareholders and we are strongly committed to a sustainable and growing dividend.
We demonstrated that commitment by increasing our dividend in 2017 for the sixth time since 2011, while also strengthening our balance sheet.
A BRIGHT FUTURE AHEAD
Weyerhaeuser is the largest owner of high-quality timbeands in the U.S., with an industry-leading wood products manufacturing business and a team of world-class employees. I’m extremely proud of what our people have accomplished together, and very optimistic about what’s ahead for us in 2018 as we continue to drive operational excellence, fully capitalize on strengthening market conditions, demonstrate disciplined capital allocation, and develop a strong team of future leaders who will make our company truly great.
We look forward to sharing our progress with you next year. Thank you for your ownership and support.
p.
Doyle R. Simons
President & Chief Executive Officer