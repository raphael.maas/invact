Vision + Discipline + Execution = Achievement
Over the past five years, we have seen an unprecedented decline in construction activity in the United
States. This has profoundly affected many segments of the American economy; building materials
being one of them.
Despite these historic challenges in our industry, as shareholders, you have legitimate expectations that
Martin Marietta Materials will continue to find new, thoughtful and creative ways to make our Company
better and generate attractive returns now and into the future. Consistent with those expectations,
we are pleased to report that in 2011, Martin Marietta Materials disciplined business approach did
drive industry-leading performance and advanced our vision for marketplace growth. Importantly, our
accomplishments were achieved in ways that will enhance long-term shareholder value.
We knew at the outset that 2011 would be a challenging year. Among other things, the prospect of a
reauthorized transportation bill seemed elusive despite an urgent need to create jobs and enhance the
nations infrastructure. Indeed, 2011 ended not with a new transportation bill, but rather an eighth
continuing resolution, extending the transportation program piecemeal through March 2012. This
delay in the approval of longer-term transportation funds, together with a persistent housing slump,
led to a 3.5% decrease in our heritage aggregates product line shipments. Aggregates volume declines
were exacerbated by higher input costs, particularly diesel fuel prices which rose nearly 40% in 2011.
Fortunately, our operating efficiencies largely offset these volume and cost challenges, demonstrating
how remarkably well-positioned our Company is to benefit from any economic upturn.
During the year, we succeeded at strategically growing our business while maintaining considerable
financial flexibility, which positions us strongly for the future. We incurred $18.6 million of business
development expenses, which can be separated into two categories: (i) expenses related to transactions
closed during the year and (ii) expenses related to our exchange offer to acquire all of the outstanding
common stock of Vulcan Materials Company (which is discussed below). Excluding business development
expenses, our adjusted earnings per diluted share for 2011 were $2.03, compared with $2.12, also
adjusted, for the prior year.
Our business continued to generate cash. Cash flow, as measured by EBITDA (earnings before interest,
taxes, depreciation, depletion and amortization), was $336 million (inclusive of business development
expenses). We also remained attentive to our working capital requirements by carefully managing
inventory levels as well as customer receivables. Likewise, our selling, general and administrative
expenses decreased from the prior year by $6.3 million, or 60 basis points as a percentage of net sales.
Our Specialty Products segment had another outstanding year, producing record profits and contributing
significantly to our performance. This result, yielding a return on sales of nearly 33%, was driven by the
Companys consistent focus on cost control, product planning, customer service and pricing momentum. The
dolomitic lime business benefitted from a resilient steel industry that experienced utilization rates averaging
nearly 75% during 2011. Similarly, our specialty chemicals business enjoyed strong sales throughout the year
demonstrating particular strength in both the hydroxide slurry and MagShield business lines.
During 2011, we continued to deploy capital prudently, to position the Company for continued
growth and shareholder value creation. We regularly evaluate new business development opportunities
in order to establish or maintain leading market positions in areas with attractive growth demographics.
In 2011, we examined over thirty acquisition opportunities and successfully completed three transactions
that added more than 400 million tons of aggregates reserves in Texas and Colorado. These included
an asset exchange with Lafarge North America that closed in December, through which we acquired
aggregates sites, vertically integrated hot mixed asphalt and ready mixed concrete plants and a road
paving business in and around Denver, Colorado. Earlier in the year, we also acquired a family-owned
aggregates and vertically integrated business in San Antonio, Texas, where population and economic
growth have consistently outperformed the national average.
We continue to carefully manage our capital expenditures and invest prudently in our business, properly maintaining our plants, property
and equipment. During the year, the Company invested $155 million in capital, focusing on proactive maintenance of our operating assets
to ensure safe and efficient operations, as well as sensible organic growth projects. These capital expenditures include our investment in a
new dolomitic lime kiln at our Specialty Products business in Woodville, Ohio, which will be substantially complete by the end of this year.
We also opened an aggregates sales distribution yard near Tampa, Florida, expanding our leading rail distribution network and complementing
our earlier growth initiatives on Floridas east and west coasts at Port Canaveral and Port Manatee, respectively.
Maintaining one of the industrys strongest balance sheets is of critical importance, especially in a challenging operating and economic
environment. Martin Marietta Materials financial discipline and focused liquidity management give us the ability to make wise capital
investments and efficiently address any near-term debt maturities. It also enables us to return capital to shareholders through attractive and
sustainable quarterly dividend distributions. Although many competitors have significantly reduced or eliminated dividends, we have maintained our dividend throughout this prolonged economic downturn without sacrificing our balance sheet strength, financial flexibility
or capital deployment capabilities.
The economic and political climate in the United States remains uncertain and, unfortunately, given the national deficit and the continued
sensitivity to all government spending, the issue of long-term federal infrastructure funding is far from resolved. However, as we entered
the 2012 election season, we were pleased to see mounting dialogue amongst elected officials and policymakers regarding the need for
a multi-year surface transportation bill as part of jobs creation. This dialogue extends from the White House to the halls of Congress, from
state legislatures to governors offices, and is also heard in municipalities and counties across the country.
Employee safety is of paramount importance to Martin Marietta Materials and we are always working toward improving our processes and
performance. We believe we have the industrys most effective safety program, one that is decision-based and focused on training and
encouraging employees to operate smarter and more efficiently. The results speak for themselves: our Companys Total Injury Incident Rate
(TIIR) and Lost Time Injury Rate (LTIR) are both improved from last year (and last years TIIR was an all-time best for the Company). We will
continue to emphasize safety in order to achieve our ultimate goal of zero incidents.
As part of our focus on industry leadership, we also believe that doing business the right way is a key component of our success. Many
companies talk about ethics, but at Martin Marietta Materials, it is a core commitment to action that has always been part of our culture
and is reinforced through systematic and hands-on ethics training. Moving forward, we will continue to adhere to the highest standards of honesty, integrity and ethical business conduct.
The strength of our Company is derived from the collective efforts of Martin Marietta Materials nearly 5,000 employees. On a daily basis,
our employees demonstrate an unrelenting commitment to excellence in executing our strategic plan to deliver value-enhancing results
to shareholders. We are grateful to each and every employee for their dedication and outstanding efforts. Together we look forward to
building upon the Companys successes.
Business growth, either acquisitive or organic, requires strategic and tactical vision as well as enduring discipline. After much careful consideration,
on December 12, 2011, Martin Marietta Materials announced a proposal to combine our business with Vulcan Materials Company to create a
U.S.-based company that would be the global leader in aggregates. This proposed combination is a compelling opportunity for the shareholders,
customers, and employees of both companies and the communities served. We are committed to completing the combination and have been
pursuing a number of steps to make it a reality. And, in all events, at Martin Marietta Materials we will continue our efforts to enhance shareholder value through business discipline, execution and vision.
On behalf of the Board of Directors, we thank our shareholders for their continued support. Our 2011 performance, as well as our strong
competitive position, provides us great confidence as we collectively take Martin Marietta Materials to new heights in 2012 and beyond.
Respectfully,