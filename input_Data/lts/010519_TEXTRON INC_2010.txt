                                                                                                                                                          1




Fellow Shareholders:

With a recovering global economy, many of our businesses                   million for these segments. For the year, we realized $692
                                                                           million in free cash flow1 --compared to $424 million in 2009.
demonstrated improved performance in 2010. At Bell,
the team delivered another year of solid execution, with
                                                                           In our Finance segment, we made excellent progress with
an increase in overall revenues and double-digit profit
                                                                           our finance receivables liquidation begun in 2009. In 2010,
margins. Volume increased in our Industrial segment--
                                                                           we reduced our finance receivables by $2.4 billion and
largely driven by the return of the global automotive
                                                                           generated $2.2 billion in net cash from the liquidation
market--which led to improved profitability. Cessna
                                                                           of finance assets.
continued to work through a tough cycle in the aviation
industry; however, there were positive indications of
                                                                           Overall, this performance allowed us to take a number of
recovery in the fourth quarter with the delivery of 79
                                                                           positive actions in 2010. We continued to strengthen our
business jets out of 179 total deliveries for the year.
                                                                           balance sheet and reduced our consolidated net debt2 to
At Textron Systems, we saw the continued steady
                                                                           $5.0 billion--down from $7.4 billion last year and from
delivery of our armored security vehicles, unmanned
                                                                           $11.9 billion in 2008.
aircraft systems and technology solutions that support
today's armed forces.                                                      Key to growth in 2010 and beyond, we accelerated
                                                                           activities around the quantity and pace of new product
Our manufacturing businesses ended 2010 up slightly from
                                                                           development, as well as the expansion of our distribution
last year with $10.3 billion in revenues and a profit of $790
                                                                           channels and service footprint.



                                                                  Operational Efficiency
    Innovation

                                                                  To ensure that these growth initiatives produce optimal
    In terms of innovation, both Cessna and Bell announced
                                                                  results, our teams continued to drive for improved
    major new products and product upgrades that feature
                                                                  operational efficiency in everything we do. From research
    improvements ranging from heightened comfort and
                                                                  and development to manufacturing activities to front-end
    performance to reduced owner operating costs. At Textron
                                                                  sales and service, we took action to speed time to market
    Systems, we enhanced the performance of our ground
                                                                  and lower operating costs.
    vehicles, invested in future precision weapons and unveiled
    kits to increase the wingspan of our unmanned aircraft.
                                                                  In total, these types of operational and customer-facing
    In our Industrial segment, we introduced street-legal,
                                                                  actions strengthen our ability to be successful in an
    low-speed vehicles for consumer and industrial use.
                                                                  increasingly competitive global marketplace. I'd like to
    We made upgrades to our existing golf car line-up and
                                                                  thank everyone whose hard work produced the solid
    broadened our aftermarket parts and accessories offerings.
                                                                  results we saw in 2010. As we move into the future,
    We also introduced a series of advanced technology
                                                                  I believe we are on the right path to making even further
    products for the plumbing and electrical trades, as well
                                                                  gains in operational efficiency; in building our brands;
    as began full-scale production of new hybrid equipment
                                                                  and in seeing our businesses, our employees, and our
    offerings for the turf-care industry. These are only the
                                                                  customers enjoy continued success.
    beginning of the innovations to come from our product
    development pipeline.
                                                                  Sincerely,

    Global Service Footprint

    During the year, we also took steps to strengthen our
    domestic and international aviation service footprint.
    In 2010, Cessna began deploying more Mobile Service
    Units and adding service centers in the United States,
                                                                  Scott C. Donnelly
    Europe and the Middle East. Also, Bell and Cessna
                                                                  Chairman and Chief Executive Officer
    combined resources to establish the first shared parts
    distribution and service centers in Europe and Asia.


