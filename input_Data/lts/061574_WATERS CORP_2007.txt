I am pleased to report to you that 2007 was a successful
year for our Company as a combination of favorable market
conditions and strong uptake of our new products resulted
in a 15% growth in revenue and a 20% increase in adjusted
earnings per share excluding a charge for curtailing our U.S.
retirement plans, purchased intangibles amortization, business
restructuring, and an equity investment impairment. The various
factors that drove our growth throughout 2007 appear to us as
sustainable trends that will continue to benefit our business
going forward.
At Waters, we continuously strive to develop instrument
systems, related supplies, and services that advance scientific
research and improve operational efficiency. Our customers are
located in all corners of the world and are involved in scientific
research studies that range from the discovery of new therapies
targeting the most threatening diseases and the development
of advanced plastics and industrial chemicals key to product
improvements in the electronics, aerospace, and automotive
industries, as well as complying with emerging food and drinking
water safety regulations. Although we service a very diverse set
of customers, we pride ourselves on being an innovation and
market leader in select analytical technologies: chromatography,
mass spectrometry, and thermal analysis. This blend of broad
market opportunities combined with a strong technological focus
is key to our industry-leading track record of delivering exciting
instrument systems to our customers and superior financial
results to our investors.
In 2007, we benefited from the rapid market acceptance of new
products including our ACQUITY UPLC, SYNAPT HDMS from
our Waters Division, and a series of new thermal analyzers from
our TA Instruments Division.
Sales for ACQUITY UPLC, the most prolific new liquid
chromatography system to hit the market in decades, grew
rapidly throughout the year. New applications for ACQUITY
allowed us to expand our customer base while repeat business
for this innovative instrument further established the technology
at early adoption accounts. The benefits of an expanded base
of ACQUITY customers has significant long term benefits for
Waters as future sales of its tailored consumables, ACQUITY
UPLC columns, promise to enhance sales growth and profitability
for years to come.
Demand for SYNAPT HDMS (High Definition Mass Spectrometry),
a new category of mass spectrometry that we first began
delivering to researchers at the end of 2006, gained momentum
throughout 2007. SYNAPT has a unique capability to differentiate
molecules by their three dimensional shape. For many biological
compounds, such as proteins, molecular shape is an important
determinant of biological activity. The applications for SYNAPT
technology range from basic studies of disease processes
to quality testing of vaccines. In fact, new applications for
this exciting technology are still being uncovered by Waters
researchers and current SYNAPT customers. Needless to say,
we feel that the early success of this innovative technology will
help accelerate demand in 2008 and reaffirm Waters leadership
in mass spectrometry.
In 2007, we continued to expand our business geographically
with significant sales growth in developing economies playing
a more noteworthy role in our overall results. Our sales growth
was particularly impressive in China, Korea, India, and Eastern
Europe. More importantly, we feel that business expansion in
developing regions of the world will continue for years to come
as their economies mature and as more scientific infrastructure
is required. Accordingly, in 2007 we continued to invest in
these areas by expanding and modernizing our facilities and
by attracting new employees.
The profitability and operational efficiency of our business
allowed us to generate very strong cash flow in 2007. In the
long run, strong cash generation is the ultimate goal of any
business enterprise and at Waters we consistently look to build
on our successful record. During the last year, we deployed
our cash to our stock repurchase program and to acquire a
calorimetry business to complement our TA Instruments
business. Looking ahead, we plan to continue to deploy our
cash toward selective acquisitions and share repurchases.
We are optimistic about our business prospects going forward
and plan to maintain the basic business strategy that has proven
to serve us well  a strategy centered on solving our customers
most challenging scientific problems with technically advanced
instrument solutions, associated consumables and services. With
our worldwide sales and support organizations, our strong brand
image and loyal customer base, we are well positioned to gain
market share while delivering superior financial results for
our shareholders.
On behalf of all the employees of Waters, thank you for your
interest in our company.
Sincerely yours,

Douglas A. Berthiaume
Chairman, President and Chief Executive Officer