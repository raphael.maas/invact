They are hard to break, but
in todays business world
we cant afford to stand still.

Many of our long-standing beliefs were shattered
during 2001. On the home front, we saw that
we Americans can be attacked on our own soil.
On the economic front, we were reminded that
economies can sputter, stall and reverse direction.

Capital markets showed us that neither stock markets nor trees grow
to the clouds. We saw that there is such a thing as company arrogance,
and that there is no such thing as a company being too big to fail.
During these precarious times, Lincoln Financial Group continued to
shatter some of its own long-standing beliefs.
People say that old habits are hard to break. We agree. But we
did just that. Back in 1912, we wrote our first reinsurance treaty and,
since that time, we evolved into one of the worlds most successful life
reinsurance companies. But as we looked into the future, we knew we
had to change. Given the demographics of our society, we recognized that
the marketplace opportunities for wealth accumulation and protection
businesses were too attractive to not put all of our resources behind
that effort. Ultimately, this realization led to the sale of our reinsurance
business to Swiss Re. Was a 90-year-old habit hard to break? You bet
it was. However, we feel were now the best-positioned company in the
financial services industry to fully capitalize on the exciting opportunities
in the high-net-worth and retirement marketplaces.
Popular wisdom also says that rules should not be changed in the
middle of the game. Under one of our previous rules, each of our
business lines was a strategic business unit (SBU) that was encouraged
to work toward maximizing its own results. These SBUs were not
rewarded for exploring ways they might team up with one another to
create better results for clients. Weve worked hard during the past
couple of years to tear that old rule into pieces. In its place we inserted

an enterprise rule. Product specialists from different business lines
now work together to insert common features, create exciting new hybrid
capabilities, and share their knowledge and experiences. Moreover,
Lincoln distribution companies now have an ability to bring comprehensive
solutions to their customers as they tap into the full array
of capabilities provided by Lincoln Financial Group.
We also adopted a new name for our Annuities business to better
reflect its broad portfolio of retirement planning solutions. Going forward,
what weve previously referred to as the Annuities operation will be called
Lincoln Retirement. This change supports our focus on the important
retirement savings market. We provide a wide range of retirement
income products, including individual annuities, fixed annuities and
employer-sponsored plans. Sometimes rules need to be changed.
What happens when habits are broken and rules are changed?
Perhaps I can quip that the proof is in the pudding, and heres a look at
our pudding. Our total cash flows improved by approximately $8 billion
in 2001 over 2000. To put that number in perspective, it means that
on every business day of the year, Lincoln averaged a net cash flow
improvement in excess of $30 million. Another example of positive results
from a change in our rules is that 76 percent of our 25 largest retail
mutual funds outperformed their Lipper peer group averages in 2001.
We attributed our relative improvement to changing the rules in our
investment processes. In addition, Lincoln Financial Advisors has been
among the highest in productivity rankings of its peer companies. And,
our retail business lines continued to produce pace-setting products.
Today, we have significant cash proceeds from the sale of our
reinsurance business, and our stock has become a currency we could use
for acquisitions. However, just because we have these resources available
does not mean we will forget the basic principles of acquisitions. At the
core, this means only doing an acquisition if it creates shareholder value.

Among the primary ways to create value are increasing scale, gaining
access to markets or distribution, or building upon brand strength.
Yet, any value created through these means can be offset if too high
a price is paid. Fortunately, Lincoln is regarded as a company highly
skilled in creating value at the right price. Simply stated, we have an
established track record as a disciplined buyer. And, having just
completed our third straight year of strong relative total shareholder
returns means were not going to do anything to jeopardize that
shareholder performance record.
The next few years will be critical for financial services companies
as the landscape becomes increasingly competitive and consolidation
continues. However, we intend to thrive during these years. To succeed,
we will work hard to penetrate the marketplace with our numerous
financial innovations such as the Income4Life Solution. This solution
permits retirees to have the peace of mind of a stream of income they
cannot outlive, combined with the ability to withdraw a percent of their
account balance to meet unanticipated needs, and to do so while controlling
how the money is invested. We will also strive to offer products to
meet all of our clients ever-changing needs. For example, our Investment
Management business line has successfully created the foundation to
permit investors to choose high-performing investment vehicles in a wide
variety of asset categories. For the 12 months ending 2001, we have
growth stock funds, value stock funds, bond funds, tax-exempt funds and

international funds that are outperforming their peer group averages.
Since none of us know what economic conditions will be like in the
future, it is reassuring to know we only have to look to a single-fund
family  Delaware Investments  to meet our total investment needs. And,
we will be taking our high-performing fund family into the 529 market
(tax-advantaged college savings plans) in 2002.
We also will continue to enhance our solid distribution system.
At the end of 2001, we announced multiple new and expanded distribution
relationships with world-class financial partners. While none of
these contributed to growth during 2001, most will contribute to results
in late 2002 and beyond. Similarly, our unusual ability to create wealth
transfer strategies that combine life insurance, annuities, tax planning,
estate planning and investment management will be in high demand
as baby boomers age with their unprecedented levels of wealth.
Another critical element to our future success is the cultivation
of a truly talented and motivated workforce. In recent years, we have
worked hard to build a solid foundation of talent. Weve experienced a
significant level of change in our ranks and weve seen immediate
benefits. Within the industry, I believe were establishing a reputation
as a talent magnet and we intend to keep that momentum going. At the
end of the day, we are only as good as our employees  and now that
means we are very good. But were not satisfied, and we will continue to
distance ourselves from our competitors by continually building upon
the strengths of our talent base.
Finally, we come to brand. Since we stepped up our branding efforts
in 1998, we have spent almost $90 million to support our brand. We are
confident it has been money well spent. Since 1998, our awareness levels
have jumped by 41 percent within our key consumer target market and
more than 90 percent of financial intermediaries know the Lincoln name.
In spite of economic uncertainty, well continue to support our brand and
consider brand strength as we explore acquisition opportunities.
Thank you for your continued support of Lincoln Financial Group.
Jon A. Boscia
Chairman and Chief Executive Officer
March 21, 2002
