DEAR STOCKHOLDERS, ASSOCIATES, BUSINESS PARTNERS AND FRIENDS:
From the beginning, Liz Claiborne Inc. has been an innovator, a change agent, and a pace setter.
This past year was all about living up to and respecting that heritage. With a significant financial miss
in the first quarter and a dismal view of the year as a whole, we turned inward to look self-critically
at what needed to changenot just to address our short-term outlook, but to once again lead the
industry in terms of financial performance and overall reputation.
The resulting assessment led to a re-engineering of our vision and strategy, our organization
structure, our portfolio, how we allocate resources, and our culture and incentive systems.
Our journey in 2007 has been well-documented, by the analysts studying the market and by the news
media at large. As painful and visible as this restructuring has been, we are convinced our actions will
serve our stockholders well.
As we began our internal restructuring, the economy took its own direction, popping bubbles in the
credit and real estate markets and significantly impacting consumer confidence and spending patterns.
Our market capitalization suffered tremendously, along with the apparel industrys as a whole.
During these increasingly difficult economic times, we are focused on implementing the changes
necessary to prepare Liz Claiborne Inc. to lead the inevitable economic rebound with enviable sales
and earnings growth and newfound brand strength.
The strategy we announced, and are vigorously pursuing, is very simply a strategy based on branding.
Every action we have taken has been aimed at more successfully fundingand executingoutstanding
fashion brands.
These actions include:
 Narrowing our portfolio to concentrate on the businesses with the best consumer and retail traction.
 Investing wiselycutting smartly. We have eliminated more than 1,300 positions, including
approximately 25% of management positions, while investing in capabilities like design and
merchandising, inventory planning and allocation, distribution systems for vertical retailing, and
enhanced supply chain performance. We have placed critical new hires in key positions. And we
have invested more in marketing to strengthen our brands. 
 Respecting our heritage by engaging Isaac Mizrahi, one of the most popular and respected American
designers and fashion icons, to dramatically re-launch the Liz Claiborne brand for Spring 2009.
With Isaac as the creative visionary, we intend to return this brand to a predictable and healthy
source of earnings.
 Building a brand-centered company by aligning all the activities of a given brand under one cross
functional, fully accountable team. In the case of the Direct Brands, they have been structured,
and operate, as mono-brand companies. In Partnered Brands, we have unwound the complexities
to create more streamlined organizations where one brand vision drives the business. As a result, for
the first time in decades, the reward systems, the design and merchandising strategy and planning,
and the data and reporting systems are now aligned by brand.
 Emphasizing real entrepreneurship by encouraging our people to think and behave like owners
to be passionate, to dream and aspire, and to lead their businessesrather than wait for top-down
corporate directives.
Now that we have refocused our strategy and have a clear roadmap to win, our people are
embodying the guiding principles we launched last yearprinciples we have built into our
language; principles that direct our investments and culture. They include making irresistible product
our first priority, marketing to build buzz and cultural relevance, concentrating on talent, because we
are in a people and talent driven business, and innovating through new supply chain solutions,
because the world requires it.
We have great people, we have strong brands, and we are more precise than ever in our direction.
We appreciate your patience in the past year and hope you are looking forward to a very bright
future with us.
Sincerely yours,
William L. McComb
Chief Executive Officer