TO OUR SHAREHOLDERS:
One of the desires every human being has is to be appreciated. That is why, for the last 19 years, I have ended my annual
report letter by thanking our shareholders, customers and employees. Considering these challenging economic times, I am
moving this appreciation to the top. I know that I speak for each member of our board of directors and our management
team, when I say we appreciate the fact that as an investor you have chosen to invest in Microchip, as a customer you have
chosen to do business with Microchip, and as an employee you have decided to join and remain with the Microchip team. I
want to assure you that your support is not taken for granted.
Fiscal year 2009 was the most tumultuous year we have seen in Microchips history. The first half of fiscal 2009 was
excellent, with Microchip achieving record sales and profits in the second fiscal quarter. Then came the global financial
crisis, and the third and fourth fiscal quarters saw significant revenue and gross margin reductions. We ended fiscal 2009
with net sales of $903.3 million, down 12.8% from fiscal 2008. Despite fiscal 2009 being a very difficult year, Microchip
had numerous accomplishments, including:
?? We were one of the few semiconductor companies that remained profitable, even at the bottom of the cycle. In fiscal
2009, our net income was $248.8 million, which was 27.5% of sales.
?? Microchip is the only major semiconductor company that has avoided a layoff for more than five years, reaffirming our
core value that employees are our greatest strength in the face of the global downturn, and positioning Microchip for
strong growth when the economy recovers. Maintaining our employee base assists our customers as they bring new
applications to market, and helps enable our customers to introduce new products to achieve incremental market
penetration. The shared sacrifices of our employees including salary reductions, time off without pay, and suspension of
bonuses, exemplify the spirit that has made Microchip such a strong force in the marketplace over the last 19 years.
?? Based on Semiconductor Industry Association (SIA) data, we gained market share in all of our target markets -- 8-bit and
16-bit microcontrollers, and analog.
?? Microchip achieved a record quarterly dividend of 33.9 cents per share and then maintained that dividend through the
bottom of the cycle. Many large S&P 500 companies have cut their dividends in the face of the economic crisis.
?? We introduced approximately 125 microcontrollers, analog products and memory products.
?? Microchip acquired three small companies as part of our Elbow Out strategy. Specifically, we acquired Hampshire
Company (resistive and capacitive touch screens) to complement our mTouch capacitive and inductive touch-sensing
portfolio, HI-TECH Software (C-Compliers) to augment our own compiler offerings, and R&E International
(semiconductors for security and life-safety applications) to extend our strong market position in these segments.
?? We introduced the much coveted PIC32 family to production. During fiscal 2009, 18 PIC32 products were released to
production, and the new family has already received a total of eight industry awards for product excellence.
?? Microchip won the EE Times ACE Award for Company of the Year and the EDN Innovation Award for our 32-bit
PIC32 microcontroller family, among other honors for business leadership and product innovation.
?? Microchip shipped 136,531 development systems, a 16.9% increase over the previous fiscal year.
?? We introduced the worlds lowest power microcontrollers, with nanoWatt XLP Technology, to enable the latest
battery-operated and green applications.

As we begin fiscal year 2010, the global economic challenges are still with us. However, we are seeing the early signs of a
recovery shaping up.
I again express my sincere appreciation to our customers, shareholders and employees for your support during these difficult
times, and I am confident that we are well positioned for growth as we begin to move into a period of economic recovery.
Steve Sanghi
President and CEO
Microchip Technology Incorporated