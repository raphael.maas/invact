To Our Fellow Shareholders:
2016 was a remarkable year for Regency Centers. We continued our positive momentum in each
key facet of our business, further positioning our company as a blue-chip REIT.
The outstanding execution of our strategy in 2016 resulted in our third consecutive year of 7%-
plus growth in per-share core funds from operations (FFO), our fifth consecutive year of growth
in same property net operating income (NOI) of 3.5% or more, and nearly $600 million of new
investments into the development and acquisition of exceptional retail centers. We entered
the year with one of the strongest balance sheets in our peer group, and by astutely taking
advantage of favorable capital markets during the year we emerged from 2016 with one of the
best balance sheets in the REIT industry. Regency�s seasoned and dedicated team of top-notch
professionals made these accomplishments happen.

Importantly, we ended the year by announcing a proposed merger with Equity One, which
owned one of the highest-quality shopping center portfolios in the country that is incredibly
complementary to our assets. You�ve heard me articulate in the past � bigger is better, but better
is best. This was the lens we used when pursuing the opportunity. The combination of Regency
and Equity One not only creates a bigger company, but also clearly the best national owner,
operator, and developer of neighborhood and community shopping centers in the industry.
Creating the Preeminent National Shopping Center Company
This transaction increases Regency�s portfolio to more than 425 properties and nearly 60 million
square feet under management�featuring properties in the best markets in the U.S., with
attractive demographics, and anchored by the most productive grocers and retailers.
The merger, which on a pro forma basis is accretive to core FFO per share while preserving a
conservative balance sheet:
Q Substantially increases the average population density and purchasing power of the
affluent and infill target trade areas where our centers are located.
Q Significantly expands our presence in high priority target metro areas of Miami-Ft.
Lauderdale, New York, San Francisco, Los Angeles, and Boston.
Q Maintains our grocery-anchored focus: nearly 80% of our combined portfolio is anchored by
highly-productive grocers.

Q Enhances future prospects to grow same property NOI.
Q Creates more opportunities for our best-in-class national development and leasing platform to
unlock value within the combined portfolio, including outstanding redevelopment projects
In sum, the Regency-Equity One combination creates the leading neighborhood and community
shopping center REIT, a company exceptionally well-positioned to deploy the best team in the
industry to harvest value from our stellar portfolio, to harness Regency�s financial strength to
profit from opportunities, and most importantly, to deliver superior shareholder value. You can
see why we are so excited that the Equity One Board agreed to merge with Regency.
Executing on All Fronts in 2016
We continued to deliver gratifying financial results in 2016, extending a run of sector-leading
performance. Our financial highlights for the year included:
Q Core FFO per share grew 8.2%.
Q The same property portfolio ended the year over 96% leased, with small-shop leasing
continuing its impressive trend to 93% leased, the highest small-shop occupancy level in
Regency�s history.
Q Total rent growth topped 11%, with new shop rent growth a strong 16.6% for the year.
Q Despite the headwinds from retailer bankruptcies and historically high occupancy levels,
our same property NOI growth was still an outstanding 3.5%.
Q Development and redevelopment starts of exceptional shopping centers exceeded $200
million. Moreover, our shadow pipeline remains robust at more than half a billion dollars of
projects of comparable high quality.
Q Debt to EBITDA, a reliable proxy for balance sheet strength, was reduced to 4.4 times, one
of the lowest among all REITS.
These accomplishments combined to grow value for our investors as evidenced by Regency�s total
shareholder return, outpacing the shopping center peer average for one, three, five and ten years.

Development: Creating Value and Premier Shopping Centers
Regency has long been recognized as one of our industry�s most astute and forward-thinking
developers. Our development and redevelopment program is an important core competency and a
competitive advantage, creating premier shopping centers and exceptional value for our shareholders.
Since 2000, we have completed more than 200 developments with an estimated value creation of
nearly $1 billion. Our disciplined approach, which has been honed by our experience during both
expansions and recessions, is particularly relevant today due to the maturity of the current cycle.
Several new developments and redevelopments stand out as emblematic of Regency�s activities in
2016. These include:
Q The Village at Tustin Legacy, in Orange County, California�a ground-up development
started in the third quarter of 2016�is already over 80% leased. Situated within a masterplanned community and with an average household income of $105,000 and a 210,000
population within a three-mile radius, Tustin Legacy is a perfect example of the kind of
projects we undertake.
Q In Miami, Florida, we are in the process of a complete tear-down and rebuild of the
Aventura Shopping Center, which is located on some of the most desirable in-fill real estate
in South Florida. In its place will be a new contemporary center, anchored by a podiumformat Publix featuring parking underneath the store.
Aventura Shopping Center | Aventura, FL
Q We completed the $18 million redevelopment of our Encina Grande shopping center this
past November. Located along the busy commuter corridor of Walnut Creek, California,
Encina�s trade area includes a three-mile household income of $110,000. The project included
the replacement of a tired grocer space with Whole Foods, the relocation of a Walgreens
including an added drive thru, and a full fa�ade remodel and upgrades featuring oudoor
dining and plaza areas. The center is 100% leased.
Encina Grande | Walnut Creek, CA 
Executing our Strategy to Enhance a Strong Balance Sheet and
Portfolio Quality
During the year Regency once again prudently applied a key component of our disciplined funding
strategy. Our formula involves using dispositions of non-core properties, free cash flow and equity
when priced favorable to our view of net asset value to fund compelling investment opportunities.
When the equity market during the first half of 2016 was kind to REIT stocks, including ours, we
completed two successful offerings. These financings funded two outstanding acquisitions with
superior growth prospects and further strengthend our balance sheet, which is among the most
conservative in the REIT sector. At the same time our Core FFO growth rate was accelerated.
One of the purchases represented the largest single property acquisition in our history, Market
Common Clarendon. Clarendon is an iconic mixed-use center in the heart of one of Washington DC�s
most affluent suburbs. The 300,000 square foot retail center is anchored by Whole Foods, Apple and
Williams Sonoma. We teamed up on the $406 million transaction with one of the top multi-family real
estate companies, AvalonBay, which purchased the 300 apartments. Apportioning the multi-family
component to a best-in-class apartment company like AvalonBay exemplifies our approach to mixeduse properties.
Positioned to Thrive in the Face of Disrupters
The benefits from our capital funding strategies together with the experience of our management
team are why the current challenging environment for many retailers has not dampened my optimism
for Regency�s future. During the 40 years I have been in the business, retail bankruptcies and store
closures have been integral parts of the landscape. Failing retailers, recessions, shifting demographic
and ethnic trends, competition, consolidation, and changing formats have all played significant roles
in causing vacancies. Time and again our management team has demonstrated its skill in navigating
these storms whether it be failing grocers such as Dominick�s, Haggen and A&P, or drug stores moving
from in-line to out-parcels.
However, it is clear that technology, particularly the impacts of online shopping and alternate delivery,
is now the largest threat to many retailers and, in turn, to retail real estate. Technology will combine
with the other risks to accelerate disruption, which will continue the increase in store rationalization
for some retailers. As a result a number of retail centers will either struggle or not even survive.
Market Common Clarendon | Arlington, VA
At the same time �the winners,� those retailers who know how to connect with their customers, which
includes the effective use of technology, will prosper and continue to need bricks and mortar space to
service and sell to their customers. We believe that by continuing to strictly adhere to our investment
strategy, the vast majority of the centers in our portfolio will not only survive, but also thrive � for the
simple reason that the better retailers and restaurants will want to continue to locate where their
business will succeed. Regency�s portfolio abounds with those sought-after places - centers that enjoy
compelling demographics in the affluent suburbs and near-urban trade areas, and a merchandising
mix that features productive anchors. So when some future disrupter causes a retailer in our portfolio
to rationalize its store count, we have found that our centers tend to remain on that retailer�s roster of
locations. In those cases in which the user vacates, we are oftentimes afforded the ability to attract a
better replacement tenant at higher rent. Although from time to time our portfolio will not be totally
immune to short-term pain from disrupters, our portfolio management strategies will further ensure
that our centers remain in demand to the leading national, regional, and local retailers. In addition we
believe that our portfolio provides insulation from the growth in e-commerce, given its convenience to
the neighborhoods and communities that we serve and the fact that an extremely high percentage of
our tenants include grocers, restaurants and service providers.
Our Future: Great Promise, Realizing our Potential
As I look ahead, Regency�s path to continue to build a great company has never been more certain.
We have the pieces in place to thrive even in the face of accelerating store rationalization. Our portfolio
is better than it has ever been, with exceptional demographics and better retailers. We have a new
vista of opportunity to continue to pursue our disciplined approach to development and redevelopment.
Our pristine balance sheet affords us the flexibility to capitalize on compelling investment
opportunities. Our industry-leading operating systems, including our Fresh Look� merchandising
and placemaking philosophy, together with our Greengenuity� sustainability practices, are forward
looking. And most importantly, we have an incredible team guided by Regency�s special culture to
lead the way.
2017 will be a busy year, one not without its own challenges, as we execute our plan to integrate
Equity One and a number of its talented professionals. The goal is to bring together two terrific
companies so that the combination equals a world class REIT. Regency will also benefit from the
addition of three astute directors to our exceptional board � Chaim Katzman, Joe Azrack, and Peter
Linneman. We remain committed to executing on our time-tested strategy and maintaining the same
high level of excellence our constituencies have come to expect from us, which we know will deliver
growth in earnings, NAV, and shareholder value.
With the best team of shopping center professionals in the business, we will move forward with a clear
purpose and with our eyes very much focused on the long game of building and sustaining a great
company. On behalf of our entire team and board, I�d like to thank our shareholders, tenants, partners
and communities for their trust in us.
Sincerely,
Martin E. Stein, Jr.
Chairman and Chief Executive Officer