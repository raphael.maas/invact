Dear AIG Shareholder,
I have said it many times over the past
year, and, as Chairman of AIG, I believe
it is worth repeating: Thank you.
Thank you to the American taxpayers
for giving the people of AIG a
chance to prove what they are worth.
Thank you to the people of AIG for
realizing we were given a second chance
and for quietly, without pausing, looking
forward and working every day
with conviction. Thank you to Bob
Benmosche, AIGs President and Chief
Executive, for persevering against all
odds imagined and unimagined, and
never wavering from what he believes.
In my career, I have been honored
to be part of and witness dramatic turnarounds.
The story of AIGs transformation
and the progress this company
has made, from my vantage point, are
remarkable. Bob deserves tremendous
praise and respect for everything
accomplished at AIG since he joined in
August 2009. To quote the old idiom,
Time will tell, but I predict that Bob
and the people of AIG may have
orchestrated the largest turnaround
in corporate history.
Less than three short years ago,
AIG leadership was steering this great
company to its breakup. Under the
theory that selling off the companys
assets was the best way to generate the
funds to repay American taxpayers,
legal mechanisms were put in place,
businesses were rebranded, and assets
were being sold. Unfortunately, these
efforts would have fallen far short of
repaying our obligations to the taxpayers.
Then Bob came along with an
outsiders fresh perspective of what
it meant to compete against AIG. As
former Chief Executive of another large
insurance company, Bob recognized
that AIG was made up of proud professionals
who were the best, the most
innovative, and consistently the fi ercest
competitors in nearly every core business
in which AIG operated.
Bob asked the Board, Isnt AIG
worth more together than the sum of
its parts? Arent AIGs people worth
more? These questions begged a
change in course  and made us reconsider
our plans. Bob knew the answers
and set about planning and executing
what ultimately raised nearly $45 billion,
nearly every cent directly payable
to the U.S. government. Eventually,
with the U.S. Department of the Treasury
and the Federal Reserve convinced
that the breakup of AIG wasnt necessary,
the complete recapitalization of
the U.S. government investment was
put in motion.
The unprecedented accomplishments,
persistence, and commitment
of Bob and the people of AIG raise
the bar on what we as Board members
think is expected of us. As a Board, we
feel the need to do our part to ensure
that these accomplishments are not
wasted. We certainly have fi duciary and
governance responsibilities mandated
by corporate charter and regulators,
but this Board feels a greater sense of
purpose and responsibility.
We are continually planning for the
future and actively engaged in providing
this unbeatable AIG team with the
tools and the leadership to continue to
be in the best position to compete. A
top priority is ensuring that AIG has the
appropriate controls over risk. Much of
this work on risk helps us be prepared
for additional regulatory supervision.
And we have a particularly keen sense
of how AIG needs to be a responsible
corporate citizen in this new era.
We are also actively working with
AIG management on certain key projects.
For example, the Board formed the
Special Technology Committee to oversee
AIG managements work to enable
the people of AIG to manage risk and
business processes in a very complex
environment.
Another priority is succession
planning. Like me, Bob joined AIG out
of retirement, and while he has made
commitments to stay for longer than
we anticipated, we are planning for his
eventual retirement. The AIG Board of
Directors has devoted much to being
prepared for the future, and we have a
strong governance structure in place.
AIG made tremendous progress
during 2011. This was the year that
galvanized the people of AIG to put
the crisis behind them and refocus on
business fundamentals. On behalf of the
AIG Board of Directors and everyone
at AIG, thank you for your continued
support and for allowing us to really
prove what AIG is worth.
Sincerely,
Robert S. Miller
February 29, 2012