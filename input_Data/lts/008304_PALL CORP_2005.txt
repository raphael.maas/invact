To Pall shareholders, customers, and employees
Fiscal 2005 was a remarkable year at Pall Corporation. Why?
We did make steady progress, continuing to build the top and bottom lines. Yet it is the context
in which we did so that is truly eye-opening. A business model in transition, an organization
undergoing an extreme makeover, two key markets and our largest geographic region sluggish 
nothing could stop us. Our increasing leadership in intrinsically high-growth markets including
water, biotechnology and energy is creating irrepressible momentum.
Customers today want integrated systems and one partner to support their broadest Total
Fluid Management (TFM) requirements whenever and wherever they need them. Palls business
model has been responsive, migrating from a niche marketer of specialty products into a broad
provider to our key markets. A vital component of this strategy is the ability to develop and sell
large, integrated systems throughout the world. Our strategically important systems business is
on fire, with sales up over 27% for the full year. Systems now make up 10% of Palls overall sales.
Pall systems vary widely across markets. Applications include beer production, purification of
biomolecules and production of drinking water. Our systems utilize sophisticated Pall engineering
expertise. Because they are crucial to customer processes, our systems business elevates the
level and breadth of customer relationships.
The selling price for a Pall-engineered system ranges from thousands to millions of dollars.
Systems have a higher cost of sales because they contain many purchased components and
machined parts. The rapid ramp-up of system sales has put temporary pressure on margins. The
reward for Pall shareholders comes with the annuity stream of disposable filter sales that follows.
In fiscal 2005, we saw this start to occur when the two markets with the largest growth in system sales, BioPharmaceuticals and Fuels and Chemicals, also showed the largest increases
in disposables sales.
Running parallel to our sales efforts, we began a far-reaching and ambitious transformation of our
organization. The old structure, with a large corporate hub of shared manufacturing and
technology, served us well for many years. When we were smaller it allowed the efficient
development of strong Pall market positions across our game board of over 100 basic SIC codes.
That phase is now almost behind us. Today we are focused on building up our capabilities within
those markets and, just as important, leveraging our greater size to significantly reduce costs.
The new structure is straightfoward. We now have two separate businesses, Industrial and
Life Sciences. Both are being integrated top to bottom. Most corporate and shared service
functions, including manufacturing, research and development and technical services, are being
reassigned between them. We aim to drive resources into customer growth-focused initiatives
within the businesses. This structure also facilitates our continuing work of reducing infrastructure
costs. We have the momentum and experience here to be overachievers.
This activity would have been more than enough excitement for one year, but Sarbanes-Oxley
also made a house call in 2005. We successfully managed the complex SOX compliance effort
through a major focus of management throughout the Pall world. The cost was substantially
higher than expected, but so are the benefits, as processes were strengthened and streamlined.
The 2005 sales and earnings increase was accomplished despite the fact that the Military
Aerospace and Blood Filtration markets faltered and Europe was sluggish at best. How did we do
this and what does it mean for 2006? The business result held together because of an increasingly
young, well-positioned disposables and systems product line. It held because of our strong ability
to market across the globe and particularly in Asia. And, it held because of our absolute insistence
that sales growth be accompanied by a reduction in costs.
What lies ahead for Pall?
Our fast-growing water business will continue to flourish. The municipal Water Processing division,
which took top honors for sales growth in fiscal 2005, is expecting another record year with all
geographic regions growing double digits. Water scarcity and environmental concerns are unyielding.
While Water Processing is our fastest-growing division, it accounted for only 23% of Palls
estimated $350 million in water filtration sales. All Pall customers, both Industrial and Life Sciences,
have complex water purification needs. In the hospital setting, Legionella and other often multiple
antibiotic-resistant bacteria, strain health care budgets and build a mountain of mostly preventable
human suffering. Our broad capabilities and experience in contamination and infection control, of
which water filtration is just one part, uniquely position Pall as a solutions provider.
In the industrial arena, our recently expanded water-focused alliance with General Electric
complements our capabilities and expands access to the large global desalination market. We
have already enjoyed significant wins together and enter the new fiscal year with a burgeoning
pipeline of projects.
With a business as eclectic as ours, it helps to look at a few of the key drivers across our
markets. Fuels and Chemicals posted its eighth consecutive quarter of growth, finishing the year
up 15%. World demand for oil, fuels, chemicals and plastics remains robust. New sources of
energy, such as coal gasification, are Pall technology strongholds.
Aerospace was a tale of two cities, up in Commercial and down in Military. The $10 million
reduction in operating profit really hurt our overall results. We do expect to have Aerospace
profitability approach its traditional range in fiscal 2006. Heres why:
Commercial Aerospace, coming off a solid year, is again poised to exceed industry growth
rates of 5%. Our success rate on new airframe platforms remains high. We have won new or
extended contracts with most major airline and cargo carriers, as well as customers in the
corporate and general aviation markets.
We expect our Military business to spring back in 2006. Last February, we announced a
preliminary $5.6 million order from the U.S. Army to outfit CH-47 helicopters with engine air
particle separators. We have since received another $5.6 million order for the same program.
Military spares orders are also likely to pick up to refill depleted inventories.
Microelectronics had a quiet year, stronger in the first half than the second for 4% overall
growth. Pall is superbly positioned in this rapidly innovating and notoriously cyclical business.
Looking forward, we foresee steady growth in 2006, particularly in Asia, as new technologies such
as flash memory, micro disc drives and the new flat panel display technologies drive
manufacturing investments.
Total Medical sales lagged as we experienced weakness in our Blood Filtration and Hospital
markets in the Western Hemisphere. Despite this, Medical operating profit margins improved to
18.6%, primarily thanks to our CoRe cost reduction programs. We recently announced several
new agreements for blood filtration products. One is a major long-term agreement with the
American Red Cross (ARC) for the majority of its blood filtration system requirements. The ARC
is a key advocate and strong voice for blood safety technologies in the U.S.
Preliminary evaluations of our new Leukotrap Affinity prion reduction filter are progressing in
Europe. The availability of this unique product is a seminal event heralding a new era in blood
safety. We are working closely with health authorities, starting with the nations hardest hit by
vCJD, the human form of Mad Cow Disease, to help protect the safety of the blood supply and
prevent the spread of this insidious disease.
BioPharmaceuticals produced an excellent year with sales up 12%. System sales grew
explosively throughout the year and were matched with a strong increase in follow-on disposables business. Systems fuel annuity sales. Growth was particularly strong in the Western
Hemisphere, as several biotech drugs went into full production. In another positive development,
sales in Europe were up over 27% in the fourth quarter, with system sales exceptionally strong.
These sales signal that Europe is entering the biotech investment phase. With our acquisitions of
Biosepra and Euroflow, Pall emerged as a stronger player in the process chromatography market.
Chromatography is a large market  estimated at about 40% of the total biotech industry spend
for filtration and purification.
Pall Corporation is in hot pursuit of its vision that one day all fluids will pass through Pall
products. Were going after it by expanding our footprint in customer facilities, by focusing on
high-growth regions, markets and applications and by using alliances and partnerships to
supplement our own substantive R&D efforts. This vision and these primary strategies guide the
decisions and actions of Pall employees around the world.
We have made the job ahead significantly easier for ourselves by setting in motion a
complicated consolidation and integration of our businesses. I applaud Palls employees for
taking on this challenge, and doing so all without taking their eyes off our customers. With our
fortified Industrial and Life Sciences groups well armed to compete globally and successfully
against the toughest competitors, Pall customers are already receiving even better service and
support.
The strong foundation laid in 2005 is prologue to a very exciting future. I, along with our senior
management team, have never been more energized nor more confident in Palls future. We look
forward to keeping you apprised of our progress throughout the year.
Eric Krasnoff
Chairman and Chief Executive Officer