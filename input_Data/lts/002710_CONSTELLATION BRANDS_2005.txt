

     Dear fellow shareholders:

     Constellation's stellar performance continued in fiscal 2005,
     and I am pleased to inform you that our results exceeded
     the company's previous achievements. On the pages that follow
     you'll learn how we attained those excellent results and
     established new benchmarks of performance excellence for
     Constellation Brands, such as record annual sales, net
     income, earnings per share and other important metrics.
     In fiscal 2005 we surpassed the $4 billion (USD) net sales mark for the first time in our company's
     60-year history, representing a year-over-year increase of 15 percent. Our annual net income
     increased 25 percent compared with the prior year, also setting a record. In addition, the last
     three quarters of fiscal 2005 each exceeded $1 billion (USD) in net sales, setting another
     record. While the numbers are always important, how we achieve, and exceed, our growth targets
     is what I believe differentiates Constellation Brands from its peers in the international beverage
     alcohol business.

     True growth
     We are achieving true growth on a daily basis, which we define as growth generating an economic
     profit on our investments above our cost of capital. Not all companies or products generate
     true growth, so I am particularly gratified to be able to talk about how well our company and
     brands performed in 2005 since we invested appropriately to support them.

     I am also pleased with our results because we met the few challenges that we anticipated would
     materialize throughout the year, and we took advantage of opportunities within the highly






                                                                  competitive and consolidating business environment we operate in daily. Constellation Brands
Strategy gained momentum
Constellation Brands' true growth strategy gained                 people proved to be more than up for the responsibilities at hand, which speaks volumes about
momentum throughout fiscal 2005, contributing to finan-           our entrepreneurial structure and corporate culture, as well as the character and mettle of
cial performance results that yielded several records. While      our global team. We triumphed and prospered to bring you the true growth performance you
record net sales, net income and other milestones are             expect, and deserve, from Constellation Brands.
gratifying and noteworthy, they could not be achieved with-
out quality products and a commitment to true growth
                                                                  The right brands
on the part of our 8,000 dedicated employees worldwide,
                                                                  We always look for opportunities to meet consumer needs which, in turn, strengthens our
whose immeasurable contributions led to our success.
                                                                  relationships with distributors and retailers and adds breadth and scale to our business.
Additionally, our portfolio breadth and operational scale         It is a philosophy and business model that works. Real insights help us create the right products
helped us to increase our business and smooth out any             to attain real reach and achieve true growth. Having the right brands is one of the keys to
peaks and valleys that routinely occur during the course          success and, with more than 200 brands in our portfolio, we have unmatched breadth. Having
of any given year and business cycle. Having a balanced           the right brands for distributors, retailers and consumers requires that we employ our wealth
portfolio, distribution strength and focused employees            of real insight into trends, as well as our vision to create products that appeal to our customers
allows us to operate efficiently and effectively in an increas-   and consumers for the long term. Maintaining the right portfolio means we add, enhance, modify
ingly more competitive and consolidated business world.           and, occasionally, retire brands as appropriate to meet increasingly more diverse consumer tastes.

In concert with our breadth and scale, our culture and            Our strategic portfolio development comes in the form of new product development, line exten-
values help guide us along the path of true growth success.       sions, acquisitions, partnerships and even differentiated packaging. Last year I spoke of the
We continue to gain momentum as we journey forward in             integration of Hardy Wine Company Limited into Constellation Brands, and how valuable that
our ultimate quest to be the beverage alcohol provider of         addition became to our offerings, particularly in the United Kingdom and the United States.
choice for consumers, distributors and retailers the world
                                                                  In the fourth quarter of fiscal 2005 we enhanced our offerings once again, this time with a trio
over. To that end, we made great strides in fiscal 2005
                                                                  of investments that added more of the right brands to our portfolio.
by leveraging our global purchasing power and migrating
to a common business applications platform, in addition
                                                                  Robert Mondavi joins the Constellation family
to integrating the three transactions we closed in December
                                                                  The most notable of these transactions, and the one that created new world wine history, was
2004, the acquisition of The Robert Mondavi
                                                                  the acquisition of The Robert Mondavi Corporation on December 22, 2004. An industry
Corporation, the joint venture to market Effen Vodka and
                                                                  icon, Robert Mondavi is credited with being the patriarch of fine wine production in California's
create other premium spirits and our investment in Ruffino
                                                                  Napa Valley, with the To Kalon vineyard being among the best terroir in the world. His wine
Italian fine wines. It was a banner year for Constellation
                                                                  legacy has given us some of the most popular and finest wines coming from California, and we
Brands and its true growth strategy.




                  are delighted that he and his wife Margrit stayed with us as   Smooth move brings Effen premium vodka aboard
                  ambassadors for the magnificent Robert Mondavi Winery.         Our premium spirits business saw Effen Vodka added to the
                                                                                 portfolio through a joint venture with product innovator
                  Our entrepreneurial structure and financial resources
                                                                                 and marketer, jstar Brands, to form Planet 10 Spirits.
                  allow us to be nimble when an opportunity such as Robert
                                                                                 This premium, flavored vodka comes from Holland, and
                  Mondavi comes along. In the fall of 2004 we took quick
                                                                                 the name "Effen" means "smooth" and "balanced" in
                  and decisive action to ensure that the Robert Mondavi
                                                                                 Dutch. Its packaging, name, positioning and marketing
                  brands remained intact as a valued part of our portfolio.
                                                                                 created excitement in the initial test markets, and Effen is
                  We are encouraged by the growth opportunities for Robert
                                                                                 now rolling out nationwide. With the ever-increasing popu-
                  Mondavi brands, both in the United States and abroad,
                                                                                 larity of vodka in the United States, we are confident that
                  and will continue to build upon the rich heritage, tradition
                                                                                 Effen will be a resounding success.
                  and quality represented by these superb wines.

                                                                                 New products and existing brands
                  The taste of Tuscany
                                                                                 Yet, acquisitions were only part of the story in 2005. We
                  In early December 2004 we became a 40 percent owner
                                                                                 also added new products to our portfolio, relaunched
                  of Ruffino, the fine wine company producing excellent
                                                                                 others and created numerous line extensions. New wine
                  products from the Tuscany region of Italy. Dating back to
                                                                                 brands that added more breadth to our offerings include
                  1877, and owned by the Folonari family since 1913, Ruffino
                                                                                 Turner Road, Twin Fin, Lorikeet, Monkey Bay and
                  exported 600,000 cases of wine to the United States in
                                                                                 Kelly's Revenge.
                  2004. In addition to our 40 percent ownership, Constellation
                  Brands became the importer of Ruffino brands to the            We also launched Ridgemont Reserve 1792 Kentucky
                  United States on February 1, 2005. Incremental growth          small batch bourbon, which is aged for eight years. It was
                  potential for Italian wine in the United States makes          quickly dubbed the toasting bourbon for consumption at
                  Ruffino a wonderful addition to our portfolio.                 special occasions. 99 Oranges became the newest member
                                                                                 of the "99" line of cordials, and Chi-Chi's added Mango
                                                                                     




Margarita to its lineup of premixed cocktails. St. Pauli        continuously energizes the 8,000 dedicated Constellation
Girl imported German beer added a draft version, Tsingtao       Brands people who bring home stellar results such as
beer from China got new 12-packs and our Mexican                those in fiscal 2005. Collectively, the Constellation Brands
imported beer portfolio strengthened its market share           team constantly raises the corporate performance bar for
position in our highly competitive beer territory, while        themselves, which benefits our customers, our consumers,
Corona Extra extended its lead over the second place beer       and you, our valued shareholders.
imported to the United States.
                                                                Strategic vision
All in all, we increased our existing portfolio 9 percent
                                                                Our goal remains to be the beverage alcohol provider
in an environment where beverage alcohol only grew mar-
                                                                of choice for all occasions. That's what we strive to achieve
ginally. Much of this growth was accomplished through
                                                                in a dynamic industry and competitive business environ-
disciplined and targeted investments in marketing that
                                                                ment. Our commitment to, and focus on, having the right
supported key brands.
                                                                products, real insights and operational scale to deliver
                                                                true growth is unwavering. Our passion for maximizing
Entrepreneurial structure and culture
                                                                revenue, minimizing costs and maintaining the high
As I mentioned early in this letter, the numbers will always
                                                                quality of our products is woven into Constellation Brands'
be important, and the brands will always be important, yet it
                                                                cultural fabric, and gives me a great sense of pride in our
is our entrepreneurial structure and culture that empowers
                                                                past accomplishments, as well as those we will achieve in
Constellation Brands people to make daily decisions in
                                                                fiscal 2006 and beyond.
the best interest of building the business while profitably
growing our brands and generating shareholder value.
Our culture fosters strategic, competitive thinking by people
throughout each operating company, and challenges
everyone to create new and better ways to run the business.

The nurturing of entrepreneurial spirit, combined with the      Richard Sands
freedom to try new and different strategies and tactics,        Chairman of the Board and Chief Executive Officer
