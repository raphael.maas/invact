To Our Shareholders:




I am pleased to report that 2006 was a very good year      We deliver on this commitment by focusing on three
for D&B, as our financial performance demonstrates.        key drivers:
We delivered core revenue growth of 6%, with positive
contributions from both our US and International               First, ensuring the consistency of our performance.
                                                           I

segments. Operating income grew 8%, which increases
to 12% excluding the impact of stock options, which          Second, ensuring that we deliver profitable revenue
                                                           I

we began expensing in 2006. Earnings per share grew        growth.
14%, marking our sixth consecutive year of double-
digit earnings growth. And free cash flow grew to          I And third, maintaining our discipline around the

$298 million, before the impact of legacy tax matters.     deployment of cash.

To put this performance in context, it has been more       These were the key drivers of our performance and
than six years now since the spin-off that brought this    growth in 2006, and they'll continue to drive our
historic company back to its roots. At that time, we       progress in the future. Let me highlight what these
announced our intention to transform D&B into a high-      drivers mean to us.
performing growth company. To deliver on that goal,
we created a strategy that we called our "Blueprint        Consistency of Performance
for Growth." Our consistent focus on this Blueprint        First, consistency of performance at D&B means "doing
strategy has been very successful and, today, we are       what we say." We're pleased with the results we've
a high-performing growth company, as evidenced             delivered over time, and we're proud of our unique
by our financial performance over time.                    track record of consistency. In fact, 2006 was another
                                                           in a series of positive years along the way for D&B.
Specifically, we've increased core revenue by more
than 40% since 2001. At the same time, we improved         Looking ahead, we're confident in our ability to once
operating income by 65% and more than doubled our          again deliver strong results and total shareholder return
earnings per share. In addition, we increased our free     in 2007. That confidence is demonstrated by our 2007
cash flow by 82%, which has allowed us to repurchase       financial guidance, which calls for ongoing profitable
more than $1 billion of D&B stock. As a result of this     revenue growth and continued improvement in all of our
performance, shareholders who have been with us            key financial metrics. Specifically, we expect to deliver:
since the spin-off have realized a 370% return on
their investment in D&B by year-end 2006.                  I Core revenue growth of 6 percent to 8 percent, before

                                                           the effect of foreign exchange, all of which will be
We're very proud of these results, and we intend to        organic.
extend this track record of growth and performance.
We're very clear on how we'll drive value going forward.     Operating income growth of 8 percent to 10 percent,
                                                           I

                                                           before non-core gains and charges.
A Commitment to "Total Shareholder Return"
As we've emphasized over time, D&B is a company            I Diluted EPS growth of 13 percent to 16 percent

that's committed to driving total shareholder return.      before non-core gains and charges.


2   D&B Annual Report 2006
 Free cash flow of $310 million to $325
I

million before legacy tax matters.

  A tax rate before non-core gains and
I

charges of approximately 37 percent to
38 percent.

Our ability to deliver this guidance in 2007
is enabled by the four core competitive
advantages we've developed over time.                   " As we've emphasized over time,
These include our trusted brand, which
                                                                D&B is a company that's
is increasingly associated with global
commercial insight; our financially
                                                            committed to driving total
flexible business model, which allows
us to unlock savings that we can then
                                                                   shareholder return."
reinvest in growth; our winning culture,
which is strongly focused on leadership
development; and our proprietary
                                                              Second, growing our Sales and Marketing Solutions
DUNSRight quality process, which is the core value          I

                                                            business by focusing on the higher growth space of
proposition we sell to our customers. A more detailed
                                                            commercial data integration.
description of these core competitive advantages is
presented on pages 6-7 of this Annual Report.
                                                              And third, growing our Internet Solutions business
                                                            I

                                                            by continuing to grow Hoover's while investing in one
Profitable Revenue Growth
                                                            or two new platforms to leverage the expertise we've
A second key driver of total shareholder return at
                                                            developed at Hoover's over time.
D&B is delivering profitable revenue growth as we
execute against the strategic stakes we've identified
                                                            We're only just beginning to execute against these
for the future.
                                                            stakes, and we are pleased with the early progress
                                                            we've made.
At our September 2006 Investor Day event, we
announced our fundamental strategic choice to
                                                               From a Risk Management data perspective, in
continue being the world's largest and best provider        I

                                                            November 2006 we announced our creation of a
of commercial insight about businesses. Within
                                                            new joint venture in China called Huaxia/D&B. We
the commercial insight market, we identified three
                                                            believe this partnership will significantly enhance
strategic stakes for the future:
                                                            our competitive position in China, and it will allow
                                                            us to double the number of Chinese business records
  First, growing our global Risk Management Solutions
I

                                                            in our database over the next few years.
business by continuing to improve our data, analytics
and platforms.






   From a Risk Management analytics perspective, in          of 2006, we signed product and technology agreements
I

February 2007, we announced our signing of a joint           with The Acxiom Corporation that have significantly
venture agreement in Chennai, India, to create a new         enhanced our data integration capabilities. By taking the
"D&B Predictive Analytics Center." The joint venture is      best of our own "Customer Information Management"
with our Indian partner company DBSAME (D&B South            solution and linking it with Acxiom's advanced
Asia Middle East), which has conducted analytics work        computing power, we can now provide D&B customers
for us and our customers in a number of markets over         with improved processing speed, increased capacity
the past two years. We believe this joint venture will       and higher levels of commercial insight. We made
significantly enhance our ability to provide advanced        significant investments to establish this relationship
analytic solutions to customers around the world.            in 2006, and to introduce our advanced "Optimizer"
                                                             commercial data integration solution, powered by
   From a Risk Management platforms perspective,             Acxiom. We continue to work closely with the Acxiom
I

we continue to drive good results from DNBi, which is        team to build a robust sales pipeline for Optimizer,
the interactive, Web-based application we rolled out         which we expect will progressively benefit us over
in the fourth quarter of 2005. By upgrading to DNBi,         the course of 2007.
customers receive an enhanced risk management
experience and virtually unlimited, real-time access to        Finally, with respect to our third strategic stake, to
                                                             I

our global database. In return, we require an increased      grow our Internet business, we are focused on growing
revenue commitment. Our target is a double-digit             our Hoover's business organically, while also seeking
increase, which generates a sizable improvement in our       to acquire new platforms that will allow us to target
average revenue per customer with each new contract.         business professionals seeking commercial information
While we are still early in the DNBi adoption cycle, we're   on the Web. These new platforms will allow us to take
seeing strong conversion rates, particularly in the small    our current assets, such as our brand, DUNSRight, and
and middle markets where the need for this subscription-     our unique Web-marketing skills, and leverage them
based solution is most acute. To build on this, we           more broadly across the Internet.
introduced new modules for DNBi in 2006, meeting
customer needs for automated decisioning and portfolio       Disciplined Deployment of Cash
management. These modules also generate a double-            The third key driver of total shareholder return at
digit price lift for us while further embedding DNBi into    D&B is our highly disciplined approach to deploying
our customers' business. Moving forward, we'll continue      our free cash flow, and we see 2007 as an important
to expand our DNBi penetration. And we will continue         year for D&B as we focus on both organic growth and
to innovate  rolling out at least one new DNBi module       acquisitions to advance our strategy. We continue to
per year. As a result, we expect DNBi to continue to be      target three key priorities for our use of cash:
a key growth driver for our Risk Management business.
                                                               First, we'll continue to invest in the business to drive
                                                             I

   We're also making progress against our second             organic growth.
I

strategic stake, to grow our Sales and Marketing
Solutions by further penetrating the high-growth             I Second, we continue to look at smart, "value-

commercial data integration space. In the third quarter      accretive" acquisitions.


  And third, we're committed to returning excess cash         Looking Ahead
I

to our shareholders.                                          We are pleased with our performance in 2006 and the
                                                              progress we've made over the past six years, since we
In terms of acquisition spending, we anticipate               set out to transform D&B into a high-performing
completing between $300 million and $500 million              growth company. At the same time, we're confident
in total acquisitions between now and the end of 2010.        about our prospects for the future. While we continue
We have a good pipeline of acquisition candidates             to perform well, we think there's lots of opportunity
we're evaluating today. And you can count on our              still ahead.
continued rigor around acquisitions, ensuring that
any candidate we pursue provides a good strategic fit,        We have just begun to execute against the strategic
and delivers returns well above our cost of capital.          stakes we identified for the future. And the core
                                                              competitive advantages we've developed will continue
Finally, in terms of returning excess cash to shareholders,   to serve us well. Going forward, we will continue to
we have a strong track record of share repurchases at         invest in our business, leveraging organic growth
D&B. As I noted, we've completed more than $1 billion         and acquisitions as we drive toward our longer term
in special program share buybacks over the past several       growth strategy. In doing so, we will remain highly
years. And, unlike a lot of companies, we've also             disciplined in the use of our shareholders' cash.
completed every program that we've announced on               Above all, we will maintain our commitment to
or ahead of schedule. Going forward, we remain                driving total shareholder return.
committed to returning around $200 million per year
to shareholders through our special repurchase                On a personal note, it is a great honor for me to
program, while also making ongoing share repurchases          lead this storied company as your CEO. Every day,
to offset dilution from our compensation plans.               I feel a tremendous amount of pride about what the
                                                              D&B team has accomplished for our shareholders
As we announced in February 2007, we have also                over the past several years. At the same time, I feel
initiated a quarterly cash dividend, representing an          a great deal of responsibility and excitement for
annualized 20% payout of our 2006 free cash flow.             what is still possible.
We see this as a milestone event for D&B, and it carries
a number of important messages:                               I thank you for your continued support. And I look
                                                              forward to reporting our further progress in 2007.
  First, it reflects our confidence in the sustainability
I

of our free cash flow going forward.

  Second, it allows us to offer a more complete total
I
                                                              Steven W. Alesio
return package and attract a broader mix of investors.
                                                              Chairman and Chief Executive Officer

  And third, it underscores our commitment to
I

deploying our cash strategically to drive total
shareholder return.

