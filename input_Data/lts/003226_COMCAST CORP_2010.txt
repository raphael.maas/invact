

DEAR FELLOW SHAREHOLDERS,
AS WE ENTER A NEW DECADE, OUR
COMPANY HAS NEVER BEEN BETTER
POSITIONED, HAVING TAKEN IMPOR-
TANT STEPS TO BECOME STRONGER,
MORE ADVANCED TECHNOLOGICALLY,
AND MORE CUSTOMER-FOCUSED.
Of course, the NBCUniversal transaction captured           As free cash flow has grown, we have accelerated
many of the headlines in the last 15 months. We            our return of capital to shareholders. In the last three
entered into it with the belief that putting great con-    years, we increased our dividend by 80 percent and
tent and great distribution together under one roof        repurchased $4.8 billion of stock. We plan to repur-
would allow us to drive the "anytime, anywhere" digital    chase $2.1 billion more in 2011.
future consumers demand. And now, having achieved
                                                           Even with a still-recovering economy, more video
scale in both areas, we see excellent opportunities
                                                           competition, and the impact of the country's digital
to continue to invest and grow, to drive innovation even
                                                           transition, we executed well as we continued to bal-
further, and to build long-term shareholder value.
                                                           ance financial and customer growth. We ended the
Overall, we have terrific momentum in our operating        year with 1.3 million net new customers, retaining our
and financial performance. In 2010, we had solid           position as America's leader in video and high-speed
growth in consolidated revenue, operating cash flow,       Internet, and now the fourth-largest residential voice
and operating income. Significantly, our free cash         provider. We also participated in a revived advertis-
flow climbed 22 percent year over year  our third         ing market.
straight year of 20 percent-plus FCF growth  reflect-
ing our discipline in managing expenses and capital.





                   Increasingly, our growth and our customer satisfac-       in with a customer-first attitude and hit the ground
                   tion depend upon continued innovation, which is           running with our existing, outstanding cable man-
                   powered by our superior digital platform. We dra-         agement team. Neil has also ramped up innovation
                   matically expanded our deployment of All-Digital,         and is relentlessly focused on execution  and we are
                   which let us triple our HD channel offerings, offer       already seeing results.
                   twice as many channels for ethnically diverse audi-
                                                                             Our CFO Michael Angelakis and his team put together
                   ences, and bring even more content and conve-
                                                                             a brilliant transaction structure for NBCUniversal that
                   nience to On Demand. It also makes our network
                                                                             worked for everyone, and then assembled financing
                   more efficient, so as we rapidly deploy DOCSIS 3.0,
                                                                             on terms better than we originally expected. This
                   we can offer increasing speeds and capacity to the
                                                                             transaction is just one example of the discipline he
                   residential and business users of our high-speed
                                                                             brings to our financial and capital allocation decisions,
                   Internet service.
                                                                             which help make the company stronger every year.
                   As we make ever-greater use of Internet Protocol (IP),
                                                                             The transaction faced a complex federal regulatory
                   we can do even more to enhance our customers'
                                                                             approval process, but our Executive Vice President
                   experience by offering more choice and control.
                                                                             David L. Cohen and our government affairs team
                   We've greatly improved our electronic program
                                                                             worked with the governmental agencies to find the
                   guides and our DVR functionality, and added inter-
                                                                             right balance, arriving at an outcome that assures
                   active features. We've also launched our authenti-
                                                                             the transaction's public interest benefits, while allow-
                   cated "on demand online" service, XFINITY TV, and
                                                                             ing us to effectively execute our plans for growth and
                   made the first of our XFINITY TV apps available for
                                                                             innovation. David's team also forged a series of agree-
                   portable devices. These complements to our cable
                                                                             ments with leaders of diversity organizations that will
                   service enhance the customer experience and rein-
                                                                             inform the shared goal of Comcast and NBCUniversal
                   force the value of our customers' monthly subscription.
                                                                             to become industry leaders in diversity.
                   We are equally committed to improving customer
                                                                             We are beginning to write another great chapter in
                   service. And, in combination with the rebranding of
                                                                             Comcast's history. The company that my father,
                   our products as XFINITY, this is driving improved
                                                                             Ralph, first took public in 1972, with $5.7 million in
                   perceptions and customer satisfaction. Our reputa-
                                                                             annual revenues, has now become a $54 billion
                   tion among our customers for being technologi-
                                                                             company (pro forma revenue for 2010) with the com-
                   cally advanced and offering good value for money
                                                                             bination of Comcast and NBCUniversal.
                   is up dramatically, and non-Comcast customers are
                   more willing to give us a try.
                                                                             From its humble roots, Comcast has become a leader
                                                                             and a dynamic company positioned to build value for
                   I couldn't be more proud of our management team
                                                                             our customers, our employees, and our sharehold-
                   and the more than 120,000 employees across our
                                                                             ers. I have huge confidence in the future of all our
                   businesses that maintained our financial and operat-
                                                                             businesses. With our entrepreneurial culture, out-
                   ing momentum while working hard to complete the
                                                                             standing assets and the collective creativity, dedica-
                   NBCUniversal transaction.
                                                                             tion and talent of our people, we will continue to develop
                   With decades of experience in the broadcasting,           and deliver products, services and content that consis-
                   cable, and entertainment businesses  including           tently delight our customers and our viewers.
                   more than a decade as President of our Cable Division,
                                                                             I am honored to help lead this amazing company.
                   and service as Comcast's Chief Operating Officer 
                   Steve Burke was ready to lead NBCUniversal. As
                   its CEO, he is overseeing a smooth transition and
                   already executing on our action plan.

                   As Steve moved into his new role, we needed a suc-        Brian L. Roberts
                   cessor who could run our cable operations with that       Chairman and Chief Executive Officer
                   same level of energy, intelligence, and competitive-      Comcast Corporation
                   ness. We found our answer in Neil Smit, who came          April 8, 2011


