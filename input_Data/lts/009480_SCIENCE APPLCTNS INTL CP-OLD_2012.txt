DEAR FELLOW STOCKHOLDERS,

As SAICs new CEO, I am proud to be part
of a company that enjoys a rich legacy
of solid performance and achievement.
During my career in the Air Force, I had
the opportunity to see SAIC, its products
and the exceptional work of its people
firsthand as a customer in the field. My
five years experience on the SAIC Board
of Directors only increased my admiration
for the company and its broader portfolio
of technical solutions. Especially in the
years since 9/11, the superb products
and services produced by SAICs scientific
minds were turned into quick reaction
capabilities, and in many cases, have been
publicly credited by the nations highest
military leaders as critical solutions to
daunting problems.
I am honored to now head this outstanding
leadership team as we begin a new era
focusing on our comprehensive growth
strategy and targeting our key areas of
expertise and expansion into adjacent
markets. Additionally, I couldnt be more
pleased to be serving with Stu Shea in
his new capacity as our chief operating
officer. Stu, along with Defense Solutions
Group (DSG) acting Group President
Tom Baybrook, Health, Energy and Civil
Solutions (HECS) Group President Joe

Craver, and Intelligence, Surveillance and
Reconnaissance (ISR) Group President
Tony Moraco, have not only my confidence
but also the full support of our Board
of Directors.
Our company remains financially strong
and well positioned for future growth. We
are operating in a challenging business
environment, but remain constant in our
focus to build an even better company. Our
performance and our reputation are built
on strong program execution, an ethical
culture, and delivering value and enduring
results to our stockholders, our customers,
our people, and the communities in which
we work. As we reflect on fiscal year 2012
(FY12) and look ahead, excellence and
integrity will continue to guide us in all
we do.
On March 14, 2012, SAIC reached a
settlement with the U.S. Attorney for the
Southern District of New York and the
City of New York concerning the CityTime
automated workforce management
system. SAIC agreed to a settlement of
approximately $540 million and to have an
independent monitor for the company for
three years.
This settlement was an important step in
our efforts to move forward as a stronger
company. We have implemented process
improvements and control enhancements
and remain dedicated to the highest level
of business ethics.
Solid Financial Performance and
Stockholder Dividend
Excluding the non-recurring CityTime
settlement amount recorded in FY12,
SAIC delivered solid underlying financial
performance in FY12. Excluding CityTime,
revenues totaled approximately $11
billion* for the year. We continued to
generate strong cash flow from continuing
operations with a record $772 million for
FY12. We entered fiscal year 2013 with
more than $1.5 billion of cash, a record
amount of nearly $32 billion in outstanding
proposals awaiting decision, and a robust
new business pipeline.
On March 20, 2012, we announced the
initiation of a quarterly dividend at $0.12
per share. The first dividend was paid on
April 30, 2012, for stockholders of record
as of April 16, 2012. This dividend reflects
our confidence in SAICs financial strength


and our commitment to deploying capital
to maximize stockholder value for the long
term. The dividend will augment our capital
deployment strategy, which will continue
to include strategic acquisitions and share
repurchases while retaining an attractive
credit rating profile.
Implementing Our Strategy
We continue to believe there will be areas
of growth in intelligence, surveillance and
reconnaissance; cybersecurity; logistics,
readiness and sustainment; health; and
energy. We will continue to invest in these
areas to differentiate ourselves and deliver
the solutions our customers require. In
the commercial market, we are seeing
strong demand in the areas of health,
cybersecurity and energy.
SAICs strategic advantage is our ability to
rapidly develop and deploy scientific and
technical solutions, and we are focused
on leveraging those innovative solutions
across and beyond our customer base.
Our strategic growth will be driven by
leveraging these differentiating capabilities
into adjacent markets by ensuring we have
sufficient credibility, mass, and momentum
in these areas.
New Business Highlights
Fiscal year 2012 was a record year for
our larger wins, with 40 contract awards
having a total expected revenue value
of $100 million or greater, a 54 percent
increase over the previous year. Many of
these large wins were a result of increased
collaboration across the enterprise 
leveraging expertise from different parts
of the company to offer differentiated
solutions  and this is a process that we
will continue to emphasize going forward.
Highlights of our FY12 business
development achievements include:
 A prime contract awarded by the
Defense Logistics Agency to provide
supply chain management, including
forecasting, procurement, inventory
management, and worldwide distribution
of military aircraft and ground tires
directly to U.S. Armed Services and
Foreign Military Sales customers.
The single-award indefinite-delivery/
indefinite-quantity (ID/IQ) contract has
a five-year base period of performance,
one two-year option, and an estimated
value of $1 billion if the option
is exercised.
 The NASA Integrated Communications
Services contract with a base award
value of $340 million over a threeyear
base period of performance and
a maximum potential value of $1.3
billion over 10 years, if all options are
exercised. SAIC supports corporate
and mission communications
needs, including local area network
management at all NASA centers.
 A two-year, $216 million contract to
provide engineering, procurement, and
construction services for the Plainfield
Renewable Energy biomass project in
Connecticut. The project is expected
to generate clean energy to power the
equivalent of 37,000 homes. SAIC
and the Carlyle Energy Mezzanine
Opportunities Group are providing
financing for the project.
 A contract from the National Institute
of Allergy and Infectious Diseases to
provide preclinical services for the
development of biopharmaceutical
products for infectious diseases. The
total funding could be up to $102
million over the 10-year period of
performance, subject to the availability
of annual appropriations.

 A task order to deliver mission support
IT services to the U.S. Department
of Agricultures Risk Management
Agency with a one-year base period of
performance, four one-year options, and
a total value of $208 million if all options
are exercised.
 Airborne Intelligence, Surveillance and
Reconnaissance contract wins totaling
$606 million to provide near-real-time
multi-intelligence capabilities that
observe and report activities associated
with insurgents, protect our troops
from improvised explosive devices, and
provide high-resolution geospatial data
in support of the warfighter.
 A multiple-award ID/IQ contract by the
U.S. Space and Naval Warfare Systems
Center Pacific to provide science, system
architecture and engineering services in
support of cyberspace operations. The
contract has a two-year base period of
performance, three one-year options,
and a ceiling value of $219 million for all
awardees if all options are exercised.
 A multiple-award ID/IQ contract from
the U.S. Department of Veterans Affairs
(VA) to provide IT services in support of
the Transformation Twenty-One Total
Technology program. This program
enables the VA to transform its IT
infrastructure, resulting in improved
quality of healthcare and benefits
services to veterans, their families
and survivors. The contract has a
five-year base period of performance
and a ceiling value of $12 billion for
all awardees.
We continued to grow our pipeline of
opportunities, submitted proposals, and
backlog during this FY. We also achieved
a 63 percent total dollar win rate on
business opportunities pursued and
awarded. Our high win rate is the result of
a solid track record of program execution
as well as targeted investments in
business development.
Strategic Acquisitions and
Divestitures
In FY12 SAIC made two key acquisitions
to enhance our competitive advantages
in growth markets. Vitalize Consulting
Solutions, Inc., a leading provider of clinical
business and IT services for healthcare
enterprises, expands SAICs health
solutions portfolio  in both federal and
commercial markets  to help customers
better address electronic health record
implementation. The combination of
Vitalizes expertise and integrating
commercial off-the-shelf software for
electronic health records and systems
with SAICs information integration, data
analytics and cybersecurity capabilities
creates a powerful combination in
the marketplace.
We also acquired Patrick Energy
Services, a provider of performancebased
transmission and distribution
power system solutions. The acquisition
strengthens SAICs energy and smart grid
services portfolio and helps the company
to better serve key customers including
investor-owned utilities, public power
providers, and transmission operators.
On the divestiture side, we sold operations
that primarily focused on providing
specialized IT services to international
oil and gas companies. The sale of these
operations better positions us to focus
on our strategic growth areas, including
market segments in the energy sector,
such as smart grid, renewable energy
implementation, and energy efficiency,
where we have successfully built our
business through both organic growth
and acquisitions.
Commitment to Corporate
Responsibility
We take pride in our business
achievements, but we also know success
is not measured in dollars alone. It is
evidenced by our ability to tackle our
customers problems in innovative ways
and to give back to our communities.
The community investments we make in
organizations that serve the military and

veterans community is one way to show
our appreciation and salute the millions
of men and women in uniform as well as
those who have served. We are a strong
supporter of the Paralyzed Veterans of
America, and our employees serve on the
boards of many organizations that make a
difference to the lives of our servicemen
and servicewomen, including the Special
Operations Warrior Foundation, Tragedy
Assistance Program for Survivors, and
several USO chapters. And this year we
continued to enhance our focus on helping
veterans build meaningful careers after
serving our country. Today, nearly 25
percent of SAICs workforce comprises
military veterans  approximately
10,000 people.
Our future success  and our nations
technological advantage  depend on a
constant supply of highly trained technical
talent. We continue to support a number
of exciting initiatives that inspire students
to get involved in science, technology,
engineering and mathematics (STEM)
careers. In FY12 we provided financial
support to leading STEM organizations,
and more SAIC volunteers than ever before
worked with student teams in robotics
competitions and other events around
the country.
This year we reinforced our commitment
to sustainable business practices by
announcing a goal to reduce operational
greenhouse gas emissions 25 percent
(from 2010 levels) by 2020. SAIC will
utilize in-house engineers who specialize
in tackling energy challenges to help
meet the new goal, and to identify and
implement additional cost-effective energy
efficiency projects across the enterprise.
A Strong Heritage  A Bright
Future
I am proud of our accomplishments and
our efforts to build an even stronger
company. We have not let the challenges
of the past year take us off course, and
we remain steadfast in our pursuit of our
strategic goals and vision. We will continue
to be proactive in meeting our customers
rapidly changing needs, make the tough
decisions required by the marketplace, and
invest prudently in our future.
As we stand on the threshold of another
year of strategic opportunities, one thing is
certain  our 41,000 employees will rise to
the challenges ahead with the same level
of commitment to ethics and integrity that
have been at the forefront of SAICs culture
since its founding in 1969.
Finally, Id like to thank Walt Havenstein,
who retired in February 2012 as CEO and
a member of our Board of Directors. His
work and dedication helped the company
navigate a rapidly changing business
environment and provided us with a solid
footing on which to build in the years
to come.

John P. Jumper
President and Chief Executive Officer