Letter to Shareholders
DEAR FELLOW SHAREHOLDERS For MeadWestvaco, as for
our entire industry, 2oo3 was an extremely challenging year. During a period
of continued market weakness, operating difficulties and abnormally high
fuel and wood costs, our company responded with energy and discipline,
managing through these challenges while setting the stage for increased
profitability as the markets improve.
We are intently focused on rewarding our shareholders with
profitable growth and a minimum annual return on capital of 1o percent.
In 2oo3, we continued to deliver on our commitment to sell nonstrategic
forestland, using the proceeds to reduce our debt, even in these challenging
markets.We remained prudent in capital spending, took modest steps to grow our business and paid a strong
dividend to shareholders. By realizing annual cost savings of more than $4oo million, we have substantially increased
our operating leveragewith additional savings to come.MeadWestvaco continues to pursue aggressive strategies,
and we are gaining momentum. These strategies, combined with our rigorous financial discipline, will ensure that
you receive the benefit of our success in the years ahead.
This report offers a look inside MeadWestvaco, illustrating how we are building a company that is more
than the sum of its parts.MeadWestvaco is a company in transformation. Packaging is a growing portion of our
business, with more than half of our sales and profits in this segment.We have three other important businesses
coated and specialty papers, consumer and office products, and a very attractive and profitable specialty chemicals
business. Our company provides value-based solutions and products through a business model that is focused
on productivity, innovation and customers.We are determined to achieve market-leading positions in each of our
businesses, delivering growth, earnings and cash flow.
PRODUCTIVITY Improving productivity is the first imperative for profitable growth. It unlocks capital and other
resources we can then redeploy to generate higher profits. The $4oo-million reduction in the cost of running our company
each year far exceeded our original targets for our first two years. By consolidating operations and information platforms,
closing or divesting our less productive assets and increasing efficiency throughout the company, we have built a more
efficient platform to drive financial performance for the future.
Our paper business clearly demonstrates the benefits of improving productivity. During the past year, as markets
have been soft, we were relentless about removing costs and taking downtime in our mills to match supply with demand.
This substantially improved the performance of this business, even in the face of historically low prices and fierce import
competition, particularly from Asia and Europe. And we see room for further improvement.
In December, we announced a productivity initiative that will increase earnings and cash flow by an additional
$5oo million over the next two years. This new initiative is well underway.Throughout the company we are streamlining
operations, promoting teamwork and idea-sharing, and enhancing the value of our products and services.
INNOVATION AND CUSTOMER FOCUS Continuous innovation and customer focus are equally important
drivers of profitable growth. Our packaging business is increasingly one of devising and delivering solutions for our

customers complex needs.Many of the worlds best-known consumer products companies depend on MeadWestvaco
packaging to make their products stand out on the shelf and to build their brands globally.
Our proprietary DVD packaging, the leader in a global market where demand continues to grow, is a case
in point.Most of the worlds leading entertainment companies rely upon MeadWestvaco packaging and graphic design
to enhance the appeal of their DVD releases while also protecting them.We are changing the face of U.S. packaging
for soft drinks and other beverage products through our innovative FridgeMaster carton, which adds convenience
and appeal on the store shelf and inside the refrigerator. As a result, it is strengthening our leadership in packaging
systems used in beverage plants and breweries around the world.
Our expertise in packaging innovation and design, combined with our knowledge of the global consumer
products markets, allows us to participate with customers throughout their packaging decision process. Our knowledge
in regulatory compliance and insight into consumer needs are also driving our success in the high-growth and highly
demanding business of pharmaceutical packaging.
Innovation and customer focus are equally essential in our consumer and office products segment, where we
manufacture and distribute many of the most recognized brands in home, school and office stationery supplies, plus
a full range of envelope products. Our leadership in this business continues to grow, based on the quality of our products
and services, strong product innovation and marketing. During 2oo3, we strengthened our competitive position and
performance through two focused acquisitionsAMCAL, which makes decorative calendars and accessories for home
and office, and Day Runner,whose wire-bound personal planners complement our other time management products.
Our specialty chemicals business also had strong performance, driven by the continuing success of our
activated carbon products that control truck and auto emissions and by our proprietary asphalt emulsifiers for road
construction and repairs.
STEWARDSHIP AND SAFETY We remain dedicated to continuous improvement in our already excellent
employee safety programs, with the goal of ensuring that all employees leave work each day healthy and free from
injury.We are equally proud of our strong record of environmental stewardship and sustainable forest management,
and we remain committed to being leaders and champions of environmental protection. Our sound, longstanding
governance practices are another important source of strength, as we pursue our strategies for profitable growth.
SUMMARY We are encouraged by the early signs of economic recovery and confident that the steps we are
taking in productivity, innovation and customer focus will set us apart from others in our industry. Our progress is
made possible by the men and women of MeadWestvaco, whose determination and commitment are helping build
the uniquely powerful enterprise we are becomingone that creates value for our customers, opportunity for our
employees and superior returns for our shareholders.
february 27, 2oo4
Sincerely,
John A. Luke, Jr.
chairman and chief executive officer
