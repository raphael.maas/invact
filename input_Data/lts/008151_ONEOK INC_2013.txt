Letter to Shareholders
ONE COMPANY BECOMES TWO
Welcome to the new ONEOK.
Earlier this year, a new ONEOK was created when we separated
our natural gas distribution business into a separate, stand-alone
publicly traded company called ONE Gas, Inc. By having two
separate companies, each will have a much greater focus on its
individual strategy, financial strength and growth potential.

The separation of the natural gas distribution business
from ONEOK to create ONE Gas is consistent with our
commitment to create long-term, sustainable shareholder
value and is the result of a shift in business mix that has
occurred at ONEOK as it has grown over the years. (See
page 6 of this report for more information on the separation
of ONEOKs natural gas distribution business into a
separate, publicly traded company.)
For more than a century, ONEOK has provided safe,
reliable natural gas service to our residential, industrial
and commercial customers. Our roots can be traced back
to 1906, when we built an intrastate natural gas pipeline
from Tulsa to Oklahoma City  a pipeline that remains a
part of our ONEOK Partners segment today. Throughout
most of our history, we were primarily a natural gas utility in
Oklahoma, with a modest amount of natural gas gathering
and processing assets. Over the years, we grew our natural
gas distribution business and expanded our operations into
Kansas and Texas, with the acquisitions of Kansas Gas
Service in 1997 and Texas Gas Service in 2003.
Transforming the Company: Purchasing
Natural Gas Liquids Assets and Managing
a Master Limited Partnership
A series of transactions from 2004 to 2006 transformed our
company and positioned us for future growth. In 2004, we
purchased the majority general partner interest in Northern
Border Partners. We then entered the natural gas liquids
business in 2005 by purchasing natural gas liquids (NGL)
assets in the largest acquisition in our companys history.
And in 2006, we became the sole general partner of
Northern Border Partners and renamed it ONEOK Partners.
As one of the nations largest master limited partnerships,
ONEOK Partners became our growth engine  and grow it
did. From 2006 to 2009, the partnership executed a more
than $2 billion capital-growth program that significantly
grew its natural gas and natural gas liquids businesses.
Today, ONEOK Partners is in the midst of a more than
$6 billion capital-growth program, building additional natural
gas- and NGL-related projects.
ONEOK benefited and continues to benefit greatly from
growth at ONEOK Partners. Through our ownership position
in the partnership, we receive cash distributions, which grew
to $536 million in 2013 from $145 million in 2006, when we
became the sole general partner of ONEOK Partners. These
growing distributions have enabled us to increase our annual
dividend paid to ONEOK shareholders by 143 percent  to
$1.48 per share in 2013 from 61 cents per share in 2006,
when we became sole general partner of ONEOK Partners.
We anticipate increasing the dividend by 53 percent in 2014,
compared with 2013, and by an annual average of 20 to
25 percent between 2013 and 2016.
Increased earnings and growth at ONEOK Partners changed
the earnings makeup of our company, leading us to ask
what kind of company ONEOK is. The natural gas distribution
business provided stable earnings, while ONEOK Partners
offered tremendous growth opportunities and attractive
returns on investment for future growth in its midstream
businesses.
One of the challenges that our natural gas distribution
business faced was competing with ONEOK Partners for
capital dollars to fund its growth. Resolving this internal
allocation of capital through the separation gives us the
opportunity to maximize ONEOKs dividends to shareholders
and allows ONE Gas to sustain its lower-risk, stable
performance by investing in and growing its rate base.
Creating a New ONEOK
The decision to separate our company did not come easily,
and it was not made hastily. Previous company leaders,
including Larry Brummett, who served as chairman and chief
executive officer of ONEOK from 1994 through his death in

2000, and David Kyle, ONEOK chairman, president and chief
executive officer from 2000 to 2007, recognized that in order
for ONEOK to grow, it would need to become more than a
natural gas distribution company. As ONEOK Partners grew
and became an even bigger part of ONEOK in recent years, the
need to separate became more evident.
The value of the growth at ONEOK Partners and the steady,
increased earnings in our natural gas distribution business were
not reflected consistently in our stock price, which we believed
was a result of investors wanting to have the ability to invest
separately in either business.
On July 25, 2013, the day we announced our plan to separate
the natural gas distribution segment, ONEOK common stock
increased 25 percent. This increase in stock price was a
confirmation that the separation increased shareholder value and
an indication of approval from the investment community and
the markets. As the separation neared, ONEOKs stock price
continued to increase steadily throughout 2013 and early 2014 
closing at an all-time high of $68.49 on January 31, 2014, its last
day of trading before the separation. On February 3, 2014, the
first day of trading for the separated companies, ONEOK began
trading at $60.71, and ONE Gas began trading at $33.30.
We further changed our structure in the first quarter 2014 with
the substantial completion of our accelerated wind down of our
energy services segment. In recent years, increased natural
gas supply, coupled with lower natural gas price volatility and
narrower seasonal and location price differentials, limited this
segments opportunities to generate revenues to cover its fixed
costs on contracted storage and transportation capacity. Our
decision in mid-2013 to wind down the energy services
segment was based on our conclusion that it no longer fit our
business strategy.
Today, ONEOK consists of the sole general partner and limited
partner interests in ONEOK Partners, which as of December 31,
2013, represented a 41.2 percent ownership of the partnership.
2013 Financial Performance
2013 net income attributable to ONEOK was $266.5 million,
or $1.27 per diluted share. These results include a noncash,
after-tax charge of $86.7 million, or 42 cents per diluted share,
associated with the energy services segment wind down; a
noncash, after-tax charge of approximately $10 million, or 5 cents
per diluted share, and after-tax expenses of approximately
$9 million, or 4 cents per diluted share, both related to the
ONE Gas separation. 2012 net income attributable to ONEOK
was $360.6 million, or $1.71 per diluted share.

During 2013, we benefited from higher natural gas volumes
gathered, processed and sold, and higher NGL volumes
gathered, as a result of completed capital-growth projects
in the ONEOK Partners segment, and new rates and higher
sales volumes due to colder than normal weather in the
natural gas distribution segment. These increases were
more than offset by significantly narrower NGL location price
differentials, the impact of ethane rejection and lower realized
natural gas and NGL product prices in the ONEOK Partners
segment, and lower results in the energy services segment.
ONEOK Partners Growth
ONEOK Partners is in the midst of a more than $6 billion
capital-growth program in its natural gas and natural gas
liquids businesses, including $1.2 billion of new projects
and acquisitions announced in 2013.
2013 was the third year of the partnerships latest capitalgrowth
program that will grow natural gas and NGL volumes
on its systems and provide added flexibility to its operations.
In 2013 and early 2014, completed capital-growth projects
and acquisitions included:
 Completing the Stateline II natural gas processing
plant in North Dakota;
 Completing the first-ever NGL pipeline  the Bakken
NGL Pipeline  to transport unfractionated NGLs out
of the Williston Basin;
 Acquiring the Sage Creek natural gas processing plant
and related natural gas and NGL assets in the NGLrich
Niobrara Shale formation in Wyomings Powder
River Basin;
 Completing the MB-2 NGL fractionator at Mont Belvieu,
Texas;
 Completing the Sterling III Pipeline, our fourth NGL
pipeline connecting the Mid-Continent and Gulf Coast
NGL market centers;
 Completing the Canadian Valley natural gas processing
plant in western Oklahoma; and
 Completing a significant portion of a 270-mile natural
gas gathering system and related infrastructure in
Divide County, North Dakota.
We anticipate that the completion of projects from this
growth program, coupled with the successful separation
from ONE Gas, will allow us to increase the dividend by an
annual average of 20 to 25 percent between 2013 and 2016,
subject to board approval.
The partnership also has a $2 billion to $3 billion-plus backlog of
unannounced growth projects that it continues to evaluate. (See
page 16 of this report for more information on ONEOK Partners
growth projects.)
Culture and Employees
ONEOKs culture, cultivated through the hard work and
dedication of our employees through the years, is something that
we have become increasingly proud of. ONEOK and ONE Gas
exist today because employees, both past and present, worked
diligently to improve our businesses and create a culture that
fostered success.
The fact that ONEOK and ONE Gas are so strongly positioned
for future success and growth is a testament to the hard work
of our employees. This culture was demonstrated following the
separation announcement last summer. To ensure the timely
completion of the separation plan approved by the ONEOK
board of directors, employees throughout the company worked
tirelessly and seamlessly to handle all of the details that had
to be done  no small feat given the work involved and
aggressive timeline.
Their work to complete the separation in a timely manner
ensured that shareholders of both companies would realize
the value created by ONEOK becoming two, highly focused
energy companies.
ONEOK changed in 2013  as we prepared to create a
new ONEOK and separate our natural gas distribution business

into ONE Gas. The separation and decision to wind down
energy services will create sustainable, long-term value for our
shareholders. These changes were the culmination of years of
growth and transition, and we believe they will result in increased
value to you, our shareholders.
We thank our employees for their efforts to make the separation
possible, and we thank you for your continued trust and
investment in ONEOK.

John W. Gibson
Chairman
Terry K. Spencer
President and Chief Executive Officer
March 7, 2014