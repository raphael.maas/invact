CHAIRMANS
MESSAGE

Strategic Priorities
Employ a high-performing and
engaged workforce 
Deliver a differentiated customer experience
Serve customers as their primary
banking partner
Embed effective risk management
Deliver financial results consistent
with a top-performing regional bank

Citizens Financial Group entered 2012 with good
momentum. Our team of more than 19,000 colleagues
continues to work hard to help our customers achieve
their goals, manage risk, give back to the community
and adapt to the ever-changing economic landscape.
I thank them for their tremendous efforts and their
commitment to helping us build a top-performing
regional bank.
Our financial performance reflected both the
strengths of our company and the challenges we
face in this unprecedented environment.
We entered the year focused on improving profitability
by growing core customer relationships, enhancing
credit quality, strengthening our balance sheet and
managing expenses. We achieved all of those goals.
F in a ncia l R e s u l t s
In 2011 we delivered net income of $506 million, our
strongest performance in four years. We also ended
the year with strong liquidity and a Tier 1 capital
ratio of 13.85 percent. This is an important measure
of financial strength, and CFG remained at the top of
its peer group at December 31, 2011.
We made progress in becoming our customers
primary bank  their bank of choice. This approach
helped us grow commercial loans and leasing, which
increased 8 percent to $28.9 billion when compared
to the previous year. It also helped us grow consumer
and small business checking balances.
We continued to embed effective risk management.
Loan quality was strong, which allowed us to lower
what we set aside for loan losses. This was a major
contributor to our increase in net income.
By remaining self funded  with a loan-to-deposit
ratio of 93 percent  and managing interest costs,
we improved our net interest margin to 2.97 percent
from 2.78 percent in 2010. We also continued to
focus on managing costs.

At the same time, generating revenue became
more difficult. The economy picked up slightly, but
prospects for long-term growth remain uncertain.
Low interest rates helped push down net interest
income, and new regulations resulted in lower
noninterest income.
We are pleased with our overall financial strength
and stability. Our progress has been recognized by
Standard & Poors, which increased the debt rating
for our two primary chartered bank affiliates, Citizens
Bank of Pennsylvania and RBS Citizens, N.A.
Our goal is to build on this momentum.

O ur pl a n to w in
The best thing we can do in this or any environment
is to focus on what we can control. Our efforts are
centered on the five strategic priorities that shape
our back-to-basics strategy.
Employ a high-performing and engaged workforce
Our more than 19,000 colleagues drive our success.
They take care of our customers and do extraordinary
work in our communities. We have made investments
to ensure that our colleagues remain well-prepared
for their roles, work as one unified team and receive
recognition for their achievements.
Colleagues participated in hundreds of thousands of
hours of training in 2011 that helped them develop
personally and professionally. And through a major
investment in desktop technology, we are providing
more colleagues with the ability to be as productive
in remote locations as they are at a CFG workstation.
Deliver a differentiated customer experience
Our colleagues and CFG compete in a highly
commoditized industry. Our core competitors offer
a similar range of products and services. To attract,
grow and retain customer relationships, we must stand
out from our peers. We are taking steps to do just that.
We know that good banking is personal. We
prepare colleagues to deliver a consistent, highquality experience with every customer interaction.
In Commercial Banking, enhanced training and
customer-focused solutions helped us earn high
ratings from middle market firms surveyed. CFG
finished first in its peer group in categories such
as likelihood to recommend, proactively provides
advice and solutions, and values long-term
relationship in a recent report published by
a leading research and consulting firm. 

We also have organized our Commercial Banking
business under one brand  RBS Citizens. This
better represents the breadth of the solutions we offer
and the team-oriented approach we take to meeting
our customers needs.
In Consumer Banking, we enhanced our distribution
platform. To build out our online and mobile
offerings, we added a new iPhone app, and we will
launch an Android app this year. Customers used
our online platform more than ever before in 2011,
interacting with us more than 200 million times.
To help meet our customers evolving and wideranging needs, we have also broadened our
education lending and wealth management services.
Serve customers as their primary banking partner
We believe we are at our best when clients look to
CFG as their bank of choice. This provides us with
ongoing opportunities to help our customers achieve
their financial goals.
Last year, for example, our Commercial Banking
business invested in and developed a number of
specialty channels. We now provide expertise in
healthcare, technology, not-for-profits, franchise
finance and sponsor finance. And we continue to
help customers meet their financial goals by offering
a range of credit solutions. CFG finished 2011 as one
of the top 10 providers of middle market loans in the
United States.
In Consumer Banking, we added YourPlace
Banking. This smart, no-cost feature brings banking
solutions right to where our customers work. It
provides them with the added ease and convenience
of banking with us when and where they want. 

Embed effective risk management
Our focus on taking care of our customers is
matched closely by our effort to manage risks.
We aim to grow our loan portfolio and asset base
while staying true to the credit risk framework we
have implemented. We made good progress on this
front in 2011. We lowered our provision for credit
losses, and our percentage of nonperforming loans
to total loans was among the best in our peer group.
Strong risk management practices will be a defining
characteristic of banks that deliver sustainable,
long-term growth. We are well positioned to be in
that category. 

Deliver financial results consistent with a top-performing
regional bank
The great thing about our plan is that if we deliver on
the first four priorities, this one will take care of itself.
We remain focused on growing revenue, maintaining
a strong balance sheet and effectively managing capital.
We believe our focus on primary customer growth
will help us increase earning assets and low-cost
deposits. That strategy, combined with effective risk
management, will help us maintain strong asset
quality. Our collective efforts are designed to fuel
stronger returns as well.
We will continue to compare ourselves to the best in
our peer group to measure our success.
R e g u l a t o r y Envir o n m ent
The new regulatory environment will continue to
impact how we operate. Some new rules being
implemented as part of the Dodd-Frank Wall Street
Reform and Consumer Protection Act will result in
lower revenue and higher compliance costs.
Our leadership team is taking a number of steps to
manage through this change. We have intensified our
efforts to control expenses across the organization and
identify areas to generate revenue growth.
Just as important, we are implementing a number
of initiatives that will help us not only comply with
these new rules but enhance the customer experience
as well. For example, customer complaints are now
logged centrally and that information is used to
sharpen processes.
We partner closely with our regulators on these
efforts as we aim to meet both the letter and spirit
of these new rules.

M a kin g a d iff e r e nce in
t h e c o mm u ni t y
Much has changed in the world of banking since
we were founded on High Street in Providence 185
years ago. But one thing has remained constant 
our colleagues care deeply about where they live
and work. Last year alone they contributed more
than 32,000 hours to charitable organizations in the
communities we serve.
Our company walks in concert with colleagues in
these efforts. CFG contributed $16 million to more
than 1,200 nonprofit organizations in 2011. Under
the banner of Citizens Helping Citizens, we focus
on activities that will help strengthen the fabric of 
our communities: Providing affordable housing.
Ending hunger. Improving education. These things
matter to those we serve. They matter to us.
We also work to improve the economy. CFG more
than doubled its Small Business Administrationrelated lending last year. These loans help fuel the
lifeblood of U.S. commerce.
Our efforts to support the community and fuel
economic growth reflect the essence of our brand 
Good Banking is Good Citizenship.
All of the elements of our strategy are designed to
work together. Engaged and satisfied colleagues
provide customers with a unique experience.
Those customers do more and more business with
us. We manage the associated risks. And we grow,
which helps us deliver value for our stakeholders
and provides us with opportunities to help our
customers, colleagues and communities prosper.
That is our goal. That is why we come to work every
day. That is why we navigate through the inevitable
challenges.
On behalf of everyone at CFG, thank you for the
privilege and opportunity to help you achieve your
financial objectives.

Sincerely,
Ellen Alemany
Chairman and Chief Executive Officer
Citizens Financial Group