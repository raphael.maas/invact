Our annual report provides an opportunity to reflect on 
the challenges 
and accomplishments of 2006, while looking forward to the exciting 
days ahead.
Overall, 2006 was a challenging year for us but in many ways also a 
successful year. We remained the worlds most profitable vacation 
company, achieving revenues approaching $12 billion and record net 
income of $2.3 billion or $2.77 per share. Our dedicated employees 
delivered memorable vacation experiences to more than 7 million 
guests worldwide, and we continued to build a base of loyal repeat 
guests across our portfolio of global brands.
Revenues grew 6.7 percent during 2006, driven by a 4.6 percent 
increase in capacity with the introduction of three new shipsHolland 
Americas Noordam, Princess Cruises Crown Princess and Costa Cruises
Costa Concordiaand a rise in revenue yields of 1.5 percent from a 
combination of higher ticket prices and onboard guest spending.
We benefited from strong consumer demand in Europe in 2006. 
Our European brands posted record revenues and earnings as we 
pursued our global strategy. Our North American brands maintained 
strong pricing for their European and Alaskan departures but Caribbean 
cruise prices were weakened by a variety of factors, including hurricane 
and economic. 
While the 2006 hurricane season fortunately did not fulfill well-
publicized storm predictions, many consumers still were reluctant to 
book a Caribbean cruise, and we priced our Caribbean product aggres-
sively to stimulate demand. 
We continue to explore destination development opportunities to 
enhance the Caribbeans appeal. Among strategic Caribbean initiatives 
recently implemented are the opening of the $42 million Grand Turk 
Cruise Center in the Turks & Caicos Islands and the addition of new 
facilities and activities at our private Bahamian island Half Moon Cay. 
We also have begun the rebuilding process at Puerta Maya, our port 
in Cozumel that was destroyed by Hurricane Wilma in 2005.
An Eye on the Bottom Line 
Maintaining disciplined control over manageable costs is ingrained in 
our corporate culture. While other operating costs on a unit basis were 
flat this year, record fuel prices increased operating expenses by $210 
million and reduced earnings by $0.25 per share. 
While a 30 percent increase in the cost of fuel is difficult to 
overcome, our fuel conservation working group launched dozens of 
initiatives aimed at reducing consumption. Those efforts reduced fuel 
consumption on a unit basis by 2 percent fleetwide, thus providing 
significant benefits to our bottom line. 
We continue to return value to shareholders through our stock 
repurchase program and increased dividends. In 2006, we repurchased 
approximately 20 million shares, for a total repurchase since program 
inception of approximately 27 million shares at a cost of $1.2 billion. 
We also increased our annual dividend 10 percent to $1.10 per share, 
which represents a cumulative increase in dividends of more than 80 
percent over the past two years.
Strategic Growth Initiatives
Our growth strategy has been to establish a presence in a geographic 
region and then add capacity to stimulate cruise growth in the region. 
As the region develops, we may further segment it, using multiple 
brands targeting different demographic groups to achieve maximum 
penetration. This strategy has worked well in the United States, the UK 
and Continental Europe. 
Shipbuilding is an essential part of our brand-building efforts, and in 
2006, we ordered seven new ships, bringing our order book to 20 new 
vessels valued at $11 billion.
We are building four ships for contemporary brand Carnival Cruise 
Lines, which attracts the broadest demographic offering fun affordable 
vacations.  Three  vessels  have  been  ordered  for  premium  brands 
Princess and Holland America, which are well positioned to capture 
affluent baby boomers as they seek longer, more exotic vacation 
experiences. 
And during 2006, we reached a decision to expand the ultra-luxury 
segment of the North American cruise business by ordering two 
450-passenger all-suite ships for Seabourn Cruise Linethe first ships 
built for that brand in 15 years. While Seabourn has operated small, 
200-passenger  all-suite  ships,  the  larger  vessels  will  offer  better 
economies of scale and facilities and amenities aimed at Seabourns 
discriminating gueststhe wealthiest segment of the population. 
The decision to more than double Seabourns capacity demonstrates 
our commitment to the luxury cruise segment and our confidence 
in the growing consumer interest in Seabourns unique yacht-like 
cruise experience.
Globalization Continues
In 2006, we continued to extend the global reach of our company 
through expansion in Europe and the launch of a cruise venture 
in China. 
The United Kingdom, Italy and Germany enjoy especially strong 
cruise demand, and Carnival operates the leading cruise brands in the 
UK and Continental Europe. We believe Europe has tremendous 
potential for growth and have ordered 11 ships for our European brands 
which are scheduled for delivery over the next four years. 
Our Costa Cruises brand is the largest cruise line in Europe and 
serves a broad European clientele. Costa has four ships on order for 
delivery through 2010, representing a capacity increase of more than 
50 percent. 
P&O Cruises and Cunard, the two leading UK brands, have three 
ships on order. Building on the success of Queen Mary 2, Cunard will 
welcome Queen Victoria later this year. P&O Cruises, which serves 
traditional British cruise customers, also has two 3,000-passenger 
ships on order scheduled for delivery in 2008 and 2010.
Our AIDA brandGermanys most popular cruise linewill add 
four new ships over the next four years, beginning with the AIDAdiva 
which is scheduled for delivery in April 2007. We also recently 
announced the signing of a letter of intent to form a joint venture with 
TUI AG, Germanys largest tour operator. TUI carried more than 7.5 
million tour passengers last year and has a remarkable 95 percent brand 
awareness among German consumers. 
Through the TUI joint venture, we plan to develop a second brand 
aimed at a different demographic of the German customer. AIDA will 
continue its highly successful club cruises concept geared toward 
younger and more active cruisers while a new TUI-branded cruise 
ship is planned, which will target a slightly older and more affluent 
German traveler. 
Spain is another region primed for expansion and though our Costa 
Cruises brand already has a strong presence there, we believe huge 
potential for expansion exists as it is one of the fastest-growing areas 
of the European vacation industry. To capitalize on this, we signed a 
letter of intent to form a new joint venture with Orizonia Corporacin, 
Spains largest travel company which operates its own two-ship fleet 
under its Iberojet division. The new joint venture would eventually be 
comprised of a multi-ship fleet aimed at a different demographic than 
the premium market that Costa currently serves.
2006 marked our entry into China, where there is great potential. 
Long-standing government travel restrictions have been relaxed, giving 
more Chinese the ability to travel abroad. As Chinas economy matures 
and its middle class expands, the number of Chinese travelers is 
expected to grow and we intend to convert those tourists into cruisers. 
The Costa China initiative was launched in July 2006 with five-day 
cruises from Shanghai aboard Costa Allegra, which was refurbished to 
serve Chinese and Asian tastes. Although we anticipated challenges 
entering China, the program has developed more slowly than expected 
during 2006. We have made a number of modifications to the program 
for 2007, including changing itineraries and marketing the China cruises 
to other Asian countries and Europe. We believe these changes will 
result in improved performance for the China/Asia program in 2007. 
An Industry Leader 
While we are proud of our leading role in the cruise industry, we also 
strive  to  provide  leadership  on  environmental  issues  and  social 
responsibility. In 2006, all of our operating companies successfully 
achieved  implementation  and  certification  of  their  environmental 
management systems to the ISO 14001 standard. These certifications 
recognize  our  commitment  to  management  of  our  environmental 
processes,  products  and  services  in  our  effort  to  minimize  our 
environmental footprint. 
As the worlds largest cruise operator, we have a duty to be a 
responsible global citizen. As part of our ongoing commitment to 
corporate responsibility, the Health, Environmental, Safety and Security 
Committee of the board of directors was formed in 2006. This 
committee has specific responsibility for monitoring compliance with 
health, environmental, safety and security policies, as well as to review 
enterprise risk and assess mitigation measures in those areas of 
our business. 
Looking Forward to 2007
In 2007, we mark the 35th anniversary of Carnival Cruise Lines, the 
flagship brand of our company. It is remarkable to reflect on the 
dramatic changes in our company and in the cruise industry over three 
and a half decades. We have come a long way since my father, Ted 
Arison, created a new style of cruising with the launch of a refurbished 
trans-Atlantic ocean liner, the Mardi Gras, in March 1972. 
2007 also is the 20th year since our initial public offering. In 1987, 
when Carnival Cruise Lines went public, the company had seven ships 
and $150 million in income. Today, Carnival Corporation & plc is a global 
company with a portfolio of industry-leading brands that produce $2.3 
billion of earnings from its 81-ship fleet. Based on our current order 
book, we will have approximately 100 ships in service by 2011. 
I take this opportunity to thank our 75,000 employees worldwide 
for their hard work and dedication in creating unforgettable vacation 
experiences for our guests. I also want to thank our board of directors 
for their support during the past year. 
I would especially like to thank retiring board member Kirk Lanterman 
for his dedicated service over the past 17 years as chairman and CEO 
of Holland America Line. Kirk was one of the pioneers of Alaska cruising 
and his hard work was instrumental in making Holland America Line 
the leading premium cruise brand. 
I also would like to acknowledge the contributions of Deborah 
Natansohn, president of Seabourn Cruise Line, who died suddenly and 
unexpectedly late last year. Debbie was a very talented executive, and 
her leadership, creativity and humor will be missed.
Finally, we express our gratitude to our shareholders for their 
continued support. And, on behalf of our shareholders, shoreside 
staff and shipboard crew, we thank the 7 million-plus guests who 
chose to spend their vacations with us last year. We hope to see 
you again soon. 
Micky Arison
Chairman and CEO 
February 14, 2007