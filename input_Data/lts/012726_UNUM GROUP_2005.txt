TO OUR SHAREHOLDERS, CUSTOMERS AND COLLEAGUES
Thomas R. Watjen
President and Chief Executive Officer
Under almost every measure, we can look
backoverthe past year as a success, with the
company meeting most of its key objectives
and continuing to build on the momentum
we have established in recent years.
Our emphasis in 2005 was simple aEUR" focus on the basic
and consistently execute our plans. While we fell short of
our expectations in several key areas aEUR" which will be th
focus of our attention in 2006 aEUR" I am generally please
with the results in the four areas we targeted:
Restore profitable, sustainable growth
aEUR cncreased before-tax operating income for all segments,
despite the lower-than-expected results in our U.S.
Brokerage Group Income Protection line. This includes
record before-tax operating income in our U.K. and
Colonial businesses.
aEUR crew sales in our target markets, as U.S. Brokerage
sales to our core markets were up 12 percent over the
previous year and Colonial sales were up slightly over
2004 aEUR" the first positive sales comparison in two years
Maintain strong balance sheet and
capital position
aEUR ceduced leverage and improved liquidity and
cash flow.
aEUR cmproved combined risk-based capital position to
308 percent aEUR" our highest level on record
Leverage our size and scale in the marketplace
aEUR caintained leadership positions in all of our markets
throughout the U.S. and the U.K.
aEUR cchieved strong growth in voluntary benefits lines, an
area of great interest to the market and our customers.
Continue to focus on operational excellence
aEUR ceduced operating expenses and enhanced
employee productivity.
aEUR caunched a Continuous Improvement program, which
has increased employee involvement in quality and
productivity improvement activities.
Although we clearly have more to do in some areas,
UnumProvident is a much stronger and better-positioned
company today aEUR" better able to face the challenges an
capitalize on the opportunities that lie ahead. YouaEUR(T
notice that the word aEURoestructuringaEUR is used only thi
one time in my letter aEUR" that period is behind us, and th
focus is on our future and aggressively competing in a
very challenging market, as well as continuing to take
the steps needed to meet the rapidly changing needs
of our customers. I am confident that good things will
continue to emerge if we redirect the energy and sense
of urgency weaEUR(TM) exhibited over these last few years
addressing issues of the past into improving our operating
performance and leveraging the unique business
franchises we have at UnumProvident.
A Year of Progress
Looking back over the past year, we had generally
strong performance across most areas of the company.
Within our U.S. Brokerage operations, the majority of
our business lines met or exceeded expectations. While
our total sales for this segment fell short of our expectations,
we saw improvement in most of the areas
targeted for growth, including sales to small and midsized
employers and in our voluntary benefits products.
We also successfully executed our renewal and persistency
management plans and achieved our pricing and
profitability targets in most of our product lines.
Importantly, we continued to reduce expenses
and improve productivity, while at the same
time generating some of the highest customer
satisfaction ratings ever.
And finally, we removed substantial regulatory
uncertainty surrounding our claims practices through
a settlement agreement with the California Department
of Insurance. We have focused a great deal of resources
on meeting the requirements of this and the multistate
agreement reached in late 2004.
Despite this progress, our flagship U.S. Group Income
Protection business fell short of expectations, due
primarily to claims performance. This area of our company
has undergone a great deal of change over the
past several years, both in response to an evolving
market and to our regulatory settlements. That has
placed a great deal of pressure on this part of the
company aEUR" still, we should have been better prepared
and we are working to make up for lost time. IaEUR(TM)c
fident that the recent changes we have made in the
claims area will lead to gradual improvement in the
consistency of our decisions as we move through 2006.
Our subsidiaries had a solid year as well. Colonial Life
& Accident, one of the leading providers of voluntary
workplace benefits, produced record before-tax operating
income in 2005, and I am very pleased with the
consistently strong returns in this business. There are
also signs that the steps weaEUR(TM) taken to reposition t
sales organization are beginning to have a positive
impact on results. Sales were up slightly in 2005, the
first positive sales comparison in two years, with
momentum building as we moved through the year.
In the United Kingdom, where we are the leading Group
Income Protection provider and the second largest
Group Life carrier, our Unum Limited subsidiary also
achieved record earnings aEUR" surpassing A lb.0 million
before-tax operating income for the first time. Although
the sales environment remains challenging, we are well
positioned in the market. In 2005, we modified our
external reporting to separate our Unum Limited
results, a move which has been favorably received by
investors and our people in the U.K.
Lastly, GENEX, a leader in disability management and
medical cost containment services, performed well
despite operating in a very tough market.
I am also very pleased with the continued strengthening
in our financial position aEUR" an important priority of ours
The combined statutory capital and surplus for our U.S.
insurance subsidiaries reached a record $4.27 billion.
Additionally, our combined risk-based capital ratio, a
regulatory measure of capital adequacy, climbed above
300 percent, an almost 100-point improvement over
2002 and also a record for this company.
Maybe even more important than the financial progress,
because of its longer-term implications, is the progress
we have made in redefining this company, blending
elements of the past with the needs of the future.
The vision and values of the aEURoewaEUR UnumProviden
outlined in the front of this Annual Report, are an
example of what this company represents today.
Our goal is to be the leading provider of employee
benefit products and services that help employers
manage their businesses and employees protect their
families and livelihoods. The values, or principles by
which all of our businesses and people operate, have
as their foundation integrity, commitment and accountability.
Embodied in these values is a culture of giving
aEUR" giving to our industry, to organizations important t
us and to the communities in which we operate.
UnumProvident Today
Through the actions weaEUR(TM) taken over the past thr
years, we have significantly improved the operating and
financial platform of our company, while maintaining
our position as the largest income protection insurance
provider in the world, with leading positions in all of the
markets we serve. As we look toward the future, one of
our greatest assets is the goodwill weaEUR(TM) created wi
the 21 million people we insure through the $6 billion in
benefits we paid in 2005. Shareholder value is created
by continuing to serve these customers well, while also
leveraging this unique market position.
IaEUR(TM) said before that our focus these last few years w
on more than just confronting past issues, but also on
designing and implementing a business plan capable of
generating more consistent and predictable results. This
has and will continue to require changes to our operating
plans including better aligning our people with the
new direction we have set for the company. This backto-
basics approach aEUR" focusing on balancing growth an
profitability, maintaining world-class service levels, better
managing the risks associated with the business and
continually striving to improve aEUR" has already begun t
positively impact our results. In 2005, for instance, we
were a more balanced business with combined sales of
Group Life and Supplemental products within our U.S.
Brokerage operation surpassing those of Group Income
Protection products. In addition, the contribution of our
subsidiaries has increased, creating more balance among
our business units.
Although some things have changed, our commitment
to our customers and producers hasnaEUR(TM) In fa
our most recent market research indicates that we
have a clear competitive advantage in key areas such
as service, claims management and communications
with our customers.
More than eight out of ten claimants aEUR" those who tur
to us during their time of need aEUR" say they would reco
mend UnumProvident to others. None of this would have
been possible without our people, who I believe have
a renewed excitement about our future. While we will
always be striving to improve, we are pleased with how
our employees, customers and business partners aEUR" thos
who know the company best aEUR" view UnumProvident
Our people, through the relationships they have in
the marketplace and their ability to manage this
complex business, are our greatest resource.
WeaEUR(TM) continued to develop our people at all level
including our leadership team, where we have broadened
the responsibility of many aEURoeteransaEUR who have succes
fully delivered in the past, while also bringing in new
people where appropriate. In 2005, we added to our
leadership team Joe Zubretsky as senior executive
vice president of finance, investments and corporate
development, as well as Charles Glick as executive vice
president and general counsel, both of whom have
already made tremendous contributions. IaEUR(TM)confide
that our entire senior team, working with the more than
11,000 employees at our company, will lead us to
even greater success in the future.
Confidence in our company will grow as we deliver
sustainable improvement in our results. Though we are
not yet immune to setbacks, we are in a better position
today to address them and to continue making progress
toward our long-term business and financial objectives.
Looking Ahead
Like 2005, the focus for 2006 is simple aEUR" maintainin
momentum in those areas that met our expectations,
while giving greater attention to those areas that did
not. I would highlight four areas in particular:
aEUR continued improvement in operating results aEUR We ar
operating at acceptable levels of profitability in most
of our products and subsidiaries, with the noticeable
exception of the Group Income Protection line in our
U.S. Brokerage segment. IaEUR(TM)confident that the rece
actions we have taken, especially in our claims area,
will lead to gradual improvement and consistency in
our results in 2006.
aEUR consistent execution of our business plan aEUR Throug
the actions we have taken the past several years, we
now have simple, more clearly understood plans in
place that, if executed well, can lead to better results
as we move through 2006.
aEUR cmprovement of our financial ratings aEUR We believ
that with our significantly improved financial strength
and flexibility, we have satisfied many of the requirements
for a ratings upgrade. We recognize, though,
that we must deliver consistent results and, as we
do, we expect that to have a positive impact on
our ratings.
aEUR cnhancement of our corporate reputation aEUR Our
reputation with certain stakeholders has been
adversely impacted by the events of the past few
years. ThataEUR(TM)already changing as we have put
number of regulatory and other issues behind us,
delivered improved financial results and, most importantly,
continued to consistently deliver superior value
to our customers. We will be taking more aggressive
steps this year to assure that our stakeholders see
UnumProvident for what it is today aEUR" a company tha
has significantly changed over the past several years.
Looking ahead, given the progress we have made around
the company, there is reason for cautious optimism.
We are now better positioned to face the challenges
and seize the opportunities in front of us. Our people
feel engaged in our business, and IaEUR(TM)confident that
are focused on the actions needed to help us meet the
three-year financial goals we outlined at our investor
conference last year. These include:
aEUR couble-digit earnings growth, driven primarily by
improvement in our U.S. Brokerage Group Income
Protection line of business;
aEUR ceturn on equity in the range of 9 to 11 percent; and
aEUR ceneration of more than $1 billion of excess capital.
Although our stock performance has improved, we
recognize that future stock price appreciation will be
heavily driven by improvements in our return on equity.
This is a clear priority of management, and we will be
diligent in seeking ways to improve our returns, including
a greater focus on capital management in close consultation
with our rating agencies and regulators. Better
managing our capital is critic al to creating future shareholder
value.
I want to thank our board of directors for their support
during a very critical period of time for our company.
We have added six new directors since August 2004,
deepening the boardaEUR(TM)insurance, financial services a
regulatory expertise, all of which will be invaluable as we
seek to leverage our market position into greater value
for our shareholders.
IaEUR(TM)proud of what our people have accomplished the
past three years and can assure you that, to a person,
they understand what needs to be done for us to continue
to build momentum. While we face challenges,
some of which are not unique to our company, we have
the determination to succeed aEUR" a determination tha
only weathering challenges like we have can create.
On behalf of our board of directors and all the employees
of UnumProvident, thank you for your continued support.
I am confident that our plans, executed well, will continue
to generate value for our shareholders.

Thomas R. Watjen
President and Chief Executive Officer