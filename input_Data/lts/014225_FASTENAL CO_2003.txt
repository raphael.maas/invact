      2003                      President's Letter to Shareholders


     The 2003 economy was a bit of a rollercoaster, but                Since the conversion, we have seen an improvement in our
     overall, Fastenal weathered the storm fairly well. We are         stores' overall performances, the most important indicator
     proud of the results our Company delivered in fiscal 2003.        being customer activity, followed by sales growth and
     Our net sales in 2003 were $994.9 million, a 9.9% increase        productivity. Packaging fasteners and making them a
     over the $905.4 in 2002. The 2002 amount included net             self-service item has proven to expedite the purchasing
     sales of $17 million from the Company's DIY Business,             process for the customer. Because of these efficiency gains,
     which was disposed of in October 2002. Excluding sales            we only had to add 92 people in the branches in 2003.
     arising out of the operation of the DIY Business, growth in
     net sales was 12% in 2003.                                        The inventory requirement for the CSP initiative is very
                                                                       high. We added $28 million dollars to the existing store
     Excluding the impact of the DIY Business, our average daily       inventories in 2003. Because of the better inventory
     sales growth each month (when compared to the corresponding       selection at the store locations, we need less backup
     month in the preceding year) increased between 8.5% and           inventory; this allowed us to reduce the distribution
     14.5% in the first half of the year, but during the second half   center inventories by $21 million. This, along with the
     we saw steady improvement. With average daily growth of           $9 million of inventory for the new stores, brings our
     16.9%, December proved to be our best growth month of the         annual inventory growth to $16 million.
     year. We believe this improvement in performance was the
     result of a strengthening national economy, as well as            These changes are both exciting and highly motivating and
     better execution by the dedicated team at Fastenal.               have really given our employees a strong sense of pride and
                                                                       ownership. Throughout the year I traveled to stores in more
     Our net earnings in 2003 grew to $84.1 million, an increase of    than 20 states, and witnessed firsthand the progress we
     11.4% over our $75.5 million in 2002. This earnings growth        have made and the enthusiasm of our store employees. I
     occurred because of higher net sales, and internal expense        believe they really appreciate the investment the Company
     control. We were very good at the latter, despite the added       is making and, in turn, have raised the bar on their own
     challenge of converting 567 stores to the new Customer            performance. They understand that when the Company
     Service Project (CSP) format and adding 151 new stores.           grows, so do their paychecks.

     New store openings continue to be a major growth                  We also recognize our Company's future success requires us
     initiative. With 151 new stores in 2003, we now own and           to constantly train our employees and develop our future
     operate 1,314 stores. We continue to research markets and         leaders. The Fastenal School of Business has continued to
     identify locations for future openings. We expect to              expand and improve its course offerings. In addition to
     continue opening approximately 12% to 16% more stores             maintaining the current course schedule, the School
     per year, or 160  210 in 2004.                                   introduced two new programs. A one-week advanced selling
                                                                       course, which is offered to our managers and outside sales
     By far the largest project we worked on in 2003 was the           people. (To date, over 550 employees have completed this
     Customer Service Project. We have been working on this            course.) The other is operational development training,
     project since the beginning of 2002. It is designed to            which is designed to help the lead people in our distribution
     improve the service we provide to our customers by                centers become better leaders. During the year, we also
     improving the inventory selection, merchandising, and, in         enhanced our five-week accelerated store manager
     some cases, the location of the store itself. Our product         development program. This program is designed for high
     managers, along with store personnel, continue to improve         potential employees seeking store manager positions.
     the original CSP inventory selection by researching what          These initiatives are designed to help us build our leadership
     customers want, and then making sure those products are           and successfully manage our growth.
     readily available in the stores. The merchandising and store
     layouts continue to improve as well. The top priority in          As a distributor, sourcing product is one of the most
     merchandising is signage and information, so customers            important areas of our business. We currently source 15%
     can more easily serve themselves. Another important part          of our product direct from Asia. Our product managers
     of this project is store location. With better inventory          have identified opportunities to source another 15%-20%
     selection and merchandising, it is important that we generate     of our product direct. These products are currently
     more customer traffic through our stores so they have a           produced in Asia and we buy them through an importer in
     better understanding of all the different products available.     the United States. Because of these opportunities we



decided to open a trading company in Asia. The company          We relocated our distribution center in Atlanta Georgia,
                                                                in the first quarter; giving us much needed additional
(FASTCO) was opened in Shanghai because of its central
location. Our corporate purchasing manager relocated his        space. We also opened our first distribution center
                                                                outside of the United States during the second quarter.
family from Minnesota to China and is the president of the
company. We currently have 12 employees in China and            It is a 62,000 square foot facility located near Toronto,
                                                                Ontario. In December 2003 we purchased a 250,000
plan to add 3-5 more in 2004.
                                                                square foot facility in Greensboro, North Carolina; this
Our international business opportunities are growing, and       will replace our current leased facility. We plan to start
we believe that two strong markets for Fastenal are in          operating from this facility in 2004.
China and Europe. Our Vice President of International
Sales appointed regional sales managers for both Europe         2003 was a difficult year for our distribution people.
and China in 2003. These individuals, both long-time            Because of the heavy investment in the CSP initiative, we
Fastenal employees, will relocate during the first quarter of   asked them to hold expenses flat to help offset the cost of
2004. These markets were chosen as a result of requests         the project. In true Fastenal spirit, they rose to the
from current customers who would like us to support them        occasion and not only met our expectation but exceeded
in other parts of the world. The sales contribution from        it by reducing expenses without sacrificing service to our
Europe and China is expected to be very low in 2004, but        stores. They ended the year with 234 fewer employees
we anticipate a greater contribution in future years.           than they had at the start of the year, a reduction of 18%.
                                                                Due to the high number of part-time employees, this was
During 2003 we made significant progress in growing our         accomplished entirely by attrition.
government business. We sold over $27 million dollars to
governmental organizations, for a gain of 54% over 2002.        Although our inventory grew more than we planned, our
Fastenal was chosen the Most Valuable Schedule Holder in        balance sheet is in as good of condition as it has ever been.
the hardware and appliance categories. The winner of this       Our receivables continue to grow at a rate about equal to
award is chosen by government purchasers, and is awarded        the rate of sales growth. Even after increasing our
based on customer service. The competition is very strong       dividend from .05 to .21 and investing heavily in our CSP
in this area, and we are very proud to receive this award.      initiative, we generated $35 million in additional cash.

Our National Accounts team had to work very hard to             As I reflect back on the past year, one constant remains
maintain their sales growth in 2003, and I'm happy to           throughout all the challenges. That is the quality of the
report they were successful. This is a competitive market,      Fastenal employee. The leaders of Fastenal work very
and the large industrial accounts they call on were trying      hard to create an environment where people are free to
very hard to control their spending. However, with the          make decisions and use their creativity to grow the
consistent efforts put forth by the Fastenal team, our          company and help create new opportunities for
national accounts program was able to grow 17.3% over           themselves. I am certain that if we continue to believe in
2002, and contributed $216 million to our 2003 sales.           our people and encourage them to share our core values
Although much smaller, our National Construction Sales          of ambition, integrity, innovation, and teamwork, we
group, which was started in 2002, signed 56 supply              will be able to continue to open stores, grow our
contracts with major construction companies and                 business, and create new opportunities for our people.
contributed almost $10 million to our 2003 sales.
                                                                On behalf of all the people who work at Fastenal, I thank
During 2003, we continued to expand the use of                  you for your continuing support, and promise to work hard
technology to improve our business. The Information             to continue to grow our business.
Systems staff continues to make improvements in our
electronic billing software as well as work on several
different programs to speed up communications with
suppliers, customers, and employees. This department is
also working on developing our new web site, which we
plan to introduce in March of 2004.


