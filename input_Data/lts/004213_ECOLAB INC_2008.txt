                


     For 85 years, ecolab's sustainable solutions
     have continued to provide sustainable
     growth  2008 was no exception
     Since our company's founding in 1923, ecolab has focused           While we were responsive to the rapidly fluctuating markets,
     on doing what's right for our customers, our associates,           the same fundamentals that have driven our success in the past
     our communities and our shareholders. We've consistently           delivered for us again: great customer service, innovative and
     led our industry in innovation and premium service. it's this      sustainable solutions, a clear business strategy and a dedicated
     commitment to driving new customer solutions that has led to       and talented group of associates. We also took additional actions
     our long history of sustainable growth.                            in response to the changing market conditions and the slowing
                                                                        global economy. We worked to achieve appropriate pricing
     through both healthy and declining economies, this commitment      for our products, aggressively managed our costs,
     to creating solutions has served us well  and 2008 was no         drove operational efficiency and productivity throughout
     exception. As the year progressed, we were tested by the global    all business sectors and streamlined our supply chain and
     markets  unprecedented increases in raw material costs,           production operations.
     rapidly changing currency exchange rates, dramatic changes
     in financial markets  and, as the year wound to a close, a        in spite of the challenges, we continued to make investments in
     significant economic slowdown. We made a number of necessary       our business. We improved our business information systems
     adjustments to our plans and implemented actions to address        to enable future growth, armed our sales people with more
     these challenges. We did this without losing focus on taking the   technologically advanced tools, and took many other actions
     right action for our business, customers and shareholders to       that allowed us to continue to deliver the sales and profit growth
     provide sustainable solutions and sustainable growth. And  as     our shareholders have come to expect. All of the decisions
     we have consistently done for more than eight decades now         we made were directed at developing sustainable long-term
     we delivered.                                                      opportunities for our business while delivering attractive
                                                                        returns today.

                                                                        Read on to learn more about our 2008 highlights:




FinAnCiAl peRFoRMAnCe
                                                                    y We also successfully rolled out our new SolidSenseTM solid
We're proud to say we achieved another strong financial
                                                                      detergent product line for several key corporate customer
performance in 2008 despite the difficult end market conditions
                                                                      accounts in the quick service restaurant (QSR) sector.
and major raw material cost increases. to offset these factors,
                                                                      SolidSenseTM concentrated capsules deliver important
we continued our efforts to aggressively drive new account
                                                                      benefits for our QSR customers by using advanced dispenser
gains, increased our product penetration in key accounts and
                                                                      engineering to automatically mix solid concentrates
launched innovative and sustainable solutions to our customers,
                                                                      with water to ensure proper use dispensing rates for the
all while making key investments to sustain our own growth for
                                                                      products. in addition, because the products are 99% water-
the future.
                                                                      free, they reduce packaging waste by more than 40% and
y ecolab's net sales rose 12% to $6.1 billion in 2008. excluding      contribute to lower freight costs, thereby reducing a facility's
  the impact of acquisitions and divestitures and when                environmental impact. this new solids product line also
  measured at fixed currency rates, sales rose 6%, fueled by          lays the groundwork for continued customer conversion
  strong growth in the u.S., Asia pacific and latin America.          throughout 2009.
y operating income was $713 million in 2008. excluding special      y With the growing globalization of the world's food supply,
  gains and charges, operating income reached $738 million,           ecolab believes it is more important than ever to make best
  representing an 8% growth over 2007. Growth was driven              practices available in all countries to assist in improving total
  by sales volume and pricing gains, improved cost efficiencies       hygiene and food safety throughout every aspect of the food
  and reduced variable compensation, all of which more than           chain. ecolab took an industry leadership position in China
  offset higher delivered product costs.                              by debuting a number of programs focused on food safety. in
y Reported diluted earnings per share were $1.80, up 6% from          September, we introduced the ecolab Food Safety program
  2007. excluding special gains and charges and discrete tax          (eFSp), a total solution to meet the food safety needs of
  items, diluted earnings per share were $1.86 for 2008, up           China's foodservice, retail and hospitality markets. the
  12% from $1.66 in 2007.                                             eFSp is modeled after the u.S. Hazard Analysis and Critical
y our return on beginning shareholders' equity was 23% in             Control point (HACCp) food safety program. eFSp focuses
  2008, the 17th consecutive year in which the company met or         on preventing hazards that could cause foodborne illness
  exceeded its long-term financial objective of a 20% return on       by applying science-based controls from raw material to
  beginning shareholders' equity.                                     finished product. like the HACCp program, the eFSp includes
y Cash flow from operations was $753 million. total debt to           several critical modules, including food safety assessment,
  total capitalization ratio was 42%. our debt rating remained        consulting, hygiene solutions, management systems, training
  within the "A" categories of the major rating agencies              and quality assurance. it was followed by the introduction of
  during 2008.                                                        a similar program for China's brewery, beverage, dairy and
y We repurchased 12.1 million shares of our common stock in           food processing markets in october, called the ecolab total
  2008, reducing our shares outstanding by approximately 4%.          Hygiene Management (etHM) solution.
y We increased our quarterly dividend rate for the 17th             y We introduced one of our most sustainable u.S. product
  consecutive year, as it rose 8% in December to an indicated         solutions - peRformanceTM - to european commercial
  annual rate of $0.56 per common share.                              laundries. peRformanceTM is a low-temperature, proprietary
y Against the backdrop of the 39% decrease of the 2008                oxygen bleach that reduces energy consumption for our
  Standard & poor's 500, our share price declined 31% in              textile Care customers and increases textile life. this addition
  2008. While better than the overall market, it was still            strengthens our global product portfolio and is another way
  disappointing, especially given our strong operating results.       to help commercial laundries reduce total operating and
  our share performance has exceeded the S&p 500 in each of           environmental costs through a combination of chemistry,
  the last five years and in 15 of the past 18 years.                 service, engineering, technology and water care solutions.
                                                                    y pest elimination combined proactive pest protection with a
neW pRoDuCtS AnD SeRViCeS                                             more environmentally sustainable customer solution through
                                                                      its successful launch of Checkpoint Catch inserts and
As end markets declined and customers faced increasing
                                                                      Checkpoint Snap trap inserts. these new patent-pending
pressure to remain profitable, ecolab continued to deliver
                                                                      devices strengthen our already proven ecolab Rodent
innovative, sustainable solutions and expert service to help
                                                                      program and are designed to prevent exterior rodents
customers optimize their operations, save water, waste, energy
                                                                      from entering commercial facilities, while reducing the
and labor costs, improve safety and drive value throughout
                                                                      dependence on rodenticides. in addition, both insert types
their businesses.
                                                                      conform to national organic program standards when used
y through our new, revolutionary ApexTM warewashing system,           without pesticides, so they give ecolab's organic customers
  restaurants gain better control of their warewashing process        more options for protection than are available with
  by decreasing energy, water and labor costs and lowering            competitors' programs.
  their operations' overall environmental impact. ecolab sales-     y Vehicle Care also launched a sustainable customer solution
  and-service associates download and analyze operating               called Blue Coral Beyond Green, an innovative industry
  data from the system's controller to help restaurants find          program that helps assure regulatory compliance among car
  additional ways to reduce their warewashing operational             wash operators across north America, and addresses the
  costs and improve efficiency. in fact, one of our top corporate     car wash consumer's desire for environmentally preferred
  ApexTM customers was able to reduce the number of                   options. As with all of our products, we look beyond simply
  dishmachine racks it washes by 11 million, reducing water and       providing the best chemistry by taking a comprehensive
  energy costs by $1 million in just one year. ApexTM enjoyed a       approach to sustainability. We assist our business partners in
  strong response and sales nearly doubled our 2008 budget.           becoming more sustainable by focusing on four key elements:
                                                                      water reduction, waste reduction, energy savings and
                                                                      personal safety.


                                                                        leADeRSHip DeVelopMent
     these examples are just a few of the more than 50 new products     Strong leadership is required to sustain our future growth.
     and services launched last year. You can find more of our latest   important leadership developments in 2008 included:
     product and service offerings in the "Review of operations"
                                                                        y in February, Barbara J. Beck, executive Vice president of
     section beginning on page 14 of this report.
                                                                          Manpower inc., joined ecolab's board of directors. Barbara
     of course, beyond products and programs, our legendary               has been president of Manpower's eMeA operations since
     customer service remains ecolab's key differentiator and one of      2006, overseeing europe (excluding France), the Middle east
     the best value-added benefits our customers receive. Already         and Africa.
     the largest and most trusted team in the industry with more        y in April, Susan nestegard was promoted to executive Vice
     than 14,000 sales-and-service associates, we continued to            president, Global Healthcare, where she focuses exclusively
     invest in our team in 2008. We expanded the tools and training       on the expansion and growth of our Healthcare business.
     we provide to ensure our associates have the technology and          We recruited Dr. larry Berger in late April to take Susan's
     support they need to help our customers run clean, safe and          previous position as Senior Vice president and Chief
     efficient operations. this commitment to personal service            technical officer.
     continues to inspire confidence among our customers that           y in August, Christophe Beck was promoted to executive
     ecolab is a dedicated partner in the long-term success of            Vice president  institutional north America. Christophe's
     their businesses.                                                    responsibilities were increased to cover both the Foodservice
                                                                          and Hospitality businesses in the u.S. and Canada.
     BuSineSS DeVelopMent
                                                                        ACHieVeMent & ReCoGnition
     We've made key investments in our company to streamline our
     operations and aggressively pursue new growth opportunities.       each year, ecolab is recognized for its global leadership across a
                                                                        number of industries where we conduct business. Here is just a
     y We successfully completed a major transformation of our          sampling of the recognition we received in 2008:
       shareholder base as Henkel AG & Co. kGaA sold its entire
       stake in ecolab, which totaled 72.7 million shares before the    y ecolab earned fifth place on "America's Best Big Companies
       sale. As part of the transaction, we purchased 11.3 million        Honor Roll." the honor roll recognizes only 16 companies that
                                                                          have consistently appeared on Forbes magazine's annual list
       shares of ecolab stock at a discount from Henkel. Henkel
       held an investment in ecolab since 1989 as part of a               of the 400 best big companies in America  also known as
       transaction in which ecolab and Henkel formed a joint              the Forbes platinum 400 - since its inception in 1999. this
       venture in europe, combining each company's european               recognition underscores our focus on consistently achieving
       commercial cleaning and sanitizing operations. in 2001, we         strong financial results in the right way.
       purchased Henkel's interest in the joint venture for cash. in    y For the second consecutive year, ecolab was named one
                                                                          of the "World's Most ethical Companies" by Ethisphere
       February 2008, Henkel announced its intention to sell its
       ecolab shares, and undertook the sale of all of its shares         magazine. the list is composed of companies that use
       in november. Despite difficult financial market conditions,        ethical leadership as a purposeful method to drive profits
       the transaction was successfully completed with the stock          and growth. We are proud to be among the fewer than 100
       sold to a strong group of u.S. and foreign institutional and       companies chosen for the award.
       retail investors. We now enjoy an even stronger and more         y in January, our ApexTM Dishmachine received the 2008
       diversified shareholder base.                                      kitchen innovations Award from the national Restaurant
     y We established a new european headquarters in Zurich,              Association. Recipients were selected based on how well they
       Switzerland. our new europe, Middle east and Africa (eMeA)         provided solutions to the many challenges facing restaurants,
       headquarters centralizes strategic leadership in the region,       including utility costs, labor, quality and efficiency. ApexTM
       and supports a more efficient, pan-european business model.        is the first all-solid, low-temperature dishmachine to use an
     y We successfully completed a pilot implementation of our            epA-registered solid sanitizer and an attached controller to
       ecolab Business Solutions (eBS) in europe during late              optimize operational efficiency and reduce environmental
       2008, and will continue the rollout to other european              impacts within the dish room. We were honored to be
       country operations in 2009. eBS is an extensive project that       included among this elite group of products.
       implements a common set of business processes and systems        y ecolab was also named one of the "top 50 Most Military-
                                                                          Friendly employers in the united States" by G.I. Jobs
       across all of europe. this platform enables us to accelerate
       our growth and improve economies of scale. the resulting           magazine. the foundational criteria include the strength
       global integration and shared information improves sales           of company military recruiting efforts, the percentage of
       and support of our pan-european organization, increasing           new hires with prior military service, and company policies
       the quality of service and the depth of commitment to              toward national Guard and reserve service. We are extremely
       our customers.                                                     pleased to receive recognition for our recruitment efforts.
                                                                        y For the fifth consecutive year, ecolab was named to Selling
                                                                          Power's list of "the 50 Best Companies to Sell For" among
                                                                          the largest sales forces in the united States. the magazine
                                                                          uses key metrics including compensation, training and career
                                                                          mobility to make its determination. We know our sales-and-
                                                                          service force is the key to our success and growth and we're
                                                                          delighted to again be acknowledged.


outlook FoR 2009
                                                                   y We will accelerate structural improvements through
Moving into the new year, we expect 2009 to test businesses
                                                                     expediting our product line makeover and Sku (stock keeping
around the globe. the deep global recession is expected to
                                                                     units) reduction plans, leveraging our lean Six Sigma
continue and will create demands on business models, strategies
                                                                     and purchasing efforts, optimizing our supply chain and
and people. Although we aren't immune, we are confident in our
                                                                     aggressively managing raw material costs.
market position and the strength of our team. We know that we
                                                                   y We will continue to realign our cost structure to protect
have the ability to execute on our strategy. We not only expect
                                                                     our future for the near- and long-term. We've taken some
ecolab to weather these conditions, but also to continue to grow
                                                                     aggressive steps toward these goals early in the year, and are
while positioning ourselves for an even stronger future.
                                                                     planning on a further optimization of our supply chain.
                                                                   y We will drive change in europe, moving forward with our
We have already made tough decisions. the most difficult was
                                                                     eBS rollout, leveraging the new pan-european management
restructuring our workforce in January. it is not easy to ask
                                                                     structure in Zurich, completing product rationalization and
associates, who have made many important contributions, to
                                                                     investing in our sales force with training and field technology
leave the company. Still, we strongly believe that these actions
                                                                     to accelerate growth and profitability.
are the right ones for the company and were necessary to
                                                                   y We will continue to invest in our most valuable resource: our
protect our future. our goal was to make sure that we
                                                                     associates. We're working on strengthening our high
realigned resources to best enable us to meet our 2009 and
                                                                     potential leadership pipeline, continuing to implement our
Strat 2015 goals.
                                                                     diversity and inclusion strategy and focusing on
there is no doubt the market will challenge us. on the downside,     development opportunities.
we're seeing declines in our foodservice and hospitality
                                                                   As with every year, with challenges come opportunities. We plan
markets and expect them to continue. on the upside, those
                                                                   to successfully manage through this very challenging year and
declines are countered by typically resilient healthcare, food
                                                                   protect our growth for our customers, associates, shareholders
and beverage, education/government and food retail markets,
                                                                   and community. We will push forward with our 2015 strategic
and our customers' increased needs for effective solutions that
                                                                   plan and preserve our core investments. We will continue to
help them reduce their costs. We have a strong market position
                                                                   invest in innovation, take advantage of merger and acquisition
with enormous potential. We will work aggressively to grow our
                                                                   opportunities, build the best team in the industry and position
market-leading position, which is presently 12% of a $50 billion
                                                                   ourselves for growth. Because no matter the obstacles we face
market. And we will rely on competitive advantages like a strong
                                                                   this year, we remain focused on delivering great value, just like
global customer base, leading technology, a dedicated sales-
                                                                   we have for the past 85 years.
and-service team and organizational efficiencies to position us
to win in this environment.

y We will drive increased market share. We know that larger
  customers fare better in these economic times and will
  aggressively drive share within corporate accounts. We will
  also continue investing in the best growth opportunities like    douglas m. baker, Jr.
  healthcare infection prevention, escalating pest elimination
                                                                   Chairman of the Board,
  operations globally, expanding our water/energy platform         president and Chief executive officer
  and penetrating emerging markets like China, east europe
  and Brazil.
y We will leverage innovation, such as our total impact
  program, to create additional value for our customers. By
  combining our products and services, we can solve big
  customer issues through a total impact approach in reducing
  costs associated with water, energy and waste. products
  like ApexTM, Dryexx and SolidSenseTM offer customers
  outstanding results while providing significant savings and
  sustainable solutions.
