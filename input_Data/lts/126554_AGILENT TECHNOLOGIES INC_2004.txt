To Our Shareholders
     Excellent financial results, great progress on operations and focused actions to address
opportunities and challenges made 2004 a very successful turnaround year for Agilent. After the
company returned to profitability in the fourth quarter of fiscal 2003, we achieved consistently
strong profitability and generated more than $700 million in cash in 2004. We completed a sweeping
operational transformation, maintained the R&D investments that fueled an outstanding year for
new products, and improved our competitive position by entering new markets and exiting others.
Agilent's 28,000 people continued to work with great skill and energy in a difficult environment to
deliver these results. We began 2005 a much stronger company than we were a year ago, and we are
excited by our opportunities to build on this year's accomplishments.

Outstanding Financial Performance in 2004
     In 2004 Agilent's orders rose 15 percent over 2003 to $7 billion, while revenue increased 19
percent to $7.2 billion. During the first half of the year, strong demand in the semiconductor and
related industries, especially wireless communications, was a key driver of this growth. In 2004 we
also did an excellent job of managing operating expenses, which were $294 million lower than in
2003. This improvement reflects the work we have done during the past three years to bring
Agilent's costs in line with the competitive realities of our markets; to date we have taken about
$800 million out of our quarterly cost structure. In 2004 we earned $349 million, or 71 cents per
share on GAAP basis, compared with a loss of $2.058 billion, or $4.35 per share, in 2003.(1) On a non-
GAAP basis, earnings were $529 million, or $1.05 per share, compared with a loss of $121 million, or
26 cents per share, in 2003.

     This was also an excellent year for asset management. Inventories were only $31 million higher
at the end of 2004 than a year ago, a great result given our revenue increase for the year of more
than $1.1 billion. In 2004, investments in property, plant and equipment declined by $87 million
from 2003. These improvements, along with our strong profitability, enabled outstanding cash
generation. We began 2005 with about $2.3 billion in cash and cash equivalents.

     In 2004, two of our businesses did very well all year and two others were stronger in the first
half of the year than in the second half. While we saw a decline in orders in wireless handset
manufacturing test markets in the fourth quarter of 2004, our Test and Measurement (T&M)
business achieved an 18 percent increase in overall orders and 15 percent growth in revenue in
2004. Driving T&M's comeback were improved conditions in many of its markets, strong new
products, outstanding expense control and the benefits of its restructuring during the past three
years. Our Life Sciences and Chemical Analysis (LSCA) business had a consistently strong year, with
healthy growth in orders and revenue, a 30 percent improvement in operating profit, and record
orders and revenue in the fourth quarter. LSCA enhanced its leadership in its core chemical analysis
business, which serves the petrochemical and environmental markets, while strengthening its
position in the life sciences business, where Agilent products and solutions are helping researchers
understand the genetic basis of disease and develop new drugs.

     After a strong first half, demand from the semiconductor and related industries slowed
significantly, as the industry worked through excess inventory and capacity. This slowdown affected
second-half results in our Semiconductor Products Group (SPG) and Automated Test Group (ATG).
For the full year, orders in SPG were up 20 percent while revenue rose by 27 percent. In the fourth
quarter we announced plans to sell SPG's camera module business to Flextronics after we
determined that this business would not achieve acceptable profit levels as part of Agilent. In ATG,
orders fell 2 percent for the full year while revenue increased by 22 percent over fiscal 2003. In 2004
we acquired IBM's flat-panel test business, which we believe complements our existing automated
test businesses and offers excellent opportunities for profitable growth.

                                                   
A Major Operational Transformation
     In 2000 we launched a major effort to transform how Agilent operates in order to make the
company more efficient and cost effective. In 2004 we largely completed this transformation, which
is starting to deliver strong returns on our investments.

     Our information technology (IT) systems have been a major focus of this effort. When Agilent
became an independent company in 1999, we began to tailor our IT systems and applications to our
needs. This year we completed the implementation of our ERP (enterprise resource planning) and
customer support systems, which are key building blocks of our IT infrastructure. We also finished
the outsourcing of some IT functions to partners who can deliver these services more
cost-effectively. In 2004 we reduced legacy applications by nearly 60 percent, reduced our overall IT
costs by 27 percent compared with 2003, and made it easier for customers to use the Agilent Web
site to find product and support information.

Maintaining Investments in Research and Development
     During the past three years, as we pursued our operational transformation and endured the
severe downturn in the electronics industry, we balanced the need to lower costs with investments
in research and development (R&D) that enable us to develop the new products that are crucial to
long-term success. In 2004 we spent $914 million on R&D, with about 7 percent of this total at
Agilent Laboratories, our central R&D facility. This year Agilent Labs extended its long track record
of vital contributions to new products.

     Based on the partnership between Labs and SPG, we introduced an industry-first laser sensor
for Logitech's latest computer mouse that achieved strong market acceptance because it greatly
extends the capabilities of the traditional mouse based on light emitting diodes. LSCA introduced a
revolutionary new liquid chromatography product that takes advantage of Agilent's core
competencies in microfluidics, optics and semiconductor materials and that has many potential
applications for sample separation and preparation in life sciences research. And in EPSG, we
launched a high-performance arbitrary wave generator, based on world class Labs research in
digital-to-analog converters and digital interpolators for RF signal generation.

Customer and Quality Focus
    New products, as well as our operational improvements, contributed to substantial customer
recognition in 2004. Nortel NetworksTM named Agilent its 2003 Technology Supplier of the Year, and
we won the Outstanding Supplier Award from RF Micro Devices in China, as well as the Global
Supplier Award from Eli Lilly and Company. We are gratified by these awards and determined to
achieve further progress addressing and anticipating customer needs.

    To help further improve our performance with customers, the Agilent Customer Satisfaction
program provides customer feedback that we track and compare with aggressive improvement goals.
We also rolled out a new quality education program to employees globally, and initiated a
company-wide Six Sigma quality program.

A Leader in Corporate Citizenship
      Outstanding corporate citizenship has two main components, and both were important
priorities for Agilent in 2004. The first element is corporate governance  the practices that bring to
life the highest standards of integrity and transparency. The other element of citizenship is the role
we play as an employer and community member. In 2004 we were proud to be ranked No. 9 on the
100 Best Corporate Citizens list published by Business Ethics magazine, which cited Agilent's
"diversity practices and superior treatment of the community." In 2004 more than 6,000 Agilent
people volunteered in company-supported education, health and human service, and environmental

                                                   2
programs or events; 16 countries where we do business have active community programs;
approximately 273,000 students were reached through Agilent-sponsored education programs; and
we improved our environmental performance in a number of areas. This year we were formally
recognized for our contributions and results in 22 communities worldwide. Our corporate
citizenship efforts strengthen Agilent's long-term competitiveness and help improve the viability of
our many communities, and we will continue to make these efforts a priority.

Business Outlook
    Agilent competes in large markets that are undergoing significant change and are becoming
mature. While overall growth rates in these markets are likely to be modest, there are numerous
opportunities for more robust growth within these markets. Our products, customer relationships
and deep technology expertise position us well to pursue these opportunities, and we will continue
to do so in 2005.

     The imbalance in semiconductor inventory and capacity that affected our results in the second
half of 2004 is a reason for caution as we begin 2005, but we do not believe that we are entering a
deep downturn like the one that started in 2001. While we expect demand in our wireless test
business to be soft in the first half of 2005, we believe the semiconductor industry will work through
the overcapacity during the first half of fiscal 2005 but will be down slightly for the full year.

     Our priorities in 2005 start with our focus on maintaining the financial and operational
discipline that was crucial to our results in 2004. We will build on the dramatic progress we have
made in our cost structure and in our IT, manufacturing and other functions. This year we will also
emphasize the need to improve all aspects of our customers' experience with Agilent. We survey our
customers in depth on a regular basis, and we are taking action to increase their satisfaction and
loyalty. In addition, we are increasing our investments in employee development in 2005, in order to
expand the skills, leadership capability and commitment of Agilent's people.

     The start of 2005 is the beginning of Agilent's sixth year as an independent company. It has been
an incredible first five years. We launched the company, grew rapidly, survived the downturn and
transformed virtually every element of how we do business. Our people and culture have been
tested, we have learned a lot, and we have emerged as a much stronger company. Now we are
embarking on a transformation in growth and innovation that is equal in scope to the operational
transformation we have made. Our overarching goal in 2005 is to achieve consistently profitable
growth. The key to doing that is to unleash the innovative capacity of Agilent's people. Our ability to
innovate, combined with our outstanding products, long-term customer relationships and greater
operational efficiency, position us well to achieve our goals in 2005 and to continue making Agilent
the company we want it to be.




Ned Barnholt
Chairman, President and Chief Executive Officer


