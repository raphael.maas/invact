Fellow Stockholders:
Im pleased to report to you that Nucor had a much improved year financially in 2014,
earning $713.9 million, an increase of 46% over 2013 earnings of $488.0 million. This
growth is a direct result of the preparations we made during the long economic downturn
to take advantage of the eventual economic recovery  a recovery which we are finally
realizing. Our focus on a long-term strategy for profitable growth continues to pay
dividends and has positioned us to outperform our peers.
If you have not read our mission statement, I invite you to do so. Taking care of our customers
is something we take very seriously, and we know that our customers are not just the people
who buy our steel. Our customers include you, our shareholders, and our teammates. I and
the other 23,600 men and women of Nucor are proud of the earnings we have delivered in
what has been a very challenging steel market. The hard work of our Nucor team continues
to create attractive value for our shareholders.

Financial Highlights
As I noted above, in 2014, Nucor earned $713.9 million, or $2.22 per diluted share, compared
with consolidated net earnings of $488.0 million, or $1.52 per diluted share, in 2013. Improved
economic conditions in key markets, like nonresidential construction, energy and automotive, as
well as the benefits we are starting to realize from the nearly $6 billion in capital expenditures
and acquisitions our company has made since 2009, helped drive this earnings growth.
Consolidated net sales increased 11% to $21.11 billion
compared with $19.05 billion in 2013. Total tons shipped
to outside customers increased by 7% over 2013. The
average scrap and scrap substitute cost per ton used
increased 1% to $381 from $376. Overall, operating
rates at our steel mills improved to 78% in 2014 from
74% in the previous year.
In the 41 years since Nucor began paying dividends, the
Board of Directors has elected to increase the base cash
dividend every year. In 2014, that practice continued as
the board voted to increase the regular quarterly cash dividend on our common stock from $0.37 to $0.3725 per share. Nucor has made
166 consecutive quarterly cash dividend payments. Total return on invested capital from 2005 through the end of 2014 was 14.0%.
Our balance sheet remains strong, with cash and short-term investments totaling $1.12 billion dollars at the end of the year, above
our targeted minimum level of $500 million. Adding to Nucors strong liquidity, our $1.5 billion unsecured revolving credit facility is
undrawn, and it does not mature until August 2018.
Our improved performance in 2014 occurred despite a difficult economic environment for the global steel industry. Many steel
markets around the world are depressed or suffer from excess capacity, and the strengthening U.S. market has made it a magnet
for foreign imports, with imports rising 38% compared to 2013. Because much of this import activity is driven by governmentowned
competitors and illegal subsidies, Nucor and other domestic steel producers have worked hard to see that foreign
governments who violate our trade laws are held accountable. We applaud several trade enforcement actions taken by the U.S.
government, including terminating its suspension agreement with Russia for hot-rolled steel imports, assessing duties on oil country
tubular goods from South Korea and five other countries and ruling that the domestic rebar industry has been materially injured as
a result of dumped and subsidized rebar imports from Turkey and Mexico.

Positioned for Profitable Growth
I am also excited to report that the entire Nucor team has been doing an excellent job executing our long-term strategy for
profitable growth. Of the almost $6 billion we invested between 2009 and 2014, about two-thirds went to capital expenditures
and one-third to acquisitions.

Our most significant investment in 2014 was the acquisition of Gallatin Steel. Nucor purchased all the equity of Gallatin Steel for a
cash purchase price of approximately $779 million. Adjusting for the net present value of the anticipated tax benefits, the realized
effective purchase price is approximately $660 million.
The Kentucky steel mill is an excellent fit for us, increasing our hot-rolled sheet steel capacity by 16% to more than 13 million tons.
Acquiring Gallatin enhances our leadership position in the flat-rolled hot band products market. It strengthens our capabilities
to serve flat-rolled customers in the growing pipe and tube industry and broadens our footprint in the Midwest region, which is
the largest flat-rolled consuming market in the United States. The mills location on the Ohio River strongly complements our raw
materials strategy, as it is well positioned to receive direct reduced iron (DRI) from our Louisiana facility. We have quickly integrated
this strategic investment into the Nucor family. Gallatin achieved profitability in the fourth quarter of 2014 (its first quarter with
Nucor) with profits exceeding the negative impact of inventory-related purchase accounting costs.
Several capital investment projects related to our raw materials and value-added products strategies began production in 2014.
Our new DRI plant in Louisiana completed its first year of operation, producing DRI at world-class quality standards with
metallization rates of 96% and carbon content exceeding 4%. Start-up of the plant has not been without its share of challenges,
but that was not entirely unexpected based on our start-up experience at our Trinidad DRI plant. We will continue to work through
these challenges; however, based on the quality of the DRI the plant has produced, we remain confident our expanded DRI
capacity will help us grow market share in the higher-value sheet, plate and Special Bar Quality (SBQ) markets. Its worth noting
that our Trinidad DRI plant, in operation since the beginning of 2007, contributed stronger earnings at the end of 2014, allowing
Nucor to take advantage of falling iron ore prices compared to competitors relying fully on scrap and purchased scrap substitutes.
This further demonstrates the strategic value of our raw materials strategy.
On the steel-making side, our Nucor Steel Berkeley sheet mill started producing 72-inch sheet steel, giving it access to a new market
segment we estimate to be approximately four million tons annually, while Nucor-Yamato began prime production of its new sheet
piling sections, expanding our product offerings to include wider piling sections that are lighter and stronger, and therefore covering
more area at a lower installed cost. Our new wire-rod mill in Darlington, South Carolina, continues to penetrate more challenging
market applications, and we are only beginning to see the benefits of expanded SBQ capacity at our steel mills in Memphis,
Tennessee, and Norfolk, Nebraska, as we enter 2015. All of these projects allow Nucor to maintain our position as a low-cost
commodity producer while expanding our portfolio into more challenging applications that are less exposed to import competition.
Even as we have pursued these new investments, we have remained committed to ensuring reliable operations at all of our existing
facilities. A significant portion of our nearly $4 billion in capital expenditures since 2009 has been spent on maintaining and
enhancing our operational capabilities.

Focused on Our Customers
The capital investments we have made and the
results they are producing continue Nucors long
tradition of operational excellence. However,
realizing our full earnings potential requires our
company to go beyond operational excellence;
we also must achieve commercial excellence.
Being North Americas most diversified steel and steel products company gives us an opportunity to build stronger relationships
with each of our customers. And we are seizing the opportunity to create value for our customers  and to capture additional
revenue for that value.
Every product group is finding ways to benefit from the opportunities that result from our broad portfolio of products and services.
By proactively assessing each customers needs and demonstrating Nucors across-the-board capability to meet those needs, we
are moving beyond merely filling an order, to creating a purchasing experience that provides exceptional value for our customers.
Achieving commercial excellence will fully capitalize on the operational investments we have made to drive earnings for Nucor and
our shareholders.


Our Teammates  Nucors Most Important Asset
As I speak to groups across North America, I consistently emphasize that Nucors teammates are our companys greatest asset
and our greatest competitive advantage. Thats the reason we list the name of each teammate on the cover of our annual report;
2014 is the 40th consecutive year for that important recognition. Our people, combined with our strong balance sheet, are two
reasons why Nucor grows stronger during economic downturns. We did not lay off any teammates at our steelmaking operations
during the recession. Instead, our teammates worked to deploy strategic capital that enhances our earnings during challenging
cycles and accelerates growth when markets improve. Now that the economy is growing at a faster pace and key markets like
nonresidential construction are rebounding, this part of our strategy for profitable growth is paying off.
We also make it a point to pay our teammates well for their hard work. Nucor teammates do not receive guaranteed incentives
or retiree benefits. Instead, we have long embraced a pay-for-performance philosophy. Therefore, Nucor teammates only receive
fully-funded profit sharing contributions when we deliver profitable results for our shareholders. This is a model where everyone
wins. Our customers get the high-quality steel products they need. Our shareholders get a profitable return on their investment.
And our teammates have received $2 billion in profit sharing retirement savings since the program was established in 1966.
Nucors investment in our teammates also extends to their families. In 1974, Nucor founder Ken Iverson created the Nucor
Foundation to provide postsecondary education scholarships for the children of Nucor teammates. Through the end of 2014,
the Foundation has provided $75.2 million in scholarships. We also provide tuition reimbursement for team members and their
spouses. This is just one more way that Nucor is investing in the future.
Before closing, I want to highlight some changes in our executive team. Implementing a thoughtful and orderly succession plan
has been a significant strategic initiative for our company. As part of this initiative, Chad Utermark was promoted to Executive Vice
President of beam and plate products, having previously served as Vice President and General Manager of Nucor-Yamato Steel
Company. Dave Sumoski was promoted to Executive Vice President of engineered bar products. Prior to joining the executive team,
Dave served as Vice President and General Manager of Nucor Steel Memphis. Chad and Dave bring tremendous experience and
leadership skills to our executive team and help us build a deeper leadership bench for the future.
Also, this year a valued member of our executive team retired. Keith Grass served as Nucors Executive Vice President responsible
for our raw materials businesses. He joined The David J. Joseph Company in 1978. Keith became DJJs CEO in 2000 and was
named an Executive Vice President of Nucor when DJJ was acquired in 2008. Through his strong leadership, Keith established
DJJ as North Americas premier metals recycler. He was also instrumental in successfully implementing Nucors raw materials
strategy. On behalf of the entire Nucor family, I want to thank Keith for his many contributions to the combined success of
David J. Joseph and Nucor.
Finally, Peter Browning will be leaving Nucors board after fifteen years of dedicated service to our company. During Peters
tenure, he has served as Non-Executive Chairman, Lead Director and Chairman of the Audit Committee and the Governance and
Nominating Committee. On behalf of everyone at Nucor, I would like to thank Peter for his 15 years of outstanding service on
Nucors Board of Directors. I personally, and our company as a whole, have benefited greatly from his wisdom and sound counsel.
As we begin 2015, the steel industry is confronting significant headwinds resulting from collapsing oil prices and the unrelenting
wave of foreign imports. We do expect energy steel demand to improve after the initial shock of inventory destocking runs its
course. We also believe that improving nonresidential and automotive markets have the potential to more than offset the impact
of lower oil drilling-related steel demand for the balance of 2015. Whatever short-term economic and steel industry conditions
we face, Nucors unrivaled position of strength will allow our team to continue to execute on our proven strategies for delivering
profitable long-term growth.
As has been true throughout Nucors history, our companys best years are still ahead of us.

John J. Ferriola
Chairman, Chief Executive Officer and President



