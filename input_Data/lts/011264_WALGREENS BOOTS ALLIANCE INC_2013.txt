Dear Shareholders:
We are pleased to update you on Walgreens performance this year as
we achieved record sales of $72.2 billion, strong profits of $2.5 billion
and record free cash flow of $3.1 billion.* Most of all, fiscal 2013 was
a year of major strategic progress as we crystalized and advanced the
transformation of our company for long-term sustainable growth and
value creation. We are Well positioned for a strong future.
Continuing to meet our long-term goal of delivering top-tier shareholder
returns, Walgreens returned $1.0 billion to shareholders this year through
dividends. Over the past five years, Walgreens dividend has grown at a
compound annual growth rate of nearly 23 percent, compared with 10 percent
for the Dow Jones Industrial Average and 6 percent for the S&P 500.
We believe the best is yet to come, as we took significant steps in 2013 to
continue to reimagine and redefine the drugstore experience in America 
and the world.
The humble Chicago corner drugstore that opened in 1901 grew into
the nations largest retail pharmacy chain through unmatched customer
service, relentless innovation, and a nationwide store network on the
best corners in America. Today, we are establishing the first global
pharmacy-led health, beauty and well-being enterprise, seizing opportunities
to lead the market in the decades ahead.
As we think globally and long term, Walgreens is keenly focused locally
every day on the customers and patients in communities across America
who count on us to meet their health and daily living needs. Many families
are still struggling with the weak jobs market. The healthcare system we
rely on is undergoing rapid and fundamental change. While the national
Affordable Care Act is expanding coverage, the cost of care is rising and
demand is challenging supply, affecting peoples financial security and
stressing providers.
While our economy and healthcare system remain challenging, Walgreens
took decisive steps this year to boost our daily living sales and prescription
volumes. Thanks to the focus, determination and spirit of service by
our 248,000 team members, we continue to transform todays challenges
into opportunities to deliver exceptional value  every day, and for
the long term.
Strategic Growth Drivers Move Forward
The Walgreens strategic transformation, now five years strong, is driven
by our purpose  to help people get, stay and live well  and our vision,
to be the first choice for health and daily living for everyone in America
and beyond. To achieve our purpose and vision, we are focused on three
strategic growth drivers:
 Create a Well Experience by transforming the customer experience
across all touch points, channels and formats.
 Transform the role of community pharmacy by offering unparalleled
access to innovative, high-quality, affordable health and wellness services
within our communities.
 Establish an efficient global platform to create the first global
pharmacy-led health and well-being enterprise with our strategic
partnership with Alliance Boots, the leading international pharmacy-led
health and beauty group, and our long-term strategic relationship with
AmerisourceBergen, one of North Americas largest pharmaceutical
services companies.
We are pleased to update you on our progress this year in advancing these
strategic growth drivers.
Creating a Well Experience
Walgreens is embracing todays consumers to become their first choice in
health and daily living. Mindful of the Great Recession and recognizing
their unlimited internet options, people are expecting maximum value 
defined by convenience and quality as well as price.
Consumers today are driving the explosive growth of digital and
mobile technologies  by 2015, more than 50 percent of U.S. retail sales
are projected to be web-influenced. Retail channels  store, online,
mobile  are blurring, and consumers today are shopping wherever and
however they can to get what they want

Our strategy of Creating a Well Experience is to build on our trusted
and historic brand, and our 8,100-plus store network, to step out of the
traditional drugstore format to create a truly differentiated experience
for consumers and patients who want more from their stores.
Well Experience starts with leading-edge store design, new layouts,
enhanced product assortments and state-of-the-art pharmacies, including
bringing the pharmacist out from behind the counter for greater interaction
with patients. More than enhancing the physical store, Well Experience
also means a highly engaged employee delivering a unique and happy
customer experience. To enhance that experience, this year we
launched a concerted employee training program to ensure a winning
company culture with Well-inspired people.
Well Experience also offers best-in-class omni-channel access 
harnessing digital, web and mobile applications  to give shoppers the
three Ws: What they want. Where they want it. When they want it.
Our newest omni-channel options allow Walgreens smartphone app
users to now print from Instagram, and our app is compatible with
Windows Phone 8.
Altogether this year, we expanded the overall number of Walgreens stores
incorporating our Well Experience concepts from 400 to more than 500.
Boosting the Well Experience across Walgreens, the Balance Rewards loyalty
program has generated extraordinary customer response. More than
83 million members have enrolled in Balance Rewards in less than a year
since introduction, making ours one of the most successful launches of
a loyalty program in the history of retail.
Millions of Balance Rewards members already have collected enough
points to treat themselves to free rewards. Nearly 80 percent of
members surveyed have told us they are delighted with the simplicity
of the program, how easy it is to sign up and use  in stores, online
or with their mobile phones.
Balance Rewards delivers more than value to customers by giving them
money-saving points on their purchases and for participating in healthrelated
activities. The program also provides immeasurable value to
Walgreens by giving us exceptional insight into customer shopping habits
and preferences  information we are putting to work to enhance pricing,
promotions, product assortments and services, and to tailor our daily living
strategies to meet individual community needs.
Rounding out the Well Experience, we are also pleased with the strong
momentum of our private brands, including Walgreens, Good & Delish,
Nice!, and many more. The companys investments in these and other
private brand lines increased private brand penetration to 22.4 percent
in fiscal 2013, up 1.8 percentage points from 20.6 percent in fiscal 2012.
We also continued to magnify and test our Well Experience concepts
with our new, out-of-the-box flagship stores in key locations across the
country. These 20,000-plus square-foot stores, with two or even three
expansive floors, surprise many Walgreens customers with an extensive
collection of innovative offerings, products and services unexpected from
a drugstore such as sushi bars, juice and smoothie bars, cafes, fresh food
and on-the-go meals, and expanded beauty departments and services
including boutiques and manicure stations. This year we opened new
flagship stores in several iconic locations: The Empire State Building
in Manhattan, the historic corner of Sunset and Vine in Hollywood,
California, and in locations in Boston, Chicago, San Francisco,
Washington, D.C., and Philadelphia  for a total of 12 so far.
We are pleased with the performance of our Well Experience stores.
Customer delight, a metric we use to gauge satisfaction, has surpassed
our expectations and goals in monthly surveys. People agree  This is
not your grandfathers Walgreens.
Transforming Community Pharmacy
As we embrace and delight todays consumer, Walgreens is also pioneering a
far greater role for community pharmacy  seizing new opportunities to serve
the healthcare system as the nations needs continue to grow and change.
This past year, Walgreens forged multiyear contracts with predictable
reimbursement rates with the major commercial payers, bringing greater
stability and certainty to our pharmacy business. Walgreens also began
participating as part of the preferred pharmacy network of three of the
top national Medicare Part D plans, giving us a leading role in serving
older Americans.
As our nation ages  about 10,000 people in the U.S. turn 65 every day 
healthcare spending continues growing. For older Americans, 13 percent
of their total living expenses go to healthcare, roughly twice the share for
consumers on the whole, and the need for chronic care is rising.
An aging population demands more pharmacy services. Today, 17 percent
of healthcare spending by older Americans is for medication, and they
fill an average of 30 prescriptions a year. With healthcare spending
estimated to rise by 5 percent a year, drug spending is expected to grow
by roughly $3545 billion by 2016, according to the IMS Institute for
Healthcare Informatics. With this growth, Forbes magazine recently
named the drugstore business as one of four healthcare businesses
that will see tailwinds in the next four years.
Healthcare reform is further driving the demand for affordable, high-quality
healthcare and is estimated to provide another 30 million people with
access to health insurance coverage.

The pharmacy profession has always played a key role in Americas healthcare
system. In the future, as pharmacists and nurse practitioners practice
at the top of their professions, together they can help fill the gap in primary
care, expand health and wellness, and lower overall healthcare costs.
A new IMS study in June supports this point. Improper and unnecessary
use of medicines, it found, increased healthcare costs by more than
$200 billion in 2012, about 8 percent of the nations total healthcare
spending that year. The study singled out pharmacists as particularly
well positioned to address this crisis by helping patients through frequent
and direct communication  about responsible use of medicines.
More broadly, the study found Pharmacies are capable of, and have been
taking on, greater responsibility to provide more primary care services.
We agree wholeheartedly, and Walgreens is well positioned to serve the
growing demand for pharmacy-led health and well-being services through
our strategy to transform community pharmacy and its three main goals:
First, to deliver comprehensive care across our strong base of healthcare
assets, including more than 70,000 healthcare service providers at
our 8,100-plus store locations, nearly 400 Healthcare Clinics staffed
by nurse practitioners and physicians assistants co-located with our
pharmacies, 370 workplace health centers on major employer campuses
and 200 health system pharmacies.
Second, to offer an easy, expert experience for patients, healthcare providers
and payers that differentiates Walgreens from its competitors.
Third, to be the strategic partner of choice  seeking targeted partnerships
and pursuing joint initiatives with healthcare providers and payers.
This year, for example, Walgreens formed Accountable Care Organizations
with three leading physician groups in Texas, Florida and New Jersey.
Walgreens is pursuing these goals through our core pharmacy business
(providing traditional medication through our retail stores and mail),
and by expanding into two fast-growing healthcare segments: first, in
specialty pharmacy, serving people with complex, chronic diseases including
HIV/hepatitis C, rheumatoid arthritis, multiple sclerosis and cystic fibrosis;
second, in health and wellness services, including meeting new nonemergency
and outpatient care needs arising from the national Affordable Care Act.
Walgreens is becoming a key part of the community healthcare delivery
team, supporting primary care physicians, health plans and health systems
to address patient needs and gaps in care.
For example, our in-store Healthcare Clinics are further expanding their
health service offerings well beyond minor and episodic care. This year we
introduced assessment, treatment and management for chronic conditions
such as hypertension, diabetes, high cholesterol, asthma and others,
as well as additional preventive health services. We have a long-term
partnership with Theranos, Inc., to provide innovative lab testing services
in Walgreens pharmacies.
As our Well Experience strategy moves pharmacists out from behind the
counter to better counsel patients on the proper use of medication and to
expand our scope of services, we are enabling our healthcare professionals
to provide even more advanced care through store design, automation,
and healthcare information technology. Harnessing technology tools such
as electronic medical records, shared billing and scheduling platforms and
integrated data, Walgreens can provide a holistic view of each patient for
health plans, physicians and other health system partners.
With our expanding healthcare assets and innovations, Walgreens is fast
becoming part of the solution to the nations changing healthcare system
and needs. We are meeting what has been described as healthcares
triple aim: Improving patient experience, driving better health
outcomes, and lowering overall healthcare costs.
Already, Walgreens is the long-standing private sector leader in providing
vaccinations, administering a total of 8.5 million this year alone.
As we provide convenient access to this preventive healthcare measure,
our immunization program demonstrates the growing patient confidence
in pharmacists as an important member of their healthcare team.
We will continue to advance our leadership.
Walgreens healthcare innovations have not gone unnoticed. For the third
time in four years, Fast Company magazine elected Walgreens as one of
the healthcare industrys most innovative companies for leadership and
innovation in healthcare services  from our ongoing transformation of
community pharmacy highlighted by our Well Experience store format 
to our combination of health and technology through development of
award-winning mobile applications and web tools to help people manage
their health  to our public-private initiatives with the U.S. Centers for
Disease Control and Prevention, U.S. Department of Health and Human
Services and others to improve access to healthcare services, while creating
a new standard for the patient-pharmacist relationship.
And we are only getting started.
Establishing an Efficient Global Platform
As we create a Well Experience and transform community pharmacy,
Walgreens  together with Alliance Boots and AmerisourceBergen 
is elevating these growth strategies across the United States and beyond.
A world of opportunities is waiting as the global market is also a growing
market for healthcare solutions. Today, nearly a billion people live in
the U.S. and Europe and by 2020, that number could reach 1.1 billion.
Like America, the global market is also an aging market with increased
healthcare spending  5 percent annual growth in Europe alone.
Growth opportunities range from expanding consumer choices of the
worlds best products to making supply chains and delivery systems more
efficient and effective, expanding access and lowering costs.
To seize these opportunities, we reached across the Atlantic and forged
a strategic partnership with Alliance Boots. A bit about Alliance Boots:
In 2006, the Boots drugstore chain known across London merged
with Alliance Healthcare to become the worlds leading integrated
wholesale-retail pharmacy provider. The Walgreens-Alliance Boots
partnership brought the best of two iconic brands together to further
advance the retail drugstore concept in America and across the world.

Last spring, Walgreens and Alliance Boots announced a strategic,
long-term relationship with the largest pharmaceutical supply chain
company in America, AmerisourceBergen, to form a collaborative
wholesale-retail model here in the United States. The 10-year strategic
long-term contract, which will distribute brand and generic
pharmaceuticals to more than 8,100 Walgreens locations across
the country, officially launched in September.
This relationship will enable Walgreens, Alliance Boots and AmerisourceBergen
to benefit from greater scale and global opportunities, and work together
on programs to improve service levels and efficiencies, while reducing costs
and increasing patient access to pharmaceuticals. Together, our three
companies will help address global healthcare challenges by making it easier
for manufacturers to bring products to market, increasing accessibility to the
benefits of global sourcing and best practices for community pharmacies,
and providing patients with better access to healthcare. The collaboration
will also generate opportunities to attract partners in new markets and
prospects in existing markets around the globe.
With Alliance Boots and AmerisourceBergen, we are positioned to become
the leading pharmacy-led health and well-being enterprise in the world,
with 11,000 drugstores and 370 wholesale distribution centers, with a
presence in 25 countries, with a pharmaceutical supply chain unmatched
in the world (including associates and joint ventures).
Walgreens is already bringing the best of Boots to communities across
America. This year, we launched the Boots No7 Men skincare product line
following the successful introduction of the Boots No7 womens skincare
line and other Boots products in four of our flagship locations. The debut
of Boots No7 and Mark Hill Salon Professional products to U.S. markets
is just a hint of whats to come.
The Walgreens-Alliance Boots strategic partnership also provides significant
value creation for shareholders. When we announced our partnership, we
set a target to achieve combined synergies of $100150 million in the first
year. We exceeded that target. The Alliance Boots and AmerisourceBergen
partnerships represent a significant strategic step forward for Walgreens,
today and tomorrow.
Looking back, 2013 was a year of advancing our near-term performance,
long-term strategic transformation, and profitable growth. We think this
is an exciting time to be part of the Walgreens family as we build on our
heritage of service and innovation for a new century around the globe.
Advancing Walgreens Corporate Citizenship
Meanwhile, in the past year, our company and people also continued
to advance the Walgreens heritage as a solid corporate citizen in four
distinct areas:
 Healthy company: The National Business Group on Health, a nonprofit
association of large U.S. employers, honored Walgreens for its
ongoing commitment to promoting a healthy workplace and encouraging
workers and families to pursue and maintain healthy lifestyles.
 Healthy people: Walgreens and the National Urban League continued
our joint Way to Well health tour, providing free health tests, assessments,
education and consulting services to residents in urban and minority
communities who experience disproportionately higher rates of preventable
disease. Walgreens and AARP also collaborated on a Way to Well health
tour. We also joined with Greater Than AIDS, health departments and
local AIDS service organizations to provide free HIV testing in support
of National HIV testing day. The more than $8.5 million raised by
Walgreens customers to help pay for treatment support and screenings
for breast cancer patients earned our company the Susan G. Komen
Foundations prestigious Mission Possible award.
 Healthy communities: After tornados devastated parts of Oklahoma,
our teams mobilized additional shipments of essential supplies to area
stores, made an initial donation of three semitrailers of food, water and first
aid supplies to the Oklahoma chapter of the American Red Cross and
offered customers an opportunity to make donations at its Oklahoma,
Arkansas and Missouri drugstores. Walgreens also donated more than
$350,000 to Hurricane Sandy relief efforts in New York and New Jersey.
 Healthy world: Walgreens is partnering with the United Nations
Foundation this coming flu season to help provide up to three million
life-saving vaccines to children in developing countries through a
donation to the Foundations Shot@Life campaign. On the environmental
front, Walgreens is building the nations first net zero retail store in
Evanston, Illinois  a building that is designed to produce more energy than
it uses  harnessing alternative energy equipment from solar panels,
wind turbines and a geothermal energy system below the stores foundation.
This is not a showcase project  were investing in alternative energy
strategies across our stores nationwide as part of our overall sustainability
plan to reduce energy use (and costs!) by 20 percent by 2020.
Looking Ahead  With Appreciation
Walgreens continues to advance our strategic transformation, driven by a
bold and  we believe  game-changing vision for community pharmacy
in America and beyond.
With our strategic foundation in place, managements focus in fiscal 2014
will be on the disciplined execution of our long-term strategic vision and
day-to-day business  all to continue delivering double-digit growth in
earnings per share, increasing return on invested capital, and providing
top-tier shareholder returns.
To share credit where due, we want to pay tribute to our 248,000 team
members in communities across the country  people who come to work
every day, night and weekend, believe in Walgreens vision and purpose,
cherish the chance to serve our customers and patients, execute with
thought, pride and precision, and lead and inspire the company at every
level. Every day we see them living our cultural beliefs: Be one, be real,
be bold, build trust, love customers, own it, live it. Every day, these
incredible people turn change and challenge into opportunity and value.
We are so proud of them and what they bring.
Thanks also to our Board of Directors, which has been steadfast in
its support and guidance as we further advance and accelerate the
Walgreens transformation.
Most of all, it is you  our shareholders and owners  who deserve the
lions share of appreciation for believing in Walgreens today and our
limitless promise for tomorrow. We look forward to moving ahead together
as we redefine Walgreens for a new generation as a place, in your
communities, that always strives to help people get, stay and live well.
James A. Skinner Gregory D. Wasson
Chairman of the Board President and Chief Executive Officer
October 18, 2013