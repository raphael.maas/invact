Dear Shareholders,
At Incyte, we believe that innovation and the discovery of new products
creates long-term value for patients and society, as well as for our employees and our shareholders. It is our commitment to these objectives that has
enabled us to make significant progress in the last year. During 2016, we
saw continued growth in the number of patients being treated with Jakafi�
(ruxolitinib), our JAK1/JAK2 inhibitor, and we also added Iclusig� (ponatinib) to our commercial portfolio as part of our European transaction with
ARIAD Pharmaceuticals, Inc. In February 2017, with Eli Lilly & Company, we
announced the European approval of Olumiant� (baricitinib).
I believe that we are on
track to reach our goal of
becoming a world-class,
global biopharmaceutical
organization. For the first
time in the history of our
company, Incyte�s total
yearly revenue surpassed
$1 billion in 2016. Our revenue growth this past year was largely fueled by 

Jakafi sales in the U.S.
which, in turn, was driven
in large part by the growing base of MF and PV
patients that are benefitting from this treatment.
The revenue generated
by our products allows us
to continue to reinvest in
our portfolio � we expect
that the investments we
are making today will
position us for long-term
success.
We have a broad and diversified selection of clinical candidates in our growing portfolio, the majority
of which were created in our own laboratories in Wilmington, DE, which is
further testament to our commitment to innovation.
We believe that our portfolio contains both first-in-class and best-in-class
candidates and that it is both unique and unparalleled for a company of
our size. It has also grown considerably in 2016. In last year�s Letter to
Shareholders and Annual Report, we detailed 14 clinical candidates, more
than half of which have since moved forward into the next stage of clinical
development. We have also added four new clinical candidates resulting
from our internal discovery efforts as well as licensing transactions in the
last twelve months. I�ll now detail some of the major advances we have
made and are planning across our portfolio.
Jakafi, first approved in 2011, has over a decade of expected patent protection remaining in the U.S. and we plan to use this opportunity to
investigate its potential utility in other disease areas. These plans include a
pivotal program in patients with steroid-refractory graft versus host disease
(GVHD), which is underway, and a pivotal program in patients with essential
thrombocythemia, which is planned to begin later this year.
Following positive proof-of-concept data published late last year, itacitinib,
our selective JAK1 inhibitor, is expected to begin a pivotal program in
patients with treatment-na�ve acute GVHD this year.
We believe that IDO1 enzyme inhibition could represent an exciting new
therapeutic combination option for patients with cancer. To that end,
our robust ECHO (Epacadostat Clinical development in Hematology and
Oncology) program is investigating epacadostat, our IDO1 enzyme inhibitor,
in combination with checkpoint inhibitors, vaccines, chemotherapy, and epigenetic therapies. ECHO-301, a Phase 3 trial of epacadostat in combination
with pembrolizumab in patients with unresectable or metastatic melanoma
is ongoing, and we look forward to sharing those data once available.
Earlier this year, we announced significant expansions of the ECHO program, and during 2017 we expect to initiate pivotal trials of epacadostat in
combination with pembrolizumab in four new tumor types and epacadostat
in combination with nivolumab in two tumor types. 

Over the last 12 months, we initiated four Phase 2 trials that, if successful,
may be registration-enabling. Our FGFR1/2/3 inhibitor, INCB54828, is being
studied across three different trials in patients with bladder cancer, cholangiocarcinoma, and 8p11 MPNs respectively. The fourth potentially-pivotal
Phase 2 trial is the CITADEL-202 study of our PI3kd inhibitor, INCB50465,
which recently began in patients with diffuse large B-cell lymphoma (DLBCL).
Our early-stage targeted portfolio also progressed well last year, including
the addition of a second BRD inhibitor, INCB57643, and our LSD1 inhibitor,
INCB59872, into clinical trials. We expect our FGFR4 inhibitor, INCB62079,
to enter clinical trials this year, and dose-escalation trials for our first BRD
inhibitor, INCB54329, and our PIM inhibitor, INCB53914, are ongoing.
Within our early-stage immuno-therapy portfolio, dose-escalation trials
of INCB01158, the arginase inhibitor we recently licensed from Calithera,
INCAGN1876, our anti-GITR agonist, and INCAGN1949, our anti-OX40
agonist, are all ongoing. We continue to perform a thorough assessment of
the profile of our PD-1 inhibitor, INCSHR1210, before determining whether
to enroll any additional subjects.
Our development program for a topical formulation of ruxolitinib continues
to progress and is currently being studied in patients with alopecia areata
and with atopic dermatitis. A trial in patients with vitiligo is expected to
begin in the coming months.
I�ll finish the portfolio review with an update on our partnered programs.
Baricitinib, marketed as Olumiant by Eli Lilly, was recently approved in
Europe for the treatment of patients with rheumatoid arthritis. In April
2017, the FDA issued a complete response letter (CRL) for baricitinib. In the
letter, the FDA indicated that additional clinical data are needed to determine the most appropriate doses as well as to further characterize safety
concerns across treatment arms. We, along with Lilly, disagree with the
agency�s conclusions and we currently expect that Lilly plans to now engage
with the FDA to discuss their concerns in an effort to determine a potential path forward. Capmatinib, the c-MET inhibitor which we licensed to
Novartis, is currently in a Phase 1b/2 trial in patients with lung cancer, data
from which are expected in 2017.
Since last year�s Annual Report we have announced three strategic transactions, which strengthened Incyte in important ways.
In December last year, we announced a strategic collaboration with Merus
NV, which provides us with long-term access to its leading bispecifics technology, Biclonics, for up to 11 programs.
Earlier this year, we announced a collaboration with Calithera Biosciences
that gave us exclusive development and commercialization rights to
INCB01158, the first-in-class, oral arginase inhibitor. We believe that arginase is an important target within the tumor microenvironment and could
have a role in combination with other immuno-oncology therapies in our
portfolio, including epacadostat.

The third transaction was the acquisition of ARIAD Pharmaceutical�s
European business which immediately expanded our footprint in Europe,
adding significant experience, resources and relationships to our existing
European organization. We are thrilled to have welcomed this group into
our Incyte family. The expansion and addition to our European team leaves
us well-positioned to maximize the potential of our broad development
portfolio.
With a strong financial position driven by expected revenues from Jakafi
and Iclusig and royalties from Jakavi and Olumiant, we will continue to
invest in the long-term success of Incyte. Looking forward, we have taken
the first steps towards expanding our company into the Asia-Pacific region,
with initial plans for a clinical development team in Japan. We are also
looking forward to moving into our newly expanded global headquarters
in Wilmington, DE.
I would like to close by thanking my Incyte colleagues, who, as we strive
to discover and develop innovative medicines for patients in need, are
our greatest asset. I would also like to sincerely thank and recognize the
patients, families, researchers, and physicians who participate in and help
to conduct our clinical trials. Together, through our shared focus on innovation and a dedication to scientific excellence, we will seek to transform the
future of cancer treatment.
Best regards,
Herv� Hoppenot
Chairman, President and CEO