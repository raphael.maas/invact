We are pleased to report another excellent year of
performance and strategy as we grow our highly diversified
Company that is positioned to remain a leader in seniors
housing and healthcare real estate. While delivering
shareholder return of 12 percent, Ventas also evolved
dramatically in 2007 with our major Sunrise REIT acquisition
as we continue to build a sustainable, profitable enterprise.
As promised, we have succeeded in significantly diversifying
our revenue streams into a desirable mix of high-quality
tenants, multiple private pay and government reimbursed
asset classes, and different geographies. And we have
given our business further dimension and growth potential
by adding operating businesses in seniors housing and
medical office buildings (MOBs) to our stable, growing
base of triple-net leased assets.
Our success in 2007 derives from the same attributes
that have helped us build a leading enterprise:
 Disciplined diversification of our assets and tenants
 Cash flow growth
 Balance sheet strength and flexibility
 Systematic, deliberate reduction of enterprise risk
 Integrity, consistency and commitment from
our management team
 Focus on stakeholders
The rigorous execution of our strategy has enabled
us to continue to outperform and deliver superior
risk-adjusted returns in a variety of economic, capital
market and reimbursement cycles.
Sunrise Senior Living REIT  A Defining Moment
The highlight of 2007 was our successful $2 billion
acquisition of 79 high-quality private pay independent
and assisted living assets in the United States and
Canada from Sunrise Senior Living REIT. From the start 
when we announced the transaction in January of
2007  we believed that Sunrise REIT would be an
extraordinary acquisition that would improve the overall
quality of our portfolio, increase our private pay revenue
base, extend our diversification strategy into international
markets and provide us with the potential for higher
future growth. And that has been the case.
The unique structure of the Sunrise REIT acquisition enables
Ventas to receive the direct benefit of the net operating
income (NOI) from our communities rather than a fixed
lease payment from a corporate tenant. These cash flows
are paid directly to Ventas by 7,000 seniors, providing
apartment-like diversification  and healthy growth
potential  to Ventass revenue base.
In the last year, we have successfully integrated into our
portfolio these excellent communities, which are managed
by Sunrise Senior Living, Inc. (NYSE:SRZ) (Sunrise). They
are distinguished by their award-winning mansion
architectural design, locations in major metropolitan
markets with excellent age and income characteristics
and significant barriers to entry, and delivery of a quality
living experience to seniors who require assistance with
tasks of daily living.
Our Sunrise mansions have produced the NOI and
occupancy levels we had predicted, and importantly,
by year-end, our private pay revenues accounted for
69 percent of our annualized revenues. Adding these
properties to our already highly productive portfolio has
elevated Ventas into new territory, making us a better,
stronger company.
The Sunrise REIT acquisition also carried us into international
markets, with 12 independent and assisted living assets
in Ontario and British Columbia. We like the Canadian
seniors housing market because its economy has different
drivers than ours, it has the biggest baby boom population
in the world, and the private pay seniors housing model
is gaining rapid acceptance by consumers. As a result,
we see ample prospects for significant growth in the
Canadian seniors housing market.
A final important growth component of the Sunrise REIT
acquisition is our exclusive option to acquire brand new
communities developed by Sunrise throughout Canada
and in defined geographical areas surrounding our
domestic Sunrise communities.
The Sunrise REIT acquisition  from start to finish  is a
classic example of the way Ventas operates. We articulated
our goals, identified a transaction to achieve those
goals, worked creatively and
tirelessly with the best advisors
and internal talent to design
a structure that enabled us
to seize the opportunity,
underwrote the cash flows
carefully, outlined the risks and
rewards to our stakeholders,
protected our right to complete
the Sunrise REIT acquisition,
worked closely with our Board
to maintain discipline, and
executed with singular focus.
Another part of our corporate
DNA is to acknowledge and
protect the downside. While
completing the Sunrise REIT
acquisition, we saw reduced
liquidity and increasing cost
of funds affecting the debt market. Acting quickly,
we completed the largest REIT follow-on equity offering
in history, raising $1.1 billion within weeks after closing
the Sunrise REIT transaction. We used the proceeds to repay
all of the acquisitions short-term financing with permanent
capital. Our timing was prescient. We weathered the
summers rapidly deteriorating equity and credit markets
from a position of strength. Moreover, we funded the
balance of our 2007 acquisitions through the opportunistic
sale of assets owned by our tenant Kindred Healthcare,
Inc. (NYSE:KND) (Kindred) at attractive rates, further
protecting our balance sheet and eliminating any need to
tap the capital markets during a particularly volatile period.
Long-Term, Consistent Excellence
At Ventas, we want to end every year as a better company
than we were at the beginning and create value for our
shareholders. We invest in the future, but also realize that
our stakeholders are entitled to current performance too,
so we try to make decisions that achieve both objectives.
Despite volatile and difficult market conditions in 2007,
we delivered 12 percent total return to shareholders,
versus a NEGATIVE 17 percent for REITs as a whole.
Our normalized 2007 Funds from Operations (FFO)
per share grew 10 percent to $2.69, and our dividend
increased 20 percent to $1.90 per share.
Our job is to deliver consistent excellent performance to
our stakeholders, and indeed we have. During EACH of
the last one-year, three-year, five-year and ten-year periods,
we were among the top 5 performing REITs within the
overall REIT universe of about 100 companies. We are
the only REIT to achieve that distinction.
Our strategy remains to create an excellent company that
owns a high-quality portfolio of diversified seniors housing
and healthcare assets that will deliver superior, consistent
cash flow growth, operate in multiple segments and
evidence lower overall risk. We want Ventas to be a
sustainable, profitable institution that is more than the
sum of our assets or our deals and that does not depend
on any one individual for success.
We believe in the size, strength and potential for growth
in the healthcare and seniors housing sector of real
estate. This is particularly true as healthcare REITs use
their increasing scale, industry expertise, contacts and
sophistication to expand their platforms, differentiate
strategies and tap into this important part of our economy
and daily lives.
The demand for healthcare and seniors housing is growing
inexorably, as healthcare expenditures trend to 20 percent
of gross domestic product (GDP). This powerful macro
trend should follow from the explosion in the over-75
population coupled with the leading edge of the baby
boomers turning 65 and becoming Medicare-eligible in
2011. Most of our assets are need-based, with relatively
inelastic demand, making them less subject to recessionary
influences. In other assets, such as private pay independent
living, market penetration is still quite low, and should
expand as more seniors and their families acknowledge
the benefits of communal senior living.
Focus on MOB Strategy
An important component of our growth and diversification
strategy will be to add more MOBs to our portfolio.
The MOB business is characterized by predictable, stable
rents, high levels of tenant retention, and increasing
demand. With over $150 million in investments, we made
significant and steady progress with our MOB acquisition
strategy in 2007. We now own over 1 million square
feet of MOBs.
We have also established important partnerships with
several MOB regional developer managers to position
ourselves for proprietary deal flow and development
opportunities. We entered 2008 with a good pipeline
and excellent opportunities in the MOB space.
We favor the MOB asset class because it has excellent
fundamentals. MOBs cater to aging baby boomers who,
as they reach 65, will confront more health issues, will
increasingly see their physicians and will become Medicare
eligible, which means doctor visits are government paid.
Also, federal healthcare policy favors delivery of healthcare
in the lowest cost, most clinically appropriate setting.
Because of notable improvements in technology, less
invasive surgical procedures and other advances, patient
care that previously was provided within a hospital now
frequently occurs in an MOB.
Because the MOB market is huge  estimates run as
high as $250 billion  and highly fragmented, we see
considerable opportunities for growth.
Most MOBs are owned by for-profit and not-for-profit
hospital systems, which are beginning to see the benefit
of monetizing their buildings to meet extensive capital
expenditure needs and free up cash to use in more
productive and strategic ways. They can redeploy the
proceeds from a sale of their MOBs to purchase new
technology, add hospital bed capacity, maintain existing
facilities in excellent shape, or even pay down debt.
Increasingly sophisticated hospital finance executives do
not want to tie up their balance sheet with real estate
assets, and recognize that REITs are more efficient holders
of MOBs. This trend should accelerate the sale of MOBs
from hospital systems, and we intend to play a role in
this transfer of assets.
Finally, hospitals are incented to eliminate management
and control of MOBs due to restrictions imposed by
federal laws. These rules are designed to insure that
interactions between hospitals and physicians occur at
arms length independent market terms. Hospitals know
that they eliminate regulatory exposure when a bona fide
third party, like Ventas, owns and operates their MOBs
and handles all real estate dealings with their physicians.
We are committed to building an MOB business that is
well-positioned to capitalize on these long-term trends, as
hospital systems accelerate divestiture of their MOB assets.
Ratings Upgrade and Dividend Increase
2008 started with exceptionally positive news when
Standard & Poors credit rating agency gave us our second
investment grade rating (BBB-), which should reduce our
borrowing costs, an essential ingredient for future success.
Even more essential: investment grade rated companies
should have access to capital even in a dislocated credit
market. We believe access to credit will be a defining
issue in 2008.
We recognized during the recent years of excess liquidity
and easy credit that the debt markets would reverse.
Rather than just enjoy the good times, we were disciplined
and made conscious decisions to position ourselves to be
strong when credit tightened. The benefits of our actions
are already apparent in the current credit crunch.
We also started 2008 with a decision to share our success
with our shareholders by increasing our dividend by
8 percent to an annualized rate of $2.05 per share. We
have continued to maintain a conservative payout ratio
even as our dividend growth remains above average.
This makes your dividend more secure and positions us
for continued dividend growth.
2008  Looking Ahead
Our 2008 outlook is cautious but optimistic. We are
focused on liquidity, safety and opportunity, in that
order. Many observers believe that real estate is in a period
of pricing reevaluation and that the housing correction is
spreading to commercial real estate. Healthcare real estate
values have held up well, supported by excellent fundamentals
and cash flow yields that remain more attractive
than those available from other real estate sectors.
I am convinced there is no better sector in real estate
than seniors housing and healthcare. The benefits of our
business  driven by the aging population, increasing
market acceptance of seniors housing, and the growing
needs for healthcare  should outweigh the negative macro
economic and market trends affecting asset valuations,
demand and financing costs. I expect our cash flows to
continue to grow during 2008 due to our business model
founded on long-term triple-net leases, the recessionresistant
character of our sector, and the reliability of
our operating cash flows.
The more sobering issue is the debt market dislocation
that began in 2007 and has deepened in 2008. We do
not believe normalcy will return to the credit markets for
some time. We expect waves of write-downs and distress
to continue as massive de-leveraging takes place. This
process will feed on itself and limit the willingness and
ability of traditional institutions to lend until they sell off
the debt they hold on their balance sheets and see a floor
in debt valuations.
Consequently, our highest priority is to remain liquid,
financially strong and flexible. To execute on this priority,
we raised $200 million in permanent equity capital during
the first quarter of 2008 and also increased our debt
capacity by expanding our revolving credit facilities to
$850 million from $600 million. At this time, we have
substantially all of that credit capacity available to us and
manageable forward debt maturities.
While we remain cautious, we also intend to execute
our business plan in a measured and deliberate way.
The platform we have built should give us a competitive
advantage in growing our business and increasing our cash
flow through new investments. Additionally, our strong
balance sheet and investment grade ratings should allow
us to raise capital at acceptable rates and we have built
significant liquidity to pursue appropriate opportunities.
We also expect to see a less competitive market for
investments as highly leveraged buyers are sidelined by
the unavailability of debt financing.
The current environment should provide us with excellent
opportunities to make outsized risk-adjusted returns for
our shareholders. The fundamental challenge in 2008
will be to make the right decisions about what to
buy, when to buy and what to pay, in a context where
there is little guidance on pricing and no visibility
on future conditions.
In all events, we intend to remain a leader in healthcare
real estate.
Relentless and Committed
Every year I am amazed at how hard the Ventas team
works, and last year was extraordinary in that respect.
I was privileged in 2007 to lead a group of skilled,
focused and collaborative colleagues.
Our Companys achievements reflect nothing if not the
Ventas teams uncompromising and unified efforts to do
our collective best for our shareholders. No one could
wish for better partners than those I have at Ventas,
including Ray Lewis, Rick Riney, Rick Schweinhart, Brian
Wood and Tim Doman, all of whom have been together
for over five years. We have a team-based approach to
excellence throughout the organization, and we operate
as a cohesive group focused on building value for
our shareholders.
2007 was a significant year for expanding our infrastructure
and intellectual capital. Investments in our business were
necessary due to the acquisition of Sunrise REIT, expansion
of our MOB operating business, and our first international
investment. During the year, I was proud to announce
several important promotions and management additions
that enhance the hard work and excellent thinking that
have become synonymous with our management team.
Earning and keeping your trust has always been essential
to our management team. Through open and honest
communication, transparency and accountability, we hope
to maintain your confidence.
Recognizing our commitment to shareholders, Institutional
Shareholder Services (ISS) has awarded Ventas a 99th
percentile governance rating, putting us in the top 1 percent
of our peer companies for the 4th consecutive year.
Your independent Board of Directors has been a major
source of our growth and progress. Through their different
perspectives, support, guidance and experience, each
Board member contributes meaningfully to our strategy
and decisions. I seek their input, value their insights,
respect their hard work, and benefit from their insistence
that we constantly improve and stay ahead of the curve.
Especially in this challenging time, our shareholders should
take great comfort in the composition and engagement of
our Board. In early 2008, we were delighted to welcome
two well-regarded hospital executives to our Board of
Directors. James D. (Denny) Shelton and Robert D. Reed
will enhance our excellent Board with their industry
knowledge and experience.
I continue to learn and grow, and I am humbled by the
opportunities I have enjoyed since joining the Company
in 1999. Building a quality team and keeping everyone
working toward the same goals remains a source of great
personal and professional pride and satisfaction. I must
add that maintaining a candid dialogue with stakeholders,
holding ourselves accountable, and delivering exceptional
returns are truly their own reward.
We are cognizant that our excellent track record does
not assure continued success, and that current market
conditions or unexpected developments could interrupt
our forward progress. In 2008, we intend to be cohesive,
vigilant and vigorous to retain our edge.
I remain fundamentally optimistic about Ventass future and
prospects despite the strains evident in todays economy
and capital markets. We are well positioned  from an
asset quality, human capital and liquidity standpoint 
to thrive and take advantage of opportunities quickly
and decisively. With your trust and support, we will work
hard to continue our position as a leader in the REIT
industry, enhance the sustainability and excellence of our
enterprise, and execute on our strategic business goals
for your benefit.

Sincerely,
Debra A. Cafaro
Chairman, President and Chief Executive Officer