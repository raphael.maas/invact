Dear Fellow Shareholders: In this my final letter to you as President
and CEO of the XL Capital group of companies (XL), I want to share
my thoughts on the past year which has been a bitter sweet period
for the industry, for XL, and for me personally.
Before I do that, I would like to review the operational business
highlights of a year in which our Company and its employees
demonstrated remarkable resilience in an environment which
became increasingly challenging.
Performance Overview
Despite the collapse of the credit markets and the damaging repercussions
on balance sheets and share prices all over the world,
XL produced net income available to ordinary shareholders of $206
million. Our operating ROE for the year was 18.6%. Book value per
ordinary share was $50.30, and our Property and Casualty (P&C)
operations achieved a combined ratio of 88.8%.
In our insurance segment, gross premiums written for 2007 were $5.4
billion; net premiums earned were $4.1 billion; and the combined ratio
was 91.4%.
In our reinsurance segment, gross premiums written were $2.7
billion; net premiums earned were $2.3 billion; and the combined
ratio was 83.9%.
Our life reinsurance operations contributed $743 million in gross written
premiums and $701 million in net premiums earned for the year.
As for the environment, coming off a record year in 2006, the
(re) insurance industry generally began to experience softening rates
in some lines of business. As a result, there was some contraction in
the valuations of stocks for P&C insurance and reinsurance companies.
Also, while 2007 was a fairly benign year in terms of natural catastrophes,
insured losses for the industry were reportedly $9 billion
higher than in 2006.
In the midst of all of this, XL was enjoying consecutive profitable quarters,
strengthening our capital management through various security repurchasing
and issuing activities, and leveraging our global network to
make the most of business opportunities where and when appropriate.
Our Strategy
In May 2007, we announced that we planned to simplify our organization
structure and sharpen our focus on our core insurance and reinsurance
businesses which would provide greater operational flexibility
and allow us to reach our overall strategic objective which is to grow
book value per share at a faster rate than our peers over the cycle.
At that same time, we announced our intention to reduce our 63%
ownership in our financial guaranty affiliate Security Capital Assurance
Ltd (SCA), which was considered outside of our core business. The
following month we promptly executed on this reduction in ownership
to 46%; a level consistent with rating agency requirements. We also
announced that we planned to exit or sell some areas of business
which were not compatible with our core P&C business such as the
municipal guaranteed investment contract (Muni GIC) and funding
agreement businesses, which are largely credit spread activities and
have a high component of structured credit exposure.
Credit Crisis
Unfortunately, shortly after this the global credit crisis struck and
continues today.
Needless to say, the credit market situation has created testing times
for many financial institutions. This is especially true of companies with
large fixed income portfolios, like XL.
Unlike many of our peer companies, however, XL also has a 46% stake
in SCA. Along with most of its industry peers, SCA is facing market
uncertainty and the financial strength ratings of its subsidiaries, XL
Capital Assurance Inc. (XLCA), XLCA UK, and XL Financial Assurance
Ltd. (XLFA), have been downgraded by Fitch, Moodys, and Standard
& Poors. Accordingly, in the fourth quarter of 2007 we wrote down
the carrying value of our investment in SCA to zero.
As a result of losses and write downs associated with XL's investment
and other relationships with SCA, our financial operating affiliate
Primus Guaranty Ltd, and our investment portfolio, our fourth quarter
results were significantly impacted by credit market related charges
totaling approximately $1.7 billion. This included realized losses on
investments of approximately $500 million, the majority of which was
related to the Muni GIC and funding agreement businesses. The nature
of this business is a spread business which required investments
in higher yielding, lower credit quality securities which have been
severely impacted by the credit market crisis.
With respect to SCA, I can assure you that we have taken a measured
and analytical approach to our exposure to  and interest in 
SCA in order to best protect the economic interests of XL and you,
our shareholders.
Rating Actions
Unfortunately, the fallout from XLs fourth quarter charges was rating
downgrades by A.M. Best, Fitch, and Moodys. However, Standard &
Poors, who are familiar with the financial guaranty business, reaffirmed
our A+ or Strong rating and we are still rated A or Excellent by
A.M. Best, A+ or Strong by Fitch, and A1 or Good by Moodys.
All outlooks are stable. The downgrades were principally attributed
to earnings volatility, not capital adequacy.
We have taken the rating agencies views very seriously and are
working to address their concerns. However, we should not lose sight
of the strengths of XLs Property and Casualty and Life operations.
Had it not been for the charges we took in the fourth quarter related
to the credit market conditions, we would have achieved a record
net income of approximately $2 billion.
Even though our share price and the downgrading of our ratings have
been very disappointing, our capital position remains fundamentally
strong and our core businesses continue to perform well.
Segment Highlights
While gross premiums written were below prior year levels in most of
our insurance lines due to market conditions, there were some areas
of growth. These included Excess & Surplus and private company
Directors & Officers insurance.
We have also seen premium growth in European construction and
European Professional lines. And of course, our insurance segment
results do not include the results from our joint venture ItaXL which
we report in earnings from affiliates. XLs share of the joint venture
premiums were approximately $118 million for the full year.
We continue to build our insurance operations reputation for excellence
and customer service, leveraging the skills of our people.
For example, we are pleased that a recent survey by Flasphler
Research Group of approximately 340 U.S. risk managers rated us
among the very top of commercial insurance providers. This surveys
findings have been validated by our insurance customer retention
rates, which continue to be very satisfactory.
In 2007, customer retention ratios were in the high 80-to-90 percent
range for our Professional and Property and Casualty businesses.
We believe that our insurance business is well on its way to becoming
a leading global specialty and large commercial insurer. It already
has a highly efficient global distribution network which is rare among
its peers and difficult for a new entrant to copy. Additionally, we are
committed to strengthening our global insurance platform, particularly
in the U.S., as evidenced by many activities during the fourth quarter
of 2007, including: the acquisition of leading loss prevention consulting
service provider Global Asset Protection Services; and the launch of
the XL WorldPass policy, which enables customers to ensure that
they are regulatory and tax compliant in all the different countries
where XL Insurance companies issue policies for them. Earlier this
year, we also opened a new XL Insurance branch office in Singapore
to act as the management and underwriting hub for the Asian region.
In our reinsurance segment, market conditions, as indicated by
January 1st reinsurance renewals, saw mid to high single digit
reductions on a risk-adjusted basis across virtually our entire portfolio.
There were some exceptions in our catastrophe book which
included programs impacted by either the European windstorm Kyrill
or the U.K. floods which had renewal terms reflective of these claims.
We remain broadly pleased with our casualty portfolio, which has a
very attractive overall return profile.
Our reinsurance business remains among the top 12 in the world
in terms of gross written premium. The diversity of the reinsurance
products and the strategic geographic locations of our operations
provide XL with operating flexibility. Since taking the lead in establishing
our European platform in Dublin, XLs European reinsurance
operations are viewed as a credible alternative leader to Europes
much older reinsurers. Our reinsurance operations in emerging markets
such as Latin America and Asia are also doing well.
Our life reinsurance business continues to grow steadily. In the U.K.,
XL Re Life is a leader in the annuity business and is expanding its
presence in the critical illness, mortality, and term life business. It is also
seeking to expand in Continental Europe. In the U.S., the relatively
young operations are seeing an increasing number of submissions
and opportunities. However, like all of our businesses, the focus is
not on becoming the largest insurer or reinsurer, but on having the
strongest analytics, the most disciplined underwriting and providing
the best products and services to customers.
While our investment portfolio faced continued pressure due to the
credit market situation, our total net investment income was $2.2
billion for the year. Of this, net investment income from our primary
P&C operations, excluding structured products, was $1.3 billion for
the year, representing growth of 18% from 2006. This increase was
largely due to higher yields in early 2007.
Net income from investment fund affiliates was $326 million for the year,
driven by an exceptional 17% return for the year in our alternative
portfolio. We are pleased with the results, and this portfolio continues
to support our efforts to build book value. Net income from investment
manager affiliates for the full year was approximately $95 million.
Moving Forward
While XLs overall 2007 financial results are not what we hoped for, we
are satisfied that we have succeeded in reducing the uncertainty around
our exposures in the current credit market and we are moving forward.
We have been talking to you the shareholders, to our customers, to
brokers, and to the rating agencies. We realize that in order to regain
our ratings, we will be required to show solid consistent earnings
and risk management, which we will do by successfully executing
our business strategy.
Customers and brokers have already shown their support, indicating
that the downgrades do not change their desire to do business
with XL. They are supportive based on our superior claims-paying
performance and reputation, integrity of underwriting behavior, and
fundamental capital and balance sheet strength. Also, to date the
downgrades have not hurt our ability to attract new customers.
In fact, customers have told us that they are less concerned about
the rating changes because they know XL has the best claims-paying
record in the industry. They are more focused on XLs culture of
excellent claims-paying service and integrity  which Im sure that
my successor will continue to maintain.
Succession Planning
The Succession Committee of the Board of Directors expects
to choose my replacement by mid spring. I plan to help the next
President & CEO with a smooth transition and to, hopefully, take up
the role of Chairman of the Board.
As you know, last December Mike Esposito retired from the Board
in order to focus on his duties as Chairman of the SCA Board. As
usual, Mike was putting the interest of others ahead of his own.
As one who has had the privilege of working with him for the past 20
years, I can state without hesitation that Mike was committed to the
success of XL. Throughout his 21 years as a director, including the last
13 years as Chairman, Mike carried out his duties with unparalleled
energy and passion. We are grateful for his contributions.
Joining the Board last November was Sir John Vereker, the former
Governor and Commander-in-Chief of Bermuda. Sir John has a
wealth of experience in international affairs.
In 2007, we also strengthened our executive management team
with the appointment of our Chief Financial Officer Brian Nocco.
Brian hit the ground running and has been instrumental in our
discussions with the rating agencies. Prior to his arrival, Fiona Luck
carried out the duties of CFO in addition to her responsibilities
as Chief of Staff. I would like to personally thank Fiona for doing an
outstanding job. During her time as interim CFO, Fiona led the
successful $1 billion preferred issue, a $325 million debt issue, the
renewal of our bank credit lines and our D&O renewal.
Earlier this year we announced that Clive Tobin planned to retire on
April 1st, 2008 as Chief Executive of Insurance Operations  a position
he has successfully occupied since 2004. Clive will concentrate on
assisting XL with strategic opportunities in emerging markets. His
successor David Duclos will have big shoes to fill, but I have every
confidence that he is up for the challenge.
In addition to these management changes, other executive management
appointments in 2007 and in the first quarter of 2008 have
included Kirstin Romann Gould to General Counsel and Secretary and
Celia Brown to Head of Global Human Resources and Corporate
Relations. David, Kirstin, and Celia are also members of the Executive
Management Board.
XL is fortunate to have so many talented employees who are intensely
committed to delivering excellence. This is one of the reasons why
I am confident that we will emerge stronger than ever from all the
adversity that we faced during the past six months.
We are focused on positive momentum generated by our insurance
and reinsurance operations over the last eight quarters. We remain
committed to our strategy of disciplined growth and diversification
in our insurance and reinsurance businesses and to delivering
a healthy ROE in excess of 15% in 2008.
Future Outlook
Although credit markets will likely remain challenging for the
foreseeable future, we anticipate that opportunities will arise from
the credit market turmoil, particularly for our investments team and
professional lines business.
While we intend to continue our disciplined focus on ROE rather
than top line growth and we will not compromise our underwriting
standards, we will, however, make the most of our presence in the
worlds major markets to target diversified growth, especially in our
insurance business, and to seek new business opportunities that
deliver value to you our investors.
We will also remain focused on our vision, which was updated in
2007 and inspired the theme of this report. The vision is to be the
leading global provider of intelligent risk solutions delivered with
integrity. It requires us to play to our strengths as technical underwriters
and providers of significant capacity, while maintaining high
standards of behavior underpinned by our core values of ethics,
teamwork, excellence, development and respect.
None of what has transpired during the past year has blurred that
vision. In fact, we are more determined than ever to achieve our
vision and our strategic objective of growing book value at a faster
rate than our peers over the cycle.
We are using XLs powerful dual platform  insurance and reinsurance
 as a competitive advantage. As one of just a handful of companies in
the market with the depth and breadth of experience to accomplish
it, this has become a key differentiator for us.
We are committed to enforcing strong risk management and a reliable
infrastructure. We are making significant progress in both areas.
Our Enterprise Risk Management Committee recorded achievements
on several fronts in 2007, including helping XL obtain from S&P an ERM
rating at the highest level of Adequate. We are also creating a senior
management position of Chief Enterprise Risk Officer to help the firm
manage risks on an aggregate basis across all of our businesses.
During 2007, we made significant investments in information technology
in both our insurance and reinsurance segments, continued
to invest in our core businesses through enhancement of various
business processes, and continued to build our support services
office in India. Each of these related initiatives requires considerable
investment and commitment of resources in an effort to achieve the
ultimate goals of increased operational efficiency and effectiveness.
In addition to this, we have made headway on another strategic
priority: product development in the insurance segment, our stated
area for growth. Over the past year we have not only rolled out new
products, we have also enhanced a number of existing products.
The decision to create a product development unit is a clear statement
of how seriously we take this. I believe we are right on track.
While XLs current share price is disappointing, as I have stated several
times throughout this letter and as you will see throughout this
report and in the year ahead, I believe XLs strength and ability to
compete in our chosen markets remains firmly intact, and XLs core
businesses continue to perform well. We have a great strategy,
a powerful team, and a solid capital foundation.
I am very much aware that XLs capital strength would not and could
not be fully possible without you  many of you have been with us
from the day that XL became a public company. I thank you for your
continued belief in XL.
Brian M. OHara
Acting Chairman and President
and Chief Executive Officer
