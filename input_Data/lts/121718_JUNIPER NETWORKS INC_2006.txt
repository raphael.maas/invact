To Our Valued Stockholders,
2006 was another year of focus and execution for Juniper Networks, as we continued to position the
Company for its next stage of evolution. Juniper finished the year with over $2.3 billion in revenue. To
put Junipers growth into perspective, this represents a revenue base that has nearly doubled over the past
three years. As we move into 2007, we see a marketplace that presents the Company with significant
opportunity to deliver strategic value to customers, scale our ability to execute, and continue to grow our
business. New demands on network infrastructure are rapidly rising, and Juniper remains at the forefront
of these trends as the vanguard of innovation and progress.
Our accomplishments in 2006 stem from the following strategic areas of focus:
1. Delivering best-in-class solutions that continue to progress with the changes in the marketplace.
Standalone products are increasingly giving way to more fully integrated solutions for the
enterprise market, and in February 2006, Juniper introduced the SSG platform, coupling the best
in security with the best in routing. For the service provider market, we saw traction with the
introduction of Ethernet line cards as well as the M120 Multiservice Edge Router, and we
launched the MX960 Ethernet Services Router.
2. Leveraging our technology for enterprises and service providers to further capitalize on the
synergies across these two markets. Juniper is currently well-positioned in both end-markets,
which are expected to have healthy spending levels in 2007. Over the past year, we have
continued to strengthen our product offerings for both enterprises and service providers, and our
concentrated approach ensures a secure delivery of virtual network services.
3. Increasing traction in the enterprise market with over 20,000 enterprise customers to date, and
an annual revenue growth rate in our Service Layer Technology segment of more than 20%
during 2006. As we approach our third year selling into the enterprise market, we continue to
expand our strategy with the evolution of the business model and leverage our best-in-class
technology. Juniper is setting the pace for the integration of security, routing and application
acceleration trends evident in the marketplace. We are pleased with our momentum with
enterprise customers as we placed second in market share for both high-end enterprise routing
and worldwide security, as reported by Infonetics in February, 2007.
4. Maintaining our focus on partner relationships to ensure the Companys ability to address the
changing needs of our customers. Our relationships with partners such as Siemens, Ericsson and
Alcatel-Lucent remained strong in 2006 as we continue to leverage our joint capabilities. We
have built partnerships with other market leaders including Symantec and Avaya as we are focus
on delivering best-in-class, integrated solutions to enterprise customers and we continue to look
for ongoing strategic collaborations that add value to our customers.
5. Striving to improve execution as well as the ongoing priority of always strengthening our
management team. A top priority at Juniper continues to be recruiting and retaining top notch
leaders who will excel in serving customers, increasing market share and strengthening revenues
and profitability. We are poised to take advantage of significant opportunities that lie ahead, and
our team is intensely focused on execution.
In 2006, the growing demand for our innovative products drove revenue over $2.3 billion, up 12%
compared to 2005. We ended the year with over $2.5 billion in cash and other investments on our balance
sheet, an increase of over 28% from 2005. During 2006, our board of directors approved a share
repurchase program of $1 billion and in early 2007 an additional $1 billion was approved.
In December 2006, Juniper announced the completion of the independent investigation related to the
Companys historical stock option granting practices. I am pleased to report that in March 2007 we
regained compliance with NASDAQ listing standards and our periodic reporting requirements with the
SEC, and we have taken the appropriate actions to verify that proper stock option accounting controls are
in place moving forward.
As we head into 2007, our strategy remains the same  to be the best supplier of traffic processing
infrastructure for the delivery of virtual network services. Concentrating on the service provider market,
we continue to see growth in IP traffic on service provider networks as a result of peer to peer interaction,
broadband usage, video, and the increasing reliance on the IP network as a mission critical business tool
in the strategies of our service provider customers, and of their enterprise customers. The quality and type
of traffic is continuing to change from generic delivery of information to high demand voice and video
where managing traffic characteristics like latency and quality of service become increasingly important
and play to the foundation and differentiation of what we can offer. The service providers network
requirements are becoming clearer and therefore the Juniper solution and the differentiation we provide
are becoming more apparent to the marketplace and to our customers.
As our enterprise strategy progresses, we remain confident in our ability to help enterprises enhance the
performance of their networks, mitigate exposure from risk and gain greater choice through open
standards and partnerships so that they can ultimately realize enhanced business performance. Juniper is
actively addressing the changing requirements of next generation data centers and is working to
accommodate the increasingly mobile employee with enhanced security, scalability, and application
performance. In conjunction with these efforts, we will also intensify a broadening of brand awareness in
the enterprise market by increasing visibility through more aggressive marketing and sales strategies.
As we look across our marketplaces in 2007, we see great opportunity. We have made solid progress in
2006, and we expect continuing evidence of that progress in the years to come. We will continue to
invest and to look outward to capture more of the considerable addressable market opportunity, and we
will also continue to look inward, to improve our execution, and invest in the talent we have assembled
and the technology that we have developed.
All of this is possible only with the support of our employees, whose continued commitment and
incredible efforts make these results possible, as well as our many partners, our customers, our suppliers,
and our long-term stockholders. I would like to thank you all for your continued support and confidence
in Juniper Networks.
Scott Kriens
Chairman and CEO