On behalf of the Board of 
Directors, it gives me great 
pleasure to share with you 
this fi
 rst Annual Report for 
the newly launched Alcoa 
Corporation. 
As a prior independent Director 
on the board of directors of 
Alcoa Inc., from which Alcoa 
Corporation was separated on 
November 1, 2016, I have a 
deep history with, and a great 
affection for, the businesses 
that became part of Alcoa 
Corporation. Therefore, it is my 
honor to now serve as Chairman of this values-based 
Company and pioneer of the aluminum industry.  
To ensure continuity and retention of valuable information 
and experience, Alcoa Corporations Board includes 
fi
 ve members of the former Alcoa Inc. board of directors, 
who now serve alongside new directors. Our board 
members have substantial leadership and management 
experience, deep knowledge of business operations 
and strategy, and valuable fi
 nancial, commodities, and 
investment expertise. Several of our board members 
have fi
 rst-hand experience with the issues global 
companies face, bringing an international perspective 
and insight into matters related to governments, 
energy and the environmentall invaluable assets as 
Alcoa operates as a globally responsible company. 
All of our board members are fully engaged and their 
diversity in experience brings richness to our Board 
deliberations. And importantly, each board member 
brings enthusiasm to their role. On behalf of the Alcoa 
Corporation Board of Directors, we are delighted to 
begin this journey with you, our stockholders, and the 
management team and employees, as an independent 
public company.
We are excited about the structure of this new company, 
which is incorporated in Delaware and features several 
governance best practices focused on the interests of 
our stockholders. For example, in addition to having an 
independent chairman and a separate chief executive 
offi
 cer, directors are elected annually and stockholders 
have proxy access to nominate director candidates. 
The management team is passionate about the 
aluminum business, and the Board and I have full 
confi
 dence in their ability to build upon the history of 
operational excellence. They are highly experienced 
with substantial hands-on industry expertise and a 
long track record of performance and execution. 
During his time at Alcoa Inc. since 2002, Chief 
Executive Offi
 cer Roy Harvey held a number of 
operational roles in Brazil, Spain and the United States; 
he also had responsibility for Human Resources and 
Environment, Health, Safety, and Sustainability before 
serving as President of the upstream business. Chief 
Financial Offi
 cer William Oplinger started working 
for Alcoa Inc. in 2000 and served as Chief Financial 
Offi
 cer and as Chief Operating Offi
 cer, responsible 
for the daily operations of the companys upstream 
assets worldwide, a role now held in Alcoa Corporation 
by Tmas Sigurdsson, who joined Alcoa Inc. in 
2004. Collectively, the senior management team has 
more than 110 years of experience in the mining, 
commodities and aluminum industries.
The team leading Alcoa Corporation has successfully 
steered through some of the most diffi
 cult market 
environments in the aluminum industrys recent 
history. Over the past few years, they executed a 
comprehensive strategy to strengthen the portfolio 
and secure the Companys position as a low-cost 
industry leader across the aluminum value chain. Alcoa 
Corporation is now a cost-competitive, large-scale 
industry leader in bauxite, alumina and aluminum 
products with excellent prospects for the future. With 
a strong tradition of product and process innovation, 
the management team will build upon the progress 
that fortifi
 ed its operations. The priorities are to reduce 
complexity, drive returns to create value for our 
stockholders, and strengthen the balance sheet. 
As Alcoa Corporation continues its story as a new 
publicly-traded company with a deep history, the 
Board and I will provide oversight and support the 
management team with an eye on managing risk and 
rewarding performance. And, as board members who 
are also stockholders, know that our interests and 
aspirations for the future success of Alcoa Corporation 
are also aligned with yours as Company stockholders. 
Thank you for your interest and investment in 
Alcoa Corporation. I now invite you to read Roys 
letter and review Alcoa Corporations inaugural 2016 
Annual Report.
Chairmans Letter
ALCOA ANNUAL REPORT 2016        BUILDING ON 128 YEARS: CHAIRMANS LETTER
02
Michael G. Morris
CHAIRMAN
OF
THE
BOARD
5
1
0
3
_
T
e
x
t
C
5
.
i
n
d
d
2
5103_TextC5.indd   2
3
/
1
6
/
1
7
6
:
3
5
P
M
3/16/17   6:35 PM
On November 1, 2016, Alcoa 
Corporation, a leader in 
bauxite, alumina and aluminum 
products, launched as a new, 
publicly-traded company. Yet 
Alcoa also has the benefi
 t of a 
128-year history dating back 
to its roots as the founder of 
the aluminum industry. Few 
management teams, if any, 
have ever had the privilege 
of leading a new company 
with such a rich heritage of 
innovation and operational 
excellence. That solid 
foundation, combined with 
the collective effort of more than 14,000 employees, has 
enabled us to become a strong standalone company, 
well-positioned for an exciting and successful future. 
Before we separated from our former parent company 
Alcoa Inc., our businesses faced signifi
 cant market 
pressures. Alumina prices were at multi-year lows and 
aluminum prices remained weak. Confronted with these 
conditions, and realizing the importance of launching a 
resilient company that could succeed through market 
cycles, Alcoans across the globe delivered approximately 
$290 million in after-tax, net performance improvements 
in 2016, signifi
 cantly exceeding our annual goal. We 
expect to build upon those savings in 2017. 
In 2016, in response to market conditions, we 
permanently closed our Warrick Operations smelter 
in Indiana and we curtailed our Point Comfort, Texas 
refi
 nery. In Suriname, we permanently closed the Suralco 
refi
 nery and bauxite mines, which had been fully curtailed 
since November of 2015. These decisions were not taken 
lightly, but were necessary to improve our portfolio.
During 2016, we also strengthened our company for 
the future. In bauxite, we grew our third-party exports 
to 6 million bone dry metric tons (bdmt) of bauxite 
shipped. In addition, our bauxite business secured its 
fi
 rst major export contract out of Western Australia, 
whose government approved Alcoas export of up 
to 2.5 million metric tons of bauxite per year for fi
 ve 
years to third-party customers. 
In the alumina and aluminum segments, we increased 
cost-effi
 ciencies and improved our positions on the 
alumina and aluminum global cost curves. Now, our 
alumina refi
 ning and aluminum smelting portfolios are 
better-situated to both benefi
 t from improving market 
conditions and withstand downturns. 
Recognizing that we have more to achieve, we entered 
2017 focused on three strategic priorities: reducing 
complexity, driving returns for stockholders, and 
strengthening the balance sheet. 
To reduce complexity, we are simplifying our processes 
and structure to make Alcoa more agile and lower-cost. 
For example, we recently combined our aluminum 
smelting, casting, and rolling segments into one 
Aluminum business unit and integrated the majority 
of the energy segment assets into it. 
To drive returns for our stockholders, we are investing 
modest capital in high growth projects with strong 
returns that will help strengthen the balance sheet. 
Since launching as an independent company, Alcoa has 
increased its cash position by $198 million and closed 
2016 with a cash balance of $853 million. Further, we 
will maintain cash optionality, carefully weighing how 
best to deploy cash and allocate capital. 
But both our past achievements and plans for the 
future would be meaningless if we succeeded without 
adhering to our strong values. Long an underpinning 
at Alcoa Inc., our values enable us to operate the right 
way. As we launched Alcoa Corporation, and in the 
spirit of reducing complexity, we distilled our values 
down to three simple principles: Act with Integrity, 
Operate with Excellence, and Care for People. These 
values resonate with our employees, guide the way we 
conduct business and interact with customers. They 
pay tribute to the culture of integrity, safety and respect 
that we inherited from our former parent company. 
My leadership team and I are excited about the 
prospects for the future. We are passionate about our 
businesses and the markets we serve. We look forward 
to the challenges and opportunities that await us this 
year and beyond. 
As an Alcoa Corporation Director and stockholder, 
I also share our Chairmans sentimentthat our 
interests, and those of our management team, are 
aligned with the interests of each of our stockholders 
for continued success.
It is an honor to lead and to carry such a rich heritage 
forward into a brand new company. I thank our 
employees, our Board of Directors and especially you, 
our stockholders, for your support.