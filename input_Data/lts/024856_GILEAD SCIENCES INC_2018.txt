A Message From
Daniel O'Day
Chairman and CEO

As I join my 11,000 colleagues at this remarkable company, I�m both humbled
by the opportunity and excited about the potential I see. During this time of
unprecedented change in our industry, few companies possess the resources,
employee passion and longstanding commitment to science and innovation that
Gilead has shown for more than 30 years.
With new challenges and greater potential for progress than ever before, Gilead is
well-positioned to build on our legacy of delivering transformational medicines for
patients. We are dedicated to discovering and developing lifesaving medicines, but
we also recognize that science alone can�t solve the world�s toughest healthcare
challenges. We believe in putting patients first � and we do this in numerous ways
by supporting access to healthcare services around the world and by forging
partnerships with patient advocacy groups and healthcare professionals.
I�ve spent a lot of time thinking about Gilead�s unique role as a leader and our ability
to shape the future of healthcare during this era of change. As we seek to discover,
develop and commercialize innovative therapeutics in the areas of unmet medical
needs, our role is defined by three core beliefs:
We Believe Discovery Never Ends
Behind every medication Gilead has developed over the last three decades is a
group of dedicated individuals � across our entire company � united by a common
desire to make groundbreaking discoveries that can help patients and save lives.
It�s this sense of purpose that has allowed Gilead to make a difference in the world.
Our shared commitment has made possible the prevention and treatment of HIV/
AIDS and fueled our quest that may one day lead to finding a cure for HIV. It�s
driven us to pursue a cure for cancer through our work in the field of cell therapy.
And it�s what led to the development of a cure for hepatitis C.

We Believe In Our People
Throughout my career, I�ve had the privilege of working with many smart and
passionate individuals, and I�ve seen firsthand what people can achieve when they�re
given an inclusive work environment in which every person and every voice receives
equal value. This is why it is so important to ensure our people are always given
opportunities to learn, grow and achieve their greatest potential. I am grateful to be
part of a company that puts such emphasis on diversity and inclusion � a core value
that is essential to Gilead�s long-term success.
We Believe Our Business Is Sustainable
As a large, global company, we understand the impact our day-to-day operations
can have on people and the environment. To help ensure we continue to execute
our mission of providing lifesaving medicines in areas of unmet need, we have built
a corporate social responsibility (CSR) program that supports patients, society, the
planet and our business. In 2018, Gilead established its CSR committee to help
ensure our business meets the highest social and environmental standards. Our
approach to CSR, and our continued support of the UNGC/UN SDGs, reaffirms our
commitment to drive short-, medium- and long-term action in the areas of human
rights, labor, environment and anti-corruption.
As chairman and CEO, I am committed to ensuring Gilead makes the most of
this unprecedented time in our industry. I am excited to be at Gilead and to work
alongside our talented employees to harness our collective commitment to doing
what is right for patients.
Thank you for believing in us.
Daniel O�Day