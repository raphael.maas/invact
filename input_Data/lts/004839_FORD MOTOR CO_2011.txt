A MESSAGE FROM
THE PRESIDENT
AND CEO

Ford Motor Company continued on its path of
profitable growth in 2011.
Around the world we are delivering best-in-class
vehicles to our customers. Our cost structure has
been dramatically improved and our balance sheet
greatly strengthened. We have taken a leadership role in addressing issues of global concern.
Now we want to go further.
Ford is a growing company operating in a rapidly expanding global automotive market. We have increased
our product investments to meet this rising demand. We are confident that we will continue to increase
our worldwide sales and improve our operating margins. The result will be profitable growth for all of our
stakeholders.
Let me explain how we will achieve this goal.
Great Products
Success in the automobile industry begins with great products.
More than five years ago we consolidated our regional automotive operations into one global product
development organization to fully leverage our worldwide resources and maximize economies of scale. We
continued to invest heavily in new products, even as we faced difficult economic conditions. As a result, we
now have a full family of outstanding vehicles with segment-leading quality, fuel efficiency, safety, smart
design and value.
In 2011 these exciting new products helped us achieve our third consecutive year of higher market share in
the United States. We also had a higher share in Asia Pacific Africa and three consecutive quarters of yearover-
year share gains in Europe. Our product momentum will continue in 2012 with the launch of a number
of new vehicles, including the introduction of the all-new Ford Fusion and Mondeo, Escape and Kuga,
EcoSport in South America, and the B-MAX in Europe.
Industry-wide vehicle sales are expected to rise significantly in the next few years, driven by accelerated
expansion in developing markets, recovery in mature markets, and sales of smaller and more fuel-efficient
vehicles. We expect our sales to increase to about 8 million units by mid-decade, up about 50 percent from
5.3 million units in 2010. Because of significant growth in the region, by 2020 nearly one-third of our sales
will come from Asia Pacific Africa, more than doubling the current percentage of global sales volume we
achieve in this region.
The strength of our business will enable us to keep investing for this future growth.

Strong Business*
Our 2011 full year pre-tax operating profit was $8.8 billion, or $1.51 per share,
an increase of $463 million from a year ago. Strong results from North
America and Ford Credit offset challenges in other parts of the world. Overall
it was our most profitable year since 1998.
Our North American operations had a pre-tax operating profit of $6.2 billion
in 2011, compared to a profit of $5.4 billion a year ago. While down from
the previous year, Ford Credits pre-tax operating profit of $2.4 billion also
contributed significantly to our full year results.
We made substantial investments in our future in 2011, but it also was a
milestone year in our efforts to strengthen our balance sheet. We ended the
year with $32.4 billion in total Automotive liquidity. Automotive debt was
$13.1 billion, a reduction of $6 billion for the year. Automotive gross cash was
$22.9 billion; exceeding debt by $9.8 billion, which was an improvement of
$8.4 billion for the year.
In 2011 we signed a four-year agreement with the United Auto Workers
that will help us improve our global competitiveness. As a result, we
will be investing $16 billion in the U.S. and adding new jobs at our U.S.
manufacturing facilities.
Because of our continuing improvement in 2011, by the end of the year we
were able to announce the reinstatement of paying a quarterly dividend to
our shareholders for the first time since 2006. Restoring the dividend is an
important sign of our progress in achieving profitable growth for all.
Better World
We believe the challenges we face as a society present us with an
opportunity to add value for everyone, including our shareholders.

Environmental and social goals are key elements of our
business strategy, and our business objectives are fully
aligned with our efforts to help build a better world.
A good example of this is our commitment to being a
leader in fuel economy. In a 2011 survey of consumers,
42 percent said fuel economy is an extremely important
new vehicle purchase consideration, which is up over
13 percent versus a decade ago. To meet this growing
need, nearly one-third of Fords U.S. vehicle lines will
feature a model with 40 mpg or more in 2012  a claim
no other full-line automaker can match.
We also are strongly committed to growing our family of
electrified vehicles. By next year, we will triple our production
capacity for electrified vehicles in North America. Our
approach is to offer our customers a variety of choices to
meet their different needs.
We now offer in the U.S. the battery-powered Focus Electric
sedan and the Ford Fusion Hybrid. By the end of the year
we will introduce the C-MAX Hybrid, C-MAX Energi plug-in
hybrid, and Fusion Energi plug-in hybrid.
We recently added our new 1.0-liter EcoBoost three-cylinder
 the smallest engine Ford has ever built  and will offer in
the future an all-new eight-speed automatic transmission.
We are building these products in factories that are lean and
clean. Between 2000 and 2010, we cut energy use at our
global facilities by 40 percent, CO2 emissions by 49 percent
and water use by 62 percent. A number of our facilities use
power from solar panels, hydro-electricity, geothermal, and
wind. We are replicating many sustainable technologies first
piloted at the Rouge Center.
Serving our communities also is an important part of
contributing to a better world. In 2011 Ford Motor Company
Fund and Community Services continued its support of
non-profit organizations in three major areas: innovation and
education, community development and American legacy,
and auto-related safety education. Ford employees around
the world also helped out in their communities through our
Ford Volunteer Corps.
Last year 25,000 Ford employee volunteers contributed
110,000 volunteer hours working on 1,200 community
projects. Those totals include our annual Global Week of
Caring, which involved 12,000 Ford employee volunteers,
244 projects and 55,000 volunteer hours on six continents.
Looking Ahead**
For 2012 we remain focused on continuing to deliver the key
aspects of our One Ford plan, which are unchanged:
 Aggressively restructure to operate profitably at the current
demand and changing model mix.
 Accelerate the development of new products that
customers want and value.
 Finance the plan and improve the balance sheet.
 Work together effectively as one team, leveraging our
global assets.
We expect to continue improving our business and deliver
improved Automotive pre-tax operating profits, strong
operating-related cash flow, and solid Ford Credit profits.
Overall, the Companys pre-tax operating profits, excluding
special items, should be about equal to 2011s strong level.
We expect full year U.S. industry sales to be in the range of
13.5 million to 14.5 million vehicles. Full year industry sales
in Europe, in the 19 markets we track, should be at the low
end of the range of 14 million to 15 million. Overall, we expect
global industry sales to be about 80 million vehicles, up
about 5 percent from 2011. We expect our full year market
share in the U.S. and Europe to be about equal to 2011.
The rapid pace of our new product introductions around
the world will continue in 2012, as will the expansion of our
production facilities in global growth markets such as China,
India and Russia. We also expect to deliver year-over-year
improvements in the quality of our vehicles worldwide.
We recognize we have challenges ahead of us, but there
are great opportunities as well. We are excited about
accelerating the realization of the full potential of the
global scale and operating margin benefits of our One Ford
plan. We also are excited about what leveraging our global
assets ultimately will deliver: profitable growth for everyone
associated with our business.
As always, we thank you for your support of our efforts.
Alan R. Mulally
President and Chief Executive Officer
March 14, 2012