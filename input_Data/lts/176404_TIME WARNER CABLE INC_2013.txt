Dear Shareholders:
I write this letter, my first as Chairman and CEO, at an exciting and historic time. On
February 13, 2014, Time Warner Cable and Comcast Corporation announced an agreement
to merge to form a world-class technology and media company, differentiated by its ability
to deliver groundbreaking products and outstanding customer experiences on a nearly
national platform. This transaction recognizes Time Warner Cables unique value and is
great news for our customers, our employees and youour shareholders.
Both Time Warner Cable and Comcast have been at the forefront of the cable industrys
most important innovations, including digital cable TV, high-speed Internet, DVRs, Video
on Demand (VOD) and WiFi, to name just a few. The combined company will further
accelerate the pace of innovation.
Reflecting on 2013
We accomplished a great deal in 2013. We reorganized our management, enabling us to
standardize best practices, streamline decision making, enhance efficiency, drive accountability
and, most importantly, better serve our customers. We also welcomed top-notch talent from
outside the company to fill key gaps within our leadership team. And we implemented various
initiatives designed to improve the customer experience and our overall performance. As a
result, we exited the year poised for significant, sustained success and with a renewed focus
on the things that matter mostputting customers at the center of everything we do, delivering
differentiated products that work simply and reliably, and providing superior service.
In 2013, we continued to drive healthy year-over-year growth in key financial metrics:
^ We increased total company revenue 3.4% to $22.1 billion, driven by growth of 21.6%
in business services and 14.4% in residential high-speed data.
^ We increased operating income 3.0% to $4.6 billion.
^ We grew adjusted diluted EPS* 15.0% to $6.61.
^ We generated free cash flow growth of 2.1% to $2.6 billion.
^ We returned $3.3 billion to shareholders through dividends and share repurchases.
We also made great strides in enhancing our residential customer experience through
the deployment of new products and features and meaningful improvements in customer
service. To highlight several, we:
^ Improved our high-speed data offerings, increasing speeds to as fast as 100 Mbps and
adding thousands more WiFi hotspots to our network. We finished 2013 with 30,000
TWC WiFi access points. And, through our Cable WiFi collaboration with other cable
operators, our customers can now use more than 200,000 hotspots across the U.S.
^ Made significant investments in our video product, deploying a graphically rich
cloud-based guide on 2.8 million set-top boxes, increasing our high-definition channels
to an average of 183, and making our TWC TV service for consumer-owned devices
more robust and more broadly available. By year end, our customers could access up to
300 live linear channels and over 4,000 hours of VOD content on a wide range of devices,
including PC and Mac computers, Apple iOS and Android tablets and smartphones,
Roku Streaming Players, Samsung Smart TVs, Xbox 360 video game consoles and
Amazon Kindle Fire tablets.

^ Expanded the availability of Time Warner Cable IntelligentHome, our next-generation
home security and automation system, across our footprint.
^ Made great strides in customer service, rolling out industry-leading one-hour
appointment windows throughout our footprint, facilitating more self-installs and
self-care, improving the troubleshooting capabilities of our customer care agents
and enhancing customer education.
Im particularly pleased with our performance in business services. Specifically:
^ We increased revenue by over 20% yet again in 2013.
^ We added 55,000 commercial buildings to our network, ending the year with 860,000
commercial buildings on net.
^ We activated more than 2,500 additional cell towers and, with our acquisition of DukeNet,
a regional fiber optic network company primarily serving the Carolinas, we ended the year
with 14,000 cell towers connected to our network.
^ We made a number of other critical investments that position us well to achieve our goal
of generating at least $5 billion of annual business services revenue by 2018.
Finally, 2013 was also a year of transition, with the retirement of our longtime Chairman and
CEO Glenn Britt. Glenn is a cable icon who had a profound impact on Time Warner Cable and
the broader industry during his 41-year career. His visionary leadership and commitment to our
business paved the way for a multitude of new products and services over the years. Just as
important, Glenns belief in conducting our business with honesty, integrity, transparency and
respect has shaped our corporate values. Our company and industry owe a debt of gratitude
to Glenn, as do I for all I have learned from him over the last 20 years.
Our 2014 Commitments
As we work to secure government approvals and plan for a successful integration of Time
Warner Cable with Comcast, our number one priority in 2014 is to execute on the ambitious
operating plan we established at the end of last year, centered on significant improvements in
reliability, quality and customer experience. These operating priorities are as relevant today as
they were before we agreed to merge with Comcast. Accordingly, we remain fully committed
to executing on them. We started the year with great operating momentum, and I am excited
about our plans.
In 2014, we will continue to revitalize our residential services business by:
^ Building on our recent success in subscriber acquisition and retention to drive stronger
and more stable subscriber trends. We intend to make significant progress toward our
goals of capturing 500,000 DSL subscribers from the telcos by April 2015 and adding
one million customer relationships over our three-year plan period;
^ Significantly improving reliability by investing in our network and meaningfully
enhancing customer service;

^ Differentiating our products in ways that matter to our customers. We will continue to roll
out our next-generation video products, deploying our cloud-based guide on several million
more set-top boxes, introducing new six-tuner DVRs, increasing our VOD capacity in the
home to 75,000 hours nationwide, continuing to increase our Internet speeds while offering
an aggressive everyday low price tier to attract price-sensitive subscribers, and making our
industry-leading IP apps even better on still more platforms; and
^ Reinventing the Time Warner Cable experience in our first two TWC Maxx markets, New
York City and Los Angeles, through significantly increased investments in our network and
equipment. Our all-digital network will deliver crisper pictures and sound and ultra-fast
Internet speeds with rock-solid quality and reliability.
We will also drive continued robust growth in business services, while at the same time investing
so we can sustain that growth for years to come. We will connect more buildings and towers to
our network and invest in new products, supported by a larger and more capable sales force.
Our media services team is poised to have a record year, capitalizing on increased political
advertising spending related to the mid-term elections.
And this year we will celebrate an important milestone in our philanthropic work, as we reach
our goal to connect one million young minds to the wonders of science, technology, engineering
and math (STEM). We are exceptionally proud of our Connect a Million Minds initiative and the
contributions we are making to inspire our nations next generation of problem solvers.
^
As we prepare to turn the next page in Time Warner Cables history, it seems fitting to
acknowledge our success as a stand-alone public company. Since our separation from
Time Warner Inc. in 2009, our company has generated total shareholder returns of over 450%,
significantly outperforming the market. Since taking over as Chairman and CEO, I have been
driven by two primary objectivescontinuing to maximize shareholder value and delivering
great customer experiences. Thanks to the support of more than 50,000 Time Warner Cable
employees who bring their passion and commitment to work every day, and the exciting plans
we have for our future with Comcast, we can proudly say we are accomplishing those objectives.
Thank you for placing your confidence and trust in us.
Sincerely,
Robert D. Marcus
Chairman and Chief Executive Officer
April 2014