In 2003, Todays the Day became
Monsters new brand slogan. It also
became our call to action.
With a renewed sense of mission and
a single-minded focus on growing
our leadership position in the online
market, we began to unleash the true
potential of Monster.
By building on our core strengths 
best-in-class technology, unequaled
brand awareness, unmatched customer
focus, and innovative products  we
challenged ourselves to become more.
We strengthened our international
presence in key markets and entered
new vertical growth sectors where
hiring activity is the strongest.
We strategically realigned our sales
channels, introduced new product
offerings and stepped up our branding
efforts with the launch of a new
integrated marketing campaign.
We implemented a plan for a more
efficient and productive operating
structure across the Company,
increasing fiscal and operational
discipline and streamlining processes
while improving customer service.
And we delivered. As a result of our
efforts that began in early 2003, we
are emerging from one of the toughest
job markets in recent history poised
for the future.
For the full year, the Monster division
posted four consecutive quarters of
margin improvement and grew to
$424 million in revenues while
achieving record deferred revenue.
Focusing on our strengths, we grew
our job seeker database to over 34.5
million resumes (were by far the
largest holder of resumes) and, with
the largest sales force in the industry,
we migrated thousands of customers
from newspaper classifieds.
By going on the offensive and
executing on our plan we were able
to increase sales, take market share,
reduce costs and grow profit margins 
without waiting for a boost in the
job market.
Seizing Growth Opportunities
In every category, in every channel,
and in every country in the world in
which we operate, were seizing
opportunities for growth.
Strengthening
our Global Presence
With approximately 6,000 employees
in 20 countries, we have unparalleled
international reach.
Monster is the leading pan-European
careers website and is growing fast
in developing markets such as India
where we recently emerged as the
leader in the online careers space 
a unique competitive advantage as
the globalization of business and
recruitment continues.
Strengthening our existing global
offerings and entering new
international markets will
continue to remain a priority
for Monster Worldwide.
Expanding Market Share
Since the 10 years from its launch,
Monster has evolved from a job
board to an online platform for
managing many of the key
events in ones lifetime.
Because of this unique platform
and our online recruitment expertise,
we have significant opportunities to
expand the Monster brand by bringing
our best-in-class products and services
to untapped vertical markets.
In 2003, we focused our efforts
on becoming more specialized
in niche sectors such as Government,
Hourly & Skilled, Diversity and Inclusion,
Staffing, and Healthcare.
That strategy is certainly paying off,
generating revenues from new sources
while proving new growth areas.
Our Monster Government Solutions division
has quickly rung up a string of
successes, including overhauling
the U.S. Governments one-stop
recruitment site for Federal jobs.
Were now doing business with
over 90 government agencies or
sub-agencies, which is a very long
way to come in such a short time.
Targeting blue-collar and other hourly
workers has also reaped considerable
returns. Nearly 3 million hourly and
skilled job seekers have registered with
Monster and, in the fourth quarter,
nearly 20% of all Monsters job postings
were hourly and skilled positions.
Feeding the Monster
The strength of our Monster-focused
business model is complemented
by our two other core businesses:
TMP Worldwide Advertising &
Communications (AdComms) and
TMP Worldwide Directional Marketing.
Through the AdComms recruitment
advertising business, we have an
established presence in the HR
community. These relationships played
an integral role in attracting the
biggest and best customers to Monster
and continue to do so today. In fact,
AdComms contributed over $60
million in revenue to Monster last year.
Our Directional Marketing business
gives us critical feet on the street
to drive business to Monster.
We help over 2,000 organizations
efficiently place their Yellow Pages
advertisements. As we expand into
the hourly and skilled labor force,
were using these relationships to
drive more employers to Monster
and capture more market share
from newspaper classifieds.

Getting Better
In the first half of 2003, our approach
focused substantially on improving
both our cost structures and our
market position.
Early last year we made operational
changes supported by new
technologies that allowed us
to better structure the company
around servicing existing
customers and gaining new ones.
We streamlined our processes, created
deeper profit and loss accountability
within the organization, and enhanced
our customer relationship management
systems to improve our marketing
efforts and lead generation.
During the second half of the year,
thanks in large part to our truly
exceptional and dedicated managers,
we made fast progress on serving
our customers better and
beating competitors.
Results from our latest survey
of employers showed the highest
satisfaction rating levels for Monster
in our history. Customer satisfaction
with Monsters sales representatives,
customer service team and products
continue to be higher than
our competitors.
As always, we continue to invest
internally to enhance our technology,
services and products. Our justlaunched
Monster Networking, while in
its early stages, is a great example
of this and we are excited about the
opportunity it presents to enhance our
users experience and expand our
platform for managing life choices.
We recently introduced our new
SmartFind Resume Search product 
an improved tool enabling recruiters
to conduct more efficient and
effective candidate searches.
When you have the worlds largest
talent pool and a database that is
growing by an average of 46,000
resumes a day, you need a targeting
tool that can keep pace. SmartFind
enables us to deliver matching
capabilities that no one in the industry
can touch. Look for more product
enhancements in 2004 as a result of
ongoing research and development.
This is a company that knows how
to make tough decisions and necessary
investments to stay competitive. By
making decisive moves, we ended
2003 as a more efficient, more
capable and more successful company.
We enter the new year with improved
cash positions, growing customer
counts, strong renewal rates and
operationally focused to execute
our Monster-focused sales strategy.
We have stayed the course and
delivered on what we promised at
the beginning of 2003. As a result,
we continue to set the standard
and lead the way in the markets we
compete and are well positioned
as the job market recovers.
Cultivating our Culture
Great people flourish in a healthy
culture. And I believe our sustained
success is the product of our culture 
of what we value, what we stand for
and who we are striving to be.
At Monster Worldwide, every employee
in every business in every region across
the globe has the same noble purpose:
bringing people together to advance
their lives.
We also have shared values that bind
us together and keep us moving in
the same direction: entrepreneurial
leadership, a passion for innovation,
embracing change, doing what we
say, and treating each other fairly.
It is a result of these shared values
and vision of the future that we came
together in 2003 as one company
and one team with a singular
passion to win.
I want to thank you, our
shareowners, for your support
during twelve extraordinary months.
And I want to thank the employees
of Monster Worldwide for their
dedication, perseverance, and their
countless hours of work to build an
even greater global company.
We enter 2004 with the tremendous
opportunity before us to be the online
destination for people to manage key
events in their lifetime. Whether they
are pursuing an education; entering
the private sector, the public sector
or the military; advancing their
career; or making a move, Monster
Worldwide has a solution.
You can be assured that I remain
committed to building a great
company where our employees arrive
each morning knowing that todays
the day they can accomplish great
things  for our clients, for
themselves, and for you.
Andrew J. McKelvey
Chairman & Chief Executive Officer
Monster Worldwide, Inc.