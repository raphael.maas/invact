Fellow Shareholders,
Fiscal 2018 was another year of solid growth, with revenue about flat as reported and
an increase of 5% on a constant currency organic basis. As reported, earnings per
diluted share increased to $3.39 and adjusted earnings per diluted share increased
10% to $4.15. From a strategic perspective, we completed all planned divestitures
during fiscal 2018 and remain on track to achieve cumulative annual cost savings of
approximately $40 million by the end of fiscal 2019 from the integration of Synergy
Health. In addition, we completed several small acquisitions that are additive to our
business.
The majority of our business grew at or above our expectations on a constant currency
organic revenue basis in fiscal 2018. Our two fastest growing segments, Healthcare
Specialty Services (HSS) and Life Sciences, both exceeded our expectations for the
year as each grew revenue 9% on a constant currency organic basis. The trends
driving these two businesses are anticipated to carry into our new fiscal year, although
year-over-year comparisons will be more difficult on top of this past year�s success.
Applied Sterilization Technologies (AST) continues to deliver strong results, with 7%
constant currency organic revenue growth for the year. AST profitability improved
faster than we had anticipated in fiscal 2018, reflecting the better than expected use of
our capacity, including that which came online this past fiscal year. While we continue
to expect improvements in profit in fiscal 2019, the year-over-year change is expected
to be somewhat more modest than we thought a year ago due to the success in fiscal
2018.
With regard to Healthcare Products, we continued to experience solid organic growth
in recurring revenues. Total segment revenue grew 2% on a constant currency
organic basis, driven by meaningful growth in both consumables and service, which
was offset by a modest decline in capital equipment shipments. We have launched
about 30 new products in our Healthcare Products segment this past year, many of
which are consumables. Looking ahead to fiscal 2019, we expect to continue a similar
pace of product introductions, but will shift a bit more to capital equipment. With new
products, solid year-end backlog and continued expectations for a stable hospital
capital spending environment, we would anticipate capital equipment revenue growth
to return to more normal low- to mid-single digit growth in fiscal 2019.
Shifting to our balance sheet, we committed to reducing debt back to our historic
levels after the completion of the Synergy combination two years ago. I am pleased
to report that we accomplished that goal in fiscal 2018. From a capital allocation
perspective, we will revert to our normal long-term priorities. Our first priority is
dividend increases in-line with growth of earnings. Our second priority is investment in
our current businesses for organic growth in revenue and profitability. For fiscal 2019,
we will be making substantial investments in our HSS segment to grow our outsourced
reprocessing business in the U.S. We have several projects in the pipeline for these
services and plan to invest in both capital to build out the facilities and in substantial
hiring during the year to run them. We remain optimistic that this business model
will succeed in the U.S. over the longer term. In addition, we will continue to invest in
AST expansions around the globe to meet increasing demand. These two areas of 
investment alone will total more than $60 million in growth capital out of our total capital
spending of approximately $190 million in fiscal 2019.
Our third priority is acquisitions, which we continue to find available in the marketplace.
We prefer tuck-ins to enhance our current businesses, but we�ll also consider deals
that will broaden our portfolio. Lastly are share repurchases, which in our current plan
for fiscal 2019 will be used to offset dilution from our equity compensation program.
Like many companies, U.S. tax reform will result in significant additional earnings for
us to strategically grow our business and return value to Customers, employees and
shareholders. On that front, we paid a one-time special bonus to all U.S. employees
other than senior executives in March of this year. In addition, we will be making
substantial investments in our business in fiscal 2019, which we think is prudent given
the additional dollars available to us from tax reform and our opportunities for organic
growth. We will be spending approximately $10 million on growing our HSS outsourced
reprocessing business and increasing R&D spending in all four of our segments.
Despite these investments, we anticipate operating profit to grow faster than revenue
on a year-over-year basis.
We are excited to start our new fiscal year with solid growth momentum, exciting new
products and services, and the team to create record years in fiscal 2019 and beyond.
In that regard, we recently announced several changes to our Board of Directors.
Jack Wareham, who has served on our Board since 2000, and as Chairman since
2005, has announced that he will retire when his current term expires at our Annual
General Meeting this summer. Jack has played a vital role in shaping our Board and
our company during his tenure. I speak on behalf of the entire Board when I say that
we were very fortunate to have his leadership as Chairman for over a decade. The
Board intends to appoint long-standing independent Board member, Dr. Mohsen Sohi,
to the role of Chairman after the Annual General Meeting. I am extremely pleased that
Mohsen has agreed to take on this responsibility. In addition, the Board also appointed
a new Director in May, Dr. Nirav Shah, who is an excellent addition.
It is an honor to lead your great Company and to work alongside esteemed colleagues
both internally and on our Board. I thank the people of STERIS for their great work, and
our Board for their continued guidance and support. We all appreciate the support of
our long-term shareholders as we continue our work to help our Customers create
a healthier and safer world.
Until next year,
Walt Rosebrough
President and Chief Executive Officer
June 2018
