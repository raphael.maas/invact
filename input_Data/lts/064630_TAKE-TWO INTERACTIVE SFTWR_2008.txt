Dear Shareholders,
2008 was a monumental year for Take-Two.
Our Company delivered record revenues
and profits, despite external distractions and a challenging global economy.
In addition to the extraordinary consumer
response to Grand Theft Auto IV, these
results refl ected the actions we began taking
in early 2007 to transform our business,
including investment in our superb creative
teams, a highly disciplined approach to
building our portfolio of world-class titles,
the divestiture or revitalization of non-core
businesses, and continued emphasis
on effi ciency throughout our company.
We are very proud of these
achievements that illustrate the three
tenets of our strategy�Creativity.
Innovation. Effi ciency.
Creativity
Our key drivers in 2008 were Grand Theft
Auto IV, Midnight Club: Los Angeles,
NBA 2K9, Carnival Games, and Sid Meier�s
Civilization Revolution.
Grand Theft Auto IV in particular was
a phenomenal critical and commercial
success, and surpassed the all-time
entertainment records for day one and
week one retail sales by dollar value
for all media. This franchise continues to
redefine how people play video games.
More and more, consumers and observers
alike see video games as a true art form
that can rival any other media�both
creatively and commercially.
With more than 1,500 of the industry�s
best developers working in our 15 studios
worldwide, Take-Two has the leading
creative talent building and developing what
we believe to be the strongest portfolio of
top selling franchises in the action/adventure, racing, sports, strategy, role-playing
and family gaming genres. We respect and
nurture creative passion, and invest the
resources to deliver triple-A products that
capture both mindshare and market share.
This year, we entered into new long-term
agreements with key talent at Rockstar
Games who have created some of our
blockbuster brands, including Grand Theft
Auto, Midnight Club, Max Payne, Bully
and Red Dead Revolver. These agreements
meaningfully align the interests of our
creative teams with our shareholders.
Innovation
We continue to innovate in both gameplay
and technology. Our proprietary Rockstar
Advanced Game Engine (RAGE) and other
technologies enable our studios to create
entertainment experiences that feature
incredible realism, which further immerse
consumers into our virtual worlds. Driving
a Lamborghini down Sunset Boulevard
in Rockstar�s Midnight Club: Los Angeles
feels just like the real thing�if not bett er.
2K Sports� NBA 2K9 is a shining example
of how thin the line has become between
the graphical quality of a televised NBA
game and an NBA video game. We�ve
incorporated an exciting feature into some
of our sports titles called Living Rosters,
which tracks real-life player performances,
including streaks, slumps and injuries that
enable our virtual athletes to perform trueto-life throughout their entire seasons.
In addition to creating unique entertainment experiences, we are innovating
through new revenue streams and distribution opportunities. We launched
our fi rst downloadable content off ering
for BioShock in 2008 and redefi ned what
downloadable content can be with the
February 2009 release of The Lost and
Damned, the fi rst of two episodes for
Grand Theft Auto IV. We also released
downloadable content in early 2009 for
Midnight Club: Los Angeles that featured
Strauss Zelnick
Chairman
Lainie Goldstein
Chief Financial Offi cer
� Committ ed to prudently
managing our business
and maintaining our sound
fi nancial position.
revenue streams, such as downloadable
content, networked gameplay and microtransactions. We will seek to capitalize
on these opportunities to maximize the
value of our intellectual property.
We recognize the challenges that every
company faces today, and remain steadfast in our determination to deliver value
by being the most creative, the most innovative and the most effi cient company in
the interactive entertainment industry. We
are committ ed to off ering the best home
for top creative talent, sett ing new benchmarks with triple-A products, making
strategic investments for growth, prudently
managing our business, and maintaining
our sound fi nancial position.
We would like to thank our colleagues
for making this a record year. We also thank
our shareholders for your continued support and look forward to reporting on our
progress in the year ahead.
Sincerely,
Strauss Zelnick, Chairman
Ben Feder, Chief Executive Offi cer
new cars and maps which added hours
of new racing experiences. While still an
emerging area, we believe that downloadable content�when done right�off ers
the potential to deepen brand loyalty
among consumers, extend the lifecycle of
our titles, diversify our revenue streams
and enhance profi tability.
Today�s consumers demand triple-A
gaming experiences and we continue to
deliver them. Our portfolio includes some
of the biggest and most popular brands in
our industry today. We have 15 internally
owned brands with sales over one million
units each and 30 individual products that
have each sold over one million units. These
successes refl ect the dedication and vision
of our creative teams to set new benchmarks in interactive entertainment.
We currently have two of the three alltime highest rated titles for Xbox 360 and
PLAYSTATION 3 with Grand Theft Auto IV
and BioShock. NBA 2K9 was once again the
top rated and top selling basketball game,
and Carnival Games was a smash hit on the
Nintendo Wii with over 3 million units sold.
The strength of our products is underscored by the vast critical acclaim that they
receive. We won Spike TV�s Video Game of
the Year Award for the second consecutive
year, with the 2008 honors going to Grand
Theft Auto IV and BioShock winning in 2007.
Effi ciency
Our team delivered on the Company�s
business plan, improved operational effi -
ciencies, reduced fi xed costs and built
a stronger foundation for the future.
We reported record results of $1.5 billion in revenue, with earnings per share of
$1.28, and ended the year with $210 million
in cash net of debt. In addition to generating record revenue and earnings, we
continued to implement and maximize cost
savings initiatives that are expected to
produce both short and long term benefi ts
for our Company.
We improved the operations of our distribution business, and entered into an outsourcing agreement for the pick, pack, ship and
warehousing functions previously handled by
our Jack of All Games subsidiary. This allows
Jack of All Games to focus on purchasing,
sales and service for their customers and
enhanced the Company�s profi tability.
We took steps to maintain our access
to the credit markets prior to the economic
downturn. Shortly aft er our management
team joined Take-Two in 2007, we obtained
a credit facility to provide additional fl exibility for our business. Given the volatility
of current market conditions, we were
pleased to have this facility already established and available for opportunities that
may arise. As a matt er of prudence, we
drew down $70 million on this facility as of
October 31, 2008.
Our strategy has been to release only a
select number of triple-A games that are
internally owned and developed, as well as
selectively publishing some licensed titles.
We believe this continues to be the right
strategy for our business.
Our team is prepared to manage
these challenging and uncertain times,
and has adopted a prudent outlook for
fiscal year 2009. In the year ahead, we
intend to explore and implement cost
savings initiatives that should benefit
our Company through more efficient and
productive operations, while continuing
to invest in a disciplined way in premium
interactive experiences. We believe that
we have the ability to run our business
efficiently without sacrificing our standards for excellence.
Leading industry analysts expect interactive entertainment to remain one of
the fastest growing segments within the
broader entertainment industry, due to
the deepening penetration of current hardware, expanding consumer audiences, and
continued growth in international markets.
There are promising opportunities for new 