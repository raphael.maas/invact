The year 2011 was a very good year for BorgWarner.
Global automotive production grew a modest 3% in 2011
compared with 2010. During that same period, BorgWarner
sales grew 26%, outperforming the industry by a wide
margin. Our strategy to focus on improving fuel economy,
emissions and performance is clearly a winning approach.

The industry began the year with cautious optimism, but struggled to gain momentum.
In March, a natural disaster devastated parts of Japan and ground industrial activity to
a halt. At the time, it was known that vehicle production would be lost, the impact of
which was measurable, and, therefore, somewhat understood. However, the impact of
a crippled Japanese supply base, which supplies key components to the industry around
the world, was not well understood. The uncertainty caused worst case scenarios to be
contemplated and action plans to be developed. Despite our best efforts to contribute to
the recovery, most of us outside of Japan were relegated to watching and waiting. During
that period, I was inspired by the resilience of the Japanese people. Their resolve and
the speed of their recovery was remarkable. In the end, the ingenuity of the industry
prevailed. Alternative sources were found and product plans were adjusted to accommodate for scarce parts. The impact to BorgWarner was moderate.
China took measures to cool down their fast-growing economy in 2011. In addition to
measures taken on a broad scale, the automotive sector was directly impacted by
reduced government-sponsored incentives for vehicle sales in addition to limiting vehicle
registration in some of the major cities. Vehicle production slowed down as a result, but
the adoption of advanced powertrain technology did not. China was our fastest growing
market in 2011 and I expect this to continue going forward for many years to come.
During the second half of 2011, the industry was focused on Europe and the potential
impact of its sovereign debt crisis. Speculation was rampant and downside risk weighed
on the minds of investors. Europe is an important market for our company. At more than
50% of our total revenue, it is our largest market, and leads the world in advanced
powertrain development. Clearly, developments in the European economy and its
automotive industry are meaningful to us. However, despite the heightened concerns,
as I write this letter, the European automotive industry continues to grow and, more
importantly, continues to adopt advanced powertrain technology at a rapid pace, driving
above-market growth for BorgWarner.

The bright spot in the automotive industry
in 2011 was North America. Light
vehicle production grew 9%, outpacing
the rest of the world. Another key development in North America was the U.S.
governments bold step toward instituting
globally competitive fuel economy standards. It is expected that by 2025, automakers will have to achieve average
corporate fuel economy standards of 54.5 miles per gallon for their fleets.
These new standards will set into motion a
tremendous amount of development work
that will accelerate the adoption of advanced powertrain technology. In fact, the
work has already begun. This should be
a major growth catalyst for BorgWarner.
Record-setting Performance
Once again, it was a record setting year
for BorgWarner on a number of different
fronts. From an operational performance
standpoint, our revenue of $7.1 billion
and our earnings of $4.45 per diluted
share were new highs for the company.
It is also noteworthy that our operating
income margin, a key measure of profitability, was 11.2% in 2011, also a
new record.We continue to deliver industry-leading
stockholder returns. In 2011, our share
price reached a new all-time high of
$82.28, but eventually gave way to
market uncertainty surrounding Europe,
and ended the year at $63.74, down
11% from 2010. A decline in share
price is a disappointing result for a
year in which we achieved record operating performance, but to put things
in perspective, our industry peers, on average, ended the year down 31%.
Furthermore, total shareholder return
for BorgWarner stockholders over the
last five years was 122%, compared
with 1% for our peer group and -1%
for the S&P 500, providing outstanding results for you, our stockholders.
 The Benefits of a Strong
Balance Sheet
 During the year, our strong balance
sheet and cash flow enabled us to
execute two important initiatives:
 We acquired Haldex Traction Systems,
a leader in advanced front-wheel drive
(FWD), all-wheel drive (AWD)
technology. The deal is expected to
accelerate our growth in the global AWD segment as the market continues
to shift toward FWD-based AWD
vehicles. The acquisition adds industry
leading FWD/AWD technologies, with a
strong European customer base, to our
existing Drivetrain portfolio. This enables
BorgWarner to provide our global customers a broader range of AWD solutions to meet their vehicle needs.  We repurchased approximately 5 million
shares in 2011, most of which are
intended to be used to settle our convertible debt obligations in April of 2012.
 All of this was done while maintaining
our investment grade credit rating, a clear
signal that our balance sheet remains
strong and provides opportunities for
the future.
 The growing importance of improved fuel
economy and reduced emissions is driving
significant growth and capital investment
for us around the globe. We are currently installing capacity in North America,
South America, Eastern Europe and Asia
to support our growth in every major
product area.

Accelerating Growth
In the fall, we announced our backlog of
$2.5 billion of net new business over the
next three years (2012-2014), another
record for our company. This backlog
encompasses some of the most promising
technologies and geographies in the auto
sector and is representative of how BorgWarner is leading the way in important
global powertrain developments. From a
product perspective, turbochargers, engine timing systems, variable cam timing,
and dual-clutch transmission modules are
BorgWarners largest growth drivers. We
continue to generate important new business in each of these product families.
Turbochargers for gasoline engines represent a significant share of the turbocharger backlog, which reflects the growing global trend toward downsized turbocharged gasoline engines to address
fuel efficiency and emissions reductions.
For example, Ford selected BorgWarners
leading gasoline turbocharger technology for its new four-cylinder EcoBoost
engine and for rear-wheel drive trucks
featuring its new six-cylinder EcoBoost
engine. The six-cylinder EcoBoost engine
has been a tremendous success for Ford
while the highly anticipated 2.0-liter EcoBoost engine is launching on the 2012
Ford Edge and 2012 Ford Explorer.
Diesel turbochargers for both on-road
and off-road commercial vehicles are
also an important part of our backlog.
Last year, we announced that we are
supplying our latest B-series generation
turbocharger to DEUTZ for its newlydesigned, 6.1-liter six-cylinder heavy-duty
diesel engine used in agricultural machinery. And, more recently, we announced
that we are supplying turbochargers for the
new Mercedes-Benz 12.8-liter six-cylinder
in-line BlueEFFICIENCY engine, the first in
its class to meet upcoming Euro 6 emissions standards. The top twenty-five customers of our three-year net new business
include six commercial vehicle customers.
Another important launch last year was
our variable cam timing technology on
Subarus new 2.5- and 2.0-liter, Boxer
engines. The 2.5-liter engine features
BorgWarners award-winning Cam
Torque Actuated technology (CTA),
while the 2.0-liter engine introduces
BorgWarners CTA technology with a
new mid position lock, an innovation
named as a finalist in the 2011 Automotive News PACE Awards. Our innovations in variable cam timing technology
allow more precise control over intake
and exhaust valve timing for better fuel
economy and lower emissions. According
to Subaru, the new four-cylinder 2.5- and
2.0-liter engines improve fuel economy
4 to 10 percent, respectively, compared
with previous models, and meet U.S.
Super Ultra Low Emissions Vehicle
(SULEV) and Euro 5 emissions standards.
Another exciting development for
BorgWarner is our growing business
in China. The rapid adoption of leading
powertrain technologies in the worlds
fastest growing automotive market has
created a tremendous opportunity for
BorgWarner. In addition to our success
with Chinese-Western OEM joint ventures, we have developed relationships
with the leading domestic Chinese OEMs
that are now bringing new technologies
to China. For example, in 2011, Great
Wall Motor Company launched its first
self-designed diesel engine featuring a
BorgWarner variable turbine geometry
turbocharger. This is the first variable
turbine geometry turbocharger available
in China. In 2012, our dual-clutch transmission technology will make its Chinese
debut with Shanghai Automotive, also a
first for a Chinese automaker. 
Additionally, our other product technologies continue to grow around the world.
Fans and fan drives, emissions systems,
traditional transmission components
and AWD devices are all meaningful
contributors to our backlog of net new
business. Notably, we are supplying
transmission components, including friction plates and roller one-way clutches,
for Hyundais new 8-speed automatic
rear-wheel drive transmission. We are
supplying our electronically controlled
Visctronic fans to Freightliner for its Cascadia trucks, a first for Class 8 commercial
trucks in North America, and our TorqueOn-Demand transfer case drives the new
2.2-liter Tata Aria, the first four-wheel
drive cross-over vehicle in India. At the
same time, we are launching new transfer
cases for AWD Ram Trucks in 2011.
The regional mix of our backlog of net
new business is aligned with the pace of
advanced technology adoption around
the world. Forty-five percent of the backlog is in Europe which remains the
epicenter of advanced development for
internal combustion engines, fuel economy and emissions improvements. Thirtyfive percent is in Asia, which continues
to expand in importance because of its
rapid growth and its demand for leading
edge technologies. Twenty percent of our
backlog is in North America, where we
see intensified customer focus on programs aimed at improved fuel economy
and lower emissions.
The Road Ahead
Looking ahead, we expect 2012 to be
another great year for BorgWarner.
Global vehicle production growth will  continue, although we expect European
production to decline in 2012 compared
with 2011. Despite this, it should be
another record year for BorgWarner
as we continue to outpace the industry.
In 2012, BorgWarner expects to increase sales in every major region of
the world, including Europe, due to
our strategic focus on improving fuel
economy and lowering emissions.
 We expect to post sales and earnings
records in 2012. Furthermore, we expect to reach a higher operating income
margin in 2012 than a year ago. Last
year, our full year operating income
margin reached 11.2%. In 2012, we
expect that margin to be 11.5% or
better. The expectation of improved
margins can be attributed to restructuring actions taken in 2008 and 2009
combined with an ongoing focus on
operating efficiency.
 As we consider the longer term, we
believe that understanding the foundation
of our past success, and applying those
principles to todays strategic initiatives,
are the keys to our future success. On
that basis, our senior leadership has
validated eight Enterprise Strategies
that will guide BorgWarner to 2020:
 Enterprise Strategies
 Accelerate the pace of INNOVATION
and product leadership into new technologies, markets and geographic regions
 Consistently drive profitable sales
GROWTH at least 10 percentage points
above the global industry growth rate
 Be the supplier / strategic partner of
choice for our CUSTOMERS, while
meeting our company objectives  Realize a QUALITY operating system
that focuses on zero defects
 Continuously improve the STRUCTURAL
EFFICIENCY of BorgWarner
 Ensure we have the right TALENT in the
right locations at the right time
 Protect, nurture and sustain the unique
and successful CULTURE of BorgWarner
 Supplement organic growth with
MERGERS & ACQUISTIONS to
achieve our product, technology,
customer and geographic goals
 These strategies have been the cornerstone of BorgWarners success for many
years, and continue to be energized
for the future.
 We spend a lot of time thinking about
the direction of our industry and our
role in it. We believe that the industry
will remain focused on improved fuel
economy and lower emissions for
many years to come, and that our
products, and the new technologies
that we are developing for the future,
will play a crucial role in the industrys
evolution. Staying focused on the
future while executing operational
excellence today is a winning formula
for achieving one of our ultimate
goals, which is to maximize your
total stockholder returns.
 Timothy M. Manganello
Chairman and Chief Executive Officer