Raytheons 2003 Annual Report tells the story of a
company that is well positioned in defense, that has re-baselined
its business aircraft operations to reflect difficult market conditions,
that has enjoyed exceptional cash flow performance and
that has put many of its issues behind it. While the company still
has work to do, I believe we are focused on the right things and on
the right path going forward. 3 3 STRONG GOVERNMENT AND
 Government and
Defense bookings for the year were $20 billion, up from $14.9
billion in 2002, pushing year-end 2003 Government and Defense
backlog to a record $25 billion, an increase of $4 billion over
the prior year. This strong bookings and backlog performance
is a direct result, I believe, of our focus on our customers.
3 3 Government and Defense sales, which constitute the lions
share of total company sales, increased 7 percent compared with
the prior year, after the elimination of intercompany sales. The
increase was led by Integrated Defense Systems, Missile Systems
and Space and Airborne Systems. Total company net sales in 2003
were $18.1 billion, up from $16.8 billion in 2002. 3 3 Income from
continuing operations was $535 million or $1.29 per diluted share
in 2003 compared with $756 million or $1.85 per diluted share in
2002. Income from continuing operations in 2003 was negatively
affected by Network Centric Systems and Raytheon Technical
Services Company, and by increased non-cash pension expense.
It was positively affected by strong operating performance
in a number of our other Government and Defense businesses.
3 3 Including the impact of discontinued operations, the companys
net income in 2003 was $0.88 per diluted share compared
with a net loss of $1.57 per diluted share in the prior year.
3 3 TAKING TH E PULSE OF THE COMPANY My career with
Raytheon began in 1972. It has been my privilege to work since
then as an engineer on the shop floor, in materials, manufacturing,
fabrication and quality, in systems integration and planning, in program
and general management  and in almost all levels of leadership.
These experiences have left me with a love for this company,
its people and its customers  and a strong sense of stewardship
on behalf of our shareholders. 3 3 While metrics are extremely
important in leading a team of 78,000 people, one also needs to
have a feel of the pulse of the company. I hold a view that you
remember a third of what you read, half of what you hear, but 100
percent of what you feel. That feeling for the pulse of the company
is very important. For this reason, I value one-on-one communication,
in person when possible, electronically when not. I try to read
all of my e-mails within 24 hours. Ideally, I like to have a clean e-mail

screen before I go to sleep each night. 3 3 A CUSTOMERFOCUSED
COMPANY Given all this, what kind of company do I
believe Raytheon is today? The answer is: Raytheon today is a
company committed to customer success. We believe that if you
help the customer succeed, you will create the right conditions to
meet the needs of all your constituents. We believe that customer
focus creates customer success, that customer success drives
growth and that growth creates shareholder value. So, we believe
that a customer focus creates shareholder value. 3 3 In our view, a
customer-focused company has three pillars: performance, relationships
and solutions. 3 3 PERFORMANCE Performance is
really as simple as promises made and promises kept. Its taking
ones commitments seriously; when we make them, we must fulfill
them. During the year, our customers and partners showed confidence
in us in many ways. We received awards for a ship-based
radar for ballistic missile defense from the U.S. Navy; a next generation
U.S. Air Force Distributed Common Ground System, the backbone
of current and future intelligence and information systems; a
U.S. Missile Defense Agency Kinetic Energy Interceptor to target hostile
missiles in the boost phase; a role as Ground Sensor Integrator
for U.S. Army Future Combat Systems; continued modernization of
the radar on the Air Force B-2 bomber; technical services for NASA;
and 50 Hawker 400XP light jets and eight Hawker 800XP mid-size
jets  from NetJets Inc. 3 3 RELATIONSHIPS Relationships are
built on trust. Our customers depend on us. One of the most emotional
examples of this personal bond is what it feels like to correspond
with someone deployed in harms way. Those who have had
this experience know that it can be wrenching to receive an e-mail
from someone stationed in a high-risk area while youre sitting at a
computer screen in the safety of your office. You think long and hard
before you compose your response. Before I press the send button,
I always ask myself: Have I listened? Have I helped? Have we done
everything we can? 33 Listening. We are a company that listens;
were getting better and better at listening, but we need to get
better still. Building positive relationships with our customers begins
with listening  and trust. Integrity is something that only succeeds
when its embedded in the culture. We ask the members of the
Raytheon team to treat the companys name as if it were their own.
We ask the leaders of the team to create and maintain an environment
in which people are encouraged to step forward so that we
can address issues early. Everything we do must
begin with integrity. 33 SOLUTIONS We must develop and provide
superior customer solutions, working as one company. Were a
technology company at our core. We plan to drive Raytheon
technology as a key differentiator. You will see many examples of
the companys technology solutions in this Annual Report. 3 3 As
we build on our strength in technology, we must also align our capabilities
with the needs of our customers. I believe we have done so.
We saw that alignment in Afghanistan and in Operation Iraqi
Freedom. Our individual businesses work hard at this. We also have
four Strategic Business Areas  Missile Defense; Precision
Engagement; Intelligence, Surveillance and Reconnaissance; and
Homeland Security  that are designed to facilitate customer solutions
that cross our businesses and may even extend beyond our
company. 3 3We understand that to succeed internationally, we
must be sensitive to the unique needs of each of our customers 
whether for Airborne Stand-off Radar systems and Paveway IV precision-
guided bombs in the U.K.; combat control systems for
Australian submarines; or solutions to meet evolving defense needs
in Japan. 3 3 RAYTHEONS VALUES We are a company based on
values: People, Integrity, Commitment, Excellence. Weve talked
about the last three. Now for the first: valuing people. That can
sound nebulous; after all, who doesnt value people? But at
Raytheon, it has specific meaning. It means treating people with
respect and dignity, welcoming diversity and diverse opinions, helping
our teammates improve their skills, recognizing and rewarding
accomplishment and fostering teamwork and collaboration. 3 3 To
me, valuing people starts with a healthy and safe work environment.
Since 1998, the company has reduced employee injuries by 78 percent.
Now that Raytheon has reduced its injury rate to a level that
many consider to be world class, we are challenging the team to
prevent every injury. The goal must be an injury-free workplace,
because anything else is unacceptable. 3 3 I do have a passion for
our customers, this company and its people. I believe that if we help
our customers succeed, our employees, partners and stockholders
will share in our success. I also believe that if we stay focused on
what matters, our company will be on a path to greatness. 3 3 For all
of the reasons above  and as illustrated in the pages that follow 
at Raytheon, Customer Success Is Our Mission.
Sincerely,

WILLIAM H. SWANSON
Chairman and CEO