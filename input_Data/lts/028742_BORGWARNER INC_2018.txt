DEAR FELLOW STOCKHOLDERS

I believe today�s rapidly changing
landscape in the global automotive
market is both fascinating and
invigorating. The pace of change and
the remarkable ideas being developed
provides more exciting opportunities
than at almost any point in the more than
90-year history of our Company. While
we experienced a considerable amount
of dynamic change at BorgWarner in
2018 as our evolution continued, many
factors remain consistent. As I write this
letter, looking back on the past year and
my new position, it is a privilege to have
assumed the role of President and CEO
for such a remarkable Company.
One of our Company�s greatest strengths
is the breadth and depth of its team 

and the ability to develop talent. I am a
product of that culture and have been
a member of our Strategy Board for
the past seven years. As such, I am fully
aligned with our current vision, mission
and the associated strategies. Of course,
plans may be calibrated and adjusted
over time, but, by and large, we are
focused on executing and accelerating
the current plans. James Verrier was an
excellent CEO and when he decided to
retire this past summer, the Company
had never been in a stronger position.
I want to thank James for his strong
leadership and advice over the years.
During my roughly 30-year career in
the automotive industry, 20 years of
which have been with BorgWarner, I 

have been fortunate enough to work with
several different businesses with many
talented people that I am very proud to call
colleagues and friends. As these people
can attest, the core focus of my business
philosophy is the relentless pursuit of
profitable growth, which I will continue in
my current role.
Today, we firmly believe we have the right
strategy and an extremely strong portfolio
of products, which allows us to focus
on delivering profitable growth, above
market growth rates across combustion,
hybrid and electric propulsion systems.
BorgWarner maintains a unique position
as a company that can specify, develop
and manufacture a broad product offering
across all three propulsion systems. 

Ultimately, we are addressing one of
the most important challenges facing
our industry as well as society: creating
our vision of a cleaner, more energyefficient world.
Soon after I assumed the CEO role,
I outlined a simple �State of the
Company,� which I believe succinctly
outlines where we stand today, and
our near-term focus. We are a product
leader in clean and efficient solutions
for combustion, hybrid and electric
vehicles. Our products help improve
vehicle performance, propulsion
efficiency, stability and air quality. We
have maintained and expanded our
competitive advantages of customer,
geographic and propulsion system
diversity. And finally, we are delivering
growth significantly above market
growth across all three major propulsion
categories.
As we look forward, there are four key
factors to our ongoing success:
� Execute a balanced propulsion
strategy
� Maintain product leadership - the
primary driver of our business
� Strengthen operational discipline
� Deliver growth across all propulsion
categories
While our business is clearly evolving,
one thing has not changed: product
leadership remains the driver of our 

business. To ensure ongoing success in
this industry and progress towards our
vision, our Company must deliver the
quality products that our customers,
end consumers and governments want
and need, in high volumes to make a
positive impact.
There are three key elements to create
and maintain our product leadership. The
first building block is organic research
and development (�R&D�), and we are
constantly reinvesting approximately 4%
of our revenue in this area. We are striving
to invest in the right products for the
future, constantly assessing our balance
and directing additional investment where
needed, while starting to deemphasize
other more mature areas. Acquisitions
are the second key element of product
leadership, which has been a proven
success in recent years. We now have a
team that is fully focused on identifying
and assessing targets and presenting
the targets to my leadership team for
consideration. Finally, we are participating
in venture capital investments, which 

allows us to efficiently keep our fingers
on the pulse of the latest ideas around
the globe and potential new emerging
technologies and trends.
When I travel around the world, I am
always amazed by diversity of talent
and backgrounds that we have in
this Company. Our vision of a clean,
energy-efficient world has attracted
high-quality engineers and talented
people of all backgrounds who want
to help our Company achieve its vision
and participate in the next generation of
propulsion architectures. We are pleased
that greater than 50% of new hires in
2018 were people with engineering
backgrounds, and our focus on training
and employee development will continue
to be a key aspect of our strategy going
forward.
One sometimes overlooked competitive
differentiator is our operational
discipline, particularly around quality and
delivery, which is even more impressive
given that we operate 68 facilities around 

the world in 19 countries, producing
hundreds of millions of parts every
year. Today, we have a renewed focus
on quality, with many of our internal
key performance metrics showing
improvement year-over-year. Our plant
managers did a fantastic job focusing
on our Quality Board�s initiatives in 2018.
These initiatives complement our already
robust quality management systems and
help better disseminate best practices
around the world. These actions are
important as we continue to build upon
our success as we look to the future.
To reiterate the long-term outlook for
BorgWarner that we first outlined in
September 2018: 

� We expect 2023 revenue of $14 billion,
before any benefits from M&A.
� We are growing faster than the industry
and will be overweight in hybrid and
electric revenue compared to the
industry by 2023, producing some
impressive growth rates.
� We also plan to be overweight in hybrid
and electric by 2023 in terms of content
per vehicle, and our penetration rates
are growing rapidly.
� Although we continue to lead the
industry, we can always improve and as
such we have set a target to deliver $1
billion of free cash flow in 2023. 

As we drive forward towards these goals,
we believe 2018 was a year of strong
execution for our Company, which laid
a strong foundation from which we can
build. Despite the industry volatility, we
delivered organic growth of more than
600 basis points over our light vehicle
market. We successfully completed
significant project launches and won
new business across combustion, hybrid
and electric vehicle platforms. The
integration of the Sevcon acquisition was
completed in 2018 and we added many
high-quality people to the BorgWarner
family. The integration was completed
successfully on time, and today our
combined teams present seamlessly
to our customers, allowing us to win a
wider range of business opportunities.
Clearly, we continue to be considered
as a balanced propulsion partner by our
customers, and we believe our industry 

position, expertise and portfolio
of products remains a significant
differentiator in the marketplace.
The current market is volatile with
headwinds facing the industry, focused
in Europe and China. In Europe,
conditions have stabilized but we still
expect industry volume declines in the
near term as customers work through
the final impacts related to WLTP1
.
In China, we expect industry volume
declines to continue near-term as
customers reduce inventory, which will
also impact the launch of some new
projects. As we look to 2019 as a whole,
we expect an industry volume decline in
the range of 2.0% to 5.0% adjusted for

our regional exposure. Overall, our team
is doing an excellent job adjusting to the
more challenging industry backdrop, and
we expect to continue to outgrow the
market in 2019.
For the full-year 2019, we expect to
produce organic revenue growth of up to
2.0%, or 250 to 400 basis points over our
expected end-market decline, meaning net
sales are expected to be in the range of
$9.90 billion to $10.37 billion. We expect
our earnings per share to be in the range
of $4.00 to $4.35. While we continue to
deliver strong market outgrowth in 2019,
we will also look at ways to adjust our
cost structure to adapt to the current
environment.
We also recently announced our
three-year net new business backlog
(�Backlog�), which includes many exciting 

opportunities. Importantly, this Backlog
will support average organic growth over
market of 5.0% to 6.0% for the next three
years. The customers in this Backlog
represent a diverse group around the
world. Our diverse customer base is
increasingly important, not in the least
because of the insights this provides
us across the entire propulsion system
landscape.
From a product perspective, we see
growth across the portfolio with 20%
of the Backlog related to combustion
propulsion systems, 70% related to
vehicles with hybrid propulsion systems
and 10% related to electric propulsion
systems. From a regional perspective,
we continue to see Backlog growth in
all of our major markets, with 25% of
the Backlog in the Americas, 15% in
Europe, and 60% in Asia, with China 

accounting for 50% of the overall
Backlog. Despite the near-term industry
volume headwinds, we remain focused
on continuing to secure new business
with our customers around the world
and expect to continue to outgrow the
market. We firmly believe that our targets
are achievable because electrification
accelerates the opportunities for us.
On behalf of the entire management team
and Board of Directors, I want to thank
all of the plant managers and employees
in our 68 facilities around the world who,
through their dedication and consistent 

hard work, allow us to maintain and
improve our competitive positioning year
after year. Our success is predicated on
the entire 30,000 strong team, working
together to achieve our vision.
I am pleased to report that our hard
work is paying off and we are currently
booking ahead of the plan based on the
combination of our unrivaled product
positioning, significant and consistent
operational excellence, and the fact
that we are the trusted partner of our
customers around the world. Looking at
each of the three propulsion categories
in more depth:

COMBUSTION
We continue to predict growth for
combustion propulsion based on the
quality of our products, which are
delivering further penetration. We remain
the market leader in combustion, and with
most of our products we are number one in
the world thanks to our technology edge,
product leadership and product efficiency.
Even as the combustion market volumes
decline, the overall addressable volume for
many of our markets continues to increase
because all of our combustion products
are also viable and available for hybrid
vehicles.

HYBRID
We start with a strong position in hybrids
based on our understanding of both
the internal combustion engine and
drivetrains that will be used in these
vehicles. This combined knowledge is
something that very few companies
can match. To this we add one of the
most diverse hybrid product portfolios
in the industry. We can supply major
components for all of the hybrid
configurations that will be used, which
is important as hybrids will have a wide
variety of design variations. 

ELECTRIC
Our competitive advantage in electrics is
our full propulsion system expertise. Our
team can specify, develop and manufacture
all of the major components of an electric
vehicle propulsion system. Our Electric
Vehicle Transmission (eGearDrive)
expertise grew out of our long history
in transfer cases. To this, we added
electric drive motor products through our
acquisition of Remy. And most recently,
we increased our Power Electronics
capabilities through the acquisition of
Sevcon. As a result, we now are in a
unique competitive position to supply 

and manufacture all of the components
of electric propulsion systems. We
can design and produce the motor,
transmission and power electronics
and are in a much stronger position
to understand the design and overall
cost of the system, which will result in
significant awards of both total systems
and sub-components.
As we look further into 2019 and beyond,
we are focused on accelerating our
evolution, while finding the pragmatic
balance with prudent financial controls.
As such, we will not lose focus on growth 

opportunities and will continue to invest
in R&D and product development. At
the same time, we expect to continue
our growth trajectory above the
industry and maintain our incremental
margin performance, while diligently
controlling costs in the weaker industry
volume environment. If there is one
takeaway from this letter, I hope it is
this: electrification accelerates the
opportunities for BorgWarner. In fact,
the hybrid and electric markets are
actually larger than the combustion
market. BorgWarner is unique in its
ability to deliver the breadth of products
at the cutting edge of technology for
all vehicle propulsion categories. We
manage our portfolio constantly so
that we are in the position to grow this
Company to $14 billion by 2023, with
$1 billion free cash flow. 

Our targets are significant but achievable
and we are absolutely focused on serving
our customers around the world and
answering with the best products that
we can deliver. We have an outstanding
portfolio of technologies, and, based
on the combined efforts of our 30,000
strong team of amazing people, our
Company has never been greater. I look
forward to serving the Company as
President and CEO and working with our
strong leaders to ensure BorgWarner
continues its long track record of strong
performance and product leadership.
Sincerely,
Fr�d�ric B. Lissalde
President and Chief Executive Officer

