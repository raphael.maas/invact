DEAR
SHAREHOLDER,

PHOTOSYNTHESIS IS INDEED AT THE HEART OF ALL WE DO
HERE AT WEYERHAEUSER. YET BECAUSE IT TAKES DECADES
FOR THIS REMARKABLE PROCESS TO CREATE MATURE TREES,
THERE EXISTS A PERCEPTION THAT OUR BUSINESS ALSO MOVES
SLOWLY. NOTHING COULD BE FARTHER FROM THE TRUTH.
THIS IS A TIME OF UNPRECEDENTED AND DYNAMIC CHANGE,
AND AT WEYERHAEUSER, INNOVATION CONTINUES TO PROPEL
US FORWARD.
LOOKING BACK: A YEAR OF TOUGH MARKETS, DECISIVE ACTION
Our 2007 performance refl ects diffi cult market conditions but also
reveals our ability to create shareholder value.
The most signifi cant event of the year occurred in the fi rst quarter,
when we closed the groundbreaking spin-split merger that
combined our fi ne paper assets with Domtar to create the largest
fi ne paper producer in North America. This innovative transaction
produced $1.35 billion in cash for Weyerhaeuser, which we used
to pay down debt. It also enabled us to reduce our outstanding
shares by more than 25 million. Our remaining Cellulose Fibers
business is now better positioned to leverage research and
development expertise, boost mill effi ciencies and margins, and
capitalize on a strong worldwide market for absorbent pulp.
We followed this value-creating deal with the announcement that
we are conducting a strategic review of our Containerboard,
Packaging and Recycling business. We are pleased with the range
of options that our board is able to consider and look forward to
concluding the process.
Plummeting housing starts posed a signifi cant challenge to our
homebuilding and wood products businesses last yearand we
dont expect conditions to signifi cantly improve in 2008. Despite
this, our Real Estate business sustained leading fi nancial performance
among large homebuilders by relying on targeted strategies
in each of its select markets and by taking swift action to prepare
for weaker business conditions. Meanwhile, our iLevel organization
took aggressive action to balance production with customer
demand throughout the year and will continue to do so as needed
in the coming months. This business is streamlined, focused and
well positioned to take full advantage of a housing rebound, which
we anticipate based on favorable demographic trends over the
long term.
To maximize value from our timberlands, we continue to consider
a range of options, including ongoing support of the industry in its
effort to pass the TREE Act of 2007tax-reform legislation that
would provide immediate value to investors. Signifi cant progress
was made in 2007, with a one-year provision of the TREE Act
passed in both the House Energy and the Senate Farm bills. With
this progress, along with the TREE Acts strong bipartisan and
broad regional support, we are well positioned for enactment in
2008. We continue to position our timber portfolio for growth
in South America and remain intensely focused on realizing all
potential revenue from owning timberlands, beyond planting and
harvesting trees.
Finally, a number of leadership changes made last year refl ects the
deep pool of talent weve cultivated to propel our company toward
continued success. In the fi rst quarter, Tom Gideon was appointed
senior vice president, Containerboard, Packaging and Recycling,
succeeding Jim Keller, who retired after 32 years with the company.
Mike Branson stepped in to fi ll Toms previous position as senior
vice president of Timberlands. In the second quarter, Patty Bedient
was appointed executive vice president and chief fi nancial offi cer,
succeeding Dick Taggart, who retired after 33 years with the
company. And in the fourth quarter, we announced Dan Fulton
would take over my duties as president, Weyerhaeuser Company,
at the beginning of 2008. He now oversees all our operations
and a number of staff functions and is working closely with Rich
Hanson to prepare for his planned retirement later this year.
In addition, Debra Cafaro was elected to our board and adds
a wealth of real estate and broad business knowledge to the
breadth of skills on our board.
LOOKING AHEAD: CAPITALIZING ON INVENTIVE SOLUTIONS,
UNIQUE EXPERTISE
The world continues to demand different and better products.
People want safe, reliable shelter and products that make life
easier. Businesses want innovative solutions that help them
become more effi cient and profi table. And we all seek sustainable,
visionary products that anticipate what peopleand our planet
will need in the future.
Our response to this challenge can be summed up with two words:
trees and innovation. We have a strong track record of taking
deliberate action to create shareholder value in the best possible
way, wherever opportunity is present. Sometimes this means
applying our industry-leading expertise to fi nd new ways to use
every part of the tree and the land it grows on. Youve seen some
outstanding examples in the fi rst few pages of this report.
Sometimes it means having the market knowledge and foresight
to restructure a business, discover a more effi cient or profi table
way to operate it or, if necessary, sell, close or scale it back.
Sometimes it means creating a brand new approach to maximizing
value from divestiture.
The recent announcement of our intention to partner with Chevron
is a perfect example of our innovative thinking. Our expertise in
large-scale forest management and cellulose conversion matches
well with Chevrons expertise in energy production and markets.
We expect this partnership to hasten the development of cellulosic
ethanol as a renewable nonfood-based fuel for cars and trucksan
endeavor that represents not only innovative leadership in the face
of one of the worlds most pressing needs but also an extraordinary
opportunity to unlock additional value from our core timberlands.
Our ability to fi nd creative solutions also applies to the way we
report our sustainability performance. Weve taken steps to
become more transparent and timely by launching a comprehensive
website, where stakeholders will have easy access to current
information about our practices, policies and results. Last year, we
set a number of sustainability goals, including drafting a policy
that confi rms our commitment to human rights, which is now
complete. We also analyzed our water consumption at primary
mills and are identifying opportunities for improvement. Product
certifi cation remains a top priority, with 94 percent of our North
American forest products currently certifi ed to sustainable forestry
standards. And we continue to support wide adoption of greenbuilding
practices by taking a leadership role in the development
of a National Green Building Standard by the National Association
of Home Builders. Our progress on these fronts and in other
critical sustainability areas will be reported throughout the year at
www.weyerhaeuser.com/sustainability.
As a leader in our industry, we have a responsibility to continually
set stretch goals in all these areas and then exceed them. In
doing so, our shareholders will benefi t from the value we create,
and customers, partners, communities and employees can be
confi dent in the sustainability of our company and the forests
from which we derive value.
This brings us back, as always, to the trees. They remain our
core resource, our chief expertise and our greatest competitive
advantage. By focusing on innovation, we will continue to unlock
a whole new world of potential in our forests and ensure that
shareholders receive the full benefi ts of these holdings. We have
the formula for success, and with it we will not only weather
change but also turn it into opportunity.

Steven R. Rogel
Chairman and Chief Executive Officer

