To Our Shareholders:
For 2015, we are pleased to have achieved the 18th year of uninterrupted
growth and profitability for Rollins (�the Company�). The success that
we enjoyed this past year is a reflection of the ongoing dedication of
our employees across our organization. Our results are driven by our
team�s commitment to our corporate mission statement: �to be the
world�s best service company.� Strategically we are dedicated to
continuous improvement � we know we can always do better, in all
parts of our business.
As you read this report, be aware that the 11,000+ men and women
who represent Rollins around the globe are hard at work implementing
our 2016 strategic initiatives: to attract new customers and to provide a
better customer service experience, while improving productivity and
accelerating our sales and profitability.

Financial Results
Revenues for 2015 rose 5.2 percent to a record $1.485 billion, compared to
revenues of $1,412 billion for the previous year. We continued to gain market
share and remain the largest pest control company in North America. All
of our service lines contributed to our growth: Residential Pest Control
revenues were up 6.6 percent, Commercial Pest Control revenues grew
3.3 percent, and Termite revenues grew 4.6 percent. These numbers reflect
the contributions from the Statewide, PermaTreat and Critter Control
acquisitions made in 2014 and 2015. Regrettably we were negatively impacted
by the currency devaluation in Canada and Australia. Net income grew to $ 0.70
per diluted share, an 11.1 percent increase over 2014 and our pre-tax profit
margin of 16.4 percent was a 5.8 percent improvement over the year before.
In 2015, we provided our shareholders dividends of $0.42 per share, which
included a special year-end dividend of $0.10 per share paid in December,
an increase of 20.0 percent over 2014. This marked the 13th consecutive year
we had a dividend increase of 12% or more to our stockholders. This was
Rollins fourth consecutive year of paying a special year-end dividend.
There is a plaque in a number of our branches that has been around for a
long time which reads: �You can�t improve one thing a thousand percent,
but you can improve a thousand things one percent.� Little by little this
adds up to big accomplishments. That�s what we look to do on a daily basis
and not surprisingly, this type of commitment leads to the next important
achievement, while often identifying the next opportunity for improvement.
We gained momentum in the rollout of our Customer Relations Management
(CRM) system last year, without negatively impacting our business. A major
operating system replacement normally puts a strain on the business;
however, this was offset by the enhancements that were received. In
December we accelerated the rollout from one region a month to two
regions per month. We expect to continue that pace with a target of
completing the rollout to all Orkin locations ahead of schedule, by the third
quarter. At the end of last year 50 percent of the branches were on the
system with over 2,600 pest control technicians using iPhones. We believe
this new branch operating system (BOSS) will be a real game changer for
Rollins. It will help us improve operating efficiencies, as well as enhancing our
routing and scheduling capabilities. Most importantly, we have expectations
that BOSS will enable us to improve our customers� experience.
Our technology team wasn�t solely focused on BOSS during 2015,
they upgraded and replaced a number of tired support systems, which
provided better financial reporting and telecommunications, while reducing
costs. Additionally we made improvements in our training initiatives and
internet presence.
In March of last year, we advanced our position as the leading wildlife
control provider in North America, with the acquisition of Critter Control.
This is the franchisor of the nation�s leading wildlife brand, with 106 

franchises coast to coast and two franchises in Canada. You might recall
that in 2010, we acquired TruTech, one of the largest animal control,
wildlife and animal removal companies in the U.S. We believe that there
is a tremendous opportunity to grow this service line while capitalizing
on the industry�s leading brand.
In the fourth quarter we announced that Orkin extended its presence in
North America, South America, Europe, the Middle East and Asia, with the
addition of nine new franchises. Those new franchises are located in Mexico,
Colombia, Republic of Georgia, Qatar, China, and South Korea.
At December 31, 2015, Orkin had 48 international franchises and 51 domestic
franchises. We look to expand our domestic franchise footprint, as well as
international, while at the same time working more closely with our franchise
partners to help them grow their businesses.
Strategic acquisitions remain a priority for us. As in the past, we will continue
to seek out companies that are a �fit� for us in both, the pest control and
wildlife areas of our business. Last year we made 12 such acquisitions.
Our marketing folks were a major contributor to our success last year. They
continued to �up their game� across all of our marketing channels�digital,
mobile, traditional media, etc. And frankly, we�re getting better at reaching
our �target prospects� and identifying the best channels in which to reach
them. As you are aware, we established an in-house analytics team a couple
of years ago to help us to do a better job of understanding customers�
preferences, and price elasticity. Their contribution is allowing us to take our
marketing plans to another level. Like so much of what we do, we see the
promoting of our brands as a work in progress. It just keeps evolving, and we
look forward to even greater contributions in this area going forward.
The service business is first and foremost a people business, and our
employees are our most important asset. A major priority for us every year is
to improve on the retention of our employees. Our employee retention rate
again improved in 2015. We continue to work with our employees to ensure
that they are receiving the very best training that they need to be successful,
and to advance their careers. We want them to feel they have a future at our
Company, not just a job. A primary focus in this area last year was refining
our recruiting and employee selection process, which helps ensure that
candidates are the right fit for our Company, across all brands and divisions.
In this regard, we continue to make inroads in attracting women to Rollins
in an industry that has traditionally been male-dominated.
Celebrating the Rollins Heritage Center
After several years in the making, on June 4, we had the Grand Opening
of the Rollins Heritage Center. This facility is located next to our Atlanta
Learning Center. It was our vision to have a place our employees and others
could visit and trace our history, beginning with Rollins Broadcasting; 

followed by Orkin and Rollins Protective Services and all the other
businesses that have been a part of Rollins since 1960. Several companies
that joined us predated Rollins� beginning, such as Waltham, Orkin,
Western, and Crane. These are featured at our center, as well as some
unsuccessful ventures along the way.
Among the memorabilia are documents and pictures showing key events
that took place, as we ultimately evolved into a solely focused pest control
company. There are giant bugs that we used in our commercials, letters from
customers, various honors, and an Orkin wagon used to deliver our services
during World War II, when gasoline and vehicles were rationed. The largest
display is the last Ford Ranger truck that came off of the assembly line, (a gift
from Ford Motor Company).
We welcome you to visit our Heritage Center if you are in the area. I think
you will enjoy the experience.
Looking Forward
We are looking forward to achieving another record year. All of our team
members will be working diligently to enhance the customer�s experience.
Our focus on continuous improvement across all areas of our business
doesn�t change. All of us at Rollins are energized by what we see as great
opportunities to further improve and grow our business in 2016 and beyond.
We are thankful for our customers, employees, partners, and for our
shareholders support.
Sincerely,

John F. Wilson
President and
Chief Operating Officer


Gary W. Rollins
Vice Chairman and
Chief Executive Officer
R. Randall Rollins
Chairman of the Board