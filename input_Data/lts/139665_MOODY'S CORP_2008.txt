dear shareholders and other readers,
In my letter to you last year I observed that the severity and
protracted nature of market dislocations confirms that
the challenges of 2007 will persist well into 2008. It is now
obvious that those challenges not only persisted but also
deepened and widened, breaching previously safe havens of
the global economy. By the fourth quarter of 2008 we were
witnessing worldwide paralysis in the global credit markets.
Determining the mechanisms that will support sustainable
recovery and restore market confidence will be a critical effort
through 2009 and will carry lasting aftereffects. For Moodys,
this recovery process represents both a responsibility and
an opportunity: Moodys Investors Service has enhanced its
rating processes and ratings transparency, summarized in
our Special Comment, Strengthening Analytical Quality and
Transparency, available to readers in the Credit Policy area of
moodys.com; Moodys Analytics has developed and invested
in important risk-management products and services, both
independently and through selective acquisitions. I will
further discuss our efforts in the context of current and longerterm
financial market dynamics, after a brief summary of
Moodys financial performance for 2008.

THE YEAR IN REVIEW
Moodys financial performance in 2008 was disappointing.
Revenue contracted by approximately $500 million and net
income by $235 million1 versus the Companys record performance
in 2007. Market conditions were sluggish from
the beginning of the year through mid-September when
credit markets collapsed and issuance activity virtually
ceased through year-end. Loss of investor confidence led to
extreme risk aversion, with demand for capital protection
clearly trumping the demand for return on capital.
Despite substandard financial performance, Moodys
demonstrated resilience through these extraordinarily
challenging business conditions. We invested in strategic
growth opportunities and remained profitable, due to
our solid base of recurring revenue and prudent expense
management. Recurring revenue  including fees from
monitoring outstanding securities and from subscriptionbased
products  accounted for 64% of Moodys
overall revenue. Cost management initiatives reduced the
Companys year-over-year expenses by 11%, partially mitigating
the effect of declining revenue on operating income
and earnings. Revenue growth was limited to our Moodys
Analytics business, which grew 15%, and the public,
project and infrastructure finance ratings unit of Moodys
Investors Service, which had a modest revenue increase.
Geographically, Moodys international revenue declined
6% versus 2007, but grew annually as a percentage of total
revenue from 40% to 48% in 2008. While the stress in
the U.S. credit markets affected other regions, especially
Europe, conditions internationally were generally less
extreme and selected markets in Asia and other areas
continued to present attractive opportunities. Foreign
currency exchange rates and the depreciation of the U.S.
dollar had a favorable impact of one percentage point on
revenue in 2008, comparable to the two points of revenue
growth attributable to exchange rates in 2007.
At Moodys Investors Service, global ratings revenue
declined by 32% in 2008, with the largest contraction coming
from structured finance. The structured finance market
faced difficulties all year, with new issuance disappearing in
the U.S. and slowing internationally in the fourth quarter.
The corporate finance ratings area also experienced a
material decline, with high-yield bonds and bank loans hit
hardest by investors limited appetite for risk. Our financial
institutions ratings business posted a modest decrease for
the year, with uneven issuance amid eroding market conditions
and countervailing policy actions, while the public,
project and infrastructure finance area achieved mid-singledigit
growth. Total ratings revenue declined by 42%
for the U.S., while international ratings revenue fell by 15%.
2008 marked the first year of operation for Moodys
Analytics  our capital markets research and bank riskmanagement
software segment. Moodys Analytics includes
several different businesses  analytic tools, economic data
and analysis, risk-management software solutions and credit
training services  along with sales of credit research and
related content produced by the rating agency.
Moodys Analytics revenue grew 15% this year against
increasingly difficult market conditions, with all
three business lines  subscriptions, software and consulting
 contributing to top-line growth. U.S. revenue
increased 9% from 2007, while international revenue
increased 21%, representing just over half of Moodys
Analytics total revenue.
Other reported financial measures for Moodys
Corporation include:
 Operating income of $748 million, down 34% from
$1.13 billion in 2007
 Net income of $458 million, down 35% from
$702 million in 2007
 Diluted earnings per share3 of $1.87, down 28% from
$2.58 in 2007

The Way Forward for 2009 and Beyo nd
As we approach the second anniversary of the current
contraction in global credit markets, we anticipate similar
or even greater stresses in 2009. De-leveraging and uncertainty
continue to affect the global financial system; credit
conditions remain tight, especially for consumers and
businesses most in need; and negative trends persist for
macroeconomic and capital market indicators, including
unemployment and corporate default rates. To understand
Moodys ongoing role, function and growth prospects,
it is first necessary to consider how credit markets, the
regulatory environment and overall business conditions
are changing. To this end, I offer some modest observations
from Moodys perspective, discuss some potential
ways forward, and assess the possible implications of these
changes for the Company.
Cyclical and Structural Market Challenges. The pandemic
of lost confidence is now well chronicled, starting with
the poor performance of U.S. subprime home mortgages
and mortgage-related securities originated in 2006 and
early 2007. It is also now clear that latent vulnerabilities
had developed within the infrastructure of the global
financial markets, as the rate of financial innovation
and market globalization outpaced existing regulatory
and oversight mechanisms. These realities have shifted
perspectives for policymakers, regulatory authorities and
market participants: the emphasis on deep but normal
cyclical conditions has waned, while concerns associated
with structural vulnerabilities have grown in magnitude
and standing. This shift is important because the former
circumstances are more likely to be endured while the
latter ones invite formal change.
With such change likely to occur in multiple markets, the
national orientation of most policymakers and oversight
bodies may lead them to retrench from global approaches.
At the same time, the de-leveraging and required recapitalization
of financial institutions threatens to curtail
foreign lending and increase reliance on domestic funding
sources. For global markets to thrive again, however,
robust regional and global coordination of markets must
accompany national oversight.
Competition and Management of Potential Conflicts in
Credit Ratings. Policymakers and private sector commentators
have suggested changes in rating agency business
models, additional levels of oversight and/or competition
as mechanisms for improving performance. Often embedded
in these suggestions are unchallenged assumptions
about structural conflicts and the nature of competition
in the credit rating industry. At the heart of this debate,
however, there is a tacit consensus that credit rating agencies
perform an indispensable market function by assessing
the risks of securities for the investing public.
A critical starting point in evaluating proposed changes for
the credit rating industry is to acknowledge that the only
parties likely to pay for ratings  whether issuers, investors
or government agents  are those interested in particular
ratings. Parties naturally want ratings that are most
beneficial to their interests, and their wishes often conflict

with the right rating as independently determined by the
agency. As a result, attempts to persuade rating agencies
about their opinions can and do come from all types
of market participants. Managing potential conflicts of
interest is a necessary aspect of the rating business, and
rating agencies must continuously manage those pressures
appropriately and transparently.
The nature of the credit rating industry overturns the
simplistic conclusion that more competition is good and
less competition is bad. If rules and regulations about
competition are to be effective and productive, they
must be based on enhancing the quality, and not simply
the quantity, of credit ratings or credit rating agencies.
Moodys has long held that healthy competition among
rating agencies on the basis of quality is in the best interest
of financial markets.
In light of the important challenges and opportunities
described above, Moodys has recommended the following
measures and considerations:
1) Potential conflicts must be subject to specific oversight
to determine if they are being effectively, demonstrably
and transparently managed. This might require, for
example, direct third-party oversight of potential
conflicts or periodic reviews of customer concentra-
tion levels;
2) Regulators, to the extent they continue to utilize
ratings, must consider the quality of ratings rather than
commoditizing them. The tendency to view officially
recognized credit rating agencies as interchangeable
diminishes the incentives for competition on the basis
of rating quality and predictive performance; and
3) Market participants must better understand what uses
ratings can and cannot serve, and then use ratings
accordingly. Improved understanding and more
informed use of ratings will enable the market to judge
the quality of competing providers.
Potential Implications for Moodys. Structural changes in
the nature and operation of markets impact both our service
to investors and our internal operations. For example,
Moodys Investors Service must continue to evaluate the
many credit implications of these structural changes and
provide the market with insight and perspective. Moreover,
it is inevitable that further regulation lies ahead for the
financial services industry. Beyond adherence to new rules
and regulations, Moodys Investors Service must prepare
for additional interest and scrutiny from oversight bodies
at the national, regional and global levels. There will be
incremental costs in meeting these obligations, but we will
adapt to meet the demands of both domestic and crossborder
markets.
For Moodys Analytics, structural changes fuel demand
for new tools and applications. In this financial crisis,
lost confidence has greatly extended the scope and extent
of financial assets under stress. The loss of confidence
corresponds to perceived weaknesses in mechanisms that
identify, measure and communicate risk exposures. To use
an analogy, if a neighborhood restaurant is reported to
have health code violations but the identity of the restaurant
and the significance of the violations are unknown,
diners are likely to avoid all the restaurants in the neighborhood.
So it is with investors, risk-sensitive instruments
and confidence-sensitive institutions: if the source and
scale of risk is unknown, then avoidance is preferable
to indeterminate risk-taking. Moodys Analytics is well
positioned to provide services that address the burgeoning
uncertainty in financial markets, and its range of products
and services was recently complemented by its acquisition
of Fermat International  a software firm specializing
in bank risk-management tools. Our offerings help
measure and report on firm-specific risk, and contribute
to market-wide development of more rigorous riskmanagement
practices.

THE GROWTH PICTURE FOR 2009 AND BEYOND
In previous shareholder letters, I wrote confidently about
globalization and disintermediation, innovations in
financial technology and global economic expansion as
powerful and interconnected drivers of growth for Moodys
business. Paradoxically, I now write about those features of
the financial market landscape as being in flux and subject
to greater uncertainty than most market participants ever
expected. From a growth perspective, 2008 was the most
difficult year Moodys has experienced in decades  fairly
characterized as a year that established a new baseline
for growth. We do not project 2009 to be any easier.
Nevertheless, intriguing and potentially substantial longterm
opportunities remain:
Disintermediation. Even as de-leveraging reduces
outstanding debt in the worlds financial system, capital
replenishment within the banking system may curtail
the volume of bank lending for years. If so, de-leveraging
among banks may be partially offset by additional
bond issuance by corporations, municipalities and
other borrowers. It is also likely that as markets recover,
future efforts to reduce the role of government as the
investor-of-last-resort in financial assets will result in a
new wave of disintermediation of assets from government
balance sheets.
Financial Innovation and Financial Darwinism. The
current crisis has brought about broad-based investor rejection
of securitized instruments. Unsurprisingly, investors
navigating the precarious shoals of the crisis have generally
not distinguished between instruments and asset classes
that have performed well under stress and those that have
not. While many commentators (and investors) currently
eschew all classes of securitization as flawed, more
refined views will inevitably emerge as markets stabilize.
Instruments that perform best through this period of deep
recession will be understood as empirically stress-tested
and gain acceptance by the market. Such outcomes will
help restore a smaller but ultimately healthier securitization
market. Issuance of structured instruments will not
reach the levels of 2006 or 2007 for a long time, if ever;
but securitization should still have a useful role to play
in a healthy global financial system and will offer growth
opportunities from current levels.
We anticipate that innovative financial products will eventually
regain investor and regulatory confidence. Moodys
will strive to play a central role in providing insight
that facilitates market understanding of such products.
In the interim, we will remain vigilant in monitoring
outstanding rated securities that are under stress, demonstrating
analytical enhancements that restore both
private- and official-sector confidence in our work, and
promoting efforts toward creating more transparency in
markets and in the work of credit rating agencies.
International Growth. Worldwide economic growth will
eventually resume, with a new equilibrium emerging
among domestic, regional and global capital markets.
With our global reach and presence in 29 countries,
Moodys is well positioned to adapt to changes within and
across markets, and to meet demands for expert opinion,
research and risk-measurement applications. We continue
to position both Moodys Investors Service and Moodys
Analytics for global opportunities by participating in
promising markets in Asia, Europe, the Middle East and
Latin America through acquisitions, joint ventures and
Moodys subsidiaries.

Growth from Moodys Analytics. In 2008 we relied heavily
on Moodys Analytics to mitigate the effects of the global
credit market contraction. While we cannot count on the
same levels of growth every year, we still expect attractive
long-term opportunities for this business. For example,
two of the most important lessons emerging from the
current crisis are the need for better risk-measurement and
risk-reporting systems, and more rigorous training in credit
and capital markets. Moodys Analytics is well positioned
to respond to these demands:
 We have deep experience in developing and delivering
risk-measurement solutions. In October 2008 we
significantly enhanced and expanded these capabilities
with the acquisition of Fermat International, a provider
of well-known bank risk-management software.
 We responded to the global demand for training in
credit and capital markets with our January 2008 acquisition
of Financial Projections Ltd., and our December
2008 acquisition of Enb Consulting. Based in the U.K.,
both firms have a well-established presence among large
institutional customers in Europe and Asia.
We will continue to expand the Moodys Analytics
business further through internal product development
and selective acquisitions.
As I did last year, I should conclude my comments on
growth by noting that cost management is an important
piece of the puzzle in a lower-growth environment.
Moodys prudently managed expenses throughout 2008,
while still funding strategic initiatives within the lines of
business. I express my deep thanks to Moodys employees
worldwide for their dedication and perseverance under
these trying conditions.
OUR COMMITMENTS TO STA KEHOLDERS
The market realities of 2008 and early 2009 are testing
participants in unprecedented ways, and have surpassed
even the most bearish early forecasts. Risks of over-reaction
and under-reaction flank policy initiatives and business
decisions every day. Yet abundant lessons have been
learned and are already being applied toward a renewal of
U.S. and global capital markets. Moodys is responding
with visible enhancements in our work processes, performance
and communication that will not only sustain, but
enhance, our role in global credit markets. Through these
improvements and other efforts we will stay the course
throughout this period of turmoil. We continue to have
confidence that the markets we serve will grow and drive
the demand for independent expertise in assessing credit
and in fostering consistent, comparative standards.
As ever, Moodys goal is to remain the leading authority
on credit risk in the global capital markets. We remain a
standards business, both because we offer global standards
for assessing credit and because we must operate to
standards that satisfy all stakeholders  investors, issuers,
shareholders, regulators, policymakers and employees.
Meeting our goal and fulfilling the expectations of our
stakeholders will always be challenging, especially when
the structure and fabric of the markets that we serve
are undergoing such profound stress and change.
We will endure, and we will succeed.
Thank you.
Raymond W. McDaniel, Jr.
Chairman and Chief Executive Officer