Dear Shareholders,
I am proud to report the results of another successful year for Visa.
For more than 50 years, Visa has helped its clients grow their payments businesses by focusing on both their shortterm
performance goals and their long-term success. In fi scal year 2012, our client-focused approach helped Visa
achieve strong business results and deliver on our commitment to remain a growth company. Net operating revenue
in fi scal year 2012 was a record $10.4 billion, a 13 percent increase over 2011. Adjusted net income was $4.2 billion,
a 19 percent increase over the prior year. Full-year adjusted diluted earnings per share came in at $6.20, 24 percent
ahead of last year.
These results were achieved during a year of remarkable change within our industry. Advances in technology are
opening up opportunities to conduct transactions in new ways and in new places. Electronic payments are rapidly
expanding around the world, taking root in emerging markets and allowing more people to enjoy the benefi ts of
fi nancial inclusion. We fully expect these changes to continue, marking the beginning of another wave of growth for
electronic payments.
To guide our business, we adhere to a simple vision: to be the Best Way to Pay and Be Paid. Visas diff erentiated
products and services help the Company maintain strong and mutually productive relationships with its fi nancial
institutions, merchants and governments around the world. Our innovation-related investments  namely the
acquisitions of CyberSource, PlaySpan and Fundamo  create value and help partners off er leading payment
products and services. Behind these products and services are a global brand and a powerful payments network
that handled more than $3.9 trillion in payments volume during fi scal year 2012. And perhaps most important are
Visas 8,500 employees who deliver an unmatched depth of payments expertise and insight to our partners every
day. Collectively, these assets create a strong foundation on which to build  our foundation for the future.
In fi scal year 2012, Visa took signifi cant steps to build on this foundation and further position the Company for
future growth. Specifi cally, Visa worked to expand its core products and services business, accelerate growth in
international markets, and invest in new payments technologies.

Visas long-term success is deeply rooted in continued
expansion of our core business around the world. One
important example of our success was a double-digit
payments volume increase for credit products in the United
States, as Visa added new clients and built upon our strong
existing relationships. A key contributor was our expanded
relationship with United Airlines, which began exclusively
issuing Mileage Plus Visa cards to all new customers in
January, following its merger with Continental Airlines.
Visas core business also adapted to signifi cant market changes in 2012, as we faced debit regulation in the United
States. Overall, the situation played out as expected and inline with our fi nancial guidance. That being said, this
regulation negatively impacted our business, resulting in reduced debit volumes as well as permanent market share
losses. Our post-regulation priority has been to work closely with our clients and help them adapt to the new
environment. And over the course of the year, we successfully secured all 15 of our top U.S. debit issuers to longterm
contracts. Finally, while we are still in the early stages of the new environment, it appears our revised debit
strategies are gaining traction and meeting Visas own operational and fi nancial expectations.
Importantly, revenue in Visas international geographies grew 16 percent during the fi scal year, accelerating progress
toward our stated goal of achieving more than 50 percent of our global revenue from international markets by
2015. In several critical markets, including Brazil, Russia, Japan, the Gulf Cooperation Council, Mexico, and sub-
Saharan Africa, we have deployed tailored acceleration plans to substantially increase revenue growth over a fi veyear
horizon.
For example, in Brazil, Visa reached an agreement with all of our major clients that will result in more than 90 percent
of our transactions in that country being routed over VisaNet (up from 63 percent). This agreement advances one
of Visas top strategic goals: ensuring a greater share of transactions get processed on VisaNet, which allows us to
deliver better fraud detection, diff erentiated information products and improved transaction quality to cardholders,
merchants and our fi nancial institution customers.
In Russia, where over 80 percent of consumer spending remains cash-based, Visa made substantial progress
expanding usage in everyday spend merchant categories, a strategy that aligns particularly well with our strong
stable of debit cards in that country. One highlight was the launch of Visa acceptance at Magnit, the countrys
largest grocery chain with more than 5,000 stores. In fact, Visa is now accepted at the four largest food retail chains
in Russia, an important step toward accelerating growth in that country

We are aggressively investing in new and diff erentiated
technologies that will increase the number of transactions
on our core business platforms, add incremental value to
the merchant community and their acquirers, and forge
new revenue opportunities for both Visa and our fi nancial
institution clients. Importantly, we know that our innovations
must create real value for our clients accountholders who
ultimately use these products and services.
This year, Visa began scaling our digital wallet service, V.me, to help Visa and its clients capture signifi cant growth
opportunities in eCommerce. The technology for V.me is fully developed, and we are now focused on expanding our
network of participating fi nancial institutions and online merchants. Currently we have more than 50 agreements
with leading U.S. issuers, including four of the 10 largest fi nancial institutions. This provides the potential for us to
bring V.me to more than 50 million consumers. At the same time, we are making progress signing up merchants and
expect to have more than 1,000 online merchants using V.me by the middle of 2013.
Meanwhile, in markets around the world, Visa continued to
deploy next-generation payment innovations that address
the unique needs of particular geographies. For example,
using Fundamos technology, Visa introduced Visa Mobile
Prepaid with MTN Group, Vodafone and France Telecoms
Orange Money. These programs will off er secure, reliable,
and globally interoperable electronic payment accounts to
unbanked consumers in several developing markets.
Technology is also opening up new opportunities for expanded merchant acceptance, as new mobile point-of-sale
(mPOS) devices, such as Square, are enabling merchants to accept card payments without the need for traditional
point-of-sale equipment and landline infrastructure. Since Visas investment in Square in 2011, we have been testing
and certifying several manufacturers around the world to provide mobile-based card acceptance solutions. Looking
ahead, we are also in advanced discussions with a variety of fi nancial institutions about launching pilot acceptance
programs in a wide range of markets over the next 12 months.

Visas products and services remove friction from commerce worldwide
by providing consumers convenient and secure access to their funds, while
reducing cash and check handling costs for merchants and governments.
Ultimately this delivers tangible economic benefi ts. According to a recent
study by Moodys Economy.com, between 2008 and 2012, the shift away
from cash and checks toward electronic payments added $983 billion to
GDP across the global economy. Additionally, that same study predicts
that a future 1 percent increase in card usage globally would produce a
0.056 percent increase in consumption and a 0.035 percent increase in
GDP.
Visas vision of being the Best Way to Pay and Be Paid has served us well for several decades. However, being the
best way means diff erent things in diff erent markets and to diff erent people around the world. Today, 2.5 billion
people lack access to basic fi nancial services. Thats why we are working with governments and other organizations
across the globe to deliver the benefi ts of electronic payments. This approach creates real value for local markets
through economic development while also creating new business opportunities for Visa. Thats why we are now
adding three important words  for Everyone, Everywhere  to our vision to guide our eff orts globally.
One example of how we are applying this expanded vision to our work comes from Rwanda, where Visa is proactively
engaging with policymakers and aligning our own growth agenda with domestic priorities. Our engagement with the
Rwandan government seeks to increase access to  and usage of  electronic payments in that country, with a keen
focus on fi nancial inclusion and fi nancial literacy. Through these eff orts, Visa is now processing domestic payments
and interbank ATM withdrawals and laying the groundwork for a scalable and interoperable mobile money program.
And our eff orts will help increase tourism volumes from international travelers, a key driver of economic activity and
a top government priority.
Of course, as we are successful bringing more participants into the fi nancial mainstream, it is important these new
accountholders have the knowledge and expertise to manage their fi nancial lives responsibly. That is why, for more
than a decade, Visa has provided free fi nancial literacy programs to millions of parents, teachers, children, and
accountholders around the world. Collectively, these programs have reached more than 20 million people in the
last four years.
To expand our fi nancial inclusion eff orts, Visa proudly joined forces with a group of well-respected leaders in the global
development community including the U.N. Capital Development Fund (UNCDF), the U.S. Agency for International

Development (USAID), The Bill & Melinda Gates Foundation, Citi, Ford Foundation, and the Omidyar Network. This
new initiative  called the Better Than Cash Alliance  brings together governments, the development community
and the private sector to advance the use of electronic payments to support people living in poverty. We look
forward to helping this alliance achieve our shared goals of empowering people and growing emerging economies.
LOOKING AHEAD
One of the most important steps toward building on our foundation for the future was the Boards completion of
a robust CEO succession process. On November 1, 2012, Charles W. Scharf became Visas new Chief Executive
Offi cer. I have every confi dence that Charlie is the right person for this job. He is a proven leader, is familiar with Visa
both as a client and as a board member and, importantly, drove innovation while at the helm of JPMorgan Chases
retail operation. I look forward to working with Charlie to make this transition seamless as I continue in my role as
Executive Chairman until I retire in March 2013.
As I entrust the leadership of Visa to its new CEO, its strong management team and its 8,500 employees worldwide,
I do so with great confi dence. We are in the midst of an important evolution in payments, but Visa has a strong
foundation on which to build, and we will continue to adapt, innovate and lead. This will allow Visa to remain the
partner of choice for clients and a driving force in bringing electronic payments to more people in more places
around the world.
I am proud of the many things  operational and fi nancial  we have achieved. More importantly, I remain excited
by the countless opportunities in front of this company and convinced of its ability to capture them.
JOSEPH W. SAUNDERS
Executive Chairman
Visa Inc.