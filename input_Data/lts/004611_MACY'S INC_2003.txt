Dear Fellow Shareholder:
2003 was a year of progress, accomplishment
and change. We deliberately
set about looking at our business in new
ways this past year, challenging assumptions
about how department stores operate
and what our
customers expect
from us. We
listened to our
core customers,
and we learned
a lot from what
they told us

 MAKE THE SHOPPING EXPERIENCE MORE FUN
 GIVE ME FASHION THATS NEW AND RIGHT FOR ME
 LET ME FIND UNIQUE PRODUCTS IN YOUR STORES
 MAKE YOUR PRICING CLEARER AND EASIER TO UNDERSTAND
 GIVE ME GOOD QUALITY AND VALUE, NOT JUST LOW PRICES
 MAKE THE STORES LESS CLUTTERED
 PROVIDE A PLACE TO REFRESH AND RELAX IN YOUR STORES
 MAKE EVERYTHING MORE CONVENIENT

As a result of this input, we began doing
some things differently. We focused more
resolutely than ever on our four key
priorities for improving our business over
the longer term  differentiating and
editing merchandise assortments,
simplifying pricing, enhancing the overall
shopping experience and communicating
better with our customers through more
cost-effective marketing.
We continued investing in our initiatives
to reinvent the department store. In our
Macys-brand divisions, we spent more
than $50 million of our total 2003 capital
expenditure budget on such physical
improvements as updated and enhanced
fitting room areas to make shopping more
fun and relaxing; easy-to-read directional
signage to make finding what customers
want more convenient; shopping buggies
to make conveying bulky purchases easier,
especially in the home area; and
conveniently located price checkers
throughout the store to make real value
easier to understand.
We began capitalizing
on the unique
strengths of our
national brands 
Macys and
Bloomingdales 
attaching the
Macys name to our
regional department
stores, and launching Bloomingdales
into the Atlanta market.
Federated has a long and valued history
with our regional department store groups,
and each of these stores has a long and
valued history in the communities they
serve. Most of these premier retail brands
have been around for more than 150 years,
coming together under the Federated
umbrella to become stronger and more

efficient operators. It is a process that
continues today
By creating Bon-Macys, Burdines-Macys,
Goldsmiths-Macys, Lazarus-Macys and
Richs-Macys, we were able for the first
time to test national advertising, with
encouraging results in the fourth quarter.
And we now are able to reach out to our
customers through unique brand-marketing
opportunities that were not feasible before,
such as the American Heart Associations
national Go Red For Women
campaign that launches this year and last
years Thanks for Sharing promotion,
the companys first nationwide causemarketing
initiative.
On the merchandising side, we identified
a number of unique new product lines,
such as H by Hilfiger, a sportswear brand
that is exclusive to Federated this year.
Other new merchandise offerings from
national vendors such as Jones New York,
Ralph Lauren, Calvin Klein and Kenneth
Cole were combined with our terrific
private brands  I.N.C, Hotel Collection,
Tasso Elba, Alfani, Charter Club,
Greendog, Tools of the Trade, The Cellar
and more. Together, these brands deliver
value and help to differentiate further our
stores, making them true retail magnets for
discerning customers.
Our results for fiscal 2003 clearly indicate
that we are making good progress in each
of these important areas. The numbers
speak for themselves:
 Sales trends improved throughout the
year and, coupled with excellent inventory
management, led to improved profitability
and a better-than-anticipated same-store
sales increase of 1.4 percent in the
fourth quarter.
 Cash flow from continuing operations,
before financing activities, totaled more
than $1 billion. This was nearly double the
prior years level, enabling us to institute a
cash dividend, repurchase approximately
16 million shares of Federated common
stock, pay down more than $450 million
of debt, and end the year with more than
$900 million in cash.
 Diluted earnings per share from
continuing operations were $3.71 for the
year, or $3.51 excluding the impact of a
one-time reduction in net deferred income
tax liabilities, up 9 percent over last years
$3.21 in diluted earnings per share.
FULL STEAM AHEAD
We entered 2004 with a full head of
steam, encouraged by strengthening sales
trends that reflect an excitement about
apparel fashion  especially mens and
womens career apparel  that we have
not experienced in a while.
A year ago, when the economy was
not as far along the road to recovery, we
talked about taking advantage of that
down period to concentrate on positioning
ourselves for the impending economic
recovery. We wanted Federated to do the
things necessary to strengthen the basic
structure of our business so we would be
ready when the time was ripe. So we
invested in store reinvent initiatives,
consolidated additional back-of-the-house
functions, and implemented strategic new
approaches to marketing and merchandising.
During this period we also changed
our corporate management structure,
establishing five vice chairs, who report
to me, to oversee strategic areas of the
business  Sue Kronick, our department
stores; Janet Grove, our merchandising
and product development areas;
Tom Cole, our logistics, systems, store
planning and credit operations; Tom Cody,
our legal and corporate administrative
functions; and Ron Tysoe, our real estate
and financial operations.
As we move through the coming months,
we believe Federated is well positioned to
be the nations best and most successful
department store retailer.
One of the key areas on which we are
focusing in 2004 is the Macys Home Store.
Home-merchandise categories  from
home textiles, dinnerware and kitchen
appliances to furniture, bedding and home
accessories  currently comprise about
20 percent of our retail sales, making
Federated one of the nations largest
home-related retailers.
Because we believe there is a significant
opportunity for sales growth and improved
profitability in this category, we determined
earlier this year that these areas would
benefit from a more coordinated, centralized
approach to buying, merchandising and
marketing. So we are in the process of
implementing a centralized home store
strategy in 2004, from which we expect
to realize significant benefits in the
next two to three years.
We also will continue to invest in
our private brands, both in apparel and
home areas, which currently account for
approximately 17 percent of our total
merchandise mix. In addition to the
new I.N.C line for men that debuts this
spring, we are introducing a new home
accessories line in the fall, called Inhabit.
We also will be increasing the number
of stores nationally that carry our new
Hotel Collection brand of luxury bedding
and textiles.
Clearly, there are some exciting things
happening  new directions, new
operations, new horizons of opportunity
that will define what is in the name

Federated. Underlying all of this, however, is
one of the finest retail management teams in
the industry, and the best people in the
business in every category of job. I believe
weve proven this time and again, as Federated
experienced a continuum of change throughout
the last decade to evolve to where we are
today. I am proud of this company, and of the
dedicated people who help to make it what it
is, and I thank each and every one of them
for what they contribute to our company on
a daily basis.
Much of our progress and success as a
company, however, is attributable to Jim
Zimmerman, who retired this year as chairman
of Federated. You can read more about Jim and
his contributions to Federated in the column
to the right, but I want to take this opportunity
to say that I am honored to succeed him and
even more honored to have benefited from
his insight, integrity and experience in the
10 years we worked together.
To you, our shareholders, I say thank you for
your investment in Federated Department
Stores, Inc. We will do our best to be worthy
of your confidence, now and in the months
and years to come.
Sincerely,

TERRY J. LUNDGREN
CHAIRMAN, PRESIDENT &
CHIEF EXECUTIVE OFFICER
APRIL 15, 2004