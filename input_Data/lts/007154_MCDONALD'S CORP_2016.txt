April 13, 2017
Valued shareholders,
Two years can make such a difference  for our customers, and all of us.
Last month marked two years since I stepped into the CEO role with one mandate: Turn around
the business. We committed to moving faster, pushing harder and taking smarter risks  and in
doing so, fundamentally changed the course of our business. Today we have a keen sense of
who we are and what we will do to win back customers.
Weve never been more energised by our potential  and its possible because of the strong
foundation weve built. What were doing in pockets now is indicative of what we can do
around the world in the future. Here are just a few examples: On a farm in Ireland, you might
meet a farmer certified through a national sustainability program, raising high-quality beef for
McDonalds restaurants more efficiently and with a smaller carbon footprint. Inside McDonalds
restaurants in Canada, you might stand alongside a Guest Experience Leader who sets a new
standard for hospitality, helping you order a premium burger at a kiosk. Outside of McDonalds
restaurants in parts of Florida, you might pull out your smartphone to order an Egg McMuffin
(at any time of day) to be delivered right to your doorstep, the ultimate form of convenience.
2016: a Year of Purposeful Change
Last year, we built on the success we started to see in the second half of 2015. We ran better
restaurants, focusing on the fundamentals of quality, service, cleanliness and value. We
restructured. Refranchised. Reduced spending. Returned cash to shareholders. And most
importantly, we grew operating income.
Customers noticed the changes we made. As of late 2016, seven of our ten largest markets had
gained or maintained market share in the Informal Eating Out category. At the same time, we
saw increasing customer satisfaction scores in nine of our ten largest markets. Our brand health
improved too  gaining strength in 2016 and significantly improved over the past few years.
Stronger customer perceptions of McDonalds helped drive stronger business results. 2016 was
our strongest year of global comparable sales (+3.8%) since 2011. Globally, we grew
comparable sales during all four quarters, marking six consecutive quarters of growth.
Operating income increased 8% (11% in constant currencies), and diluted earnings per share
was up 13% (16% in constant currencies)
1. Restaurant cash flows were at record highs in many markets around the world, including the U.S.
Building a Better McDonalds
To continue growing the business for the long term, we need to grow guest counts. In our nine largest markets, we have a 15% share of the 89 billion annual visits to Quick Service Restaurants2. Theres tremendous opportunity to increase our share.
We know that for consumers to choose McDonalds, we need to reach them where it matters most. Thats why we undertook the largest customer research project in our history. The results showed we can bring customers back to McDonalds by doing what weve historically done better than anyone else  serving delicious meals, quickly and affordably.
In every touchpoint with customers, we need to live up to our brand purpose. That means making delicious, feel-good moments easy for everyone  and were moving quickly to do just that. Were focused on actions that tap into our unique competitive advantages, including our iconic brand, unmatched global scale and local community connection.
On March 1 of this year, we unveiled our Velocity Growth Plan for increasing guest counts through targeted actions to:
 RETAIN our customer base and protect our areas of strength, particularly family occasions and food-led breakfast.
 REGAIN customers weve lost through greater emphasis on food quality, convenience and value.
 CONVERT casual customers and fuel growth through greater emphasis on coffee and snacks and treats.
Were concentrating on our biggest opportunities  and stopping what doesnt add to our bottom line. Above all, we remain relentlessly focused on the fundamentals of running great restaurants.
Were also challenging the status quo. We asked ourselves: How can we build on our existing plans and deepen our relationship with customers? Digital and delivery rose to the top as our
two biggest global areas of opportunity. Theyre each big and powerful enough to accelerate all of our efforts.
On digital, were re-shaping customer interactions and energising our existing service models  whether customers eat in, take out or drive thru. We recently committed to bringing mobile order and pay to 20,000 restaurants around the world by the end of this year. Were already running pilots in the U.S., U.K., China and Australia, with plans to scale quickly across other markets. In the U.S. alone, we committed to launch mobile order and pay  as well as curbside check-in  in all of our traditional restaurants by the end of 2017.
Were also well-positioned to take advantage of the significant opportunities that exist with delivery. One of our greatest competitive advantages is that were closer to more customers than any other restaurant company in the world, with nearly 75% of the population in our five largest markets living within three miles of a McDonalds. On top of that, we have 20+ years of delivery experience in Asia and the Middle East. Were working aggressively to identify the right operating models for bringing delivery to more customers  whether we control all aspects of the delivery process from end-to-end or partner with third parties for ordering and fulfilment. We have a number of pilots underway and plan to scale quickly based on results.
At the same time, were accelerating deployment of Experience of the Future (EOTF) in the U.S., building on what weve learned in the U.K., Canada and Australia. Were giving customers greater choice and control over their overall experience: how they order, what they order, how they pay and how theyre served. We will bring the full EOTF package to roughly 2,500 U.S. restaurants by the end of 2017, with the goal of converting most traditional U.S. restaurants by the end of 2020.
McDonalds Next Chapter
In my first letter to you two years ago, I said I looked forward to McDonalds next chapter. I said we would emerge from this time of transition more responsive to customer expectations and market conditions and better able to deliver enduring, profitable growth for our franchisees, suppliers, company employees and shareholders.
Im proud of the progress weve made on that commitment and the solid foundation we built in 2016. Now were ready to pick up velocity and continue growing the business in 2017 and
beyond. As part of that next chapter, weve announced new average annual financial targets
3, beginning in 20194:
 Increase systemwide sales 3-5%;
 Grow our operating margin to the mid-40% range;
 Deliver earnings per share growth in the high-single digits; and
 Raise our return on incremental invested capital target to the mid-20% range.
In addition, we will return $22-24 billion to shareholders for the three-year period from 2017-2019. Looking ahead, we also expect to decrease our G&A and capital expenditures, with our annual G&A run-rate projected to be $1.8-$1.9 billion beginning in 2019 and our capital expenditures projected to drop by $500 million after weve modernised our U.S. restaurants.
I have every confidence well deliver on our growth targets by focusing on those actions that bring the biggest benefit to the most customers in the shortest possible time. Thats the path toward becoming an even better McDonalds. When we combine our size and scale with focus and speed, well be truly unstoppable.
Steve Easterbrook
President and CEO