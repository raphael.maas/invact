To Our Shareholders:
2013 was a terrific year at VF Corporation.
Its difficult to pick one or two things that were most proud of. We turned in a strong financial performance: record annual revenues of $11.4 billion, up 5 percent; record gross margin performance, improving 160 basis points to 48.1 percent; and record adjusted earnings per share of $2.73, representing an increase of 13 percent. VFs reported earnings per share reached $2.71  also
a record. Meanwhile, operating income increased, reaching $1.6 billion, and net income reached $1.2 billion.
Beyond our performance by the numbers, we also
opened an ideally situated  and environmentally responsible  international headquarters in Stabio, Switzerland; successfully integrated the Timberland brands European operations into our international business; completed five SAP implementations that will enable our continued growth in Asia; placed 11th on Chief Executive magazines Best Companies for Leaders, and, once again, were selected as one of FORTUNE magazines Worlds Most Admired Companies.
Simply put, it was quite a year  a year that saw our stock price increase 65 percent, hitting record highs. And thats compared with a benchmark S&P 500 Index that rose
30 percent in its best performance since 1997. To the extent that our share price is a proxy for investor satisfaction and confidence, were pleased with the strong response to our performance and the outlook for our company.
Our dividend remains a high priority for us, and in 2013 we announced a 21 percent increase in our quarterly dividend rate. That marked our 41st consecutive year of higher dividend payments to shareholders. Our track record of returning cash to shareholders is solid. In 2013, we returned nearly $700 million to shareholders in the form of cash and share buybacks.
Powerful Brands. Powerful Platforms.
We define a powerful brand as one that has strong equity with consumers, that has a business model that is capable of self-funding its future and that consistently generates superior returns for our shareholders. In other words, weve set the bar high.
At VF, we have powerful brands. They are diverse,
iconic and positioned to serve customers and consumers around the world. We treasure our brands. We value their uniqueness. And we invest in them, so that  collectively and individually  our brands can continue to engage people wherever they live and however they shop. The insights we have into consumers wants and needs help us innovate, merchandise and tell stories
that bring our brands to life in our consumers hearts
and minds. We work hard to connect with people around
the world. The strength of those connections is measured by our growth.
VFs business platforms also play a vital role in that growth. Across our company, our teams collaborate and share, providing capabilities that include: strategy and innovation counsel, leadership development, direct-to-consumer expertise, supply chain leadership, international know-how and sustainability excellence. Their contributions consistently make our strong brands stronger

Achieving Near- and Long-Term Growth
In 2011, we established a five-year plan that was intended to be in place through 2015. But by early 2013, it was evident that we would surpass many of the five-year goals earlier than planned. The right thing for us to do was to challenge ourselves, to rethink whats possible and to introduce a new five-year plan.
In June 2013, we presented what we call our 17x17 Plan, aimed at achieving $17 billion in revenue by 2017. The plan defines targets for the years 2013 through 2017 and outlines the actions well take to create an even more successful VF.
In summary, starting with 2012 as the baseline, by 2017
we plan to achieve annual:

Revenue of $17.3 billion, representing a five-year compound annual growth rate (CAGR) of 10 percent,
with organic growth representing eight percentage
points and acquisitions representing two points;

International revenues of $7.4 billion, equating to 43 percent of total VF revenues, based on a five-year CAGR of 13 percent;

Direct-to-consumer revenues of $4.4 billion, representing
25 percent of total VF revenues, based on a five-year
CAGR of 14 percent;

Gross margin of 49.5 percent, up 300 basis points compared with 2012;

Operating margin of 16 percent, up 250 basis points compared with 2012;

Earnings per share of $4.50, equating to a five-year
CAGR of 13 percent;

Return on invested capital of 20 percent, up 360 basis points compared with 2012;

Cash flow from operations reaching $2.4 billion, with
a five-year cumulative total of $9.5 billion; and,

An annual dividend of $1.80, at a payout ratio of 40 percent.
We are confident that our 17x17 Plan is achievable. Weve delivered this rate of growth before, and we plan to do it again. Underlying our plan is our focus on four of VFs growth drivers.
First, we will lead in innovation. That means developing
a constant stream of new and better products, new and better store environments, and new and better digital experiences that deliver what consumers want.
Second, we will connect with consumers. Were confident in our ability to do that, not just because we understand our consumers, but also because we listen to them. We ask, and they tell us what they want, what inspires them and how best to communicate with them.
Third, we will serve our consumers directly, wherever and however they want to engage our brands.
Fourth, we will expand geographically. We will grow in more mature markets by leveraging our platforms and extending our strong brands. And we will continue to develop our brands in emerging markets where we have abundant opportunity and the means to capture that opportunity.
One VF: Our Culture, Our Advantage
Before I close, a few words about the business culture that
is a true competitive advantage for VF. Its a culture of respect,
of treating people the way youd want to be treated. Its a culture
of listening more than talking. Its a culture of collaboration and sharing. And it didnt happen overnight. We will turn
115 years old in 2014. Our culture has been carefully nurtured by the tens of thousands of people who came before us.
Its a culture that enables us. It enables us to take risks,
and it allows us to fail  because we know that those around us will help pick us up, dust us off and push us to do better. That VF culture gives us strength; it unites us and gives
us great potential to succeed and grow as One VF.
We sum up our story in six simple words: Powerful Brands. Powerful Platforms. One VF. Those words represent who we are and how we will seize the opportunities ahead. And, after a truly exceptional 2013, those opportunities are exciting.
Im grateful to you, our shareholders, to our 59,000 associates and to our customers and consumers around the globe for choosing to join us on our journey.
Eric C. Wiseman
Chairman, President & Chief Executive Officer