Dear Fellow
Stockholders
Ambac and the financial guarantee industry
faced historic challenges in 2007 and into 2008.
The global credit market distress, the swift
collapse of the subprime mortgage market midway
through the year, and the subsequent rating agency
downgrades of various mortgage-backed securities,
made 2007 the most difficult period in our
37-year history. This reality is reflected in our
financial results, which include the first reported
financial loss for Ambac. Most notably, these events
undermined confidence in Ambacs business
model and in our ratings outlook.
RAISING CAPITAL AND
MAINTAINING OUR RATINGS
Restoring market confidence and maintaining
our important triple-A ratings with Moodys and
Standard & Poors has been our top priority. In
early March 2008, we announced a $1.5 billion
common stock and equity unit offering that was
successfully executed in a difficult environment.
As a result, our capital position has been strengthened
and our triple-A franchise has been preserved.
Our professionals continue to be focused on
actively meeting the needs of issuers and investors,
improving financings with our valuable guarantee.
Throughout the capital enhancement process,
many parties offered us alternatives. Yet, as I have
said publicly, their terms were unacceptably high.
Let me emphasize this important point: solvency
has never been an issue. Ambac has adequate
capital, a strong balance sheet and a steady stream
of future earnings assured through our unearned
premiums. It is inconceivable that any investor
holding an Ambac-insured bond will miss a single
principal or interest payment. We are proud that,
ultimately, we addressed our capital issues with a
market-based solution that balanced the interests
of all our stockholders.
The successful resolution of our capital issue
was testimony to the dedication of my Ambac
colleagues and to the support received from the
Board of Directors, stockholders, policyholders
and clients. I would also like to thank the government
officials, particularly Sean Dilweg and his
staff at the Wisconsin Office of the Commissioner
of Insurance, and Eric Dinallo and the New York
State Insurance Department, for their valuable
perspective and guidance.
REFOCUSING OUR BUSINESS
The capital raise, combined with our strategic
review of all aspects of Ambacs business, will
improve the quality of our portfolio, dampen
earnings volatility, and reduce ratings uncertainty.
In February 2008, we announced a six-month
suspension of new structured finance business in
support of further strengthening our capital
position. We have focused our new business
generation efforts on lower risk and lower
volatility sectors, particularly in the U.S. municipal
business, as well as utilities, infrastructure and
health care in the U.S. and around the world.
Diversity of our portfolio strengthens Ambac,
as acknowledged by the rating agencies, and
our future business will encompass more than
U.S. public finance. We will, however, concentrate
on general obligation transactions and transactions
involving projects essential in nature. We will
also further sharpen our ability to examine
the profitability of individual transactions to
realize adequate compensation for the risks we
are assuming.
We recently implemented significant changes
to our underwriting and risk management systems.
Specifically, we created the position of Chief
Risk Officer reporting directly to the Board of
Directors to ensure that we build a culture that
is grounded upon a fundamental view towards
underwriting and managing risk in our portfolio.
David Wallis, who has done an outstanding job
leading our surveillance operations, is now our
Chief Risk Officer, responsible for three principal
areas: Capital and Risk Analysis, Portfolio Risk
Management, and Credit Risk Management.
This organizational change increases emphasis on
fundamental risk management and places stricter
limits on transaction size and capital usage for
large transactions. These strategy adjustments
will enable us to improve capital management
and will ensure we remain a top-rated source of
financial peace of mind for issuers and investors.
REINVIGORATING OUR
LEADERSHIP POSITION
Since 2007, we have learned valuable lessons about
the complexity and correlation of the financial
instruments and securities structures available in
the market. Since founding the financial guarantee
industry 37 years ago, a balance between growth
and safety has kept Ambacs business on the
right track. The events of the last 12 months
have helped put this balance in the forefront of
everything that we will do. We believe the financial
stress we experienced is limited to a small portion
of our portfolio and does not invalidate the
business model.
Lost amidst the noise of worldwide credit
market distress and Ambacs strategic refinements
was Ambacs strong pace and quality of new
business developed in the first half of 2007.
In fact, 2007 was the companys second best
year in terms of credit enhancement production.
We believe this demonstrates the essential nature
of our business and serves as an indicator of the
enduring strength of our franchise. Global new
business activity reflected the expertise and
teamwork of professionals across all business lines:
Public Finance, Structured Finance and Global
Infrastructure Finance.
RESTORING CONFIDENCE
FOR FUTURE GROWTH
Early in 2008, Robert Genader made a personal
decision to retire as Chairman, President and
CEO. At the Boards request, I stepped into that
role on an interim basis. I would like to thank
Bob for his 20 years of dedicated service to
Ambac and our clients.
In my short tenure as CEO, the most gratifying
factor for me has been the commitment and
excellence of Ambacs people. All of our employees
have dedicated themselves to applying their impressive
array of skills to address the tasks at hand. Their
willingness to go the extra mile throughout this
unprecedented time has been greatly appreciated.
On behalf of all our stockholders, I extend my
thanks to each one of them.
We are optimistic about Ambac and about
our industry. We believe the fundamental strengths
of our business model remain intact. Indeed,
todays environment is an opportunity for Ambac
as current pricing is likely to provide the best
risk-adjusted returns we have seen in many years.
All of us at Ambac are working hard to
rebuild confidence and we are committed to
continuing to provide a high level of transparency
as we seek to restore our stable ratings outlook.
We believe that this clear-sighted goal and our
return-to-basics approach will enable Ambac to
regain its position as a leading financial guarantee
company in the years ahead, assisting issuers, protecting
bondholders and rewarding stockholders.
Thank you for your continued confidence.
Sincerely,
Michael A. Callen
Chairman, President & CEO