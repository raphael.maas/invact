To our shareholders,
At Synovus, our strengths are here.
They�re here with the customers we serve as individuals, families, and business
owners, providing resources, counsel, and encouragement for life�s opportunities and
challenges. They�re here in the communities we�re privileged to serve as neighbors
and friends, with deeply-rooted local leadership and a shared stake in the future.
They�re here with team members, who are empowered by a relationship-focused
culture that is our primary competitive advantage. And they�re here with you, our
investors, who entrust us with your confidence and capital every day
2018 Performance
We built on our strengths � and became
a stronger company � in every respect in
2018. We delivered solid growth in revenue,
net income, and earnings per share, and
we raised the bar on our profitability and
efficiency targets. We increased loans and
deposits while further diversifying our
balance sheet and strengthening liquidity
and credit quality. We completed the
system-wide transition to a single Synovus
brand, and announced and finalized the
largest acquisition in our company�s history.
And we made significant progress in a wide
range of areas � from talent acquisition to
customer experience � that will make us
even stronger in the future.
Total revenues in 2018 were $1.4 billion, an
increase of 10 percent from 2017, excluding
the $75 million Cabela�s transaction
fee(1). Net income available to common
shareholders grew 55 percent to $410
million in 2018, compared to $265 million in
2017, and diluted earnings per share were
$3.47 in 2018, up 60 percent from $2.17
in 2017. Net interest income increased
12 percent in 2018 to $1.1 billion, and
non-interest income increased 4 percent,
excluding the Cabela�s transaction fee(1).
Revenues and profits again grew faster
than expenses. Non-interest expense
increased 1 percent and adjusted noninterest expense(2) increased 4 percent
in 2018 as we continued to spend and
invest judiciously, driving our efficiency
ratio for the year down 196 basis points to
57.99 percent, and our adjusted tangible
efficiency ratio(2) down 354 basis points to
56.33 percent. Return on average assets
was 1.35 percent in 2018, an increase of 46
basis points from 2017, and adjusted return
on average assets(2) was 1.41 percent in
2018, an increase of 37 basis points from
2017. Return on average common equity
was 14.55 percent in 2018, up 523 basis
points from 2017, and adjusted return on
average common equity(2) increased 443
points in 2018 to 15.29 percent. 

We returned $281 million to common
shareholders in 2018 with $106 million in
common stock dividends and $175 million
in common stock repurchases, and we
expect to return approximately $500 million
to common shareholders this year through a
20 percent dividend increase and additional
common stock repurchases.
Total loans were $25.9 billion at the end of
2018, an increase of 5 percent from $24.8
billion at year-end 2017. Commercial and
industrial (C&I) loans grew 6 percent to
$12.8 billion at year-end 2018 from $12.0
billion at the end of 2017. C&I loan growth
continued to benefit from our nationally
recognized senior housing team, as well as
from Global One, our life insurance premium
financing division, which more than doubled
its loan volume in 2018. Consumer loans
totaled $6.6 billion at the end of 2018, up
13 percent from $5.9 billion, with growth
across all consumer lending categories
during the year. Commercial real estate
(CRE) loans ended 2018 at $6.6 billion,
down 5 percent from the end of 2017, as we
continued to see a higher velocity of paydowns and pay-offs.
CRE has declined from 44 percent of our
loan portfolio at the end of 2008 to 25
percent of total loans at year-end 2018, with
C&I lending increasing to 49 percent and
consumer loans increasing to 26 percent.
As a result of our balance sheet de-risking
and credit discipline, non-performing asset
and loan ratios remained at or near historic
low levels. Non-performing assets were
$114 million at the end of 2018, down 12
percent from $131 million at year-end 2017,
and the non-performing asset ratio declined
9 basis points from the end of 2017 to 0.44
percent at December 31, 2018. The net
charge-off ratio was 0.20 percent in 2018
compared to 0.29 percent in 2017.
The common equity Tier 1 ratio was 9.95
percent at year-end 2018, compared to 9.99
percent at December 31, 2017, and the total
risk-based capital ratio was 12.37 percent
at the end of 2018, versus 12.23 percent at
year-end 2017.
Total average deposits in 2018 were $26.3
billion, up 4 percent from $25.4 billion in
2017. Average core deposits(2) increased
3 percent to $24.5 billion in 2018 from
$23.8 billion in 2017, with average growth
in money market accounts, non-interest
bearing demand deposits, and time
deposits of 5 percent, 4 percent, and 4
percent, respectively, for the year.
Our fiduciary, asset management,
brokerage, mortgage, and insurance lines
of business also grew in 2018. With the
addition of new talent and new markets,
retail brokerage assets increased 48
percent from 2017 and revenues from our
fiduciary, asset management, brokerage
and insurance business increased 14
percent. And in a challenging year for
the mortgage industry, total consumer
mortgages increased 11 percent from 2017,
and the number of closed portfolio loans
increased 14 percent year over year.

Consumer banking benefited from the first
full year of our refreshed credit card suite
of products, with new credit card accounts
increasing nearly 170 percent in 2018.
Consumer banking also saw solid early
results from new marketing programs and
a new mass-market-focused free checking
product, with consumer checking sales in
the second half of the year increasing more
than 40 percent compared to the second
half of 2017.
Transformational Milestones
In June, we wrapped up the transition
of 28 locally branded bank divisions to
the Synovus brand. After 18 months of
planning, 250 branches, 3,500-plus
signs and thousands of check kits, name
tags, and business card changes, it�s
impossible to overstate the importance of
the single-brand project to the future of
our bank. It went off without a hitch, and I
couldn�t be prouder of the entire Synovus
team for completing this immensely
complex project with the greatest efficiency
and effectiveness.
In July, we announced the acquisition
of Florida Community Bank (FCB), the
number one Florida-headquartered,
Florida-focused community bank. The FCB
transaction made Synovus a $45 billion
bank, with a very strong presence in the
fastest-growing markets in the country�s
fourth-largest state, including all top 10
Florida metropolitan areas. The addition
of FCB � with its complementary culture,
capabilities, and commitment to service �
significantly increases the value-creation
potential we offer customers, communities,
and shareholders. It also increases Florida�s
contribution to our overall business by
extending our footprint in a state where
we�ve operated for more than 30 years. The
FCB acquisition closed January 1, 2019, and
we expect platform, systems, and brand
conversion in the second quarter of 2019.
The speedy integration of FCB will
benefit enormously from the experience
we developed during the single-brand
transition, and is an excellent example of
our ability to leverage cultural synergies
� within Synovus and with our new team
members from FCB � and to operate as a
single organization. These two strengths
were increasingly apparent in 2018
following the single-brand transition, as
team members could more clearly promote
our full capabilities and more deeply
connect with internal partners to serve
clients as one Synovus.

The recognition we received in 2018
provided further affirmation of another
strong year for our company. We won 22
awards in the 2018 Greenwich Excellence
Awards for Small Business Banking and
Middle Market Banking and four awards in
the 2018 Greenwich Best Brand Awards.
For the fourth consecutive year, we were
named one of America�s most reputable
banks by American Banker and Reputation
Institute, and again ranked in the top 10
among both customers and non-customers.
We were also named one of American
Banker�s �Best Banks to Work For.� This
list has long been dominated by smaller
community banks, so our appearance on
it was a strong validation of the peopleand service-focused culture we strive to
cultivate internally, and extend outward to
our customers and our communities.
Well-positioned to Compete
The progress we made in 2018, and the
ambitious plans we�re pursuing this year,
build optimism about our ability to achieve
our refreshed three-year financial targets:
10 percent-plus earnings per share growth,
1.45 percent return on assets, 17 percentplus return on average tangible common
equity(2), and an adjusted tangible efficiency
ratio(2) at or below 50 percent.
Creating better customer experiences
will be a major area of concentration in
2019, beginning � as this letter goes to
press � with the launch of My Synovus,
our all-new online banking platform and
mobile app. We�ve also added a new
online mortgage lending portal to our
digital product set, and we�ll soon offer
an online consumer lending product.
Progress continues in leveraging our
high-touch, highly personal delivery model
to strengthen our offerings and service
to affluent customers who are at various
stages of reaching their financial goals.
We will continue to invest prudently in these
and other initiatives that enable organic
growth in 2019. Efforts to deepen our share
of wallet with existing customers � and
acquire new ones � include accelerating
the work we�ve already done to re-build our
retail product offerings, aided by the use
of more and predictive data to strategically
guide investments in a differentiated
customer experience. We also strengthened
the way we�re reaching customers and
prospects in our markets by more closely
aligning our community, retail, and
wholesale banking, wealth management,
IT, and operations expertise under the
leadership of Chief Operating Officer Kevin
Blair, newly appointed to the role in late
2018. We expect this unified team to better
leverage our internal partnerships and
shared market relationships and intelligence
to achieve stronger growth in high
opportunity markets and segments.
As a relationship bank, our products and
services are only as effective as our people.
In 2019, we will continue to develop the
skills and capabilities of current team 

members and acquire additional talent
needed to serve customers in new
geographies and new specialty lines of
business. We remain committed to bringing
varying perspectives to the table, and
accurately reflecting the communities and
customers we serve, so our efforts and
focus on ensuring an inclusive and diverse
team will only accelerate with our continued
growth and expansion.
And as always, we will continue to
demonstrate throughout 2019 that Here
Matters in every community in which
we operate. Synovus team members
volunteered nearly 30,000 hours through
more than 4,300 Here Matters opportunities
in 2018. We collectively contributed more
than $915,000 to United Way, and provided
$127,000 in scholarships to 126 students
through our Jack Parker Scholarship Fund
that benefits children of Synovus team
members. The company�s philanthropic
giving exceeded approximately $2 million
to more than 500 non-profits and agencies
doing worthwhile work across our footprint.
I close by thanking Cathy Allen, Jerry Nix,
Dr. Mel Stith, and Phil Tomlinson, who will
retire from our Board of Directors as of the
2019 Annual Shareholders� Meeting. All four
have demonstrated steadfast leadership
over the years and we are very grateful for
their wisdom, guidance, and dedication. Our
company is certainly better for their service.
Finally, I thank our team members,
customers, and shareholders for your
loyalty, trust, and investment in our future.
You are enduring sources of strength for
our company, providing the energy and
motivation every day to become a better,
stronger bank for you in 2019 and for years
to come. We are beyond grateful for your
continued support.
Sincerely,
Kessel D. Stelling,
Chairman and Chief Executive Officer