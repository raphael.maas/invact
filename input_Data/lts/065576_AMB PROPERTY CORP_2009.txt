The past year was one of the most challenging periods in our companys
26-year history, as we faced a severe global economic downturn and
the broadest decline in the financial markets in decades.
You may recall in last years letter my acknowledgment of what we felt at
the time were the beginning stages of a temporary interruption in global
trade. We viewed the dramatic dip as a brief pause in an otherwise
long-standing secular trend and we still believed fervently in the drivers
of our business. It was also an opportunity for us to test our investment
thesis and our operating model.
At the time of this writing, the leading indicators of customer demand
appear to be recovering, and there are clear signals that global trade is on
the cusp of a substantial upswing. Importantly, we are seeing increased
interest both from our private capital investors and our customers, as they
actively reengage in discussions and decisions regarding new investments
and space requirements.
Careful Plan Achieved in a Difficult Year
Late in 2008, addressing the escalating economic crisis, our senior team
devised a strategic plan designed to strengthen AMBs financial footing,
reduce our cost structure and set up the company for future growth
opportunities. Subsequently, during 2009 we executed on the key
priorities identified in the plan, and the results of these efforts helped
us close out the year on a decidedly positive note.
We worked hard to ensure that AMBs financial position was one of the
strongest in the industry, and to that end we completed $2.7 billion in
capital market transactions. Notably, we closed on more than $763 million
in dispositions and contributions at a weighted average cap rate of 6.8%,
raised more than $550 million in a common equity offering, issued $500
million of senior unsecured notes and repurchased $214 million in bonds
The result of this activity decreased our share of debt to assets from
51% to 44%, trimmed the companys share of debt by more than $700
million and increased liquidity to $1.4 billion.
In addition, we pushed forward on a broad-based cost reduction plan
that began in the fourth quarter of 2008. Consequently, we reduced our
total global workforce by approximately one-third and cut gross general
and administrative costs by approximately $60 million on a run-rate
basis. The decision to reduce our workforce was difficult but made for
all the right reasons; essentially to build a leaner and stronger company.
I wish I could say that this fact made the process easier for everybody
involved, but it did not.
It is worth noting that while we removed excess capacity from our capital
deployment teams, we cut costs without curtailing our global acquisition
and development resources; thus our capacity for growth remains
intact. In many cases we were able to retain key people in our most
productive platforms by temporarily reassigning them to related leasing,
operations and customer service functions as we endeavor to complete
the build-out and lease-up of our development portfolio.
In a year when market pressures were intense, we remained focused
on delivering consistent long-term results for our investors, and on
supporting our customers throughout the crisis. Our accomplishments
in 2009 were the upshot of a dedicated and engaged team managing
through adversity with conviction and optimism. In the spirit of lessons
learned, we will remain vigilant about preserving our balance sheet and
cost structure regardless of the economic landscape. As I reflect on the
circumstances of the past twelve months, I am convinced that we were
able to quickly formulate and decisively execute a strategy in the midst
of such a volatile environment because of the cohesive strength and
depth of our team. In short, everyone pulled together and as a result
AMB is in an excellent position to consider new growth initiatives.
Growth Priorities Have Been Established for 2010
Our collective mindset going into 2010 is indicative of a resilient,
energized and more efficient organization functioning in a rebounding
market. In order to capitalize on our position we have established three
key growth priorities for 2010.
First, we will improve the utilization of our existing assets by increasing
occupancy in our core operating portfolio closer to our historic average
rate of 95%, by completing the lease-up of AMBs development portfolio,
and by realizing value from our land-bank through the formation of new
co-investment ventures, sales and build-to-suit projects.
Second, we intend to invest in high-quality, well-located industrial
assets with total returns in excess of our cost of capital. Deal flow
appears to be ramping up, and while we are currently evaluating
multiple opportunities across the globe, we will be patient and deliberate
in our approach. Our strong balance sheet and attractive currency put
us in an excellent position to provide solutions to institutional investors,
private developers and lenders who find our operating partnership
structure an attractive way to achieve their de-levering and tax objectives;
all of which provides us with access to a proprietary pipeline. We also
believe that some of our best investment opportunities can be found
in our own open-end funds, where we have recently added to our
ownership on an accretive basis.
Our third growth initiative is to form new private capital vehicles. We are
actively engaged in discussions with a number of institutional investors
across the globe who would like to put their money to work in the
logistics sector. They are under-allocated in industrial real estate and
see attractive values in our strategic locations. Recent market activity
reflects an increasing level of investor interest in focused operators with
stable operating platforms and experienced management teams.
Interestingly, returning investor demand is being met by a scarcity of
product. These conditions are providing a strong floor on valuations
and bode well for AMB and our co-investment partners.
Another focal point for the year will be to complete our cross-functional
effort that we launched in 2009 to develop world-class operating and
internal reporting systems. This initiative will improve the efficiency, cost
structure and scalability of our back-office operations. Upon completion
we expect both to realize an additional $5 million in annual savings and
improve our decision-making tools.
Long-term Prospects Are Positive for the Logistics Space
We believe that the key aspects of our business are on solid ground
and that we are in a very competitive position. We remain optimistic
about the return to growth in 2010 as well as the long-term prospects
for our business.
Indicators for a global economic recovery are pointing up. Current
consensus forecasts for global GDP growth in 2010 stand at approximately
4%, which is a level that implies a solid recovery, sets the stage
for a sharp rebound in trade and an improving demand for our product,
particularly in our seaport and airport markets.
We believe that the global supply chain and logistics real estate are at
an inflection point. While many economic indicators are turning positive,
we continue to operate in a demanding and competitive environment as
the market absorbs the current supply of vacant space. Thus, we expect
occupancies to bounce along near current levels for the first part
of 2010 before showing signs of improvement in the latter half of the
year. This will be followed by a continuation of rent roll-downs, which
we anticipate seeing for some time, but market rents should begin
increasing by the end of 2010.
Replacement costs, however, will drive value in the long term. The
situation today is that there is no new product coming on line and
construction is not economically feasible until rents go up significantly,
driven by an uptick in demand, which we think is inevitable, particularly
in the key markets tied to global trade. As this comes to fruition, we
think our global footprint, strategic locations and focused strategy will
further differentiate AMB from our competitors. In other words, the
rising tide is not going to raise all boats equally.
In September we announced the retirement of John Roberts, an
esteemed colleague and friend who over the years helped AMB
in a variety of capacities, most recently as president of the companys
private capital group. He will be missed not only for his contributions to
the bottom line but also for the positive impact he had on our corporate
culture. At the beginning of 2010, leadership of the private capital group
was transitioned to Guy Jaquier, a 10-year member of our executive
management team who has spearheaded much of the companys
expansion outside the United States. Guy will continue to quarterback
the companys efforts in Europe and Asia, where he is supported
by an outstanding lineup of managing directors, and I have no doubt
that he will guide our talented private capital group to new heights.
As we enter 2010 against a burgeoning macroeconomic backdrop,
we are pleased to be in a solid financial position and excited about the
opportunities for growth. The recent recession has offered us the
challenge of a lifetime, and I believe that we will be a healthier company
when the dust settles. Our platforms, people and capital around the
world are prepared to continue to support the activities of our customers,
produce leading returns for our investors and further position AMB
as the leader in global industrial real estate.
Thank you for your continued support and confidence.