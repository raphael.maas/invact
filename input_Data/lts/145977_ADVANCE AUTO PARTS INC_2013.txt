Letter to ourSTOCKHOLDERS
I am pleased to present to you our 2013 Annual Report. I would like to thank all our  Team Members for their hard work and  for their commitment to better serve our customers and grow our business during the past year.   In  2013,  Advance  achieved  record  sales  of $6.5 billion, strong operating income and consistent free cash flow generation. 2013  was  also  a  historic  year  as  the  Company  took  a  major  strategic  step  forward through the acquisition of General Parts International, Inc. positioning us for sustainable  long-term  growth  and  value  creation  as  the  largest  parts  provider  in  the  automotive  aftermarket  industry.  We  closed  on  the  transaction  just  after  our  2013  fiscal  year  end.  In  addition  to  the strategic and financial merits of the acquisition, our confidence and conviction was reinforced by our December 2012

acquisition  of  BWP    the  second  largest  operator  of  owned  and  independent  Carquest  stores.  As  a  result  of  the  acquisition  and  subsequent  integration  of  BWP,  we  had  the  opportunity to begin working with General Parts which  enabled  us  to  gain  valuable  insights    regarding  our  potential  combined  strengths.  I  am confident and excited about the opportunity this combination presents for our shareholders and our more than 71,000 Team Members.Looking  back  at  2013,  our  focus  and  related  initiatives were concentrated on execution of our fundamentals aimed at driving our sales growth, improving our customer service and growing our profits. We made significant strides in our overall operations with in-store execution improvements and meaningful steps toward improved efficiency and  effectiveness.  Specifically,  we  have  been  simplifying  how  we  run  our  stores,  driving  productivity  improvements  and  investing  in  our  Team Members through comprehensive product and  leadership  training  programs.  Overall,  we  are very proud of the progress we made as we remained  focused  on  execution  and  the  team  worked  with  intensity  to  improve  our  customer  experience and deliver on our priorities.

Our  Commercial  business  saw  positive  sales  gains resulting from national and regional account growth,  incremental  growth  in  our  e-commerce  business  and  continued  positive  response  to  the leading e-services we offer. Delivery speed and reliability continued to improve across the board. We continued driving improvements to our in-market availability and assortment through thesupport  of  374  HUB  stores  and  the  positive  impact from our daily delivery capabilities from our Remington, IN distribution center. Together, Advance Auto Parts and Autopart International opened 172 new stores in 2013, a 35 store increase over 2012 openings. The performance of our new stores positions us well for 2014.

Strategically,  our  relentless  pursuit  of  growing our Commercial business advances forward   through   the   acquisition   by   adding  1,248  Carquest  company - owned  stores,  105  Worldpac  branches  and  servicing  approximately  1,400  Carquest  independent  customers.  During  2013,  we also made progress on the integration of  the  124  acquired  BWP  stores.  The  newly  combined  business  will  provide  a  platform  for  continued  growth  of  our  Commercial  business  and  enhance  our  competitive  position  in  DIY.  We  will  also  be  able  to  solidify  our  market  position  and  increase  service  levels  to  a  broader  and  more  diverse  customer  base  in  the  industry.  The  acquisition  increases  our  reach with immediate coast- to - coast and  North American market coverage. It also expands  our  platform  to  include  the  important  independent  customer  channel  while  building  on  our  commitment  to  national  accounts,  large  bay  garages,  heavy-duty, fleet and government programs.

As we combine the institutional experience and capabilities of Advance including our in-market inventory availability, technology, store portfolio and DIY experience together with General Parts, we will be able to leverage our collective size, scale and capabilities to serve our existing customers better than ever and uniquely position us to acquire new customers. We  are  also  very  excited  by  the  addition  of  General Parts talented and experienced senior management team. The General Parts team brings with them significant commercial industry experience, institutional knowledge and long-standing commercial relationships. With over 130 years of combined company experience, Advance and  General  Parts  have  parallel  histories  of  successful growth through acquisitions supported by outstanding Team Members. Culturally, our two companies  share  complementary  operational  strengths and values. These cultural synergies and common values are critical elements of success  in  any  acquisition  and  we  are  well  positioned as we look ahead. At Advance Auto Parts, we believe that service extends beyond our doors into the communities

we live and operate in. We take pride in our ability to positively impact so many lives and also consider it one of our greatest responsibilities. Just as our operations are driven by our values, so is our commitment to serving our communities and improving the lives of those in need. In 2013, we were privileged to give back over $8 million to partner organizations that align with our four  focus  areas:  Health;  Arts,  Education  and Culture; Serving Military and Civilians in Need; and Disaster Relief.We look for ward to 2014 with excitement as we continue toward our destination and never ending mission to be the best. We will build on our 2013 progress as we focus on continued sales growth, serving our customers and improving our profits as we leverage the size and scale of the combination to deliver on the compelling financial benefits of the General Parts acquisition.

Sincerely,Darren R. Jackson, Chief Executive Officer
