Dear Shareholders:
As I look back on the past year, I am gratified by the outstanding efforts of our Associates and
all we have accomplished. The 7,000 plus Associates of Lennar have guided our Company to
another year of growth and record-breaking business performance. Lennar, is indeed a
Company driven by the strength, depth and tenure of its people, and I would like to thank
each and every Lennar Associate for their exemplary commitment to excellence in 2001.
We solidified our leadership position in the industry by generating $6 billion in revenue, net
earnings of $418 million, and earnings per share of $6.01. We enhanced the strength of our
balance sheet with an almost 20% return on net capital; our $1 billion credit facility paid
down to zero; $824 million of cash at year end; and a net debt to total capital ratio of 29%.
This performance has positioned us well in a homebuilding marketplace that is poised for
consolidation.
Over the past 10 years the largest homebuilders have experienced consistently strong EPS,
revenue and deliveries growth. They are now positioned with strong management teams and
liquid balance sheets. These larger homebuilders will continue to turn size into bottom line
benefits through reduced construction costs, Internet sales, branding potential and additional
improvements in capital structure. With industry-wide EPS multiples averaging less than
10x and consistent EPS growth of over 20% compounded annually, we are confident that
the marketplace will continue to take notice of the investment opportunities of large scale
homebuilders. Lennar's performance among this elite group speaks for itself. Lennar is an
industry leader.
Our defined business process, diversification, and strong balance sheet are what continue to
drive our performance. We are focused on simplicity and consistency with all of our divisions
being measured on the same standards and all contributing to the bottom line. Our growth
is driven by and dependent on the strength of our balance sheet; and our balance sheet is very,
very strong.
Our due diligence and bottom line expectations have resulted in successful acquisitions,
including eleven acquisitions since 1997 - with U.S. Home being the pinnacle and Patriot
Homes in Baltimore and Don Galloway Homes and Sunstar Communities in the Carolinas
being the most recent. These acquisitions have uniquely positioned us as one of the most
diverse builders in the marketplace. Our product offerings now range from homes for firsttime
buyers to homes for the active adult. Our price points range from under $100,000 entrylevel
homes to over $1 million ocean view homes. Our dual marketing strategy of both Design
StudioSM and Everything's IncludedSM provides us a vehicle for internal growth, faster and more
efficient use of our net assets and a larger market share capture. Our geographic diversification
now finds us in 16 states and 44 metropolitan areas from the east coast to the west coast.
While we are proud of our performance and financial accomplishments, 2001 was a year filled
with a number of challenges as well.

As a Company we lost our co-founder and valued member of our Board of Directors, Arnold P. Rosen. Arnold was a
mentor to me personally as I learned the real estate business and grew through the ranks of Lennar. Arnold set many
of the standards of business and conduct to which we adhere as a Company today, and his rich tradition of personal
philanthropy is ingrained permanently in the Lennar culture of helping our community benefit as we grow in size and
stature as a Company. Lennar has benefitted greatly from Arnold's inspiration as co-founder of the Company, his
knowledge and work ethic in management, and his personal integrity and decency in guiding our business. Arnold's
presence will be missed by all of us here at Lennar.
National events have also presented challenges in 2001. Our country has weathered a national economic recession,
which has raised questions of cyclicality in the homebuilding industry. Additionally, America as we know it changed
in light of the tragic events of September 11th. In responding to these challenges, we have found the strength of culture
and conviction that make us uniquely Lennar.
Throughout 2001, we have moved our business forward with our heads, while we acted with our hearts. The events
of September 11th challenged us to revisit those values that make corporations succeed, or fail. In the hours and days
immediately following that tragic day, our management team revisited priorities and expectations for the future. Our
primary goal was to ensure that all 7,000 plus Associates of the Lennar Family of Builders and Financial Services
remembered that our Company is a part of a great American society, and that each of us, while experiencing the pain
of that tragedy, also realized that we could be a part of the healing process. We collectively, and each individually,
assumed leadership roles in our industry, our Company, and within ourselves to demonstrate beyond a doubt the
meaning of Caring - the Lennar Way.
Caring - the Lennar Way has meant many different things to us.
Shortly after September 11th Lennar was among the first companies to participate in helping those directly affected by
joining with our fellow high production homebuilders and the National Association of Homebuilders in creating The
Homebuilders Care Victims Relief Fund, with a contribution of over $1.3 million from the coffers of our Company,
and the hearts of our Associates. In addition, this past holiday season we all joined in a program we called Lennar's
Random Acts of Caring. All of our 60 operating divisions sought out worthy charitable organizations to focus their
attentions on and to make a difference. Whether it was a child with cancer being attended to in California, a Toys-for-
Tots program in Texas, or working with senior citizens in Florida, our Associates chose to make a difference. This
Random Acts of Caring initiative was highlighted by the completion of our 500,000th home in Miami, Florida - a home
built together with Habitat for Humanity and Special Olympics and donated to a family with special needs.
We also care for business and our Associates. There was little question that our rate of sales would be impacted. We
searched for ways to reduce the costs of constructing our homes, and ways in which we could eliminate non-essential
costs of conducting business. We examined our expenditures for land and we reviewed our levels of inventory while
seeking to increase our closings. We took all these actions because they were good business decisions. But, we were also
driven to fulfill our goal of maximizing employment opportunity for all our Associates while still protecting the very
franchise that we and those before us have spent almost 50 years creating. Our goal was that not one job be lost by a
Lennar Associate due to the events of September 11th, while carefully protecting the investment of our shareholders.
We are proud that both goals have been met.
As we commence 2002, we expect that overall industry starts may be down due to the national economic recession and
overall consumer uncertainty. Nevertheless, Lennar is a large public homebuilder with an excellent track record that is
committed to growing our Company, as well as earnings for our shareholders. We see a marketplace where supply is

limited relative to demand. Inventories of completed homes are at record lows and the availability
of new homes is constrained due to the difficulty of entitling land. Americas used
housing stock is aging and new homes are more attractive than ever. On the demand side,
interest rates remain low, keeping homes affordable and home ownership rising. Household
formations are projected to continue to grow steadily due to immigration. And, the national
economy is projected to improve as consumer confidence strengthens. With 128,000 homesites
owned or under our control at year end, and the highest level of capital liquidity in our
history, we are positioned to continue the growth of our Company.
In conclusion, I am most pleased with the balanced approach to managing our Company
that continues to drive Lennar. It permeates our Company, focus and culture. Lennar
continues to excel within our industry and within the business world as a leader by all
financial metrics. At the same time, we have been and remain passionate about caring for
our families, communities, and country.
2001 has given all of us cause to reflect. We are all much more focused on family much
more focused on giving back to our communities much more focused on making each
minute of our lives count. We at the Lennar Family of Builders and Financial Services are
no strangers to this way of thinking - for this way of thinking has been at the very heart of
Lennar for almost five decades. That is why it is with pride, commitment and a sense of
responsibility that we operate under the banner: America Calls Us Home.
Sincerely,
Stuart Miller
President and Chief Executive Officer, Lennar Corporation