Dear Fellow Shareholders,
This past year was one of substantial change for the healthcare
industry, bringing uncertainty to the market place while
creating opportunities for HCP to help our clients address the
significant new challenges they are facing.
OPERATIONALLy AND FINANCIALLy,
2013 wAS A SOLID yEAR FOR HCP:
 Achieved 8% growth in FFO as adjusted per share
and 14% growth in FAD per share, a measure
supporting consistent dividend growth
 Generated solid 3.1% same store cash NOI growth
 Raised $800 million of 10-year bonds at an
attractive 4.25% coupon
 Continued to improve our credit profile by reducing
financial leverage to 39%, secured debt ratio to 6.8%
(the lowest level in over five years) and Net Debt to
adjusted EBITDA below 5.0x
 Completed approximately $600 million of investments
 Delivered $957 million to shareholders in the
form of dividends
 Results enabled a 3.8% increase in the
2014 dividend in January of this year
HCP has delivered 29 years of consecutive dividend growth,
a source of tremendous pride for our Company and a reflection
of our commitment to growing shareholder value. As a further
testament to this extraordinary achievement, HCP continues
to be the only REIT in the S&P 500 Dividend Aristocrats Index.
Looking ahead, it is increasingly clear that the opportunities
for us are compelling as healthcare service providers need
HCPs responsive capital to compete, grow and thrive in a
rapidly changing industry. As our clients grapple with the
uncertainty resulting from the Affordable Care Act and other
aspects of healthcare reform, HCP, as a knowledgeable
participant in the healthcare industry, stands ready to fund
their capital requirements and facilitate the implementation
of their strategic decisions. We are positioning HCP as the
preferred capital partner for our clientsone that can increase
their financial success because we understand and support
their changing business needs.
My focus and top priority is smart investment growth, and
we are moving in several new directions to achieve this.
In the fourth quarter of 2013, our professionals and I met with
more than 50 hospital administrators and 40 major tenants
to better understand their operational strategies and to
discuss approaches and solutions to improve their financial
performance. We explored various factors impacting their

businesses, including service and operating issues, tenant
mix, changing doctors needs and the design of our buildings.
We worked with Tenet Healthcare to reverse its decision to
exercise purchase options and instead extend its leases on
three of our acute-care hospitals. This enabled Tenet to focus
on its recent acquisition of a $4 billion hospital system while
allowing HCP to preserve $23 million of recurring annual rent.
The result was a financial win-win for both of us, further
cementing our long-term relationship.
Similarly, we worked with Kindred Healthcare to swap assets
that were not in its core markets for assets that were a better
strategic fit. Furthermore, we recently worked with Brookdale
Senior L iving and Emeritus Corporation in conjunction with
their pending merger, which upon completion will create the
largest senior housing operator in the United States. HCPs
significant, long-term relationships with the teams at both
companies provide a solid platform for future growth.
Finally, we have expanded our acquisition teams to have more
touch points in todays ever-changing market place. We are
targeting a more expansive pipeline of transactional prospects
across a broader range of operators. We still like and will
continue to pursue large transactions, but we will also seek
to expand our portfolio to include a more diverse platform of
operators as we look for continued investment growth.
Please allow me a moment of personal reflection. Five
months ago, I accepted the Boards invitation to become
President and CEO, confident that HCP is the countrys finest
healthcare REIT. I am proud to have been chosen to lead
an organization with a well-deserved reputation for market
responsiveness, a strong investment grade balance sheet and
proven investment expertise to maximize the value-creation
potential for shareholders. My experience spending five years
on HCPs Board has enabled me to step seamlessly into the
leadership of HCPs talented team without skipping a beat, with
HCP continuing to deliver outstanding financial and operating
performance.
I am even more optimistic today about HCPs strengths and
am confident that we have the talent, expertise, nimbleness
and balance sheet to cultivate mutually beneficial partnerships
with our clients that will not only increase the prospects for
their success, but also deliver consistent investment growth
and drive long-term value for HCPs shareholders.
We appreciate your continued support and confidence in HCP.
Lauralee E. Martin
President and Chief Executive Officer