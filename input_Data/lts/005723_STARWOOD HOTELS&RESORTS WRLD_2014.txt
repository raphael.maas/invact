DEAR FELLOW
STOCKHOLDERS

As a member of Starwoods Board of
Directors for more than eight years, and with
deep hospitality experience in my career
heretofore, I was called upon earlier this
year to take on the role of CEO at Starwood
on an interim basis. With our global team
of talented associates, we will fulfill the
mandate given by the Board to drive change
at the company.
Our Board is conducting a comprehensive
executive search to determine the right
candidate to lead our company over the
long run. In the meantime, we have a clear
mission to improve operational efficiency
throughout our global network and to
accelerate Starwoods growth. Our bias
is towards action, and we are intent on
optimizing stockholder value.
Looking back over the past year, there was
much to be proud of in 2014. Starwood
delivered another year of growth in our
revenue per available room (RevPAR), and
both adjusted EBITDA and earnings per
share (EPS) were ahead of our expectations.
Across each of our three global divisions,
we posted rising RevPAR Index, which
means increasing market share and is a
sign of the global strength of our worldclass
hotel brands. Starwood also achieved
our second-best signing year ever for new
hotels to open in the future.
Starwood made significant progress in
delivering on our asset-light strategy in
2014. We continued the sale of hotel assets,
while retaining long-term management or
franchise contracts, including trophy assets
like the Sheraton on the Park in Sydney
and The St. Regis Rome. Plans were also
laid in 2014 to take another major step
toward becoming asset-light, with the
announcement a few months ago of our
intent to spin-off our vacation ownership
business. This is expected to be completed
later in 2015.
In 2014 we returned approximately $2.4
billion to stockholders through our dividends
and stock repurchases and, in doing so,
reduced the number of shares outstanding
by 10%. We were able to deliver this return
to stockholders by increasing our leverage
levels to reach our long-term leverage
target, cash flow from operations and cash
proceeds from asset sales.
Heres a look at 2014 by the numbers:
 Grew Same-Store Worldwide Systemwide
RevPAR by nearly 6% in constant dollars
and management fees, franchise fees and
other income by 9.5%
 Opened 74 new managed and franchised
hotels with approximately 15,000 rooms
 Signed 175 deals for new hotels, more than
any year since 2007
 Sold eight wholly-owned hotels and one
unconsolidated joint venture for gross cash
proceeds of approximately $817 million
 Returned approximately $2.4 billion to
stockholders through dividends and stock
repurchases
Our approximately 180,000 associates in
some 100 countries are core to our success,
and our achievements in 2014 would not
have been possible without their dedication
and engagement.
EXPANDING OUR FOOTPRINT
AND PIPELINE
In 2014, we opened 74 hotels, with
approximately 15,000 rooms, of which 51
were new hotels and 23 were conversions
of hotels from other brands. Net of hotels
that left our system, we increased our
global footprint by 2%, which was below
our long-term growth target of 4% to 5%.
Building on the strength of our 175 signings
for new hotels in 2014 and our pipeline of
480 hotels with 108,000 rooms, our focus
in 2015 is to accelerate the growth of our
footprint back to our long-term target. We
are deploying additional development
resources in key markets around the
world, focusing on growth segments and
conversion opportunities.

Building on The Luxury Collections success
in the luxury segment, we are launching
the Tribute Portfolio, a collection brand
that will enable us to bring high-quality
independent hotels into our system that
dont naturally fit with our existing nine
lifestyle brands. However, we believe these
hotels will appeal to our Starwood Preferred
Guest (SPG) members and should be a
considerable source of growth for Starwood
in future years.
We are also working closely with key
developers to use our balance sheet in
creative ways to drive unit growth.
DRIVING TOPLINE GROWTH
AND PURSUING OPERATIONAL
EXCELLENCE
A key part of our strategy is to enhance the
unique character and consumer appeal of
our world-class brands. While our RevPAR
Index gains during 2014 demonstrate
that we are making progress, there is
nonetheless much more to do. Particular
focus in the coming months will center
around each hotel brand with a renewed
marketing and sales push. Our aim is to put
them more in the spotlight that they deserve
on the world hospitality stage.
Combined with our topline initiatives, we
can achieve a higher level of operational
excellence, both at our hotels and in
our corporate structure. Ensuring that
our system costs and corporate costs
lead to Starwood being an efficient and
competitive hotel operator is critical in
delivering value to our hotel owners and
driving stockholder value. In 2015, while
striving to improve our hotel offerings for
our guests and customers, we also will be
aggressively managing our costs.
EXECUTING ON OUR
ASSET-LIGHT STRATEGY
We advanced our asset-light strategy
during 2014 with the sale of eight hotels and
one unconsolidated joint venture for gross
proceeds of approximately $817 million.
With those asset sales, and the hotels
we intend to contribute to our vacation
ownership business as part of the spin-off,
we are approximately 40% of the way to the
goal of generating $3 billion in asset sales
by the end of 2016. Looking ahead to the
rest of 2015, transaction markets continue
to look strong and demand for our hotels
remains high.
LOOKING AHEAD
Looking ahead to 2015 and into
the foreseeable future, we expect a
favorable climate for continued growth in
global lodging.
Prospects for the U.S. economy lead to
optimism for the hotel business in the
United States and throughout the Americas,
and limited new supply points to rising room
rates for some time to come. In Europe, the
economy is less robust, but even so, our
view is that hotel performance will improve
modestly. Looking to other markets around
the world, our competitive posture in Asia
Pacific remains particularly encouraging,
although conditions are mixed depending
upon the country. However, the underlying
secular growth in demand for high-end
hotels throughout the world continues, and
we are bullish on the prospects for our
business and industry over the long term.
For a company with a brand presence
in some 100 countries, the increasing
strength of the U.S. dollar is a doubleedged
sword. A thriving U.S. economy
bolsters our prospects, but revenues
denominated in currencies other than the
U.S. dollar can carry an unfavorable foreign
exchange impact.
In summary, we should see industry
fundamentals working in our favor, and that
will be enhanced by our renewed focus on
driving growth and operational excellence
in 2015. We are ever mindful that all of our
efforts are dedicated to capturing value for
our stockholders and giving you a network
of hotels throughout the world of which you
can be proud.
Adam Aron
Chief Executive Officer
Starwood Hotels & Resorts Worldwide, Inc.

