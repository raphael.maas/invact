                                 TO OUR STOCKHOLDERS:







    Our strategic plan is to build a portfolio of assets that       during 2000-2002 of $423 million, or 37% of
    enhance financial performance and provide long-term             upstream capital expenditures.
    profitable growth. We have made significant progress
                                                                    We are currently investing in 12 field developments
    in the last five years to improve our competitive
                                                                    in the deepwater Gulf of Mexico, West Africa, North
    position both in exploration and production and
                                                                    Sea and Southeast Asia. We estimate that these
    refining and marketing. Exploration and production
                                                                    developments should add in excess of 100,000
    is our engine for profitable growth where we invest
                                                                    barrels of oil equivalent per day of new production
    the majority of our capital expenditures. We have
                                                                    by 2006. In addition, the lower costs of this new
    strengthened future profitability through an increase
                                                                    production combined with other cost cutting
    in new field developments, sales of lower value,
                                                                    initiatives should reduce unit costs by $2-3 per
    mature properties and a focused, higher impact
                                                                    barrel by 2006.
    exploration program. We intend to continue to
    increase reserves and production outside the mature            RESHAPING OUR EXISTING ASSET PORTFOLIO
    regions of the United States and North Sea. With                In 2003 we sold $545 million of assets of which
    respect to refining and marketing, we plan to enhance           $478 million were mature or high cost exploration
    financial returns from our existing assets and to grow          and production properties. We sold our interests in
    retail marketing opportunistically.                             the shallow water Gulf of Mexico, the Jabung Field
                                                                    in Indonesia, and in several small fields in the
    In 2003, our major accomplishments were:
                                                                    United Kingdom sector of the North Sea.
     MAJOR SHIFT IN EXPENDITURES
                                                                    We also completed three significant asset swaps
     TO FIELD DEVELOPMENTS
                                                                    in 2003. In the first, we swapped mature, high cost
     Over the past several years we have significantly
                                                                    assets in Colombia for a 25% interest in significant,
     shifted our exploration and production capital
                                                                    long-lived natural gas reserves in the Malaysia-
     expenditures to field developments. In 2003 we
                                                                    Thailand Joint Development Area, bringing
     invested approximately $700 million, or 56% of
                                                                    our interest in the area to 50%. In the second
     our upstream capital spending in developments,
                                                                    transaction, we transferred a 14% interest and
     and in 2004, we plan to invest approximately
                                                                    the operatorship of the Scott and Telford Fields
     $900 million, or about 60% of our upstream capital
                                                                    in the North Sea in exchange for an additional
     expenditures in developments. These amounts
                                                                    22.5% interest in the Llano Field in the deepwater
     compare to average annual development spending


2
 Gulf of Mexico, bringing our interest in the field to     Net income per barrel of refined products sold ranked
 50%. We also exchanged our 25% stake in Premier           in the top quartile versus competitors for 2003. The
 Oil plc for a 23% interest in the Natuna Sea Block A      first quarter of 2004 has started well, and we antici-
 in Indonesia.                                             pate another year of strong financial performance for
                                                           our refining and marketing business.
 RESTRUCTURING OUR EXPLORATION
                                                            STRENGTHENING OUR FINANCIAL POSITION
 AND PRODUCTION ORGANIZATION
 Last year we took steps to restructure our explo-          We significantly strengthened the Corporation's
 ration and production business. As part of this            financial position in 2003. Proceeds from asset
 reorganization, we reduced headcount by about              sales totaled $545 million; we generated free
 30%, or roughly 700 positions. As a result, we             cash flow, after capital spending and dividends,
 expect to achieve annual after-tax cost savings            in excess of $150 million, and during the fourth
 of $30 million. Sixty percent of these savings are         quarter we issued $675 million of Mandatory
 expected to be realized in 2004, and the full amount       Convertible Preferred Stock. These steps allowed
 in 2005.                                                   us to reduce debt by over $1 billion in 2003 and
                                                            resulted in our year-end debt to capitalization ratio
 CREATING VALUE THROUGH FOCUSED,                           declining to 42.5% from 54% at the end of 2002.
 HIGH-IMPACT EXPLORATION
                                                            With $518 million of cash at the end of the year, an
 Our exploration strategy is to drill fewer, but higher
                                                            undrawn credit facility of $1.5 billion and negligible
 impact wells than in the past. In 2004, we expect to
                                                            debt maturities over the next several years, we
 drill about 15 high impact exploration and appraisal
                                                            believe that we will have ample liquidity and finan-
 wells. In 2004, over one half of our exploration
                                                            cial flexibility for the foreseeable future.
 activity will be in the deepwater Gulf of Mexico.
                                                           We deeply appreciate the many contributions and
 Last year we announced several discoveries, includ-
                                                           dedication of our employees. We are grateful to our
 ing two significant deepwater Gulf of Mexico wells:
                                                           directors for their advice and guidance. We thank our
 the successful appraisal of our Shenzi discovery,
                                                           shareholders for their continued support.
 in which we have a 28% interest and which encoun-
 tered about 500 feet of net oil pay, and Tubular Bells,
 in which we have a 20% interest. Both Shenzi and
 Tubular Bells will be further appraised in 2004.
 REFINING AND MARKETING
 Our refining and marketing business posted its best
                                                           JOHN B. HESS
 annual results in a decade. Earnings at HOVENSA,          Chairman of the Board
 our refining joint venture in the U.S. Virgin Islands,    and Chief Executive Officer
 and our retail and energy marketing businesses            March 3, 2004
 were strong.


