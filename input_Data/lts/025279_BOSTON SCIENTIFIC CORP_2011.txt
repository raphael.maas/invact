To Our Employees and Stockholders:
A steadfast focus on innovation, market expansion
and organizational efficiency made 2011 a pivotal
year in the evolution of Boston Scientific.

Weve built momentum in the past year and believe we have
positioned ourselves to deliver higher levels of value for patients,
customers, stockholders and employees in 2012 and beyond.
In 2011, we launched a number of innovative products, and
we plan to accelerate new product introductions with at least
40 more in 2012. Our development pipeline is full of innovations
that we expect will satisfy customers, help reduce healthcare
costs and, most important, help patients around the world live
healthier, longer lives.
We are expanding our global reach to build on our leadership
positions across our portfolio by investing in China, India and
other important emerging markets. China and India produced
strong double-digit growth for us in 2011, and we believe they
can do so again in 2012.
We believe our strengthened balance sheet, solid free cash flow
performance, reduced debt levels and recent credit rating
upgrades give us the financial flexibility to invest in research and
development, pursue organic growth and acquisition opportunities,
as well as repurchase company stock.
We have both a vision of restoring sustained, profitable growth
and a defined path forward for achieving our growth aspirations.
Innovation and market expansion are the keys to our success.
The Passionate Pursuit of Innovation
Innovation is the lifeblood of our industry, a key driver of
growth and a way of life for everyone at Boston Scientific.
Despite challenging economic conditions, we invested almost
$1 billion, or 12 percent of 2011 net sales in research and
development (R&D), and completed four acquisitions in the areas
of structural heart, atrial fibrillation, peripheral vascular disease,
and deep brain stimulation. Supplementing our internal R&D
efforts, we plan to continue exploring acquisitions of products
and technologies that expand our ability to serve patients.
Two principles guide our new product development and
acquisition investments:
 Boston Scientific products improve the quality of patient care.
We focus on innovative, less-invasive medical devices and
procedures that reduce risk, trauma, procedure time, and the
need for aftercare.
 Our products improve the overall productivity of healthcare
delivery. In todays cost-sensitive healthcare environment,
we believe third-party payers are more likely to reimburse
for products that lower long-term, overall costs.
We will focus on products that fulfill unmet physician or patient
needs, have growth potential supported by demographic or
disease-prevalence trends, and align well with our technology
strengths and the markets we serve.
As an example, we recently exercised our option to acquire
Cameron Health, Inc., which has developed the worlds first and
only commercially available subcutaneous implantable cardioverter
defibrillator  the S-ICD System.2 Unlike conventional
ICDs, the S-ICD System leaves the heart and blood vessels

untouched because it uses no wires, called leads, to connect
to the heart, simplifying the implant procedure and potentially
reducing long-term complications related to leads.
In coronary artery disease, we expect to enter U.S. trials as early
as mid-2012 with the SYNERGY Everolimus-Eluting Stent
System, a coronary stent with a bioabsorbable polymer that we
expect to further enhance the safety profile of drug-eluting stents.
In the structural heart category, we anticipate that European trials
will begin in the second half of 2012 for the Lotus Valve System,
the only fully repositionable, retrievable aortic heart valve of its
kind that allows physicians to treat patients with severe aortic
stenosis. In our NECTAR-HF clinical trial, we are studying the use
of autonomic modulation therapy to stimulate the vagus nerve
to inhibit the progression of heart failure in a large population
of patients who are not candidates for established therapies.
Growth Through Market Expansion
Boston Scientific is a market leader across the majority of markets
we serve. We are first or second in 23 of 36 product categories.3
We believe this is a strong indicator of both product quality and
customer loyalty and that it bodes well for us at a time when
supplier consolidation and physician employment by hospitals are
becoming a common industry practice.
Most important, we believe our market leadership position is
a strong platform for growth as we add products to our portfolio
and expand our global presence. We see particularly strong
growth opportunities in China and India, where faster-growing
economies, changing demographics, a developing middle class,
and other factors are creating rapid growth in demand for
advanced medical technology such as ours.

For example, during 2011 we estimate that we held a
35 percent share of the worldwide market for drug-eluting
stents (DES), with the bulk of our sales coming from the U.S.,
Japan and Europe. In contrast, our market share in China and
India is currently in the single digits. The combined annual DES
market in these countries is estimated to be nearly $700 million,
with an annual growth rate of about 20 percent, so there is
enormous near-term opportunity to increase our share with
our proven, best-in-class products in China and India.
We are investing significantly in people, infrastructure,
distribution, and clinical science in China and India with the
goal of expanding our ability to interact with the large number
of physicians in those countries who, like us, are working to
address unmet clinical needs and improve the quality of patient
care. We are committed to deepening our collaboration with
the Chinese and Indian medical and scientific communities by
strengthening our organization in those markets. In China, for
example, we plan to invest $150 million over the next five
years to establish a manufacturing facility and a world-class
training center for Chinese healthcare providers.
As we continue to build a solid foundation for long-term growth
in China, India and other emerging markets, we recently created
a new Asia-Pacific regional structure. This pan-Asia model is
designed to accelerate our growth, leverage our capabilities and
further optimize our performance across multiple countries in
Asia, the worlds fastest-growing region.
Reducing Costs and Improving our Financial Flexibility
Improving our cost structure continues to be a priority
for Boston Scientific. We are making real progress in our
goal to generate gross operating profit improvements of
$650-$750 million over the next several years through various
initiatives. For example, we expect the transitions to our
PROMUS Element Plus Stent System in the U.S. and the
PROMUS Element Stent System in Japan to improve our
gross margins. This is a self-manufactured product that we
believe will significantly help us reach our goal of restoring
sustained profitability.
We are vigorously pursuing other initiatives to reduce the cost
of goods sold, restructure our R&D processes and improve
quality and productivity across our manufacturing organization.
We also announced our 2011 restructuring program last July
and expect to reduce our annual pre-tax operating expenses
by $225-$275 million exiting 2013, a portion of which will be
reinvested in target growth areas.
In 2011, we reached our near-term targeted debt level of
approximately $4 billion, with no debt maturities on the horizon
until 2014. Following the announcement of our approximately
$1.3 billion stock repurchase programs in July 2011, we bought
back about 5 percent of our outstanding shares over the
balance of the year. All three major credit-rating agencies have
upgraded us to investment-grade status, citing the progress we
have made in strengthening our financial fundamentals.
We now have expanded flexibility in terms of our capital
allocation strategy and expect our free cash flow to give us
resources for acquisitions and continued share repurchases
while providing a safeguard to help manage unanticipated
contingencies.
New Leadership
Ray Elliott retired as CEO in 2011. We are grateful for his
contributions to the Company, which helped us become
a more focused, efficient and competitive organization.

We are delighted that Mike Mahoney joined Boston Scientific
as President and a member of our Executive Committee last
year. Mike is expected to become the Companys CEO on
November 1, 2012. He is an experienced healthcare business
executive with a proven track record of more than 22 years of
experience in medical devices, capital equipment and healthcare
IT services businesses with best-in-class global companies.
His seasoned background and long-term commitment to
Boston Scientific made him the best choice for this critical
role. Since joining the Company, Mike has actively engaged
with employees, customers and patients, which should help
ensure a seamless transition when he assumes the role of CEO.
Advancing Toward Growth
We are pleased with what we accomplished in 2011 but know
that we are just starting the next phase of our journey toward
renewed growth. We remain optimistic about our future and
our ability to increase profitable growth through an unwavering
focus on patients, innovation and market expansion.
Thanks to our stockholders, customers, employees, and
neighbors in the community for your trust and confidence.
All of us at Boston Scientific share the commitment to
make healthcare better while creating value for all of our
constituencies. We appreciate your continued support.
Sincerely,

William H. (Hank) Kucheman
Chief Executive Officer
March 16, 2012

Pete Nicholas
Chairman of the Board