DELIVERING IN
FISCAL 2018
Accenture delivered outstanding financial results in fiscal
2018, reflecting excellent demand for our differentiated
services�as well as our continued success in putting
innovation at the heart of how we serve clients across
industries and around the world.
I am particularly pleased that we delivered double-digit
revenue growth in local currency for the third time in four
years, gaining significant market share. Our performance was
broad-based once again, with strong revenue growth across
all operating groups and geographic regions�double-digit
or high single-digit growth in each case. We also delivered
excellent earnings per share and record free cash flow�
enabling us to return substantial cash to shareholders while
continuing to make significant investments in the business.
Here are some highlights:
� We delivered record new bookings
of $42.8 billion, a 12 percent increase
in local currency.
� We grew net revenues 10.5 percent in
local currency to $39.6 billion�another
all-time high.
� We delivered diluted earnings per share of
$6.34, compared with $5.44 in fiscal 2017.
After excluding $0.40 per share in charges
related to tax law changes in fiscal 2018
and a $0.47 per share pension settlement
charge in fiscal 2017, adjusted EPS of $6.74
in fiscal 2018 increased 14 percent.
� Operating margin was 14.8 percent,
consistent with our adjusted fiscal 2017
operating margin of 14.8 percent, which
excludes the 150 basis-point impact of
the pension settlement charge.
� We generated very strong free cash flow
of $5.4 billion and returned $4.3 billion in
cash to shareholders through dividends
and share repurchases.
� We announced a 10 percent increase in
our semi-annual dividend, to $1.46 per share,
shortly after fiscal year-end.
1
These excellent results enabled us to deliver significant
shareholder value in fiscal 2018. Accenture shares provided
a 32 percent total return for the year ended August 31�
12 percentage points above the S&P 500 Index.
Over the last five fiscal years, we have delivered compound
annual revenue growth of 9 percent in local currency and
10 percent compound annual growth in adjusted earnings
per share. Our compound annual total return to shareholders
for this five-year period was 21 percent, compared with
15 percent for the S&P 500.
Driving Differentiation
Our strong and durable performance reflects
the successful execution of our growth strategy.
In 2013, we recognized that the technology
revolution would significantly disrupt companies
and entire industries globally. We began
moving aggressively and strategically to further
differentiate Accenture in the new digital world�
to help clients disrupt rather than be disrupted,
and to capture new growth opportunities in
a very competitive environment.
Today, our business is focused on providing
end-to-end capabilities at scale across the full
spectrum of professional services�spanning
strategy, consulting, digital, technology and
operations. By combining our market-leading
capabilities across these businesses with our
deep industry expertise, Accenture is uniquely
positioned to drive large-scale transformation
for our clients. We are shaping strategy in the
C-suite, delivering cutting-edge technology
solutions and running operations for our
clients�giving us an unmatched capability
to integrate our services and commit to
tangible client outcomes.
Accenture remains the partner of choice for
many of the world�s leading companies and
largest government agencies on mission-critical
transformation programs. For example, we are
helping DowDuPont with its post-merger preparations to separate into three new companies.
We serve more than three-quarters of the
FORTUNE Global 500 and 92 of the top 100.
We also continue to build strong, long-term
relationships with our clients, and 97 of
our top 100 clients have been with us for
10 years or more.
Another key component of our growth
strategy is the significant investments we
have made in new, high-growth areas to
drive our differentiation and competitiveness.
In particular, our rapid rotation to �the New��
digital, cloud and security services�has clearly
set us apart, and we further strengthened
our leadership position in these areas in
fiscal 2018. Net revenues from the New
grew about 25 percent in local currency
to $23 billion�approximately 60 percent
of total revenues�more than double the
revenues just three years ago.
2
Innovating in the New
While the New has now become core to our
business, we continue to invest and innovate
to capture the next waves of growth. During
fiscal 2018, we evolved Accenture Digital
to be even more relevant to our clients by
focusing on three key market sectors:
� To further strengthen Accenture Interactive�
which was again recognized by Ad Age as the
world�s leading provider of digital marketing
services�we launched Intelligent Marketing
Operations, combining platforms, analytics
and artificial intelligence to run marketing
campaigns as a seamless managed service.
� We also launched Accenture Industry X.0,
which is focused on the digital reinvention
of industries with smart, connected products
and services using the Internet of Things,
connected devices and digital platforms.
� In addition, we created Accenture
Applied Intelligence by bringing together
our capabilities in advanced analytics
and artificial intelligence, which we are
increasingly embedding at the heart of
our clients� businesses.
Acquisitions are essential to building critical
skills and capabilities in strategic, high-growth
areas, which enhance our differentiation
and drive organic growth. In fiscal 2018, we
deployed more than 70 percent of our total
acquisition investment of $658 million to
extend our leadership position in Accenture
Digital. Key acquisitions in Accenture
Interactive included Mackevision in Germany,
Meredith Xcelerated Marketing in the United
States, HO Communication in China and
Altima in France. We also completed several
acquisitions that enhanced our Industry X.0
and Applied Intelligence capabilities, including
US-based Pillar Technology and Kogentix.
Another priority for Accenture is continuing
to leverage our unique position as the leading
partner of the key players in the technology
ecosystem�including SAP, Microsoft, Oracle,
Salesforce and Workday�which are also rotating
to the New with cloud-enabled platforms
incorporating advanced analytics, artificial
intelligence and machine learning capabilities.
Accenture is the clear global leader in implementing these platforms�through our Intelligent
Platform Services�to meet the needs of our
clients. For example, we are helping a wide
range of clients around the world to transform
their businesses with SAP S/4HANA solutions�
from The Hershey Company in the United States,
to Latin American utility Celsia, to Lion,
Australia�s largest brewer.
We continue to invest in our unique Accenture
Innovation Architecture, where we collaborate
with clients in new ways to develop, scale and
deliver disruptive, leading-edge solutions. Our
Innovation Architecture combines specialized
capabilities across the company�from research,
ventures and labs to studios, innovation centers
and delivery centers.
We continue to grow our network of more than
100 world-class innovation facilities, which are
strategically located in key innovation capitals
around the globe. In fiscal 2018, we opened six
Liquid Studios, where we accelerate software
development, and nine innovation hubs�in
Atlanta, Boston, Canberra, Columbus, Detroit,
�Our strong and durable
performance reflects the
successful execution of
our growth strategy.�
3
San Francisco, Tokyo, Washington, D.C.,
and Zurich�which bring together multiple
elements of our Innovation Architecture.
Through our open innovation program, we
have engaged with more than 5,000 start-ups
and made 28 strategic minority investments
over the last five years. In areas like blockchain,
extended reality and quantum computing,
we already are investing in and developing
solutions in the �next New� technologies.
We worked with Anheuser-Busch InBev and
several other organizations to successfully
test a blockchain prototype that eliminates
the need for printed shipping documents,
providing significant efficiencies and speeding
up ocean cargo transactions.
Another important indicator of our innovation
capabilities is our intellectual property portfolio,
which now includes more than 6,800 patents
and pending patent applications in areas like
artificial intelligence, blockchain, cybersecurity,
extended reality and the Internet of Things. Our
intellectual property is an important corporate
asset that differentiates Accenture�s services
and drives value for us in the marketplace.
Our People and Our Communities
As a talent- and innovation-led organization,
Accenture�s top priorities include attracting
the best people and investing to further develop
their highly specialized skills. In fiscal 2018,
we invested more than $925 million in the
development of our people, leveraging continuous
learning opportunities that are customized
for the individual in an on-demand, digital
environment. We continue to make substantial
investments in re-skilling, and have now
trained more than 290,000 people in New IT,
including automation, Agile development
and intelligent platforms.
We are particularly focused on building the
best possible leadership team in our industry.
During fiscal 2018, we promoted about 700
new managing directors and hired nearly 300
from outside Accenture�adding very significant
specialization and industry expertise.
Accenture is deeply committed to inclusion
and diversity, offering an inclusive environment
regardless of age, disability, ethnicity, gender,
religion or sexual orientation. We embrace
diversity as a source of innovation, creativity
and competitive advantage. Women now
make up 42 percent of our global workforce,
and we work proactively to ensure pay equity
among our people.
I am especially proud that Accenture was
named the top company�Number 1�on the
Thomson Reuters Diversity & Inclusion Index,
as well as Number 2 on CR Magazine�s list of
�100 Best Corporate Citizens.� We also were
recognized once again by Ethisphere as one
of the �World�s Most Ethical Companies�
and by FORTUNE as one of the �100 Best
Companies to Work For.�
With our partners, our clients and our
communities, we innovate to improve the lives
of millions of people around the world�now
and for generations to come. Through our
Tech4Good initiative, we collaborated with
Grameen Foundation India to apply artificial
intelligence and augmented reality to help
disadvantaged women access financial services.
Leveraging digital solutions, we made further
progress toward our Skills to Succeed goal
of equipping 3 million people by 2020 with
the skills to get a job or build a business. We
are supporting both Skills to Succeed and
Tech4Good through our new commitment
to invest more than $200 million over three
years in education, training and skills initiatives.
4
We are minimizing our environmental
footprint and fostering sustainable
growth for Accenture and our clients,
and reached our goal of a 50 percent
reduction in per-employee carbon
emissions three years ahead of schedule.
We remain committed to helping to
accelerate the global shift to a low-carbon
economy and reducing the impact of
climate change.
In closing, I want to thank all Accenture
people around the world for their unique
passion and dedication to our clients
and our business, which truly enabled
us to deliver such strong financial results
in fiscal 2018. As we move into the new
year, we have excellent momentum in
our business and are very well-positioned
in the marketplace. With our highly
differentiated capabilities, the significant
investments we are making, and our
disciplined management of the business,
I am very confident in our ability to continue
gaining market share and delivering
value for all our stakeholders.
Pierre Nanterme
Chairman & CEO
October 24, 2018