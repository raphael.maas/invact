 Dear Fellow Shareholders


In 2000 our new company produced a record-setting year and
successfully completed one of our industry's biggest mergers.

I'm proud of our talented and energetic team. Our share price
closed the year at $67, just a fraction shy of our all-time high.
Combined with four quarterly dividends, the increase in
share price produced a total return exceeding 80 percent.
                                      We surpassed the average total returns on the Dow Jones and S&P
                                  Utilities indexes, which closed the year with total returns of 51 percent
                                  and nearly 60 percent, respectively. Last year's total company return beat
                                  our goal of 15 percent--a mark we've set for future years.
                                      It's tempting to give ourselves all the credit for last year's share price run-
                                  up. Make no mistake: The increase reflected the market's confidence in our
                                  ability to deliver solid long-term earnings, dividends and share price appreciation.


                  The Market's Return to Sanity

                                  But our share price run-up also marked a return to sanity by the stock
                                  market in general. Last year, investors in so-called "New Economy" stocks
                                  and dot.com darlings saw their billion dollar investments shrink to millions.
                                  Renewing their interest in companies with real earnings and real cash flow,
                                  they returned to "Old Economy" investments like those in the utility and
                                  energy sectors.

Last year's total company return beat our goal of 15 percent
--a mark we've set for future years.

                                      And our sector had its own share of volatility in 2000. Nightly newscasts
                                  and headlines publicized problems in California, where surging power prices
                                  and chronic short supply gave deregulation critics plenty of false ammo.
                                  Short on hydropower and facing natural gas constraints, California actually
                                  imposed curtailments early in the winter. It's become increasingly clear to
                                  everybody--even anti-competition proponents--that problems in the Golden


                           
                                              State stem not from the free market, but from California's own confused
                                              and poorly timed restructuring plan.
                                                  Fortunately, most other states have avoided similar mistakes. But the need
                                              for new capacity to meet increasing demand is evident in other parts of the country.


                               Nearly 9,000 Megawatts on the Horizon

                                              Today, we're the largest producer of Btus in the Midwest, Northeast and
                                              Mid-Atlantic regions of the United States, home to 40 percent of the nation's
                                              demand for energy. We'll build an even larger position in this region when
                                              we complete our $1.3 billion acquisition of the 1,954-megawatt Millstone
                                              Nuclear Power Station, located in Connecticut on Long Island Sound.

   Today, we're the largest producer of Btus in the Midwest,
   Northeast and Mid-Atlantic regions of the United States, home
   to 40 percent of the nation's demand for energy.

                                              Adding Millstone to our growing generation fleet will boost our combined
                                              utility and non-utility electric capacity by 10 percent. We should see at least
                                              an extra 5 cents in earnings per share each year in the first two years of own-
                                              ership--and even greater annual contributions beyond that. In 2001, we'll
                                              also bring on line nearly 700 megawatts of additional gas-fired generation
                                              in Virginia and at our Elwood facility in Illinois, expanding what long-timers
                                              call "iron in the ground."
                                                  In the years immediately following this, Dominion's power generation
                                              team has nearly 3,850 megawatts of new capacity either under construction
                                              or on the drawing boards. We've secured turbines and identified site possibili-
                                              ties for another 2,400 megawatts. In total, we hope to have nearly 9,000
                                              new megawatts in service by 2005, most of it gas fired. Gas from our own
                                              production company delivered by our own pipeline will fire four of these
                                              facilities in Ohio, Pennsylvania and West Virginia. Locally, our work to site
                                              and permit these facilities is running smoothly.


                               Expanding Our Pipeline

                                              We're expanding our natural gas pipeline and storage system--already
                                              one of North America's largest. Growth in new gas-fired generation, built
    Expanding our pipeline
     system will help meet
                                              by Dominion and others, should help spur an expected 20 percent increase in
  a 20 percent increase in
new natural gas throughput
                                              gas throughput over the next five years.
   over the next five years.



                                                  But over the past year, as gas prices have reached new and unprece-
                                              dented highs, some pundits have called into question the viability of gas
                                              as a generation fuel. We believe these high prices cannot be sustained.
                                              The laws of supply and demand will work to bring down the market price
                                              of natural gas within the next few years.



                                                   Firing the Economy: Growth in Natural Gas
                                                   Exploration and Production
                                              In fact, gas is firing the economy. Industry forecasts project growth in natural
                                              gas consumption of nearly 50 percent in just ten years--half of which will
                                              fuel power units. Virtually every major power facility being developed in the
                                              United States will use clean-burning natural gas.
                                                  Increased oil and natural gas prices helped our exploration and
                                              production businesses contribute $1.14 to operating earnings in 2000.
                                                  Last year's high prices reminded us of the need to develop the
    
                                              abundance of natural resources within our reach. Last year, our combined
    



    Industry forecasts project growth in natural gas consumption of
    nearly 50 percent in just ten years--half of which will fuel power
    units. Virtually every major power facility being developed in the
    United States will use clean-burning natural gas.

                                              exploration and production unit produced 316 billion cubic feet of natural gas
                                              equivalents from a balanced mix of properties. This represented growth of
                                              about 190 percent from Dominion's stand-alone 1999 production. We're
                                              reinvesting some of our profits to develop additional production.
                                                  In gas production alone, we've become one of the nation's largest
                                              independent producers.



                               Selling Gas or Power--When Conditions Are Right
                                              It's ironic--and instructive--that the energy sector witnessed such
                                              volatility during our first year as a combination electric and gas company.
                                              It showed why our merger made sense. As I told you last year, our merged
        Dominion is one of
                                              company has the ability to hedge shifting commodity prices. We can serve
        the nation's largest
   independent natural gas
                                              consumers efficiently and reliably under a variety of market conditions
 producers with operations
in Canada and Appalachia,
                                              while delivering value to you, our shareholders.
     and on the Gulf Coast.


                                                     In other words, we'll sell gas directly when market conditions for gas
        Dominion produced an
80 percent total return for the
                                                 sales are good. When demand for gas slackens and prices go down, we'll
year, besting industry indexes.

                                                 burn the gas in our power plants, put it on the wires, and sell the energy
                                                 as electricity. Sometimes we'll use our storage system to store the gas and
                                                 wait for a better sales day.

       In our view, it's critically important to own and operate businesses
       and infrastructure that can produce and deliver energy in any form.

                                                     In our view, it's critically important to own and operate businesses and
                                                 infrastructure that can produce and deliver energy in any form. In tomorrow's
                                                 marketplace, institutional and individual customers will shop for Btus
                                                 --whether electrons or gas molecules--just as surely as they shop for
                                                 credit cards and long-distance phone service.



                                  Trading and Marketing
                                                 Last winter's high prices and cold weather made it tough on many retail
                                                 customers. They don't have the ability to hedge. We worked with regulators
                                                 --and on our own--to help soften the blow. We boosted our contributions
                                                 to emergency relief programs and worked to educate customers about them.
                                                 We promoted energy conservation. When we could, we even encouraged
                                                 our customers to shop around for better deals.



We'll bring greater urgency and innovation to our longstanding
tradition of operational excellence and customer service.

                                      Last year, our trading and marketing groups contributed about
                                 20 cents per share, and we're looking for a bigger contribution in 2001.
                                 At present, we're one of the nation's largest electricity marketers, with
                                 about 136 million megawatt-hours traded in 2000. We rank in the top 25
                                 on the natural gas side, with trading volumes of roughly 1.2 trillion cubic
                                 feet. Our goal is to more than triple our power volumes by 2005 and achieve
                                 a five-fold increase in gas volumes during the same period.
                                      In short, we aim to build value in old ways, new ways and ways
                                 we're just discovering. We'll bring greater urgency and innovation to our
                                 long-standing tradition of operational excellence and customer service.



                  Top Nuclear Performance
                                 When our acquisition of Millstone is complete in 2001, its units will join
                                 our other top-rated nuclear power stations. In 2000, just as in past years,
                                 Nucleonics Week magazine rated our North Anna and Surry facilities in
                                 Virginia as two of the most efficiently run nuclear stations in the United
                                 States. For the fifth year in a row, the magazine ranked North Anna the
                                 nation's top performer on a three-year basis, which more accurately
                                 reflects the impact of refueling outages on production costs. Surry came
                                 in at number four.
                                      In April, Surry Units 1 and 2 set an industry record for models of their
                                 type by running 277 days concurrently. Unit 1 achieved a world-record
                                 refueling outage for a three-loop Westinghouse reactor in 23 days,
                                 15 hours and 45 minutes. August 19 marked the 100th day that all four
                                 Dominion units operated concurrently, the sixth time in our history this
                                 has happened. Our nuclear team is taking these strides by demonstrating
                                 what I call a "bias for action."



                  A Bias for Action
                                 Last May, our Bulk Power organization showed us their bias for action in
                                 a big, bold way. In the Virginia port city of Norfolk, a tall crane being
                                 barged along the Elizabeth River snagged company transmission lines



                                 more than 150 feet above the river. Four conductors crashed into the
                                 water, damaging two others and blacking out a major portion of downtown
                                 during rush hour. Under old ways of business, repairs to the line would
                                 have halted traffic on the river for a week to ten days while the cable
                                 was lowered and work performed from another barge.
                                     Instead, three Bulk Power employees--Mark Allen, Don Koonce and
                                 Danny Bowers--acted quickly and decisively. Our people chartered a
                                 specialized helicopter and crew so that they could work on the damaged
                                 cables from the air. Leaning out of that chopper, the aerial linemen spliced
                                 lines together above the water, bringing the substation back into operation
                                 in a matter of hours.


                 Operating Earnings Rise

                                 All in all, we had an excellent year.
                                     Our 2000 operating earnings rose to $3.33 per share, up 10.6 percent
                                 over 1999 with an operating return on equity of about 12 percent. Our
                                 earnings performance exceeded our growth target of 8 to 10 percent.
                                 Recognizing the full potential of the combined company, we twice increased
                                 projected operating earnings for 2001. We now project per-share operating
                                 earnings of $4.10--a level that we originally believed we would reach
                                 by 2004--with continued annual growth in the range of 8 to 10 percent in
                                 coming years. We set this target expecting that gas and oil prices will decline
                                 from historic highs, our electric production costs will remain low, and demand
                                 for gas and electricity in our geographic market will continue to grow.

We're committed to continuing our $2.58 annual dividend. Dividends
are an important, if neglected, component of total return.

                                     We're committed to continuing our $2.58 annual dividend. Dividends are
                                 an important, if neglected, component of total return. Maintaining the dividend
                                 is important to many shareholders who depend on dividends for income.
                                     Some Wall Street pundits say a company intent on share-price growth
                                 can't afford to reward its investors with cash. I disagree. Unless the laws
                                 of gravity change, our share price should rise with rising earnings. And the
                                 percentage of earnings we pay as dividends will go down--something
                                 Wall Street will view favorably. At year's end, our payout was about
                                 77 percent of total earnings. Over time, we'll work to have earnings growth
                                 move that in the neighborhood of 50 to 55 percent.


                        
                   Voting with Our Pocketbooks

                                   Last year, I told you I wanted our senior management--an energetic,
                                   ambitious bunch--to begin each day warm under the collar at any thought
                                   or hint of a decline in share value or dividend reductions.

Last year, I purchased an additional ten times my annual salary in shares,
making me one of the company's largest individual shareholders.

                                       Under voluntary guidelines adopted by our board, Dominion's
                                   officers are expected to own company shares in amounts totaling from
                                   three to eight times their base salaries. This is a major obligation
                                   which required most officers to borrow money. Loans to purchase
                                   these shares come due in five years, and our dividends help pay the
                                   interest. We're all personally on the hook if, in the interim, we have
                                   not made sound business decisions and grown the company profitably.
                                   Last year, I purchased an additional ten times my annual salary in shares,
                                   making me one of the company's largest individual shareholders.
                                   Together, our senior management team also makes up one of the largest
                                   shareholder groups, with more than 2.4 million shares valued at year
                                   end in excess of $161 million.
                                       Overall, our entire family of employees--executives, management,
                                   staff, field or supervision--own about 15.6 million shares. At year's end,
                                   our total investment in our business exceeded $1 billion. Employees are
                                   Dominion's biggest shareholder as a group, a confidence vote straight
                                   from the pocketbook.


                   Technology and Telecommunications: New Opportunities

                                   In the telecommunications and technology arena, we're fortunate to have
                                   in-house expertise at Dominion Telecom that opens new opportunities.
                                   Dominion Telecom plans to invest approximately $700 million over the
                                   next two to three years to build out our fiber-optic network. We'll use our
                                   expanded network--planned to span more than 9,000 route miles--to
                                   provide Internet, video conferencing and other broadband services.
                                       Yes, we're looking to be a niche player. We aren't interested in going
                                   toe-to-toe with the national telecoms. But we see a real opportunity in
                                   offering telecom capacity to Cleveland, Buffalo, Toledo and other under-
                                   served cities in our core energy markets.


                              19
We'll use our expanded network--planned to span more than 9,000 route miles
--to provide Internet, video conferencing and other broadband services.

                                     Our power technology group is investing in fuel cells and other distrib-
                                 uted generation technologies. They're studying new innovations to discover
                                 how we can apply them to improve service, work smarter and better, and
                                 protect the environment better.


                 Stricter Environmental Regulations: A Fact of Life

                                 We take our environmental responsibilities seriously. Protecting the
                                 environment is a growing concern for many Americans. They expect and
                                 deserve responsible stewardship from industry. Our challenge is to comply
                                 fully with all federal and state environmental regulations while growing
                                 the company and increasing shareholder value.
                                     Late last year, we announced a groundbreaking agreement in principle
                                 with the United States Environmental Protection Agency. It commits us to
                                 more than $1.2 billion--most of it already planned and budgeted--in
                                 environmental protection projects over the next dozen years at several
                                 of our coal-fired power stations in West Virginia and Virginia.
                                     We're pleased with this agreement and the certainty it will give us going
                                 forward. And it will have a huge and positive impact on the environment.




                            20
       Our wholesale marketing
                                    Industry Change Still Unfolding
business traded about 136 million
      megawatt-hours last year.

                                                    We've been active in Washington and in state capitals helping resolve many
                                                    matters that affect us beyond environmental protection. At the state level,
                                                    West Virginia became the latest state to begin deregulating. In Pennsylvania,
                                                    more than 500,000 customers switched their electricity supplier. In Virginia, our
                                                    home base for retail electric operations, one of the largest pilot programs in the
                                                    history of electric utility restructuring lifted successfully off the launch pad. The
                                                    lessons learned from this pilot will smooth our move into competition next year.


                                    Managing Our Debt

                                                    Growth doesn't come cheap.
                                                        We've got to spend money to reach new markets, provide top-notch
                                                    customer service and manage the challenges I've just set forth. Last year, we
                                                    raised $2.5 billion in debt capital through six well-received debt issues in
                                                    addition to more than $750 million in equity or equity "hybrid" securities. We
                                                    also received commitments totaling $5.5 billion from 30 financial institutions
                                                    to support our growth.
                                                        We borrow under two unwavering conditions: First, we strive to maintain
                                                    our strong investment grade ratings. Second, we maintain our bankers' confi-
                                                    dence by committing to repay our obligations as quickly as possible.




                                               21
                    Last year, Dominion maintained its strong "High BBB" bond ratings
                from both Moody's and Standard & Poor's--ratings considered strong for
                corporates. We've reduced our debt levels aggressively by selling non-core
                international, financial services and other assets. At year's end, those moves
                produced nearly $1.1 billion in net proceeds, with more on the way. We've
                targeted debt as a percentage of our total capitalization to be about
                55 percent by the end of 2002.
                    In short, we had to stretch our balance sheet. Our chief financial officer
                compares it to a rubber band that's tight--nowhere near the snapping
                point--but now returning to more comfortable levels.


Beyond the Call of Duty

                Along the way, we also managed to give back what we could to our commu-
                nities. In October, more than 400 employees in five states volunteered to
                help beautify their neighborhoods by working at schools and parks to create
                nature trails, gardens and environmental education centers.
                    The very next month, several hundred more employees in Virginia
                pitched in to build Habitat for Humanity houses. United Way recognized our
                employees with its "Spirit of Caring Award"--the organization's
                highest honor. Beyond that, our company gave more than $6 million to health
                and human services, community development, environmental, cultural and
                educational efforts in the states where we operate.



A Special Tribute
                I want to pay special tribute to my fellow employees for their willingness
                to adapt to an ever-changing competitive marketplace. Their initiative and
                creativity demonstrate courage and the ability to focus on the task at hand
                in a very dynamic environment. My hat is off.
                    I want to thank our board of directors for its continued guidance. We
                have a top-notch board that always keeps us on track. We also appreciate
                the hard-working vendors, suppliers, contractors and agencies who help
                us serve nearly 3.8 million retail energy customers. To our customers,
                I express deepest gratitude for your continued confidence in our delivery,




           22
                                         I want to pay special tribute to my fellow employees
                                         for their willingness to adapt to an ever-changing
                                         competitive marketplace.

                                         price, products and service. To serve you at the highest possible level, we
                                         pledge to find new ways to improve our products and services.
                                             None of this would be possible without you--our owners. Thank you
                                         once again for your continuing investment and confidence. As always,
                                         we promise to be careful stewards of your investment and to look for
                                         opportunities to grow our company even more profitably.



                                              New Heights
                                         Last summer, we offered to help state wildlife officials house five Peregrine
                                         falcons in hopes of creating a nesting pair of the endangered species in
       
                                         Central Virginia. The birds lived 22 stories above downtown Richmond atop


                                         the Dominion building on Cary Street.
                                             The falcons grew up in comfort and safety, under the care of trained
                                         state professionals. But they had never flown, and the day finally came
                                         for them to set out on their own. They stepped one-by-one beyond the
                                         familiar nesting box, walked to the roof's edge and stretched their untested
                                         wings. One bird hopped off, fell over the side and disappeared for several long
                                         seconds before soaring up, flapping high above the building and winging
                                         towards the sun.

     I'm feeling bullish. We'll do our best to soar.
                                             He took flight and surpassed our highest expectations. It reminds me
                                         of our own first year as a newly combined company. I'm feeling bullish.
                                         We'll do our best to soar.


                                         Sincerely,




                                         Thos. E. Capps
                                         Chairman, President and Chief Executive Officer



