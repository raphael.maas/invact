TO OUR shareholders:
In 2014, we extended our winning streak for increased sales
and earnings at Macy's, Inc. while also launching plans for our
next phase of continued improvement for profitable growth.

We are particularly proud to have met our long-term objective
for a profitability rate that places us among best-in-class
retailers, as measured by Adjusted EBITDA (earnings before
interest, taxes, depreciation and amortization) as a percentage
of net sales. We now are shifting our resources and energies
to growing the topline faster while maintaining this high
profitability rate.
                                                                       other cost initiatives in 2013). On this basis, earnings per
Our company has been on an extraordinary journey of                    share rose by double digits for the sixth consecutive year.
reinvention. We have created Macy's and Bloomingdale's                 Including these items, earnings per diluted share were $4.22
omnichannel brands that are nationwide and increasingly                in 2014, up from $3.86 in 2013.
global, and achieved significant increases in operating earnings      Return On Invested Capital  a key measure of how
and cash flow. We became a magnet for the best talent in               efficiently the company uses its capital  rose again in 2014
retailing at all levels of our organization, and added $18 billion     to 22.4 percent, up from 21.5 percent in 2013. This was our
to our market capitalization since 2009. In the past six years,        sixth consecutive year of improvement in ROIC.
our Total Shareholder Return has been nearly 700 percent.
                                                                     Refinement of M.O.M. Strategies
2014 Financial Highlights                                            A key factor in Macy's success has been the consistent and
Here are highlights from our fiscal 2014 financial results, which    disciplined execution of our core strategies of My Macy's
are presented and discussed in greater detail in the company's       localization, Omnichannel integration and Magic Selling customer
2014 Form 10-K (including information on pages 17 to 20              engagement, which are known by the acronym of "M.O.M."
regarding our non-GAAP financial measures):
                                                                     M.O.M. remains the blueprint for the Macy's brand today 
 Comparable sales on an owned plus licensed basis grew              with each element of the strategy coming closer together to
  by 1.4 percent, and by 0.7 percent on an owned basis.              work holistically. My Macy's, Omnichannel and Magic Selling
  This was our fifth consecutive year of comparable sales            are no longer very separate, distinct subjects. They feed
  growth. Macy's, Inc.'s total sales have grown by nearly            and support each other. We also have become very
  $5 billion over the past five years, even with a somewhat          proficient at applying what we learn to continuously
  smaller portfolio of stores, as we have embraced an                improve our business for our customers. We know we
  omnichannel approach to business.                                  must keep moving forward with fresh thinking.


                                                  ADJUSTED
                                                  EBITDA
                                                  As a percent
                                                  of net sales




 Adjusted EBITDA as a percent to net sales was 14 percent,          My Macy's localization, developed in 2008 and launched
  rising from 13.6 percent in 2013  and up from 11.3 percent        nationwide in 2009, remains a differentiator and sustainable
  in 2009. Reaching the 14 percent level has been a long-            competitive advantage for Macy's. Our intention has always
  standing goal, and we fully expect to remain among the             been to deliver a merchandise assortment and shopping
  most profitable retailers.                                         experience in each location that meets the need of the
                                                                     local customer.
 Earnings per diluted share were $4.40, an increase of
  10 percent from $4.00 in 2013, excluding certain items             Now with six years of My Macy's experience and a significant
  (merchandising and marketing restructuring, store and              amount of progress, we are refining our approach to localization
  field adjustments, store closings and a premium for early          with a renewed emphasis on driving sales growth with a process
  retirement of debt in 2014; impairment, store closings and         that is streamlined and leads to quicker decisions.
                                                                                                                                        1
    We are devoting more attention to enhancing our assortment and            Our second avenue for future growth involves all-new business
    sales performance by climate zone. We are getting more granular           ideas. This is what Peter Sachse, now Chief Innovation and Business
    on serving ethnic and multicultural customers. And we are focused         Development Officer, and his new team will be tackling.
    on flowing merchandise receipts into each door at exactly the right       For example, we are looking seriously at international expansion
    moment in the shopping cycle.                                             for Macy's and Bloomingdale's. We already ship online orders into
    There is absolutely no doubt that our customers today are                 100 countries. We have a very successful Bloomingdale's store in
    Omnichannel in how they shop and where and when they buy.                 Dubai. Macy's and Bloomingdale's have announced new stores to
    So Macy's has to be with them at every turn.                              open in Abu Dhabi in 2018.
    Research has shown that about two-thirds of all shopping trips            We have learned that consumers worldwide know Macy's and
    today start online with customers researching the options at their        Bloomingdale's, and they like what they see. We are continuing to
    desktops or on smartphones or tablets. Then, the customers often          analyze various markets to determine what might be next.
    come into the store to touch, feel and try-on the merchandise they        We have just begun to study a Macy's off-price business, but we
    saw. Maybe they buy in the store. Or maybe they buy the item later        are moving fast and plan to begin a pilot in fall 2015. Note that we
    while sitting at home in the evening.                                     currently have 13 Bloomingdale's Outlet stores (with a 14th to
    We are now operating the company with a single view of our                open in fall 2015), and we are excited about the growth potential
    customer, inventory and business  no matter how, when or where           of this channel.
    the customer is shopping.                                                 We now are structured to generate and pursue multiple new ideas,
    In a move to achieve a higher level of shopping convenience,              all underpinned by a test-and-learn methodology that maximizes
    in 2014 we rolled out Buy Online Pickup in Store (BOPS) to all            upside opportunity while minimizing risk.
    Macy's and Bloomingdale's locations nationwide, and successfully
    piloted same-day delivery to customers in eight markets during            Bluemercury Acquisition
    the holiday season.                                                       In March 2015, we completed our acquisition of Bluemercury,
                                                                              Inc., widely recognized as America's largest and fastest-growing
    Magic Selling has been closely aligned with My Macy's as we
                                                                              luxury beauty products and spa services retailer, for $210 million
    helped local customers understand that the products at their Macy's
                                                                              in cash. Bluemercury was Macy's, Inc.'s first acquisition in 10
    were selected to meet their needs  whether it be by size, color,
                                                                              years and provides us an entirely new channel for growth.
    style, fabric weight or brand.
                                                                              Bluemercury currently operates about 62 specialty stores in
    Moving forward, Magic Selling is also a vital ingredient for
                                                                              18 states, typically in prime street-level locations and urban
    tapping the full potential of Omnichannel. We have new tools and
                                                                              lifestyle centers, as well as an online business. Macy's, Inc.
    technology on the selling floor to engage our store customers.
                                                                              plans to operate and significantly expand Bluemercury stand-
    Magic Selling is focusing these new customer touch points to
                                                                              alone specialty stores, enhance its online capabilities and add
    grow sales.
                                                                              Bluemercury private brand products and shops to selected
    We are encouraging additional "radiated sales" as customers               Macy's stores over time.
    come to a store to pick up BOPS orders or to return or exchange
                                                                              Bluemercury's existing management team and organization
    merchandise purchased online. Mobile point-of-sale devices, kiosks
                                                                              remain in place.
    and tablets help our associates to better understand customer
    needs and provide quick and effective service using technology            2015: An Upward Transition
    that our customers love.
                                                                              Corporations sometimes use the word "transition" to mean
    Pursuing Profitable Sales Growth                                          they need time to re-think their strategy. At Macy's, Inc., however,
                                                                              we see 2015 as a "transition" year in a different sense of the word.
    Beginning in 2015, we see Macy's, Inc. entering a new phase
    of growth as we re-focus on increasing topline sales, and doing so        Over the past decade, we have worked to mold a high-
    while maintaining the excellent rate of profitability achieved in 2014.   performance culture that encourages innovation and calculated
                                                                              risk-taking while reaching very high standards for profitability.
    We think of our growth prospects in two parts.
                                                                              This is our foundation.
    First, we have plenty of opportunity ahead for organic growth
                                                                              To build on this foundation in 2015 and beyond, we will be
    within Macy's and Bloomingdale's existing businesses. Toward that
                                                                              investing to fuel faster total and comparable sales growth while
    end, we announced in January and in early February 2015 that
                                                                              maintaining a high rate of profitability. We expect to continue to
    we have aligned our organization, including our senior leadership
                                                                              increase sales and earnings again in 2015, even as we pilot and test
    under the direction of Macy's, Inc. President Jeff Gennette, and
                                                                              new growth ideas that have the potential to be scaled-up quickly to
    Bloomingdale's Chairman and CEO Tony Spring, around the needs
                                                                              serve the evolving needs and expectations of our customers.
    of an omnichannel customer. As a result, we can make decisions
    faster, move quicker and be more responsive to how and when our           We have assembled the best team in retail to take us forward, and
    customers choose to shop with us.                                         I am both excited and confident about the future. Our company is
                                                                              changing  quickly, and for the better. Thanks for your support in
    We are changing the way we buy and plan merchandise so store,
                                                                              our upward transition.
    online and mobile shopping is seamless. Marketing is driving
    demand across channels using traditional and digital techniques.
    We are embarking upon a new and more robust approach to
    rewarding loyal customers. And we are continuing to look for
    opportunities to fill "white space" in our assortment with license        Terry J. Lundgren
    agreements that bring new categories back to Macy's in partnership        Chairman and Chief Executive Officer
    with deeply experienced specialty retailers.
2
