TO OUR STOCKHOLDERS:
KLA-Tencor performed extremely well in fiscal year 2012, in both absolute and relative
terms. We delivered record free cash flow and near-record revenue, with our year-over-year
revenue growth rate significantly exceeding that of our industry. We also strengthened
our position in core process control markets, with products introduced during the year
receiving very strong customer acceptance. Furthermore, we actively returned value to our
stockholders by increasing our dividend and continuing our stock repurchase program.
Our strong performance this year showcases the companys market, technology and
business model leadership. Further, it demonstrates that we are successfully executing our
long-term strategic objectives and delivering industry-leading performance in growth and
profitability, while generating stockholder returns that far outpaced the overall market.
The following is a summary of our performance during fiscal year 2012 within each of our
four primary strategic objectivesCustomer Focus, Growth, Operational Excellence and
Talent Development:
CUSTOMER FOCUS: MARKET LEADERSHIP
The driving force behind our Customer Focus objective is successful collaboration with
our customers. We measure success in our Customer Focus objective through superior
customer satisfaction and market share. We introduced 10 new products in fiscal year
2012, expanding our technology and market leadership.
GROWTH: IN EXCESS OF THE MARKET
Our Growth objective is to deliver long-term growth in excess of the industry. Our revenue
growth in fiscal year 2012 was roughly flat compared with fiscal year 2011, in a period
where industry capital spending was sequentially down. KLA-Tencors strong relative
growth is a function of strong adoption of process control at the leading edge and our
strengthening market position.
OPERATIONAL EXCELLENCE: SUPERIOR INDUSTRY BUSINESS MODEL
With respect to Operational Excellence, the focus of our ongoing efforts is on improving
our operating efficiencies and our overall business model to deliver superior profits and
cash flows.

TALENT DEVELOPMENT: ATTRACT AND RETAIN TOP TALENT
Our Talent Development strategic objective centers on attracting, retaining and engaging
our global employee base. We continue to maintain low attrition, retaining our innovative
workforce and providing opportunity for advancement, while also consistently meeting our
early career hiring goals. Based on this data and the feedback from our successful fiscal year
2012 employee engagement survey, our personnel clearly remain committed and invested
in KLA-Tencors strategic goals and long-term success.
Overall, were very proud of KLA-Tencors successful results in fiscal year 2012. The major
factors underlying our results continue to be effective collaboration with customers and
business partners and successful execution by our worldwide workforce. Id like to
acknowledge and thank our employees for continuing to help KLA-Tencor excel and for
shaping our future success.
Thank you for your continuing support of KLA-Tencor.
Sincerely,

Rick Wallace
President and Chief Executive Officer