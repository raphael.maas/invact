To our stakeholders:

Welcome to the Alpha Natural Resources 2013 Year in Review website.  We are excited to share with you our journey, but first I want to provide my perspective on quite an eventful year.    

Any way you look at it, the market for coal was challenging in 2013, continuing the trends of 2012.  We faced increased competition from natural gas, the economic recovery in the U.S. and Europe was slow, and growth rates in Asia were not as robust as they once were.  We also felt the impact of aggressive regulations here in the U.S., including regulations that resulted in many coal-fired power plants closing or announcing plans to do so.  But we did not stand still; we took decisive action, as you will see throughout this report.

We have advanced our business in key aspects, working to reinforce our industry leadership and position our business for the future.  This year, for the first time, we are reporting in accordance with the Global Reporting Initiative (GRI), the worlds most recognized framework for sustainability reporting. These achievements speak to the quality and resolve of our people. 

Our accomplishments in 2013 were driven by our shared commitment to our mission, business objectives and values, and by our engagement with key stakeholders.  When we are aligned and working with a common purpose, we can build a better business.  Put simply, we are Stronger Together.    

At Alpha, our mission is to fuel progress around the world.  As an abundant, affordable and reliable resource, coal brings economic and societal benefits to people everywhere.  

The numbers reinforce our view.  According to the International Energy Agency (IEA), coal is projected to surpass oil as the worlds primary energy source in the next couple of years.  Forecasts from industry analysts point to around 100 million tons of increased metallurgical coal demand by the end of the decade.  

Modernized nations, including the U.S., have been built on coal, which has powered homes and businesses for over a century and helped create steel to underpin our critical infrastructure.  Today, coal remains a vital part of the energy mix that keeps power prices affordable.  In emerging markets, coal represents an opportunity for reliable and affordable electricity and all that comes with it.  

We never lose sight of coals enduring contribution to the U.S. or the life-changing opportunities that it creates for developing nations.  It drives us every day.  In 2013, we stepped up our efforts to raise awareness and champion coals contribution to the worlds development and growth and to advocate for pragmatic energy policies and regulations, particularly in the U.S.  We will continue to stand up for our industry in the years ahead.  

Next, we aim to position Alpha not only to survive the tough market but to thrive into the future.  Toward this goal, we took decisive action last year.  

With exports representing a sizeable opportunity for future growth, weve worked hard to expand our international platform by taking advantage of our export capacity, which is the largest among U.S. coal companies.  In the past year, our team has traveled the world to meet with existing and new customers, and our shipments reached 29 countries in 2013.  

We also aggressively eliminated costs from the business.  We drove savings by increasing efficiencies and productivity in the areas of maintenance, sourcing and operating practices.  Unfortunately, in these market conditions, we were forced to idle a number of mines and reduce our workforce, particularly in Central Appalachia.  This was a difficult decision that we did not take lightly, but it was unavoidable considering the drastic changes taking place in our markets.  

Underpinning these efforts, we continued to take a disciplined and conservative approach to financial management, as we always have done at Alpha.  We strengthened our financial position and balance sheet through a series of capital markets transactions.  Additionally, we monetized some of our Marcellus Shale natural gas assets through a deal we completed in January 2014 with Rice Energy.

Our entire team lives our values as showcased by the progress we made in safety, environmental stewardship and giving back to our communities.  

Those of you who know Alpha know that we have a philosophy called Running Right, which was developed to advance safety performance but has since been integrated into everything we do.  Its the embodiment of our values.  Running Right starts with the relentless pursuit of continuous improvement; we empower our people to speak up and take action when they see an issue or opportunity, and we work to ensure that their voices are heard and that best practices are shared throughout the organization.  This is the right thing to do, and it helps build a better and stronger businessincreased regulatory compliance with safer and more efficient operations.  

Safety is our core value and highest priority at Alpha, and we made some impressive gains over the past year.  We recorded the best combined total reportable incident rate (TRIR), an industry benchmark for safety, in our history, and had no fatal accidents in 2013.  One of the proudest moments of my career came in June of last year when we opened the Running Right Leadership Academy, a state-of-the-art education facility and comprehensive training ground for safety and operational excellence. 

We made significant progress in compliance with water quality and other environmental regulations over the past year.  This follows from investments in technology and the application of best practice stewardship.  We also invested in research to further understand and drive environmental performance.   

We continued to give back to the communities where our people live and work.  Due to market conditions, we had to reduce our philanthropic budget, but we focused our efforts on maximizing the impact of the community investments that we could make.  In total, we made more than $2.8 million in charitable contributions to support local hunger relief, social services and culture across our operational footprint.  Our communities have been hit particularly hard by the economic and regulatory pressures affecting coal industry employment, and we have been there to support them as much as we can.

Looking ahead, the marketplace for Alpha and the industry will continue to be difficult in 2014, but weve taken steps over the past year to create a sustainable business.  We believe we have a strong foundation and assets to build upon, and with a shared mission, business objective and values, we can continue to drive success and serve the interests of all our stakeholders.

