Fiscal 2008 was a strong year with some notable
accomplishments. We have prepared HP to perform
well and are building a company that can deliver
meaningful value to our customers and stockholders
for the long term. Looking ahead, it is important to
separate 2008 from 2009, and acknowledge the
difficult economic landscape. While we have made
much progress, there is still much work to do.
2008Solid Progress and Performance in a Tough
Environment
With the acquisition of Electronic Data Systems
Corporation (EDS), we continued implementing a
multi-year strategy to create the worlds leading
technology company. Additionally, we made solid
progress on a number of core initiatives, including
the substantial completion of phase one of HPs
information technology transformation.
Fiscal 2008 was also a difficult year, during which
economic conditions deteriorated. HP proved our
ability to execute in a tough environment.
In 2008, HP delivered:
 Net revenue growth of 13 percent, or 8 percent in
constant currency, to $118.4 billion
 GAAP operating profit of $10.5 billion
 GAAP diluted EPS of $3.25, up from $2.68 in the
prior year
 Non-GAAP operating profit of $11.8 billion*
 Non-GAAP diluted EPS of $3.62, up from $2.93
in the prior year*
HP gained share in key segments, while continuing
to show discipline in our pricing and promotions.
Software, services, notebooks, blades and storage
each posted double-digit revenue growth,
highlighting both our market-leading technology and
improved execution. Technology Services showed
particular strength with double-digit growth in
revenue for the year and improved profitability.
The EDS AcquisitionDisciplined Execution of a
Multi-year Strategy
In August, HP completed its acquisition of EDS, a
global technology services, outsourcing and
consulting leader, for a purchase price of $13
billion. The EDS integration is at or ahead of the
operational plans we announced in September, and
customer response to the acquisition remains very
positive.
The addition of EDS further expands HPs
comprehensive, strategically assembled portfolio that
provides unparalleled capabilities for delivering endto-
end solutions. Over the course of several years,
HP has established a track record of making
acquisitions, integrating them, supporting them with
R&D and turning them into market leaders with
enhanced profitability. HP is following the same
game plan with EDS, which is an excellent fit both
financially and operationally. EDS provides a strong
base of recurring revenues and a meaningful
opportunity to capture synergies and expand
earnings per share. More importantly, EDS adds a
Dear Fellow Stockholders,
CEO letter
world-class, globally scaled services capability to
HPs established leadership in hardware and
management software. This is a powerful
combination that puts HP in a distinct position to
lead the industry and deliver for our customers and
stockholders.
Solid Progress on our Core InitiativesA Milestone
in the IT Transformation
Even as we have undertaken the integration of EDS,
multiple transformations of corporate functions,
business groups and business processes are
underway across HP. In fiscal 2008, we made good
progress, removing more than twice the costs from
corporate shared services than we did in fiscal 2007.
Perhaps no single initiative has been as important as
our information technology transformation. Begun in
2005, phase one of this effortthe complete rebuild
of HPs IT infrastructure and processesachieved
substantial completion by the end of 2008.
The transformation of HPs IT reduces costs, provides
more reliable information, establishes a simplified
and dependable IT infrastructure, improves business
continuity and supports the companys growth. We
also expect to flip the ratio of our IT spend from 75
percent on maintenance and 25 percent on
innovation to 75 percent on innovation and 25
percent on maintenance. This was all done primarily
with HP hardware, software and services, and the
capabilities of our closest partners.
The benefits of the IT transformation are expected to
include:
 Consolidation of more than 85 internal data
centers to six next-generation data centers
 Consolidation of 6,000 applications to 1,500
standardized applications
 Reduction of annual energy consumption in data
centers by 60 percent
 Increase in processing power of 250 percent
For all that we have accomplished in creating a
more efficient company, there is still a great deal of
work left to do and several more years of cost cutting
for HP to reach the benchmarks we have set. We
expect to reduce our cost structure in fiscal 2009 by
more than $1 billion in constant currency over fiscal
2008 levels through previously announced initiatives
in our corporate functions and the EDS business.
Additionally, we have significant cost savings to
realize within our segments to improve their
competitiveness. Our goal: to have the best
operations of any company in the world.
Looking Ahead: Long-term Opportunity; Near-term
Challenges
In the long term, we see extraordinary opportunities
for HP. The amount of information on the planet is
exploding. It has been estimated that the digital
data-set doubles every 18 months. All of that
information needs to be captured, stored, processed,
shared, printed and viewed. Consumers need
always on, always connected access to the data that
is increasingly central to their lives. Enterprises are
straining to meet ever-growing demand with aging,
complex, proprietary and inefficient IT infrastructures.
These dynamics are creating a massive disruption in
the IT marketplace and a massive opportunity for HP.
With a comprehensive portfolio of hardware,
software and services, HP is well positioned to help
customers manage and transform their IT
environments.
In the near term, we expect that economic conditions
in 2009 will be extremely challenging. It will take
continued discipline and tough decision making to
stay the course and continue executing our strategy
in the coming months.
However, our company has significant competitive
advantages:
 A strong balance sheet
 Diversified revenues with one-third of our revenue
and well over half of our profits from recurring
sources such as services and supplies
 A lean, variable cost structure and commitment to
continue to eliminate all costs that are not core to
the companys success
 Proven financial and operational discipline
These advantages will be put to good use by our
outstanding executive leadership team. We will
continue to make strategic investments for the future
in sales coverage, opportunistic acquisitions,
research and development, and customer service and
support. Our plan is to get through this period
without losing any muscle in the organization or
changing our swing in the marketplace.
Great companies rise to the top in tough times, and I
believe HP is one of the best. We expect that HPs
ability to execute in a challenging marketplace will
differentiate us from our competitors and enable HP
to emerge from the current environment as a stronger
force in the industry.
Thank you for your investment in HP.
Sincerely,
Mark V. Hurd
Chairman, Chief Executive Officer and President
