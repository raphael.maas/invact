Zebra has an intimate understanding of operational workflows in the key industries that we serve.
Our expertise enables us to help our customers drive performance and successfully address
increased demands in the marketplace:
� Retail shoppers want more convenience and flexibility in how they purchase and receive goods
� Transportation companies need to deliver in hours, rather than days
� Patients demand a higher quality of care at a lower cost
� Manufacturers are increasing efficiencies across their value chains
Enterprise Asset Intelligence (EAI) is integral to our strategic focus at Zebra, and makes our
solutions unique. Our devices and intelligent infrastructures sense information about assets,
products, and processes. This information, including status and location, is then analyzed to
provide actionable real-time insights to reduce friction in workflows, improve productivity, and
enable greater insight into business operations. Savanna, our cloud-based data intelligence
platform, is an essential component of our overall offering, powering the analytics behind our
data-driven solutions. This EAI framework provides a digital view of the entire enterprise and
ultimately gives a performance edge to front line employees.
To Our Investors
Board of Directors
Michael A. Smith, Chairman 1,2,3
Chairman and Chief Executive Officer
FireVision, LLC
Anders Gustafsson
Chief Executive Officer
Zebra Technologies Corporation
Chirantan J. Desai 2
Chief Product Officer
ServiceNow
Richard L. Keyser 2,3
Chairman Emeritus (Retired)
W. W. Grainger, Inc.
Andrew K. Ludwick1
Chief Executive Officer
Bay Networks (Retired)
Ross W. Manire 1,3
Chief Executive Officer
ExteNet Systems, Inc.
Frank B. Modruson1
Chief Information Officer (Retired)
Accenture
Janice M. Roberts 2
Partner
Benhamou Global Ventures
1 - Member of Audit Committee
2 - Member of Compensation Committee
3 - Member of Nominating and Governance Committee
Executive Officers
Anders Gustafsson
Chief Executive Officer
Olivier C. Leonetti
Chief Financial Officer
William J. Burns
Senior Vice President,
Enterprise Visibility and Mobility
Michael Cho
Senior Vice President,
Corporate Development
Hugh K. Gagnier
Senior Vice President,
Asset Intelligence and Tracking
Joachim Heel
Senior Vice President,
Global Sales
Jim L. Kaput
Senior Vice President, General Counsel
and Corporate Secretary
Jeffrey F. Schmitz
Senior Vice President,
Chief Marketing Officer
Michael H. Terzich
Senior Vice President,
Chief Administrative Officer
Colleen M. O�Sullivan
Vice President,
Chief Accounting Officer
Stockholder Information
Global Corporate Headquarters
Zebra Technologies Corporation
Three Overlook Point
Lincolnshire, Illinois 60069
U. S. A. Phone: +1 847 634-6700
Fax +1 847 913-8766
Annual Meeting
Zebra�s Annual Meeting of Stockholders
will be held on May 17, 2018, at 10:30 a.m.
(Central Time) in Lincolnshire, Illinois.
Independent Auditors
Ernst & Young LLP Chicago, Illinois
Investor Relations
Investors are invited to learn more
about Zebra Technologies Corporation
by accessing the company�s website at
investors.zebra.com
Transfer Agent and Registrar
Computershare
P.O. Box 505000
Louisville, KY 40233-5000
Overnight Delivery:
Computershare
462 South 4th Street, Suite 1600
Louisville, KY 40202
Telephone:
+1 800 522-6645 or +1 201 680-6578
TDD for hearing impaired:
+1 800 231-5469 or +1 201 680-6610
Website:
www.computershare.com/investor
Form 10-K
The Zebra Technologies Corporation Form
10-K Report filed with the Securities and
Exchange Commission is incorporated
in this annual report. The Code of Ethics
for Senior Financial Officers is posted
on Zebra�s website. Please contact the
Investor Relations Department at the
Corporate Headquarters for additional
copies of the Form 10-K, or visit our
website to view an online version of the
Form 10-K, or the Code of Ethics for
Senior Financial Officers.
Equal Employment Opportunities/
Affirmative Action
It is the policy of Zebra Technologies
Corporation to provide equal opportunities
and affirmative action in all areas of its
employment practices without regard to
race, religion, national origin, sex, age,
ancestry, citizenship, disability, veteran
status, marital status, sexual orientation
or any other reason prohibited by law.
In 2017, we strengthened our value proposition and delivered solid operating performance.
� We completed the integration of our Enterprise Visibility & Mobility business, including a global Enterprise Resource Planning
(ERP) system implementation. It represents more than two-and-a-half years of commitment and focus by the entire Zebra team
and concludes our transition to One Zebra.
� We continued to extend our market leadership and deliver innovative solutions that have resonated with our partners and
customers, providing them increased visibility into business operations to achieve higher levels of growth, productivity, and
service. Enhancements to our broad portfolio of products and solutions included:
- Additions and refreshes to the industry�s broadest and most mature offering of enterprise-grade Android�-powered mobile
computing devices
- Expansion of our leading portfolio of next-generation 2D data capture devices
- Being first in the industry to offer a full portfolio of smart, connected printers with unrivaled manageability though our Link-OS
operating system
- New innovative solutions, such as SmartLens� for Retail, and SmartPack� Trailer, that further our vision and aspire to transform
workflows in key vertical markets that we serve
� Strong profitable growth, combined with disciplined working capital management, generated the cash necessary to pay
down $454 million of debt principal, exceeding our 2017 debt reduction goal by more than 50 percent. We also completed a
comprehensive debt restructuring that reduced our average interest rate by approximately two percentage points, generating
more than $45 million of annualized interest savings.
We remain focused on our key priorities to build upon our industry leadership and drive shareholder value.
� First, we are extending our lead in the core business through our unmatched scale, innovation, and relationships with customers
and partners. These are competitive differentiators in our traditional markets, and fostering them is crucial to our ongoing success.
� Second, we are committing our focus and resources to drive growth in attractive adjacent markets that leverage the strength of
our core. We continually evaluate opportunities where we are underpenetrated, as well as other emerging areas.
� Third, we are advancing our EAI vision by leveraging Zebra�s deep knowledge of workflows, and capitalizing on key technology
trends in mobility, cloud computing, and the proliferation of smart tools.
� Our fourth area of focus is to further enhance Zebra�s financial strength by increasing cash flow and optimizing our capital structure.
In closing, I�d like to express my gratitude for the dedication of our employees and the support of our partners and customers.
In 2017, we extended our market leadership and exceeded our financial targets. We are encouraged by our momentum into 2018
and are well positioned for continued success.
Sincerely,
Anders Gustafsson
Cover.indd 2 4/4/18 4:53 PM
UNITED STATES
SECURIT