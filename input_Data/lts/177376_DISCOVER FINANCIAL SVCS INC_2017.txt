To Our Shareholders:


 

The Future of Banking
Consumers want a trusted financial partner that is always available, easy to use, rewarding and innovative. Discover provides a personalized, secure experience that puts our customers in control. Our direct digital approach is efficient, and we are available where customers shop and on the devices they use. We operate our own branded payments network and provide 100% U.5.-based customer service-so customers know to expect the best from Discover. For decades our values have guided us to always treat customers and employees fairly, which is one thing that will never change.


 


DFS 10-year Anniversary
Ten years ago when I rang the opening bell at the New York Stock Exchange, it signaled an important new chapter in the history of Discover as we  became an independent, publicly traded company.

Not long after the launch of the DFS ticker symbol, the financial crisis hit the country hard.
Despite the challenging economic environment, Discover continued to focus on providing
an exceptional customer experience and maintained its conservative approach to managing credit. By staying true to our Discover Values, we weathered the storm better than our competitors, and emerged as a strong and stable leader in the industry.









DISC	DFS	10 YEA RS
 













As a company, we pride ourselves on delivering meaningful solutions that are driven by consumers' changing needs. Over the past decade, we introduced a suite of Discover it credit cards, along with services and features such as the Discover Mobile App, FICOฎ Credit Scores for free, and Freeze It, which allows customers to prevent new purchases when their card is misplaced or stolen. We significantly expanded our direct banking product line, which now includes private student  loans, personal loans, home equity loans and deposit products. Recognizing the importance of providing global network acceptance to our
card members, we acquired Diners Club International and partnered with others to grow U.S. merchant acceptance and expand our international footprint.

All of these efforts have helped us drive greater consumer consideration and engagement with the Discover brand. Our shareholders have benefited as our year-end stock price increased from just over $15 to nearly $77/ share over the decade, representing one of the best performing large financial services companies over this period. (See the 10-year timeline below and the financial summary on page 9.)


Our Progress in 2017
In 2017, we achieved 9% growth each in total loans and consumer deposits, a 16% increase in card new accounts and a 12% boost in overall payments transaction volume. We also grew revenue faster than expenses and maintained strong profitability.

After several years of historically low levels of credit loss across the industry, charge-off rates began to rise in 2017. Discover's net charge-off rate for the year was 2.70%, below the average of our key competitors. Diluted earnings per share were $5.42 and return on equity was 19%. We returned significant capital to shareholders as we repurchased 8% of our shares and as we increased our dividend for the seventh year in a row.





 





	DFS begins trading on NYSE
	Discover.com visits: 178 million
	#1 in Customer Loyalty for 11 years (Brand Keys)
	Discover Student Loans
 





	Acquired Diners Club International
	Biodegradable credit card
	Employee volunteerism: 35,000 hours
 
*1.'ทfl
	DFS becomes a bank holding  company
	Discover Mobile App for Apple iPhone
	ATM global network: more than doubled
 







Beyond our financial performance, we were recognized externally for the quality of our products and services and for the way we treat our employees. Discover was ranked as a "top place to work" in each of our major locations across the country. And for the fifth year in a row, we were listed as a Best Place to Work for LGBTQ Equality by the Human Rights Campaign's Annual Corporate Equality Index.

With a focus on continuously improving our operations and serving customers ever more effectively, I would like to highlight a number of other accomplishments.

Discover Card

We introduced an innovative free service to alert cardmembers when their Social Security number is found on any of thousands of risky websites or when new financial accounts
are opened in their name on their Experianฎ credit report. We launched in-app messaging to make it easier for customers to reach us when and where they want, and we offered consumers fast pre-approvals for new credit card accounts on Discover.com with no impact to their credit report.

Given our commitment to "We treat you like you'd treat you," we continue to invest heavily in leading digital and analytics tools and technologies, and in 100% U.S.-based customer service. The payoff is lower customer attrition and higher growth in loans from our credit card customers. As a result of this focus, the Discover Mobile App ranked "Highest in Customer Satisfaction among Mobile Credit Card Apps," according to J.D. Power's first U.S. Credit Card App Satisfaction Study 5 M.

Personal Loans

For people trying to finance major purchases or consolidate debt and improve their financial futures, personal loans continue to be a valuable product. Last year, we enhanced digital content to boost our search engine rankings and expanded our ability to accept loans online, making us more accessible to consumers and helping us to achieve a 14% increase in loans.









*1."ท'	ij.111
 
	Purchased The Student Loan Corporation, became 3rd largest private student  lender
	Highest Customer Call Satisfaction in Card Industry (Service Quality Measurement Group)
 
	Annual Discover card sales: $100+ billion
	Orange Bowl sponsorship
	25th anniversary of Discover credit card
 







Student Loans

The process of selecting and applying to a school and then financing a college
education can be overwhelming. To assist parents and their children in making informed decisions, we launched several resources, including CollegeCovered .com, financial aid consultations, and an interactive tool to compare aid packages and outline family costs, something that is unique to Discover. We also launched radio and addressable TV ads
to targeted groups. These and other efforts contributed to an 11% increase in organic student loan growth. With faster growth than the other large student lenders, we believe we are now the second largest originator of private student loans.

Deposits

Discover Bank ranked second highest in customer satisfaction among 15 U.S. direct banks, according to J.D. Power's first Direct Retail Banking Satisfaction Study 5 M. As consumer deposit balances grew 9% to reach $39 billion total deposits, we continued to do more to meet consumer needs. At the end of the year, we launched a new broad market checking product with a unique rewards program that pays 1% cash back on up to $3,000 in qualifying debit purchases each month.The no-monthly-fee, no-minimum-balance checking product, which is attracting thousands of new customers to Discover, also provides access to 60,000 no-fee ATMs.

Payment Services

With digital wallets growing in popularity, we enabled customers to use their Discover card to make payments in Samsung Pay and to redeem their Cashback Bonus in Apple Pay.We also helped Apple enable their person-to-person payments program, Apple
Pay Cash. Additionally, we announced that beginning in April 2018 we would no longer require signatures for credit and debit transactions in the U.S., speeding up the checkout experience while maintaining a high level of security.









ij.iij	ij.IQ

 
	Pathway to Financial Success Education Program
	Annual cash back payout: $1+ billion
	ATM network: 850,000 in 100+ countries
 
	Cashback Checking
	Discover it Card
	Global Payments Network: 3rd largest in acceptance  locations
	Home Equity Loans
 







To further enhance international acceptance and payments volume, we signed a strategic alliance agreement with a new domestic network in Vietnam and implemented a networkญ to-network relationship in Turkey. We also expanded our reach in Canada, Hong Kong and other markets.

Our PULSE debit network business has persevered through some volume challenges over the last couple of years. Our turnaround efforts have paid off as we achieved 14% volume growth in 2017 due to enhanced merchant and acquirer routing, and the addition of new issuers and incremental volume from existing ones.


Tax Cuts and Jobs Act of 2017
In December, Congress approved a revamp of the corporate tax system. With primarily U.5.-based operations, Discover has historically had a high effective federal tax rate. While there were some negative impacts to our fourth quarter results, the lower tax rate will have a clear benefit to Discover's future earnings and enable us to make important investments in our business, our people and the communities in which we operate. Our shareholders will benefit from larger after-tax profits.

After the tax cut was publicized, we announced that we would be seeking to maximize long-term shareholder value by reinvesting 20% to 30% of the tax savings in 2018.
Roughly 75% of this will be used in increased marketing and technology investments
to accelerate our business growth. The balance will be invested in our employees and our communities. We announced two initial employee actions designed to help attract, retain, and reward exceptional talent: a $1,000 bonus for our 15,000+ non-executives and a new $15.2 5 hourly minimum starting wage in our customer care centers. Our communities will benefit through increased funding of programs designed to improve financial literacy, support diversity and inclusion, and strengthen the communities in which we operate.








ij.IJj

 
	Discover it Chrome Card for students
	Rewards for Good Grades (Student Loans)
	FICOฎ Credit Scores for free for cardmembers
 
	Discover it Miles Card
	Diners Club acceptance: 24 million locations
	Freeze It
	Cashback Match for new card members
 







Our Focus in 2018
As we look ahead, Discover will focus on four priorities: driving profitable growth across all of our products and services; enhancing our capabilities and operating model; increasing efficiency; and maintaining our focus on risk management.

Our activities will include developing new products and features that will help us continue to differentiate our focused direct banking model. One example: we will add more features to our new Cashback Checking product to make it even more valuable to consumers. And to increase our efficiency, we will implement a new service platform to make it easier for our call agents to meet customer needs.


Giving Back
Companies have a duty not only to build their business but also an obligation to use their influence and resources to help make their communities better. Discover takes that responsibility to heart.

I want to thank our employees for their commitment to helping make their city or town better and for being such wonderful representatives of the Discover brand. Last year, they volunteered more than 5 5,000 hours, and together we donated millions of dollars to  non-profit  organizations.

A major focus of Discover's charitable giving is to help people achieve a brighter financial future. In 2012, we launched Pathway to Financial Success, a five-year effort to provide financial education in public high schools. More than one million students from 1,900 schools and districts across all 50 states have benefited from $13 million in grants. As part of Discover's enhanced community investments, we will expand our










ij.11	ij.IH

 
	Discover it Secured Card
	Check Your Rate (Personal Loans)
	"Highest in Customer Satisfaction  with credit card companies, 3 Years in a Row" (J.D. Power)
	Credit Scorecard for consumers age 18+
 
	#1 in Customer Loyalty for 21 years (Brand Keys)
	2nd largest private student lender
	Discover Mobile App logins: 408 million
	Consumer Deposits: $39 billion
	Employee volunteerism:  5 5,000+ hours
 







reach beyond high schools to provide free financial education to middle schools, colleges and special interest groups through both on line and classroom activities.

As a company, we continue to uphold our Value of "Doing the Right Thing." In the aftermath of several natural disasters, Discover worked with customers to waive fees and ease their financial burden. We also donated more than $3 million to the American Red Cross for disaster relief efforts, with money raised through an employee and cardmember  matching campaign of both Cashback Bonus and direct donations.


Our Journey
I'm very proud of what Discover has accomplished over our first decade as an independent public company-working through much adversity and against tough competition. As we look ahead to 2018 and beyond, I believe that our Discover Values, digital and branding capabilities, and direct banking focus position us well to serve our customers, employees, communities and shareholders for the foreseeable future. Thank you for joining us on this journey.



 

David Nelms
Chairman and Chief Executive Officer March 7, 2018
