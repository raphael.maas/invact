A MESSAGE TO OUR SHAREHOLDERS
                                                                              Your company laid the groundwork in 2014 for more
                                                                              sustainable growth in the years ahead.
                                                                              We made signiicant investments in our regulated utility
                                                                              operations to upgrade and strengthen our electric
                                                                              infrastructure, enhance the reliability of service to customers,
                                                                              achieve greater operating eficiencies, and meet the increased
                                                                              demand driven by the shale gas industry. These investments
                                                                              in our transmission and distribution businesses are designed
                                                                              to better position FirstEnergy for future success.
                                                                              Through pending and approved rate and regulatory
                                                                              proceedings in Ohio, Pennsylvania, New Jersey and West
Anthony J. Alexander                   Charles E. Jones
Executive Chairman of the              President and Chief Executive Oficer   Virginia, we're focused on ensuring our electric rates are
FirstEnergy Corp. Board of Directors
                                                                              better aligned with the cost of maintaining and upgrading
                                                                              our system to meet the increasing energy needs of customers.
                                                                              We also set a new course for our competitive generation
                                                                              business that is intended to limit risk and enable us to take
                                                                              advantage of future market upside. We are creating a more
                                                                              solid foundation that will help us succeed in a dificult energy
                                                                              market and comply with new environmental requirements.
                                                                              These and other initiatives have placed your company
                                                                              in a much stronger position to meet the challenges that
                                                                              lie ahead. We are encouraged by this progress and the
                                                                              continued growth in our service area's commercial and
                                                                              industrial sectors.


                                                                              G R O W I N G O U R R E G U L AT E D O P E R AT I O N S
                                                                              From 2014 through 2017, we expect to invest $4.2 billion
                                                                              in Energizing the Future, an initiative to modernize our
                                                                              transmission system across our 10 operating companies.
                                                                              This initiative focuses on strengthening one of the nation's
                                                                              largest transmission systems, which is expected to be our
                                                                              primary growth platform for years to come.
                                                                              As part of these efforts, we are deploying advanced
                                                                              technologies designed to enhance system reliability and
                                                                              security and to meet expected demand growth in our
                                                                              service area. Initial projects are moving forward along the
                                                                              backbone of our electric system in Ohio and our Penn Power
                                                                              service area. Our work is expected to expand east through
                                                                              2017 and involve 7,400 circuit miles, 70,000 poles and
                                                                              towers, and upgrades to more than 170 substations.
                                                                              We're also building a stronger, more resilient system by
                                                                              reinforcing critical components and investing in smart
                                                                              technologies, including advanced grid monitoring to help
                                                                              prevent certain outages from occurring or to reduce their
                                                                              scale and duration. Real-time monitoring capabilities are




                                                                                                                                                 1
designed to cut costs, support predictive maintenance, and
help us make better decisions regarding when equipment
should be scheduled for maintenance or replacement.
In 2014 alone, we invested $1.4 billion on more than
1,100 projects to enhance the durability and lexibility of
our transmission system. These efforts included rebuilding
140 miles of transmission lines and upgrading substations
with advanced surveillance and security technologies.
In addition, our investments are focused on meeting load
growth in the Marcellus and Utica shale regions of our western
Pennsylvania, eastern Ohio and West Virginia service
areas. For example, we're building new infrastructure to
accommodate the expected increase in demand for electricity
from new shale gas facilities, pipeline compressor stations
and other energy-intensive operations. Among other
projects, construction of a new substation and transmission
line near Clarksburg, W.Va., will support an existing gas
processing plant and help reinforce the regional grid, and
a planned transmission substation near Burgettstown, Pa.,
will serve a facility that separates natural gas into dry and
                                                                    will bring our revenues in line with our costs, help ensure
liquid components while beneiting more than 40,000 customers
                                                                    continued reliability, and provide service enhancements to
of West Penn Power.
                                                                    customers. In March 2015, the Administrative Law Judges
We expect shale gas development to account for approximately        recommended to the Pennsylvania Public Utility Commission
1,100 megawatts (MW) of new load over the next four years          that the settlement agreements be approved. In New Jersey,
the equivalent of about 1 million homes. This represents            the Board of Public Utilities' March 18, 2015, ruling on Jersey
approximately 50 percent of our projected increase in               Central Power & Light's rate case enabled recovery of
industrial demand through 2019.                                     $736 million in expenses incurred to restore service following
                                                                    devastating storms in 2011 and 2012. The ruling is expected
We're also encouraged by ive consecutive years of growth in
                                                                    to result in a revenue reduction of approximately $34 million.
the industrial sector of our distribution business. This trend is
a strong indicator of our region's positive economic future.        In addition, the Federal Energy Regulatory Commission
                                                                    (FERC) accepted our rate proposal for our ATSI subsidiary,
Several recent actions are designed to help ensure timely
                                                                    which controls 7,400 circuit miles of transmission lines.
and appropriate recovery of our investments in our regulated
                                                                    The proposal, which is subject to refund based on the
operations while offering signiicant beneits to customers.
                                                                    inal outcome of the case, features a forward-looking
The Public Service Commission of West Virginia approved             transmission rate structure to enable more timely cost
our rate case settlement agreement for our Mon Power                recovery and investment return.
and Potomac Edison utilities. The agreement will result
                                                                    In 2014, we also moved forward with a program to install
in recovery of approximately $63 million in additional
                                                                    approximately 2 million smart meters across our Pennsylvania
revenues annually for reliability investments, storm damage
                                                                    service area, scheduled to be completed by mid-2019.
expenses, and investments in operating improvements and
                                                                    Pennsylvania law requires us to provide smart meters to
environmental compliance at our regulated, coal-based
                                                                    all customers and allows for recovery of costs related to
power plants in the state.
                                                                    this program.
Our Powering Ohio's Progress plan, if approved as proposed,
                                                                    Our company continues to leverage other advanced
would freeze base distribution rates while helping ensure
                                                                    technologies to enhance service reliability to customers
continued availability of more than 3,200 MW of our critical
                                                                    and improve eficiency. For example, we rolled out new
baseload generating assets serving the long-term energy
                                                                    applications for smart phones and mobile computers that
needs of Ohio. The plan is designed to deliver signiicant
                                                                    enable our employees to quickly provide information about
beneits to our Ohio customers by helping safeguard them
                                                                    hazards and damage following major storms. The data is
from future retail price increases and volatility, promoting
                                                                    automatically transferred to ield dispatchers, enabling
economic development, retaining local jobs, preserving local tax
                                                                    them to more effectively prioritize work and expedite power
revenues, and powering manufacturing and other industries.
                                                                    restoration efforts. We're also offering customers more
In February 2015, our Pennsylvania operating companies iled         ways to stay connected with us, including text messaging,
for approval of comprehensive settlement agreements that            alerts and an enhanced mobile website.



                                                                                                                                      3
    SETTING A NEW COURSE FOR OUR
    COMPETITIVE BUSINESS
    In the face of evolving competitive markets, we took proactive   We believe this strategy will better position us to beneit from
    steps to reposition our competitive generation business,         opportunities as markets improve while limiting the risk from
    with a focus on reducing our exposure to risk and pursuing       continued challenging market conditions.
    higher-margin sales while leaving a portion of the generation
                                                                     As we pursue this new strategy, we also remain vigilant in our
    we produce available to capture future market opportunities.
                                                                     efforts to prudently manage capital expenditures across our
    As part of our repositioning efforts, we are limiting our        generating leet. For example, at our Beaver Valley Nuclear
    exposure to weather-sensitive demand in mass market and          Power Station, we deferred from 2017 to 2020 a planned
    certain commercial and industrial (C&I) sales channels. We       Unit 2 reactor head and steam generator replacement after
    intend to maintain our sales efforts to attract strategic,       determining the unit can continue to operate safely and
    large C&I customers whose demand for electricity is mostly       reliably.
    unaffected by weather. We also are continuing sales to
                                                                     We're conident that these and other actions have placed our
    Ohio governmental aggregation communities and pursuing
                                                                     competitive business in a more stable position, enabling us to
    wholesale power auctions where opportunities align with our
                                                                     assess market conditions and participate when, and where,
    generation portfolio. Both of these channels produce positive
                                                                     opportunities are most promising.
    margins and involve minimal customer acquisition costs.




                                                                                            Our new Waldo Run transmission substation in
                                                                                            Doddridge County, W.Va., supports the area's
                                                                                            Marcellus shale gas industry and enhances
                                                                                            service reliability for Mon Power customers.




4
M E E T I N G E N V I R O N M E N TA L                                         L E A D I N G T H E C H A R G E F O R V I TA L
REQUIREMENTS                                                                   MARKET REFORMS
In June 2014, as part of its efforts to reduce U.S. greenhouse                 We actively support efforts to ensure competitive energy
gas emissions under the Clean Air Act, the U.S. Environmental                  markets adequately value baseload coal and nuclear plants,
Protection Agency (EPA) proposed state-speciic guidelines                      which are essential to maintaining grid reliability. These
for the regulation of carbon dioxide (CO2) from existing                       efforts helped produce initial market reforms supporting price
power plants. Scheduled to be inalized in mid-summer                           stability and service reliability for our customers.
of this year, the EPA proposal  called the Clean Power
                                                                               Extreme weather events, including record low temperatures in
Plan  would provide guidance to the states for developing
                                                                               January 2014, resulted in power price volatility, underscoring
implementation plans to reduce their power sector emission
                                                                               the implications of our region's growing dependence on
rates. The EPA also separately proposed standards for
                                                                               less-reliable resources. These include natural gas, which is
regulating carbon emissions from new, modiied or
                                                                               challenged by supply system constraints; demand response,
reconstructed power plants.
                                                                               which depends on customers curtailing their electricity
We're concerned about a proposal that currently allows                         consumption during peak periods; and intermittent renewables.
only 6 percent of existing nuclear generation, which emits
                                                                               We're encouraged by a Capacity Performance product
no CO2, to count toward achieving emission reduction
                                                                               developed by our regional transmission organization,
targets. We also will monitor new details that emerge as the
                                                                               PJM Interconnection, to recognize the value of baseload
regulatory process evolves and as state regulators design
                                                                               generation. The product is a step in the right direction and
their implementation plans.
                                                                               may provide additional revenue to generating resources
While the EPA's proposed carbon standards are being                            that have onsite fuel storage, a high degree of availability
challenged in the courts, we continue to make signiicant                       and operational lexibility. We will continue to work closely
progress in improving the environmental performance of our                     with PJM to improve the proposal, as well as to pursue other
generating leet.                                                               efforts that recognize the value of a diverse and dependable
                                                                               generating leet.
By adjusting the mix of our generating assets during the
past three years, we're now operating a cleaner, more
eficient portfolio. In 2015, nearly 100 percent of the power
we produce is expected to come from low- or non-emitting
sources, including nuclear, scrubbed coal, natural gas and
renewable energy. Through these and other environmental
efforts, we are on track to achieve a 25 percent reduction
below 2005 levels of CO2 emissions this year.
We also are on target to exceed benchmarks established
by the EPA's Mercury and Air Toxics Standards. As part
of this effort, we have identiied several opportunities to
reduce compliance costs, and now expect to spend a total of
approximately $370 million on this effort.




          Top Right: Projects underway at our Fort Martin Power Station in
        Maidsville, W.Va., are designed to enhance the plant's performance
                   and prepare it to meet new environmental requirements.
 Lower Right: This environmental control equipment is part of a $1.8 billion
     retroit completed in 2010 at our W.H. Sammis Plant in Stratton, Ohio.
                                                                                                                                                5
    BUILDING ON OUR MOMENTUM                                         Dear Fellow Shareholders:
    We're conident that the aggressive steps we took during
                                                                     It's been a great privilege to serve as FirstEnergy's president
    2014 will help deliver greater inancial stability, build
                                                                     and chief executive oficer and, more recently, as executive
    shareholder value, and better position your company for
                                                                     chairman of your Board of Directors.
    future success.
                                                                     I'm proud of our management team and what we've
    We are continuously evolving to meet the energy needs
                                                                     been able to accomplish together. Starting with the 1997
    of our customers who rely on electricity to power their
                                                                     merger that formed FirstEnergy, we created one of the
    businesses and everyday lives. Regardless of the
                                                                     nation's largest energy companies, serving 6 million
    challenges that lie ahead, our dedicated employees will
                                                                     customers across a six-state service area. In recent years,
    remain focused on producing and delivering safe, reliable,
                                                                     we enhanced the reliability of our regulated utilities and
    affordable and clean electricity to our customers.
                                                                     improved the eficiency of our competitive generating leet.
    We thank you for your continued support of FirstEnergy.          And, in 2014, we focused our efforts on achieving more
                                                                     sustainable growth for your company in the future.
                                                                     As our employees prepare for the challenges that lie ahead,
    Anthony J. Alexander
                                                                     I'm conident they will succeed under the leadership of your
    Executive Chairman of the FirstEnergy Corp. Board of Directors
                                                                     new president and CEO, Chuck Jones. Chuck and the entire
    Charles E. Jones                                                 FirstEnergy team remain dedicated to enhancing the value
    President and Chief Executive Oficer                             of your investment.
    March 18, 2015                                                   Thank you for your support.




6
