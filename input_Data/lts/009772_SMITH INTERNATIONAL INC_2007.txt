Last year, we witnessed a continuation in what we
consider a longer-term, global energy upcycle.
The worldwide hydrocarbon market remains supply
constrained  which presents signifi cant challenges for
oil and gas operators who are striving to grow production.
Exploration and production companies are increasingly
relying on new drilling technologies and techniques to
develop reserves in more complicated geologic formations,
including unconventional gas plays in North America and
higher-risk deepwater developments. These changing
drilling trends are creating opportunities for Smith as we
focus on delivering products and services that improve
rates of recovery in the reservoir.
Barring a major deterioration in the global economic environment, we
believe fundamentals for our industry will continue to be attractive 
heavily infl uenced by a number of planned developments in regions outside
North America, including the
Former Soviet Union, Latin
America and other key drilling
markets where our businesses
are focused. Moreover, the higher
level of investment in deepwater
drilling programs, which will
unfold over the next several
years, provides us tremendous
opportunity for growth in our
M-I SWACO operations. We
continue to be the global leader
in deepwater drilling applications
with more than a 50 percent
market share worldwide
The current business environment presents growth opportunities for
Smith in areas such as ultra deepwater drilling, re-entry applications and
environmental solutions. Our success in developing technology for, what
many operators would consider, key niche product markets will continue
to pay dividends for our customers and stockholders.
2007 Financial Results
Last year, our operations delivered record fi nancial performance.
We benefi ted from the continued shift in spending towards the Latin
American and Eastern Hemisphere markets  where Smiths oilfi eld
business operations are concentrated. Although spending in the North
American region slowed last year, largely infl uenced by Canadian
drilling economics, markets around the rest of the world grew strongly,
infl uencing our results.
Smiths consolidated revenues increased to $8.8 billion last year, 20 percent
above 2006. Our Oilfi eld segment operations, which contributed the
majority of the year-over-year revenue growth, increased 23 percent 
outpacing the fi ve percent improvement in global drilling activity.
Higher revenue volumes translated into improved operating margins.
In a period with relatively modest activity growth, we were able to
improve oilfi eld margins 100 basis points  which had a favorable
impact on our bottom line. Net income for the year totaled $647 million,
or $3.20 per share, a 29 percent improvement over the earnings reported
in the 2006 year.
Even if we experience limited improvement in North American drilling
activity this year, we believe 2008 will be another solid growth year for
Smith. With many of our competitors in the industry experiencing declining
margins, we expect to see further operating margin expansion  albeit
at what could be a more modest rate going forward. We attribute this, in
part, to our product focus. Our business is service-oriented in nature and
our markets have high technological barriers of entry which means they
dont tend to attract the level of outside capital investment thats been
seen across other industry product segments, such as pressure pumping,
drilling rigs and other capital goods. We believe our product positioning
differentiates us from the majority of our peers. This factor, and our
exposure to higher-margin offshore developments which are expected to
accelerate in the latter half of 2008, should contribute to improved Oilfi eld
segment margins.
The slower-relative growth rates expected in the near-term should
enable our operations to generate sizable free cash fl ow. We continue to
favor using the cash provided by our operations to invest in technologybased acquisitions to broaden our product and service portfolio. If
we have limited success in identifying acquisition opportunities with
attractive capital return profi les, were committed to returning excess
cash to our stockholders through share buybacks and regular dividends.
In 2007, we distributed $160 million of cash via our dividend and
buyback programs  which translates into roughly one-quarter of last
years earnings and operating cash flows
Our results in 2007 are a direct result of the depth of
experience of our people, our portfolio of advanced
technologies and solutions, and a global infrastructure that
enables responsive customer service. More importantly,
we believe weve undertaken a number of initiatives that
will enable us to continue our leadership in these areas for
years to come.
Positioned for Growth
We are truly Smith International  with people, manufacturing
facilities, research centers and other support assets located
close to our customers around the world. We develop
industry-leading solutions specifi cally tailored to deliver
optimum drilling performance in challenging formations and
operating conditions. Using the industrys most advanced
and accurate drill bit design system, we recently introduced
the SHAHEEN line of diamond bits which are specifi cally
developed for use in abrasive carbonate structures unique to
the Middle East market. We understand how our technologybased products and services perform under a variety of
operating conditions and can effectively deploy solutions to
improve drilling performance.
We see a number of key trends emerging across the industry
and believe we are favorably positioned to capitalize on these
growth prospects. Worldwide upstream capital investment is
expected to continue to expand  with international spending
growth outpacing that of the North American region. We
have strong market positions in Latin America, the Former
Soviet Union, the Middle East and other high-growth areas,
which provide a platform for future expansion. Moreover, we
believe the increased importance of national oil companies
(NOCs) in the global energy industry will infl uence the future.
NOCs currently control approximately 80 percent of world oil
reserves and account for nearly three-quarters of production.
We believe NOCs and their governments will likely have more
control over the pace and the manner in which oil and gas
resources are developed  which could have implications
for Smith and other service companies with a high level of
technological expertise.
And, fi nally, were well-positioned to benefi t from the higher
level of deepwater activity which will occur over the next
few years supported by nearly 90 deepwater drilling units
which are scheduled to enter the market through 2011  with
deliveries accelerating in the 2009 and 2010 calendar years.
This trend has a favorable impact on our fl uid business
as deepwater projects typically generate higher revenue
intensity than an onshore well at better margins. Operators
continue to seek technological advances which improve
deepwater economics, such as our WARP Advanced Fluids
Technology which has demonstrated a number of technical 
benefi ts in challenging extended-reach wells. Our premium
fl uid systems reduce the number of days required to drill an
offshore well and contribute to lower overall costs.
Looking Ahead
The landscape in our business is changing daily which makes
this a very exciting time for Smith and the oilfi eld services
industry in general. As we expand our product offerings
and strengthen our service capabilities to better serve our
customer base and grow our operations, we remain focused
on maintaining the fi nancial discipline our stockholders have
come to expect.
Our strategy remains the same  provide leading
technology to assist operators in lowering the overall
cost of developing recoverable reserves. Our people, our
portfolio of advanced technologies and solutions, and our
vast global network favorably position us to serve our
world today and in the future.
It goes without saying that the Companys success is, and will
continue to be, shaped by the 20,000-plus individuals who
make up the Smith family. Our people are remarkable  and
we appreciate the time, effort and sacrifices that they make
on a regular basis for the Company.
Finally, on behalf of the entire Smith organization, wed like
to extend our thanks to Mr. Clyde Buck for his longstanding
service to the Company. He has been a key member of the
Board of Directors for the past 15 years and will be retiring
at the upcoming Annual Meeting. Clyde will serve as a nonvoting advisory member of the Board and continue to provide
guidance and direction to the Smith management team.
Doug Rock
Chairman of the Board and
Chief Executive Officer
Margaret K. Dorman
Senior Vice President,
Chief Financial Officer
and Treasurer