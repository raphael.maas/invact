To ourshareholders

Brewing. Besides describing what we do every day, the
word implies something happening, something in the
works. In the most positive sense possible, theres something
going on at Coors, and its pretty exciting.
Were achieving good, consistent financial results, were
well positioned to benefit from changing industry dynamics
and were more committed than ever to executing a disciplined,
focused strategy for growth and profitability.
This business is all about execution. Executing at an
absolute level of excellence is what makes winners. Coors
executed well in 2000, enabling the company to benefit
from a positive industry environment that included favorable
demographic trends, continued strong growth in
demand for light, refreshing premium-priced beers and
a positive pricing environment.
We had to overcome some significant cost challenges
to meet unprecedented demand, particularly for our
higher-cost packages. But the measure of a great organization
is its ability to respond to challenges, and respond we
did. Through the skills, dedication and teamwork of our
people and partners, we were able to achieve our fifth
consecutive year of strong double-digit earnings growth.
Some highlights from the year:
 We again outgrew the U.S. beer industry  our unit
volume grew more than four times faster.
 We set company records for annual unit volume, sales,
net income and earnings per share, and grew returns
significantly  return on invested capital approached
12 percent, up from 6 percent in 1996.*
 We continued to invest significantly in our brands and
sales organization, both domestically and internationally.
 We began construction of a new bottling line at our
Elkton, Virginia, plant.

 We entered into a joint venture with Molson, which
added import brands to our portfolio and, through a
contract brewing arrangement, gained access to extra
production capacity for our U.S. brands. We also
strengthened our existing Coors Canada partnership.
Review of 2000 financial performance
Our scorecard for the year was pretty good. For the 53-week
fiscal year ended December 31, 2000, the companys net
sales were $2.4 billion, an increase of 8 percent over 1999s
52-week figure of $2.2 billion. Including the extra week in
the fiscal year, 2000 sales volume reached 22,994,000 barrels,
showing 4.7 percent growth over the previous year.
Distributor sales to retail for the year 2000 increased by
approximately 6.5 percent over 1999. Excluding the extra
week in fiscal year 2000, sales volume and sales to retail
increased 4.1 percent and 4.4 percent respectively, year to
year. As we expected, because sales in the 53rd week were
seasonally low, this extra week did not materially affect
the companys annual earnings.
Excluding special items, after-tax income for fiscal
year 2000 was $114.5 million. This, compared with the
$95.8 million we achieved in 1999, represented a 19.6 percent
increase. Basic earnings per share rose 19.2 percent
to $3.11 and diluted earnings per share reached $3.06, up
19.5 percent from $2.56 per share in 1999, not including
special items.
A number of key factors drove our companys fiscal
2000 results, including volume growth, increased pricing,
the success of key international businesses and higher
interest income.
Overall, our brand volumes grew at more than four
times the industry rate. Coors Light extended its string
of consecutive years of mid-single-digit growth to six.
Original Coors was flat for the year with tough fourthquarter
comparisons, but Killians Irish Red, Zima and
Keystone Light volumes all experienced significant growth.
Throughout 2000, we continued to improve sales execution
and grow key brand equities by increasing our sales
and marketing investments by more than $30 million.
The industry pricing environment remained positive in
2000  we were able to increase our domestic net prices modestly
with no significant negative effect on volume trends.
We again had success internationally, particularly in
Canada with Coors Light, that countrys number-one light
beer, and in Japan, where Zima volume nearly doubled in
2000. Both grew earnings faster than our domestic business.
And we continue to be excited about our relationship
with Murphys in Ireland  working together, we have so
far been able to build Coors Light to a 5 percent market
share in the bottled lager segment. Elsewhere on the international
front, our decision early in 2000 to close our
brewing operations in Spain began to reap financial benefits
toward year-end.
Our 2000 results also were positively impacted by higher
interest income as we built cash balances, increased the
return on those balances and had lower debt than in 1999.

Looking ahead to 2001
Well be watching the same factors in 2001 that we discussed
this time last year: volume, pricing and cost of goods.
We entered 2001 with solid volume momentum in four
of our five largest brands. With strong brand equities and
our sales execution solid and getting better, our goal is to
outperform the industry volume growth rate for the fifth
consecutive year.
On the pricing front, we will again seek to strike a
balance between price competitiveness and building strong
brands. The factors that will affect pricing in 2001 include
the level of promotional discounting, the degree of valuepack
activity and the extent of any new front-line increases,
all of which are difficult to predict.
We expect cost of goods to be up modestly in 2001 with
most of the same challenges we saw in 2000  raw material
prices and shifts in demand toward higher-cost packages.
We believe the biggest cost factor will be mix shifts, where
we are continuing to increase the percentage of popular but
more costly longneck bottles in our package mix. Higher
anticipated sales of import beers by Coors-owned distributorships
and increasing raw material and energy costs will
be factors as well.
We have addressed a number of the issues that
increased costs as we operated at nearly full capacity
throughout much of 2000. We added to bottle and multipack
capacity and improved many production processes,
all of which will help us meet growing demand more efficiently
in 2001 and beyond  although one-time project
expenses to implement these measures will largely offset
cost savings in the short term.
A bright future
We are extremely optimistic about the future of this industry
and our place in it. Sure, its competitive, but we feel
very strongly that Coors, with a solid portfolio of brands,
premium light beer focus, strong young adult appeal, manufacturing
scale and one of the best teams in the business,
has all the attributes necessary to be a winner. We have
plenty of room to improve and, as we said in last years
letter to you, thats just the way we like it.
But we never forget that being a brewer and marketer of
alcohol beverages carries with it an awesome responsibility.
Underage drinking, drunk driving and reckless overconsumption
are very serious issues. Coors always has been
strongly committed to helping address them through
marketing responsibly, actively partnering with distributors
and retailers, and supporting legislation and educational
programs. This commitment will continue.
Wed like to thank our employees, partners and shareholders
for the roles they played in our success in 2000.
Theres something great brewing here at Coors, and youre
all a big part of it.

Peter H. Coors
President and Chief Executive Officer,
Adolph Coors Company
Chairman, Coors Brewing Company
W. Leo Kiely III
Vice President,
Adolph Coors Company
President and Chief Executive Officer,
Coors Brewing Company
