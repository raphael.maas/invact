The year 2001 was extraordinarily challenging
for the semiconductor industry and for Altera.
Following a year of robust growth, 2001 marked
the sharpest decline ever experienced in semiconductor
industry revenues. Alteras revenues
for 2001 were $839 million, down 39 percent
from the prior year. Income on a pro forma basis
was $132 million, $0.33 per diluted share.
The year began with slowing sales due to a North
American inventory correction that started in
late 2000. By the first quarter of 2001, customer
end demand in many of Alteras North American
markets had begun to contract, further reducing
demand for Alteras products. As the year
progressed, economic conditions weakened
worldwide, extending the negative impact on
the company. Overall, North America saw the
most severe change in revenues as sales declined
52 percent from 2000 levels due to recessionary
economic conditions and high levels of customer
inventory. While all our markets contracted, the
greatest change occurred in the communications
market, where revenue declined 47 percent. The
fourth quarter appears to be the bottom of this
cyclical downturn, as customers began to order
products to replace depleted inventory, leading
to improved customer backlog and a favorable
ratio of incoming orders to sales.
As a result of the dramatic change in business
environment, from exceptionally strong growth
in 2000 to the sharp contraction in 2001, and
despite severely reduced wafer procurement,
Alteras inventories grew well beyond levels seen
in earlier contractions. The company booked its
first material inventory write-downs and has
revised its inventory model going forward to shift
more of the companys demand to a build-to-order
basis. In addition, newer products will rely on
more flexible die bank inventory so that, in the
future, the company can hold less inventory
without sacrificing customer delivery performance.
This rapid change in business conditions also
required that Altera curtail discretionary spending
and implement several cost reduction and
restructuring measures. We were able to reduce
overhead and infrastructure costs without jeopardizing
our development momentum and sales
presence. Charges associated with these restructuring
activities and the inventory write-downs
more than offset the ongoing profits of the
company, leading to a net loss for the year of
$39.8 million, $0.10 per diluted share.
On a pro forma basis (excluding the inventory
write-downs), gross margins were 63.8 percent of
revenuea good showing for a semiconductor
manufacturer in any year, and an unusually strong
result in a year of revenue declines. In such a
difficult year, this performance demonstrates the
benefits of Alteras fabless business model, which
delivers strong gross margins throughout the
business cycle.
We invested aggressively in new technology in
2001. R&D spending of $171 million was similar
to the prior year, and with this continuing commitment,
Altera is well positioned to drive top-line
growth in the coming years as industry conditions
improve. SG&A spending has dropped steadily
during the year. The companys SG&A spending
in the fourth quarter was 23 percent below our
peak spending in the first quarter of 2001. Pro
forma operating income for the year was 17.7
percent of revenue, well below our long-term
expectations for the company. Alteras balance
sheet remained strong and debt-free, with a cash
position of $806 million. As part of the companys
ongoing share repurchase program, during 2001
Altera repurchased 7.2 million shares of the
companys stock at a cost of $183 million. Since
the beginning of this program in 1996, Altera
has returned $890 million to shareholders through
share repurchases.
Despite the operating difficulties, the company
reached several new milestones this year. Most
importantly, we became more competitive across
all dimensions of our business. The increased
R&D investments of the past several years are
paying off, and customers now take advantage

of Altera system-on-a-programmable-chip (SOPC)
solutions to address more of their design needs.
New product sales grew, with the greatest gains
in the APEX 20KE family, an advanced fieldprogrammable
gate array (FPGA) device family.
Total FPGA-based product revenues were $477
million in 2001. Sales of complex programmable
logic devices (CPLDs), composed of the Classic
and MAX families, were $299 million, and we
widened our CPLD market share lead in 2001.
The revenue potential from new design wins
increased sharply during the year, setting new
records. This performance demonstrates the
power of the companys growing range of products,
which capitalize on Alteras pioneering
first-to-market breakthroughs, more powerful
intellectual property offerings, and higher levels of
software performance. These design wins set the
stage for future growth as they enter production.
Integrated Product Portfolio
Alteras new products and our SOPC capability
enable our customers to use programmable logic
across a much wider variety of applications.
Our goal is to provide more SOPC value to our

customers through a comprehensive and integrated
series of products that leverages advanced
Altera programmable logic technology.
Through the new products we introduced in 2001,
we have taken major steps to deliver increased
SOPC value, and we lead the industry with innovative
first-to-market products. The APEX 20KC
devices, introduced during 2001, were the first
devices to offer the performance benefits of allcopper
interconnect. Extending the leadership
established in 2000 with Nios processor-based
Excalibur solutions, the first custom-designed
soft microcontroller in a PLD, Alteras Excalibur
solutions now include an ARM9 embedded
microprocessor integrated within a programmable
device. Altera is the first major PLD company to
offer this capability to customers. Altera also introduced
Mercury devices, the first programmable
ASSPs that combine a high-speed transceiver and
programmable logic, and HardCopy devices,
the first to offer a low-cost migration path for
high-density designs transitioning from prototype
to production. Alteras high-density APEX II
family, built for the data path with its unique
fast and flexible I/O features, also began shipping.
The newest APEX II devices have become the

first PLDs utilizing advanced 0.13-micron process
technology. These product introductions helped
Altera solidify our position as a leader in the
semiconductor industry.
Alteras core competency remains firmly rooted
in programmable logic, and the distinctive value
created by these new products rests in their
programmable capability. These new products
each offer distinctive features and complement
each other in numerous ways, making Altera
that much more attractive to customers. For
example, a high-density APEX II device or
Excalibur embedded processor solution becomes
even more valuable to a customer because of
the presence of a low-cost HardCopy migration
path. In another case, a Mercury customer can
take advantage of easy APEX II integration and
use an APEX II device with its data path focused
performance as part of an overall design. It is
these types of synergies that make Alteras
product range so attractive.
Software advantages are as important to our
customers as new silicon capabilities. Tens of
thousands of system designers around the
world use the companys development software.

Stratix Devices
Altera ushers in a new era for programmable logic with the
introduction of the Stratix device family, the industrys
fastest, most powerful PLDs. Building on a long history
of innovation, Altera is once again delivering the fastest
devices in the industry and achieving the highest levels
of integration with the most memory, logic elements, and
DSP functionality ever in a PLD.
The Stratix architecture is a result of intense customer
input. Architectural innovations allowed Altera to address
our customers needs for more processing power, speed,
functionality, and cost efficiencies. This revolutionary
family streamlines the customers design process to
speed time-to-market despite significantly increased
device complexity.
Altera has simplified the design process by enabling a
true timing-locked block-based design methodology. For
the first time ever, a designer, or teams of designers, can
work on specific areas of a design and lock in the timing
integrity to break the endless loop of re-optimization during
integration. The powerful combination of our Stratix devices
and Quartus II design software allows design teams to
shorten their design cycles and increase overall system
performance by as much as 100%.
Altera is committed to deliver the best, most innovative
solutions to our customers, and the new Stratix device
family is the next step. Using Stratix devices and Quartus II
software, Altera customers can focus on their core competencies
and speed their products to market with greater
cost-efficiency and less risk.

The unmatched breadth of Alteras MegaCore
intellectual property (IP) offerings and third-party
IP cores gives Altera customers even more flexibility.
Separate IP cores can be combined at a
system level using Altera software tools and development
kits, easing complex system integration.
The Next Step
Altera innovations are part of a long-held strategy
to develop PLDs that customers use to create
SOPC solutions. Altera has led the industry at
every step as PLDs have evolved from simple
glue logic to fully system-capable devices. Alteras
continued SOPC-driven innovation means that
Altera products will continue to displace other
silicon-based solutions.
The newly introduced Stratix family takes
SOPC potential to new levels. Stratix devices
employ a new architecture that was created to
address rapidly expanding system complexity.
Stratix devices are equipped with the industrys
first programmable digital signal processing
(DSP) blocks, making the devices well suited
for a wide variety of computation-intensive
applications. Additionally, the unique Stratix
architecture delivers more bandwidth, logic, and
memory than any previous Altera PLD, offering
customers additional flexibility and up to twice
the performance while simplifying the design
process. The Stratix architecture was also
designed to facilitate the development of future
Stratix products, which will shorten Altera
product development cycles and reduce future
development costs. Customers are already benefiting
from the advantages of Stratix devices as
they design with the February 2002 Quartus II
software release, and their interest in the Stratix
architecture has been extremely encouraging.

The Potential
Altera is changing the competitive landscape in
the semiconductor industry. Altera PLDs are
becoming more attractive to customers because
of their SOPC capability, while at the same time
they are uniquely suited to take advantage of
the economics of increasingly advanced process
geometries. As the front-end costs to develop
new-generation chips climb exponentially, large
revenue streams will be required to recover
these costs. Fewer and fewer customers will have
the volume potential to justify an advanced ASIC
design, and ASSP vendors will struggle to create
standard parts that will support the necessary
volumes. The PLD model, on the other hand,
spreads the costs of developing advanced technology
across a large number of customers who
use the product for a wide range of customerspecific
applications. This approach makes
programmable logic an even more attractive path
to access the advantages of the most advanced
processes. The convergence of these realities
and Alteras SOPC strategy provides fertile
ground for future growth.
With design win momentum in 2001 and the
introduction of new standard setting products,
we have significantly increased our growth
potential. Our new product pipeline is full, and
2002 will bring additional revolutionary products
that deliver more performance for even the most
demanding SOPC applications. We are still at the
beginning stage of expanding the application
space for PLDs. With our broad product portfolio,
our expanding ability to address new applications,
and our time-to-market advantages, we have a
great opportunity to capture market share from
other semiconductor alternatives.
HardCopy Devices
Product development teams today face uncertainty, high
development costs, resource constraints, and time-tomarket
pressure when considering conversion from a
high-density PLD to an ASIC. Alteras HardCopy devices
offer a new solution, unique in the market, that extends
the flexibility, power, and time-to-market advantages of
PLDs to the more cost-sensitive applications traditionally
served by ASICs.
HardCopy devices provide a low-risk, cost-effective, timesaving
alternative to ASICs for high-volume production.
Through a combination of proprietary silicon design and an
automated conversion process, Altera helps customers
move seamlessly from a programmable solution to a
low-cost, custom implementation of their designs. Using
competitively priced HardCopy devices, product development
teams can enjoy the benefits of a shorter development
time and enter the market earlier, while creating a costeffective
solution for volume production.
Customers using HardCopy devices save money, time,
and resources, all with no risk. Together, these advantages
result in a cost of ownership that no ASIC
conversion can match.
HardCopy
HC20K1500E
Actual Die Size Reduction
The die size for a HardCopy device is up to 70% smaller than the
die size of the equivalent PLD, allowing a significantly lower unit cost.
APEX 20KE EP20K1500E
Special Thanks
We asked a lot of our people this year, and they
deserve special thanks. They have remained
resilient and dedicated to delivering the advanced
Altera technology that makes our customers
winners. Innovation requires equal parts of
inspiration and hard work. We are fortunate to
have a gifted and intensely customer-focused
Altera team working around the world to create
Alteras success.
Our customers are also key to the progress we
made during this year of great change. I have
learned from visiting these customers during
this, my first full year at Altera, that their interest
in programmable technology is expanding and
they are pleased to see our ongoing technical
leadership bring them more value. The insights
we gained from them helped shape our products
in countless ways.
We also communicated frequently with our
shareholders and are very grateful for the
encouragement we have received during this
particularly difficult year. We remain committed
to keeping our focus on building value for our
shareholders both today and the years ahead.
Thank you for your support.
John Daane
President and Chief Executive Officer
