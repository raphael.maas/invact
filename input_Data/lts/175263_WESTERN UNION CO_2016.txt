DEAR FELLOW STOCKHOLDERS,
I�m proud of the progress Western Union made over the course of 2016, operating from a position of strength as a global leader in cross-border money movement.
We continued to strengthen the foundation of our business, generating over $5.4 billion in revenue and $1 billion of operating cash flow, while returning nearly $800 million to stockholders through share repurchases and dividends over the course of the year. And it�s notable that we delivered another year of solid, consistent business performance despite global economic challenges, including soft economies in oil-producing countries and an uneven geopolitical environment.

The stability and health of the business allowed us to focus our attention on driving toward our 2020 Vision. And that starts with the opportunity that is uniquely available to us to extend our position as a market leader in cross-border money movement.

Digital Growth
Our technology investments over the last several years have continued to pay off, with westernunion.com money transfer transactions increasing 27 percent year over year, making this the fastest growing part of the business.

Online and mobile are critical focus areas for the company. As customers become increasingly digital, and in particular mobile-savvy, Western Union has expanded its mobile offerings to serve their needs. Mobile money transfers now account for nearly 60 percent of WU�s digital transactions globally. Whether it be via a smart phone, laptop, or retail agent location, customers can choose where and how they want to send and collect their funds.

WU.com is now in 37 countries and the mobile app is available in 18 countries, allowing customers to easily send and receive money to more than 200 countries and territories around the world. And we are intent on expanding these services to more and more customers in more and more countries.

In Business Solutions, we launched WU Edge, a cloud-based platform providing practical, real-time digital capabilities for global businesses of any size to make payments across borders.

Customer Choice
With more than 550,000 agent locations in more than 200 countries and territories, we are in tune with the evolving needs of our retail and payments customers. In 2016, Western Union managed on average 31 transactions per second serving more than 150 million customers worldwide.

A new feature in our mobile app gives consumers the flexibility to choose multiple ways to send money including from an account, card, at one of more than 100,000 ATMs or kiosks, or cash via an agent location.

We also expanded our business payments platform to enable large enterprises to send high volumes of cross-currency and cross-border payments simultaneously. Our enhanced Mass Payments product leverages our unique access to global banking networks to drive speed and efficiency, which has helped us become one of the largest non-bank providers of international payments in the world.

Compliance Programs
Earlier this year, we announced settlement agreements with U.S. federal and state government agencies that resolved investigations focused primarily on the company�s oversight of certain agents, and whether its antifraud programs, as well as its anti-money laundering controls, adequately prevented misconduct by those agents and third parties. The conduct at issue mainly occurred from 2004 to 2012. While these settlements had a significant impact on 2016 financial results, we share the government�s goal of protecting consumers and the integrity of our global money transfer network, and are committed to continuing to enhance our compliance programs.

Our business is based on trust with our customers, and we place a high priority on compliance�not just because it is the right thing to do, but because we want to create the safest global environment for the people, families and businesses that use our services.

Over the past five years, Western Union increased overall compliance funding by more than 200 percent, and now spends approximately $200 million per year on compliance, with more than 20 percent of our workforce currently dedicated to compliance functions. We�re proud that consumer fraud reports associated with Western Union money transfers continue to be extremely low�fewer than one-tenth of one percent of all consumer-to-consumer (C2C) transactions over the past 10 years.

Other enhancements we continue to make to our compliance programs include: hiring employees with law enforcement and regulatory expertise; strengthening consumer education and agent training; bolstering technology investments; and partnering with various consumer advocacy and law enforcement groups. In 2016 alone, our consumer protection education and outreach efforts touched nearly seven million people.

Our Legacy
Since our founding 166 years ago, Western Union has been known as a company that makes a difference in the world. We believe that �when money moves, better things happen��for individuals, businesses and society. As we deliver our brand�s purpose, we realize our potential as an economic enabler for the world�s economies, communities and individuals.

We�re extremely proud that more than 80 percent of our employees contributed their own funds to support the work of the Western Union Foundation this past year, a remarkably high level of employee participation.

It is a privilege to serve as a founding partner of the White House Call-to-Action for private sector engagement on the global refugee crisis. As part of this long-term commitment, the Foundation made a three-year commitment to the Whitaker Peace and Development Initiative, an organization that develops programs to foster peace and reconciliation in disadvantaged communities worldwide. We are proud to partner with our flagship Education for Better participants�including Save the Children in Asia, SOS Children�s Villages, Junior Achievement, National Academy Foundation, Teach for All and Education for Employment�to provide 50,000 women and youth with the job skills needed to succeed in in-demand 21st century jobs over the next five years.

Looking Ahead
Our business decisions are being driven by the voice of the customer. These choices range from our operating model to our investments, how we work together and how well we serve customers. Western Union has begun a lean management process of looking at every aspect of our organization. Known as �The WU Way,� this multi-year effort is intended to more rapidly identify and capture new long-term growth opportunities, improve the customer experience, drive cost efficiencies and foster innovation.

Some early priorities include: delivering end-to-end improvements in how we serve our customers and how we work with agents; improving the productivity of our network; and building upon our technology success even further by creating a more agile and centralized fintech organization.

While I�m proud of our accomplishments and resilience in 2016, I�m even more excited about 2017, and intensifying our focus on serving our customers well.

We are part of a very dynamic and fast-changing global economy. Western Union�s track record of innovation since our founding 166 years ago has helped build a powerful cross-border money movement platform. And we are well-positioned to help our customers navigate the shifting landscape.

Through our distinct combination of agent locations and digital products, Western Union is ready and committed to help our customers move money with ease, simplicity and confidence.

I look forward to sharing our progress with you again next year.


Hikmet Ersek
President, Chief Executive Officer and Director