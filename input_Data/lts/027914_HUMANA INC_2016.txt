DEAR FELLOW
STOCKHOLDERS,
Healthcare is in a dynamic state, and the public is
looking to companies like Humana to make health
benefits more affordable while improving the overall
cost of care and consumer experience. To that end,
Humanas innovative strategy continues to capitalize
on industry changes to progress toward these goals
through our integrated care delivery model.
We understand that healthcare is complicated,
and dealing with multiple physicians and other
healthcare professionals can be a confusing and
daunting task. That is one of the principal reasons
why Humana will enhance its integrated care
delivery strategy in key areas to enable a better
and more seamless locally delivered health care
experience for our members.
Our 2016 operating results are strong, particularly given
the complex operating environment during the extended
transaction process that was terminated by mutual
agreement on February 14, 2017.
As we renew and enhance as an independent company,
we are continuing to build on our integrated care delivery
strategy by intensifying our focus on the cohort of people
living with chronic conditionsparticularly those who are
aging into or are already in Medicare Advantage plans. This
population is fast-growing, with more than 85 percent of
seniors having at least one chronic condition and 65 percent
having multiple chronic conditions.
We believe by extending and expanding the local
capabilities of our Healthcare Services businesses and
supporting doctors and other healthcare professionals
with unsurpassed population-health tools, we can
remove barriers for our members. This will better
help them achieve their best health by enabling
both members and healthcare professionals easier
engagement in more holistic healthengagement that
brings together the lifestyle and clinical factors so critical
to improving lifelong health and well-being. We expect
this will, in turn, continue to drive growth across the
enterprise as growth in our health plans furthers growth
in our Healthcare Services businesses. We believe this will,
in turn, lead to favorable returns for our stockholders.
We will continue our practice of evaluating our
portfolio of businesses to ensure focus on those lines
of business where our integrated care delivery strategy
can add value for health plan members, healthcare
professionals and our government program and
employer group customers.
Over the past few years, we also have attempted to bring
accessible, quality healthcare to individuals in a number
of state markets across the U.S. through our Affordable
Care Act healthcare exchange offerings and have worked
diligently to maintain the viability of the product and
continued availability across most parts of the country.
However, due to a persistently unbalanced risk pool in
this market driven by programmatic factors, we will not
continue to offer this coverage for 2018.
Turning to other elements of our strategy for 2017
and beyond, we have established a thoughtful capital
deployment plan to raise our cash dividend and to
implement a strategic share repurchase program.
These efforts should jump start activities such as
these that were frozen while the proposed merger was
pending. Additionally, we will focus our mergers and
acquisitions efforts on capabilities that enhance the
clinical and consumer experience for our members.
We believe our strategic, operating and capital
deployment plans not only improve the value Humana
brings to members and healthcare providers, they also
enhance the growth platform for both our health plans
and our Healthcare Services businesses, positioning us
well for long-term, sustainable growth.
We look forward to the future with confidence, while
gratefully acknowledging the enormous contributions to our
success over the past quarter-century of Jim Murray, our
retiring Executive Vice President and Chief Operating Officer.
Jims exceptional mentoring for many of our leaders,
particularly those in segment president roles, has positioned
us well for the future as our segment presidents are ready
for broader responsibilities within the organization.
Throughout its history, Humana has been dedicated
to improving peoples health, from our start as a
nursing home company more than 50 years ago
through our transition to a hospital company and
today as a health and wellness enterprise.
We look forward to continuing to help our members
achieve their best health by simplifying their healthcare
experienceone that combines their unique needs
with the clinical aspects critical to improving their
lifelong health and well-being.
Sincerely,
Kurt J. Hilzinger
Chairman of the Board
Humana Inc.
Bruce D. Broussard
President, Chief Executive Officer and Board member
Humana Inc.