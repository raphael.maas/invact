Dear Fellow Shareholders:
2010 was a value-creating year for our Company. While
our financial results reflected the impact of lower natural
gas prices, we achieved the best operating results in our
Companys history. Proved reserves increased 42% to 4.4
Tcfe. Reserve replacement from all sources totaled 931%.
Importantly, this reserve growth was achieved at an
industry-leading finding and development cost of $0.71
per mcfe. Despite selling our legacy Ohio properties for
$325 million, production grew 14%, representing our
eighth consecutive year of production growth. At Range,
we are keenly focused on growth on a per-share basis.
We are pleased to report that once again Range achieved
double-digit per-share growth in both production and
reserves. Not only did we achieve double-digit per-share
growth, but we coupled it with one of the lowest cost
structures in the industry.
Our financial results felt the impact of a 19% drop in
realized commodity prices. Lower prices more than offset
higher production, causing cash flow to decline 14%.
Due to the announced agreement to sell the Barnett
Shale properties, a non-cash impairment was recorded
at year end, resulting in a reported loss of $318 million
for the year. Adjusted net income comparable to analysts
estimates was $89 million or $0.56 per share. We are
encouraged by the tangible results we saw on the cost
side of the income statement. Depreciation, depletion
and amortization expense, the largest expense item for
most natural gas and oil companies, decreased 14% on
a unit-of-production basis. Direct operating costs, our
second largest line item, were reduced 12% on a per-unit
basis. Importantly, we ended 2010 with a stronger financial
position as our liquidity reached nearly $1 billion.
Looking ahead, we are off to an excellent start
in 2011. Recently, we announced the signing of an
agreement to sell our Barnett Shale properties for $900
million, subject to adjustments. Assuming the sale is
completed in April as planned, coupled with the recent
renewal and extension of our bank credit facility, we
will have the strongest balance sheet in our history
with $1.9 billion of go-forward liquidity. We expect the
Barnett sale to be a catalyst for Range to become cash
flow positive in 2013. By selling our more mature, higher
cost properties, like the Ohio properties in 2010 and the
anticipated Barnett sale in 2011, we are strengthening
our financial position and preserving the resource potential
of the Marcellus Shale and our other key projects for
our shareholders. Currently, we estimate our unproven
resource potential to be 35-52 trillion cubic feet equivalent,
which is 8 to 12 times our current proved reserves.
For 2011, we anticipate that production will increase
by 10% despite selling the Barnett Shale properties,
which currently make up more than 20% of our production.
In fact, we expect to fully replace the Barnett Shale
production by the end of the third quarter 2011. The
Marcellus Shale will drive these results, as we anticipate
doubling our Marcellus Shale production from a rate of
just over 200 Mmcfe per day at year-end 2010 to a rate
of 400 Mmcfe per day by year-end 2011. For 2012, we
are currently expecting Marcellus Shale production to
reach 600 Mmcfe per day by year end. Company-wide, we
anticipate 2011 production to increase by 25% to 30%.
For both 2011 and 2012, we currently project that finding
and development cost will average $1.00 per mcfe or less.
Due to the success of a number of shale gas plays
in the United States, our domestic natural gas supply
has increased dramatically, causing natural gas prices
to decline. While this is good news for our country and
consumers, it makes for a challenging time for natural
gasfocused production companies. Many companies
are shifting their capital spending to more speculative
oil projects. At Range, we are uniquely positioned to
prosper in this period of low natural gas prices. Over the
past several years, we have taken a disciplined approach
toward lowering our cost structure by selling our more
mature, higher cost properties and investing our capital
into higher return, lower cost properties.

As a result, we have the highest quality drilling inventory
in our history. Our $1.38 billion capital budget for 2011
is focused primarily on the Marcellus Shale. In the southwest
portion of the play, where most of our 2011 capital
will be spent, we estimate that we can generate 50%+
rate of return at $4.00 natural gas prices.
While there is much rhetoric regarding domestic
energy policy, we see natural gas as a primary element to
our countrys energy future. Natural gas is substantially
cleaner than oil and coal. Natural gas is domestic and
creates jobs in the U.S. Natural gas is plentiful and can
be produced safely, while protecting our environment
using todays technology. Importantly, natural gas can be
developed at a cost substantially lower than oil. Natural
gas is also critical for the development of renewable
energy, because when the sun doesnt shine and the wind
doesnt blow, natural gas will be there to provide a lowcost
backup to wind and solar electric generation. The
United States today finds itself in a precarious position
with regard to energy policy. We rely almost exclusively
on foreign oil to power our transportation system. Coal is
the largest source of electricity generation, and much of
the electric generation fleet is old and not environmentally
efficient. While there is no single cure, natural gas
provides a safe, secure, low-cost solution that is readily
available today.
In this report, we are focusing on Uncovering
Tomorrows Energy Today. Thanks to new shale plays
like the Marcellus Shale, we have uncovered an abundant
supply of clean, affordable, domestic energy that
is available right now. In addition to thanking all of the
Range employees for a job well done, we wish to thank
our fellow directors and most importantly, our fellow
shareholders, for your steadfast support.
John H. Pinkerton
Chairman & Chief Executive Officer
jeffrey l. ventura
President & Chief Operating Officer