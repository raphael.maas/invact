                                         To Our Shareholders
                                         I am pleased to report that instead of being just another strong year for Cummins,
    "As a result of
                                         2007 was a record setter and an outstanding year in most respects.
  our outstanding
      technology,
                                         Sales exceeded $13 billion  a 15 percent increase over 2006. Earnings before
  Cummins Heavy
                                         interest and taxes were $1.2 billion  or 9.4 percent of sales. Net earnings were
     Duty monthly
                                         $739 million, compared to $715 million for the previous year.
     market share
  averaged above
                                         This fourth straight year of record sales and profits reinforces our value promise to
    40 percent for
                                         shareholders. As of the end of 2007, Cummins' investors have enjoyed a five-year
           the last
                                         average annual total return of 58 percent.
     two-thirds of
         the year."
                                         Along with this outstanding performance, we have increased our dividends by 67
                                         percent and have purchased almost $500 million in stock over the last two years.
                                         In December, we announced plans to buy back another $500 million worth of shares.
                                         We also executed a pair of two-for-one stock splits, one during 2007 and another
                                         in early 2008.






         Our success in 2007 was driven by increased sales in all our business segments.
         Power Generation, Distribution and Engine once again delivered excellent results.
         They had record revenues, increased share in many of their markets and offered
         innovative products and services, which delighted Cummins' customers around
         the world.

         Notable among our segments were Power Generation, which continued its trend of
         improving results  going from an EBIT loss of $19 million in 2003 to $334 million in
         EBIT in 2007  and Distribution, which has seen its sales more than double in the last
         four years, growing to $1.5 billion in 2007.

         The Components segment also experienced a growing demand for its products,
         which are highly valued by both Cummins and our customers. The successful
         technologies in our filtration, aftertreatment, turbocharger and fuel system component







                                                                                                                  5
   "We announced                        products have allowed us to produce cutting-edge, environmentally-friendly
                                        customer solutions for toughening emissions standards around the world.
      exciting new
     products that
                                        While Components' performance exceeded previous years, this segment's results
    were launched
                                        still need to improve significantly. To ensure Components future success, we have
          or will be
                                        stepped up our focus on operational issues, particularly in the Emission Solutions
        introduced
                                        and Turbo Technologies businesses.
    during the next
     several years,
                                        We have a proven track record of dealing with operational challenges, as illustrated
          including
                                        by the turnarounds in the Heavy Duty engine and Power Generation businesses
      engines that
                                        earlier in the decade. So we remain very optimistic about Components' ability to
        meet tough
                                        achieve significant sales and profit gains in the future.
  global emissions
         standards."


Along with our financial success, we are proud of several major business and product
accomplishments during 2007.

We announced exciting new products that were launched or will be introduced during
the next several years, including engines that meet tough global emissions standards.

Among our new offerings was the next generation XPI high pressure fuel system,
which was introduced on our 2007 C/L mid-range engines. In 2010, XPI will be
applied to heavy-duty on-highway truck engines to meet the upcoming 2010
emission regulations. The XPI was created through our partnership with Scania.

As a result of our outstanding technology, Cummins Heavy Duty monthly market
share averaged above 40 percent for the last two-thirds of the year. In my 36 years
at Cummins, I cannot remember this large a market-share shift. We also enjoyed
growing market leadership around the world, including transit bus, power generation,
marine, medium-duty trucks, recreational vehicles and many more.




   "For the second                     For the second time in five years, Cummins received recognition as Diesel Progress
                                       magazine's Newsmaker of the Year. We also earned a prestigious award for customer
 time in five years,
                                       satisfaction from J.D. Power and Associates for engine performance, cost of
Cummins received
                                       ownership and warranty in the heavy-duty vocational engine segment. In addition,
     recognition as
                                       Cummins QSB3.3 was awarded the Diesel Engine of the Year for 2007.
   Diesel Progress
        magazine's
                                       Our outstanding Six Sigma efforts continued to improve productivity and increase
     Newsmaker of
                                       operational efficiencies for Cummins and our customers. We closed a record 2,800
           the Year."
                                       projects in 2007, including some 500 customer-focused projects, and we have now
                                       trained 7,000 belts since 2000.

                                       Living Our Values

                                       During 2007 we continued our focus on demanding that everything we do leads to
                                       a cleaner, safer, healthier environment. We published our fourth Sustainability Report.
                                       We have pledged to reduce our greenhouse gas emissions from our facilities by
                                       25 percent by 2010 from 2005 levels (adjusted for sales) as part of our commitment to
                                       the U.S. Environmental Protection Agency's Climate Leaders Program.



         The Cummins/Chrysler team that developed the innovative 6.7-liter diesel engine
         and engineered it into the Dodge Ram heavy-duty pickup truck won Chrysler's
         Environmental Leadership Award. We were named to the Dow Jones World
         Sustainability Index for the third time.

         Along with these initiatives, we were recognized for activities that reflect our efforts to
         operate with integrity and to create a great place to work.

         We were selected a top company for diversity and the No. 1 company in the U. S. for
         Asian and Asian-Americans for the second time. We were singled out as one of the
         "100 best places to work in IT" by IDG's Computerworld magazine. We were listed
         among the top ethical companies in the U. S. for the ninth straight year. We also were
         cited as one of the Top Companies for Leaders 2007 by Fortune magazine.
         Cummins' joint venture partner, KPIT Cummins, won the prestigious India-based
         Golden Peacock Award for Excellence in Corporate Governance.




                                                                                                
        "We expect                     Employees continued to demonstrate their ongoing commitment to corporate
                                       responsibility. Whether it was helping employees in China left homeless by massive
   earnings growth
                                       flooding or sending care packages to U. S. soldiers abroad or feeding orphans in
      to accelerate
                                       Rwanda or volunteering for Special Olympics, Cummins employees opened their
   again in 2008 as
                                       hearts to help others. This generous spirit has been a tradition at Cummins for almost
     every segment
                                       90 years.
       improves its
          operating
      performance,                     Looking to the Future
         and we are
                                       As we look to the future, our focus will continue to be profitable growth.
  predicting strong
                                       We expect earnings to grow faster than sales in 2008 as every segment improves
    sales increases
                                       its operating performance, and we are predicting strong sales increases and nearly
      and nearly 20
                                       20 percent growth in our EBIT. This high level of confidence in our business results
    percent growth
                                       is based on several important factors.
        in our EBIT."



First, Cummins' advanced technology has opened new doors for our products and
will drive our longer-term growth. In particular, our technology will play an important
role in markets related to more stringent emissions standards, better fuel economy,
generating electricity and the needs of developing countries, such as China, India,
Brazil, Russia, Vietnam and Nigeria.

Second, we have already increased our capital investment in many parts of the world
in anticipation of these growing markets. We consider new plants and facilities 
particularly in China and India  as a primary requisite for being a significant
participant in these markets.

Along with the manufacturing and technical centers we built in recent years, we are
planning 19 new plants and 42 capacity expansion projects, many of which are
underway. These include a new fuel system plant in China, our light-duty project
with Beiqi Foton Motor Company in Beijing and major expansions in India for both
midrange and high-horsepower engines, power generation and turbochargers.




               "Our                    Over the next five years, our consolidated entities will make $2.5 billion in capital
                                       expenditures. Together with our partners, we will make another $1 billion in capital
   investments in
                                       expenditures in our joint ventures. Our investments in China and India through 2009,
  China and India
                                       including contributions from our joint venture partners, will total nearly $600 million.
    through 2009,
          including
                                       Most of these investments will drive profitable growth in the future as new engine
     contributions
                                       platforms begin production in 2009 and 2010. Investments such as those in high-
    from our joint
                                       horsepower engine capacity, fuel systems, turbochargers and aftertreatment will pay
 venture partners,
                                       more immediate returns.
   will total nearly
      $600 million."
                                       Along with our willingness to invest capital in our quest for profitable growth, we are
                                       continuing to look for new partners in those areas where it makes sense. Our access
                                       to these markets is made easier by our joint venture relationships, which allow us to
                                       partner with established global companies, such as Dongfeng Automotive
                                       Corporation in China, Tata Motors Ltd. in India and Kamaz Inc. in Russia, with limited
                                       investment and volume commitment from our partners providing good returns.




Third, Cummins is more diversified and better able to handle the cyclical demand in
our markets than at any time in our history. For example, even as some markets have
softened, new ones have emerged.

A fourth reason for my optimism is the improvement in our ability to deliver bottom-
line performance. We have focused on margins and controlling costs, and, as a
result, over the past seven years we have grown EBIT at four times the rate of sales.

Finally, our balance sheet is in good shape, with very little debt, positive cash flow
and fully-funded qualified pensions. This financial health gives us the flexibility to
invest in the people, products, facilities and technologies we need to capture
profitable growth opportunities when they arise.




                                                                                   
              "We have                   These are exciting times for Cummins. We have outstanding products and technology
                                         leadership, excellent partners, expanding markets and growing share, a disciplined
           outstanding
                                         approach to investments and a strong balance sheet. Our people are extremely well
         products and
                                         prepared to take advantage of the profitable growth opportunities before us.
            technology
            leadership,
                                         Let me conclude by expressing my gratitude to the Cummins Board of Directors,
             expanding
                                         whose guidance on behalf of the Company and its owners provides enormous value.
          markets and
                                         I would like to single out the outstanding efforts of John Deutch, who has left the
        growing share,
                                         Board after serving as a member for 10 years. John has had a distinguished career
          a disciplined
                                         both in government and in education, serving as a professor at the Massachusetts
          approach to
                                         Institute of Technology since 1970. As a member of the Board's Finance Committee
      investments and
                                         and Chairman of the Technology and Environmental Committee, John could be
               a strong
                                         counted on to ask the tough questions and lend valuable scientific insight to many
        balance sheet."
                                         issues affecting the Company. We wish him well in his future endeavors.



         Finally, I want to express my appreciation to the 37,800
         Cummins employees worldwide, as well as to our
         customers, suppliers, vendors, contractors and other
         stakeholders, including the communities where we
         work, for continuing to lend their support to this truly
         great company.

         Tim Solso

         Chairman and Chief Executive Officer
         Cummins Inc.




                                                                