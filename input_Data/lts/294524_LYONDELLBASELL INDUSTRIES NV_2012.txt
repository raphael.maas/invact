To Our Shareholders
In 2012, we seized the moment. Our profits reached
a new record level and total annual shareholder
return was 89 percent. We joined the S&P 500 and
earned an investment grade credit rating.
High achievement begins with high expectations. When I joined LyondellBasell four years ago, we publicly stated that our company
would target becoming the top competitor in our industry. I believed in the capabilities of our talented people and our advantaged
assets. We adopted a strategy based on a very simple value proposition - operate safely and efficiently, manage our business very well
and return value to shareholders. Despite its simplicity, our back to basics strategy has remained unchanged and is the foundation of
our continued success.
By staying true to our core strategy, we delivered on our promise to create value for shareholders in 2012. We maintained an emphasis
on Operational Excellence and continued our disciplined use of cash. We strived to maximize the efficiency and value of each of our
business segments as we acted swiftly to seize the moment and secure our future.
For the fourth consecutive year, our safety and environmental performance improved around the globe. Injuries have decreased 50
percent since 2009. In 2012, employees and contractors set a new record, ending the year with a recordable injury rate of 0.23  a
strong improvement over the 0.35 rate achieved in 2011. Environmental incidents decreased 31 percent and process safety incidents
decreased 14 percent. These numbers are near best in class for our industry. However, our ultimate goal is zero injuries and incidents.
We intend to continue our progress toward safety perfection.
In 2012, LyondellBasells profits reached a new record level and total annual shareholder return was 89 percent. We outperformed
most industry peers and generated $5.9 billion of EBITDA and earnings of $4.96 per diluted share from continuing operations. I am
proud that we also continued to offset inflation and maintained flat underlying fixed costs for the fourth consecutive year. Through
disciplined cash management, we further strengthened our balance sheet. As a result of these actions, we joined the S&P 500 Index
and have been elevated to an investment grade credit rating.
From a commercial perspective, recent shale gas developments continued to create a global advantage for our assets in the United
States as we benefited from lower natural gas and related natural gas liquids prices. We advanced our North American olefins growth
projects and announced additional expansion options. Upon completion, our olefins expansion programs will increase our total U.S.
ethylene capacity by approximately 20 percent  equivalent to the size of an existing cracker. We will be on stream earlier and at lower
cost than building a new plant. We also advanced other major projects, such as signing a memorandum of understanding to advance
our propylene oxide joint venture in China. In Europe, we focused on restructuring efforts due to the difficult market environment. We
developed plans for further optimization that will strengthen our competitiveness.
From a manufacturing perspective, we completed maintenance turnarounds at two of our largest sites and several other locations.
These and other investments paid off as evidenced by more than 60 production records achieved during 2012. In the U.S., we
increased natural gas liquids feedstock flexibility. LyondellBasell is now producing close to 85 percent of its U.S. ethylene from
advantaged feedstocks.
Overall, 2012 was a very successful year in terms of our annual results and our progress on growth initiatives. The 13,500 people of
LyondellBasell understand our strategy and how they individually contribute to our long-term success. We are proud of what we have
achieved together and believe we are well positioned for continued success. We will not rest on our laurels. LyondellBasell is on its way
to becoming the best company in our industry.
Sincerely,
James L. Gallogly
Chief Executive Officer