DEAR FELLOW STAKEHOLDERS

On November 18, 2010, the General Motors team, along
with our United Auto Workers partners, experienced
something special as we stood together on the balcony
of the storied New York Stock Exchange. As we rang the
opening bell to the unmistakable sound of a Chevy
Camaro engine at full throttle, we knew we were doing
much more than just starting a new trading day. We
were revving up Wall Street, and setting the pace for
our companythe new General Motors.
We truly are building a new GM, from the inside out.
Our vision is clear: to design, build and sell the worlds
best vehicles, and we have a new business model to bring
that vision to life. We have a lower cost structure, a
stronger balance sheet and a dramatically lower risk
profile. We have a new leadership teama strong mix of
executive talent from outside the industry and automotive
veteransand a passionate, rejuvenated workforce.
Entering the public equity market capped a year of historic
change. And yet, at that moment on the NYSE balcony,
for us and for our employees around the world, it was
crystal clearour work was just beginning.
A strong foundat ion
GM made important operational and financial progress
in 2010, and set a strong foundation to build upon for
the future.
GM, with its joint venture partners, maintained its leading
market position in the key growth markets of Brazil, Russia, India and China (the BRIC countries) during the year, with
a particularly strong performance in China, where GM and
our partners delivered 2.4 million cars and trucks. Meanwhile, we continued the aggressive implementation of our
restructuring plan in Europe to position our business to
operate at a lower break-even level.
Our plan is to steadily invest in
creating world-class vehicles, which
will continuously drive our cycle of
great design, high quality and
higher profitability.
In the United States, GM saw robust sales across all of
our brandsCadillac, Chevrolet, Buick and GMCthanks
to an exciting lineup of vehicles that have captured the
imagination of customers and critics alike, epitomized
by the launch of the Chevrolet Voltthe groundbreaking
extended-range electric vehicle. GM vehicles won
numerous awards throughout the year, led by the Volt,
which ran the table with the North American Car of the
Year, Automobile Magazines Automobile of the Year,
Green Car of the Year, a Car and Driver 10 Best Award
and Motor Trend Car of the Year.
Continuing that progress, we kicked off 2011 with another
big win with the Chevrolet Silverado HD winning the top
honor as Motor Trend Truck of the Year. It was the first time GM brands captured both of the prestigious Motor Trend
Car and Truck of the Year awards since Buick and Chevrolet
captured both awards in 1979.
And across the globe, other GM vehicles are gaining similar
acclaim for design excellence, quality and performance,
including the Holden Commodore in Australia, Chevrolet
Agile in Brazil, Buick laCrosse in China and many others.
We also acquired AmeriCredit to form GM Financial,
expanding the fi nancing options for customers in the
United states and Canada who want to buy or lease
new GM vehicles.
We lowered our cost base and restructured operations
in north America to achieve a break-even level near the
bottom of the economic cycle. Despite depressed industry
conditions, GM posted $5.7 billion of earnings before
interest and taxes (eBiT) in north America. We signifi cantly
improved our balance sheet, reduced debt by more than
$11 billion and improved the funding level of our U.s.
pension plans with a $4 billion cash contribution. At the
same time, we maintained our strong liquidity position
through positive cash fl ow and a new $5 billion revolving
line of credit.
in november we completed a landmark $23 billion public
off ering, the largest in history, allowing the U.s. and
Canadian governments to reduce their ownership stakes
in the company. The successful off ering was an important
vote of confi dence for the progress and potential of the
new General Motors.
BRiGHTeninG FinAnCiAl PiCTURe
We were pleased to achieve profi tability in our fi rst full
year as a new company, with 2010 net income attributable
to common stockholders of $4.7 billion on revenue of
$135.6 billion. We achieved earnings per share of $2.89
on a fully diluted basis.
our 2010 progress
 is early evidence
 of a new business
 model that begins
 and ends with great vehicles.
GM recorded adjusted eBiT of $7 billion and positive
automotive free cash fl ow of $2.4 billion in 2010. excluding
a $4 billion discretionary contribution to the U.s. pension
plans, free cash fl ow would have been $6.4 billion for
the year.
The companys progress is early evidence of a new
business model that begins and ends with great vehicles.
We are leveraging our global resources and scale to
maintain stringent cost management while taking
advantage of growth and revenue opportunities around
the world, to ultimately deliver sustainable results for
all of our stakeholders.
The strength of the new GM:
a new business model centered on our
vision of designing, building and selling
the worlds best vehicles; a leaders
leverage to economic growth in key
mature and emerging markets worldwide;
and a new balance sheet with a significantly improved risk profile.
The road ahead
It was a good year for GM, but we have a lot of work ahead
of us. Although great opportunity abounds around the
globe, risks do as well. In a still-recovering global economy,
uncertainty surrounding the crisis in Japan, a volatile
oil price environment, higher commodity prices and an
increasingly competitive automotive marketplace, we will
build on our 2010 progress by concentrating on three
critical areas.
First, we will remain focused on our top prioritydeveloping and introducing great new products to our valued
customers worldwide. Key launches in 2011 include the
fuel-efficient and sporty Chevy Sonic and the Buick Verano
luxury small car in the United States; the Opel Zafira
seven-seat MPV and the Ampera extended-range electric
car in Europe; and our all-new Baojun brand in China.
As we regain our financial footing, we expect the number of
new product launches to steadily rise over the next several
years. And these new products will increasingly embrace
advanced technology to reduce fuel consumption and
emissions, improve safety and enhance the overall driving
experience for our customers.
Advanced technology is key to GM product leadership in
the future. This is what makes the Volt so important for GM.
Its not just another me too vehicle. While it is by no means
the ultimate solution for reducing our dependence on oil,
the Volt is a glimpse into whats possible from GM. We will
leverage what we have learned in its developmentfrom
the automotive battery, electric power control and other
new technologies to the processes and partnerships we
createdto help us accelerate the pace of innovation
across the company.
Second, we will continue to sharpen our focus on how we
engage customers. Fundamental to this is designing and
producing vehicles that surprise and delight them. Were
doing that by listening to customers, taking a wider view
to predict emerging trends, ensuring we have the right
features and technologies in our vehicles to set them apart
from the rest, and enhancing our advertising and marketing
efforts to more effectively connect with customers.
A key priority is to sharpen our focus
on engaging our customers. Thats
critical to ensuring that we have the
right features and technologies in
our vehicles to win in a competitive
marketplace.
Were working closely with our revitalized dealer network
in the United States to deliver the best sales and service
experience possible, and we continue to implement a highly
disciplined inventory management approach worldwide to
create an integrated, holistic brand experience everywhere
we sell vehicles. In all efforts, well never forget whats most
important: our customers.
our third area of focus is fi nancial discipline. We will
maintain a sharp global focus on cost management as
we invest in products and technology and expand to
meet increasing demand. At the same time, we are building a strong and resilient balance sheet, and expect to
maintain a minimal level of debt and to take meaningful
steps toward fully funding our U.s. pension plans.
BUilDinG THe neW GM
We are moving with increased speed and agility, and
implementing change faster than ever before. We are
becoming a company with the capability, resources
and confi dence to play off ense, not defense. instead of
creating new vehicles that are just better than their
predecessors, were working to design, build and sell
vehicles that defi ne the industry standard.
i would like to close this letter with sincere thanks, from
every one of us at General Motors, to the American and
Canadian people and their governments. We will always
be grateful for their support in GMs hour of greatest need,
and we are determined to prove that this was an investment worth making.
i would also like to thank the investors who made our
public off ering so successful. Most of all, i want to thank
our employees, retirees, dealers, union partners and many
other stakeholders who stood by us through the toughest
of times, and who made the sacrifi ces necessary for us
to create our new company.
For the fi rst time in decades, the playing fi eld in the
auto business is level. now, the best car truly can win.
GM can now dedicate its full attention to designing,
building and selling the worlds best vehiclessomething
that we hadnt been able to do in the past, due to a
historical cost structure that was unsustainable, distracted
our resources and hindered our ability to compete.
We will never forget the path that led to the old GMs
bankruptcy and the sacrifi ces that were made by many to
create the new GM. Most important, we learned from that
experience, we understand why it happened and we will
never go back there again. That is our commitment.
GMs strong 2010 results were evidence of what we now
can achieve. Make no mistake; we have a long road ahead.
There will be many bumps and unexpected bends.
And we are building the right vehicle to navigate them:
The new GM.
Thank you.
sincerely,
Daniel F. Akerson
Chairman & Chief executive officer
