Although our financial results were not as strong in the last monfiof
the year as we had forecasted, we remain optimistic about the future.
As we look ahead, we believe that our business strategy and our lineup
of compelling product offerings position us well for continued growth.
For nearly four decades, Intel has embraced change and an unwavering
commitment to moving technology forward, but perhaps at
no time has this been truer than now. Since our founding in 1968, we
have successfully transformed from our roots as a memory company
to become the worlds largest microprocessor company. Today, we
are reinventing Intel once again, to focus on the growfiopportunities
presented through platformsadvanced solutions that integrate
Intel microprocessors and other technologies, such as complementary
chipsets and communications chips, all optimized to work together.
The technology fueling our new platform strategy is still deeply
rooted in our strengths in silicon architecture design and manufacturing
technology, but today our employees share a new common mission:
to delight our customers, employees and stockholders by relentlessly
delivering Intel platform and technology advancements that become
essential to the way we work and live.
Platform Strategy. During 2005, we set in motion a broad corporate
re-alignment around our platform strategy, creating six business
groups that target growfiopportunities where we believe we can
offer significant value to end users:
The Mobility Group designs and delivers platforms for notebook
PCs and handheld devices. Intel Centrino mobile technology is an example
of one of our first successful platforms. This platform combines
a processor, chipset and wireless network technology to optimize
performance, battery life, thin-and-light system design and wireless connectivity
for mobile PC users. Due largely to the sales of these products,
the Mobility Group grew 59% and accounted for more than one-fourth
of Intels revenue in 2005. In early 2006, we introduced a new generation
of this platform, Intel Centrino Duo mobile technology, based on
our new dual-core Intel Core Duo processor.
The Digital Enterprise Group focuses on computing and communications
platforms for end-to-end business solutions as well as consumer
desktop PCs. It is our largest operating segment, accounting for 65%
of our total revenue. Among several other introductions, the group
delivered our first dual-core Intel Xeon processors for server platforms
in 2005.
The Digital Home Group delivers computing and communications
platforms for consumers in the emerging digital home, wifian emphasis
on entertainment applications and consumer electronics devices. The
first systems based on our new digital home platform, Intel Viiv
technology, premiered in early 2006. Intel Viiv technologybased
systems are designed to make it easier to download, manage and share
the growing amount of digital content available worldwide, and enjoy
it on a choice of TVs, PCs or handheld devices.
The Flash Memory Group provides NOR  ash memory products,
which are commonly used for storage in cell phones, set-top boxes,
networking products, DVD players and other devices. Beginning in
2006, the groups products include NAND  ash memory products
manufactured by IM Flash Technologies, a newly formed company
announced by Intel and Micron Technology in 2005. NAND  ash
memory is a popular storage technology for digital audio players,
digital cameras and other consumer electronics devices.
The Digital HealfiGroup targets business opportunities in healthcare
research, diagnostics and productivity, as well as personal
healthcare. The group plans to have new platforms in trials in 2006.
Finally, the Channel Platforms Group works to expand on Intels
success in global markets by focusing on developing and selling
Intel products to meet the unique needs of local markets worldwide.
An example of the groups 2005 activities is the development of
a platform specifically for Chinas growing Internet cafe industry.
Manufacturing and Dual Core. At the heart of our platform
approach is our ability to consistently deliver architectural innovation
coupled wifiworld-class, high-volume manufacturing. At the end
of 2005, we manufactured the majority of our microprocessors on
90-nanometer process technology in our 300mm fabrication facilities,
but we also began production of our dual-core microprocessors using
our industry-leading 65-nanometer process technology. In 2006, we
are rapidly increasing high-volume production of our 65-nanometer
dual-core processors to support our platform initiatives.
Dual-core processor technology is a vital part of Intels vision of
multi-core computing. Compared to processors wifionly one core, multicore
processors (processors wifitwo or more cores) deliver higher
system throughput and simultaneous management of activities
while balancing power requirements. We have more than 15 multicore
projects in development for a variety of market segments.

In 2005, we reported our 19ficonsecutive year of profitability, underwent our largest reorganization, embarked
on a rebranding effort and helped usher in the dual-core eraa major technological leap in computing architecture.
Growth. In 2005, for the first time in history, more than 200 million PCs
were sold worldwide in a single year. Shipments of PCs grew by approximately
16% in 2005, the third consecutive year of double-digit growth.
Intel continues to benefit from this growth, especially in the mobility
segment and in emerging markets just beginning to embrace technology.
We are also optimistic about future growfias users continue to discover
new ways to use Intel technology, such as Intel Viiv technology, which is
helping transform the way people enjoy entertainment in the digital home.
In addition, we are focused on opportunities through new customer
relationships. We are pleased that Apple Computer introduced its first
MacBook Pro* laptop and iMac* desktop computers based on the Intel Core
Duo processor in early 2006. Also, Research In Motion announced that it
will use Intel XScale architecturebased cellular processors for its nextgeneration
BlackBerry* handheld communications devices.
Our investments in Intels manufacturing network have enabled us
to take advantage of growfiopportunities. In 2005 alone, we spent
$5.8 billion in capital investments. Because of our continuing commitment
to investments in capacity, Intels manufacturing network is now
unmatched in scope and scale in our industry.
A New Era. The redesigned logo on the front cover of this report
signals a new era for Intel that builds on our rich heritage as one of the
worlds most trusted technology leaders. We refreshed our previous logo,
a symbol of a more industrial age, and believe that the new logo with
its Intel. Leap ahead. tagline is a simple expression of our continuing
mission to challenge the status quo and deliver technologies that make
life better and richer for everyone.
I would like to thank the employees of Intel for their continuing hard
work and dedication. It is an honor for me to assume the position of
Intels chief executive officer, following in the very large footsteps of my
four predecessors. All of us at Intel can be inspired by their legacy as we
move forward and help change the worldagainby leaps and bounds.
Paul S. Otellini, President and Chief Executive Officer
Paul Otellini, a 31-year Intel veteran, took over as chief executive officer.
The selection of Paul to become CEO was perhaps the most important
action taken by the Board in 2005. This culminated several years of
internal discussion and analysis by Board members as they reviewed
the performance and development of key senior executives. Paul was
selected as the best candidate to lead Intel going forward, bringing
wifihim new views and strategies while maintaining the unique culture
and style of the company.
In parallel wifiPauls elevation to CEO, the Board has continued
its evaluation of the next tier of executives in its quest to identify
and maintain a list of qualified internal candidates to succeed Paul. As
Andrew Grove described in his chairmans letter last year, succession
planning is an ongoing event at Intel, and the Board works wifithe
CEO to identify, develop and groom the next CEO. We have formal reviews
on the topic twice a year, and the Board actively interacts wifisenior
executives on a routine basis to form independent assessments of the
capabilities and strengths of various candidates. While we anticipate a
long and successful career for our current CEO, we want to be prepared
in advance for the next leadership transition.
Similarly, in the continuing evolution of our corporate governance
practice, perhaps the most important of the Boards recent actions
was its adoption of a majority vote standard, to begin in 2006, for the
election of directors in uncontested elections. Director candidates rarely
run against opposing candidates as in political elections; therefore,
stockholders want a meaningful way to affect the election of directors.
As a matter of director accountability, we think it is important that
stockholders be able to vote yes or no for director nominees. And
if the no votes win, the Board has the authority and responsibility to
decide whether to retain or replace that director.
Corporate social responsibility is an increasingly important topic
in boardrooms and among stockholders. At Intel, we recognize that
the companys continuing success rests upon our ongoing dedication
to corporate excellencein the boardroom, in the workplace and
in the communities where we operate. In May 2006, Intel will publish
its fiffiCorporate Responsibility Report, available on the Internet
at www.intel.com/go/responsibility. The report provides information
about the companys performance on a wide variety of environmental,
healfiand safety issues, as well as social programs and performance.
I encourage all of our stockholders to review this report, as the material
in it complements that contained in our financial statements and
provides important information about how our company operates
in todays complex world.
Craig R. Barrett, Chairman
In my first letter as chairman, I would like to review the actions of the Board in 2005 in the areas of succession
planning, corporate governance and global citizenship. 2005 marked a year of leadership transition at Intel.
