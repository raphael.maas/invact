A Message from Roger K. Newport
2016 was an exciting year for AK
Steel, as we made substantial progress in
executing our long-term strategy.
Our results during the year
demonstrated the success of the entire
g y
AK Steel team in executing our strategy
and implementing one of our guiding
g g
principles of: �We Can and We
pg g
Will� control items within our control.
Throughout the year, we continued to
enhance the financial strength of our
g y
company through improved profitability
g
and various capital market transactions.
We also made great strides in research
and innovation to further strengthen
g
our foundation.
Strengthening Financial Position
Our financial results reflected our
strategy to reduce our exposure to
commodity products. As a result, a very
difficult decision was announced in late
yp y
2015 to temporarily idle our Ashland
Works blast furnace and melt shop
p y
operations. This enabled us to make
important progress to enhance our sales
portfolio and operations.
p pg
The actions of our team generated
p
improved results and a stronger
AK Steel. Our work to optimize our
footprint, relentlessly focus on costs, and
p
reduce exposure to commodity spot
markets generated improved margins.
This, along with the successful completion
g pg
of several capital market transactions,
g p
allowed us to significantly strengthen our
p
balance sheet. These actions position us
well for the future.
As a result, for the full-year 2016, our
revenues were approximately $5.88
y
billion on shipments of about 6.05
pp y
million tons, resulting in an average
selling price of $969 per ton.
g
I am pleased with the progress we
made in strengthening the financial
p pg
position of our company. We successfully
g g
executed two capital market transactions
which resulted in $600 million in cash
p
proceeds that were utilized to lower
our debt levels and cash interest costs.
This also resulted in nearly doubling our
liquidity and cash position from $0.7
y g
billion at the end of 2015 to $1.35
qy p
billion at the end of 2016.
In addition, we completed the
refinancing of $380 million of our senior
secured notes, which lowered our cash
interest costs and extended our debt
maturity profile. And, we were able to
de-risk our balance sheet by transferring
y p
about $210 million of pension assets
y
and liabilities to a third party insurance
company. These actions will reduce
our exposure to volatility in the financial
p y
markets that can impact our pension
investment position.
All of these transactions resulted in
p
a stronger balance sheet and an
improved capital structure. This also
enhances our financial flexibility, which
p p
will allow us to continue to execute our
long-term strategy.
Trade Action Progress
After extensive investigations, the
Department of Commerce and the
g
International Trade Commission
announced positive final determinations
in the carbon steel trade cases for hotp
rolled, cold-rolled and corrosion-resistant
products. In addition, in March 2017,
these agencies made final positive
p
determinations in our trade case covering
stainless steel products. Anti-dumping
and countervailing duties were assessed
against foreign producers engaging in
g
illegal trade practices.
While this was good progress, we
were disappointed that our appeal of
g pg
the 2013 Grain Oriented Electrical Steel
trade case was denied in November
2016. We are evaluating the best
action forward to ensure fair trade of
these products.
The carbon steel and stainless steel
rulings have helped level the playing field
and counteract injury being caused by
dumped and subsidized imports. We will
continue to monitor and vigilantly address
circumstances that arise when imports
are dumped here in the U.S. In addition,
we will work with the administration
to identify long-term solutions to
address unfair trade and global steel
y g
overcapacity, which is driven primarily
by China.
Safety and Social Responsibility
Safety is and always will be the
foundation of all that we do. We are
y y
proud of our performance which shows
that all of our employees continue to take
p p
safety seriously each and every day.
p y
AK Steel remains an industry leader
in safety results, with a safety record in
2016 that was 4.3 times better than
the industry average. In addition to our
longstanding industry-leading results,
we were still able to improve our safety
g g yg
performance by 16% over the levels
p
achieved in 2015.
Eight of our facilities�Ashland, Butler,
Coshocton, Mansfield, Rockport and
g
Zanesville Works, AK Tube and AK
Coal�had zero OSHA recordable
cases during the fourth quarter. And
for all of December, our 8,500
g q
employees worked with zero OSHA
recordable events.
Zanesville Works and Coshocton
Works were recognized in 2016 for
outstanding safety performance by the
g
Ohio Bureau of Workers Compensation.
g yp y
Both sites earned the 100% Award for
p
Safety for operating the entire year of
2015 without any lost-time injuries
or illnesses.
Dearborn Works was recognized in
2016 by the American Metal Market�s
Awards for Steel Excellence for
y
�Best Operational Improvement�
for safety, quality, reliability and
p p
environmental progress.
From an environmental standpoint,
2016 was also an excellent year. The
company set multiple annual water 

and air records, in addition to working
on several air emission and waste
reduction projects.
The company�s improved environmental
performance surpassed our previous
py p
air compliance record by more than
75%. Mountain State Carbon coke
plant operations improved their air
and water compliance performance in
pp p
2016 by 95% with a record year in
both categories. Middletown Works
significantly out-performed their previous
g
air compliance record set in 2012.
Additionally, multiple recycling projects
are being developed in our company
that reduce disposal needs for waste
g p py
water and other materials.
In 2016, we continued to drive
employee engagement. We launched
a number of internal programs to drive
p y gg
a more open culture and work together
to lead continuous improvement. Our
employees are our most important assets,
and our actions allowed us to make the
most of their inspiring talent, energy and
commitment.
We also established our Great Ideas
program to provide our employees and
suppliers an additional platform to share
pg p py
their ideas about enhancing margins and
driving innovation.
We launched our AK CARES program
to support and recognize our employees
who donate their time and talents to
participate in hundreds of nonprofit
projects and organizations in their
community each year.
AK Steel continued to match employee
contributions to nonprofit charitable
p
organizations through the company�s
AK Steel Foundation. The AK Steel
Foundation enables the company to
make a positive difference in the lives of
p y
others, each year supporting nonprofit
p
programs, scholarships and grants in
communities where our employees live
and work.
Research and Innovation Expansion
We increased our investment in
research and innovation in terms of both
people and facilities.
We expanded our innovation team
from 2015 through 2016 by 30%.
p
We increased the number of projects
g y
initiated each year to develop new
products or processes. We more than
tripled the number of projects we
p p
initiated in 2016 compared to 2014.
We also invested $36 million in a new
Research and Innovation Center. This
facility is designed to drive expanded
collaboration among our employees and
with our customers and suppliers.
In 2016, we completed a major
capital investment at our Dearborn Works
hot-dipped galvanizing line. We will use
this new equipment to introduce two new
NEXMET� advanced high strength steels
in the first quarter of 2017�NEXMET
g g
1000 and NEXMET 1200�and to
develop additional grades of next
generation advanced high strength steels.
We launched NEXMET 440EX, the first
g g
of our innovative NEXMET high strength
steel products for use in automotive
g
lightweighting, specifically designed
p
to help our customers meet future
g g gp y g
Corporate Average Fuel Economy (CAFE)
requirements. This new, exposed surface
pg y
quality product is designed for use in
q p
applications where appearance and dent
resistance are critical�for example, on
pp pp
the exposed panels of cars and trucks.
p
Our NEXMET 1000 and NEXMET
1200 next generation advanced high
strength steels are expected to be
available for customer qualifications in
g p
early 2017.
We also announced a new thirdgeneration steel, called NanoSteel�
NXG� 1200. We were recognized
along with the NanoSteel� Company
with a global award for �Breakthrough
g p y
Solution of the Year� for this work. These
g g
new products will begin to enhance our
foundation of new products for use in
p g
future vehicle platforms.
p
We expanded our carbon and stainless
tubular steel manufacturing operations
p
into Mexico to meet the needs of our
g p
customers located there. This expansion
helps secure steelmaking in America as
we provide flat rolled steel to all of our
p g
AK Tube facilities. We are also making
p
progress in driving new innovative tubular
products and processes as part of our
pg g
strategy to expand our downstream
business.
At Butler Works, we completed a major
expansion of our CARLITE line to produce
p j
more high efficiency electrical steels for
p p
use in the electrical transformer market.
g y
Strategy Execution
In 2017, we will continue to execute
our strategy�each part of which is
designed to enhance the value of
gy p
our company:
� We will be offering new products
p y
to our customers as the result of our
g p
investment in our galvanizing line at
Dearborn Works completed in the
fourth quarter of 2016.
p
� Our Research and Innovation team
will continue to develop new products
and processes for carbon, stainless,
p p
electrical and tubular products with the
objective of announcing new product
p
offerings for our customers in 2017
j gp
and beyond.
� We will continue our focus on
enhancing margins throughout every
aspect of our business.
g g
� And, we will work to identify
p
downstream opportunities that will add
value to our steel products to further
pp
expand our margins and provide even
greater value to our customers.
Thank you
Our operating and financial
performance in 2016 reflected the
p g
results of the collective efforts of the
p
entire AK Steel team in making high
quality products to serve the needs of
g g
our customers. I would like to thank our
customers for their business. I would also
like to thank our shareholders for their
continued support.
Our goal is to create value for our
pp
investors and customers. While we made
great strides in 2016, we recognize that
we still have a lot of work to do, and
g g
we will continue to move our company
forward through the execution of our
p
long-term strategy with the objective of
g
enhancing shareholder value.
AK Steel�s Research and Innovation Center opened in November 2016, in Middletown, OH.
Roger K. Newport
April 2017