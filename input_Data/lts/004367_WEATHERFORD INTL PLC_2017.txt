In 2017, we set the stage for the future of our Company. When I accepted this job last March,
I recognized the tremendous opportunity available to Weatherford, and also the time and effort
it would take to realize its full potential. I�m pleased to say that the organization has embraced
this drive for change and come together as One Weatherford to build a stronger Company.
Looking back on the year, one of the first steps we took to bolster the foundation of our
Company was engaging in a thorough and comprehensive evaluation of the way we operate.
We took a hard look at our entire organization and concluded that while Weatherford has many
strengths, there was a lot of room for improvement in efficiency and consistency.
We addressed these areas for improvement head-on with an organizational realignment.
Our new structure clarifies responsibility, prioritizes process discipline, and solidifies a
strong connection between our operational leadership and field-level decision makers. By
breaking down silos and creating alignment to a common set of goals and objectives, we built
accountability into the fabric of our organization.

Our new organizational structure is a cornerstone in our plan to increase market share,
drive out cost, deliver improved earnings, generate positive cash flow, and ensure
Weatherford�s success in a changing market. We are building on the momentum created
by our realignment by initiating a strategic transformation program designed to yield at
least $1 billion in improved earnings within 18 to 24 months. The clarified organizational
structure will ensure the transformation is anchored on process discipline, accountability,
flawless execution, and innovation.
Since our last annual report, we have welcomed three new Board members: Roxanne
Decyk, David King, and Angela Minas. The fresh perspectives of these skilled and
experienced business leaders will be valuable assets to our organization going forward.
At the same time, we bid a fond farewell to longstanding Directors David Butters and
Robert Moses as they retire from our Board at this year�s Annual General Meeting. I want to
personally thank both David and Robert for all they have contributed to Weatherford over
many years of service.

OUR RESULTS
Our 2017 accomplishments lay the critical foundation for change in 2018 and beyond
as we continue on our path to deliver improved financial performance and long-term
shareholder value.
- Refreshed our leadership team
- Lowered our support ratio
- Reduced nonproductive time
- Positioned ourselves to achieve significant cost savings
- Redefined our culture and focused on organizational health
- Diversified our board
- Monetized non-core assets

As we have charted the course for the future of our Company, we have strengthened our
focus on our customers, who are essential partners in our success. All of the changes
we have put into place to make Weatherford a more profitable company also benefit our
customers by making Weatherford a stronger partner and a more competitive force in the
oilfield services marketplace. Our new structure, processes, and procedures enable us to
enhance the quality and consistency of our products and services, develop value-added
technologies, and work more closely with customers to find solutions that address today�s
most pressing challenges.
Since I joined the Company, our customers have consistently told me that they truly
appreciate our collaborative approach as well as the technologies and services we offer.
The customer safety and service quality awards we received in 2017 are testaments to
their support as well as to our strong commitment to flawless execution. Our overall
service quality performance improved in 2017, as demonstrated by our 23% year-over-year
reduction in nonproductive time.

We also developed and commercialized several new technologies in 2017, including
the ForeSite� production optimization platform, which leverages data from the entire
production ecosystem to maximize production and reduce costs; the AutoTong� system,
which delivers consistent connection makeup and assures greater well integrity; the
Raptor� 2.0 cased-hole evaluation system, which reveals bypassed oil and gas reserves
with accurate through-casing reservoir characterization; the HeatWaveTM Extreme LWD
service, which delivers high-resolution triple-combo logging-while-drilling data in ultrahightemperature wells; and the Endura� dual-string section mill, which creates a stable and
verifiable rock-to-rock barrier during plug and abandonment operations.