
                          To Our Stockholders,
Edwards Lifesciences' 50-year history dates back to 1958, with the
development of the first commercially available artificial heart valve
by Miles "Lowell" Edwards and Dr. Albert Starr. Our company
has continued to build on this legacy by partnering with clinicians
to pioneer new technologies, which have helped make Edwards the
leader it is today.
   As I reflect on our past, I am impressed by how much we have
accomplished, and most importantly, how we have helped millions
of patients improve their quality of life. Over the past decade,
we have accelerated our rate of innovation, and our new product
pipeline today is more robust than ever before. Several of these
new technologies promise to truly transform patient care. I believe
our company's best days are ahead of us as we continue to develop
innovative solutions for people fighting cardiovascular disease.






                                                             A Legacy Of Leading
This was a year of outstanding performance for Edwards Lifesciences in several
important areas. First, we achieved total underlying sales growth of 12 percent,
making this our best year ever for top-line growth. Our market-leading Heart
Valve Therapy franchise achieved even higher growth for the year, propelled by the
continued momentum of our Edwards SAPIEN transcatheter heart valve in Europe.
The successful launch of new surgical heart valves in the U.S. and Japan also helped
lift our growth. In addition, during the introductory year of our Edwards SAPIEN
transcatheter heart valve in Europe, we treated approximately 2,000 patients who
may have otherwise gone untreated, and exceeded our initial sales expectations.
       In 2008, we continued to make great progress in our U.S. PARTNER clinical
trial for our Edwards SAPIEN valve and began a CE Mark trial in Europe for
Edwards SAPIEN XT, our next generation transcatheter heart valve. At the same
time, use of our FloTrac system continued to grow rapidly within our Critical Care
franchise. In addition, the unveiling of our continuous glucose monitoring plans
represents a potential new growth platform for the company. Finally, we successfully
integrated the CardioVations minimally invasive surgery product line into our
Cardiac Surgery Systems franchise.






       From a financial perspective, in addition to strong sales performance, our 2008
results were enhanced by improvement in our gross profit margin. We were able to
leverage our performance to lift our non-GAAP diluted earnings per share growth
approximately 20 percent for the year. Lastly, during 2008, we demonstrated our
financial strength by generating record free cash flow of $166 million and ending
the year with a positive net cash position.


                           Extending Leadership Through New Products
Edwards' legacy of leadership in the treatment of advanced cardiovascular disease
continues today, with more than 90 percent of our sales coming from products that
are in number one positions globally.
       Expanding upon our tissue valve platform, in 2008 we introduced the
PERIMOUNT Magna mitral valve in the U.S. This is the first mitral tissue valve
to feature an asymmetric shape that mimics the native mitral anatomy. This valve is
uniquely designed to provide superior performance with ease of use benefits.
       We also introduced the Carpentier-Edwards PERIMOUNT Magna pericardial
aortic heart valve in Japan and this valve is on its way to becoming number one in
that country. The Magna valve combines more than 20 years of clinical experience
with the world's most advanced tissue engineering technologies to deliver excellent
durability and performance.






      We continued to make important progress in the PARTNER clinical trial in
the U.S. for our Edwards SAPIEN transcatheter heart valve and we have enrolled
more than 700 patients to date. With this technology, we hope to provide a longer
and improved quality of life for the substantial number of patients whose severe
aortic stenosis goes untreated today.
      During the year, the first human implants of our Edwards SAPIEN XT next-
generation transcatheter heart valve were performed and we initiated a CE Mark
clinical trial in Europe. This valve's lower delivery profile will make it available to
an even wider group of patients.


                                                      Leading By Example
In March, I was elected to represent our industry as chairman of the board of
directors of the Advanced Medical Technology Association (AdvaMed), the leading
advocacy group for the medical technology industry. I am honored to serve as
AdvaMed's chairman, while continuing to guide Edwards in developing life-saving
innovations for patients. During my two-year AdvaMed chairmanship, I intend to
continue focusing on doing what's best for patients, while also working to increase
awareness of the benefits of innovation in medical technology, to enhance industry
ethics and to strengthen relationships with key stakeholders on a global basis.








      Edwards Lifesciences was built by forging strong and enduring relationships
with clinicians. Medical innovation is dependent on these clinician partnerships,
and we believe that being transparent about these relationships will help the
public better understand the critical role they play in the advancement of medical
technology and the improvement of patient care. To that end, we have decided to
voluntarily disclose financial relationships with U.S. physicians starting in 2009.
      The Edwards Lifesciences Fund continued to fulfill its mission to assist
organizations that help patients in their fight against cardiovascular disease, and
support the communities in which we live and work. This year, the Fund awarded
approximately $2.3 million in grants to 81 non-profit organizations, which was our
strongest year of giving to date.


                                                             2009 Outlook
We expect to carry our 2008 momentum into 2009 and deliver strong results
while making substantial investments in our future. In Heart Valve Therapy,
we anticipate launching two new valves in the U.S. and doubling our Edwards
SAPIEN transcatheter heart valve procedures. In Critical Care, we have planned
for additional extensions to our FloTrac system and hope to introduce a continuous






glucose monitoring system in Europe by the end of the year. Lastly, we anticipate
completing enrollment in our U.S. PARTNER trial and gaining a U.S. IDE
approval for the Edwards SAPIEN XT valve.
      We will also remain focused on achieving our financial goals, which include
generating total sales of $1.24 to $1.30 billion, reflecting 10 to 12 percent underlying
growth, and achieving diluted earnings per share growth of 15 to 19 percent.
      We have much to look forward to as we continue to fulfill Miles "Lowell" Edwards'
vision by living our Credo: Helping Patients is Our Life's Work, and Life is Now.


                                                                     Sincerely,




                    Michael A. Mussallem, Chairman and Chief Executive Officer

