Dear fellow shareholders:
In the face of very challenging times for the global economy, I am proud our team
was able to post strong, double-digit earnings growth in fiscal 2009. In addition to
our good financial results, we received several awards in recognition of our stature
in the industry. On March 3, 2009, our stock was added to the Standard & Poors 500
Index, a distinguished list of Americas leading companies. We were named one
of the 100 Best Corporate Citizens by Corporate Responsibility Officer magazine.
We were also recognized as the Most Innovative Company at the 2009 American
Business Awards. All of these awards are testament to the hard work and dedication
of our talented employees.
The effects of the recession did impact our top-line results. Sales declined in each of our business segments.
In part, the decline was attributable to changes designed to improve our business. These include an intentional
reduction of production at our Jennie-O Turkey Store segment and product rationalizations in our Grocery
Products segment. Sales declines also resulted from decreased pricing as various commodity costs declined, as
well as from our exit from the Carapelli joint venture olive oil business. We intend to restore our sales momentum
in fiscal 2010 and have a number of initiatives in place to support that effort.

Balanced model
Our balanced business model helped us again during the recession, as sales of our
retail canned meat and value-added meat products offset decreased foodservice and
microwave product sales. Buoyed by our advertising and promotional efforts, items such
as our SPAM family of products and Hormel chili showed excellent growth during the
year. Somewhat counterintuitively, sales of more expensive value-added items such as
Hormel pepperoni and party trays also grew strongly during the year, indicating that
value means more than just price in the eye of the consumer.
In addition, our balance between center-of-the-store items and value-added protein
products provided more diversity of product types to meet different consumer needs
within retail stores. For example, our canned meat and microwave tray items meet the
need for an easy meal within the heat-and-eat category, while our Hormel refrigerated
entrees, pepperoni and party trays meet the need of a main course for a family meal,
ingredients for a wide variety of meal options, and appetizers for all occasions.
2009 op erating highlights
These recessionary times led to reduced consumer spending, even on food. Fortunately
for us, we have some of the best known and trusted value-oriented products in the store.
Our Grocery Products group is a repository of leading brands in the center of the
store, including the SPAM family of products, Hormel chili and Dinty Moore stew,
among others. Construction of our Dubuque, Iowa, production facility is on track and the
plant is scheduled to begin manufacturing in January. This facility will eventually be used
to produce both canned meat and microwave products.
We are excited about our prospects for the new MegaMex Foods joint venture which
started operations at the beginning of fiscal year 2010. This expanded entity will provide
retailers with a comprehensive portfolio of Mexican foods under leading brands, including
Herdez salsa, Chi-Chis salsa, La Victoria Mexican sauces, Doa Mara moles and
jarred cactus, Embasa peppers and Bfalo hot sauce. With both authentic and mainstream
Mexican offerings, these products resonate across multiple demographics, allowing
us to become a one-stop shop for our customers.
In our Refrigerated Foods segment, the Meat Products group generated increased
sales of many products which met a variety of needs in this value-seeking time. Included
among these are Hormel  pepperoni and party trays, Hormel  Natural Choice lunch
meats and the Di Lusso product line, all of which posted strong sales growth during
the year.

The Foodservice group had a more difficult time meeting their sales goals, as
decreased travel and recreational occasions contributed to the consumer trend of
eating out less often. Nevertheless, they did an excellent job of focusing on valueadded
solution products to meet the needs of foodservice operators. Products that
distinguish the Hormel Foodservice team from its competitors include Hormel 
Natural Choice lunch meats, Bread Ready presliced meats, Austin Blues barbeque
meats and Caf H ethnic meats.
Reduced production levels established by our team at Jennie-O Turkey Store
allowed them to minimize the impact of low commodity turkey meat prices resulting
in an improved year. Supported by advertising and promotional efforts around the
healthy nature of turkey, sales of value-added turkey products helped Jennie-O
Turkey Store stay profitable throughout the year, an achievement not duplicated by
many others in the poultry industry this past year.
The Specialty Foods segment ended the year with a small decline in segment
profits, due primarily to decreased sales of nutritional and ready-to-drink products
produced by Century Foods. However, Specialty Products saw increased sales of
its private label products during the year. Diamond Crystal Brands also had a good
year, driven primarily by increased sales of its nutritional products to institutions.
Our International group was challenged by export pork bans arising from the
novel H1N1 flu virus and currency exchange issues. Nevertheless, they finished
strong, giving them momentum for fiscal year 2010.
Balance sheet and cash flow
As a result of the national credit crisis in the fall of 2008, we reduced inventory
levels and closely scrutinized our capital budget in fiscal 2009. These decisions
were designed to increase our cash flow and bolster our cash balance. Although
we have since returned to our normal capital spending process, the cost savings
measures we adopted will serve us well in the future.
Our strong balance sheet allowed us to maintain our investments in food safety,
product quality, innovation and our brands. This strength puts us in an enviable
position to make capital investments to grow our business organically and to make
strategic acquisitions.
Food safety
Food safety issues have been a focal point in the news this past year. We have
always set very high standards to deliver consistent, safe and high quality foods.
Food safety continues to be a priority for us, which is borne out by our food safety
record, one of the best in the industry. We are not resting on our laurels, however,
but continue to seek out new technology to keep our company at the forefront of
best practices in the food safety arena.

Dividends/share repurchases
For fiscal year 2010, we increased our dividend rate by 10.5%, demonstrating confidence
in our ability to grow our business. This will represent our 44th consecutive year of
increased dividends, maintaining our elite status among the Standard & Poors 500
companies.
We repurchased $38 million of shares during the year, and currently have 1.1 million
shares remaining to be purchased from the 10 million share authorization currently
in place.
Director/senior management changes
Joining the Board this year is Susan K. Nestegard, executive vice presidentGlobal
Healthcare Sector for Ecolab Inc. Long-time members Gary J. Ray and Luella G. Goldberg
retired from the Board. Our company has benefitted greatly from their leadership and
insight, and we wish them the very best in retirement.
James T. Anderson was advanced to the position of assistant controller effective
May 18, 2009.
Outlook
We are optimistic about our long-term growth prospects. Our strong balance sheet gives
us the ability to grow organically and through acquisitions. Leveraging our category leadership
and investing in innovation and our brands will help fuel our growth.
Although we believe the recession will continue to impact consumer behavior in fiscal
2010, we expect a year of both increased sales and earnings. We anticipate our sales
results will start slowly and gain momentum as the year progresses. Any significant
improvement in the economy during the year will provide us with additional upside
potential.
Our greatest asset is our employees. I would like to acknowledge, with thanks, their
important contributions to the success of this great enterprise. Their dedication and
perseverance in the face of numerous challenges helped us maintain our leadership
position in so many critical areas. I am confident they will take us to new heights, as we
continue to grow our sales and profits for the benefit of you, our shareholders.

Jeffrey M. Ettinger
Chairman of the Board, President
and Chief Executive Officer