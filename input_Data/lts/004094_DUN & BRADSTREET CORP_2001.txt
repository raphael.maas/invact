To Our Shareholders
Fellow Shareholders,                                      important presence on the Web is one that delivers
Last year at this time, I reported that we had an aspi-   the majority of its revenue over the Web.
ration to transform D&B from what was an underper-
                                                          In 2001, we delivered EPS growth of 16.7% (ex-
forming company with underleveraged assets into a
                                                          cluding the impact of non-recurring items) which
high-performing growth company with an important
                                                          exceeded our expectations. And, while we increased
                            presence on the Web.
                                                          core revenue growth by 1% this year, we are not
                            We called our strategy for
                                                          satisfied with these results. Our revenue from the
                            achieving that aspiration
                                                          Web increased to 33% by the end of 2001, com-
                            our Blueprint for Growth.
                                                          pared to 17% in 2000.
                            I am happy to report to
                                                          Our Blueprint is our road map to realizing our
                            you now that our trans-
                                                          aspiration. Our Blueprint strategy is clear, simple, and
                            formation is well under
                                                          focused. Outcomes are measurable: all aspects of
                            way, that we have made
                                            our Blueprint plan have metrics and timelines for
                            good progress in imple-
                                                          completion. All of our team members understand this
menting our Blueprint strategy, and that we are
                                                          strategy, and their compensation plans are linked to it.
moving toward becoming a growth company with an
                                                          Every part of our Blueprint, and each of us within
important presence on the Web.
                                                          D&B, is focused on creating shareholder value.
Let me take a moment to explain what I mean by
                                                          There are five parts to our Blueprint strategy:
a "growth company with an important presence
on the Web."
                                                              Leverage the Brand
                                                          
                                                              Create Financial Flexibility
                                                          
To us, a growth company is one that generates high
                                                              Enhance Our Current Business
                                                          
single-digit revenue growth, and earnings per share
                                                              Become an Important Player in E-Business
                                                          
(EPS) growth in the mid-to-upper teens on a con-
                                                              Build a Winning Culture.
                                                          
sistent basis year after year. A company with an






Our Brand is at the center of our Blueprint and           Winning Culture, a culture focused on winning in the
powers all of our growth initiatives. D&B is a powerful   marketplace. There are challenges in changing a
brand; one that gives us credibility on the Web           culture, especially in the face of the unprecedented
as well as in the physical world, one that makes it       events of 2001. To help create that culture, we have
possible for us to partner with other strong brands,      concentrated on improving our leadership at all lev-
one that enables us to acquire and retain customers.      els of the organization because we believe that the
                                                          more superb our leadership, the more superb our
Creating Financial Flexibility enables us to invest in    execution -- and the more shareholder value we
growth and deliver shareholder value at the same time.    deliver. We are looking outward, putting our cus-
We have developed an investment decision process          tomers first. And most importantly, we are channel-
in which almost every dollar we spend is viewed as        ing our vision through one lens: improving
flexible; we have created a culture where we continu-     shareholder value. Put simply, our interests as lead-
ously re-evaluate how we are spending money.              ers are tied directly to your interests as shareholders.

We are enhancing our Current Business both                We are on a journey to transform D&B with you, our
by improving the breadth and depth of our data,           shareholders. We feel proud of our progress so far,
and by introducing new value-added products that          and we expect to continue to make good progress in
focus on our customers' needs, such as our Data           2002 toward our aspiration of becoming a growth
Integration Toolkit.                                      company with an important presence on the Web.

We are in position to become an important player in       Sincerely,
E-Business. We bring a high level of trust and confi-
dence to the Web for our customers, and we continue
to move more of our revenue generation to the Web.
                                                          Allan Z. Loren
                                                          Chairman, Chief Executive Officer and President
Enrolling everyone in our organization to imple-
ment our Blueprint strategy requires building a
