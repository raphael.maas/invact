BorgWarner has the financial strength, technological focus and skilled
workforce to actively manage through current economic conditions
and continue our growth. 

For many companies around the world, including BorgWarner,
2008 was an extraordinary year that will long be remembered
as a watershed in the history of economics and business.
Despite the daunting challenges facing the global economy
and our industry in particular, BorgWarner has the financial
strength and management experience necessary to weather
this storm and prepare for a recovery. When the crisis
subsides, the future holds outstanding growth opportunities
for BorgWarner driven by our technological focus on
improved fuel economy and emissions reduction, and the
skills of our worldwide workforce. I believe that companies
like BorgWarner that survive the current crisis will form
the foundation of a renewed, sustainable, and vital global
auto industry.
A Year of Extremes
We expected 2008 to be a challenge, but could not have
predicted that the first half of the year would be so markedly
different from the second half. We enjoyed strong demand
for our products in the first half of the year, leading to record
sales and earnings. With the escalation of gasoline prices
by mid-year, vehicle builds in North America contracted.
The economic crisis and loss of consumer confidence that
took hold in the third quarter and accelerated into the fourth
quarter, created a domino effect around the world that
brought auto production to a virtual standstill by year-end.
Throughout this time we were adjusting our production
and workforce levels to cope with daily changes. The
deep declines in North America and the overnight cuts in
European production schedules challenged our ability to
reduce our costs at the same pace. The robust start to the
year was offset by dramatically reduced demand by yearend, which impacted our financial performance. The
year was not without its achievements, however:
 Our fuel-efficient technology helped both our customers
and our businesses receive accolades throughout the year.
We were associated with a record number of awards in
2008, from recognition for technology which helped a host
of vehicles win environmental honors to industry innovation
awards for our turbochargers, all-wheel drive systems, dual
clutch transmission technology, variable cam timing and
ignition systems.
 Customers continued to adopt our solutions for improved
fuel efficiency with a number of major launches of our
dual clutch transmission technology, and the turbocharger
business award of a major portion of the Ford six-cylinder
EcoBoost engine program. 

 While expansion programs were temporarily slowed by
year-end, we are well positioned for growth in emerging
markets like Eastern Europe, China and India.
 In China, we established a joint venture during the year
with a consortium of 12 top Chinese automakers to
produce dual clutch transmission modules. This significant
collaboration establishes dual clutch technology as the
preferred transmission solution for Chinas major domestic
vehicle manufacturers.
 Our dividend was increased by 9%. We have increased
the dividend in each of the last seven years, and over that
period, the dividend has more than tripled.
Managing through Global Uncertainty
We proactively approached 2009 prepared to deal with
uncertain industry conditions, especially in North America
and Western Europe, which together represent about 80%
of our revenue. We have successfully managed through
difficult market environments before and expect to apply
that same focus and determination to the current situation.
While industry conditions are difficult, the strong underlying
fundamentals of our company remain strong and we plan
to protect our foundation for future growth.
 Our underlying strengths include:
 Powertrain technology leadership that drives our growth.
 Customer and geographic diversity that minimizes our
exposure to any single customer or market.
 An operational focus that actively manages the size and
overhead costs of our business.
 Financial structure and liquidity that are strong.
As business conditions continued to worsen in all of our
major selling regions during the fourth quarter of 2008, we
took aggressive cost control measures. We entered 2009
having already undertaken significant restructuring actions
in North America and Europe, and having implemented
three and four week shutdowns at most of our worldwide
operations. We had made the tough but necessary decisions
to reduce our workforce by about 4,400 people or 24%, and
had moved to four-day work weeks in many European
operations.
Each of our business units has focused on adjusting their
operations to the appropriate staffing levels for the low
production volumes that we are predicting for 2009. Our
teams are working aggressively to respond to ever-changing
customer and business needs. We are reducing inventory
and capital spending. At the same time, we are continuing to
spend wisely on research and development to enhance our
future growth and powertrain technology leadership position.
Against this backdrop of a global recession, we continue to
gain new business. Our net new business of $2.1 billion over
the next three years provides a good, longer-term view of
our expected growth beyond the current crisis.
Of the total net new business, about 80% is from enginerelated systems and the remainder is drivetrain-related.
From a product perspective, turbochargers and dual clutch
transmission technologies remain our major growth platforms.
Our growth is tied to issues of global importance for drivers
and manufacturers  fuel efficiency without sacrificing
performance, cleaner air and vehicle stability. We have powertrain technologies that enhance the driving experience
while conserving energy, whether it is through technology
aimed at clean diesels, advanced gasoline engines, hybrids
or electric vehicles. We believe that when combined, our
products can enable fuel economy improvements of up to
40% over todays most common powertrains. We will weather
the current economic downturn with an enhanced focus on
operational efficiency while remaining passionate in our
commitment to powertrain technology leadership.
What do we expect in 2009? As I prepare this letter in
February of 2009, we see depressed levels of auto production
around the world, with a level in North America that we have
not seen in almost 30 years. As a result, we expect a decline
in 2009 sales, but anticipate that earnings and cash flow will
be positive. Based on our current information, the foreseeable
future remains difficult with no real industry turnaround
predicted until mid-2010. We are better positioned than most
companies however, and we have the fortitude to endure this
downturn and will emerge leaner and stronger.
We Will Emerge Stronger
The current period of global economic crisis and dramatic
industry schedule reductions has strained even the best
performing companies around the world. BorgWarner
maintains the financial capability to continue to invest in
research and development, as well as to pursue strategic
acquisitions to further strengthen our competitive position in
the marketplace. We are taking actions to mitigate the impact
of the current global economic situation, while continuing to
position ourselves for the future.
While the current economic crisis is expected to slow global
car sales in the near-term, BorgWarner remains at the
forefront of global automotive trends. Clean air and fuel
economy are major issues that automakers around the world
must address for many years to come. The strategic moves
that the company has made in recent years are paying off.
We expect to continue to outpace the growth of the auto
industry and to strengthen our competitive position through
our leading technology and strong focus on cost control.
These are extraordinary times for our company and for our
industry. Despite the external economic environment that is
beyond our control, our employees around the world will
continue to concentrate on effectively executing our technology
driven growth strategy. We have always been a lean company
and are now asking more from our people than ever before
as we proactively protect the future of BorgWarner during this
difficult period.
We know there is strength in our entrepreneurial spirit and
BorgWarner Pride that will see us through this recession.
When the global economy turns around, BorgWarner will still
be the leading powertrain supplier to the global auto industry!

Timothy M. Manganello
Chairman and Chief Executive Officer