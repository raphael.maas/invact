Dear Shareholders and Friends,


In 2018, we executed on our goals and achieved
success the right way — the Vulcan Way — through a
steadfast commitment to our shareholders, customers,
employees and the communities we serve. Along the
way, we continued Reaching Higher. We delivered
pre-tax earnings growth and enhanced profitability
while also growing the business in strategic locations,
even as we faced severe weather events that disrupted
operations in some states for weeks at a time. We
advanced our world-class safety performance, improving
on our record-setting results from the previous year.
With a clear and compelling strategy, a lean and locallyled operational structure, and unparalleled positions
in attractive long-term growth markets, we are wellequipped to take advantage of market opportunities or
market challenges. And we are especially well-situated
to benefit as infrastructure demand in key Vulcan states
continues to grow, fueled by substantial increases in
state and local funding.
Even though the year provided plenty of headwinds for
the construction industry, our people were more than up
to the challenges we faced. Despite weather disruptions,
a 32 percent increase in the cost of liquid asphalt, and a
25 percent increase in the cost of diesel fuel during the
year, we delivered strong top and bottom line growth
— a testament to our operating discipline and the talent
of our people. For the year we increased total revenues,
earnings from continuing operations before taxes, and
adjusted earnings before interest, taxes, depreciation,
and amortization (EBITDA), while decreasing our
overhead expenses as a percent of total revenues. We
finished the year with adjusted earnings from continuing
operations of $4.05 per diluted share, compared to
$3.04 per diluted share in the previous year, and with
same-store flow-through of incremental aggregates
revenue to the bottom line of 64 percent.
We performed well in 2018,
and I am confident that we will
continue Reaching Higher to
deliver even stronger results in
the future as we benefit from
the operational strength of our
business and improving market
trends in the public sector.
Dramatic Growth in Public
Infrastructure Funding
We begin 2019 on the cusp of a major expansion
in public infrastructure construction demand. State
transportation funding legislation and local ballot
measures are bringing about important increases in
public spending for much needed projects and are
finally beginning to generate new highway construction
and the repair and maintenance work necessary to
address the country’s failing infrastructure — all projects
that depend upon aggregates as the fundamental
building block.
As one example of many, we are just beginning to
benefit from highway work in California made possible
by SB-1, The Road Repair and Accountability Act,
signed into law in 2017. SB-1 provides an additional
$52.4 billion over a ten-year period to fund the state’s
dilapidated transportation infrastructure, essentially
doubling prior funding. As the largest producer of both
aggregates and asphalt in California, we are uniquely
positioned to serve SB-1 projects in the Golden State.
We expect greater demand for aggregates and asphalt
from these projects in 2019 and for years beyond, as
new highway work and other major transportation
projects get underway. 

The same trends are occurring in other Vulcan-served
states. In fact, nine of our key states that generate
almost 80 percent of our revenue have passed legislation
over the last three years that raises their transportation
infrastructure funding by nearly 60 percent over 2015
levels. California, Florida, Georgia, Maryland, North
Carolina, South Carolina, Tennessee, Texas, and Virginia
have all stepped up to address their transportation
infrastructure needs and boost their economies.
Altogether, state laws and local initiatives to increase
transportation infrastructure funding have added
more than $20 billion per year of funding capacity in
just these nine Vulcan states. To put it in perspective,
that’s nearly half the annual amount provided to all 50
states under the federal transportation law, the Fixing
America’s Surface Transportation (FAST) Act. We expect
more Vulcan-served states to follow suit in 2019 and
the following years. The momentum and need is there.
In last November’s elections, 352 local transportation
funding initiatives appeared on ballots in 31 states, and
79 percent of them were approved by voters. Voters
want better infrastructure. Many of America’s political
leaders are waking up to that fact.
We are excited about accelerating public sector
growth. At the same time, demand from private
sector projects has remained stable in our markets,
providing a solid base for overall growth. In fact, private
construction activity has continued to improve in several
of our important markets, particularly housing and
nonresidential construction in the South and West.
Overall, we see significant room for growth, with
demand for aggregates still below historical averages,
and well below past peaks in demand, even as
population and economic activity continue to increase in
our key markets. We believe that we are in the middle
stages of the market upturn that began more than five
years ago. We see significant upside in revenues and
profitability, and we are working to ensure that our unit
cost of production per ton of product sold — already the
best in the industry — continues to improve, resulting in
even stronger profitability.
Hand-in-hand with our sharp focus on operational
excellence, we also continued our leadership in proactive
capital stewardship. We maintained excellent liquidity,
reinvested in our core operations, maintained our
investment grade credit rating, successfully pursued
profitable growth opportunities, and returned $282
million to shareholders through dividends and share
repurchases, an increase of $89 million over the
previous year.

The Vulcan Way
Value is created and sustained not only though strong
financial results, but through a steadfast, long-term
commitment to our customers, employees, communities,
and the environment. The construction materials we
provide create more than just strong financial results
— they build stronger communities. Our materials are
the building blocks that make possible the roads and
bridges, schools, houses, public and private buildings,
hospitals and places of worship for communities
throughout America.
Beyond infrastructure, we are
also focused on building stronger
communities by giving back and
contributing to many worthwhile
civic and cultural initiatives
that are important to us and
to our neighbors.
We are positive, long-term members of dynamic,
thriving, and growing communities. We are proud of
this social responsibility commitment — doing the
right thing, the right way, at the right time. That is
the cornerstone of our philosophy, and we call it
“the Vulcan Way.”
It is a fundamental commitment for us. It guides
everything that we do, and we are constantly striving to
achieve more. We believe in encouraging and enabling
our people to accomplish great things, both at Vulcan
and within our communities. The backbone of Vulcan
is our people, and our people hold themselves to the
highest standards.
The most important aspect of our commitment to our
people is keeping them safe. We are an industry leader
in safety, and that is very important to us. We take our
record of excellence seriously, and we are committed to
improving on it. Our 2018 safety record was even better
than our world-class 2017 performance, remaining
at less than one injury per 200,000 employee-hours
worked. Our goal is zero injuries in the workplace.
We believe that is achievable. In fact, in the past 12
months, 94 percent of our facilities have not had an
employee lose a single hour of work due to an injury. It’s
a well-earned achievement, the outcome of a relentless
focus on the safety and well-being of our people.
Developing Our People —
Aiming Higher
We are also highly focused on the development and
growth of our people. We are all working to continually
improve our performance and to help each other do it.
We are investing not just in our employees’ safety,
but also in their professional advancement and growth
as leaders.
Our leaders and teams work from carefully developed
SMART1
 goals. They share them, and they revise and
update them, with a constant eye on continuous
improvement and performing at the top of their game
no matter what is thrown at them. Our work together
is exciting, energizing, and fulfilling. That is one
reason why our Organizational Health Index scores, as
measured by McKinsey & Company and compared to
1,700 other companies, are consistently among the 

highest. Our employees perform at Vulcan with a great
deal of satisfaction in the work that they do, but they
are also determined to always get better.
Sustainability and
Environmental Stewardship
We take great pride in our sustainability and
environmental stewardship initiatives, which underpin
our commitment to long-term growth. Vulcan’s
operations ended the year over 98 percent citation-free
in both state and federal regulatory inspections. We are
also continually working to increase energy efficiency
and reduce emissions. Our operations are not a major
source of greenhouse gas emissions as measured by the
U.S. Environmental Protection Agency. Nevertheless,
we have reduced greenhouse gas emissions by more
than 49 percent since 2015. And unlike metal mines,
there is no toxicity associated with by-products from
our quarries. These by-products can be reclaimed and
re-blended into saleable materials. However, in no way
does that diminish our commitment to do better every
day in protecting the environment, and we are
always striving to drive further improvements in
environmental stewardship.
Every site Vulcan operates has unique environmental
characteristics that call for a tailored, site-by-site
stewardship strategy that includes a strong focus
on water re-use and recycling. Our commitment to
sustainable growth is also demonstrated by our recent
investment in two new, state-of-the-art ships that serve
important and growing markets on the U.S. Gulf Coast
and up the Atlantic coast as far as Charleston, South
Carolina. Compliant with Energy Efficient Design Index
standards, these ships have much lower emissions and
are far more fuel efficient than our prior fleet.
We focus intently on the environment when our
quarries are in use, but we are also focused on their
best use when production is completed, looking far
into the future. More than forty Vulcan operations are
certified by the international Wildlife Habitat Council
(WHC) in recognition of our creation and preservation
of wildlife habitats at these sites. We employ extensive
land reclamation practices at our sites as we look to
the end of their productive life, while also holding
thousands of acres of land in conservation easements.
Proper stewardship of the land, for the long term, is
of great importance to us. Our team at Vulcan’s Calica
Quarry in Mexico, for instance, has been recognized by
the Mexican government for its reforestation program
that produces more than 20,000 plants and trees from
our nursery there — plants that are native to the land
in Mexico’s Yucatán Peninsula. Over the last 15 years,
Vulcan’s Calica facility has continued to receive “Clean
Industry” certificates for outstanding environmental
performance as determined and issued by PROFEPA,
Mexico’s federal environmental protection agency in
charge of enforcing environmental laws.
In this year’s annual report, we
describe some of our investments
in employees and in environmental
sustainability. These investments
are the right thing to do, are good
for our business, and have helped
us deliver superior performance.
In closing, I’m extremely proud of our resilience in 2018.
We faced no shortage of challenges, but our people still
delivered impressive results and positioned us extremely
well for 2019 and the ensuing years. We looked out for
each other, for our Company, and for our communities
and the environment, all consistent with our sharp focus
on creating long-term value for shareholders.
We are excited about the future and committed to
capitalizing on the opportunities before us in the right
way — the Vulcan Way. Our customers, partners,
communities, and shareholders have come to expect
a world-class level of commitment, service and focus
from us, and we will honor that expectation by always
Reaching Higher.
As always, thank you for your support and
investment in Vulcan Materials.
Sincerely,
Tom Hill
Chairman, President and Chief Executive Officer