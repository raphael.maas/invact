Dear Stockholder:
Seventy- ve years and one guiding principle: putting clients  rst.  is years annual report looks at
why weve done what weve done for the last 75 years. By meeting our clients needs, delivering strong
investment performance, and providing outstanding client service, we have built a successful company
that is a great place for clients to invest, a great place to work, and a great company in which to invest.
Our 75th anniversary year was notable for the dichotomy between how investors felt about their
investments and how those investments actually performed.  e year began with a minute-by-minute focus
on the eurozone  nancial crisis and ended with a play-by-play account of the  scal cli negotiations in
Washington. We had a lengthy presidential campaign and witnessed the frothy Facebook IPO. Despite all
this, and the ebb and  ow of the news cycle, virtually all asset classes and markets provided positive returns
for the year. In essence, 2012 was a better year than it felt.
STRONG MARKET RETURNS DESPITE ECONOMIC CONCERNS AND PARTISAN WRANGLING
Equity markets around the world performed strongly, and most  xed income markets generated healthy
returns as well. In the U.S., the large-company S&P 500 Stock Index advanced by 16%.  ere was not one
day in 2012 when the index was down on a year-to-date basisthe  rst time this has happened since 1979.
Global equity markets also performed well, paced by returns in emerging markets and several European
countries.  e MSCI All Country World Index ex USA returned just over 17%.  e big surprise was
Europe, which, despite the headlines, was a notably strong source of equity returns in 2012, up nearly 20%.
And you know that investors are willing to take risks when the best-performing global markets include
Venezuela, Pakistan, and Greece.
Fixed income securities also provided positive returns, with riskier assets performing particularly well.
Emerging market bonds and high yield bonds generated the best  xed income returns. Treasury securities
lagged corporate bonds as the Treasury market was one of the least rewarding  xed income markets
during the year. As was the case in 2011, money market returns hovered barely above zero, re ecting the
continuing accommodative policies of the Federal Reserve.
Last year we commented on the need for practical, courageous, and bipartisan political leadership to
address our continuing  scal challenges. A er watching the year-end  scal cli deliberations, it is hard
to feel con dent about the un nished business that remains. With $16 trillion in federal debt growing
at $1 trillion per year, we are on an unsustainable  scal path. If interest rates rise, the country will be
in a particularly di cult position. While the New Years Day tax bill was a  rst step, much remains to
be done. Improved corporate and investor con dence depends on what happens in Washington.
To put this debate into context, consider the following  ctitious scenario: With annual revenues of $3 billion,
a company spends $4 billion per year, resulting in annual losses of $1 billion. It borrows $1 billion
each year to cover these operating losses. Its lenders are beginning to worry. Management fails to agree on
an operating plan until the  rst day of the year, leaving its employees in a state of confusion. Even the
last-minute budget is not a sound long-term operating plan. Would you want to invest in such a company?
Investors in Treasury securities may increasingly ask the same question.

FINANCIAL PERFORMANCE
Because 2012 was a rewarding year for our clients, it was also a successful year in terms of our  nancial
performance. Assets under management rose from $489.5 billion at the end of 2011 to $576.8 billion as
of December 31, 2012. Average assets under management increased from $497.1 billion to $549.4 billion.
Clients entrusted us with approximately $17 billion of net new investments. Rising asset values and positive
cash  ows caused revenue to increase from $2.75 billion in 2011 to $3.02 billion in 2012. Net income rose
from $773 million to $884 million. Earnings per share on a fully diluted basis rose 15% from $2.92 last
year to $3.36 in 2012. Our return on equity was about 24%, which is particularly notable given the lack
of leverage on our balance sheet. Stockholders equity totaled $3.8 billion. We have no debt and healthy
liquidity, with $2.0 billion in cash and investments.
Re ecting this solid performance, last February the Board of Directors approved an increase in our annual
dividend from $1.24 to $1.36 per share, a nearly 10% increase and the 26th consecutive year in which we
have raised the dividend since our 1986 IPO. At the end of the year, the Board approved and we paid a
$1.00 per share special dividend.  is extraordinary dividend was paid in light of our exceptionally liquid
 nancial position. In addition to these dividends, we spent approximately $135 million to repurchase
2.3 million shares of stock. In aggregate, we returned nearly $740 million to our stockholders through
dividends and stock repurchases.
BUILDING OUR CAPABILITIES, MANAGING RISK, AND HELPING OUR COMMUNITIES
Strong investment results and satis ed clients enable us to report such solid  nancial results. Our returns
for clients were generally strong across the board with 78%, 84%, and 78% of our mutual funds
outperforming their Lipper benchmarks for the 3-, 5-, and 10-year periods ended December 31, 2012,
respectively. Our institutional performance was also competitive in most areas. Representative of the
great work our investment teams have done, Morningstar named David Giroux, who manages our Capital
Appreciation Fund and related institutional accounts, its 2012 Allocation Manager of the Year for the U.S.

One of our notable success stories is the growth of our asset allocation strategies including our retirementdate
funds and trusts.  is has been a fertile opportunity for us as individuals, pension funds, and other
institutional investors have sought solutions-based investment options to invest for retirement and manage
retirement assets. We are fortunate to have a variety of high-performing portfolios that have attracted
signi cant client assets. Our asset allocation team has done an outstanding job.
We continued to invest in our investment and client service capabilities in 2012. We spent approximately
$77 million on a variety of technology and capital projects. People are the most important resource in
our business, and we continued to seek out the best talent around the world.  e new year will bring
more of the same.
We continued to devote considerable resources toward building our global investment capabilities.
Our U.S. investment o erings are outstanding, and we are focused on further broadening our capabilities
outside the U.S. We introduced a new global value equity strategy, launched an Australian equity trust,
and opened the Emerging Markets Corporate Bond Fund to complement our existing suite of emerging
markets  xed income o erings. We believe emerging markets investing is one of our key strengths and
that we are well positioned to take advantage of the long-term opportunities in developing markets.
Risk management remained a top priority in 2012. As companies in many industries have seen, its essential
to identify potential problems and implement processes to prevent them. As has been an integral part of
our history, we continued to be an engaged and supportive member of the communities in which we live.
One of the key elements of our 75th anniversary celebration was an energetic emphasis on charitable giving
and community service. We are pleased to note that our associates remain enthusiastic advocates in our
communities, setting a record for volunteer hours.
TRANSITIONS
 ere were several key transitions in 2012, and two deserve special mention. Having stepped down from
the New Horizons Fund in 2010, Jack Laporte retired from the company in December. One of our most
successful investors, Jack joined T. Rowe Price in 1976, initially as an equity research analyst. He rose to
oversee our equity research department; served as a director of T. Rowe Price Group for many years; and
is best known for his outstanding leadership of the New Horizons Fund, which he managed so successfully
for so long. Jack has always been the consummate professional and a source of tremendous investment
wisdom and perspective.
Elayne Flomenbaum retired a er a 50-year career with the company. Elayne worked in a variety of
administrative roles over literally half of a century. Imagine beginning your career with T. Rowe Price
during the Kennedy administration. We celebrated Elaynes remarkable career at our annual meeting
last spring. We congratulate Jack and Elayne on their outstanding careers. We will miss them both and
wish each of them the best.
We will also miss a valued member of our Board of Directors, Al Broaddus, who will retire at our annual
meeting in April. Al has served on our Board since 2004 and, most recently, has been our lead independent
director. He has provided tremendous perspective on economic, monetary, and policy issues. Al has been
a constant source of good judgment and common sense. Al, we join all of our stockholders in thanking you
for your outstanding contribution.

In an attempt to  ll Als very large shoes, we elected one new Board member, Mary Bush, in October and
elected another, Freeman Hrabowski, in early January 2013. Both are outstanding leaders with a wide range
of relevant experience and perspective. You can read about our new directors backgrounds in this years
proxy statement. We look forward to many years of their advice, counsel, and constructive criticism.
LOOKING AHEAD
 e year ahead will present many investment challenges.  e global economy should grow at a modest pace
as we continue to recover from years of too much individual and government debt as well as the e ects
of the 20082009  nancial crisis. Partisan wrangling is likely to continue in Washington. Job creation
remains the #1 economic priority as the unemployment rate continues to show only gradual improvement.
Europe appears to be through the worst of its financial crisis and recession. Japan is implementing a
range of pro-growth policy actions. Emerging markets such as China and India should continue to march
ahead at healthy, albeit volatile, rates of growth. As always, many geopolitical hotspots remain, and new
ones undoubtedly will emerge.
As we enter 2013, your company is in a very strong competitive position. Our investment organization is
talented and deep and has performed well for our clients. We have consistently demonstrated that our
investment process and results are repeatable. Our sales and client service capabilities are both focused
and global in scope. We continue to maintain an exceptionally strong  nancial position. While we do
not have a grand strategic plan for our next 75 years, we will work diligently one year at a time to put
our clients  rst. Each of our associates understands that the only way to ensure our continued success
as a company is through the continued success of our clients.

Thank you for the confidence you have
shown by investing in T. Rowe Price.

Brian C. Rogers
Chairman and CIO
James A.C. Kennedy
CEO and President