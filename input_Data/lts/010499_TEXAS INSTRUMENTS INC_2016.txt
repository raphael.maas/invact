�It was a good year and we made solid progress in making TI a stronger
company. In 2016, our revenue grew 3%. Gross margin and operating
margin expanded to 61.6% and 35.9% of revenue, respectively. Most
importantly, free cash flow increased to $4.1 billion, or 30.5% of revenue,
reflecting the quality of our product portfolio and the efficiency of our
manufacturing strategy.�
Rich Templeton, chairman, president and CEO