                

To our stockholders:

2008 was an eventful year for Adobe, for the U.S., for the entire world. Despite a global financial crisis and a broad economic
downturn, Adobe delivered another year of solid growth. We continued to execute against our strategy. We launched innovative
new products. And we remained focused on our mission to revolutionize how the world engages with ideas and information.

Highlights of 2008 include the launch of Adobe Creative Suite 4, the largest software release in Adobe's history. We also
unveiled Adobe Acrobat 9 and an update to Adobe LiveCycle ES software, a combination that increased demand for our enterprise
solutions. We introduced exciting advances in the Adobe Flash Platform, the leading technology for video, games, and interactivity
on the web. In addition to these product-related highlights, we expanded and deepened relationships with customers and partners
worldwide, reached out to our communities, and achieved another year of record financial results.

Just as the economic and political landscape is changing, the consumer and technology landscape is also evolving. The world
is moving from single-screen to multiscreen media consumption as people are demanding a consistent experience across
multiple devices--from their TV to their computer monitor to their mobile device. Success for our customers hinges on their
ability to deliver multiscreen experiences that build a meaningful and lasting connection with the person on the other side of
the screen. And that is why Adobe remains committed to our vision of enabling the most compelling applications, content, and
video--on screens of every size, across operating systems, accessible from anywhere at any time.


2008: Steady growth continues
Despite the uncertain economic environment, Adobe delivered double-digit revenue growth for the sixth consecutive year.
Fiscal year 2008 revenue was $3.58 billion, a 13% increase from fiscal year 2007 revenue of $3.16 billion. In addition, Adobe's
annual net income grew to $871.8 million in fiscal 2008, compared with $723.8 million in fiscal 2007. Diluted earnings per share
for fiscal 2008 were $1.59, compared with $1.21 in fiscal 2007.

We achieved solid results across all of our businesses in fiscal 2008 and reported growth in every business segment. Our largest
business segment, Creative Solutions, achieved revenue of $2.07 billion in fiscal 2008, representing 9% year-over-year growth.
Revenue in our Business Productivity Solutions business grew 16% year over year, with revenue of $1.06 billion. Within this
business, revenue in our Knowledge Worker segment grew to $811 million (an increase of 11%) and revenue in our Enterprise
segment grew to $253 million (an increase of 32%). Our Mobile and Device Solutions business segment achieved revenue of
$113 million, a 115% increase from the prior year, while revenue in our Other business segment was $330 million, an increase
of 15% from the prior year.

With an ongoing focus on operational efficiency, Adobe continued to have a robust balance sheet, ending the fiscal year with a
cash, cash equivalents, and short-term investments position of $2.02 billion. Results from our operations once again generated
strong cash flow, which we use to invest in Adobe's future.

In fiscal 2008, we also focused on returning excess cash to our stockholders through our stock repurchase programs.
We repurchased a total of 58.3 million shares during the year, returning approximately $2.14 billion to our stockholders--
underscoring Adobe's commitment to delivering long-term value to stockholders.


Driving further ahead: Innovation remains a focus
Adobe's award-winning technologies and software continue to redefine business, entertainment, and personal communications
by setting new standards for producing and delivering engaging content. Renowned for innovation, Adobe continued to push
the boundaries in 2008.

Adobe Creative Suite 4 was a milestone release, delivering radical workflow breakthroughs that bring down the walls
between designers and developers. Packed with hundreds of feature innovations, the Creative Suite 4 product line advances
the creative process across print, web, mobile, interactive, film, and video production. Adobe's biggest software release to
date, the Creative Suite 4 family includes 6 editions, 13 point products, 14 integrated technologies, and 7 services. With new
levels of integration and expressiveness for our Adobe Flash technology across the entire product line, the Creative Suite family
continues to be the industry-leading design and development software for virtually every creative workflow.

We also introduced groundbreaking advances in our Adobe Acrobat family of products that help knowledge workers share
their ideas and collaborate more effectively. Among the many new features in the latest release, Acrobat 9 delivers native
support for Flash technology, the ability to unify a wide range of content in rich PDF Portfolios, and access to real-time
collaborative capabilities for sharing a PDF document with colleagues. It also works with Acrobat.com, a new set of hosted
services, to provide unprecedented levels of collaboration online.

In addition, we released a new version of Adobe Acrobat ConnectTM Pro software, our comprehensive web conferencing and
eLearning solution. The new version includes functionality that enriches online communication and collaboration, helping
organizations and schools provide high-impact rapid training, interactive virtual classes, and dynamic online meetings.

Our update to Adobe LiveCycle ES enables enterprises, government agencies, and other organizations to more effectively
engage customers, constituents, partners, and employees in key business processes--inside and outside the organization.
Among the many enhancements, LiveCycle ES Update 1 adds new components for rapid development of content-rich
applications, automated conversion of two- and three-dimensional CAD design data to PDF, and new Adobe Solution
Accelerators to help customers expedite deployment of enterprise applications.

In the burgeoning market for Software as a Service (SaaS), Adobe is on the cutting edge with offerings such as Acrobat.com,
Photoshop.com, and Scene7 We are even making it possible to develop applications online, using our SaaS version
                             .
of LiveCycle, LiveCycle ES Developer Express.

In 2008, we announced further advancements to the Adobe Flash Platform, a complete system of integrated tools, frameworks,
clients, and servers to develop video, content, and web applications that run consistently across operating systems and devices.
The Flash Platform development tools, tightly integrated with Creative Suite 4, offer the foundation for agile design and
development workflows, making it faster and easier for creators to create, developers to deliver, and companies to profit.

To improve deployment of applications created with Flash, we're continuing to drive innovation in our nearly ubiquitous client
software. Released in 2008, the Adobe Flash Player 10 runtime introduced new capabilities in 3D graphics, publishing-quality
text, audio, custom filter effects, and other advances. Adoption has been swift--Flash Player 10 was installed on more than
55% of computers worldwide in just the first two months of its release, far outpacing the installation rate of previous versions
of the software.

Flash technology has also solidified its position as the number one format for video on the web. Worldwide, Flash Player is on
over 98% of connected computers and delivers more than 80% of web video [Source: December 2008 data from independent
research firm comScore].

In the mobile market, handset manufacturers have shipped more than 800 million devices equipped with Adobe Flash Lite
technology, and we will reach over 1 billion devices with Flash in the first half of 2009. With smartphones that can offer
computing power comparable to personal computers, and new Internet-enabled devices hitting the market every day, there
is great opportunity in providing a consistent runtime environment across desktops, mobile phones, televisions, and other
consumer electronics devices. We believe that Flash Player and its companion runtime, Adobe AIR are the logical choice for
                                                                                                      ,
this consistent runtime environment. With the Open Screen Project, we've partnered with leaders in the mobile, desktop,
and device ecosystems for the purpose of enabling consumers to engage with rich Internet experiences seamlessly across
any device, anywhere.

These are just a few of the innovative products, technologies, and partnerships Adobe announced in 2008 that will help
our customers create and deliver compelling content and applications more effectively than ever before. As cultural and
technological trends demonstrate, people want to interact with peers, brands, and companies--and the content they
deliver--in more meaningful and dynamic ways. Adobe is well positioned to meet these expectations with solutions that
spark creative endeavors, improve organizational productivity, enable efficient collaboration, and engage the hearts and
minds of customers around the world.


Corporate social responsibility: Adobe makes an impact in the community
Since Adobe was founded, a commitment to community involvement and philanthropy has been among the company's
core values. A highlight for Adobe in 2008 was the formal introduction of the Adobe Foundation, created to leverage Adobe's
human, technological, and financial resources to drive social change and improve the communities in which we live and work.
Our extensive corporate giving programs benefit a wide variety of causes, from supporting education to reducing hunger and
poverty. We are particularly proud of our global signature philanthropy program, Adobe Youth Voices, which provides youth in
underserved communities with the critical skills they need to become active, engaged members of their communities and the
world at large.

Driven by a strong belief that corporations have a responsibility to their local communities and the global environment,
Adobe has consistently taken a proactive approach to resource conservation, waste reduction, environmental protection,
and sustainability. At our San Jose headquarters, for example, we've implemented more than 60 projects to drastically reduce
the use of electricity, natural gas, and water. As a result, Adobe is the first organization to earn five Leadership in Energy and
Environmental Design (LEED) certifications by the U.S. Green Building Council, and the only company in the world to receive
four Platinum certifications. Also, Adobe recently launched an environmentally sensitive redesign of its software packaging that
combines recyclable cardboard derived from sustainably managed forests, a die-cut production method and folding inserts to
minimize glue, and reduced size and fewer inserts to limit materials use.

We are very proud of our employees, who are demonstrating our values through a variety of individual and group volunteer
efforts that are making a positive impact in cities around the world. We thank the entire Adobe team for their commitment to
making our company the best it can be, and doing the same for the global communities and constituencies we serve. Moreover,
we're proud that employee satisfaction continues to be high; for 2008, we had the honor of being recognized as the 11th best
company to work for in the U.S., according to FORTUNE magazine's 12th annual "100 Best Companies to Work For" survey.

Looking ahead to 2009, we expect challenges. And with challenge comes opportunity. As we've done in the past, Adobe will
continue to focus on executing against our strategy and strive to fulfill our mission of revolutionizing how the world engages
with ideas and information.

Sincerely,




Shantanu Narayen                             John E. Warnock                              Charles M. Geschke
President and Chief Executive Officer        Chairman of the Board                        Chairman of the Board
