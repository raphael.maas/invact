To Our Shareholders

We are pleased to report that Graham Holdings had a better year in 2016 than in 2015.
There was no single action that drove most of our progress. A combination of continued
focus on cost reductions and operating margin improvements, moderate investments
in organic growth initiatives, acquisitions in line with our investment philosophy and
divestment of non-core businesses and assets all made meaningful contributions to our
results this year.

More specifically, Kaplan and Graham Media Group,
our two major operating units, each reported improved
results, and our Other Businesses showed positive
trends on both the top and bottom lines.
Operating income for the Company rose to $304 million in 2016, up from an operating loss of $81 million
in 2015. To be fair to 2015, however, I should note
that a non-cash impairment charge at our Higher
Education business of just under $260 million greatly
diminished last year�s reported results. Adjusting for
that charge diminishes the scale of 2016�s improvement; but, nonetheless, adjusted operating income
(non-GAAP) increased by $127 million.(1)
Revenue for 2016 continued its multi-year downward
progression, declining by just under 4%, from $2.586
billion to $2.482 billion. Revenue declines at Kaplan,
most significantly at Kaplan University, were somewhat
offset by improvements at other businesses.
Total adjusted operating expenses (non-GAAP) declined
in 2016 by 9%(1) led by the reset cost structures at the
Kaplan and Graham Holdings corporate offices. Much
of the major restructuring has been completed, and we
continue to look for ways to spend our dollars more
efficiently. I don�t think costs will be reduced again as
substantially as they were in late 2015 and early 2016,
but we are committed to remaining as lean at the corporate level as we can and focusing our expenses on
those that are necessary for the Company. Our margin
of safety as a company will increase with an improved
operating margin. Below, I once again include a
graphic that tracks our adjusted operating income
margin (non-GAAP) of 10.7%(1) in 2016 and improvements or declines as compared to recent years.

While in 2016 we were able to increase operating
income in the face of revenue declines, we know
this is most unusual and ruefully acknowledge we
have not found some new business formula buried
in the depths of a �Graham & Dodd� book. In order
to achieve the long-term operating income growth
we believe possible, we need to return to top-line
growth; more on that later in this letter.
So what decisions did we make in 2016?
In 2016, we continued the portfolio shift that was
enabled, and in many ways necessitated, by the
Cable ONE spin-off in 2015. Here is a brief list of
what we did:
u We agreed to acquire two broadcast television
stations within Graham Media Group. The deal
was completed in January 2017.
u We closed on our acquisition of Mander Portman
Woodward (MPW), a set of sixth-form schools in
the U.K. We have been very pleased with the first
year of operations at MPW. In conjunction with
the acquisition, we took out �75 million in debt at
what we believe are favorable terms.
u Dekko acquired Electri-Cable Assemblies as a
tuck-in to its business, which further strengthens
its power and data business.
u We merged our two healthcare businesses to
achieve greater scale and operating efficiency.
u We sold Colloquy, a unit of Kaplan that offered
online learning solutions to colleges and universities, as it did not meet our financial objectives.
u We sold our long-held waterfront real estate
located in Alexandria, VA.
u We opportunistically repurchased shares of our
stock throughout the year. Outstanding shares in
the Company went down by about 4% at a cost of
$109 million.
Graham Media Group delivered again in 2016.
Boosted by both the Olympics and political advertising, GMG delivered its largest ever operating
income year. The stations, in most cases, managed
to add to their leads in terms of local rankings. Our
investment in increased news programming over the
past several years has created unique content and
brand attachment that allowed many of our stations
to navigate the trend away from linear television better than most.
Perhaps most impressively, WKMG, our CBS affiliate
in Orlando led by Jeff Hoffman, wins the award for
most improved. Jeff grew our rankings in nearly every
time slot as compared to prior years, all while providing tremendous coverage of the Pulse nightclub
shooting and Hurricane Matthew. Jeff�s performance
fits right in with the rest of our station managers at
Graham Media Group. Whether it be an all-time high
in broadcast cash flow at WJXT, or becoming the
leader in key time slots at KPRC, or having a market
leading share by a tremendous margin like that at
KSAT, or deftly navigating a challenging market like
WDIV, each one of our stations performed optimally
and made us proud.
In May, we announced we were buying WCWJ in
Jacksonville and WSLS in Roanoke from Nexstar
and Media General in conjunction with their merger. 

If you told me on January 1, 2016, that we would sign
an agreement to acquire two broadcast TV stations
five months later, I would not have believed you. The
circumstances that led to our ownership of WCWJ
and WSLS are unlikely to occur again, and, indeed, I
still pinch myself they happened at all. My view that
the long-term future of broadcast TV is a cloudy crystal ball remains unchanged. However, when you can
own two stations in the market and strengthen your
affiliation with NBC, while doing so at a decent price
for shareholders, you make the deal. Our overfunded
retirement plan helped us, as we assumed some of
the pension liabilities of the seller.
Kaplan had a year of mixed results, but the overall
picture was an improvement from the previous year.
Operating income increased from 2015 largely due
to reduced restructuring charges and lower overhead. Operating results at several Kaplan units were
substantially hurt by the strength of the U.S. dollar;
that, combined with continued headwinds at Kaplan
University, led to a mixed year of operating results
overall. The strength of the U.S. dollar and other
geopolitical events led our English-language business to suffer its worst year under Graham Holdings
ownership. A reduced cost structure implemented
in late 2016 should allow us to reap the benefits of
improved results if the operating environment for
that industry stabilizes or improves, as we believe
it will.
Of Kaplan�s larger businesses, one of the biggest
economic engines for Kaplan is now the Kaplan
Professional Education business, or KPE, led by
Andy Temte. KPE provides best-in-class training and
preparation for corporate clients in areas such as the
Series 7 and CFA exams. As a shareholder, you should
get to know this business. As long as corporations
need to provide ongoing training, certification,
continuing education and professional development
for their employees, Kaplan Professional will be a
key partner. It is important to understand that this
is a cyclical business, and the results may be uneven
from year to year. In economic cycles where hiring
is dampened, less training is usually needed. While
others may fret when the inevitable down cycles
occur, we view them as opportunities to go shopping
to build out our product offerings, while not paying
peak market prices.
My belief is that under Andy Rosen, Kaplan�s results
will improve more years than not. His efforts from
years ago provided the Company with new platforms
on which to build and proved to be a godsend when
Kaplan University encountered turbulence. Had
Andy not invested in our Professional Education and
Kaplan International businesses, the Kaplan story
would be much different today.
How does Kaplan grow revenue going forward? Our
ability to recruit high quality students from dozens of
countries is a key advantage, the envy of our competitors and the strongest moat in Kaplan�s business.
Kaplan will focus on strengthening this moat and
finding new business opportunities that can leverage
it further. MPW was one such example in 2016 � our
ability to recruit international students into MPW
should make it a much better business � and our
hope is that it will not be the last. How we tap into this
ability over the coming years could likely determine
the future performance of Kaplan and be the best
place for Kaplan to find attractive investment returns.
By nature, I hesitate to offer predictions, but when
they are near certainties, I think it is in the best
interest of shareholders that I do: It will be close to 

impossible for 2017 to be a repeat of 2016, and we
will almost certainly report a decline in operating
income for 2017. With no Olympics revenue, minimal
political advertising and a very favorable network
compensation agreement with NBC ending in 2016,
Graham Media Group will report lower results. Emily
Barr and her team will navigate these waters well;
but, when GMG�s expected declines are combined
with the continued headwinds at Kaplan University,
it will be extraordinarily difficult for the other businesses to fill the gap. However, we do expect our
non-GMG and KU businesses to grow in 2017 and
become more meaningful contributors to the operating income pie than in years past.
Where does value come from moving
forward?
I believe the job of the management team at Graham
Holdings falls into three buckets: 1) how well we
run our existing businesses; 2) our skill in investing
the cash they generate; and 3) our ability to create
organic growth. I thought it would be useful to take a
tour of our thoughts on each of these subjects in the
second portion of this letter.
How do we run our existing businesses?
Our effectiveness at running our existing businesses
is largely tied to the quality of the managers in
charge. Our decentralized operating structure puts
a premium on having first-class leaders at the helm
of our businesses. We�ve been lucky to have Emily
Barr and Andy Rosen involved in the Company for
years. They embody the culture and spirit of Graham
Holdings and understand that there is greater gratification in two dollars tomorrow than a dollar today.
In the same vein, we have cultivated a number of
talented up and coming managers to rise to the level
of executive leadership. One such leader is Laura
O�Shaughnessy. While I may be somewhat biased
(I am writing about my wife), Laura has become a
star leader at SocialCode and has a real opportunity
to build a strong long-term business for GHC. She
has consistently focused on high-quality offerings for
SocialCode�s clients, while keeping the costs of the
business in check. We are lucky to have the set of
leaders that we do.
We want our larger businesses to maintain an operating margin of at least 10%. The reason for this is
simple: It is our belief that anything less than 10%
does not provide the margin of safety to be able to
reinvest in the business when it is logical to do so,
or to withstand the inevitable bumps in the road,
whether they be macroeconomic, regulatory or
otherwise. Graham Holdings as a whole has been
working hard to achieve this goal, while also working with our operating companies to achieve and
exceed the 10% target.
How do we reinvest cash generated?
There are four things we consider doing with our
capital: invest in existing businesses, acquire new
businesses, repurchase shares and pay dividends.
In 2016, we did all four of these things, although I
suspect this may not be true every year. 

Our preference is to allocate capital into our existing
businesses. We know the management, understand
the businesses and think our best returns will be
found here because of those two things. When we
look outside of our existing businesses, we have a
very high set of criteria to compensate for the inherent unknown associated with businesses we don�t
already own:
u Well-run, profitable businesses in fields we can
understand
u Strong management with a dedication to continuing to run the business
u Businesses we believe have at least 10 years of
stable or growing earnings ahead of them
u Reinvestment opportunities that are readily
apparent within the business
If these look familiar to you that is because they are
the same criteria I shared with you in the 2015 letter
and are the criteria I have used since I joined the
business in 2014. We think they have served us well
and will continue to do so.
How do we think about organic growth?
One fundamental belief we have is that cash is cash.
Whether a dollar is used in an acquisition, as capital
investment, or to develop a new business idea, our
bank account is debited that dollar. Our treasury
balance seems not to care if an investment is classified as M&A, CapEx or a reduction in operating
income; and, neither do we. We believe our cash
outlays should be measured solely on the basis of
the potential for attractive returns, and we should
pay little attention to accounting classification when
making investment decisions. The fallacy that all cash
is not created equal often leads to a preference for
acquisition because the fear of being misunderstood
on organic investments is so great. This simply is not
the case at Graham Holdings.
We think at times there are attractive business
opportunities � both within existing businesses and,
occasionally, when evaluating an entirely new business. Several examples of these exist within our
companies. At Kaplan, our Pathways business started
organically and needed the ability to stomach several
years of development costs. Through nearly a decade
of results, the return on invested capital of this business looks very good with much runway ahead of it.
SocialCode was a new business started entirely from
scratch in 2010 and now has the potential to become
a meaningful contributor to overall company value
and has reduced its capital needs as it has begun to
achieve scale. More recently, we�ve started two new
businesses for which we are optimistic: Panoply, an
on-demand audio company that is quickly becoming
one of the leaders in the nascent podcasting industry;
and, CyberVista, a cybersecurity training and workforce development company. Not all of these bets
will work out, and it takes strong organizational discipline to ensure that new initiatives do not become
new entitlement programs. Our commitment to you
is that we will maintain constant vigilance in assessing
the progress of new business initiatives. Even so, we
will undoubtedly determine some of these efforts are
ill fated. When that happens, we will stop the initiative,
study to learn where we went wrong and look at the
increased operating income due to the discontinued
operational investment as the silver lining.

If most years could be like 2016, I would take them
in a heartbeat; but, I wouldn�t take everything about
last year. Gerry Rosberg, our longtime Senior Vice
President of Strategy and Planning, retired from the
Company in July. Gerry joined The Washington Post
Company in 1996 and was responsible for an untold
number of good decisions in the Company�s history,
including our Classified Ventures investment and
many of our Kaplan acquisitions that proved to be
critical in growing the Company. The best part about
Gerry is not his deal-making abilities, even though
that was a core part of his job description; rather,
Gerry made everyone around him better. He led by
example, took self-sacrifice to an absurd level and
looked to pass credit to others, even when he was
most deserving.
Hal Jones, Senior Vice President and Chief Financial
Officer, also announced that he will retire in early 2017
after 28 years of service. For those who know Hal, you
know he is defined by conservative accounting positions, strong ethical commitment and little hesitation
in speaking his mind. He took over as CFO just prior
to the financial crisis, staring down the barrel of $400
million in debt due in February 2009. (Keen observers
may note the market ultimately bottomed in March
2009.) Hal navigated those waters with the same calm
steady hand you�ve seen through the past nine years.
Simply put, Gerry and Hal are two of the best
examples of the culture and spirit that drove The
Washington Post Company and what now drives
Graham Holdings Company. They helped shape the
culture and environment of the modern-day GHC,
and we were lucky to have them for so long.
Hal will be succeeded by Wallace Cooney. Wally has
served as the Chief Accounting Officer of the Company
for the past nine years and previously served as the
Controller. The depth and breadth of his experience
will serve our Company well, and I am thrilled to have
him at my side in his new role. Gerry will be succeeded
by Jake Maas, a seasoned executive who joined GHC
in 2015. Jake hit the ground running since he arrived
and has been involved in every transaction as part of
the senior leadership team.
We continue to believe we have a unique platform
that combines a long-term viewpoint and patient
deployment of capital with a focus on growing intrinsic
value per share for you, our partner. We are fortunate
to have great leaders at our businesses to help drive
our results day in and day out. Management takes
its role as stewards of your capital with the utmost
seriousness, and it permeates our thinking daily. We
think our formula will provide elevated returns to you
over time. We look forward to continuing to partner
with you and thank you for the confidence you have
placed in us.
Timothy J. O�Shaughnessy
President and Chief Executive Officer
February 24, 2017


