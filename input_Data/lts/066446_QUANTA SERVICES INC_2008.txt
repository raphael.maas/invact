I am pleased to report that Quanta Services achieved another year of solid
performance in 2008. Despite a very challenging economic environment,
revenues reached record levels, and we continued to grow our backlog of
work. Demand for infrastructure services remained high, especially in the
areas of power transmission and renewable energy. During the year, our
financial strength, diversity and emphasis on renewables were the primary
elements that allowed us to capitalize on increasing demand and build on
our market-leading position. These are also among the strengths that leave
us well positioned for further success in 2009 and beyond.
A Position of Strength
Revenues in 2008 were a record $3.78 billion, compared to $2.66 billion in 2007,
resulting in an increase of approximately 42.3 percent. Operating income rose to
$289.2 million in 2008 from $169.5 million in 2007 for an increase of 70.6 percent.
At year-end, our twelve-month backlog, which is defined as the work expected to be
completed under signed contracts over the next 12 months, was $2.58 billion. Our
total backlog of work expected to be performed under signed contracts in the future
was $5.19 billion. We believe our performance in 2008 along with our twelve-month
and total backlog levels indicate another strong year for Quanta in 2009, although it
is currently unclear as to what the ultimate impact of the nations economic crisis will
be on our business.
Our financial flexibility also improved in 2008. At year-end, we had approximately
$438 million in cash and $315 million of available borrowing capacity under our
$475 million senior secured revolving credit facility. Additionally, the holders of
almost all of our $270 million aggregate principal amount of 4.5 percent convertible
subordinated notes exchanged their notes for equity in Quanta. This leaves Quanta
with no significant maturities of debt until September 2012. With our strong cash
position and credit availability, we have the financial strength to fund almost any
project, as well as the ability to capitalize on opportunities in the industries we serve.
A Commitment to Diverse Customer Base, Revenues and Services
Diversity is the cornerstone of Quanta, beginning with our geographic reach and scale
of operations. Our nationwide presence allows us to serve customers across the United
States, which is important for those with networks spanning multiple states or regions.
The scale of our operations also allows us to quickly mobilize employees for emergency
restoration services.
Our diversity extends to our customer base as well, which lends strength
to the ongoing growth of our revenue base. In 2008, our largest customer
made up only 4.4 percent of our revenues. Our top ten customers
represented 30.3 percent of our total revenues, and our top twenty
customers made up approximately 43.3 percent of revenues. Diversity can
also be found in our types of revenues. In 2008, 57 percent of revenues
were derived from electric power services, 21 percent from gas services,
14 percent from telecommunications and cable television services,
7 percent from ancillary services and approximately 2 percent from
dark fiber licensing.
Quanta delivers diverse services across the electric power, gas,
telecommunications and cable television industries. This allows us to offer
complete solutions for virtually any type of network infrastructure. Each
employee receives extensive training with a core focus on safe operations.
Regardless of the service provided, safety is always the top priority.
Electric Power & Gas
Our electric power and gas operations have continued to perform
strongly. Demand remains high, and it appears that our top customers
remain financially stable despite the current economic environment. We
secured several significant contracts in 2008, including an Oklahoma
Gas and Electric contract for the construction of approximately 120 miles
of 345,000-volt transmission infrastructure near Oklahoma City. We
also successfully completed a major transmission project for Northeast
Utilities (NU) that contributed to the Middletown-Norwalk line being
energized ahead of schedule and below budget. In January 2009,
we expanded our second contract with NU to a total anticipated value
of $950 million. This contract covers various transmission projects in
the northeastern United States and is currently in the pre-construction
phase. Also in the first quarter of 2009, we signed a contract with
National Grid to provide engineering and installation services throughout
the New England region.
In 2008, our gas operations successfully completed various projects
related to the Barnet Shale natural gas gathering initiatives. Looking
forward, we see opportunities in natural gas gathering and pipeline
installation and maintenance services, although recently this industry
has been impacted by reduced demand and lower natural gas prices.
Telecommunications, Cable Television & Dark Fiber
Our telecommunications and cable television operations represented a
significant portion of our business in 2008. Though AT&T and Verizon
remain committed to their fiber-to-the-node projects, implementation will
likely be at a reduced pace through at least the first half of 2009. We
believe this business to be promising in the long term. We also remain
committed to our dark fiber licensing business, which continues to
experience a high level of demand primarily in the education and
healthcare industries.
A Resource for Renewables
Renewable energy is a rapidly emerging opportunity for Quanta. With
our nationwide reach, more than 14,500 employees and the financial
strength to fund large projects, we are well positioned to capitalize on the
increasing focus on reducing carbon emissions. We believe our strong
relationships with utilities are a key element to support the growth of
our business in the development of renewable generation infrastructure
and grid interconnections. We achieved our renewable revenues goal of
$150 million in 2008, and we expect that number will increase to at
least $300 million in 2009. We have also built a solid foundation in
solar generation, as evidenced by the Denver International Airport
solar project, as well as the recently completed installation of Sempra
Generations 10-megawatt photovoltaic power generation facility near
Boulder City, Nevada. The Nevada installation is currently considered
the largest thin-film solar power project in North America.
Over the past year, we also streamlined the cost of engineering, design,
procurement, project management and construction solutions related
to large-scale utility and commercial solar generation facilities. As the
price of solar panel generation continues to become more competitive
with other alternative energy solutions, we believe the solar market
will grow substantially.
Services related to wind generation comprised the majority of our
renewable revenues in 2008. Though wind generation lost some
momentum in late 2008, capital began to flow back into this
market in early 2009. The American Recovery and Reinvestment
Act of 2009 (ARRA) extended the renewable tax credits for wind
energy production until 2012. Many experts believe this will stabilize
the wind market and open the door to additional investment.
The ARRA includes provisions for investments in renewable energy,
energy efficiency and related infrastructure, as well as rural broadband
networks. We believe this stimulus plan along with the enforcement
of Renewable Portfolio Standards in 29 states will provide additional
opportunities for Quanta over the next several years.
Closing Thoughts
This letter could not possibly be complete without recognizing our
employees whose lives were disrupted by the hurricanes, ice storms and
fires that occurred in 2008. Hurricane Ike struck Houston, yet our
operations did not miss a beat. Many of our employees left their homes
and families to support restoration efforts, while their own houses
remained without power. We are grateful for our employees courage
and commitment during a very challenging situation.
In 2008, we continued to strengthen our executive team. Jim ONeil
took his post as President and Chief Operating Officer in October 2008.
Since joining Quanta nearly a decade ago, Jim has been instrumental
in improving our operations, and I know he will continue to apply his
expertise, dedication and drive to our future success.
In closing, I would like to thank all of our stakeholders for their continued
support. Though I am pleased with our performance in 2008, I believe
Quanta is positioned for even greater success in the future
.
Respectfully,
John R. Colson
Chairman and Chief Executive Officer