Dear fellow shareholders:
I am pleased to report that Prudential Financial delivered another strong performance in 2013.
Our outstanding results for the year were broadly based, reflecting strong growth across our
businesses, coupled with effective pricing discipline and risk management. We also benefited
from meaningful contributions from the significant transactions we have completed over the
past three years: our acquisition of Star Life and Edison Life from AIG in 2011, the completion
in late 2012 of two landmark pension risk transfer transactions, and our acquisition in early
2013 of the U.S. individual life insurance business from The Hartford.
The strength of our operations was proven by the achievement of our objective to deliver a
return on equity in 2013 of 13 to 14 percent. In fact, we exceeded the top end of that goal.
In addition, in 2013 we altered our schedule of dividend payments to shareholders from
annual to quarterly, ultimately delivering more than $800 million in Common Stock dividends
during the year.
The achievement of our earnings objectives and other actions throughout the year
demonstrate our ongoing commitment to maintaining a strategic mix of high-quality
businesses, managing risk appropriately, deploying capital effectively, and pursuing profitable
growth for the long term.
We continue to adhere to four key characteristics that drive our companys success:
1. Our unique business mix, which we have developed by deliberately focusing on our core
strengths and on markets in which we see opportunities. The complementary nature of our
portfolio of operations provides a mix of growth and stability, helping balance risk across
the company.
2. Our high-quality businesses, which have outstanding leadership, robust fundamentals and
strong positioning in the markets in which we operate.
3. Our financial strength, which enables us to continue delivering on our promises to our
stakeholders, while also seizing opportunities to innovate and grow.
4. The talent and integrity of our people, who remain our most important point of
differentiation in the market. Our commitment to nurturing a diverse and inclusive culture
helps us attract, develop and retain high-quality employees.
In short, 2013 was a year of major progress and accomplishments for the company on
many fronts. We maintained strong momentum in our businesses and further enhanced our
long-term positioning.
For 2013, on an after-tax adjusted operating income basis,* our Financial Services Businesses
earned $4.586 billion, or $9.67 per share of Common Stock, compared to $3.019 billion, or
$6.40 per share of Common Stock in 2012. On this basis, our earnings per share represent a
20 percent compound growth rate over the past three years.
Based on U.S. generally accepted accounting principles, our Financial Services Businesses
reported a net loss of $713 million, or $1.55 per share of Common Stock, in 2013, compared
to net income of $479 million, or $1.05 per share of Common Stock, in 2012. The 2013 net
loss was mainly driven by volatility in the currency markets and remeasurement of non-yen
product reserves in our Japanese insurance operations, which we consider non-economic since
we support these reserves with investments in appropriate currencies. Changes in the market
value of derivatives we use to manage our interest rate exposure were also a factor, largely due
to rising U.S. interest rates.
Our worldwide assets under management continued to increase, reaching more than
$1.1 trillion by year-end 2013. This growth is an important indicator of the confidence
our clients continue to have in Prudential.

Our strong operating results for the year were driven by
solid underlying performance across our businesses. Based
on our excellent performance, our solid positioning in the
marketplace and the talent of our people, we remain confident
in our long-term prospects and excited about our potential
as a company.
Achieving our 2013 ROE objective
In 2010, we announced our objective to achieve an ROE of
13 to 14 percent in 2013. We believed then, as we do now,
that achieving and sustaining that level of performance would
demonstrate our superior performance relative to our peers.
Our ability to achieve  and exceed  that goal in 2013
confirms our belief in our earnings power. Yet we never saw
that objective as a one-year, once and done objective.
Looking ahead, we remain confident in our ability to sustain an
ROE of 13 to 14 percent over the long term.
Building on our momentum
In 2013, we benefited from the contributions of several
targeted acquisitions and transactions we have completed
since 2011 in key markets and segments that complement our
strategic business mix.
We have substantially completed the integration of the
Star Life and Edison Life companies with our Gibraltar Life
operation in Japan. This acquisition is delivering what we
expected in terms of both synergies and business results. We
are benefiting from expanded distribution capabilities from
this acquisition, including new sales professionals and thirdparty
distributors, as well as the addition of a large force of
independent agents to our existing channel.
The integration of the U.S. individual life insurance operations
we acquired from The Hartford in 2013 is also well on track.
Through this acquisition, our U.S. life insurance operations
achieved greater scale, expanded product offerings and
broader distribution capabilities. Our domestic individual
life insurance business is already benefiting from a unified
distribution system, strong connections with financial
institutions, and our solid positioning in the brokerage general
agency channel.
The two landmark pension risk transfer transactions we
completed in 2012 contributed to the achievement of record
earnings in 2013 in our Retirement unit. We are confident
that the pension risk transfer market will continue to offer
exciting opportunities to our company, for which we are
uniquely suited.
In addition to producing significant contributions to our
earnings, these acquisitions and transactions have brought
more diversity to our stream of earnings and, thus, more
balance to our operations as a whole. Their successful
execution has further distinguished our company within our
industry and positioned us for further growth.
In 2013, we continued to invest in and expand our operations
in pursuit of other promising opportunities, particularly on the
international front. In August, we announced an agreement to
purchase Uni.Asia Life Assurance Berhad (UAL), a mid-sized
life insurer in Malaysia, through a new joint venture with Bank
Simpanan Nasional, a government-owned bank. We closed
this transaction in January 2014, and hold 70 percent of
UAL. Malaysia is an attractive market, with low life insurance
penetration, a well-developed regulatory environment and
long-term growth potential. This transaction is aligned with our
strategy to build upon our success in Japan, South Korea and
Taiwan, and expand our footprint in growth markets in Asia.
Our success with all of these transactions demonstrates our
ability to identify, seize and execute on opportunities that
offer the potential for significant earnings and that further
differentiate Prudential in the marketplace.
Operating high-performing businesses
We continue to maintain a carefully assembled portfolio
of high-quality businesses focused on two interrelated
customer needs  protection and retirement. This mix of
businesses reflects our long-term strategy of retaining a
clear focus on core businesses where we have proven
strength and see opportunity.
The solid performance of our businesses in 2013
demonstrates the strength of our businesses and our strategy.
Individual Annuities account values surpassed the
$150 billion milestone at year-end 2013, totaling more than
$154 billion, up 14 percent from year-end 2012. We continue
to manage our annuities business prudently, adapting our
products to the current environment to maintain our return
prospects and improve our risk profile, while maintaining our
value proposition.
Our Retirement business achieved record account values
in 2013, reflecting strong sales and net flows. Total account
values for the Retirement operation were approximately
$323 billion as of year-end 2013, up 11 percent over the
prior year.

In our Asset Management segment, we recorded about
$24 billion of institutional and retail net flows for the year.
The segments assets under management totaled nearly
$870 billion at year-end 2013, up about 5 percent from
a year earlier.
Sales in our Individual Life Insurance operation reached
$731 million for 2013, up 77 percent over 2012. This
increase reflects the impact of The Hartford acquisition,
which contributed to an improvement in our competitive
position and expanded distribution through third-party
distributors, including banks and wirehouses.
In Group Insurance, sales totaled $313 million for the year,
representing a decrease of about 29 percent from 2012.
We continue to take steps to focus our Group business on
products where we see long-term opportunity, with a goal of
achieving sustainable, profitable growth. We are encouraged
that the operations 2013 results demonstrated improved
claims experience.
For the year, sales in our International Insurance division,
on a constant dollar basis, were about $3 billion, down from
nearly $4 billion the prior year. This result reflects deliberate
actions we have taken to control our product concentration
and maintain appropriate returns.
Deploying capital to support sustainable growth
Effective capital management remains a hallmark of
Prudential and fundamental to our overall strategy.
Maintaining robust capital and liquidity positions helps shield
the company from market volatility and enables us to retain
the financial strength and flexibility we need to pursue new
opportunities, consistent with our long-term strategy.
In 2013, we reconfirmed our commitment to delivering
additional value to our shareholders through share repurchases
and dividends. During the year, we declared four quarterly
Common Stock dividends totaling more than $800 million,
including a dividend of $0.53 a share in the fourth quarter
that represented a 33 percent increase from prior quarters.
In addition, we continued to repurchase stock, acquiring
$750 million of Common Stock during 2013. From the
commencement of share repurchases in July 2011 through
December 31, 2013, the company has acquired 41.3 million
shares of its Common Stock under its share repurchase
authorizations, at a total cost of $2.4 billion.
Contributing to development of appropriate and
meaningful regulation
We recognize the importance of meaningful regulation of our
industry and our company. Financial strength is part of our
value proposition, and being well regulated is important to
our company.
In September, Prudential received notice that the Financial
Stability Oversight Council had designated the company as a
non-bank Systemically Important Financial Institution. As a
result of this determination, the company is being supervised
by the Board of Governors of the Federal Reserve System and
is subject to stricter regulatory standards. In July, we were
designated as a Global Systemically Important Insurer, which
may also result in heightened regulation.
We continue to participate in active and constructive
discussions with the Board of Governors of the Federal Reserve
System, as well as other regulatory authorities, to develop
an effective framework for group supervision of insurers, and
one that reflects the important differences between insurance
companies and banks. We will remain engaged at both the
global and domestic levels in influencing the development
of regulatory standards that are beneficial to consumers and
preserve competition within the insurance industry.
Fulfilling our responsibility as a corporate citizen
We recognize the importance of fulfilling our obligations to all of
our stakeholders, including our regulators, our shareholders, our
customers and our employees. Maintenance of a robust program
of corporate governance is one of the ways in which we achieve
this objective. We continue to enhance our efforts related to
our commitment to sustainability, including our work related
to shareholder engagement and to diversity and inclusion. Our
focus on these areas is a reflection of our corporate culture,
which we believe helps differentiate us in the marketplace.
We also maintain our long-held commitment to corporate
social responsibility and to supporting the communities
where Prudential people live and work, through direct
contributions and investments, as well as the time and
talent of our employees.
In 2013, we provided contributions of nearly $29 million
through The Prudential Foundation, as well as approximately
$100 million in social investments. Prudential employees
around the world continued to uphold the companys long
tradition of volunteerism. In October, more than 22,000
volunteers, including employees, friends, family members and
clients, in the United States and 11 other countries, took part
in our 19th annual Global Volunteer Day.
We continue to support the revitalization of Newark, N.J., which
has been our headquarters for nearly 140 years. In April 2013,
we demonstrated our commitment to Newark in a very visible
and exciting way, as we broke ground for the construction of a
new building, which will be our third office facility in the city.

The new Prudential tower is just one of our many investment
projects in Newark. In total, our current investments in
Newark will support more than 1,000 new residential units
and 22 acres of open space, and leverage over $1 billion in
current and new development activities in the city. We look
forward to continuing to contribute to Newarks resurgence.
Nurturing a culture of excellence
Prudentials people  their talents, ideas and integrity 
represent our most important competitive advantage. Thats
why we are fully committed to offering an environment where
employees feel free to do their best work and empowered to
bring their best ideas.
That commitment requires ensuring that diversity and
inclusion are integral to our company culture  and not just
in our approach at the workplace. We recognize that we
need to embed diversity and inclusion in every aspect of our
operations  among employees, in working with our customers
and our suppliers, and in helping the communities where we
live and work  to have the ability to reach and support diverse
markets around the world.
In 2013, we continued our series of signature research
that examines financial trends in Americas multicultural
communities by launching our second biennial study on
the African American financial experience. Together with
our recent research into the financial experience of women,
Hispanic Americans and lesbian, gay, bisexual and transgender
Americans, these studies are revealing compelling information
about the needs of customers across these populations.
We also continued to support a variety of programs designed
to help Veterans develop meaningful careers after their military
service and between deployments. In addition to helping an
often overlooked segment of our population, our work to help
our Veterans successfully transition from the military
to the corporate world is another important aspect of our
talent management efforts. As with our efforts related
to diversity and inclusion, we are reaping rewards from our
work with Veterans by identifying, attracting and retaining
talented new employees.
In 2013, our efforts to foster diversity and inclusion, support
Veterans and their families, and be an employer of choice
were again recognized by a wide variety of organizations. We
were also pleased to again be named by FORTUNE as one
of the worlds most admired companies in the life and health
insurance category. We are gratified by this external validation
of the caliber of Prudentials people.
Sustaining our performance
We are very proud of our performance in 2013. We delivered
on an ambitious objective and demonstrated the earnings
power of our operations.
Our challenge now is to sustain a level of performance that
differentiates us from our peers. We believe that the same core
strengths that have served us well in the past  our financial
strength, the uniqueness of our business mix, the quality of
our operations and the talent of our people  will help us meet
this challenge. As always, we are committed to achieving our
objectives in the right way, with integrity and a relentless focus
on quality control.
Thank you for your continued confidence in Prudential. I look
forward to reporting to you on our future accomplishments.
JOHN STRANGFELD
Chairman of the Board,
Chief Executive Officer and President