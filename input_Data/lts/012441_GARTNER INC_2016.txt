Dear Shareholders:

Technology is impacting every enterprise at an
increasingly rapid pace. Whether seeking new sources
of growth, business model transformation or operational
efficiencies, technology is part of the solution for
virtually every enterprise.
Gartner is at the heart of technology. Every
company is becoming a technology company,
creating unparalleled opportunity for Gartner. Our
clients depend on us for the insight and advice they
need to successfully leverage technology within the
digital economy.
Five Elements That Drive Our
Sustained Growth
� At the core of Gartner is our strong value
proposition, delivering unique and competitively
differentiated insights on clients� mission-critical
priorities. We provide high client value at a very
low cost.
� We have a vast market opportunity, driven by
the pervasive criticality of technology-driven change.
We have increased our addressable market over
time through strategic acquisitions, organic growth
and product development.
� Our winning strategy for growth is to deliver
market-leading insights through innovative,
differentiated offerings while operating at scale.
We are growing our organizational capabilities to
capture our vast market opportunity, while ensuring
we remain focused on continuous improvement
and innovation in everything we do.
� We have an extraordinary business model,
based on recurring revenue and high retention rates.
Our clients are highly diversified across geography,
industry and client size. We have high incremental
margins and generate free cash flow substantially in
excess of our net income.
� We are exceptional at operational execution.
Gartner is a performance-driven organization.
Our tenured, stable leadership team has breadth
and depth, which has led to a sustained track record
of double-digit performance in our key metrics.
In addition, our strong cash flow generation and
strategic balance sheet management have delivered
consistent shareholder value.
Gartner Drove Another Year of Double-Digit
Growth in 2016
The macroeconomic environment remained challenging
in 2016. Many major economic countries and regions
around the world experienced slower economic growth
or declines and virtually all currencies continued to
weaken relative to the U.S. dollar.
Despite this backdrop, we delivered another year of
double-digit growth in 2016. For the full year 2016, we
generated more than $2.4 billion of revenue and $457
million of normalized EBITDA, representing year-overyear growth of 14% and 10% respectively, excluding the
impact of foreign exchange. Diluted earnings per share
excluding acquisition adjustments was $2.96 in 2016, a
year-over-year increase of approximately 25% excluding
the impact of foreign exchange and free cash flow
increased to $347 million.
In fact, we�ve delivered 7 years of double-digit growth,
reflecting the tremendous value we deliver to our clients
whether they are thriving or are in financial distress.
We�re Not Standing Still
We continue to make investments that will augment
our current offerings or expand our market opportunity.
In 2016, we added more depth to our global analyst
community with the acquisition of U.K.-based Machina
Research. We grew our salesforce, closing 2016 with
more than 2,400 highly talented associates tasked
with capturing our vast market opportunity. 

Gartner�s acquisition of SCM World further expanded
our existing supply chain offerings, enabling us to
deepen our relationships with many of the largest
enterprises in the world.
Further Fueling Long-Term Growth:
CEB Acquisition
In early 2017, Gartner announced an agreement to
acquire CEB. This highly complementary acquisition will
further advance our strategy to drive long-term growth
and value for our shareholders. On a pro-forma basis,
the combined business had approximately $3.4 billion of
revenue and $0.7 billion of adjusted EBITDA in 2016.
The Gartner brand is known for delivering insights to all
levels of IT, Supply Chain and Marketing professionals.
CEB is widely admired for delivering best practice and
talent management insights to executives in other
functions such as HR, Sales, Finance and Legal.
Together, we are more than 13,000 associates strong
and the leading global research and advisory company
serving all major functions in the enterprise.
Business Segment Review
Gartner Research, our largest and most profitable
business, closed another record-breaking year with
reported revenue of $1.8 billion, an increase of 17%
year over year, excluding the impact of foreign exchange.
We closed 2016 with reported contract value (a key
indicator of future revenue and profitability) of $1.9
billion, an increase of 14% year over year, excluding the
impact of foreign exchange, and the highest reported
contract value in Gartner history.
Gartner Consulting generated revenue of $346 million
in 2016, an increase of 6% year over year, excluding
the impact of foreign exchange, and we closed the year
with $104 million of backlog (a leading indicator of future
growth for Consulting) � in line with our operational
target for this measure. Gartner Consulting extends
the reach of our Research business by deepening our
relationships with our largest clients. We continue to
invest in our Managing Partner strategy, ending 2016
with more than 120 Managing Partners.
On a full-year basis, Gartner Events reported revenue
of $269 million, an increase of 6% over 2015, excluding
the impact of foreign exchange. In total, the 66 events
we held in 2016 attracted more than 54,000 attendees.
Gartner Events is the leading global technology
conference provider, enabling IT, supply chain and
digital marketing professionals around the world to
experience our research, interact with our analysts
and meet with technology providers � all in a single
forum. We remain focused on ensuring every single
event we produce is a must-attend conference in every
geography in which we operate.
Consistent Execution for Long-Term Results
These are exciting times for Gartner � we are the
strongest company we have ever been. We deliver
incredible value to our clients whether they are
thriving or in financial distress. The acquisition
of CEB, along with the investments in our organic
business, further advances our strategy to provide
high-value research and insights that address our
clients� mission-critical priorities.
In summary, we�re confident in our ability to continue
our track record of sustained growth for many years
to come.
On behalf of everyone at Gartner, thank you for
your support.
Gene Hall
Chief Executive Officer
Craig Safian
Chief Financial Officer