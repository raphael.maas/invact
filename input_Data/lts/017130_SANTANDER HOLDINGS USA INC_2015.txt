Message from Ana Botin 
8 
2015 ANNUAL REPORT 
Message from Ana Botin 
Dear fellow shareholders, 
In 2015, we delivered on everything we promised a year ago and delivered in the right way. We increased our earnings and used them to pay a higher cash dividend, to invest in our business and to strengthen our capital base organically. This success has put us ahead of our strategic plan. 
2015 At a Glance 
We have increased our number of loyal customers by 1.2 million and improved customer satisfaction so that we are now in the top 3 in 5 of the countries where we operate, which is our aim in all markets. According to our internal surveys, we are all feeling more engaged in our work. 
The results of more loyal customers and a more engaged team are a strong operating performance and a net statutory profit in 2015 of 6 billion: 
		Customer loans grew by 6.4%. 
		Customer revenues grew by 7.6% to 42 billion. 
		Underlying profit after tax (excluding PPI and other one-off effects) grew by 13%. 
		This growth in revenues and profitability has allowed us also to grow our capital organically by 40 basis points, to 10.05% (10.15% excluding PPI) and to grow our cash dividend per share by 79%. 
		Finally, our company is more valuable than 1 year ago, as measured by our tangible net asset value (TNAV) per share, which grew by 0.11. Those of you who acquired shares at the time of our capital raise on January 8 2015 and still hold them, have received a cash dividend per share of 0.11 and a total dividend per share of 0.40, equivalent to 6% of your investment. 
GROWING CUSTOMER LOANS 
The purpose of capital buffers is to protect our customers, shareholders and employees. We take this responsibility extremely seriously. 
Our prudential minimum capital requirement today is to maintain a Common Equity Tier1 (CET1) of 9.75%. Our capital adequacy currently stands at 12.55%, a buffer of 280 bps, equivalent to 16 billion. 
The reason we have these excess buffers is to get ready for 2019 when we will converge to the regulatory requirements known as Basel III. 
Customer loans 
+6.4% 
GROWING CUSTOMER REVENUES 
But since that date our market valuation has fallen by 36%. This is probably related to a different perception of the strength of our capital and the extent of our regulatory capital buffers and to the concern about our presence in certain emerging markets. 
Customer revenues 
+7.6% 
(42,000 million) 
INCREASING OUR PROFIT 
6,000 
million (+3%) 
AND GROWING OUR CAPITAL 
+40 bps 
organic capital growth 
Net profit 
9 
2015 ANNUAL REPORT 
Message from Ana Botin 
Since 2007, our preprovision profit has been on average 2.3 times the provisions we incurred 
Our Brazilian team delivered strong recurring profits and made significant one-off positive contributions. 
IN BRAZIL 
(+13% in euros) 
14% 
NPLs 
3.2% 
Best among private banks 
As we announced to investors last September, our goal is to have a CET1 capital ratio fully compliant with Basel III criteria of more than 11% by December 2018, when our regulatory requirement will be 10.5%. I am confident that with the uplift we achieved in 2015 and our current growth and capital generation, we will meet our target. 
We have set this goal of above 11% to align with the highest prudential standards for two reasons. First, our required minimum is less because our model is considered less interconnected, and easy to resolve. Second, we need lower management buffers over this minimum because of the relatively low volatility of our earnings and our better relative performance under stress scenarios. 
The key factors in favor of Santander are: Our business is less volatile than that of our peers. We have paid a dividend every year 
for 50 years. 
 We went through the financial crisis without reporting any quarterly loss. We paid dividends every year and at the lowest point, in 2012, we delivered a net statutory profit of 2.3 billion, as our retail and commercial banking activities continued to be profitable practically in every market. 
 Our subsidiaries are autonomous in managing their capital and liquidity. We have more than sufficient capital to operate safely, to satisfy regulators in all of our markets and at the Group level, and to provide the returns expected by our investors. 
 Since 2007 we have generated profits before taxes of 93 billion. Our pre-provision profit has been on average 2.3 times the provisions we incurred. We are now transforming our bank to expand our capacity to generate capital. This will make us even more resilient throughout the business cycle. 
However, what best explains our market underperformance since our historical high valuation of 100 billion in April last year are concerns about the future of Brazil. 
Brazil is going through a challenging period, but our bank performed excellently there this year. Our team delivered strong recurring profits and made significant one-off positive contributions. Net statutory profit grew by 33% in local currency and by 13% in euros in 2015. Our return on tangible equity (RoTE) in Brazil was a healthy 14%. Finally, our balance sheet in Brazil which represents 8% of total Group customer loans- shows the lowest non-performing loan ratio among the top Brazilian private banks: 3.2%. 
Today in Banco Santander, as our performance in 2015 shows, we have the people, the vision and the resources to deliver for our shareholders. 
We will manage the business to deliver on earnings per share (EPS), dividends per share (DPS) and TNAV per share as I laid out in my letter last year and at our Investor Day in September. 
Profit 
+33% 
RoTE 
10 
2015 ANNUAL REPORT 
Message from Ana Botin 
The Santander Moat is large and deep 
In summary, todays market is not considering the full value and strength of our model and our diversification. 
Warren Buffett often says that he likes to invest in companies with a moat, a competitive advantage which protects profits and market share over time. 
Our moat is our critical mass in every one of our 9+1 (Santander Consumer Finance Europe) core markets, where we serve a total of 121 million customers. This provides consistent earnings, quarter after quarter and through the cycle. 
We have earned the trust of our customers over many years, through hard work and careful stewardship of their financial affairs. Our relationship managers talk to many of these customers every day. They have helped them through difficult times, supporting when others who know them less well might have walked away. 
We also operate in a carefully assembled mix of developed and developing markets. When one or two markets are struggling, others are thriving. 
Santander Consumer Finance is the top consumer bank in Europe. In Mexico, we are the main bank for small and medium sized enterprises. In Poland, our bank is the most profitable among its peers. We have the second largest private bank and the most profitable one in Portugal. And that doesnt take into account the continued strength of our most important banks in Spain and the UK, which have performed well despite continued low interest rates. 
The combined growth of our continental European business this year has delivered 2.2 billion attributable profit, or 35% growth; our UK and US businesses delivered 2.6 billion attributable profit, 10% more, representing 31% of total attributable profit1. 
The second half of this years disappointing share price does nothing to undermine my belief in our diversified structure which has been built to do exactly what it is doing: providing predictable earnings with lower volatility through the cycle. 
Our critical mass, our personal relationships with customers and our geographical diversification combine to create the large, deep moat around us. 
These are the sources of our unique competitive advantage and what give us confidence that we can deliver earnings at the same time as we adapt our business for the future. 
They are the foundations upon which we are building Santander for the next 50 years. 
We have scale and financial strength on our side and we are learning how to think and act like a challenger at the same time. 
OUR MOAT: 
Critical mass in 
9+1 
markets 
Personal relationships: 
121 
million customers 
Geographical diversification 
Consistent earnings through the cycle 
1. Excluding the corporate centre and Spains Real Estate activity. 
11 
2015 ANNUAL REPORT 
Message from Ana Botin 
We have made great strides in helping people and businesses, our customers, prosper 
Customers 
When I wrote to you last year and laid out my vision and plans to transform Santander, I said that the measure of our success will be that wherever we operate our customers are the ones who champion our services and bring in new customers. We have made great strides in helping people and businesses, our customers, prosper. I would like to review in detail what we have achieved in 2015. 
Banking is an industry which will look very different very soon because of technology. But it remains at its heart a personal business. It is about satisfying the needs and aspirations of our customers, of families wanting to buy homes and businesses wanting to expand. Our daily work is about serving our 121 million customers today and to anticipate what they will need tomorrow: a loan as well as the latest mobile app to fit seamlessly into their digital lives. 
Our focus this year and going forward, will be to earn the loyalty of our customers and encouraging greater use of our digital banking services. In simple terms: we want more of our customers to do more of their banking with us. And we are ready for them to do more of their banking digitally. 
Progress in 2015 
 In the UK, one out of every three new accounts is now opened via our digital channels.  In Poland, our customers can now apply for a cash loan using their phones and receive a 
response within 60 seconds. In Spain, a new 1|2|3 account is opened every minute through our digital channels. 
As a result of these efforts, we have reached our targets and grown our number of loyal customers by 1.2 million and our digital customers by 2.5 million. 
In the markets where our number of loyal customers has increased the most, so has our revenue. And this progress is reflected in rising customer satisfaction. In five of the markets we serve, we are ranked among the top three banks for customer satisfaction. We care a lot about these customer satisfaction rankings and loyalty numbers because they set the pulse of our business. If they are strong and healthy, our company is too. 
People 
Santander built a strong, successful culture over many years. This culture was at the root of our expansion and growth. Now we need to change. This is going to take hard work and time, but we are well on our way. 
Internally, we have been undergoing a process of profound cultural change. We are reevaluating every one of our processes to ensure that we can be true to our values, purpose and aim, and be ready to embrace new technology sweeping through financial services. 
I want every member of our global team to feel motivated and inspired by these changes, to know that we will do everything we can to support them in their work. I am asking for the same commitment to change from myself, my board and my most senior executives, as I am from those who work in our branches and help our customers every day. 
Our latest survey of our global team showed that many more of us believe in this process today than when we started a year ago. We are rethinking how we measure performance and create incentives. Our program of flexiworking has been especially popular. We want our teams to guide us, to let us know how they can contribute most to our organization. 
Loyal customers 
+1.2MM 
(+10%) 
We want more of our customers to do more of their banking with us 
Digital customers 
+2.5MM 
(+17%) 
12 
2015 ANNUAL REPORT 
Message from Ana Botin 
During 2015 we have worked to agree on the behaviours that will help us build a bank that is more Simple, Personal and Fair. 
There are eight of them: show respect; truly listen; talk straight; keep promises; actively collaborate; bring passion; support people; and embrace change. It is a short list on purpose. It is meant to be achievable. 
We value honesty, energy and directness in our families and friends, and we should expect no less from our colleagues at work. 
Across the organization, we are focused on becoming more efficient and more transparent. At the corporate centre, we have reduced the number of divisions from 15 to 10 as well as the number of top executives and executive board directors at the Group level. 
This has allowed us to reduce the total cost of compensation for those at this level by 23%. 
Good governance has taken on fresh importance since the financial crisis, and we are working harder than ever to appoint the best people and create the clearest lines of accountability between all of our operations. Our industry is complex by nature, but our business should never be more complex than necessary. 
We are constantly seeking the ideal balance between our corporate centre and our countries. We trust our local teams because they are closest to our customers. But we also want them to take advantage of being part of a global Group. 
From our centre in Spain, we offer products and best practices, ranging from technology systems to control, which enable our subsidiaries to capture significant economies of scale. The synergies created by this system are worth 3 points of our cost-to-income ratio. Our in-country teams can stay close to their markets while operating more efficiently than their competitors. There are no intermediate levels between our country heads and the Group CEO, because we believe that a lean corporate structure, with the fewest possible layers of management, is the best guarantee of simplicity and transparency, and will deliver for both our customers and our shareholders. 
This relationship between the centre and our subsidiaries is essential to continue to improve our cost-to-income ratio, a key measure of efficiency that remains one of the best in the industry at 47.6%. And our simple, geographically ring-fenced subsidiary model results in the lowest Financial Stability Board additional capital recommendation among our peers. 
Shareholders 
Until the situation in Brazil began to deteriorate in mid year, the relative performance of our share price was comparable to that of our peers and the major indexes. 
The long-term story of Brazil is the growth and development of one of the largest emerging economies in the world. We are going to endure the current situation, be patient and be strongly positioned when Brazil resumes its upward journey. 
It is important that our shareholders recognize this, and consider the growth in our TNAV per share in 2015. There is always a lot of noise in finance, but the strong, underlying signal coming from Santander is of steady growth and value-building. I am convinced that our share price will eventually come to reflect this and our shareholders patience will be rewarded. 
Many more of our people believe in cultural change than they did when we started one year ago 
Corporate centre value added:The synergies created by this system are worth 3 points of our cost-to-income ratio 
% of Group underlying attributable profit 
South America 29% 
NorthAmerica Europe 15% 56% 
13 
2015 ANNUAL REPORT 
Message from Ana Botin 
THE DIVERSITY OF OUR GEOGRAPHIES WITH CRITICAL MASS(profit growth in euros) 
We derive enormous benefits from the way our diverse geographies and retail and commercial banks with critical mass minimize our risks and even out our revenues. 
During 2015, we grew our net profits (in euros) in Spain and SCF by 18%, in the UK by 27%,in Portugal by 63%, in Brazil by 13% and in Mexico by 4%. These businesses represent 81% of our customer loans and 76% of our Group profits. Chile reduced its profit by 9%, US by 21% and Poland by 15%. These three businesses represent 16% of our customer loans and 17% of our profits. 
We see great potential for us to grow in Poland where we are leaders in digital channels and where loans are growing by 11%. We are working to improve our operations in the USA: we have put in place a new team in recent months, composed of top talent at both the executive and board levels. We know what we have to do in the USA to succeed on all fronts. 
Our model proved its worth during the financial crisis, throughout which we never posted Spain: SCF: a single quarter of losses. We never required a bail-out in any of the countries where we 
+18% +18% 
UK: Portugal: 
operate. Though designated a global SIFI (Systemically Important Financial Institution) we have the lowest capital charge among global SIFIs. And for these reasons, we need lower capital buffers, as noted previously, than other international banks with different models. 
It was not an easy decision to change our dividend policy, as we did last year. But we have to pay a dividend that reflects the reality of the macro-regulatory situation and our earnings, and is consistent with our strategy. What is important, is that our model delivers enough profits to reinvest further in: profitable growth; a strengthened capital base; and an increased dividend per share. 
Communities 
We continued our support for higher education through our Santander Universities programme which now reaches more than 1,200 universities around the world. Last year, we awarded around 35,000 scholarships to students attending these universities, as well as investing in programmes to improve financial inclusion and education. 
We have launched the UK Discovery Project, helping people prosper through enhanced education, skills and innovation, which will support a million people by 2020. 
We also supported around 7,000 entrepreneurs and 500 start-ups through our community programs to promote job creation. 
Our target is to support 4.5 million people between now and 2018. Looking ahead 
It is said that strategy rarely survives first contact with adversity. But after eighteen months in charge of Santander, I am confident that the plan we have in place is the right one. 
We are building from a strong and diverse base. Santander built a reputation over the past three decades as an expansive, acquisitive bank, venturing from Spain to markets across Europe and the Americas. I cannot rule out future bolt-on acquisitions in our 9+1 core markets, provided they make both strategic and financial sense, but for the immediate future we are focused on growing loyal customers and organic growth. 
We are overhauling our operations and our management to make them more Simple, Personal and Fair. We want our employees to feel happier and prouder than ever to work for Santander. We are building and learning new technologies so that we can revamp our internal processes 
+27% +63% 
Brazil: 
Mexico: 
+13% +4% 
14 
2015 ANNUAL REPORT 
Message from Ana Botin 
and develop better products and services for our customers, whilst remaining best-in-class in efficiency. 
And we are lowering our cost of risk with an average target for 2015-2018 of 1.2%. 
Our goal is to grow earnings and dividends per share annually, reaching double digit EPS growth by 2018, from a stronger, more resilient capital base with a CET1 above 11%. 
Over the coming year, we anticipate different contexts for the developed and developing economies where we operate. In the developed economies, we envisage steady low GDP growth and falling unemployment. Low oil prices and low interest rates will be good for both individual and corporate customers. 
Interest rates in the United States seem to be moving upwards, but the return to normality in the credit markets after years of quantitative easing is going to take time. Political uncertainty persists in parts of Europe, and a new president will be elected in the United States in November. Our base case scenario is low and flat yield curves in the developed markets for quite some time. 
In the developing economies, we are always braced for greater volatility. But the underlying trends remain hugely promising. We are well placed in markets with young and growing populations, low banking penetration and low borrowing levels, where we can earn returns on equity far higher than those we earn in the developed markets. As I mentioned above, diversification is our strength. 
Listening to our customers and anticipating what they want from us; fixing things fast when we make mistakes; making their interactions with us Simple, Personal and Fair, each and every time - these are our main goals, today, tomorrow and as far into the future as we can see. 
To guide us, we will focus on our purpose: to help people and businesses prosper. This is the Santander Way. It is the foundation for our success. And we have a clear aim: to be the best retail and commercial bank, earning the lasting loyalty of our people, customers, shareholders and communities. 
Strong corporate governance is vital to all of our work. Banco Santanders board is fully involved in the Groups oversight. I would like to thank Juan Rodriguez Inciarte and Sheila Bair for their invaluable contribution to the bank. 
We have strengthened our boards both centrally and in our regional subsidiaries, drawing on strong independent directors to provide fresh perspectives and advice. 
2015 has been a year of tremendous learning and progress for me personally and for Santander. 
We can see a clear path to the objectives we have set ourselves for 2018. But we still have to walk that path and turn the unforeseen bumps ahead into opportunities if we want to deliver on our purpose of helping people and businesses prosper. 
We still have to act each day in a way that is more Simple, Personal and Fair. The digital revolution in finance wont happen by itself. We aspire to lead in ensuring that it delivers on its promise for our customers above all. 
With the support of our nearly 4 million shareholders, a Board committed to our objectives and an excellent team, I am confident we will succeed. 
Ana Botin 
We are well placedin markets with young and growing populations, low banking penetration and low borrowing levels, where wecan earn returns on equity far higher than those we earn in the developed markets 
2018 TARGETS: 
> Increase EPS reaching double digit growth in 2018 
>CET1>11%> Average cost of credit 
2015-2018: 1.2% 
> Increase DPSand TNAV per share 
