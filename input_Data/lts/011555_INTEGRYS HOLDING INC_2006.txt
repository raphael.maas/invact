                                                           Dear Fellow Shareholders




                    


                                                 The global economy is impacting Americans in many ways. For our part,
                                                 Integrys Energy Group is embracing a new economic environment that

                                                 will change the way our industry and our company operates. That change

                                                 has invigorated our workforce and stimulated our progress. This past

                                                 year you could "see the energy" in our employees as they embraced the
                                                 changes and created a stronger yet leaner company with a conservative

                                                 risk profile--a company prepared to deal with global pressures and
                                                 continue our commitment to customers, shareholders, and the

                                                 communities we serve. Let's look closer to "see the energy."



See the Energy in Utility Mergers                                 See the Energy in Growing Utilities
and Acquisitions                                                  Shareholders can also see the value in our growing utility
                                                                  businesses. Electric load at Wisconsin Public Service
Shareholders can see the value in our strategy of continuing
                                                                  Corporation exceeded 2,400 megawatts for the first time in
to grow our existing operations, which includes our natural
                                                                  its 123-year history. The actual record of 2,425 megawatts
gas utility business. On July 10, 2006, we announced a
                                                                  was attained on July 17, 2006. This is a 3 percent increase
proposed merger with Peoples Energy Corporation and
                                                                  over our previous record of 2,357 megawatts set in August
completed the merger just seven and a half months later.
                                                                  2005. To meet this load growth, we continue to grow our
Effective February 21, 2007, Peoples Energy became a
                                                                  generation capability with the construction of Weston 4,
wholly owned subsidiary of WPS Resources Corporation,
                                                                  a 500-megawatt coal-fired facility. This plant will use state-
and WPS Resources changed its name to Integrys Energy
                                                                  of-the-art technology to minimize environmental and social
Group, Inc. and its ticker symbol to TEG. Peoples Energy's
                                                                  impacts. This is a utility asset investment with a total cost
utility operations serve about one million natural gas
                                                                  to us of about $549 million. Wisconsin Public Service
customers in Illinois. The merger creates a larger and more
                                                                  owns 70 percent and will operate the facility. Our partner,
diversified regulated utility business. We are now better
                                                                  Dairyland Power Cooperative, owns 30 percent. The project
positioned to serve our regulated customers with our
                                                                  is scheduled for completion in 2008. I'm pleased to report it
constructive approach with regulators and our willingness
                                                                  is on schedule and within budget.
to make capital investments.

Financially, this merger holds tremendous promise. Our
                                                                              Financially, this merger
integration teams are focusing on achieving $94 million
                                                                             holds tremendous promise.
of steady-state annual synergies within five years, and we
anticipate reaping many of the synergy benefits by 2008.
Costs to achieve those synergies are expected to total about
                                                                  We are also growing our investment in transmission with
$186 million.
                                                                  American Transmission Company (ATC). As of the end of
                                                                  January 2007, we own about 31 percent of ATC, which has
As part of the merger, we increased our quarterly common stock
                                                                  an authorized return on equity of 12.2 percent through 2012.
dividend rate to 66-cents-per-share after February 21, 2007.
                                                                  This is a solid utility investment with substantial growth
This resulted in a prorated dividend for the first quarter of
                                                                  prospects because ATC has a $3.1 billion capital investment
2007. We anticipate that Integrys Energy Group will continue
                                                                  budget until November 2016. Our ownership in ATC will
the 66-cents-per-share quarterly dividend rate in the future,
                                                                  increase throughout 2007 to about 34 percent as we finalize
subject to evaluation by the Integrys Energy Group Board of
                                                                  the construction of the Wausau, Wisconsin, to Duluth, Minnesota,
Directors as future business needs dictate.
                                                                  transmission line and will stabilize at about 35 percent in 2008.
With this merger, our holding company headquarters moved          This is a 220-mile, 345-kilovolt transmission line scheduled
to Chicago. All of our utilities retained their identity and      for completion in 2008. Construction of this line began in
headquarters in their existing locations.                         August 2005, and 145 miles of the line were completed by
                                                                  December 2006. This portion of the line has been energized and
As part of our ongoing growth efforts, we also acquired two
                                                                  placed in service. Construction of the remaining 75 miles from
natural gas distribution operations in 2006. The acquisition of
                                                                  Stone Lake, Wisconsin, to the Minnesota border near Duluth
natural gas distribution operations in Michigan was completed
                                                                  is underway. The Minnesota portion was completed in 2005.
on April 1, 2006, which added approximately 166,000 customers
                                                                  The overall project is ahead of schedule and within the
in southern Michigan. Three months later, we completed the
                                                                  authorized spending limit.
purchase of natural gas distribution operations in Minnesota,
adding approximately 207,000 more customers concentrated in       Growth in our existing utility natural gas infrastructure
165 cities and communities including Eagan, Rosemont,             will also continue. Guardian Pipeline announced plans to
Rochester, Fairmount, Bemidji, and Cloquet, and Dakota            construct a natural gas pipeline from Ixonia, Wisconsin,
County. These operations became Michigan Gas Utilities            to Green Bay, Wisconsin. This will provide northeastern
Corporation and Minnesota Energy Resources Corporation,           Wisconsin with an alternative pipeline supplier. Currently a
wholly owned subsidiaries of Integrys Energy Group, Inc.          single natural gas pipeline serves all of our 306,000 natural
                                                                  gas customers in Wisconsin.
The natural gas utility acquisitions in Michigan and Minnesota
in 2006 and the merger with Peoples Energy in Illinois in 2007    Wisconsin Public Service expects to invest approximately
add customers, add talented employees to our team, provide a      $73 million, subject to regulatory approval, in new natural
much larger utility platform for our company, and support our     gas pipeline laterals off of Guardian's pipeline to serve the
future progress in the regulated energy business at acceptable    Sheboygan/Plymouth and Green Bay/Denmark areas. This is
levels of risk.                                                   another solid utility investment that we expect will provide
                                                                  benefits for customers--adequate supplies for future demand,
                                                                  better access to competitive natural gas, improved reliability,

                                                                                           
and support for economic growth and business expansion
                                                                    See the Energy in Financing
in northeastern Wisconsin.
                                                                    Our Business
See the Energy in Growing                                           You could see the energy in our finance area this past year as
Nonregulated Businesses                                             we completed several major financing activities. In May 2006,
                                                                    we settled our forward equity agreement by issuing 2.7 million
You can see the energy at Integrys Energy Services, Inc.,
                                                                    shares of common stock and receiving $139.6 million in
our nonregulated energy supply and services company, as
                                                                    proceeds. In addition, shareholders invested more than
it continues to grow while managing risk. Integrys Energy
                                                                    $22 million in Integrys Energy Group's Stock Investment
Services increased its retail electric support capability,
                                                                    Plan in 2006 through optional cash payments and
including the energy pricing, energy supply, and customer
                                                                    reinvestment of dividends.
acquisition functions. Its indirect sales channel has been
strengthened to expand its reach to customers. It opened            Our finance team also arranged a five-year unsecured
a new office in Houston, Texas, to serve retail electric            revolving credit agreement for $500 million for Integrys
customers and expanded operations in the retail electric            Energy Group, bringing our total short-term borrowing
sector in Illinois, as that state implemented full retail           capacity at year end to $1 billion for Integrys Energy Group.
competition effective January 2007. Integrys Energy Services
                                                                    In April 2006, Integrys Energy Services entered into a
renewed the contracts for standard offer services in northern
                                                                    $150 million, 364-day credit agreement to finance its
Maine and continued its growth in the Canadian markets.
                                                                    margin requirements. As of December 31, 2006, the entire
It also opened a new office in Denver, Colorado, with a
                                                                    $150 million available under the credit agreement was
talented team having substantial expertise in the energy
                                                                    utilized by Integrys Energy Services.
markets, and it is in the process of integrating with Peoples
Energy's nonregulated business.                                     We also completed debt financings in 2006, including
                                                                    $300 million of junior subordinated debt for Integrys Energy
See the Energy in Asset, Portfolio, and                             Group. This is a hybrid instrument and treated by the credit
Risk Management                                                     rating agencies as 50 percent debt and 50 percent equity. In
                                                                    2006, we also issued 30-year bonds totaling $125 million for
The success of any business depends on having the right
                                                                    Wisconsin Public Service and an additional $22 million to
assets at the right time and the courage to divest of assets or
                                                                    refund existing Weston tax-exempt debt for a better interest rate.
invest in others as required. We have been actively assessing
the performance of our assets, forecasting future markets,
                                                                    See the Energy in Our
and making adjustments when necessary.
                                                                    Regulatory Relations
Integrys Energy Services has been at the forefront of asset
                                                                    Our relations with the regulatory agencies are key to our
management, for example, with the sale of the Kimball natural
                                                                    success. We strive for openness, honesty, mutual respect, and
gas storage facility in April 2006, the sale of the Sunbury power
                                                                    integrity in these relationships, both with the agencies' staffs and
plant in July 2006, and the sale of the Niagara Falls generation
                                                                    with the commissioners. This formula has been effective in
facility in January 2007. Wisconsin Public Service announced
                                                                    obtaining rate relief when needed and providing reasonable
the shutdown of two aging coal-fired units at the Pulliam
                                                                    rates for customers and reasonable returns for our shareholders.
generating station, and Integrys Energy Group adjusted its
portfolio of investments with the sale of its interest in           In 2006, we obtained rate relief in both Wisconsin and Michigan.
Guardian Pipeline, LLC.
                                                                      The Public Service Commission of Wisconsin granted
In our continued effort to focus on our rigorous asset                approval for Wisconsin Public Service to increase its retail
management strategy and to remain true to our efforts to              electric rates by $79.9 million (10.1 percent) and its retail
focus on our core competencies, we have announced that                natural gas rates by $7.2 million (1.1 percent) effective
we will divest of Peoples Energy Production Company,                  January 1, 2006. The 2006 rates reflected an 11 percent
which was acquired through our merger with Peoples                    return on common equity with a 59.7 percent targeted
Energy Corporation. This divestiture will lower our business          regulatory common equity ratio.
risk profile, refine our focus, and provide funds to reduce
debt and run our business. We anticipate the divestiture              In addition, on January 12, 2007, the Public Service
should be completed by the end of 2007.                               Commission of Wisconsin granted approval for Wisconsin
                                                                      Public Service to increase its retail electric rates by
These asset and portfolio adjustments are designed to
                                                                      $56.7 million, or 6.6 percent, and its retail natural gas
improve returns to investors and lower our risk profile.
                                                                      rates by $18.9 million, or 3.8 percent. These latest rates
                                                                      reflect a 10.9 percent return on common equity with a
                                                                      57.46 percent targeted regulatory common equity ratio.


  The Michigan Public Service Commission granted approval
                                                                   See the Energy in Our Efforts to
  for Upper Peninsula Power to increase its retail electric
                                                                   Reduce Costs, Improve Efficiency,
  rates by $3.8 million (4.8 percent) effective June 28, 2006.
  The rates reflect a 10.75 percent return on common equity
                                                                   and Lower Risk
  and a 54.9 percent common equity ratio.
                                                                   Our mission is "to provide customers with the best value in
We filed for rate relief in Illinois on March 9, 2007.             energy and related services." This mission requires us to
                                                                   operate efficiently and effectively to reduce costs while
  The Peoples Gas Light and Coke Company filed with the
                                                                   lowering our risk profile.
  Illinois Commerce Commission to increase its base rates
  for natural gas customers by $102.5 million in additional        Employees have exerted a great deal of energy to fulfill
  annual revenue. The proposed increase reflects an                this mission, and we have made substantial progress. Our
  11.06 percent return on common equity and a 56 percent           Competitive Excellence initiative uses "lean enterprise" tools to
  common equity ratio.                                             eliminate the costs of non-value-added activities. This initiative
                                                                   started in 2005, and we have catalogued over $18 million in
  North Shore Gas Company filed with the Illinois Commerce
                                                                   cost-avoidance and cost-reduction outcomes from teams
  Commission to increase its base rates for natural gas
                                                                   engaged in this activity. At the same time, our dedication to
  customers by $6.3 million in additional annual revenue.
                                                                   formal project management tools has enabled us to better
  The proposed increase reflects an 11.06 percent return on
                                                                   manage the costs of our approximately $1 billion construction
  common equity and a 56 percent common equity ratio.
                                                                   expenditure program for the period 2007 through 2009 and the
                                                                   integration of our new subsidiaries and assets.
The rate case approval process in Illinois requires receipt of a
written order from the Illinois Commerce Commission within
                                                                   
11 months from the date of filing, which would be during the
first quarter of 2008.
                                                                   to the Challenges We Face
                                                                   Challenges to our success continue to materialize, and you
See the Energy in Dividends for                                    can see the energy employees bring to bear to confront and
Shareholders, Strong Credit Ratings,                               overcome these challenges. One of these challenges is the rising
                                                                   cost of energy for customers. Consumers across the country
and Excellent Governance Ratings                                   have experienced price increases for nearly all energy products,
Shareholders can definitely see the energy each quarter as we      including electricity, natural gas, and gasoline. We take our
deliver dividends. Integrys Energy Group once again increased      responsibility to protect our customers from price volatility and
its quarterly dividend to $0.575 in September 2006 and raised      price increases very seriously. We have taken steps to mitigate
its dividend to $0.66 starting February 21, 2007. This is the      the impact on customers by adjusting our fuel portfolio,
48th consecutive year of dividend increases for our corporation    negotiating with suppliers, and expanding our capability to
and the 66th consecutive year of paying dividends.                 hedge against the unpredictable. We cannot control the
                                                                   national policies and markets that cause price pressure and
Integrys Energy Group's current corporate credit rating from
                                                                   volatility, but we are steadfastly committed to do what we can
Standard & Poor's is A-. The senior unsecured debt rating from
                                                                   to protect our customers.
Moody's is at A3. We believe our credit ratings continue to be
among the best in the energy industry. The ratings allow us to     Another challenge that will become more acute in the next few
access commercial paper and long-term debt markets on              years is the availability of skilled and professional workers. As
favorable terms.                                                   any successful management team knows, employees with all
                                                                   their skills, knowledge, ideas, innovation, and creativity are the
Governance ratings have become increasingly important to
                                                                   life blood of our company and the foundation of our success. It
many investors. Institutional Shareholder Services (ISS) is
                                                                   is how we create value for you, our shareholders. We currently
a leader in the governance ratings area, and it reviews
governance policies at various corporations and provides
                                                                   Long-Term Financial Goals
guidance to institutional shareholders. As of February 1, 2007,
                                                                      Provide investors with a solid return on their investments.
ISS's corporate governance quotient indicated that Integrys
                                                                      Grow our earnings per share at 6 to 8 percent on an
Energy Group's governance policies are considered superior to
                                                                      average annualized basis.
86.9 percent of the Standard & Poor's 400 companies and
71.0 percent of the utility companies. Our governance ratings         Achieve 70 to 80 percent of our earnings from our
improved dramatically since ISS began its ratings service and         regulated energy supply and services company, leaving
we continue to give this area our attention.                          20 to 30 percent from our nonregulated energy supply
                                                                      and services company.
                                                                      Manage the risk profile of our business portfolio.
                                                                      Continue growth in the annual dividend payments.


                                                           have talented teams in all locations, but the availability of new
   Returns on Investment                                   talent is impacted by the shrinking pool of workers and fewer
                                                           enrollments in the science and technology curriculums across
                                                           the country. We have taken steps to expand and focus our
                                                           recruitment efforts and support industry efforts to encourage
                                                 $5.00
                                                           students to enroll in technical programs. We enlist your support
                                                           to help strengthen America's capability and talent pool.
                                                 $4.00

                                                           See the Energy in Our Commitment
                                                 $3.00
                                                           to the Environment
                                                           At Integrys Energy Group we are committed to protecting the
                                                 $2.00
                                                           environment. We take seriously our obligation to provide
                                                           stewardship of the natural resources you have entrusted to us,
                                                 $1.00
                                                           and we manage our operations to minimize the impact we
                                                           might have on the environment. Our new Weston 4 facility will
                                                 $0.00
                                                           be one of the most efficient generating units in the country with
                                                           state-of-the-art environmental controls. Weston 4 represents a
                                                           modernization of our fleet of generating plants that will allow us
                                                           to reduce the amount of emissions produced for each megawatt-
                                                           hour of electricity that we generate. Integrys Energy Group
                                                           expects to maintain or decrease the amount of greenhouse gases
                                                 $2.50
                                                           released per megawatt-hour generated, and supports research
                                                           and development initiatives that will enable further progress
                                                 $2.00
                                                           toward decreasing our carbon footprint.
                                                 $1.50
                                                           For more details on our environmental activities and results,
                                                           please see the Environmental Performance Report sent to
                                                 $1.00     shareholders along with this Annual Report.

                                                           See the Energy in Our Strategy
                                                 $0.50

                                                           This past year the energy in our strategy was clearly visible
                                                 $0.00
                                                           as we funded the construction of the Wausau, Wisconsin, to
                                                           Duluth, Minnesota, transmission line project and the Weston 4
                                                           generating unit. At the same time, we added approximately
                                                           1.4 million new natural gas customers in Michigan, Minnesota,
                                                           and Illinois, creating a larger and more diversified utility
                                                           business. These strategic movements enabled us to realize
                                                 $350.00
                                                           synergies, grow the company, and provide a platform for
                                                           our future success.
                                                 $280.00

                                                           There is additional energy in our strategy for the future, but
                                                           the basic elements of our strategy have not changed greatly:
                                                 $210.00


                                                             We will continue to invest in quality utility operations;
                                                 $140.00

                                                             We plan to prudently expand the nonregulated business
                                                             activity of Integrys Energy Services;
                                                 $70.00


                                                             We will continue to manage our portfolio of energy and
                                                 $0.00
                                                             energy-related investments to provide targeted returns
                                                             from all assets and control our risk profile;

                                                             We will continue our focus on financial strength with a
                                                             strong balance sheet, quality credit ratings, and solid
                                                             returns for shareholders; and

                                                             We will continue our focus on reducing costs, improving
                                                             efficiency, and lowering risks.


          In 2006, the efforts of our management team and the
See the Energy in Our Leadership                                    employees who support them resulted in another successful
With the completion of our merger with Peoples Energy, the          year. My thanks to all employees for their role in completing
Integrys Energy Group Board of Directors was expanded to            three successful combinations and providing value to our
include, in addition to the nine continuing directors from          shareholders, customers, and communities. I'm proud to be
WPS Resources Corporation, seven directors from Peoples             a part of the Integrys Energy Group team.
Energy. They include:
                                                                    See the Energy Others Recognize
Keith E. Bailey, James R. Boris (non-executive
                                                                    Accomplishments can be their own reward, but it is also
chairman), William J. Brodsky, Pastora San Juan Cafferty,
                                                                    satisfying when others recognize accomplishments. In 2006,
Diana S. Ferguson, John W. Higgins, and Michael E. Lavin.
                                                                    we received much recognition.
I welcome them and their insights to the Board of Directors.
                                                                    Recognition for outstanding service to customers is always
Energetic, skilled, and diligent employees are key to our
                                                                    the most satisfying for me. I'm proud to say that we have,
success, but we must also have leaders with the same attributes
                                                                    for many years, received great marks for customer service
to guide our efforts. We are fortunate to have very capable
                                                                    from our regulated and nonregulated customers.
leaders throughout our organization, and we will continue to
invest in their development and careers. The members of the
                                                                    For the first time in recent years, we received national
Integrys Energy Group Management Team are:
                                                                    recognition. Forbes magazine designated Integrys Energy
                                                                    Group (formerly WPS Resources) as the utility industry's
  Larry Borgard  President and Chief Operating Officer,
                                                                    "Best Managed Company in America" in its January 9, 2006,
  Integrys Gas Group
                                                                    issue. Fortune magazine designated Integrys Energy Group
  Chuck Cloninger  President, Minnesota Energy                     (formerly WPS Resources) as the most admired energy
  Resources Corporation                                             company on its prestigious 2006 list of "America's Most
                                                                    Admired Companies" in its March 6, 2006, issue. And
  Gary Erickson  President, Michigan Gas Utilities Corporation
                                                                    Fortune again recognized us when it named us second on that
  Diane Ford  Vice President and Corporate Controller              same list of "America's Most Admired Companies," which
                                                                    will be in its March 19, 2007, issue. We were also one of ten
  Brad Johnson  Vice President and Treasurer
                                                                    finalists for Platts 250 Global Power Companies of the year.
  Peter Kauffman  Secretary and Chief Governance Officer
                                                                    This year, the MastioGale annual survey ranked Integrys
                                                                    Energy Services (formerly WPS Energy Services) number two
  Tom Meinz  Executive Vice President - External Affairs
                                                                    in its customer valued index, fourth in overall customer
  Phil Mikulsky  Executive Vice President and                      satisfaction, and fourth in the regional marketer category.
  Chief Development Officer                                         J.D. Powers ranked Wisconsin Public Service an "All Time
                                                                    Best Residential Electric Performer," ranked it fourth out of
  Steve Nance  President, Peoples Energy Production Company
                                                                    19 Midwest utilities in its 2006 electric study, and fourth out of
  Tom Nardi  President, Integrys Business Support                  20 Midwest utilities in its 2006 natural gas study.

  Joe O'Leary  Senior Vice President and Chief Financial Officer   You can see the energy in everything we do at Integrys Energy
                                                                    Group. You can see the energy in our employees as they work
  Mark Radtke  President, Integrys Energy Services, Inc.
                                                                    diligently to create a premier energy company that provides
                                                                    customers with the best value in energy services, shareholders
  Desiree Rogers  President, The Peoples Gas Light and
                                                                    with attractive returns and safety of their investments, and
  Coke Company and President, North Shore Gas Company
                                                                    communities with a partner.
  Charlie Schrock  President, Wisconsin Public
                                                                    Thank you for your investment in our company and for the
  Service Corporation
                                                                    trust you have placed in us. We will protect your investment
  Bud Treml  Senior Vice President and Chief Human                 as our own, and will continue to earn your trust.
  Resource Officer
                                                                    With Warmest Regards,
We also had many new employees joining our ranks from
Michigan Gas Utilities, Minnesota Energy Resources, and
Peoples Energy. I welcome each of them to the Integrys Energy
Group team. Our success is built around people, and they will
all help our organization grow, become stronger, and realize        Larry L. Weyers
our new vision of "People Creating a Premier and Growing            President and Chief Executive Officer
Energy Company."
                                                                    March 9, 2007


