A Letter from the Executive Chair of the Board
The 16 million people whom PG&E* serves count on us each and every day to provide energy that is safe, reliable,
affordable and clean. Just as important, they look to us to prepare for tomorrow, with a focus on developing smart,
sustainable long-term energy solutions that will ensure our communities continue to prosper and thrive.
PG&E�s accomplishments in 2016 show we are committed to meeting these crucial challenges.
We built upon our industry-leading progress on safety, earning an unprecedented number of independent certifications
for the quality and integrity of our gas operations � recognition we welcomed with the understanding that our safety
work is never finished, and there is always more to do. We recorded the second-best performance for electric system
reliability in our entire history, despite a winter of exceptionally powerful storms. And our customer satisfaction scores
climbed to the highest level in years.
At the same time, we continued to set the pace in providing power that is among the cleanest in the nation. Nearly
one-third of the electricity we delivered in 2016 came from state-qualified renewable resources � four years ahead
of California�s schedule. Even more significant, nearly 70 percent of PG&E�s electricity mix now comes from sources
that emit no greenhouse gases.
We also delivered solid financial results for the year and announced our first dividend increase in six years � a step
that will help enable PG&E to compete more efficiently and affordably for capital to fund further investments in safety,
reliability and clean energy in the years ahead.
Looking forward, we are focused on three major areas: further strengthening our operational excellence, above all on
safety; remaining our customers� provider of choice as they consider a growing array of energy options; and positioning
PG&E for the future as California transitions to a clean energy economy.
We recognize that our customers� needs and expectations are evolving faster today than ever before. We also recognize
that California�s clean energy goals, which PG&E firmly supports, will continue to drive large-scale transformation in
the state�s energy sector, and that PG&E will play a vital role.
Our customers, many of whom pride themselves on their environmental stewardship, are already leading the way. PG&E
has now connected nearly 300,000 private rooftop solar arrays � or 25 percent of the total for the entire country. The
same is true for transportation, with PG&E�s grid supporting one in five of the nation�s electric vehicles.
Meanwhile, state leaders continue to assert California�s policy leadership on climate change and the State�s resolve to
dramatically reduce greenhouse gas emissions.
We see these shifts in our sector as an opportunity. Achieving California�s vision will require sustained and smart
investments. It will require innovation. And it will require working in close partnership with others at the local, state
and national levels who can help us achieve our shared aims.
Last year�s landmark agreement to phase out PG&E�s Diablo Canyon nuclear plant by 2025 is a prime example. The joint
proposal we submitted to the California Public Utilities Commission, in partnership with key labor and environmental
partners, will increase investment in energy efficiency and renewables beyond the current state mandates. The proposal
includes PG&E�s voluntary commitment to a 55 percent renewable energy target in 2031, an unprecedented pledge
by a major U.S. energy company.
PG&E is now making new capital investments at a pace of approximately $6 billion per year as we continue to modernize
our system, integrate renewables and other distributed energy resources, and take advantage of new technologies
coming out of Silicon Valley and elsewhere.
One pivotal development came in December, when our state regulator authorized PG&E to invest $130 million over
the next three years to install the infrastructure for 7,500 public charging stations to support the growth of electric
vehicles across our service area.
Combined with our improvements in safety and reliability over the last six years, such investments have put PG&E in a
position to produce tangible value for customers and shareholders. Even so, we understand that additional investments
come at a cost, and that to be successful we must keep our service affordable.
Over the last few years, we�ve reduced PG&E�s cost structure in a number of areas. This January, we announced steps
that will deliver further savings in 2017. Going forward, we are determined to find additional opportunities to reduce
costs, while never compromising on safety.
iii
A Letter from the Executive Chair of the Board (continued)
The vital importance of our safety commitment was underscored last year by the August verdict that found Pacific
Gas and Electric Company guilty of 6 of the 12 charges brought by the federal government in a case stemming from
the 2010 gas pipeline explosion in San Bruno: the convictions included one count of obstructing a federal agency
proceeding and five counts of violating federal pipeline safety regulations.
Even though it involved events of the past, this was a painful verdict. Ultimately, we made the choice not appeal the
jury�s decision, concluding that was the right outcome for everyone.
Nothing can ever give back the San Bruno victims and their families what they lost. The best way we can honor them
is to demonstrate, through our actions, an unwavering dedication to the safety of the public, our employees and
contractors.
With that in mind, we will be focused on fulfilling the terms of the probation and working with a court-appointed
outside monitor to become the safest, most reliable energy company in the country.
I have no doubts about what PG&E can achieve in the years to come. I�m also convinced that we have the leadership
that future will require.
On March 1, 2017, Geisha Williams, who most recently served as President, Electric of Pacific Gas and Electric Company,
succeeded me as CEO and President of PG&E Corporation. Nick Stavropoulos, formerly President, Gas of Pacific Gas
and Electric Company, became President and Chief Operating Officer of Pacific Gas and Electric Company. Together,
they have more than six decades of experience in our business.
Having worked alongside Geisha and Nick for many years and knowing their record of accomplishment, I have every
confidence in PG&E�s continued success under their leadership. I look forward to continuing to work with them in my
new role as Executive Chair of the Board of PG&E Corporation.
As I step aside after six years as CEO and President of PG&E Corporation, I am grateful to our 23,000 employees for
the strides they have made and I am proud of the direction we�ve set.
Thank you also to our customers, investors and other stakeholders for following PG&E�s progress and for your support
as we work together to build a better California.
Sincerely,
Anthony F. Earley, Jr.
Executive Chair of the Board
PG&E Corporation