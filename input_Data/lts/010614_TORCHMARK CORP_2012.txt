LETTER to Shareholders*
Before we talk about Torchmarks operations, wed
like to make a few comments about our Chairman,
Mark McAndrew, who stepped down as CEO
last June after over 30 years of service to the
Torchmark companies.
Mark navigated Torchmark through one of the most
difficult economic environments in modern history.
We emerged from the financial crisis stronger
than before without government assistance and
without issuing equity. Weve both worked with
Mark for over 26 years and have always admired his
remarkable ability to quickly sort through complex
issues and develop innovative solutions.
Mark ran the Company with integrity, always making
decisions with the best interests of Torchmarks
shareholders, customers and employees in mind.
His contributions as Chief Executive Officer will
benefit Torchmark for years to come.
Leadership Structure and Philosophy
When Mark stepped down as CEO on June 1,
2012, we were appointed co-CEOs. Were often
asked why Torchmark chose a co-CEO structure.
While well admit it is unusual, we believe the
arrangement works best for Torchmark now
because of the unique relationship that we have.
We have worked closely together at Torchmark
and all the insurance subsidiaries for over 26 years.
Our complementary skill sets and our trust in each
other provide a depth to the position that wouldnt
exist with a single CEO.
The co-CEO arrangement facilitates a sound
collective decision-making process and also
allows us the flexibility to more easily oversee daily
operations and plan for the future.
We both believe in the Torchmark business model
- selling simple, protection-oriented products to
middle-income families through captive distribution
with products that generate strong margins
and high cash flow, minimizing administrative
expenses, maintaining a conservative investment
approach, and managing our capital to maximize
shareholder value.
Our target market is vastly underserved and
provides significant opportunity for growth as has
been evidenced by the Companys performance in
recent years.
Due to our focus on stable, protection-oriented
products and conservative management of
investments, Torchmark has delivered consistent
growth.

As you can see, net operating income per share
has increased steadily over the last ten years.
The compound annual growth rate over the past
ten years was 8.3% despite the recent financial
crisis. For the last five years, the compound annual
growth rate was 8.7%. For 2012, we grew net
operating income per share by 15.1%, our best
year in the past fourteen years.
Book value per share has also grown steadily.
Excluding net unrealized gains or losses on our
fixed maturities, our book value per share grew
at a compound annual growth rate of 9.7% over
the last five years, while our reported GAAP value,
which includes net unrealized gains or losses, grew
at a compound annual growth rate of 16.1% over
the past five years.

2012 Highlights
2012 was a very good year for Torchmark.
Highlights include:
yyNet operating income per share grew 15%.
yyAmerican Income life sales grew 12%, while the
agent count increased 18%.
yyDirect Response life sales grew 3% despite a
difficult economy.
yyLiberty National began to turn around:
??Agent count and sales grew steadily
beginning in February, and
??Life underwriting margin increased from
22% of premium to 26%.
yyFamily Heritage Life Insurance Company of
America was acquired without restricting future
buybacks or mergers and acquisitions activity.
yyWe refinanced debt on favorable terms.
yyWe repurchased 7.5 million shares or 7.4% of
the outstanding Torchmark shares.
All in all, a very good year for Torchmark.

The largest component of net operating income
is underwriting income. Underwriting income is
about 70% of our pre-tax operating income.

Life underwriting income is the largest component
of underwriting income. It produces 72% of our
underwriting income, while health, excluding
Medicare Part D, produces 22%. We focus on
life insurance as it has higher margins, generates
significant investment income and is less
competitive and regulated than health insurance

American Income Life is our largest and most
profitable distribution channel.

As you can see in this chart, life premiums at
American Income have steadily increased at a
10-year compound annual growth rate of 9.1%.
Underwriting margins as a percentage of premium
have consistently ranged from 30-33% before
administrative expenses.
2012 was a great year for American Income. Life
sales were up 12% over 2011, and agent count was
up 18% over a year ago. We continue to focus
on developing middle management to ensure
sustainable agency growth. We also continue to
refine our agent training programs and financial
incentives.
Our Laptop Sales Presentation program provides
us with a wealth of valuable data we can use to
manage the agency force. This data allows us
to monitor and break down agency activity at all
levels of the organization: by individual agent, by
individual managers, by region, etc.
American Incomes labor union affinity and
underserved target market allow it to operate in a
niche that provides plentiful opportunity for future
growth. We expect to see strong net life sales
growth throughout 2013, beginning with low to
mid-single-digit growth through the first quarter
and then rising to 10-14% for the entire year.

Our second largest distribution channel is Globe
Life Direct Response. As you can see in the chart
below, Direct Response life premiums have grown
at a 10-year compound annual growth rate of 7.1%.

Our basic strategy is to continually search for new
ways to reach our market, utilizing an innovative
approach that is constantly evolving. Over the past
few years, Internet marketing and our inbound call
center have been our fastest growing sources
of new production. Five years ago, Internet
marketing and inbound calls produced about 5% of
Direct Responses new business. Today, Internet
marketing and inbound calls produce approximately
40% of our Direct Response new business.
Despite a difficult economy, Direct Response life
sales grew 3% in 2012. We expect sales to be
relatively flat through the first quarter, but we are

optimistic that the initiatives we are putting in place
throughout 2013 will increase response rates,
resulting in mid-single-digit sales growth for the
full year.
Furthermore, the Postal Services recently
announced plan to discontinue Saturday mail
delivery is not expected to have any impact on our
direct mail business.

Unlike American Income and Globe Life,
Liberty National has a recent history of slightly
declining premiums.

Several years ago we began to convert Liberty
from a fixed cost model to a variable cost
model. We accelerated this process and began a
turnaround program late in 2011, when we made a
management change at Liberty along with several
other significant changes, including:
yyOffice operating expenses are now the
responsibility of branch managers, rather than
the home office.
yyAll new agents are hired as independent
contractors rather than employees.
yyWe added a new layer of middle management.
yyWe cut a significant portion of non-deferrable
acquisition costs.
While this is a significant change in the culture of
this distribution system, we knew these changes
were necessary to produce acceptable profit
margins on the business we write and to put
Liberty in a position to grow going forward.
The changes we made are paying off as we had
steady sequential gains in both sales and agent
count in 2012 beginning in mid-February, and our
life underwriting margin as a percent of premium
has increased from 22% to 26%.

We are excited about the acquisition of Family
Heritage Life. This is the kind of company we
have been looking to acquire  a company selling
basic protection insurance to middle-income
families through a captive agency. Their offering
of supplemental health policies with a cash back
feature, such as return of premium, in non-urban
areas gives them a unique operating niche.
The integration of the Family Heritage Life operation
has gone very smoothly so far. We believe there
is potential for strong long-term sales growth
through geographic expansion and integration of
Torchmarks agent recruiting techniques.
This acquisition will not restrict future share
repurchases or mergers and acquisitions activity.

The second major component of net operating
income is excess investment income. In 2012,
it was $237 million or about 30% of pre-tax
operating income.
Excess investment income is net investment
income less the required interest on the net
policy liabilities and the interest on our debt. The
primary component is the investment income
earned on our investment portfolio, which totaled
$12 billion at December 31, 2012 on an amortized
cost basis.

Over the years, we have followed a conservative
long-term investment strategy. With the
high underwriting margins generated by our
insurance products we dont have to stretch for
investment yield.
As shown in the chart below, 96% of our investment
portfolio consists of fixed maturities. These
assets consist primarily of long-term, investment
grade corporate bonds. We have no commercial
mortgage-backed securities or securities backed
by subprime or Alt-A mortgages.

We invest in long term, fixed-rate assets because
they provide the best match for the long duration
and fixed-rate nature of our policy liabilities.
Of the $12 billion of fixed maturities, $11.4 billion
are investment grade, and $585 million are below
investment grade.

The percentage of below investment grade (BIG)
bonds to total fixed maturities is 4.9% compared to
6.4% a year ago and higher levels in prior years.
At the current level, with our relatively low portfolio
leverage, the percentage of BIG bonds to equity,
excluding net unrealized gains on fixed maturities is
17%, which is less than most of our peers.
Overall, the portfolio is rated A-.
Low Interest Rate Environment
There has been much discussion and concern
recently about a prolonged low interest rate
environment and the impact it would have on
insurance companies. While lower interest rates
pressure the net investment income of all life
companies, we believe we face less exposure
to a lower for longer environment than most of
our peers.
As long as we are in this low interest rate
environment the portfolio yield will continue to
decline, and thus pressure excess investment
income.
However, the impact on Torchmark will be
diminished by the fact that on average, only 2% 
3% of fixed maturities will run off each year over
the next five years.
To quantify the potential impact of an extended
low interest rate environment, we performed
stress tests assuming new money rates of 4.25%
and 4.00% for the next 5 years. These scenarios
result in a portfolio yield of about 5.50% to 5.55%
at the end of 2017.
At these rates, we would still earn a small spread
on the net policy liabilities while earning the full
550-555 basis points on our equity. In either
scenario we will still generate substantial excess
investment income.
Although an extended low interest rate environment
pressures our investment income, it does not pose
a threat to our balance sheet. Since we primarily
sell non-interest sensitive protection products,
we dont see a reasonable scenario that would
require us to write off deferred acquisition costs
or put up additional GAAP reserves due to interest
rate fluctuations. In addition, we do not foresee a
negative impact on our statutory balance sheet, as
the results of our cash flow testing indicate that our
reserves are more than adequate to compensate
for lower interest rates.
Although we dont prefer a lower for longer
interest rate environment, we feel that it will have
less impact on us than many of our peers.
With high underwriting margins, we generate
significant underwriting income and dont have
to rely on excess investment income to generate
positive operating earnings.
In addition, the stress test results give us
confidence that we can maintain or grow the
current level of excess investment income per
share in an extended low rate environment.
That said, we strongly prefer higher interest rates.
Because of our product profile and our strong,
consistent cash flow, we would benefit from a
spike in rates. We would not be concerned about
the resulting unrealized losses in the investment
portfolio because we have the intent and more
importantly, the ability, to hold our investments
to maturity.

We define free cash flow as the cash that is
available to the parent company from the annual
dividends received from the insurance subsidiaries
less the interest expense on our debt and less the
dividends paid to Torchmark shareholders. The net
amount left over is free cash that can be used
for any corporate purpose.
Because of the products we offer and our high
underwriting margins, we have a large, stable block
of policies in force that consistently generates
substantial free cash flow year after year.
As you can see in the previous chart, we generated
strong free cash flow even at the height of the
financial crisis. In 2013, we expect to generate
free cash of around $355 to $365 million

On an ongoing basis, we evaluate alternative uses
of our free cash, but share buybacks have generally
been the most efficient use.
We began our share repurchase program in
1986, and have purchased Torchmark shares
in all years since then, except 1995 after we
acquired American Income Life. In the last 27
years, we have repurchased 74% of the Companys
outstanding shares.
As mentioned, we expect to generate $355-365
million of free cash in 2013. If market conditions
are favorable, we plan to use most, if not all of this
cash for share repurchases.

For over 25 years, Torchmarks management and
board of directors have agreed on the importance
of distributing excess capital to the shareholders.
We have accomplished this through share
repurchases and dividends, with an obvious
emphasis on share repurchases.
As a result, Torchmark has consistently distributed
a large portion of earnings to our shareholders. In
fact, in the last 10 years, Torchmark distributed
81% of net income our shareholders.
Maintaining our cash flow and returning a
substantial portion of earnings to shareholders
will continue to be an important part of our
business model.
Summary - What Does Torchmark
Have to Offer Shareholders
Torchmarks growth potential  While many
people see life insurance as a mature industry,
Torchmark operates in a vastly underserved
market with little competition where the majority
of individuals either have no life insurance or do
not have enough life insurance. Our extensive
experience along with our ability to control costs
allows us to operate effectively in the middleincome
market. And with the acquisition of Family
Heritage, we expect to reverse the recent trend of
declining health sales.
Our high underwriting margins  Our
underwriting margins are among the highest in
the industry. We dont have to rely on investment
income to generate profits.
Our conservative investment philosophy -
Because of our high underwriting margins, we
can generate strong profits without having to take
significant investment risks.
A sustainable mid-double digit ROE - Our
return on equity excluding net unrealized gains on
our fixed maturities was 15.5% in 2012, and we
expect to maintain ROE at around the 15% level.
A safe haven in a low interest rate environment -
We are much less exposed to the low interest rate
environment than most of our peers. We are very
confident that it poses no threat to our balance
sheet and we expect to grow net operating income
per share close to 10% per year even in a low
interest rate environment.
Our insurance operations are relatively
immune to the economy - While low interest rates
impact our net investment income, our insurance
operations have always proven relatively immune
to swings in the economy. Even though sales
can be more challenging in a difficult economy,
our persistency has never been impacted and our
earnings are driven by our inforce block

We have strong reliable free cash flow  Our
statutory earnings are driven by our large, stable
inforce block. Even if we had no sales in a given
year, we would still expect to generate over $300
million dollars of free cash flow.
Our return of cash to shareholders  Torchmark
has a long history of using free cash flow to
repurchase stock. Since 1986, we have repurchased
74% of our stock. We expect that the majority, if
not all, of our free cash flow in 2013 will be used
to repurchase shares as long as market conditions
are favorable.
In summary, we are very excited about Torchmarks
prospects and expect 2013 to be another good
year. Thank you for your investment in Torchmark.
Larr y Hutchison
Co-Chief Executive Officer
Gary Coleman
Co-Chief Executive Officer