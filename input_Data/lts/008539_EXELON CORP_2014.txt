Exelon 2014 Summary Annual Report                            Le er to Our Shareholders




Dear Exelon Shareholder,

Exelon Corporation had a
strong year in 2014. We took
significant steps to diversify
and grow the business
through investing in utility
infrastructure and technology,
building new generating
plants, expanding existing
lines of business and moving
into new technologies and
markets. Our core operations
remain excellent, and we
delivered top quartile utility
reliability and best-ever
nuclear generation.
                                    Christopher M. Crane
                                    President and Chief Executive O cer




                                                                                    |3
Exelon 2014 Summary Annual Report                                                                                       Le er to Our Shareholders




A transformation is occurring in our industry, with dramatic                          Operations
changes in consumer behavior, emergence of new technologies
                                                                                      Exelon's operating performance in 2014 remained very strong.
at an unprecedented pace, and continuing industry consolidation.
In this complex environment, Exelon's integrated business model                       Our ability to leverage expertise and experience across the
and our unmatched presence across the energy value chain o er                         three utilities was demonstrated by outstanding performance
a distinct competitive advantage through which we will drive                          in safety and reliability. Each of our three utilities was in the
value for customers and shareholders.                                                 top decile of its peer group for safety, and top quartile for
                                                                                      both outage frequency and duration. Each utility set new
Financial Performance                                                                 winter peak demand records during the polar vortex, and met
Our GAAP earnings were $1.89 per diluted share in 2014,                               that demand successfully.
compared to $2.00 in 2013. On an operating (non-GAAP) basis,                          Generating plant performance in 2014 was exceptional. Exelon
our earnings were $2.39 per diluted share, compared to $2.50                          Power closed the year with 96.5 percent gas and hydro dispatch
in 2013. The overall $0.11 decrease in non-GAAP operating                             match and 95 percent wind and solar energy capture. Nuclear
earnings primarily reflects lower realized energy prices at the                       achieved a 94.2 percent capacity factor in 2014. The three
generation business and increased storm response costs at the                         nuclear plants of CENG, our joint venture with lectricit de
utilities. These were o set in part by increased utility distribution                 France, also operated very well at a 92.6 percent capacity
rates and reduced post-retirement benefit costs. GAAP earnings                        factor, and were fully integrated into Exelon fleet management
in 2014 also reflect impairments of certain generating assets                         this year. The Exelon generation fleet, with its mix of nuclear,
given 2014 commodity price declines, partly o set by gains on                         gas, hydro, solar, and wind assets, is more than twice as clean
the sales of some generating assets.                                                  on a low-carbon basis as any of the 20 largest generation
Even though natural gas forward prices remained low, Exelon's                         fleets in the country1, and provides unparalleled reliability to
financial results were consistent with our plan. Exelon's share                       its customers in all seasons.
price was up 35.4 percent for the year, with total shareholder
return of +40.6 percent compared to an average return of +29
percent for our peers, and +13.7 percent for the broader market.

We continued the work we began in 2012 to strengthen our
balance sheet. We sold several generation assets this year, in
part to finance the acquisition of Pepco Holdings and recognize
the increased value those assets had to other market participants
relative to our own estimates. We are reinvesting that capital
in growth.




Each of our three utilities was in the top decile of its
peer group for safety, and top quartile for both outage
frequency and duration. Generating plant performance
in 2014 was exceptional.

1
    M.J. Bradley & Assoc. (2014), Benchmarking Air Emissions of the 100 Largest Electric Power Producers in the U.S.


                                                                                                                                                     |4
Exelon 2014 Summary Annual Report                                                                        Le er to Our Shareholders




A Culture of Innovation
To thrive in an industry undergoing fundamental change, Exelon        Exelon is committed to its pursuit of growth opportunities,
is building a culture of innovation that promotes investigation and   especially in our approach to new technologies. Our objective
early adoption of promising new technologies and processes.           is to identify the opportunities for both Exelon and its customers
We intend to identify technologies that are about to emerge and       in what some view as "disruptive" technologies, thereby
capitalize on them, rather than merely react to the disruption.       enabling innovation and capitalizing on market and technical
We are investing in new markets, and we are empowering our            changes. Key examples of that focus include:
frontline workers to use their hands-on knowledge of the business
                                                                       Our partnership with Bloom Energy to finance fuel cell
to bring us innovation and creativity.
                                                                        projects at commercial facilities, to expand our service
                                                                        o erings to large customers and build on our growing
Growth Strategy and Investments
                                                                        distributed energy business;
Exelon's strategy is to preserve the value of its core assets while
capitalizing on emerging trends and technologies to diversify          Our pursuit of microgrid initiatives in both the competitive
into growth opportunities. We continue to create value for our          and regulated businesses  innovative projects that can
shareholders by finding and providing value for our customers.          identify and pilot new technologies, expand options available
We acted on our strategy in 2014 through expansion of existing          to customers, and establish sound frameworks for market
business lines, investment in utility infrastructure, investment in     participation and value capture;
new technologies and redeployment of capital for greater value.
                                                                       The company's investment in NetPower, a company building
The most significant strategic move of the year was the                 a first-of-a-kind demonstration natural gas-fired plant designed
announcement in April of our merger with Pepco Holdings,                to produce zero air emissions, with potential commercial
Inc. (PHI) The addition of the PHI utilities to our company will        applications for the captured carbon dioxide; and
create substantial value for customers, given our demonstrated
                                                                       Staying actively engaged in the emerging technology arena,
ability to leverage scale and e ciencies to improve service
                                                                        exploring and investing in such technologies as energy
reliability and reduce cost.
                                                                        storage, alternative vehicles and sophisticated digital systems
We continue to invest in and grow our existing businesses:              enabling customers to use energy more e ciently.
utilities, competitive retail and generation. Investment in
regulated utility advanced technology, infrastructure and
reliability constituted a key part of our growth in 2014 and will
continue. ComEd has made impressive progress on installation
                                                                        We acted on our strategy
of smart meters throughout its service territory, and PECO
and BGE are more than 80 percent complete on their smart
                                                                        in 2014 through expansion
meter projects. These investments provide greater reliability
and improved service for our customers and a stable return
                                                                        of existing business lines,
for the company.
                                                                        investment in utility
We added 215 megawatts (MW) of new generation to the portfolio
in 2014: nuclear uprates, wind projects and solar. The generation       infrastructure, investment
development pipeline is also robust with more than 2,100 MW in
our two ERCOT combined-cycle gas turbine projects, more than            in new technologies and
100 MW in near-term wind projects, 120 MW in the expansion of
our Perryman gas plant in Maryland, and other potential wind            redeployment of capital
and gas generation projects. We materially increased the size
of the competitive retail power and gas business through the            for greater value.
acquisitions of Integrys Energy Services and ETC ProLiance,
improving profitability while adding scale and providing a larger
channel to market for our generation.


                                                                                                                                      |5
Exelon 2014 Summary Annual Report                                                                         Le er to Our Shareholders




We are leveraging our competencies and existing assets to move
into adjacent markets. We are growing our distributed energy
                                                                         We materially increased
platform, including biomass, compressed natural gas fueling
stations and behind-the-meter solar, to take advantage of market
                                                                         the size of the competitive
opportunities and provide our customers with more options.
                                                                         retail power and gas
Energy Policy Advocacy
As is the case throughout the energy industry, federal and state
                                                                         business through the
policies and regulations are important to Exelon's business. For
that reason, Exelon is and will continue to be a leading voice in
                                                                         acquisitions of Integrys
the discussion and debate of energy public policy.
                                                                         Energy Services and ETC
A key policy focus remains ensuring a level playing field for our
clean, reliable generation, including nuclear. The U.S. Environmental    ProLiance, improving
Protection Agency's (EPA) Clean Power Plan draft was released
in June, proposing to restrict carbon dioxide emissions from             profitability while adding
fossil plants. The draft was well-intentioned but fell short of
addressing the importance of nuclear in achieving the nation's           scale and providing a
environmental goals. We are working to improve the plan as the
EPA finalizes it and states begin to consider how to implement it.       larger channel to market
As a result of widespread fossil outages and problems with fuel
supply during last year's polar vortex, PJM has developed a
                                                                         for our generation.
redesign of its capacity market to create a capacity performance
product that will recognize the value of reliable supply. Federal
Energy Regulatory Commission approval this summer would enable
                                                                         Conclusion
implementation in the next capacity auction. We are working with         Exelon remains the premier operator in our business, while
each of our regional transmission organizations to build market rules    we actively invest in new technologies and markets. We will
and practices that will ensure resource adequacy for our customers.      continue to diversify and grow our business with discipline,
                                                                         reliably serve our customers, and support energy policy that
Despite the outstanding performance of our nuclear fleet, some           ensures fair competition for our assets.
of our Illinois plants continue to face economic challenges,
because their clean energy and best-in-class reliability are not
properly valued in today's markets. Four State of Illinois agencies
have issued a report confirming the economic and environmental
benefits of the state's nuclear plants and identifying policy
options to address the issue. The right energy policy for Illinois
should guarantee reliability and improve the environment, while          Christopher M. Crane
creating and retaining jobs, growing the local economy, and              President and Chief Executive O cer
minimizing cost. It is di cult to envision such a policy without         April 2015
nuclear as a critical part of the energy mix.

Our forthcoming decisions on nuclear asset rationalization will be
driven largely by the outcome of the PJM capacity performance
reform, policy decisions in Illinois and other states, and the outlook
for EPA's Clean Power Plan. Putting in place the right public
policies is clearly the best answer. But our obligation to you,
our shareholders, remains the same: sustained profitability
from our assets while meeting our obligations to customers
and communities.

                                                                                                                                        |6
