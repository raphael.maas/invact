Dear fellow shareholders:
Our country began the past decade experiencing Y2K
and ended the decade recovering from a severe recession.
Range began the past decade as a small struggling
company and ended the decade as a financially strong,
prospering enterprise, whose stock was the third
best performing S&P 500 stock in its sector for the
last decade. During this time period, our production
volumes more than doubled, our proved reserves
quadrupled and total assets increased six-fold. Most
importantly, the beneficiaries were Ranges shareholders,
as our stock price increased by more than 2,000%!
This is extraordinary performance. While many factors
played a role in our success, upon reflection, the most
critical element was a highly talented, motivated
and loyal group of employees who worked diligently
toward a set of common goals each and every day. It is
the relentless and dedicated efforts of our terrific team
of employees that we truly thank and congratulate for
making the past decade such a rewarding and transformative
period for Range and its shareholders.
As we enter a new decade, we are uniquely positioned
to continue to build and grow our Company. Our mission
is simple  to continue to grow our production and
reserves at a top-quartile or better cost structure. While
our strategy is simple, it is difficult to execute. We begin
the new decade with 3.1 Tcfe of proved reserves and
2230 Tcfe of resource potential from our leasehold
position that covers 2.5 million net acres. Importantly,
our financial position is strong, as we maintained our
financial discipline in the chaos of 2009 by internally
funding our capital program, reducing our debt outstanding
and increasing the liquidity under our bank
credit facility to nearly a billion dollars.
Several years ago, we undertook an initiative to
methodically upgrade our property base. We focused
on developing higher growth, lower cost and economically
more attractive projects. As a result, our three
core projects are currently the Marcellus Shale, Barnett
Shale and the Nora Field area. All three of these areas
are low cost and generate attractive returns even at low
natural gas prices. While improving our property base,
we also began to divest of our lower growth, higher
cost properties. Selling these properties allows us to
reinvest the proceeds in our higher return projects,
lowers our cost structure and allows our technical
teams to focus on higher return activities. Recently, we
announced that we had entered into an agreement to
sell our Ohio tight gas sand properties for $330 million.
Assuming the Ohio sale closes as planned in late March,
Range will have sold approximately 5,700 wells since
the beginning of 2008. This represents roughly 50% of
our total well count, but only 10% of our production
and reserves.
Looking back at the past decade, clearly the most
important accomplishment was the discovery of the
Marcellus Shale in Appalachia. Since completing the
discovery well in 2004, we have accumulated 1.3 million
net acres in the play. Of these, 900,000 net acres
are high-graded, or considered highly prospective. We
have also assembled a 170+ person team in Pittsburgh
and have put in place much of the pipeline infrastructure
that will allow us to ramp up our Marcellus production.
By year-end 2009, we had drilled over 100 horizontal
wells in the Marcellus, and Ranges net production had
reached slightly more than 100 Mmcfe per day. Our
goal for 2010 is grow our Marcellus production to 180

to 200 Mmcfe per day and double that again in 2011.
Based on Ranges and the industrys results to date,
many now believe the Marcellus Shale play could
become one of the largest natural gas fields in the
world. To think that Range, a company with market
capitalization of only $78 million at the beginning of
the last decade, discovered such an enormous natural
gas field is an amazing accomplishment. While all of
us at Range are exceedingly proud of this achievement,
we are more excited about the impact it will have on
the value of Range. Importantly, the Marcellus Shale
will likely have a meaningful impact on the countrys
energy policy. With the Marcellus Shale play and the
other shale gas plays that have been discovered across
the U.S., our country has an opportunity to redefine
its energy policy by embracing natural gas. By using
natural gas to generate more of our electricity, heat
our homes and potentially fuel our vehicles, the U.S.
can reduce its dependence on foreign oil, dramatically
decrease carbon emissions and create hundreds of
thousands of jobs to help strengthen and expand our
economy. As we enter the new decade, the Marcellus
Shale and our other key projects are game changers
for Range. We now have one of the lowest cost structures
in the industry, so we can continue to generate
attractive returns on our capital and continue to grow
and expand our production and reserves. Having a
low cost structure, a strong financial position and a
top-tier organization equates to a very bright future.
In this report, we focus on the key projects that drive
this bright future. In addition to thanking all of the
Range employees, we wish to thank our fellow directors
for their wise counsel and most importantly, our
fellow shareholders for their steadfast support.

John H. Pinkerton
Chairman & Chief Executive Officer

jeffrey l. ventura
President & Chief Operating Officer