DEAR FELLOW SHAREHOLDERS
Eugene I. Lee, Jr.
President and Chief Executive Officer
Charles M. Sonsteby
Chairman of the Board
Darden has been on an incredible journey, and this year we were
able to accomplish great things by delivering outstanding experiences
every day, in every restaurant.
We know that when we focus on creating memorable moments for
our guests, we can grow our business. Thats why everything we do
is rooted in our mission  to be financially successful through great
people consistently delivering outstanding food, drinks and service in
an inviting atmosphere, making every guest loyal.
We bring our mission to life every day through our 150,000
team members in more than 1,500 restaurants across
the country who carry out the four pillars of our Back-to-
Basics operating philosophy  culinary innovation and
execution, attentive service, engaging atmospheres and
integrated marketing. To many, this may all sound simple,
but consistently doing the little things well to create
memorable moments for our guests is extremely difficult.
We continued to leverage our four competitive
advantages that are key to helping our businesses
drive sales growth and expand margins. They are:
 The significant scale of our Company
 The breadth and depth of our data and consumer insights
 Our commitment to rigorous strategic planning
 Our results-oriented, people culture
At a time when our industry faced considerable
headwinds  including increased and more diverse
competition  we are proud of the progress we have
made. Our businesses outperformed the industry1,
creating significant value for our shareholders, our guests,
our team members and the communities we serve.
Delivering Value
to Shareholders
In fiscal 2016, we grew total sales by 4.4 percent2
driven by same-restaurant sales growth of 3.3 percent 
exceeding the industry by more than 400 basis points1
while further simplifying our operations to reduce nonguest-
facing costs by approximately $95 million. Our
sales growth and cost management efforts resulted in
a 37.9 percent increase in adjusted diluted net earnings
per share to $3.53.3 On a reported basis, diluted
net earnings per share from continuing operations
increased 84.1 percent to $2.78 in fiscal 2016.
During the year, we completed a thorough review of our
strategic options to improve shareholder return resulting
in a transaction that was highly successful on many
fronts. We meaningfully improved our capital structure
by completing our comprehensive real estate strategy 
which included a tax-free spinoff of certain real estate and
restaurant assets into a new public company, Four Corners
Property Trust (FCPT). This strategy enabled us to reduce
our debt by approximately $1 billion while preserving the
Companys investment-grade credit profile. Given the
current trading multiples of both companies, we believe
we have created significant value for shareholders.
Continuing our commitment to enhancing shareholder
value, we returned more than $450 million through dividends
($265 million) and share repurchases ($185 million). As of the
end of the fiscal year, we had $315 million remaining of the
$500 million of share repurchases the Board authorized in
December. This June, we increased our quarterly dividend
to $0.56 per share. With this increase, the Darden dividend
after the real estate spinoff will be greater than the Darden
dividend before the real estate transaction was completed.
We continued to leverage our scale through aggressive
cost management programs and supply chain optimization.
This, combined with operational simplification and better
overall cost management at the restaurants, enabled
us to deliver $130 million in annual savings since fiscal
2015. We also simplified our organizational structure to
become more decentralized and operations-focused,
with clearer leadership accountability. And across our
portfolio, we increased the sharing of data and insights
on our guests needs and preferences to increase
guest traffic, drive in-restaurant behavior, enhance the
guest experience and improve financial returns.
In December, we introduced our value-creation framework.
Our goal over time is to deliver long-term earnings
after tax growth of 7 to 10 percent, derived from:
 Same-restaurant sales growth of 1 to 3 percent
 New restaurant growth of 2 to 3 percent
 EBIT margin expansion of 10 to 40 basis points
We expect to pay out approximately 50 to 60 percent of
our earnings after tax as dividends and repurchase $100
million to $200 million of our shares annually, leading to
a long-term Total Shareholder Return of 10 to 15 percent
 composed of EPS growth and dividend yield.
In summary, we have a value-creating business model
that generates significant and durable cash flow to fund
future growth and return capital to shareholders.

Delivering Value to Our Guests
Our strategy and positioning for each business
enabled us to build momentum and identify new ways
to exceed the expectations of our guests. In fiscal
2016, we successfully delivered against our mission
as we grew total sales to $6.9 billion with samerestaurant
sales growth at each of our businesses.
For the second consecutive year, Olive Garden
delivered positive same-restaurant sales growth  at
3.1 percent in fiscal 2016. The continued success of
Olive Garden is rooted in our laser focus on enhancing
the guest experience at every touchpoint. We boosted
culinary innovation by leveraging brand equities and
flavor profiles that loyal guests enjoy the most, and
we complemented this innovation with an expanded
menu that offers guests a variety of choices that
span a wide range of prices. To further improve the
guest experience, we simplified our operations and
ensured our restaurants were properly staffed.
Additionally, we continued to meet our guests growing
demand for convenience through our successful OG To-Go
platform and the national launch of large-party catering
delivery. Finally, to tell our guests about whats new in
our restaurants, weve evolved our marketing to be more
integrated, targeted and personally relevant. This focus on
enhancing every touchpoint has improved the perceived
value of Olive Garden and has resonated with guests
as we outperformed the industry by 390 basis points.1
The momentum at LongHorn Steakhouse continued with
its third straight year of same-restaurant sales growth  at
3.5 percent in fiscal 2016. This success comes from an
emphasis on building loyalty through culinary innovation
and our relentless pursuit of delivering flawless guest
experiences. Through culinary platforms like Peak Season
and Chef Showcase, LongHorn continually introduced new
menu offerings that were seasonally relevant and made
with fresh, quality ingredients. In addition, we reduced
the number of menu items, simplifying procedures to
allow our restaurant teams to execute at a higher level.
We also invested in team member training, which helped
us defend our industry-leading retention. Finally, we
continued to reach guests with impactful advertising,
which was recognized by Ace Metrix when it named
LongHorn the Brand of the Year for casual dining for
the second year in a row. All of this resulted in LongHorn
outperforming the industry by more than 400 basis points.

Seasons 52 had a strong year, growing same-restaurant
sales 4.7 percent. As an on-trend concept  giving
guests the opportunity to enjoy hand-crafted cuisine
that is healthful without sacrificing taste or style 
Seasons 52 is uniquely positioned to capitalize in a
dining segment that is poised for continued growth.
Our focus on enhancing operational execution and
evolving the menu received an incredibly positive
reaction from our guests, giving us momentum as we
build the pipeline for value-creating new restaurants.
Bahama Breeze continued to significantly outperform
the casual dining industry1 with same-restaurant sales
growth of 4.8 percent. Positioned as an island oasis
from the everyday, Bahama Breeze paired culinary
innovation with high-energy, in-restaurant events.
These events created signature experiences that
reinforced the business unique positioning while
attracting more guests  particularly Millennials.
At Yard House, same-restaurant sales grew 2.3 percent, the
third consecutive year of growth, as we continue to build
a loyal guest base. Its from this base that we know that
our extensive beer selection, scratch kitchen and specially
curated rock music continued to strike a chord across
multiple demographics that joined us for lunch, happy hour,
dinner and late-night happy hour. We are extremely excited
about the growth we have planned for this business.
For the sixth consecutive year, The Capital Grille grew
same-restaurant sales  growing 3.9 percent in fiscal
2016. The Capital Grille is the ultimate relationship
brand, offering a welcoming and club-like dining
experience. Delivering high-touch experiences is
nothing new for us, and we continued to leverage
technology to enable us to personally connect with
more guests to create exclusively tailored visits.
Eddie Vs achieved same-restaurant sales growth of
1.8 percent. With its positioning as the destination for
a glamorous night out, this fine-dining restaurant was
able to capitalize on its ability to deliver unforgettable
experiences  setting new sales records during
key holidays throughout the year. Additionally, we
were able to implement operational initiatives that
simplified execution while preserving the high levels
of food quality and service for which were known.
We have a strong and differentiated portfolio of businesses,
each with anticipated growth ahead of them. Reflecting this
growth opportunity, we are ramping up the development
pipeline for future sites and anticipate opening 24 to
28 new restaurants in the coming fiscal year.

Delivering Value to Our Team
Members and Communities
Everything we do starts with people: our 150,000
team members, the nearly one million guests we
serve each day and the individuals who live in the
communities that are home to our restaurants. Thats
why we strive to provide meaningful and rewarding
employment, deliver outstanding food and service to
our guests and give back to our local communities.
For many of our team members, Darden is their first
employer. This is a role we take great pride in  and a
responsibility we take seriously. For some, a job in our
restaurants is the start of a career path to management
positions within the Company. In fact, half of our
restaurant managers began their careers with us as
hourly team members. For others, it enables them
to further their education and eventually pursue a
career elsewhere. Whatever the case, we know that
the skills and experience we provide will help our team
members grow and succeed within the Company  or
wherever their career paths ultimately take them.
Inclusion and diversity are woven into the fabric of our
culture. Every day we leverage our differences to support
the business strategy as we create an environment
where all of our team members can reach their greatest
potential. In fact, 52 percent of our team members are
women, and 49 percent are minorities. Additionally, our
team members span five generations  Matures, Baby
Boomers, Generation X, Millennials and Centennials.
We also maintain strong relationships with more than
25 organizations that have a mission to advance diverse
communities. And were proud to have been recognized
by the Human Rights Campaign Foundation for scoring
100 percent on the Corporate Equality Index.
We have a social responsibility to our guests and communities,
which is why our approach to citizenship is a key component
of how we fulfill our mission. We bring this to life through:
 A commitment to our Food Principles  that great food
starts with quality ingredients that are sustainably sourced
 Our protection of the natural environment and resource
conservation efforts in our restaurants
 Connection and support for the vibrant communities
where we live, work and serve
We invite you to read more about our commitment
to citizenship at www.darden.com/citizenship.
We are doing what matters most for our guests 
delivering memorable experiences that will bring them
back time and time again. Fiscal 2016 was a great
year for Darden, but theres more work ahead.
With that in mind, we want to thank our dedicated team
members for their commitment to excellence. They are the
foundation of our success. We also want to express our
appreciation to the Board of Directors for providing strategic
guidance and their commitment to effective, transparent
corporate governance. Finally, we want to thank you, our
fellow shareholders, for your continued support of Darden
and our vision. We look forward to rewarding that support
through our ongoing commitment to value creation.

Charles M. Sonsteby
Chairman of the Board
Eugene I. Lee, Jr.
President and Chief Executive Officer