Dear Distinguished Shareholders,
The Board of Commissioners had reviewed the business
strategic and policies formulated and implemented by the
management of the Company under the supervision and
guidance of the Board of Directors in 2015, and received the
report on PT Century Textile Industry Tbk. (the Company)s
activities and management reports submitted by the Board
of Directors, and we had studied the statement of !nancial
position of the Company as at 31 March 2016, and its !nancial
performance and its cash "ows for the year then ended, which
have been audited by the public accounting !rm of KPMG
Siddharta Widjaja & Rekan No.L.16-157-16/VI.29.001 dated 29
June 2016, we provide approval on the !nancial statements.
The textile industry and textile products (TPT) in the 2015 still
face continuous challenges from previous years. The main
factors that a#ected the condition of business textile due to,
the high of labor cost and the increase of electricity price for
industry sector.
During 2015, The Board of Directors had well implemented
its duties and responsibilities in managing the Company.
The Board of Directors has also tackled various internal and
external challenges by taking strategic and wise measures.
This is based on our consideration regarding several steps that
the Board of Directors has taken in responding to market and
economic conditions during 2015. The Company was then
able to maintain stable business growth and performance
amid the global economic uncertainties.
Total sales for the year ended 31 March 2016 amounted to
US$ 32,938 thousand, decreased by 5.66% compared to the
annualized of three-month period 31 March 2015. Decrease in
sales is mainly due to slower market demand since the market
in America, Europe and Japan are not fully recovered.
The Companys recorded pro!t for the year of US$ 1,275
thousand in the year ended 31 March 2016, increase
compared with loss the annualized of three-month period 31
March 2015 amounted US$ (1,771) thousand.
Generally, the performance better than in previous year. With
respect to the Companys performance in the year 2015,
we recommend to the Board of Directors to increase the
Companys competitiveness both in the quality and good 
prices in the local market and export through strategic market
development with strong innovation.
The Board of Commissioners is optimistic that the Company
is able to increase its business growth through strategic
policies to deal with any existing challenges and with the
commitment to continuously improve the implementation of
GCG principles. The Board of Commissioners mandates that
all ranks of the management and employees can corporate
in synergy with all stakeholders to successfully deliver prime
service that is beyond customers demand.
The Board of Commissioners is also satis!ed that the Board
of Directors has adequately considered the risk factors which
can a#ect the performance of the Company,
In the year 2015, there were no changes in the composition of
the Board of Commissioners of the Company.
Concluding this reports, The Board of Commissioners wish
to thank to the Board of Directors, All Shareholders, business
partners, Audit Committee and all employees for their trust
and support given to us for the dedication and hard work to
achieve better development of the Company.