To Our Shareholders:
At Discover, our Mission is to help people achieve a brighter fi nancial future. By effi ciently
working to exceed customer expectations, we produce strong growth and profi tability,
which benefi ts our shareholders.
Fundamental to our success is our dedication to the Discover Values, which have been in
place for more than 15 years. They guide the �what� and the �how� of our business as we
strive for fair, responsible and ethical behavior.
Perhaps the most signifi cant is our fi rst Value��Doing the Right Thing��because it
infl uences all our actions with consumers, business partners, employees, and the
communities in which we operate. A prime example is our long-time commitment to
provide a better customer experience by not outsourcing any customer service and
locating all of our customer care centers in the United States.
In 2016, we further advanced our Vision to be the leading direct bank and payments
partner as we achieved a year of accelerating loan and deposits growth, continued to
expand our global network acceptance, and achieved very strong profi tability.

Our Progress
2016 highlights included:
� Earnings per share of $5.77 per diluted share (12% higher than 2015)
� Return on Equity of 21%
� Total loan growth of 7%
� A net charge-off rate of 2.16%
� Network volume of $307 billion, including $121 billion from Discover card sales
We achieved these results by focusing on growing loans and revenue across Direct
Banking while managing expenses, leveraging our payments network to benefi t our
Card business and our partners, and enhancing controls and risk management systems
and processes.
In addition to a year marked by strong fi nancial results, we received a number of
important awards. In 2016, for the third consecutive year, Discover was ranked
�Highest in Customer Satisfaction with Credit Card Companies� by J.D. Power. We also
were named #1 in Customer Loyalty by Brand Keys, continuing a 20-year streak. We
were ranked fourth among the Global 100 Banks in the Laff erty Bank Quality Ratings,
and Money Magazine named three Discover it cards to their Best Credit Cards List. 

We also work hard to foster a positive, diverse and inclusive work environment. Over
the last several years, an annual survey by Willis Towers Watson showed that employee
engagement at Discover is higher than at other fi nancial services companies. We
believe more engaged employees provide a great customer experience and give us a
competitive advantage in the marketplace.
Discover also continues to be recognized as a top workplace in the states where
we have a major presence, including Arizona, Delaware, Illinois, Ohio and Utah. In
addition, we have been named as a top place to work for LGBT equality by the Human
Rights Campaign�s Corporate Equality Index, for Asian-Pacifi c Americans by the Asia
Society, and for millennials in Fortune�s 100 Best Workplaces list.
And fi nally, we were recognized with the Employer Support of the Guard and Reserve�s
2016 Pro Patria Award from the state of Delaware for our pioneering Military Work at
Home job and training program.
2016 Direct Banking Accomplishments
Discover�s Direct Banking portfolio includes credit cards, as well as student, personal and
home equity loans. Our deposit products include money market accounts, certifi cates of
deposit, and savings and checking accounts.
Discover Card
Competition in the credit card industry increased in 2016, especially in the area of cash
rewards. Discover�s response included disciplined investment in our popular 5% Cashback
Bonus program, continuation of the Cashback Match program for new cardmembers, and
emphasis of key elements of our rewards program, such as ease of redemption.
In addition, Discover introduced several new products in 2016, building on the popularity
of the Free FICO� Credit Scores that we began providing to cardmembers in 2014. This
past year, we launched the innovative Credit Scorecard to provide Free FICO� Credit Scores
to all qualifi ed consumers so they can better manage their credit. Millions of people 

visited the Credit Scorecard (www.creditscorecard.com) website last year, and we expect
engagement to continue to build, creating more awareness about our products and
services. We also launched the Discover it Secured card to help people establish or
improve their credit history.
As a result of our continued focus on rewards, service and value, we grew card loans by 6%
in 2016�our highest growth since 2000. Importantly, we did this without sacrifi cing credit
quality: our credit card net charge-off rate remains well below the industry average.
In 2017, you can expect additional enhancements in cash rewards, as well as our
products and features, which will further diff erentiate Discover from competitors. We will
also continue to leverage our proprietary payments network to the mutual benefi t of our
merchant partners and Discover cardmembers.
Student Loans
Discover is the third largest provider of private student loans. In 2016, we invested in fi netuning the application process and improved the end-to-end customer experience, resulting
in a record $1.4 billion of originations and commitments.
In 2017, we will implement the fi nal phase of the rollout of a world-class core banking
platform. By consolidating multiple platforms into one application, we gain a number
of effi ciencies, including self-service options for our existing customers and making it
easier for our potential customers to obtain fi nancing for the education of their choice. We
will continue to invest in student loan awareness and education programs that will help
students and their parents make better fi nancial decisions.
Personal Loans
Discover provides personal loans with a fi xed rate and term to help consumers
consolidate their debt or pay for life�s big events. In 2016, we invested in our digital
capabilities by launching a redesigned mobile-responsive website and application,
making it easier for consumers to obtain the money they need. We launched a Debt
Consolidation Calculator and Check Your Rate functionality to provide consumers with a 

fast and easy way to estimate their interest rate and payments without impacting their
credit score.
Last year, we originated a record $4 billion in personal loans. Looking ahead, we will
enhance our digital marketing to increase product awareness and consideration.
Home Equity Loans
Home equity loans are our newest product off ering, and in 2016, we focused on
streamlining processes and simplifying the customer experience. We made
the home equity website mobile-responsive, expanded loan limits and updated
payment timeframes.
Deposits
Direct-to-consumer deposits grew 17% to $36 billion in 2016, in part due to better
customer retention. We also improved the cost eff ectiveness of what is now our
single largest funding source. In the coming year, we will continue to enhance our
deposit products and make improvements to our infrastructure through increased
process automation.
2016 Payment Services Accomplishments
In Payment Services, we increased pre-tax profi t while also navigating the challenges and
opportunities that come with new technologies, capabilities and players. One example
is the industry�s ongoing transition to EMV/chip cards in the United States. Discover is
actively working to help cardmembers, merchants and acquirers with this new, more
secure technology.
In 2017, we will concentrate on increasing global acceptance and volume on our network.
We will also continue to focus on providing our partners in the payments industry with
secure transactions on a dependable infrastructure, with more fl exibility and better
service than our competitors. 

Discover Values in Action
As impactful as the Discover Values are to our business, customer and employee
interactions, I believe they also extend to the communities in which we operate. As we
enter the sixth year of our Pathway to Financial Success program, we have exceeded
our initial $10 million commitment to bring fi nancial education to our nation�s high
schools. So far, we have reached more than 500,000 students at 1,700 public schools
and districts across all 50 states. Educating our youth about smart saving and spending,
along with good credit health, is an important step in helping people achieve a brighter
fi nancial future.
Additionally in 2016, our employees volunteered more than 60,000 hours of their
time, and together we donated millions of dollars to non-profi t organizations. Discover
employees tutored and mentored children, constructed playgrounds, renovated schools
and helped build new homes. I am grateful that our people show so much commitment to
and compassion for their local communities.
Finally, I would like to thank you�our customers, employees, partners and shareholders�
for putting your trust in Discover. You have helped us establish our position as an industry
leader. We understand that we exist to serve you.
David Nelms | Chairman and Chief Executive Offi cer | March 7, 2017