FELLOW SHAREHOLDERS:
Fiscal 2008 was an outstanding year for Western Digital,
demonstrating the effectiveness of our strategy and of our
enhanced business model.
Customer satisfaction with WDs broad product line, high quality
and reliability, service excellence and overall value proposition
continues to drive our business growth. Underpinning our value
proposition is a business model based on a relentless focus on
highly efficient operations, resulting in what we believe is the hard
drive industrys most competitive total cost structure.
In fiscal 2008 we delivered outstanding financial results, outpaced
the growing global hard drive industry, established technology
leadership in the 2.5-inch notebook and 3.5-inch desktop PC
markets, and completed and integrated the largest acquisition
in company history. As a result, we entered fiscal 2009 well
positioned to continue our profitable growth.
Our fiscal 2008 performance was noteworthy, including strong
year-over-year growth by many key measures:
 Shipments of hard drives increased 38 percent, to 133 million,
driven by significant growth in new markets.
 Revenue grew 48 percent, to $8.1 billion.
 Operating income increased 142 percent, to $1.0 billion,
delivering on our stated goal of profitable growth.
 Earnings per share grew 54 percent, to $3.84, driving a
significant increase in shareholder value.
 We increased our total capital and research and development
spending by 71 percent, to $1.1 billion.
 We completed our $1 billion acquisition of Komag, a leading
media supplier, resulting in WD being an even more vertically
integrated player that can better scale into fast-growing markets.
Our new media operation is already generating solid technology
and cost contributions to the overall business.
 We finished the year with $1.1 billion in cash and cash
equivalents, an increase of over $400 million.
This is a great time to be in the growing global hard drive industry.
Storage demand and applications for hard drives continue to
proliferate in computing and consumer markets as both workplace
and lifestyle changes generate massive volumes of data to be
secured conveniently and cost effectively on hard drives.
 In fiscal 2008 the HDD market generated revenues in excess
of $35 billion with 540 million hard drives shipped, while
forecasted demand for fiscal 2009 exceeds 620 million units.
 On a unit basis, the overall hard drive market is looking at
a five-year CAGR of approximately 13 percent, while those
markets served by WD are forecast to grow in excess of
16 percent annually.
 The industrys fastest growth opportunities continue to be in the
notebook PC and branded product markets, areas of continued
emphasis for WD.
While maintaining our focus on the high-volume desktop PC
market, we remain committed to our multi-year diversification
effort. As a result, hard drive revenues from non-desktop markets
expanded to 56 percent of revenue in fiscal 2008, compared with
43 percent in fiscal 2007 and 29 percent in fiscal 2006. More
specifically:
 We saw tremendous growth this year in the 2.5-inch notebook
PC and branded products markets:
 We tripled our 2.5-inch drive shipments, year-over-year, to
37 million units and with continued new product execution,
we are well positioned for continued growth and market
expansion in this space.
 We increased sales of our branded products by 60 percent
year-over-year to $1.4 billion and we continued to add new
products and product features to the line up, to strengthen
our leadership position in the external storage space and
expand our available market.
 At the same time we made good strides in other non-traditional
markets such as Serial ATA (SATA) drives for the enterprise
space and 3.5-inch drives for consumer electronics (CE).
 In Enterprise SATA, a fast growing portion of the enterprise
storage market, we continued to innovate with the
introduction of our 3.5-inch WD RE2-GP SATA drives with
GreenPower technology and the WD VelociRaptor drive
family, the industrys first 300 gigabyte (GB) 10,000 RPM,
2.5-inch drive.
 In CE, the introduction of our power-saving GreenPower
technology to our WD AV hard drives, combined with
improved costs and our demonstrated field quality
performance, led to enhanced value for our customers and
improved contribution to WDs business.
We have made and continue to make significant investments in
the technologies and infrastructure that will enhance our ability
to keep on diversifying the business and compete as a full-line
industry leader. We will do so with the product portfolio required
to capitalize on the growth trends across all markets and with the
capacity and cost structure to do so efficiently and profitably. There
were several tangible examples of return on these investments in
fiscal 2008:
 We achieved the industry-leading technology position in both
2.5-inch notebook and 3.5-inch desktop PC markets;
 Utilizing our GreenPower technology, we launched a family of
hard drives specifically designed to consume substantially less
power than standard drives;
 We began shipping our new line of WD Scorpio Black
7200 RPM 2.5-inch SATA hard drives in capacities up to
320 GB for high-performance notebook computers, expanding
our available market in the 2.5-inch sector; and
 We completed the rollout of the popular My Passport series of
portable hard drives with a refreshed design in multiple colors
and we began shipping many other new and next-generation
branded products, addressing higher capacity points, Mac
users, solutions for data backup, and the growing recording
capacity needs of digital video recorder (DVR) users.
We have also added to our design and development capabilities
with significant expansion of our technical workforce in Lake
Forest, San Jose, and Asia, the addition of a new design center
in Longmont, Colorado, and the acquisition of the hard drive
controller intellectual property rights, design tools and design
team from STMicroelectronics. This in-house hard drive controller
capability will enhance the efficiencies of our development
process with close engagement from industry system-on-chip and
channel suppliers. Our previously announced plan to upgrade
and expand our Fremont wafer facility is on track and we have
already produced the first wafers from our new 8-inch pilot line,
ahead of schedule.
We are pleased with WDs continued profitable growth and we
remain focused on a strategy to build a company that will thrive
in the very attractive storage industry in the years to come. We
are confident in our ability to continue competing successfully
and we look forward to applying WDs operational excellence,
disciplined business model and the WD team values of passion,
action, productivity, perseverance, innovation, and integrity in order
to drive continued profitable growth for WD and to create value for
our shareholders.

John F. Coyne
President and Chief Executive Officer