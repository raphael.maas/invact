LETTER TO SHAREHOLDERS*
Torchmark had another good year in 2017. Return on equity, excluding net unrealized gains on fixed maturities, was 14.3% and total premiums
grew 5%. Agency sales grew 8%, driven by increases in both agent count and productivity. We have had sales growth in each of our exclusive
agencies now for four years in a row. While Direct Response sales have declined, this was by design due to actions taken to maximize profit
dollars. We are encouraged with recent results as we have seen the margins stabilize. We expect to begin to see Direct Response sales growth
again in 2019.
Many investors ask us how Torchmark differs from other life insurance companies. The answer can be found in our business model, a model that
has consistently facilitated significant growth regardless of general economic conditions. The components of this model are discussed below:
“ We believe Torchmark has a
very bright future. Many times
we hear that life insurance is
a mature market with limited
growth potential. For Torchmark,
this is simply not true. We
are uniquely positioned to
thrive and build shareholder
value well into the future.”
 *Throughout this letter net operating income represents net operating income from
continuing operations.
MARKET
the middle-income market – a vastly
underserved marketplace with significant
growth potential and relatively little
competition.
PRODUCTS
We focus on basic life and supplemental
health products that best meet the needs of
our marketplace. These products are simple
for agents and customers to understand
and are not impacted by interest rate or
equity market fluctuations.
CONTROLLED DISTRIBUTION
We market our products primarily through
exclusive agency and direct response
channels. This enhances our ability to limit
competitive pressures and control costs.
MARGINS
Torchmark has a long history of
administrative efficiency. Our ability to
control both acquisition and administrative
costs helps produce healthy underwriting
margins. As such, we don’t have to rely
on investment income to generate
operating income.
CASH FLOWS
Torchmark’s highly persistent block of in force
business produces strong excess cash flows
year in and year out. The persistency of our
in force block has always been very stable,
regardless of macro-economic conditions.
Nearly 90% of our premium revenue comes
from policies sold in prior years.
RETURN OF EXCESS CAPITAL TO
SHAREHOLDERS
Due to the significant amount of excess
cash flow produced each year, Torchmark
has returned approximately 71% of its net
income to shareholders through share
repurchases and dividends since 1986
(absent the positive impact that tax reform
had on 2017 net income, this ratio would be
76%). We remain committed to returning
excess capital to shareholders.
4 | TORCHMARK CORPORATION | LETTER TO SHAREHOLDERS
2007 2009 2011 2013 2015 2017
$14.77
$17.88
$21.31
$25.85
$30.09
$39.77*
We will continue to follow this business model. That being said, we
must continually refine that model through innovation and the use
of technology in response to the changing needs and expectations
of our agents and customers. It is imperative that we work with
customers and agents in a manner that best meets their needs and
preferences.
In order to ensure that we maximize our ability to be nimble and
adapt to a rapidly changing environment, we continue to make
significant investments in technology. These investments are
designed with the following goals in mind:
• Facilitate a digital consumer experience
• Improve the agent experience and ease of doing business
• Replace back office legacy systems with modern, commercial
solutions
• Expand the use of data analytics
• Protect against cyber threats
While these investments require significant resources and attention,
we believe they are a critical component of profitable future growth.
Torchmark has produced steady growth in earnings per share and
book value per share as evidenced by the charts below.
* Book value per share, excluding net unrealized gains on fixed maturities,
calculated prior to the tax reform adjustment would have been $34.68.
BOOK VALUE PER SHARE
(Excluding Net Unrealized Gains or Losses on Fixed Maturities)
Compound Annual Growth Rate:
10 Year: 10.4%, 5 Year: 11.1%
BOOK VALUE PER SHARE
Compound Annual Growth Rate:
10 Year: 13.9%, 5 Year: 11.6%
2007 2009 2011 2013 2015 2017
$14.47 $16.40
$25.27 $27.66
$32.71
$52.95*
* Book value per share calculated prior to the tax reform adjustment would have
been $45.52.
2007 2009 2011 2013 2015 2017
$2.30 $2.05
$3.02
$3.79 $4.16
$12.22*
* Net income per share calculated prior to the tax reform adjustment would have
been $4.88, resulting in a 10-year compound annual growth rate of 7.8% and a
5-year compound annual growth rate of 6.3%.
NET INCOME PER SHARE
Compound Annual Growth Rate:
10 Year: 18.2%, 5 Year: 27.7%
2007 2009 2011 2013 2015 2017
$2.18 $2.32
$2.91
$3.65
$4.13
$4.82
NET OPERATING INCOME PER SHARE
Compound Annual Growth Rate:
10 Year: 8.3%, 5 Year: 7.8%
LETTER TO SHAREHOLDERS | TORCHMARK CORPORATION | 5
Underwriting income reflects premiums less policy benefits,
acquisition costs, and administrative expenses. In contrast to many
other life insurance companies, most of Torchmark’s operating
income is generated by insurance underwriting margin, rather
than investment income. Underwriting income accounted for
approximately 73% of pre-tax operating income in 2017.
Torchmark consistently generates a strong return on equity (ROE).
In 2017, the ROE, excluding net unrealized gains on fixed maturities,
was 14.3%. On a GAAP basis, 2017 ROE was 28.2%. The higher GAAP
level was due primarily to the impact of tax reform.
INSURANCE OPERATIONS
COMPONENTS OF NET OPERATING INCOME
($ in millions, except per share data)
PER SHARE
Underwriting Income $625 $5.25
Excess Investment Income 239 2.01
Tax and Parent Expenses (288) (2.42)
Stock Compensation Expense, Net of Tax (2) (0.02)
Net Operating Income $574 $4.82
COMPONENTS OF UNDERWRITING INCOME
($ in millions)
$ AS % OF
PREMIUM
Underwriting Margin
 Life $604 26.2%
 Health 219 22.5%
 Other 11
Total $834 25.4%
Administrative Expenses Net of
Other Income (209) 6.4%
Underwriting Income $625 19.0%
We prefer to focus on basic protection life insurance and
supplemental health insurance products that provide more
predictable profitability and better meet the needs of our market
than other types of coverage. Our products provide protection
to families against loss of income due to the death or illness of a
breadwinner by helping with final expenses, mortgage payments,
tuition or other household expenses.
6 | TORCHMARK CORPORATION | LETTER TO SHAREHOLDERS
AMERICAN INCOME LIFE
As can be seen in the charts below, American Income has a long
history of impressive sales and agent count growth. In addition, life
premiums have grown at an 8.5% compound annual growth rate
over the past ten years.
For more than 50 years, American Income has operated as an
all-union company with a unionized labor force. Through a strong
relationship with organized labor, American Income enjoys a natural
affinity with the market it serves – working families. While the
union affiliation is core to the American Income business, we have
diversified over the years through a strong push to focus on other
sources of leads such as referrals.
We believe American Income has the potential for significant future
growth due to its unique competitive position in an underserved
market and its proven ability to grow the agency force.
AMERICAN INCOME LIFE  LIFE SALES
($ in millions)
10-Year Compound Annual Growth Rate: 9.3%
2007 2009 2011 2013 2015 2017
$92
$128
$142 $153
$198
$223
DISTRIBUTION CHANNELS
We reach the market through several distinct channels, each serving
a particular niche.
The chart below shows the distribution of underwriting margin
among our distribution channels.
2017 TOTAL UNDERWRITING MARGIN
American Income Life
Globe Life Direct Response
General Agency
Liberty National LIfe
Family Heritage Life
44%
7%
15%
17%
17%
AMERICAN INCOME LIFE  AGENT COUNT
10-Year Compound Annual Growth Rate: 10.5%
2007 2009 2011 2013 2015 2017
2,545
4,154 4,381
5,302
6,552 6,880
LETTER TO SHAREHOLDERS | TORCHMARK CORPORATION | 7
FAMILY HERITAGE LIFE
Family Heritage primarily markets limited-benefit health products in
non-urban areas through a door-to-door approach. Most of these
products include a return of premium feature that generates better
persistency, margins and investment income than typical health
insurance products.
Family Heritage was purchased by Torchmark late in 2012. Since
2013, health sales and agent count have grown 7% and 9%,
respectively, driven by incorporation of Torchmark recruiting
programs and a focus on agency development. We expect to see
consistent growth as Family Heritage continues to incorporate best
practices of other Torchmark agencies and cultivate an agency
culture centered on recruiting and development of agency middle
management.
LIBERTY NATIONAL LIFE
Over the last several years, Liberty National has been transformed
from a fixed-cost home service model to a variable-cost model.
After a long history of flat to declining life premiums, we believe
Liberty National is positioned for sustainable growth. Life premiums
grew 2% in 2017, the first annual increase since 2003.
As can be seen below, total sales and agent counts have grown at
a compound annual growth rate of 7% and 8%, respectively, over
the past five years. This growth is being driven by development of
agency middle management that is responsible for the recruiting
and training of new agents. We are encouraged by the turnaround
at Liberty National and are very optimistic regarding Liberty
National’s long-term growth prospects.
LIBERTY NATIONAL LIFE  SALES
($ in millions)
5-Year Compound Annual Growth Rate: 7.3%
2012 2013 2014 2015 2016 2017
$47 $45
$52 $54
$60
$67
LIBERTY NATIONAL LIFE  AGENT COUNT
5-Year Compound Annual Growth Rate: 8.2%
2012 2013 2014 2015 2016 2017
1,419 1,430 1,498 1,478
1,758
2,106
FAMILY HERITAGE LIFE  HEALTH SALES
($ in millions)
4-Year Compound Annual Growth Rate: 6.7%
2013 2014 2015 2016 2017
$44
$47
$50 $51
$57
FAMILY HERITAGE LIFE  AGENT COUNT
5-Year Compound Annual Growth Rate: 8.9%
2012 2013 2014 2015 2016 2017
702 695
785
911 909
1,076
8 | TORCHMARK CORPORATION | LETTER TO SHAREHOLDERS
GLOBE LIFE DIRECT RESPONSE
This unit began operations over 50 years ago as a direct mailonly
distribution channel. Over the years, we have transitioned to
a multifaceted approach, with the addition of insert media and
electronic media distribution. Having multiple direct response
channels provides us a unique competitive niche in the direct
response market because it gives us more ways to monetize leads
and allows us to reach consumers more effectively.
While Direct Response has a long history of sales growth, sales have
declined in the past couple of years due to operational changes
that have been made in response to higher than originally expected
claims which emerged in 2015 in certain blocks of business. These
operational changes were designed to maximize margin dollars,
knowing that sales would be negatively impacted. We expect to
begin to grow sales in 2019 through further use of analytics and
innovation. However, our primary focus will continue to be the
maximization of profit dollars, rather than sales levels.
The value of the Direct Response unit extends far beyond direct
response sales production. The expertise and resources of this unit
also support the recruiting, lead generation and data management
efforts of our agencies. We expect Direct Response to continue to
help drive Torchmark’s success well into the future.
UNITED AMERICAN
This unit primarily markets Medicare Supplement insurance through
independent agents and brokers to individuals as well as union
and employer groups. The Medicare Supplement market is very
different than the markets we serve through controlled distribution.
It is a highly regulated, competitive market that is relatively easy
for companies to enter and exit. While short-term sales trends are
difficult to predict because of the use of independent distribution
and the impact of adding large groups, this unit has a relatively
stable in force block that continues to grow as shown in the
chart below.
We have been in the Medicare Supplement market since the
inception of Medicare. We have the expertise and the infrastructure
to administer this business efficiently. The use of independent
producers allows us to take advantage of opportunity when market
conditions are favorable and avoid competitive pressure when
market conditions are not as favorable.
DIRECT RESPONSE  LIFE SALES
($ in millions)
10-Year Compound Annual Growth Rate: 1.8%
2007 2009 2011 2013 2015 2017
$114
$132 $137 $144
$164
$136
UNITED AMERICAN  HEALTH SALES
($ in millions)
5-Year Compound Annual Growth Rate: 7.7%
2012 2013 2014 2015 2016 2017
$42 $41
$84
$72
$56
$61
UNITED A
LETTER TO SHAREHOLDERS | TORCHMARK CORPORATION | 9
INVESTMENT PORTFOLIO
The strength of our underwriting margins allows us to
maintain a conservative philosophy and not make risky bets on
investments. While we seek to maximize risk-adjusted returns,
the most important criteria when considering new investments is
preservation of principal.
We choose to invest almost exclusively in long-term fixed
maturities as these fixed-rate investments best match our long
term fixed-rate liabilities and enhance our ability to manage capital
as efficiently as possible.
As can be seen in the chart above, invested assets have grown from
about $9 billion at the end of 2007 to just under $16 billion. This
growth was achieved in spite of the fact that we spent $4.0 billion to
repurchase shares over that period.
INVESTMENT OPERATIONS
We evaluate the investment function on a stand-alone basis rather
than allocating investment income to the insurance operations.
Excess investment income is the measure used to evaluate the
performance of the investment segment. The components of
excess investment income can be seen in the chart above. Excess
investment income produced 28% of our pre-tax operating income
in 2017.
In recent years, growth in excess investment income has been
limited by the low interest rate environment. We are encouraged
by the prospect of a higher long-term interest rate environment as
it would provide higher excess investment income because of the
positive impact on net investment income.
EXCESS INVESTMENT INCOME
($ in millions)
Net Investment Income $848
Required Interest on Net Policy Liabilities (524)
Interest on Debt (85)
Excess Investment Income $239
INVESTMENT PORTFOLIO  DECEMBER 31, 2017
Invested Assets ($ in millions)
$ AS % OF
TOTAL
Fixed Maturities (at amortized cost) $14,995 95%
Policy Loans 530 3
Other Investments 235 2
Total $15,760 100%
COMPONENTS OF NET OPERATING INCOME
($ in millions, except per share data)
PER SHARE
Underwriting Income $625 $5.25
Excess Investment Income 239 2.01
Tax and Parent Expenses (288) (2.42)
Stock Compensation Expense, Net of Tax (2) (0.02)
Net Operating Income $574 $4.82
2007 2009 2011 2013 2015 2017
$9.2
$10.3
$11.4
$13.0
$13.8
$15.8
TOTAL INVESTED ASSETS AT
AMORTIZED COST
($ in billions)
10-Year Compound Annual Growth Rate: 5.6%
10 | TORCHMARK CORPORATION | LETTER TO SHAREHOLDERS
CAPITAL MANAGEMENT
operations, maintain appropriate capital levels, and maximize both
the amount of and return on excess cash flow.
We define excess cash flow as the cash available to the parent
company from the dividends received from the insurance
subsidiaries to the parent after paying dividends to Torchmark
shareholders and interest on debt.
The next chart illustrates the significant excess cash flow generated
routinely at Torchmark.
2007 2009 2011 2013 2015 2017
6.96% 6.81% 6.49%
5.91% 5.83% 5.60%
FIXED MATURITY PORTFOLIO YIELD
(at end of year)
FIXED MATURITY PORTFOLIO YIELD
This chart demonstrates the negative impact of lower interest rates
on investment yields over the last ten years. As we indicated earlier,
we hope for higher interest rates – the sooner the better. We are not
concerned about the possibility of unrealized losses resulting from
higher interest rates as we have the intent and more importantly,
the ability, to hold our investments to maturity.
However, if rates don’t rise as anticipated, we can continue to thrive
in a low interest rate environment as our products are not sensitive
to interest rate or equity market fluctuations.
EXCESS CASH FLOW
($ in millions)
2007 2009 2011 2013 2015 2017 2018
$353
$281
$367 $364 $358
$330 $320-
330
Estimate
Excess cash flow has been somewhat lower in the last couple of
years due to slight declines in net statutory income. These declines
have resulted in part from the strong life sales growth in recent
years and investments in technology discussed earlier. While life
sales growth and these technology investments are detrimental to
statutory net income in the short run, they will produce growth in
excess cash flow in the long run. 
LETTER TO SHAREHOLDERS | TORCHMARK CORPORATION | 11
RETURN TO SHAREHOLDERS
($ in millions)
SHARE
REPURCHASES
DIVIDENDS
PAID
(A) TOTAL
CASH
RETURNED
(B) NET
INCOME
(A)/
(B)
2008 $427 $49 $476 $427 111%
2009 $47 $47 $94 $383 25%
2010 $204 $50 $254 $499 51%
2011 $788 $49 $837 $497 168%
2012 $360 $56 $416 $529 79%
2013 $360 $61 $421 $528 80%
2014 $375 $65 $440 $543 81%
2015 $359 $67 $426 $527 81%
2016 $311 $67 $378 $550 69%
Subtotal $3,742 $4,483 83%
2017 $325 $69 $394 $1,454* 27%*
10Year Total $4,136 $5,937 70%
SHARE REPURCHASES
* Ratios were calculated using net operating income.
TOTAL SPENT
(IN MILLIONS)
NO. OF SHARES
(IN 000’S)
AVERAGE
PRICE P/E RATIO*
 $427 17,185 $24.83 10.9
2009 $47 4,613 $10.12 4.4
2010 $204 8,560 $23.78 9.4
2011 $788 28,347 $27.78 9.5
2012 $360 11,219 $32.13 9.7
2013 $360 8,280 $43.48 11.9
2014 $375 7,155 $52.42 13.4
2015 $359 6,292 $56.99 13.8
2016 $311 5,208 $59.78 13.3
2017 $325 4,126 $78.67 16.3
Our share repurchase program has been in place now for over 30
years. During that time, there has only been one year in which we
did not repurchase stock. That was in 1995 due to the acquisition
of American Income. Since 1986, we have spent $7.1 billion to
repurchase 79% of the outstanding shares of the Company.
Returning excess capital to shareholders is core to our business
model. As noted earlier, we have returned approximately 71%
of our net income to shareholders through dividends and share
repurchases since 1986. While share repurchases have been the
most efficient use of excess capital over the years, we continually
evaluate alternative uses to help ensure we maximize shareholder
value.
 * The Company made a one-time adjustment to 2017 net income as a result of remeasuring
deferred tax assets and liabilities at the newly enacted corporate rate.
Excluding the tax reform adjustment, the ratio of total cash returned to net income
would have been 68% in 2017 and 82% over the past ten years.
TAX REFORM
We want to add our thoughts here regarding the impact of the tax
reform legislation recently enacted. Overall, tax reform will be very
positive for Torchmark in the long term due to taxation of future
profits at the new 21% tax rate. However, due to various other
provisions in the new law that impact taxable income, we anticipate
our cash tax savings in the short term from the new law will only be
about $5 million to $10 million per year. Over time we will begin to
see more benefit.
The impact of tax reform on required capital levels is yet to be
determined. Regardless of the positions the rating agencies and
regulators eventually take on this issue, we have a great deal of
flexibility. We plan to take actions that will be in Torchmark’s best
interests and will provide the best overall value for our shareholders.
12 | TORCHMARK CORPORATION | LETTER TO SHAREHOLDERS
Note: Torchmark cautions you that this Letter to Shareholders may contain forward-looking statements within the meaning of the federal
securities law. These prospective statements reflect management’s current expectations, but are not guarantees of future performance.
Accordingly, please refer to Torchmark’s cautionary statement regarding forward-looking statements and the business environment in which
the Company operates, contained in the Company’s Form 10-K for the period ended December 31, 2017, found on the following pages and on
file with the Securities and Exchange Commission. Torchmark specifically disclaims any obligation to update or revise any forward-looking
statement because of new information, future developments, or otherwise.
We believe Torchmark has a very bright future. Many times we hear that life insurance is a
mature market with limited growth potential. For Torchmark, this is simply not true. There
is great need for our products in the middle-income market. Our task is to further reach out
and demonstrate that need to potential customers as we grow our distribution channels.
We have essentially been selling the same products in the same markets for the past fifty he past fifty
years and the need for our products today is greater than ever. Due to the tremendous
amount of data and experience Torchmark possesses with these products and this market,
we are uniquely positioned to thrive and build shareholder value well into the future.
Thank you for your investment in Torchmark.
CONCLUSION
LARRY M. HUTCHISON
Co-Chairman and
Chief Executive Officer
GARY L. COLEMAN
Co-Chairman and
Chief Executive Officer