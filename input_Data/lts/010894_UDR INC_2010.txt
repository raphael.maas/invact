Dear Shareholder,

The past year was a welcomed change to the challenges the world has recently endured. A new normalcy seems to
have returned in which consumers and companies alike are learning to adapt to greater volatility in their everyday
lives. UDR has adapted as well, but let me be the first to say that does not mean we have settled for complacency. I
can say with great certainty that we will look back on 2010 as a year that UDR capitalized on some tremendous
opportunities. Specifically, we improved the business by executing $2.4 billion of transactions, delivering
progressively better operating results throughout the year, while positioning the Company for further growth. We
enter 2011 a stronger more flexible company that will continue to capitalize on opportunities that deliver long term
value to our shareholders.
Our operating results for 2010 reflect the impact of the recession. However, we were encouraged by the
improvements in our operations as the year progressed, and are optimistic for 2011 as we begin the year with pricing
power in a majority of our markets.
Our long term strategic focus is on three objectives: strengthen the balance sheet, strategically grow the enterprise
and make continued investments in our operating platform.
Strengthened balance sheet. We raised $468 million of common equity at a weighted average price of $19.06 per
share. We were also very active during a historically low interest rate environment completing $1.2 billion of debt
transactions. These strategic decisions directly reduced the overall weighted average interest cost of our $3.6 billion
of debt by 30 basis points to 4.2% and increased our fixed-charge coverage ratio, adjusted for non-recurring items,
to 2.3 times from 2.0 times.
Expanded the portfolio. We completed $435 million of acquisitions consisting of 1,374 homes, with average
monthly income per occupied home of $2,000, and one land parcel with the potential to develop approximately 315
additional homes. We also entered into a $2.4 billion joint venture with MetLife, Inc. that owns 26 communities
with 5,748 homes and 11 land parcels with the potential to develop approximately 2,300 additional homes. Total
monthly income per occupied home for these communities is greater than $2,250. The transaction with MetLife,
Inc. was a significant step forward in improving the overall quality of our portfolio.
We continue to focus on development and redevelopment as a means of enhancing our portfolio. This past year we
completed 1,849 homes at a total cost of over $380 million. Additionally, we have 1,708 homes in seven
communities that are still under development or redevelopment representing an estimated cost of $430 million.
In the future, we will continue to grow our presence in urban markets that have favorable job formation, low singlefamily home affordability and a favorable demand/supply environment for multifamily housing. In 2010, we
expanded our portfolio through investments in Northern and Southern California, Baltimore, Boston, Seattle and
Washington D.C.
Continue to transform operations. Our investment in technology is twofold: a commitment to deliver the best
experience for our residents while at the same time increase our operating efficiency. We will continue to make
considerable investments in the training and development of our associates to ensure that technology and customer
service together create a flexible service-oriented culture. As a result, we saw electronic payments by residents
increase from 62% to 79% and service requests entered via our resident portal increase from 40% to 79%, year-overyear. We believe that the benefits of these technology initiatives will provide our residents with a higher level of
convenience and service as well as help us drive higher revenue and reduced expenses leading to greater operating
margins. 

Since unveiling the third generation of our apartment search website visitor traffic is up nearly 35%. Additionally,
we completed the rollout of our online renewal program and UDR residents now have the ability to renew their
expiring lease entirely online. The results are encouraging as we have improved our ability to manage resident
turnover and enhance our pricing power.
2011 Outlook and Beyond: The current operating environment is attractive to our business and as you will read
below you will understand why we are excited for the future of our Company.
Decline in homeownership. From its peak in 2004 of nearly 70%, the rate of single-family homeownership is
expected to decline and stabilize closer to the long-term average of 65%. This precipitous decline in
homeownership is expected to increase the number of renters by more than four million.
Positive change in demographics. The growing population of 18 to 35 year-olds will be a significant driver in
apartment demand as this demographic has a 60% propensity to rent. Over the next ten years, the population of
those in their primary renter years is expected to increase by almost five million.
Record low supply of new apartments. New apartment home construction is at a 50- year low. Additionally, the
supply of new apartments coming to market in 2011 is projected to be approximately one-third of the long-term
average.
Looking to the Future. The combination of an industry-leading operating platform and an experienced
management team, along with favorable supply and demand tailwinds, positions us favorably to grow our cash flow
and continue to create shareholder value. Based on our expectations for strong cash flow growth in 2011, our Board
of Directors increased the common share dividend declared in 2011 by nearly 10% to $0.80 per share.
Finally, I would like to thank our more than 1,600 associates for their hard work and dedication and our customers
and shareholders for their continued loyalty and support.
We look forward to a prosperous future together.

Thomas W. Toomey
President and Chief Executive Officer