2005 was an 
extraordinary year 
for Sunoco --- with 
best ever health, 
environment and safety 
("HES") performance 
and record financial 
results. 
We achieved best 
historic performance in the areas of employee 
and contractor safety as well as air and water 
permit exceedences. Operations Excellence 
in HES is a top priority within Sunoco and the 
foundation of all that we do. 
From a financial perspective, it was the 
best year in the best three-year period for 
Sunoco since the Company focused purely on 
downstream manufacturing. The combination 
of favorable refining margins and an improved 
Sunoco asset portfolio and operational 
performance have led to three consecutive 
years of increases in earnings and share price. 
2005 income before special items* was over 
$1 billion and earnings per share was a record 
$7.36, up 75 percent from 2004. Return on 
Capital Employed was a sector leading 32 
percent. We increased our dividend by 33 
percent, reduced our shares outstanding 
by 4 percent and significantly strengthened 
our balance sheet. Share price increased 92 
percent for the year. 
During the 200305 period we have: 
 generated approximately $2 billion in 
earnings before special items* and 
provided a 400 percent total return to 
our shareholders; 
 
 grown each of our businesses and 
increased the total assets of the Company 
from $6.4 billion to $9.9 billion; 
 funded a capital program (including 
acquisitions) of approximately $3.1 
billion and made over $1.1 billion in 
share repurchases...while reducing the 
net debt-to-capital ratio (per our revolving 
credit agreement) from 45 percent at 
December 31, 2002 to 17 percent at 
yearend 2005; and 
 increased our dividend by 60 percent 
(100 percent with our second quarter of 
2006 increase) and reduced our net 
shares outstanding by 13 percent. 
Among the many factors contributing to our 
strong performance over this timeframe, 
two clearly stand out: (1) the structural 
improvement that has occurred in the refining 
industry, and (2) the continuing gains in 
operating performance achieved by Sunoco's 
Refining and Supply business. 
After many years of overcapacity, growing 
refined product demand is increasingly 
pushing the limits of U.S. and global refining 
capacity. Refined products, which in the 
oversupplied market of the 1980s and 90s 
were largely priced on a cost basis, are now 
selling more on value --- how much the market 
will pay for an essential but limited product. 
While still cyclical and volatile and impacted by 
fluctuations in inventory levels, tightness in the 
supply/demand balance for refined products 
looks likely to continue as demand grows and 
significant new supply takes time to bring on 
line. With new, tighter product specifications 
for gasoline and diesel fuel and the expected 
elimination of MTBE from the gasoline pool, 
2006 is set to be a particularly challenging year 
for the industry. 

In any margin environment, but particularly 
in those we have experienced over the past 
several years, the significant improvements 
made in our refining operations have been 
especially beneficial. We have been on a 
consistent trend of improvement in key 
measures of safety, reliability and energy 
efficiency. In 2005, our refinery throughput 
and production were at record levels, with 
crude oil and conversion unit utilization levels 
at 98 percent and 101 percent, respectively. In 
a year of record refining margins, we produced 
eight million barrels more than we did in 2004 
--- truly "getting more from existing assets." 
While Refining and Supply has led the way, 
our other businesses --- Retail Marketing, 
Chemicals, Logistics and Coke --- have also 
made advancements in the quality and 
competitiveness of their asset portfolios and 
provide a good base of more steady earnings 
and cash flow for the Company. Over the 
past three years, total earnings from these 
non-refining businesses have averaged $213 
million per year. In a more normalized market 
environment for crude oil prices and refining 
margins, and with continued development 
of our coke-making technology and business, 
we would expect these businesses to show 
increasing earnings power in the future. 
We are also distinctive by our share repurchase 
activity, which has been a consistent and 
material element of our overall strategy. Over 
the past six years, we have spent almost $1.7 
billion towards share repurchases --- over 60 
million shares at an average price of $27.84 per 
share --- and reduced our shares outstanding 
by 26 percent. Our biggest investment has 
been in Sunoco --- and it has been an important 
contributor to growing shareholder value. 
We believe there is still significant 
improvement to be gained by continuing 
to execute our core strategies. Substantial 
investment and organic growth is planned 
for our Refining and Supply business --- $1.9 
billion over 200608 to increase capacity by 11 
percent (100,000 barrels per day), increase our 
crude oil flexibility, improve our product yields 
and reduce our overall breakeven margin. In 
any margin environment, this program will 
significantly strengthen our refining operations. 
We will continue to be conservative regarding 
acquisitions and the management of our 
financial capacity as we retain a healthy 
awareness of the unpredictability and fragile 
tipping point from good to poor markets in 
our businesses. The market values, and our 
shareholders expect, such behavior. 
Our success over the past few years could not 
have been achieved without the hard work and 
dedication of our employees. They remain the 
cornerstone of Sunoco and I thank them for 
their contribution. I would also like to thank 
our Board of Directors for their thoroughness 
and counsel and you, our shareholders, for 
your continued support and confidence. I also 
offer a special thanks to Rick Lenny who will 
be leaving our Board in May. His wisdom and 
energy will be missed. 
While "staying the course," we recognize 
the market will change and provide various 
challenges and opportunities. We are confident 
we can approach either from a stronger 
position than ever before. 



