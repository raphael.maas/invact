To Our Stakeholders:
I am pleased to report that
Tyco International delivered strong
performance in fiscal 2011.
We achieved solid growth in our
revenue, operating margin and
earnings while further strengthening
our core security, fire protection
and flow control businesses.

Our progress in 2011 and over the four previous years positioned
us to take an important step to promote future growth and value
creation. In September 2011, we announced plans to separate
Tyco International into three independent, publicly-traded
companies. Following a thorough assessment by our Board of
Directors and senior leadership, we concluded that creating three
companies from our current portfolio of businesses was the best
way to unlock the full potential of Tyco International.
Before discussing these exciting plans in further detail,
let me highlight some of our companys many accomplishments
during 2011.
Business Performance
Fiscal 2011 revenue of $17.4 billion grew 9%, excluding the results
of our electrical and metal products business. Our organic
revenue (which excludes the impact of acquisitions, divestitures
and foreign currency) grew 4%, reflecting improving conditions
in a number of our end markets. Operating margins expanded
160 basis points to 11.9%. This strong revenue and margin
expansion helped drive earnings sharply higher. Income from
continuing operations grew 39% to $1.6 billion, or $3.27 per

diluted share, from $1.1 billion, or $2.31 per diluted share. Excluding
special items, income from continuing operations totaled $3.24
per diluted share for 2011 compared to $2.68 for 2010a 21% gain.
We continued to generate strong cash flow, finishing the year
with $1.4 billion in cash.
Operational and Strategic Accomplishments
We continued to invest in growth opportunities last year by
increasing our capital spending 12%. We also increased our
research and development spending by 12% to support the work
of our R&D teams around the world in developing new technology
and innovative solutions for our customers. Additionally, we
increased our sales and marketing spending by 5%, focusing on
strengthening our presence in key vertical and emerging markets
and building awareness around product offerings.
One such offering is ADT Pulse, our interactive security platform
that we introduced in the North American residential market
in fiscal 2010. We are now introducing ADT Pulse to our small
business customers to strengthen our position in this important
segment of the security industry. ADT Pulse makes it possible
for anyone with a web-enabled device to remotely access and
control not only a security system, but also lights, thermostats,
video cameras and other appliances in their home or small
business. In our fire protection business, we launched the Simplex
4100ES (eServices) fire alarm system, a product featuring
Internet-based technology thats designed to simplify installation,
improve serviceability and reduce life-cycle costs for engineers
and end-user customers. Groundbreaking features include
remote service diagnostics, remote program downloads over
the customers IT network, and mass storage of vital system data
within the systems panel. The Simplex 4100ES represents one
of our most important fire alarm system launches in recent years
and has been well received by our customers.

Our investment in growth opportunities also included several
strategic acquisitions. In our security solutions segment,
we purchased Signature Security to strengthen our sales,
installation and service capabilities in Australia and New Zealand.
Additionally, we acquired Visonic Ltd., a leader in developing
advanced wireless technology for the security industry. Visonic
has a strong presence in Europe, the Middle East and Africa. In our
fire protection segment, we added Chemguard, Inc., broadening
our global fire suppression offerings while augmenting our
presence in the Middle East and Latin America. And in the flow
control segment, we acquired a 75% stake in KEF Holdings,
gaining a local valve manufacturing presence in the Middle East,
while broadening our product and service offerings in that region.
Meanwhile, we took a further step to streamline our portfolio
with the sale of a majority stake in our electrical and metal
products business. Through these portfolio moves, we
strengthened our competitive position in our core platforms while
increasing our recurring and service revenues that are sources
of consistency and stability. These revenues grew to 45% of total
revenues last year.
During the year, we continued to focus on improving productivity
through cost-containment initiatives and restructuring programs.
In recent quarters, productivity gainsin conjunction with the
gradual upturn in the global economy and the resulting increases
in organic growthhave fueled the improvement in our
operating margin.
We also made very good progress last year in the area of capital
allocation. In addition to funding organic growth and acquisitions,
we continued to return excess cash to shareholders. We used
$1.3 billion of cash to repurchase 6% of our outstanding shares
and increased our annual dividend by approximately 20% during
fiscal 2011.
Positioning Tyco for the Future
We are on track with our plan to separate Tyco International into
three independent, publicly-traded companies, consisting of a
standalone ADT North America residential and small business
security company; a standalone flow control company; and a
standalone fire protection and security company. Each will have
highly-skilled employees and talented, experienced leaders. All
three will have industry-leading positions in large and fragmented
industries. Importantly, the new companies will have greater
flexibility to pursue their own strategies to achieve growthboth
organically and through acquisitionsthan they would under
Tycos current corporate structure. Pages 5 to 7 in this report
provide overviews of the proposed new companies.
Each company will have its own independent board of directors.
We expect that a number of current Tyco board members will
serve on the boards of the three companies. Upon completion of
the transaction, I expect to become non-executive chairman of
the fire protection and security company, a director of the flow
control company and a consultant to the ADT North America
company. In addition, current Tyco directors are expected to
serve as non-executive chairmen of the flow control company
and the ADT North America company.
With all three businesses well positioned to operate as separate
entities, we expect the transition to proceed smoothly and
seamlessly. The separation of Tyco International into three
separate companies requires your approval. We plan to hold
a special general meeting of shareholders in the fourth quarter of
2012; pending your approval, the new companies are expected
to begin operations by October 2012.
In Closing
Tycos separation into three standalone companies should truly
unlock the potential of our businesses. I am proud of the progress
we have made on many fronts since I came on board in 2002.
It was difficult then to envision what this company has become
todaya global leader in all of its businesses with a reputation
for transparency, honesty and integrity. That progress would
not have been possible without the passion, hard work and
commitment of our more than 100,000 employees around the
world. To our dedicated employees and all Tyco stakeholders,
I offer my most sincere thanks for your continued and loyal
support of our company.
Edward D. Breen
Chairman and Chief Executive Officer