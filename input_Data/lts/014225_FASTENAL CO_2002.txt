
                                        President's Letter to Shareholders
For Fastenal, 2002 was a better year than 2001 but not a great one. We were able to grow both our sales and our earnings in
2002. Sales in 2002 were $905.4 million, a 10.7% increase over sales of $818.3 million in 2001. We continued to see
improvement in our sales growth throughout much of the year. The industrial economy was difficult to read in 2002. We saw a
slow but continual improvement in our business for the first six months of the year, then saw a weakening in July. After July,
sales slowly gained momentum through November, again slowing down around the holidays. Despite the uncertain economy,
we opened 144 stores increasing our total to 1,169 at year-end.
Our net earnings in 2002 were $75.5 million, an increase of 7.7% over $70.1 million in 2001. This was made possible by the
increase in sales and the gain we had on the sale of our DIY Business (see description on page 5). Our people did a good job of
expense control in 2002, but we had some large unexpected increases in our expenses for general self-insured claims late in the
year. These expenses were to cover changes in claim estimates from 2002 and earlier years and should not be reoccurring.
In October, we completed the sale of our DIY Business to The Hillman Group. We learned a lot about retail sales and
customer service in the 13 months we owned this business. Based on this, we felt Fastenal could best service its industrial and
construction customers by applying what we learned to our Fastenal stores. We wanted to focus all of our energy and resources
on improving our stores.
After purchasing the DIY Business, we put a team of experienced Fastenal employees in place to lead the business. These
individuals spent a great deal of time in hardware and lumber stores, setting displays and merchandising product. They soon
came to us with many ideas that they believed could improve our stores. These ideas centered on stocking a broader inventory
in our stores and displaying it so our customers can service themselves. The team believed this would improve the efficiency
for both our people and our customers. Our sales force, marketing department, and finance people researched this further and
concluded it was worth the investment to assemble a team and develop this concept. Internally, the concept is known as the
Customer Service Project, or CSP. Our product development and marketing people, along with the help of our suppliers, spent
months determining which products should be represented and how they should be merchandised. In the next 12 months you
should see significant changes taking place at our Fastenal stores. We are re-merchandising them with an open floor design, a
broader product selection, improved signage and improved store fixtures, and we are making system changes that will speed
up the invoicing process.
Since June 2002, we converted approximately 75 of our existing stores to and opened approximately 80 stores with this new
format; each time getting a little better at the process and achieving better results. The early returns on our investments look
favorable. Based on these results, and our continued commitment to improve our customer service, we intend to continue to
move forward on this project. Our plan is to convert approximately 80 stores per month and to open all new stores with the
CSP format in 2003. Our goal is simple: to position our stores to be the best industrial products distributor in each of the
markets we are in.
The Fastenal School of Business experienced continued success and growth in 2002, both in the number of people served and
also in the programs offered. The number of employees attending Fastenal School of Business courses in 2002 increased
approximately 50% from 900 in 2001. An accelerated training program for high potential employees seeking store manager
positions started mid year and continued to expand. The school will continue to expand its course offerings in alignment with
our business and employee needs.
For our National Accounts group, 2002 was a good year. We added 65 new accounts, bringing our total number of national
account customers to 211. We continued to see improvement in our sales growth throughout much of the year. In the first
quarter, our national account sales grew 20.4% over the previous year, and in the fourth quarter such sales grew 30.9%. In
2002, national account sales represented 20.7% of our overall sales. Our efforts to sell to government accounts were also very
successful. Sales to the government grew 53.1% over 2001. In 2002, government sales represented 1.7% of our overall sales.


In the third quarter, we started a new specialty sales group focused on the largest construction companies in North America.
This is a small group of experienced employees who focus their efforts on developing national sales agreements with large
construction companies.
Our marketing department continued to work on product development in 2002; adding more than 63,000 new parts to existing
product lines. Late in 2001, we added raw materials (or metals) to our product offering, and in the second half of 2002 we
added a selection of metals to 25 stores. This selection consists of over 300 items in different materials, shapes, and sizes. This
product offering is focused on selling smaller quantities to the maintenance market.
Although the Internet is not changing our business like some thought it would, we continue to use it to improve our business.
In 2002 we introduced our new electronic billing software. This project was developed internally and allows our customers to
access their account information 24 hours a day. This program has received great reviews from our customers. In December
we introduced a new website designed for recruiting and screening prospective employees. The initial results have been very
promising. Take a look at www.fastenal.com.
During 2002 we purchased two new distribution centers. The first is a 198,000 square foot facility in Atlanta, Georgia that will
open in the first quarter of 2003. This facility will replace our existing distribution center in Atlanta, which is 54,000 square
feet. The second is a 62,000 square foot facility in Kitchener, Ontario. This facility will be our first distribution center in
Canada and is expected to open in the second quarter of 2003.
Although the results we reported for 2002 were less than great, I believe the efforts put forth by the Fastenal team were,
indeed, great. I want to thank every one of them. We are better positioned than we have ever been to grow our business, to
grow the opportunities for our employees, and to create better returns for our shareholders.
Thank you for your support and your belief in the Fastenal Team.

    