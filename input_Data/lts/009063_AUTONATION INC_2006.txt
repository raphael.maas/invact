2006 was a year of great strategic importance for
AutoNation. It marked our 10th year of operation
as an automotive retailer, a decade in which we
proudly defied a critical consensus that a company
like ours could never be built  or that, if it could be
built, it couldnt be sustained. Not only did we
establish ourselves as the largest retail enterprise
in the history of automotive retailing, but we have
grown considerably since our founding.
Todays automotive retail environment is
remarkably dynamic, with technological advances
influencing consumer expectations, energy
concerns affecting consumer behavior and
manufacturing upheavals reshaping the retail
landscape. Success in such an environment is
more than a matter of accommodating change;
its a matter of anticipating it  and todays
AutoNation is clearly up to the task.
In fact, weve put in place a series of initiatives
over the past 12 months that have been specifically
calibrated to drive our operations forward for
years to come. Foremost among these is the
Smart Choice program. Under Smart Choice,
customers receive an easy-to-read sales menu
that lists everything from the purchase price to
financing options to monthly payments and more.
Its the clearest way that weve found to present
the core information that customers need to
make an informed decision, simplifying the buying
process, empowering customers and providing
full disclosure of transaction terms where 

theyre critically needed. Our pilot program in
South Florida was so well received by the buying
public that its now being rolled out to all of our
stores nationwide.
We credit efforts like this, efforts that emphasize
our customer-focused sales process, for a sound
financial performance in a year that was particularly
challenging for our industry. In fact, despite highly
volatile oil prices and a number of significant
setbacks for domestic manufacturers, AutoNations
results were impressive enough that our peers,
analysts and automotive executives named us the
countrys most admired automotive retailer in
FORTUNE magazines annual poll for the fifth
time in the past six years.
Such admiration is not surprising when seen
in the context of our accomplishments over the
course of the year. Consider, for example, that
we sold nearly 600,000 vehicles, a notable figure
in light of the difficult sales environment. Our
full-year adjusted earnings per share from
continuing operations of $1.54* represented an
8% increase over the previous years $1.43*,
and our annual revenue of $19.0 billion was 1%
higher than the prior year.
2006 also saw continued progress in our strategy
of shifting our store mix towards higher-volume
import and luxury brands, a strategy driven largely
through our acquisition and divestiture program.
In 1999, approximately 60% of our new vehicle
revenue was attributable to our domestic franchises

(consisting of General Motors, Ford and Chrysler franchises), while 40% came from import and luxury franchises. By
2006, we had essentially inverted those figures, with 62% of new vehicle revenue coming from import and luxury stores
and 38% coming from domestic franchises.
At the same time that weve been rebalancing our brand mix, weve also advocated that manufacturers of vehicles
in all classes and at all price points adopt energy efficient technology as widely as possible. AutoNation has earned
a leadership position among retailers for promoting a sound national energy policy and urging the development of
plug-in hybrids and other alternatives to the traditional combustion engine, and we will continue to endorse such
efforts in the years ahead. We are committed to actively advancing the goal of energy independence, and we expect
that all of us will benefit from the economic, ecological and geopolitical advantages that such independence fosters.
While we expect a continued challenging automotive retail environment in 2007, we believe we have positioned the
company well for the future. AutoNation remains the only automotive retailer with a position in the S&P 500.
Were ranked 115 on Fortune magazines list of the countrys largest companies. We employ more than 26,000
associates across 16 states. And weve done it all in only 10 years of operation.
We owe a particular debt of gratitude for such remarkable success to the active participation of our dedicated and
distinguished board of directors. We have been the beneficiaries of exceptional guidance and insight from our board
at every turn, and we thank each of our directors for their continued dedication.
Of course, as our company evolves, so does the composition of our board. This is the final year we will be privileged to
have Edward S. Lampert and Irene B. Rosenfeld serve with us as board members. At the upcoming annual meeting of
shareholders, Mr. Lampert has chosen not to stand for reelection to the board so that he may concentrate his efforts
on the stewardship of ESL Investments and Sears Holdings. And Ms. Rosenfeld has declined to stand for reelection as
a board member so that she may more directly focus her attention on her role as chairman and chief executive officer
of Kraft Foods.
We benefited tremendously from the guidance of Eddie and Irene over the years. We wish them both well in their future
endeavors and remain fully committed to upholding the same high standards of ethics, integrity and professionalism
they exemplified while serving with our company. Were confident that with your ongoing support along with the
insight, expertise and dedication of our associates, AutoNation will continue to deliver the performance that you, our
coworkers and our customers expect and deserve.

THE AUTONATION EXECUTIVE MANAGEMENT TEAM
Mike Jackson Michael E. Maroone Jonathan P. Ferrando Mike Short
Chairman & President & Executive Vice President, Executive Vice President &
Chief Executive Officer Chief Operating Officer General Counsel & Secretary Chief Financial Officer
