t o o u r s h a r e h o l d e r s :

Our accomplishments in calendar year 2008 were the most significant in our company�s 29 years. We completed the largest
merger in the history of our industry; on a non-GAAP basis, we delivered record net revenues and operating income, with
double-digit growth; and we significantly strengthened our portfolio of leading entertainment franchises.
These accomplishments follow 16 years of strong performance by Activision, Inc. Since 1998, we had a cumulative average
annual growth rate in our share price of 25% as compared to the Standard & Poor�s 500, which decreased at an average
annual rate of 1%, during that same period of time. Over those 10 years, we also outperformed every other third-party
video game company in providing superior shareholder returns.
On July 9, 2008, we completed our transaction with Vivendi to create Activision Blizzard, the world�s largest and most
profitable online and console video game company. Activision Blizzard was formed by combining Activision and Vivendi
Games, Vivendi�s interactive entertainment business, which includes Blizzard Entertainment��s top-selling PC franchises�
Warcraft�, StarCraft� and Diablo� and World of Warcraft�, the world�s #1 subscription-based massively multiplayer online
role-playing game. Activision Blizzard is now a leader in both console and subscription-based online games.
Our focus on well-defined market opportunities combined with strong global execution enabled us to deliver outstanding
financial results for calendar year 2008. In the past, we reported our results to you principally on a GAAP basis. However,
as a result of our merger, which required reverse merger accounting, Activision�s historical financial results are included from
the date of the business combination, July 9, 2008 onwards, but not for prior periods in our GAAP financial filings. In
addition, increased online functionality for certain of our PC and console games requires us to defer revenue and costs related
to the sale of these games over an estimated service period. While the adoption of this accounting treatment does not change
the economics of our business as the cash related to the sale of a game is still collected upfront, it does affect year-over-year
comparisons. As such, we review non-GAAP financial measures in managing our business and assessing our operating
performance. We understand that many of you will also want to consider year-over-year comparisons. Therefore, we believe
it is helpful for us to discuss our financial performance on a non-GAAP basis for year-over-year comparisons. A reconciliation of our GAAP to non-GAAP financial measures can be found at the back of this annual report.
For 2008, Activision Blizzard�s non-GAAP comparable-basis segment net revenues were $5 billion, marking our 17th
consecutive year of net revenue growth. We had the most profitable year in our history with non-GAAP comparable-basis
segment operating income of $1.2 billion and non-GAAP operating margin of 24%.
During the year, we successfully integrated Vivendi Games� operations into Activision and we are on track to attain the top
end of $100�$150 million in merger integration and cost savings exceeding our original range by $50 million. We ended
2008 with approximately $3.1 billion in cash and investments and no debt, which provides us with the financial flexibility
to pursue opportunities as they arise.
In these difficult and volatile economic times, our cash gives us a competitive advantage. As we have always done, we will
use our capital wisely for the benefit of our shareholders. This includes repurchasing our common stock under our authorized
$1 billion share buy-back program. We had purchased approximately $126 million of our common stock at an average price
of $9.68, as of December 31, 2008.
In calendar 2008, Activision Blizzard excelled. Our results are due to the hard work and exceptional skills of our more
than 7,000 worldwide employees, our continued commitment to delivering quality entertainment experiences to consumers
around the world and the dedication of a solid experienced management team led by Mike Griffith, Mike Morhaime and
Bruce Hack.
Now that we have successfully completed the merger and integration of Blizzard Entertainment and Activision, Bruce Hack,
who served as our Chief Corporate Officer, has decided to return full time to New York City. He has handed over the reins
of Chief Corporate Officer to Thomas Tippl, our Chief Financial Officer. It is a great reflection on the strength of our
organization that we are able to fill Bruce�s role from our own executive team.

m a r k e t g r o w t h e x p e c t e d t o c o n t i n u e
As technology continues to transform the media landscape, global consumer demand for interactive virtual- and digitalcontent has increased. We believe that this growth can continue even in these unprecedented economic conditions.
In the U.S., the average number of movie tickets per person declined 6% from 2003 to 20071
. From 2004 to 2007, hours of
network television consumption per person declined 6%2
 and the sales of recorded music in a physical format declined 12%3
.
Yet, the consumption of digital media over the same period increased 107%4
 and video games grew by 46%3
. Not only are
video games growing, but they also are capturing a larger share of all media and offer a cost-per-hour value that is more
appealing than other forms of entertainment.
Today, there are more than 400 million hardware consoles in the market worldwide. In 2008 alone, the North American and
European software market totaled $24 billion, an increase of 20% year over year, according to The NPD Group, Charttrack
and GfK. We believe this growth comes from new audiences recognizing how differentiated games are from movies and
television. Consumers want to compose their own music, lead squads into combat, embark on magical quests and embody
characters in environments that are worlds apart from their everyday lives.
The dynamic growth in interactive media provides enormous opportunity. What sets us apart from our competitors in
taking advantage of it is our entrepreneurial inventiveness and our ability to marry consumers� desires with creative game
experiences, as well as an integrated rewards and compensation program that enables our employees to remain focused on
strategic priorities. This discipline has allowed us to build a company that turns ideas, artistry, game concepts and brands
into engines of growth.
t h e p o w e r o f o u r p o r t f o l i o
The tremendous shift in consumer consumption of media continues to confer added importance to brands. According to The
NPD Group, Charttrack and GfK, 90% of the top-ten best-selling games worldwide in calendar 2008 were based on proven
franchises. Few companies today can match the breadth and diversity of Activision Blizzard�s portfolio of proven entertainment franchises.
For the calendar year, we generated 75% of our revenues from owned franchises. We had three of the best-selling console and
PC franchises in North America and Europe�Activision�s Guitar Hero� and Call of Duty� and Blizzard Entertainment�s
World of Warcraft�and four of the top-10 console and handheld games across all platforms�Call of Duty: World at War�,
Call of Duty 4: Modern Warfare�, Guitar Hero III: Legends of Rock� and Guitar Hero World Tour�. Additionally, World
of Warcraft: Wrath of the Lich King� was the #1 best-selling PC game worldwide for the calendar year and the fastestselling PC title in the history of the industry.
In 2008, we once again grew the Call of Duty franchise with Call of Duty: World at War selling more than eight million
units worldwide in just November and December, according to The NPD Group, Charttrack and GfK. The franchise continues to top the Xbox LIVE� Marketplace charts with millions of people playing Call of Duty games online. During the
calendar year, life-to-date sales of the franchise exceeded $1 billion and the popularity of Call of Duty has never been greater.
Our Guitar Hero franchise continues to redefine gaming by delivering innovative entertainment experiences that tap into the
universal dream of being a rock star. The convergence of gaming with the passion of music has brought Guitar Hero to the
forefront of entertainment. Guitar Hero is not only changing how video games are played, it is also introducing recording
artists to new audiences and letting consumers interact with music in a whole new way. According to Nielsen SoundScan, one
artist whose songs appeared in Guitar Hero III: Legends of Rock experienced as much as 800% in download sales growth.
During the calendar year, the franchise debuted on the Nintendo DS� with Guitar Hero: On Tour�, which resulted in our
largest North American launch ever for the DS platform. We also introduced a cooperative band experience for consoles with
Guitar Hero World Tour adding a drum controller and microphone to the popular guitar controller. The game set a new 

standard for interactivity through its innovative Music Studio. For the first time, players can lay down and mix their own
music tracks, and then share their user-generated music with other players via our proprietary music sharing platform
GHTunes�. To date, there have been more than 230,000 user-generated songs posted to GHTunes and more than 35 million
songs from a variety of artists downloaded by Guitar Hero players to supplement the music that comes with the game.
Today, World of Warcraft is the world�s most successful massively multiplayer online role-playing game, with more than 11.5
million subscribers. World of Warcraft is among a handful of Western entertainment properties that have been successful in
Asia. Blizzard Entertainment�s highly profitable subscription-based online model virtually eliminates piracy issues that have
traditionally hindered Western entertainment in the region. In February 2009, Screen Digest reported that global broadband penetration is expected to reach 515 million households by 2012, a 75% increase over 2007, and Asia is expected to
grow at a faster rate than the rest of the world. Blizzard Entertainment is established in the three regions which are expected
to have the most growth, North America, Europe and Asia, and continues to seek opportunities to introduce the game into
new emerging markets, as it did in Russia and Latin America in 2008.
In calendar 2008, the global strength and reach of our product portfolio translated across all platforms. According to The
NPD Group, Charttrack and GfK, worldwide for the calendar year, we had:
� two of the top-10 titles in dollars on the Nintendo� Wii�,
� four of the top-10 titles in dollars on the Xbox 360� entertainment system from Microsoft,
� three of the top-10 titles in dollars on the Sony� PLAYSTATION� 3 video game console,
� four of the top-10 titles in dollars on the PC and,
� the #1 best-selling third-party game on the Nintendo DS.
d r i v e n b y o u r va l u e s
We have never made the mistake of celebrating current achievements at the expense of future performance. Therefore,
during these challenging economic times, we will continue to aggressively optimize our costs and drive operational
efficiencies throughout our organization, while taking advantage of opportunities to strengthen our leadership position.
We have stayed on course and delivered on our promise of revenue growth, profitability and cost efficiencies. With that
same disciplined focus, we will continue managing our finances to grow our business and deliver superior long-term returns
to our shareholders.
The employees of Activision Blizzard are people whose passionate commitment to excellence characterizes their lives. They
choose to work here because we are a company that nurtures the imagination, values creative expression and rewards success.
More than any other asset, it is their integrity, insight, innovation and dedication to what they do that is the reason that we
enjoy our reputation as a premier entertainment company.
Today, our business�the business of creating quality interactive entertainment content and distributing it worldwide�
spans the most dynamic parts of the digital economy with potential for explosive expansion in the years ahead. During a year
of economic uncertainty, Activision Blizzard distinguished itself from its competitors, and while we are proud of having
fulfilled that promise in 2008, we are more determined than ever to do so in the future.
Sincerely,
Robert Kotick Brian Kelly
Chief Executive Officer Co-Chairman
Activision Blizzard, Inc. Activision Blizzard, Inc