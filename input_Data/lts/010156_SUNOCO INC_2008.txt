To Our Shareholders:
Despite challenging market conditions and significant volatility in commodity prices, Sunocos
financial performance in 2008 was strong. The Company earned $874 million before special items*, with
earnings per share of $7.46, up 7.5 percent from 2007.
Rising crude oil prices in the midst of a weakening global economy led to income before special items
in the first half of 2008 that was just above breakeven levels. However, in the second half of the year,
commodity prices fell from their record levels in July and hurricane-related refinery outages reduced
industry supply. Sunocos operations performed well during this period and the second half of 2008 proved
profitable for all of our businesses.
Refining business captures market opportunities
While 2008 clearly highlighted the volatility of the refining market, the $515 million earned by
Refining and Supply also demonstrated the improving flexibility of our refinery operations and commercial
activities, as well as our nimbleness in responding to market demand.
We expanded our ability to process lower-cost crude oil feedstocks and reduced our purchases of
certain higher-premium West African grades. As a result, we significantly reduced our crude oil acquisition
costs compared to market benchmarks, particularly in the second half of the year.
In addition, by realizing the benefit of certain capital projects completed in 2007, we optimized
refinery operations to produce a mix of products that better reflected market demand. In 2008, we made and
sold more higher-value distillate fuel and less gasoline and residual fuel than in previous years.
Non-refining businesses deliver strong results
Market volatility in 2008 also highlighted the value of our diversified business portfolio. In the
aggregate, our non-refining businesses contributed a record $427 million to earnings.
Retail Marketing earned a record $201 million as retail margins expanded in the second half of the
year. Despite lower industry demand for gasoline and diesel fuel, Sunocos strong branded presence
continued to serve us well in the marketplace.
The $36 million earned by Chemicals in 2008 represented an improvement from last year. However,
the markets for both polypropylene and phenol products continue to be challenging.
Logistics earned $85 million due to a record contribution from Sunoco Logistics Partners L.P. (NYSE:
SXL), which continued to effectively execute its growth strategy. During 2008, SXL provided $76 million
in cash distributions to Sunoco, up more than 20 percent from 2007.
SunCoke Energy earned a record $105 million in 2008. During the year, we began operations at our
second cokemaking facility in Haverhill, Ohio and began construction on a new facility in Granite City,
Illinois that is expected to be completed in 2009.
Safe, reliable and environmentally sound operations
Our financial success has always been built upon a foundation of safe, reliable and environmentally
sound operations. Outstanding performance in these areas continues to be a core value and top priority.
In 2008, Sunoco had its best historical environmental performance in key areas such as water
exceedances, spill management, and air exceedances. In particular, Refining and Supply reduced air
exceedances by 30 percent through significant investments in emissions control technology and other plant
modifications.
Overall employee personal safety performance, however, fell below expectations despite best-ever
results for contractor safety in Refining and Supply and SunCokes coal mining operations. Employee
personal safety and process safety management are two areas that will receive even closer attention in 2009.
Sound financial position
Following a year characterized by significant turbulence in the financial markets, Sunocos financial
position at the end of 2008 remains strong. We ended the year with a net debt-to-capital ratio of 37 percent
(as defined in the covenant of our revolving credit agreement), $240 million of cash on the balance sheet
and $1.4 billion of available committed borrowing capacity, which includes approximately $200 million
available to Sunoco Logistics Partners L.P. Maintaining the Companys financial flexibility in these
uncertain economic times will continue to be a top priority in 2009.
Outlook for 2009
The coming year promises a challenging market for petroleum and chemical products as we expect
continued economic weakness and increased global refining supply. In response, we will continue
optimizing refinery operations to capture market opportunities and expect our non-refining businesses to
continue providing a solid base of earnings and cash flow.
In Refining and Supply, we expect to complete a capital project at our Philadelphia refinery that will
improve our ability to upgrade heating oil into ultra-low sulfur diesel fuel. Sunoco Logistics Partners L.P. is
expected to benefit from a recent $185 million acquisition of pipeline and terminaling assets, as well as from
other growth projects. Coke will realize a full-year contribution from the recently expanded Haverhill
cokemaking facility and is expected to begin operations at its new facility in Granite City by the end of the
year.
In addition, we are actively pursuing opportunities to create shareholder value in all of our businesses.
We intend to complete our strategic review of the Tulsa refinery, either with a sale of the facility or its
conversion to a terminal by the end of 2009. We will continue our portfolio management program to highgrade our mix of retail marketing outlets and improve returns on invested capital. We will evaluate the
market potential for a sale of the Chemicals business and continue to pursue growth and development
opportunities in Logistics and Coke. Finally, we are pursuing a disciplined business improvement initiative
across the Company that will result in meaningful savings on the way to a first quartile cost structure.
All of this will not be possible without the continued hard work and dedication of our employees. The
men and women of Sunoco are our strongest asset and nothing we aim to do is possible without them. I
would also like to specifically thank Jack Drosdick who elected to retire last August after capably guiding
the Company for over eight years as Chairman and CEO. A man of dignity and professionalism, he
delivered an invaluable contribution to the Company. Finally, I offer a special thanks to Andy Pew and Jack
Ratcliffe who will be leaving our Board of Directors in May. Their wisdom and dedication will be missed.
In the coming year, we are certain to face challenging markets that will require tough decisions and
disciplined attention to operational excellence. However, with Sunocos sound financial position, highquality portfolio of assets and outstanding employees, I am highly confident that we are prepared for further
success in 2009.

LYNN L. ELSENHANS
Chairman, Chief Executive
Officer and President