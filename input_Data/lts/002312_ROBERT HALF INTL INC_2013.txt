To Our Stockholders

Robert Half had a successful year in 2013. Leading
contributors to growth were U.S. staffing operations,
particularly our technology staffing division and our
permanent placement unit, and Protiviti, our internal
audit and consulting subsidiary. Earnings grew faster
than revenues. Net income of $252.2 million and
diluted earnings per share of $1.83 grew 20 percent
and 22 percent, respectively. By the end of the year,
the company had reported 15 straight quarters of
year-to-year earnings per share gains in excess of
15 percent. Total revenues of $4.25 billion increased
3 percent from the prior year.
The U.S. economy improved modestly in 2013. Real
gross domestic product (GDP) grew at 1.9 percent
for the full year, with third- and fourth-quarter
GDP increases of 4.1 percent and 2.4 percent,
respectively. Monthly growth in U.S. nonfarm
payrolls was steady but measured. A shrinking
labor participation rate was a contributing factor
in the jobless rate declining to a five-year low of
6.7 percent in December.
As it turned out, last years labor market conditions
were constructive for the temporary staffing industry.
Multiple periods of fiscal uneasiness helped lift
demand for contingent workers. As the year closed,
the number of temporary workers as a percentage
of the total U.S. workforce neared the all-time high
established in 2000. In fact, 10 percent of all jobs
created last year in the United States were in the
temporary help services industry.
We believe the increased penetration of temporary
professionals into the labor force is part of a broader
secular trend and reflects the appeal of these workers
as a flexible staffing option for employers. Our industry
helps businesses and other enterprises manage
variable workloads and specialized project demands.
Now, more than ever, the temporary staffing industry
brings to employers a valuable tool in controlling
costs and raising productivity.
Robert Halfs non-U.S. staffing operations were challenged
by a less favorable economic backdrop and
weaker staffing demand, particularly in Europe. Weak
conditions persisted, but early signs of improvement
began to appear late in the year. While it is too soon
to conclude that a European recovery is under way,
we believe our international businesses are wellpositioned
to benefit as those economies and labor
markets strengthen.
Financial Condition
Robert Halfs performance last year once again
demonstrated the attractive cash-generating characteristics
of our business. Cash provided by operating
activities was $309 million, up from $289 million
in 2012. Total operating cash flow produced in the
past decade totaled $3 billion.
Capital expenditures in 2013 were $54 million,
modestly below initially budgeted amounts. A
significant part of last years outlays was directed
to information technology infrastructure projects,
including software development. Spending on those
assets is consistent with our long-standing focus on
bringing technological innovation to our business.
The overriding aim of all of our capital spending is
to increase the productivity of our staff and make it
easier and more convenient for our clients and job
candidates to do business with us.
After netting cash used for investing activities, last
years free cash flow was $211 million. The past
five-year total was nearly $1 billion. We continued our
established practice of returning cash to shareholders
in 2013. We have repurchased RHI shares in the
open market yearly since 1997 and paid cash dividends
quarterly since 2004.
Last years open market share repurchases totaled
3.3 million shares at a cost of $118 million. We
ended the year with a board authorization to

repurchase 8.1 million shares. Our board of directors
declared dividends equivalent to $0.64 per share
in 2013, which consumed a total of $89 million of
cash. Last years $0.16 per share quarterly dividend
amount compared with $0.15 per share in the prior
year. The board recently upped the quarterly payout
to $0.18 per share. It was the 10th consecutive
annual increase since a cash dividend was initiated.
In the decade since our first cash dividend, distributions
have compounded at a 13 percent average
annual rate.
Our balance sheet is solid. Our largest asset is
accounts receivable at $552 million, or 37 percent
of our $1.5 billion of total assets. Our customer
base is broadly diversified, with no industry or individual
company concentration. Year-end days sales
outstanding (DSO), which represents the average
collection period for receivables, was 46.3 days.
This is in line with our historical experience. We
concluded 2013 with a cash and cash equivalent
balance of $276 million, just $12 million below the
prior-year total. We essentially have no short- or
long-term debt. The companys unlevered return on
equity for the year was 29 percent, and the 20-year
average return on equity was 25 percent.
Staffing Operations
Demand for professional staffing services remained
strong last year. Our three accounting and finance
staffing divisions represent the majority of our staffing
business domestically and internationally. These three
divisions produced a combined $2.4 billion in revenue,
or 64 percent of last years global staffing revenue.
Accountemps provides accounting personnel on a
temporary basis. It was our initial temporary staffing
offering and is our largest single business unit.
Its 2013 revenues of $1.5 billion were 41 percent
of the staffing total and were down 1 percent from
the prior year. Accountemps business last year was

limited to an extent by the termination of banking
clients foreclosure review projects  governmentmandated
investigations on behalf of customers
whose homes had been foreclosed upon by financial
institutions. The absence of that business made
for a slow start to the year. However, the revenue
stream was gradually restored by adding new clients.
Accountemps U.S. operations performed well in the
latter months of 2013.
Robert Half Management Resources provides
senior-level accounting and finance professionals
on a project basis, often for longer-duration engagements.
This business was launched in 1997. Last
years revenues of $501 million were 14 percent
of the staffing total and decreased 1 percent from
the prior year. Robert Half Management Resources
continues to partner successfully with Protiviti.
Together, they offer a unique delivery model that
combines Robert Half Management Resources
experienced professionals with Protivitis consultants
and business-solution providers to serve a broad
range of client needs.

Robert Half Finance & Accounting provides
specialized permanent placement services. It was our
original business that was created in 1948. Its 2013
revenues of $348 million produced 9 percent of the
staffing total and increased 4 percent from the prior
year. We have been in this business for 65 years
and are widely recognized by job candidates and
employers as the market leader. The networks we
have established over decades and the data we have
accumulated from candidates and clients help us do
what we do best, which is to identify experienced
professionals who are a match for our clients exacting
hiring requirements.
OfficeTeam is our specialty administrative staffing
unit. It was our first expansion business beyond
our core accounting and finance specialties and was
started in 1991. Last years revenues of $832 million
accounted for 22 percent of the staffing total and
increased 2 percent over the prior year. We have
been able to establish a solid presence in this business
over the past two decades, in part, by leveraging
our reputation in accounting and finance staffing.
Robert Half Technology was launched in 1994
and provides information technology support and
development professionals. Revenues in 2013
totaled $525 million, which represented 14 percent
of the staffing total and were 10 percent ahead of
the prior year. We have identified IT staffing as an
important growth opportunity for us and accordingly
have increased internal staff investment domestically
and in select international markets. The market
size is significantly larger than that for accounting
and finance staffing. In an increasingly digital world,
small to midmarket companies, our backbone client
base, increasingly need more sophisticated IT capabilities
when they are hiring for IT positions.
Weaker economies outside the United States limited
overall growth in companywide staffing revenues last
year. Temporary and consulting staffing revenues
make up the largest of our three reporting business
segments. Global revenues for this segment
were $3.4 billion, up 1 percent from the prior year.
That modest growth rate is based on a 4 percent
increase in U.S. revenues and a 7 percent decrease

in non-U.S. revenues. The contrast between U.S.
and non-U.S. performance also was apparent in our
permanent placement business, our second reporting
segment. Companywide permanent placement
revenues grew 12 percent in the United States and
declined 8 percent in non-U.S. locations in 2013,
which resulted in a 4 percent revenue increase
overall versus the prior year. We are seeing evidence
that select global markets may be in the early stages
of strengthening. We expect to benefit from stronger
hiring demand should a full recovery take hold.
Protiviti
Protiviti, our third reporting segment, had an excellent
year in 2013. Revenues of $528 million were
17 percent higher than those in 2012, representing
Protivitis best year-over-year gain since 2005.
U.S. revenue increased faster at 21 percent, while
non-U.S. business grew 4 percent. On a global basis,
Protiviti improved its profitability in all four quarters
and reached double-digit percentage operating
margins in the final three months of 2013.
All of Protivitis client solutions services grew in
2013, most notably IT consulting, risk and compliance,
and internal audit. Client demand was driven
by generally improving market conditions and a
more stringent regulatory environment. Protiviti also
achieved growth in its service offerings related to
operations, governance and risk. Protiviti has received
marketplace recognition and numerous third-party
awards for its consulting and technology solutions.
Protivitis 2013 operating results from non-U.S.
locations showed significant improvement. Companyowned
locations and independently owned Member
Firms serve clients through a network of 75 locations
in 25 countries. In 2013, Qatar and South
Africa joined the roster of Protivitis Member Firm
locations, and the South Korea office was converted
to Member Firm status. These moves deepened our
service capabilities, enabling us to better address
the needs of our global client base.
Protiviti continues to partner with Robert Halfs
professional staffing divisions on a blended service
delivery model. This unique approach combines
consulting and specialized staffing under one roof
at competitive prices. Global and regional consulting
competitors lack the flexible resource capabilities of
our staffing operations. At the same time, staffing
competitors lack in-house access to best-in-class
consulting methodologies like those of Protiviti.
Capitalizing on Our Strong Brand
Robert Half pioneered specialized financial recruitment.
We have a long and rich history in professional
staffing and have built widely recognized and
respected specialty staffing divisions that are
anchored by the global Robert Half brand. That
recognition did not happen overnight. We have
invested heavily for decades in supporting our
brands with robust marketing and public relations
programs. Those efforts continue with investments
in a variety of newer online channels as well as
traditional marketing channels. Our recruiters are in
the marketplace daily knowing they are backed by a
powerful global brand. In short, we believe that none
of our direct competitors can match the high profiles
enjoyed by our Robert Half brands.
Last year, we introduced a new logo and other design
updates that place greater emphasis on the Robert
Half name. The new logo was created with ease of
use and readability in mind, particularly targeting digital
media formats and mobile applications.
Looking Ahead
We are optimistic as we enter 2014. Economic
data indicate small and midsize businesses  a key
market segment for Robert Half  are beginning
to hire again. These companies know that getting
the right candidate and assignment match can be
critically important. The costs of an ill-fitting hire can
affect small businesses disproportionately compared
to larger firms that may have a deeper bench of

workers from which to draw. It is not uncommon for
small companies to be without a dedicated human
resources department. In these cases, they often
rely on our expertise and access to suitable candidates.
More often than not, they value speed and
quality over discounted pricing models.
IT staffing is an important growth opportunity for
Robert Half. We continue to invest in internal staff
committed to helping companies and other entities
strengthen their IT capabilities. As businesses strive
to keep up with technological innovations, they are
investing in customized software, IT security, robust
websites and social media. Finding the specialized
talent needed to execute these initiatives is often
challenging, especially for small companies that now
have many of the same IT needs as larger entities.
Regardless of size, virtually every firm needs a website,
wants web and data analytics, desires an intranet, and
sees efficiencies in cloud computing.
U.S. job growth for financial analysts, accountants
and auditors is expected to outpace the national
average between 2012 and 2022, according to
the U.S. Bureau of Labor Statistics. Bookkeeping,
accounting and auditing clerks are among the top
15 occupations with the highest projected number of
new jobs created by the early part of the next decade.
The increasingly stringent regulatory environment
around the globe is creating demand for both Protiviti
and staffing services. Financial services regulations
being issued under the U.S. Dodd-Frank Wall
Street Reform and Consumer Protection Act and the
demands of Basel II and III are prompting companies
to seek compliance expertise. We also see opportunities
with healthcare providers and insurance
exchanges that are managing healthcare enrollment
and services resulting from the implementation of
the U.S. Patient Protection and Affordable Care Act.
Robert Half has a lengthy record of financial and
operational success. We enjoy among the best
reputations for stability and ethical standards in our
industry. Our management team is tenured, and our
staffing and consulting professionals are passionate
about their work. They are deeply committed to
providing unparalleled service. We remain focused
on the future while staying true to our roots as a
staffing and consulting industry leader.
We would like to convey our gratitude to our
employees and management teams for their hard
work and pivotal contributions in 2013. We would
also like to thank the members of our board of
directors for the encouragement and guidance they
provide and you, our stockholders, for your contin -
ued confidence and support.
Respectfully submitted,
Harold M. Messmer, Jr.
Chairman and
Chief Executive Officer
March 14, 2014
M. Keith Waddell
Vice Chairman, President
and Chief Financial Officer
March 14, 2014