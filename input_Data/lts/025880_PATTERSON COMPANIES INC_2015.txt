To Our Shareholders:
Three years ago, we set out on a new
course to position Patterson Companies
for the future, guided by a refreshed
strategy that would re-evaluate our
portfolio of businesses, deepen
our customer commitment, improve
efficiencies and turn our financial
strength into market advantage and
long-term shareholder value. Clearly,
fiscal 2015 was a pivotal year for
Patterson Companies. We delivered
against many of our strategic
intents, finished the year strong
and set the stage for a new chapter
in Pattersons growth.
Re-evaluating our portfolio led to
acquiring the leading production animal
health distribution company, Animal
Health International, Inc. We also
explored the sale of Patterson Medical,
resulting in a definitive agreement to
sell this business executed in the first
quarter of fiscal 2016. We expect to
close on this transaction during the
fiscal 2016 second quarter. Together,
these portfolio changes allow us to
deepen our focus on our dental and
animal health businesses  and
transform the strategic growth profile
of the company.
During the year, we also made substantial
progress on our multi-year, global
information technology initiative. This
initiative will move into the critical, pilot
testing phase in the months ahead and,
when complete, will enhance both our
efficiency and ability to scale as we grow.
In Patterson Dental, growth in our
technology sales reflected the central
role we play in helping practitioners
modernize their offerings to improve
patient experiences and outcomes.
Additionally, we moved forward with
our special-markets division, expanding
our ability to meet the unique needs of
larger dental groups, and are beginning
to see early progress.
In fiscal 2015, we completed the
integration of Patterson Veterinarys
U.K. operation, National Veterinary
Services, Limited (NVS). This division is
performing well post-integration, with
an attractive growth profile, and it
affirms our ability to successfully
integrate sizable acquisitions as we
pursue new avenues for growth.
Across Patterson, we streamlined our
cost structure during the fiscal year,
which set the stage for solid profit gains
and positions us well going forward.
We also successfully transitioned the
role of CFO. We are grateful to Steve
Armstrong for his 15 years of dedication
to Patterson and to Ann Gugino for her
ability to seamlessly step into this role
and help shape the transformation we
are pursuing.
Finally, Patterson Companies delivered
on our financial guidance, generating
adjusted diluted earnings per share at
the higher end of our stated range.
Achieving our milestones  turning
strategic intention into tangible action
 is how we are delivering results and
positioning Patterson for growth.
In fiscal 2015, all three of our businesses
posted healthy results. Consolidated
revenues reached $4.4 billion, up 8.7
percent on a constant currency basis.
Adjusted for one-time transaction costs,
diluted earnings per share rose to $2.27.
We also made significant strides in
improving efficiency and operating cash
flow and returned $129.3 million to
shareholders in dividends and share
repurchases. We retain a strong balance
sheet, which gives us the flexibility to
pursue our growth objectives.
PATTERSON DENTAL
Patterson Dental contributed nearly
60 percent of total sales in fiscal 2015.
The pace of technology sales growth,
in particular, during the year further
supports our belief that dentists want to
modernize and invest in technology that
improves patient experience, clinical
outcomes and productivity. It also
affirms the impact of Patterson Dentals
industry-leading sales, technical
service and after-sale support,
which are so critical to supporting
the technology investments made
by our customers.
Dental market fundamentals further
improved in fiscal 2015, and the trends
that we believe are benefiting Patterson
Dental continued. A gradually improving
job market and rising consumer
confidence are stimulating additional
growth in dental consumables due to
increased office visits and overall
demand for dental services.
We continue to take steps to further
capitalize on this opportunity. During
the year, we acquired Holt Dental, a
regional dental supply company located
in the Upper Midwest, that will expand
our local reach. Bolt-on acquisitions
like this continue to augment our
key dental initiatives.
PATTERSON VETERINARY
At nearly one-third of total sales in fiscal
2015, Patterson Veterinary, which will
ultimately operate as Patterson Animal
Health, continued to grow. During the
year, we completed the integration of our
U.K. division, NVS. This unit continues to
be a strong contributor and formidable
competitor in this market, supported by
a talented team and solid execution. As
pet ownership and per-owner spending
continue to rise, our companion animal
businesses are the focus of our strategy
to expand Pattersons equipment and
technology platform. Like our dental
business, our goal in Patterson Animal
Health is to take advantage of favorable
market conditions through our
comprehensive technical service and
support package, as well as our premier
sales force.
The trends in the production animal
market are equally compelling and our
prospects now get even more exciting.
The Animal Health International
acquisition doubles the size of our
animal health business. We have
significantly expanded our presence
in the production animal market, which
has its own very promising long-term
growth prospects, and it creates
meaningful leverage across our
animal health platform.
PATTERSON MEDICAL
Patterson Medical, our rehabilitation
supply and equipment unit, represented
approximately 10 percent of our fiscal
2015 total sales. Medicals improved
performance during the year reflects the
hard work of a team that is committed to
refining their market focus and everyday
execution. The decisions we made to
reposition and streamline this division
paid off. The fourth quarter of fiscal 2015
capped four consecutive quarters of
sales growth, excluding currency and
divestiture impacts. Today, Patterson
Medical has a strong platform,
momentum in the business and a key
position in the marketplace as the global
leader in the physical therapy,
occupational therapy and sports
medicine markets.
DELIVERING RESULTS,
POSITIONED FOR GROWTH
We enter fiscal 2016 with a focused and
scalable platform, clear momentum
and an organization built to capture
opportunities within our markets. We
also start our new fiscal year by
welcoming our Animal Health
International colleagues to the Patterson
family as we begin the important work of
integrating our two cultures.
We remain committed to our balanced
capital allocation strategy. Even with
the acquisition of Animal Health
International, we maintain a strong
balance sheet that gives us a lot of
flexibility. During the fiscal 2015 fourth
quarter, our board of directors approved a
10 percent increase in our dividend. We
are fortunate to have the ability to use the
full range of mechanisms, including
investments in our core business,
dividends and stock buybacks, to create
value for our shareholders.
Our passion for performance is matched
by our enthusiasm to maximize the
opportunities ahead while building on
our culture that allows all employees
to grow and develop.
Reflecting that commitment, Forbes
named Patterson to its List of 100 Most
Trustworthy Companies in America for
the fourth year in a row. Patterson
is one of only two companies in 2015
that have appeared on the list four
consecutive years. I credit each of our
employees, and the culture of integrity
they help foster, for this honor.
Patterson was also added to the Honor
Roll for the 2014 Minnesota Census
of Women in Corporate Leadership.
This distinction, given by the School
of Business and Leadership at
St. Catherine University in St. Paul,
Minnesota, recognizes Minnesota public
companies with 20 percent or more
gender diversity in their executive
ranks and on their board of directors.
Let me take this opportunity to
acknowledge our employees for their
commitment to making Patterson
Companies the strong, high-integrity
organization that it is. We also want to
recognize our customers and partners
for their loyalty, especially during this
exciting period of transition. Lastly, wed
like to thank our shareholders for their
support as we continue to position
Patterson Companies for growth.
We look forward to updating you as
we progress through fiscal 2016.
Scott P. Anderson
Chairman, President
and Chief Executive Officer