to our shareholders
2011 marked 10 years since
The Principal completed its initial
public offering.
Becoming a public company set
the stage for a decade of growth as
The Principal continued its evolution
from a largely insurance-based
organization to a global investment
management leader.
What was aspirational then has
become a reality. The challenges
we faced in that decade made us
stronger. The decisions we made
continue to set the stage for future
growth.
Our proven strategy plus our
ability to execute times 10 years
equals tremendous success. Thats
the power of The Principal.
THE POWER OF 10 YEARS
Reflecting on the 10th anniversary of becoming a
public company, I cant help but think back to the
strategy we laid out for growth and the long-term
success of the company. At the time, we were largely
an insurance-based company and a retirement leader
in the United States. Our goal was to accelerate
growth in our U.S. defined contribution businesses
and drive growth and profitability in our international
businesses. Our strategy was to continue what we
were doing so successfully in the United States
building out our advisor-based distribution platform
to provide leading employer-based retirement and
protection solutions to the underserved small and
medium-sized business market.
In addition, we established a strategy to become a
leading global investment manager and to expand our
international operations.
Fast forward 10 years to 2011.
n We are a global investment
management leader
managing portfolios for investors
in nearly 60 countries.
n We have achieved strong growth
more than tripling assets under
management and nearly doubling
operating earnings as we continue to shift
to a more fee-based business model.
Throughout that time, and just as we have for
more than 132 years, weve remained committed to
helping customers achieve their financial dreams.
Day after day.
Year after year.
Think global. Act local.
Going public gave us access to capital and the identity
we needed to accelerate the growth of our business in
the United States and around the world. It gave us the
foundation to turn our aspirations into reality.
A t the end of 2001, only 6 percent of total company
earnings came from Principal Global Investors and
Principal International. In just 10 years, that grew to
26 percent.
Principal International alone grew from breaking
even in 2001 to earning more than $150 million in
2011. Its been a transformative time for The Principal,
to say the least.
A s middle class populations grow at a rapid pace
in emerging markets, such as China, India and Brazil,
were helping families plan for and achieve the financial
dreams they might not have ever thought possible.
In the United States weve grown our retirement
and long-term savings businesses through a strong
local distribution network, as well as through our
alliance relationships. Weve grown in emerging
international markets by aligning with top-tier
distribution partners, including:
n China Construction Bank, the second largest
commercial bank in the world
n Banco do Brasil, the largest bank in
Latin America
n C IMB Group, the fifth largest banking group by
assets in Southeast Asia
n Punjab National Bank, the second largest public
sector bank in India
The success weve achieved in Principal
International is only part of the outstanding
international growth weve achieved in the last 10
years. Weve also successfully executed a multiboutique
strategy in Principal Global Investors to
provide a broad range of asset classes, investment
styles and portfolio structures. The result? We now
manage portfolios for investors around the world,
spanning nearly 60 countries and including 10 of the
worlds 25 largest pension plans.
Looking back on 2011
Now, 10 years since going public, The Principal is at
the strongest financial position in our 132-year history.
We entered 2011 with a sense of optimismU.S. and
most global markets were up, we saw increasing signs
of growth and momentum in our businesses and the U.S.
economy was starting to rebuild.
Optimism continued throughout the year, and
despite external challenges, including choppy market
conditions, the widening of credit spreads and a low
interest rate environment, 2011 results were solid.
n Total company operating earnings were
$878 million as of Dec. 31, 2011.
n A ssets under management ended the year at
$335 billion.
More importantly, the fundamentals of our
business remained strong in 2011. Growth metrics
such as sales, client retention and net cash flows
increased across our businesses, contributing to
increased momentum going into 2012.
n Full Service Accumulation had its second highest
sales year on record at $8.4 billion, an increase
of 27 percent over 2010, as well as record client
retention, resulting in positive net cash flows of
$3.8 billion.
n Principal Funds had record sales of $11.2 billion,
an increase of 20 percent over 2010, and record
net cash flows of $2.2 billion.
n Principal Global Investors had $11 billion in
mandates awarded in 2011, more than double
the amount awarded in 2010.
n Principal International ended the year with
assets under management of $52.8 billion,
including record net cash flows of
$5.5 billion.
n Individual Life sales were $186 million in
2011 and Specialty Benefits Division sales
were $285 million, both double-digit
increases over 2010.
These strong results demonstrate our ability to
execute our strategy and focus on the things we can
control. In our 132-year history weve faced many
headwinds and have weathered two of the worst
financial crises in U.S. history. Yet here we are
today, stronger than ever and poised for continued
long-term growth.
2011: a year of focused execution
I am very pleased with how well our employees
executed on our strategy in 2011, which is reflected
in strong sales and customer retention. Another key
example of the companys ability to execute is our
deployment of capital throughout 2011.
The companys fee-based business model generates
increasing amounts of free cash flow, which allows
us to invest in our businesses and increase value for
shareholders. The Principal started and ended the year
with $1.6 billion of excess capital, while deploying
more than $1.1 billion of capital throughout 2011,
including:
n $550 million on opportunistic share repurchase
n $214 million on a common stock dividend
n $350 million on strategic acquisitions
In 2011 we announced and closed on three strategic
acquisitions that were well aligned with our global
investment management strategy:
n Principal Global Investors acquired a majority
stake in Finisterre Capital and Origin Asset
M anagement, which are asset management
firms based in London with an emphasis on
emerging and global developed markets, areas
of strong demand from institutional investors.
n Principal International acquired the HSBC
A FORE. This acquisition adds scale to our
mandatory defined contribution operations in
Mexico and makes The Principal the seventh
largest AFORE in Mexico. It also gives us an
exclusive five-year distribution agreement
with HSBC-Mexico, which boasts Mexicos
fourth largest distribution platform.
The power of the next 10 years
Rapid change is inevitable over the next 10 years.
The Principal is ready. Well continue to:
n Grow as a global investment management leader
n Shift to a more fee-based business model,
generating increasing amounts of excess capital for
strategic acquisitions or return to shareholders
n Become a more globally diversified company
And well continue to work hard to help growing
businesses and individuals in the United States and
around the world reach their financial dreams.
The 10 years since The Principal went public
have been sometimes challenging but always exciting.
But there is no doubt our best times are ahead of us,
and I cant wait to see whats in store.
We have the right strategy.
Were in the right markets.
We actively and effectively manage risk.
And we have talented, focused employees.
As we mark this important anniversary, I would
like to thank our employees for their dedication over
the years. And on behalf of the board of directors, Id
like to thank you, our shareholders, for your continued
support and your confidence in us going forward.
Please be sure to cast your vote in time for the Annual
Meeting of Shareholders on May 22, 2012.
Larry Zimpleman
Chairman, President and
Chief Executive Officer