to our shareholders
2012 was an exciting year for Target, as we devoted meaningful
resources to driving performance in support of our publicly
stated sales and financial goals, while transforming Target to
seize the tremendous opportunities we see in the most dynamic
and disruptive retail landscape in generations.
Total sales and diluted earnings per share reached new highs
of $72.0 billion and $4.52, respectively. We invested $3.3 billion
of capital in our U.S. and Canadian businesses, and we returned
over $2.7 billion to our shareholders through share repurchase
and dividend payments. And our full-year results were right on
track with our Long-Range Plan to reach at least $100 billion in
sales and $8 in earnings per share in 2017.
In addition to our financial successes, we achieved significant
strategic and operational milestones in 2012, including the
launch of our first CityTarget stores in Chicago, Seattle,
San Francisco and Los Angeles; extending our fresh-food
remodel program to another 238 general merchandise stores;
achieving record-setting sales penetration through our 5%
REDcard Rewards loyalty program; and announcing an agreement
to sell our U.S. credit card receivables to TD Bank
Group, a strong partner aligned with our goals for portfolio
growth and profitability. We also surpassed $4 million per week
in charitable giving to support communities we serve.
In 2013, well continue to pursue a strategy that is being shaped
by our guests expectations for more shopping flexibility and
price transparency, and the rapid pace of change in technology.
To ensure that we continue to strengthen our guests love for
our brand and deliver the surprise and delight they have come
to expect, well leverage our greatest asset, our stores, in
combination with increased investment in our digital platforms,
to create a seamless, relevant and personalized experience.
Differentiation with exceptional value, which represents the
foundation of our Expect More. Pay Less. brand promise, will
continue to set Target apart in the marketplace. We remain
committed to offering a truly unique assortmentthrough our
design partnerships, outstanding portfolio of owned brands
and curated selection of signature national brands. And
we are equally unwavering in our commitment to provide a
compelling value proposition, as we showed by expanding
our Price Match Guarantee to include select online competitors.
Were also collaborating closely with our vendors on channelmanagement
strategies that sharpen prices and improve selection
for our guests.
Meanwhile, in 2013, we will also undertake the largest, singleyear
store expansion in Targets history. After two years of
exceptional dedication and hard work by our team, weve begun
opening Target stores in Canada and are on track to open 124
stores across all 10 provinces by year end. In addition, well
extend our new CityTarget urban format to additional locations
in Los Angeles and San Francisco, and, for the first time, to
Portland, Oregon.
We are excited by the physical and digital growth ahead of us.
By staying focused on creating a superior shopping experience
for our guestswhether they are in urban or suburban markets,
in the U.S. or Canada, in our stores or digital channelswe
believe Target will continue to thrive. And, this strategic clarity,
in combination with our powerful brand, gives us confidence in
our future: confidence in the values that have guided our company
for 50 years, confidence in the talent and passion of our 361,000
team members and confidence in our continued ability to deliver
profitable growth for many years to come.
Gregg Steinhafel | Chairman, President and CEO, Target