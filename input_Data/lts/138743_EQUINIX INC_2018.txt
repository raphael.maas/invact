Dear Shareholders,
As I reflect on this past year, I am humbled and grateful to be at
the helm of an incredible team of almost 8,000 dedicated Equinix
employees around the globe in service to each other, to our customers
and to our shareholders. In 2018, we celebrated our 20th anniversary,
a great opportunity to reflect on and appreciate what we�ve built, and
a perfect time to sharpen our vision of where we are headed next.
Back in 1998, when Al Avery and Jay Adelson founded Equinix, they
believed they not only had the opportunity but also the responsibility
to create a company that would be the steward of some of the most
important digital infrastructure assets in the world. Two decades later,
we have expanded upon that vision to build Platform Equinix�, with
unmatched scale and reach, spanning 200 International Business
Exchange� (IBX�) data centers across 52 markets in 24 countries.
Inside our IBX facilities, our high-value ecosystems consist of nearly
10,000 participants, a veritable who�s who of the digital world, including
approximately half of the Fortune 500 and one-third of the Global
2000. And these ecosystems thrive because Equinix has the most
comprehensive global interconnection platform in the industry, with
over 333,000 physical and virtual interconnections, more than four
times any other provider. Our priority for the future of Platform Equinix
is, and will remain, delivering exceptional, durable and quantifiable
value to our customers. 

In 2018, our differentiated business model drove strong growth and
operating performance, while maintaining our #inserviceto mindset. In
service to our customers, achieving over 99.9999% of operational reliability.
In service to our shareholders, delivering our 64th consecutive quarter of
top-line growth and eclipsing a key milestone of over $5 billion in revenue
for the year. In service to the communities where we operate, volunteering
over 16,000 hours to local causes, and sourcing clean and renewable
energy across 90% of our global platform. And in service to each other
by building on our exceptional culture and reinforcing our commitment to
make sure Equinix is a place where everyone can confidently say, �I�m
safe, I belong and I matter.� Our compelling advantages continue to fuel
our business model, and a rapidly expanding market opportunity gives us
solid momentum as we look to 2019 and beyond.
As we envision the possibilities for our next 20 years, it is clear that digital
transformation is reshaping nearly every industry across the globe. Our
customers and partners are thinking differently about how they interact
with their customers and with every element of their supply chain. Digital
transformation, and the infrastructure that fuels it, has emerged as a top
priority for virtually all companies. The major tech trends, whether it be
artificial intelligence, internet of things, big data or 5G, are all amplifying
this digital tailwind. This makes digital the growth engine of the global
economy, transcending the macro-economic volatility that we are seeing
in the market today.
In the wake of this digital transformation wave, a clear architecture of
choice has emerged for our customers. That architecture is global, highly
distributed, hybrid and multicloud. And for a variety of reasons, customers
are increasingly looking to locate this modernized architecture at Equinix,
leveraging our platform to achieve performance, security, compliance,
flexibility and total cost of ownership benefits that can only be supported
by the physics of proximity and the economics of aggregation. Equinix
is often the digital edge for our customers, and they are leveraging our
global footprint as the nexus for their digital transformation agenda. In
the fourth quarter alone, our bookings spanned across more than 3,000
customers, with a quarter of them buying across multiple metros and 60%
of our recurring revenues now come from customers deployed across all
three of our operating regions.

We are continuing to innovate to differentiate our services and deliver
greater value to customers. We rolled out interregional connectivity through
Equinix Cloud Exchange Fabric� (ECX Fabric�) to enable customers
to dynamically interconnect across Equinix�s global platform, regardless
of location and saw strong uptake on this new offering. We are further
extending our leadership as the trusted center of a cloud-first world
through our hyperscale initiative, enabling us to capture strategic large
footprint deployments from select customers, while mitigating strain on
our balance sheet by employing off-balance sheet structures to help fund
this opportunity. And lastly, we are taking steps to make Equinix a more
powerful, easier-to-use, more accessible platform, focusing on a new
generation of Edge Services that will help us support our customers� growth,
and both sustain and enhance our cabinet yields over the coming years.
In closing, we believe that Equinix is uniquely positioned to help customers
pursue their digital transformation agendas by leveraging our superior
global reach, scaled digital ecosystems, the most comprehensive
interconnection portfolio in the industry and our 20-year track record
of service excellence. We remain focused on six priorities for this year,
including expanding our go-to-market engine, evolving our portfolio of
partners and products, and delivering on our hyperscale strategy, all while
caring deeply for our people and culture, and remaining steadfast in our
commitment to deliver against the revenue, margin expansion and AFFO
per share targets laid out at our last Analyst Day in 2018. As we look
forward, I am immensely grateful to have been a part of the Equinix story
thus far, and I am as energized and optimistic as ever about the opportunity
in front of us. Thank you for joining us on this amazing journey. We look
forward to updating you on our progress!
Charles Meyers
President and Chief Executive Officer
Equinix, Inc.
Peter Van Camp
Executive Chairman
Equinix, Inc.
Keith Taylor
Chief Financial Officer
Equinix, Inc. 