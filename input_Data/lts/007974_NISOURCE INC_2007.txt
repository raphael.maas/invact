Dear Fellow Stockholders:
At NiSource, our aspiration is to be the premier company in our industry, with a strong financial profile; a wide range of
investment-driven growth opportunities; robust and sustainable earnings and cash flow; top-tier safety, service and
reliability metrics; and a solid foundation of engaged, aligned and safe employees.
I am pleased to report that 2007 was a year of significant progress that brought us closer to achieving that aspiration.
Across virtually every dimension of our business, our team made extraordinary progress in fixing, building and
repositioning NiSource in 2007. Certainly, advancing the implementation of NiSources Path Forward business
strategy, which is premised on long-term infrastructure investments, is at the forefront of these accomplishments. Key
components of that implementation effort included bold steps to resolve an array of legacy operating, regulatory and
commercial issuessetting the stage for what promises to be a pivotal year for our company in 2008.
From an earnings standpoint, NiSources core regulated natural gas and electric operations continued to deliver solid
performance, with results for 2007 that exceeded our outlook. Meanwhile, our teams made significant progress during
the year to reposition the company to deliver long-term growth. With this important foundational work largely
completed, each of our business segments is now poised to do what they do bestexecute the key investment,
regulatory and commercial elements of our business plan, deliver on our commitments, and Achieve Our Aspiration of
becoming The Premier Regulated Energy Company in North America.
Throughout this report and NiSources accompanying 2007 Form 10-K, you will see evidence of progress made during
the year. For now, I would like to highlight just a few examples.

Recommitting to our Core Regulated Assets and our Strategic Plan
Central to our long-term growth strategy is the underlying strength of NiSources core regulated assetsour local
natural gas distribution companies, our Indiana electric business and our extensive natural gas transmission and
storage operations.
During 2007, we confirmed the value of those assets through our deliberate and disciplined strategic and financial
review process. The review considered a broad range of options, financial techniques and structures within the context
of our commitment to sustain NiSources dividend and maintain our investment grade credit ratings.
As we reported in May, the review concluded that NiSources existing set of core assets are fundamentally strong, with
an array of long-term organic growth prospects. We are convinced we can maximize the value of those assets through
our investment-driven strategic plan and deliver long-term, sustainable earnings and cash flow growth for our
shareholders. And we are backing that strategic plan with an unprecedented $1 billion plus annual investment program
that will be tightly integrated with our regulatory and commercial activities.
Executing on our Four-Part Plan
Even as our strategic and financial review process was concluding, NiSource teams were actively advancing a wide
array of initiatives as part of the companys four-part business plan. That plan centers on: 1) Expansion of and
commercial growth of the gas transmission and storage business; 2) Regulatory and commercial initiatives at our
utilities; 3) Financial management; and 4) Process and expense management.
At the forefront of those initiatives in 2007 was the progress we made on our development of a master limited
partnership (MLP) as a strong complement to our NiSource Gas Transmission and Storage (NGT&S) growth strategy.
That work led to our December announcement that our new, wholly-owned subsidiary, NiSource Energy Partners, L.P.,
had filed a registration statement with the Securities and Exchange Commission (SEC). The filing, subject to SEC
review, proposed an initial public offering, with the partnerships initial asset being the Columbia Gulf Transmission
Company system stretching from Louisiana to Kentucky.
Beyond the MLP initiative, our NGT&S group also continued to develop and execute on a steady stream of pipeline and
storage growth projectsan important plank in our long-term growth platform. NGT&S projects advanced during 2007
included:
 The initiation of service under the Hardy Storage project,
 The Eastern Market Expansion, which received FERC approval in January of 2008 and is targeted
for completion in early 2009,
 A major expansion of capacity into the Florida Gas Transmission system,
 Several new low-cost, high-return connections to the Columbia Gulf system,
 The June launch of construction on the Millennium Pipeline in New Yorkslated for completion
in late 2008.

Our Balanced Approach
The NiSource portfolio of low-risk, regulated assets generates
about $3 billion in annual net revenues, ranking among the
nations largest integrated energy firms and squarely among
the Fortune 400 list of top U.S. companies. Our operating
income is balanced between each of our three business
segments. NiSource stock trades on the New York Stock
Exchange under the symbol NI.

It is also important to note that the NGT&S team delivered improved operating income during 2007, despite tempered
opportunities for optimization-related services due to less volatility in the natural gas market. Driving this improvement
was the near-full subscription of the Columbia Gulf system and higher throughput as a result of increased storage
injections, gas-fired electric generation demands, and marketing activities. NGT&S also delivered increased earnings in
2007 from our Hardy Storage partnership with Piedmont Natural Gas, which began operations in April.

This impressive list of 2007 key accomplishments is proof positive of our teams ability to execute our game plan to
position the company to deliver on our four-part business plan for long-term, sustainable growth. We continue to move
aggressively, and thoughtfully, to engage our stakeholders, make disciplined investments, and position our teams to
execute on NiSources growth strategy.
Charging into a Pivotal Year
With significant groundwork having been laid in 2007, we view 2008 as a pivotal year in our long-term growth strategy.
In fact, we are charging into 2008 with three critical areas of focus:
 Executing on Major Infrastructure Enhancement
Projects, which will constitute a significant portion
of NiSources more than $1 billion annual ongoing
capital investment program.
 Achieving our Key Regulatory Initiatives, including
gas base rate cases in Pennsylvania and Ohio, as
well as NIPSCOs electric rate case scheduled for
filing on July 1, 2008.
 Advancing our NGT&S Growth Strategy, including
securing approvals and timely construction of
announced projects, developing an array of potential
new growth opportunities, and continuing with the
formation of NiSource Energy Partners, L.P.
As we aggressively pursue these priorities, we are
keenly aware and appreciative of your continued
support. Our investment-driven utility and pipeline
growth strategies clearly require time and considerable
effort to bear fruit. Nevertheless, we are convinced that
the foundation we have built, and will continue to build
over the next several years, will position NiSource to
generate meaningful and sustainable future earnings
and cash flow growth.
Doing What We Do Best
With our strategy clearly set, employees all across
NiSource are hard at work todaycommitted to
achieving our long-term aspiration, step-by-step, brickby-
brick. And the capstone is that our entire team is
energized, and intently focused on executing this plan. Quite frankly, as a regulated energy company, we welcome
these priorities. This is what we do best. As always, we remain committed to communicating with our investors and all
other stakeholders in a transparent and timely manner regarding these and all of our efforts. Ongoing updates will be
provided on www.nisource.com.
Thank you once again for your continued interest in and support of NiSource.
Sincerely,

Robert C. Skaggs, Jr.
President and Chief Executive Officer




