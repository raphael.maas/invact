Accenture�s financial performance in fiscal 2014 clearly
demonstrates that we are executing very well against
our strategy. In a market environment that remains highly
competitive, we delivered on our initial business outlook, with
particularly strong momentum in the second half of fiscal 2014.
For the full fiscal year, we increased our market share,
generated record revenues and new bookings, expanded
operating margin, generated strong cash flow and returned
substantial cash to shareholders.
Here are some highlights:
� We delivered record annual bookings of $35.9 billion.
� We grew net revenues 5 percent to $30.0 billion.
� We delivered diluted EPS of $4.52, up 7 percent from
adjusted fiscal 2013 EPS of $4.21, which excluded $0.72 in
benefits from a reduction in reorganization liabilities and
final determinations relating to tax liabilities.
� Our operating margin was 14.3 percent, an expansion of
10 basis points from the adjusted fiscal 2013 operating
margin of 14.2 percent, which excluded 100 basis points
in benefits from a reduction in reorganization liabilities.
� We generated free cash flow of $3.2 billion and maintained
a very strong balance sheet, closing the year with
$4.9 billion in cash.
� We returned $3.8 billion in cash to shareholders through
dividends and share repurchases, and we announced a
10 percent increase in our semi-annual cash dividend
shortly after fiscal year-end.
Our strategy in action
We remain focused on our long-term vision for market
leadership, which is all about leveraging our unique position
as the leading independent, global, professional services
company�providing end-to-end services that help the
world�s largest companies as well as government agencies
transform and achieve tangible, measurable results.
During fiscal 2014, we made a number of changes in our
operating model and organization structure to enhance
our capabilities and make Accenture even more relevant,
differentiated and competitive in the marketplace:
� We created Accenture Strategy, a unique capability and the
first in the market to bring together business strategy and
technology strategy, equally and at scale. Accenture Strategy
is one of the world�s leading strategy consultancies,
providing a distinctive perspective to senior management
teams at clients such as Grupo Globo and Mondelez.
� We launched Accenture Digital by combining our
capabilities in digital marketing, analytics and mobility.
With more than 28,000 professionals, Accenture Digital is
the world�s largest end-to-end digital capability and works
with many industry leaders�including all of the top 10
global pharmaceutical companies as well as all of the
top 10 consumer products companies.
� We formed Accenture Operations, bringing together our
capabilities in business process outsourcing with our
infrastructure and cloud services to provide clients with a
more compelling value proposition�running key operations
�as a service� and at scale. We see exciting opportunities in
the marketplace to offer flexible, cloud-based services that
enable clients to plug in and get results quickly, paying only
for what they use.
� In Accenture Technology, we further enhanced our
Global Delivery Network, recruiting significant talent and
investing to build intelligent tools to increase efficiency and
productivity. We continue to play a leading role in the
technology ecosystem as the No. 1 partner of key technology
providers such as SAP, Oracle, Microsoft, HP and salesforce.com,
as well as many other established and emerging companies.
� We infused more talent into our five industry-specific
operating groups�our primary market channel. We further
strengthened our management and technology consulting
capabilities, enhancing our capacity to serve clients and to
more quickly assemble integrated teams with specialized
skills from across Accenture. 
During the year, we continued to make targeted investments
across the company�to build and launch highly differentiated
capabilities, both organically and through acquisitions;
to recruit and develop new talent, including hiring about
80,000 people; and to further strengthen our marketplace
positioning and the Accenture brand, which once again was
named one of the world�s top 50 brands by Interbrand.
Over the past two years, we have invested more than
$1.5 billion in strategic acquisitions to enhance our
capabilities in key growth areas. Our largest acquisition
in fiscal 2014�Procurian�complements our existing
sourcing and procurement capabilities and makes
Accenture the clear market leader in procurement
business process outsourcing.
While we have fine-tuned our operating model and
expanded our capabilities, we also continue to enhance�
and to benefit from�our core competitive advantages in
the marketplace. We have strong and enduring relationships
with many of the world�s leading companies�operating at
the heart of their businesses and addressing their most
complex strategic issues. We are proud that our clients
include more than three-quarters of the FORTUNE Global
500, and that 95 of our top 100 clients in fiscal 2014 have
been Accenture clients for at least 10 years.
Another key differentiator is our broad and deep industry
knowledge across more than 40 industries. More than
187,000 Accenture people have certified industry skills or
are aligned with specific industries. We provide end-to-end
industry- and function-based business services that
address the issues unique to a given industry or enterprise
function. Our industry business services include Accenture
Digital Video Services for media, entertainment and
communications companies such as BT, and Accenture
Accelerated R&D Services for pharmaceutical companies
such as Merck and Pfizer.
We continue to strengthen our leadership position in
technology, cultivating innovation in our Accenture
Technology Labs as well as through our ongoing work with
hundreds of start-up companies. We are also innovating at
scale with industry leaders in the �Industrial Internet of
Things.� We partnered with GE to launch our Intelligent
Pipeline Solution�which Columbia Pipeline Group is now
implementing�and formed a joint venture with Siemens to
deliver smart grid solutions. In technology delivery, the scale
and scope of our Global Delivery Network remain unmatched
in the marketplace, with 205,000 people working from more
than 50 delivery centers and at client sites around the world.
We have a broad global footprint, serving clients
consistently wherever they operate, yet we also provide
strong local execution and market relevance. During fiscal
2014, we aligned our organization around three geographic
regions: North America (the United States and Canada),
Europe, and Growth Markets (Asia Pacific, Latin America,
Africa, the Middle East, Russia and Turkey). We continue
to focus our investments in countries where we see the
greatest growth potential�including the United States,
our largest market, where we have consistently delivered
high single-digit or double-digit revenue growth over
the last four years.
We also remain committed to running Accenture as a
high-performance business, managing the company with
rigor and discipline to increase our efficiency and enhance
our competitiveness. This expands our capacity to reinvest
to further differentiate our capabilities�and achieve our
ultimate goal of delivering sustainable, profitable growth
over the long term.
Investing in our people and our
communities
One of our top priorities is having the best talent�with
highly specialized skills at the right levels in the right
locations�to meet the evolving needs of our clients.
We are deeply committed to the ongoing career
development of our more than 305,000 employees.
In fiscal 2014, we invested $787 million in training and
professional development, and we introduced Accenture
Connected Learning, which provides our people with
expanded opportunities to deepen their skills and
capabilities through a broad range of physical and
virtual learning environments. We are proud to be
recognized for the sixth consecutive year as one of
FORTUNE�s �100 Best Companies to Work For.�
We also remain committed to making a measurable
difference in the communities in which we live and work.
Through our Skills to Succeed corporate citizenship
initiative, we will equip more than 700,000 people
around the world by 2015 with the skills to get a job
or build a business.
Our environmental strategy spans our entire operations�
from how we run our business to the services we provide
our clients to how we engage with our employees and
suppliers�and we are pleased to have achieved a reduction
in per employee carbon emissions of more than 36 percent
from our fiscal 2007 baseline. In 2014, we were included
on CDP�s Climate Performance Leadership Index for our
actions to mitigate climate change.
In closing, I want to thank the men and women of
Accenture for their hard work and dedication to serving
our clients. We have excellent momentum in the
marketplace, and our investments are positioning us
to be even more differentiated and competitive. I am
confident that we will continue driving profitable growth
and delivering value for our clients and shareholders
in fiscal 2015.
Pierre Nanterme
Chairman & CEO
December 15, 2014