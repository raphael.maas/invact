To our shareowners,
As one of the world’s leading producers of renewable fiber- based packaging, pulp and paper products, we know it’s our responsibility to create long-term value for all stakeholders in the most responsible and sustainable manner.
I’m confident in the future of International Paper. By investing in attractive, fiber-based markets, controlling costs, managing capital spending and focusing on deliberate improvement efforts to increase productivity and efficiency, we have generated strong sustainable free cash flow despite challenging economic conditions.
In 2016, we developed The IP Way Forward to drive the next wave of improvements and continue to meet our commitments to our shareowners, customers, employees and communities. The IP Way Forward consists of five Strategic Drivers critical to our success: sustaining forests, investing in people, improving our planet, creating innovative products and delivering inspired performance.
Sustaining FORESTS
Our entire business depends on the sustainability of forests. By providing a dependable market for responsibly grown fiber, we provide an economic incentive for landowners to grow, harvest and regenerate forests for continuous, sustainable use.
We will continue to lead the world in responsible forest stewardship to ensure healthy and productive forest ecosystems for generations to come. Our investments in forest restoration and sustainable forest management help increase forest carbon stocks and mitigate climate change.
Since 2013, we have invested more than $7.5 million in the Forestland Stewards Initiative, a partnership with the National Fish and Wildlife Foundation (NFWF) aimed at protecting and enhancing ecologically significant forestlands in the United States. Our investment generated more than $24.5 million in matching funds for a total conservation impact of $32 million. In 2016, we renewed our commitment to this partnership and pledged to increase our contribution to $10 million over the next five years.
Investing in PEOPLE
Our most important measure of success is ensuring all employees, contractors and visitors arrive home safely every day. In 2016,
we expanded our focus on prevention with Safety Leading Indicators to identify hazards and unsafe actions before accidents occur.
Engaged employees drive sustainable results, and good leaders engage, align and inspire colleagues. Building our capability means not only improving our assets, but also developing our employees, retaining
our top talent and creating a culture of inclusion, where individuals feel respected, are treated fairly and have opportunities to do their best every day.
We continue to be a force for good in our communities. By mobilizing our people, products and resources we can help address critical community needs where our employees live and work. In 2016,
we increased our charitable contributions and focused our community engagement strategy on four signature people causes: education, hunger, health and wellness and disaster relief.

Improving our PLANET
Our sustainability story is embedded in the renewability and
recyclability of our products, and in the way we operate.
More than 70 percent of the energy used in our global mill system
is generated from renewable, carbon-neutral biomass. We set our Vision 2020 Goals using a 2010 baseline; since then, we’ve reduced our energy use and greenhouse gas and other air emissions, resulting in a reduced environmental footprint and lower energy costs.

Innovative PRODUCTS
We transform renewable resources into recyclable products people depend on every day. Our packaging products protect and promote goods, enable worldwide commerce and keep consumers safe. Our pulp for diapers, tissue and other personal hygiene products promote health and wellness.
Our papers facilitate education and communication, and our food service products provide convenience and portability for on-the-go consumers.
In December 2016, we acquired Weyerhaeuser’s pulp business and integrated it with our existing business to create our Global Cellulose Fibers
business, the world’s premier manufacturer of fluff and specialty pulp. This acquisition gave us best- in-class manufacturing assets and capabilities,
a valuable patent portfolio and an expanded innovation team that will help us improve and grow our entire pulp business. We also gained additional mills in key coastal locations, which allows us to expand our exports of fluff pulp around the world.
We continue to focus on creating innovative, sustainable and recyclable products that help our customers achieve their objectives and satisfy changing global consumer demand.
Inspired Performance
We deliver long-term value for our stakeholders by establishing advantaged positions in attractive, fiber-based market segments with safe, efficient manufacturing operations near sustainable fiber sources.
Challenging market conditions and increasing costs for fiber and energy caused us to fall short of our 2016 earnings goal. However, we remain focused on generating strong free cash flow, creating value with returns greater than our cost of capital, returning cash to shareowners, maintaining a strong balance sheet and making investments for future growth.
OUTLOOK
In 2017, we expect to continue our trend of strong cash generation and returns well above our cost of capital.
We expect higher margins and earnings in our North American Industrial Packaging business through implementation of price increases, growing customer demand and internal improvement initiatives. We also expect to improve margins with continued strong operations and extensive cost reduction efforts across many of our other businesses.
We expect to complete the conversion of the Madrid mill to recycled containerboard in the second half
of the year, which will enable us to offer better products for our customers and to increase earnings in our European Industrial Packaging business.
Our Ilim joint venture remains well-positioned for another strong year of performance.
As we integrate Weyerhaeuser’s pulp business, we expect to achieve synergies and continue to evolve our ability to meet customer and global demands with innovative products. Through this integration, we will improve our overall product mix and, consequently, the earnings of our global cellulose fibers business.
And with the strong free cash flow that comes from all of this, we will continue to create value with a near-term focus on debt reduction.
We are confident in how International Paper is positioned and the value-creating opportunities
we are pursuing. Our global team is committed
to continuously strengthening our people and local communities, using resources responsibly and efficiently and ensuring our businesses are safe, successful and sustainable for generations to come.