SHAREHOLDER LETTER

2018 IN REVIEW
We are pleased to report another productive and successful year for the Company
in 2018. Favorable economic conditions
supported by a vibrant technology sector
contributed to rental revenue growth near
long-term averages, while progress was
made on key initiatives focused on improving efficiency, employee engagement and
resident satisfaction. We also modestly
outperformed our primary financial targets
in 2018, with Core Funds from Operations
per share (�FFO�) growth of 5.5%, based
on 2.8% growth in same-property revenue.
U.S. economic momentum continued at a
healthy pace during the year. The labor
market�s enduring strength remained the
foundation for growth for the nation and
the West Coast. Job growth across the
Essex markets was 2.1% in 2018, with most
of the outperformance emanating from
technology industries. Positive job growth
on the West Coast continued despite low
unemployment rates, in part driven by
favorable migration patterns of highly
skilled workers from various East Coast
metros for high-paying jobs in the technology sector. This trend has supported a shift
of prosperity to key West Coast innovation
centers, contributing to strong personal
income growth and improved housing
affordability across the Essex markets.
Throughout the year, shares of Essex stock
experienced periods of high volatility, mostly
attributable to two primary factors: First,
through November, we experienced heightened speculation and concern about the
impact of increased rental housing regulation, including California�s Proposition 10,
which was ultimately rejected by popular
vote. Second, a meaningful market correction took place in the fourth quarter, partially
related to concerns over higher interest
rates, which led to a 7% share price decline
in the Apartment sector in the month of
December. Despite the volatility, we generated a 4.8% total return to shareholders in
2018, narrowly outperforming the multifamily peer average and many REIT indices.
During the year, the Company�s investment strategy evolved in reaction to
changing capital markets conditions and
higher capital costs, ultimately resulting
in Essex being a net seller of property.
Apartment values remained relatively
stable during the year, while our cost of
capital increased primarily due to rising
interest rates. Amid these conditions, we
opportunistically sold four communities
comprised of 918 apartment homes for a
total contract price of $417 million, using
the disposition proceeds to fund the
under-construction development pipeline, complete two acquisitions for $139
million, and repurchase $51 million of
common stock. Continued shortages of
labor within the construction trades is
causing production costs to rise faster
than rents, leading to a slowdown in new
multifamily development starts in our
West Coast markets. Consistent with this
trend, we are reducing our direct development exposure, focusing instead on
apartment development in connection
with our preferred equity platform to
garner better risk-adjusted returns.
2018 HIGHLIGHTS
� Achieved same-property revenue and
net operating income (�NOI�) growth of
2.8% and 2.9%, respectively, exceeding
the multifamily peer average.
� Generated $20.5 million of promote
income from our co-investment portfolio.
� Continued our focus on corporate social
responsibility by participating in the Global
Real Estate Sustainability Benchmark
(GRESB) survey for the third consecutive
year. The Company earned its third
�Green Star�, the highest possible 


designation. In addition, we achieved LEED
Gold certification at our recentlycompleted Station Park Green community
located in San Mateo, California.
� Amended one of our unsecured credit
facilities, increasing the maximum
amount available for borrowing from
$1.0 billion to $1.2 billion, and extending
the maturity to December 2021.
� Increased our dividend by 6.3%, our 24th
consecutive annual increase.
LOOKING AHEAD
As we look to 2019, we expect the U.S. economy to continue growing at a steady pace,
supported by productivity growth and constrained to some extent by a tight labor market. Based on our view of the national
economy, we expect the West Coast metros
to be among the top performing U.S. metros
with respect to rent growth. We expect critical housing shortages to be an on-going
issue, especially in our coastal California
markets, given limited new apartment and
for-sale deliveries in 2019. Due to the tight
labor market, we expect employers to
aggressively seek skilled workers, resulting
in upward pressure on wages. Job openings
in California and Washington at the top 10
public technology companies, all of which
are headquartered in Essex markets,
recently hit nearly 24,000, the highest level
since we started tracking this data several
years ago. Given this backdrop, we expect
market rent growth to be near long-term
averages for our West Coast footprint. While
our near-term outlook for the U.S. economy
is favorable, we remain vigilant of potential
headwinds facing the global economy
including uncertainties surrounding trade,
interest rates, and slowing economic growth.
Investment activity in 2019 will largely
depend on the capital markets and our
cost of capital. As we have done throughout our history, we will remain disciplined
and find ways to opportunistically create
value for shareholders while maintaining
our financial flexibility and balance sheet
strength. Finally, we continue to invest in
technologies to improve the customer
and employee experience and drive efficiencies throughout the company.
The Company will celebrate its 25th anniversary as a public company this upcoming June. Since our initial public offering in
1994, the Company has generated a 16%
compound annual total return to shareholders, the highest of all public REITs
over this time period, and increased the
dividend per share every year since its
IPO, resulting in a 6.4% compound annual
increase. 2019 is expected to be our 25th
year of increasing the dividend, which
would make the Company a �Dividend
Aristocrat�. We are proud of these accomplishments and seek to continue this
strong track record through our disciplined approach to investing, capital allocation and operational excellence.
IN CONCLUSION
In closing, with a strong balance sheet,
opportunistic approach to capital allocation and a proven and experienced management team we are in a great position
to continue creating shareholder value.
We are thankful to our shareholders,
partners, employees, and residents for
their unwavering support as we look forward to another successful year ahead.
Sincerely,
George M. Marcus
Chairman
Michael J. Schall
President & Chief Executive Officer