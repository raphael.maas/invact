Fellow Shareholders:
Looking back on 2003, we take pride in our performance during a very
challenging year. Net earnings in 2003 were $3.01 billion, a 42% increase
from 2002. Earnings per diluted share (EPS) were $5.87, a 46% increase
from 2002, and our second highest reported EPS as a public company.
Our 2003 return on average shareholders equity was 15%, up from 11%
in 2002. Our return on average tangible shareholders equity was 20%,
well above the 15% earned in 2002.
By its nature, our business doesnt lend itself to predictable recurring
earnings. As we often remind our people, Goldman Sachs cannot control
the market environment in which we work. But we can control our relative
performance. By this standard, we judge 2003 to be a success and further
evidence of the strength and resilience of our franchise.
Despite our success in 2003, most of us will remember the past year as one in which we faced the continuing
fallout from the collapse of the high-tech bubble and the series of corporate scandals that followed. It
was an environment marked by sharp criticism and intense scrutiny of corporations generally, and financial
services firms in particular.
At Goldman Sachs, we continue to experience enhanced regulatory scrutiny in all areas of our business.
While at times not without challenges, we see this as an opportunity to learn from our mistakes, and those
of others, as we strive to do all we can to restore investor confidence, to improve the structure and practices
of financial markets and, above all, to strengthen our own policies and procedures. Our highest priority is to
enhance our reputation for integrity in everything we do.
It is a challenging agenda and the implications are far-reaching. Success will, to a large degree, be measured
by the extent to which financial institutions like Goldman Sachs can continue to operate effectively. This is
important because the role played by integrated investment bankswhich bring together the providers and
users of capitalis central to well-functioning capital markets and to sustained global growth. Firms like ours
are in many ways agents of change. We facilitate entrepreneurship and innovation, help create opportunities
for individuals and companies alike, and act as a catalyst for economic reform around the world.
This year our letter is a little longer than usual because we address in more detail some of the most
important opportunities and issues we face as a firm.
INVESTMENT BANKING
Investment banking activity continued to suffer from
diminished corporate and investor confidence in a
tough business environment. Industry volumes for
completed mergers decreased again in 2003 and equity
underwriting volumes remained low. Global completed
mergers decreased 17% from 2002, and were 70%
below volumes in 2000, the prior peak. Our results
reflected this difficult industry environment. Investment
Banking net revenues were $2.71 billion, down 4%
from 2002, and pre-tax earnings were $207 million,
down from $376 million the year before.
However, despite this difficult environment, we
maintained our focus on serving clients and continued
as the market leader in our core franchise businesses,
including mergers and acquisitions, and IPO, equity,
and equity-linked underwriting. In addition, we developed
a prominent position in the issuance of high-yield
securities. We think the strength of our franchise and
the quality of our client relationships are evident in this
performance. We do not, however, seek to be number
one in all areas. Pursuing market share in some products
makes no sense to us when the profitability is too
small or non-existent.
We believe that our role as a core advisor to clients
has never been stronger. Our investment banking professionals
continue to play a vital role in advising senior
executives and a broad range of clients throughout the
world. Through these relationships, our people are able
to identify the needs of our clients and offer solutions
by providing advice, products and services from across
our firm.
TRADING AND PRINCIPAL INVESTMENTS
Once again in 2003, Trading and Principal Investments
produced excellent results. Net revenues were $10.44
billion, a 21% increase from 2002. Pre-tax earnings
were $3.51 billion, a 64% increase from 2002.
We measure the effectiveness of our trading businesses
by evaluating overall profitability relative to the
risk we assume and the opportunities available. While
there is no perfect measure of market risk, a topic
well discuss later in this letter, our risk levels were
higher in 2003 than in 2002. We were very pleased
with the results our businesses were able to produce by
effectively deploying incremental capital.
Fixed Income, Currency and Commodities (FICC)
had another record year, with net revenues of $5.60
billion, a 20% increase from 2002. During 2003, FICC
operated in a generally favorable environment characterized
by tightening corporate credit spreads, low
interest rates, a steep yield curve and strong customer
demand. As we look forward to 2004, we do not see
clear signs that FICC activity levels will slow.
However, we know that there is no such thing as a
trading backlog and our business opportunities will
always depend on the overall environment.
One important aspect of our FICC business that is
often overlooked is the range and diversity of activities
it comprises. Within the five major areas of FICC
interest rates, credit, mortgages, currencies and commodities
are a wide range of individual operations
around the globe. While there can be no guarantee
about performance in any of our businesses, we believe
that this diversity is an important strength.
Our Equities business continued to face a very challenging
environment. Equities net revenues of $4.28
billion increased 7% compared with 2002, primarily
due to higher net revenues in principal strategies. While
equity markets certainly improved in 2003 relative to
the previous few years, conditions remained tough.
Commission rates and spreads have continued to
decline, the need to commit capital in a variety of circumstances
is rising and volume growth is low.
At Goldman Sachs, we have focused on the optimal
size and structure for our Equities business in this difficult
environment. We are pleased with the results of
this effort, which we believe will be an important driver
of future performance.
Beginning with the appointment in 2002 of common
management for our securities businesses, we
have been more closely coordinating the activities of
our FICC and Equities businesses to share best practices,
capture synergies and drive efficiencies. In 2003,
we continued this work, combining our Equities cash
and derivatives client businesses under one leadership
team. This builds on the experience of a similar combination
in FICC in 2000 and will position us well to
capture a range of opportunities.
RESEARCH
Research remains a critically important part of the
Goldman Sachs franchise. We believe that a strong,
differentiated research effort that is firmly aligned with
the interests of our investing clients will be an important
part of our Equities business for many years to come.
Of course, 2003 began with the brokerage industrys
global settlement with various regulators on equities
research-related matters. As we have said before, in
hindsight we and others could have done a better job.
However, we had already begun implementing changes
in our research business long before the final settlement,
and we have been working diligently to comply
in every respect with the new ground rules.
We will continue to shape our research so that the
qualities our clients value mostindustry expertise,
independent thinking and timely insightsare at the
forefront of our proposition to investors.
SLK SPECIALIST
Our SLK stock specialist business, as well as the other
specialists on the New York Stock Exchange (NYSE),
have been, and continue to be, the subject of an intense
regulatory review and enforcement action. Much attention
has focused on the conduct of specialists within
the existing NYSE rules, as well as potential changes
to the rules governing the role of specialists.
The role of the specialist and the rules of the NYSE
should be evaluated against the proper objectivethe
development of the best market structure for all participants.
In our view, the market structure of the future
can take any number of forms, but the goal should be
to produce a system that is fair to all investors and one
where liquidity is maximized and investors can prioritize
price, speed and cost of execution.
We have in the past stated that we are not wedded
to any particular market structure. Goldman Sachs is
active and successful in markets around the world,
many of which make greater use of electronic interfaces
than the NYSE. In fact, we have invested in a
number of electronic trading platforms. We believe,
however, that the specialist performs a valuable role in
maintaining an orderly market, particularly during
times of market stress or when there are imbalances or
dislocations with regard to a single stock. This system
has helped to maintain the NYSE as the leading equities
exchange in the world. We also believe investors
are best served by large liquidity pools and greater use
of electronic structures. By centralizing, rather than
fragmenting, liquidity, investors will achieve the best
possible outcomes.
At this point, we are unable to predict the outcome
of the regulatory review or the impact of potential
reforms. We can assure you, however, that we will
continue to cooperate fully with the regulators and to
assist them in every way we can to develop the best
equities market possible. Whatever changes are
adopted, we feel confident in our ability to compete
successfully across our equities franchise, which
remains an industry leader.
RISK MANAGEMENT
It is often reported in the media that we have increased
our trading risk in recent years to offset the decline in
investment banking activity levels. In fact, the same
shocks and trends in the economy that have led to the
sharp declines in investment banking have also created
significant trading opportunities for our clients and for
Goldman Sachs.
We believe our willingness to take significant trading
risk for appropriate reward is one of the distinguishing
features of our firm and gives us a competitive advantage.
We benefit in the marketplace because our
clientsmany of whom are among the worlds largest
and most sophisticated institutionsvalue our ability
to tailor solutions and our willingness to commit our
capital to meet their needs.
Given the significant levels of risk we run in our
business, we consider risk management to be one of
our most vital functions. Risk management begins at
the top of the firm with the establishment of risk limits
for major business units and the involvement of our
most senior people in critical decisions. In setting limits,
there is no magic formula. But we do size our risk in
proportion to our capital base and our overall earnings
power. Since we went public in 1999, our shareholders
equity has grown more than threefold, to $21.63
billion as of November 2003.
One principal tool we use to measure market risk
is Value at Risk (VaR), a statistical measure of the
potential loss in value of our trading positions due
to adverse market movements over a defined time
horizon with a specified confidence level. We use a
one-day horizon and a 95% level of confidence. This
means that, about once a month, our actual daily trading
revenues should be less than our expected daily
trading revenues by an amount at least as large as our
VaR. In 2003, our average daily VaR was $58 million.
As important as it is to understand what VaR is
telling you, it is important to understand what it is not
telling you. VaR in no way captures a worst case
scenario. Shortfalls on a single day can exceed reported
VaR by a large amount. In addition, shortfalls can
accumulate over a longer period, such as a number of
consecutive trading days.
As a predictor of trading results, VaR historically
has worked relatively well in stable markets and less
well in very volatile and illiquid markets. Recognizing
this fact, we also perform various scenario analyses,
asking What if? about any number of possible
events. These scenarios are used to establish limits and
to attempt to keep our risk manageable.
No one likes trading losses, but they are a feature
of our business. In fact, it is our willingness to tolerate
such occasional, sizeable losses that enables us to earn
attractive returns over time. And, even when our trading
businesses are performing well, results can be uneven.
BALANCE SHEET AND LIQUIDITY
In 2003, our balance sheet grew, reflecting the higher
activity levels in our trading and investing businesses
and our willingness to deploy our capital to serve
clients. While we face risks across every aspect of our
business, access to liquidity remains the single most
important issue for any financial services firm. There
are many events that can create problems for a financial
institutionmacro events, trading losses, reputational
damages, to name a fewbut the most serious
is a lack of liquidity.
As a result, we place major emphasis on assuring
our access to liquidity. The cornerstone of our
approach is a cushion we maintain in the form of
cash and highly liquid securities that averaged some
$38 billion in 2003. We think of this cushion as liquidity
support in the event of unexpected dislocations in
financial markets.
DERIVATIVES
The use of derivatives, a key part of our trading activity,
has been the subject of increased debate recently.
Derivatives span a wide rangefrom a simple interest
rate swap that allows a company to convert its fixed
rate borrowing into a floating rate exposure, to more
complex instruments like credit derivatives that allow
investors to hedge risk associated with credit exposures
in their portfolios.
While derivatives can be misused, they are a vital
tool for risk management and risk dispersion. Over the
last few years, we have witnessed seven of the ten
largest corporate bankruptcies ever with surprisingly
little dislocation in global credit markets. The dispersion
of risk permitted by derivatives appears to
have contributed very significantly to this outcome.
Moreover, those financial institutions that have had
some of the greatest problems in recent years were
hurt in many cases by large unhedged exposures to
equity markets.
Of course, the use of derivatives, like other financial
instruments, requires a rigorous, hands-on approach to
risk management and control. These features were
notably absent in some companies whose troubles have
been widely publicized. At Goldman Sachs, we maintain
an independent control function that monitors all of
our trading positions and independently verifies their
fair value.
PRINCIPAL INVESTMENTS
After two disappointing years, Principal Investments
produced positive results in 2003. Net revenues
increased to $566 million, up from negative $35 million
in 2002. The increase was attributable to gains and
overrides from real estate and corporate investments,
including a $293 million unrealized gain on our investment
in Sumitomo Mitsui Financial Group (SMFG).
We expect the results from this business to be
uneven, as there is a time lag between investing and
harvesting. In 2003, we were fortunate to make a
number of significant investments and we are optimistic
about their return potential. We believe we
have improved the quality of the portfolio in the last
few years and have made some substantial investments
that can drive the performance of this business over
the cycle.
We also sold some successful investments during
2003, including part of our holding in Kookmin Bank
in South Korea. This deal highlights some of the characteristics
of our business that we believe stand us in
good stead for the future: our global reach and relationships,
our willingness to deploy significant capital
swiftly and our ability to take a long view.
Our investment in convertible preferred stock of
SMFG carries with it significant volatility. We hold the
investment at fair value, which is derived primarily from
SMFGs common share price. Since our investment in
February 2003, SMFGs share price has closed as low as
164,000 and as high as 665,000. Significant changes
in SMFGs share price produce significant changes in
the fair value of our investment, which we report as
net revenues each quarter.
Our decision to invest in SMFG was based on our
long-term confidence in Japan and the opportunity to
strengthen our relationship with one of the most
important financial institutions in Japan. Short-term
fluctuations in the value of SMFG are not cause for
particular concern to us, as long as the fundamentals
of SMFG are steady and improving, which we believe
to be the case.
ASSET MANAGEMENT AND
SECURITIES SERVICES
Our Asset Management and Securities Services business
had an excellent year. Net revenues were $2.86
billion, a 14% increase from 2002. Pre-tax earnings
were $968 million in 2003, compared with $947
million in 2002.
In Asset Management, net revenues were $1.85 billion,
a 12% increase from 2002, primarily reflecting
an increase in average assets under management, the
contribution from the acquisition of Ayco and increased
incentive income. Total assets under management
increased 7% to a record $373 billion. Two closely
related factors are critical to building our business: the
ability to generate attractive investment performance
and the ability to raise new assets.
During 2003, our investment performance, together
with our distribution strength, allowed us to generate
$15 billion of net client inflows across non-money
market asset classes. These increases were offset by
$19 billion of outflows in money market assets, as
economic prospects improved and higher returns were
being generated in other asset classes.
Within our Securities Services business, net revenues
were $1.01 billion, up 17% from 2002. This business
continues to benefit from the creation and growth of
new and existing hedge funds. In addition, the rally in
equity markets helped to increase customer balances.
We believe our Asset Management and Securities
Services business represents one of our best opportunities
for substantial growth.
EXPENSES
Managing expenses in this challenging environment
has been one of our highest priorities. During 2003,
excluding the impact of acquisitions, we reduced our
headcount by 7%. While painful, these reductions
were necessary to scale our operations to the available
opportunities and were a key factor in our ability to
generate an attractive return in 2003.
Our largest expensein a people businessis
compensation, and we track this expense as a percentage
of the net revenues we generate in our businesses
overall. For 2003, the ratio of compensation to net
revenues was 46% versus 48% in 2002.
We remain focused on controlling our non-compensation
expenses. However, there are some areas that
remain difficult to forecast. For instance, in 2003 we
took provisions of $159 million for a number of litigation
and regulatory proceedings. Given the range of
litigation and investigations underway, these expenses
may remain high.
STRATEGY AND COMPETITIVE DYNAMICS
Goldman Sachs is not a financial services conglomerate
but an integrated investment bank, securities firm and
asset manager. This focused strategy has allowed us
to build a strong global franchisewe take pride in
being a market leader in Europe and Asia as well as
the United States. It has also allowed us to benefit
from the long-term growth of the global capital markets
which we believe will continue to provide us with
excellent growth opportunities over the cycle.
We aspire to be the preeminent global provider of
advisory, financing, investment and risk management
services to corporations, institutions, governments and
high-net-worth individuals. To succeed in this mission,
the firm has always placed great reliance on attracting
and retaining outstanding people. And we work hard
to foster teamwork and encourage creativity, client
focus and innovation. We believe that our unique culture,
coupled with the quality of our people, is
Goldman Sachs most important competitive strength.
Our business has always been highly competitive
and cyclical. We face strong competition today, as in
the past, from larger competitors, but we dont view
our size as a competitive disadvantage because we
have never been constrained by a lack of capital. We
believe our biggest challenge is to strengthen our culture
of teamwork and excellence in the face of the growing
size and scope of our business. We are determined to
meet this challenge because we believe our ability to
do so is critical to our continued success in executing
our global strategy and serving our clients.
STRATEGIC TRANSACTIONS
In 2003, we completed a number of strategic transactions.
Our first announcement involved our $1.25
billion investment in SMFG, which we mentioned
above. We are pleased with the performance of our
investment as well as the other aspects of our relationship
with SMFG.
With the credit loss protection provided by SMFG,
we initiated our William Street credit extension program.
This capability has given us an innovative way
to extend credit selectively to our investment-grade
clients, while reducing our credit and liquidity risks.
By the end of fiscal 2003, $4.32 billion of credit commitments
had been made under the program. In addition,
our business cooperation agreement with SMFG
has already resulted in a number of initiatives. In
October, we announced the formation of a joint venture
to facilitate the corporate recovery of certain SMFG
borrowing clients and to accelerate SMFGs plans to
improve its asset quality.
In September, we combined our Australian operations
with JBWere to create a new venture called
Goldman Sachs JBWere. We own 45% of the new entity,
one of the leading investment banking and securities
firms in Australia.
We also made several acquisitions in 2003. Our
approach to acquisitions is to strengthen our business
and build shareholder value, principally through emphasizing
bolt-on deals where we can add new clients or
acquire new products to provide to our existing clients.
2003 offered us a number of such opportunities.
In July, we acquired The Ayco Company, a leading
provider of sophisticated, fee-based financial counsel-
ing in the United States. Ayco enables us to develop
further our high-net-worth and asset management
businesses by using its extensive portfolio of financial
planning capabilities, including tax, estate and charitable
planning services.
We also made two acquisitions of power generation
assets. In October, we acquired East Coast Power, owner
of the 940-megawatt cogeneration facility in Linden,
New Jersey, which sells some 80% of its power to
the New York City market. In the same month, we
announced the acquisition of Cogentrix Energy, an
independent power producer based in Charlotte, North
Carolina, adding interests in 26 power plants and 3,300
megawatts of generating capacity to our portfolio.
These generation facilities were acquired to complement
our existing commodity trading and merchant
energy restructuring capabilities. Of course, ownership
of physical power assets brings incremental responsibilities
of which we are particularly mindful.
BOARD OF DIRECTORS
In June, we announced that Claes Dahlbck, nonexecutive
Chairman of Investor AB, and Edward M. Liddy,
Chairman of the Board, President and Chief Executive
Officer of The Allstate Corporation, were joining
our Board of Directors. Claes and Ed are both distinguished
business leaders as well as thoughtful corporate
directors, and we welcome the contributions they
are already making to the Board. The Board has also
nominated Lois D. Juliber, Chief Operating Officer
of the Colgate-Palmolive Company, to stand for election
to our Board of Directors at the March 31 Annual
Meeting. In addition, John Thain and John Thornton
retired as directors and we thank them for their service
on our Board and to our shareholders.
OUTLOOK
On balance, 2003 proved to be a more favorable operating
environment than we expected at the beginning
of the year. Markets rose, economic growth improved
in most major economies and business confidence rose.
Although we expect these trends to continue in
2004, we cannot, of course, predict with certainty
what global events, economic or political, in fact will
shape the markets in which we work. But, we can
and willpursue a strategy that permits us to seize
business opportunities in an environment of continuing
uncertainty and possible volatility. We owe you, our
shareholders, nothing less. In terms of our own industry,
it is clear that, if anything, regulatory scrutiny has
intensified in the U.S. and in other countries around
the world. We anticipate that this will continue to be a
feature of the environment in which we operate.
That said, we look forward with confidence to
2004 and beyond. Our franchise is stronger than ever.
Our people remain focused, dedicated and enthusiastic.
We are committed to serving you, our shareholders, by
delivering long-term growth and by producing real
value for our clients through products and services
that strengthen the global capital markets and support
economic growth.
henry m. paulson, j r.
Chairman and Chief Executive Officer
lloyd c . blankfein
President and Chief Operating Officer