Dear Fellow Stockholders,
The strong performance by The Este Lauder Companies in fiscal 2016 is a testament to the
strength of our diversified business and multiple engines of growth, disciplined strategy
and capacity for execution. Our results reinforce our entrepreneurial spirit, which inspires
our Company to innovate and navigate change to continue exceeding the expectations of
prestige beauty consumers worldwide and delivering value to stockholders.
When Mrs. Este Lauder founded our Company, she challenged the luxury beauty establishment
at the time with groundbreaking products and a High-Touch experience that enabled her
to develop a personal connection with consumers. Since then, we have continued to lead in
prestige beauty by building a world-class brand portfolio, developing high-quality, innovative
products, and cultivating a culture of anticipating and embracing change. We continue to be,
at heart, an entrepreneurial company that breaks accepted norms of our industry.
The dynamics of our industry are changing, and todays global prestige beauty consumers
increasingly seek unique experiences from brands with authentic points of view across platforms.
Our ability to quickly and nimbly shift resources to our most compelling opportunities, our
ability to deliver aspirational, High-Touch brand experiences and our talented team enable us
to continue to deliver industry-leading performance in an evolving environment. Importantly,
we are continually seeking ways to execute even more efficiently.
In fiscal 2016, we delivered net sales of $11.26 billion, adjusted net earnings of $1.21 billion and adjusted
diluted earnings per share of $3.20. In constant currency, adjusted net sales rose 7 percent, which was
ahead of global prestige beauty growth, and adjusted constant currency earnings per share increased
13 percent.* Cash flow from operations was $1.79 billion. During the fiscal year, we increased our
dividend rate by 25 percent, repurchased $890 million of our outstanding shares and returned over
100 percent of free cash flow to stockholders.
We capitalized on shifting consumer preferences across categories and regions to fuel our growth in
makeup and fragrance during fiscal 2016. We also saw strong performances from our online, specialtymulti
and freestanding store channels. Our creative approach to digital engagement, particularly across
social media, contributed to these changes, driving consumption both in-store and online.
Our results reflect our well-diversified, brand-building powerhouse of unrivaled creativity and innovation.
We are confident that we are well-positioned to sustain our strong and profitable growth over the long-term.
PRESTIGE BEAUTY: DYNAMIC AND GROWING
Our Company has a broad geographic footprint  our products are sold in nearly every corner of the
world. We see long-term opportunities to reach more global prestige consumers with our portfolio of
over 25 brands. Moreover, we are fortunate to be the only large company solely focused on prestige
beauty  a thriving, growing segment of the beauty industry.
The growth of prestige beauty is propelled by powerful and enduring global megatrends, including
greater consumption in emerging markets that could comprise 25 percent of prestige beauty retail sales
in 10 years, and the simultaneous expansion of the global middle class which is occurring at a brisk pace.
The industry should also benefit from changing demographics in the United States, where half the
population will be over the age of 50 by mid-2017 and enjoy greater disposable income. In addition to
these Ageless consumers who continue to inspire our efforts, Millennial consumers  born between
1980 and 1998  have demonstrated their insatiable appetite for innovation, novelty and individuality.
We view Millennials fresh approach positively and believe it reinforces prestige beauty as a high-growth
consumer area, in many cases exceeding mass beauty in key countries around the world.
SECURING OUR FUTURE LEADERSHIP:
OUR 10-YEAR COMPASS
The diversity of our brands and products, combined with our social media capabilities, enables us to
capture unique, of-the-moment opportunities. For the longer-term trends, we turn to our 10-year
Compass, an important tool that identifies future trends that we believe will drive prestige beauty and
our most promising growth areas.
Staying ahead of the curve is critical to success in our industry. For example, the Millennial generation is
now the largest demographic group in the world and continues to inspire the prestige beauty landscape.
While the current Millennial focus on instant camera-ready beauty is driving growth in makeup today,
we anticipate that their demand for skin care products will grow as they age.
We are also focused on positioning our brands as leaders with Generation Z. These consumers, born
after 1998, will soon outnumber Millennials. They are the first generation to be raised in the era of the
smartphone  many do not remember a time before social media  and, in the United States, they will
be the most ethnically diverse generation in history. We are hard at work developing the innovative
products and experiences to delight these new, younger consumers and draw them to our brands.
Today, beauty consumers from all generations expect seamless experiences among online, mobile and
offline channels, and we are continuing to enhance our omnichannel capabilities to serve them.
For example, consumers can book makeovers online at MACs first ever Makeup Studio store and at
Bobbi Browns United Kingdom beauty counters. These online bookings have increased store traffic and
strengthened our consumers connection to our brands. Additionally, digital platforms like Cliniques
The Wink offer creative and engaging ways for a consumer to interact with the brand seamlessly  from
her laptop or mobile phone to the department store.
We are also building our presence in emerging product subcategories. Just as we are meeting the
increased demand for products in high-growth subcategories like masks and lipsticks, we are focusing
on developing innovative products in other high-growth subcategories for the future.
LAYING THE FOUNDATION FOR THE FUTURE
With an eye to the future, we continue to make strategic decisions to position us for continued leadership
in prestige beauty. In the second half of fiscal 2016, we launched Leading Beauty Forward, a multi-year
initiative to build on our strengths and better leverage our cost structure to free resources for investment
to continue strong growth momentum. This initiative is designed to fuel our long-term profitable growth,
enhance our go-to-market capabilities and reinforce our leadership in global prestige beauty.
Leading Beauty Forward is expected to help increase our effectiveness by infusing additional speed and
agility into our business, so that we can more quickly respond to our changing industry. It should also
help us create a better, more efficient cost base and improve productivity to continue investing in our
brands growth and consumer-facing efforts, including new products, social media, communications,
in-store merchandising, point-of-sale activities and advertising.
Cost savings from the initiative are expected to provide more resources to continue improving profitability,
while the Company further builds its business to be more sustainable, efficient and effective.
ENGINES OF GROWTH
STRONG GROWTH IN MAKEUP AND LUXURY FRAGRANCE
Our makeup-focused brandsMAC, Bobbi Brown and Smashboxdrove growth in our high-performing
makeup category, providing instant beauty for the selfie generation. Compelling social media
campaigns like MACs MIX + MASH global Instagram program, and the wide availability of digital
tutorials, contributed to the categorys strong performance. Additionally, our multi-category brands,
Este Lauder and Clinique, experienced solid growth in makeup on a global basis.
Adjusted sales in this vibrant category grew 13 percent in constant currency in fiscal 2016, reflecting
double-digit gains in constant currency every quarter. Our growth was balanced across regions,
including Asia/Pacific  a market that is traditionally focused on skin care  led by Este Lauder, Clinique,
Bobbi Brown and MAC. In fact, MAC is the top prestige beauty brand in several markets in the region.
Our high-end brands lifted the fragrance categorys performance in fiscal 2016, inspiring consumers
around the world with products that offer authentic and unique points of view. Our artisanal fragrances
emphasized distinguished voices in this space, with a focus on craftsmanship, artistry, originality and
individuality. We were very pleased with the performance of two of our newest additions to our
fragrance portfolio, Le Labo and Editions de Parfums Frdric Malle. Jo Malone London and Tom Ford
saw exceptional results on a global basis, with double-digit sales growth in every region.
We continue to believe that luxury fragrance will outpace the broader fragrance category, and we are
investing to weight our portfolio towards this tier. To that end, we welcomed By Kilian to our Company
this year  a prestige fragrance brand that embodies sophistication and modern luxury. This brand has
terrific potential for global expansion, and we look forward to nurturing its growth.
CONTINUED CHANNEL DIVERSIFICATION
Over the past 20 years, we have evolved our distribution to reach more consumers in high-growth prestige
channels, and we anticipate that this will continue into fiscal 2017 and beyond. While our distribution
strategy differs across our brand portfolio, our focus is on diversified channels that enhance our global
reach and connect us with our consumer when and where she shops, whether at a store counter or on
her mobile phone. We have many opportunities for greater consumer reach in each of our regions. For
example, MAC is not as widely distributed as our other brands. In fact, it has approximately one tenth the
number of doors of Este Lauder and Clinique, and about half the distribution of several direct competitors.
We believe there are millions of untapped new consumers whom MAC can potentially reach.
Department stores remain our largest channel globally, and we experienced strong growth from our
stores in international markets and online. At the same time, we are sharply focused on faster growing
channels around the world. This year, we enjoyed strong results from e- and m-commerce, specialtymulti
and freestanding retail stores.
For the first time in our history, we exceeded $1 billion in online sales through both Company and
retailer e- and m-commerce platforms, an increase of 27 percent in constant currency. As global internet
connectivity increases, seamless shopping experiences across distribution channels will eliminate the
term offline. This will be an incredible opportunity for companies with effective strategies for online
and mobile channels, two of the key investment areas we are targeting through Leading Beauty Forward.
We are looking to our online success in the United States and the United Kingdom as models for how
to best add brands and depth in other existing geographies, while driving expansion into new ones. We
are also enhancing our mobile capabilities, which were responsible for over 35 percent of our global
online sales in fiscal 2016.
We strengthened our presence in specialty-multi retailers this year, where our sales rose double-digits. Our
global business at Sephora grew double-digits, with strong success from Smashbox and GLAMGLOW.
Este Lauder launched the Millennial-oriented line, The Este Edit, exclusively in North America in
Sephora stores and on sephora.com. We are also encouraged by our business with Bluemercury, the
most high-end focused specialty-multi retailer in the United States, where some of our fastest growing
brands are featured. In Ulta, we are pleased with our rapid double-digit growth. Outside of the United
States, we have strong growth in Boots and Douglas, led by Smashbox.
In fiscal 2016, freestanding stores accounted for over 10 percent of our business and our sales in the
channel rose double-digits. We are focused on building new retail capabilities, testing new store formats
and connecting the consumer to online via omnichannel initiatives. Freestanding stores embody a
brands identity and represent a great opportunity to engage in an experiential prestige environment.
Additionally, they allow us to test and launch new concepts, such as Origins Discovery retail design
which illustrates the journey from plant to formula. Jo Malone London opened its first Global Premier
Boutique on Londons iconic Regent Street in fiscal 2016, offering consumers the very best in experiential
retail such as an indoor scented garden, Artisans Studio for gift customization, and first ever interactive
digital Fragrance Combining Cabinet.
Overall, international passenger traffic growth was solid across all regions in fiscal 2016, helping to drive
growth in the Travel Retail channel. Our strategic approach to the channel emphasized diversification
by region, category, brand and type of airport, and we currently lead in prestige skin care and makeup
combined. We are positioned for even greater success in Travel Retail as we nimbly deploy resources to
areas with greater traffic. We are expanding our offerings in luxury fragrance  a category that performs
well in the channel, partly due to travelers purchasing fragrances as gifts  with brands like Tom Ford,
Jo Malone London and Le Labo. We see tremendous opportunity in the channel over the long-term,
particularly given that air travel is expected to double over the next 15 years.
INNOVATIVE DIGITAL ENGAGEMENT
Our strategy to win with todays digitally savvy consumers is constantly evolving  much like the consumers
themselves  and continues to be grounded by our High-Touch approach to consumer engagement
and retention. Reaching our consumers in a High-Touch manner has been part of our Companys DNA
since the beginning. We are building on this unique strength for the digital age, as consumers expect an
instant, seamless shopping experience across distribution channels. We have produced popular how-to
videos, makeup tutorials from style-experts and tips from key influencers, enabling our consumers to
establish a personal rapport with our brands both in-store and online.
MAC communicates with more than 26 million followers across its digital platforms, offering inspirational
looks and technique tips from its talented pool of over 20,000 global artists. Jo Malone London is the
number one beauty brand on Pinterest, leveraging the platforms focus on lifestyle and bridal trends.
And, La Mer launched its Celebration of an Icon campaign in Los Angeles by sharing behind-thescenes
content on Snapchat.
CONTINUED GLOBAL SUCCESS
Geographically, each of our regions contributed to constant currency growth in fiscal 2016. We are
especially proud of the success of our business in Europe, the Middle East & Africa (EMEA), which
demonstrated resilience in a challenging environment. Adjusted net sales increased 12 percent in constant
currency, driven by the strong performance of the makeup and fragrance categories. By understanding
consumer insights to target demographic pockets with the right brands, products and services, we have
been successful at launching new brands and reaching consumers wherever they shop. Many of our
launches have resonated well, and we believe we are number one in prestige skin care and makeup
combined in the EMEA region.
In the Americas region, adjusted net sales rose 5 percent in constant currency, led by double-digit increases
in Canada as well as Latin America, where we became the number one prestige beauty company this
year. Our business in the United States rose double-digits in specialty-multi stores and online. However,
weak traffic in mid-tier brick and mortar department stores and tourist-driven doors curtailed sales
growth in the United States to low-single-digits. Inbound tourism in the United States declined as the
strong dollar led tourists to seek alternative destinations.
In the Asia/Pacific region, adjusted net sales rose 4 percent in constant currency, with the strongest
growth from important markets like Australia, Korea and Japan. Shifts in regional consumer preferences
accelerated sales growth in the makeup and fragrance categories. In fact, we are leading the growth
of luxury fragrance in the region, and believe we grew our share nearly four points in the category in fiscal
2016. Jo Malone London ranks among the top three prestige fragrance brands in key markets in Asia/Pacific.
Additionally, online sales now represent more than 6 percent of our total sales in Asia/Pacific, with net
sales growth of 48 percent in constant currency versus last year and double-digit increases in every market.
Our business in Hong Kong remained weak as Chinese travel to the area continued to decline.
Our business in emerging markets continues to develop rapidly. Net sales in constant currency rose
15 percent overall and jumped 25 percent excluding China, with the strongest growth from Brazil, Turkey,
Mexico and Russia. We continue to build our presence in emerging markets, bringing our brands and
high-quality products to new consumers around the world. MAC continues to be a trailblazer in
sub-Saharan Africa, where it most recently entered Zimbabwe. The brand also offered locally relevant
products, such as its Modern Twist Kajal eye liners, inspired by trends in the Middle East. In Brazil, a strategic
emerging market for us, Jo Malone London and La Mer were introduced and are performing strongly.
We are committed to China and Chinese consumers around the globe. Our business this year was
healthy, as we entered 14 new cities, with most brands growing double-digits in constant currency and
retail sales gaining momentum. We have strategically diversified our business in China across category
and channel, as consumer preferences shift to makeup and fragrance as well as online purchasing. Our
online business had the fastest growth of over 70 percent and accounted for approximately 10 percent
of total sales in China, reflecting success at both our brand sites and third party sites like T-Mall.
WORLD-CLASS INNOVATION
Innovation and creativity are key pillars of our success at The Este Lauder Companies. We demonstrated
our dedication to creativity, as well as speed and agility in executing our innovative ideas, throughout
fiscal 2016. For example, over half of our R&D initiatives were completed in less than 12 months, and
approximately 24 percent of our global sales were from new products. Our global patent portfolio has
increased 40 percent in the last four years, primarily in skin care and strategic areas with high consumer
demand. Our newest patents cover technology that will anchor high-quality products that we believe
will surprise and delight our global consumers.
Our innovations across high-growth skin care subcategories are resonating with consumers, such as
masks and other products with instant benefits. These include Smashboxs Primers, Este Lauders
Advanced Night Repair Concentrated Recovery PowerFoil Mask, Origins Original Skin Retexturizing
Mask with Rose Clay for instant radiance, GLAMGLOWs masks for various skin care concerns, and
Cliniques Pep-Start Hydroblur Moisturizer.
In addition to our employees, we look to our most demanding consumers around the world as inspiration
for innovation. In particular, Korea, Japan and California are sources of innovation. To that end, we invested
in the South Korean brand Dr. Jart+, one of Koreas most promising, high-growth skin care brands.
AN EXTRAORDINARY TEAM: DEVELOPING
TALENT WITH AN EYE TO THE FUTURE
Our Companys success is fueled by our talented and dedicated employees, including an unparalleled
Executive Leadership Team. As we have consistently demonstrated, we have the right blend of experienced
industry experts and enthusiastic change agents to win in this highly competitive environment against a
backdrop of political and economic volatility.
This year, many of our most talented and creative up-and-coming leaders stepped into important new
roles that will allow them to have a broader impact on our success and to further develop their skills
and talents. We continue to bring on new talent to create the products of the future and partner with
entrepreneurs behind some of the most high-growth brands in the prestige beauty space today  brands
that have the potential to become the global, iconic brands of the future.
One of our unique strengths is our focus on emphasizing education and leadership at all levels. We
are energized by our reverse mentoring program, matching highly engaged members of our executive
leadership with our empowered Millennial employees. The Este Edit, Clinique Pep-Start and the launch
of Smashboxs freestanding store concept are successful examples that resulted from this collaboration,
and we have more creative ideas in the pipeline.
We take pride in an employee base that joins us from all over the world. Inclusivity and diversity are
hallmarks of our Company, and we continue to recruit a workforce that increasingly reflects the
consumers we serve. We are particularly proud that women represent the majority of our global
workforce and over half of our senior leadership positions. Our results in fiscal 2016 are a testament to
the execution by our people around the globe, and we thank them for their hard work and commitment.
We are also extremely fortunate to have a Board of Directors comprised of individuals with diverse and
complementary business and leadership experiences. The members of the Board provide invaluable
advice and counsel to our management team, review the Companys strategic plans and support
managements focus on long-term value creation. I know I speak for our entire senior leadership team
in thanking the members of our Board for their counsel and oversight as we position the Company for
continued success.
FOCUSED ON THE LONG-TERM
The Lauder Familys dedication to preserving the entrepreneurial spirit of the Company and its
commitment to long-term growth are important underpinnings of the success of The Este Lauder
Companies. As William Lauder has said, our strong heritage and values infuse everything that we do,
and I believe that this dedication truly distinguishes our Company from our industry peers.
It is my privilege to partner with William, whose unfailing strategic capacity, dedication to cultivating
the leaders of the future and commitment to sustainably running our business are integral to our
Companys performance.
We are well-positioned for fiscal 2017 to be another year of strong growth for The Este Lauder Companies.
We remain focused on the fastest-growing segments of our industry, and we continue to build on the
success of our various categories in emerging markets as we capitalize on expansion opportunities
for our mid-sized brands and nurture our smaller brands. We expect to see increased consumer reach
for our brands across the channels in which our products are sold. Our innovation in these areas will be
key to supporting our continued global leadership.
Our diversified business is anchored by multiple engines of growth, a wealth of creativity and the ability
to quickly react and adapt to events and trends, making us optimistic about our long-term success. Over
the next three years, we expect to generate sales growth of 6 to 8 percent and double-digit EPS increases,
excluding restructuring and other charges, in constant currency. For fiscal 2017, we expect to once again
outpace global prestige beauty growth and gain share, despite external headwinds and challenges.
As we continue to anticipate and navigate change, we remain focused on our unique strengths that
make us the leader in global prestige beauty: our unrivaled brand portfolio; our amazing global talent;
our commitment to high-quality, innovative products and prestige services; and our outstanding
High-Touch approach. We will continue to build on the strengths that have propelled our growth,
while embracing new opportunities for even greater success tomorrow.
I thank you, our valued stockholders, for your continued support as we look ahead to great achievements
in fiscal 2017 and beyond.
Fabrizio Freda
President and Chief Executive Officer