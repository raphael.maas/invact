                                                                                            the health and expansion of New York's
                                             as they've been over the past year.
Reliable. Resourceful. Responsible.
                                                                                            economy. We are, in effect, a partner and
                                             The hard work of our 14,000 employees
                                                                                            platform for job creation in the nation's
                                             enabled us to meet our own high
Con Edison has maintained an
                                             standards of service and reliability,          capital of nance, media, insurance,
unwavering focus on the basics
                                                                                            and real estate. The smooth functioning
                                             and those of our customers. The role
                                             Con Edison and its people played in            of this dynamic economy depends
This past year was one of continued
                                                                                            heavily on uninterrupted energy service.
                                             the rebuilding of lower Manhattan after
accomplishment and outstanding service
                                                                                            Our transmission and distribution
                                             the events of September 2001 was
for your company. During a period of
                                                                                            infrastructure enables customers to build,
                                             instrumental in the city's recovery.
uncertain capital and energy markets and
                                                                                            develop, and manage their businesses
extreme volatility in our industry, Con
                                                                                            effectively. In so doing, we support the
                                             The service areas of our regulated
Edison maintained an unwavering focus
                                                                                            local economy, and encourage growth in
                                             companies, Con Edison of New York and
on the basics of our core business and a
                                                                                            the employment and tax bases. As
disciplined approach to new opportunities.   Orange and Rockland Utilities, include
                                                                                            business grows and demand for energy
                                             the nation's most important commercial
                                                                                            rises, so does our need to continue to
                                             hub and some of its most densely
In 2002 Con Edison provided excellent
                                                                                            build and modernize our systems.
                                             populated residential areas. In all, we
service to customers and solid returns
                                             serve nearly half of New York State's
to shareholders. We showed that integrity,
                                                                                            We strive to communicate openly and
                                             population and deliver 44 percent of all
consistency, and diligence can lead
                                                                                            fairly with investors
                                             electric energy consumed in the state.
to positive results even under difcult
conditions. In recognition of our            All our customers  whether they are
                                                                                            Earnings in 2002 were $668.1 million,
                                             tenants of the Empire State Building or
accomplishments, Platts/BusinessWeek
                                                                                            or $3.14 per share, before one-time
                                             residents of Middletown  need an energy
named Con Edison Company of New York
                                                                                            charges to recognize the cumulative
"Energy Company of the Year," citing         supply they can count on. The reliability
                                                                                            effect of new accounting principles.
                                             of our system assured that record usage
overall excellence in safety, delivery of
                                                                                            This was $.08 per share lower than in
energy, customer care, technological         levels in the summer of 2002 could
                                             be met, a testament to our preparation         2001, reecting the soft economy,
innovation, and environmental concern.
                                                                                            the very warm winter of 2001-02, and
                                             and commitment.
                                                                                            a gas rate reduction, offset in part by the
Our performance in 2002 was the
                                                                                            hot summer weather and productivity
                                             Even as we leverage our expertise
result of many factors. Our nancial
                                                                                            improvements. After charges of $.11
discipline and strategic focus have          in related markets, Con Edison remains
                                                                                            per share to reect the adoption of newly
                                             intensely focused on its core business.
enabled us to maintain a strong balance
                                                                                            issued accounting rules relating to
                                             We deliver energy. And we deliver it
sheet and nancial exibility, which are
                                                                                            goodwill impairment and certain wholesale
                                             efciently, safely, reliably, and protably.
of particularly great value when the
                                                                                            energy transactions, earnings per
                                             Our future is directly aligned with
capital markets are as unpredictable






                                                                                                               percent of Con Edison's net income in
share were $3.03. Our condence in                       The nation's most reliable source
                                                                                                               2002. The growth in our business stems
                                                         of electricity, gas, and steam
the company's strength and stability is
                                                                                                               from an increase in energy usage by
reected in our dividend, which we
                                                                                                               our customers, which is to a great extent
                                                         Every day your company provides the
increased in January 2003 for the 29th
                                                                                                               a function of economic activity in our
consecutive year, to $2.24 per share.                    nation's most reliable source of electricity,
                                                                                                               service area. To that end, we continually
                                                         gas, and steam for commercial and
                                                                                                               work with government, businesses,
                                                         residential use. Con Edison of New York
In 2002 our return to shareholders
                                                                                                               and strategic partners to promote and
far outpaced the broader markets and                     serves New York City and Westchester
                                                                                                               support economic development. As we
our industry counterparts. Con Edison                    County. Orange and Rockland Utilities
                                                                                                               plan for and invest in the future, we do so
common stock ended the year at $42.82,                   serves parts of three states  Rockland,
                                                                                                               with the condence and assurance that
                                                         Orange, and Sullivan counties in
an increase of 6.1 percent for the year,
                                                                                                               stem from these important relationships.
                                                         New York; Bergen, Passaic, and Sussex
which compares very favorably with an
                                                                                                               At the same time, we constantly strive
18.8 percent decline in the Standard                     counties in New Jersey; and Pike County
                                                                                                               to increase the efciency of our operations
                                                         in Pennsylvania. These core regulated
& Poor's Electric Utilities Index and a 23.4
                                                                                                               by controlling costs and by realizing
                                                         businesses deliver energy to nearly
percent decline in the Standard &
                                                                                                               improvements from our investments in
                                                         3.5 million electricity customers, through
Poor's 500 Index. Including dividends,
                                                                                                               innovation and research.
                                                         a vast network comprising 37,872 miles of
total return to Con Edison shareholders
in 2002 was 11.9 percent.                                overhead distribution lines and 92,540
                                                         miles of underground distribution lines, the          Participating in the new energy landscape
                                                         largest underground electric distribution
Our balance sheet and overall nancial
                                                         system in the world.                                  In recent years, Con Edison has
position remain strong. Your company's
                                                                                                               adopted and adhered to a disciplined
regulated businesses maintained
                                                         Your company also serves 1.22 million                 approach to unregulated business
A+ bond ratings in 2002. Debentures
                                                                                                               opportunities. We have made investments
                                                         gas customers and approximately
issued by Consolidated Edison, Inc., our
                                                                                                               focused on a series of markets and
                                                         1,850 steam customers. Industry experts
holding company, were rated A by both
                                                                                                               sectors related to our core business.
                                                         have consistently cited Con Edison
Standard and Poor's and Moody's. Our
                                                                                                               Our four competitive businesses, which
                                                         of New York for its reliability. In 2002,
equity ratio at year-end was 48.2 percent,
                                                         an independent industry group, PA                     can operate in concert to meet
giving us one of the strongest capital
                                                                                                               customers' evolving needs, are Con
                                                         Consulting, named the company the most
structures in the industry. Investors
                                                                                                               Edison Communications, Con Edison
                                                         reliable utility in North America.
recognize and appreciate our disciplined
                                                                                                               Development, Con Edison Energy, and
and straightforward approach to nancial
                                                                                                               Con Edison Solutions. Providing such
                                                         Characterized by sound performance
matters. We strive to communicate openly
                                                                                                               services as broadband infrastructure,
                                                         and steady cash ow, our two
and fairly with investors.
                                                                                                               electric generation, and wholesale and
                                                         utility subsidiaries accounted for 97




                                                                                               magazine has once again recognized
retail management and marketing                  and other agencies, to achieve stability of
                                                                                               Con Edison of New York as one of the
services, these companies are                    energy supply and an orderly market.
                                                                                               top 50 companies for minorities.
participating in the new competitive
energy landscape.                                At Con Edison we've worked hard to
                                                                                               Contributing to the rich fabric of life in
                                                 earn and retain the trust and respect
                                                                                               our region
We never strayed from the basics                 of our stakeholders: investors,
                                                 customers, employees, policy makers,
                                                                                               We do more than work in the
                                                 and other companies in the industry.
The year 2002 proved to be a tumultuous
                                                 The credibility we've established with        communities we serve. We live in them,
one for the energy industry, as many
                                                 these constituencies is among our most        too, and contribute to their improvement
companies encountered difculties arising
                                                 valuable assets, and we protect it by         through extensive volunteer activity and
from failed investments in high-risk
                                                 adhering to high standards of corporate       through support of a wide variety of
activities, excessive leverage, or
                                                                                               educational, cultural, arts, health care,
                                                 governance and business practices.
questionable business practices. Because
                                                                                               and environmental programs.
we never strayed from the basics 
                                                 Con Edison can meet the challenges
in our mission or in our strategy  Con
                                                                                               Education is a special focus of your
Edison has avoided these pitfalls.
                                                 Our employees are dedicated to                company's engagement with the
                                                 meeting the needs of our customers            community. For example, we have
New York's restructured energy
                                                 and communities. They're On It for New        supported numerous programs, such as
markets are evolving, but without the
                                                 York, 24 hours a day. Our employees'          Green Horizons, that teach environmental
sorts of supply disruptions and price
                                                 concern for others, backed up by solid        values and practices to the next
distortions evident in some other regions.
                                                 technical expertise and a commitment          generation, and encourage career
All New York customers today have a
                                                 to keep developing their skills, is a key     development in the eld. Many of New
choice of purchasing electric energy and
                                                 asset for our company. Our talented           York's arts and cultural institutions, large
natural gas from independent energy
                                                 workforce gives us the condence that we      and small, receive support from Con
services companies. We give our
                                                 can meet the challenges we'll face in         Edison  one of the ways we contribute
customers access to the information
                                                 the coming years.                             to the rich fabric of life in our region.
and processes through which they can
make informed choices about their
                                                 We benet from serving the most diverse       Our efforts to improve the environmental
energy supply and use. We work closely
                                                 population in the country. Our workforce      quality of our region are also a daily
with government ofcials and policy
                                                                                               part of our jobs, and they are being
makers, including the New York State             reects that diversity, and we are
                                                 committed to promoting and encouraging        recognized. In 2002 we were singled out
Public Service Commission, the New York
                                                 diversity at every level of the company.      by the Environmental Protection
Independent System Operator, the
                                                 We are proud that, for example, Fortune       Agency for facilitating the removal of
Federal Energy Regulatory Commission,






mercury pressure regulators and                 systems, or in the reliability of its supply,
for waste prevention by recycling, buying       or in the performance of its stock.
recycled-content products, and reusing
                                                Rather, our strength is evident in the
equipment, thereby diverting 171 tons
                                                consistency of our long-term nancial and
of waste from landlls. And for the
                                                operational performance, in our solid
second consecutive year, O&R received
                                                balance sheet, and in our credibility in the
the Tree Line USA Award from the
                                                nancial markets. We are committed
National Arbor Day Foundation for its
environmental stewardship.                      to maintaining both operational reliability
                                                and nancial strength, so that we can
                                                perform critical work and accommodate
A broad, diverse, and experienced slate
                                                future energy needs.
of directors

                                                This past year has been a time when
Con Edison has assembled a broad,
diverse, and experienced slate of               many investors came to recognize and
                                                appreciate the importance of the basics.
directors, each of whom brings unique
                                                In education, the basics refer to the
skills and perspective to the table. In June
                                                "three Rs." At Con Edison, we've got
2002 we welcomed Frederic V. Salerno
                                                our own basic three Rs: reliability,
to our board of directors. Mr. Salerno is a
                                                resourcefulness, and responsibility. We
native New Yorker and a 37-year veteran
                                                strive to deliver the most reliable energy
of Verizon Communications. He retired
                                                service, to be resourceful in solving
from that company as vice chairman
                                                problems and harnessing technology to
and had also been chief nancial ofcer.
                                                improve operations, and to be responsible
His extensive experience in
                                                to all our stakeholders. While signicant
telecommunications, strategic planning,
                                                challenges undoubtedly lie ahead, we
and business development will be of great
                                                look forward to a long future of service
benet to our board.
                                                and accomplishment.
At Con Edison, we've got our own
basic "three Rs": reliability,
resourcefulness, and responsibility
                                                Eugene R. McGrath
Con Edison's enduring strength isn't            Chairman, President, and
measured just in the size of its distribution   Chief Executive Ofcer
