Dear Stockholder:
You are cordially invited to attend the Annual Meeting of Stockholders of Level 3 Communications, Inc.
(�Level 3�) to be held at 9:00 a.m. on May 25, 2017, at the Level 3 Headquarters, 1025 Eldorado Boulevard,
Broomfield, Colorado 80021.
As you may know, on October 31, 2016, Level 3 entered into an Agreement and Plan of Merger (the �merger
agreement�) with CenturyLink, Inc. (�CenturyLink�), Wildcat Merger Sub 1 LLC (�merger sub 1�), and WWG
Merger Sub LLC (�merger sub 2�). The merger agreement provides, among other things, that subject to the
satisfaction or waiver of the conditions set forth therein (i) merger sub 1 will merge with and into Level 3 (the
�merger�), with Level 3 surviving the merger as an indirect wholly-owned subsidiary of CenturyLink, and
(ii) immediately following the effective time of the merger, Level 3 shall merge with and into merger sub 2,
with merger sub 2 surviving the subsequent merger as an indirect wholly-owned subsidiary of CenturyLink.
The merger agreement and the merger were approved by Level 3�s stockholders and CenturyLink�s
shareholders approved the proposal to issue shares of CenturyLink common stock to Level 3 stockholders
in connection with the merger. The transaction is subject to regulatory approvals, including expiration or
termination of the applicable waiting period under the Hart-Scott-Rodino Antitrust Improvements Act, review
by the U.S. Federal Communications Commission, certain state regulatory approvals and other customary
closing conditions.
At the Annual Meeting, you will be asked to consider and act upon the following matters:
� the election to our Board of Directors of 11 directors, each for a one-year term until the 2018 Annual
Meeting of Stockholders or until their successors have been elected and qualified; and
� the approval, on an advisory basis, of our named executive officers� executive compensation; and
� the approval of a proposal to indicate the frequency in which our stockholders will conduct an advisory
vote on the executive compensation program for our named executive officers; and
� the ratification of the appointment of our independent auditor; and
� the transaction of such other business as may properly come before the Annual Meeting or any
adjournment or postponement thereof.
Our Board of Directors recommends that you:
� elect the 11 nominees for director, each for a one-year term until the 2018 Annual Meeting of
Stockholders;
� approve, on an advisory basis, of our named executive officers� executive compensation;
� approve the proposal to have the stockholders conduct an advisory vote on the executive compensation
program for our named executive officers each year; and
� ratify the appointment of our independent auditor.
Information concerning these proposals and other important information is contained in the attached Notice
of Annual Meeting and Proxy Statement. It is important that your shares be represented at the Annual
Meeting, regardless of the number you hold. To ensure your representation at the Annual Meeting, if you are
a stockholder of record, you may vote in person at the Annual Meeting. We will give you a ballot when you
arrive. If you do not wish to vote in person or if you will not be attending the Annual Meeting, you may vote
by proxy. You can vote by proxy over the Internet by following the instructions provided in the Notice of
Internet Availability of Proxy Materials previously mailed to you, or, if you requested or otherwise received
printed copies of the proxy materials, you can also vote by mail, by telephone or over the Internet as
instructed on the proxy card you received. If you attend the Annual Meeting, you may vote in person even if
you have previously returned a proxy card. This year members of our senior management will not be making
a presentation or holding a question and answer session following the completion of the formal business
portion of the Annual Meeting. The Annual Meeting will conclude at the end of the formal business.
Sincerely,
James O. Ellis, Jr.
Chairman of the Board

April 7, 2017
