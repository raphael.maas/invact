Dear Fellow Stockholders:
2013 represented a year of transition for our
company. Our acquisition of The Warnaco Group,
Inc. (Warnaco), gave us direct control over
the two largest Calvin Klein apparel categories,
jeanswear and underwear, and added the Core
Intimates (Warners and Olga) and Speedo
businesses into our Heritage Brands business. 
2013 Year in Review
Organizationally, there were significant
changes within our largest brand, Calvin
Klein, as the business model shifted from
being license-driven to a more directly
operated business, with direct operations
now expanded to Europe, Asia and Latin
America. We believe that these changes
will lead to stronger and more profitable
operations for PVH in both the Calvin Klein
and Tommy Hilfiger businesses.
While the economic environment continued
to be volatile in 2013, with fragile consumer
sentiment in most parts of the world,
we navigated the obstacles well, as our
brand portfolio, led by Calvin Klein and
Tommy Hilfiger, continued to resonate with
consumers worldwide. We exceeded our
earnings per share and revenue guidance
set forth at the start of the fiscal year,
with non-GAAP earnings per share of
$7.03.* Our revenues increased over 35%
to $8.2 billion, as we added the acquired
businesses and saw sustained growth
in our core businesses, with consistent
profitability and cash flows. We reported
earnings before interest and taxes of
$967 million,* an increase of 29%* over
last year, as our strong brand positioning
and execution of our business principles
drove our financial performance, despite
the uncertain macroeconomic environment
and the significant strategic investments
we have needed to make to upgrade and
stabilize the acquired Calvin Klein businesses.
Status of Warnaco Acquisition
During 2013, we made significant progress
integrating the Warnaco businesses into
PVH and investing in the infrastructure
critical for our long-term global success.
Our teams converted systems across
North America and Europe, putting
associates on common platforms across
each business. In 2014, our teams will
focus on systems conversions in Asia
and our sourcing integration is on track
for completion for the Fall 2014 season.
We also invested in people, filling many
of the over 200 open positions across the
acquired businesses, including a President
of Calvin Klein Asia and a President of
Calvin Klein Europe. Jeanswear has been a particular focus
since the acquisition closed. We have
taken strategic actions to reposition Calvin
Klein Jeans beginning with the Fall 2014
lines, with improved product and designs
that are more consistent with our other
Calvin Klein branded product. Our teams
have been elevating our sourcing platform
to coincide with these improvements and
are working to enhance and upgrade our
wholesale account distribution and retail
presentation. These measures are expected
to deliver healthier financial metrics in the
U.S. and Europe. Additionally, we have
taken actions to eliminate excess inventory
levels across the acquired businesses.
Our newly acquired Speedo, Olga and
Warners businesses are also proceeding
well in terms of their integration. Overall,
we are pleased with the status of the
integration and the advancements we
are making, and we remain on track to
achieve synergies identified through the
acquisition.
Setting the Stage for
Continued Long-Term Growth
During 2013, we took significant steps to
strengthen our organization for the longterm and deepen our focus on our global
designer lifestyle brands, Calvin Klein and
Tommy Hilfiger. The Warnaco acquisition
provides a broader global platform for
both of our designer lifestyle brands and
has enabled us to continue to transform
from a primarily North American multibrand business with strong European
Tommy Hilfiger operations, to a more
diversified, global organization with
significant operations in four distinct
regions, and with Asia and Latin America
now accounting for approximately 20%
of our operating income. As a much larger
organization, we continue to operate
under our core business principles, which
value leaders who hold the highest ethical
standards, while also driving results across
the business.
We took key steps to enhance and
further develop the acquired businesses
by making significant investments in their
infrastructure, including filling high-profile
senior management positions, improving
systems and processes and elevating
the design and quality of Calvin Klein
jeanswear and underwear. These changes
will put the brand in a much stronger
position to grow globally, strengthen its
customer base and thrive over the longterm. With the direct global control of the
brand image and commercial operations
for the two largest Calvin Klein apparel
categories, jeanswear and underwear,
our teams took steps to strengthen
management, improve operations, unify
our brand messaging, and coordinate
and improve design, merchandising,
retail distribution and marketing functions
on a regional and global basis. We believe
that these steps, which we will build upon
in 2014, will strengthen Calvin Kleins
image, positioning and execution across
all markets to drive sustainable growth.
While we still have additional areas of
investment and development planned
for 2014, the integration is progressing
on track and we are confident that the
steps we are taking will position PVH for
sustainable long-term financial growth
and drive stockholder value.
Another major development was our sale
of the G.H. Bass & Co. business in the
fourth quarter of 2013, which allows us
to further invest in our more profitable
apparel businesses, while also reducing
sales and earnings volatility.Calvin Klein and Tommy Hilfiger represented
over 75% of our 2013 revenue. The global
appetite for these two designer lifestyle
brands continued to be strong, as Calvin
Klein and Tommy Hilfiger posted notable
sales and market share gains, while also
expanding profitability. In spite of the
weak consumer environment, our powerful
designer lifestyle brands grew comparable
store sales low single-digits on top of
challenging multi-year comparisons, driven
by compelling product, strong brand
awareness, engaging marketing programs
and prudent inventory management. Our
Heritage Brands business also performed
well, despite weakness in Heritage Brands
Retail, posting wholesale sales growth and
market share gains across most of the
categories in which we compete.
We also continued to strengthen our
balance sheet, as solid operating results
allowed us to pay down $500 million of
debt this year, despite the significant
investments we made in integrating
the Warnaco businesses. This amount
was well above our initial projection of
$400 million and left us with a 2.8x* net
leverage ratio at the end of 2013.
Calvin Klein
Calvin Kleins designer lifestyle positioning
remains strong, as evidenced by the brands
global retail sales of $7.8 billion in 2013.
Our Calvin Klein business posted strong
results, even while integrating the Warnaco
businesses, navigating inventory, product
and sales challenges across the Calvin Klein
global jeanswear business and addressing
wholesale account and store performance
issues in Europe. Sales growth was driven
by strong product execution in mens
and womens underwear, sportswear and
accessories, which conveyed Calvin Kleins
provocative, modern, American designs,
new category additions and expansion
within existing and into new markets. The
consumer appeal for the brand remains
high, as demonstrated by Calvin Kleins
#8 ranking on MediaRadars Most Talked
About Fashion Brands of 2013 list.
During 2013, our Calvin Klein revenues
grew over 140% to $2.8 billion, driven by
the acquisition of the Warnaco businesses.
We experienced over 60% growth in North
America and revenues for the rest of the
world grew four-fold. Calvin Klein North
America had strong performance both at
retail and wholesale, with 3% comparable
store sales growth and the pre-acquisition
wholesale business posting 8% sales
gains, while also growing average unit
retail prices. Calvin Klein also continued
to post gains in China and Brazil,
where the brands position is strong, as
consumers appreciate its aspirational
lifestyle. Strength in these regions helped
offset weakness in Europe and the North
American jeanswear business, as we
restructure and rebrand Calvin Klein Jeans
in the marketplace beginning in 2014.
With 2013 as a year of transition and
investment, Fall 2014 will start to highlight
our product innovation  improvements
to quality, fit and fashion, particularly in
jeanswear  along with an enhanced
presentation at retail.
Tommy Hilfiger
The Tommy Hilfiger brand achieved
record global retail sales of $6.4 billion
in 2013, which increased 6% over 2012,
as we continued to deliver strong product
assortments featuring the preppy with a
twist designs that the brand is known for.
Global retail sales were generally strong
across all regions, reinforcing our optimism
in the long-term growth trajectory of the
business. Our North American business
was the highlight, as it posted robust
sales growth while also improving operating margins. The tremendous
growth occurring in North America has
resulted from the investments we have
made since acquiring the business in early
2010, particularly in the areas of design,
supply chain, distribution and marketing.
Complementing the North American
business was the continued strength
experienced by our European business, in
spite of the challenging macro environment.
Revenues for Tommy Hilfiger were $3.4
billion, an increase of 7% against 2012,
with 8% growth in North America and
6% growth for the rest of the world. Our
two largest geographies, Europe and
North America, achieved strong sales
growth, with comparable store sales up
6% in Europe and up 4% in North America,
despite volatile consumer sentiment
and difficult multi-year comparisons in
these regions.
We continue to focus on elevating our
products, touching existing and new
consumers with our eclectic Hilfiger
family marketing campaigns and investing
in the in-store experience to drive the
brand globally while continually improving
our financial returns. We believe that our
largest long-term growth opportunities
lie in the high-growth emerging markets
of Asia and Latin America. Accordingly,
in January 2013, we established a joint
venture in Brazil and further supported our
existing joint ventures in China and India.
While we were pleased with most regions
in which we operate, we continue to take
actions to reposition and elevate the
Tommy Hilfiger brand in Japan, where the
brands underperformance persists.
Heritage Brands
Our Heritage Brands business had a
defining year in 2013, as we continued
to execute on our turnaround plan and
also integrated the Core Intimates and
Speedo businesses. We also took
important steps to align our Izod and Van
Heusen wholesale, retail and licensing
operations by brand, which should result
in more consistent brand messaging,
marketing, product assortments and point
of sale presentations across channels.
Our Heritage Brands wholesale business
strengthened its market position across
key categories including dress shirts,
neckwear, woven and knit sport shirts,
bottoms, underwear, bras and panties.
Notably, the pre-acquisition businesses
generated sales gains, as the turnaround
efforts began to drive improved financial
results. We are also pleased with the
performance of the newly acquired Speedo,
Warners and Olga businesses, which, like
our other Heritage Brands businesses, are
more replenishment-based and generally
provide consistent profitability, healthy
margins and strong cash flows.
The Heritage Brands Retail division,
however, continued to post disappointing
results. This led to our decision to sell
Heritage Brands biggest retail business,
which was also the largest underperformer,
G.H. Bass & Co., a footwear-centric
business that did not match well with
our otherwise apparel-based businesses.
As a result of the sale, wholesale is
expected to represent over 80% of our
Heritage Brands revenue. We continue
to focus on rationalizing our Heritage
Brands retail store base and improving
productivity and profitability at our IZOD
and Van Heusen stores.CSR
At PVH, we are committed to
Corporate Social Responsibility
(CSR). We have been promoting
human rights and worker health and
safety for over 20 years, and we
continue to seek industry change
through various CSR initiatives.
CSR is a key consideration in
making business decisions, as our
company greatly values integrity and
accountability. We strive to make a
positive impact on the environment
and the people in the communities
where we live, work and operate.
As an industry leader, we understand
our obligation to promote and
maintain sustainable business
practices that take into account the
interests of all of our stakeholders.
During 2013, PVH undertook many
CSR initiatives, as the acquisition of
Warnaco increased our size, supplier
base, product offerings and countries
of operation, and created unique
integration challenges. As we grow,
we are taking a broader approach
to CSR, which accounts for social,
environmental and community
impacts across the entire lifecycle
of our products  from source to store.
We took a key role by being one of
the first brand owners to commit
to the Accord on Fire and Building
Safety in Bangladesh, a binding
agreement to establish a broad
factory inspection, safety and training
program in Bangladeshs garment
manufacturing industry. We also
created a Chemicals Management
Commitment and Action Plan and
joined a coalition of leading brands
which are committed to eliminating
hazardous chemicals from the supply
chain by 2020.
Executing our business strategies in
an ethical manner is inherent in our
culture; accordingly, we report publicly
on our CSR accomplishments,
challenges and goals. Transparency
in our CSR reporting practices
promotes awareness, dialogue and
collaborative work toward better
practices.
We believe that our dedication to CSR
across our brands and businesses will
become a key competitive advantage
as we strive to deliver future growth
and stockholder value.
2014 and Beyond
Although 2013 presented us with
several hurdles, we took key steps
to strengthen our Calvin Klein and
Tommy Hilfiger businesses, and
set out the groundwork to enhance
and expand our brands globally.
With control over Calvin Kleins two
largest apparel categories, jeanswear
and underwear, and expanded
operating platforms in Asia and Latin
America, we are optimistic about
the opportunities to enhance our
offerings and improve and expand
the global distribution for our two
designer lifestyle brands. We believe
that the strategic initiatives and plans
that we have implemented for all our
businesses will enable us to achieve
significant increases in revenue and
profitability over the long-term.
While we see several headwinds for
2014, as the global macroeconomic
environment remains uncertain
and our investment spending in the
acquired businesses will weigh on
our 2014 earnings, particularly in the
first half of the year, we are optimistic
about the future. The opportunity
that we identified from the acquisition
remains significant and we intend
to continue to repay debt of at least
$400 million annually, realizing interest
savings, which we intend to reinvest
in our high-growth Calvin Klein and
Tommy Hilfiger businesses.
Finally, but perhaps most importantly,
I would like to recognize the hard
work, dedication and commitment
from our talented associates
globally, who are the lifeblood of our
company. Integrating the acquired
businesses continues to be a major
endeavor, but our associates have
managed the integration seamlessly,
allowing us to meet or exceed our
internal deadlines. Through the
combination of our outstanding
associates, world-class brands and
the execution of our sound business
practices, which focus on passion,
integrity, individuality, partnership
and accountability, we believe that
we can continue to grow our brands
while also delivering improved
long-term financial returns to you,
our stockholders.
Emanuel Chirico
Chairman & Chief Executive Officer