Fellow Shareholders,
Fiscal 2016 marked my fifth year at the helm of this iconic company. During that time,
we have been on a journey of reflection, refinement and change. We have changed our
portfolio, our organization structure and our leadership team. And I am proud to say,
as a result, we have changed our performance.
We have taken unmistakable actions to reposition Campbell to drive long-term
sustainable sales and earnings growth. We declared our purpose, Real food that matters
for lifes moments; acquired four businesses in faster-growing spaces: Bolthouse Farms,
Plum, Kelsen and Garden Fresh Gourmet; entered more appealing markets such as Asia
while exiting underperforming businesses in Europe; and improved our cost structure
and increased supply chain productivity.
More recently, we completely reorganized the company to improve our agility and
responsiveness, creating three divisions with clear portfolio roles and implementing
successful cost savings initiatives expected to generate $300 million in annual savings
by the end of fiscal 2018. These initiatives are delivering results ahead of schedule.
Savings have come from three key components:
 Headcount reductions, which are largely behind us;
 Zero-based budgeting and new policies to curb spending; and
 Integrated Global Services, a shared services group building important
capabilities while lowering costs.
Combined, these efforts are creating an ownership mindset at Campbell where
employees treat every dollar as if it were their own. This is allowing us to invest back
in the business while expanding margins.

A Changing Market and Changing World
The confluence and acceleration of the seismic shifts that are reshaping the established order
of our industry  the rapid and monumental changes in demographics, consumer tastes,
opinions and behaviors; the light-speed pace of technological change and the volatility of new
global economic and political realities  creates the unprecedented context for our efforts.
In essence, we have given Campbell an extreme makeover in a rapidly and fundamentally
changing market. Our progress has been methodical. Undeniably, we have improved our
company and shifted our center of gravity. But we are not satisfied.
We have higher aspirations: for the food we make, the role we play in peoples lives and our
growth trajectory. That is why it is necessary to relentlessly improve our food, our business
and our culture to forge a meaningful and lasting place in the lives of new generations
of consumers.
Fiscal 2016 Results
This year, we delivered double-digit adjusted earnings growth,* including expanded
adjusted gross margin with significantly improved supply chain performance, and achieved
better-than-expected cost savings. Sales decreased 1 percent to $7.961 billion driven by
the adverse impact of currency translation and a decline in organic sales, partly offset by the
benefit from the acquisition of Garden Fresh Gourmet. Organic sales decreased 1 percent*
driven by lower volume, partly offset by higher selling prices. While reported Earnings Per
Share (EPS) decreased, full-year adjusted EPS increased 11 percent to $2.94.* Reflecting
confidence in our long-term growth prospects and our strong profit performance this year,
on Sept. 1, 2016, the Board of Directors approved a 12 percent increase in our
quarterly dividend.
Our Americas Simple Meals and Beverages and Global Biscuits and Snacks divisions
delivered significant margin expansion, driving double-digit profit growth. In the Americas,
our Prego pasta sauces, Plum products and Pace Mexican sauces grew sales, but overall
sales remain inconsistent as portions of the portfolio underperformed their categories,
particularly beverages and ready-to-serve soup. In Global Biscuits and Snacks, Pepperidge
Farm delivered strong performance as we continued to invest in Goldfish crackers, Milano
cookies, Tim Tam biscuits and fresh bakery, and our Asia Pacific team drove solid organic
sales results in a highly competitive trading environment. We continued to focus on our
developing markets presence, including efforts to expand sales and distribution in China
through our Kelsen business, as well as support our brands in Malaysia and Indonesia.
Our Campbell Fresh division faced short-term execution issues, including a protein
drink recall and challenges in our carrot business, that led to organic sales and earnings
declines. We are taking steps to ensure the business performs to its potential, including
enhanced beverage production processes and addressing customer service issues in our
carrot business. We have also made organizational changes, creating a new structure to
foster agility and collaboration across the different brands within the division. We remain
confident in our C-Fresh strategy, its popular on-trend brands and its ability to deliver
long-term growth consistent with its portfolio role.
While we have made progress, we know we have more work ahead. We recognize we need
to deliver sales growth across the company  and it remains a top priority. We have plans
to continue to improve sales performance in every division, as well as make investments to
improve long-term growth. We are pursuing four strategic imperatives, as we continue to
strengthen our core business and expand in faster-growing spaces while building a high
performance organization.

Whats Next?
Our strategy for unleashing the power of our purpose,
performance and potential is centered on four strategic
imperatives that we believe will drive the greatest value
over time:
1. Elevate Trust Through Real Food, Transparency
and Sustainability
Today, food matters. More than ever. People yearn to
feel a stronger connection to their food: where it comes
from, how it is grown or raised and the people behind
the companies that make it.
At Campbell, we recognize our role in helping restore
this intimate connection to food. We also believe people
should have access to delicious, affordable food
prepared with care that they can trust.
Our entire food system is in need of reinvention: global
population growth, water scarcity, climate change and
constraints on agricultural land are placing massive strain
on the food system. There is a growing number of people
who do not have ready access to good, affordable food.
This is our challenge. This is also our opportunity  to
reinvent our company and in the process become the
most trusted food company in the world. We know it is
the right thing to do for consumers, our company, our
shareholders and our planet. Drawing on our strengths in
vegetable nutrition, our history of making affordable food
and our scale, we are uniquely positioned to make more
real food available to more people.
Our purpose has had a profound impact on how we think,
talk and act about our food. We are using it as a filter for
decision making and have made steady progress, but we
realized we needed a more precise definition of real food
to drive faster, better and more consistent decisions and
outcomes.
So we asked a simple question without a simple answer.
What is real food? We drew inspiration from one of
our early leaders, Dr. John Dorrance, who pioneered
good, affordable food in America with the invention of
Campbells condensed soup.

This real food philosophy will help differentiate Campbell
and make our foods and beverages more appealing to
more people. We fully recognize that we will not be able
to achieve our goals immediately, but we will never stop
looking for ways to improve our food.
How are we doing this? First, we are removing certain
ingredients from our food, such as artificial flavors and
colors. More importantly, we are making our products
better and more accessible to people. Ultimately, we
believe this will lead to increased sales over time.
While not listed on any label, trust may be the
most important ingredient in our food. We are
embracing transparency and aim to set the standard
for the food industry.
2. Increase Engagement and Drive Sales
Through Digital and E-Commerce
We continue to meaningfully shift
our marketing efforts to digital
and mobile platforms to
connect with the
next generation
of consumers.
In fiscal 2016, 35 percent of our advertising dollars were
spent on digital compared to 19 percent in fiscal 2015.
We expect digital and mobile to account for between
35 and 45 percent of our total advertising spend
moving forward.
The growth of e-commerce is equally important. U.S.
consumers spent more than $59 billion on groceries
in 2014.1 Only a fraction of that was spent online.
Today, Campbells online sales are less than 1 percent
of total sales. We have plans to make this a more
meaningful percentage of total sales in the future.
3. Continue To Diversify Our Portfolio in Health
and Well-Being
Consumers continue to redefine the meaning of health
and well-being. That phrase means different things to
different people, and food companies like Campbell
must meet a variety of needs.
We start with a unique portfolio to deliver on these
needs, from baby food, carrots and crackers to juice,
fresh bakery, sauces and soup.
Our health and well-being strategy leverages our
existing capabilities and is centered on attractive spaces
where we have the strongest likelihood of success.
We are focused on three growth areas: extending
our packaged fresh portfolio; expanding organic and
clean label offerings in center store; and increasing our
presence in naturally functional foods by leveraging our
vegetable and whole grain capabilities.
4. Expand Our Presence in Developing Markets
Nearly 70 percent of global biscuits and snacks growth
will come from Asia and Latin America over the next five
years.2 Today, only 5 percent of our sales come from
these regions. Our plans to increase our presence within
faster-growing markets is centered on our Global Biscuits
and Snacks division.
We have three iconic brands, each strong in their home
markets: Goldfish in the U.S., Kjeldsens in China and our
Tim Tam brand in Australia. We must increase household
penetration and purchase frequency of these beloved
brands in their home markets while expanding into
new geographies.
We must also continue to build these brands, particularly
in Asia where we have established operations. We plan
to accelerate our growth in the foothold markets of
China, Indonesia, Hong Kong and Malaysia.

New Models of Innovation
We are supporting our strategic imperatives by pursuing
new models of innovation, including smart external
development.
Defining the future of real food requires fresh thinking
and an ecosystem of partners. New companies with
new business models are being fueled by an influx of
venture capital. Since 2010, food startups have received
more than $8 billion in funding.3 We have developed
a framework that incorporates a disciplined M&A
approach, partnerships and venture investing, combined
with internal and external innovation.
We are beginning to participate in venture opportunities
through our planned $125 million investment in Acre
Venture Partners, L.P., an independently managed fund
formed in February 2016 to invest in innovative new food
and food-related companies.
Long-Term Growth Targets
We continue to target organic sales growth of 1 to 3
percent as each division delivers against its portfolio
role. Excluding the impact of currency translation, our
long-term target for adjusted Earnings Before Interest
and Taxes (EBIT) growth is 4 to 6 percent and adjusted
EPS growth is 5 to 7 percent.

A New Campbell Is Emerging
Signs of change are visible everywhere: in our people, our
thinking, our actions, our products and our performance.
We are inspired by our heritage, clear-eyed about our
challenges and focused on defining the future of real food.
The strategic imperatives we are pursuing provide the
most compelling path to increase shareholder value.
We know we have further to go and more to do to
complete our strategic transition. But we remain excited,
optimistic and confident in our direction and our
progress. I have often said, you can either lead change
or be a victim of it. It is much more rewarding to lead it,
and Campbell is doing just that.
In closing, I want to thank our Board of Directors, the
Campbell Leadership Team, our employees and our
shareholders. With your ongoing support, I am confident
that the steps we are taking will help drive long-term
sustainable sales and earnings growth for Campbell.
Best,
Denise M. Morrison
President and Chief Executive Officer