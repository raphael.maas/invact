Dear shareholders

In 2012, the CSX team:
All of these achievements, and more, were made while facing a sharp
drop in coal revenue of more than $500 million, brought about by
a rapid increase in the availability of inexpensive natural gas.

 Set a company record in safety and led the
major U.S. freight railroads in their safest
year ever.
 Drove customer satisfaction levels to all-time
highs with excellent performance in every
service measurement.
 Achieved inflation-plus pricing for our
excellent service, supporting the companys
ability to invest in the future at record levels.
 Delivered productivity savings of nearly
$200 million  far above an already high
bar set at the beginning of the year.
 Grew merchandise and intermodal
volumes combined by four percent in
a still-uncertain economy.
 Increased net earnings for the eighth
time in nine years to $1.9 billion

As soon as we saw this coming, the team began
to make adjustments in the operations and
reminded each other to stay focused on the
things we control  namely safety, service and
productivity. Experience has shown that when
CSX does those things well, we can turn good
conditions into great results, or bad conditions
into better results.
While profitability did not meet our own
expectations due to the coal impacts, it did
improve once again in a tough environment.
Whats most encouraging is that in the process
CSX became an even better, faster and more
capable company  one that continues to skillfully
tackle near-term challenges while looking over
the horizon to big, long-term opportunities.
2013: another transitional
year for CSX markets
In 2013, we expect the coal market weakness to
continue, but moderate as the year progresses.
We also anticipate further gains in the
merchandise and intermodal businesses, which
should outpace the slow, steady growth that is
expected in the economy.
If Congress is able to pass more meaningful
legislation to improve the long-term fiscal
outlook and restore confidence, there is little
reason the economy cannot gain momentum as
the year moves along. Our customers across
many industries believe that America is primed
for real recovery, and so do we.

But whatever degree of uncertainty
characterizes the near-term economy, it
pales in comparison to our confidence in
the long-term outlook for this industry.
If you have read past annual report letters,
you know that I believe it is important to
remind investors and potential investors
of the trends that are driving long-term
opportunities for CSX:
 the inevitable movement of more freight
as the population and its consumption rise;
 the pressing need to deliver freight
efficiently between ports and people as
global trade continues to build;
 the increasing congestion on the nations
highways, driving freight to rails;
 the re-industrialization of America as
the countrys efficient labor force and
relatively inexpensive energy combine to
create cost advantages for local or regional
U.S. producers and demand for exports;
 the challenges associated with labor, fuel
and other costs at trucking companies,
which today are partnering with us for
longer-haul movements; and
 the nations need for more environmentally
friendly transportation solutions.

Even when one considers the evolution
of energy markets and the changes in coal
demand, a baseline level of coal will be
critically needed both domestically and
globally for many years to come. And as the
energy mix changes, more opportunities
for products such as crude oil, hydraulic
fracturing sand, and renewable energy
products and components will abound.
At the same time, CSX will continue to
pursue growth in other merchandise and
intermodal areas, made possible by all of
the trends described previously, along with
great service, forward-looking investments
and a workforce that is relentlessly focused
on success.
Real, compelling service
CSX employees are excited to offer an
essential service to customers and the
nation at large.
Our network connects some of the most
active economic centers of the world with
small farming towns and seaports alike. The
company plans to invest in that network at
a record $2.3 billion level this year, on top
of approximately $8 billion weve invested in
the past four years.
Part of that investment continues to go for
strategic initiatives to support profitable
growth, including CSXs National Gateway

that will provide a double-stack-cleared route
between the Mid-Atlantic ports and the Midwest.
The entire National Gateway is now well over
halfway complete and should be fully up and running
in 2015.
Investment in the future remains the highest
priority for capital, and it is part of a balanced
program of capital deployment overall. The
second priority is dividends to shareholders.
In 2012, CSX paid dividends of 54 cents per
share, up 20 percent from the year before. The
company expects a dividend payout range of 30
to 35 percent of trailing 12-month earnings, to
be reviewed annually.
Also in 2012 CSX fulfilled its commitment to
complete a $2 billion share buyback program,
repurchasing $734 million during the course
of the year. The company has repurchased
$7.9 billion of shares since 2006, representing
approximately one third of total shares
outstanding today. We remain committed to
share buybacks to return cash to investors.
Building capital with our customers
Beyond investing in rails, equipment, facilities
and technology, were making an unprecedented
investment in time and energy to focus
employees on the needs of customers.
In last years annual report I told you about the
5,000 customer site visits conducted in 2011 and
the opportunities the team identified to serve
them better and grow.
Extending that effort further, CSX is employing
game-changing initiatives and training to better
align our network and process capabilities with
the specific needs of customers, and setting an
entirely new and higher standard for how all
CSX employees deliver service excellence to the
companies we serve.
Living out our vision
CSXs long-standing vision is to become the
safest, most progressive North American
railroad, relentless in the pursuit of customer
and employee excellence.
In 2012, we lived up to that aspiration in safety
with the best personal injury rate in our history
and a near-best train accident rate.
We did this while continuing to make a
fundamental transformation in how we serve
customers and respond to their needs.
The response of CSX employees played a
critical role in the restoration efforts following
Hurricane Isaac and Superstorm Sandy. They
quickly restored rail service and reconnected

their neighbors to vital commodities, services
and markets, and provided humanitarian support
from the company and their own resources.
During Sandy, CSX moved American Red Cross
emergency vehicles that had been brought in from
other states and partnered with Dignity U Wear
to donate approximately $1 million worth of new
clothing to citizens of New York and New Jersey.
Beyond those efforts, throughout the year CSX
continued to partner with several national and
regional organizations to make communities
better. Knowing that our futures are inextricably
linked, weve become focused on partnerships
that develop the leaders of tomorrow  especially
City Year and Future Farmers of America. In
addition to drawing us closer to our communities,
these young people are teeming with ideas and
energy, and they reinforce some of the bigger
reasons we do business  including a deep and
demonstrated commitment to the environment.
CSX is committed to reducing greenhouse gas
emissions intensity by six to eight percent by
2020 and were proud to have again been named
to the Dow Jones Sustainability Index for North
America in 2012. One CSX train can carry the
load of 280 trucks, which means fewer emissions
and relief from congestion on the highways.
Those environmental benefits are one of the many
reasons CSX communicates its brand message
under the tagline, How Tomorrow Moves.
Telling this story has helped us realize even more
that tomorrow has as much to do with the
actions we take today as it does the benefits
that await our customers, communities and
shareholders over the next several years.
We hope that our quick response to the tough
conditions in 2012, and the responsible actions
of our 32,000 employees, gave you even more
confidence in this team and the investment you
have made in the company.
Thank you very much for your partnership and
belief in CSX, both today and tomorrow.
Michael J. Ward