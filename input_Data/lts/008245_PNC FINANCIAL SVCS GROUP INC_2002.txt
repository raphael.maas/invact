DEAR FELLOW SHAREHOLDERS:
In my 30 years with PNC, I 
cant remember a year as unique
and challenging as 2002.
Yet, despite this difficult
environment, I am tremendously
proud of what our team of
24,000 employees accomplished.
 We reported earnings of $1.2
billion and generated a 19%
return on equity in 2002.
 Our employees continued 
to win the confidence of our 
customers, as customer satisfaction neared all-time highs in 
several areas, and more employees referred new business than
ever before.
 We also continued to strengthen our balance sheet, build a
more valuable mix of businesses, and make strides in other
areas critical to enhancing our
competitive position. 
In short, what we accomplished in 2002 reflects our
teams commitment to achieving
our goal of growing PNC and
delivering premium returns for
the risk were willing to take. 
Clearly, though, the 
economic pressures facing our
industry had an impact.
In a year marked by declining equity markets, record 
bankruptcies, lower capital
investment, and weak credit 
quality, revenue softened in a
number of our businesses. 
In addition, we also had 
to work to regain shareholder
confidence after restating our
2001 earnings.
As youll read, weve taken a
number of steps to address these
issues, and I believe we enter
2003 better positioned to create
value for all the constituencies
we serve.
For a variety of reasons, issues
related to risk management 
and corporate governance drew
additional national attention 
last year. These issues can 
dramatically affect a companys
integrity and performance  
and we take them very seriously.
Our efforts to enhance our
risk management and corporate
governance practices intensified
after restating our 2001 earnings.
In order to settle a related 
inquiry by the Securities and
Exchange Commission, we 
consented to an SEC cease and
desist order. We also entered into
written agreements that focused
on risk, management and financial controls with the Federal
Reserve Bank of Cleveland and
the Office of the Comptroller of
Currency following examinations
by those regulatory agencies. Im
pleased to say that we have worked
closely with our regulators to
aggressively address these issues. 
We also engaged highly
regarded consultants McKinsey &
Company, Promontory Financial
Group, and Wachtell, Lipton,
Rosen & Katz to assist us in
developing a broad risk management and corporate governance
plan. Throughout the year, we
made tremendous progress.
 We named a senior executive,
Tom Whitford, to the newlycreated position of Chief Risk
Officer to help implement an
enterprise-wide approach to 
risk management. 
 We created management committees to address risks associated with
significant transactions, products,
services, and capital commitments.
 We worked to dramatically
improve regulatory relations,
and recruited Jack Wixted 
from the Federal Reserve Bank
of Chicago to serve as Chief
Regulatory Officer.
We added Vice Chairman 
and CFO Bill Demchak and
Vice Chairmen Bill Mutterperl
and Joe Whiteside, who 
bring additional talent and
experience to our senior 
leadership team.
 We also added new board 
members  J. Gary Cooper,
Richard Kelson, Anthony
Massaro, and Stephen Thieke
 who provide significant
financial and industry expertise.
In addition, to further
enhance corporate governance,
our employees adopted a
Statement of Principles and we
strengthened our Code of Ethics. 
I believe the extensive
amount of time and energy we
spent on risk management and
corporate governance in 2002 will
serve to reinforce our companys
strong foundation.
In this economy, we also continued to strengthen critical elements of our balance sheet, 
such as our liquidity and capital
positions. Our loan-to-deposit
ratio is 79%, down from 106%
at the end of 2000, and our 
Tier I capital ratio is 8.8%. Each
is the strongest in our companys
recent history and among the
best in our peer group.
We expect to proactively
manage our capital in 2003. Our
top priorities include investing 
in our businesses and resuming
our share repurchase program. 
We also continued an extensive effort to reduce credit risk in
2002, removing roughly $2.8
billion in higher-risk, low-return
assets from our balance sheet.
In addition, our business
mix continues to fuel a diverse
revenue stream. In 2002,
deposits, asset management, processing, and lending each
contributed roughly equal portions of total business revenue.
I believe weve been succesful in building a strong balance
sheet and enhancing risk 
management and corporate 
governance. Moving forward,
were working to harness the
energy weve focused on these
issues to help improve performance business by business. 
This happens to be a particular strength of Joe Guyaux, the
30-year PNC veteran we named
president of PNC in August
2002. In his new role, Joe heads
all of our banking businesses,
including Wholesale Banking
and PNC Advisors. He is 
working to build on the 
remarkable success Regional
Community Banking achieved
under his leadership. 
Some of our businesses have 
performed well through this
challenging economy. Our
deposit-driven Regional
Community Banking (RCB)
franchise contributed 55% of
total business earnings in 2002.
Through developing a culture
that fosters sales and customer
satisfaction, the RCB grew its
checking account base by 7%. 
We believe our premier 
technology platform and highquality customer service should
enable us to make these relationships more beneficial to both 
the customer and PNC.
Another of our businesses,
BlackRock, once again delivered
strong results. It grew earnings
24% in 2002 to $133 million by
leveraging its expertise and solid
track record in fixed income and
liquidity performance. 
Since 1996, BlackRock has
fueled extraordinary growth in
earnings and assets under management. I believe its investment
management expertise, combined
with strengths in risk management,
should enable BlackRock to continue delivering premium growth.
In a number of our other
businesses, the environment 
has been more challenging.
In Wholesale Banking,
which includes Corporate
Banking, PNC Real Estate
Finance, and PNC Business
Credit, weve worked to reduce
risk and improve asset quality.
Because we made a strategic
decision to exit many lowerreturn credit relationships, revenue has been pressured. Were
largely done with that work now. 
Were looking to drive future
growth in this business by focusing on middle market clients in
the regions we serve. We have a significant opportunity to deepen
relationships by selling fee-based
treasury management and capital
markets products. In addition, 
eliminating redundancies 
should help Wholesale Banking
reduce expenses.
At PNC Advisors and PFPC,
our wealth management and global
fund servicing businesses, the declining equity markets and client attrition have adversely affected revenue. 
We had built up these businesses in past years to compete 
in what many anticipated would 
be a robust and rapidly growing
equity market. As that is no longer
the case, were implementing strategies to improve operating efficiency
while also emphasizing client 
retention and growth. 
The team at PNC Advisors is
focused on its investment performance and high-touch service as
keys to attract and sustain a strong
customer base. PFPCs sales team
has been restructured to better 
leverage the companys leading
technology-based solutions. 
Along with our balance sheet 
and diverse mix of businesses, 
we believe two other factors differentiate PNC  our technology 
capabilities and our people.
Driven by Chief Information
Officer Tim Shack, who is also now
leveraging this expertise as Chief
Executive Officer of PFPC, were
using technology to make business
easier for our customers, improve
processes for our employees, and
create value for our shareholders.
This success has been widely 
recognized by industry experts.
Information Week ranked PNC
35th overall and the second highest
bank in its annual list of the nations
top 500 technology innovators. 
In addition, an initiative that helped us increase retention in Regional
Community Banking was 
recognized by technology 
consultant Peppers and Rogers 
as one of the ten best customer
relationship management programs in the country.
Technology alone does not 
get the job done. 
Ive always believed that our
team of 24,000 employees is our
best advantage. Theyve demonstrated an unwavering commitment to this company, and each
day they live the values that
grace the cover of this report.
Our employees dont simply differentiate PNC  they define it. 
Even with the challenging
environment, their sense of
teamwork and community spirit
stood out this year. Through a
program called the Chairmans
Challenge, more than 80% of
non-sales employees in the
regions we serve referred new
business to our sales staff, which
closed on 57% of these leads.
Our employees also reached
their goal of contributing 1 million volunteer hours to local 
community groups. I cant thank
them enough for their contributions. In addition, communityrelated funding from The 
PNC Foundation totaled more
than $12 million last year.
Our entire management
team recognizes the importance
of attracting and retaining 
talented people and strengthening the fabric of our community.
Thats why Im proud of the
diverse array of programs weve
implemented to accomplish
those goals. 
In fact, in 2002 we were recognized by Money
magazine for
having one of the nations top 25
benefits plans, and we earned a place in Business Ethics magazines
list of top 100 corporate citizens. 
Im also tremendously
indebted to our senior leadership
team and our board of directors.
They have put in countless hours
as weve worked to implement
PNCs strategic vision. Their support, commitment, and contributions have been extraordinary. 
I believe the company Ive
described for you has the ability 
to perform well for all of our constituencies in 2003, although we
realize there are challenges ahead.
Many of these challenges are
shared by others across our industry. Prospects for economic growth
remain uncertain, and were one 
of a large number of companies 
that will be adversely impacted by
additional costs to fund pensions
and expense stock options. 
With that said, our strong
balance sheet, capital management, and efficiency initiatives
should help our returns even if
the environment does not
improve. If the economy does
recover, our diverse business 
mix and targeted marketing 
campaigns should enable us to
post even better results.
Finally, Id like to thank our
shareholders and customers for
your confidence in PNC. I
couldnt be more excited about
our company. We have developed an unwavering commitment to creating value for you,
and we look forward to earning
your continued faith.
Sincerely,
James E. Rohr
Chairman and 
Chief Executive Officer