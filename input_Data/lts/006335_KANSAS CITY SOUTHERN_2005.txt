LETTER TO OUR SHAREHOLDERS
In 1887, Arthur Stilwell founded the railroad company
which was to become Kansas City Southern (KCS).
Unlike the 19th century business moguls who built
predominantly east-west oriented railroads, Stilwell
envisioned a railroad operating on a north-south axis
from the U.S. heartland to the Gulf of Mexico. He was
successful in achieving this dream by building a railroad
from Kansas City to Port Arthur, Texas, on the Gulf.
Later, he envisioned a railroad from
Kansas City all the way to a Pacifi c
Ocean port on the coast of Mexico.
That part of Stilwells dream remained dormant until 1995 when
Mexico announced its intention to privatize its primary rail lines.
In anticipation of this, KCS and Grupo TMM, S.A. de C.V. (Grupo
TMM) formed a joint venture for the express purpose of pursuing
ownership of Mexicos premier rail property, the Northeast Line.
In 1995, KCS also became a 49% owner of the Texas Mexican
Railway (Tex Mex), the connection between The Kansas City
Southern Railway Company (KCSR) and the Northeast Mexican
railroad at Laredo, Texas. After conducting an exhaustive study
of the Mexican freight transportation market, the partners
submitted an aggressive bid, and in late 1996 were notifi ed by
the Mexican government that they had won the rights to the
Northeast concession. In June 1997, Transportacion Ferrovaria
Mexicana S.A. de C.V. (TFM) began commercial operation.
In large part due to a pent-up demand for reliable rail service
in Mexico, TFM was successful right out of the gate, with
revenues growing in the double digits during the fi rst few years.
Because of years of unreliable performance prior to privatization,
the percentage of total inter-city freight tonnage handled by Mexican railroads was a paltry 12%, compared to U.S. railroads,
which at that time handled approximately 40% of the U.S.
freight tonnage. By providing an improved service product,
TFM immediately began to increase its market share.
Nevertheless, even in the early days of TFMs operations, KCS
management recognized the full potential of the investment
could not be achieved unless the U.S. and Mexican railroads
functioned as a single entity under common control. To
maximize the potential of the joint enterprise, it was imperative
to seek out cross-border business opportunities. Separately,
KCSR and TFM were essentially mid-haul railroads with lengths
of hauls in the 500 mile range. Only by working together could
hauls be extended to 1,000 to 2,000 miles or more. Unfortunately,
a joint venture arrangement was not conducive to promoting
these opportunities. As a result, the growth of traffi c over
the separate railroads lagged far behind the market potential.
Concluding that the enormous potential of the U.S.-Mexican
rail line could be unlocked only through a single ownership,
Kansas City Southerns management essentially returned to
Stilwells century-old dream of creating a railroad stretching
from Kansas City through Mexico to a Pacifi c Coast port.
The process of putting the three railroads together under a
single owner and operator has been extensively chronicled.
It was at times diffi cult and frustrating, but throughout the
multi-year process KCS management remained steadfast in
its determination to create a seamless railroad network.
Negotiations between the two owners to combine the railroads
began in 2002, which resulted in the fi rst TFM acquisition
agreement announced in April 2003. Differences soon arose
between the partners, which led to delays, further negotiations,
legal proceedings, and arbitration. Finally, in December 2004,
an amended TFM acquisition agreement was signed, setting
the stage for KCS to become the owner and operator of
the combined rail system. This, in turn, led directly to three
milestone events in 2005. First, on January 1, Mexrail, Inc., the holding company for the Tex Mex, was consolidated into KCS. Second, on April 1, KCS acquired Grupo TMMs shares of Grupo
Transportacion Ferroviaria Mexicana, S.A. de C.V. (Grupo TFM), making KCS the majority
owner of TFM. And, third, on September 12, KCS and the Mexican Government
resolved a number of longstanding disputes, which resulted in a settlement that gave
KCS full control of TFM. In December 2005, KCS changed the name of its Mexican
subsidiary from TFM to Kansas City Southern de Mxico (KCSM) to better position it
as an integral component of the total KCS international rail network.
Fulfi llment of this long-sought-after vision validates the prodigious efforts of
generations of KCS people who tirelessly fought to ensure the survival of KCS as an
independent company. Where many looked at KCS as an anachronism destined to be
swallowed up by the seemingly more viable giants surrounding it, the KCS people saw
the potential of their company and dedicated themselves to its survival and expansion.
Without the dedication and unswerving commitment of these people, the milestone
events of 2005 would not have taken place.
Achieving sole ownership of the combined network also validates the resolve of KCS
management and its Board of Directors. While the Company always enjoyed the support of a loyal group of shareholders who understood its potential, many within the
fi nancial community and transportation industry questioned whether KCS would, or
even should, survive. The questioning became even more intense as investors watched
the railroad industry undergo widespread consolidations in the 1990s. With full awareness of these concerns, management and the Board carefully studied alternatives
before deciding in 1995 to pursue a course of continued independence and expansion.
Once the decision was made, there was no wavering. Through periods of success and
frustration, management never strayed from its strategic goals and constantly strove to
deliver on its promises. Finally, in 2005, KCS was able to pull off an accomplishment with
historic implications within the transportation industry.
The Stilwell dream, the KCS dream, of a railroad from the middle of the United States
to the heart of Mexico and beyond had been realized. THE ROAD AHEAD  A NEW VISION IS BORN
But the story does not end there  not by a long shot. The creation of its expanded rail
system was never really the end game for KCS, but rather the vehicle by which it could
accomplish its ultimate business goals. These goals were set in the mid-1990s as the
Company committed to staying independent and using its unique geographical position
to grow its franchise. The goals were based on the belief that there was enormous,
largely untapped, economic growth potential along a north-south axis in North America,
and the route structure of KCS made it uniquely positioned to exploit that potential.
While often cited as a catalyst in the shift to greater north-south
economic activity, the 1994 signing of the North American Free Trade
Agreement (NAFTA) was just one milestone  although certainly a
major one  in a movement that had been developing over the last
quarter of the 20th century. Even before NAFTA, Mexico was evolving
into the manufacturing hub of North America, as not only U.S. and
Canadian companies but also Asian and European companies were
building Mexican manufacturing facilities. At the same time, economic
growth patterns were shifting in the U.S. Having been, for more than a
century, afterthoughts in terms of the nations economy, the southeast,
south-central and southwest regions began to experience what was to
become explosive economic and population growth.
In essence, the economic growth axis in the U.S. was changing at the same time Mexico
was developing its economic and manufacturing muscle. While the major established
population centers on both the east and west coasts of the United States guaranteed the
continued relevance of those areas, the regions of fastest economic growth were along a
north-south axis basically paralleling KCSRs route structure.
In addition, almost simultaneous with the development of Mexico, the North American
economy began to feel the infl uence of Asian imports. Consumer goods from China
and other Asian countries began to move in ever-greater volumes into the U.S. Signifi -
cantly for KCS, as Asian exports grew, more pressure was being placed on the capacitystrained U.S. western ports. Increasingly frustrated by delays and costly ineffi ciencies,
shippers have been forced to seek alternatives, including moving products through the
Panama Canal and then up to eastern U.S. ports. Asian import growth is not going to
slow down anytime soon. It is projected that in ten years approximately one-third of the
U.S. gross national product will be directly impacted by Asian trade.Prior to the acquisition of KCSM, the problem for KCS was
that its rail network did not extend far enough to really take
advantage of either the shift in North American economic
patterns or the expansive Asian trade growth. In most respects,
KCSM (formerly TFM) had the same problem. Both systems
stopped at the U.S.-Mexican border. Shippers faced both real
and psychological barriers at the border. Changing from one
railroad company to another, or from railroad to truck, was
just too cumbersome to be practical for shippers needing
effi cient, reliable and time-sensitive transportation services.
This obstacle proved to be not only a limiting factor on trade
growth between the U.S. and Mexico, it also acted as a
deterrent to the growth of Asian freight that used Mexico
as a gateway into the vast North American market.
The consolidation of KCSM under KCS has eliminated the barriers to long-haul, cross-border freight movements. Not only
has it created a highly effi cient conduit for products to move
between the North American countries, it provides a desirable
land bridge option for bringing Asian goods into Mexico and
moving them to major population centers in Mexico and the
southwest, southeast and lower midwestern United States.
The traffi c potential is enormous. Citing pent-up demand, shippers are already working with KCS to begin daily intermodal
service between the southeast U.S. and central Mexico. That
service, Atlanta to the Mexico City area, will begin in mid-2006.
Moreover, the natural deep-water Pacifi c Ocean port of Lazaro
Cardenas has been identifi ed by several major ocean-shipping
companies as the most viable underdeveloped port in North
America. Hutchison Terminals, the worlds largest port operator,
has the concession rights to operate one port operation at the
Port of Lazaro Cardenas and has begun a massive three-phase
construction project that will result in expanding capacity at the
port to two million twenty-foot-equivalent containers (TEU's)
annually. Further, there is ample room for even more expansion
and more port operations at Lazaro Cardenas in the coming years.
KCSM, which will build a state-of-the-art terminal facility at
Lazaro Cardenas, is the sole railroad serving the port. Simultaneous to the start-up of the Atlanta-to-Mexico City daily
intermodal service, KCS will also begin daily intermodal service
between Lazaro Cardenas and southeastern U.S. destinations
in mid 2006. By 2007, the number of dedicated U.S.-Mexico
trains along this corridor is projected to more than double and
will continue to grow at a brisk pace well into the future.
The new vision for KCS is to capture the full economic value
that its expanded franchise holds. No transportation company
stands to benefi t more than KCS from the changed North American economic growth pattern, changes in population densities, and expanded
Asian trade. At the same time, management fully understands that turning a vision into
a reality requires strong strategic planning, hard work, good execution and persistence.
These are the qualities that led KCS to create its 6,000-mile rail network, and they are
the qualities that will allow it to realize its full value.
ENHANCING INFRASTRUCTURE
Infrastructure improvements in the United States and Mexico have been ongoing for
years on the KCS rail network and were stepped up in 2005 to ensure that KCS will have
a well-maintained and well-designed track structure, intelligently designed state-of-the-art
terminals, and suffi cient locomotive power to effi ciently handle expansive business
growth. KCS actually began its intensive capital program in 2004 in the U.S. when its
capital investment totaled 18.3% of total revenues compared to the Class I average without KCS of 14.5%. In 2005, the Companys capital investment in the U.S. grew to more
than 20% of total revenues. Over $1 billion has been invested in capital improvements
in Mexico since the start-up of TFM in 1997.
While system improvements were made across the board in 2005, the following four
areas received particular attention, as they will have the greatest return on capital over
the next few years.
Shreveport
The Deramus Yard in Shreveport, Louisiana, is the hub of KCSR operations and will become even more important with the growth of traffi c between Mexico and the southeast.
In 2005, KCSR began a multi-year project to increase capacity at Deramus Yard by 50
percent. The north classifi cation lead will be modifi ed, additional
receiving/departure tracks will be constructed, existing receiving/departure tracks will be extended, and Centralized Traffi c
Control (CTC) will be extended through Shreveport. The bottomline goal of the extensive Deramus Yard project is to be able to
process more cars faster through this central rail hub.
Meridian Speedway
For nearly a decade, KCS management has viewed its 320-mile
rail corridor between Shreveport, Louisiana, and Meridian, Mississippi, as destined to become one of the most valuable
rail corridors in the nation. As traditional gateways at Chicago,
St. Louis, Memphis and New Orleans have become congested,
capacity-constrained railroads have been forced to seek out
new interchange points. Meridian is a natural solution as it is
uncongested and is at the east end of a straight rail corridor
that parallels Interstate 20 between the southeast and southwest. Over the past 10 years, traffi c on the Meridian Speedway,
as the corridor has been labeled, has grown from approximately six to eight trains per day to 22-24 per day.
In order to keep up with the growth of traffi c over the Meridian
Speedway, KCS has invested nearly $300 million on the cor- Speedway, KCS has invested nearly $300 million on the corridor since it purchased the property in 1993. In 2005, KCSR
re-laid all its rail in the Vicksburg, Mississippi area. In addition,
the track was undercut and a new drainage system was added
along the main track in the 11 miles between the east end along the main track in the 11 miles between the east end
of Vicksburg and the Mississippi River Bridge. As a result of
this upgrade and a line change at Vicksburg, each train can
now handle one-third more tonnage up the ruling grade at now handle one-third more tonnage up the ruling grade at
Vicksburg. As part of the project a 9,000-foot siding at Vicksburg was completed in early 2006. CTC has been added to an
important 50-mile stretch between Jackson and Vicksburg, important 50-mile stretch between Jackson and Vicksburg,
Mississippi. Expansion was begun on the important classifi cation and interchange yard at Jackson, Mississippi. Also, main
track expansion capacity in Jackson began with completion track expansion capacity in Jackson began with completion
projected in 2006. In 2005, KCSR completed the second year
of a fi ve-year project with the Mississippi Department of of a fi ve-year project with the Mississippi Department of
Transportation to improve safety at 88 public grade crossings by installing 18 sets of crossing signals and gates, and
upgrading 22 grade crossing surfaces.
In 2005, three events thrust the Meridian Speedway to the In 2005, three events thrust the Meridian Speedway to the
forefront of North American transportation logistics planning. forefront of North American transportation logistics planning.
First was the completion of the KCS acquisition of KCSM. With First was the completion of the KCS acquisition of KCSM. With
its fully integrated rail system fi nally a reality, both railroad and its fully integrated rail system fi nally a reality, both railroad and
trucking companies have begun earnest discussions with KCS trucking companies have begun earnest discussions with KCS
to exploit service opportunities from the southeast to Mexico. As noted earlier, dedicated U.S.-Mexican rail service, incorporating
the Meridian Speedway, will begin in the fi rst half of 2006.
Second, Katrina, the devastating hurricane that hit the Gulf
States in late August, moved up the timetable for shifting traffi c
to the Meridian Speedway that had traditionally moved through New Orleans.
Norfolk Southern (NS) and CSX properties were seriously damaged by Katrina,
as was the Port of New Orleans. With
New Orleans temporarily inoperable,
signifi cant traffi c was rerouted over the
Speedway. Recognizing the effi ciency
of the Meridian Speedway, CSX opted
to enter into an agreement with KCS
to reroute interchange traffi c with KCS
that previously moved through New
Orleans onto the Meridian Speedway
on a permanent basis.
The fi nal and most telling indication of
the importance of the Meridian to the Shreveport corridor was
the joint venture agreement signed by KCS and NS in December
2005. Terms of the agreement include NS investing $300 million
in cash, with $260 million to be used specifi cally for capital
improvements and capacity expansion on the Meridian Speedway.
When completed in four years, the track and signaling enhancements will result in enough capacity to handle more than twice
the current traffi c, or between 40 and 50 trains, with seven-hour
faster transit times between Shreveport and Meridian. Implementation of the joint venture agreement is subject to Surface Transportation Board (STB) approval, which is anticipated in 2006.
Tex Mex Track Rehabilitation
The Tex Mex is the vital connection between KCSR and KCSM. In
January 2005, KCS assumed full ownership of the Tex Mex and
immediately began the design and implementation of system
improvements along the Tex Mex to expedite transit times and
improve service effi ciency and reliability.
Much of the investment in line improvements is being funded
through a $50 million low-interest loan to the Tex Mex from
the Federal Railroad Administration (FRA) under its Railroad
Rehabilitation and Improvement Financing
(RRIF) program. KCS is using the loan to
upgrade 146 miles of track, rehabilitate 26
bridges, construct two new sidings and
lengthen another one, and replace 75,000
crossties. The rail yard at Laredo has been
upgraded and expanded. When the infrastructure improvements are completed in
2006, transit speeds on the Tex Mex will
increase from 25 to 50 mph. The improvements will also increase system capacity.
The Tex Mex connects with KCSR via 378
miles of trackage rights over the Union
Pacifi c (UP). KCS anticipates beginning
in 2006 the rebuilding of an abandoned
line segment between Rosenberg and Victoria, Texas, which will
reduce by approximately 157 miles the distance KCS trains will
have to operate over UP trackage rights and shorten the route
by 70 miles. When completed, this line segment will signifi cantly
reduce transit times, lower operating costs and provide another
boost to expedited train service between the U.S. and Mexico.
Locomotives
In 2005, KCS undertook an aggressive effort to reduce the
average age of its locomotive fl eet, increase the number of its
higher horsepower locomotives, and enhance its locomotive
fl eet to effectively handle growing long-haul traffi c and more
coal and grain trains.
The Company purchased 33 new (SD70ACe) locomotives with
delivery of the fi nal shipment coming in the fi rst quarter of
2006. The shift of ownership of 73 (SD70ACe) locomotives from Mexico to the U.S. also allowed the
Company to enter into a more economically
attractive lease arrangement and gain relief
from an expensive maintenance contract while also allowing the locomotives to fl ow freely
between Mexico and the U.S. The Company also entered a lease agreement on 41 (SD90MAC)
locomotives, and in 2006, KCS plans to add Distributed Power to 117 locomotives presently
in service, which will provide greater power to better enable movements over steep grades in
both Mexico and the U.S.
In 2006, KCS will be looking to retire and dispose of approximately 100 older locomotives,
which will further reduce the average age of its fl eet, result in fewer service breakdowns, and
reduce maintenance costs. All in all, the upgrading of its locomotive fl eet is another important
element in the Companys preparation for long-haul cross-border business growth.
KCSM Infrastructure Plans
Immediately after assuming majority ownership of KCSM on April 1, 2005, KCS began investing in track and signaling improvements at key KCSM locations. In 2005, key infrastructure
improvements were completed around Monterrey, which is the hub of KCSM operations. New
siding and main tracks were installed to relieve congestion between Monterrey and Saltillo,
and improve transit times for through trains traveling between the U.S. and the primary
Mexican industrial region where large automotive plants, appliance manufacturing facilities
and many other important rail customers are located.
Starting in 2006, major KCSM capital projects will include beginning construction of a stateof-the-art rail terminal at the Port of Lazaro Cardenas to facilitate the transfer of containers
from ships onto trains for destinations in central Mexico and the U.S. In addition, a large
intermodal facility will be built to serve traffi c coming
into and out of the San Luis Potosi/Queretaro, Guadalajara, Mexico City triangle  an area with a population
of approximately 55 million people. Mega-Mex, which
is the name that will be given to the intermodal facility,
along with Lazaro Cardenas, will be the primary origination and termination points for trains in Mexico.
The quality of KCSMs track is comparable to that of any
of the U.S. Class I railroad. Most improvements to the
track will encompass the extension of sidings in order to
handle longer trains, and in selected high-density areas, the addition of secondary tracks. For the most part, the KCSM track system has an adequate number of sidings to handle higher
volumes; however, at 6,000 to 6,500 feet, most are not long enough to handle the planned longer trains. Therefore, in order to
increase capacity, work will continue throughout 2006 to extend sidings to 9,000 to 10,000 feet along the main route from Lazaro
Cardenas through Mega-Mex to Nuevo Laredo.
KCS and KCSM management have also had considerable success working with Mexican customs offi cials to reduce the time trains are
held before being allowed to cross the border. By coordinating the efforts of KCSM operators and Mexican border offi cials, most trains
are now able to cross the border within 1-3 hours. This contrasts with trucks, which can experience delays of 1 to 2 days.
U.S.-Mexico Systems Integration
The effort to integrate the operations of the three railroads began in 2005 and
will remain a primary area of emphasis throughout 2006. By mid-2006, the KCS
operating platform, Management Control System (MCS), will be installed on KCSM.
Once installed, MCS will have a dramatic impact on operating effi ciencies and will
greatly enhance train operations, crew planning and dispatching. Central to KCS
integration process is the implementation of a shared services approach through
the organizations. Wherever possible, KCS is merging KCSM and KCSR functions.
Shared services will improve productivity and profi tability as well as encourage employees of both companies to explore cross border approaches to both business
opportunities and problem solving. Throughout 2006, emphasis will be focused on combining fi nance
and accounting, legal, information technology, human resources, and purchasing functions.
In 2005, considerable progress was made in organizing an international purchasing and materials group.
This team, composed of employees from KCSM, KCSR and Tex Mex, is responsible for all purchasing and
materials logistics for the entire KCS system. Centralizing these responsibility areas enables the Company
to use its expanded size to negotiate more favorable per-unit costs for materials used system-wide. Also,
in 2005, the Company created an international intermodal and automotive sales and marketing team. Putting
these units together sharpens the focus on longer-haul, cross-border traffi c, the ultimate key to KCS attaining its full potential value.
SAFETY
With all the monumental accomplishments of 2005 it would be easy to overlook the Companys achievements in the area of safety.
That would be a serious oversight, because nothing can derail a railroads progress faster than unsafe operations. While less
publicized than new business opportunities, safety is a vital element in profi tability. This has become even more true as railroads
are forced to operate in extremely litigious environments. With a very aggressive plaintiffs bar, the only way railroads can control
casualty and insurance costs is to eliminate accidents to the greatest extent possible. For this reason, KCSRs 2005 safety performance is particularly gratifying.Year-over-year, KCSRs FRA reportable personal injuries were down 21%, lost work days due to injury
fell 58%, grade crossing collisions were reduced 14%, and train accidents dropped by 11%.
In 2006 and beyond, the goal is to bring the same intense focus on safety to the entire system. In 2005,
KCSR became one of the safest Class I railroads in the U.S. In 2006, it is planned that the entire KCS rail
system will rank among the elite.
A FUTURE OF VAST POTENTIAL
Considerable work remains to be done before KCS achieves the next level of its corporate vision  the
realization of the full value of its franchise. However, there are many reasons why KCS shareholders
should be confi dent that the process to realize the Companys full value will be steady. An enormous
U.S.-Mexican market already exists for the rail service that only KCS is in position to provide. Additionally, the vast potential for Asian import traffi c moving to North American destinations through the Port
of Lazaro Cardenas, with KCS being the sole railroad provider, has not
yet begun to play out.
In addition, KCS has extensive experience working in Mexico, the
importance of which can hardly be overstated. Over the past 10 years
the Company has forged important working relationships with Mexican
government offi cials, union leaders, customers and employees. Since
putting the systems together, KCS has put great emphasis on fostering
an atmosphere of mutual respect and familiarity throughout all levels of workers and all disciplines. KCS
is committed to creating working environments in which employees have great pride in the companies
for which they work. At the same time, employees will be incentivized to look beyond the border separating the companies when developing their long-term goals. The ultimate value of KCS has less to do with
the separate success of its U.S. operations and Mexican operations than it does with the combined
success of the companies.
Finally, shareholders should be confi dent that the same resolute, determined qualities that have characterized
KCS management and led to the successful consolidation of the three companies in 2005 will be the driving
force behind the Company ultimately realizing its full potential value. KCS management knows what it must
do to achieve its goals and will steadfastly adhere to its plans for achieving them. There will be the inevitable
bumps in the road, just as there will be additional growth opportunities not even perceived today. The constant
will be that KCS will continue to place the value of its shareholders investments before anything else and
will remain committed to providing long-term value to those who continue to support the Companys vision. 