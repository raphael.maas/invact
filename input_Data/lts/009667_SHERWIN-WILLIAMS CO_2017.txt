The Sherwin-Williams Company described in the pages of this
annual report differs in many meaningful ways from the company
we reported on just one year ago.
With the completion of the Valspar acquisition on June 1, 2017, we are a larger, more
diversified and more global enterprise. We are also a more complex and disparate company
with higher balance sheet leverage. I have heard from many institutional investors over the past
year that large-scale acquisitions often fail to create significant shareholder value. Based on our
experience thus far, I believe our acquisition of Valspar will prove to be a convincing exception to
this rule.
The successful integration of Valspar will create a f
transaction and integration costs. Our reported results also include
a one-time benefit from deferred income tax liabilities related to
the Tax Cuts and Jobs Act enacted in December 2017 and Valspar
subsidiary mergers (Deferred income tax reductions).
Strength in the core Sherwin-Williams business underpins
these consolidated results. If you exclude the impacts from Valspar
and the Deferred income tax reductions:
• Consolidated sales for the year increased 5.6 percent
to $12.5 billion,
• Operating Profit improved 6 percent to $1.92 billion,
• Profit Before Tax grew 6.5 percent to $1.84 billion,
• EBITDA increased 5.8 percent to $2.12 billion,
• Comparable diluted Earnings Per Share increased
11.1 percent to $14.27 per share.
Record cash generation during the year enabled us to invest
in growth, increase our annual dividend for the 39th consecutive
year, and pay down debt at a rapid pace. The Americas Group
opened 101 net new stores, finishing the year with 4,620 stores
in operation. We also paid $319.0 million in cash dividends and
retired over $1 billion in debt. Given these priorities, we made no
open market purchases of our common stock for treasury in 2017.
With the distractions inherent in integrating two large
organizations into one, it was vital to keep our teams focused on
delivering the value to our customers that will drive sustained
growth and profitability over the long term. Each of our three
reportable segments contributed to our success in 2017.
THE AMERICAS GROUP
The Americas Group segment teams in the United States,
Canada, Caribbean and all of Latin America have been focused on
collaboration and sharing resources and expertise. We strongly
believe these efforts will result in accelerated growth across
the region.
In the U.S., Canada and the Caribbean, we remain the
largest operator of specialty paint stores, servicing the needs
of architectural and industrial painting contractors and do-ityourself
homeowners. In Latin America, we sell a variety of
branded architectural paint, coatings and related products through
company-operated specialty paint stores and third party retailers,
dealers, licensees and other distributors.
In 2017, The Americas Group net sales increased 8.8 percent
compared to the prior year to $9.12 billion. The growth was driven
by higher architectural paint sales volume across all segments
and selling price increases. Full-year net sales by stores in the
U.S., Canada and the Caribbean, open more than 12 calendar
months, increased 6.3 percent, and net sales in the Latin America
region increased 4.5 percent, both as compared to the prior year.
Segment profit increased 10.2 percent to $1.77 billion, and segment
operating margin increased 30 basis points to 19.4 percent
compared to the prior year.
By our estimate, our company-operated stores in the U.S.
grew architectural paint sales volumes at a rate of approximately
two times the rate of U.S. market growth, and our protective and
marine coatings business reversed a two-year negative sales
trend. During the year, we opened 87 net new stores in the U.S.,
Canada and the Caribbean, bringing total store count in the region
to 4,267. Our customers responded favorably to multiple new
product introductions, including Extreme Cover™ Interior Stain
Blocking Paint and Primer in One, Emerald® Interior/Exterior WaterBased
Urethane Trim Enamel, and ProMar® 200HP Zero VOC
Interior Acrylic. J.D. Power recognized us for “Highest in Customer
Satisfaction among Interior Paints.”*
In Latin America, selling price increases enabled us to grow
revenues in the full year, though underlying economic conditions in
the region remained challenging. We continued to position ourselves
for long-term growth, adding 14 new company-operated stores
in the region, bringing our total to 353. We also added 65 new
dedicated dealers, bringing our total to more than 700. We are
adopting some best practices from our U.S. stores in Latin America,
with initial emphasis on customer segmentation and new account
development. Some high-potential new products introduced in the
region during the year were ProCraft® flat interior paint, ProCraft®
ceiling paint, and Krylon® Super Maxx premium fast-drying aerosol,
all based on successful product platforms in the U.S. and Canada.
We also released our ColorSnap® App in Spanish and Portuguese
to assist consumers with the challenging process of color selection
and coordination.
CONSUMER BRANDS GROUP
The Consumer Brands Group sells a broad portfolio of branded
product lines through a variety of independent retail outlets in the
United States, Canada, United Kingdom, China, Australia and New
Zealand. In 2017, the Consumer Brands Group net sales increased
41.1 percent compared to the prior year to $2.15 billion, but segment
profit decreased 25.0 percent year-over-year to $226.0 million
and segment operating margin decreased 920 basis points to
10.5 percent compared to 2016. Segment profit and operating
margin in the year include acquisition purchase accounting charges
of $107.6 million that were partially offset by Valspar profit from
operations of $71.7 million. Excluding the impact of Valspar, core
sales for the Group decreased 8.4 percent, core segment profit
decreased 13.0 percent, and core operating margin decreased
100 basis points, all compared to the prior year.
The results for our core Consumer Brands Group business in
2017 were disappointing. Demand was soft across most product
categories, retail channels and geographies throughout the year.
This weakness was particularly acute in Europe and in smaller retail
accounts across the U.S. and Canada. Many retailers responded
to slower sales of architectural paint to Do-It-Yourself customers
*Sherwin-Williams received the highest numerical score among Interior Paints in the
J.D. Power 2017 Paint Satisfaction Study, based on 4,625 total responses from 12 companies
measuring experiences and perceptions of customers, surveyed February-March 2017.
Your experiences may vary. Visit jdpower.com.
 3
by scaling back inventory. The impact of weak sales volumes
on segment profit was exacerbated by rising raw material costs
throughout the year.
Although the Consumer Brands Group results fell short of
expectations in 2017, we remain confident in the future prospects
for this Group for many reasons. The combined portfolio of brands
managed by this talented team is second to none in the paint and
coatings category in terms of appeal to retailers and end users.
Brands such as Valspar® and HGTV HOME® by Sherwin-Williams
paints, Minwax® stains and varnishes, Purdy® applicator tools,
Cabot® stains, Krylon® aerosol paint and Thompson’s® WaterSeal®
waterproofing products, to name a few, rank among the bestknown,
most sought-after brands in their respective categories.
Above all, we are excited about the category expertise and
strong customer relationships the Valspar acquisition brings to
this segment. The integration of Valspar sales and marketing
teams is, for the most part, complete with little or no customer
disruption. We are addressing the persistent raw material inflation
by implementing price increases across all retail channels and
customers, which we expect will benefit results for the Group
in 2018.
In 2017, the Valspar paint brand was recognized by J.D. Power
as the “Highest in Customer Satisfaction among Exterior Paints.”*
The power of the Valspar brand and the caliber of the Valspar
sales team have strengthened our relationship with key retailers
and provided additional tools and expertise to help convert more
shoppers into buyers. Valspar also enhances our global scale,
bringing us platforms to build upon with Huarun™, a leading
Chinese domestic paint brand, and Wattyl®, a leading product
brand and a chain of company-operated outlets in Australia and
New Zealand.
The success of our reportable segments is supported by a
highly efficient global supply chain and research & development
organization, all managed within our Consumer Brands Group
segment. Innovation remains a core competency of this
organization, validated by the introduction of more than 20 new
architectural products in 2017. With the addition of Valspar’s
supply chain assets, we now operate 84 manufacturing and
distribution facilities worldwide. While this added capacity will
reduce the need for future capital investments, we see meaningful
opportunities to further optimize our global footprint in support of
profitable growth. In 2017, we exited three non-essential facilities
and announced our intention to consolidate five others. We opened
a new distribution center in Waco, Texas, and a new manufacturing
site in Nantong, China, to help us capitalize on our rapid growth in
both regions.
Our commercial transportation fleet covered a record
74 million miles in 2017 – 7.3 percent more than in 2016 – and
was awarded a SmartWay Excellence Award for superior
environmental performance and reduction of freight emissions
through collaborative and operational practices. Our fleet also
earned the National Private Truck Council (NPTC) 2017 Fleet of
the Year Safety Award. Safety and quality remain top priorities
throughout the organization, and many of our facilities have
experienced no recordable injuries for multiple years. At the end
of 2017, we had 28 VPP Star sites, 12 OHSAS 18001 sites in the
U.S. and 44 ISO 14001 sites globally – more certifications than any
other paint and coatings manufacturer.
PERFORMANCE COATINGS GROUP
In 2017, net sales by our Performance Coatings Group increased
90.4 percent compared to the prior year to $3.71 billion. Segment
profit increased 16.1 percent to $298.5 million, but segment profit
margin decreased 510 basis points to 8.1 percent compared to
2016. Segment profit and operating margin for the year include
Valspar profit from operations of $231.1 million partially offset
by acquisition purchase accounting charges of $183.1 million.
Excluding the impact of Valspar, core sales for the Group increased
3.0 percent, core segment profit decreased 2.6 percent, and core
operating margin decreased 72 basis points, all compared to the
prior year.
The Performance Coatings Group provides differentiated
product technology and value-added services to a diverse group of
industrial coatings customers. We made impressive progress on
the integration of Valspar operations during the year, organizing
this new segment into industry-specific business units to enhance
focus and competitiveness. There are significant opportunities to
further optimize this structure as we aggressively pursue our value
capture targets.
The addition of Valspar’s coatings business significantly
increases our operating scale outside North America and broadens
our technology offering, both of which will enable us to create
new opportunities. Geographically, Valspar adds sales volume
and large-scale manufacturing and distribution to our operations
in Europe and Asia, enabling us to serve more customers more
profitably. The combination also results in a more complete
product offering and diverse service model. For example,
combining Sherwin-Williams’ local-market, small-batch blending
capability with Valspar’s expertise in supplying highly customized
coatings solutions to large original equipment manufacturers
*Valspar received the highest numerical score among Exterior Paints in the J.D. Power
2017 Paint Satisfaction Study based on 3,139 responses from 12 companies measuring
experiences and perceptions of customers surveyed February-March 2017. Your experiences
may vary. Visit jdpower.com.
With the completion of the Valspar acquisition
on June 1, 2017, we are a larger, more diversified
and more global enterprise.
4
(OEMs) makes us a compelling single-source coatings provider
for OEMs and their tier supplier networks. Valspar also brings us
two entirely new profitable growth opportunities in packaging
coatings – coating liners for food and beverage packaging – and
coil & extrusion coatings – coatings used in the manufacture of
appliances, HVAC equipment, metal building products and more.
We’ve also begun to leverage our combined technology
portfolio, which is significantly enhanced by Valspar’s waterborne
formulations and resin development capabilities. Recently released
examples of industrial coatings innovation include AquaGuard®
low-VOC coatings for general industrial applications, the
Sayerlock® HydroPlus™ family of low-VOC high-performance wood
coatings, dynamic clearcoat CC200 premium coatings, DeBeer®
waterborne coatings, and ValPure® V70 non-BPA epoxy coating
for can coating.
BOARD AND MANAGEMENT CHANGES
In April, Michael Thaman was elected to our Board of Directors
and appointed to the Compensation and Management
Development Committee. Mike currently serves as Chairman,
President and Chief Executive Officer of Owens Corning, a Toledo,
Ohio-based world leader in composite solutions and building
materials systems and a market-leading innovator of glass fiber
technology. He has demonstrated strong leadership in a variety of
roles at Owens Corning for 25 years, and his broad financial and
international experience will serve the Board and our shareholders
well. We look forward to his contributions to Sherwin-Williams for
many years.
In December, Sean Hennessy announced his retirement after
33 years of dedicated service to the Company, including 15 years as
the Company’s Senior Vice President – Finance and Chief Financial
Officer. Over the last year, Sean served in the role of Senior Vice
President – Corporate Planning, Development and Administration,
supporting Al Mistysyn in his successful transition to Senior Vice
President – Finance and Chief Financial Officer and assisting in the
acquisition and integration of Valspar. Sean will be missed, and we
wish him a very happy and healthy retirement.
Our continued focus on internal talent development resulted
in several leadership appointments during the year. Mary Garceau
was promoted to Senior Vice President, General Counsel and
Secretary. Mary joined Sherwin-Williams in 2014 as Associate
General Counsel for our Paint Stores Group and previously served
as Vice President, General Counsel and Corporate Secretary for
Bob Evans Farms, Inc. and Thirty-One Gifts, LLC. Dennis Karnstein
was named President & General Manager of Industrial Wood
Coatings Division, Performance Coatings Group. Dennis joined
Sherwin-Williams in 1989 as a Management Trainee in our Paint
Stores Group. After working his way up through the ranks in
our paint stores organization, Dennis joined our Global Finishes
Group as Senior Vice President & General Manager for Product
Finishes Europe and President & General Manager, Product
Finishes Division of Global Finishes Group. Most recently, Dennis
served as Senior Vice President of Global Integration for the
Valspar acquisition. Bruce Irussi was promoted to the position
of President & General Manager of General Industrial Coatings
Division, Performance Coatings Group. Bruce joined SherwinWilliams
in 1984 as a Professional Coatings Representative, and
worked his way up as Branch Manager, Sales Manager, Product
Finishes Division Area Sales Manager, District Manager and
Regional Facility Manager and Senior Vice President of Sales,
Product Finishes Division – North America. Most recently, Bruce
was President & General Manager of the Product Finishes Division
of our Global Finishes Group. Robert Lynch was promoted to
President & General Manager of the North America Division of
our Consumer Brands Group. Rob joined the Company in 2000
and most recently served as Senior Vice President of Sales within
our Performance Coatings Group. Lee Diamond was promoted to
President & General Manager of Canada Division, The Americas
Group. Lee joined the Company in 1991 and most recently served
as Vice President of Sales for the Southeastern Division of The
Americas Group.
Diana Strongosky was promoted to Senior Vice President
Operations North America. Diana joined the Company in 1988
and most recently served as Senior Vice President of R&D within
our Global Supply Chain organization. Kathleen Szczesniak was
promoted to Senior Vice President of R&D within our Global
Supply Chain organization. Kathleen joined the Company in 1998
and most recently served as Vice President for R&D within our
Performance Coatings Group.
I have often said the acquisition of Valspar represents the
greatest one-time infusion of talent in Sherwin-Williams’ history.
Shortly after closing the transaction, we announced several
appointments of Valspar personnel to our senior leadership team.
Aaron Erter was named President of Consumer Brands Group.
Aaron joined Valspar’s consumer business in 2011 after serving
15 years with Black & Decker Corporation, where he held
leadership positions in sales, global marketing, product
Continued growth in residential and
commercial construction and remodeling
across North America should benefit The
Americas Group and our Consumer Brands
Group, and positive momentum in many
industrial end segments worldwide should
benefit our Performance Coatings Group.
5
development and new business development. Bryan Young was
named Vice President – Corporate Strategy & Development.
Bryan joined Valspar as Vice President of Corporate Development
in 2015 and previously held similar positions at Agrium and
Rockwell Automation. Karl Jorgenrud was named President &
General Manager of Protective & Marine Division, Performance
Coatings Group. Karl joined Valspar in 1994 and most recently
served as Vice President & General Manager of Valspar’s Global
Functional Coatings. Sam Shoemaker was named President &
General Manager of Global Packaging, Coil, and Coatings Resins
& Colorants Division, Performance Coatings Group. Sam joined
Valspar in 2012 and most recently served as Valspar’s Senior
Vice President of Global Packaging. He previously worked for The
Dow Chemical Company. Mike Bourdeau was named President
& General Manager of Coil & Extrusion, Performance Coatings
Group. Mike joined Valspar in 1986 and most recently served
as Vice President, Global Coil & Extrusion. Colin Davie was
named President & General Manager, Global Coating Resins &
Colorants, Performance Coatings Group. Colin joined Valspar in
2012 and most recently served as Valspar’s Vice President, Global
EPS/CCA. He previously worked for DuPont and Ciba Specialty
Chemicals plc. Finally, J.R. Benites was named Regional President
& General Manager of Latin America, Performance Coatings. J.R.
joined Valspar in 1997 and most recently served as Senior Vice
President and President of Valspar’s Latin America Region with
executive responsibility for Global Coil.
OUTLOOK FOR 2018
We begin 2018 with expectations for another strong year in
most of our businesses. Continued growth in residential and
commercial construction and remodeling across North America
should benefit The Americas Group and our Consumer Brands
Group, and positive momentum in many industrial end segments
worldwide should benefit our Performance Coatings Group. It is
our intention to reinvest a portion of the savings from recent U.S.
tax reform into growth initiatives, particularly in The Americas
Group, and our expanded global footprint and technical capabilities
will provide greater exposure to many new growth opportunities in
emerging markets.
From a profitability standpoint, we should continue to
benefit from operating expense control and volume growth, both
domestic and abroad, but rising raw material costs are likely to
constrain gross margins, especially early in the year. Raw materials
represent roughly 85 percent of the cost of goods sold for most
paint and coatings products, and we anticipate inflation across
the raw material basket in 2018 to average in the mid-single digits
in percentage terms – perhaps higher in the first half of the year.
This inflation will likely be broad-based across petrochemical
feedstocks, pigments and packaging. We will continue to closely
monitor the raw materials markets and are prepared to implement
additional price increases if conditions so warrant.
On a final note, the longer-term financial targets we presented
at our Financial Community Presentation on October 3, 2017,
will serve as a gauge to measure our success in managing the
complexities of a large-scale integration process while maintaining
a steadfast focus on the reason we are here – to help our
customers be more successful. Using combined Sherwin-Williams
and Valspar pro-forma 2016 results as a baseline, and 2020 as a
target date, we expect to grow net sales at a compound annual rate
of 4 to 6 percent, expand EBITDA margin to a range of 18.8 to
21 percent from 16.6 percent, increase adjusted free cash flow –
net operating cash less capital expenditures – to a range of 10 to
10.5 percent of net sales from 8.9 percent, and grow core diluted
net earnings per common share(1) at a compounded annual growth
rate of 9 to 12 percent. By 2020, we expect to achieve at least
$1 billion in cumulative savings from the integration of Valspar.
To all employees of the new Sherwin-Williams, I offer my
heartfelt thanks for all of your hard work, skill and commitment
this past year; I truly believe we have the best team in the business,
and that is integral to our success. On behalf of the approximately
60,000 Sherwin-Williams employees around the world, we offer
our thanks and appreciation to our customers, suppliers and
shareholders for your continued trust and confidence.
John G. Morikis
Chairman, President and Chief Executive Officer