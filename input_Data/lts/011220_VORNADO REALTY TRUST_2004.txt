Vornados Funds from Operations for the year ended
December 31, 2004 was $750.0 million, $5.63 per
diluted share, compared to $518.2 million, $4.44 per
diluted share, for the year ended December 31, 2003.
Net Income applicable to common shares for the year ended
December 31, 2004 was $571.0 million, $4.35 per diluted
s h a re, versus $439.9 million, $3.80 per diluted share, for the
previous year. Here are the financial results by segment:
% of 2004

This letterWe use Funds from Operations, Adjusted for Comparability
as an earnings metric to focus on the core re c u rring business
by eliminating certain one-time items. The following chart
reconciles Funds from Operations to Funds from Operations,

We use Funds from Operations, Adjusted for Comparability
as an earnings metric to focus on the core re c u rring business
by eliminating certain one-time items. The following chart
reconciles Funds from Operations to Funds from Operations,

2004 was a remarkable year, a record breaker for money
making by any measure.
n Vornados total return to shareholders was 46% in 2004.
After all is said and done, our trading price is the report
card we value most.
n Vornados 2004 EBITDA was $1.286 billion. FFO was
$750 million.(1)
n Our New York and Washington Office, Retail and Mart
businesses each performed at or above budget and
each had outstanding same store results. Our financial
performance continues to benefit from our strategy of
having several lines of business (our three-legged stool)
and from concentrating our assets in New York and
Washington, DC, the two very best markets in the nation.
n We made tremendous progress towards accomplishing
our goals for AmeriCold.
n In 2004 and so far this year, we had an additional
unrealized mark-to-market gain of $200 million on our
Alexanders s h a res, as its stock price doubled.
731 Lexington Avenue, Alexanders flagship tower,
the new home of Bloomberg LP and One Beacon Court,
enjoyed universal acclaim.
n As of this date, we scored a $181 million unrealized gain
on our investment in Sears and a $62 million unrealized
gain on our investment in GMH.
n We recently agreed to invest about $450 million of equity
for a one-third i n t e rest in Toys R Us, partnering with
private equity firms Bain Capital and Kohlberg Kravis
Roberts.
Lease, Lease, Lease
The mission of our business is to create value for our
s h a reholders by growing our asset base through the addition
of carefully selected pro p e rties and by adding value thro u g h
intensive and efficient management. As in past years,
M i k e( 2 ) and I are pleased to present leasing statistics for our
businesses. In our business, leasing is what its all about.
Office Merchandise Mart

David Greenbaum, Dean of our faculty, operates our New
York office business, where we own 13.4 million square
feet of office space at an incredible average price of
$175 per square foot.( 3 ) ( 4 ) David has spent the last years
harvesting values for us by growing rents, year in and
year out:
Average Average
Initial Rent Escalated Rent

The New York office division contains 800,000 square feet
of street retail in the base of our office buildings. David
shares responsibility for leasing this space with Sandeep
Mathrani, who runs our retail business. Sandeep has
responsibility for an additional 500,000 square feet of
Manhattan street retail. In total, we own 1.3 million square
feet of street retail in Manhattan (and thats not counting
Alexanders square footage) which, considering our assets
in Penn Plaza around Macys, around 14th Street, around
Bloomingdales, on Madison and Fifth Avenues, and
now in SoHo, may be our single best business.
David reports that the tide has definitely turned in
New York. Tenants are once again expanding in the city,
and in our buildings, i.e., Federated Department Store s
added another floor to its New York headquarters space
at Eleven Penn Plaza this year, as did Rainbow Media,
and on and on. Another great moneymaker is dealing
blocks of space that become surplus as a result of merg e r s ,
relocations, etc. For example, MONY merged with AXA,
rendering 289,000 square feet at 1740 Broadway surplus.
We will re p o rt in the Second Quarter the deal that David did:
AXA will pay $52 million to cancel this lease that we
believe is some $10 below market. Same story with
Arista Records at 888 Seventh Avenue and H e a l t h
Insurance Plan at 7 West 34th Street and on and on,
year in and year out.
Like the calm before the storm, the market feels to us as
if it is consolidating before a wave of rising rents. The rent
increases of the last cycle (19982000) were led by a
p ronounced spike in apartment prices. Eerily, the exact
same dynamic is happening today, in spades. If you
t h i n k c o m m e rcial real estate is expensive, just look at
apartment prices in New York, or even better, look at art
prices worldwide.
It has been my observation that over the long term in
New York, replacement cost has been an accurate predictor
of asset values. For what its worth, rising land prices
and rising construction costs are now driving re p l a c e m e n t
cost higher. If you can find a site, a new office building in
New York has to cost over $650 per square foot. This
rising umbrella price bodes well for our values.
In the first quarter of 2005, we transferred our 440,000
s q u a re foot office building at 7 West 34th Street from the
O ffice division to the Mart division. A perfect storm perm i t t e d
this to happen. The New York Gift Mart Association
tenants, many of whom are our Mart Division tenants in
other locations, were displaced from a building on lower
Fifth Avenue. At the same time, David was able to vacate
7 West 34th Street when one office tenant declined to
renew and the other vacated its space with seven years
left on its lease. David negotiated a $29 million lease
termination payment and Chris Kennedy lassoed the Gift
Mart tenants.
Washington Off i c e
Mitchell Schear, the first-rate real estate guy who runs
our Washington office business, is brand building. Our
flagship asset in Washington is Crystal City, a 28 building,
7.8 million square foot city in a Grade A location on the
western boundary of Reagan National Airport, on the
shores of the Potomac, overlooking the Capitol. Our brand
builder is freshening up this asset. The just completed
retail streetscape(5) adds an exciting Hollywood front and a
collection of first-rate restaurants that stretches three
blocks long. Crystal City is an established location, which
is only getting better (Mitchells slogan is... getting
i n c reasingly cool), and has been increasingly well re c e i v e d
by prospective tenants as well as the real estate bro k e r a g e
c o m m u n i t y, who in the end are our sales force. We leased
1.5 million square feet in 2004 in Crystal City. We leased
2.8 million square feet overall in Washington.
Last year, Crystal City landed two of the biggest deals in
Northern Virginia, attracting Public Broadcasting Service, a
prestigious new tenant who leased 122,000 square feet,
and relocating and expanding Federal Supply Service into
262,000 square feet. We lost a big fishVerizon, an
800,000 square foot prospect moving out of Manhattan, who
in the end predictably chose nearby New Jersey. My guess
is that we never really had a shot at this New York-centric
client, but the process was instructive. Verizon and its
advisors conducted an exhaustive search focused on New
Jersey and Northern Virginia. While we didnt make the
deal, we handily won a rigorous competition to be best in
Northern Virginia. This bodes well for our future.
Re-leasing the space being vacated by the Patent
Trademark Office is going a bit slower than the selfimposed
schedule we established in this letter last year.
We may be running behind, but remember, we have great
space and a great team. Whatever minor shifts there are in
the lease-up schedule, our long-term (and short - t e rm)
values are well established and safe in Crystal City.
Mitchell is making pro g ress attracting more corporate
tenants to Crystal City, adding to our government and
government-contractor clients. There is one such user
for almost 250,000 square feet pending now.
Teardowns do work. Time and again, we have found
that our land as a raw development opportunity is worth
m o re than the land encumbered by a tired building.
Mitchell (along with David and me, too) is actively studying
a redevelopment program for the oldest Crystal City
buildings. The scheme involves razing four buildings
containing 650,000 square feet (220,000 square feet of
which is PTO space) and rebuilding with four spanking
new, much larger buildings containing about 1.4 million
square feet, of which 350,000 square feet would be
f o r-sale residential condominiums in a white hot market.
This project would be phased over five years as the older
buildings come off lease; the first phase would be the
condos. We create value here both by doubling the square
footage and getting a premium for being brand new.
This project is still only in the planning stages and will, of
course, require the usual governmental approvals.
We acquired our Washington office assets in stages,
completing the acquisition on January 1, 2002. Deal pricing
was about $200 per square foot at a 10.3% cap rate,
which we underw rote down to 10.0% to re s e rve for the
re-leasing capex associated with the PTO move-out. We
love this real estate and are certain that rents, which
have been flattish for the last three years, will rise 
us in the future. In any event, even with flat income and
even assuming a today cap rate for this portfolio as high
as 7%, the cap rate compression from 10% to 7% has
created a mark-to-market unrealized gain of over $1 billion
for us, so far.

Our retail business is flourishing under Sandeeps leadership.
His numbers are gre atRetail EBITDA increased $25 million
this year; same store increased 5.5%.
Redevelopment of existing assets, especially large c o mplex
retail pro p e rties, continues to be a terrific business for
us and is a favorite of mine. Here are some highlights:
In December 2003, we paid $145 million to acquire the
B e rgen Mall, a 900,000 square foot fixer-upper on eighty
a c res in Paramus, New Jersey. We spent 2004 in the design,
planning and pre-leasing phase. It is an indication of
how completely we intend to re c o n f i g u re and reposition this
asset that we have canceled both a 139,000 square foot
Value City lease paying $12 million and a 338,000 square
foot Macys lease receiving $2 million. We plan to re d e v e l o p
this pro p e rty into a lifestyle center and will invest at
least an additional $100 million. In March 2005, we form a l l y
submitted our plans to the borough of Paramus, beginning
an expected one-year approval pro c e s s .
We recently obtained approvals to add 190,000 square
feet of freestanding boxes to our Green Acres super
regional mall in Valley Stream, Long Island. Immediately
after the Christmas selling season, we will begin these
physical additions, as well as a full cosmetic renovation of
the interior mall.
In March 2005, we purchased a 50% interest in Beverly
Connection, a two-level shopping center located dire c t l y
a c ross from Beverly Center regional mall in Los Angeles,
California. Our shopping center contains approximately
322,000 square feet and a parking stru c t u re with over
1,500 spaces. The venture intends to redevelop the pro p e rt y
and add retail, residential condominiums and assisted living
facilities. This project is subject to governmental appro v a l s .
S a n d e e p s retail group, working closely with Michelles
acquisitions group, Joe, We n d y, Alan and Mike and me,
was the first line of responsibility in analyzing the megadeals:
Toys R Us, Sears and Merv y n s. Going forw a rd,
all our senior management will be involved in Toys R Us,
but Sandeeps group will spearhead the real estate side.
Hotel Pennsylvania
Hotel Penn had EBITDA of $15.6 million in 2004, up from
$4.6 million. In a tightening New York City hotel market,
our budget for 2005 is $19 million. Mike and I believe
this asset (in the end, a development play) is much more
valuable today than it was just last year, and certainly
much more than the $149 million for which it is on our
books (original cost was $176 million). Timing and patience
are everything.
AmeriCold Logistics
It is the objective of our Company to reduce our investment
in AmeriCold (our refrigerated warehouse affiliate),
simplify its structure, improve its performance and, at the
proper time and price, eventually exit. We made real
progress toward these goals this year.
In February 2004, AmeriCold completed a $254 million
financing that allowed us to withdraw $135 million, our share
of the net proceeds. This financing substantiated a value
of this investment which was significantly higher than most
of our shareholders and analysts had been carrying.
Our initial investment here was made in 1997. Since that
time, and in order to comply with REIT tax regulations,
Vornado Realty Trust and its partner, Crescent Real Estate
Equities, have owned AmeriColds real estate assets,
and two independent companies (so-called paperclips),
one from us (Vo rnado Operating Company) and one fro m
Crescent, have owned the operating business. Simplifying
this complex, awkward structure has been a goal of
ours for years. We did just that in November 2004, when
subsidiaries of Vo rnado and Crescent bought the operator
f rom Vo rnado Operating Company and Cre s c e n t s paperc l i p ,
thereby combining AmeriColds real estate and operations,
through the use of a TRS. This was made possible by a
private letter tax ruling on transfer pricing that Joe Macnow
m a s t e rminded in 2004. Subsequently, Vornado Operating
Company was dissolved.
Hotel Pennsylvania
Hotel Penn had EBITDA of $15.6 million in 2004, up from
$4.6 million. In a tightening New York City hotel market,
our budget for 2005 is $19 million. Mike and I believe
this asset (in the end, a development play) is much more
valuable today than it was just last year, and certainly
much more than the $149 million for which it is on our
books (original cost was $176 million). Timing and patience
are everything.
AmeriCold Logistics
It is the objective of our Company to reduce our investment
in AmeriCold (our refrigerated warehouse affiliate),
simplify its structure, improve its performance and, at the
proper time and price, eventually exit. We made real
progress toward these goals this year.
In February 2004, AmeriCold completed a $254 million
financing that allowed us to withdraw $135 million, our share
of the net proceeds. This financing substantiated a value
of this investment which was significantly higher than most
of our shareholders and analysts had been carrying.
Our initial investment here was made in 1997. Since that
time, and in order to comply with REIT tax regulations,
Vornado Realty Trust and its partner, Crescent Real Estate
Equities, have owned AmeriColds real estate assets,
and two independent companies (so-called paperclips),
one from us (Vo rnado Operating Company) and one fro m
Crescent, have owned the operating business. Simplifying
this complex, awkward structure has been a goal of
ours for years. We did just that in November 2004, when
subsidiaries of Vo rnado and Crescent bought the operator
f rom Vo rnado Operating Company and Cre s c e n t s paperc l i p ,
thereby combining AmeriColds real estate and operations,
through the use of a TRS. This was made possible by a
private letter tax ruling on transfer pricing that Joe Macnow
m a s t e rminded in 2004. Subsequently, Vornado Operating
Company was dissolved.
Mining For Real Estate
We were born retail developers, but early on we learn e d
about mining. Some of you may remember an oilman
named T. Boone Pickens, who in the 1970s discovered
that one could mine for oil cheaper on the New York Stock
Exchange than in the oil fields. We learned that lesson
too, with respect to mining for retail square footage in the
stock market, where irreplaceable retail real estate can
be obscured by a failing retailer. Extracting these values is
indeed a core competency of ours, and one that has cre a t e d
vast wealth for our shareholders. Our 1980 takeover of
Vornado, whose sole asset at the time was the failing Two
Guys discount store chain, and our investment in
A l e x a n d e rs are both examples of this, as are our re c e n t
investments in Sears and Toys R Us.(11)
Toys R Us was started by the brilliant Charles Lazarus in
1978. It was the first category killer in America and started
a retailing revolution. Fast forward to today. The modern
Toys R Us is comprised of three segments: Domestic
Toys, International Toys (together Global Toys) and Babies
R Us. Toys Board initially offered for sale only Global
Toys, but after a process decided to sell the entire
company. We were the winning bidder with partners
Bain Capital and Kohlberg Kravis Roberts, with each of
us owning an equal one-third. Toys R Us has $11 billion
in sales and 1,500 stores.( 1 2 ) We will pay $6.6 billion for
the equity, with a total price of $7.2 billion taking into
account net debt and expenses. Our plan for this business
is simple. International Toys and Babies R Us are
growth businesses that we intend to continue to grow
aggressively. Domestic Toys is a turn around, underpinned
by real estate asset values. While each equal partner will
p a rticipate in the entire enterprise in full measure, as you
would expect, we will be the leader on the real estate side.
In between Mervyns and Toys R Us came Sears. Sears
was generally perceived as a slowly but surely declining
retailer  la Two Guys and Alexanders, but we saw a
collection of truly great (many irreplaceable) assets: 128
million square feet in 873 stores, of which over two-thirds
are owned or ground leased in (Im guessing) 70% of the
best malls in America; the Kenmore appliance brand;
the Craftsmen tool brand; 54% ownership of s e p a r a t e l y
traded Sears Canada; the Orc h a rd supply chain; a highly
profitable in-home service business and more.
We acquired 1,176,600 shares directly and 7,916,900
s h a res derivatively for a 4.3% position. ESL Investments,
headed by the investor Ed Lampert, already had a 14.6%
position pre-dating our position by years.
One of the outcomes we anticipated (although not our
preferred outcome) was the Kmart-Sears merger proposed
by Ed Lampert, the majority owner of Kmart, as well
as the largest holder of Sears. We investigated numerous
a l t e rnatives while avoiding saber rattling and in the end
elected to support the merger and receive all stock.( 1 3 )
H e re s the math: Marked-to-market as of today, we have
a recognized $20 million short - t e rm capital gain and an
u n recognized $161 million capital gain that we intend
to defer until it becomes long-term in September 2005.
Our total economic gain as of today is $181 million.( 1 4 )
more complicated. Our current position
includes 271,000 shares of Sears Holdings held directly and
2,492,000 shares held derivatively, for a total position in
Sears Holdings of 2,763,000 shares. When we sell, we
expect to distribute a capital gain dividend that if measure d
today would be $181 million, or $1.24 per share, of which
$1.10 per share would be taxed as a long-term capital gain.
For the re c o rd, Vo rnado retains the right at any time to
buy or sell the securities of Sears Holdings as we deem
appropriate.
T h e re is a downside to all this. The announcement of our
investment in Third Quarter SEC filings triggered an i m m ed
i a t e 23% spike in Sears trading price on ten times normal
volume. Its better for business to accumulate
positions anonymously.
Think about it; in a few short months we earned over
$240 million on our Sears and GMH investments and,
if we include the appreciation on Alexanders stock,
almost one-half billion dollars, all in addition to the regular,
and growing earnings of our core real estate business.
Who else can say that? We are one of the very few
R E I Ts with the track re c o rd and skill set to handle these
kinds of complex, highly profitable transactions;(15) for us,
these are right down the middle. In an environment where
cash is trash and everything appears to be fully priced,
we may put more capital to work in these sorts of deals,
i.e., where we buy a sheep in lambs clothinga l w a y s
m e a s u red, always careful, always proportional.
West Side Story
In the Penn Plaza district of Manhattan, Vornado owns
5.6 million square feet of office space in five buildings,
the 1.4 million square foot 1700 room Hotel Pennsylvania,
and almost 500,000 square feet of retail space. We have
accomplished a great deal here and, with income up and
cap rates down, have probably doubled our money. This
is still a work in process, with so much more value to cre a t e ,
it may be a lifes work.
We are partnering with the Related Companies (developers
of the Time Warner Center) with respect to a competitive
Request For Proposal to acquire the Farley Building, which
will become the home of the Moynihan Rail Station. This
McKim, Meade & White landmarked structure sits on a
double-size super block directly across Eighth Avenue
from Madison Square Garden. The east half of Farley will
be dedicated to the train station, the west half will be
available for 700,000 square feet of redevelopment and
there are one million square feet of available air rights.
A great outcome here would be if Madison Square Garden
were to choose to build a new state-of-the-art arena in the
west portion of the Farley Building, thereby freeing up the
old Madison Square Garden site for development.
We are Madison Square Gardens landlord, its parent
Cablevisions landlord and Bloomberg LPs landlord. The
epic fight between the football Jets and the Mayor on
the one hand, and Madison Square Garden on the other,
over the building of a stadium and convention center versus
apartments and offices on the West Side of Manhattan
is being waged over a site only blocks away from our
s t ronghold, and at this time has an unpredictable outcome.
Any outcome here will enhance this West Side District,
which the City Administration and real estate community
have earmarked as the future growth corridor of New York
all good-to-great for us.
Capital Markets
We are committed to maintaining our investment grade
credit rating. In August 2004, we issued $250 million
of unsecured notes due August 2009 at a rate of 4.772%.
This was our third issuance in this market.
We do multiple financings each year and 2004 was no
exception. In New York, where we have 20 buildings,
In 2004, we issued $492 million of fixed price pre f e rre d
in four issues at an average coupon of 6.45% and
redeemed $308 million of preferred at an average coupon
of 8.49%.
In March 2005, we sold $500 million of 3.875% 
d e b e n t u res due 2025, convertible into common shares at
an initial exchange price of $91.25, a 30% premium. These
securities are puttable in 2012, 2015 and 2020 andseven support property level finance totaling $1.1 billion
and 13 are unencumbered by debt. Just for fun, lets look
at three New York property level financings we did this
year where the debt trail validates our value creation.
redeemable by us at par after 2012. Our thinking here was
simple and, we believe, sound. We bought down the
coupon of this debt by 140 basis points to 3.875% by
issuing a seven-year option to purchase our common stock
at an initial price of $91.25, a 30% premium. We hope that
the option will be exercised, and quickly.
We always run our business with larger cash balances than
most and lower debt ratios than most. In the end, our
ability to seize opportunities and make money comes in
large measure from our financial might.
Thanks to Wendy, our great Capital Markets Queen.
Other Matters
Mike did it again. Starting in July 2004, we advanced
$114 million to GMH Communities Trust, Gary M.
H o l l o w a y s student and military housing business, in
a highly structured mezzanine-bridge loan with equity
features. Mike had partnered with Gary in a successful
student housing business during his Goldman Sachs days.
All went according to planGMH completed its IPO in
November, just three plus months after we made our
investment. This transaction resulted in an economic gain
to us of $62 million.
This year, we expect to finally end the Prime Group saga
when we sell our 4 million shares for $7.25 per share(16)
pursuant to an announced offer expected to close in third
q u a rter 2005. In the end, this investment, a mistake that
soiled our books and analysts re p o rts for years, ends up
with a loss of about $10 million.
Please see page 57 of our financial statements for
Deloitte & To u c h e s 404 Managements Internal Contro l
opinion, which Joe Macnow, our CFO, and his whole
financial team are justifiably proud of, and worked very
hard to achieve. Sarbanes-Oxley compliance is Joes night
job. Special thanks to Joe, our longest tenured division
head, for all he does during the day.
Every time we, or anybody, has sold an asset over the last
five years, or not bought one, its been a big, big mistake.
To state the obvious, the real estate bull market we have
predicted for years in this letter is now in full bloom. It
may be caused by low interest rates; it may be caused
by excess liquidity in the worldwide system; it may be
caused by a flight to hard assets; it may be technical
worldwide institutions are under-allocated in this asset
class; or it may even be for who knows what. And, it is a
bull market in the face of flattish rents. Some think it is a
bubble. I do not. Today, most p ro p e rties are bought and
sold in a modified auction process. Where there were 3 or
5 bidders years ago, there a re 20 or 30 today. The ru l e
used to be, the higher the price, the fewer the bidders.
To d a y, a billion dollar pro p e rty sells as easily as a $50 million
one. As the market marches on, each sale is higher than the
last one. Thats a bull market. Over the last several years,
real estate has repriced. I believe this is a long cycle
move. Get used to it; give or take 10%, these prices are
here to stay, for some time.
A short word about interest rates. While we must run the
business as if interest rates were going to rise, it seems
to me, there is an anchor holding them down. Thats just
an observation.
Other Matters
Mike did it again. Starting in July 2004, we advanced
$114 million to GMH Communities Trust, Gary M.
H o l l o w a y s student and military housing business, in
a highly structured mezzanine-bridge loan with equity
features. Mike had partnered with Gary in a successful
student housing business during his Goldman Sachs days.
All went according to planGMH completed its IPO in
November, just three plus months after we made our
investment. This transaction resulted in an economic gain
to us of $62 million.
This year, we expect to finally end the Prime Group saga
when we sell our 4 million shares for $7.25 per share(16)
pursuant to an announced offer expected to close in third
q u a rter 2005. In the end, this investment, a mistake that
soiled our books and analysts re p o rts for years, ends up
with a loss of about $10 million.
Please see page 57 of our financial statements for
Deloitte & To u c h e s 404 Managements Internal Contro l
opinion, which Joe Macnow, our CFO, and his whole
financial team are justifiably proud of, and worked very
hard to achieve. Sarbanes-Oxley compliance is Joes night
job. Special thanks to Joe, our longest tenured division
head, for all he does during the day.
Every time we, or anybody, has sold an asset over the last
five years, or not bought one, its been a big, big mistake.
To state the obvious, the real estate bull market we have
predicted for years in this letter is now in full bloom. It
may be caused by low interest rates; it may be caused
by excess liquidity in the worldwide system; it may be
caused by a flight to hard assets; it may be technical
worldwide institutions are under-allocated in this asset
class; or it may even be for who knows what. And, it is a
bull market in the face of flattish rents. Some think it is a
bubble. I do not. Today, most p ro p e rties are bought and
sold in a modified auction process. Where there were 3 or
5 bidders years ago, there a re 20 or 30 today. The ru l e
used to be, the higher the price, the fewer the bidders.
To d a y, a billion dollar pro p e rty sells as easily as a $50 million
one. As the market marches on, each sale is higher than the
last one. Thats a bull market. Over the last several years,
real estate has repriced. I believe this is a long cycle
move. Get used to it; give or take 10%, these prices are
here to stay, for some time.
A short word about interest rates. While we must run the
business as if interest rates were going to rise, it seems
to me, there is an anchor holding them down. Thats just
an observation.Other Matters
Mike did it again. Starting in July 2004, we advanced
$114 million to GMH Communities Trust, Gary M.
H o l l o w a y s student and military housing business, in
a highly structured mezzanine-bridge loan with equity
features. Mike had partnered with Gary in a successful
student housing business during his Goldman Sachs days.
All went according to planGMH completed its IPO in
November, just three plus months after we made our
investment. This transaction resulted in an economic gain
to us of $62 million.
This year, we expect to finally end the Prime Group saga
when we sell our 4 million shares for $7.25 per share(16)
pursuant to an announced offer expected to close in third
q u a rter 2005. In the end, this investment, a mistake that
soiled our books and analysts re p o rts for years, ends up
with a loss of about $10 million.
Please see page 57 of our financial statements for
Deloitte & To u c h e s 404 Managements Internal Contro l
opinion, which Joe Macnow, our CFO, and his whole
financial team are justifiably proud of, and worked very
hard to achieve. Sarbanes-Oxley compliance is Joes night
job. Special thanks to Joe, our longest tenured division
head, for all he does during the day.
Every time we, or anybody, has sold an asset over the last
five years, or not bought one, its been a big, big mistake.
To state the obvious, the real estate bull market we have
predicted for years in this letter is now in full bloom. It
may be caused by low interest rates; it may be caused
by excess liquidity in the worldwide system; it may be
caused by a flight to hard assets; it may be technical
worldwide institutions are under-allocated in this asset
class; or it may even be for who knows what. And, it is a
bull market in the face of flattish rents. Some think it is a
bubble. I do not. Today, most p ro p e rties are bought and
sold in a modified auction process. Where there were 3 or
5 bidders years ago, there a re 20 or 30 today. The ru l e
used to be, the higher the price, the fewer the bidders.
To d a y, a billion dollar pro p e rty sells as easily as a $50 million
one. As the market marches on, each sale is higher than the
last one. Thats a bull market. Over the last several years,
real estate has repriced. I believe this is a long cycle
move. Get used to it; give or take 10%, these prices are
here to stay, for some time.
A short word about interest rates. While we must run the
business as if interest rates were going to rise, it seems
to me, there is an anchor holding them down. Thats just
an observation.Other Matters
Mike did it again. Starting in July 2004, we advanced
$114 million to GMH Communities Trust, Gary M.
H o l l o w a y s student and military housing business, in
a highly structured mezzanine-bridge loan with equity
features. Mike had partnered with Gary in a successful
student housing business during his Goldman Sachs days.
All went according to planGMH completed its IPO in
November, just three plus months after we made our
investment. This transaction resulted in an economic gain
to us of $62 million.
This year, we expect to finally end the Prime Group saga
when we sell our 4 million shares for $7.25 per share(16)
pursuant to an announced offer expected to close in third
q u a rter 2005. In the end, this investment, a mistake that
soiled our books and analysts re p o rts for years, ends up
with a loss of about $10 million.
Please see page 57 of our financial statements for
Deloitte & To u c h e s 404 Managements Internal Contro l
opinion, which Joe Macnow, our CFO, and his whole
financial team are justifiably proud of, and worked very
hard to achieve. Sarbanes-Oxley compliance is Joes night
job. Special thanks to Joe, our longest tenured division
head, for all he does during the day.
Every time we, or anybody, has sold an asset over the last
five years, or not bought one, its been a big, big mistake.
To state the obvious, the real estate bull market we have
predicted for years in this letter is now in full bloom. It
may be caused by low interest rates; it may be caused
by excess liquidity in the worldwide system; it may be
caused by a flight to hard assets; it may be technical
worldwide institutions are under-allocated in this asset
class; or it may even be for who knows what. And, it is a
bull market in the face of flattish rents. Some think it is a
bubble. I do not. Today, most p ro p e rties are bought and
sold in a modified auction process. Where there were 3 or
5 bidders years ago, there a re 20 or 30 today. The ru l e
used to be, the higher the price, the fewer the bidders.
To d a y, a billion dollar pro p e rty sells as easily as a $50 million
one. As the market marches on, each sale is higher than the
last one. Thats a bull market. Over the last several years,
real estate has repriced. I believe this is a long cycle
move. Get used to it; give or take 10%, these prices are
here to stay, for some time.
A short word about interest rates. While we must run the
business as if interest rates were going to rise, it seems
to me, there is an anchor holding them down. Thats just
an observation.
Corporate Govern a n c e
Stanley Simon was a director of Vornado when I got here
in 1980.(17) After 45 years of service and now at age 87,
Stanley will be retiring from our Board at the May Annual
Meeting. No director has been more loyal to a company,
more devoted to the goal of creating shareholder wealth,
nor has worshipped more the god of ethics. Stanley comes
to each Board meeting having read every word of every
document, resplendent in a bright, some say gaudy tie,
cigar-smoking and sharp as a tack. Mike is bemused by
the fact that Stanley always asks the single best question
at every Board meetingI am used to it by now. He also
asks the unaskable questions. He understands how
boards work and how public companies function. He
understood corporate governance 40 years before
Congress voted 522 to 3 for Sarbanes-Oxley. And he
knows how to take measured risks. We have learned a
lot from you, old friend. Stanley Simon will be the first
trustee emeritus in Vornados history.
The trustees who oversee Vo rnado and for whom Mike
work are smart, wise, committed and involved.
 understand our business intimately. They are huge
 whose interests are absolutely aligned with all
of  shareholders.
This year our Board is delighted to nominate two new
independent t rustees, Michael Lynne and Anthony W.
Deering. Michael Lynne is Co-Chairman and Co-Chief
Executive Officer of New Line Cinema Corporation and
Tony Deering was the  Chairman and Chief
Executive Officer of The Rouse Company. We welcome
these seasoned businessmen who will play an important
role in guiding our future.
Mike and I and the people who run Vornado are excited
about the future. We currently have in-house the 
to take Vo rnado to the next level. When we get the re n t
increases we think we will in the ton of fine real estate
we already own, get the PTO space back on the rent 
complete our pipeline of developments, find a good
acquisition or two (or ten) and sprinkle in a blockbuster
now and thenyou can do the math.
In each area of our business, be it 888, Paramus, 2345
Crystal Drive, the Mart, Atlanta, or wherever, Vornados
people worked hard and smart this yearand it showed.
Our people polish the Vornado brand everyday. Mike and
I appreciate and admire their efforts.

Steven Roth
Chairman and CEO
