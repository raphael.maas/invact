                      To Our Shareholders

AVALON DANBURY, CT




                     Accelerating fundamentals, abundant capital, "condo-mania" and rising valuations were a few of the
                     dominant trends shaping the real estate markets in 2005. Each of these trends offered opportunities
                     for value creation, and AvalonBay participated:
                       Our Same Store NOI growth accelerated throughout the year, topping 6 percent in the fourth quarter.
                       As investors struggled to allocate abundant capital, our $4 billion Development Pipeline gained
                       in importance.
                       We expanded planned asset sales by 300 percent, targeting condo converters willing to pay premium
                       prices over income buyers.


                        sector average of 12 percent.


                     to be an important performance measure



                     return of 23 percent was achieved while
                     maintaining modest average Leverage of
                     just 26 percent, well below the industry
                     average. Simply said, relative risk-adjusted
                     returns were outstanding. This is the third

                     returns, following 65 percent in 2004 and
                     30 percent in 2003.


                    


Access to desirable
amenities, like the 18-hole
golf course at Avalon
Pines, Enhances the Lives
of Our Residents.
                         



We've also achieved strong growth in other key metrics such as Earnings per Share (EPS), Funds
from Operations (FFO), Net Operating Income (NOI), and Net Asset Value (NAV).


strategy of deeply penetrating our chosen markets with a broad range of products and services and an intense focus
on our customer.


  The economy and its impact on the industry and our performance;
  The key attributes of our strategy that create value for shareholders; and
  How we are positioned for growth and continued outperformance.


2005 IN REVIEW




price appreciation, pushing the "rent-vs.-own" economics further in our favor. Job growth and low

industry continued at a brisk pace. Public-to-private M&A -- another prominent trend in 2005 --



spot" of the real estate cycle. As the spread between new development yields and dispositions
widened, we responded by expanding development activity and increasing dispositions, creating
value on both sides of this historically wide spread.




Condominium sales and conversion of rental to for-sale was "the"
story for most of the year. We sold into this strong market, selling



eight-year weighted average holding period. Economic Gains
totaled $185 million. With a substantial portion of these sales made

units from the rental market and improving the performance of
our remaining assets. The level of condo conversion activity may
impact the liquidity for asset sales, but we are not unduly reliant
on condo converters as a source of capital or sales.
Gains on asset sales and rising operating income drove
EPS to a record level of $4.21, an increase of 44 percent
over 2004. FFO was $3.77 per share, increasing 12 percent over
2004 and exceeding our original outlook. The principal reasons
for this outperformance were better than expected revenue
and operating results, opportunistic land sales and a sustained
low interest rate environment. Continued job growth, modest
new supply and a reduction in existing apartment inventory from
conversion activity further supported revenue growth. These
improving fundamentals and focused execution allowed us to
achieve Same Store revenue growth of 3.6 percent and, through
constrained expenses, NOI growth of 4.2 percent.

Anticipating these improving fundamentals, we increased development starts in 2005 to $880
million from $240 million in 2004, timing new apartment deliveries for what we expect to be a
robust leasing environment in 2006 and 2007.





such as that provided by
Avalon Juanita Village,
often represents the key
attraction for our young
professional residents.




LOOKING FORWARD--POSITIONED FOR GROWTH

With the backdrop of an expanding economy, continued job formation and revenue growth that
accelerated into 2006, we are optimistic about the next several years and are well positioned for

and visible, supported by:
  The economy, our markets and our product;
  A sector-leading development program;
  A well-positioned capital structure; and
  A "cycle-seasoned" management team.

These attributes position us for growth and are discussed further in the remainder of this letter.

Positioned for Growth -- the Economy, Our Markets and Our Product
Key economic forecasts suggest continued economic and employment growth. In addition, the
outlook for industry liquidity is favorable, with no signs that abundant access to capital--debt and
equity, public and private--is abating.
Forecasts for economic, employment and population
growth within our markets translate into continued
strengthening of demand and supply fundamentals, which
should help support the strongest rental revenue growth
since 2001.
The same forecasts that support improving apartment
fundamentals generally support all housing--including
the for-sale market. However, shifting demographics, the
gap in "rent vs. buy" economics and softening home sales
suggest that home ownership levels may have reached a
plateau, with the relative balance between for-sale and for-rent housing demand shifting back in favor
of rental housing. Given our market concentration, asset quality and product diversity, AvalonBay is




                                                                      
range of prospective residents and a portfolio that is highly occupied, we expect to see revenue
growth from our existing assets and from an accelerated pace of new development leasing. The
acceleration of revenue growth in late 2005 should continue into 2006 at levels of 5 to 6 percent--


Positioned for Growth-- a Sector-Leading Development Program
This positive outlook supports our operating, development and capital recycling programs. Our
Development Pipeline now stands at $4 billion, comprised of $1 billion under construction and
$3 billion in planning. This is the largest development program in the multifamily sector. With
an average land basis per unit of approximately $35,000 and access to attractive capital sources
to fund the pipeline, we expect future development activity to contribute to earnings growth for
years to come. Adding to the pipeline is challenging,
as competing for fully entitled land against for-sale
developers is just plain not economic. We responded
to this challenge with creative thinking while taking
measured risk, successfully adding a diverse set of
new opportunities:
  In the greater Boston area, we acquired a 300-acre
  site of a former state hospital, gaining entitlements



  architectural features of the structure.
  In New York City, we undertook a multi-million dollar environmental remediation effort to
  begin construction of a $170 million high-rise apartment community adjacent to our successful


  In the greater Washington DC market, we acquired several commercial properties that we intend

  while controlling the land and advancing the entitlement process.








Opportunities such as these are created by having an established local presence in each of our
supply-constrained markets. We have over a hundred experienced construction and development




escalation and scarce subcontractor resources, which add complexity and risk to new development.
These challenges require increased vigilance on our part to contain cost increases, and we have

insulate us from these pressures.
After more than twenty years in our markets, we have developed a unique infrastructure of
development, investment and property management skills. This translates into a competitive



value of our $9 billion of operating assets in place as well as land under control, further enhancing
our competitive advantage.
The value creation from our development activity is not captured in our balance sheet or earnings


Positioned for Growth--a Well-Positioned Capital Structure Prepared for External Growth
Our $4 billion Development Pipeline is an important source of future
earnings growth and value creation, and we believe our balance sheet
will continue to support that growth. With current Fixed Charge
Coverage and Leverage of 3.0x and 26 percent, respectively, we have



future earnings growth will not be "taxed" with

rise, providing cost-effective development capital and

the prospects for future earnings growth, our board
voted to increase the annual dividend by 10 percent,
from $2.84 to $3.12 per share effective March 2006.
This represents the largest percentage increase in the

Long-term growth and safety of the dividend is also
our focus. Since 1995, the growth in our recurring
dividend was twice the multifamily sector average. This was acheived while maintaining one of
the lowest payout ratios in the sector.


created through selective capital allocation to acquisitions. Accordingly, we further positioned for
growth through private equity, creating the AvalonBay Value Added Fund, L.P. This investment
management fund provides enough equity capital to purchase and leverage investments totaling
$900 million. This approach provides access to an alternative capital source, is consistent with our
desire to keep our common stock scarce and presents an opportunity to create value for private
investors. Investment management also provides an attractive and recurring income stream for our
management efforts.
A well-executed capital allocation strategy creates value. Access to multiple capital sources allows us
to direct public equity to more accretive development while directing private equity to acquisitions.
This helps ensure optimal value creation from our capital allocation efforts.




Positioned for Growth--a "Cycle-Seasoned" Management Team
The period from 2002-2004 that marked a severe downturn in operating fundamentals was

tested strategy remained intact. Convinced our strategy was sound, we positioned the company
for the upturn --installing new systems, streamlining processes, adjusting our development and
construction infrastructure to match current and prospective opportunities, and preparing the
balance sheet for the next up-cycle.
We weathered the storm well, with a stock price double its pre-downturn level, a positive outlook
from the rating agencies and a seasoned group of professional management that understands how



CONCLUSION

Our 2005 results are part of a recurring theme of
general outperformance over a sustained period of time,
as AvalonBay outperformed the sector in EPS, FFO per
Share, NAV per Share Growth and Total Shareholder



this 10-year period remain in place.
Today, apartment fundamentals are improving, with
third-party economic forecasts calling for continued
economic and employment growth. Many of our markets
are projected to outperform, with revenue growth in

rising, and future revenue growth is supported by the
high occupancy platform we currently enjoy. Leasing
activity at our development communities is strong.




We are positioned for growth.
returns over an extended period. We have a competitive advantage in our development program
in some of the strongest markets in the country. We have a high-quality and diverse offering of
apartment homes that will be in demand as economic growth continues. We have an integrated
real estate operating platform poised for additional value creation as we build out and add to our $4
billion pipeline. We have a well-positioned balance sheet that supports growth. Finally, we have a
"cycle-seasoned" management team that is committed to our strategy, but nimble enough to make
adjustments when opportunities arise. And although our strategy will evolve, it will not drift. We
remain committed to these core elements of our time-tested strategy:
  To further penetrate our supply-constrained markets that offer strong long-term fundamentals;
  To create value through pursuit of ground-up development of land for which we gain entitlements;
  To effectively allocate capital through targeted acquisition and disposition activity;




As always, I would like to thank our shareholders for their support, our associates for another year of
outstanding achievements and our residents for making an AvalonBay community their home.




   Bryce Blair
   Chairman and
