Dear Stockholders,
2010 was a truly remarkable
year with explosive growth
across our FOSSIL brand of
products and multi-brand
watch portfolio. Continued
product innovation, increased
marketing initiatives and
continuing momentum in
the overall watch market
culminated in sales exceeding
a new milestone of $2 billion.
FOSSIL brand product sales, combined across
all product lines and geographies, were up 24% for
fiscal year 2010 as well as the 4th quarter, while
our global multi-brand watch business grew at an
astounding rate of 40% for fiscal year 2010 and 42%
in the 4th quarter. Our growth was broad-based
across all geographies and brands underscoring the
significant strength of our business model and the
strong execution of our team. Our consistent ability
to lead the market with innovative use of materials
and designs continues to resonate with our customers
and allows us to compete in a full price manner,
strengthening brand value and profitability.
For our core FOSSIL brand, targeted marketing
initiatives including catalogs, internet advertising,
search and social media accelerated sales in our
wholesale, online and owned retail channels of
distribution. These initiatives drove Fossil same store
sales growth of 19% for the year, in addition to 50%
growth in ecommerce sales. We ended fiscal year
2010 with 364 stores, a net add of 10 doors from last
fiscal year, with all of the net pick up in store growth
taking place outside of the U.S. During fiscal 2011, it
is our intent to leverage FOSSILs brand strength and
owned store economics through a net increase in
store count of approximately 55 doors while also
launching several commerce-based websites outside
of the U.S. We are also exploring the opportunities
within our multi-brand portfolio to expand our watch
concession footprint, primarily in the Asia Pacific
market, in addition to leveraging our Watch Station
retail format.
The broad-based growth we achieved last year
opened increasing avenues of growth for our
company. Given the strength of the FOSSIL brand
combined with our resources and positioning in
the global watch market, we have a much larger
opportunity than we thought just a short time ago.
This is especially true in Asia, which we believe could
represent one-third of our sales over time versus
just 13% of sales currently. With FOSSIL and our
portfolio of fashion brand watches, we have a huge
runway ahead of us all over the world. Our focus
in fiscal 2011 will be to continue to capitalize on
all our opportunities while continuing to build our
capabilities to maximize the long-term growth of
the company.
Finally, we would like to thank our 10,500
employees worldwide as well as our customers,
suppliers and stockholders for their continued
support and dedication.
Sincerely,
kosta n. kartsotis
Chairman of the Board
and Chief Executive Officer