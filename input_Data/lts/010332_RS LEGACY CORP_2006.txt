Dear Shareholders:
The year 2006 was a year of mixed fortunes for our company.
I am sure that you are painfully aware that we posted an
unprecedented net loss for the first three quarters of the year.
In early July, the Board invited me to join RadioShack in the
joint capacities of Chief Executive Officer and Chairman of
the Board. It is in that capacity that I am proud to be writing
to you now.
Annual and Fourth Quarter Results Despite the net loss for
the first three quarters of the year, we were able to deliver
net income for the year of $73 million. Our financial results
for the fourth quarter  though by no means reflective of our
performance ambitions  were good enough to effect the
change in trend. Net income for the quarter was $85 million,
with operating income of $146 million for the quarter. Annual
operating income totaled $157 million. Our cash balance
stood at $472 million at the end of the year, an improvement
of 111% over the previous year-end total of $224 million.
I thought it might be helpful to describe below some of the
approaches supporting these results, which will serve as our
guideposts during 2007.
Sales and Gross Margin Adjusted comparable sales for the
fourth quarter were down 5.5% year over year, while margins
rose. There has been a fair amount of commentary that,
while the companys financial performance improved during
the fourth quarter of 2006, ultimately RadioShack must
focus on growing its sales base, particularly in the wireless
category. We would agree with that statement, but with one
important proviso. We believe that sales increases are good
when they are profitable and bad when they are not. We are
working hard to improve our margin management process at
RadioShack. If that results in negative same store sales for
a time, it should ultimately provide a strong base of profitable
sales on which to rebuild our companys franchise while
increasing the companys profitability.
Costs Active decision-making on how we spend our expense
dollars is a cornerstone of our approach. With the passage
of time, in many companies (and households for that matter)
a sort of autopilot approach takes over, in which cost allocation
entitlements from previous years are the driving force.
This was the case in our company. The process of moving from
an entitlement based program to one driven by ongoing,
active decision-making on costs has been challenging for all
of us over the past few months. We believe we have begun
to make progress and we will keep at it during 2007.
Growth in Cash Balances As I noted above, our year-end
cash balance increased by $248 million, composed of a free
cash flow1 improvement of $190 million together with $58
million from sales of assets and other financing activities.

As you can see, despite our improved profitability during
the fourth quarter, we produced less EBITDA in 2006 than
in 2005. Other factors enabled us to increase free cash
flow year over year. As indicated in the previous table, both
inventory levels and capital expenditures contributed to
the positive change. These improvements came as a result
of our increased level of scrutiny on decisions as to where
we should allocate capital resources. We now also subject
contract commitments to the same review process, viewing
long-term contracts in the same category as capital commitments.
We believe that increased cash on our balance sheet
gives us the flexibility to take advantage of opportunities as
they present themselves.
Our Brand One of the many things that drew me to joining
RadioShack last year was the strong heritage of our brand in
terms of product quality and innovation, convenience of our
store locations, and service delivered by our associates as
trusted advisors. This was a position we had earned over a
long period and from which we had recently drifted. We have
a lot of work before us to build on that base and, therefore,
are beginning to explore ways to enhance our product offerings
and strengthen our service culture as it is experienced in
our 6,000 stores (both company-owned and dealer locations)
across the United States. Brand building takes time and
requires that we acquire an in-depth understanding of our
customers wants and needs, both in a day-to-day and aspirational
sense. Through this understanding, we will develop
a more robust view of how best to communicate our brand
value to existing and potential customers, both in our stores
and through the various media choices available.
People The future of our company is dependent on the
quality of its management resources. We have many talented
people within the company whose performances can be
improved through targeted training. We also need to
strengthen our team by adding outstanding individuals from
outside our company to enable us to improve the quality of
our decision-making and execution.
I hope this note has been useful in communicating how
we plan to operate our company during 2007. Many of the
processes and initiatives I have described to you are in the
early stages and will require improvement during the year to
achieve their purpose. We believe that by focusing in these
areas we will be able to deliver improved results in 2007,
making headway towards delivering the level of performance
you have the right to expect of a well-run business.

Sincerely,
Julian C. Day
Chairman and Chief Executive Officer