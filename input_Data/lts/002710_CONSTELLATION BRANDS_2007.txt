                                                                                                                                 Letter to Shareholders




Greetings:

I am pleased to report that Constellation Brands earned record diluted           As we continue to travel the path of long-term growth, there will
reported earnings per share of $1.38, as well as record diluted            occasionally be detours or rough sections of roadway. In the short-
comparable earnings per share of $1.68 in fiscal 2007, even after          term, one of those has been Australia, where due to a combination
encountering some challenges during our year-long journey. It was          of circumstances including nature's bountiful grape harvests over the
an exciting year of change as we initiated refinements to our business     past few years, the wine surplus has temporarily rippled across the
in order to focus on Constellation's core beverage alcohol production      oceans and slowed our growth in the U.K. for reasons explained later
and marketing strengths. We sold our U.K. water business in addition       in this report. Additionally, a mutual desire by both Constellation and
to some non-core wine assets. We added Vincor International Inc.,          its U.S. distributor base led to a decision to lower wine inventories to
Canada's premier wine company to our business, and we announced            accommodate our customers' needs and the improved capabilities
the acquisition of SVEDKA, the fastest growing major imported              brought about, in part, by the consolidation of this tier. We view these
premium vodka in the U.S. We consummated the SVEDKA acquisi-               factors, and a few of lesser impact, that we reported on March 1, 2007,
tion in early fiscal 2008. During fiscal 2007 we entered into a joint      (fiscal 2008), as aberrations from our recalibrated business growth
venture agreement with Grupo Modelo to import, market and sell             model of high-single-digit to low-double-digit organic EPS growth.
premium beers into the entire U.S., including their superb, industry       However, we believe that over the mid- to long-term (3-5 years) we will
leading, Mexican beer portfolio. Additionally, we reorganized the          return to these growth rates off of the record high fiscal 2007 base.
structure of our wine business to maximize asset utilization, value              Our commitment to creating shareholder value remains at the top
creation and employ a more holistic go-to-market approach in the           of our priority list, and we will continue to leverage our decentralized
U.K. Early in fiscal 2008 we announced that our Matthew Clark              structure and entrepreneurial spirit to achieve that goal. Also near the
wholesale business in the U.K. had become part of a joint venture          top of our emphasis list is our focus on Corporate Social Responsibility
with that nation's largest pub owner, Punch Taverns. These changes         (CSR). To Constellation, CSR begins with our core values that date
were all implemented with goals of growing our business and creating       back to the founding of our company in 1945. Simply stated, we care
long-term shareholder value.                                               about the environment and we care about people. That won't change
      While Constellation Brands has been very active in beverage          because caring is part of our cultural fabric. We care about our
alcohol mergers and acquisitions in recent years, over the past year       shareholders, our customers, our suppliers, our business partners,
or so it has become abundantly clear that beverage alcohol                 our employees and our families, as well as about our communities
acquisitions in general, and spirits in particular, have become very       and what makes them strong. Our strength comes from the approxi-
pricey. Therefore, while there may be opportunities to fill out our        mately 9,200 dedicated people who will ensure that we continue to
beverage alcohol portfolios through smaller, strategic acquisitions,       do those things which made us into the company we are today.
there may not be as much need for large acquisitions as we go                    We also believe that strength emanates from diversity of points
forward. This is particularly relevant now that we have a well-developed   of view. Therefore, this year's annual report format is somewhat
wine portfolio with terrific breadth that covers all price segments. We    different than in past years. In addition to hearing from me, we believe
will always keep our eyes open for strategic acquisitions that further     you should hear from those who are closer to the day-to-day operation
our long-term growth objectives, while concurrently pursuing our           of the business. Rob Sands, Constellation Brands president and
European expansion strategy in 14 countries where our analytics tell       chief operating officer; Andy Berk, Constellation Beers and Spirits,
us there are growth opportunities for Constellation's portfolio.           chief executive officer; Jon Moramarco, chief executive officer of
      We also see great opportunities for base business growth for         Constellation International; and Jos Fernandez, chief executive
our portfolios, and to capture them we identified some evolutionary        officer of Constellation Wines U.S., will each provide insights about
trends in certain markets, such as the U.K., where per capita con-         our fiscal 2007 business in question and answer interview sections.
sumption of wine is higher than in most industrial nations. We know        We hope you'll agree that this format provides an interesting insight
U.K. consumers have been slower to trade up to better quality and          into the growing world of Constellation Brands.
higher-priced wines at as quick a pace as consumers have in some
of our other core markets  something we hope to change in the             Thank you for supporting our company, and our brands, in fiscal 2007.
near future. And while we have seen what we view as a temporary
slowing of growth for Australian wine in the U.K., wine from Australia     Sincerely,
is under developed and has considerable upside potential in countries
such as Canada. Indeed, there continue to be many remaining growth
opportunities to be harvested, and for us to pursue.

