A LETTER FROM TIM WENTWORTH
Shaping the future of healthcare

Our pharmacy technicians work to ensure people get the medicine they need.
It�s a pretty straightforward job, until it isn�t.
Last year, as Hurricane Irma was forecast to hit Florida, and most people were leaving
the area, the available couriers who normally deliver prescription drugs to our patients
could not cross the state line. Terrell Saddler works in our Norcross, GA facility
and he had a choice to make: Hope the courier company figured things out, or take
care of things himself. Terrell didn�t drive away from the storm; he went toward it,
driving south for hours, by himself, to hand-deliver medicine to two of our pulmonary
hypertension patients in Florida.
In challenging times, we are relentless, we are focused, and we do right for patients.
That�s our culture. That�s Terrell Saddler. That�s Express Scripts.
In 2017, our resolve was tested � by a healthcare environment fraught with friction
and fragmentation, by attacks on the value of our work, and even by extraordinary
weather events. In every instance, we were good stewards of your investments, making
decisions to put our company on a path of sustainable future growth while delivering
the patient care, client service and financial savings that have been our hallmark.
Preparing our business to win in the future.
Over the past 30 years, our business and our industry have evolved from straightforward
claims adjudication to providing complex care. The true value of what we do is not
based on whether we stand alone or as part of another healthcare business. Our value
is predicated upon delivering better care and outcomes to those we serve.
As we look ahead, we see many opportunities to partner � formally or informally � with
other companies across healthcare. Working with, and potentially combining with,
like-minded companies can expand our ability to deliver better care. As a consequential
healthcare leader, we are frequently approached by those who appreciate our singular
focus and the results it achieves.
To that end, on March 8, 2018, Cigna and Express Scripts announced a definitive
agreement whereby Cigna will acquire Express Scripts in a cash and stock transaction
valued at approximately $67 billion. We expect the transaction to be completed by
December 31, 2018. If approved, the combination would allow our two companies to
do even more to simplify healthcare, create more value and drive better affordability
and access to medicine.

We will always consider creative ways to put our
innovation to work for our clients and patients. If a
combination, joint venture or partnership enables us
to deliver smarter pharmacy solutions, we have the
flexibility and willingness to pursue it so clients win,
patients have better care and we grow.
To build our future, we must make tough decisions
every day.
In April 2017, after careful consideration, and
months of negotiations, we understood that Anthem �
our largest client � planned to move its business
at the end of our contract which runs through
2019. While we were disappointed in Anthem�s
decision, we found ourselves with an opportunity
to disclose the contribution of its business to our
financial performance which, in turn, highlighted
the underlying strength of our core business.
Express Scripts has never been built around one
client. Our book of business is strong and sustainable.
By disclosing Anthem�s contribution, we removed a
potential distraction to our company and provided
greater visibility to our shareholders. Further,
we refocused our efforts on shaping our future
growth � a future defined by leveraging more of our
core competency, pharmacy benefit management,
while adding capabilities and broadening our
reach within the nation�s healthcare industry.
Pharmacy is the gateway to better health.
Prescription medicine is often the first-line
treatment for the most serious diseases. We
provide pharmacy benefit management for more
than 80 million people, filling more than 1 billion
adjusted prescriptions annually.1 This gives us an
outsized opportunity to improve healthcare.
By providing certainty around cost and value, we free
up resources that enable businesses to better
compete in a global economy and our health plan
clients to win in their respective markets. By managing
integrated care in Medicare, Medicaid and other
health programs, we help federal, state and local
governments make the most of their budgets
and free up funds to support their priorities.
Clients choose us because they recognize our model
is explicitly built around the patient. And when
patients and pharmacy excellence are your focus,
your team is better empowered to do incredible
work. Whether as a single employee, or as a
company, we uniquely put medicine within reach.
Nowhere is the value of our work
more apparent than in the drug
trend we manage for our clients.
This past year, we held the overall rate of growth in
prescription drug spending to 1.5% for our commercial
plans � the lowest increase Express Scripts has
measured in 25 years. The vast majority of our
patients saw out-of-pocket costs hold steady at about
14% of total prescription drug cost. The average
patient copay increased by just 12 cents. Medicare
and Medicaid trend came in slightly higher, while
health exchanges actually registered negative trends.
Significantly, 44% of our clients spent less per
person on prescription drugs in 2017 than in
2016 � a negative drug trend. These results stem
from our clients� willingness to develop and implement
solutions that reduce costs while preserving access
and enhancing quality.
Still, some patients wrestle with high drug costs.
We expanded our leadership solutions by launching
Inside RxSM to help those who are uninsured,
underinsured or have high out-of-pocket costs.
This drug-discount program helps the estimated
30 million Americans who pay full price for their
prescription drugs. Through our purchasing power,
we negotiate rebates and expand affordable access to
brand and generic drugs at the point of sale to patients
in need. Inside Rx has more than 100 commonly used
medications in its program. People using Inside Rx
can now save, on average, 40% on their prescriptions
at nearly 40,000 retail pharmacies nationwide.
We are now managing a patient�s entire journey.
As strong as we are in pharmacy, most healthcare
costs are borne on the medical side. In late 2017,
we finalized our $3.6 billion acquisition of eviCore

healthcare, the nation�s leading medical benefits
management company. Together, our superior platform
and clinical expertise help manage a patient�s journey
from pre-diagnosis through treatment and cure.
Our companies will make value-based care a
reality. Integrating pharmacy and medical data to
modernize utilization management through broader
connections and intelligent systems eliminates friction
and fragmentation for providers and patients.
A complex system can have terrible health
consequences. Consider our country�s opioid crisis.
For years, Express Scripts led the way through our Fraud,
Waste and Abuse program; advocacy for pharmacy and
physician lock-in; and support for prescription drug
monitoring programs. We have a unique ability to rally
disparate parts of the healthcare ecosystem around
a common goal to help stop abuse before it starts.
Developed in partnership with our clients, our
Advanced Opioid ManagementSM program is the first
comprehensive solution for opioid abuse. In the first 90
days after the solution launched, we observed a nearly
60% reduction in the average days� supply for patients
receiving an opioid prescription for the first time � from
18.6 days� supply per claim before launch, to 7.5
days� supply per claim after the start of the program.
Our leadership helps families.
I will never forget a mother who carried a framed
photograph of her daughter to an event we hosted.
Nicky was a young woman whose bright future
ended too soon. Nicky�s opioid abuse descended
into heroin abuse and, ultimately, caused her death.
Her mother�s story motivates us to be relentless
every day, so fewer families suffer such tragedy.
We also help those who struggle with complex
diseases like cancer, HIV, mental illness and other
conditions that require a special focus and care
model. Our specialty pharmacy, Accredo�, leads
the way in patient care. Accredo employs over 500
nurses nationwide who help bring together a patient�s
pharmacy, medical and home-based services to drive
better outcomes. We close millions of gaps in care
annually because we surround patients with quality
care, apply technology to improve decision making
by healthcare professionals, and make the use of
specialty medicines more affordable and accessible.
One definitive sign of our value
is the tremendous interest
health plans have shown in
exclusive specialty pharmacy
network arrangements.
Accredo�s superior clinical value combined with
cost efficiencies is a key indicator of future growth
opportunities for Express Scripts. Spending on
specialty drugs, which accounted for 41% of total
spending under the pharmacy benefit, increased 11%
in 2017, the lowest increase we have ever recorded.
This spend was driven by 8% higher utilization and
a 3% increase in average unit cost, driven lower
through the combined clinical success of Accredo
and our Express Scripts SafeGuardRx� value-based
suite of solutions. Our innovative SafeGuardRx
programs address conditions such as cancer, diabetes,
inflammatory conditions and multiple sclerosis.
In the past year, our clinical
programs alone returned $32
billion in savings to our clients,
excluding any value derived from
rebates and retail discounts.
Our strategies ensure patients take the most
appropriate medication, from a clinical and economic
standpoint, to generate better value. For example,
we enrolled approximately 8 million patients in our
Inflammatory Conditions Care Value ProgramSM in
2017 and drove a 13% increase in adherence and a
40% reduction in costs for those enrolled. Our clients
see the improved care and greater financial value.
That is why we now have more than 20 million people
enrolled in this single program.
Strong financial results follow the care we provide
patients, our singular alignment with clients, and our
focus on driving value across the entire spectrum of care.

Our 2017 GAAP earnings per diluted share grew 44%,
driven by an 8% increase in Operating Income and
the deferred tax implications of federal tax reform
enacted in December 2017. On an adjusted basis,
we generated $7.10 of consolidated adjusted earnings
per diluted share in 2017, which represents growth
of 11% versus 2016.2
As a result of delivering on expectations, driving
strong financial performance and bringing a laser
focus to our core business, our share price has
improved markedly. This recognizes our long-term
value and our willingness to stand with patients
and payers.
We are focused on the future.
As we look ahead, we plan to invest $600 million
to $650 million to improve the patient, client,
physician and employee experience. We expect
the related changes, which make us even more
flexible and adaptable, will better connect services,
enhance accountability and generate future growth.
By investing in our company, you are investing in our
employees. They deserve the opportunity to perform
at their best because, when they do, incredibly good
things happen for the people we serve. Whether
setting up a mobile pharmacy during a natural
disaster, building an easier online experience for
a patient, or delivering medicine to a deployed
soldier, our 27,000 people are just like Terrell
Saddler: relentless, focused and all in for patients.
Beyond the financial return on your investment,
you are allowing us to unleash innovation, creativity
and compassionate, quality patient care at greater
levels than ever before, so we can make medicine
safer, more affordable and more accessible.
Thank you for believing in Express Scripts.
Sincerely,
Tim Wentworth
President and Chief Executive Officer