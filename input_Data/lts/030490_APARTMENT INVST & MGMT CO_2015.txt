March 7, 2016
My Fellow Shareholders,
On behalf of Apartment Investment and Management Company and the entire Aimco team, I am pleased to provide to you our
2015 10-K, our Corporate Citizenship Report, and the Proxy Statement for the Annual Meeting of Shareholders to be held in
Denver on April 26, 2016.
2015 was a solid year for Aimco and its shareholders. We continue to make good progress in our five areas of strategic focus:
Property Operations; Redevelopment and Development; Portfolio Management; Balance Sheet; and Culture. I am pleased to
share with you the following highlights:
1. Property Operations:
During 2015, Keith Kimmel, head of Property Operations, and his team in the field led by Kevin Mosher and Didi
Meredith, focused on increasing revenue and controlling expenses, all while improving customer satisfaction and retention.
In our conventional same store portfolio, revenue was up 4.5% year-over-year, expenses were up 2.1%, and Net Operating
Income (�NOI�) was up 5.6%. Customer satisfaction scores increased from 4.10 in 2014 to 4.15 in 2015. High customer
satisfaction led to low turnover (48.6%) and average renewal rent increases of 5.5%.
In 2016, we plan for more of the same: higher rents earned by increased customer satisfaction scores; continued cost
discipline; plus, additional investment focused on site team productivity.
2. Redevelopment and Development:
In Redevelopment, we had a productive and successful year under the leadership of Patti Fielding, head of
Redevelopment and Construction Services, and her able lieutenants, including Wes Powell, Steve Cordes and Mike
Englhard. In 2015, we invested $118 million in six redevelopment communities with more than 2,500 apartment homes.
In California, we completed multi-year redevelopments at Lincoln Place in Venice, Preserve at Marin in Corte Madera,
and Ocean House on Prospect in La Jolla. We also completed construction at 2900 on First in Seattle. Strong consumer
demand for our redeveloped apartment homes drove the lease-up of Ocean House in La Jolla and second generation rent
increases averaging 13% at Lincoln Place, Preserve at Marin and Pacific Bay Vistas.
In 2015, we continued phased redevelopment of two Center City Philadelphia properties, Park Towne Place and The
Sterling. At Park Towne Place, our teams largely completed construction activity in the South Tower and in the Town
Center, an approximately 35,000 square foot amenity and commercial building at the heart of the community. At the end
of February 2016, 84% of the completed apartment homes in the South Tower and 87% of the completed apartment homes
at The Sterling were leased at rental rates in excess of our underwriting.
John Bezzant, our Chief Investment Officer, also leads our development activities, and invested $116 million during
2015: about $100 million at One Canal in Boston and the balance at Vivo in Cambridge. One Canal will be completed
in April and we are already leasing apartment homes. We acquired Vivo mid-year while it was under construction. It
was largely completed by October and was 53% leased at the end of February. Here too, the rents achieved exceed our
underwriting.
In 2016, we plan to invest between $180 and $220 million in redevelopment and development, as well as $75 million
in other property upgrades.
3. Portfolio Management:
Thanks to the hard work of John and his transaction team, our portfolio gets better and better. In our conventional
portfolio, monthly revenues per apartment home were up 10% year-over year to $1,840. This rate of growth reflects the
impact of market rent growth, and more significantly, the impact of portfolio management activities. During 2015, we sold
eight of our lowest-rated conventional apartment communities with about 3,600 apartment homes, for proceeds to Aimco
of $377 million. These communities had average revenues per apartment home of $1,043, 43% below the average revenues
of our retained portfolio. The proceeds from these sales and the sale of three affordable communities were reinvested in
redevelopment and development, property upgrades, and acquisitions.
During 2015, John acquired for $129 million three apartment communities with a total of 300 apartment homes:
Mezzo in Atlanta, Georgia; and Axiom and Vivo in Kendall Square in Cambridge, Massachusetts. During 2015, Aimco
also agreed to acquire for $320 million an apartment community under construction in Northern California. We expect
to acquire this property upon its completion this summer and we have already begun its leasing. We expect to fund the
acquisition in part through a property loan and the balance with proceeds from the sale of two communities: one in
Phoenix, already sold, and one in Alexandria, Virginia.
In 2016, we plan to continue to upgrade our portfolio by making �paired trades� to sell our lowest-rated apartment
communities to invest the proceeds in redevelopment and property upgrades. With these activities and expected market
rent growth, we forecast average revenues per apartment home up 6% in 2016 to approximately $1,950 by year-end.
4. Balance Sheet:
We enjoy a safe balance sheet. Paul Beldin, our Chief Financial Officer, and Patti Fielding share responsibilities for
capital markets activities. Thanks to their efforts, the quality of our balance sheet was confirmed during 2015 by investment
grade ratings from two rating agencies.
We enjoy ample liquidity, with $675 million of cash, restricted cash, and availability on our bank lines. Our pool
of unencumbered apartment communities was valued at more than $1.8 billion at year-end, providing us additional
financial flexibility.
Our leverage, as measured by the ratio of Debt plus Preferred Equity to EBITDA was 6.8x at the end of 2015, 11%
lower than in 2014.
We expect this ratio to continue its decline in the next few years as we hold leverage roughly constant in amount while
EBITDA increases due to greater contribution from operations, especially from the lease up of One Canal in Boston and
the property to be acquired in Northern California.
5. Culture:
Our culture and our team are the key to our success. We emphasize a collaborative, respectful, and performanceoriented culture that enables the continuing transformation of the Aimco business, all while maintaining the high morale and
team engagement that led the Denver Post to recognize Aimco as a Top Place to Work in Colorado for a third consecutive
year. Culture is a priority for every teammate and Jennifer Johnson, head of human resources, is our leader.
�Giving back� is also integral to the Aimco culture. Miles Cortez and Patti Shwayder lead Aimco Cares, a program
by which Aimco teammates contributed more than 6,000 hours of voluntary services to 89 worthy organizations across
the nation.
Shareholder accountability is yet another Aimco value. Throughout 2015, more than two-dozen members of
management met with investors on numerous occasions, including at our Investor Day in Philadelphia. Lisa Cohn, our
General Counsel and Chairman of our Investment Committee, also meets regularly with our largest shareholders holding
about two-thirds of our shares to review such governance matters as executive pay and proxy access.
As we begin 2016, we plan to build upon our 2015 results and continue toward our goal to be �the best owner and operator of
apartment communities, inspired by a talented team committed to exceptional customer service, strong financial performance
and outstanding corporate citizenship.�
In all that we do, we are guided by the wise counsel of our independent directors, each well qualified and highly engaged. With
their help and that of all my teammates, this is a wonderful time to work at Aimco!
On behalf of my colleagues on the Aimco Board of Directors � Jim Bailey, Tom Keltner, Lanny Martin, Bob Miller, Kathleen
Nelson, Mike Stein, and the newly-elected Nina Tran � and the entire Aimco team, I thank you for your investment. We are
working hard to make it more valuable.
Sincerely,
Terry Considine
Chairman and CEO