Letter to Stockholders
Our consistent strategic focus served
Baker Hughes well in 2002. Market conditions
during the year proved to be even
more difficult than anticipated, as the economic
recovery was disappointing, and
exploration and development activity did
not rebound as expected. By maintaining
focus on our ongoing strategies, Baker
Hughes achieved performance differentially
better than our competitors. While I
expect 2003 to bring an improved market,
the magnitude and timing of the improvement
remains uncertain. We will continue
to exploit genuine opportunities that exist
for Baker Hughes in any market.
Our financial condition continued to
improve in 2002, as we repaid debt and
accumulated cash. In a year when the
average worldwide rig count was down
19%, Baker Hughes revenues declined
by only 2% from the prior year. We also
achieved oilfield margins of 14.9% for
the year, indicative of our product line
focus, cost management, and market
share strength.
Operating profit after tax was
$309.3 million in 2002, down 35% from
$472.7 million, excluding goodwill amortization
in 2001. Total revenues were down
2% for the year at $5,020.4 million, compared
to $5,139.6 million in 2001. Oilfield
revenue was $4,901.5 million in 2002,
down 2% from $5,001.9 million in 2001.
Process revenues were $118.9 million in
2002, down 14% from $137.7 million
in 2001.
Baker Hughes Incorporated recorded
pre-tax charges of approximately $90 million
in the fourth quarter of 2002, as
a result of our seismic joint venture
WesternGecos write-down in the value
of its multiclient library, closure of its landbased
seismic operations in the U.S. lower
48 states and Canada, and reduction of its
marine seismic fleet. We continue to own
30% of this leader in the seismic industry.
Focus on strategies For the past two
and a half years, Baker Hughes has concentrated
on executing a few basic strategies,
which kept us on course in 2002 and
will continue to set the direction for 2003
and beyond.
Oilfield Focus To sustain a leading position
in the industry we know best, Baker
Hughes concentrates on the oilfield services
business and on products and services
for oil and gas wells.
Baker Hughes increased its oilfield
focus in November 2002 by successfully
completing the sale of EIMCO Process
Equipment to Groupe Laperriere &
Verreault, Inc., of Montreal, Canada for
$48.9 million, subject to post-closing
adjustments. With this divestiture, and
the 2001 sale of the majority interest
of our process-related production and
refining process business, Bird Machine is
Baker Hughes only remaining non-oilfield
operating unit.

This Annual Report to Stockholders, including the letter to stockholders from Chairman Michael E. Wiley,
contains forward-looking statements within the meaning of Section 27A of the Securities Act of 1933, as amended,
and Section 21E of the Securities Exchange Act of 1934, as amended. The words will, expect, should, scheduled,
plan, believe, promise, anticipate, could and similar expressions are intended to identify forwardlooking
statements. Baker Hughes expectations regarding these matters are only its forecasts. These forecasts
may be substantially different from actual results, which are affected by many factors, including those listed
in Managements Discussion and Analysis of Financial Condition and Results of Operations contained in Item 7
of the Annual Report on Form 10-K of Baker Hughes Incorporated for its year ended December 31, 2002. The use
of Baker Hughes, our, we and similar terms are not intended to describe or imply particular corporate organizations
or relationships.
Indonesia
Baker Atlas Reservoir
Characterization InstrumentSM
(RCI) service
enables oil companies
to acquire formation
fluid samples and
reduce the need for
expensive well testing.
The RCI service and
other new technologies
have gained Baker Atlas
a major share of the
deepwater wireline
logging market.

Baker Hughes oilfield operations consist
of six strong product line divisions that
focus on key areas of the oilfield service
business. With this organization, our divisional
engineering and manufacturing
teams can focus on developing and providing
best-in-class products, and our field
operations managers can focus on delivering
best-in-class services to their customers.
We believe this focus is a competitive
advantage that enables us to outperform
competitors on a product-line level and
helps us to be leaders in almost every
product line.
While our operating divisions concentrate
on specific product lines, key
resources, such as capital and technology
funding, are allocated from a corporate
perspective.
Baker Hughes makes technology
investments using our Technology Road
Map (TRM) process. The TRM assesses
each divisions product portfolio and
technology programs to assure they are
aligned with current and future client
needs and the companys long-range strategy,
and identifies research projects to
receive corporate funding. Our divisions
also use a rigorous Product Development
Management process to make sure that
each new product not only meets a customer
need, but also has the potential to
earn an adequate return on investment.
High Performance Culture During 2002,
Baker Hughes continued developing a high
performance culture based on our Core
Values of Integrity, Teamwork, Performance
and Learning, as well as our Keys to Success.
Applying these principles, employees
know that each person can make a difference
in creating Baker Hughes future.
In 2002, Baker Oil Tools
deployed its first
InForce Intelligent Well
System (IWS) in the Gulf
of Mexico and incorporated
an IWS completion
in an Indonesian
multilateral well. IWS
technology enables oil
companies to remotely
monitor conditions in
a producing well and
control flow to correct
problems without
costly interventions.

I have made it clear to our executives,
managers and employees, that unquestioned
Integrity is the foundation for our
individual and corporate actions. Honesty,
openness and compliance are required in
all our business dealings. Through ongoing
programs for legal education and financial
controls, BHI employees worldwide have
a more thorough understanding of the
Baker Hughes Business Code of Conduct,
the requirements of our Foreign Corrupt
Practices Act policy, and our system of
internal controls.
In October 2002, Standard and Poors
ranked Baker Hughes as one of the three
best S&P 500 companies in terms of overall
disclosure of financial information in
our annual report and proxy statement.
To support the companys
Performance, executives and managers
throughout the enterprise have aligned
their priorities with company objectives
through their annual Performance
Contracts and incentive pay programs.
At the field operations level, all of our
divisions have initiated flawless execution
programs in various forms. Our customers
choose our products for the most demanding
applications and expect them to perform
because our brand is built on performance
At onshore data
centers in Houston
and Stavanger, Norway,
INTEQ reservoir navigation
experts can control
offshore operations
to guide AutoTrak
drilling/LWD assemblies
to achieve optimum
well placement within
the reservoir. Customer
engineers can monitor
the same real-time data
sets simultaneously.
Norway

and reliability. From my experience as a
customer and from talking to our customers
today, I know that reliability and
performance are key to their selection
of contractors.
Our enterprise-wide commitment to
Health, Safety and the Environment (HS&E)
recognizes and rewards outstanding safety
performances, and our HS&E record has
steadily improved. Over the past five years
our total recordable incident rate per
200,000 manhours, a common HS&E
metric, has fallen 74%.
In 2002, Baker Hughes launched a
Measure and Compare marketing campaign
that encourages customers to compare
the performance gained from using
our products and services to results delivered
by our competitors. We are confident
that we will gain market share as more
customers measure and compare.
Baker Hughes, as never before, is gaining
leverage from Teamwork across divisions.
An initiative sponsored by operations
vice presidents from all six divisions has
established regional teams in seven major
regional markets around the world. As a
result, we are realizing the power of sharing
technology, sales strategies, infrastructure,
and human resources across business
units. Hughes Christensen and INTEQ team
together to offer OASIS high performance
drilling services, and Baker Atlas and Baker
Petrolite have joined forces in our Pipeline
Management Group.
Learning is the Core Value that helps
us grow as individuals and as a company. In
2002, we increased both our non-technical
and technical training programs. More than
1,000 training courses were conducted at
the Baker Hughes Education Center during
the year, and instructor-led courses have
been supplemented by many online training
programs. Our emphasis on training
and personal development have better
prepared our employees to provide superior
customer service in the future.
New Technology We maintain high levels
of relative performance by developing new
technology. In 2002, every division introduced
new, best-in-class technologies
that save customers time, increase their
reserves, increase productivity, and/or
improve their HS&E performance.
INTEQ leveraged more than 6.5 million
feet of drilling experience with the Auto-
Trak rotary steerable system to introduce
the third-generation AutoTrak G3 system
with more advanced measurement-whiledrilling
components and improved reliability.
These features enable its deployment to
more remote areas. Also in 2002, INTEQ
introduced its improved APLS Elite porosity
logging module and the new APX
acoustic logging system. These new
systems combine with the AutoTrak G3
tool to form an integrated, steerable
quad-combo drilling and formation
evaluation assembly.
Also during the year, INTEQ improved
customer drilling efficiency by introducing
high performance Navi-Drill X-treme
drilling motors, which combined with
Hughes Christensen bits to outperform all
competitors off Canadas East Coast and
in the Middle East. INTEQ also brought to
market a smaller version of its VertiTrak
straight hole drilling system and set performance
records while drilling deep gas
wells in the foothills of Western Canada.
With the acquisition of Apollo Services,
INTEQ has launched a new Fluids
Environmental Service to address customer
needs for drilling mud systems and waste
management services that comply with
environmental regulations. This new service,
along with added completion fluids
offerings, makes INTEQ a full-service
drilling fluids company.
Worldwide success of the Hughes
Christensen Genesis PDC diamond bit

In 2002, Baker Hughes
contributed to the success
of deepwater projects
in the Gulf of
Mexico, Brazil, West
Africa, and offshore
Canada. Advanced
drilling systems, environmentally
friendly
drilling fluids, efficient
formation evaluation
services, premium completions
systems and
leading flow assurance
programs from Baker
Hughes enabled oil
companies to find
and develop deepwater
reserves.
Gulf of Mexico

line has helped the division regain its leadership
in the diamond drill bit market. The
Genesis process combines new technical
features with a team-based design process
to create a customized drill bit for each
application. Genesis bits have gained an
average performance improvement of
50% over competitive bits.
In addition, the new Hedgehog drill
bit has greatly improved drilling performance
in harder formations, such as those
in Algeria and Oman, expanding the market
for impregnated diamond technology
and commanding premium prices.
Baker Atlas has built on earlier successes
with its Reservoir Characterization
InstrumentSM formation fluid sampling
service, adding Sample ViewSM downhole
analysis and single-phase tanks to develop
a superior, differentiated service.
Baker Atlas also continued the introduction
of its 3DExplorerSM resistivity service
for evaluating laminated pay zones to find
easily bypassed reserves. The division also
began pre-commercial operation of its new
EARTHImagerSM service that provides wellbore
images in oil-based drilling muds.
Baker Oil Tools continued its leadership
in completions with several important
innovations. The division introduced
expandable completions systems  including
durable sand control screens, expandable
packers, and expandable liner hangers 
on three wells offshore Indonesia.
Baker Oil Tools also installed its first
InForce Intelligent Well System in the Gulf
of Mexico, and provided an InForce system
as part of a multilateral completion in an
Indonesian well. Other multilateral well
advancements included HookHanger
completions in China, Siberia, Alaska,
and the Middle East, and the introduction
of FORMation Junction technology to
new markets in West Africa, Venezuela,
and Saudi Arabia.
Baker Oil Tools also created value for
deepwater operators with High Pressure-
High Temperature (HPHT) completions,
Neptune subsurface safety valves,
and remotely operated disappearing
plug technology.
Baker Petrolite continued its leadership
in deepwater flow assurance with its
FATHOM program of chemical solutions
to control corrosion, asphaltene, paraffin,
and hydrates in deepwater wells and
flow lines. The new HI-M-PACT antiagglomerate
hydrate inhibitor delivered
operating cost savings and increased
production on a number of deepwater
projects in the Gulf of Mexico.
Centrilift advanced rotating artificial
lift technology by installing more than
100 LIFTEQ electric submersible progressive
cavity pumps. The division also
extended the application range of ESP
systems to replace gas lift completions on
offshore wells, to provide long-life completions
on subsea wells, and to prevent water
coning in onshore wells in Venezuela.
Other Centrilift innovations include a new
variable speed controller, downhole pump
monitoring sensors, and improved systems
for dewatering coalbed methane.
Growth Opportunities In addition to
introducing new products, Baker Hughes
is growing by entering new markets. For
example, in 2002, Baker Hughes signed
a significant agreement with the Russian
oil company, Sibneft OA, to provide a full
range of services in Western Siberia and
establish operations bases there with the
capability of supporting multiple projects.
Previously, Baker Hughes had limited its
participation in the Russian market to
product sales.
Baker Petrolite is building on its
strength as a specialty chemicals leader
in North America to increase its international
business. The division also launched
an internal performance program called
Pride North America

In Canada, INTEQ field
service personnel prepare
a VertiTrak drilling
system for operation on
a deep gas well. Developed
with ENI-Agip
S.p.A., the automated
system keeps wells vertical
in difficult formations,
improves drilling
efficiency, and reduces
operating costs. Verti-
Trak systems were
deployed successfully
in Canada, Russia,
Italy, Argentina, and
the United States
during 2002.
EXCELerate to improve profitability by creating
and capturing value, and differentiating
its specialty chemical offering through
engineered solutions and services.
Baker Hughes has targeted the critical
well market, which is characterized by
deep water, extreme well depths, high
operating costs, and challenging environments.
This is a growing market segment as
operators look for large discoveries. All of
our divisions are well positioned to exploit
opportunities presented by critical wells.
For example, Baker Atlas has increased
its market share in deepwater fields
through continued technology investments
in differentiating, higher-end tools. Their
technology and excellent wellsite service
have gained growing acceptance from
super-majors who operate in deep water.
Baker Atlas market share in the deepwater
Gulf of Mexico has grown from almost
nothing in 1998 to 30% in 2002. Successes
on projects in West Africa and Brazil
have added to this momentum.
Baker Hughes also is capitalizing on
growth opportunities by combining and
leveraging internal resources into new
products or services. The new Pipeline
Management Group is one example. It
builds on Baker Atlas experience in performing
tethered pipeline inspections and
Baker Petrolites expertise at assessing
pipeline system integrity and controlling
corrosion with chemical programs. The
timing is right for this business as new
government regulations are requiring
that thousands of miles of pipelines in
the United States be inspected to verify
their integrity.
Greg Nakanishi, Vice
President, Human
Resources; Steve Finley,
Senior Vice President and
Chief Financial Officer;
Alan R. Crain, Jr., Vice
President and General
Counsel; Michael E. Wiley,
Chairman, President and
Chief Executive Officer;
and Andy Szescila, Senior
Vice President and Chief
Operating Officer.

Financial Flexibility and Discipline
Over the past two years, Baker Hughes has
worked hard to restore financial discipline
and flexibility. We have reduced debt by
$1.4 billion from peak levels in 2000. Our
debt to total capitalization ratio is now
31%. With such relatively low debt levels,
Baker Hughes has the flexibility to pursue
various strategic options, including internal
and external investment opportunities. In
September 2002, the Board of Directors
authorized a program to repurchase up
to $275 million worth of company shares.
During the third and fourth quarter, we
used approximately $49.1 million of this
authorized amount to purchase and retire
1.8 million shares.
Financial discipline is part of the Baker
Hughes culture throughout the organization.
We conduct quarterly performance
reviews with each division to discuss
financial results, the market environment,
product development, internal controls
and pricing. We are especially determined
to exercise pricing discipline so that we
receive fair compensation for our products
and services in keeping with the value
provided to our customers.
We also exercise financial discipline
when allocating capital. Major projects
are consolidated and ranked according to
each projects merits and the performance
of the sponsoring division. Investments
at Baker Hughes target growth areas
that promise required returns. Our capital
expenditures, about equal to depreciation
in 2002, have primarily been devoted
to rental tools and manufacturing efficiency
improvements.
Outlook The year 2003 holds many of
the same uncertainties of 2002. Doubts
about the economy and the potential for
war in the Middle East have impacted
exploration and production spending decisions.
We expect North American drilling
in 2003 to increase 1015% compared to
2002, as gas drilling resumes. Depending
on world events, international activity in
2003 could be up as much as 5% compared
to 2002. Overall, we expect worldwide
exploration and production spending
in 2003 to increase about 46% from
2002 levels.
Regardless of market conditions,
Baker Hughes will continue to set aggressive
goals and strive to be the premier
oilfield service company. We will act with
integrity, maintain our strategic focus,
introduce new technology, deliver reliable
performance, and strive for fair prices,
while enhancing our ability to service
our customers.
Finally, I would like to thank our
stockholders for their confidence in Baker
Hughes as an investment, our customers
for their ongoing business, and our
employees for the dedication to performance
that held our course steady through
a difficult year.
Michael E. Wiley
Chairman, President and CEO