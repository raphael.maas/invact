A MESSAGE FROM OUR CEO
Gilead marked its 30th anniversary in 2017. As I refl	on the company's history, it's clear we've had an incredible journey from the very beginning, and in 2017, we saw remarkable achievements in our mission of providing treatments and cures for people living with some of the world's most serious diseases.
We've grown from a small Northern California company to an organization with a portfolio of more than 25 products and having a worldwide presence with offices in more than 35 countries. Even though we've grown significantly, we are united by our values, and by the pursuit of innovation for one purpose: to help people with life- threatening illnesses around the world.

Delivering innovative medicines is possible because of the hard work of our highly dedicated employees, who are not afraid to tackle difficult scientific problems or to challenge assumptions to combat the complexities of diseases like HIV and chronic hepatitis C virus (HCV). We started working on HIV treatments in the late 1980s, when HIV/AIDS was seen as a death sentence that disproportionately impacted marginalized communities. Today, our medicines are helping people diagnosed with HIV enjoy healthier lives - and a significant percentage of those individuals are over 50, a statistic that seemed impossible 30 years ago. In addition to working relentlessly to develop numerous therapies that improve the lives of people with HIV, we've worked to raise awareness of how HIV intersects with social determinants of health, and understand that to have an impact on the epidemic, we need to address more than just the disease.

In 2017, we've remained committed to our belief that even the most difficult and complex illnesses can be treated, or even cured. With that strong commitment, we will continue to use our expertise to advance scientific innovation and position our business to deliver strong results.


Scientific Innovation and Transformative Therapies
This year we expanded our work in oncology with the acquisition of Kite Pharma, Inc., moving us into a leadership position in the exciting field of cell therapy, an approach to fighting cancer that involves reprogramming a patient's own immune cells to fight the disease.

Just two weeks after the Kite acquisition closed, the FDA approved Yescarta(r), the first chimeric antigen receptor T (CAR T) cell therapy for treatment of adult patients with relapsed or refractory large B-cell lymphoma after two or more lines of therapy. CAR T cell therapy is a revolutionary approach to cancer treatment that involves harvesting a patient's T cells and genetically modifying them with special receptors that identify and destroy cancer cells when reintroduced into the patient's body. Yescarta is the first in what we hope to be a long line of groundbreaking cellular therapies for Gilead and Kite.

We further invested in cell therapy by acquiring Cell Design Labs, Inc. in December, gaining new technology that allows for the more precise programming of cells to attack

tumors. We believe the addition of this technology and the expertise of the team at Cell Design Labs will help researchers at Kite and Gilead more rapidly bring forward cellular therapies that are potentially both safer and more effective.

We have continued to lead and innovate in HIV. We have multiple medicines available to address the needs of individuals living with HIV, and are continuing to investigate new ways to manage the disease, such as long-acting treatments and new single tablet regimens for patients whose virus is resistant to most drugs. We also remain focused on prevention. As of the end of 2017, Truvada for pre-exposure prophylaxis (PrEP) is now reaching more than 153,000 people in the United States who are at-risk of sexually acquiring
HIV, for use in combination with safer sex practices, and we are working to increase appropriate use of Truvada for its prevention indication in other parts of the world. We also continue to pursue internal discovery research that may lead to a cure.

In the area of liver disease, we made considerable progress with our products for the treatment of HCV and chronic hepatitis B virus (HBV), and advanced trials with investigational medicines for nonalcoholic steatohepatitis
(NASH), a form of liver fibrosis that poses an increasing threat to public health as a rising cause of liver transplantation.
In addition, we have clinical trials underway for products targeted at inflammatory diseases, including rheumatoid arthritis, ulcerative colitis and Crohn's disease.


Supporting Our Communities
During the past year, we've continued our efforts to improve public health worldwide - including at home in the
United States. In 2017, we launched the Gilead COMPASS (COMmitment to Partnership in Addressing HIV/AIDS in Southern States) Initiative(tm), which pledged a landmark $100 million over 10 years to help address HIV/AIDS in the Southern United States. Through the Gilead COMPASS Initiative, we're focusing on making a meaningful difference by supporting capacity-building and shared knowledge, working to advance well-being, mental health and trauma-informed care, and increasing awareness and education to decrease stigma. We elected to focus on the Southern United States because of
the severity of the epidemic in this region. Although the South accounts for just one-third of the U.S. population, it is home  to approximately 44 percent of all people living with HIV in the United States. In addition to this specifi initiative, we're also

focused on providing support to more than 2,000 organizations globally to support their critical missions in serving impacted communities.

In the same way we are driven to innovate and improve care, we have a very clear mission to minimize our environmental footprints across our worldwide business operations and supply chain. We recognize that improving global health requires caring for the health of the environment, and our actions can positively influence environmental and social ecosystems worldwide. We've made significant progress reducing our impact. For example, at our headquarters in Foster City, California, we began purchasing renewable energy to help meet 50 percent of our annual electricity demand.
We launched a worldwide facilities master planning effort to support green building and responsible growth practices, and establish a pathway for reducing energy, water, waste and greenhouse gas emissions. We also implemented a
Supplier Code of Conduct that holds the companies we work with to high social, economic and environmental standards. Our medium- to long-term sustainability strategies include installing on-site renewable energy systems at key corporate facilities, achieving "zero waste" at some of our manufacturing sites as we have in Cork, Ireland, for the fifth consecutive year, and continuing to implement green and sustainable chemistry practices to reduce the amount of raw materials and water necessary to manufacture our products. In the long term, we expect to take even more definitive steps toward minimizing our social and environmental footprints, while fostering sustainable business growth.


Gilead's Future
In 2018, we will continue to reach more people with serious illnesses around the world. We will build new partnerships and collaborations to advance scientific innovation and support people in managing their health, even in the face of tremendous obstacles. In everything we do, we are determined to step up to the most daunting challenges with creative, sustainable solutions that make life better for people around the world. On behalf of our employees, our
leadership team and our Board of Directors, I'm proud of what we accomplished in 2017 and look forward to our continued success in the coming years.

Sincerely,
John F. Milligan, Ph.D.
President and Chief Executive Officer


