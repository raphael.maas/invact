A Message from the Chairman & CEO

Dear Friends:                                                                                        the way in which we interact with our cus-
                                                                                                     tomers. All options are being analyzed and
To grow, prosper and remain sustainable,                                                             when our plan is complete, we will share it.
we must embrace change and transforma-                                                                   We expect natural gas will play a larger
tion. Change requires leadership, courage                                                            role as shale reserves are developed. We
and an open culture. Transformation de-                                                              expect gas suppliers to responsibly ad-
mands candor, trust and collaboration as we                                                          dress environmental and safety issues.
engage with many different stakeholders to                                                           Other energy sources also will play a bigger
find the best solutions. Above all, sustaina-                                                        role, including renewable resources where
bility requires shared commitment, clear in-                                                         they are accepted, nuclear power, hydro
tent and a solid plan of action. The changes                                                         power, demand response programs and
under way at AEP will transform our opera-                                                           energy efficiency. Transmission will be a
tions, reduce our environmental impacts                                                              more critical resource, too. Consequently,
and ensure our long-term financial health.                                                           our capital investments will shift as we
     Our sustainability journey has already                                                          build natural gas plants, continue to invest
carried us far. Our accomplishments in-                                                              in transmission and update aging equip-
clude significant reductions in air emissions;                                                       ment. This strategy will benefit our custom-
improved employee safety and health;                                                                 ers, our investors, the environment and our
improved system reliability; investments in technologies that will        other stakeholders. But it will come at a cost.
shape the future of energy production, transmission, distribution
and use; the growth of our modern transmission business; reduc-           Energy Policy Lacking
tions in our own energy use; increases in our fuel diversity, includ-     We are in desperate need of a comprehensive federal energy policy
ing the use of renewable resources; sustainable dividends for our         that addresses environmental concerns and energy security and
shareholders; a stronger balance sheet; new partnerships with             establishes a long-term energy strategy for the nation. Only a coor-
many stakeholders; and improved service to our customers. We              dinated national plan can ensure our energy security and reliability.
are proud of these achievements.                                          Without it, energy-related decisions will, out of necessity, be more
     But the future looks much different than before. The road            tactical than strategic.
ahead is paved with significant challenges for our company and                 If the U.S. Environmental Protection Agency's (EPA) proposed
our customers that were not on the radar even three years ago. A          rule to control mercury and other emissions is a harbinger of the
combination of factors is forcing us to look at our business in a new     agency's plans for several other regulations, our transformation
light. Key drivers include eroding tolerance by customers for rate        may be accelerated, but with serious financial consequences. It
increases; denial by state regulators to recover our investments in       most certainly will increase our costs to comply as well as what our
carbon capture and renewable energy; slow economic recovery               customers pay for electricity.
in most of our states; more complex and stringent environmental                Let me be clear: We support the Clean Air Act. But the EPA's
regulations that will push customer rates still higher; the rising cost   process and timelines are not realistic, and that is what we take
to operate older, less efficient coal units; the cost-competitiveness     issue with. We support rational environmental regulation that pro-
of natural gas compared with other fuels; a dramatically different        vides significant public health and environmental benefits. But
political landscape; and new concerns about the future of nuclear         compliance requirements must be affordable and at an achiev-
power in the wake of the Japanese nuclear crisis.                         able pace. Already, we know that compliance with the proposed
     We are developing a transition plan that addresses grid reli-        new hazardous air pollutants rule on the prescribed timeline will be
ability, customer bill impacts, sustainable job creation and the need     extremely difficult and will prompt premature retirements of some
for a more diverse fuel portfolio in the future. It will also transform   coal units across the country.

                                                                                    




We are in desperate need of a comprehensive federal energy policy that
addresses environmental concerns and energy security and establishes a
long-term energy strategy for the nation.
                                                                          
     The price tag could be staggering; we won't know the final cost
until all of the regulations are finalized, but we estimate the cost of    
compliance under the EPA's timeline could be more than double
                                                                                                                                                 
                                                                           
what we have spent so far for environmental controls. Our concern

                                                                                                                                                  
is the failure to consider all of the regulations in total, rather than   
one-by-one. This impedes our ability to determine which coal units
                                                                                                                                                  
                                                                           
have to be closed and which will remain in service. This uncertainty
                                                                                                                                                
                                                                           
complicates our resource planning. We also are deeply concerned
                                                                                                                                                
                                                                           
that the EPA's process does not consider the loss of jobs and com-
munity tax revenues and the impact of higher electric rates, espe-
                                                                          
cially on low-income customers and electric-intensive industries.
                                                                           
     We hope the EPA will listen to our feedback. A few changes to
                                                                                                                                               
                                                                          
the combined rules would bring tremendous value, allowing us to
achieve compliance without harming local and national economies.
                                                                                  
     Global warming continues to be a concern and one that must
be addressed internationally. Our position on this issue has not
                                                                           technology be brought to maturity and demonstrated on a com-
changed. No single country or company can solve it. It is our funda-
                                                                           mercial scale if we want to maintain coal as an option in a carbon-
mental belief that the best way to address this issue is through inter-
                                                                           constrained world. When there is a federal requirement and/or
national collaboration. That's why in 2011 we signed agreements
                                                                           adequate funding to support CCS, AEP is up to the challenge. But
with two of China's largest energy companies to support knowl-
                                                                           without these assurances, the future of our project is very unclear.
edge and technology exchanges related to carbon capture and grid
                                                                                  We need other options for coal, too. That's why the John W.
developments. We also believe that, here in the United States, the
                                                                           Turk, Jr., ultra-supercritical coal plant under construction in south-
best way to address climate change is through legislation.
                                                                           west Arkansas is important. Once complete, this will be one of the
     The lack of a strategic energy policy also hampers the devel-
                                                                           most efficient coal plants in the United States. It is also the first
opment and deployment of new and expensive technologies that
                                                                           application of this technology in this country.
we need to address climate change. Without a legislative mandate,
regulators are telling us loud and clear that they are not willing to
pay for them. AEP took the global lead and the financial risk to           Balancing All Stakeholder Needs
                                                                           We are engaged in candid, ongoing discussions with regulators,
advance carbon capture and storage (CCS) because this tech-
                                                                           legislators and many other stakeholders about our concerns. These
nology is critical if we are serious about reducing carbon dioxide
                                                                           are difficult and complicated issues, and there are no easy solu-
emissions. But regulators in Virginia rejected our request to recover
                                                                           tions. If we work together, we think the outcome can be positive
costs associated with the project. We are still evaluating a regula-
                                                                           for stakeholders, customers, the environment and society. We
tory decision on the project from West Virginia. We learned a lot
                                                                           believe sensible regulation and policy can be crafted that balances
from our 20-megawatt (MW) CCS validation project at the Moun-
                                                                           the costs and benefits. If done right, it would create jobs and eco-
taineer Plant in West Virginia, enhancing our knowledge of the pro-
                                                                           nomic opportunity, achieve the long-term environmental goals, and
cess and technology. But substantial financial challenges remain.
                                                                           spread the costs over time to avoid unnecessary financial hardship
     Through a joint funding effort with the U.S. Department of
                                                                           for customers. We will continue to connect with our stakeholders
Energy's Clean Coal Power Initiative Round 3, we have begun geo-
                                                                           and seek their input as we move forward.
logic, engineering and design work for a commercial-scale 235-
                                                                                  We are preparing to undergo a transformation that will signifi-
MW project that could be operational by 2015. We also recently
                                                                           cantly change our business. Our responsibility is to manage and
received $4 million from the Global Carbon Capture & Storage Insti-
                                                                           reduce our environmental impacts as we deliver reliable electricity
tute in Australia to support engineering work. It is essential that this

                                                                                                                                                      
                                                                                                      
Our 2010 earnings of $ 3.03 per share on an
ongoing basis were at the upper end of our                                                                                      
projected range and exceeded 2009 ongoing                                                                                        
                                                                                                                                 
                                                                                                                                 
earnings per share of $ 2.97 . Our total share-                                                                                
                                                                                                                                
holder return was nearly 9 percent for 2010 .                                                                                    



to customers. We will increase our earnings potential as we invest         steady, solid, competitive return in today's marketplace.
in our existing distribution and generation infrastructure and expand           I am delighted that, twice last year, our board of directors
the transmission grid inside and outside of our service territory. We      voted to increase the quarterly dividend to shareholders. The total
will do it by developing our work force to build, operate and main-        quarterly dividend increase of 12 percent helped to keep our total
tain new technologies essential to our success. We also are com-           shareholder return competitive and allowed us to reward our share-
mitted to engaging our stakeholders in honest and open dialogue            holders until our earnings growth can accelerate from increased
about our plans while doing a better job of informing our customers        capital investment and sales growth. It was a thrill to ring the clos-
about the cost and value of electricity.                                   ing bell at the New York Stock Exchange in June 2010 in celebra-
     Our business transformation touches every one of our oper-            tion of AEP's 400th consecutive quarterly dividend payment, a rare
ating companies, customers, shareholders, employees, communi-              occurrence indeed. On March 10, 2011, we paid our 403rd con-
ties, legislators and regulators. To be successful, we must continue       secutive quarterly dividend to our shareholders.
to deliver strong earnings this decade and beyond; we cannot meet
our commitments unless we have the resources to do so.                     Operational Performance
                                                                           Operating more efficiently and adapting to the changing economic
                                                                           and regulatory environment requires our work force to change, too.
2010 Financial Performance
We achieved strong financial results last year, in part due to aggres-     We reduced our work force by more than 2,400 employees in 2010
sive cost cutting and favorable weather. We also rewarded our              in a realignment of our cost structure with a slow but steady eco-
shareholders with higher dividends. Our continued financial health         nomic recovery. Most of those who left did so voluntarily.
and ability to create long-term value for shareholders depends upon             I am pleased to report that, despite this restructuring and
our ability to transform our earnings as we transform our company.         associated distractions, our overall safety performance was excel-
     As we shift our focus from coal generation to resource diver-         lent. AEP employees share a deep commitment to safety and
sity and the efficient delivery of energy through transmission and         health and strive to live up to that commitment every day. I am pro-
grid investments, we also will transform the earnings stream of the        foundly grateful to our employees and our leadership for their dedi-
company. That is our integrated plan for growth.                           cation to safety and health. No employee lost his or her life while
     The actions we took during the past two years put us on the           working for AEP in 2010, fewer employees were harmed on the job
road to slow and steady growth this year. Revenue increased to             and the severity of injuries was at a near-record low. Our board of
$14.4 billion from $13.5 billion in 2009, largely due to successful        directors passed a resolution commending employees for perform-
rate cases, which allowed for recovery of capital already invested         ing at a level that was among the best in our company's history.
and for incremental costs such as fuel. Weather and a 5 percent                 We have made significant progress, but we can and will do
rebound in industrial sales also helped revenues.                          better to improve our safety and health record. Three employees
     Operations and maintenance (O&M) expenditures for the en-             lost their lives on the job during the past five years. We will not tol-
tire system, on a Generally Accepted Accounting Principles basis,          erate any compromise of safety standards, and we will continue to
increased $449 million in 2010. The increase primarily related to          work hard to achieve and maintain zero harm.
$293 million (pretax) of severance costs due to our cost reduc-                 Our environmental performance is excellent: We are a top per-
tion initiatives and $114 million (pretax) of expenses associated with     former in our industry by almost every measure. We are proud that
dollar-for-dollar rate recovery. Our capital investments of $2.2 billion   this performance is outstanding for a utility of our size and scope.
were down from almost $2.5 billion in 2009 as we responded to              However, we fell short of our goal of zero environmental violations.
regulator and customer concerns about rate increases.                      In 2010, we had three violations and paid minimal fines of less than
     Our 2010 earnings of $3.03 per share on an ongoing basis              $10,000. As we learn from these events and take steps to prevent
were at the upper end of our projected range and exceeded 2009             recurrences, our goal continues to be zero violations.
ongoing earnings per share of $2.97. Our total shareholder return               Our distribution system reliability improved in 2010. The aver-
was nearly 9 percent for 2010, providing investors in AEP with a           age length of time that customers were without power and the fre-





quency of interruptions improved significantly, helping us to achieve     critical global energy resource for the future.
our best performance in five years. Our gridSMART  initiative, un-             Finally, a few personal notes. I have tried to prepare for the
der way in four states, will transform our relationship with our cus-     future since my first day here in 2004. We created an extensive
tomers from one in which the customer uses power and gets a bill          succession planning initiative to broaden the knowledge and skills
to one in which we work together to save electricity, lower energy        of our executives and to ensure that the most qualified candidate
demand and consumption, and reduce customer costs.                        takes the helm when I step down as CEO. I am very proud that this
    Our transmission strategy to expand inside and outside of our         process resulted in the board having external candidates and four
service territory also moved forward. Among our successes were            strong internal candidates from which to choose. The board named
the establishment of transmission companies in Ohio, Michigan             Nick Akins as president of AEP, and he will work closely with me this
and Oklahoma. Applications are pending in West Virginia, Kentucky         year as I continue in the roles of chairman and CEO. If the succes-
and Indiana.                                                              sion plan continues according to schedule, Nick will also become
                                                                          CEO later this year.
                                                                                Two long-time board members will retire this year, bringing yet
Preparing for the Future
We all have a role in ensuring the quality of our energy future. We       more change to our leadership team. We are most grateful for the
believe that our customers want to use energy more efficiently, and       dedicated service of Donald M. Carlton and E.R. "Dick" Brooks,
in most of our service territories, programs and technology are           both of whom have served since 2000. Prior to the 2000 merger of
helping them to accomplish this goal. Moving forward, we want to          AEP and Central and South West Corp. (CSW), Dick was CEO of
operate our system more efficiently; diversify our fuel generation;       CSW for nearly 10 years. Both board members have been passion-
develop a more robust grid to enable the utilization of cleaner, more     ate advocates for employee safety, our nuclear program and our
efficient and economic energy; and prepare for the electrification of     company's tradition of excellence in governance.
the transportation sector.                                                      We are in a time of great transformation. Our vision for cleaner,
    We have many initiatives under way that position us to achieve        more affordable and more reliable electricity is central to America's
these goals. We will also work to prepare our regulators, employ-         economic recovery and growth. As a nation, we must embrace
ees, customers and communities for the full impact of coal unit           energy as a powerful engine for our country's economic future; as
closings, new environmental mandates and the true cost of clean           a company, we must strive continually to balance the needs of our
energy. We are ready to listen to ideas they may have for solu-           customers and shareholders with measurable benefits to the envi-
tions to these complex problems. We will continue to communicate          ronment and society. The men and women of AEP are making bold
these issues with our stakeholders and collaborate with them to           changes that will lead us toward a more secure energy future; I
find common ground and pursue common sense solutions.                     invite you to join us in leading this exciting transformation.
    We are saddened by the terrible loss of life and destruction
associated with the earthquake and tsunami that hit Japan in              Sincerely,
March. We are also concerned about the events at the Fukushima
Daiichi Nuclear Station in northeastern Japan. Although all the les-
sons to be learned are not yet known, AEP remains committed to
learn from these events and to operate our Cook Nuclear Plant to          Michael G. Morris
one standard  "Excellence." We also believe that it would be unfor-      Chairman & Chief Executive Officer
tunate and inappropriate to discount nuclear energy as a viable and       April 2011

