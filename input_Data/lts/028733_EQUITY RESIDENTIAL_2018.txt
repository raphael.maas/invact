Dear Fellow Shareholders:
Strong high wage job and income growth, positive demographics and a consumer preference
for a rental lifestyle in our highly desirable markets has created a supportive backdrop for our
business. Our markets continue to pull in the skilled knowledge workers that drive economic
growth in the United States. This, in turn, attracts employers to our markets seeking to locate
and expand their businesses and draw on this talented pool of workers, resulting in strong
demand for our product.
At Equity Residential we pride ourselves on the astute allocation of your capital. Real estate is a
capital intensive business and we have a proven track record of creating value throughout the
real estate cycle by allocating your capital in a superior risk adjusted manner. We also have
created a best-in-class operating platform to run our properties. Our 2,700 employees are
focused on delivering remarkable customer service to our residents so they will stay with us
longer, be willing to pay higher rent for a great experience and will tell their friends about how
much they love living in an Equity Residential property. Increasingly, we are using technology to
improve our residents� experience with us and to operate more efficiently. We have a
conservative balance sheet that enhances returns and value creation while maintaining capacity
to take advantage of the next opportunity. We are committed to sustainability, diversity and
inclusion, the total well-being of our employees and being a responsible corporate citizen in the
communities in which we operate. We call these our �Equity Values� and these values are deeply
embedded in our culture. We believe that our stakeholders value stability, liquidity,
predictability and accountability and that is the mission to which we remain unwaveringly
committed.
FOCUS ON OUR INVESTMENT STRATEGY
In 2018 Equity Residential:
� Acquired five apartment properties consisting of 1,478 apartment units for
an aggregate purchase price of approximately $707.0 million at a weighted
average Acquisition Capitalization Rate of 4.4%. These apartment properties
are located in Seattle, Boston, Hoboken, New Jersey and Denver.
� Re-entered the Denver market with the purchase of two apartment
properties (which are included in the count above) and acquired an
additional apartment property in Denver in the first quarter of 2019. The
Company now owns and operates 1,000 apartment units in the Denver
market.
� Sold five apartment properties consisting of 1,292 apartment units for an
aggregate sale price of approximately $706.1 million at a weighted average
Disposition Yield of 4.1%, generating an Unlevered IRR of 8.7%.
� Completed the stabilization of four development projects in Seattle (two
properties), San Francisco and Washington, D.C. for a total development cost
of approximately $794.8 million and a weighted average Development Yield
of 5.2%.
� Began the construction of a 469-unit, 44-story apartment tower in Boston,
which is estimated to be completed in 2021 at a total development cost of
approximately $409.7 million and stabilize at a Development Yield of 6.1%.
We have focused our portfolio in urban and high density suburban locations in Boston, New
York, Washington, DC, Southern California, San Francisco, Seattle and now, Denver. These are
the places where our country�s knowledge-based economy will continue to grow, creating highwage jobs that attract large segments of the population opting for high-quality, well-located
rental housing. These are also the markets that feature the highest cost of single-family home
ownership. Across the country we continue to see a �re-urbanization� of the city center and an
�urbanization� of the town centers of many close-in suburban markets. There are permanent
populations of affluent renters in these places that are attracted to a lifestyle where they can
complete their errands by walking or have easy access to public transportation.
As the following charts show, there has been a meaningful increase in the propensity of highincome households and highly-educated individuals to live in high-density urban areas.
We can�t help but notice that many of the technology companies, which are major drivers of our
economy, are attracted to these markets as well as they seek out employees with the skills to
drive their business. Their employee base in these markets fits right into the demographics of
our typical renter. In both the New York and Washington, D.C. markets, we have a number of
properties that are an easy commute from Google and Amazon�s recently announced
expansions. In fact, 70% of our net operating income (NOI) in the Washington, D.C. market
comes from properties within five miles of the new Amazon HQ2.
While we continue to look for opportunities to expand our portfolio in these locations, we also
look for other markets that share these same characteristics. Denver is a market where we
previously had a presence but not the portfolio that we wanted for the long-term. We sold that
portfolio in 2016, but said at the time it was a portfolio, not a market exit. We are pleased to
report that we re-entered Denver with the acquisition of two properties in late 2018 and added
one more in early 2019. Denver is a top 20 MSA that continues to experience meaningful
growth. The market features characteristics that we think lead to long-term outperformance for
a rental market: strong high wage job growth, high single family home prices and a very
attractive lifestyle for our target demographic. Like many of our markets, Denver has been
experiencing elevated levels of new supply. We have underwritten our acquisitions
conservatively with this competition in mind and see the increased supply as an opportunity to
amass a portfolio in Denver at attractive prices from owners less able to hold for the long term.
We regularly review our portfolio strategy, looking at the mix of markets and submarkets as well
as individual assets. In our regular investor presentations, we have shared with you our thought
process on the factors that drive good long-term performance. There are markets that we do
not currently operate in that share many of the key characteristics of our markets and as part of
our continued program of portfolio refinement, you may see us add another market or two in
the future. We will also likely work to increase the percentage of our portfolio in dense suburban
submarkets to bring us more balance.
Long Term Rent Growth & Occupancy
The current demand to own apartment assets in our markets remains strong and we have not
seen capitalization (cap) rates or values move in our markets, other than New York, where cap
rates have held but NOIs have declined. As a result, we continue to be conservative on the
acquisition front. We plan to acquire approximately $700 million of assets in 2019 with the
funding for those acquisitions coming from the proceeds of a like amount of dispositions. We
believe that this activity should be accretive to our earnings as we sell some low cap rate New
York assets and reinvest in higher cap rate markets.
EQR Markets Have Consistently Outperformed
Other Markets in Capital Appreciation
THE DRIVERS OF DEMAND IN OUR MARKETS
The apartment business continues to benefit from the continuing growth in demand for rental
housing in our markets and, in particular, for the high-quality, well-located assets that we own
and manage. Demand for rental housing is driven primarily by household formations from the
millennial segment of our population, also known as the Echo Boom Generation. These young
adults, born between 1981 and 2000, currently total approximately 78 million people and are
disproportionately renters. Millennials now comprise the largest segment of the U.S. population
and the impact they are having and will continue to have on rental housing cannot be
overstated. We will continue to see demand from this group as the largest sub-segment of this
cohort is now turning 28 years old while the median age of our resident is 33 years old.
Following the Millennials is Generation Z, which comprises the more than 70 million people
born between 2001 and 2014. Equity Residential is extremely well positioned to benefit for
many years to come as a result of the huge impact these generations will have on rental
housing.
As of Q1 2018.
Source: U.S. National Center for Health Statistics, Rosen Consulting.
Births By Generation
EQR Median
Resident Age is 33
Demographics and Lifestyle Choices Will Continue
to Create Renter Households
1.0
1.5
2.0
2.5
3.0
3.5
4.0
4.5
1911
1913
1915
1917
1919
1921
1923
1925
1927
1929
1931
1933
1935
1937
1939
1941
1943
1945
1947
1949
1951
1953
1955
1957
1959
1961
1963
1965
1967
1969
1971
1973
1975
1977
1979
1981
1983
1985
1987
1989
1991
1993
1995
1997
1999
2001
2003
2005
2007
2009
2011
2013
2015
2017
Greatest Silent Boomers Gen X Millennials Post Millennials
Young adults are attracted to rental housing for the flexibility it offers as they pursue their
careers and life interests. We attract these residents to Equity Residential because of the quality
of our assets and the locations that we offer. They choose to stay with us because of our
outstanding property staff and the exemplary services we provide.
Residents also will stay with us longer as they delay the life choices that most commonly drive
the decision to buy a home. The Millennial Generation is marrying later and having children
later in life than its predecessors and therefore is likely to stay in rental housing significantly
longer than past generations. We see this in our portfolio as our residents renew their leases
with us at a record rate.
For many households, remaining in rental housing is a lifestyle choice--a desire to live in close
proximity to friends and favorite activities. For others, it is an economic reality which is
particularly true in our markets. In addition to providing job opportunities and lifestyles that
attract so many Millennials to our high-density, urban and suburban apartments, our markets
have median single family housing costs well above the national average. So while our residents
generally have high incomes and do not pay a disproportionate amount of their income in rent,
these high single family home costs and corresponding large down payment requirements
do make it far less likely that our residents
can afford a single family home if, in fact,
they wished to make that lifestyle
decision. In addition to high prices and
large down payments, the student loan
burdens of many of our residents will
make single family home ownership
much more of a dream and far less of a
reality.
EQR Resident Average Household Income: $157,408*
Average Annual Rent as a Percent of Income: 19.3%*
*For move-ins in the last 12 months
11.4x 9.0x 9.0x 5.7x 5.4x 4.0x 4.9x 4.2x 4.6x 3.5x
(1) PHADO represents Phoenix, Houston, Atlanta, Dallas and Orlando as a proxy for low barrier to entry markets.
Source: American Community Survey; CoreLogic; National Association of Realtors.
As of Q1 2019.
Median HH Income
Median Home Price
$1.0M $986
$597
$478 $474
$409 $386
$259 $255 $226
1,000,000
1,200,000
800,000
600,000
400,000
200,000
0
Median Household Income vs. Median Home Price
EQR is Strategically Located in Markets With High
Cost of Single Family Housing
$88 $109 $66 $84 $87 $101 $78 $62 $55 $64
Manhattan San
Francisco
L.A. Boston Seattle D.C. Denver National Miami PHADO(1)
Price/
Income
Market research also shows that the primary reason people buy a single family home is a
lifestyle choice, one often made when they are having children and looking for a place to raise
their families. More than 40% of our apartment units are occupied by a single resident, a
demographic that has an extremely low propensity to purchase a home. In fact, we saw less
than 6% of our residents leave us to buy a home in 2018, which is the same level that we have
seen over the last five years.
There is absolutely no denying that Millennials are an important part of today�s renting
demographic. But they are not the whole story. Currently, about 17% of our apartment units are
occupied by people age 50 and older. Many demographers expect the share of aging Baby
Boomer rental households to grow as they shift away from home ownership and, like
Millennials, seek the simplicity, flexibility and overall lifestyle that rental apartments offer.
People of all ages and walks of life are participating in the re-urbanization of our nation as they
are attracted to the urban core and dense suburban centers for ease of access to public
transportation, dining, entertainment, culture, night life, education, employment, health care
and more. The demographic picture across our nation is a powerful generator of current and
future demand for rental housing. And, as much of this demand will be in the high density
urban and suburban centers of our markets, we believe we are extraordinarily well positioned
and have a long run of strong growth yet ahead of us.
DEMAND REMAINS STRONG AND SUPPLY WILL BE ABSORBED
We have seen elevated levels of new supply in our markets over the last several years, which is
not surprising given the strong renter demand discussed above and the strong demand from
investors for exposure to our sector. Our markets usually see deliveries of approximately 1.0% of
inventory, but that number has trended towards 1.5% recently.
In 2018, we saw elevated supply in
most of our markets, particularly
New York, but strong demand from
new renters and excellent retention
of existing residents allowed us to
still deliver solid revenue growth,
albeit below our long term average.
EQR 2018 Same Store Turnover: 51.1%, down 2.3% from
2017
EQR 2018 Same Store Average Renewal Rent Increase:
4.9%
In 2019, we believe supply that is competitive with our properties will materially decline in New
York and be lower in Boston. In our other markets, we expect supply levels to continue to be
elevated in 2019, especially in portions of Los Angeles. Going forward, we believe that we will
see a decline in deliveries in all our markets as land costs have remained high, construction costs
continue to rise much faster than rent growth and equity investors become increasingly skittish
as the economic cycle matures.
Equity Residential has a capable development capacity of its own. However, for the same
reasons of high land prices and escalating construction costs, we have become less active
developers of late. In the right environment, we see development as an excellent opportunity to
create long term value for our shareholders by building new streams of income where it is
difficult or impossible to acquire existing assets. So, while our current development pipeline is
considerably smaller than in years past, we currently have about $520 million of assets under
development, including our $410 million West End Tower in Boston. We also have
approximately $800 million of recently completed developments in various stages of lease-up,
which we expect to contribute more than $40 million to our NOI in 2019. In 2018, we stabilized
four new developments, in Seattle (two properties), Washington, D.C. and San Francisco, which
had a total development cost of approximately $794.8 million and a weighted average
Development Yield of 5.2%. These assets would sell at cap rates from the low to mid 4%s,
reflecting the high value created by our development business in this cycle. Our team continues
to look for the right combination of location and economics to develop new projects.
We generate approximately $250 to $300 million in annual free cash flow after paying our
dividends and our capital expenditures. We had been using this cash flow to complete our
development pipeline, which has produced a tremendous return on that investment. In 2019,
our development funding will be reduced and will allow us to allocate that capital to other
shareholder beneficial activities, including a recently announced 5.1% increase in our common
share annual dividend.
As we continue to enjoy great rental demand across our portfolio of well-located assets, we see
unit renovations as another area of significant value creation. We continue to spend
approximately $40 million each year renovating kitchens and baths. This work keeps our assets
competitive in the marketplace while delivering strong returns on incremental capital. We also
plan to spend several million dollars on customer facing renovation projects in our common
areas � like club rooms, lobbies and gyms � in order to ensure that our properties remain
competitive with newly delivered properties in our markets. We are pleased to report that we
are able to do all of this while keeping our capital spending to approximately 8% of average
rents, which is considerably lower than the sector average.
2018 OPERATING PERFORMANCE
2018 was a year of elevated supply across our markets, but our teams hunkered down and
worked their portfolios lease by lease to produce:
� Earnings Per Share (EPS) of $1.77, which was an increase of 8.6% over 2018,
and Funds From Operations (FFO) per share of $3.14.
� Normalized FFO per share of $3.25, which was an increase of 3.8% over 2017.
� Same store revenue growth of 2.3%, which was at the top end of our original
guidance range.
� Physical Occupancy of 96.2% for the full year, renewal rate growth of 4.9%
and the highest resident retention in our history.
� An increase in our common share annual dividend to $2.16 per share, a 7.2%
increase over the 2017 dividend.
2019 OPERATING PERFORMANCE GUIDANCE
In 2019, we expect to produce:
� EPS of $1.88 to $1.98, an increase of 9.0% at the $1.93 per share midpoint
over our 2018 performance, and FFO of $3.26 to $3.36 per share.
� Normalized FFO per share of $3.34 to $3.44, an increase of 4.3% at the $3.39
per share midpoint over our 2018 performance.
� Same store revenue growth of 2.2% to 3.2%, an improvement, at the 2.7%
midpoint, of 40 basis points over our performance in 2018.
� Same store Physical Occupancy of 96.2% in 2019, which is the same level
achieved in 2018.
� Renewal rate increases of 4.9% on average, which is equal to our 2018
performance.
� Same store operating expense growth of 3.5% to 4.5%, which reflects
continued pressure from property taxes, which account for over 42% of our
annual operating expenses as well as pressures from payroll, which accounts
for over 22% of our annual operating expenses, as wages rise across the
country due to great demand for our highly-skilled, first-rate property
professionals in a growing apartment rental industry.
FOCUS ON OPERATIONS AND INNOVATION
Maximizing revenue by balancing occupancy and rental rate is at the heart of our operating
strategy. We have a strong focus on resident retention, which paid off in 2018 with renewal
increases of 4.9%, on average, and the highest retention in our history, despite elevated levels of
new supply in many of our markets. This elevated supply creates lots of choices for our
customers so keeping our residents happy, in place and willing to pay more for their apartments
is more important than ever.
In 2018, we recorded the highest Customer Loyalty Scores in our history. This is all the result
of our Incredibly talented team of hard working professional. This group is focused on their
residents while being determined to deliver long term growth for our shareholders.
Teamwork and collaboration are the
bedrock principles of a successful
enterprise and are in our DNA. Our
teams at the properties work hard all
year, lease by lease, to deliver.
46.0% Customer Loyality Score (CLS) up from 41.8% in
2017.
The highest score in the Company�s history.
While the business of renting someone a place to live has been around since the dawn of time,
the technology driving the business today continues to evolve at a rapid pace. We remain at the
forefront of harnessing innovation and advances in technology to serve our customers better
and operate more efficiently. Our focus on technology and innovation is far reaching and covers
a number of areas. In operations, we are equipping our property service teams with technology
to improve efficiency and transparency. We are also currently reviewing and will soon be testing
�smart home� systems for door locks, thermostats and lighting controls at a number of
properties in hopes of finding the right mix of what residents want in their homes. We are
expanding our data and analytics capacity to enable an analytics strategy that drives business
performance. We have also made investments in two private equity funds focused on real estate
technology with the purpose of getting an early look at technologies across the real estate and
operating space.
The goal of these activities is to continually improve upon our best in class operating platform
to create remarkable customer experiences and drive the performance that you have come to
expect from Equity Residential.
� We maximize rents for new leases through the utilization of our customized
pricing system that uses market data on current and projected demand and
availability to create both spot and forward pricing every day for every single
apartment unit we manage.
� Our great success with renewal rate growth is due to our motivation to retain
our residents and that process begins the day they move in with us with a
relentless focus on customer service. Very satisfied residents stay longer and
say great things about us. In 2018, we reported all time high retention and
that corresponds to our highest recorded satisfaction metrics on google +
and our own internal loyalty index.
� We use a standardized, centralized purchasing system to control our
operating expenses and a business intelligence platform that allows all our
team members to quickly identify and address issues and opportunities.
ADDRESSING THE AFFORDABLE HOUSING SHORTAGE
In contrast with the recent elevated levels of higher end rental housing construction, little new
work-force housing has been built in this cycle. In our markets, as in many places in our country,
there is a lack of affordable, work-force housing. This has caused political pressure to enact rent
control regulations in some of our markets. We are opposed to rent control. Time and time
again, history has shown that rent control is bad public policy. Independent academics from
major universities all agree that these policies will discourage new construction and reduce the
availability of affordable and middle-class housing. We are in favor of market based solutions to
address the issue, including better zoning regulations and public/private partnerships to build
and fund these projects. We are committed to working directly and through industry
associations with the various policymakers in our markets to find a solution that works for
everyone.
FINANCIAL STABILITY
We believe that prudent capital management through operating cycles and transformative
events enables financial flexibility, better access to capital and reduced interest rate risk. We are
focused on maintaining a strong and flexible balance sheet, characterized by low leverage and
excellent liquidity, which preserves shareholder value during capital market dislocations and
provides us with the capability to take advantage of opportunities when others are capital
constrained. And because real estate is a cyclical and capital intensive business, we maintain
access to the unsecured bond market as well as Fannie Mae and Freddie Mac and the life
insurance companies.
Some Highlights:
� 5.3x Net Debt to Normalized EBITDAre
� Senior unsecured debt ratings of A-/A3/A by S&P, Moody�s and Fitch,
respectively, which are some of the highest long-term credit ratings in the
real estate business.
� A $2.0 billion unsecured revolving credit facility, which matures in January
2022 and has an interest rate of LIBOR plus a spread (currently 0.825%) with
an annual facility fee of 0.125%.
� A $500 million unsecured commercial paper program.
� We have taken advantage of positive near-term arbitrage opportunities by
locking in historically low interest rates and extending weighted average
maturity from six years in 2013 to nearly nine years today.
In February of 2018, we issued a $500 million, 3.5% 10-year unsecured bond. As a reflection of
our strong credit, this issuance had the lowest credit spread for any 10-year REIT benchmark
offering in history. We used the proceeds from this issuance to prepay $550 million in 6.08%
secured debt due in 2020. In November of 2018, we were the first apartment REIT to issue a
�green� bond. This was a $400 million, 10-year unsecured issuance at a coupon of 4.15%. As a
result, the Company will allocate an amount equal to the net proceeds of approximately
$396.7 million from this issuance to one or more eligible green projects, such as our recently
developed 855 Brannan community in San Francisco, which received LEED Home Platinum
certification.
We are pleased to have consistently demonstrated the benefits of our conservative balance
sheet management through numerous operating cycles and transformative events.
OUR COMMITMENT TO ESG
Equity Residential has a company-wide commitment to our �Equity Values� which includes
Sustainability, Diversity and Inclusion, Corporate Social Responsibility and Total Wellbeing. Our
employee-led Equity Values Council helps drive our efforts on these important issues by acting
as change agents to drive initiatives and create awareness. We also engage our stakeholders for
feedback on key issues and environmental, social and governance factors, which helps guide
our investment and operating strategy. In 2018, we enhanced our ESG report and, for the first
time, had our Greenhouse Gas Emissions Inventory, energy consumption, water consumption
and waste generation reviewed and assured by a third party. You can read more about our ESG
efforts in our annual ESG report, available in the Investors section of our website,
www.equityapartments.com. We were very pleased to be named as the 2018 Global Residential
Listed Sector Leader by the Global Real Estate Sustainability Benchmark (GRESB) survey, a
globally recognized analysis of the sustainability performance indicators of more than 900 real
estate portfolios worldwide. This was the fifth year in a row for us to be recognized as a sector
leader by GRESB. In addition, we were the recipient of the 2018 National Association of Real
Estate Investment Trusts (Nareit) Residential Leader in the Light award for sustainability, the
third year in a row that we received that recognition.
A FOCUS ON OUR EMPLOYEES
You can�t have an organization as special as ours without a fantastic team of professionals
across our entire enterprise. Their commitment to their customers, their colleagues and their
communities is unmatched.
They worked through some real challenges with new supply in their markets this year to
produce terrific renewal results, the highest retention in our history and the best Customer
Loyalty Scores we have received since we started tracking them. We were very pleased to be
recognized by both Glassdoor and Indeed as a top employer. Glassdoor recognized Equity
Residential as a 2019 Best Places to Work. We were ranked as the top real estate company and in
the Top 50 on the overall list. Indeed recognized Equity Residential as a top-rated workplace in
Boston, Washington, D.C., Seattle and Los Angeles.
A FOCUS ON LEADERSHIP
Having strong leaders at the helm of our enterprise has been tremendously important in the
long-term value creation that you have come to expect from Equity Residential.
Two of our great leaders decided to retire, which made 2018 a year of transition for us at Equity
Residential. David Neithercut retired at year-end after 13 years as our Chief Executive Officer
(CEO). David drove the transformation of the Company from suburban garden to the urban and
high density suburban portfolio we proudly own today. We thank him for his tremendous
contributions to the Company in his more than 25 years with the organization and are fortunate
that he will continue to serve as a member of our Board of Trustees. David Santee retired after
23 years with Equity Residential, including five years as our Chief Operating Officer (COO). David
was instrumental in establishing the technology platform that Equity Residential thrives on and
driving the integration of many portfolio acquisitions over his long career with the company.
Just as importantly, David was the embodiment of our mantra of �enjoy the ride.� We wish him
the very best in his retirement.
With these retirements came the appointment of Mark Parrell as CEO, Michael Manelis as COO
and Bob Garechana as Chief Financial Officer (CFO). Mark has been with Equity Residential for
twenty years and served as our CFO for the last eleven years. In that role he was a member of
our Executive and Investment Committees and a key architect of our strategy. Michael is a
twenty-year Equity veteran who most recently served as our EVP of Operations and worked
closely with David Santee in running our portfolio and platform. Bob has more than 14 years of
experience at Equity Residential, including the last 10 as our Treasurer. We are excited about the
new senior leaders and look forward to them filling some very big shoes.
Succession planning is a vitally important job for both the senior management team and the
Board of Trustees, all of whom were deeply involved in the preparation of Mark, Michael and
Bob for their new roles and the smooth transitions we have enjoyed. We are very pleased to
have received such positive feedback from both employees and the investment community on
how we managed this process.
We are incredibly fortunate to have a very strong Board of Trustees made up of thoughtful and
highly-engaged individuals with a variety of backgrounds who are dedicated to working with
our management team and the whole organization to create value for our shareholders.
This year we are also saying goodbye to Gerry Spector, who will retire from our Board of
Trustees in conjunction with our Annual Meeting of Shareholders in June. Gerry has been on our
Board since our initial public offering in 1993 and retired as our COO in 2007. His contributions
to our growth, our performance and our culture are too many to count. We are incredibly
thankful for all that he has done and wish him the very best in his retirement.
Our Board acknowledges the importance of board refreshment and has been laser focused on
this key topic. Since 2016, three trustees, including Gerry Spector, who had served since our
1993 IPO have retired and been replaced with trustees with diverse backgrounds and
perspectives. As part of this refreshment process, we are pleased to have added Ray Bennett to
our Board in the fall of 2018. Ray is the Chief Global Officer, Global Operations for Marriott
International, Inc. He brings to our Board extensive management and operational experience in
hospitality, customer service and procurement on a global level as well as valuable insight into
brand management, consumer insight and product differentiation.
We are proud to report that in 2018, Equity Residential was recognized as a Corporate
Champion by the Women�s Forum for gender diversity and our three female trustees have been
recognized by Women, Inc. as 2018 Most Influential Corporate Board Directors.
2019 AND BEYOND
We will continue to benefit from strong fundamentals in 2019 that will drive the absorption of
the new supply we discussed above.
You should expect to see us continue with our history of innovation in utilizing technology to
better operate our portfolio and serve our customers. We will harness data to help guide our
portfolio investment decisions
While we expect the re-urbanization of our cities to continue as powerful demographic and
economic forces shape our country�s future, we will continue to refine our portfolio through
selective dispositions and acquisitions in our current markets as well as investing capital in some
potential new markets.
Equity Residential is uniquely positioned to benefit from both the re-urbanization of the country
as well as strong demographic trends and we look forward to many years of growing income,
dividends and shareholder value on your behalf.
We appreciate your continued support.
Regards,
Sam Zell Mark J. Parrell
Chairman President and CEO