TO OUR FELLOW SHAREHOLDERS:
"Day in and day out, our more than 74,000 dedicated Team Members across 47
states roll up their sleeves prepared to out-work the competition and provide the
industry-leading levels of customer service that have come to define the name
OReilly Auto Parts.

O
nce again, we are very pleased to report to you that Team OReillys
unrelenting focus on providing consistently excellent customer service
has produced another year of record-breaking results. Day in and day
out, our more than 74,000 dedicated Team Members across 47 states roll up
their sleeves prepared to out-work the competition and provide the industryleading
levels of customer service that have come to define the name OReilly
Auto Parts. Our long-term success is the direct result of the hard work and
dedication of our Team Members, and we would like to thank each one for
their commitment to our success.
Our Teams commitment to being the dominant supplier in each of our markets
produced an industry-leading 4.8% increase in comparable store sales
in 2016, which was on top of a 7.5% increase in 2015, and a 6.0% increase in
2014, and represents our 24th consecutive year of positive comparable stores
sales growth since becoming a publicly traded company in 1993. While we
are very proud of our consistent top line growth, we remain highly focused
on continued profitable growth. Our teams persistent focus on expense
control ensured that our sales performance translated into profitable growth,
evidenced by our record 19.8% operating margin and 17% increase in diluted
earnings per share in 2016.
Our ability to continually generate record-breaking results is directly related to
our Teams unwavering focus on providing consistently high levels of service
to our customers, providing the strong base required to capitalize on a stable,
favorable industry demand environment. A key driver to current and future
demand in our industry is total miles driven in the U.S., which continues to
exceed three trillion miles each year, supported by relatively low gas prices
and improvements in U.S. employment levels. Ongoing healthy levels of new
car sales and stable, low scrappage rates result in a growing vehicle fleet, and
sustained improvements in engineering and manufacturing have produced
higher quality vehicles, which can be reliably driven at higher mileages, and
result in an aging vehicle fleet that now averages 11.6 years old. We believe
the growing, aging vehicle fleet will continue to benefit our industry, as these
higher mileage vehicles are driven trillions of miles each year, resulting in
ongoing wear and tear and experiencing more routine maintenance cycles,
which, in turn, supports stable, long-term demand for our products.
While a favorable long-term industry demand environment is beneficial,
delivering the repeated, record-breaking success we proudly report to you
each year is primarily the result of our greatest competitive advantages  our
Team Members and our Culture. Since 1957, our Team Members unrelenting
focus on our Culture has been the key to our success and continues to be what
propels our sales growth beyond that of our peers. We understand that our
customers rely on our friendly, knowledgeable Parts Professionals to answer
their questions and get them the right parts to complete the job they are
working on, and for over 30 years, we have executed our dual market strategy
of providing this high level of service to both our do-it-yourself and professional
service provider customers. We operate in very competitive markets,
and winning our customers repeat business requires our Team Members to
consistently execute our dual market strategy and live our Culture Values of
excellent customer service, hard work, enthusiasm, professionalism, teamwork,
respect and honesty each and every day, and we believe our results prove our
Culture is alive and well.

OUR CULTURE:
Our COMMITMENT to our
customers and our Team
Members. We are ENTHUSIASTIC,
HARDWORKING PROFESSIONALS
who are DEDICATED to
TEAMWORK, SAFETY, and
EXCELLENT CUSTOMER SERVICE.
We will practice EXPENSE
CONTROL while setting an
example of RESPECT, HONESTY,
and a WIN-WIN ATTITUDE in
everything we do.

team members
our greatest asset

OReillys promote from within philosophy allows us to continually perpetuate our Culture and instill our values
in each new member of Team OReilly. Our experienced, well-trained, technically proficient store teams are
equipped to provide industry-leading customer service in every new store we open. In 2016, we successfully
opened 210 net, new stores and, in 2017, we plan to continue our profitable growth with the addition of 190
net, new stores in both new and existing markets. Our stores are supported by our robust, regional, tiered
distribution network comprised of 27 distribution centers augmented with more than 300 hub stores, which
provide our stores same-day access to a vast inventory of hard-to-find parts.
Our customers depend on us to provide them with expert advice, excel
-
lent service, a competitive price, and the parts they need to complete their
jobs. The ability of our Professional Parts People to consistently satisfy
our customers needs is supported by our sustained competitive advan
-
tage in parts availability. Our industry-leading robust, tiered, regional
distribution network provides our store teams with access to the right
parts faster than our competitors. Through our 27 distribution centers,
with inventories that average 148,000 SKUs, we replenish our stores five
nights a week. Each of our stores carry an average inventory of 23,000
SKUs that is individually tailored to each local market based on an analysis
of vehicle registration data, market demographic information and local
customer purchasing patterns. Additionally, almost all of our stores have
same-day access to hard-to-find parts from an extensive inventory selec
-
tion either directly from a distribution center or from one of our 312 hub
stores, which stock an average 45,000 SKUs. Our distribution network
continues to expand in step with our store growth to ensure our industryleading
parts availability is in place for each new store we open, and we
have the capacity to service more than 650 additional stores within our
existing distribution footprint. Our industry-leading parts availability and
our well-trained, experienced Team of Professional Parts People, who are
ready and eager to provide superior customer service each day an existing
or new store opens, are key factors in our mission to be the dominant sup
-
plier of parts in all markets.
Our long-term strategy of profitable growth focuses on maintaining and
enhancing our existing store base and distribution network, organically
growing through new store openings, and consolidating the industry
through the prudent acquisitions of existing auto parts suppliers. During
2016, we successfully opened 210 net, new stores throughout the U.S. and
are excited to open our 5,000th store in 2017, as we plan to open 190 net,
new stores in the coming year. We continue to be very pleased with our
new store performance, resulting from well-trained, professional new store
teams supported by industry-leading parts availability, and our promote
from within philosophy ensures our new store managers instill the OReilly
Culture in every new Team Member. During 2016, we also grew through
the acquisition of Bond Auto Parts. With a remarkably similar company
culture and history, combined with a complementing geographic footprint
in New England, Bond Auto Parts fit perfectly into our criteria for acquisi
-
tions. We will continue to look for strong automotive aftermarket chains,
such as Bond Auto Parts, that mirror our Company Culture, enhance our
national footprint and generate a strong return on our investment.
An unwavering commitment to excellent customer service and our relent
-
less focus on profitable growth generated $978 million in free cash flow
during 2016, and our ongoing initiative to optimize our capital structure
allowed us to increase our leverage during the year through the issuance
of $500 million in senior notes. After reinvesting back into our business in

2016, we returned over $1.5 billion of our excess capital to you, our shareholders,
through our share repurchase program. Since the inception of our
share repurchase program in 2011, we have returned $6.9 billion through
the repurchase of 56.9 million shares, at an average price of $120.50 per
share, and in 2017, we plan to continue to prudently repurchase shares as a
tool to continue to deliver value, after we have exhausted all other means of
profitably reinvesting in our business.
As we plan for the future success of OReilly Auto Parts, we will never forget
that our Team Members and our Culture are the keys to our continued
profitable growth. We are very proud of the depth and experience levels
we have developed over time in our management Team, and we understand
that long-term succession planning is a vital key to maintaining our
Culture. We continue to rely upon our promote from within philosophy
to accomplish this goal, and with that focus in mind, we announced the
promotions of Greg Johnson and Jeff Shaw to Co-Presidents of OReilly in
February of 2017. Greg began his OReilly career 34 years ago as a parttime
distribution center Team Member, and Jeff began his OReilly career
28 years ago as a parts specialist in our stores. Both of these experienced
leaders have dedicated their careers to the success of OReilly, progressing
their way up through the Company with hard work and a relentless
focus on providing excellent customer service. Greg and Jeff are the very
embodiment of the OReilly Culture, and they will continue to perpetuate
our Culture throughout the Company in their new positions for many
years to come.
Once again, we would like to thank all of our Team Members for their
dedication to our ongoing success. We would also like to thank you, our
shareholders, for your trust in our Company, and we look forward to
extending our long track record of profitable growth into 2017

