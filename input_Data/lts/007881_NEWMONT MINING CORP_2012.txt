Dear Stakeholders,
It was a great honor to take the helm as Newmonts President and Chief Executive
Officer on March 1, 2013. I have spent my first year here building a deep knowledge
of our operations and projects and laying the foundation to improve our competitive
position. The next chapter of Newmont history will be marked by a relentless focus
on creating value for our shareholders. To do this, my priorities are to:
 Improve safety, efficiency and costs at our existing operations and strengthen the
business ability to thrive in all cycles;
 Restrict capital investment to assets and projects that generate returns above our
cost of capital and represent a good strategic fit;
 Build significant cash flow through profitable production;
 Allocate capital to shareholder returns and prudent growth projects while
maintaining a strong balance sheet;
 Continue to give investors leverage to gold price through our gold-priced linked
dividend; and
 Maintain or regain the trust and support of our host governments and communities
through industry-leading social and environmental practices.
2012 Performance and 2013 Outlook
In 2012, we reduced our total injury rate by six percent and in the last quarter of the
year, we achieved our lowest injury rate in company history. However, this strong
performance was overshadowed by the loss of our colleague Alan Campbell at our
Exodus mine in August 2012.

In 2013, we will continue to move toward our goal of zero harm. Safety is a shared
accountability and every employee will now be evaluated on his or her contribution
to meeting our collective goals. Safe operations are productive operations and
strengthening our performance will deliver benefits to all Newmont stakeholders.
We generated significant value in 2012,
delivering net income from continuing
operations of $1.9 billion and cash flow
from operations of $2.4 billion on an
attributable production base of 5.0 million
ounces of gold and 143 million pounds
of copper. Gold and copper production are expected to remain stable in 2013, but
increases in Africa and Indonesia are expected to boost Newmonts free cash flow
significantly in 2014 and 2015.
In 2012, Newmont returned record regular dividends of $695 million to shareholders.
At $1.40 per share, our dividends were the highest in the gold industry. We plan to
continue to offer superior returns through our gold price-linked dividend, which has
returned approximately $1.2 billion to shareholders since its inception in April 2011.
We replaced reserve depletion for the fourth consecutive year and recently declared
99.2 million ounces of gold reserves and 9.5 billion pounds of copper reserves on an
attributable basis. Key additions include 2.9 million ounces of gold reserves at our
Merian project in Suriname and 2.6 million gold inferred resources at our Long Canyon
project in Nevada. We reduced our exploration spend by 12 percent in 2012 and will
trim it by another 25 percent in 2013 as we focus on our best opportunities.
In 2012, we began to report our all-in sustaining costs under the evolving World
Gold Council definition. Our ability to achieve cost reductions of $130 million in 2012
brought these costs down to $1,149 per ounce of gold. We expect all-in sustaining
costs to remain flat this year, and will offset the effects of inflation and lower grades
by reducing our administrative, exploration, advanced projects and sustaining capital
expenditures by between 15 and 20 percent in 2013.
Future focus
Newmonts operations are maturing, driving operating costs and capital expenditures
upward. Growth is also becoming more capital-intensive as we venture into remote
regions with increasing infrastructure requirements and higher resource constraints.
At the same time, investors and host countries are expecting a bigger share of the
value Newmont generates, and better safety, social and environmental performance
than ever before. Taken together, these dynamics create an imperative for change.

Business as usual is no longer an option. We need to build on our strengths and
address our weaknesses to reposition Newmont as an industry leader, and compete
successfully for investor dollars, resources and talent.
Newmont turns 92 this year and has a strong record of creating value for its
shareholders, treating its host communities with respect, and safeguarding the
environment. We pride ourselves on our ability to work safely in tough operating
environments and to deliver value from our assets. None of this will change on
my watch.
We remain bullish on the outlook for gold and copper due to strong and growing
investment demand, rising standards of living and relaxed monetary policies. While we
are cautious in our planning assumptions,
we believe these trends will continue
to support strong gold prices  and we
will continue to offer our shareholders
superior value through our gold pricelinked
dividend.
Our profitability is improving and we
expect to increase free cash flow
significantly in 2014 and 2015 at current
metal prices with the start-up of Akyem and the transition to higher ore grades at Batu
Hijau. We have begun to turn the tide in controlling our costs but we have more to do.
Finally, we are becoming more disciplined about allocating our capital investment to
projects with robust fundamentals.
At the heart of our business is a strong, geographically diverse asset portfolio and
reserve base backed by a talented team. These are the assets and attributes we take
great pride in, but to break out of the pack, we also need to improve how we deliver
our strategy for 2013:
Health & Safety
Our attention to safety is good but needs to be better. Our priorities are to create a
broader sense of accountability with our employees and contractors and apply our
standards and procedures consistently everywhere we operate to reach our goal of
zero harm.
Operational Excellence
We have made good progress on cost control and capital discipline, but we must
change our trajectory. We launched our Full Potential program  a systematic
approach to move our operational performance to industry leading status across
key metrics  earlier this year.

Value creation
We are reconfiguring our Technical Services program to improve how we leverage
technology and innovation to deliver value. Across the entire business, our goal is to
strengthen our managerial focus on execution, and deliver on expectations without fail.
Growth
We are building more rigor into our capital allocation processes and will continue
to delay or stop projects until I am comfortable that the business cases, technical
accuracy and social acceptance warrant funding.
People
We run an international business and we need to build our capabilities and our
diversity to succeed. I am appointing new leaders to complement our existing team
and shifting our focus to maximizing profitability with a much greater sense of urgency.
Sustainability & External Relations
We need to do a better job of understanding and delivering what our government
and community stakeholders need and want  whether it relates to water, jobs or
investment agreements. One example is our work to build reservoirs and rebuild trust
in Peru in order to realize the full potential of our Conga project.
I hope this gives you a sense of my priorities as we continue on our path toward
delivering top quartile shareholder returns while leading the industry in safety,
environmental stewardship and social responsibility.
We are building on a solid foundation, and I know you join me in acknowledging our
outgoing leaders for their many contributions to Newmonts success. These include
Richard OBrien, former CEO, Russell Ball, who will be stepping down as CFO later this
year, and John Prescott, who has guided us as a Board Director since 2002.
Thank you for your continuing interest and support.
Sincerely,
Gary Goldberg
President and Chief Executive Officer