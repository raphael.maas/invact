To Our Shareholders
In November 2007, we marked the milestone of our 10th
anniversary as a public company. Over the past decade, we
have successfully executed our growth strategy of partnering
with outstanding boutique asset managers and leveraging our
scale to enhance the growth and operations of our Affiliates.
Through the strong organic growth of our Affiliates, as well as
accretive new investments, we have generated compound
annual growth of 21 percent in Cash Earnings Per Share over
that time, as compared to four percent growth in the S&P
500 Index.
We have created substantial value for our shareholders over
the long term by building a strong and diverse business that
includes many of the worlds leading boutique asset managers.
Our Affiliates have outstanding performance records in their
investment disciplines and established reputations for
providing excellent client service, and they are well-positioned
to continue to deliver superior results for their clients and
generate organic growth at their firms. We have increased our
exposure to fast-growing asset classes, such as international
equities and alternative investments (including through our
2007 investments in two high quality alternative managers),
which now account for 30 percent and 20 percent of our earnings, respectively. We also continue to increase our exposure
to fast-growing client segments worldwide  non-U.S. client
assets at our Affiliates are approximately $85 billion, or 30
percent of our total assets under management.
AMG has a proven history of delivering strong and consistent
earnings growth even in challenging market environments,
such as those from 2000 to 2003, and the volatile markets in
the second half of 2007. With over 300 products across a
wide array of investment styles and asset classes around the
globe, AMG is highly diversified, and the increased breadth and scale of our business has positioned us to continue to
deliver strong results for our shareholders. In the United States,
we participate broadly in domestic equity investment styles,
from deep value to aggressive growth, as well as quantitative,
real estate and other alternative strategies. Our earnings
stability is enhanced by our substantial international client
base, as well as the increasingly global profile of our
investment products, including international developed market
equities, emerging markets equities, and multi-strategy,
currency and market neutral strategies. Finally, our increasing
exposure to alternative investments provides us with earnings
that are generally less correlated to the broader equity markets. 
In 2007, our participation across a range of investment styles and asset classes
around the globe generated strong earnings growth, with Cash Earnings Per Share
of $6.65, or growth of 17%, and EBITDA of $418 million, or growth of 22%. We believe international equities will continue to benefit from
long-term secular growth trends, and our participation in
this area will generate strong growth going forward. Two of
our largest Affiliates, Tweedy, Browne Company and Third
Avenue Management, recently reopened highly regarded
international equity products based on their view that the
current market environment has produced a growing number
of new investment opportunities worldwide. In addition, our
Affiliate Genesis Investment Management is one of the largest
and most experienced emerging markets equities managers
in the industry, with outstanding long-term investment
performance and excellent prospects for continued growth.
The environment for domestic growth equities products,
which contribute 25 percent of our EBITDA, was strong for
much of the year, and we had excellent results among our
growth equities managers. Affiliates specializing in this area
offer a broad range of outstanding products with superior
absolute and relative performance over the long term. As a
group, our Affiliates, including Friess Associates, TimesSquare
Capital Management, Frontier Capital Management
Company, and Renaissance Investment Management,
continued to build on their impressive one-, three- and fiveyear performance records. Most notably, Friess Associates,
manager of the Brandywine Funds, was a finalist for
Morningstars Domestic-Stock Manager of the Year award.
In the domestic value equity area, which contributes 20
percent of our EBITDA, we have an outstanding array of
products managed by such highly regarded and experienced
managers as Tweedy, Browne, Third Avenue and Systematic
Financial Management.
In the growing alternative investments segment of our
business, we have enhanced the diversity of our product mix
to include more than 40 distinct investment strategies, many
of which have low correlation to the equity markets. Our
alternative investment products are focused on a range of
strategies, including distressed securities, quantitative global
macro, activist investing and credit alternatives. Given
the breadth of our alternative product offerings, we have
significantly increased the potential contribution of performance fees to our earnings, and we expect that
performance fees will continue to provide a meaningful
contribution to our earnings growth in the future.
We further expanded our exposure in the alternatives area
during 2007 by completing accretive investments in two high
quality firms with outstanding performance records,
BlueMountain Capital Management and ValueAct Capital.
BlueMountain, a leading global credit alternatives manager, has
a proven track record of delivering exceptional absolute returns
for its clients. ValueAct is a premier active value investment
manager with strong prospects for continued growth. Both of
these firms offer products with little correlation to our current
portfolio of performance-fee products.
We continue to enhance our Affiliates growth and
profitability through our distribution platforms both in the
United States and internationally. Around the globe, our
Affiliates have already demonstrated their strong appeal to
non-U.S. clients, but we see additional opportunities to
support client relationships in key international markets,
particularly markets where institutional investors are
increasingly demanding the expertise of outstanding boutique asset managers. Over the past year, we launched our global
distribution platform with the opening of our Sydney office
(in early 2007) to support institutional marketing initiatives
in Australia, followed by the opening of our London office
(in early 2008) to support marketing efforts throughout the
Middle East.
In the U.S. intermediary-driven marketplace, Managers
Investment Group continues to provide AMG Affiliates
with a platform to meaningfully expand the distribution of
their products to retail investors. Managers team of
experienced sales professionals distributes single- and multimanager Affiliate mutual fund and separate account products
to intermediaries, including broker-dealers, banks and
independent advisors, as well as in the sub-advisory and
defined contribution marketplaces.
In addition to the growth of our existing Affiliates, AMG
continues to grow through accretive investments in new
Affiliates. We have an established track record of successful
investments, and an excellent reputation as an innovative and
supportive partner to our Affiliates. AMG is widely regarded as the succession planning partner of choice among growing
boutique firms, and prospective partners are also increasingly
attracted to the range of strategic support services we offer,
from initiatives that can help firms expand their product
offerings and broaden their distribution capabilities, to our
legal and compliance resources. Our success in partnering
with boutique firms positions us to execute upon a range of
very attractive investment opportunities within the universe
of both traditional and alternative managers.
We have also positioned our capital structure to support our
growth opportunities. Our business generates strong and
recurring free cash flow, and we are disciplined in our
approach to allocating our capital when making investments
in new Affiliates, investing in growth initiatives on behalf
of our existing Affiliates, and repurchasing our stock. AMG
maintains an investment grade rating and a strong balance
sheet, with substantial liquidity and financial flexibility.
In 2007, we enhanced our financial capacity through the ssuance of a $500 million convertible trust preferred security
and an increase in our credit facility, to $950 million. More
recently, in early 2008, we further strengthened our balance
sheet through the conversion to equity of our $300 million
floating rate convertible securities and $300 million
mandatory convertible securities.
In conclusion, through the continued execution of our
business strategy  partnering with the highest quality
boutique asset managers in the world, and leveraging our scale
to support and enhance their growth  we are confident that we will generate substantial value for our shareholders in the
future. We are grateful to our Affiliates, employees, Board of
Directors and service providers for their contributions to our
ongoing success, and to our shareholders for their support.
Sean M. Healey
President and Chief Executive Officer