To our Shareholders,

First some challenges we are facing and then a summary
of all the good things we accomplished in 2011.
2011 was another challenging year for the U.S. economy
and the financial markets in general. The growth of GDP in
the United States was an anemic 0.85 percent for the first
half of 2011 and only picked up slightly to end the year at an
estimated 1.70 percent. The financial markets experienced
significant volatility related to sovereign issues in Europe,
discontent on the political front in Washington, D.C., and the
mid-year downgrade of the United States� credit rating by
Standard & Poor�s. All of these issues certainly had effects
on the operations of Duke Realty.
On the challenging side, the occupancy in our suburban
office portfolio continues to languish as high unemployment
and low job growth continue to impact our office-using
customers. Our suburban office occupancy continued to
average just above 85 percent during the year. This is down
from an historical rate of 92�95 percent occupancy in a
normal, growing economy.
The office leases that we did complete during the year reflect
a tenants� market as rental rates held relatively flat to down
a couple percent, and tenant improvement and concession
costs remained high. This trend is likely to continue into 2012
as unemployment appears as if it will stay at elevated levels
for at least another year.
The other area where we are experiencing very low activity is
in our new development business. Development starts in 2011
at $203 million were up over 2010 starts; however, 2009, 2010
and 2011 have been three of the lowest annual development
start levels in the last 15 years. What is particularly startling is
that we have started development of just two new industrial
buildings to be held in our portfolio in the last three years. 

This is not a reflection on Duke Realty as we remain one
of the premier industrial developers in the country. This is a
reflection on the state of the economy, with virtually no new
industrial development occurring. As a focused developer
with a strategically located land bank for future development,
this has been a particularly frustrating period. Our belief is
that new development opportunities will improve in 2012,
but will still be well below historic normal levels.
In spite of these economic headwinds, we were able to make
great progress on our strategic objectives and completed
another strong year of core operations.
First, the operational highlights. We signed nearly 25 million
square feet of leases in 2011. This is a solid year for any
economic environment. Approximately 76 percent of our
leasing activity was in our industrial portfolio. This leasing
activity led to overall occupancy above 90 percent at yearend, the highest in a number of years. We also renewed
nearly 70 percent of our tenants, a tribute to the complete
customer satisfaction program we have in place to take
care of our tenants.
The bulk industrial side of our core business continued
to show improvement during the year. Occupancy in
our industrial portfolio increased nearly 1.5 percent during
the year. This was driven in large part by an increase in
warehouse needs of major retailers and reflects some
growing strength in consumer demand during 2011. However,
our occupancy in this piece of the business, at 91.9 percent,
is still well below a normal rate of over 95 percent. We expect
to see this area of the business continue to improve in 2012.
The medical office sector of our business continued to be a
steady performer during 2011. More than 59 percent of our
new development starts during the year were medical offices.
Overall occupancy in our medical office portfolio increased
by nearly 5 percent during the year as we had good success
leasing up speculative space in a number of our recently
developed projects. We expect to continue to see good
things in the medical office business in 2012.
The other area where we had a very successful year was
on our asset repositioning strategy. During the year, we
completed over $1.6 billion of asset sales of primarily
suburban office assets, and we acquired $747 million of
primarily bulk industrial and medical office assets. Through
these transactions, we made significant strides towards our
mix of assets by product type goal of owning 60 percent bulk
industrial, 25 percent suburban office and 15 percent medical
office properties. We are now at 54 percent industrial, 33
percent suburban office and 13 percent medical and retail.
The key transaction in this repositioning was the $1.06 billion
sale of suburban office properties located primarily in the
Midwest to the Blackstone Group. The market conditions of
historically low interest rates and our ability to deliver these
assets substantially unencumbered, allowed us to complete
this strategically significant transaction at what we believe is
very favorable pricing for us. The suburban office properties
in many geographic markets require significant capital
expenditures to operate and re-lease so we continue to focus
on acquiring and developing bulk industrial and medical
office buildings which we believe deliver us an equivalent and
more stable net cash return. We are now one of the largest
industrial-focused, publicly traded companies in the nation.
We also have one of the best industrial and medical office
portfolios you can own through a public company.
We also made solid progress on our capital strategy and
deleveraging goals. We reduced our outstanding debt and
redeemed some of our higher coupon preferred stock with
proceeds from our asset sales. We continue to focus on
driving higher debt-service coverage ratios by generating
more cash flow from our properties, taking advantage of
low interest rates and reducing our debt with a portion of
our property disposition proceeds. We are focused on doing
this without reducing our cash flow, or adjusted FFO per
share, and we have been successful at that goal. We continue
to maintain our investment-grade debt ratings allowing
us access to the debt markets at very competitive rates.
In 2012, you will see us continue to improve our leverage 

metrics, assuring a solid balance sheet poised for growth
opportunities as economic conditions improve.
One other operating highlight for 2011 is the successful
completion of our third-party construction contract for
the Department of Defense (DoD) in Washington DC, what
we have referred to as the BRAC project. This is the largest
development project in which Duke Realty has ever been a
part. This project was a true partnership with our customer,
the DoD, and our contracting friends. The good news
for all of us as taxpayers is that we came in on schedule
and under budget! This project truly demonstrates
Duke Realty�s talent and skills as a top-quality property
developer. Thanks to all involved.
I would also like to recognize two of our directors who
are leaving our Board after many years of service to
Duke Realty. Geoff Button and Barry Branch who have
been on our Board for 18 and 13 years, respectively, have
added tremendous counsel and guidance to our company
over those many years. We will miss them and cannot
thank them enough for all they have done for Duke Realty.
We�d also like to welcome Alan Cohen and Peter Scott
to our Board. We look forward to learning from their
experience and having them be part of our team.
And finally, thanks to all of our associates at Duke Realty
who are second to none in our industry. And, thanks to
you, our shareholders, for all of your trust in our strategy
and your support of our efforts. We are proud of our
performance in 2011 and look forward to making 2012
another outstanding year for Duke Realty.
Dennis D. Oklak
Chairman and Chief Executive Officer