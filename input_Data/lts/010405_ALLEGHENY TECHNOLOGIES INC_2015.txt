MESSAGE FROM THE CHAIRMAN, PRESIDENT AND CEO

2015 WAS AN INCREDIBLY
DIFFICULT YEAR
Slow growth in both domestic
and global economies and the
resulting rapid fall of prices for
most commodities adversely impacted
our business. Average monthly LME
nickel prices declined 45 percent from
December 2014 through December
2015. The base price for the most
common commodity stainless sheet
product decreased 25 percent to
an historic low that was reached in
December 2015. ATI sales to the
Oil & Gas/Chemical & Hydrocarbon
Processing Industry market, our second
largest market, were down 28 percent
compared to 2014 due to significant
reduction in the price of both crude
oil and natural gas. Stainless steel and
most commodities remain in global
oversupply.
ATI sales in 2015 were $3.7 billion.
Regrettably, net loss attributable to
ATI was $378 million, or $(3.53) per
share. Results included $348 million
of pre-tax charges for goodwill and
long-lived asset impairments in our
Flat Rolled Products segment, inventory
reserves reflecting impacts of declining
raw materials values, and costs of
severance and facility idling actions.
We maintained a solid liquidity position
with approximately $150 million in
cash on hand at the end of 2015 and

implemented a $400 million assetbased
domestic lending (ABL) facility,
which was undrawn at year-end
2015. The ABL facility contains no
leverage or interest coverage ratios
and is collateralized by the accounts
receivable and inventory of ATIs
domestic operations. For the fourth
quarter 2015, we reduced our quarterly
cash dividend to $0.08 per share from
$0.18 per share in order to conserve
cash. Cash generation from operations
will be a key focus throughout 2016.
Capital expenditures in 2015 were
$145 million. We expect capital
expenditures in 2016 of approximately
$240 million as we make the final
payments for our Hot-Rolling and
Processing Facility (HRPF), complete
the expansion of our nickel-based
alloy powder facility and our titanium
investment casting facility, and expand
our STAL joint venture. Looking forward,
we believe annual capital expenditures
will be in the range of $75 million to
$100 million beginning in 2017 through
the end of the decade.
Our safety focus continued across all
of ATIs operations. Our 2015 OSHA
Total Recordable Incident Rate was
1.88 and our Lost Time Case Rate
was 0.29 per 200,000 hours worked,
which we believe to be competitive
with world-class performance in heavy
manufacturing industries.
We continued our strategy of
focusing on key global markets that
have attractive long-term growth
expectations and require products
that have significant technical barriers
to entry. We have taken and will
continue to take actions to maintain
solid liquidity, improve our competitive
position, and return ATI to sustainable
profitability.
MULTI-YEAR STRATEGIC CAPITAL
PROJECTS COMPLETED
In 2015, we completed multi-year
strategic capital projects that
are intended to strengthen and
enhance ATIs leadership in the
production of advanced specialty
materials and components. We expect
these investments to now begin to
provide a return on our invested
capital after extended construction
and qualification phases.
 The Hot-Rolling and Processing
Facility, or HRPF, is in operation.
The HRPF is a critical part of our
strategy to transform our flat-rolled
products business.
 We achieved PQ qualification for
our products used in jet engine
rotating parts made with titanium
sponge from our Rowley, UT facility.
MARKET/CUSTOMER FOCUS,
TECHNOLOGY LEADERSHIP, AND
UNSURPASSED MANUFACTURING
CAPABILITIES
I believe ATI is unmatched in the
specialty materials industry when
it comes to technology and new
product development. We have an
excellent record when it comes to
quality and delivery performance.
And we are unsurpassed when it
comes to manufacturing capabilities.
ATI is a technology leader in the
alloy systems and products that we
produce. Technology leadership
is an important part of our value
proposition to strategic customers.
We have invented and introduced
new alloys and products that provide
the enabling technology to meet the
increasingly more difficult demands
of global markets including the
aerospace, oil & gas, and medical
markets. The production of these
new alloys and products to meet the
demanding customer requirements
and specifications often requires
our unsurpassed manufacturing
capabilities.
Today, our ability to manufacture
industry-leading mill products,
near-net shapes, parts, and
components from our range of alloy

systems, positions ATI with a unique
supply chain that can provide value
to our customers and create value for
our shareholders. Our manufacturing
capabilities are unmatched. They are
modern and unique in the world. None
are better and, in many cases, I can
say that we are the best.
Nearly all of these investments
have been in the United States.
We believe in U.S. manufacturing
and we understand that the ability to
manufacture the specialty materials
products that ATI produces is a
critical and core competency of
the United States.
Our strategy is to be the best at
creating value for strategic customers
and achieve compound annual growth
through business cycles in diversified
global markets. This strategy better
positions ATI for long-term profitable
growth and enhances opportunities
to create value for our stockholders
across business cycles.
We must improve the bottom line
and efficiently use our capital. So,
we must earn a premium to our
weighted average cost of capital.
Currently, ATIs weighted average
cost of capital is about 10 percent.
Our target after-tax return on capital
employed is a minimum of 15 percent
through a business cycle. Because our
end markets are cyclical, we may not
earn 15 percent every year-some years
it should be more, some years it may
be lessbut through a cycle, trough
to peak, our goal is a minimum of
15 percent after-tax return on capital
employed. Given the nature of the
end markets that we serve, we believe
the business cycle is approximately
five to six years.
Rich Harshman,
Chairman, President and CEO