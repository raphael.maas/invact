TO OUR SHAREHOLDERS
Revenue for fiscal year 2006 was
$2.071 billion and net income was
$380 million. KLA-Tencors fi nancial
position remained strong with cash,
cash equivalents and marketable
securities of $2.3 billion and no
long-term debt.
However, the numbers do not tell the entire story, as
fi scal year 2006 brought other signifi cant challenges.
We discovered that some of our past stock options had
been retroactively priced  primarily those granted
from 1997 to 2002. When we fi rst learned of potential
stock option issues, our Board of Directors immediately
appointed an independent special committee to
investigate. The special committee found that former
executives of the Company did retroactively price certain
stock option grants for all employees who received
those grants, and that we did not account for those
grants correctly. Based on the special committees
fi ndings, we took appropriate actions to address the
issues and verify that proper controls are in place
moving forward.
As a result, we also went through the process of
restating our financial statements for the periods in
question. This annual report includes the results of the
restatement for all periods through June 30, 2006.
With the completion of the internal investigation and
restatement, we are looking ahead and focusing on our
core business activities.

In fiscal 2006, KLA-Tencor set a plan in motion to
sharpen its strategic focus and position the Company
for growth.
After rigorous analysis, the Company has decided to
concentrate on the inspection and measurement
markets that we believe we can lead through product
differentiation. We believe inspection and measurement
will continue to outgrow the semiconductor capital
equipment market and KLA-Tencor will benefit from
this growth. As part of this focus, we also decided that
we will divest from markets where we do not have a
clear strategy to lead the market. This focus will enable
the Company to do what it does best: create inspection
and measurement solutions that solve mission-critical
production problems for our customers.
The semiconductor market is increasingly driven by
consumer appetite for electronics. As a result,
chipmakers need to bring products to market at a
faster rate. Semiconductor manufacturers must
continuously innovate.
To meet these market demands, chipmakers are
introducing new processes, new designs, and new
materials to make smaller, faster, cheaper and cooler
devices  all of which drive the need for greater use
of advanced inspection and measurement solutions.
There is a constant stream of new technologies and
techniques such as immersion lithography and high k
dielectric materials that require in-depth knowledge of
inspection and measurement. Only KLA-Tencor has 30
years of experience helping our customers meet these
demands. We are partnering earlier and earlier with
our customers to help them conquer the challenges of
new product development and translate those products
into full production. Our customers rely on our tools to
maintain high production yields in their fabs, one of the
most important factors driving their profitability.
As the growth in inspection and measurement continues,
we are growing the Company both organically
through new product development and also through
acquisition.
KLA-Tencor has a long history of bringing new products
to market, and fiscal 2006 was no exception.
KLA-Tencor introduced its latest-generation optical CD
metrology system, the SpectraCD-XT. It provides costeffective
inline CD and profile measurements of critical
device structures that help enable early prediction of IC
performance and yield at the 90nm and 65nm nodes.
The 2367 was also announced. It is our fifth-generation
broadband UV/visible brightfield inspection system.
The 2367 is the fastest tool for earliest capture of yieldimpacting
defects on critical layers. An extension of
the widely adopted 23XX platform, the 2367 delivers
increased sensitivity and a 2x faster data rate to help
chipmakers sample more frequently and capture critical
layer defects earlier.
In addition to our broadband UV tool, we had
significant success with our 2800 family of Deep UV
ultra-broadband brightfield inspection tools, introduced
in 2005. Over this past year, we have received major
orders for this technology which has been a critical
tool for our customers for their 65nm and 45nm
development for lithography and etch patterning,
because of its sensitivity and flexibility.
The Starlight-2 is a new platform that delivers the
most cost-effective and comprehensive contamination
control solution for photomask requalification and
reliability assurance, a growing area of customer need.
The Starlight-2 provides semiconductor fabs with
a contamination inspection solution for all types of
photomasks, including mainstream extreme resolution
enhancement technique (XRET) photomasks, at the
65nm node and below.
In addition to our internal product development, we
also positioned the Company for further growth in
the metrology market with the acquisition of ADE.
The purchase strengthens our position in the wafer
manufacturing market and enables KLA-Tencor to
deliver a broader range of metrology products to the
semiconductor market. We also continued to fund
the KT Venture group which is tasked to discover and
nurture next generation technologies. As we look
ahead in 2007, we will continue to pursue acquisition
opportunities that are in line with our corporate
strategy and our business model.
We are pleased with the Companys performance this
past year which is a result of our close partnerships
with our customers. This achievement would not be
possible without our employees and suppliers who
delivered a year we can be proud of. On behalf of the
management team, we would like to thank them for
their efforts.
As we embark on 2007, we will continue to move
the Company forward by executing on our strategy
to be the worlds leading inspection and measurement
company. Over the next year, we will direct our
attention to four key areas: customer focus, targeted
growth, operational excellence and employee development.
We believe these are the right priorities for
our customers, employees and shareholders and will
position the Company for more success in the future.
Rick Wallace
Chief Executive Officer
John Kispert
President and Chief Operating Officer