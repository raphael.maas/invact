                                         to our shareholders
                                                                    CIGNA's 2010 results were strong, with significant
                                                                    contributions from each of our ongoing businesses,
                                                                    powered by a team of 30,000 employees around the
                                                                    world. The CIGNA team is a source of pride as our
                                                                    employees fulfill the global goal of improving life
                                                                    and health for customers and communities
                                                                    everywhere they live and work.
                                               Our strong operational and financial achievements
                                          
                                             were made possible through the continued,
                                                                    effective execution of our three-pronged growth
                                                                    strategy to Go Deep, Go Global and Go Individual,
                                                                    which includes broadening our existing customer
                                                                    relationships and growing our global customer base.
                                                                    We are effectively executing on our growth strategy
                                                                    while maintaining high standards of service and
                                                                    clinical quality for our customers and clients.




     



                 Go Deep, simply put, means expanding our leadership in the targeted
                  markets we currently serve, whether they are geographic, product or
                  buyer segments. In the U.S., this strategy served us well as we ended
                  2010 with 11.4 million HealthCare customers, 3.6 percent higher than
                  at year-end 2009. This customer growth was driven by our sales to
                  mid-sized employers and companies with 51  250 employees, along
                  with attractive growth in our Disability business. Outside the U.S.,
                  we achieved double-digit premium growth in our Health, Life and
                  Accident business, and attractive growth in our Expatriate business.

                  Go Global means building on our established global operations and
                  capabilities in order to expand into new geographies with product lines
                                                                                                WE ARE LEARNING FROM OUR
                  and distribution channels that position us for additional profitable growth
                                                                                                EXPERIENCE WITHIN AND OUTSIDE
                  over the next three to five years. An example of this is the acquisition of
                  Vanbreda International, a Belgium-based company with a strong foothold
                                                                                                THE U.S., WHERE WE CURRENTLY
                  in the European expatriate market. This acquisition made us the largest
                                                                                                HAVE APPROXIMATELY 6.5 MILLION
                  provider of benefits to expatriates and other globally mobile individuals,
                  with more than 700,000 customers. Go Global also means that we will           INDIVIDUAL POLICIES IN FORCE.
                  leverage our broad set of capabilities across borders to meet emerging




                  needs. New product launches in Spain, Korea and China further
                  exemplify our global acceleration.

                  Go Individual refers to the way we run our business  by focusing on
                  the individual needs of everyone we serve, regardless of how he or she
                  accesses coverage with us. We are learning from our experience within
                  and outside the U.S., where we currently have approximately 6.5 million
                  individual policies in force. Our introduction of a new Individual
                  Private Medical Insurance product for expatriates and other globally
                  mobile individuals is evidence of this strategy in action.




                                         THREE-PRONGED GROWTH STRATEGY




                       GO                            GO                            GO
                      DEEP                         GLOBAL                      INDIVIDUAL





                                                               Reflecting strong customer retention and new sales in each of our targeted
                                                               market segments, consolidated revenues increased by 15 percent to $21.3
                                                               billion for full-year 2010, compared to $18.4 billion for full-year 2009. We
                                                               achieved adjusted income from operations* of $1.3 billion, or $4.64 per share,
                                                               reflecting robust earnings contributions from each of our ongoing businesses 
                                                               HealthCare, Group Disability and Life, and International. In fact, adjusted
                                                               income from operations increased by 16 percent in 2010, and we delivered
                                                               strong revenue growth in each of our targeted markets. Shareholders' net
                                                               income for full-year 2010 was $1.35 billion, or $4.89 per share.
                                            

                                               Within our U.S. HealthCare business, our results benefited from strong
                                                               clinical outcomes and targeted customer growth as well as the industry-
                  
                                                               wide impact of lower-than-expected medical cost trend. Approximately
                                            90 percent of our medical customers are in self-insured or experience-rated
                                                               business arrangements with us, meaning that lower medical costs directly
                                                               benefit our corporate clients and their employees in these highly transparent
                                                               programs. We have delivered very good organic membership growth for
                                                               our HealthCare business. Specifically, we realized approximately 8 percent
                                                               growth among mid-sized employers, and approximately 11 percent growth
                    among companies with 51  250 employees. These results demonstrate
                     that our clients and customers recognize the value of our integrated
           
                                                               product capabilities and programs that offer incentives for lowering costs
                                                               by improving health.

                                                               In our U.S. Group Disability and Life segment, our results show the
                                                               direct benefit of our leading disability management programs. These
                                                               programs help employees return to work faster, which increases workforce
                                                               productivity, and importantly, drives cost savings for clients, (employers)
                                                               and customers (employees). In delivering value to our employer clients, we
                                                               achieved top-line growth of 10 percent in our Disability business last year.



                  Our International segment, which includes our Health, Life and
                  Accident, and Expatriate Benefits businesses, delivered strong top-line
                  growth of 21 percent, along with corresponding earnings growth. In this
                  context, we are fully leveraging our established global network of more
                  than 900,000 health care professionals and facilities, and our capabilities
                  to expand into new geographies with differentiated products. Our presence
                  across the globe allows our team to bring new products to market more
                  quickly than others. The strategic acquisition of Vanbreda International
                  further enhanced the growth prospects for our International Expatriate
                  Benefits business.



                                                                                                 CASH AND SHORT-TERM
                  Overall, we continue to have a strong balance sheet and good financial
                                                                                                 INVESTMENTS AT THE PARENT
                  flexibility. In 2010, we met our capital management goals. Specifically,
                  we deployed more than $600 million of capital to fund the Vanbreda             COMPANY WERE $810 MILLION
                  International acquisition; repurchased approximately 6.2 million shares
                                                                                                 AT DECEMBER 31, 2010.
                  of our stock on the open market for $200 million; made a meaningful
                  contribution to our pension plan; and took advantage of the current



                  interest rate environment to refinance a portion of our long-term debt
                  with a substantially lower interest rate. Cash and short-term investments
                  at the parent company were $810 million as of December 31, 2010.

                  In 2010, we acquired Kronos Optimal Health to add to our suite of
                  health coaching resources, and we sold our non-core Intracorp workers'
                  compensation case management business. Both are examples of our steady
                  progress toward our strategic goals. All in all, I feel very good about the
                  strategic, operating and financial achievements we delivered in 2010.


                  In today's rapidly evolving marketplace, there is an increasing need for
                  programs and services that improve health quality while reducing cost.
                  Our aim is to transform the delivery of health care to our customers.
                  We are achieving this by improving engagement through technology,
                  offering innovative health and wellness programs and collaborating
                  with others to improve cost and health outcomes.




                                           We provide information to help people make good decisions about their
                                           health, but we don't view our customers in terms of data and numbers.
                                           We see each customer as a complete individual. For example, in the U.S.,
                                           we launched Chronic Care Support for mid-sized employers, a program that
                                           takes an integrated medical and lifestyle approach. This initiative enables
                                           our health coaches to access an individual's full health history so that they
                                           can understand the whole person and evaluate his or her health risks and
                                           conditions. The coach helps individuals develop a personal health plan
                                           specific to their complete health needs. A second initiative, "Better Health.
                                           Guaranteed.SM" is designed to help individuals at risk for diabetes, stroke,
        OUR ACCOUNTABLE CARE
                                           cancer and other illnesses. These risk factors will potentially comprise
                                           85 percent of a company's health care costs in the next few years.
        ORGANIZATIONS, IN WHICH WE

                                           Our Accountable Care Organizations are another example of a total health
        STRENGTHEN PARTNERSHIPS
                                           improvement approach that increases engagement, improves outcomes,
                    WITH PHYSICIANS,
                                           and ultimately lowers cost by aligning information and financial incentives
                                           with health care professionals. CIGNA uses technology to provide more
                    HOSPITALS AND THEIR
                                           information to the primary care physicians responsible for monitoring and
                    PATIENTS, IS ANOTHER
                                           facilitating all aspects of an individual's medical care, in collaboration with
                    EXAMPLE OF A TOTAL     a team that includes nurses and health educators. Today, we have more than
                                           120,000 individuals and more than 1,000 physicians in Accountable Care
        HEALTH IMPROVEMENT APPROACH
                                           Programs spanning 11 states. Recent results from one of our pilot programs
        THAT WE EMPLOY TO INCREASE         are encouraging and show that our performance regarding gaps in care,
                                           which account for more than half of the errors and complications in
        ENGAGEMENT, IMPROVE HEALTH
                                           medical care in the U.S., is 10 percent better for individuals in Accountable
        AND ULTIMATELY LOWER COST.         Care Organizations.



                                           POSITIONED FOR SUCCESS

                                           CIGNA's diversified portfolio of programs and innovative solutions in the
                                           U.S. and abroad enables us to meet changing customer and market needs
                                           today and in the future. Opportunities resulting from the reshaped
                                           regulatory environment brought about by health care reform within the
                                           U.S., as well as the increasingly global economy, are well-aligned with many
                                           of the long-standing strengths of our organization. For example, over the
                                           past decade, we have continued to build our International operation from
                                           the ground-up, featuring a unique direct-to-consumer distribution model
                                           in partnership with affinity groups such as banks.



                  Today, our global Health, Life and Accident business is well-established
                  and thriving, consistently delivering double-digit, top- and bottom-line
                  growth over the past several years, while maintaining strong profitability.
                  This business is also poised to benefit from attractive growth opportunities,
                  particularly with the emerging middle class in expanding economies
                  around the world.

                  We have demonstrated our ability to adapt in changing markets. A key
                  part of our strategy is to pursue additional opportunities  beyond our
                  current, well-established, market positions  in high-growth segments,
                  with a particular emphasis on "retail," or individuals of all ages. We expect
                  to harness our current capabilities to serve those markets in the future.
                  One resource we will optimize is the vast expertise we have in our
                  International operations, particularly in alternative distribution methods
                  and supplemental market capabilities. We distribute our Health, Life and
                  Accident products directly to individuals through telemarketing, direct
                  response TV and the internet, rather than traditional captive broker or
                  agent models. We have the unique ability to maximize our global expertise
                  and capabilities in a wide range of health systems around the world.



                  A FINAL WORD

                  We enjoyed positive, forward momentum as we headed into 2011, and
                  I am confident in our ability to achieve our full-year 2011 strategic, financial
                  and operating goals. I believe our diversified portfolio of businesses enables
                  us to effectively meet evolving customer and market needs  today and in
                  the future.

                  This is a time of extraordinary change and extraordinary opportunity.
                  All of us at CIGNA are excited about the road ahead, guided by our mission
                  to improve the health, well-being and sense of security of those we serve
                  and our commitment to deliver outstanding value to our shareholders.
                  I want to thank every CIGNA professional for the outstanding contributions
                  they made toward our successful 2010, and you, our shareholders, for the
                  confidence you have placed in us through your investment.

