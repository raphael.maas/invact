DEAR SHAREHOLDERS

2017 was an incredible year of evolution for your Company, one
that continued our focus on our long-term growth strategy and
further solidified our position as a leading global entertainment
brand. MGM Resorts reported consolidated Net Revenues of
$10.8 billion, Net Income attributable to MGM Resorts of
$2.0 billion and Adjusted Property EBITDA of $3.2 billion.
Our Domestic Resorts achieved Net Revenues of $8.3 billion
and Adjusted Property EBITDA of $2.5 billion. In Macau,
MGM China continued to perform well with Net Revenues of
$2.0 billion and Adjusted Property EBITDA of $525 million.
In 2017, MGM Resorts focused on further strengthening
the organization in alignment with our strategic and
operational goals as �One Company, One Culture.� As we
continue to build on our position as one of the world�s leading
entertainment brands, we remain committed to delivering
excellence in four critical areas.

IMPROVING OPER ATIONAL STRENGTH AND SERVICE DELIVERY

This year, we built upon our highly successful Profit Growth
Plan, which is integrated and deeply embedded into the fabric
of our Company, with Continuous Improvement to further
drive operational efficiency and financial profitability. In 2017,
our domestic resorts produced same-store Adjusted Property
EBITDA margins of approximately 31%, an improvement of 141
basis points year over year.
W hile operational efficiency is crucial to delivering
shareholder value, our long-term success also depends on our
ability to continually raise the bar in delivering outstanding
service to our guests. Our new service-training program called,
�We Are the Show� elevates our expectations of service delivery
and further unifies the MGM Resorts brand experience across
our wide portfolio of assets and experiences.
Meanwhile, our efforts to reduce leverage, including
redemption of our most expensive debt, have resulted in an
improved credit profile and stronger financial position. We have
strengthened our balance sheet and are on track to reduce our
consolidated net leverage to three to four times by the end of
2018 to maximize the efficiency of our capital structure. 

STR ATEGICALLY INVESTING TO CAPTURE OPPORTUNIT Y
IMPROVING OPER ATIONAL STRENGTH AND SERVICE DELIVERY
We continue to target and prudently invest in new opportunities
to grow our competitive advantages. We are expanding on our
position as the largest convention operator in Las Vegas, with
over three million square feet of convention and meeting space
across our Las Vegas Strip properties, to address growing demand
and to further increase our customer base. MGM Resorts added
200,000 square feet of convention facilities at ARIA in early
2018 and will add another 250,000 square feet of new space to
the MGM Grand Conference Center at the end of 2018.
Additionally, the transformation of the Monte Carlo Resort
and Casino into the Park MGM and NoMad Hotel in Las Vegas
is expected to be completed by the end of 2018. Park MGM�s
design is inspired by urban parks and gardens and combines
timeless European style with an intimate, modern sensibility.
When completed, Park MGM is expected to house approximately
2,700 guest rooms while NoMad Hotel Las Vegas is expected to
feature 292 guest rooms and suites. The property anchors the
mid-Strip Park Las Vegas neighborhood with the T-Mobile� Arena,
Park Theater, New York-New York and ARIA resorts.
Finally, in October MGM Resorts International sold the
real estate assets of MGM National Harbor to its affiliated real
estate investment trust, MGM Growth Properties, for nearly
$1.2 billion. The deal, which was the largest real estate deal in the
history of Prince George�s county, provided financial flexibility
for MGM Resorts.

EXPANDING OUR GLOBAL ENTERTAINMENT BR AND

In regards to brand building, 2017 was a watershed year.
MGM Resorts sold over 8 million tickets in 2017 across
almost 8,200 events, bringing �WOW� moments to our guests
including concerts, Cirque du Soleil� shows, and marquee boxing
events. We look to build upon this success next year. MGM
Resorts announced a two-year special engagement with Grammy
Award-winning superstar Lady Gaga, whose shows will be held at
the new, 5,300-seat Park Theater. Lady Gaga joins our already
star-studded resident shows with the likes of Bruno Mars, Cher
and Ricky Martin.
Our home city of Las Vegas, also cemented itself as an
exciting new sports destination and MGM Resorts is proud to
have played a role in helping to bring professional sports to the
city. Las Vegas is now home to the NHL�s Golden Knights, and
our own WNBA team, the Las Vegas Aces, will make its debut
in 2018 at the Mandalay Bay Events Center. We look forward
to the continued evolution of the city in sports, including the
relocation of the NFL�s Raiders franchise into their new home
just west of Mandalay Bay.
Further, consistent with our global entertainment brand, we
launched our first-ever corporate brand campaign, �Welcome
to the Show.� The integrated campaign launched nationwide on
national TV, during the Emmys, in NYC�s Times Square and
across social media platforms.
Looking forward, we celebrate a series of international
and domestic resort openings in 2018 that will enhance the
MGM brand.
Debuting in February 2018, MGM COTAI in Macau is the
latest addition to the MGM Resorts portfolio in China. Designed
as the �Jewel Box of Cotai,� this integrated resort offers 1,400
hotel rooms and suites, meeting space, a high-end spa, retail
offerings and food and beverage outlets. It also offers several
innovative creations, including the first international Mansion
at MGM and Asia�s first dynamic theater. The theater, installed
with the world�s largest permanent indoor LED wall, is the size
of three tennis courts and can seat up to 2,000 people. MGM
COTAI also houses a unique art collection of over 300 pieces
of contemporary art and will become Macau's most creative
culinary destination, featuring novel dining concepts created by
world-renowned celebrity chefs. We are especially proud that
MGM COTAI is the largest property and the first private sector
project ever to achieve the China Green Building (Macau) Design
Label Certification.
In the United States, we will further expand our East Coast
presence with MGM Springfield in Western Massachusetts,
scheduled to open by September 2018. Combining new
construction with revived historic buildings, the development
will offer a 250-room boutique hotel, more than 125,000 square
feet of gaming space, a spa, retail and dining options.
Also in Asia, the Bellagio brand will expand with the debut of
the first international Bellagio hotel in Shanghai. The focus for
the new building rests on recreating the opulence and signature
style of the Bellagio in Las Vegas, combining Italian fashion and
design with local culture.
Meanwhile, we are excited about our first-ever project in the
Middle East, which is the construction of a premier, two million
square-foot MGM Resorts-branded resort on 26 acres on Dubai�s
famed Jumeirah beachfront. The project, fully funded by our
partners, allows us to expand our MGM and Bellagio brands while
contributing our design, management and operating expertise. 

C O M M I T M E N T T O C O R P O R A T E S O C I A L R E S P O N S I B I L I T Y 

In 2017, we continued our long-standing commitment
to Corporate Social Responsibility as a fundamental
business imperative. Through our practices in Diversity and
Inclusion, Philanthropy and Community Engagement and
Environmental Sustainability, we enhance our Company�s
competitiveness and long-term business value, and strengthen
our communities as well.
This past year, we made inclusion a centerpiece of
leadership education, affirming the expectation that at MGM
Resorts, we produce leaders who are skilled both in technical
disciplines as well as in leading diverse teams to produce
exceptional value. We received many exemplary rankings by
third-party validators for our inclusion and supplier diversity
practices and stood proudly and publicly for equality and
human dignity on a number of social issues.
Strong and stable travel destinations are integral to our
success and we make substantial investments to help improve
the quality of the communities in which we operate. We are
proud that our employees logged more than 114,000 hours
of volunteerism in our giving system and nearly 68% of our
domestic employees made donations to more than 1,500
nonprofit organizations through our MGM Resorts Foundation
(the "Foundation"). Following the senseless and horrific events
of October 1st, the Foundation donated a total of nearly $5
million to the Las Vegas Victims fund and various nonprofit
organizations that support first responders. For our community
service efforts, Points of Light, the world�s largest organization
dedicated to volunteer service, recognized MGM in the 2017
Civic 50 as one of the 50 most community-minded companies
in the nation.
Our leadership in environmentally responsible operations
and development continuously focuses on water and energy
conservation, materials management, green supply chain,
green building and employee education. MGM was formally
recognized in 2017 by the U.S. Department of Energy (�DOE�)
as part of its Smart Energy Analytics (�SE A�) campaign for
use of advanced building analytics across more than 50 million
square feet of building space.

MA XIMIZING SHAREHOLDER VALUE:
A STRONG FOUNDATION TODAY, CONTINUED GROW TH TOMORROW

The collective strength of our organization, the power of our
brand and our continued focus on executing our strategic plan
continues to deliver results for our shareholders.
Notably in 2017 we achieved two critical, long-term milestones:
the implementation of a quarterly dividend of $0.11 per share,
which we have since increased to $0.12 per share, and the launch
of a $1 billion share buy-back program. MGM Resorts returned
approximately $580 million to shareholders in the form of dividends
and share buybacks during the year. Our ability to achieve these
goals is a direct reflection of our ongoing commitment to total
shareholder return while demonstrating confidence in our ability
to grow our business and maintain a strong balance sheet.
Our successes haven�t gone unnoticed as MGM Resorts
was added to the S&P 500 Index in July 2017. This significant
milestone represents the efforts of the 78,000 men and women
dedicated to delivering the finest entertainment experiences
at MGM Resorts and our continued commitment to maximize
shareholder value.
While we celebrated many achievements in 2017, we also
remember those impacted by the tragic events of October
1st. We owe a deep gratitude to the first responders, including
many of our own within the MGM family, and various law
enforcement agencies who acted heroically in the face of
unimaginable circumstances. Las Vegas is a great, unique city
and we are firmly confident in the resilience of this community
and its ability to remain a leading entertainment and business
destination for years to come. We remain #VegasStrong.
I wish to thank our committed shareholders and our
passionate employees, whose dedication is at the heart of our
enduring strength and success.
 JIM MURREN
 Chairman and Chief Executive Officer