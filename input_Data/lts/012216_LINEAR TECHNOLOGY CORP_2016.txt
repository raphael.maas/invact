To Our Stockholders,
35 years in business and 30 years on NASDAQ.
The result of 35 years of Analog Excellence,
delivering innovative solutions to our customers
and industry-leading financials to our
stockholders. We create this value by inventing,
developing and manufacturing the hard stuff
solutions that enable our customers to create
systems that are smaller and run more efficiently,
precisely and reliably.
Electronics permeate every facet of our lives, and
at Linear we focus our design efforts on the
fastest growing and most stable analog markets.
These span the transportation and industrial
markets, which together represent 66% of our
business, as well as communications
infrastructure, networking and computing. In
transportation, we are witnessing major
technological breakthroughs, with developments
in hybrid/electric and all-electric vehicles,
advanced driver assistance systems, safety
systems, and the promising future of autonomous,
self-driving vehicles. Linear is participating in all
of these, resulting in transportation growing from
20% to 23% of our annual revenue in fiscal 2016.
As expected, the first half of fiscal 2016 was
weak due to global macroeconomic conditions,
which affected the industrial market most. But
this market rebounded in the second half, helping
us achieve solid sequential growth in those
quarters, and industrial remains our largest market
at 43%.
As a result, revenue for the year decreased 3.5%
to $1.424 billion. After a decline in the first
quarter and modest growth in the December
quarter, we delivered good growth in the second
half of the year. Net income of $494.3 million
decreased 5.1%. Operating income was $633.6
million and was 44.5% of sales. The Company
generated cash flow from operations of $685.1
million, of which $303.4 million was paid out in
dividends and $119.8 million was used to
purchase common stock. During the year, the
quarterly per share cash dividend was raised 7%
in January and this was the 24th consecutive year
the Company increased its dividend. This strong
financial performance is reflective of the value
our solutions bring to our customers as we
continue to drive innovation.
Combining with Analog Devices. On July 26,
Analog Devices and Linear Technology
announced a definitive agreement for Analog
Devices to acquire Linear Technology. Upon
completion of the acquisition, Analog Devices
will be the premier global analog technology
company, with strengths in data converters,
power management, amplifiers, interface, RF and
microwave products. Linear Technology
stockholders will participate in Analog Devices
business if the acquisition is completed. Linear
Technology stockholders are expected to hold
approximately 16% of the combined company on
a fully diluted basis based on the merger
consideration of $46.00 per share in cash and
0.2321 of a share of Analog Devices common
stock for each share of Linear Technology
common stock held at the close of the transaction.
For 35 years, Linear Technology has had great
success by growing its business organically.
However, the acquisition of Linear Technology
by Analog Devices has the potential to create a
combination where one plus one truly exceeds
two. Analog Devices is a highly respected
company. By combining our complementary
areas of technology strength, Linear Technology
and Analog Devices will advance technology in
the analog and power semiconductor markets,
As we look toward the future, we can reflect on
what we have built together over 35 years. We
have built a strong company, in solid growth
markets, a world leader in high performance
analog and power management products. Our
customers place a high value on what we
deliverproducts that consistently push the
performance envelope, our ability to anticipate
and solve their system design challenges, and our
industry leading technical team.
We want to thank our stockholders, our
customers, and our dedicated employees for
making this possible.
delivering innovative analog solutions to
customers worldwide.
Sincerely,
DONALD P. ZERIO LOTHAR MAIER ROBERT H. SWANSON , JR.
Vice President, Finance and Chief Executive Officer Executive Chairman
Chief Financial Officer