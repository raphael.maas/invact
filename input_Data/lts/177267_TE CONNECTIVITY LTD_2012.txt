A MESSAGE TO OUR STAKEHOLDERS
Tom Lynch - Chief Executive Officer
Our vision is to be the world leader in providing
the connections that enable a more connected
world. This vision represents a tremendous
opportunity for our company. The markets we
serve are approximately $100 billion in size
and are expected to grow between five and six
percent annually. The worlds need for more
and greener energy and information anywhere,
anytime are long-term positive trends that
require more electronics and more connections.
Five years ago, we set out to transform our
company to capitalize on this opportunity and
consistently deliver strong performance. We
made significant changes to harness our capabilities and deliver an extraordinary experience
for our customers, outstanding value for our
investors, and a great place to work for our
employees. We have not wavered from our
strategy even as we encountered unprecedented
economic volatility and uncertainty.
STRENGTHENING TE IN 2012
2012 was another year of good progress. We
strengthened the company and positioned TE
for sustainable strong performance over the
long haul.
We acquired Deutsch and divested two nonconnectivity related businesses. With these
decisions, over ninety percent of our business
is focused on connectivity, up from seventy
percent in 2007. This focus allows us to leverage
our scale as a competitive advantage.
The acquisition of Deutsch established TE as
the leader in harsh environment connectivity.
With Deutschs complementary markets and
product range, we are providing connections
for the most challenging applications on the
planet. The integration is going well and we
expect the financial returns to exceed our
original expectations.
Investment in innovation continues to be a top
priority. In 2012, we invested approximately
$700 million, or five percent of sales, in engineering, research and development, up from
$560 million in 2010. For the second consecutive year, our patent filings were up ten percent
and Thomson Reuters recognized us as a Top
100 Global Innovator. Our investments led to
a robust product pipeline and breakthroughs
including aluminum connectivity products for
the automotive industry and technologies that
deliver 25 gigabit per second speeds in the
data center.
Our lean program, the TE Operating Advantage,
is now deployed in every location in the company.
Over the past five years, we have reduced our
manufacturing footprint by fifteen percent while
increasing our production capacity. In 2012, both
on-time delivery and quality improved by ten
percent, with several of TEs businesses leading
the industry in these areas.
TEs corporate responsibility program gained
momentum and was recognized by leading
external organizations. In 2012, we were listed
on the Dow Jones Sustainability Index North
America.
Over the last five years, we have made significant
investments in employee training and leadership development. These investments and our
long-term approach to succession planning have
created a deep leadership bench throughout
the organization. 
In a world where
everything is connected,
Every Connection Counts.
Beginning fiscal 2013, we organized the company
into four major business segments with fifteen
industry-focused business units. This structure
allows us to serve customers better, leverage
our capabilities more effectively and further
optimize our efficiency. It also enabled several
of our senior business leaders to move into
expanded roles, which has been great for the
organizations vitality.
2012 FINANCIAL PERFORMANCE
Our Automotive and Aerospace and Defense
businesses delivered another year of strong
performance. This performance was more than
offset by the softness we experienced in the
telecommunications and industrial markets,
resulting in slightly lower sales and earnings for
the year. We held adjusted operating margins
at thirteen percent, despite the sales decline,
due primarily to productivity improvements
realized from our lean program.
For the fifth consecutive year, free cash flow
approximated ten percent of revenue, exceeding
$1.4 billion in 2012. We returned $526 million
to shareholders through dividends and share
repurchases and increased our dividend by
seventeen percent.
Our significant and consistent cash flow generation was an important factor that led to the
recent upgrade of our credit rating to BBB+ by
Standard and Poors.
LOOKING FORWARD
TE is a focused connectivity company today.
We are strongly positioned to capitalize on the
long-term fundamental trends driving the need
for greater connectivity in the markets we serve.
I am bullish about the prospects for all of our
businesses. Each has a robust product pipeline
and the global presence our customers need.
The following are our key objectives:
 Transportation Solutions  Our goals are to
continue growing faster than the market and
extend our leadership position. The industry
is expected to grow to 100 million vehicles
per year, and electronic content in vehicles is
projected to increase. This growth is great for
our business. Our 2,000 engineers, located
close to every major manufacturer in the world,
are delivering more innovative products than
ever before to enable our customers to provide
safer, more fuel-efficient and more connected
vehicles.
 Industrial Solutions  The four markets we
serve in this segment  Industrial Equipment;
Energy; Aerospace, Defense and Marine; and
Medical  require products that must stand up
to and perform in the most demanding settings.
TE is a leader in these attractive markets with
a product portfolio to solve connectivity needs
even in the most challenging environments. With
a greater need for energy efficiency, communications and advanced technology, these markets
are fueling the need for more electronics and
innovation. We expect to capitalize on these strong growth trends by leveraging TEs existing
leadership position and the products and capabilities of Deutsch.
 Broadband Networks  The insatiable need
for information anytime and anywhere is driving
the demand for ultra high-speed broadband
networks. In some places already, a gigabit of
bandwidth is available to the home. The only
way to deliver these speeds is with a fiber-rich
broadband network. TE is the market leader
connecting every part of this network. In the
past two years, the networks market has been
soft. During this time, we have reduced costs
and accelerated our new product development
efforts. We are poised for significant sales and
profit acceleration as the market returns to
growth.
 Consumer Devices  This is a $15 billion
connectivity market today, and growing rapidly
as forecasted sales of devices double by 2016.
With ever-expanding capabilities, smart phones
and tablets are becoming indispensable to users.
The need to be always-on and connected requires
more connectivity and innovation than ever
before including smaller, faster and more reliable
connectors, antennas and cables. Creating
innovative products has been a key focus of
TE and our new designs are being increasingly
adopted by the leading device brands. 2012 was
a turnaround year and 2013 is a year of building
momentum in this business. I fully expect that
TE will deliver strong performance in this market
over the longer term.
 Shareholder Returns  We expect to continue
to generate significant free cash flow, and to
return approximately two-thirds of our free
cash flow to shareholders. We have proposed
an additional dividend increase of nineteen
percent effective June 2013. This increase will
bring our annual dividend to $1 per share.
TE is a strong company and we have improved
every important aspect of our business over the
last five years. We are committed to performing
well and returning significant capital to our
shareholders in a tough economy, and are poised
to deliver strong sales, earnings and cash flow
growth as the global economy recovers.
I have never felt better about our companys
focus and capability. Thank you to our customers,
shareholders and employees for your continued
support.