Dear Shareholders: Your company has made great progress -- on
safety, on customer service, on financial results and on living our core values.
It is a great pleasure to be able to share what our team has delivered.

From 2004 to 2006, CSX more than doubled              Further demonstrating confidence in the
its operating income and earnings per share,          long-term prospects for the company, the
while increasing revenues by nearly 20 percent.       CSX Board of Directors in February of 2007
During that time, shareholder value nearly            authorized a share repurchase of up to $2 billion.
doubled. In 2006 alone we delivered a total           The company intends to complete this new
return of 37 percent, higher than any major           program, which replaced a previous repurchase
North American railroad.                              program, by the end of 2008. At the same
                                                      time, the company increased its dividend by
                                                      another 20 percent, more than doubling the
Record Results                                        dividend since September of 2005.
CSX reported record 2006 operating income
of $2.1 billion, a 38 percent increase over 2005,
                                                      Revitalized Safety and Service
and record revenues of $9.6 billion, up 11 percent
for the year. In addition, we achieved a sub-80       The results delivered to shareholders in
percent operating ratio  equivalent to a 20-plus     2006 were supported by impressive safety
percent operating margin  for the first time         and customer service improvements in
in nearly a decade. We are now focused on             Surface Transportation, all made possible by
achieving a mid-70s operating ratio within the        the new operating disciplines put in place by
next two years.                                       our employees while rail traffic remained at
                                                      record levels.
The excellent operating achievements in our
businesses, combined with a strong balance            Employee injuries and train accidents were
sheet, enabled CSX in 2006 to increase its            down 21 percent and 24 percent, respectively.
dividend by 54 percent, repurchase $465 million       At the same time, our railroad employees
worth of shares, and invest $1.4 billion in Surface   increased on-time train originations by 49
Transportation capital improvements for long-         percent and on-time train arrivals by 56
term growth. The company also implemented             percent, as freight moved more quickly and
a 2-for-1 stock split.                                efficiently across the network. As a result,
customer satisfaction levels, which we measure          To prepare for continued growth, the team
through third-party research, have increased            remains on schedule with capacity expansions in
significantly, and our railroad now ranks as one        the rapidly growing Southeast and populous
of the leading service providers in the industry.       Northeast. We also announced plans to create a
                                                        high-volume rail corridor to transport intermodal
In short, it was a banner year for our transportation   traffic between California and the Southeast.
businesses. The safety, service and financial
improvements are expected to continue in
                                                        Looking Ahead
2007 with overall freight demand increasing.
                                                        We will continue to work with local, state
                                                        and federal leaders to promote public
Robust Demand                                           policies that enable railroads to create
In 2006, Surface Transportation revenues grew           shareholder value while providing a service
in all markets. Volumes grew in key markets             that is essential to America. Railroads,
such as coal and agricultural products, with            which have been a vital part of the nation's
some decline in automotive, phosphate and               economy for almost two centuries, are
housing-related sectors. Favorable overall market       playing a leadership role in addressing
conditions, combined with enhanced customer             modern-day challenges, including the
service and reliability, enabled our businesses to      nation's need for energy supply and
command strong pricing, while fuel surcharges           environmental preservation. Railroads offer
helped to offset volatile fuel prices.                  a significant advantage in fuel efficiency
                                                        compared to trucks and have made
Strong freight rail demand is expected to               outstanding efforts to reduce greenhouse
continue as shipments to and from the ports             gas and other emissions from locomotives.
need to be hauled across the country.                   The nation's rails are also an important
Railroads offer by far the most cost-efficient          transporter of domestically abundant coal,
and environmentally friendly way to get the             as well as renewable energy sources such
products to their destinations.                         as ethanol and wind turbines.
                 

We have every reason to be optimistic about         of urgency and passion, and the recent
the future of our economy, our industry and         successes in our businesses offer a view of
our company. I hear it in the voices of customers   their enormous potential and opportunity.
across many business sectors, and I see it in
the faces of veteran railroaders and thousands      CSX's opportunities are vast, not only because
of men and women now joining our ranks.             of the transportation needs of our country,
                                                    but also because of our demonstrated ability
In this, our 180th year in business, we are         to improve customer service.
confident that we can continue to deliver on
our targets of double-digit growth in operating     We are well on our way to achieving our vision
income, earnings per share and free cash flow       of becoming the safest, most progressive
through 2010, while finally meeting or exceeding    North American railroad, relentless in the
our cost of capital.                                pursuit of customer and employee excellence.


This is an exciting time at CSX. Employees
are executing on our strategies with a sense
