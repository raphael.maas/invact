We had a pretty good year. Funds from operations per share increased 4.3 percent
in 2004 to $2.93 from $2.81 in 2003. See the Computation of Funds from
Operations table for a reconciliation of net income to funds from operations.
Several of the initiatives we started in 2002 are coming to fruition. In addition, we
expanded our franchise and positioned ourselves for meaningful growth in 2005. Lets look
at what we did in 2004 and its implications for 2005.
Self-Storage Business
Our self-storage business generated the bulk of our earnings and cash flow growth in 2004.
This came from three main areas: our consistent group of properties, the lease-up of our
development properties and the acquisition of mostly mature facilities from third parties. The
contribution to earnings for each is as follows:

Consistent Group Properties
Our consistent group properties, which constitute approximately 80 percent of the aggregate
net rentable square feet of our total portfolio, generated net operating income (before
depreciation) growth of 4.8 percent. This was due to a 4.9 percent growth in revenues
combined with a 4.9 percent increase in expenses. The expense increase was more modest than
we anticipated and the revenue growth greater, a wonderful combination.
We were able to drive average occupancies to 91 percent in 2004, up almost 2 percent from
2003 and 7 percent from 2002. This, combined with higher average rental rates, led us to a
very acceptable 4.9 percent revenue growth. We are hopeful of modest revenue growth going
into 2005, but it wont come from occupancy growth. Rental rates and customer management,
including use of promotional discounts and media, will need to produce 2005 revenue growth.
With respect to expenses, 2004s increase of 4.9 percent was far more moderate than 2003s at
10.5 percent. Our 2004 expense increase was driven primarily by property taxes, payroll and
repairs and maintenance. Going into 2005, we expect about the same level of overall expense
increase, somewhere between 4 percent to 5 percent.
Two bright spots on the expense front are the telephone reservation center, where we saw a
25 percent decrease in the fourth quarter and a 2 percent decline for the year. We expect a
10 percent plus decline in 2005, as a result of our initiative, now substantially completed, to
eliminate the sale of both trucks and Pick-Up and Delivery service out of the national phone
center. The other area is insurance cost which declined 10 percent in the fourth quarter and
was up only 1.6 percent for the year. We expect a softer renewal market and improved cost
controls to reduce our property insurance costs by at least 5 percent in 2005.
We expect advertising and media costs to remain volatile, as we adapt to competitive
conditions, which leads us to one of our few disappointments in 2004. Despite an increase in
excess of a million dollars in promotional spending, our gross customer move-in volume was
down 5 percent, to 586,000 compared to 617,000 last year. Our media costs in addition to
our promotional discounts per move-in are exceeding $50. It is clear we have some work to
do in this area. We hope to report more positive news regarding the efficacy of our media
programs as 2005 unfolds.
In this regard, in mid-February, we kicked off a new coordinated advertising campaign
designed to enhance our rental activity and generate increases in brand preference. Our
campaign slogan, Your stuff will be happy here, is designed to focus the consumer on
Public Storage versus generic self-storage product awareness. We expect positive customer
traffic and greater brand awareness to validate this new program.
In 2005 and 2006, we expect an increase in repairs and maintenance expense as well as
capitalized (long life) expenditures. We have reorganized our facilities maintenance function,
expanded staffing and are completing a physical assessment of our real estate assets. We want
all of our properties in rent-ready condition at all times.
In 2005, we will undertake new initiatives to improve and standardize how we hire, manage,
train and retain our personnel. These efforts, along with others already in place, are designed
to further reduce field personnel and supervisory staff turnover, increase productivity and
improve customer service. The action words in our organization with respect to people are
standardization, enhanced training and increased accountability. In the near term, we expect
an above inflation rate increase in personnel costs.
Development Properties
Our development properties continued their lease-up. Returns on our invested capital are
still relatively modest, but improved to 5.5 percent. We anticipate our development
property yields and income will improve for the next couple of years as we increase rates
and continue to drive occupancies. We are hopeful of expanded growth in this area in
future years.


Longer term, we hope to start expanding our development pipeline. The challenges are great
and the competition is fierce.
Expansion Properties and Pick-Up and Delivery Conversions
We have 20 former Pick-Up and Delivery (PUD) locations being converted to self-storage.
These are great locations where we already own the land and building and for marginal
costs of about $35 per foot we can add self-storage space. Many of these are contiguous to
existing self-storage properties, so we are reasonably confident of demand. Our
opportunity here is about 1.4 million net rentable square feet which we hope to build out
in 2005 and 2006.
Last year we also initiated what we call a repackaging program. This applies primarily to our
older facilities that have strong consumer demand, but lack a retail facade. We also can
expand the density of the properties, increasing the rentable square footage without any
additional land costs. Some of these repackagings will be done just to maintain our
competitive position in the marketplace. However, we expect most will generate attractive
returns in excess of 10 percent on the marginal capital invested to enhance the property. In
2004, we spent $13 million on these activities and expect this to be a growing area of capital
investment going forward.
Acquisition Properties
Our acquisition properties were truly the bright spot of our capital deployment activities
in 2004. We acquired more properties, in better markets and of better quality, than
anyone in the industry last year. We invested approximately $270 million in 47
properties, more volume than the last several years. Our high quality acquisitions give us
a highly visible market presence in several markets where we had modest to nascent
presence before. We acquired most of these properties at prices close to or modestly above
what it would cost to build. We expect our return on these investments to appreciate over
the next couple of years.
Overall, the self-storage business generated very positive growth in 2004. New supply was
modest, at 1 to 2 percent of the industry, less than underlying demand growth. We do not
expect a dramatic increase in supply, despite abundant capital for real estate investments. The
risks of building and prolonged fill-up rates have increased developer uncertaintydespite
record low interest rates.
Pick-Up and Delivery
We contracted our containerized storage business further in 2004, narrowing its scope to 12
facilities in six markets. We think we have the markets and management team focused, with
the right potential customers and the correct operating strategy. We dont expect immediate
results, as 2005 will be a transition year to a stabilized business model that can produce
acceptable returns on invested capital on a consistent basis. We believe in the business and
believe it can be an attractive investment long term.
We expect profitability at the operating unit level in 2005but we will be investing all of it
in expanded yellow pages and media programs. Over the last two years, we have closed over
30 facilities and extracted over $10 million of capital from this business. We have narrowed
our focus and 2005 will see us concentrate on marketing and execution.
Investment in PS Business Parks
Our investment in PS Business Parks (PSB) produced modest results in 2004. That is
the rear view mirror. Unlike self-storage, PSBs business is much more volatile and
susceptible to economic cycles. The last three years have been the down cycle as
customer demand evaporated and landlords were left fighting for modestly viable
customers. Generous concessions, especially in the form of tenant improvements,
have lead to dramatically reduced returns on invested capital. For PSB, the pain has
been much less than most. A good product, minimal dot com and telcom tenants
and very capable operating personnel helped manage the storm. In 2005, PSB needs
to adapt to a dramatically improved economic environment and we expect to
significantly reduce our capital commitment to customer space. Similar to the selfstorage
business, PSB faces an extremely competitive environment to acquire
additional properties. As Warren Buffet says, When money is cheap - assets are
dear, and money is cheap right now.
PSB refinanced several preferred stock issues this year, lowering its permanent funding costs
to below 8 percent. PSB may have additional opportunities in the coming years to refinance
about $100 million of these preferred equities.
PSB should produce modest earnings improvement but dramatically improved value
creation going forward. The winds are shifting in its favor and PSBs shorter than average
lease duration should enable it to take advantage of improving market fundamentals sooner
than most.
Financing
During the first quarter of 2005, we completed the major planned refinancing of our high
coupon preferreds, which we started last year about this time. Upon completion, our $2.4 billion
of outstanding preferred equities will have a blended cost of about 7 percent. We will see
the benefits of this program in 2005, which will be partially offset by some possible prefunding
for potential 2006 redemptions. In 2006, we have an opportunity to redeem three
large preferred stock issues totaling approximately $825 million at a blended rate of 8.1 percent,
assuming a favorable rate environment.
With respect to our financial reporting, internal controls and Sarbanes-Oxley, we are happy to
report that our financial team, lead by John Reyes, did an absolutely superb job. We hired no
consultants or special advisors. Existing personnel under the direction of Todd Andrews, our
exceptional Controller, performed all compliance work and testing and we received a clean
opinion from our auditors.
Conclusion
During 2004, two long-time competitors went public via IPOs. The self-storage industry
is gaining broader investor acceptance along with the Real Estate Investment Trust (REIT)
industry. This is positive for our owners and management. Good competition will hopefully
better enable us to generate growing returns on invested capital.
In 2005, we will continue to differentiate ourselves from the competition and continue our
focus on People, Product and Pricing, with the objective of long-term, sustained growth in
earnings and cash flow per share. With our focus on the three P's, we believe we are well
positioned to maintain our leadership role in the self-storage industry.

Ronald L. Havner, Jr.
Vice-Chairman and Chief Executive Officer

Harvey Lenkin
President and Chief Operating Officer