Looking back on our 75th year of service in 2013, we are pleased to celebrate another productive year at Dillards. We reported
our fourth consecutive year of positive comparable store sales. While we experienced a 30-basis-point decline in gross margin, we
executed tight control over operating expenses which declined $39 million and 40 basis points of sales enabling us to report solid earnings growth of $7.10 per share versus $6.87 for the prior year.
Importantly, this sound operational performance led to another year of strong cash fl ow from operations exceeding
$500 million. Following the pattern of the past few years, we turned strong cash fl ow into shareholder returns through
a combination of share repurchases and dividends totaling $309 million. Notably, over the past fi ve years, we have
returned over $1.7 billion to shareholders primarily through disciplined execution of our share repurchase programs.
In todays challenging and competitive environment, this substantial shareholder return would not have been possible without our successful efforts to become a better retailer. Just a few years ago, we launched a comprehensive
effort to elevate product and people over price and promotion at Dillards. As a result, we believe we have created
considerable distance in the marketplace between our stores and traditional, moderate department store peers in
terms of merchandise mix, presentation and customer service. We are continually working to provide an unmatched shopping
experience at Dillards by emphasizing and supporting highly revered national and exclusive brands, seeking exciting new brands and
products, and cultivating a culture of service that exceeds customer expectations.
We are proud of our performance during our 75th anniversary year, and we recognize that our continuing ability to serve you, our
shareholders, depends upon our ability to serve our customers. We believe we are well positioned to provide a premium shopping
experience in 2014 because of the ongoing contributions of 40,000 dedicated associates, and we truly thank them for their service. We
look forward to serving you in 2014.
William Dillard, II Alex Dillard
Chairman of the Board & President
Chief Executive Offi cer