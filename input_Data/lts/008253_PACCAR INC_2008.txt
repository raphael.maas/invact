to our shareholders
PaccaR had a very good year in 2008, even as the global recession had an 
increasingly negative impact on the companys results throughout the year.  PaccaRs 
success is due to its global diversification, superior product quality, technology-led 
process efficiency and strong results from aftermarket parts and financial services.  
the company has delivered an impressive 70 consecutive years of net income.  
customers benefited from PaccaRs record $805 million of capital investments and 
research and development, which enhanced manufacturing capability, developed 
innovative aftermarket support programs and accelerated new product introductions.  
PaccaR delivered 126,000 trucks to its customers and sold $2.3 billion of aftermarket 
parts.  PaccaR had record truck deliveries in Europe, which were offset by a depressed 
truck market in the U.s. and canada.  PaccaR Financial services generated $3.4 
billion of new loan and lease business.
net income of $1.02 billion on revenues of $15.0 billion was the fourth highest in 
the companys 103-year history.  PaccaR declared cash dividends of $.82 per share, 
including a special dividend of $.10 per share.  Regular quarterly cash dividends have 
increased over 500 percent in the last 10 years.
Industry Class 8 truck sales in North America, including 
Mexico, declined to 179,000 vehicles, compared to 
207,000 the prior year.  Over 3,000 fleets declared 
bankruptcy due to lower freight volume, higher fuel 
prices and the recessionary economy.  The European 
heavy truck market in 2008 was 334,000 vehicles, 
compared to 337,000 in 2007, a strong performance that 
declined abruptly in the fourth quarter 2008.  Many of 
our competitors are discounting their vehicles below 
cost in the challenging market.  There may be some 
competitors exiting the business in the next few years 
due to lack of profitability.
Even in this troubled market, PACCAR continued to 
be one of the leaders in financial performance for 
capital goods companies worldwide.  After-tax return on 
beginning shareholder equity (ROE) was 20.3 percent in 
2008, compared to 27.5 percent in 2007.  The companys 
2008 after-tax return on revenues was 6.8 percent.  
PACCAR has distributed over $3.5 billion in dividends 
and increased shareholder equity to $4.85 billion during 
the last ten years.  PACCARs average annual total 
shareholder return was 17.6 percent over the last decade, 
versus a negative 1.4 percent for the Standard & Poors 
500 Index.  The fragility of global financial institutions 
provided a timely reminder of the merits of PACCARs 
conservative business approach and quality product and 
customer service focus.
 FoR thE   FUtURE  PACCARs solid 
profits, excellent balance sheet and intense focus on 
quality, technology and productivity enhancements have 
allowed the company to invest $3.8 billion since 1999 
in capital projects, products and processes.  Yearly 
productivity and efficiency improvement of 5-7 percent 
and capacity improvement of over 100 percent in the last 
five years have enhanced the capability of manufacturing 
and parts facilities.  PACCAR is recognized as one of the 
leading applied-information technology companies in the 
industry, and innovation continues to be a cornerstone 
of its success.
Capital investments were a record $463 million in 
2008.  One exciting multi-year initiative is the 
construction of PACCARs engine assembly plant in 
Mississippi, which builds upon our legacy as a premier 
engine manufacturer.  Other major capital projects 
during the year included the unveiling of an enhanced 
engine research and development center in PACCARs 
Technical Center (Mt. Vernon, Washington), opening of 
a new parts distribution center (PDC) in Budapest, 
Hungary, and a 20 percent capacity improvement at 
Leylands manufacturing facility.
PACCAR continues to examine business opportunities 
in Asia, with its primary focus on China and India.  
PACCAR is increasing its purchases and component sales 
in China through its Shanghai office.  The business 
opportunities in Asia have dampened, but still present 
attractive long-term returns.
siX   sigma  Six Sigma is integrated into all business 
activities at PACCAR and has been adopted at 190 of the 
companys suppliers and many of the companys dealers.  
Its statistical methodology is critical in the development 
of new product designs, customer services and 
manufacturing processes.  Since inception, Six Sigma has 
delivered over $1.2 billion in cumulative savings across 
the company.  In addition, High Impact Kaizen Events 
(HIKEs) leverage Six Sigma methods with production 
flow improvement concepts.  Almost 13,000 employees 
have been trained in Six Sigma and 9,700 projects have 
been implemented since its inception.  Six Sigma, in 
conjunction with Supplier Quality and Development, 
has been vital to improving logistics performance and 
component quality by the companys suppliers.
  PACCARs 
Information Technology Division (ITD) and its 650 
innovative employees are an important competitive 
asset for the company.  PACCARs use of information 
technology is centered on developing and integrating 
software and hardware that will enhance the quality and 
efficiency of all products and operations throughout 
the company, including the seamless integration of 
suppliers, dealers and customers.  In 2008, ITD provided 
innovative advancements in new manufacturing software 
and infrastructure capacity upgrades.  Over 17,000 dealers, 
customers, suppliers and employees have experienced 
the companys technology center highlighting surface 
computing, tablet PCs, an electronic leasing and finance 
office and an electronic service analyst.
tRUc k s  U.S. and Canadian Class 8 industry retail 
sales in 2008 were 153,000 units, and the Mexican market 
totaled 26,000.  The European Union (EU) industry 
heavy truck sales were 334,000 units.
PACCARs Class 8 retail sales in the U.S. and Canada 
achieved a market share of 26.0 percent in 2008 
compared to 26.4 percent the prior year.  DAF achieved 
14.1 percent share in the 15+ tonne truck market in 
Europe.  Industry Class 6 and 7 truck registrations in the 
U.S. and Canada were 60,000 units, a 31 percent decrease 
from the previous year.  In the EU, the 6- to 15-tonne 
market was 80,000 units, down 5 percent from 2007.  
PACCARs North American and European market shares 
in the medium-duty truck segment were excellent, as the 
company delivered 23,000 medium-duty trucks and 
tractors in 2008.
A tremendous team effort by the companys 
purchasing, materials and production personnel 
contributed to improved product quality and 
manufacturing efficiency during challenging market 
conditions.  High commodity prices, which began to 
abate during the year, were partially offset by PACCARs 
excellent long-term supplier partnerships, which enabled 
production and efficiency improvements.
PACCARs product quality continued to be recognized 
as the industry leader in 2008.  Kenworth dominated 
customer satisfaction awards in the Class 8 markets and the 
DAF CF was the 2008 Fleet Truck of the Year in the U.K.
Over 65 percent of PACCARs revenue was generated 
outside the United States, and the company realized 4
excellent synergies globally in product development, sales 
and finance activities, purchasing and manufacturing.  
DAF Trucks achieved record truck production and sales 
and excellent market share.
Leyland Trucks is the United Kingdoms leading truck 
manufacturer.  Leyland expanded its innovative bodybuilding program that has delivered over 750 custombuilt-bodied vehicles to customers.
PACCAR Mexico (KENMEX) had a challenging year 
as the Mexican economy slowed and truck fleets were 
reduced.  Their new manufacturing facility is delivering 
improved efficiency and product quality.
PACCAR Australia achieved good results in 2008.  The 
introduction of new Kenworth models and expansion 
of the DAF product range in Australia combined for a 
24.5 percent heavy-duty market share in 2008.  PACCAR 
Parts Australian sales delivered another year of record 
performance.
PACCAR International exports trucks and parts to 
over 100 countries and had an excellent year due to strong 
sales buoyed by natural resource exploration globally.
AFTERMARKET   CUSTOMER   SERVICES  PACCAR 
Parts had an excellent year in 2008.  With sales of 
$2.3 billion, PACCAR Parts is the primary source for 
aftermarket parts for PACCAR products, and supplies 
parts for other truck brands to PACCARs dealer networks 
around the world.  Over five million heavy-duty trucks 
are operating in North America and Europe, and the 
average age of these vehicles is estimated to be over six 
years.  The large vehicle parc creates excellent demand 
for parts and service and moderates the cyclicality of 
truck sales.
PACCAR Parts added new distribution centers and 
expanded current facilities to enhance logistics 
performance to dealers and customers.  PACCAR Parts 
continues to lead the industry with technology that 
offers competitive advantages at PACCAR dealerships.  
Managed Dealer Inventory (MDI) is now installed at 
1,050 PACCAR dealers worldwide.  PACCAR Parts 
enhanced its Connect program, a software solution for 
customer fleet-maintenance management.  The web-based 
application provides fleets with the tools to reduce their 
vehicle operating costs.
 PACCAR Financial Services 
(PFS) conservative business approach, coupled with 
PACCARs superb S&P credit rating of AA- and the 
strength of the dealer network, enabled PFS to earn good 
results in 2008 despite turbulent worldwide financial 
markets.  The PACCAR Financial Services group of 
companies has operations covering three continents 
and 20 countries.  The global breadth of PFS and its 
responsive credit application processes supported a 
portfolio of 166,000 trucks and trailers, with total assets 
of $10 billion and earned a pre-tax profit of $217 million.  
PACCAR Financial Services is the preferred funding 
source in North America for Peterbilt and Kenworth 
trucks, financing 32 percent of dealer sales.
The unsettled financial markets and resulting credit 
crunch presented a daily challenge that increased 
funding costs for our customers and prompted a 
contraction in our finance companies assets.  Special 
praise is merited for PACCARs treasury and finance 
teams who diligently, creatively and positively managed 
the company through a very challenging market in 2008.
PACCAR Financial Europe (PFE) completed its 
seventh year of operations, with increased assets and 
good profits as it served DAF dealers in 16 European 
countries.  PFE provides wholesale and retail financing 
for DAF dealers and customers, and finances 21 percent 
of DAFs vehicle sales.
PACCAR Leasing (PacLease) earned its 15th
consecutive year of record operating profits and placed 
4,900 new PACCAR vehicles in service in 2008.  The 
PacLease fleet grew to over 32,500 vehicles as 18 percent 
of the North American Class 6-8 market selected fullservice leasing to satisfy their equipment needs.  PacLease 
substantially strengthened its market presence in 2008, 
increasing its global network to 362 outlets, and 
represents one of the largest full-service truck rental 
and leasing operations in North America.
  PACCAR is a global 
environmental leader.  A significant accomplishment 
during the year was earning ISO 14001 environmental 
certification at all PACCAR manufacturing facilities in 
Europe and North America.  PACCAR introduced 
medium-duty hybrid-electric vehicles, which can achieve 
up to a 30 percent fuel economy improvement.  Several 
of the manufacturing facilities achieved Zero Waste to 
Landfill status during the year.  PACCAR employees 
are environmentally conscious and utilize van pools, car pools and bus passes for 30 percent of their business 
commuting.  
  The dedicated efforts of PACCARs 
18,000 employees enabled the company to distinguish 
itself as a global leader in the technology, capital goods, 
financial services and aftermarket parts businesses.  
Superior product quality, technological innovation and 
balanced global diversification are three key operating 
characteristics that define PACCARs business philosophy.
In North America and Europe, the recession will have 
a negative impact on the truck market in 2009.  The 
implementation of updated North American engine 
emission standards in 2010 may encourage some 
operators to slightly pull forward their truck purchases.  
Current estimates for Class 8 trucks in North America 
indicate that annual industry sales could be similar to 
2001 and range from 130,000-170,000 units.  This is one 
of the lowest levels of sales in the last 10 years.  Sales 
for Class 6-7 trucks are expected to be between 50,000-
60,000 vehicles.  The European heavy-duty truck market, 
which had a strong year in 2008, could decline by 40 
percent, with sales between 200,000-240,000 trucks, 
while demand for medium trucks should range from 
50,000-65,000 units.  
Though PACCAR had a very good year in 2008, with 
several operating divisions achieving record results, the 
outlook for 2009 appears difficult due to tumultuous 
economic conditions.  The company has taken proactive 
steps to adjust production rates as well as structurally 
reduce costs throughout the organization.  The benefits of 
geographic diversification, dedicated employees, quality 
products, modern facilities, innovative IT systems and a 
strong balance sheet, working in tandem with the best 
distribution network in the industry, are the fundamental 
elements that make PACCAR a vibrant, dynamic 
company.  PACCAR is well positioned and committed to 
maintaining the profitable results its shareholders expect, 
by delivering global leading products and services.
PACCAR recognizes two significant retirements.  Vice 
Chairman Mike Tembreull retired upon completion of 
over 38 years of exemplary service, in which he was 
instrumental in the profitable growth of PACCAR 
Financial and the integration of DAF into PACCAR.  
Jim Pigott is retiring after 37 years on the Board of 
Directors.  Jim also worked for PACCAR for thirteen years 
and was an inspirational leader on the Board, sharing his 
wisdom, insight and global business knowledge.  We 
thank Jim and Mike for their dedication and wish both 
a happy and healthy retirement.
PACCAR and its employees are firmly committed to 
strong quality growth and are proud of the remarkable 
achievement of 70 consecutive years of net profit.  
PACCAR plans for the long-term, and our shareholders 
have benefited from that approach.  The embedded 
principles of integrity, quality and consistency of 
purpose continue to define the course in PACCARs 
operations.  The proven business strategy  delivering 
technologically advanced, premium products and an 
extensive array of tailored aftermarket customer services 
 enables PACCAR to approach growth opportunities 
pragmatically with a long-term focus.  In a challenging 
recession, PACCAR continues to enhance its stellar 
reputation as a leading technology company in the 
capital goods and financial service marketplace.
