To our shareholders: 
In 2010, the economic environment in which nextera energy operated remained very challenging, with weak natural gas and power 
prices and slack demand for electricity in most of the markets in which our company operates.Despite the challenging environment, 
we achieved strong adjusted earnings per share growth of 6.2 percent,
1
 well outpacing most of our peers, and our adjusted return 
on equity (roe) of 13.2 percent
2
 put us in the top quartile of the 34 companies included in the S&p utilities Index. 
at the same time, we maintained a strong 
balance sheet and liquidity position relative 
to most of our peers. our capital expenditure 
program remained robust in support of future 
growth. and in perhaps the most difficult market 
for new renewables projects we have seen in 
many years, we nevertheless executed long-term 
power purchase contracts for a company record 
1,238 megawatts (MW) of wind generation over 
the course of the year.
We also did not lose sight of our core value of 
operational excellence.We had our best-ever 
year for fossil and hydro reliability, our secondbest year ever for wind reliability, and we made 
substantial progress in moving toward our goal of operating a 
single, unified nuclear fleet.our transmission and distribution 
reliability at Florida power & light Company (Fpl) kept the 
company in the top quartile of utilities nationwide when 
ranking by annual minutes without power, and our Fpl cost 
position continued to be in the top decile as measured by 
cost per retail kilowatt hour.We also had our best year ever 
in terms of safety both at Fpl and across the 
entire enterprise.
our operational excellence is a key element in 
our ability to deliver value for our customers.
at Fpl, we provided residential customers 
with monthly bills that were the lowest of all 
55 utilities in the state as of December 2010 
and 24 percent below the national average (as 
of July 2010, the most recent data available). 
our commercial and industrial bills were 
also the lowest of all reporting utilities in the 
state of Florida and well below the national 
average. at the same time, Fpl delivered its 
customers 99.98 percent service reliability, 
helped protect the environment with a greenhouse gas 
emissions rate 35 percent below the national average, and 
boosted the Florida economy through investments that 
created thousands of direct and indirect jobs. likewise, at 
nextera energy resources, llC, and subsidiaries (energy 
resources), operational excellence is critical to our success. 
We maintained top quartile or better performance at our generating facilities in both reliability, as measured by 
equivalent forced outage rate, and cost, as measured by 
non-fuel operation and maintenance (o&M) cost in cents per 
kilowatt hour, while developing customized products tailored 
to the specific needs of our wholesale and retail customers.   
at nextera energy, we have long believed that, if we consistently 
focus on delivering superior operational performance and 
on meeting the needs of our customers, we will be able to 
sustain superior growth and roe. In turn, our ability to deliver 
superior growth and roe will enhance shareholder value over 
time. During 2010, we underperformed the S&p 500 electric 
utilities Index by a percentage point on total shareholder 
return. We believe this slight underperformance was primarily 
a function of investors preference for primarily regulated 
businesses versus competitive businesses during periods of 
financial instability. Given our balanced mix of regulated and 
competitive businesses, it shouldnt be a surprise that we 
underperformed in light of the state of our economy.  
However, viewed over longer timeframes, which encompass 
both the ups and the downs of different cycles that we 
believe affect our industry, we have continued to deliver 
superior value to our shareholders. For the five years ended 
on Dec. 31, 2010, we delivered a total shareholder return of 
48 percent, compared to the S&p 500 electric utilities 20 
percent. over 10 years, our total shareholder return was 107 
percent, compared to 57 percent for the S&p 500 electric 
utilities. For reference, over the same 10-year period the 
S&p 500 returned 15 percent. of special note to those who 
invest in nextera energy to provide stable income, we have 
grown our dividend at a 7 percent compound annual growth 
rate from 2002 through 2010 and provided a 10 percent 
increase in the dividend announced in February 2011.We 
will remain focused on building long-term value through a 
track record of outperformance across the industry's various 
business cycles.
Florida Power & Light Company
For the full year 2010, Fpl recorded net income of $945 
million, up from $831 million in 2009. earnings per share 
grew by 12 percent, from $2.04 to $2.29. While cost recovery 
on the two units at the West County energy Center was the 
most significant driver of earnings growth for 2010, the most 
important longer-term development was the rate settlement 
approved by the Florida public Service Commission in 
December. the settlement agreement effectively freezes 
base rates for our customers through 2012, provides cash 
cost recovery for a third highly efficient and clean combinedcycle natural gas unit at the West County energy Center, and 
positions Fpl to earn a retail regulatory roe at or near 11 
percent in both 2011 and 2012 assuming, among other things, 
normal weather and operating conditions. Going forward, 
our expectation is that the investments our shareholders are 
making in Florida will earn fair and reasonable returns. 
another highlight of the year took place in December, when 
we brought online a 75-MW solar thermal array that is fully 
integrated with and augments the steam produced by one 
of our combined-cycle natural gas units at our power plant 
in Martin County, Fla. this array represents the last of the 
original 110 MW of solar projects approved for cost recovery 
in 2008 by the Florida public Service Commission. as with all 
of our recent Florida projects, these solar arrays created good 
jobs and boosted local tax revenue. 
Fpls strong financial performance in 2010 was matched by 
its high levels of operational performance. the companys 
fossil fleet achieved a record level of fuel efficiency in 2010, 
saving our customers more than $600 million in fuel costs 
as a result. Fpl brought the system-wide heat rate down to 
8,043 British thermal units (Btu) per kilowatt hour, compared 
with the average heat rate for the industry of 10,060 Btus 
per kilowatt hour in 2009, the most recent year for which 
data are available. Since 2001, Fpls heat rate has improved 
by 17 percent.
Just as important have been our efforts to prudently manage 
costs, which have been key to our ability to keep customer 
bills low. For all of 2010, Fpls non-fuel o&M expenses were 
1.5 cents per kilowatt hour, compared with the 2009 industry 
average of 2.2 cents per kilowatt hour. and on service 
reliability, the company delivered its best-ever performance on 
both frequency of interruptions and momentary interruptions. 
Going forward, we believe our investments in Floridas 
electrical infrastructure will continue to produce value for 
shareholders by delivering tangible benefits for customers. 
over the four-year period from 2011 through 2014, Fpl plans to bring into service $6.5 billion worth of major capital projects 
ranging from combined-cycle natural gas plants to nuclear 
uprates to a more intelligent, automated transmission and 
distribution system. We expect these investments to provide 
our Florida customers even more affordable, reliable and 
clean electric service. 
NextEra Energy Resources
energy resources, the competitive energy business of 
nextera energy, reported full-year 2010 net income on a 
Gaap basis of $980 million, or $2.37 per share, compared 
with $759 million, or $1.86 per share, in 2009. on an adjusted 
basis,
3
energy resources earnings were $800 million, or 
$1.93 per share, compared with $792 million, or $1.94 per 
share, for the full-year 2009.
It is important to put these adjusted results into perspective. 
While energy resources adjusted earnings grew slightly, 
most competitive businesses in our sector showed significant 
declines in earnings. the contributors to earnings were 
the addition of new wind projects, higher production from 
the companys nuclear plants and existing wind projects, 
and higher results from the companys gas infrastructure 
business. these were offset by higher interest costs (primarily 
due to investment in the business), weak market conditions 
affecting our merchant texas gas assets, lower earnings from 
the companys proprietary power and gas trading, and writeoffs associated with the planned future repowering of two 
wind projects in California.
energy resources added 754 MW of wind generation in 
north america in 2010. at the end of the year, we owned 
8,298 MW of wind generation. not only do we remain the 
leading owner of wind generation in the united States, but we 
are now the fourth largest wind generation owner in Canada. 
energy resources also signed long-term power purchase 
agreements on a record 1,238 MW of wind generation in 
2010, increasing financial certainty in this part of the business. 
all told, 94 percent of the 2011 gross margin and 88 percent 
of the 2012 gross margin on our existing power generation 
assets was contracted or hedged as of Dec. 31, 2010. 
In May 2010, we indicated that we thought we could add 
3,500 to 5,000 MW of wind generation by the end of 2014 
from a 2009 base of roughly 7,500 MW. We continue to 
believe we can accomplish this goal. one opportunity we 
see over the next few years is repowering older wind 
sites. In the fourth quarter of 2010, we signed an agreement 
to repower a 78-MW wind facility in California, and over 
the next four years we see 300 to 400 MW of repowering 
opportunities.
on the solar front, our Genesis project in the Mojave Desert 
was approved by the California public utilities Commission, 
the Bureau of land Management, and the California energy 
Commission in 2010. Site work began in January 2011, and 
we expect the projects twin 125-MW arrays to be completed 
in 2013 and 2014, respectively.
our two 50-MW solar projects in extremadura, Spain, 
also made progress in 2010. In December, the Spanish 
government published its revised feed-in tariff. as we had 
expected, the net effect of the changes was roughly neutral 
to our project. Combined, the full construction of Genesis 
and Spain Solar would represent an approximately $2.3 
billion investment in solar energy, and we expect the projects 
to be significant earnings drivers for the company starting in 
the 2013/2014 timeframe.
on a smaller scale, we acquired approximately 40 MW of 
solar projects currently under development in Canada, 
and we commissioned the 5-MW paradise solar project in 
new Jersey.
By leveraging our leading position in wind energy, which 
has given us strong relationships with customers, a skilled 
land-acquisition team, excellent construction capabilities, 
and superior resource analytics, we expect to add 400 to 
600 MW of solar projects by the end of 2014 from a 2009 
base of 148 MW of owned capacity. Significantly, we already 
have long-term contracts for 390 MW of our planned solar 
development.
The Transition to a Clean-Energy Economy 
the country underwent a significant shift in the political balance 
of power last november, one with broad ramifications for 
americas energy policy. While an explicit price on carbon, 
which is something I continue to believe in, is not on the horizon, 
I still believe it is possible for policymakers to reach agreement on an all of the above energy package that encourages more 
renewables along with other forms of generation. 
over the last 50 years, the nation has been on a path of 
steady environmental improvement. although environmental 
concerns subside during economic downturns, I see nothing 
that overturns the countrys basic commitment to gradually 
strengthening its environmental protections. the transition 
to a clean-energy economy will happen. the pace may have 
changed, but the ultimate destination has not, and almost no 
company is better positioned for that shift than nextera energy. 
this shift has the potential not just to address our environmental 
challenges but to spur new clean-energy jobs. over the past 
five years, nextera energy has invested more than $25 billion 
in the nations electrical infrastructure and supported more 
than 60,000 direct and indirect jobs, and we plan to invest at 
least as much over the next five years.
Recognition for Our Leadership Role
as nextera energy has grown its leadership position in the 
electric power industry over the past decade, the recognition 
the company receives continues to increase. In 2011, we 
were named no. 1 in our sector on Fortune magazines 
Most admired Companies list for the fifth year in a row. as 
a part of this annual analysis, Fortune ranks companies on 
individual attributes, and in 2011 we were named one of the 
10 most socially responsible companies in the world, joining 
industry leaders such as Whole Foods Market, Walt Disney 
and nestl. In 2010, we were named among the 10 most 
innovative companies in the world, along with apple, Google, 
Intel and others.
We received recognition in other areas as well in 2010. We 
were named to the prestigious Dow Jones Sustainability 
Index for the second year in a row and to the Carbon 
Disclosure projects leadership Index for the third year in 
a row. For corporate responsibility, we were named one of 
the nations 100 Best Corporate Citizens for the second 
time. and we were named one of the Worlds Most ethical 
Companies by Ethisphere magazine, making us one of only 
36 companies in the world to appear on the list all four years 
it has existed. on corporate governance, we were one of only 
43 companies out of more than 4,200 evaluated to receive a 
perfect score of 10.0 from Governance Metrics International. 
Ready for the Next Era 
When I wrote to you in this space one year ago, I was still 
Chairman and Ceo of a company called Fpl Group. now, 
of course, we are nextera energy, Inc., and the change went 
so smoothly and seems so natural that its hard to believe it 
happened only nine months ago. to me, the most important 
aspect of the name change is how it properly re-brands the 
company as a forward-looking enterprise that sees the future 
and gets there first.
to help guide nextera energy into that future, we added 
a new member to the Board of Directors in 2010. Kenneth 
B. Dunn, retired dean of the tepper School of Business at 
Carnegie Mellon university, joined us in July 2010. His keen 
market insights and financial acumen will serve us well as we 
continue to build our business. Kens appointment increased 
the size of our board from 12 to 13 members.
In closing, I want to thank nextera energys 15,000 employees 
for their talent and hard work, and particularly for their fine 
effort in a very challenging year. I am convinced that we are 
better positioned now to deliver value to our shareholders 
than we were 10 years ago, when we began a period of 
outperforming our benchmark by 107 percent to 57 percent in 
total shareholder return.
Lewis Hay, III
Chairman and Chief executive officer
  