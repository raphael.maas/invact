DEAR FELLOW SHAREHOLDERS

In the year since our last letter to you, we have
witnessed a recession that was deeper and more
traumatic than any in recent memory. Unemployment
levels entered double digits, mortgage delinquencies
in the United States reached record levels, and foreclosure
filings followed suit. And yet, despite these
occurrences, Hudson Citys proven business model
and conservative banking principles sheltered your
investment from the worst of the storm. While the
current economic conditions brought increasing
levels of non-performing loans and charge-offs,
they were not significant enough to imperil our
11th straight year of record earnings.
THE SECRET TO OUR SUCCESS
Hudson Citys financial performance is not the result
of precarious financial engineering instruments,
such as hedges or derivatives, nor is it an outgrowth
of additional fees for investment banking or mortgage
banking services. Instead, Hudson City achieved
record performance by making sensible residential
first mortgage loans, which we continue to keep
on our own books. During 2009, we were able to
fund substantially all of our loan production with
deposit growth.
We believe in doing business the right way. Hudson
Citys customers can feel confident that what you
see is what you get. We have never offered paymentoption
loans or loans with negative amortization.
And our deposit customers receive similar benefits
of honesty and transparency, as well as assurance
that our deposit products do not contain hidden
fees and charges. Furthermore, to meet our customers
service expectations, we manage the small details,
such as ensuring that every Hudson City branch has
its own phone number (answered by a person willing
to help you) rather than requiring customers to talk
to a machine or respond to telephone prompts. In
turn, our business model and core values have led
to a consistent track record of financial strength and
a high degree of customer loyalty that increase
long-term shareholder value.
RECORD EARNINGS AND MORE
Earnings for the year totaled $527.2 million, or
$1.07 per diluted share. This is an 18.3% increase
over 2008. During 2009, our net interest margin
grew to 2.21% from 1.96% in 2008 as funding costs
decreased at a faster pace than the yields on our
interest-earning assets. One important component

of our earnings growth is expense management.
Through our industry-leading efficiency ratio, we
are able to maintain a competitive advantage in
both deposit and loan products. In 2009, our
efficiency ratio was 20.80%, meaning that it cost
us $0.21 in overhead to produce one dollar of
revenue. We maintain our efficiency, in part,
because we stay focused on our customers
mortgage and savings needs rather than trying
to be all things to all people. We simply target
customers looking for exceptional value and
deliver competitive deposit yields, low fees, and
competitive mortgage rates.
We grew our assets by $6.12 billion during 2009
to $60.27 billion at December 31, 2009, comprising
loan growth of $2.28 billion and additions to our
securities portfolio of $3.41 billion. This achievement
was funded with deposit growth of $6.12 billion.
Our loan production in 2009 amounted to $9.22
billion as compared to $8.10 billion in 2008. The
increase in loan production was due in large part
to customers who refinanced loans from other
banks. We regard these as very high-quality
loans because they meet our strict underwriting
standards based on an updated property appraisal,
and they were previously performing loans at
other banks that are simply repricing to a lower rate.
REALITY CHECK
With all of the good news to report about our
performance during 2009, there is no escaping
that the economic recession affected our asset
quality, as it did with other banks. Our primary
loan products are residential first mortgage
loans. As unemployment rates rise, some
borrowers find it difficult to make regular loan
payments. Compounding the unemployment
effects are weak housing markets that make it
challenging to sell a home at just the time that
declining house prices are reducing borrowers
equity. Non-performing loans amounted to
$627.7 million at December 31, 2009, as compared

to $217.6 million at December 31, 2008. At
December 31, 2009, non-performing loans
included $613.6 million of one- to four-family
first mortgage loans as compared to $207.0
million at December 31, 2008. Although we
experienced increased levels of charge-offs in
2009, which totaled $47.2 million, our chargeoffs
have been moderated by our loan product
philosophy and underwriting standards that
resulted in an average loan-to-value ratio, using
appraised values at origination, of 61% at yearend
which has helped to protect our portfolio.
We are very pleased with our record deposit
growth in 2009. Many customers sought out
Hudson City as a safe haven for their money
during these tumultuous times. Our long-standing
reputation and financial strength helped us to
increase deposit market share in 96% of our
branch locations for the period June 2008
through June 2009. Furthermore, Hudson City
branches average more than double the depositsper-
branch of the Banks competitors, amounting
to $188 million in deposits-per-branch compared
to the national average of $76 million for FDICinsured
institutions.We believe this is an important
indication of customer satisfaction.

Our financial results since Hudson Citys initial
public offering in 1999 demonstrate our ability
to leverage capital and provide consistent earnings
growth. We believe that as the economy emerges
fromthe recession, there will be further opportunities
for Hudson City to continue to grow its franchise

and prosper. In December, we filed a shelf registration
statement with the Securities and Exchange Commission.
The shelf registration statement enables us to easily
access the capital markets should an opportunistic
transaction arise. In addition, we believe that the
banking regulators, as well as the banking committees
of the United States Congress, may be calling for
increased regulatory capital requirements. While we
are currently considered to be well capitalized with
a Tier 1 leverage capital ratio of 7.59% and a total
risk-based capital ratio of 21.02%, this shelf registration
statement affords us greater flexibility to continue to
execute our businessmodel and to pursue opportunities
that current market conditions may provide.
A BOND OF TRUST
In summary, Hudson City is performing admirably
and is well positioned for the future. While there
is uncertainty surrounding banking legislation and
regulatory reform, we believe that our financial
strength, strong capital position, and customerfocused
business model will enable us to benefit
from a housing market recovery and strengthening
economic conditions when they occur. In the meantime,
we expect our business model to continue to
serve our shareholders well. It has been this strategy

of sticking to our core values and principles that has
yielded record earnings and allowed us to continue
to prosper without the need for government assistance.
On behalf of the Board of Directors and all of our
employees, we thank you for your confidence and
pledge to continue to earn your trust. We will continue
to lead the Bank in a way that makes you proud 
with quality, passion, and integrity.
Ronald E. Hermance, Jr.
Chairman, President and Chief Executive Officer
Denis J. Salamone
Senior Executive Vice President and Chief Operating Officer