2016 RESULTS
2016 was another year of strong performance
for Whirlpool Corporation as we delivered record
results for the fifth consecutive year through
strong operational execution and decisive actions
to adjust to changes around the world.
Throughout the year, we quickly adapted our plans and focused on what we could control to more than offset continued
global economic volatility. We delivered earnings per share growth of 14 percent, revenue growth of 1.6 percent, excluding
the impact of currency, and $630 million in free cash flow.
During the past few years, macroeconomic volatility has been a factor in many of the countries in which we operate. We
continued to experience that volatility during 2016. The Brexit decision in June had a negative impact on British currency
and demand, while uncertainty in emerging markets generated additional currency and demand weakness, especially in
Brazil, Russia and China. These challenges, along with a further strengthening U.S. dollar, had a combined negative impact
of approximately $600 million in revenue and $2 per share of earnings.
Our earnings growth and strong cash generation enabled us to create long-term shareholder value through the execution of
our balanced capital allocation approach. We invested in our innovation pipeline through $660 million in capital expenditures
while increasing our dividend by 11 percent and buying back $525 million in common stock. These investments continue to
be supported by a strong balance sheet, an increased capacity to invest and the confidence that our operating plans will
deliver extraordinary levels of shareholder value both now and in the future.
FRAMEWORK FOR LONG-TERM VALUE CREATION
Our long-term value creation framework is built upon the strong foundation we have in place: our industry-leading brand
portfolio and robust product innovation pipeline, supported by our best-cost global operating platform and executed by
our exceptional employees throughout the world. We measure these value-creation components by focusing on a few
key measures:
 Deliver 3 to 5 percent annual organic net sales growth across our global footprint
 Grow earnings per share by 10 to 15 percent annually
 Expand EBIT margins to 10 percent plus by 2020, through strong cost productivity programs and further leveraging our
strong brands and innovative new products
 Generate free cash flow of 5 to 6 percent of net sales by 2018, which represents over 85 percent earnings to free
cash conversion
We remain confident in our ability to effectively manage our business regardless of the operating environment and expect
to continue delivering long-term value for all of our shareholders.
GLOBAL BRAND AND PRODUCT LEADERSHIP
As we have done consistently, we prioritized reinvesting in our business during the year despite changes in the external
environment. We continued to invest in product innovation, with approximately $660 million in capital expenditures, more
than $600 million in research and development and more than $350 million in brand marketing investments. Our ongoing
commitment to fully invest in both core appliances and adjacent businesses helped us launch more than 100 innovative
new products in 2016.
We also made significant progress on our acquisition-integration activities in Europe and China. We remain on track to
deliver planned cost synergies and restructuring benefits as we realized more than $200 million of benefits globally during
the year. Our product platform integrations in Europe remain on schedule, as by mid-2017 more than 80 percent of our
products will have been transitioned or optimized as we work to identify the best of the best across our transformed
European business. We also advanced our integration in China, including investing in a new technology center and
manufacturing operations in Hefei to support our growth strategy in this important emerging market.
Our industry-leading brand portfolio, which includes seven brands with more than one billion dollars in revenue, remains
unmatched in the industry. The combination of our global and regional brands uniquely positions us to reach more than
90 percent of consumers globally. When combined with our strong innovation pipeline, these brands allow us to bring
meaningful innovation to consumers throughout the world.
PEOPLE EXCELLENCE AND OPERATING EXCELLENCE
The passion and commitment of our people are key enablers of our success. The strength of our global teams is magnified
by the experience of our leadership teams and their daily execution of our plans. In spite of significant global economic
volatility, our employees in every region executed decisive actions to bring our innovation to consumers. The record results
we delivered in 2016 were only possible through the hard work and dedication of our more than 93,000 employees.
The combined excellence of our people and our world-class operating platform enables us to fully invest in our strong
brands and product innovation pipeline. The resulting combination of brand strength, product innovation and best cost
structure is a winning formula which has helped us deliver record financial results.
LOOKING FORWARD TO 2017
As we look to 2017 and beyond, we continue to see multiple avenues for profitable growth and margin expansion.
In the United States, we anticipate strong replacement appliance demand as well as further growth of housing starts
and existing home sales in 2017. The long-term trends demonstrate strong demand for housing, real wage growth, healthy
consumer sentiment and low levels of unemployment. Each of the past 16 quarters has seen appliance industry growth
in the U.S., and we are confident that the fundamentals underpinning the industry will support further growth in the
coming years.
Internationally, our strong footprint and operating platform across emerging markets will position us to capitalize on future
growth in those economies. We are seeing signs of continued strength in India, demand recovery across Eastern Europe
and stabilization in Brazil as we head into 2017. The strong dollar era will likely persist, though we do not anticipate currency
devaluation at the levels we experienced in 2015 and 2016. We do expect some continued global economic volatility,
specifically in the United Kingdom, and some increase in raw material costs. In total, we expect the cumulative impact
of global economic volatility to be less of a headwind in 2017 than in recent years.
There are significant opportunities for growth in all parts of our business, and we will continue to prioritize investing in
innovation to launch new products throughout the world. We also remain committed to cost productivity by leveraging our
right-sized fixed cost structure and reducing platform complexity globally.
In 2017, we expect to deliver another record year through the focused execution of our strategy, with earnings per share
growth of 916 percent and generating approximately $1 billion in free cash flow. We will continue our balanced approach
to capital allocation, including reinvesting in our business, attractive dividend payouts, meaningful share repurchases and
assessing value-creating M&A opportunities.
AN ENDURING VISION OF VALUE CREATION
We remain committed to being the best global branded consumer products company in every home around the world.
Inspired by that vision, our long-term value creation strategy remains unchanged. We have demonstrated time and again
our ability to overcome the challenges of a volatile global environment through the resolve of our people and the strength
of our global footprint. We will continue to leverage our global operating platform, consumer-relevant innovations and
unmatched brand portfolio, and we believe our ability to generate future earnings growth and free cash flow is very strong.
Thank you for your continued support of our brands and products and your investments in our company. We look forward
to furthering our legacy of long-term value creation for our shareholders while improving the lives of consumers throughout
the world every day.
Jeff M. Fettig
Chairman of the Board and
Chief Executive Officer