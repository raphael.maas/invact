To Our Shareholders:

Throughout 2011, we have executed well against our primary objectives: optimizing
our existing businesses and efficiently deploying capital. We are particularly pleased
with the momentum we have developed throughout our growing franchise.
Since completing the Smithtown and RiverBank transactions in December 2010 and,
more recently, the Danversbank transaction on June 30 of last year, we have successfully
integrated and converted three banks, taking out more costs than were estimated at
the time of each transaction. Integration and systems conversions have become a core
competency for this organization.
As a result of our acquisition of Danvers Bancorp and five de novo branches, we now
have a total of 30 branches in the Boston market and plan additional branch openings
there in 2012. Combined with our legacy Massachusetts franchise, we are currently
the seventh largest bank in both the state and the Boston metropolitan statistical area
(MSA), with 59 branches statewide.

We also anticipate increasing our presence in New York when we complete our recently-announced acquisition
of 56 branches from Citizens Financial Group, Inc., which will bring our total number of branches across Long
Island and southern New York state to 94 at the transactions close.
The positive reception for Peoples United in both the Boston and Long Island markets is a testament to our
strong reputation, which is built on our 170 year old brand and outstanding customer service throughout the
financial crisis. Further, we offer the full breadth of products and services our customers need, while providing
a true relationship-based solution.
Our organic growth rates are well in excess of peers and are driven by strong relationships within legacy
markets as well as success in entering new markets. Organic growth is further enhanced through a reinvigorated
asset based lending effort and a business banking team that garnered six national and two regional awards from
Greenwich Associates for Excellence in Small Business Banking. We were the only New England-headquartered
bank recognized in the firms survey this year.
Our 2011 financial results were impressive.
 Revenue: Our revenue was $1.2 billion in 2011, an increase of 27%.
 Efficiency ratio: Our efficiency ratio was 64.4% compared to 72.4% for 2010, reflecting 800 basis points
of improvement. We achieved this by successfully integrating prior acquisitions, making better use of technology
and widening spans of control to create a flatter, more efficient organization.
 Operating earnings: Our operating earnings were $237 million, up 89%.
 Operating leverage: Of our total increase in revenue, 43% reached our bottom line, notable because,
as we are all aware, growing revenue faster than expenses leads to success in any industry.
 Asset quality: Non-performing assets as a percentage of loans and other real estate owned stood at 2.00%
as of December 31, 2011, approximately one third lower than our peers. Net loan charge-offs were 28 basis points
for the year, which placed us in the top 10% of our national peers. Asset quality has been and continues to be
industry-leading.
 Loans and deposits per share: Loans per share grew by 20% and deposits per share grew by 18%.
Over the past three years, loans and deposits per share have grown at compound annual rates of 18%
and 14%, respectively.
 Operating earnings per share: Our operating earnings per share were $0.68, up 94%.
 Operating returns: Our operating return on average assets was 91 basis points, up 60%. Our operating
return on average tangible equity was 7.4%, an increase of 111%. Normalizing our capital base to peer levels,
operating return on average tangible equity was 12.5%, comparing favorably to peers.

 Capital levels: Our tangible common equity to tangible assets ratio stood at 12.0% as of December 31,
approximately 400 basis points above our peers. We have made progress in regard to capital deployment,
but still have work to do, considering our low risk profile.
 Return to shareholders: During 2011, we returned $468 million  or over 10% of our current market
capitalization  to shareholders in the form of dividends ($221 million) and repurchases ($247 million).
SIGNIFICANT OPPORTUNITIES
Our three largest opportunities are in deepening our presence in our target markets, enhancing cross-sell
initiatives and improving operating leverage.
Deepening Our Presence
Given our 170-year track record of superior customer service, our large bank capabilities and decades of
consolidation within the industry, we are uniquely positioned. We have a historic opportunity as we deepen
our presence in all of our markets, particularly the Boston and New York City MSAs. We lead with our employees
who embody our brand and the superior customer service that Peoples United Bank has come to represent.
On February 5, 2012, we launched a new marketing campaign to deliver our message throughout the footprint
with the tagline, What know-how can do.SM
As I noted earlier, on February 28, 2012 we announced the acquisition of $325 million in deposits in 56
branches from Citizens Financial Group, Inc. These branches are located primarily within Stop & Shop stores
on Long Island and in Westchester County, NY. In each of the last 14 years we have been ranked the #1 operator
of in-store branches in the country. That success is built on our deep relationship with Stop & Shop and the
connection between our traditional and in-store branch networks. With a strong traditional branch presence
on Long Island and in Westchester County, we expect the newly acquired in-store branches will provide a lift
to traffic in all of our branches within these regions.
Enhancing Cross-Sell
We have excellent capabilities to deliver a broad product set to meet our customers goals; our opportunity is
to serve our customers more completely. We will earn incremental business from current customers through
persistence, solid training, appropriate incentive-based compensation plans and a strong understanding of
customers individual needs.
Improving Operating Leverage
We are targeting and remain on track to reach a 55% efficiency ratio run-rate by mid-2013. We will carefully
eliminate expenses without impairing revenue growth or customer service. We have benchmarked the entire
company to determine which areas are outsized relative to market norms and where spans of control have
become too narrow. We have established the Expense Management Oversight Committee, which provides
a forum to analyze costs horizontally in addition to the traditional vertical, or business unit, perspective.
Technology introduced over the past year and in 2012 will allow us to better leverage our teams talents.
Finally, we have stepped up our focus on capital allocation and will be carefully evaluating the profitability
and strategic importance of properties and business units throughout the franchise.
As we continue to grow organically, enhance our cross-sell efforts and reduce the absolute level of expenses,
our true earnings power and franchise value will become increasingly apparent.
Thank you for your continued support.
Sincerely,
John P. Barnes
President and Chief Executive Officer
Peoples United Financial, Inc.
February 28, 2012