2011 LETTER
To SHAREHoLDERS

at Newell Rubbermaid we are sharpening our
choices and strengthening our capabilities to
accelerate performance. The transformation that
began six years ago building Brands That Matter
and insight-driven innovations that win in the
marketplace has created a solid foundation. we now
have a stronger and more tightly focused portfolio
of leading brands with a margin structure that
allows brand investment. as 2012 unfolds we are
writing the next chapter of the Newell Rubbermaid
story. Our new Growth Game Plan is the strategy
we are implementing to fulfill our ambition to build
a bigger, faster-growing, more global and more
profitable company

2011 Results Mixed
Our 2011 financial performance was mixed, with weaker results in the first half of the year offset by a
stronger second half.
 Net sales grew to $5.9 billion, a 3.6 percent increase from the prior year;
 Core sales, which exclude the impact of foreign currency, grew 1.8 percent, the net result of nominal
growth in the first half of the year and 3.5 percent growth in the second half;
 Normalized earnings per share increased 6 percent to $1.59 per share;*
 Normalized operating margin contracted 20 basis points to 12.5 percent, as productivity, pricing and
mix did not fully offset input cost inflation;*
 Operating cash flow remained healthy at $561 million.
Our robust cash flow allowed us to both strengthen our financial position and return increased value
to shareholders. We reduced debt by $192 million, closing the year at $2.2 billion, our lowest debt
level since 2007. In addition, we returned value to shareholders through a 60 percent increase in
our quarterly dividend and the repurchase of 3.4 million shares.
In addition to our financial performance, the business accomplished a number of milestones that will
help lay the foundation for future growth.
 Completed the implementation of an SAP enterprise resource planning system across our North American
business and are preparing for a second quarter 2012 go-live in our Europe, Middle East and Africa region;
 Designed and launched Project Renewal, an initiative designed to simplify the business and release
$90 to $100 million of cost savings to invest back into growth;
 Introduced major innovation platforms including InkJoy, HYGENTM Clean Water System, and
Parker IngenuityTM;
 Invested in major marketing campaigns including Sharpies Uncap Whats Inside and Irwins
National Tradesmen Day;
 Redesigned our North American customer development organization into One Newell teams that will
carry the full bag of Newell Rubbermaid products to our strategic customer partners.
Accelerating Growth
Im convinced that Newell Rubbermaid is poised to write the next chapter of our story, one based
on unlocking the full potential of the organization to enable accelerated growth. Earlier this year we
launched our Growth Game Plan, a powerful new multi-year strategy to deliver this acceleration.
At the heart of the Growth Game Plan is a belief in growth as our primary source of long-term value.
We operate in global markets totaling over $120 billion but have an average market share of only
5 percent despite our portfolio of leading brands and fragmented competition. This means we have
a real opportunity to invest in our brands in order to build market shares to their full potential and
achieve a leading competitive position for the long term.

There are a lot of big ideas in the Growth Game Plan. Its a transformation strategy that will direct
our efforts for the next several years, and we have already begun putting the plan into action.
The first step was to simplify our business. We have refocused around two fields of play, Consumer
and Professional. Our previous organization of 13 autonomous global business units averaging only
$450 million of sales made it difficult to maximize the value of the corporation. Our resources were
atomized around too many independent, small activities. By organizing around two fields of play and
defining a clear repeatable business model for each, we can bring scale and coherence to what we
do in order to maximize growth.
Secondly, we need to make sharper portfolio choices in order to fuel accelerated growth of our brands
and businesses that have the greatest right to win. We have to go where the growth is happening in
the world. The developed world represents about 85 percent of our sales today but only yields about
13 percent of our core sales growth. The long-term health of the company requires us to consolidate
our developed market shares to clear leadership positions and expand our geographic footprint
so we have big positions in the faster growing emerging markets. By creating a Where to Play
framework that prioritizes our best business ideas, we will be single-minded about redeploying
resources to higher-growth emerging market opportunities, while tasking slower growth businesses
to win more effectively within their current geographic footprints. We expect all of our businesses to
grow, but in different ways and at different rates.

The final element of our Growth Game Plan we call Five Ways to Win. It comprises a call to
action on five separate fronts to drive accelerated growth:
1 Make our brands really matter. We will grow all our brands by refocusing on the highest-impact
growth levers.
2 Build an execution powerhouse. We will strengthen the organizations capabilities where it
matters most.
3 Unlock the trapped capacity for growth. We will continue to simplify the business and direct
increased resources to consumer- and customer-facing activities that create demand.
4 Develop the team for accelerated growth. We will build a performance culture focused on output.
5 Extend beyond our borders. We will build a truly international organization, including much more
scale on the ground in key emerging markets.
With the clarity of our two fields of play and the Where to Play choices, guided by our Five Ways
to Win, our efforts can be much better aligned with our growth platforms. This is a multi-year
strategy and there is a lot to do, but we are already getting started.
The people of Newell Rubbermaid are energized and excited by the challenge inherent in our new
Growth Game Plan. Every day I encounter new evidence that our employees are truly strengthening
their game. I could not be more confident of their commitment to our Growth Game Plan or their
passion to drive increased performance going forward. We have a clear path to build a bigger,
faster-growing, more global and more profitable company.
Sincerely,
Michael B. Polk
President and Chief Executive Officer