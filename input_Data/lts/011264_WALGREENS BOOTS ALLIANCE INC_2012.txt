Dear Shareholders:
We are excited to bring you up to date on the bold new direction
Walgreens is taking to help ensure market leadership, growth and
profitability well into the future.
Four years ago, Walgreens launched one of the most important strategic
and operational transformations since our founding in 1901, driven by a
series of growth strategies. As we continue to advance these strategies,
we have made substantial progress in our transformation. And fiscal 2012
was a breakthrough year in this transformation as we expanded globally
and digitally, turned drugstores into health and daily living destinations,
emerged as a leading force in community healthcare, and launched a
groundbreaking customer loyalty program called Balance Rewards.
Today, Walgreens is much more than a traditional drugstore  were fast
becoming a global pharmacy-led, health and wellbeing enterprise inspired
by a humble purpose: To help people get well, stay well and live well.
By pursuing our purpose, Walgreens seeks to own the strategic territory
of well, the place where health and happiness come together for our
customers. That intersection is both a physical place  with Walgreens
8,000 stores on some of the best corners in America  and also a virtual
place with constant advances in online and mobile access.
In short, by helping people to get, stay and live well at the corner of
happy and healthy, we can achieve our goal to become My Walgreens
for everyone in America, the first choice in health and daily living.
Creating the first global pharmacy-led, health and wellbeing enterprise
In fiscal 2012, Walgreens further crystallized and accelerated our strategic
transformation with dynamic new initiatives and disciplined execution.
Through this strategic focus, Walgreens has established a strong platform
for growth and long-term value creation, both globally and in expanded
lines of business.
The most powerful accelerant of our growth strategies this year was the
strategic partnership we announced in June 2012 with the Swiss-based
Alliance Boots GmbH, to create the worlds first pharmacy-led, health
and wellbeing enterprise.
The WalgreensAlliance Boots partnership combines the strengths and
expertise of two iconic brands with complementary geographic footprints,
shared values and a heritage of trusted healthcare services dating back
more than 100 years. It also positions our Company for shortand
long-term growth in three major ways:
 Accelerates our key growth strategies. Bringing together the strengths
and expertise of each company will drive forward Walgreens strategies
to: 1) transform the traditional drugstore to a health and daily living
destination; 2) advance community pharmacy; 3) delight our customers
with an outstanding experience through enhanced employee engagement;
4) expand across new channels and markets; and 5) reinvent our cost
structure through continuous improvement and innovation.
 Creates an unmatched supply chain. Together, Walgreens and Alliance
Boots have the largest pharmaceutical wholesale and distribution
network in the world, including associates and joint ventures, with more
than 370 distribution centers, delivering to more than 170,000 pharmacies,
doctors, health centers and hospitals in 21 countries. Well also be the
worlds largest single purchaser of prescription drugs and one of the largest
global buyers of many health, beauty and other daily living products.
 Provides a platform for global expansion beyond the U.S. and Europe.
Walgreens is already the largest pharmacy chain in the United States,
and Alliance Boots is the leading pharmacy-led, health and beauty group
across Europe, with a fast-growing presence in emerging markets.
Together, the WalgreensAlliance Boots global pharmacy-led, health
and wellbeing platform is positioned for selling and distributing products
in 26 countries through a supply chain unmatched in the industry.
The strategic partnership with Alliance Boots also is expected to unlock
significant economic value for shareholders, as Walgreens goal is for
the combined synergies across both companies to deliver $100 million
to $150 million in the first year and $1 billion by the end of 2016.
We also expect the transaction to be accretive to Walgreens net earnings
per diluted share in fiscal 2013 by roughly $0.23 to $0.27, excluding
one-time transaction costs.
While fiscal 2012 was marked by the Alliance Boots transaction and
other strategic breakthroughs, it also brought significant headwinds.
First, overall economic conditions remained weak, affecting consumer
spending. Second and more significantly, since January 2012, we made
a strategic decision that Walgreens would not participate in a major
pharmacy benefit network, Express Scripts, as we were unable to reach a
contract agreement that provided fair reimbursement for the pharmacy,
health and wellness services we provide. We said at the time that we
believed the long-term ramifications of accepting Express Scripts proposal

with below-market rates and minimal predictability for the services
we provide would have been much greater than any short-term impact
to our earnings.
In July, however, we announced with Express Scripts that we had reached
a multiyear agreement that works for both parties. This agreement is
consistent with our Companys principles, and is good for our shareholders,
our employees and the patients and communities we serve. In the meantime,
we strengthened relationships with many other pharmacy and healthcare
partners that value the choice, cost-effectiveness, convenience and service
that Walgreens provides to its patients.
For the fiscal year, Walgreens maintained total sales at $71.6 billion,
achieved profits of $2.1 billion, controlled selling, general and administrative
costs, and generated record operating cash flow of $4.4 billion.
Overall, we returned $1.9 billion to shareholders through dividends
and share repurchases for the year. Over the past five years, Walgreens
dividend achieved a compound annual growth rate of nearly 24 percent,
compared with 3 percent for the Dow Jones Industrial Average and
1 percent for the S&P 500.
Progress on core growth strategies and initiatives
In fiscal 2012, we escalated the pace and progress of our transformation
by substantially advancing and executing each of our five key growth
strategies and new initiatives.
Transform the traditional drugstore:
Walgreens continued to redefine and revolutionize the drugstore experience.
As todays busy and budget-stretched consumer seeks quality and convenience
as well as value, Walgreens is positioned to give them what they
want, when they want it, and where they want it. That is why our vision
of My Walgreens is much more than a drugstore, but a retail health
and daily living destination, one that provides convenient multichannel
access to goods and services with best-in-class customer experience.
To that end, this year we completed our three-year plan to refresh Walgreens
stores with our customer-centric retailing initiative across the chain
that enhances the stores look and feel and the shoppers experience,
leading to higher customer satisfaction and increased sales.
Raising the bar even higher, we ramped up the rollout of our groundbreaking
Well Experience pilot stores and store formats in select markets. These
stores bring together new urban retail design concepts, enhanced product
assortments, innovations in our pharmacies and health clinics, improved
customer engagement and new e-commerce technologies from our
multichannel strategy. All position Walgreens to step out of the traditional
drugstore format to create a new and unique customer experience.
Designed around customer needs, Well Experience stores include several
drugstore innovations. Pharmacists are located in front of the pharmacy,
out from behind the counter, offering greater and more personal patient
interaction and consultation. Nurse practitioners are located adjacent to
the pharmacies, providing broader healthcare services, from vaccinations
and health testing to non-emergency medical treatment. Well Experience
stores also feature our expanded fresh food selections, as well as enhanced
beauty departments with an array of prestige and niche cosmetic,
skincare and haircare brands not typically found in drugstores.
Through 2012, we have now opened or converted nearly 350 Walgreens
locations with our Well Experience format, including a market-wide
transformation of all 68 Walgreens stores in the Indianapolis area, and
four expansive new upscale flagship stores in Chicago, New York City,
Las Vegas and Puerto Rico. These 20,000-plus square foot stores
feature everything from sushi bars, juice and smoothie bars and
cafes to manicure stations. Customers walk in, look around, and say,
Wow  this is Walgreens?
As we reinvent the drugstore experience, Walgreens continued to expand
our selection of private-label health and daily living brands that deliver
real value to consumers. One of our newest private brands, Nice!, which
launched in 2011, offers more than 400 high-quality grocery and
household products at prices up to 30 percent below other national
brands, and joins our Good & Delish and Pet Shoppe brands.
Walgreens stores also offer prepared meals for immediate consumption
by on-the-go consumers, expanded grocery items for quick, convenient
pickup, and a growing selection of fresh produce and healthy food
choices that are crucial to underserved food desert communities
lacking convenient nutritional options.
By serving as a food oasis in many of our communities, Walgreens has
been recognized by First Lady Michelle Obama through her Lets Move
initiative for helping to fight the childhood obesity epidemic. This is one
way Walgreens is able to do good while doing good business.
The WalgreensAlliance Boots partnership will accelerate our strategy
to transform the traditional drugstore. Not only will Walgreens offer
Alliance Boots global health and beauty brands, including the broadly
popular No7 and Botanics brands, but we will also have access to
the impressive Alliance Boots product development laboratories based
in Nottingham, England, to bring even more unique health and beauty
products to the market, our shelves and consumers around the world.
Then, as fiscal 2013 opened, Walgreens launched a groundbreaking
customer loyalty program called Balance Rewards that offers
easy enrollment, instant points and endless rewards. It even rewards
customers for healthy behavior such as getting flu shots and exercise.
Balance Rewards will take our Well Experience to a new level.
Customers can enroll easily at the store counter, online, or with
a mobile device, redeem points instantly for nearly all store or

online products, and receive bonus awards when they keep coming back.
We award points for prescriptions  and even when members join in the
Walk with Walgreens personal exercise program. The customer insights we
learn through Balance Rewards also will enhance our customer experience.
Advance community pharmacy:
The U.S. Supreme Courts decision upholding the core provisions of the
Patient Protection and Affordable Care Act was the years top healthcare
news. In a rapidly evolving healthcare market, Walgreens continued to
seize new opportunities to deliver convenient, high-quality, affordable care 
and help address the nations healthcare needs.
Building from our foundation of approximately 8,000 stores, more than
26,000 pharmacists, more than 37,000 pharmacy technicians and 1,300
nurse practitioners at the 700 Take Care Clinics and workplace health and
wellness centers, Walgreens is quickly moving forward to serve as a leading
community pharmacy, health and wellness provider, and valued partner for
payers and patients, as the first place for daily healthcare. Our healthcare
strategy is designed to link all patient touch points and channels  stores,
online, telephone and home delivery  to achieve three goals:
 Enhance our core pharmacy business. We are promoting and providing
medication adherence, diabetes management, childrens health, wellness
education, HIV care and other services that build trusted relationships
by providing best-in-class patient experience. To that end, we are pursuing
efficiencies that free up pharmacist time to engage with patients and practice
at the highest level of their profession.
 Expand beyond our core pharmacy business to provide broader retail health
and wellness services, from prevention and screening services to chronic
care management. Walgreens acquired BioScrip, Inc.s community specialty
pharmacy business to expand our role in serving HIV, oncology and transplant
patients and others with chronic and complex health conditions that require
intensive drug therapies. We are also expanding nurse practitioners scope of
services beyond minor episodic care and broadening their duties to include
closer collaboration with primary care physicians.
 Evolve from our traditional pharmacy, health and wellness business to
become a valued partner for doctors, health plans and health systems, helping
to manage a variety of patient populations while improving total health
outcomes and lowering costs.
Recognizing how we are transforming community pharmacy, Fast Company
magazine named Walgreens to its list of Most Innovative Health Care Companies
for the second time in three years. Demonstrating this innovation, Walgreens
continued to be the nations number-one private sector provider of flu
vaccinations. When the U.S. Secretary of Health and Human Services,
Kathleen Sebelius, the nations top healthcare official, received her flu shot
last winter, she went to Walgreens.
Delight our customers:
By Well Experience, Walgreens means more than stores, product selections
and prices that delight. Well Experience comes down to the women and
men of Walgreens who deliver health and happiness when they engage our
customers and patients.
As we reimagined our stores, we also raised the bar on employee-customer
engagement with significant investments in training and development of our
leadership and store teams. The curriculum includes an intensive Leadership
Academy and, in fiscal 2013, Walgreens University for employees.
Also, this year we launched a new customer-experience curriculum and change
management program for our front-line store team members, emphasizing
three principles of service: Easy  make it easy for our customers to do business
with us; Recognize  care for and connect with customers; and Solve  own
our customers issues, and see them through to resolution. Store managers
are training to better engage and motivate employees.
Our customer experience pilots have focused on giving our customers a
different experience than all other retailers at the key touch points in
pharmacy, beauty and daily living. In addition, we now have rich customer
segmentation data that will allow us to differentiate even further,
personalize our services and measure our effectiveness. For example,
we are piloting a service model for chronic-care patients, such as those
with diabetes or HIV, that will help them take control of their healthcare
and wellness decisions.
Our goal is to ensure every Walgreens employee adopts an extraordinary
customer-service mindset, and that we embed actions and behaviors that
advance our customer care.
Expand across new channels and markets:
As the world becomes more interconnected through global commerce and
e-commerce, Walgreens continued extending our reach to new channels
and markets.
We broke even more new ground this year in harnessing e-commerce and
mobile technology. Leveraging our convenient brick-and-mortar locations
with leading-edge online options, we are giving customers even more of what
they want  a broader selection of products and services, where they want
it  whether delivered to their homes or available for pickup at the store,
and when they want it  two days, next day or next hour.
As we advanced our integration of drugstore.com  a leading online retailer
we purchased last year, which has performed beyond expectations 
we opened a new 50,000-square-foot e-commerce distribution facility.
We also launched new technologies for customers to interact with Walgreens
through smartphones and apps.
Walgreens can now send a text message to patients when their prescriptions
are due for refill, which patients can complete with a simple refill reply
text. Combining the convenience of online shopping with our neighborhood
stores, we expanded Web Pickup services at pilot locations, allowing
consumers to shop online at Walgreens.com and pick up orders at their
Walgreens store in as little as an hour.
Going further, customers who check in at any Walgreens store through
Foursquare on their smartphones instantly receive a unique scannable coupon
on their phones that is redeemable in the store, with no texting, reply or
other steps required. Customers can also print photos at any store directly
from their iPhone and Android mobile devices. And to advance our role as a
community healthcare leader, Walgreens can now deliver patient data directly
to primary care providers to improve the coordination of care.
Expanding into new markets, with the Alliance Boots partnership, Walgreens
now has interests in businesses in Europe and across the globe.
Together, Walgreens and Alliance Boots will have unmatched supply chain and
procurement expertise, offering customers innovative solutions and optimal
efficiencies. Well have a portfolio of retail and business brands, including
global health and beauty product brands that will be unparalleled in the
industry. This will give us a unique platform for growth and the opportunity
to further develop a diversified and robust profit pool across the U.S., Europe
and key emerging markets.
The partnership also advances Alliance Boots strategies. As Stefano Pessina,
executive chairman of Alliance Boots, said, This strategic transaction represents
a further vital step in achieving our vision of becoming a global pharmacy-led
healthcare leader. We believe that it will bring clear benefits to all stakeholders,
creating significant and sustainable industrial value through synergies and
the deployment of our joint expertise. The fit is natural; Walgreens consumer
profile in the U.S. is similar to Boots in the U.K. in many ways: a trusted and
much-loved pharmacy brand with a strong heritage.
In the transaction, Walgreens exchanged $4.0 billion in cash and 83.4 million
shares of stock for a 45 percent equity ownership stake in Alliance Boots,
and will have the option to proceed to a full combination by acquiring
the remaining 55 percent of Alliance Boots within the next three years.
The transaction has been structured to allow synergies to be realized by
the respective management teams working closely together on key projects,
while providing the opportunity to progress to full integration. Both companies
believe this transaction structure increases the potential for value creation,
while reducing the initial business disruption and allowing time for
thoughtful integration planning.

Meanwhile, Walgreens continued to expand its presence in the U.S.
market. This year, we purchased a 144-store regional drugstore chain
in the mid-South region, which includes USA Drug, Super D Drug,
Mays Drug, Med-X and Drug Warehouse stores.
Reinvent our cost structure through continuous improvement
and innovation:
After achieving our goal of delivering more than $1 billion in annual
pre-tax cost savings from 2008 to 2011, Walgreens continued process
improvement and innovation have become part of our corporate DNA
at the Company.
As we continued driving significant improvements in our cost structure,
controlling our expenses, and making surgical cost reductions as needed,
these steps helped our Company to weather the still-recovering economy
and the disruption to our prescription volume this year. They also helped
us free up investment capital for our strategic initiatives. Numerous
advances in operating efficiency  including energy-saving measures
that make Walgreens a retail leader in environmental protection 
also helped reduce costs.
The WalgreensAlliance Boots partnership provides real cost-efficiency
opportunities, most significantly in four areas: 1) procurement synergies,
including purchasing prescription and over-the-counter drugs;
2) daily living products; 3) goods purchased not for resale; and
4) revenue synergies from introducing Alliance Boots product brands
to Walgreens and Duane Reade stores. We also expect revenue synergies
from sharing best practices, particularly in pharmacy operations,
health and wellness services and logistics.
Looking ahead
In fiscal 2013, managements overarching focus will be on three major
areas: First, executing the Alliance Boots strategic partnership and
realizing the anticipated synergies and benefits as the first global
pharmacy-led, health and wellbeing enterprise. Second, expanding our
Well Experience stores and concepts throughout Walgreens. And third,
advancing the role that community pharmacy can play in healthcare
across the communities we serve every day.
We expect to make substantial progress in each of these areas with
a relentless focus on disciplined execution and pursuit of optimum
efficiency  all toward reaching our long-term goals of double-digit
growth in earnings per share, increasing return on invested capital,
and top-tier shareholder returns. Looking ahead, we believe we are
right on track to deliver.
On a more personal note, Walgreens steady and substantial progress in
our strategic transformation over the past several years stands in tribute
to the vision, leadership and steady guidance of Al McNally, our nonexecutive
chairman of the board from 2009 until this year, and who has
served on the board of directors since 1999. Al has played an instrumental
and indispensable role as Walgreens developed and advanced our transformational
strategies. Importantly, his mantra of disciplined planning
and execution has become hardwired throughout the management team
and company. As Al rotates from his role as non-executive chairman,
consistent with board policy, we are grateful that Walgreens will continue
to benefit from his service on the board.
In further changes to the Walgreens board, we welcomed new director
Janice M. Babiak, former partner at Ernst & Young LLP. Also, with the
completion of Walgreens initial investment in Alliance Boots in August,
Stefano Pessina, executive chairman of Alliance Boots, and Dominic
Murphy, director and member of KKR & Co. L.P., joined the Walgreens
board of directors, as several members of Walgreens most senior
management team joined the Alliance Boots board of directors.
This was a challenging year for our 240,000 employees. We asked a lot
from them. But with their usual determination, commitment and spirit
of service, fueled by the delight they provide customers and patients, the
Walgreens team stepped up, rolled with the changes, met the challenges
head on, and pulled closer together to deliver Well at Walgreens. Our
progress this year in advancing our transformation while achieving solid
results is a tribute to these remarkable men and women. It is also a
tribute to you, our shareholders, for your trust and confidence in our
Company, and we sincerely thank you.
James A. Skinner Gregory D. Wasson
Chairman of the Board President and Chief Executive Officer
October 22, 2012