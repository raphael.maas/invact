Dear Shareholders,
2012 was a year of significant accomplishment at Time Warner Cable. We delivered good results
while making the investments needed for longer-term success in a highly competitive business.
And I am pleased to report that, as a result of our actions, we continued to build shareholder value.
2012 Accomplishments and Highlights
We drove healthy year-over-year growth in many key financial and operational metrics in 2012:
^ Total company revenue increased 8.7% to $21.4 billion.
^ Business Services revenue increased 29.4% to $1.9 billion.
^ Advertising revenue increased 19.7% to $1.1 billionan all-time record.
^ Primary service units (PSUs)the sum of video, high-speed data and voice subscribers for
both residential and business serviceswere 28.9 million at year end, up from 27.1 million at
the start of 2012.
^ Operating income increased 9.2% to $4.4 billion.
^ Diluted earnings per share grew to $6.90 from $4.97.
^ Free cash flow topped $2.5 billion.
Given our strong cash flow and solid balance sheet, we paid dividends and repurchased
Time Warner Cable shares totaling $2.6 billion and acquired Insight Communications for $3 billion.
Taking a closer look at the steps we took to drive our 2012 performance and better position
the company for the future:
^ We increased the speed of our standard tier of residential Internet service by 50%, and our
Internet customers now have even more choices with the addition of optional usage-based
tiers available in nearly 90% of our footprint. We added 10,000 WiFi access points, which, when
combined with the thousands of hot spots activated by our WiFi arrangements with other cable
companies, give our Internet customers access to more than 50,000 hotspots nationwide, one
of the most robust WiFi networks in the U.S. And we improved the mix of our subscriber base,
with more subscribers to our Turbo, Extreme and Ultimate tiers.
^ We continued to enhance our TWC TV platform. With up to 300 live linear channels
available on the iOS and Android platforms, PCs and MACs, as well as on Roku devices, we
believe we offer the most advanced linear IP video product in the industry. We also added
4,000 on demand assets to the iOS app. Our apps have been well received; in December alone,
more than three-quarters of a million unique customers used the TWC TV app
almost four million times. In addition, we bolstered our programming lineups
in 2012, particularly in sports, and we launched two new regional sports
networks (RSNs) to serve as home to the Los Angeles Lakers in Southern
CaliforniaTime Warner Cable SportsNet and Time Warner Cable
Deportes, the first-ever Spanish language RSN in the U.S.

^ We continued to enhance our residential phone product. We introduced voicemail to
text and visual voicemail for iOS and Android, as well as our Global Penny Phone Plan
which allows calls to over 40 countries for just a penny a minute.
^ On the customer service front, we introduced one-hour service windows almost
everywhere in our footprintand, in select markets, we added guaranteed
appointment times at the start of each shift. We also more than doubled the
self-installation rate on a year-over-year basis in the fourth quarter. And we overhauled
our customer-facing web and app presence, including launching the MyTWC customer
service app, which achieved 650,000 downloads and enabled customers to engage with
VoiceZone, bill pay and account management features on a range of devices.
^ We successfully integrated the former Insight Communications systems, acquired on
February 29, 2012, leveraging greater economies of scale and offering our new customers
state-of-the-art technology and new product innovations that will make their lives simpler
and easier.
^ We accelerated our investment in the people, plant and products for our rapidly-growing
Business Services. In 2012, we nearly doubled our fiber connectivity to commercial
properties in the U.S.
^ We increased the network capacity that underlies our high-speed data offerings to
accommodate more traffic and deliver higher speeds. We also took the first step in
consolidating our technical operations to support the companys entire footprint with the
opening of our first national Data Center, a 178,000-square-foot facility in Charlotte, NC.
In addition, we built out our own content delivery network, or CDN, so we can effectively
deliver IP video without reliance on third party providers.
Not only did we do great things on the business front, but we also continued to play an
important role in the communities we serve.
^ Thanks to the heroic efforts of our employees during Superstorm Sandy, more than 80%
of Time Warner Cables 300,000 impacted customers in the Greater New York-New Jersey
Metropolitan area had their services restored within five days after Sandy hit. Beyond our
operations, philanthropic efforts included food trucks that served 30,000 meals, mobile
charging and WiFi stations for severely
impacted communities without power
and connectivity, house-to-house
visits to check on the well-being
of our neighbors, support
for community clean-up
projects and donations
of $600,000 to
storm relief.

^ We passed the halfway mark to connecting a million young
minds to the wonders of science, technology, engineering and
math (STEM) through our Connect a Million Minds initiative. I could
not be more proud of the work we are doing to advance the STEM
fields for our industry and the country.
Focus for 2013
We enter 2013 with a strong and steady business, but also with the realization that we need to continually
step up our game to meet the challenges of a world in which technology changes faster each year and our
competitors grow more adept. In 2013, we are focused on five areas for driving long-term, profitable growth.
Residential Services: Focus on improvements in packaging, customer retention and the overall customer
experienceincluding product enhancements and better customer service.
Business Services: Increase our share with small businesses; continue to move up market to serve
mid-sized businesses; and expand our presence in the growing carrier spaceincluding through cell tower
backhaul. To do this, we will continue to invest in expanding our sales force, connecting more buildings and
towers to our network and enhancing our product portfolio.
Operational Effectiveness: Drive continuous improvement, with initiatives in areas such as field operations,
call centers and dispatch to reduce unnecessary work, reduce waste and drive standard best practices
companywideall while improving the quality of our customers experience.
Investments in Our Future: Bring more content to more devicesanytime, anywhere; more
than double our WiFi network; and launch our next-generation home security, monitoring,
automation and control platform system-wide in 2013, up from approximately 25% of our
footprint at year-end 2012.
Disciplined Financial Management: Actively manage our balance sheet, and drive long-term
shareholder value through rigorous and disciplined investment decisions that lead to
consistently strong cash flow generation. Continue to return excess capital to shareholders
through both dividends and share repurchases.
I applaud the strong resolve of our more than 51,000 Time Warner Cable employees to help our customers
enjoy better all of the things that they are passionate aboutand to deliver on that brand promise ethically,
transparently and with a firm commitment to the mission and values that we strive to uphold every day. I am
incredibly proud to note that, each year, our company earns recognition from a variety of organizations, both
for our people and as one of the top places to work.
We remain confident in the long-term prospects for our business. On behalf of everyone at Time Warner Cable,
thank you for entrusting us with the management of your company.
Sincerely,
Glenn A. Britt
Chairman and Chief Executive Officer
March 2013