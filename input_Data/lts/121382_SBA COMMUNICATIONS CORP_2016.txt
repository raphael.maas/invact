to our shareholders:
2016 was another solid and steady year for SBA.
Throughout the year, we continued to execute
well operationally against the constant backdrop
of growing wireless demand. Customer activity, in
the form of organic growth through newly signed
leases and amendments, remained comparable to
2015 levels as our customers remained focused on
network quality and reliability. During the year, we
saw continued growth in site leasing revenue, tower
cash flow, adjusted EBITDA, AFFO and AFFO per
share. Also during 2016, we rolled out our longterm goal of producing at least $10.00 of AFFO
per share by 2020. We take a long-term view of
our business, and we believe AFFO per share has
the greatest impact on shareholder value creation.
It underscores the strength of our business and
long-term recurring cash flow potential of SBA.
In order to maximize growth in AFFO per share,
we will continue focusing on same-tower organic
growth, operational excellence, optimizing our
capital structure, and a disciplined approach to
capital allocation.
Organic growth continues to be the top priority
for SBA as we believe organic growth is the
most valuable type of growth we can produce,
as it typically comes with very little additional
capital investment. We ended 2016 with a 7.0%
same tower year-over-year gross organic leasing
revenue growth rate in the United States and
11.6% for our international business, in both cases
excluding the impact of churn. Organic growth is
primarily made up of contractual escalators and
new tenant leases and amendments to existing
sites. 2016 represented a steady and consistent
amount of new leasing activity in the United
States primarily from the refarming of 2G and 3G
spectrum to LTE, as well as some AWS-1 and
700 MHz spectrum deployments. There remains
much additional spectrum to be deployed. Further
deployments such as AWS-3, WCS, 2.5 GHz, 600
MHz and FirstNet will require new equipment to
be installed at existing and possibly new macro
sites, supporting steady organic growth for
years to come. International prospects for future
spectrum deployments and the related need for
additional equipment are similarly bright. We
believe the high quality of our assets, exclusive
nature of our sites, and the critical role our assets
play in our customers� wireless networks all
provide a solid foundation for continued organic
growth moving forward.
To maximize the impact of organic growth, we
need to execute well. We pride ourselves in our
operational execution, measured by our margin
performance. In 2016, we posted the highest
EBITDA and tower cash flow margins in company
history; 69.1% and 79.6%, respectively. We believe
these margins reflect our ownership of the highest
quality assets in the industry and our operational 

excellence. We have ample opportunity to expand
these margins in the future through new tenant
revenue additions with very little added expense,
as well as direct cost reductions through land
purchases and prepaid ground lease extensions.
With respect to the latter, in 2016 we spent $66.2
million on land buyouts, easements and ground
lease term extensions.
We continuously focus on optimizing our capital
structure. Our balance sheet is in great shape
with plenty of liquidity and financial flexibility. SBA
continues to be a preferred issuer in the debt capital
markets as illustrated by the recent securitization
and High Yield refinancings we completed in
2016, both resulting in reduced coupon interest
rates. We reduced our year-end overall weighted
average cash interest rate by 40 basis points, as
compared to the end of 2015. Notwithstanding the
fact that we operate with materially higher balance
sheet leverage than our U.S. public tower company
peers, we maintain one of the lowest aggregate
average costs of debt in our industry at 3.5%. We
continue to feel very confident about how we have
structured our balance sheet with our current mix
of debt and equity, staggered debt maturities and
access to multiple different debt markets. In 2016,
we raised $1.8 billion in debt financings and ended
the year with a leverage ratio of 7.6x, just outside
of our target range of 7.0x to 7.5x net debt to LQA
EBITDA. We ended the year with a leverage
ratio slightly above our target range because we
took advantage of some very attractive stock
repurchase opportunities during the fourth quarter.
We also increased our EBITDA to cash interest
coverage ratio by a half turn compared to the end
of 2015 to 3.8x. We believe our focus on optimal
balance sheet structure will continue to be a
meaningful driver of value for SBA.
We believe we allocated capital wisely in 2016,
investing almost $900 million in new assets and
stock repurchases. We built 372 new sites and
acquired 531 sites, adding 903 total sites to our
company in 2016. We ended the year with over
26,000 sites owned worldwide. While the number
of sites added in 2016 is comparatively lower than
in years past, we remain committed to achieving
portfolio growth that meets our investment criteria
and is ultimately accretive to AFFO per share.
While we did not achieve our portfolio growth goals
of 5% to 10% annually in 2016, it was not because
of a lack of opportunities. We simply thought the
better decision long-term for our shareholders
was to pass on some relatively unattractively
priced acquisition opportunities and invest the
capital instead in stock repurchases. However,
we believe there will continue to be many new
site opportunities available to us and we remain
committed to continuing to grow our portfolio.
While new assets are our first choice for allocating
capital, we are very disciplined about the price we
will pay. If we cannot find the right opportunities
at the right prices we will allocate capital towards
share buybacks when we believe our stock is
intrinsically undervalued. We did just that in
2016. We spent an aggregate of $550 million on 


buybacks at an average price of $102.16, which
we believe to be a terrific value, retiring almost
5.4 million shares. We will continue to remain
flexible and opportunistic around where we
allocate our capital.
Our international footprint continued to grow in
2016, including expanding into a new market,
Chile. Chile is a stable economy and has many
needs for continued wireless investment. It has
familiar, high quality investment grade tenants,
including Am�rica M�vil and Telef�nica, customers
that we know very well. We are excited about the
potential for this new SBA market. In our existing
international markets, we continued to see a
healthy mix of amendments and new leases, tilted
slightly towards new leases. We would expect this
dynamic to continue as these markets are generally
less mature and require more points-of-presence
to provide adequate coverage, similar to the United
States wireless market of fifteen years ago. While
our focus has primarily been on the Western
Hemisphere, we look at any and all opportunities
across the globe. For any new market, we look
for a number of positive characteristics before
we invest, such as the need for future wireless
investment, the strength and number of wireless
providers, political and currency stability and
our ability to achieve the necessary scale of our
operations. We are excited about the prospects
of continued portfolio growth in both our existing
international markets and potential new markets.
Approximately 8% of our 2016 total cash site leasing
revenue, excluding pass-through reimbursable
expenses, is denominated in currencies other
than the U.S. dollar. Almost all of that revenue was
generated in Brazil. Foreign currency exchange
volatility in Brazil remained a headwind in 2016,
although the currency strengthened during the
second half of the year, and has recently been
more stable. On a currency neutral basis, we had
a steady year, and even saw some increases in
operational leasing activity in the fourth quarter
of the year. We continue to remain convinced that
Brazil will be an excellent long-term investment.
Demographic trends, smartphone sales, network
needs, new spectrum and the competitive carrier
dynamic will continue to support Brazil as a
growth market for network investment for many
years to come.
During 2016, we began operating in compliance
with all requirements necessary to be treated
as a Real Estate Investment Trust (a �REIT�)
and, subsequent to year-end, we completed the
merger of SBA Communications Corporation into
a wholly owned subsidiary in order to facilitate 


our compliance with certain REIT rules. We are
excited to formally join the REIT community as
we believe REIT status will be an important step
in creating additional shareholder value. While
dividends are going to be a part of our future,
we will likely not issue a dividend until we have
exhausted our existing net operating loss carryforward positions, which are estimated to last four
to five more years. We do not anticipate our REIT
tax election to materially change our operations or
capital allocation decisions in the near term.
We are as optimistic about the future as we
are pleased with our past success and value
creation. The future is bright, driven by strong
expectations around growing demand for mobile
data services, particularly video. Analysts
and prognosticators all uniformly predict
continued explosive growth in wireless. Here
are a few highlights from Cisco�s annual �Visual
Networking Index: Global Mobile Data Traffic
Forecast Update, 2016�2021 White Paper�:
� Global mobile data traffic grew 63% in 2016.
� Mobile data traffic has grown 18-fold over the
past five years.
� Almost half a billion (429 million) mobile
devices and connections were added in 2016.
� Globally, smart devices represented 46%
of the total mobile devices and connections
in 2016.
� Projected 47% growth per year in global
mobile data traffic over the next five years.
- 45% and 35% in Latin America and North
America, respectively.
This type of demand can only be met with
increased network enhancements, whether
through the deployment of new spectrum or, in the
absence of new spectrum, through cell-splitting
and densification. Given the strong anticipated
demand and the high quality of our assets, we are
well positioned and intend to remain a key player
and partner in wireless network infrastructure
around the globe.
In closing, I must recognize the contributions of
our employees and customers to our success. I
also want to thank you, our shareholders, for your
continued confidence in and support of SBA. I look
forward to communicating with you again soon.
Sincerely,
Jeffrey A. Stoops
President and Chief Executive Officer