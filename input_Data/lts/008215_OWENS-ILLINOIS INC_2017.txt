Dear Stakeholder,
Marked by positive progress and improved performance, 2017 was another successful year for O-I.
I�m inspired by the encouraging feedback I�m hearing from customers, the focus and conviction I see
from our employees, and the improved results we�re delivering, quarter after quarter.
Through year-end 2017, we delivered eight consecutive quarters of strong earnings and cash flow
generation, meeting or beating the street�s expectations. O-I achieved full-year adjusted earnings of
$2.65 per share, an increase of 15 percent versus the prior year, as we continued to deleverage the
balance sheet.
Building additional capabilities in the commercial, manufacturing and supply chain space added to the
top line with higher shipments year-on-year, and the bottom line, through tangible benefits of our total
systems cost approach. Working together as one enterprise, enhancing the customer experience and
executing on our strategic initiatives has helped us grow earnings and generate strong cash flow.
Looking forward into 2018, our transformational journey will continue. We are planning for significant
asset investments early in the year, repairing assets and improving their flexibility and reliability. In
addition, strategic investments in our footprint will enhance our ability to meet customer needs from a
more sustainable, value-generating cost point. We continue to address productivity across all
businesses, functions, processes and geographies by better managing costs, improving our
organizational design and capabilities and implementing best-in-class business practices.
I am extremely pleased with the progress realized across the enterprise. Our employees are more
networked and collaborative � for the benefit of all stakeholders� like never before. We continue to
build a very simple, effective and engaged organization.
In 2018, we expect to meet or beat all the key financial targets we shared during Investor Day 2016.
We expect positive trends in volume across the globe and further improvement in total systems cost.
The expected increase in segment operating profit and margin expansion should drive further growth
in earnings and free cash flow. In turn, we have shifted towards a more balanced capital allocation �
investing in O-I stock via repurchases and continuing to deleverage.
In closing, I want to thank you for your support and reiterate my optimism for O-I�s bright future. I
remain confident that the steps we are taking will drive long-term sustainable success for our
shareholders, customers and employees.
Best Regards,
Andres Lopez
Chief Executive Officer 

March 29, 2018 
