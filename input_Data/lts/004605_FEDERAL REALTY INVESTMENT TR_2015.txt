DEAR
 SHAREHOLDERS,

A few months back, I was struggling
to find a way to articulate our business
strategy and talk about the many
initiatives and projects that we had
underway in 2015. As a company,
weve been around for more than
50 years and are keenly aware that
commercial real estate is prone to
the same ups and downs and other
cyclicality as the overall U.S. economy.

When market conditions are not kind to the acquisition
market, development often looks better; when leasing is
weak in one area of the country, its often strong in another
and so on. Its why weve always worked hard to balance
our business plan in every possible respect so as to get
through the inevitable cycles with less volatility than we would
otherwise be subject to. But how to articulate that? And then
it hit me: baseball!
Ive loved the game since I was a kid and always marveled
at how a few guys could do it all. They had the core athletic
skills to hit for both power and batting average. They had
great defensive fielding ability and arm strength. And of
course they had elite speed. Baseball scouts and others in
the game refer to a player who has exceptional abilities in
all five of those core skill areas as a five-tool player. In the
history of baseball, there are only a handful of players who
can legitimately be considered big-league five-tool players.
Willie Mays is one of them, Ken Griffey Jr. is another.
It is this exceptional balance and level of competence to
which Federal Realty aspires. It is the completeness of the
business planthe determination not to be dependent on
any one mode of growththat were most proud of, and that
was most evident in 2015. So, what are our five tools?

IT IS THE COMPLETENESS OF THE
BUSINESS PLANTHE DETERMINATION
TO NOT BE DEPENDENT ON ANY
ONE MODE OF GROWTHTHAT WE
ARE MOST PROUD OF.

First, the diversity of our core portfolio in terms of retail
format, geography and tenant base, all in the highest-quality
locations in the country. That real estate portfolio produces
a formidable core foundation on which to grow (one
that produced same-store growth of more than 3.5% for the
fourth year in a row).
Second is the skill set built and honed over many years
to re-develop those centers. The teamwork and
collaboration to pull off these large and small tactical
re-developments necessitate the industrys most creative
and innovative leasing, development and management
personneland as I write this, we have more than a dozen
such re-developments underway.
Third, we have pioneered and refined our unique knowledge
and skill set to create vibrant and enduring mixed-use
communities through a mixed-use development
organization that is second to none. Renowned names
like Santana Row and Bethesda Row are being followed
with tomorrows great communities like Pike & Rose and
Assembly Row. More than $170 million was invested in
these initiatives in 2015 alone, over $1.5 billion cumulatively.
Fourth is not only our ability to acquire shopping centers
(anybody can do that), but our focus and refinement of an
acquisition approach that sees opportunities in real estate
acquisitions that others dont see, in terms of finding
re-development opportunities that refill our re-development
and development coffers. Our acquisitions in 2015 of
San Antonio Center, CocoWalk and Sunset Place are
prime examples.

And fifth and finally, the refinement of our balance sheet
over many years (to ensure low leverage through regular,
right-sized equity issuances, unsecured borrowings and
well-laddered maturities lead to the maximum flexibility)
afforded the strongest balance sheet in our sectorand
one of only three (out of 198) public REITs with an A credit
rating by both Moodys and Standard & Poors.
We stumble a bit now and then (as did Willie and Griffey),
but our constant striving to be the strongest five-tool player
keeps us all very focused on our craft. In 2015, the net
result of that concentration was an increase to the declared
dividends per share to our shareholders for the 48th
consecutive year in a row (no other REIT can say that),
a 7.7% increase in funds from operations (FFO) per share
(excluding debt prepayment costs), and hundreds of millions
of dollars of capital investment in real estate that should
continue to stoke the fire of earnings and value creation for
many years to come.
The consistency, stability and balance of our business plan
is a relentless pursuit for our team, which is made up of the
most dedicated and talented real estate professionals in the
industry, governed by a Board of Trustees solidly focused on
shareholders interests.
Were truly a family of colleagues at work, constantly
pushing and challenging one another and making the
necessary judgment calls that are hardly ever black and
white. On behalf of each of us, thank you for the privilege
of managing your Company, and we look forward to being
part of your investment portfolio for the long term.

Sincerely,
Donald C. Wood
President & CEO