PUTTING A FACE ON OUR VALUE
To Our Shareholders: At ITT Industries we spend a great deal of time focusing on creating value. Value has many
dimensionsto customers, to investors and to employees. But the real value of ITT Industries is embodied in our people.
This years annual report captures that value in the faces of more than 150 of the men and women who serve our
customers and our shareholders around the world each day. In addition to their own achievements, they represent the
creativity and commitment of 38,000 coworkers, who together make this a strong company. In the pages that follow,
we tell a few of their stories  stories of creativity, heroism, imagination and dogged determination to find a better way
to pursue a higher purpose.
Our stated vision is to become a premier multi-industry company. In 2002, we made progress on this path to
premier status. By any measure, 2002 was a strong year for our company  with significant growth in revenues, earnings
and cash flows, despite significant market pressures and economic challenges. These results did not just happen;
they were only made possible by the collective efforts of thousands of individuals.
2002 : GROWTH IN A DIFFICULT ENVIRONMENT
With our ongoing focus on creating value, Im very proud of our performance in 2002. Our revenues for the year were
up seven percent to $4.99 billion. Earnings reached $379.9 million, a nearly 22 percent increase over 2001, after adjusting
for the adoption of Statement of Financial Accounting Standards No. 142. Cash flows from operations grew 25 percent
to $595 million, providing the funding needed for several strategic acquisitions, and enabling us to make voluntary
prefunding contributions of $250 million to our U.S. salaried pension plan in 2002 and 2003. We also recommended to
our Board a seven percent increase in our quarterly dividend, and this was approved for the first quarter of 2003.
Despite the sluggish economy, three of our four business segments increased sales and posted better than expected
growth during the year. Our defense business realized significant new contract wins and grew its backlog to a record
$2.8 billion. We experienced strong demand for our water and wastewater treatment products and continued strong
activity in our automotive fluid handling systems and leisure marine businesses. This growth more than offset adverse
market conditions in the industrial pumps sector and a further decline in demand for electronic components, most
notably from market conditions in the communications and commercial aircraft markets.
STAYING TRUE TO OUR VISION AND VALUES
During the past year, the confidence of the investing public was shaken by endless news of accounting scandals, insider
trading and a disturbing lack of oversight by those responsible for corporate governance. During these uncertain
times, we have worked even harder to maintain the trust placed in us by all of our stakeholders. At
ITT Industries, our commitment to ethical behavior is captured on the cover page of our Code of Conduct: Do the
right thing  always. This phrase provides a foundation for the values with which we do business.
Our Code of Conduct has served as the focal point for our statement of business principles for many years. It is
published in multiple languages, updated on a regular basis and distributed to all of our employees so that they have a
clear and unequivocal understanding of our commitment to the highest ethical standards. This code lives and breathes
through the decisions and actions of our employees around the world. It is through their commitment that we maintain
the trust placed in us by our customers and shareholders.
In 2002, we were gratified that the market recognized our company for both our steady improvement in financial
performance and our ongoing history of meeting commitments. Our total return to shareholders (share price
increase plus dividends) for the year was just over 20 percent. This represents our third consecutive year of doubledigit
returns, which far surpassed the S&P 500 Index and virtually all of our peer multi-industry companies.
To achieve our vision of becoming a premier multi-industry company, we are focused on three core strategies:
leadership development, operational excellence and growth. In 2002, we made progress in each of these areas.
LEADERSHIP DEVELOPMENT
In my view, leadership development is the starting point for the deployment of our strategies. The leadership skill of all
of our people is the driving force behind our strong financial and operating results, and the reason our customers,
communities and investors have confidence in our company. We recognize that we have effective leaders at every
level of the organization. At our most recent Executive Forum, we introduced the concept of Value-Based Leadership
Development, recognizing that developing effective leaders is a critical part of increasing overall value in the company.
ITT Industries ability to maintain momentum and achieve growth in both strong and struggling markets is a testament
to our leadership talent. Our management team is a collection of forward-looking leaders who are continually
called upon to make the right decisions  tough decisions  that allow us to improve our processes, manage our costs,
satisfy our customers and inspire our employees. Their track record in this regard was impressive in 2002, and the
result was improved financial performance and increased investor confidence.
To foster leadership skills throughout the organization, in 2002 we created the position of Chief Learning
Officer (CLO) and launched the ITT Leadership Academy, which is responsible for managing our continuity planning
process, creating premier leadership development processes and developing a wide range of training and education
programs. We are cultivating hundreds of change agents who are creative, quick and decisive in developing solutions.
Leadership is not a new concept at ITT Industries. Each year, we bring together teams of high-potential employees
for an intense two-week development program at the Ashridge Management Institute in the United Kingdom. The
program has been one of our most popular leadership initiatives. To date, more than 150 employees, or nearly onethird
of our entire executive population, have completed this program. Ashridge graduates are leading ITT businesses
and teams around the globe, helping their companies to succeed in both emerging and mature markets and helping
their people to unleash their full value-creating potential. I am particularly appreciative of the efforts of John Procopio,
who was instrumental in the creation of the Ashridge program, the Leadership Academy and our new mentoring program,
and who served as our inaugural CLO.
OPERATIONAL EXCELLENCE
Value-Based Six Sigma (VBSS), the linchpin of our continuous improvement efforts, was launched three years ago and
has become an integral part of the ITT Industries culture. We are striving to make VBSS a way of thinking and managing,
and, as I like to remind our employees, VBSS is one of our most effective growth strategies. By reducing our time
to market, enhancing the quality of our products and services, and delivering these products on time, we will serve our
customers better and grow faster.
By the end of 2002, our company had trained more than 350 VBSS Black Belts and Champions to become factbased
problem-solvers, able to find solutions through a systematic and measurable process. The VBSS savings we achieved
during the year helped improve cash flow for our company.
Last year marked two important milestones in our VBSS evolution. First, most of our class instructors are now
ITT Industries employees  Master Black Belts  who have replaced outside trainers. We have now developed the
needed internal expertise to sustain our VBSS knowledge base, enabling us to tailor the curriculum to our unique organizational
values and objectives.
In addition, we greatly accelerated our VBSS efforts with the introduction of Green Belt training for 1,700 executives
and managers in the company. Our VBSS Black Belts and Champions can now devote their time and attention to
wide-ranging improvement projects, while our Green Belts bring their new skills to bear in their daily jobs. Our goal is to
train 10,000 Green Belts, weaving the continuous improvement process into the fabric of every job function.
GROWTH
The achievement of our vision starts with strong leadership at all levels of all of our businesses and by ensuring that our
products and services are of the highest quality. With these strategies in place, we can focus more directly on growth.
Value-Based Management (VBM) provides an overall framework for strategic planning in the company. Internally,
our growth is fueled by a steady stream of new, differentiated products and services. Innovative products are the lifeblood of
our company. Over the years, ITT engineers have played a critical role in the development of technical innovations that today
are commonplace, including television, fiber optics, global positioning satellites, submersible pumps and night vision.
In 2002, we launched a new effort aimed at making sure our solutions solve real customer problems. With
Value-Based Product Development (VBPD), we are training our business development, marketing and engineering

teams to capture the voice of the customer better so we can develop products that meet both the customers spoken
and unspoken needs. This VBPD training will have a major impact on future contract wins by enhancing our ability to
listen to our customers and respond to their needs with well-designed solutions.
In addition to organic growth, in 2002 we completed nine acquisitions. The new companies represent annualized
revenues of more than $160 million and are consistent with our strategy of building on strong core businesses.
Precision Stainless, Martin Petersen Company, Svedala Robot, the Pure Water Division of Waterlink, Flowtronex,
Royce Instruments, Innotech and PCI Membranes add complementary capabilities to our Fluid Technology business
and strengthen our global leadership position in the water and wastewater treatment industries.
Xybion Electronics produces rugged, intensified cameras for harsh environments and expands the ability of our
Night Vision business to meet the needs of its military and commercial customers.
In early 2003, we finalized the acquisition of VEAM/TEC, a leader in the design and manufacture of
military/aerospace connectors. With annual sales of almost $50 million and an excellent reputation for providing customized
connector solutions, this global business is a perfect complement to our Cannon connectors business.
Going forward, we remain focused on identifying attractive acquisition candidates that build on our core
strengths and allow us to expand into adjacent markets.
2002 BUSINESS SEGMENT PERFORMANCE
Our Fluid Technology segment realized nearly $2 billion in revenues in 2002, up seven percent from the prior year.
The water and wastewater-related businesses achieved 11 percent revenue growth, reflecting continued strong demand
for our pumps, technology and expertise in these markets. With Flygt, Goulds, Sanitaire and Bell & Gossett, we have
the worlds leading brand names and the biggest share of the market. Our recent acquisitions in the water and wastewater
segment further position us as a major provider of total systems solutions in this area. Two acquisitions in our
Engineered Process Solutions Group strengthened our market position in the growing biopharmaceutical market.
Our Defense Electronics & Services segment had another strong year. Revenues were up 16 percent to
$1.5 billion, and our backlog grew 10 percent through the year to a total of $2.8 billion. Our Systems Division contributed
greatly to this success, landing a large number of new service contracts to support U.S. military efforts at bases in
the Middle East and the United States, and winning a $1 billion contract to maintain and modernize the U.S. Air
Forces global network of homeland defense radars and optical sensors. Our Avionics division also had great success
with its Suite of Integrated Radio Frequency Countermeasures (SIRFC). This multiplatform, multinational program
gives aviators the ability to detect, disrupt and evade air defense threats  and has the potential for $2 billion in sales.
ITT Industries Motion & Flow Control segment recorded revenues of $935 million for 2002, up four percent
from the year before. We benefited from market share gains and favorable market conditions in the automotive and
leisure marine businesses. Additionally, we benefited from new product introductions in the leisure marine market,
such as the Sensor-Max and Rule-Mate pumps, both of which won industry awards for innovation in 2002.
In the Electronic Components business segment, revenues fell 10 percent in 2002, a result of the unprecedented
decline in the mobile telephony and telecommunications markets. The impact of this decline has been offset in part by our
growing presence in the Asia-Pacific market, which helped us win key contracts in China, Japan and Korea. In addition,
more than 36 percent of segment revenues in 2002 were generated by new products, such as our Photonic High Density
fiber optic connector. We believe we are well positioned to capitalize on any market recovery in 2003 and beyond.
NEW FACES
In 2002, we strengthened our management ranks with several key new leaders.
Scott Crum was named Senior Vice President and Director of Corporate Human Resources. He joined us after
having served in senior administration, quality improvement and human resources roles with General Instrument
Corporation, Motorola and Northrup Grumman. Scott will be responsible for establishing policy and leading the
companys global human resources strategy. He replaces Jim Fowler, who retired after 20 years of service. We appreciate
the many important contributions Jim made during his tenure at ITT.
Steve Faas joined us in early 2002 to serve as a Vice President and Chief Information Officer. Most recently, he
served in the same capacity for the Energy Products division of General Electric, and he has already made great strides in
developing a streamlined, uniform IT infrastructure that will simplify sharing information among our global businesses.
Also in 2002, we promoted Mark Steele to the newly-created position of President, ITT Industries  China.
With a dozen facilities and almost 7,000 employees in China, our interests in the region are growing rapidly, and we
look to Mark to lead our companys expansion efforts in this strategic country.
Recently, our Board elevated two corporate officers, Brenda Reichelderfer and Don Foley, to Senior Vice
Presidents of the Company. Brenda heads up our Motion & Flow Control business, while Don is the corporate
Treasurer and Director of Taxes. In addition, the Board elected Steve Gaffney, who leads our VBSS initiative, to Vice
President of the Company, reflecting the critical role played by our VBSS efforts in the Company.
In 2002, the Board selected a tenth director, Ralph Hake, who is chairman and chief executive officer of the
Maytag Corporation. As the leader of Americas third largest appliance maker, Ralph brings a broad understanding of
issues facing manufacturing companies like ITT.
I consider our Board to be a critical asset to ITT Industries. I look to them for their breadth of experience and
keen oversight. Each of our board members is an experienced leader; all have led large organizations and most currently
serve in the top leadership post in their own organizations.
We welcome the new focus on corporate governance. As an organization, we have long been committed to a
practice of ongoing forthright communication with our shareholders. In light of the many changes during the year, we
have reexamined our own compliance and disclosure practices to ensure that they meet or exceed the new legislative
and regulatory requirements. Some of these changes will be evident in this annual report and in other shareholder communication
efforts throughout the year.
Our responsibility to our shareholders is taken very seriously, and our Board plays a key role in overseeing this
process. In the past year, we have asked them to devote additional time to Board meetings and activities, and all of
them have risen to the challenge. During these meetings, the conversations are candid, productive and focused on critical
issues. I appreciate their spirited engagement and expert counsel.
LOOKING TO THE FUTURE
In planning for 2003, we are anticipating that the difficult economic and market conditions we faced in 2002 will continue.
Despite these challenges, I believe we are well positioned for continued growth, and I am confident that we can
achieve it.
This confidence is based on my personal belief that we have the right people to help guide us on the road ahead.
Our company is filled with leaders, innovators, problem-solvers and solution-providers. They are committed to solving
our customers problems and creating a work environment in which each of our employees can make a meaningful and
appreciated contribution. I am grateful to our employees for their ongoing commitment. They take pride in knowing that
the jobs they are doing truly matter to our customers and to the world at large. It is a privilege for me to be part of this team.
Louis J. Giuliano
Chairman, President and Chief Executive Officer


