
TO OUR SHAREHOLDERS:


                                                                  At Bard we believe being a market leader  having a number
These are exciting times at Bard.
We are pleased to report that our company is performing           one or two position  is critical to being successful in any
exceedingly well. We achieved excellent financial results in      market. We are disciplined in our application of this philosophy.
2003 and continued to make great progress with our strategic      In 2003, roughly 80% of our net sales came from product
growth initiatives. This success emanates  in no small part     lines in which we are market leaders. Our diversified product
from the outstanding commitment of our employees and the          portfolio  some 100 product lines covering nearly 10,000
leadership of our management team throughout the company.         product codes across our four businesses  balances the
                                                                  risks inherent in a competitive industry and contributes
At Bard we have a great sense of pride and optimism about
                                                                  significantly to Bard's consistency and reliability.
the work we do in helping people lead longer, healthier and
more productive lives. Our commitment to the patients we          One of our key operating imperatives is cultivating and
serve motivates us in our work every day and contributes          advancing our relationships with customers  nearly a
to our success. Bard's consistent financial performance is        century-old practice at Bard. Our service to customers
a tribute to our employees, who have successfully converted       differentiates us in the industry because we develop a
their desire to help people into the ability to manufacture,      customized, flexible and integrated approach to meet their
distribute and market medical technologies that make a            needs  providing a broad array of clinical and business
difference in patients' lives all over the world. We commend      solutions. Our call-point focused structure engenders natural
their efforts in achieving this goal.                             collaboration between our sales force and clinicians, the
                                                                  true source of our innovation. This relationship creates
In 2003, we completed our executive management transition
                                                                  Bard `experts' who understand first-hand the needs of
following the retirement of William H. Longfield on August 7th,
                                                                  our customers and enable us to respond with innovative
after an eight-year term as Bard's chairman and chief executive
                                                                  products and solutions targeted specifically to meet these
officer. Our Board of Directors and senior management team
                                                                  opportunities. As you read through the pages of this annual
developed and executed a comprehensive succession plan
                                                                  report, you will see why we are confident of our approach 
and, as a result, it was business as usual at Bard during this
                                                                  one that is particularly well suited to customers and
seamless transition period. We want to recognize Bill's
                                                                  differentiates us in the medical technology industry.
outstanding leadership during his tenure and look forward
to our future collaboration as he continues to serve as           During this past year, Bard businesses brought many new
a director of the company.                                        products and therapies to market, expanding our leadership
                                                                  positions in several markets and creating momentum for
Looking back over Bard's excellent financial performance
                                                                  future growth:
for the past five years, we have a lot to be excited about.
Historically speaking, our company has never performed             In our endovascular business, we gained significant
better. For 19 straight quarters, Bard has delivered consistent   momentum throughout 2003. New products, such as our
net sales growth of between 7 and 10 percent on a constant        CONQUESTTM PTA balloon catheter, FLUENCYTM stent graft and
currency basis, and we hit a new high of 11 percent net sales     RECOVERY  vena cava filter were major contributors. Our self-
growth in the fourth quarter of 2003. On all fronts, our          expanding peripheral stent products, led by our innovative
financial achievements for the year were strong.                  LUMINEXXTM stent, continued to provide significant growth in
                                                                  this area. The RECOVERY filter was the first of its kind in the
Full Year 2003 Financial Highlights:
                                                                  U.S.  giving clinicians greater flexibility in the use of vena
 Net sales growth  13% as reported; 9% constant currency
                                                                  cava filters. Clinical trials for our AV access stent graft have
 Gross profit margin  57.5% versus 54.3% in 2002
                                                                  concluded, and our carotid stent trial will begin later this year.
 R&D expenditures  $87.4 million, up 42% over 2002
                                                                  Joining the product line in 2004 is our VACORATM vacuum
 Net income  $168.5 million (up 9%) as reported
                                                                  assisted biopsy device, which will compete in the largest
 Net income  $203.7 million (up 15%) excluding the
                                                                  segment of the breast biopsy device market.
  items identified in the financial highlights on page 1
 EPS  $3.20 (up 9%) as reported                                  In our specialty access business, we introduced our new
 EPS  $3.87 (up 16%) excluding the items identified             HEMOSPLIT  dialysis access catheter with its proprietary split
  in the financial highlights on page 1                           tip design. Entering the market at mid-year, it has met with
 Cash and short-term investments (at year end)                  strong demand and made a significant contribution to sales.
  $422.0 million                                                  Our implantable port and, in particular, our PICC catheter
 Debt to total capital ratio (at year end)  13.8%               product lines continued to provide great growth in this
                                                                  business in 2003.




 Our soft tissue repair business continued its strong             construction of a 170,000 sq.ft. world-class manufacturing
performance, growing more than 20 percent for the fourth           facility in Humaco, Puerto Rico. Sized and designed to support
consecutive year. Our ventral hernia repair franchise, led by      Bard's strategic growth plans, this facility is expected to open
our VENTRALEXTM and COMPOSIX  KUGEL products, was the            in Q4 2005.
primary contributor to our performance in 2003. We were            Long-term Growth Initiatives
especially pleased to see strong growth in our international       As we have seen in 2003, the efficiencies generated from
business. Today, with more than 75 percent of our soft             operations provide the funding to support our growth plans.
tissue sales in the U.S., we see continued opportunity for         While we continue to target at least 12 percent earnings growth
growth abroad.                                                     for our shareholders, we are investing the incremental dollars
 In our urology business, the BARDEX  I.C. Foley catheter        generated from these improvements in three specific areas:
with BACTI-GUARD*silver coating continues to gain market           Technology Investment through Research and
share on its proven record for dramatically reducing urinary       Development  Our commitment to R&D investment is
tract infections  benefiting both the patient and hospital.       evidenced by an increase in R&D spending over the past
 We also continued our solid growth in sales and market           two years, from $53 million in 2001 to a run rate of nearly
share within Bard's brachytherapy business, which provides         $100 million in Q4 2003. We have implemented world-class
outstanding clinical benefits to patients diagnosed with           R&D processes across our businesses to maximize the
prostate cancer. During 2003, we made several small                productivity of our efforts and create a steady stream of
acquisitions to strengthen this franchise. They included certain   innovative technologies.
assets of Prostate Services of America, Inc. and Imagyn             Business Development  We will complement our R&D
Medical Technologies, Inc. and the assets of Source Tech           efforts with strategic acquisitions of technologies, product
Medical, LLC. These transactions reflect our strategy to           lines or businesses  targeting opportunities in the faster
be a consolidator in the brachytherapy business.                   growing segments of our markets.
Operations Review                                                   Sales Force Expansion  In 2003, we increased our sales
Over the past several years we have made significant               force by 10 percent in the U.S., concentrating on our fastest
improvements to our manufacturing structure and processes.         growing businesses. We are currently evaluating the size
Our efforts to refine purchasing, manufacturing and                and deployment of our sales organizations in the balance
distribution have generated considerable savings for the           of our U.S. businesses and in Europe.
company. Most important, these streamlining programs
                                                                   In the next several years, we believe each of these initiatives
have provided the fuel we need to fund our long-term growth
                                                                   has the potential to increase our revenue growth rate above
initiatives. We would like to acknowledge and thank our
                                                                   the range we have consistently delivered during the last
manufacturing employees all over the world for their continued
                                                                   five years.
dedication and commitment to operational excellence.
At the helm of this tremendous effort, Joseph A. Cherry,           Another significant initiative underway is the implementation
vice president of operations, has provided the leadership,         of an Enterprise Resource Planning (ERP) system, which
focus and determination to make it happen. Clearly, Joe            will enable Bard to conduct business on a common platform
has made an indelible mark on redefining Bard's future,            with a consistent flow of information across the organization.
and we thank him for his outstanding effort. The following         Among its many benefits, the ERP system will enhance order
illustrates a few of the more notable accomplishments              processing, production planning and information accuracy
in operations:                                                     and timeliness  while standardizing, integrating and
                                                                   simplifying our business processes. This new approach will
 Manufacturing cost improvements were the primary driver
                                                                   allow us to leverage the scale of a $1.4 billion enterprise in
of the 3.2% increase in gross margins as a percent of net
                                                                   our business transactions, while maintaining the agility of
sales in 2003.
                                                                   a decentralized organization, deployed at the call-point level.
 In July, we broke ground on a 412,000 sq.ft. state-of-the-art    The resources saved and efficiencies gained from these
distribution center in Covington, Georgia. This facility will      benefits will also help fuel investments for future growth.
allow us the flexibility to process consolidated orders for
                                                                   Board and Organizational Changes
products from all of our businesses, to U.S. customers.
                                                                   Throughout the year, our Board of Directors has provided
We anticipate a Q2 2004 opening of this facility.
                                                                   expertise and guidance on a variety of issues, from succession
 In August, we completed a major expansion of our                 planning to investment strategy. Their knowledge and counsel
manufacturing facility in Kulim, Kedah, Malaysia, where            are vital, and their commitment to this company is steadfast.
we have enhanced and expanded existing production                  We thank each of them for their valuable contribution.
areas and constructed new laboratories. We also initiated

                                                                                                                                 
                                                                   


This year we bid farewell to one of our long-standing directors,
Regina E. Herzlinger, who retired from the Board after 13 years
of distinguished service. Regina, a professor of business
administration at Harvard Business School, was truly a
guiding light for our company, and we will miss her counsel.
In 2003, we welcomed Theodore E. Martin to our Board. Ted
was president and chief executive officer of Barnes Group Inc.,
a manufacturer of precision metal parts and distributor of
industrial supplies, until his retirement in December 1998.
We look forward to the insight and experience Ted will bring
to the Board in the coming years.
Since our last annual report to shareholders, the Board elected
five new corporate officers: Amy S. Paul and Brian P. Kelly
to group vice president; Scott T. Lowry to vice president and
treasurer; Brian R. Barry to vice president, regulatory and        
clinical affairs; and John A. DeFord, Ph.D., to vice president,    
                                                                  
science and technology.
                                                                  
In 2003, we bid farewell to one of our distinguished corporate     
officers, James R. Adwers, M.D., our former vice president,       
medical affairs, who retired after more than eight years of        
                                                                  
exceptional service to Bard. We wish Jim well in his retirement.
                                                                   
Outlook                                                          
With further increases in product innovation and sales             
resources, we will pursue opportunities in growing markets         
to enhance our consistent and reliable performance and,            
ultimately, to yield improved returns for our shareholders.        
Our number one operating goal is to increase our revenue         
growth rate through a very deliberate and thoughtful              
                                                                  
operating strategy. Our employees are eager to meet the
challenges inherent in such an endeavor. Indeed, our               
                                                                 
achievements in 2003 are symbolic of the strength and
                                                                   
diversity of Bard and we are very excited about the future.
                                                                  
Finally, we want to thank you, our shareholders, for your         
loyalty and support. On behalf of our management team and         
employees, we are grateful for your continued confidence          
and share your high expectations for Bard.                      
                                                                 
We look forward to serving you well in 2004 and beyond.
                                                                  
Sincerely,                                                        
                                                                   
Timothy M. Ring                 John H. Weiland                   
                                                                  
Chairman and                    President and
                                                                  
Chief Executive Officer         Chief Operating Officer
                                                                 