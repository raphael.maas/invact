Message from the Chairman and the Chief Executive Officer 

2008 was a tumultuous year. With the near collapse of the global fi nancial system,
businesses suffered declines in net income and asset values and consumers stopped
spending. The Dow Jones Industrial Average dropped 34 percent, Real Estate Investment
Trust (REIT) indices dropped as much as 41 percent and consumer confi dence plummeted.
Without a doubt, 2008 was a terrible year for real estate stocks.
An economy like this reminds us how fortunate we are to be offering one of lifes
basic necessities  shelter. While consumers may put off the purchase of new cars
or fl at-screen televisions, everyone needs a place to live.
For UDR, 2008 can best be characterized by three words: transformation, preparation
and performance. In 2008, we transformed our portfolio, took aggressive steps to
prepare for our future and delivered operating performance near the top of our industry.
Transformation
We completed a $1.7 billion sale of 25,684 homes and reinvested $1 billion in acquiring 4,558 homes.
These acquisitions are newer communities in attractive urban markets, offering convenient access to large
employment centers, major transportation corridors and exciting lifestyle amenities.
Our communities are now concentrated in 23 markets, taking advantage of low single-family home affordability
and positive demographic and employment trends. The average age of our portfolio has been reduced from 24
years to 16, and our average same store monthly income per occupied home has grown to $1,176, well above
the national average of $962.
Preparation
In the midst of deteriorating capital markets and operating environments, we took aggressive steps to increase
our fi nancial fl exibility, reduce our cost structure and adjust our redevelopment and development deliveries.
We secured more than $1 billion of cash and credit capacity, by:
b Raising $194 million of equity capital;
b Closing on a $240 million, two-year unsecured term loan facility;
b Securing $438 million from the expansion and extension of a
Fannie Mae facility and other agency fi nancing; and
b Obtaining fi ve construction loans with a capacity of $179 million.

During the fi rst quarter of 2009, we further expanded our credit facilities, which, when added to existing
resources, increased our total available fi nancial resources to $1.3 billion. With debt maturities, including
available extensions, totaling just $280 million in 2009 and $348 million in 2010, we believe we have more
than enough cash and credit capacity for the foreseeable future.
We also reduced overhead costs by approximately 17 percent in 2008 and sharply curtailed development
activities to just $425 million, a 60 percent reduction from 2007.
Performance
Finally, in 2008, we continued to deliver solid operating results:
b We delivered the second-highest growth in same store net operating income among
our eleven publicly traded peers;
b We maintained high same store occupancy at 94.8 percent; and
b We continued to lead the industry in web-based marketing initiatives, with approximately
50 percent of our new leases now originating on-line.
These results enabled us to declare dividends totaling $2.28 per common share, or approximately $352 million,
consisting of a regular annual dividend of $1.32 per share, plus a special dividend of $0.96 per share arising
from our portfolio sale.
Looking Ahead
We are confident that your Company is well positioned  strategically, organizationally and financially  to
compete effectively in all the markets we serve. And we are also confi dent that, over time, the economy will
recover, markets will correct themselves and our business will participate in the inevitable rebound. We fully
expect 2009 to be a very challenging year. We have prepared for this challenge with increased financial
fl exibility, a solid portfolio and an extraordinary team of associates working every day to win their markets.
We also have a seasoned Board and management team  with many recessions under their belts 
experienced in delivering results in good times and bad.
We appreciate your continued support.

Robert C. Larson
Chairman
Thomas W. Toomey
President and
Chief Executive Offi cer