To Our Stakeholders

More than 200 million years ago when dinosaurs roamed the earth, there were frogs. While some
scientists believe todays birds may share some of their ancestry with dinosaurs, cataclysmic
changes in the planetary environment eliminated the dinosaur, as we typically think of them.
Not so the frog. In fact, frogs went on to adapt in a spectacular way to environmental change. Today there
are more than 4,900 frog species found in a remarkable variety of climates and habitats around the world.
Of course, most of us have been led to believe frogs are not nearly so well genetically coded. There
is a widespread anecdote describing a frog slowly being boiled alive. The story goes that if you put a
frog in boiling water, it will sense danger and jump out. But if you put the frog in a pot of cool water and
slowly increase the heat to boiling, the frog will not perceive the danger and will be slowly cooked. The
story is used to illustrate numerous points, including why society doesnt act to curb climate change. We
dont physically feel or smell the danger. While the story makes a great metaphor, it is only that. Modern
scientists will tell you the frog will frantically seek to jump in either case, if you give it a way out. That is
a lesson often learned the hard way. No one can predict the future with certainty, and while an adaptive
strategy developed through an analytically-based, forward-looking point of view is the foundation of
Entergys business model, it is critical to always maintain an immediate exit alternative and the will to
make that leap from the path you had planned.
At Entergy, we continue to not only adapt  for example, setting and outperforming voluntary limits
on our own CO2 emissions  but also to utilize the safety net of exiting a business strategically when risk
management is not an effective strategy for protecting the stakeholders. When the regulations in the
U.K. were changing, we anticipated the eventual market volatility. To reduce our exposure, we sold one of
our two power plants for a substantial profit and the other was put to the banks, walking away from our
modest equity investment instead of years of bleeding shareholders cash justified by wishful thinking that
we would make it up later.
In the energy trading business, when our competitors, basically Wall Street firms, began giving free
credit to non-creditworthy counterparties, we refused to compete by warehousing market-induced credit
risk. We sold the business for a substantial profit to a Wall Street firm that subsequently was one of the
firms rescued in the credit default swap debacle.
The Proposed Spin-Off and Merger of Our Transmission Business
Its a long list of actions we have taken over the years to create shareholder wealth or protect credit
quality. Most recently on Dec. 5, 2011, we announced the spin-off of our transmission business to our
shareholders and the subsequent merger of that business with ITC Holdings Corp.
We first explored the idea of creating a standalone transmission business in 1999, under a structure that
included retaining a passive ownership at Entergy. Since that time, Regional Transmission Organizations,
like the Midwest Independent Transmission System Operator and others, have developed a track record
of adding value and improving market efficiencies. Throughout this period, we consistently advocated for
independent transmission structures.
Adapting to the lessons learned over the past decade, the ITC transaction is different than we previously
proposed. It incorporates a complete spin-off of Entergys electric transmission business, or Transco.
Entergy common shareholders will continue to have ownership in the transmission business, but Entergy
Corporation will not. Immediately after the spin-off, Transco will then merge into a subsidiary of ITC. Prior
to the merger, ITC expects to effectuate a $700 million recapitalization, currently anticipated to take the
form of a one-time special dividend. The merger will result in Entergy shareholders receiving 50.1 percent
of the shares of the new ITC; existing ITC shareholders will own the remaining 49.9 percent. In addition,
ITC will assume $1.775 billion in indebtedness issued by Entergy in connection with the internal separation
of the transmission business.

Many in our industry see transmission as a way to grow
earnings or rate base, or they have a strategy that seeks ways to get bigger through mergers with other investor-owned utilities. So why? Why shrink our company, particularly by a spin-off of a fast-growing business? Our obligation is to the shareholders of the company and, of course, other stakeholders like our customers and employees. These are the real people. The company is an artificial person created by law. If the owners, customers, employees and other stakeholders are better served, then the company has achieved the purpose for which it was created. In todays changing environment, focus is far more valuable than size. In the world of mammals, most scientists will tell you larger species, like dinosaurs, may evolve faster, but become extinct more quickly: live harder, die faster. In business, sustainability is no different.
After closing the merger with ITC, Entergys transmission business will be part of a completely independent electric transmission company. ITC is a leading independent transmission company with an excellent track record of service and safety. Entergy Corporation becomes a smaller company by approximately $3 billion of assets that will be spun off to our owners. Theres no gain to the corporation and less immediate earning power for Entergy. On the other hand, the ITC transaction allows Entergy to maintain its financial flexibility, which we believe will enable ongoing investment, better access to capital and protect credit quality needed to serve our customers. The expected annual capital needs of the transmission business are four to five times the cash provided by the depreciation allowance. Moreover, this outcome is consistent with congressional intent and the Federal Energy Regulatory Commission policy and direction and addresses transmission issues raised by other entities that rely on our utility operating companies transmission system. We are listening to our regulators now to gain a better understanding of their perspectives and any concerns. Completion of the transaction is targeted for 2013, subject to receipt of necessary approvals.
Nobody likes the sound of shrinking the company or admitting that maybe a different organization can meet certain needs better than your current organization. Our employees do a phenomenal job as evidenced by receipt of the Edison Electric Institute Emergency Response Award for 14 consecutive years. As a vertically integrated utility and generation company, we handle nuclear operations, license renewal of nuclear plants, establishment of new distribution standards to meet the risks of rising sea levels, storm surges, and stronger and more frequent hurricanes, and a host of other issues. ITC wakes up every morning and only thinks transmission. They bring a single focus, and for those enamored with size, it will be one of the largest electric transmission companies in the country, with more than 30,000 miles of transmission lines.
As Adam Smith pointed out in The Wealth of Nations, when ownership is separated from management, the latter will inevitably begin to neglect the interests of the former. That is a trap we are determined to avoid. A corporation exists to serve its shareholders, not to serve itself to the detriment of its owners. I am convinced Entergys owners will be better served by this transaction and I have no doubt an independent electric transmission company that is part of an RTO with real-time markets for power is superior to any other model for customers and suppliers.
Our board has always recognized the importance of the dividend, which is taking on a more prominent role for our shareholders in todays low interest-rate environment. Even with some of the earnings growth associated with the transmission business migrating to ITC, the current long-term financial outlook supports maintaining Entergys dividend at the current $3.32 per share annualized level after closing the ITC transaction. Any dividend from ITC that our shareholders are expected to receive would be in addition to the Entergy dividend.

In this case, each Entergy shareholder receives ownership in two companies and presumably higher combined earnings and dividends. The employees of our electric transmission business receive better career opportunities as part of a transmission-only business. Our customers and suppliers will be part of the best structure to drive economic efficiency, achieve an open and robust market, and provide access for low-cost generation and efficient transmission use and expansion.
The business and financial landscape facing the utility industry is undergoing its own transformation. From volatile commodity price markets and evolving and uncertain environmental regulations, to the longer-term need for multi-trillion dollar industry-wide capital investments and potentially catastrophic risk from climate change, the future will offer unprecedented challenges. The issues we face at Entergy are no different. There is no place to hide. There is no safe path. The path we have chosen will not be easy. It will require outstanding execution; it contemplates an end-state that will create sustainable value for all stakeholders. At the same time, we believe we must be flexible and adaptable to bring our vision to reality.
Our Track Record of Adaptation
While our track record of adaptation at Entergy doesnt come close to the frogs 200 million years, we have been at it for many years. Our point-of-view-driven business model gives us the foresight to identify and effectively adapt to changing market conditions. This approach has created value for our stakeholders through major transactions such as the purchase of generation assets, including our Northern U.S. nuclear fleet, and, as I previously referenced, the formation and subsequent sales of the Entergy-Koch joint ventures trading and natural gas pipeline businesses. Our industry-leading storm response capability has been honed from years of experience, and the operational capability is now matched by efficient financial recovery and regulatory mechanisms put in place after hurricanes Katrina and Rita in 2005. We have also adapted in many other ways over the years, developing new technologies, improving processes and adding capabilities to better serve
our customers, shareholders and communities.
Our DNA:
Safety, Sustainability and Operational Excellence
Even as we have evolved as an organization, there are key elements of our company that do not change. These elements define Entergy in the same way that being amphibian with a short body, webbed digits, protruding eyes and no tail defines a frog.
We are defined by our unrelenting focus on safety and our goal of achieving an accident-free work environment. We are defined by our pursuit of sustainability, which drives us to work hand-in-hand with numerous stakeholders to achieve economic, societal and environmental priorities. We are defined by our employees drive for operational excellence throughout our organization. From our storm restoration records to our continuous nuclear operating-run records, our employees take pride in setting the standard for the industry.
With this as our DNA, we can act confidently on our points of view. We have the proven ability to adapt to even the most difficult of conditions without losing sight of the overall goal to create sustainable value for all stakeholders.
2011 Results:
Strong Operational Performance
In 2011, we took actions in our utility business and within Entergy Wholesale Commodities to adapt to and take advantage of opportunities during these changing times. We delivered strong operational performance, excellent customer service and generated record operational earnings per share for the 11th time in the past 12 years. Strategically, we made moves to bolster our generation portfolio in both businesses. We announced proposals to move the utility operating companies to MISO and then a separate transaction to spin off and merge our transmission business with ITC. We also continued our ongoing efforts towards securing renewed licenses for our Northeast nuclear fleet. Returns to shareholders reached nearly $800 million through a combination of dividends and share repurchases. And we were named again to the Dow Jones Sustainability North America Index. It marks the 10th consecutive year that Entergy has been included on either the DJSI World Index or DJSI North America Index, or both, in recognition of our sustainability leadership, a distinction held by no other U.S. utility.
At the same time our 2011 total shareholder performance was dismal in comparison to our peers. Our total shareholder return ranked in the bottom quartile of our peer group. Concerns surrounding our Northeast nuclear plants, including the Indian Point Energy Center near New York City, have contributed to limiting investors willingness to take what they often think of as political risk that is outside our control and often unpredictable. Also devastating in 2011 was our employee safety performance. We lost a long-time, dedicated and respected co-worker in a traffic-related pedestrian accident and another employee was severely injured on the job. As a result, it is tough to look back on the positives last year that many in our organization achieved without recognizing the shortcomings in the basic areas of safety and total shareholder return.

We cant change the perception of the political risk associated with nuclear power overnight; not when countries like Germany announce their intent to exit nuclear generation completely, or when a technology-savvy country like Japan experiences an event like Fukushima. All we can do is continue to safely operate our plants at the highest possible performance levels, ensure we do not shortchange risk management in the design, investment and maintenance of our plants, assure that the public has the facts relative to the safety of our plants, and rely on the regulatory and legal systems to protect our right to operate a safe plant.
More generally, the strategies, plans and initiatives under way today set the foundation toward our vision of the future for Entergy and the industry.
Our Utility Business:
Finding Opportunity in Challenge
Our utility business is committed to safely providing affordable, reliable and clean power to its customers. In years past, our utility operating companies have faced multiple challenges to achieving that goal. They have worked diligently to develop solutions to address each challenge.
One of our top priorities has been to secure flexible regulatory mechanisms that allow our utility operating companies the opportunity to earn returns commensurate with investment alternatives of comparable risk. In recent years, we realized significant improvement in achieving authorized returns on equity. In fact, over the last 12 months, we were near the top of our industry in making the most of the opportunities available to us. All Entergy jurisdictions use alternative rate recovery mechanisms including riders and/or Formula Rate Plans to reduce regulatory lag. While efficient and effective, they do not eliminate the need for rate cases. Late last year, we filed a base rate case in Texas, and plan to make rate case filings in Louisiana by January 2013. In other jurisdictions, including Arkansas, the next base rate case is likely to align with the timing related to the System Agreement exits and the proposed move to MISO.
Our utility operating companies also moved to address their ongoing generation capacity needs. Examples of the build, buy or contract actions taken in 2011 include: Entergy Arkansas and Entergy Mississippi each announced plans to purchase a combined-cycle gas-turbine unit; Entergy Louisiana requested regulatory approval to build a 550-megawatt CCGT unit at its existing Ninemile Point plant, including selling a portion of
the output to Entergy Gulf States Louisiana and Entergy
New Orleans; and Entergy Texas entered into a 10-year, 485-megawatt power purchase agreement with Calpine Energy Services, L.P., with 50 percent of the output to be sold to Entergy Gulf States Louisiana. Thanks in part to these types of efforts to develop efficient regulatory constructs and identify opportunities to meet our customers long-term generation needs, the past five-year increase in average residential rates for Entergy utility customers was substantially less than the U.S. average. At the same time, customer service performance improved.
Finally, our utility operating companies continued to work tirelessly in 2011 to find an acceptable solution to address the upcoming exits of two utility operating companies from the System Agreement as well as long-term arrangements for the transmission business. After comprehensive review and analysis, we determined that joining MISO is expected to provide substantial long-term benefits for Entergy utility customers. We identified potential customer savings of up to $1.4 billion in power production and related costs in the 2013 to 2022 time frame. These benefits derive from joining an RTO with substantial scale and a Day Two market. Day Two refers to an RTO that includes day-ahead and real-time energy markets. MISO has a functioning Day Two market today that will generate savings for our customers on day one. The other RTO evaluated  the Southwest Power Pool  does not, even though comparative cost-benefit analysis assumed SPP will get there by December 2013. Formal requests to join MISO have been filed, or are being prepared for filing, with our retail regulators. Decisions are expected by fall 2012.
Joining MISO effectively provides a reliable and cost-effective option for Entergy Arkansas and Entergy Mississippi to exit the System Agreement in December 2013 and November 2015, respectively. It replaces the expiring Independent Coordinator of Transmission arrangement for the system. The target implementation date is by December 2013 for transferring functional control of transmission facilities to MISO.
Given the numerous challenges faced and overcome in recent years, its fair to say our utility business sets a standard for successful adaptation in a difficult climate of change. It has a long record of delivering affordable, reliable power to its customers and is on track to deliver 6 percent to 8 percent compound average annual net income growth over the 2010 to 2014 period (2009 base year), set before the announcement of the spin-merge of the transmission business. Details on how the long-term financial outlook will be affected by the proposed transmission business spin-off and merger will be provided at a future date. It is important to note that, with a 2013 targeted closing date, the transmission business will be part of the utility for a majority of the duration of this financial outlook, and Entergys shareholders will continue to have ownership in
both businesses after the transaction closes

Entergy Wholesale Commodities:
Preserving and Enhancing Value
Our goal at EWC is to preserve and enhance the value that exists in our wholesale generation portfolio. Operationally, EWC set the second highest annual net generation for its nuclear fleet in 2011. EWC also completed its purchase of the Rhode Island State Energy Center, a 583-megawatt CCGT plant located in the ISO New England market. The investment adds standalone economic value and also diversifies EWCs portfolio across fuel type and dispatch merit. The Rhode Island State Energy Center provides a valuable backstop against firm sales from the Pilgrim Nuclear Power Station and Vermont Yankee Nuclear Power Station, providing another tool to manage risk and reduce the unit-contingent discounts we have experienced in past hedging actions.
EWC is keenly focused on price risk management. The steep drop in forward prices since mid-2011 across the entire forward curve illustrates the importance of maintaining vigilance. Near-term forward prices continue to be constrained by excess reserve margins and domestic shale gas production that is outpacing demand. In addition, the mild winter resulted in storage greater than anticipated. Previously, we accelerated our near-term hedging activities consistent with our short-term point of view on natural gas and power prices. Hedging activity, excluding the Palisades Power Plants long-term power purchase agreement, through the end of 2011 resulted in 77 terawatt-hours of planned nuclear generation hedged through 2016 at $800 million above end-of-February-2012 market prices. Long term, our point of view on power prices remains bullish on heat rate expansion associated with ongoing economic growth and implementation of new environmental regulation and/or legislation, and the expectation of a more disciplined approach to drilling activity.
Preserving the value of EWC also involves gaining approvals for continued operation of Pilgrim, Vermont Yankee and Indian Point. The license renewal process has become frustratingly long, extending well beyond the Nuclear Regulatory Commissions stated target of 30 months to review license applications in proceedings with contentions. At more than 72 months, Pilgrims license renewal process has the dubious distinction of being the longest in history. In January, the Atomic Safety and Licensing Board dismissed the last pending late-filed Pilgrim contention and formally terminated its proceedings. Final decision making now rests with the NRC, which may choose to wait to issue the 20-year extended operating license until all appeals are resolved.
After more than 60 months of a thorough and exhaustive safety and environmental review, in March 2011 Vermont Yankee received its license renewal from the NRC. Subsequently, it became clear that Vermont state officials were singularly focused on shutting down the plant by withholding authority from the Vermont Public Service Board to grant the Certificate of Public Good for continued operation after March 21, 2012. While filing a lawsuit against the government is never a simple decision for a corporate board and was not our preferred strategy, the decision to do so was made after a multi-pronged, multi-year effort failed to find common ground with the state. Entergy and our Vermont Yankee nuclear power plant became a political football that was kicked almost daily. It was frustrating to be in a public battle where there were seemingly no rules of conduct. Respect for others and the truth are among our most basic values, and we never violated these during the process. Instead, we filed a lawsuit in the federal District Court for the District of Vermont, seeking to halt the states actions to shut down the plant through legislating authority for itself that is constitutionally reserved for the federal government alone.
In a 102-page order in January 2012, the District Court ruled that certain of the states attempts to force closure of Vermont Yankee are, in fact, unconstitutional and forbade the state from continuing to act in this manner. This decision is good news for our approximately 600 employees at Vermont Yankee, the environment and the community, and also stands for the proposition that the rule of law applies to everyone. Shortly after the order, Entergy filed a motion requesting that the VPSB grant, based on the existing record in its proceeding, Vermont Yankees pending application for a new CPG, and the state of Vermont filed a notice of appeal of the District Courts ruling to the U.S. Court of Appeals for the Second Circuit. In February 2012, the VPSB submitted a list of questions to the parties involved in the proceeding concerning Vermont Yankees application for a CPG. The VPSBs questions relate to, among other things, the effect of the recent decision on certain aspects of the VPSBs authority to issue a CPG and Vermont Yankees authority to store spent fuel from its operations after March 21, 2012. We believe the intent of the District Courts decision was that Vermont Yankee could continue to operate under its renewed NRC license until a final decision is reached on the CPG request. Based on the VPSBs questions, we made a number of filings asking the District Court to provide clarity for all parties regarding certain aspects of the decision and its impact on the continued operation of Vermont Yankee while the VPSB considers our pending application for a CPG. The VPSB is an independent body with commissioners sworn to uphold the law, which we expect to act lawfully and professionally in granting the CPG. Our most recent action is indicative of our resolve to assure our stakeholders rights are protected.
In New York, we are still in the early stages of license renewal for Indian Point. The ASLB is expected to begin initial hearings on the admitted issues by the end of 2012. This stage of the license renewal process could take many years as suggested
by the Pilgrim license renewal proceeding, where nearly
four years elapsed between the initial hearing and final

ASLB action. Pilgrim involved two admitted issues versus 14 issues still to be heard for Indian Point. In the meantime, Indian Point will continue to operate under the timely renewal doctrine, which automatically extends a license past its original term so long as renewal proceedings are pending. On the other hand, at the end of February 2012, the period for submitting contentions on Grand Gulf Nuclear Stations 20-year license renewal application closed without any contentions filed. As a result, the NRCs schedule to make a decision on Grand Gulfs license renewal is tentatively set for September 2013.
Also at Indian Point, at the state level, the administrative
law judges of the New York State Department of Environmental Conservation began hearings in October to resolve issues identified in the water quality certification and water discharge permitting proceedings. Final decisions on these matters could take up to two years and are appealable in New York state court.
During 2011, attention on potential outcomes related to license renewal came to the forefront earlier on in the regulatory process in the aftermath of the nuclear events in Japan following its catastrophic earthquake and tsunami. We believe the record shows Indian Point can clearly operate safely for another 20 years and plays an important part in New Yorks energy supply, as further supported by an independent study by Charles River Associates commissioned by a New York City agency. CRA concluded that the impact of closing Indian Point would raise electricity prices by $10 billion to $12 billion, increase carbon and nitrogen oxide emissions and compromise electric reliability unless generation and/or transmission facilities were added. At the same time, we recognize and appreciate the value of certainty that would come from amicably resolving the situation in New York sooner rather than later. The governor of New York has been outspoken against the plant continuing to operate beyond its current license period. On the other hand, the decisions that affect the plants future are made by independent, expert bodies. Political leaders or elected officials can voice their opinions, but not influence those experts opinions or judgment, even though they may at times appoint the members.
Should an opportunity for achieving certainty arise at Indian Point that is fair to our stakeholders, we will pursue that end with creative ideas and a mind open to the needs and ideas of others. Our ultimate goal is to preserve the value of this vital asset for all stakeholders, including the 1,200 employees and countless other people who would lose their jobs if power prices skyrocketed or reliability deteriorates as predicted if Indian Point is shut down prematurely.
Climate Change:
Proposing a Simple, Direct Approach
Entergy is a long-time, active advocate for policy action to address climate change. In our point of view, climate change poses unacceptable risk to our region, our business, our society and our planet. Even frogs, survivors of any number of cataclysmic events in their 200 million years on the planet, are threatened
by climate change, pollution and human population growth. Some estimate that more than a third of all amphibians  mostly frogs and toads  have been lost, and more are disappearing every day. Unaddressed, climate change could cause up to 50 percent of all species to face extinction. So what can we do?
Weve presented guidelines in the past for a sustainable carbon policy, but in the face of political realities and the urgency of the climate change issue, we now advocate a simpler approach. Our approach includes immediate adaptation efforts in vulnerable areas, elimination of inefficient climate-related subsidies and mandates, a national carbon fee on every ton of CO2 across the economy, and a large innovation effort by government directed toward basic research and funding demonstration projects.
We believe America needs to be a part of a global strategy to address climate change. We are among the 10 percent of nations that produce 90 percent of emissions. We led the way into the climate situation with early industrialization that drove unprecedented economic prosperity. We should lead the way out, using American ingenuity, our sense of duty and the bully pulpit that comes as the worlds moral and economic leader. There is no nation better suited to the task.
On the other hand, action here in the U.S. on climate change has raised some legitimate fears. At the top of the list is the concern that if other large polluters like China or India dont follow the U.S. lead, we have little chance of making a meaningful difference. We advocate leadership but not unilateralism.
Dr. David Victor of the University of California, San Diego, the author of the climate change book, Global Warming Gridlock, published last year to excellent reviews by such prestigious publications as The Economist, has offered new thoughts. Part of the answer lies in helping China and India understand their strong self-interest in cutting emissions of warming gases. Part also rests on looking at the full range of emissions that cause climate change. While most policy has focused on CO2, it is also important to limit short-lived pollutants such as soot (black carbon), methane and ozone in the lower atmosphere (smog). Ton for ton these short-lived pollutants are more potent than CO2. Methane, which is the only directly emitted greenhouse gas of these short-lived climate forcers, accounts for only 15 percent of global greenhouse gas emissions; however, these forcers when taken together are responsible for approximately 50 percent
of the near-term warming influence. Technologies exist to
make deep cuts in these now. But more importantly,

reducing these pollutants would have immediate positive impacts
on air pollution, crops and water supplies  things the Chinese
and Indian governments care about. Soot, for example, already
kills about two million people annually. The point Dr. Victor
makes is countries will only do what they can do. And political
support is essential in every country. Cutting these pollutants
will have a tangible effect on warming, but substantial reductions
in CO2 are also needed. Victors strategy is a way to align the
self-interest of the large emitters in the developing world with
Americas interest in less global warming.
Right now, around the world, the U.S. is short on credibility
and the diplomatic tools to assure the rest of the world will follow
our lead. Dr. Victors work deserves serious consideration in
the climate change debate. The U.S. cannot engage in unilateral
economic disarmament by charging for use of the environment
while others continue to take it for free. But this is not the
unsolvable issue some portray it to be.
Built to Last
When most people think of frogs, the first thing to come to mind
isnt their remarkable evolutionary and adaptive record. For
some, its the boiled frog anecdote and for others its the story of
The Princess and the Frog, first penned by the Grimm Brothers
under a similar title and story line. Most recently it was made into
a highly entertaining movie by Walt Disney Animation Studios,
set in the jazz age in our home city of New Orleans. The story line
is basically the same. The handsome prince is turned into a frog.
But in this story so is the princess. Both are returned to their
former status by a kiss between the frogs. And, of course, they
live happily ever after  a delightful story, but fantasy.
The world, our nation, our industry and our company face
enormous challenges. We must deal with reality, not happy talk
that relies on the improbable to the impossible. There is no magic
wand we can wave and turn a frog into more than it is. Putting two
frogs together in a merger will not create a deity capable of solving
the problems we face. The issues we face will require ingenuity,
commitment, setting priorities, making sacrifices for the greater
good and simply hard work. We cannot afford indecision or
wishful thinking to deter us from the work clearly at hand.
Our point-of-view-driven business model helps us make
the right decisions for our stakeholders and the long-term
success of Entergy. However, implementing the model, making
decisions and taking action require leadership and execution
by talented, experienced people with a winning mentality. We
have an abundance of such people at Entergy. Throughout our
organization, our employees have demonstrated the ability to
find opportunity within each challenge. Our organization has a
proven ability to adapt to changing market conditions. As we
work towards making plans and initiatives to address todays
challenges, new opportunities will open up for our employees.
I have no doubt they are prepared for what lies before us.
At the top ranks, every member of our senior leadership
team is ready and able to lead the organization under the
direction of our experienced board of directors. Through many
changes  both internal and external  our leadership team
has demonstrated resilience and adaptability. In January 2012,
two key members of our leadership team, Group President of
Utility Operations Gary Taylor and Executive Vice President
and General Counsel Bob Sloan, announced their retirements.
We thank Gary and Bob for their significant contributions to
our organization. While they will be missed, we are fortunate
to have exceptional depth of talent at Entergy that is ready and
able to lead our organization forward in its efforts to deliver.
We are an organization where everyone is expected to roll
up their sleeves every day and get their hands or work gloves
dirty. While we strive to stay focused every day on long-term
sustainability and serving our stakeholder needs, we are also
prepared to take the gloves off to remove unnecessary or
unreasonable roadblocks put up by those who oppose our efforts.
Over the last 13 years we have grown operational earnings
per share at a rate 2.5 times the average and total annual
shareholder return at 1.5 times the average of Philadelphia
Utility Index members or top quartile. However, 2012 will be a
difficult year on earnings as commodity prices are at the lowest
point in years, and may not recover in the short term. But, like
the frog, well adapt.
Were encouraged by the progress we made in 2011 in
strategic areas of our business and yet we recognize there
is still much to do. Well make reasoned and, if necessary,
tough decisions  as dictated by market conditions  with
the overarching goal to deliver sustainable value to our
stakeholders over the long term. We are grateful for the
confidence our shareholders have expressed in Entergy and
its leadership team. We are a 24-hour-a-day, 7-day-a-week
business. If there were such a thing as overtime, I assure you,
we are working it to achieve that end.
J. Wayne Leonard
Chairman and Chief Executive Officer