Three years ago, we set out on a journey to become the
benchmark BioPharma company.
We evolved our Mission. We developed our strategy.
We headed in a new direction. In effect, we made several
significant changes, while maintaining our steadfast
commitment to the patients and communities we serve.
Today, our BioPharma transformation is well under way
and near completion. Our portfolio is focused exclusively
on medicines. Our operations have been streamlined and
simplified. And our company now combines the global
experience, established commercial infrastructure and
development capabilities of a major pharmaceutical
company with the agility, biologics expertise and entrepreneurial
spirit of a biotechnology firm.
As a result, Bristol-Myers Squibb is now stronger and
better equipped to deliver meaningful results  a fact
clearly demonstrated over the past year:
 We achieved 4 percent sales growth;
 We had a groundbreaking year with respect
to clinical data;
 We received key regulatory approval of
products across the world;
 We acquired ZymoGenetics, a renowned
cutting-edge biotechnology company; and
 We increased our dividend for the second
consecutive year.
In effect, last years success proves that our BioPharma
transformation is both very real and very promising.
2010: A Year of Impressive Results
Granted, 2010 was a challenging year across the board.
The global financial crisis had reached into every sector
and affected every company. For those in the pharmaceutical
and biotechnology industries, the challenges
were even greater, due to pricing pressures in Europe
and the financial impact of health care reform in the
United States.
Bristol-Myers Squibb, however, not only weathered
the storm; our company actually had a good year. This
is true from both a short-term and long-term perspective.
Our financials were strong  with sales up and
expenses down  and our pipeline of products became
even more robust.
This balance between short-term and long-term is
absolutely essential. To be a successful differentiated
BioPharma company, we must drive results today, while
strengthening our company for the future. It is what we
have done, and it is what we plan to continue to do.
Financial Performance
With respect to financial performance, we continued
to drive shareholder value. In fact, with an 8.8 percent
increase, our total shareholder return (including dividends)
was one of the best in the industry.
MESSAGE FROM THE CHIEF EXECUTIVE OFFICER
Bristol-Myers Squibb 2010 Annual Report 1
TO OUR STOCKHOLDERS
Our BioPharma Progress
Net sales from continuing operations were $19.5 billion 
up 4 percent over the previous year. That includes a
6 percent increase in U.S. sales, with double-digit growth
from Plavix, Sprycel, Sustiva, Baraclude and Orencia as
well as initial sales of Onglyza.
Meanwhile, efficiencies were realized throughout the
organization, including from the 50 percent reduction in
manufacturing plants brought about by the three-year
network rationalization effort completed last year. With
a focus on state-of-art technology, only 12 of the original
27 manufacturing plants remain.
Our strategic focus on capital management led to strong
yields. We ended the year with $10 billion in cash and
marketable securities, while completing our strategic
acquisition of ZymoGenetics, a cutting-edge biotechnology
firm, as part of our ambitious, albeit focused String of
Pearls strategy. We retired $750 million principal amount
of outstanding debt, increased our dividend by 3 percent
and initiated a $3 billion share repurchase program.
Products and Pipeline
In 2010, we obtained several significant regulatory
approvals. Sprycel was approved in the U.S. and Europe
for use as a first-line treatment in newly diagnosed
adults with Philadelphia chromosome-positive chronic
phase chronic myeloid leukemia. Kombiglyze XR was
approved in the U.S. as the first once daily, extended
release, fixed dose combination of a DPP4 inhibitor and
metformin for adults with type 2 diabetes. And Orencia
gained approval in Japan for rheumatoid arthritis and in
Europe for second-line use in rheumatoid arthritis, while
a subcutaneous formulation was filed in the U.S.
With respect to clinical data, 2010 was a very positive
year. We reported important Phase III data for three of
our new molecular entities. Yervoy (ipilimumab) showed
an unprecedented survival benefit in second-line metastatic
melanoma patients; Eliquis (apixaban) showed
a significant decrease in the risk of stroke without an
increase in bleeding for warfarin-unsuitable, atrial fibrillation
patients; and dapagliflozin, a novel first-in-class
SGLT2 inhibitor, continued to show a significant glucose
lowering effect with additional reductions observed in
secondary endpoints of weight loss and blood pressure
lowering in type 2 diabetes patients.
We completed six regulatory submissions for new
medicines in the U.S. and Europe. And with another
year of positive benefit/risk data on Nulojix (belatacept)
2
October 2007
String of Pearls strategy launched with
acquisition of Adnexus Pharmaceuticals.
Ixempra (ixabepilone) approved by the
U.S. Food and Drug Administration
(FDA) for advanced breast cancer.
December 2007
Bristol-Myers Squibb unveiled the
BioPharma strategy supported by three
pillars: innovation, selective integration
and continuous improvement.
January 2008
Sale of Bristol-Myers Squibb Medical
Imaging.
June 2008
Bristol-Myers Squibb acquired Kosan
Biosciences, a cancer therapeutics company.
July 2008
Bristol-Myers Squibb expanded the Productivity
Transformation Initiative, expected to result
in $2.5 billion in annual productivity savings
and cost avoidance by 2012.
Erbitux (cetuximab) approved in Japan.
THE THREE PILLARS OF
OUR BIOPHARMA STRATEGY
Innovation Continuous
Improvement
Selective
Integration
Our BioPharma Progress
for patients with kidney transplants, we completed our
resubmission to the U.S. Food and Drug Administration.
To ensure sustainability of our pipeline, we moved 15
new high-quality compounds into preclinical development
and achieved a record number of proof-of-concept
transitions, with six more compounds now progressing
toward Full Development.
Corporate Responsibility
As in years past, our commitment to the patients and the
communities we serve extended well beyond our core
business markets and into the lives of millions of people
who generally do not have access to quality health care.
In 2010, we continued our work to help reduce health
disparities in various therapeutic areas and in various
places throughout the world. Cancer in Europe. Hepatitis
in Asia. HIV/AIDS in Africa. Mental health and well-being
in the U.S. In January, when Haiti was struck by a catastrophic
earthquake, Bristol-Myers Squibb, our Foundation
and employees lent a helping hand, contributing
nearly $7 million in cash and product. And later in the
year, the Bristol-Myers Squibb Foundation launched an
ambitious program, Together on Diabetes  a new fiveyear,
$100 million initiative to improve health outcomes
of people living with type 2 diabetes in the U.S.
We took our commitment to sustainability to the next
level. We approved our Sustainability 2015 Goals, which
lays out a plan to address social, economic and environmental
challenges in the communities we serve; we also
joined the United Nations Global Compact, the worlds
largest voluntary corporate citizenship initiative.
This is our way forward.
This is our way of
becoming and remaining
the benchmark
BioPharma company.

August 2008
Partnership with PDL BioPharma (since transferred
to Abbott Laboratories) announced
to develop elotuzumab, an investigational
treatment for multiple myeloma.
Sale of ConvaTec completed.
September 2008
Initial public offering for subsidiary
Mead Johnson Nutrition announced.
December 2008
Oncology collaboration with Exelixis
announced.
Agreement with AstraZeneca expanded
to develop and commercialize dapagliflozin
in Japan.
January 2009
Collaboration with ZymoGenetics announced
to develop novel treatment for hepatitis C.
March 2009
Global collaboration with Nissan
Chemical Industries and Teijin Pharma
announced for the development of a
treatment for atrial fibrillation.
April 2009
Commercialization agreement with
Otsuka extended for Abilify (aripiprazole).
Chief Executive Officer Lamberto Andreotti held a number of global town hall meetings
in 2010 to update employees on the companys progress to become the benchmark
BioPharma company. He is pictured here at the companys Shanghai office.
Our BioPharma Progress
2011  The Way Forward
In 2011, we expect the external challenges to persist.
But we also expect to build on the momentum created
last year as we continue to position our company
for long-term success as a focused, differentiated
BioPharma company.
To be sure, it will be a year of transition  the last full
year of exclusivity for Plavix and Avapro, but also a
period of potential significant product launches and
regulatory submissions.
We have several new molecular entities under regulatory
review, including Yervoy for second-line metastatic
melanoma, Nulojix for kidney transplantation, Eliquis
for thrombosis prevention and dapagliflozin for type
2 diabetes. We expect to receive additional important
clinical data for Yervoy (first-line metastatic melanoma)
and Eliquis (stroke prevention in atrial fibrillation),
which may lead to regulatory submissions.
In 2011, we are also anticipating four significant
Phase III transitions in hepatitis C, Alzheimers disease
and oncology.
Taken together  the product launches, the regulatory
actions, the clinical data  all of this should keep us on
track to achieve our 2013 goals and position us well
for sustained growth for 2014 and beyond.
Our BioPharma Future
When I became CEO in 2010, I set out to accomplish
a few key goals during my first year. Bring together a
strong management team. Build on the firm foundation
established by my predecessor, Jim Cornelius. Take our
BioPharma transformation to the next level.
As the record makes clear, we succeeded in every respect.
Bristol-Myers Squibb has an absolutely first-rate senior
management team  one that clearly stands out with
respect to experience, skill, passion and global diversity.
We have navigated through an often challenging
external environment to increase sales, reduce costs,
and grow our promising pipeline. We have continued to
streamline our operations, develop our people, and produce
the innovative science that helps patients prevail
over serious diseases.
This year, we will continue this transformation, and we
will continue working to deliver results, while positioning
Bristol-Myers Squibb for longer-term growth. We will overcome
obstacles. We will seize opportunities. We will strive
to exceed expectations. And guided by our firm commitment
to the highest business standards and ethics, we
will continue to discover, develop and deliver innovative
medicines that help patients prevail over serious diseases.
This is our way forward. This is our way of becoming and
remaining the benchmark BioPharma company.