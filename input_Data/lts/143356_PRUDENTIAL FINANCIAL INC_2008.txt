Dear fellow shareholders:
2008 was a challenging year, to put it mildly. The credit
markets that began their downward spiral in mid-2007
essentially seized up by year-end 2008. The U.S. economy
slid deeper into recession. The equity markets recorded
their worst performance since 1931, with the S&P 500
Index down more than 38 percent in 2008. And the crisis
has spread to financial markets around the globe.
I would characterize the extreme turbulence in the
markets as driven by two factors that have made the
markets themselves the most important fundamental
influence on the economy. The first is the frozen credit
markets, where uncertainty over asset values has extended
into uncertainty over many aspects of our financial system
and the institutions within that system. In this environment,
rates and spreads may not reflect healthy flows and
fundamental valuations, and many important markets
remain distressed. In addition, credit is not available on
normal terms in many segments of the market.
The dysfunctional credit markets triggered the second
factor, which is massive deleveraging. In many cases, asset
sales have been driven by the need to sell whatever can be
soldincluding stocks and commoditieswithout regard
to fundamentals. Prices have often reflected conditions in
other financial markets rather than the traditional
underpinnings of value, which is amplifying uncertainty
and perceived risk. As one market observer pointed out,
Value is as irrelevant at the bottom as it is at the top, and
that certainly seems to be the case in this market.
This negative environment has affected nearly every
company in America, but the financial services industry has
been particularly hard hit. And Prudential Financial has
not been immune.
Like many other insurance companies, our stock price
has come under significant pressure as a result of the
ongoing crisis, as have our earnings. On a generally accepted
accounting principles basis, our Financial Services
Businesses reported a net loss for 2008 of $1.10 billion, or
$2.42 per share of Common Stock.
On an after-tax adjusted operating income basis,* our
Financial Services Businesses earned $1.12 billion in 2008
and posted earnings per share of Common Stock of $2.69.
This reflects a difficult and unprofitable fourth quarter and
compares to a record year in 2007 of $7.21 per share.
While these earnings are disappointing, they are largely
a result of the adverse market conditions and are not
indicative of our long-term prospects and earnings power
in more normal markets. In fact, many elements of our
business strategy and portfolio will serve us well in this
economic crisis and beyond.
 WE ARE A COMPANY THAT MANAGES FOR
THE LONG TERM.
 WE ARE A COMPANY WITH A BALANCED
PORTFOLIO OF BUSINESSES AND RISKS.
 WE ARE A COMPANY WITH BUSINESSES
THAT ARE COMPETITIVE IN THEIR MARKETS.
 WE ARE A COMPANY WITH SOLID CAPITAL
AND AMPLE LIQUIDITY POSITIONS.
 WE ARE A COMPANY WITH A STRONG TRACK
RECORD FOR ACQUISITIONS.
 WE ARE A COMPANY THAT HAS THE BEST
TALENT IN THE INDUSTRY.
 WE ARE A COMPANY THAT HAS KEPT ITS
PROMISES FOR 133 YEARS.

So while we do not, by any means, underestimate the
challenges todays market environment presents, we believe
we are well positioned to weather the economic storm better
than most. And we have a lot to be optimistic about.
We manage for the long term
We have always taken the position that we are in it for the
marathon, not the sprint. In other words, we manage the
company for the long term, while being prepared for shortterm
issues like stock market volatility. Our response to
this challenging environment is no different; we are not
simply riding this out but actively managing through it.
Of course, there is a lot of judgment involved. On the
one hand, these market conditions will not last forever, so
we are managing the short term in a manner that does not
compromise the long term. On the other hand, we have,
where appropriate, reduced the risk profile of certain
products and activities, which will help decrease our own
volatility in the futureeven in the event of another
extreme market environment.
This approach is serving us well in managing through
this challenging environment, and its why we believe it
requires a combination of short-term fortitude and longterm
optimism.
We have a balanced portfolio of businesses and risks
We have a set of businesses that are diverse, healthy and
competitive in their markets. In fact, we believe we have
the best business mix in the industry, and the virtues of this
balance are more evident todayduring down timesthan
during normal market cycles.
Our businesses are diversified geographically. We have
an attractive mix of businesses in both U.S. and international
markets. In fact, our International Insurance and Investment
Division has contributed more than 35 percent of our pretax
adjusted operating income for years now.
Our business portfolio is balanced between growth and
protectionthose businesses oriented toward growing
wealth for our clients and those oriented toward protecting
wealth. Our growing wealth businesses of retirement,
annuities and asset management offer our greatest longterm
growth prospects, while our protecting wealth
businesses of individual and group insurance are stable
contributors to our earnings and cash flow.
Our businesses are diversified by market, as we serve
both individual and institutional clients.
And our businesses are driven by a wide range of often
uncorrelated risksmortality risks, credit risks and equity
risks. In fact, a number of our businesses have little to no
exposure to the U.S. equity markets. In this environment,
our risk diversification is even more important and a
tremendous source of strength.
For example, our international insurance business and
our U.S. group life insurance business are driven mainly
by mortality risks and by investment margins that are
unrelated to whats happening in the U.S. equity markets.
And the benefits of that are evident in their earnings.
Our international insurance business had a record
year, recording pre-tax adjusted operating income of
$1.75 billion, compared to $1.60 billion in 2007. The results
were driven in large part by strong performances in
Prudential of Japan and Gibraltar Life.
Our group life insurance business also delivered record
earnings in 2008. Pre-tax adjusted operating income for this
business was $340 million, a $54 million increase over 2007.

As you can see, these businesses are an important part
of our diversification picture and represent a strong shock
absorber as we go through this unusually volatile time.
Of course, the businesses most sensitive to the equity
marketsspecifically annuities and asset management
were adversely affected by the wild fluctuations in the
financial markets. Due primarily to these market declines,
account values in our variable annuity products were
down, and pre-tax adjusted operating income for our asset
management business was $232 million in 2008, down
$469 million from 2007 results. But it is important to note
that these businesses serve us well in positive market cycles
and help drive our earnings growth, further illustrating the
importance of a diversified earnings picture.
Our businesses are competitive in their markets
I have often said that I would rather have strong businesses
in difficult markets than weak businesses in robust
markets. And we do indeed have strong businesses.
While the environment in which we are operating is
volatile and constantly changing, the fundamentals of our
businesses are not. We continue to enjoy positive net flows
in our U.S. annuity, retirement and asset management
businesses. Net sales of our variable annuity products have
held up very well in relation to the overall market. In 2008,
those sales totaled $2.2 billion, slightly higher than in 2007.
In our full-service retirement business, we have seen an
upward trend in net additions reflecting strong customer
deposits and sales. In fact, we have had positive net flows
in this business for the last five consecutive quarters, which
amounted to $3.9 billion in 2008 alone. We are also proud
of the fact that we continue to earn a plan persistency rate
of 95 percent in our full-service retirement business. This
is not only among the best in the industry, we believe it is
also the best indicator of the satisfaction clients have in the
products and services we provide.
In our asset management business, we believe thirdparty
institutional flows are the strongest indicator of our
competitiveness. In this industry, net flows between
3 percent and 5 percent are hard to sustain over the long
haul. However, we have generated positive net flows with
institutional clients in the mid- to high single digits for
several years now, pointing to the confidence investors
have in the strong skill sets we have built across a wide
range of investment disciplines.
In 2008, we also saw positive sales growth in our U.S.
group insurance business, as well as our international
insurance business. Our individual life insurance business
also recorded solid sales, and our term life insurance
portfolio, in particular, has been very successful in the
marketplace. Prudential ranked second in the industry in
term life sales for 2008.
At the end of 2007, we introduced a new term life
insurance policy called MyTerm, which offers customers of
select banks and other financial institutions term life insurance
over the Internet in just 10 minutes. In 2008, eight regional
banks were actively offering the product to their customers.
We continued to expand our distribution channels and
deepen our relationships with third-party distributors
across our businesses as well. For the fourth year in a row,
we were the No. 1 seller of variable annuities through the
independent broker-dealer channel. Our success in
increasing our share of third-party sales is resulting in more
cost-effective distribution.
We have solid capital and ample liquidity positions
Of course, in todays environment key elements of financial
flexibility and strengthnamely, investment portfolio,
capital and liquidityare of heightened interest. Id like to
address where we stand in these areas.
Lets start with our investment portfolio. While the
current results and values in our investment portfolio
reflect the adverse conditions in the financial markets, we
are comfortable with our overall portfolio and the risks we
are taking.
Our company has long had a strong credit culture, and
risk management is a core competency for us. We understand
credit risk, we take that risk judiciously and we manage it
effectively. Our broad-based capabilities across asset classes
allow us to achieve a high degree of diversification in our
investment portfolio. In fact, we are a market leader in many
investment disciplines, including private fixed income,
commercial mortgages and equity real estate.
We have had a cautious outlook with regard to the U.S.
credit markets for more than two years now. As a result, our
portfolio is well balanced and conservatively positioned. Our
holdings of public and private debt are significantly
underweighted in financial institutions. Our commercial
mortgage portfolio is well diversified by property type and
by location. Ninety-four percent of our commercial
mortgage-backed securities portfolio is rated AAA. Finally,
subprime asset-backed securities are a manageable exposure
for us, at $5.4 billion on an amortized cost basis within the
Financial Services Businesses. Eighty percent of these
investments are at investment grade. To put this in
perspective, the $5.4 billion of subprime asset-backed
securities represents less than 3 percent of the total Financial
Services Businesses investment portfolio.
So while we will likely suffer additional losses before
the markets rebound, we are confident that our portfolio
will perform relatively well.
Turning to capital, we believe we have the resources
available to maintain capital consistent with our AA
insurance claims-paying ratings objectives. As for liquidity,
we currently have ample sources on our balance sheet,
including $4.4 billion of cash and short-term investments at
the parent company level. We have ample liquidity to meet
all of our obligations.
But we are not standing still. In 2008, we took steps to
further strengthen our capital and liquidity positions. For
example, in December we announced our intention to
exercise our lookback option and sell our minority stake
in Wachovia Securities to Wells Fargo. The amount we
expect to receive far exceeds the value reflected on our
balance sheet, and we expect to realize this value on or
before January 2010. The sale of our stake in Wachovia
Securities will provide us with additional capital to sustain
possible market disruptions in the future.
We are well positioned to grow
We have deployed capital in the markets we think are
attractive and we have high-quality business models in
place in those markets to capture growth opportunities.
In particular, we have solid growth engines in our
international insurance business and U.S. retirement and
annuities businesses. Each of these businesses has not only
taken advantage of growth opportunities in 2008, but also
laid a solid foundation for long-term growth.
On the international front, we continue to concentrate
on a limited number of attractive countries and on deepening
our presence in the markets in which we currently operate.
And we executed on that strategy in 2008 by introducing
new products and services, as well as by growing organically.
In the United States, baby boomers continue to express a
growing need for products that provide guaranteed income
for what will be a longer retirement period. In addition, these
turbulent financial markets have encouraged clients and their
financial advisors to sharpen their focus on retirement income
security. Most individuals have realized the hard way that
they are in no position to manage the risks they are seeing
today on their own. With our innovative products and strong
reputation as a leader in the retirement market, we are
uniquely positioned to serve this market need.
In January 2008, we introduced Highest Daily Lifetime
Seven, a variable annuity optional living benefit that offers a
protected withdrawal value based on 7 percent annual
compounded growth on the highest daily account value. We
continue to be the only company that offers optional benefits
that can guarantee an annuitys highest daily value. This
option has proved very popular in the marketplace, especially
given customers heightened interest in downside protection.
In 2008, 65 percent of Prudential variable annuity purchases
included one of our highest daily options.
In October, we further added to our retirement
capabilities by acquiring MullinTBG, the largest
independent provider of nonqualified executive benefits.
We can now offer the industrys premier nonqualified
administration and funding solutions, complementing our
robust capability in the qualified market.
Changes in demographics and public policy in
international markets are creating opportunities for retirement
income products similar to those we have had in the United
States for some time now. Given our success in the variable
annuities, defined contribution and defined benefit markets in
the United States, coupled with our risk management skills
and our success in captive and third-party distribution, we
believe we are well positioned to also compete in the global
retirement market. And we plan to further expand our
retirement presence internationally in 2009.
Our solid acquisition and integration track record is
another source of strength for us as we pursue growth
opportunities. Of course, financing such opportunities is
more challenging in the current economic environment, but
our proven skills in this area will certainly help us as we
continue to pursue opportunistic acquisitions that add
value for our company and our shareholders.
Our employees are using their talent to strengthen
our company and communities
In challenging times like these, it is not just the quality of
our company and the way we do business that sets us apart.
The strength of our leadership team and the dedication of
our more than 40,000 employees around the world are also
key competitive advantages for us. That is why we remain
so focused on attracting and developing the best talent in
the industry. And our longstanding commitment to
fostering a work environment that values and demands
respect, teamwork, inclusivity and opportunity for every
employee is what keeps that talent here.
In 2008, we earned accolades in the marketplace for
continuing to be a great place to work. For the second year
in a row, BusinessWeek named Prudential among the best
places to launch a career; Working Mother magazine ranked
us among its Top 100 Companies for the 19th consecutive
year; and the Human Rights Campaign gave us a perfect
score on its Corporate Equality Index for the sixth year
running. We also garnered spots on both Hispanic Business
magazines Diversity Elite 60 list and Latina Style magazines
list of the 50 best companies for Latinas.
In addition, we once again earned esteemed recognition
from Fortune magazine. For the second consecutive year,
the magazine ranked Prudential as the most admired
insurance company in the world, giving us the highest
marks in the categories of innovation and global
competitiveness.

Our employees talent, hard work and commitment
arent just strengthening our company, but improving
communities around the world where we live and work as
well. On Prudentials 14th Global Volunteer Day held in
October 2008, more than 30,000 employees and their
family members, friends and clients worked on 800 projects
in 12 countries. Since the programs inception, Prudential
volunteers have contributed 1.7 million hours of personal
time on Global Volunteer Day, which, according to the
Points of Light Foundation, have a cumulative value of
more than $30 million.
The Prudential Foundation is strengthening communities
where we do business by enhancing economic and education
opportunities, as well as arts and civic infrastructure. In
2008, the Foundation directed approximately $23 million in
grants to organizations focused specifically on these areas of
need, and Prudential contributed an additional $20 million to
nonprofit organizations to improve the quality of life in
communities around the world.
Our Social Investment Program is also making an
impact on these pressing issues. In 2008, we provided more
than $60 million in investments to support affordable
housing, education and economic development, and
throughout the programs 32-year history we have invested
more than $1.2 billion in cumulative financing in these
critical areas.
With the economy in turmoil, we know many
community groups and the people they serve are feeling
more strained than ever. So The Prudential Foundation
formed the 2008 Fund for Families to further help
communities in need across the country. The initiative
dispersed $1 million to soup kitchens, pantries, shelters and
emergency service programs in 12 cities at a time when
they needed it mostthe holidays.
We have kept our promises for 133 years
Our company was started in 1875 on a promiseto offer
affordable life insurance to the working class. We have
demonstrated our commitment to helping people achieve
financial security and peace of mind ever since. For
example, in the 1930s, during the Great Depression, we
went to great lengths to help farmers keep their land. In
fact, it was company policy to abstain from foreclosing
on farm property if there were any possibility that the
farmer could ultimately get back on his feet. We
understand what it means to keep the promises and
commitments we make.
In times like these, it is more important than ever that
we continue to meet our obligations and keep the trust of
our shareholders, our clients and our employees. We
believe the company we have built and the steps we have
taken since we went public seven years ago will help us
continue to do just that. 
Our strong balance sheet, financial flexibility and
balanced mix of businesses and risks position us well to
weather the economic storm better than most. But we
aspire to do more than that. By remaining focused on our
strategy, risk management, business fundamentals and
meeting the needs of our clients, we expect to emerge from
the current downturn strongerand to keep our promises
as we have done for the last 133 years.
I would like to thank our employees for their
dedication and commitment to strengthening our
company. I would like to thank our clients for their
continued confidence in Prudential. And I would like to
thank our shareholders for their ongoing support. I look
forward to leading this company through these most
challenging of times and demonstrating why our longterm
optimism is justified.

JOHN STRANGFELD
Chairman of the Board,
Chief Executive Officer
and President


