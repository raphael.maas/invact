from the
Chairman
and
CEO


FMC Technologies had a very good year in 2004. Sales increased 20 percent and earnings
per share were up 63 percent over 2003. As with most years, not everything we did was
successful, but we had many more successes than disappointments. The successes were in
areas that we can continue to build on, and, hopefully, our disappointments are unusual
occurrences that we can avoid in the future.



    The successes were:                                                                    The disappointments were:


                                                                                                Our performance on the
   Our energy businesses, including                    $36 million while maintaining a
                                                                                                 Sonatrach contract within Energy
    both Energy Production Systems                      significant ownership stake in
                                                                                                 Production Systems, which lost
    and Energy Processing Systems,                      one of the leading Floating
                                                                                                 $13 million after tax primarily due
    took advantage of growth in oil-                    Production Storage and Offshore
                                                                                                 to higher costs associated with
    field activity levels by increasing                 (FPSO) system manufacturers in
                                                                                                 weather-related delays.
    sales 26 percent over 2003.                         the world.
                                                                                                A series of Florida hurricanes
   Our subsea systems business,                       Our focus on working capital
                                                                                                 destroyed a third of the Florida
    within Energy Production                            management allowed us to limit
                                                                                                 citrus crop, resulting in the small-
    Systems, again led the way by                       our growth in working capital
                                                                                                 est orange crop in 12 years and
    continuing its increase in market                   during the year. We reduced our
                                                                                                 the smallest grapefruit crop in 66
    share to report sales of over                       net debt by over $150 million in
                                                                                                 years. This negatively impacted
    $1 billion and orders received                      2004.
                                                                                                 our FoodTech business in the
    during the year of $1.5 billion.
                                                                                                 second half of 2004 and is likely
                                                       We were successful in resolving
                                                                                                 to negatively affect FoodTech in
   Our Airport Systems business                        a series of tax disputes and
                                                                                                 the first half of 2005.
    performed well in a difficult airline               tax audits during 2004 that
    industry environment, growing                       provided us with $12 million in
                                                                                                A lack of orders for our blending
    sales by 25 percent.                                lower domestic and foreign
                                                                                                 and transfer business within
                                                        income taxes.                            Energy Processing Systems,
   We exchanged our ownership in
                                                                                                 which created an operating loss
    MODEC International LLC for
                                                                                                 and caused us to impair
    cash and stock in MODEC, Inc.,
                                                                                                 $6.5 million of goodwill
    realizing an after-tax gain of
                                                                                                 associated with the business.




Even including these disappointments,                                              In our Energy Production Systems
                                           Business performance
                                                                                   business, we increased sales in all
earnings per share were up 63 per-         From our company's formation in
                                                                                   businesses as total sales rose 31 percent
cent and we saw in 2004 a 26 percent       2001 through 2004, our growth in
                                                                                   over 2003. We delivered subsea sys-
increase in order backlog. As I said       earnings has been driven by our
                                                                                   tems for major oil companies such as
before, 2004 was a very good year.         energy businesses, especially our
                                                                                   BP, Shell and Kerr-McGee in the Gulf
                                           subsea business, which has capital-
Our stock responded well to our
                                                                                   of Mexico, ExxonMobil offshore West
                                           ized on the secular growth in deep-
performance and appreciated over
                                                                                   Africa, Petrobras offshore Brazil, and
                                           water development. Oilfield activity
38 percent in 2004. This compares
                                                                                   Statoil in the North Sea. During 2004,
                                           levels improved in 2004 and had a
favorably with the Oil Service Index
                                                                                   we received orders totaling $1.8 billion,
                                           positive effect on most of our Energy
(OSX), which was up 32 percent, and
                                                                                   which included subsea orders for BP's
                                           Systems businesses. Our Energy
the S&P 500 Index, which increased                                                 Greater Plutonio project, Total's Rosa
                                           Systems business revenue reached
9 percent in 2004. Since our IPO in
                                           almost $2 billion in 2004.
2001, our stock price has improved
61 percent compared to an increase
of 2 percent in the OSX and a decline
of 3 percent in the S&P 500 Index
during the same period.


                                  
         project and Woodside Energy's                Our FoodTech business experienced a
    Chinguetti project, all offshore West        challenging year in 2004 as four hurri-
         Africa, as well as Statoil's Tampen          canes impacted the Florida citrus crop
    Area project in the North Sea. Higher        and our revenue and profits from cit-
                 costs and extreme weather effects on         rus processing. Consolidation among

                                   the Sonatrach contract limited earn-         the major juice producers increased

                                   ings growth in Energy Production             competitive pressures on citrus prof-

                                   Systems to 8 percent in 2004.                itability. However, our food processing

                                                                                systems business experienced a
     In our Energy Processing Systems
                                                                                recovery and improvement in its North
     business, we saw strong demand
                                                                                American and Asian markets, particu-
        from oilfield service companies for our
                                                                                larly in freezing and cooking systems.

                                   WECO/Chiksan equipment. We
                                                FoodTech sales were flat compared
                                   also experienced strong demand in
                                                                                with 2003, and earnings declined
                                   measurement systems and material
                                                                                16 percent in 2004.
                                   handling systems. Conversely, our
                                   blending and transfer business was           Airport Systems performed well in
                                   adversely impacted by order post-            2004 despite a difficult airline industry
                                   ponements. Sales grew 14 percent             environment. Our ground support
                                   over the prior year, and earnings,           equipment business benefited from
                                   which included a $6.5 million goodwill       the strengthening businesses of inter-
                                   impairment charge for blending and           national airlines and ground
                                   transfer, declined by $2.9 million in        handling companies. In addition, we
                                   2004.                                        benefited from incremental volume
                                                                                improvements in our Jetway passen-
                                                                                ger boarding bridge and ground sup-
                                                                                port equipment businesses. We deliv-
                                                                                ered 70 Halvorsen cargo loaders in
                                                                                2004 and received orders from the
                                                                                U.S. Air Force for product enhance-
                                                                                ment work and 40 loaders to be deliv-
                                                                                ered in 2005. Revenues improved 25
                                                                                percent and operating profit improved
                                                                                29 percent over 2003.



                                           strength
                                           The growth in adjusted income and          In 2004, we continued to invest in the
Financial performance
                                           our disciplined capital management         advancement of new technologies
Our diluted earnings per share were
                                           provided a 12.3 percent after-tax          upon which future growth will be
$1.68 in 2004. The reported earnings
                                           return on investment, which is near        based. Our technology development is
for all periods presented include the
                                           the top of our industry and higher than    driven by the needs of our customers
expensing of employee stock options.
                                           most industrial companies. We believe      and our commitment to provide them
The 2004 results were impacted by a
                                           that earning high returns on the capital   with the most effective solutions for
number of unusual items. The
                                           that shareholders entrust to us is one     their challenges.
exchange of our equity interest in
                                           of our most important jobs and one
MODEC International generated a gain
                                                                                      Our high-pressure/high-temperature
                                           that will create shareholder value.
of $0.52 per share. We recorded a
                                                                                      subsea system, capable of operating
non-cash loss of $0.09 per share for       Investments in technology
                                                                                      under pressures of 15,000 psi and
the impairment of goodwill in our
                                           Throughout our short history as a          temperatures up to 350 F, was
blending and transfer product line.
                                           separate public company, FMC               installed in 2004. Employment of this
                                           Technologies has grown through con-
After excluding these unusual items,                                                  system on the Na Kika project in the
                                           tinuous improvement in its technology
the remaining adjusted income per                                                     Gulf of Mexico helped Shell and BP
                                           and teamwork with customers, which
share, a non-GAAP measure (reconcil-                                                  win a distinguished achievement
                                           has translated into strong financial
iation on page 100), of $1.25 per                                                     award at the 2004 Offshore Tech-
                                           performance. In all our businesses,
share is comparable to historical earn-                                               nology Conference. We have provided
                                           technology has reinforced our strong
ings. It is a 21 percent improvement                                                  subsea systems for seven of the last
                                           relationships with our customers and
over 2003 and represents an 18 per-                                                   eight projects that have received this
cent annual growth rate since our for-     led to the formation of enduring cus-      prestigious award. At the 2004 confer-
mation in 2001.                            tomer alliances. This combination of       ence, developments in our riserless
                                           technology and teamwork is our most        light well intervention technology
Additionally, in 2004 we had unusually
                                           distinctive competitive advantage, as      received a Spotlight on New
low income tax expense, primarily due
                                           evidenced by our leading market            Technology award.
to favorable resolutions of a tax dis-
                                           positions in many of the industries in
pute and foreign tax audits completed
                                           which we compete.
during the year, which nearly offset the
loss on the Sonatrach project.

Our focus on working capital manage-
ment limited the amount of capital
required to support our growing busi-
nesses. Working capital, excluding
cash and debt, increased only
11 percent during 2004. Working cap-
ital discipline, in combination with low
capital expenditures and the proceeds
from the MODEC International conver-
sion, allowed the Company to gener-
ate free cash flow sufficient to reduce
net debt by $154 million. This brought
our net debt to $39 million at year-end
2004. We also continue to benefit
from low-cost debt, having locked in a
2.9 percent interest rate through mid-
2008 for up to $150 million in debt.

We also continued development of an      Our joint venture company, GTL                             poultry. Additionally, we developed
all-electric subsea production system.   MicroSystems, is moving forward with                       technologies to enable the sterilization
A total of 16 production systems with    commercial development of gas-to-liq-                      of food packaged in paper containers.
electric choke valves operated prob-     uids (GTL) technology. When it is com-
                                                                                                    Within our airport equipment busi-
lem-free in the North Sea throughout     mercially developed, our objective is to
                                                                                                    nesses, we focused on development
2004. The all-electric subsea system     enable small volumes of stranded gas
                                                                                                    of our new RampSnake product,
will use simpler controls than conven-   reserves to be converted into liquid
                                                                                                    which is an articulated belt loader for
tional systems, which rely on            form with a small plant that can be
                                                                                                    narrow-body aircraft. It is designed to
hydraulics.                              located on offshore platforms.
                                                                                                    promote safer working conditions for
We advanced our subsea separation        We have made a number of technolo-                         baggage handlers and better produc-
and processing technologies and were     gy advancements in our measurement                         tivity for airlines.
engaged to perform studies for           business, particularly with multiphase
                                                                                                    All of these technology advances are
Petrobras and Statoil designed to        metering. Multiphase metering is set-
                                                                                                    being achieved by working with our
demonstrate our separation technolo-     ting new standards for operational effi-
                                                                                                    customers to understand their chal-
gy for subsea applications. We have      ciency, in both surface and subsea
                                                                                                    lenges and develop systems and
been conducting independent              environments.
                                                                                                    products to meet those challenges.
research and development activities in
                                         In our FoodTech business, we contin-
subsea processing technology and
                                         ue to improve the efficiency of our cit-
acquired controlling interest in CDS
                                         rus extractors, increasing yield and
Engineering for its state-of-the-art
                                         value for our customers. In 2004, we
separation technology in 2003. We
                                         introduced a new, more accurate por-
believe that subsea processing repre-
                                                                                                            Our safety
                                         tioner for our chicken processing cus-
sents a long-term growth opportunity
                                         tomers that uses 3-D optical technolo-
                                                                                                      record is among
for us.
                                         gy and high-pressure waterjets to cut

                                                                                                        the best in the
                                                                                                              industry.
FMC Technologies' food pro-
cessing solutions include the
M-fryer, the first of a new gen-
eration of immersion fryers
engineered to meet the needs
of global convenience food
processors. The series of fry-              
ers is designed to produce uni-                               
form quality, shorter prepara-
tion times and enhanced food
safety.                                                                                                   
 people
                                                                                                           
                                           By partnering                                                   Enfield projects;
Customer alliances
                                           with customers,                                                 CNR's Baobab proj-
A cornerstone of our strategy has
                                           we are able to                                                 ect; Total's Rosa proj-
been and continues to be developing
                                           develop innovative                                          ect; Kerr-McGee's Red
close working relationships with our
                                           solutions to the most                                     Hawk and Gunnison proj-
customers in all of our businesses. In
                                           significant problems they                            ects; Statoil's Norne and
our Energy Production Systems busi-
                                           face. Our subsea engineers work              Tampen Area projects; and Norsk
ness, these relationships have taken
                                           alongside our customers' engineers.          Hydro's Ormen Lange project repre-
the form of alliances, frame agree-
                                           By deeply immersing ourselves in our         sent some of the current projects
ments and other types of partnerships
                                           customers' businesses, we are able to        where we are solving our customer's
on a regional or global basis. We
                                           provide them the reliability, practicality   technical challenges on a day-to-day
believe that we have more of these
                                           and economy they value. BP's                 basis. At the same time, our alliances
types of arrangements with key cus-
                                           Thunder Horse, Atlantis and Greater          and frame agreements enable us to
tomers  such as BP, Shell, Kerr-
                                           Plutonio projects; Shell's Na Kika,          invest in technologies that our cus-
McGee, Statoil, Woodside Energy and
                                           Coulomb and Llano projects;                  tomers need. By working collabora-
Norsk Hydro  than any other compa-
                                           Woodside Energy's Chinguetti and             tively with our customers, we are able
ny in our market sector. We have also
                                                                                        to assist them in getting the most out
formed collaborative alliances with oil-
                                                                                        of their oilfields while we strengthen
field service companies in our Energy
                                                                                        our own market positions and grow
Processing Systems business, air
                                                                                        our businesses.
cargo companies in our Airport
Systems business and citrus proces-
sors in our FoodTech business.



                                               results
                                                                                      Based on our solid backlog position
People and principles                      Outlook for 2005
                                                                                      and business activity level entering the
Our success is ultimately determined       We look forward to a year of solid per-
                                                                                      year, combined with the dedication of
by our people. They create technology      formance in 2005. Our energy busi-
                                                                                      a talented team of employees, we
that meets customer needs and make         nesses, driven by the secular growth
                                                                                      believe that 2005 will be another good
our alliances work. We believe that        of subsea and the continuing high oil-
                                                                                      year for FMC Technologies.
nothing is more important than provid-     field activity levels, should have a
ing them with a safe place to work.        strong year. The Airport Systems busi-
Our safety record is among the best in     ness should see some growth as our
                                                                                      Sincerely,
                                           new RampSnake product debuts
our industries. We also believe that
our commitment to ethics is essential      and equipment replacements increase.
to maintaining the integrity of our com-   FoodTech's performance will be ham-
pany with employees, customers,            pered by the impact of the 2004
investors and suppliers. This commit-      Florida hurricanes on the citrus crop.     Joseph H. Netherland
ment guides our business dealings,                                                    Chairman, President and Chief
                                           One of our challenges for the future is
corporate governance practices and                                                    Executive Officer
                                           to find a way to utilize our excess cash
compliance with regulations such as                                                   February 21, 2005
                                           to increase shareholder value. We
the Sarbanes-Oxley legislation.
                                           have recently announced a stock
We are fortunate to have the services      repurchase program of up to two mil-
of a capable and experienced man-          lion shares. Additionally, we continue
agement team and Board of Directors.       to look for acquisitions that comple-
In 2004, our management depth was          ment and build on our current busi-
further strengthened with four key         nesses. We want businesses with
executive appointments  Peter             technology that provides unique
Kinnear to Executive Vice President,       advantages and that are available at a
Charlie Cannon to Senior Vice              reasonable cost. If we are unable to
President and John Gremp and Tore          find these kinds of acquisition oppor-
Halvorsen to Vice President. All four      tunities, we plan to return the capital
are proven leaders, with distinguished     to shareholders through further stock
company and industry backgrounds.          repurchases or dividends.
We said farewell to Dolph Bridgewater,
who retired from our Board in 2004.
Although his contribution will be
missed, we will continue to benefit
from the efforts of our other excep-
tionally qualified Board members.




Our success is ultimately determined
by our people, such as this techni-
cian at our facilities in Kongsberg,
Norway. They create technology that
meets customer needs and make our
alliances work.
