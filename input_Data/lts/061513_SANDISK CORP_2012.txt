Dear SanDisk Stockholders,
2012 marked the 25th anniversary of the invention of NAND flash memory technology. Looking back, it is
amazing to see how this technology has transformed, and in many cases disrupted, industries while enabling the
creation of new markets and devices to enhance our lifestyles. Today, flash memory technology is a key driver of
a pervasive smart digital lifestyle for both consumers and businesses. The confluence of key secular trends of
mobility, expanding digital content and increasing connectivity is driving a greater need for high-performance,
cost-effective flash-based storage solutions for use at the desktop and in consumer applications. The enterprise
data center is also being transformed by growing deployment of flash-based solutions in both servers and storage
systems.
SanDisks expanding portfolio of solutions is widely used in many of the products that are at the center of these
key secular trends. SanDisk is dedicated to providing robust, reliable and affordable storage solutions for
consumer and enterprise applications in a wide variety of formats and devices. Our products enable users to
efficiently and effectively capture, retrieve, share and preserve digital content. We have great expertise and
passion in delivering these solutions and our 4,500+ patents granted worldwide are vital to both current NAND
flash and future memory technology storage solutions.
Recap of 2012
In 2012, the mobile market remained the largest driver of our business accounting for 46% of our revenue. Our
mobile products are used in smartphones, feature phones, tablets, eReaders and other portable devices. Our
consumer electronics offerings including cards, USB flash drives and audio/video devices drove 31% of our
revenue. Client and enterprise solid-state drive (SSD) product sales almost tripled from 2011 to account for 9%
of our 2012 revenue. Our client SSD products are used in thin-and-light computers, notebook and desktop
personal computers, while our enterprise SSD products are used in data center storage and server infrastructure.
Licenses of our intellectual property contributed to 7% of our revenue and a similarly sized contribution came
from wafers, components and accessories.
In the first half of 2012, many of our mobile OEM customers began reducing the rate of card bundling with the
sales of their mobile phones and tablets. As the market share leader in mobile cards, we were impacted more than
our competitors and we saw lower sales of our mobile cards through OEMs. The mobile OEM de-bundling trend
contributed to an industry-wide flash memory supply and demand imbalance, causing aggressive price declines
during the first half of the year. Compounding the weak pricing and reduced card bundling, our first half revenue
opportunities were constrained as we re-aligned our embedded product portfolio to meet market requirements.
This combination of card de-bundling, market imbalance and embedded product transitions caused our results in
the first half of 2012 to be significantly weaker than our initial expectations.
Despite these challenges, thanks to the adaptability and agility of our SanDisk team, we recovered very quickly
in the second half of 2012 with sequential growth in revenue, profitability, and cash flow and expanded market
share. Specifically, we began supplying our customers with a broader range of embedded products for mobile
devices, thus driving embedded revenue to a quarterly record of $500 million in the fourth quarter of 2012.
Additionally, we achieved strong growth in the computing market with combined revenue from our enterprise
and client SSDs almost doubling year-over-year in the fourth quarter of 2012. We also maintained, or increased
our retail market share in both developed and emerging geographies.

From a manufacturing perspective, our solid execution in the 19-nanometer technology transition helped lower
product costs at a healthy rate, and 19-nanometer technology accounted for the majority of our production output
in 2012.
Despite 2012 being a year of mixed business trends, we delivered revenue of $5.05 billion, with GAAP earnings
of $1.70 per share. Our business generated $530 million in operating cash flow and our cash balance increased by
approximately $100 million over the course of 2012, after investing in property, plant & equipment and
acquisitions, as well as spending approximately $230 million on share repurchases.
Expanding Horizons
Smartphones and tablets are becoming increasingly powerful. As an increasing number of these devices use
multi-core processors, these products are beginning to perform many of the functions of a traditional personal
computer. Additionally, the convergence of smartphones and tablets is giving rise to newer categories of mobile
devices, such as the Samsung Galaxy Note, LG Optimus and Sony Experia Z, that are even more capable than
traditional smartphones. These new products require robust and high-performance storage solutions with
requirements that play to the strengths and key capabilities of SanDisk NAND-flash memory offerings.
Specifically, providing impressive power and performance characteristics in various form factors requires deep
system design and integration capabilities, and these are part of SanDisks core competencies. I am pleased to
note that all key smartphone and tablet manufacturers use SanDisk products.
The computing and storage markets are experiencing unprecedented changes. The traditional mechanical hard
disk drive (HDD) in a personal computer is either being displaced by a stand-alone SSD or being complemented
with a flash-based solution. With the addition of flash-based technology in these devices, consumers are now
able to experience a near instantaneous wake-up of their devices the moment the power is turned on. Because
of flash memory, computer users are also enjoying faster application responsiveness, longer battery life, less
weight and greater durability. Our client SSDs are increasingly being designed into many notebook computers to
enable these features. I am proud to say that our client SSD business has achieved solid traction and ten leading
PC OEMs are leveraging our client SSD solutions to bring innovative products to market.
In the enterprise SSD market, cloud-based offerings as well as enterprise data centers are presenting tremendous
opportunities for the use of SSDs. Users of cloud storage and applications expect instantaneous access to data via
high-speed connections, and enterprise SSDs enable reliable storage and quick delivery of this information. We
have had very good success with our second-generation SAS drives as more storage system configurations are
being implemented with this technology along side or in-lieu of existing hard disk drives. We believe we are one
of the top 2 suppliers of SAS SSDs to storage OEMs. Our SAS drives incorporate a proprietary multi-core
controller chip and a sophisticated firmware stack, the combination providing highly differentiated performance
characteristics that benefit our enterprise customers. For the PCIe market, we are supplying our first-generation
SSD, focused on small and medium business applications, and we expect to launch our next generation, higherperformance
PCIe SSD later in 2013. We are now supplying our enterprise SSDs to four of the seven tier one
enterprise storage OEMs.
In 2012, we made two software related acquisitions, FlashSoft and Schooner, to further enhance our enterprise
SSD capabilities. The FlashSoft team designs and develops high-performance data caching solutions that
significantly boost the input-output speed of data traversing through the SSDs. The Schooner teams offerings are
designed to accelerate the performance of enterprise application software such as databases. Our SSD software
solutions are designed to improve infrastructure performance and availability and also lower overall data center
cost. We expect that our investment in software will enhance our SSD solutions in the years ahead.
Our position in retail remains solid with global market share leadership, supported by a well-segmented product
line made available across more than 250,000 storefronts worldwide. SanDisk products, known for their quality
and reliability, continue to command a price premium over competition in retail. SanDisks retail presence is a
key differentiating aspect of our company.

Continued Leadership in Flash Memory Technology
We continue to make progress on our three-pronged strategy for memory technology. First, we are scaling the
current 2-dimensional NAND flash technology as far as manufacturing tool capabilities will permit. Our current
19-nanometer technology node will remain the main manufacturing technology for much of 2013. Later in 2013,
we plan to begin the transition to our 1Y-nanometer technology node and we are working on development of a
1Z-nanometer node. Second, we are making good progress in developing Bit Cost Scalable (BiCS) NAND
technology, which is a vertical, 3-Dimensional NAND technology architecture. This technology, we believe, can
leverage our existing manufacturing infrastructure and provide a mid-term solution until our third prong becomes
commercially feasible. The third prong is our 3-Dimensional resistive RAM (ReRAM) technology, which we
believe could be the ultimate successor to both 2-Dimensional NAND and BiCS NAND technologies. We
believe that our three-pronged technology strategy positions us well for continued leadership in solid-state
technology.
Vertical Integration: A Key SanDisk Advantage
Our vertically integrated business model, which includes high-volume NAND-flash manufacturing facilities in
ventures with Toshiba Corporation, provides us leading-edge captive wafer capacity. We also benefit from our
captive Shanghai, China-based facilities to assemble and test our products. Our products are created in our global
research and development labs with concurrent NAND memory, controller and firmware development, and this
integrated approach has been a key enabler of our success over the last 25 years. As we look into the future, and
as our business becomes increasingly OEM and enterprise driven, we believe that our vertical integration
capabilities will become even more of a key differentiator for, and an enabler of, our continued success.
Summary
As we look back on the past quarter century of the flash memory industry, it is important to take note of how far
we have come. In 2012, we made strong progress on many fronts and exited the year with solid momentum.
Looking forward to SanDisks 25th anniversary as a company in 2013, it is clear to me that our future has never
been brighter.
We are a leader in the markets we helped create and we are expanding our reach into rapid growth areas such as
SSDs. Our strong balance sheet provides us with the flexibility to make strategic investments, which will
enhance our ability to build long-term shareholder value. I would like to thank our employees for their dedication
and contribution to SanDisks success. I would also like thank you for your continued support of SanDisk and I
hope you will remain an important member of our exciting journey.
Sincerely yours,
Sanjay Mehrotra
Co-founder, President and Chief Executive Officer