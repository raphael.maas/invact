

We spent 2002 getting our business in shape. No doubt the environment was worse than we expected,
the weather better than expected, and the accounting profession entered the fray to make everything
more complicated than expected. Nonetheless, we were steadfast in our determination to deliver, and
more importantly, to expand on a strategy in which we have a great deal of confidence.



   We lived by the idea that we had to earn our right to grow.           announced an additional dividend increase of 8 percent, making
That meant some tough decisions and hard work. It also meant             it an annual rate of $1.04 per share, up from $0.96 per share.
changing or stopping some of the things we were doing and                    When I look back on the challenges we and the rest of the
focusing on what we do best.                                             industry faced in 2002, I'm very proud that we produced such
   The results of our efforts are gratifying: a strong balance           strong financial results and increased the dividend. To see the
sheet, a solid utility business, a fast-growing competitive supply       real strength of our performance, you have to look at what
business, and an enhanced focus on our customers. In addition,           happened beyond the company.
we continued our leadership in full disclosure and delivered                 As we all know, 2002 was a very difficult year, especially
strong financial results.                                                for the energy sector. The Dow Jones Utility Index was down
                                                                         27 percent. Valuations were affected by the weak energy
Solid Earnings Growth                                                    business environment, the fact that many companies had
Against a backdrop of lowered earnings expectations in our               earnings decreases year over year, the credibility issues
industry, we reported solid earnings growth. Our reported                associated with trading and accounting scandals, and a true
earnings per share were $3.20 in 2002, compared with                     credit and liquidity crisis for many of our peers.




          is to be the first-choice provider for customers seeking       Our mission is to be the nation's leading energy manager and
Our vision

energy solutions in the complex and changing energy marketplace.         competitive supplier, generating and delivering power and natural
                                                                         gas safely and reliably to our customers while acting in the interests
                                                                         of our communities, employees, shareholders, and the environment.




                                                                         Sharpening Our Focus
$0.57 in 2001. Our 2002 reported earnings include some
benefit from special items --primarily non-core asset sales.             We were one of the first companies to recognize these shifting
Our 2001 reported earnings included special items as well--              market dynamics, and we acted quickly.
primarily losses due to contract termination and workforce                   We increased our focus on generating and selling energy
reduction costs. As detailed on the financial highlights page,           and we sold $708 million of non-core assets -- businesses and
our earnings for 2002-- excluding special items -- were                  operations not directly involved in our core business. We
$2.52 per share versus $2.41 per share in 2001.                          continued to invest in our risk management processes and
    Our 2002 earnings reflect a negative impact of $0.32 for a           strengthen our control procedures. I believe this has put us in
shift from mark-to-market to accrual accounting for certain              the forefront of the industry. It also has allowed us to avoid many
parts of our competitive supply business, which was precipi-             of the issues that befell our competition.
tated by changes in the way we do business. This approach                    More importantly, we knew that a strong and stable balance
was validated by the Emerging Issues Task Force of the                   sheet would mean the difference between success and failure.
Financial Accounting Standards Board issuance of EITF 02-3               By selling non-core assets and extending the maturity of
late last year.                                                          $2.5 billion of debt, we have constructed one of the best balance
    Despite the implementation of this conservative policy, we           sheets in the industry and positioned our company to grow.
were still able to grow our earnings -- excluding special items --           We constructively renegotiated our contract with the
4.6 percent relative to 2001 earnings per share.                         California Department of Water Resources, which allowed us to
                                                                         resolve a significant uncertainty and provide greater visibility into
Performing Well in a Challenging Environment                             our earnings. We also made great progress in integrating our
For the year, our stock closed up 4.8 percent over the 2001              new acquisitions -- Nine Mile Point and NewEnergy -- into the
closing price. Assuming reinvestment of dividends, our total             Constellation family.
return to shareholders in 2002 was 8.5 percent. We also                      And finally, we sharpened our focus on the right business
doubled our dividend in 2002. And in January 2003, we                    model. Namely, we worked aggressively to develop further a




                                                                     
competitive supply business that balances our generation                We also have a platform that we believe can grow faster than
and regulated distribution businesses and enhances our                  the industry averages. The employees at Constellation worked
growth potential.                                                       very hard last year to create this platform in the face of much
                                                                        adversity in the industry. Their commitment, dedication, and
                                                                        focus are the main reasons for our success.
Where We're Headed

Through our competitive supply platform, we serve as the
energy cost manager for utilities and large commercial and              Our Thanks

industrial customers throughout the country. We moved to                I want also to pay tribute to two retiring Directors who have
                                                                        made significant contributions to this company for many years.
expand that business platform with the acquisitions of
                                                                        First, Chris Poindexter, my predecessor as Chairman and CEO,
NewEnergy from AES, and Fellon-McCord and Alliance Energy
                                                                        has retired from the Board of Directors. His 35 years of service
Services from Allegheny Energy.
                                                                        to this company span from the early years of building the
   We have a significant share of a large but fragmented
                                                                        Calvert Cliffs Nuclear Power Plant to his courageous
market. In fact, by our estimates, we are the largest provider of
                                                                        stewardship of the company through the deregulation process.
power and energy cost management to wholesale, commercial




            will help us in our mission to reach our vision:
Our values


  integrity, teamwork, social and environmental responsibility,
   and customer focus guide our actions.
  speed, accountability, passion for excellence, and creation
   of value measure our performance.




                                                                        His loyalty and impact on Constellation will be a permanent
and industrial customers in deregulated energy markets. And
                                                                        legacy. Bev Byron, retiring in April, has been on the Board of
we're seeing great opportunities to continue to grow our share.
                                                                        Directors for 10 years. Her insight and commitment to our
   We believe we have a strong competitive advantage in our
customer relationships, our physical assets, our intellectual           company have made a real difference.
capital, and our five years' experience in modeling, evaluating,
assuming, and managing the unique risks associated with
energy supply and cost management.                                      Sincerely,

Why We'll be Successful

In simple terms, we generate and we sell energy. Most
importantly, we meet our customers' energy needs.
    One of the key elements of our future success will be this
customer focus. Managing the risk and complexity of energy
use and cost is a unique skill that is highly valued by our large
customer base.
    We also will be successful by continuing to focus on opera-
tional productivity. Process improvement has become part of our         Mayo A. Shattuck III
culture, and we have reaped large savings from a number of              Chairman of the Board, President and Chief Executive Officer
initiatives throughout the company. With our launch of Six Sigma        March 7, 2003
in 2002, we have institutionalized the notion that we must keep
getting better and more efficient at everything that we do.
    Finally, we will be successful because we have a business
model that allows for strong, stable, and predictable cash flow.

