Dear Shareholders:


We�re living in a time of unprecedented change.
Technology pervades every functional area of the
enterprise, and the pace and impact of technology-driven
change are accelerating. Technology-driven innovation
is disrupting entire industries. The scope, scale and
economic impact of technology are enormous, and the
rate of macroeconomic and political volatility is increasing.
Leaders need help determining how to adapt
to this change.
Across every functional area of the business, leaders
turn to Gartner for the help they need.
Gartner has the technology expertise and, with the
2017 acquisition of CEB, we also have deep business
expertise across all major functional areas of the
enterprise � including HR, Finance, Marketing, Supply
Chain, Sales, Product and Legal.
Our unmatched combination of expert-led, practitionersourced and data-driven research steers clients toward
the right decisions on the issues that matter most.
We equip business leaders across the enterprise with
indispensable insights, advice and tools to achieve
their mission-critical priorities.
We�re trusted as an objective resource and a critical
partner by more than 12,000 organizations in more than
100 countries � across all major functions, in every
industry and enterprise size.
This represents an enormous market
opportunity.
We estimate that the combined market opportunity
across the functional areas we serve is almost $200
billion. Our contract value at December 31, 2017 was
$2.8 billion. This means we can grow at double-digit
rates for a very long time.
We know how to capture that opportunity. Gartner has
an attractive business model, with recurring revenue,
high renewal rates and strong contribution margins, all
of which enable us to generate long-term, sustained
double-digit growth in contract value, revenue, earnings
and free cash flow.
We know how to grow syndicated research businesses.
We�ve developed proven practices for success and we
drive globally consistent execution of these practices.
The CEB acquisition integration has gone
extraordinarily well.
In less than one year, we have fully integrated our
two organizations, bringing together about 10,000
heritage Gartner associates with about 5,000 heritage
CEB associates. We have met our expectations of
capturing synergies. We�ve launched a number of new,
seat-based products. We determined the Talent
Assessment business did not fit strategically, set it up
as a stand-alone business, and in April 2018, divested
the business.
We made operational changes at heritage CEB to improve
the business. The CEB acquisition is proceeding ahead
of our initial expectations.
We continued to deliver great results
with another year of double-digit contract
value growth.
While we were making great progress with the CEB
integration, the heritage Gartner Research business had
its best year yet. Contract value growth accelerated to
16% excluding the impact of foreign exchange. Once
again, we had double-digit growth in every region across
every size company and in virtually every industry. Wallet
retention was 106%, up 2 percentage points year over
year, and client retention was 84%, steady versus the
prior year. These metrics are near our all-time highs. We
ended the year with almost 12,000 enterprises as clients,
up 7% year over year.

Consolidated Results
For the full year 2017, including the contribution from
acquisitions, we generated $3.5 billion of adjusted
revenue and $661 million of adjusted EBITDA1
, representing
year-over-year growth of 44% and 45% respectively,
excluding the impact of foreign exchange. Diluted earnings
per share excluding acquisition and other adjustments
was $3.31 in 2017, a year-over-year increase of
approximately 12%, and free cash flow was $265 million.
Business Segment Review
Gartner Research, our largest and most profitable
business, closed another record-breaking year with
adjusted revenue of $2.6 billion, an increase of 41% year
over year, excluding the impact of foreign exchange. We
closed 2017 with reported contract value (a key indicator
of future revenue and profitability) of $2.8 billion, an
increase of 46% year over year, excluding the impact
of foreign exchange, and the highest reported contract
value in Gartner history.
On a full-year basis, Gartner Events reported adjusted
revenue of $344 million, an increase of 28% over 2016,
excluding the impact of foreign exchange. In total, the
69 events we held in 2017 attracted more than 67,000
attendees. Gartner Events is the leading global technology
conference provider, enabling professionals from across
all enterprise functions around the world to experience our
research, interact with our analysts and meet business
executives from the world�s leading companies � all in
a single forum. We remain focused on ensuring every
event we produce is a must-attend conference in
every geography in which we operate.
Gartner Consulting extends the reach of our Research
business by deepening our relationships with our largest
clients. Gartner Consulting generated adjusted revenue
of $328 million in 2017, an increase of 3% year over year,
excluding the impact of foreign exchange, and we closed
the year with $95 million of backlog (a leading indicator of
future growth for Consulting) � a 7% improvement from
the prior year.
Gartner Talent Assessment and Other, a new segment
last year, generated adjusted revenue of $226 million in
2017. In early April 2018, we divested the Talent Assessment
portion of this segment.

We�re focused on prudent capital allocation.
Our capital deployment strategy has been consistent
over time. After ensuring that we have appropriately
invested in our business to sustain long-term, doubledigit growth, we use our free cash flow and available
balance sheet flexibility for strategic, value-generating
acquisitions and to return capital to our shareholders
through our share buyback programs. Since closing our
acquisition of CEB, we�ve reduced our gross debt from
around $3.6 billion to $3.3 billion.
Gartner is a growth company.
2017 was a great year for Gartner. We had strong
operating results, we closed on and integrated CEB,
and we invested to drive future growth.
Gartner�s attractive business model, with recurring
revenue, high renewal rates and strong contribution
margins, enables us to generate long-term, sustained
double-digit growth in contract value, revenue, earnings
and free cash flow.
Over the medium term, our objective is double-digit
growth in revenue and adjusted EBITDA, with steady
margins. Because of our low capital intensity and
negative working capital model, we expect to grow
free cash flow at double-digit rates as well.
We remain excited about our business, our prospects
for growth and our strategy to create value for our
shareholders over the long term.
On behalf of everyone at Gartner, we thank you for
your support.
Gene Hall
Chief Executive Officer
Craig Safian
Chief Financial Officer

