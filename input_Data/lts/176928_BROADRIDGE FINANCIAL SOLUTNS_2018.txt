To our stockholders
INTRODUCTION
Fiscal 2018 was an exceptional year for Broadridge. We reported strong financial results
and increased the level of investment in our business by spending on new technologies,
new products and investing in our associates. These achievements were recognized in the
marketplace as Broadridge generated total shareholder return of 55%, putting us well into
the top quartile of S&P 500 companies. Speaking of the S&P 500, Broadridge shares
were added to that index in June 2018, recognition of our strong track record of execution
and value creation as a Fintech leader since becoming a public company 11 years ago.
Broadridge enters fiscal 2019 better positioned than ever for future growth. As CEO,
I keep a mental scorecard of where we stand on culture, strategy, products, investments,
client satisfaction, pipeline, and backlog to help me assess the outlook for our company.
Culture is the most important element, and Broadridge has built a strong and unique
client-focused corporate culture, based on the Service-Profit Chain. Strategy is next. We
have the right strategy and the right products to take advantage of the large and growing
demand for our services by extending our strong franchises in governance and capital
markets and by targeting new opportunities in wealth management and in international
markets. Further, we are making the investments we need in new products and new
technologies to drive long-term growth. Critical to my CEO scorecard is that each of those
investments is tied to a tangible market opportunity, with real dollars behind it and clear
senior executive sponsorship. Finally, we are winning in the marketplace. The combination
of our focus on clients and broad product set drives strong client satisfaction, which, in
turn, results in a 97% client revenue retention rate. Looking forward, our sales pipeline is
strong and includes multiple discussions with clients about potentially transformative
transactions for our technology and communications solutions. Our backlog, aided by
another year of record closed sales, rose 18% to a very healthy $295 million. When I look
at my scorecard, I am more bullish than ever.
Given that strong position, my tenure and age, the time is right for Broadridge to transition
to new leadership. I am extremely pleased that Tim Gokey, our current President and
Chief Operating Officer, and my long-time partner in managing our business, will become
Broadridge�s next CEO on January 2, 2019, and I will become the Executive Chairman of
the Broadridge Board of Directors. There is no more committed and capable executive
in Fintech than Tim. Since joining Broadridge in 2010, Tim has served in a variety of
leadership positions. As the architect of the turnaround in our GTO segment and author 

of our current growth strategy, Tim has played a key role in helping us transform Broadridge
into a Fintech leader. He will bring to his new role a deep knowledge of the financial
services industry honed as a Partner at McKinsey and executive at H&R Block, a keen
understanding of the importance of innovation to our future success, and a proven track
record of delivering sustained growth at Broadridge. I am confident that our company
will be in strong hands.
In my new role as Executive Chairman, I will manage the Board and will continue to
work on regulatory matters, including digital adoption and raising retail investor
engagement. Les Brun, our current Chairman, will continue to serve on our Board as
Broadridge�s Lead Independent Director.
STRONG FISCAL 2018 PERFORMANCE
Broadridge delivered very strong financial results in fiscal 2018, including record closed
sales up 14%, total revenues up 5% to $4.3 billion, 10% growth in Adjusted operating
income and Adjusted EPS growth of 34%. Just as importantly, we were able to balance
strong bottom-line performance with making the important investments that will sustain
our market leadership and deliver long-term growth.


ARTIFICIAL INTELLIGENCE
We are bringing to market innovative solutions that help our clients harness
the transformational power of AI.
We continue to recognize the potential of artificial intelligence to help our clients streamline
operations, make better decisions and enhance customer engagement. We are exploring
ways to apply AI, including machine learning and robotic process automation, in innovative
ways across segments and functions, from trade allocation and securities financing to
investment accounting.

I will start my review of those results with my favorite topic: closed sales, or, to be more
specific, record closed sales! Broadridge reported a record $215 million in closed sales,
up 14% from fiscal 2017. What is really exciting about this performance is not just the
growth, but the breadth and quality of the mandates we�ve won, which touch all aspects
of our business, worldwide. This remarkable performance reflects our strong value proposition
as we help our clients get ahead of today�s challenges to capitalize on what�s next and
drive the transformation of their businesses.
For example, our Governance business signed a fast-growing new client and an extension
with a major existing client that included a significant expansion of services. In Capital
Markets, we added the North American operations of a major Asian bank and won two
new clients in the Japanese market. And, in Wealth Management, we grew our business
with a significant retail brokerage client, further expanded our reach in the Canadian
market, and recorded notable wins in our front-office advisory solutions products.
Both of our operating segments performed well in fiscal 2018. The Investor Communication
Solutions (ICS) segment delivered revenue growth of 3% in fiscal 2018 to $3.5 billion.
ICS recurring fee revenues, which exclude distribution revenues and event-driven revenues,
grew a solid 5%. 

Our cognitive marketing tool combines AI with advanced analytics and deep
learning technology to help advisors identify prospects and make personal connections.

The biggest contributors to this growth were our core proxy and mutual fund governance
products, which benefited from the continued demand for stocks, mutual funds, and
exchange traded funds (ETFs). As I�ve said in the past, the popularity of these investments
has consistently trended upward over time, creating increased demand for our governance
and proxy services, and fiscal 2018 was no exception. We also benefited from strong
demand for our data and analytics products and for our wealth management products,
which target front-office financial advisors.
Customer communications revenues were flat for the year, in part because of subscriber
losses at some of our larger clients. More encouraging is that our clients are increasingly
recognizing the value that Broadridge can bring to them as a result of our investments in
digital distribution. Our customer communications pipeline is strong and we are building
the digital capabilities that will drive continued market leadership.
Event-driven activity was another important contributor to the growth in ICS revenues.
By definition, event-driven activity does not reoccur on a predictable annual cycle. In
fiscal 2018, Broadridge benefited from a proxy vote at a very large mutual fund complex
as well as several large equity proxy contests.
The activity behind these revenues is very much a part of the corporate governance
process in which Broadridge plays such a critical role. When mutual fund complexes go
out for a proxy vote every five or so years, they are able to do so because Broadridge built
and maintains an infrastructure that enables them to communicate with millions of
investors and accurately count tens of millions of votes to get them the approvals they
need on governance matters. Another driver of event-driven revenues is a proxy contest,
where in addition to management, activists also send a proxy, with both sides using our
infrastructure to make multiple distributions in an effort to persuade shareholders to
vote one way or the other. Regardless of whether the meeting is routine or contested,
Broadridge is the honest broker in this process and we prove it every day through
transparency and independent verification.
Our Global Technology and Operations (GTO) segment keeps getting stronger, with
revenue growth of 10% to $912 million. The biggest growth driver continues to be the
addition of new clients onto our platforms. Our clients continue to face pressure to
reduce both the cost and complexity of their operations and they see Broadridge as a
partner who can help them achieve that goal. 

An added benefit to our growth in fiscal 2018 was an increase in trading volume across
both equity and fixed income markets. A third growth driver was the increasing demand
for non-trade services, including professional services, where clients are turning to us to
help them implement new technology and streamline their operations.
As in the past, we converted Broadridge�s revenue growth into even stronger earnings
growth while continuing to invest in our business. We strengthened our existing product
suite by investing in new technologies like blockchain, cloud, and AI. We increased investment
in products that will drive growth over the next three to five years, like fixed income,
tax, advisor tools, data and digital. And we invested in our associates by increasing benefits
and raising our minimum hourly wage as part of our focus on the Service-Profit Chain.
Even with these investments, we delivered strong 10% growth in Adjusted operating
income. Combined with the benefits of tax reform, this resulted in a 33% increase in
Adjusted net earnings to $504 million and a 34% increase in Adjusted earnings per
share to $4.19.
I am very pleased with our strong financial results.
BALANCED CAPITAL ALLOCATION STRATEGY
We generate a significant amount of cash flow every year. What we do with that cash
�your cash�to grow our business and create value for our stockholders is one of the
most important decisions we make. At Broadridge our focus remains on using that cash
in a balanced way to invest in our business and reward our stockholders through dividends
and share repurchases.
We view acquisitions as an integral part of the investments we are making in broadening
our product lineup. Broadridge invested $148 million in M&A in fiscal 2018, completing
six tuck-in acquisitions to strengthen our data capabilities and expand the range of services
we offer to funds and corporate issuers. We also invested $98 million in capital expenditures
and software investments, including the upgrade of our GTO center of operations, where
we moved into a modern facility in the Gateway complex in downtown Newark, New Jersey.
We returned a total of $391 million to stockholders. Broadridge�s commitment to a
strong dividend is an important component of our strategy to create long-term shareholder
value. This year, we returned $166 million through the dividend, and, consistent with
our commitment to a target payout ratio of 45% of our Adjusted net earnings, the Board
approved a 33% annual dividend increase to $1.94 per share for fiscal 2019. Broadridge has 


increased its dividend every year since becoming a public company. That�s 11 consecutive
years, including seven years in a row of double digit increases, of delivering higher returns
to our shareholders.
Share repurchases are the final aspect of our balanced capital allocation strategy. In
fiscal 2018, we repurchased 2 million shares, returning $225 million to stockholders in
the process.
A LARGE AND GROWING MARKET
An important reason I am so confident in the outlook for Broadridge is our large and
growing addressable market. We have estimated the size of the market to be $40 billion.
From my seat, I see Broadridge�s market opportunity as unlimited for all practical purposes.
Client focus is a core principle of our Service-Profit Chain culture and I spend at least
50% of my time�more when my other responsibilities allow it�focused on client issues.
So I hear firsthand in meeting after meeting about both the challenges and opportunities
that our clients face. I know corporate issuers and mutual funds are looking for the most
efficient and effective means of communicating with shareholders, and that their needs
here have only increased given cost pressures and the rise of activism as an asset class. 

 hear regularly how financial institutions face pressure to cut operating costs, optimize
capital and liquidity, meet increasingly complex regulatory requirements across multiple
geographies, and adapt to evolving technologies. And I understand that wealth managers
are seeking ways to remain relevant and profitable and differentiate themselves in the
face of the rapid growth of alternative brokerage options, the growth of ETFs/passive
products, and ever-changing regulations, among other challenges.
The good news is that Broadridge brings solutions to bear on each one of those problems
and more. As a Fintech leader, our breadth and depth of knowledge and hands-on delivery,
together with our powerful technology platforms, empower our clients to navigate the
needs of today and tomorrow for their own clients.
A CLEAR STRATEGY TO DELIVER NETWORK VALUE AND LONG-TERM GROWTH
Another reason I am confident in our future is that we have the right strategy to go after
that large and growing market opportunity. It�s a clear strategy, because if there is a lesson
I have learned in four decades of developing and executing business plans, it is that there
is a direct relationship between a plan�s clarity and its execution. So there are only three
primary elements to the Broadridge growth strategy. First, we will extend our Governance 

BLOCKCHAIN

We are applying innovation to real-world challenges for measurable results.
Broadridge is a blockchain leader. As our clients face increasing cost pressure, transitioning
technologies and other challenges, we continue to invest both organically and strategically
in blockchain innovation. We have successfully partnered with leading global banks on
proofs of concept and live pilots, including a global proxy pilot with Banco Santander,
JP Morgan and Northern Trust to test practical uses for blockchain in investor voting

franchise. Second, we will drive growth in our Capital Markets franchise. And third, we
intend to build our next franchise business in Wealth Management. We see compelling
opportunities across all three fronts in both North America and globally.
A key word in that strategy is franchise. At Broadridge, we define franchise as a business
having a truly differentiated value proposition and, more importantly, as one that creates
network value. The concept of network value is critical to our plans and important in
understanding how we think about our business. The basic concept of network value is
that the value for our services increases as we add more clients to our platform and greatly
exceeds that of any point-to-point solution. How does Broadridge deliver network value
to our clients? Five ways:
1. Our mutualized platform enables economic efficiencies for all parties across mission
critical functions. That is the power of scale.
2. Our visibility across the ecosystem allows us to enhance all clients� compliance, risk
management and other capabilities. That �halo effect� comes from the breadth of our
client base, which gives us the ability to address issues emerging at one client and apply
a solution that serves all.
3. Our ability to harness data from the network to deliver critical and real-time insights
to all participating clients beyond just their firm, providing a broader and more accurate
market perspective.
4. Our scale and deep focus allow us more rapidly to transition our clients to next-gen
technologies, including what we are calling the Broadridge ABCDs of innovation: AI,
Blockchain, Cloud and Digital.
5. Our platforms enable clients to take advantage of our industry-leading cybersecurity
capabilities, which require significant and sustained levels of investment. Our clients
need to know that their data is secure as it passes through our platforms and we are
among only a handful of firms who have the scale to make those investments and to
earn that level of trust.
At the end of the day, it�s really quite simple. Our aim is to make sure clients understand
the benefits that being in Broadridge�s network, along with hundreds of other clients,
can provide in the form of lower cost, reduced complexity, and increased effectiveness.
Alone you�re a company. Together, we�re an industry.
The last part of our strategy is about ensuring that we keep our focus on the long-term
growth of our business. That means having the fortitude to invest in new products and 

new technologies that will be critical to the next phase of our growth. All too often, and
especially for publicly-traded companies, the pressure to generate near-term profits can
crowd out medium-term investments. At Broadridge, we are committed to sustaining
our leadership position by making sure that we deliver value to our clients today...and
even more value tomorrow.
All of our activities are underpinned by a long-term commitment to exceptional client
service aligned with the Service-Profit Chain.
Our growth strategy is clear.
EXTENDING OUR GOVERNANCE FRANCHISE
So how are we doing on the execution?
I will start with governance, where we are driving four broad opportunities to extend our
Governance franchise.
The first large opportunity is to drive the next-generation of regulatory communications.
Recent actions by the Securities and Exchange Commission (SEC) have only increased
my excitement about how Broadridge can apply our technology to transform how funds
and issuers communicate with their shareholders. The SEC appears to have clear goals:
utilize technology to increase shareholder engagement, further drive down costs, and
strengthen our system of corporate governance. From my perspective of 40 years of
experience in engaging with the SEC on these issues, I have never seen greater alignment
between its goals and Broadridge�s unique ability to facilitate those goals through the
creation of enhanced content, delivered digitally in a personalized format, compounded
by our long-term history of driving down costs. We�re not aware of another company that
can match our capabilities.
We are working with clients and with the SEC to create enhanced content for ETF and
mutual fund investors and, potentially, for equity investors. That means interim and
annual reports that deliver more relevant information to shareholders in a format that
is easy to read and digest, and works both on paper and in an interactive digital format
that costs less to deliver. This capability is unique to Broadridge.
We continue to drive conventional and next-generation digital delivery. We invested
further in our digital capabilities with the acquisition of ActivePath, and we have realigned
our customer communications organization to focus more intensely on growing our digital
revenues. Our clients are taking notice and increasingly see this as a key differentiator
for Broadridge.

Finally, Broadridge is making blockchain a reality in proxy voting. After running a non-U.S.
shareholder meeting on blockchain for each of the past two years, we now have an
end-to-end solution built on distributed ledger technology for the U.S. proxy market,
and in fiscal 2018 we conducted the first annual meetings on blockchain for North American
issuers. Broadridge will join that list this fall, as our 2018 Annual Meeting will also be
conducted on blockchain. We also successfully completed pilots in partnership with our
Capital Markets clients to test the effectiveness of blockchain in bilateral repurchase
agreements, demonstrating the power of this technology to improve these processes as
well. More and more, Broadridge is a blockchain leader.
Data is the second broad opportunity and focus area for us to extend our Governance
franchise. We are integrating the data from our governance products with other data
sources to deliver insight to our clients, especially our investment management clients.
Over the past year we have made several acquisitions to extend our data capabilities
for global asset managers, including Spence Johnson, the Morningstar fund advisory
product, and MackayWilliams. We expect these acquisitions to both broaden and deepen
our available data sets and help sustain the momentum we are seeing in the marketplace. 

CLOUD

We are accelerating adoption of cloud technology by enabling a
streamlined, secure and cost-effective migration path for clients.
The cloud continues to open up new opportunities for firms to optimize operations,
reduce costs and grow profitably. Broadridge is uniquely positioned to help our clients
capitalize on these opportunities. Our standardized processes and technology, built on
a leading cloud platform and supported by a dedicated team, help mutualize costs and
reduce risks. 

A third and critical element of our strategy is to build a leading omni-channel, cloud-enabled
communications hub for high value customer communications. As I have shared with
you in the past, our clients are frustrated that 20 years since email access has become
ubiquitous, the vast majority of bills and statements continue to be delivered on paper
to a physical mailbox. Having already saved our clients billions of dollars over the past
decade by successfully digitizing 64% of regulatory and proxy communications, we see this
as a significant opportunity. Broadridge aims to play a key role in helping the industry transform
from the two-dimensional world of email and physical mail into a more multi-dimensional
world that incorporates an ability to deliver communications directly to a consumer�s
chosen cloud drive. To make this vision a reality, we have created a dedicated digital sales
team and invested in digital technology that enhances the consumer experience around
consumer statements, bills, and regulatory communications, furthering our capacity to
deliver enhanced digital content for our clients. We are also testing our ability to deliver
statements directly to a widely-used cloud drive.
Our clients are paying attention. They know that they need to transform their digital
delivery capabilities to be ready for this evolution. Broadridge�s work in building out our
next-gen communications hub is a critical selling point with key strategic clients and we
are seeing a growing pipeline of opportunities. 

The last element of our governance strategy is an increased focus on serving corporate
issuers. Almost every public company around the globe relies on Broadridge in varying
degrees in their annual meeting process. We see an opportunity to continue to build on
these relationships to offer a broader array of services. In fiscal 2018, we expanded our
range of issuer services through the acquisition of Summit Financial, adding document
management capabilities to our product portfolio.
In addition, we are ramping up our efforts to encourage sustained engagement with
retail shareholders by corporate issuers. Too often, it takes a proxy contest for corporate
managements to reach out to retail shareholders to get their message out and encourage
them to vote. This is a missed opportunity. We are working to persuade companies
to communicate regularly, keeping their retail shareholders up to date on key strategic
matters and on their performance. Making an effort to drive retail shareholder engagement
on a regular basis seems to me to be an inexpensive form of insurance. That is especially
true when Broadridge can help them do it for a tiny fraction of what they might spend
in the event of a contest.
We are extending our Governance franchise.
DRIVING OUR CAPITAL MARKETS FRANCHISE
The second pillar of our growth strategy is to drive our Capital Markets franchise by helping
our clients simplify and transform their complex technology and operations eco-systems.
We continue to make strong progress in bringing new clients onto our global post-trade
technology platform. For example, we now have a major client up and running across
Europe, Asia and the Americas on a truly global platform. Earlier in the year we won a
new contract with a Tier 1 bank in which we will bring six separate corporate actions
systems into a single global platform managed by Broadridge. Demand for mutualization
remains strong, and our recent wins in Japan and elsewhere point to the strength and
breadth of this opportunity and validate our efforts to further build our business outside
North America.
We are also working to create additional network value for our capital markets clients.
One important effort we are making is in the fixed income market. Today, approximately
$5 trillion of fixed income transactions take place on our post trade processing platform
used by 18 of the 23 primary dealers. We see an opportunity to apply AI capabilities to
the unique transaction data on our platform in order to bring additional liquidity to the
very illiquid corporate bond market. We are in early days on this effort but have 

committed real resources and I am excited about the long-term potential to deliver huge
value to our clients.
We are driving our Capital Markets franchise.
BUILDING A WEALTH MANAGEMENT FRANCHISE
Third, we see a significant opportunity to create value for our clients and stockholders
by building a franchise business in wealth management. Wealth managers are faced
with critical and conflicting mandates to reduce cost, integrate new technologies and
deliver enhanced services to their clients, all while meeting ever increasing regulatory
requirements. It�s an enormous challenge and they are asking Broadridge to help them
reduce the cost and complexity of their operations to accomplish their goals.
Broadridge already has a big footprint in wealth management, as we service the top 20
North American wealth providers in some capacity, and we have a set of strong products
aimed at wealth management clients in both our GTO and ICS segments. The next step
in creating a franchise is to develop an integrated platform that will create a seamless
network that brings together our proprietary solutions across the front�, middle�, and
back-office operations with other best-in-class capabilities. Client interest in this product
is very high.
In the meantime, we continue to see strong demand across both GTO and ICS for our
wealth management point solutions. In GTO, we have won some significant new back-office
business from retail wealth players in both the U.S. and Canada. In ICS, as I noted earlier,
our front-office solutions for financial advisors contributed nicely to our overall growth
in fiscal 2018.
We are making good progress in building the next Broadridge franchise in wealth
management.
Finally, the drivers of demand across our markets are global so we are also working to
expand the use of our solutions in markets outside North America. We notched several
significant international sales wins in 2018, which is evidence that our efforts are paying
dividends. We estimate a multi-billion dollar market opportunity exists internationally,
and we are seeing clients expand their use of our services across new regions as new clients
onboard to our platforms.
Across governance, capital markets, wealth management and international, our strategy
is working. 


READY FOR NEXT
At Broadridge, we use �Ready for Next� as our tagline to remind clients that we will help
them navigate risk, enrich client engagement, optimize efficiency and grow their business.
To me, as the CEO, it is a commitment made to both our clients and associates that the
work we do today is important and valuable and that we intend to ensure that the work
we do tomorrow is more important and even more valuable. To you as an investor, it is a
clear signal that we are spending real dollars today on new products and new technologies
that are tied to real opportunities to drive our growth in the future.
Our long-term focus paid off in fiscal 2018. Much of the recurring revenue growth we
saw last year was a direct result of the initiatives we have been driving the past three
years. Investments in growing our data capabilities, extending our product line in wealth
management and in enhancing our global post-trade technology platform all contributed
to our revenues and should continue to sustain our growth over the next several years.
Going forward, we will continue to invest in new products and technologies that will
drive our innovation roadmap and develop new products and services and prepare our
clients for next generation challenges. Increasingly, our investment focus is on new
technologies, especially the Broadridge ABCDs of AI, Blockchain, Cloud, and Digital. We
believe these offer compelling opportunities for Broadridge to utilize its experience and
unique market perspective to build-out solutions that allow clients to turn disruption
into opportunity for growth, and uncertainty into confidence.
BROADRIDGE CULTURE AND THE SERVICE-PROFIT CHAIN
One of the great lessons of my career is the importance of corporate culture to our
success as a company and it is an area about which Tim and I are both passionate.
Broadridge is committed to creating value for all stakeholders through the Service-Profit
Chain, through which highly-engaged associates deliver superior service, generate strong
business performance, and create shareholder value. We independently measure both
associate and client satisfaction.
We view every client as a 100-year client, and client satisfaction is the only common
metric on which every one of our more than 10,000 associates is compensated. That
foundation of partnership bolsters the work we do and earns our clients� trust every day.
As a result, we are consistently rated by our clients as a top vendor, which in turn creates
greater loyalty and willingness to work more with Broadridge.

DIGITAL


Building on our heritage of innovation, we continue to drive digital
transformation.
We see digitization as not just a center of efficiency and cost savings for our clients�
but as an essential component for growth. That�s why we are committed to driving
digital transformation. Our track record speaks for itself: Our investments have saved
our clients billions of dollars over the past decade. And we are still extending our
technology, data and channel partnerships to transform and accelerate the pace of
digital adoption. Our recent acquisition of an Israeli-based digital technology company,
ActivePath, helps our clients enhance the experience associated with statements, bills,
and regulatory communications for their customers.

It takes engaged, dedicated and knowledgeable associates to serve clients well and create
real and sustainable competitive advantage and value. Broadridge understands this, and
we are committed to a culture of service and performance. This year, we took important
steps to further enhance our employee pay and benefit programs, and to bolster the depth
and talent of our management team across the organization. We increased hourly wages,
expanded parental leave and vacation days, and raised our charitable giving match. We
strive to be an employer of choice and are passionate about creating an environment in
which every associate can thrive, build their knowledge and skills, and be rewarded for
doing so.
We are also pursuing Corporate Social Responsibility efforts designed to elevate the
communities we touch around the world. The Broadridge Foundation is at the heart of
our Company�s community efforts, and partners with organizations focused on providing
educational opportunities in underserved areas. These programs have an impact on the
lives of thousands of children around the world, and Broadridge is proud to play its part.
We are also dedicated to reducing our environmental impact. This includes a focus on
reducing paper waste, an issue that is aligned with the goals of our business to drive
digital communications. Every client that converts to paperless delivery of shareholder
materials contributes to fewer trees cut down, less carbon dioxide in our environment and 

a reduction in the emissions stream that physical communications require. We�re proud
of the positive impact we are having on the environment by driving digital adoption.
THE BEST IS YET TO COME
This is my last stockholder letter as CEO. After four decades of building and leading
businesses, including nearly a dozen years as CEO, it�s hard for me to imagine what my
life will be like after I leave Broadridge. The good news for me is that time has not come
yet. I am delighted that Tim has encouraged me to stay and our independent Board
members have appointed me Executive Chairman. In my new role I will manage the Board
and work with Tim on specific regulatory matters for which I have an incredible passion,
including digital adoption and working to better engage retail investors. My new activities
will in many ways take me back to my entrepreneurial roots, when I focused less on the
day-to-day management of the company and more on value propositions to evolve the 

governance marketplace to create opportunities for our business and to improve the
effectiveness and efficiency of the process for brokers, issuers, funds and regulators.
I am more excited about the future of our company today than when I sat down in my
extra bedroom 30 odd years ago to write the first business plan for our communications
business. The opportunities for Broadridge are bigger than ever. We have the right
strategy, the right products and the right investments to ensure that we can deliver value
to our clients today, more value tomorrow, and even more value in five years. We also
have in Tim Gokey the right next CEO and a strong and deep management team to lead
our company to that next level of growth.
I want to thank all of our stockholders for their confidence in our management team and
me. I also want to thank our Board of Directors for their independent guidance and insights
that are so important to effective governance.
Lastly, and most importantly, I want to thank my fellow Broadridge associates. At work,
as in other areas of our lives, it is the people we surround ourselves with that make the
difference. At Broadridge, the hard work and dedication of our associates have enabled
us to build the great Fintech company that it has been my privilege to lead.
The best is yet to come.
Sincerely,

Rich Daly
Chief Executive Officer


