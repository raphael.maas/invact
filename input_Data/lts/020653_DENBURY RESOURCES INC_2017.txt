DEAR FELLOW SHAREHOLDERS

I believe that 2017 will be remembered as the beginning of a powerful transformation for Denbury. Against a backdrop of high volatility in oil prices - which touched a low of S42 in June - we made the decision at mid-year to meet that volatility head on,
and structure Denbury for a long-term S50 oil price environment.
Implementing this change required us to make tough decisions
and take bold steps to become leaner and more cost-efficient throughout our organization, as well as to ensure that our capital investments would deliver attractive returns at S50 oil prices.
While these changes were difficult, I am pleased to report that today we are a better, stronger, and more efficient company.
Reflecting on our 2017 accomplishments, I am extremely proud of how our employees continued to maximize the value of our
long-lived legacy fields, with multiple key fields increasing in production, even with limited capital investment over the past several years. Our sustained focus on execution improved the consistency of project delivery, production reliability, and overall Health, Safety, and Environmental performance. I am also pleased with our multiple other accomplishments,  including:
�	Initiating debt transactions  that reduced our total debt principal by S184 million, with potential to increase to a
total reduction of S329 million, if the convertible notes issued in those exchanges fully convert into shares of common stock;
�	Streamlining and focusing our organization, which lowered our general and administrative expense to its lowest level
si nee 2008;

�	Remaining disciplined with our capital, spending less than cash flow, and underspending relative to our original capital
budget by 20%;

�	Resuming quarterly production growth in the second half of the year and more than replacing 2017 production with
reserve additions, both of which had been declining for the last few years;

�	Identifying and maturing a significant number of exploitation opportunities across our asset base, and
successfully completing our first Mission Canyon exploitation well in the Cedar Creek Anticline; and

�	Using our strategic C02
 
position to enter a new C02
 

EOR
flood at West Yellow Creek in Mississippi, and acquiring a
working interest position in Salt Creek Field in Wyoming.

I believe our actions and accomplishments in 2017 have laid the foundation for our future success.  With recent oil prices showing positive signs of recovery, the combined impact of higher prices, lower costs and lower debt are beginning to significantly
improve our debt metrics. While reducing our leverage to a 3 to 4 times debt/EBITDA ratio is a major priority for the Company in the near-term, our fourth quarter 2017 annualized total debt/ EBITDA ratio reflected a 4.4 times level, providing a foreseeable path to more significant improvement in a sustained S55 to S6o oil price environment.
One of Denbury's unique attributes is our extraordinary weighting to crude oil, representing 97% of both our current production and our year-end 2017 proved reserves, a higher level than any peer in the industry. We are very excited about our exposure to crude oil and the total potential within our existing fields, which is roughly quadruple our year end 2017 proved reserves of 260 million barrels of oil equivalent, and are equally excited by the significant growth opportunities and upside within our portfolio.
A great example of one of these growth opportunities is our recent Mission Canyon exploitation success at Cedar Creek Anticline, where our first horizontal well came on line in late 2017. The well was successfully drilled with a 4,800-foot lateral section targeting a four-foot interval, completed open hole, and
produced at an average of 1,050 barrels of crude oil per day over its first 30 days. We plan to accelerate development of this play, and we just completed drilling two additional wells, with four more wells planned for 2018, and around 20 wells planned for the


2019+ time frame. We also expect that Mission Canyon will be an ideal EOR target, and we are integrating a potential Mission Canyon C02 flood into our rapidly progressing Cedar Creek
Anticline  EOR development plans.

With its combination of significant reserves potential and short cycle development time frame, exploitation will be a focus in 2018, and we plan to invest over S30 million in advancing this program. In addition to further development at Mission Canyon, we plan to drill a well to test the Perry Sand at Tinsley Field, and another well to test prospective unconventional Powder River Basin acreage underlying our Hartzog Draw Field. Our teams continue to add new opportunities to an already sizable exploitation portfolio as they work through our 600,000 net
acre leasehold.

We believe it is important to continually evaluate our assets along their life cycles to determine when to divest of an asset to optimize our portfolio.  In 2017, we identified several of our mature oil fields in the Gulf Coast region as candidates for
divestiture, and we began marketing them for sale in early 2018. In 2017, we also initiated a sales process to sell around 4,000 surface acres for non-oil and gas development near our
Conroe and Webster oil fields. We believe this represents a significant hidden value and expect the proceeds from such sale will both provide flexibility and further enhance our balance sheet. Our ongoing asset evaluation also considers acquisition opportunities, as we seek strategic additions to enhance and upgrade our asset portfolio and long-term development  opportunities.
As noted above, 2017 was a transformative year for Denbury, but our work is far from done. In 2018, even with oil prices moving above S6o, we will maintain our high focus on improving the fundamentals of the Company, and continue to build on our
 
foundation of strong execution, consistent project delivery and production reliability. We are targeting an investment decision on C02 EOR development at Cedar Creek Anticline in the first half
of the year, which would increase oil recovery and provide
significant cash flow for many decades from this extraordinary field. On the Gulf Coast, we will work to find pathways to drive
incremental value from our surplus C02 and pipeline capacity,
and we will maintain a strong focus on continuing to strengthen our balance sheet.

I am very excited about the future for Denbury. Our proven ability to live within cash flow aligns perfectly with shareholder calls for returns-focused capital discipline. Our extraordinary gearing to crude oil provides the opportunity to generate significantly
higher cash flow in an improved oil price environment, while positioning our cost structure to thrive in a S50 oil price world both increases our profitability and protects our downside. Our long-lived assets continue to perform through the thoughtful and innovative stewardship of our dedicated employees. We have a strong foundation to build upon for many years to come.



Sincerely,

 

Chris  Kendall President and
Chief Executive Officer March 30, 2018
