Dear Fellow Shareholders:

The global economic crisis which           generation fleet. To compensate, we           restructuring and competitive energy
began in 2008 affected your company        pursued a growth strategy around the          markets, even though BGE's rates have
in profound ways. We had to make           emergence of competitive markets              always been--and continue to be--in
dramatic shifts in strategy, reshape the   across the United States. We have             line with or below our peers in the
management team and fundamen-              strongly advocated the wisdom of              MidAtlantic and Northeast regions.
tally change the mix of our earnings       competition in energy markets ever
                                                                                         As we developed our strategic platform
power. Both the credit markets and         since. During this period, our increased
                                                                                         to address the competitive markets, we
the commodities markets experienced        earnings power allowed us to build and
                                                                                         became a leader in the marketing and
sudden and sharp changes in 2008           purchase new generation plants. We
                                                                                         trading of commodities, particularly
and required prompt action and             also developed the leading competitive
                                                                                         electricity, gas and coal. Our success
transformation at Constellation Energy.    platform to sell electricity to large
                                                                                         in this business eventually led us to
We have emerged chastened, but also        commercial and industrial customers.
                                                                                         conclude that we should have a larger
confident in our new business model
                                           In many respects, ours was a contrarian       platform and a larger balance sheet to
and the opportunities that we can
                                           strategy, and while we enjoyed many           maintain the growth potential of these
pursue in the energy marketplace. To
                                           years of strong results, we've also           businesses. To that end, we agreed in
understand the effects of 2008, it is
                                           encountered headwinds. Commodity              December 2005 to merge with FPL
useful to look back over a longer period
                                           prices increased dramatically since           Group, a partnership which would have
to put events into perspective.
                                           2001, and the price of electricity did        created one of the nation's largest gener-
Our company has been challenged in         not keep up with the cost of fuel inputs.     ators of clean power with our combined
many ways in the past decade, and yet      The rate caps in Maryland eventually          fleet of nuclear and renewable power
it remains a leader in many sectors of     expired and BGE's residential customers       plants. Unfortunately, the timing of
the energy market with excellent people    were faced with large percentage              this merger collided with the end of
and an excellent mix of physical assets.   increases in their bills in 2006. Adding      residential rate caps in Maryland, and
Ten years ago in 1999, the energy          to the price pressure on power were           the resulting political and regulatory
market in our home state of Maryland       the investments we needed to make to          firestorm ended our efforts to merge
restructured from a regulated model to     meet or exceed improved air emission          with FPL.
a competitive model, and many states       standards and reduce the carbon
                                                                                         After the collapse of the merger, we
across the nation also opened their        footprint of our activities. Given the
                                                                                         engaged in 2007 and through 2008 in
electricity markets to competition.        rising commodity prices and higher
                                                                                         a number of steps to achieve a similar
To facilitate the restructuring process    costs for environmental upgrades,
                                                                                         strategic result, including exploring the
in Maryland, we agreed to a six-year       power prices have increased significantly
                                                                                         sale or joint venture of our commodities
rate freeze for our Baltimore Gas and      compared to the below-market levels
                                                                                         activities. We also sought out and
Electric (BGE) residential electric        under rate caps. During the transition
                                                                                         achieved a settlement with the State
customers. This cap kept residential       to market prices, it has been difficult for
                                                                                         of Maryland, which was intended to
power prices below market rates at a       many of our constituencies to under-
                                                                                         achieve greater regulatory and political
time when commodity fuel costs were        stand or appreciate the merits of
                                                                                         stability by putting the angst of the
rising, and it also reduced the profit-
                                                                                         controversial 1999 restructuring process
ability of both our utility and our
                                                                                         behind us.



                                                                                                                                  
While our businesses were growing            it. That path was to sell Constellation       This series of events in the latter half of
nicely through this period, they were        Energy to an entity which had the             2008 had a high degree of transactional
also dependent on a healthy credit           strongest perceived financial where-          complexity, but I believe that when we
market to support them. Market-based         withal, as well as an established strategic   complete the EDF transaction later this
trading activities require capital on        interest in the energy business. On           year we'll find ourselves in a very strong
both a direct and contingent basis.          Sept. 19, we entered into an acquisition      place.
Contingent capital has to be available       agreement with MidAmerican Energy
                                                                                           The decade-long history since restruc-
to adjust for commodity price swings         Holdings Company, a subsidiary of
                                                                                           turing began has presented many
and potential rating agency actions,         Warren Buffet's Berkshire Hathaway. At
                                                                                           obstacles but also many opportunities.
among other reasons. When we                 the time, MidAmerican was unmatched
                                                                                           We grew our commodities businesses in
entered the unprecedented global             as a financial and strategic partner. Its
                                                                                           response to competitive market oppor-
credit crisis of 2008, commodity prices      credibility in the markets allowed us to
                                                                                           tunities, and then rapidly downsized
had already been experiencing their          maintain our strong customer franchises
                                                                                           them as the capital markets collapsed.
own unprecedented level of volatility.       and stabilize our platform. As the credit
                                                                                           The success of these businesses over
The combination of the two forces            markets continued to erode between
                                                                                           those years, however, allowed us
precluded our ability to consummate          September and December, we worked
                                                                                           to invest and build several other
a strategic action to broaden and            diligently to reduce risk from our
                                                                                           important, more enduring businesses.
stabilize our balance sheet since our        platform and downsize our commod-
                                                                                           We purchased three nuclear plants
likely partners all experienced similar      ities businesses, which required the kind
                                                                                           to establish a fleet; built the largest
pressures. The credit environment had        of credit support no longer available in
                                                                                           competitive retail and wholesale power
suddenly frozen in a fashion where it        the market.
                                                                                           businesses in the country; established
was difficult to support the existing
                                             In early December, our partner in the         a leading platform to pursue the new
business on our own.
                                             new nuclear business, EDF Group,              nuclear renaissance; and developed a
The violence of the markets in the fall      launched a bid to acquire one-half of         cleaner and more efficient fleet of fossil
of 2008 was unlike anything anyone in        our nuclear fleet in the form of a joint      and renewable generation plants. These
the financial markets had ever experi-       venture. Our Board of Directors made          surviving businesses, along with our
enced. There was, no doubt, a need           the determination that EDF's offer            regulated utility, BGE, have allowed us
to react urgently and instinctively. It      was superior to MidAmerican's, and            to increase our fundamental earnings
was a crisis, and many decisions had         we successfully terminated the merger         power by 50 percent since 2001.
to be made in a market environment           agreement with MidAmerican. This
                                                                                            These gains and the broadening of
which was changing by the hour. In the       was the kind of outcome we anticipated
                                                                                           our earnings base are particularly
face of financial institutions collapsing    might happen with the passage of time
                                                                                           meaningful when our long-term
around us, we needed to address the          after the crisis in September, and we
                                                                                           performance is compared to the broader
potential ramifications of a credit rating   were pleased that the successful bidder
                                                                                           market overall. From November 2001,
downgrade and the inability to protect       was not only our established partner,
                                                                                           when I became CEO, through the first
ourselves against it. We, therefore, made    but the largest and most financially
                                                                                           quarter of 2009, our stock performance
the decision to preserve the franchise       sound nuclear operator in the world.
                                                                                           falls in the middle of all S&P 500
in the best way we had available, while
                                                                                           companies. The middle of the pack is
maintaining the option to achieve a
                                                                                           not where we want to be long-term,
higher value if conditions warranted




but this example illustrates that our       divested our London-based coal and            Regulatory Commission (FERC), and
expanded earnings base endured the          freight business and our wholesale            we're in the review process with other
worst of an unprecedented financial         natural gas business. As a result of these    federal and state regulatory agencies.
meltdown in reasonably good fashion.        activities, we expect to receive $1 billion   We still anticipate closing this trans-
Over time, we believe this strong,          in collateral to support our ongoing          action in the third quarter of this year.
stable earnings power will translate        businesses. Our de-risking activities will    I can say without hesitation that
into greater stock value. Everyone at       continue through this year.                   the EDF joint venture is in the best
Constellation Energy has worked hard                                                      long-term interest of our investors.
                                            Our strategic realignment will shift
to adjust the mix of our earnings and                                                     It allows the company to remain
                                            the company's business model to more
thereby adjust to a new world where                                                       independent and publicly traded,
                                            stable, risk-adjusted earnings. The main
risk premiums on capital deployed have                                                    preserving the potential for long-term
                                            driver of our profitability has been,
increased dramatically.                                                                   share price appreciation when the stock
                                            and will continue to be, our fleet of
                                                                                          market recovers. The EDF joint venture
Having provided that background and         high-value plants, including our nuclear
                                                                                          also complements our option to pursue
context, let me review the performance      units, in energy markets such as PJM
                                                                                          the renaissance of nuclear power. This
for 2008. For the year, one-time events     and New York. The output from our
                                                                                          can become an exciting element of our
and special items primarily drove the       plants is largely unsold for 2011 and
                                                                                          strategic plan because this should be the
lion's share of the losses. Excluding       beyond. When the economy recovers
                                                                                          right time in history to be among the
the MidAmerican merger termination          and energy prices rise in the years
                                                                                          leaders in developing low-carbon energy
costs and other special charges, our        ahead, these plants should drive stable
                                                                                          resources such as new nuclear facilities.
2008 adjusted earnings would have           earnings growth. BGE is the second
been $3.57 per share. During the year,      cornerstone of our earnings foundation,       It's also clear, however, that the lingering
our employees continued their focus         providing stable earnings during this         recession continues to take a toll on our
on operational excellence, despite the      recessionary cycle and poised for growth      performance and that of virtually every
market upheaval. Our nuclear units          when economic activity returns to more        sector of our economy. What's encour-
generated a record 32 million megawatt      normal levels. Our wholesale and retail       aging, though, is that even in the midst
hours of electricity in 2008, and we        customer supply businesses represent          of this economic malaise we're seeing
were among the industry leaders with        the third earnings driver. These              many positive signs that the energy
a fleet capacity of 94.7 percent. Early     companies will continue to be leaders         and environmental agendas are moving
this year, we set a world record for        in the sector this year and should            in the right direction in Maryland,
a pressurized water reactor with the        contribute about a third to our overall       Washington, D.C., and across the
692nd consecutive day of operation for      earnings in 2009.                             nation. These agendas translate directly
Calvert Cliffs Unit 2.                                                                    into business opportunities and our
                                            Our key strategic priority for the year
                                                                                          company in some respects is ahead of
We're now nine months into a compre-        is to close the EDF transaction, and
                                                                                          the curve and well positioned. These
hensive strategic realignment and we        the process is on track. We've already
                                                                                          are just a few examples:
have sharply reduced our exposure to        received two key federal approvals,
commodity price risk. We have signifi-      including the U.S. Committee on                 BGE residential customers are
cantly pared back the scope of energy       Foreign Investment in the United                embracing new tools to help them
trading activities. Earlier this year, we   States (CFIUS) and the Federal Energy           manage and reduce energy usage.




                                                                                                                                    
    The utility has enrolled more            Our commercial energy customers           2008 was a year of unprecedented
    than 90,000 customers in its             are committing to energy conser-          market upheaval and extraordinary
    PeakRewardsSM program, which             vation and renewable technologies.        challenge. Our results were disap-
    was launched in May of 2008 and          Constellation NewEnergy is finding        pointing. However, we took prompt
    provides bill credits to customers       great interest in its demand response     and prudent steps to respond to the
    who select a smart thermostat or         programs, in which customers receive      exceptional circumstances. As a result,
    switch to cycle their central air        payments from grid operators to curb      we have largely regained our footing
    conditioners during times of peak        energy usage during peak periods. In      and secured an excellent partner for
    demand. BGE is also launching a full     2008, Constellation Energy Projects       our nuclear business in EDF. In a short
    suite of energy efficiency programs      & Services, our energy services unit,     period of time, we've made considerable
    to serve residential, commercial and     deployed eight new solar installa-        progress on a promising growth plan for
    industrial customers. BGE plans in       tions at customer sites in five states,   2010 and beyond.
    the months ahead to seek Maryland        including the first solar project for a
                                                                                       I would ask our shareholders to assess
    Public Service Commission authori-       Maryland state agency, the Maryland
                                                                                       Constellation Energy's long-term value
    zation and federal stimulus funding      Environmental Service. Altogether,
                                                                                       proposition and to view the 2008
    through the American Reinvestment        we own nearly five megawatts of
                                                                                       results in the context of the collapse
    and Recovery Act of 2008 to launch       distributed solar generation projects
                                                                                       of credit markets and this unrelenting
    a multi-year Smart Grid initiative       nationwide and believe this can be a
                                                                                       global economic downturn. The
    throughout the service territory.        growth area for the company if the
                                                                                       building blocks of our growth plan
    Building on the highly successful        proper federal and state incentive
                                                                                       are largely in place and will further
    results of our pilot program last        programs are put in place.
                                                                                       solidify when we close our joint venture
    summer, Smart Grid would empower
                                             The most dramatic signs of progress       with EDF. Your management team
    all customers to manage their energy
                                             on the policy front are associated with   and Board believe that Constellation
    consumption and peak demand in
                                             new nuclear, as even one-time critics     Energy's values and people remain
    response to real-time pricing and
                                             are now embracing this proven and         strong, and those two elements will
    usage information. Smart Grid
                                             safe technology as the most efficient     continue to be crucial as we face
    would also enable new applications,
                                             and effective way to address global       the future.
    including growing development
                                             climate change. These projects have
    of renewable energy and "smart
                                             the potential to create thousands
    charging" of future plug-in electric
                                             of construction jobs, hundreds of
    vehicles. BGE anticipates that over
                                             permanent plant positions and
    time these and other transformational
                                             improve grid reliability. A new unit
    initiatives would reduce peak demand
                                             three at Calvert Cliffs would generate
    by as much as 1,700 megawatts,
                                             1,600 megawatts of non-greenhouse
    significantly offsetting the need to                                               Mayo A. Shattuck III
                                             gas-emitting electricity, enough to
    construct new, more expensive power                                                Chairman, President and
                                             power 1.3 million Maryland homes.
    generation facilities. These programs                                              Chief Executive Officer
    would significantly modernize the
                                                                                       April 16, 2009
    grid, enhance reliability and directly
    address key environmental initiatives
    such as climate change.

