Message from Ana Botin 
Our purpose is to help people and businesses prosper. 
Once again, we delivered on all our promises and did so in the right way. We made excellent progress against our long-term strategic goals. 
In 2016, we lent more and improved service to our customers, earned more for our shareholders and supported our employees and communitiesin a sustainable, inclusive way. 
6 2016 ANNUAL REPORT 
Our achievements in 2016 reflect the strength and resilience of the Santander model and the efforts of all 190,000 of our colleagues: 
		We increased lending to our customers by 16 billion. 
		We continued to invest in technology, maintaining our best in class efficiency while also improving customer service. 
		We further strengthened our corporate governance and risk model and increased our Common Equity Tier 1 (CET1 Fully Loaded) capital by 3 billion euros, reaching a ratio of 10.55%, exceeding our target. 
		We maintained our position as one of the most profitable banks in the world, with an underlying return on tangible equity of 11.1%, higher earnings per share of 0.41 (up 1%) and a cash dividend per share rising to 0.17 (up 8%). Our total net asset value per share also grew in 2016 by 15c to 4.22. The market is recognizing our progress, as shown by the total shareholder return of +14% for our shares in 2016. We also worked hard to improve how we work: Our success at this time of exponential change will depend on an ever stronger culture where customers come first, uniting our banks in Europe and the Americas. By making better use of technology and collaborating more effectively across the Santander Group, we are making it easier than ever for our clients to travel, to trade and to fulfill their financial needs. Greater collaboration among countries and people is critically important to increasing prosperity for all. We made further progress in ensuring that everything we do is more simple, personal and fair. We are strengthening the links between our core markets and producing tangible benefits 
of working across the group, both for our customers and for our shareholders. 
Our strategic progress in 2016 
Our aim is to become the best retail and commercial bank, earning the lasting loyalty of our people, customers, shareholders and communities. 
Customers 
This year, we earned the trust of 4 millionnew customers, raising our total numberof customers to 125 million. We did this by improving the service and products we offer in every one of our ten core markets. 
Our 1I2I3 strategy is based on creating customer value, which leads to attractive financial returns as these customers do more business with us. 
It is not a product strategy or market share driven. And it is a profound change from the strategy Santander pursued in the past in Spain. It drives stable long terms relationships, as 1I2I3 customers consider Santander their primary bank. It hence drives stable, current account balance growth. 
In the UK the 1I2I3 strategy has fundamentally changed our bank, from relying on volatile and expensive savings customers, to real banking customers, in just 5 years. 
As the English say, the proof is in the pudding. Santander UK current account balances grew from 12 billion to 65 billion, after being stuck at 12 billion for many years, and we have gained an average of 420,000 loyal customers per year during that period. 
Within this customer group, among other financial benefits, our cost of credit is a quarter of what it was in 2011. We have also improved savings margins as these customers did more of their business with us at lower cost. 
In 2016 we have achieved all our goals 
Lending to customers 
+  16 billion Best in class efficiency 
48% 
cost to income ratio 
FL CET1 
10.55% 
(+50bps) 
Underlying RoTE 
11.1% 
Cash dividend per share 
 0.17 
(+8%) 
2016 ANNUAL REPORT 7 
Message from Ana Botin 
Santander now ranks among the top 3 banks for customer service in all but one of our 9 core countries. This was one of our key strategic goals for 2018. We achieved it in 2016. 
A circle 
190,000 
employees 
In Spain the strategy is similar but not identical, as our teams and services are able to develop relationships faster and therefore are already growing fees, in a declining market - not by raising fees but by growing the number of customers that bank with us. 
In 2016, our 32% loyal customers growth drove a 6% growth in fees, with 50% of the commercial activity concentrated in 1I2I3 customers, who buy 1.7 times more products and services than the rest. 
Similar strategies adapted to local conditions have been launched in Mexico, Brazil, Portugal and other markets, based on the same principles of adding value to our customers, transparency and excellence in service, as the way to achieve profitable growth. These principles are the core of our loyal and digital customer strategy. 
Thanks to the Groups investment in technology, the number of customers using our digital banking services rose by more than 25% in 2016 to just under 21 million. 
Santander now ranks among the top 3 banksfor customer service in all but one of our 9 core countries. This was one of our key strategic goals for 2018. We achieved it in 2016. 
Our technology is allowing us to improve customer service while ensuring our cost to income ratio, a measure of efficiency, remains among the best in our industry. 
Great products, great service and great value lead to loyal customers. In 2016 the number of people who consider Santander their primary bank rose by 1.4 million to 15.2 million. Loyal customers do more business with us, which means our fee income from value-adding services rose by 8.1%. 
We also agreed or completed three transactions which will allow us to serve more customers and continue to deliver profitable growth in the medium term in our core markets: 
 We integrated Banif in Portugal  a deal that has helped us increase the return on tangible 
equity in our Portuguese franchise to a best-in- class 13%. 
 We reached an agreement to acquire Citigroups retail assets in Argentina  enabling us to increase our market share and strengthen our franchise in a significant growth market. 
 And most recently, we agreed to purchase the outstanding 50% stake in Santander Asset Management. This will allow us to expand our asset management business and the range of our offerings to customers. 
These transactions align well with our strategy and allow us to offer better service to customers and strengthen our competitive advantage. 
People 
The talent and motivation of our 190,000 employees are the foundations for our success. Implementing a strong, common culture and purpose across the Group remains the main priority of the new management team. 
We aim to be one of the top 3 banks to work for in the majority of our geographies. We have now achieved that goal in four of our ten core markets. 
Our annual employee engagement survey isa valuable tool in listening to the views of our people and ensuring we take action to improve. The results this year show we are on the right path, as more than three quarters of our teams support our Simple|Personal|Fair culture. 
We have a clear plan for continuous improvement in corporate behaviors and our remuneration will be significantly determined by our progress. 
During 2016 we completed a significant restructuring process in a number of business areas to further improve our efficiency and operational excellence. While these processes are never easy, we have done our best to manage the exits of some of our people in a way that is fair to all, investing for the future. 
Serve 
125 
million customers 
Deliver for 
3.9 
million shareholders 
Helping 
1.7 
million people 
8 
Our critical mass and retail-commercial model, focused in 10 markets in Europe and the Americas, has allowed us to deliver more predictable earnings than our peers across the cycle, even in adverse macro- economic conditions. 
Communities 
While delivering for our teams and on our financial and commercial targets is essential, making sure that we achieve our goals in the right way is even more important, as it ensures sustainability and continuous customer value creation. 
This means supporting a culture that rewards the behaviors we believe to be right, encouraging colleagues to speak up, actively collaborate and embrace change. 
It means building a business that aims high, that our customers, people and shareholders can rely on in the long term. And it means taking our responsibilities to the communities we serve seriously. 
This year Santander was recognized by the Dow Jones Sustainability Index as the best bank in Europe for our commitment to sustainability, contribution to social progress and for our protection of the environment. 
I am extremely proud of the work we have done in 2016 to support our communities and there are a number of initiatives I would like to highlight: 
		In 2016 we further increased our support for universities across all the markets in whichwe operate. We are now in partnership with 1,200 universities, providing more than 35,000 scholarships and grants. 
		Across Latin America we have supported 250,000 micro finance projects. 
		More than 60,000 employees took part in volunteering programmes during the year. These ranged from financial education for students, the elderly and people on low incomes in Europe to support for childhood education in Brazil, where 4,000 employees worked to improve learning in 214 schools. 
		In Spain, our coaching and mentoring programmes helped the disadvantaged,the socially excluded, and victims of gender violence to improve their skills and find jobs. 
Shareholders 
Our critical mass and retail-commercial model, focused in 10 markets in Europe and the Americas, have allowed us to deliver more predictable earnings than our peers across the cycle, even in adverse macro-economic conditions. 
We proved this yet again in 2016 as we increased our revenues with strong growth in fee income and improvements in credit quality. Together with our focus on costs, this delivered an increase in our attributable net profit of 4%, to 6,204 million. 
Our local operating performance was even better, as excluding one-off items and currency movements, our underlying profit before tax in constant euros rose by 12%. 
The strength of our business model and our ability to generate profits year after year are starting to be recognized by the European Central Bank. In December, following its Supervisory Review and Evaluation Process (SREP), it decided to reduce the amount of capital we are required to hold as a proportion of our assets. 
Strong corporate governance is at the coreof prudent risk management. It is critically important to ensuring the sustainability of any business. 
In April the Group held the first meeting of its International Advisory Board (IAB), a group of CEOs and leaders with expertise in strategy, technology and innovation. The IABs focus is on digital transformation, cyber security and how to apply new technologies to improve service and do so efficiently. 
And in September we were delighted to announce the appointment of Homaira Akbari as a new independent member of our Board of Directors. Homaira is a distinguished scientist, technologist and business leader. She has deep experience in developing and implementing the most advanced technology at scale and will help us further advance our digital transformation. 
Banco Santanders International Advisory Board 
Chairman 
Mr Larry Summers 
Former US Treasury Secretary and President Emeritusof Harvard University 
Members 
Ms Sheila Bair 
Former Chair of the Federal Deposit Insurance Corporation and President of Washington College 
Mr George Kurtz 
CEO and co-founder of CrowdStrike 
Mr Charles Phillips 
CEO of Infor and former Presidentof Oracle 
Ms Marjorie Scardino 
Former CEO of Pearson and member of the Board of Directors of Twitter 
Secretary 
Mr Francisco DSouza, 
CEO of Cognizant and director of General Electric 
Ms Blythe Masters 
CEO of Digital Asset Holdings 
Mr Mike Rhodin 
Senior Vice President of IBM Watson 
Mr James Whitehurst 
CEO of Red Hat 
Mr Jaime Perez Renovales 
9 
Message from Ana Botin 
The Santander business model is built upon strong foundations 
1Serving 125 MM customers financial needs, with critical mass in 10 markets with c.1 Bn people drives profitable growth 
2Geographic diversification drives predictability of earnings=less capital 
3Subsidiary model with strong culture of working together drives efficiency and service excellence 
Her appointment strengthens the Boards international and technology expertise and brings the proportion of women on the Board to 40% - one of the highest in international banking. 
I would like to take the opportunity to thank Angel Jado for his dedication and outstanding contribution to our Board for many years, and wish him every success for the future. 
And I would like to recognize the hard work and commitment of all our Group and Subsidiary banks Board members, and thank them for their continued support throughout the past year. 
Our unique opportunities for growth 
I am proud of our teams progress this pastyear. In 2016, we delivered strong operational performance in all our businesses and at Group level, as well as reaching or exceeding our cost of equity in 95% of the groups investments. But we can do much more and much better. 
The Santander business model is built upon strong foundations, well suited for the world ahead of us: 
		We serve the financial needs of 125 million customers. We have critical mass in 10 markets with one billion people, which drives profitable growth. 
		Geographic diversification leads to more predictable earnings and means we require a less capital intensive model than our global peers. 
		Our subsidiary model allows us to be closeto our customers and our strong culture of working together drives efficiency and service excellence. The proof is our predictability. We are one of only three major international banks to remain profitable throughout the crisis. 
The European Banking Authoritys stress tests this year show Santander to be the most resilient bank among our peers. 
We have increased our core capital by 17 billion and have grown our profits by 40% over the past three years. 
We have paid dividends consistently for more than 50 years. 
And we generate some of the most stable and growing earnings per share among our peers. 
But what matters looking forward is our great potential for organic growth. This is why I am confident Santander will be one of the winning companies over the next decade. 
Looking forward... 
The UKs vote to leave the EU and Donald Trumps victory in the US presidential elections confounded most expert predictions in 2016. And we have considered these and other potential unexpected international outcomes as we develop our plans. 
For the next few years the effect of Brexit on our UK business will be as a consequence of anticipated slower growth in the economy as well as the weaker sterling effect on translation of our earnings. 
However our strong balance sheet of prime mortgages and primary banking relationships allows us to be confident we will continue to deliver around or above our cost of capital. 
Brazil should return to positive growth, after two years of recession, thanks to a soundset of economic reforms. It is a countrywith 220 million people and a large andgrowing middle class. Spain is expected to remain at the forefront of Europes recovery. The UK will continue to play a key role in the global economy. And Mexicos government has shown it can manage through challenging times. 
10 
OUR EIGHT CORPORATE BEHAVIORS 
Talk straight 
Truly listen 
Keep promises 
Bring passion 
Support people 
Show respect 
Actively collaborate 
Embrace change 
Our performance the past year, for example, in Portugal (399 million profit, +33%) or Argentina (359 million profit, +52%) shows that we cando very well for our customers and shareholders even when the macro conditions are not 
ideal. But we would always rather have the macroeconomic winds behind us. 
Santander Brazil earned 1.79 billion in profits and is the single largest earnings contributor to Group. For the last two years, we delivered a return on tangible equity of 14%, despite 
an adverse economic environment: net profit to the Group grew by 15% in 2016 and did so in a sustainable way, adding 500,000 loyal customers and 2 million digital customers. 
In the USA we have made regulatory progress, as we committed, we have improved how we manage the business and reduced risk. 
At SBNA, we have lowered the cost of deposits, improved service and efficiency. 
Our retail and commercial Bank in the USA- which represents 5% of our Group capital invested - will deliver significant growth over the next few years and I remain confident it will deliver considerable value to shareholders. 
...through active collaboration 
Our model of local subsidiary banks and teams, together with our ability to collaborate across countries gives us our unfair advantage. In a world which we anticipate being increasingly insular, this is a winning combination. 
It leads to better outcomes for customers and value creation for shareholders. 
We already do this better than most, as our best- in-class cost-to-income ratio demonstrates. But we can do more. 
Allow me to share some examples: our new CRM (Customer Relationship Management) 
tool in Poland was first developed in Chile and then taken and improved by the UK. It has been deployed through our internal open services model and is now being implemented in Mexico. 
Santander Wallet is a single global solution for around 400,000 of our commercial clients.It allows them to use a single wallet for all payments and channels, and offers additional, real-time, value added services to help our customers build their business. It has been launched in Brazil and Spain and will soon be available in Mexico and Chile. 
Our microcredits programme in Brazil, supported by an equally strong programme of financial education, has helped 129,000 entrepreneurs to start and grow their businesses. 
We are now expanding microcredits to Mexico to bring many more people into the financial system and pursue our goal of sustainable, inclusive growth. 
As a management team, we are totally committed to embedding our behaviors and leading by example. 
Commercial and financial performance is a given. 
But what will make us a winner is a culture that allows people to speak up, to embrace change, to accept diversity of all types, so that we can fully realize the potential in our teams and in our markets. 
As a management team, we are totally committed to embedding our behaviors and leading by example. Commercial 
and financial performance is a given. But what will make us a winneris our culture. 
11 
Message from Ana Botin 
PRIORITIES FOR 2017 
Increase the number of loyal customers by a further 1.8 million to 17 million 
Raise the number of digital customers to 25 million 
Broadly stable cost to income ratio 
Continue to strengthen our capital 
Increase value for our shareholders 
...and the trust of 125 million customers, in both developed and developing markets. As I mentioned, we serve 125 million customers in ten core markets in Europe and the Americas that are home to more than 1 billion people. 
We have critical mass in all our markets. We have scale and we are in the right places. This represents a huge potential for value creation. 
Our biometric technology, a joint project between Mexico and Brazil has made banking easier and more secure for 6 million customers in the past year. In the UK we have reducedthe time it takes our customers to complete a mortgage application from up to 3 hours to less than 40 minutes, in a heavily regulated process. 
We are collaborating with some of the most innovative FinTech startups through Santander Innoventures. And in H1 we will launch a new platform for Openbank, a digital bank in Spain which serves more than a million customers, with just 100 people and is already profitable. 
Looking forward, through active collaboration, we have a unique opportunity for growth. To deliver on this opportunity we will invest in our people and in better use of technology, and 
we will work across the group to improve the customer experience and our efficiency. 
Our strategic priorities and goals for 2017 
We have clear goals for 2017: to increase our number of loyal customers by a further 1.8 million to 17 million, and keep developing our value-adding services. 
We will continue to invest in technology to raise the number of digital customers to 25 million, while improving service and efficiency, aiming for a broadly stable cost to income ratio. And we will increasingly do it working across geographical boundaries. 
Our aim is to continue to grow our capital by another 40bp whilst increasing our earnings and dividend per share and continuing to grow the value of our company, as measured by tangible net asset value per share. The delivery of our 2017 and 2018 goals will keep adding tangible net asset value to our shares. 
Conclusion 
2016 has not been an easy year, but we have delivered on our promises, and done so in the right way. Once again, we lent more, we earned more, and we became a better bank on every significant measure. 
In a changing and complex environment, for the first time in years, we expect positive GDP growth in all of our markets in 2017. 
The financial system plays an important rolein supporting economic growth. And the Santander model is based on supporting this growth by maintaining appropriate capital levels, strong corporate governance and prudent risk management. 
These are uncertain times. Volatility is growing and growth, overall, might slow. Technology is creating disruption. Automation is threatening jobs. In the short term, we need to retrain people, to encourage lifelong education so we 
12 
Going forward, we have many opportunities for profitable growth in Europe and the Americas, in an environment we anticipate will be volatile but generally better than 2016 
can bring everyone with us in this new wave of growth, and ensure it is sustainable. 
We have reflected on these trends, what they mean for us, and how to build a businessthat delivers in a sustainable way, with great performance for shareholders but that also cares about making a difference. 
At Santander we are in an extraordinary position to help. Lets start with our 125 million customers. Add in their family members. Then all the businesses we serve, with employees ranging from a few to tens, even hundreds of thousands. 
Every action we take to enable inclusive, sustainable growth has a powerful multiplier effect which will help the lives of millions of people. That is the power of our model. 
In my first letter to you, only 2 years ago, I set out our strategy. Maintaining our traditional strengths, and foundations, we embarked on a profound process of change. 
It is the sort of change that is not fully reflected in the news that generates media headlines. It is not just about acquisitions or appearing at the top of the rankings. 
Our transformation is global and goes beyond these metrics. It is mostly about how we organize ourselves, how we behave, to succeed in a world changing at exponential speed all around us. 
A more diverse, multicentric world, where being local is a must. 
And at the base of our transformation is a culture of being local in each one of our markets while also encouraging a shared way of doing things that is Simple and Personal and Fair; this 
culture binds us together across our 10 markets, fosters innovation and attracts the best talent. 
At our Investor Day in London in September 2016 we set out our 2025 vision: to be an open platform for financial services. Importantly,as we transform the Group to succeed in the medium term, we are delivering today. 
As I said when we announced our earningsfor 2016, going forward, we have many opportunities for profitable growth in Europe and the Americas, in an environment we anticipate will be volatile but generally better than 2016. The key to our success for 2017 and beyond will be an ever-stronger collaborative culture across the Group and a shared purpose to help people and businesses prosper. 
I am confident Santander will continue to deliver because of the 190,000 people who work hard and work together every day, and to whom I would like to say thank you. 
And to all of you, to our customers, to our shareholders, our communities, thank you for your trust. 
With your continued support, the best is yet to come. 
Ana Botin 
Group Executive Chairman 
I am confident Santander will continue to deliver because of the 190,000 people who work hard and work together every day, and to whom I would like to say thank you 
