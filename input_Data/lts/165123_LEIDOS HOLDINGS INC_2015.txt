Dear Fellow Stockholders,

This past year was an important one for Leidos. We made tremendous progress removing hurdles to our growth
and focusing on a philosophy of accountability and responsibility that runs deep in our history. We have
strengthened this foundation by implementing operational efficiencies and adding world-class talent to our
team  all aimed at enhancing performance and delivering results for our customers.
Streamlining our business portfolio was the obvious place to start this journey, and we achieved greater focus
through our efforts over the past months. We clearly defined the key markets where we deliver the most value
and crafted plans to address them. Our core C4ISR, cyber, data analytics, systems engineering, and agile
software development capabilities will propel our growth and help us meet the rapidly evolving needs of our
customers. They will also enable us to achieve cross-market innovation across our entire customer set 
commercial and government, international and at home.
While we made a lot of progress this year, the financial performance in one of our sectors was below expectations, and we incurred a large impairment charge as a result. In light of this and to improve our prospects for
the future, we made some difficult decisions, such as divesting a number of businesses, reducing our cost
structure, adjusting our fiscal year schedule, and freezing pay levels for your executive officers for the
 upcoming year.

 As we continue our efforts, our talented employees will make all the difference. I am pleased to report that the
innovative culture that set the foundation of our companys more than 45-year history is still alive and well today
at Leidos. I see it every day in our employees. The highly technical and innovative services and products that
Leidos provides depend upon these individuals  world-class professionals in key technical fields that require
high levels of training, skills, and in many cases, security clearances. Their expertise and experience helps them
solve some of the most daunting problems facing our world. To help attract and retain this talent, we've
launched initiatives to help make Leidos one of the best places to work, with more planned in the year ahead.
I am proud of what we have accomplished so far. The rest of the leadership team and I are fully committed to
keeping the momentum going as we move forward.

Chairman and Chief
Executive Officer
Roger Krone
