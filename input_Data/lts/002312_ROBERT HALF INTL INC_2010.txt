To Our Stockholders 
Robert Half International's financial results rebounded in 2010 as the global economy finally 
began to improve. The Great Recession, as it has come to be known, officially ended in June 
2009 and by nearly all measurements was the most severe economic setback we have seen in 
the post-World War II years. Our experienced corporate staff and dedicated field organization 
performed exceptionally well during this challenging period, and we believe we are now well 
positioned to participate in any unfolding recovery and expansion. 
Last year's revenues totaled $3.2 billion, up 5 percent from those reported in 2009. Net income 
of $66.1 million was 77 percent higher than the year ago amount. Diluted income per share of 
$0.44 compared with $0.24 in 2009, an 85 percent gain. Our financial position remains solid. 
We generated $137 million of free cash flow in 2010 and ended the year with $315 million in 
cash and cash equivalents and virtually no debt. 
Preliminary data show that U.S. real gross domestic product grew 2.8 percent in 2010.That 
compares with a restated year earlier decline of 2.6 percent. Still, the economy gained strength 
unevenly through the year. For example, growth rates at the beginning and end of 2010 exceeded those in the spring and summer months. Midyear fears that the European sovereign debt 
crisis would retrigger a global recession proved to be unfounded. Positive economic momentum 
was restored late in the year. 
Labor markets improved only modestly during 2010. By historical standards, the number of 
unemployed remained high throughout the year, and the jobless rate has just begun to inch 
down in recent months. The U.S. economy added more than 1.1 million private sector jobs in 
2010.That was the most since 2006, but it was just a fraction of the employment lost during 
the recession and its aftermath. 
Demand for our services showed a clear strengthening trend in 2010.Yearoveryear quarterly 
percentage changes in companywide revenues for the first through fourth quarters were: 
10 percent, 3 percent, 13 percent and 15 percent, respectively. Looking back, we can identify 
the second quarter as a key inflection point. It was during that period that all of our businesses 
--- temporary staffing, permanent placement and Protiviti --- began to show positive year over 
year revenue gains. The trend was confirmed with the acceleration of revenue growth in the 
third and fourth quarters of 2010.We are encouraged that, thus far, the current rebound in our 
financial results is among the fastest post downturn recoveries our business has produced in its 
63year history. 
With the help of every Robert Half International employee, particularly our experienced field 
and corporate services leaders, we persevered through the recent period of economic adversity. We did so while positioning the business to grow. One important investment we made was 
adding staff in select branch locations to accommodate both current and anticipated client demand. This is a strategy that served us well following the preceding downturn. During the early 
months of recovery back in 2003, we disproportionately grew our headcount, which positioned 
us to be able to do more business as conditions improved. We hope to benefit similarly in the 
current environment. 

Financial Condition 
Robert Half International's financial position remains solid, in large part because of the company's proven ability to 
generate cash throughout all phases of a business cycle. Last year, we produced $176 million in cash from operating activities, bringing the cumulative five-year total to $1.7 billion. Capital expenditures in 2010 were $35 million, 
equivalent to 1.1 percent of revenues. This is well below the 10year average of 2.0 percent of revenues. A significant 
portion of capital outlays went toward relocating our Northern California corporate services location to a new 
facility in San Ramon, Calif., a move which, after a transition period, should reduce our rental expense significantly. 
The move also enabled us to consolidate multiple office buildings into a single location, affording us greater efficiency, 
improved collaboration and room for expansion. 
We anticipate current year capital spending to increase to a range of $50 million to $55 million. A portion of those 
outlays will come from rolling over amounts that were budgeted, but unspent, in 2010.We also will be investing in 
enhancements to our information technology (IT) infrastructure worldwide. 
Last year's free cash flow was $137 million, and the five-year cumulative total was $1.3 billion. We have a long 
history of returning cash to stockholders. We have accomplished that over the years by board approved share 
repurchase authorizations since 1997 and by paying cash dividends starting in 2004. Some $96 million of last year's 
cash was used to make open market purchases of approximately 3.7 million shares of our common stock. Authorizations covering the purchase of an additional 11.4 million shares are in place; we expect to continue to repurchase 
shares depending on market conditions. 
Our board declared a $0.13 per share cash dividend in last year's first quarter. The $0.52 per share annual distribution totaled $77 million. The board recently lifted the quarterly payout to $0.14 per share. The quarterly dividend 
has been increased yearly since the initial payment in 2004 and has increased at an average annual rate of 13 per 
cent over that seven year period. 
Robert Half International is a service business and, therefore, it is not capital intensive. It requires few fixed assets 
and no inventory. Accounts receivable, however, is an important asset to which we pay close attention. That balance 
totaled $423 million at yearend, or about one third of our total assets. Despite the difficult economy over the past 
few years, we've seen no change in our ability to make timely collection of receivables. Our allowance for uncollectable accounts and other adjustments to receivables proved to be more than adequate throughout the financial crisis, 
the recession that followed and in last year's incipient recovery. 
Our yearend cash and cash equivalents balance of $315 million provides us with stability and financial flexibility. 
Besides the comfort it gives us during stressful economic times, our liquidity allows us to capitalize on growth opportunities as they arise. We continue to concentrate on expanding the business organically; we made no material 
acquisitions in 2010.The focus on internal growth relies on our strong belief that the global markets for specialized 
staffing services are underpenetrated. Consistent with that belief, we proactively launched growth initiatives last 
year. These included significant investments in Robert Half Technology, our information technology staffing unit. This 
division addresses a large and growing sector of staffing services. We believe that our solid reputation and strong 
relationships with clients' finance and accounting departments provide us with a good foothold, and we see a clear 
opportunity to strengthen our position in IT staffing. We also added to headcount in selected Accountemps and 
OfficeTeam offices, generally satellite locations, where attrition during the recession left us understaffed. 

Divisional Highlights 
The Robert Half International name has become synonymous with personnel recruitment in the accounting and finance specialties since the company's founding in 1948. Our three accounting and finance staffing divisions remain the backbone of the 
company; they produce more than 50 percent of our annual revenues. In recent decades, we have expanded the business by 
internally developing other professional staffing divisions, by entering overseas markets and by launching Protiviti. 
Accountemps is our largest division, with 2010 revenues of $1.2 billion, or 38 percent of the companywide total. Its full year 
revenues were essentially unchanged from the prior year, but the final two quarters showed accelerating growth. Accountemps 
enjoyed good revenue momentum as we entered 2011. 
Robert Half Management Resources focuses on higher-level accounting and finance projects, often of longer duration. Its 2010 
revenues were $387 million, or 12 percent of the companywide total, and were essentially unchanged from the prior year. As 
with Accountemps, this division showed a much stronger second half revenue gain. Robert Half Management Resources also has 
evolved to play an important role in providing staffing support to Protiviti on appropriate engagements. 
Robert Half Finance & Accounting, our original business, provides specialized permanent placement services. It produced $221 
million in revenues last year and accounted for 7 percent of the companywide total. This unit's revenues increased 22 percent 
from the prior year. That was an atypically strong performance at this stage of the economic cycle, which we attribute to the 
severity of personnel reductions during the recession. Employers simply cut too deeply and found it necessary to begin restoring staffing to levels needed to meet awakening business demand. The extent of the layoffs meant the ranks of the unemployed 
included highly skilled employees rarely available in the labor market. We actively made our clients aware of the situation, and 
many seized the opportunity to hire this great talent. 
OfficeTeam is our administrative staffing unit. Its $630 million of revenues last year grew 13 percent from the prior year and 
represented 20 percent of the companywide total. This division began showing strong results as early as the second quarter of 
2010.That pattern is consistent with past experience, whereby administrative staffing demand recovers before finance and ac 
counting segments following a recession. 
Robert Half Technology, our information technology staffing business, had revenues of $336 million, equivalent to 11 percent 
of the companywide total and 9 percent ahead of the prior year. Demand for IT staffing has bounced back nicely as clients 
have begun to spend on projects deferred or curtailed during the recession. Looking beyond the near term, we have identified 
this unit as an important future growth vehicle. It is clear that over the past decade or so, Internet and website activities have 
become critical to the operations of our mainstay small to midmarket clients. For many of these customers, it is uneconomical 
to hire fulltime IT staff, and relying on temporary staffing support is a better option. We have begun to increase our investment 
in Robert Half Technology with the aim of achieving a leadership position in this growing business sector. 
International staffing revenues of $805 million increased 6 percent from the prior year and were 29 percent of total staffing 
revenues. Translating stronger foreign currencies into U.S. dollars had the effect of reducing the reported yearly growth to 4 percent. Trends in our non-U.S. staffing businesses generally tracked those of their domestic counterparts, with quarterly growth 
increasing as the year unfolded. By geography, our Asia-Pacific operations were strong, particularly those in Australia, where 
demand from industries related to natural resources was healthy. Permanent placement activities in non-U.S. markets, especially 
in Europe, have grown to become a bigger percentage of staffing revenues than is the case in the United States. Last year, non 
U.S. permanent placement growth matched the domestic increase. Non-U.S. markets now produce almost half of companywide 
permanent placement revenues, nearly twice the percentage recorded just five years ago. 
Protiviti 
Protiviti is our risk and business consulting and internal auditing unit that was established in 2002. Its 2010 total revenues of 
$386 million were 12 percent of the companywide total and were essentially unchanged from the prior year. Revenues in the 
United States were 72 percent of Protiviti's total and grew 3 percent. International activities produced the other 28 percent of 
revenues and slipped 5 percent from 2009. Protiviti significantly reduced its operating losses from the prior year and reported 
profits in the last two quarters of 2010. 
Protiviti's global revenue trend has shown itself to be similar to our staffing operations in its sensitivity to swings in the economy. 
However, profitability in recent periods has proved to be more exposed to the effects of the economic slowdown. In 2010, we 
kept Protiviti's highly skilled workforce largely intact, even in the face of uneven post recession demand and aggressive competitive pricing. We did this knowing that we would need key personnel in place to help us participate fully in a stronger demand 
environment. 
Protiviti has evolved considerably since its launch nearly nine years ago, particularly its business mix. While Sarbanes-Oxley Act 
(SOX) compliance work drove this division's annual revenues in its early years, revenues from SOX work fell to less than 20 
percent of last year's total. As SOX activity has ebbed, Protiviti has grown its consulting business through its experts specializing 
in risk, advisory and transaction services. 
More recently, the global financial crisis has triggered a new round of financial regulatory oversight. In the United States, the 
DoddFrank Wall Street Reform and Consumer Protection Act became law in July 2010. It has been described as the most 
sweeping change to financial regulation in the United States since the Great Depression. Similar reform measures have been 
introduced around the world, and Protiviti's financial services industry practice is well positioned to assist clients in complying 
with these rules and regulations. In addition, Protiviti's information technology practice has successfully expanded its consulting 
services in IT security and privacy, IT application controls, and IT infrastructure management. 
Another important shift has been Protiviti's increased use of variable cost project workers to support internal Protiviti staff on 
engagements. Robert Half International is uniquely able to supply Protiviti with a deep reservoir of project and consultant talent 
through its staffing units. Access to this flexible talent pool has emerged as a competitive differentiator for the business. 
Our People 
Our employees are critically important to our success. We believe we have among the most experienced teams in our industry. 
We work hard to hire and retain our best people so they can continue to provide outstanding service year after year. Our 
staffing and recruiting professionals focus on finding candidates with the best skills and work culture fit for our clients. We know 
that clients want choices when they engage a staffing firm. We are able to offer customers a wide selection of talent to meet 
their needs. 
New ways of recruiting candidates and applying for jobs online continue to emerge. These include specialized job boards, as 
well as networking and social media sites. We use these platforms to expand our own business networks, and our clients have 
benefited. We also invest heavily in online and job board advertising, and we have a very visible presence on LinkedIn, Facebook 
and Twitter to extend our reach. But we believe it is the personalized service Robert Half offers that provides us with a key 
competitive advantage. Our professionals recognize the importance of the one-on-one attention they devote to their clients 
and job candidates, including our ability to provide exactly the right match for open jobs. We believe this attention to detail is 
what differentiates our service offerings. 
Looking Ahead 
Robert Half International accomplished much in the first decade of the new millennium. We opened 43 new offices; we expanded our geographic footprint by entering 14 new countries, bringing the total to 23; and we launched Protiviti, our business 
and risk consulting and internal audit subsidiary. 
The near-term business backdrop for our services is positive, in our view, particularly if the recovery that was under way last 
year develops into a sustained expansion. Historically, our industry has performed well following each of the 11 recovery periods that have followed recessions in the post-World War II years. The severity of the recent downturn afforded many employers with a new appreciation for the value of businesses like ours. Staffing firms demonstrated that they could infuse flexibility 
into an employer's labor cost structure by introducing temporary and project professionals. It may turn out that wider adoption 
by employers of flexible staffing models, including the use of project professionals at higher skill levels, could be a legacy of the recession. 

Robert Half International is well positioned to participate in any industry wide recovery. Despite the fiscal pressures that ac 
companied the downturn, we consciously opted to keep our domestic and international office networks largely intact. We also 
retained our best people and, recently, have increased headcount selectively. We continued to support our leading brand names 
through marketing activities, and ongoing investment in technology to increase the productivity of our field operations was uninterrupted. In brief, we have the resources in place to satisfy any growing demand. 
Longer term, we continue to believe that we have underpenetrated our mainstay staffing market --- providing small to midsize 
companies with specialized professional personnel. We interact with a small fraction of the hundreds of thousands of middle 
market customers that represent our primary target market in the United States and, especially, internationally. In addition, there 
is an important demographic story here. We are now entering a period when we will see a growing number of baby boomer 
retirements globally, which could create talent shortages. Finding talent for our clients is what we do best. 
We look for Robert Half Technology to also be a contributor to our short and long-term growth. The market opportunity 
that this business provides is measured in tens of billions of industry wide revenue dollars annually. It is no longer just the largest 
companies that have a strong need for IT support and IT development professionals. Over the past several years, our mid 
market clients have built up their IT infrastructures and web presence. Many of these businesses routinely have a need for skilled 
talent to help them maximize the value of their IT investments. Clients often seek assistance in areas such as web analytics to 
determine who visits their websites, to evaluate traffic patterns and to gather competitive intelligence. Likewise, many are keenly 
interested in how web developers and social media specialists can help them gain market share. IT staffing is currently the 
hottest segment of the staffing market, and we have invested heavily to participate more fully in its growth. 
It seems likely that interest by U.S. and international regulators in financial reform will continue in the short and long term. 
This trend should provide potential revenue sources for our accounting and finance staffing divisions, and for Protiviti. We are 
uniquely able to combine the work of Protiviti's consultants and our specialized staffing divisions in a blended effort to capture 
new business. 
No part of our 2010 results or our vision for the future would be possible without the contributions and commitment of our 
employees. Our field and corporate management teams are pivotal to the success of Robert Half International year after year. 
We greatly appreciate their talent and leadership. 
We would also like to thank the members of our board of directors for their guidance and you, our stockholders, for your 
ongoing support. 