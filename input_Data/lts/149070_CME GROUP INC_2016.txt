Our record 2016 performance reflected our ability to
meet clients increasing demand for risk management in
a year marked by financial market surprises, including
Brexit, the U.S. presidential election, oil price volatility
and concern about growth in China. These and other
events marked some of the moments that clearly defined
the growing importance of CME Group products to customers
hedging and investment strategies worldwide.
Our volumes surged in response, driven by participants
in financial, energy, agricultural and metals markets
globally  demonstrating that CME Group is where the
world comes to manage risk.
Full-year 2016 annual average daily volume reached
a record 15.6 million contracts, including records in interest
rates, energy, agricultural commodities, metals, total
options and electronic options. Annual revenue increased
8 percent compared with 2015. This, coupled with operating
expenses being up only 4 percent, drove net income
of $1.5 billion, up 23 percent compared with 2015, and
diluted earnings per share of $4.53, up 23 percent.
Our strong results in 2016 are validation of the effectiveness
of our operational discipline and growth strategy
in an era of accelerating change.
Maximizing core business growth globally
In a world swept by unpredictable economic and geopolitical
changes, the global demand for our diverse
risk management products and services continues to
increase. The backdrop for our interest rate product line
has become more interesting as the U.S. Federal Reserve
becomes more active. Structural changes in the energy
markets have helped fuel growth across our energy
complex. Further, the regulatory environment in the
United States is improving while there is still regulatory
uncertainty in Europe.
In this environment, the global relevance of our products,
the ability of our customers to access our markets
around the clock, and the continued electronification of
our industry-leading options franchise have supported
growth outside the United States at a faster rate than
within. Both here and abroad, we have worked more
collaboratively with our market participants to launch
meaningful products that solve customer challenges.
In particular, our most successful new product ever
has been Ultra 10-Year Treasury Note futures, which
launched in January 2016 and traded more than 17 million
contracts over their first year. Product extensions
with significant impact were the S&P 500 and E-mini
S&P 500 Wednesday Weekly options, which averaged
more than 50,000 contracts per day in just one quarter
since launching in late September.
We also launched CME Bloomberg Dollar Spot Index
futures, S&P 500 Total Return Index futures and S&P
500 Carry Adjusted Total Return Index futures. Further,
we began clearing the first interest rate swaptions trades,
which are designed to help transform the interest rate
swaps markets by offering customers greater capital
efficiencies.
In addition, our international product suite was
expanded with regionally specific products such as
European wheat, aluminum futures and E-mini FTSE
emerging index contracts, which appeal to risk management
needs unique to particular geographies.
In 2016, approximately 24 percent of our electronic
volume, which translated to more than 30 percent of our
trading revenues  as well as 50 percent of our market
data revenue  were reported as coming from outside
the United States.
Diversifying our business and revenue
In addition to our core derivatives business, we have
another key segment that offers clients a variety of
market data services for futures, equities and cleared
swaps markets. This includes our S&P joint venture with
McGraw-Hill  combining the capabilities of the index
businesses of S&P and Dow Jones.
Further, in 2016 we launched E-mini Russell 1000
futures, and are planning to launch Russell 2000 futures
in 2017. This is the result of a licensing agreement we
have with FTSE Russell to better help investors around
the world manage equity index exposure. By developing
a range of equity index products based on FTSE Russells
key benchmarks, we offer the margin efficiencies of
trading multiple indexes on one platform and through a
single clearing house.
Delivering capital and cost efficiencies
Regarding the regulatory environment that directly
impacts the futures industry, it is clear that present
levels of regulation have made U.S. markets attractive
worldwide. Because of Dodd-Frank, transactions in overthe-
counter (OTC) markets that were formerly private
have been pushed onto cleared platforms like ours at
CME Group. Looking ahead and considering current
trends, we expect less regulation in the future.
Since the OTC clearing mandates began in 2013,
there has been greater customer participation and a
gradual increase in the number of firms holding large
exposures in futures. We expect capital efficiencies and
centralized clearing to continue to be important for our
clients globally.
As an essential part of our efforts, we continue to
introduce tools and services to assist customers with
portfolio margining. At year-end 2016, 40 unique marketplace
participants utilized CME Groups portfolio
margining services.
One example of a new tool is CME CORE, which is
an interactive margin calculator that enables clients to
optimize their capital by providing insights on margin
requirements prior to trading.
Also during 2016, CME Clearing partnered with
clearing member firms to increase usage of compression
services, which reduces notional outstanding and,
therefore, facilitates more efficient use of capital.
Further, within the past five years, we introduced
multilateral compression for our cleared swap customers
through a partnership with TriOptima, a NEX Group
company. We also have added trade reporting services in
the United States, Europe, Canada and Australia. During
2016, we cleared swap transactions with a notional value
of more than $29 trillion.
We are pleased that, in recognition of our ability to
meet the rapidly evolving risk management needs of our
customers, we were named Exchange of the Year in
2016 by GlobalCapital as well as Best Futures Exchange
by both Markets Media and Risk Magazine.
Focusing on innovation, enhanced execution
and returning capital to shareholders
Through CME Ventures, we have made minority investments
in emerging technology companies. Over the long
term, their innovative products or services could have
an impact on CME Groups key business drivers and the
broader financial services ecosystem.
Innovations in new products and services, in operational
structures and systems, and in business execution
have helped extend CME Groups overall success
and accelerate earnings growth and cash flow. All these
strengths, plus expense discipline, supported our ability
to return capital to shareholders.
We declared dividends during 2016 of $1.9 billion,
including the annual variable dividend for 2016 of
$1.1 billion, which was paid in January 2017. We are continuing
to move forward, and announced a 10 percent
increase in our next regular quarterly dividend to
66 cents per share. Cumulatively, the company has paid
a total of more than $7.5 billion in quarterly and variable
dividends since adopting the annual variable dividend
structure in the beginning of 2012.
All of our efforts are designed to increase the returns
to shareholders of CME Group by expanding strategically,
operating even more efficiently and continuing to serve
our customers worldwide as they navigate uncertainty
and pursue opportunities for growth.
TERRENCE A. DUFFY
Chairman and Chief Executive Officer
March 10, 2017
