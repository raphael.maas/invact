Growth
Purpose
Passion

to our shareholders:
With this years annual report, we present our vision for the
new decade ahead. It is a vision for a smarter, better world
one where individuals, markets and countries have unprecedented
access to the knowledge and insights needed to
adapt, grow and prosper. In the pages that follow, we share
the stories of how we at McGraw-Hill are working with
our partners and customers around the globe to build this
future together.
Individually, these stories span the breadth of our business
from the credit ratings and investment services of Standard &
Poors, to the financial data and analytics of Capital IQ, to
the pedagogical and professional programs of McGraw-Hill
Education, to the information services of brands such as
J.D. Power and Platts.
Taken together, these stories form a portrait of a company
on the cutting edge of globalization and digitization. At a
time when these forces are reshaping the economic landscape,
we are providing information and knowledge for the
common good in ways never before possible. Through this
commitment to innovation, the employees of McGraw-Hill
are promoting new opportunities for growth at every level.
Academic growth for students. Career growth for workers.
Economic growth for markets and nations. And above all,
strong and enduring financial growth for you, our shareholders.
renewed momentum for growth
I am pleased to report that your company ended 2009 with
renewed momentum after successfully managing through
a period of unprecedented turbulence in the global economy.
While diluted earnings per share fell 7.2 percent for the year
as a whole, they rose an impressive 43.2 percent in the
fourth quarter. In January, we announced plans to resume
our share repurchasing program and increase our annual
dividend for the 37th consecutive year. Since 1974, our
annual dividend has grown at an average compound rate of
9.9 percent. And since 1996, we have returned approximately
$9.4 billion to shareholders through dividends and
stock buybacks.
After a strong finish to 2009, we enter 2010 with many
reasons for confidence. We anticipate a year of strong growth
as we innovate digitally around the world. With the continuing
recovery of the global economy, we believe a smarter,
better world will need the financial, education and business
information services we provide more than ever. In all three
of these key markets, the trends for our business are pointing
in one directionup.
Financial Services During the financial crisis, the world
saw why healthy credit markets require transparency
and accountability. At their most basic level, these markets
connect capital with those who need it for development
and growth. When entrepreneurs and small businesses
cannot access financing through these channels, job creation
and economic growth can be seriously damaged.
Fortunately, conditions have improved dramatically since
the credit crunch brought the global economy to a virtual
standstill in 2008 and early 2009. A combination of low

interest rates, narrowing spreads and government stimulus
programs has pumped new life into corporate and municipal
debt markets.
As Standard & Poors Chief Credit Officer Mark Adelson
explains on page 6, credit ratings play an important role in
the bond markets by helping investors assess risk. Thus,
with the revival of these markets has come renewed demand
for our credit ratings.
Despite this progress in the corporate and municipal debt
markets, the securitization market has recovered far more
slowly. This is understandable given the prominent role that
mortgage-backed securities played in the recent financial
crisis. Yet in the long run, we believe a growing economy will
continue to need securitization to meet the global demand
for borrowing.
As investors return to this market, we have taken significant
steps to improve our ratings in response to the lessons of
the financial crisis. These steps include measures to better
account for the impact of periods of severe economic stress.
For example, before giving a security our highest rating of
AAA, we consider what would happen if the country faced
an economic crisis on par with the Great Depression.
As we have made these internal reforms, we have also worked
closely with governments around the world on new regulations
for the financial industry as a whole. We have supported
strengthening regulations for credit ratings in ways that
increase accountability and transparency, preserve analytical
independence, foster competition in the industry, and
achieve a globally consistent standard to prevent regulatory
arbitrage. In the year ahead, we hope the U.S. Congress
will complete a financial reform package that accomplishes
these goals and others designed to make a more stable and
sustainable global financial system.
On both the credit ratings side and the investment services
sidewhich I discuss more laterStandard & Poors
employees ultimately measure their success by the value
their insights bring to investors. While the global financial
system has changed in many ways during Standard & Poors
150 years of operation, our focus on serving all investors
will always be paramount.
Education It is a cruel fact that unemployment often remains
high long after economic recovery begins. In todays tight
job market where candidates outnumber openings, education
can make all the difference between landing a new position
and losing out. As a result, more students and workers are
returning to school with a common goal: to learn the skills
to succeed. In the United States, college enrollments surged
last year.
For McGraw-Hill Higher Education, these dynamics produced
a year of robust growth. While college enrollments will likely
not increase as quickly in 2010, we expect the divisions
momentum to continue. As McGraw-Hill Education Senior
Vice President Jeff Livingston explains on page 13, higher
education and postsecondary training are no longer a luxury
for some; they are a necessity for all.
In the elementary-high school market, state budget woes
continue to constrain spending in the United States. Yet even
during these lean times, education is an investment in the
future that no country can afford to postpone. As a result,
we see governments at all levels making new investments
in education.
At the state level, the market for new textbook adoptions will
be nearly twice as large this year as last. At the federal level,
meanwhile, the American Recovery and Reinvestment Act set
aside an extraordinary amount of funding for education
more than $100 billion in all. In the year ahead, the administration
will release more of these funds to states and districts.
In this changing educational environment, we have reorganized
our pre-k12 business into four new centers: intervention
and special needs; college and career readiness; literacy
and the humanities; and STEM (science, technology, engineering
and math). These centers mirror the priorities driving
education reform today. And together with our assessment
services, they will provide personalized and effective
pedagogical programs for equipping students and educators
for the 21st century.

Business Information At a time of increasing competition
among companies and nations, leaders around the world
are asking: Which industries will provide the foundation for
enduring growth in this new decade? At McGraw-Hill, our
business information brands are on the frontlines of answering
this question with cutting-edge analysis of the fields
that will define the future of the global economyenergy,
automobiles, aviation, construction and more.
In all these fields, we have moved from simply providing
information to customers to using digital technology to
embed our insights into their workflows. With advertising
now accounting for less of our total revenueonly about
two percentwe made the difficult decision to sell
BusinessWeek last year. For 80 years, the magazines writers
and editors embodied McGraw-Hills commitment to
integrity and independence, and we will always take pride
in our history together.
digital and global innovation
As we help create a smarter, better world in each of these
areasfinancial services, education and business information
we are also becoming a smarter, better company.
Across our company, our employees are improving business
processes by designing new digital platforms and raising
standards for customer service. In all, they are transforming
McGraw-Hill into a more digital, more global and more
sustainable business.
More digital While Americas economy experienced little
growth during the first decade of the 21st century, the same
cannot be said of the Web. As the graphic on page 18 shows,
digital data will have multiplied fivefold in the four years
leading up to 2012. In a world producing such unprecedented
quantities of data, individuals and businesses need quality
benchmarks, analysis and authoritative educational
programs more than ever. To meet this growing demand,
McGraw-Hill has moved to the forefront of the digital world.
For the energy market, Platts offers an advanced Web-based
eWindow that enables buyers and sellers to submit their trading
positions with the click of a mouse. In turn, the company
uses the real-time data from this system to provide the market
with important price assessments. At Standard & Poors,
meanwhile, subscribers for Capital IQs Web-based financial
tools increased by nearly 10 percent last year. On page 15,
Executive Managing Director Randall Winn explains how
Capital IQ has developed one of the worlds fastest-growing
fundamental research operations.
Nowhere is the digital revolution clearer than in education.
In short, the digitization of education is the opportunity
of the century for personalizing and improving learning for
students regardless of distance and time. Last year, we
introduced McGraw-Hill Connectan all-digital course
manager that already counts more than a million college
students and professors among its users. This year, we are
taking digital learning to the next level.
For college students, we are developing new editions of
McGraw-Hill LearnSmartour all-digital, adaptive study
program. As this program makes students smarter by tailoring
study materials around their individual needs, we are also
making the program smarter by learning from the performance
data it gathers.
For college faculty members, we are launching McGraw-Hill
Create for digital, custom publishing. Using this Web-based
platform, instructors can build custom course materials from
a selection of nearly 4,000 McGraw-Hill books and thousands
of articles, case studies and other resources.
While these new technologies are changing the way we provide
education, they will not change the underlying philosophy
that drives our business. We firmly believe scientifically
proven content matters most, as do the teachers and students
who rely on our work in 65 different languages. Our business
is not just a matter of publishing textbooks and computer
programs from different sources of information. It is a matter
of adding value by understanding the instructional science of
what students need to know, when they need to know it,
and the different ways they learn it best. And no one understands
this new learning frontier better than the editors,
authors and digital innovators of McGraw-Hill Education.
More global In the wake of the financial crisis, some have
questioned the future of globalization. Yet ultimately the
benefits of global trade and investment have been undeniable.
They have opened new markets for businesses and created
good jobs for millions in the developing and developed
worlds alike. Instead of retreating from globalization, we

must now ensure that these benefits accrue for every segment
of society, in every country.
For McGraw-Hill, the benefits of global trade and investment
have also been undeniable. In total, over the past eight years,
revenue growth from abroad has outpaced our domestic performance.
Last year, foreign sources accounted for 29 percent
of our revenue, with emerging markets growing in importance.
Outside the United States, India has become our third largest
market thanks to the rapid growth of our financial services
and education businesses in the country. For example, we
recently announced a new joint venture with Tata to work
with industry experts in creating a professional development
program for Indias growing retail industry.
In November, I spent a week meeting with government and
business leaders in another critical emerging market
China. During my trip, I announced three new ventures to
support the countrys transition from a mostly export-driven
manufacturing economy toward a more service-based
knowledge economy.
First, McGraw-Hill Education will partner to launch a new
English-language professional development program for
Chinese engineers, so we can help them acquire the skills to
compete in an increasingly global field. Second, J.D. Power
will partner to develop a first-of-its-kind index for evaluating
Chinese cities as destinations for business process outsourcing,
so they can better showcase their capabilities to
multi national companies. Finally, in recognition of Shanghais
growing stature as a center of global finance, Standard &
Poors announced plans to open a new Greater China headquarters
in the city.
More sustainable Around the world, environmental concerns
are driving a new market for sustainability. As businesses
and consumers increasingly factor the environment into
their decisions, this market will continue to expand, and
so will the need for consistent standards, financial models
and training programs.
At McGraw-Hill, our brands are uniquely positioned to
provide these services. By drawing on their expertise, we
are building a growing business around sustainability. For
example, the increasingly diverse S&P Indices brand offers
new investment tools that balance financial performance
with environmental responsibility. Most recently, we
partnered with the World Banks International Finance
Corporation to develop a carbon efficiency index for
emerging market companies.
As we help lay the foundation for a greener economy around
the globe, we are also working to become a greener company
at home. This year, our Corporate Responsibility and
Sustainability report will include significant targets for
reducing our environmental footprint.
In 2010, we look forward to all these elements coming together
to produce a year of global growth and innovation. I have
confidence in the future of this company because I have confi-
dence in the character and caliber of our employees. They are
our greatest asset. Their integrity builds trust for our brands.
Their insights set our analysis apart in a world awash with
choices. And their passion drives our commitment to joining
with our partners, customers and communities in creating a
smarter, better world.
The stories that follow are the stories of our work together.
We are fortunate to be part of a company where we can serve
the interests of our shareholders by serving the needs of
our world. On behalf of all my colleagues at McGraw-Hill,
I thank our distinguished and dedicated Board of Directors
for giving us that opportunity. Most important, I thank you,
our shareholders, for your continuing support.
Sincerely,
Harold McGraw III
February 24, 2010