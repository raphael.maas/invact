TO OUR
SHAREHOLDERS,
CUSTOMERS
AND EMPLOYEES

At ATI, our vision is focused on Building the World�s Best
Specialty Materials and Components Company.�
2017 was a year of substantial progress toward our long-term
vision through much improved fi nancial results and important
progress on several strategic initiatives. It was a year that
showcased ATI�s potential to achieve long-term sustainable
profi tability across the business cycle, and to consistently
create value for our shareholders and customers, and
opportunities for our employees. We believe that our eff orts
in 2017 laid the foundation for continued growth and fi nancial
improvement in 2018 and beyond.
In 2017, ATI sales were $3.5 billion, marking an increase
of 13% versus the prior year. This growth was driven by the
ongoing expansion in next-generation jet engine production
that is benefi tting our High Performance Materials and
Components (HPMC) segment, and by growth in several
other end markets, most notably oil & gas, impacting the
results of our Flat Rolled Products (FRP) segment. Both
segments increased operating profi t and margin levels
through improved product mix, higher operating rates, and
cost control. To clearly demonstrate the power of incremental
revenue growth and enhanced product mix on our business,
we grew 2017 revenues by $390 million and segment
operating profi t by approximately $280 million year over year.
Our HPMC segment, which generated 76% of its sales from
aerospace & defense products, posted 2017 sales
of $2.1 billion, representing 7% growth versus the prior year.
As I previously mentioned, the ongoing next-generation jet
engine production expansion, as well as strong growth in the
defense, oil & gas, and construction & mining markets, drove
signifi cant year-over-year increases. Sales to the electrical
energy and medical markets were both lower compared
to 2016. The growth in next-generation jet engine products
carries a substantial operating margin improvement primarily
due to enhanced product mix resulting from our development
of diff erentiated, innovative products. Next-generation
jet engines require more advanced materials to withstand
higher temperatures and pressure levels, and this material
shift benefi ts ATI. Segment operating profi t increased more
than 45%, further demonstrating the operating leverage this
business can achieve through revenue growth and enhanced
product mix. John Sims and his team
did a great job throughout 2017
in meeting our customer�s
demanding growth plans.
We expect this focus
to continue in 2018 and
beyond as we progress
through the industry-wide
production expansion,
and strive to continue
creating value for
our customers and
our shareholders.

Our FRP segment capitalized on strategic decisions made
over the last several years to post its fi rst profi table year since
2012. Annual revenues increased more than 20% compared
to 2016, primarily due to a strong rebound in the oil & gas
market, including downstream hydrocarbon and chemical
processing, and pipeline infrastructure projects. In addition
to demand increases, 2017 results benefi tted from comparison
to a 2016 period that was unfavorably impacted by the
seven-month work stoppage that ended in March 2016.
The FRP segment�s operating profi t recovery was dramatic,
swinging from a 2016 loss of more than $160 million to a profi t
of nearly $40 million in 2017. FRP�s U.S. domestic operations
benefi tted from improved product mix and increased asset
utilization due to our ongoing eff orts to focus on diff erentiated
products for oil & gas and aerospace & defense applications.
We continued to work diligently to control our costs and
to capitalize on our prior restructuring eff orts. FRP segment
results benefi tted signifi cantly from the contributions
of our STAL Precision-Rolled Strip� joint venture in China.
Bob Wetherbee and his team made substantial progress
on improving the segment�s fi nancial results and on several
strategic initiatives in 2017. We expect great things from
this group in the years to come!
Throughout 2017, we continued our eff orts to strengthen
our balance sheet and improve our cash fl ows. Our 2017
results demonstrated the positive impact of improved
operational performance and longer term strategic initiatives.
We generated $157 million of cash from operations in 2017,
excluding a $135 million contribution to the ATI Pension Plan.
We ended the year with over $140 million in cash on hand
and approximately $305 million available under our assetbased lending agreement. Aligned with our prior guidance,
we signifi cantly reduced 2017 capital spending versus prior
years to $123 million. 2017 marked the end of a multi-year
elevated capital expenditures cycle that included investments
necessary to help meet the demands of the next-generation
jet engine production expansion and to complete Flat Rolled
Products� Hot Rolling and Processing Facility (HRPF). In the
fourth quarter, we issued additional common stock and
used the signifi cant majority of the proceeds to early
redeem $350 million of high coupon debt due in June 2019.
This action will reduce our 2018 interest expense by
$32 million, and as a result, we now have no signifi cant debt
maturities until 2021. We continued to take actions to reduce
balance sheet risk by decreasing liabilities in the ATI Pension
Plan through voluntary lump sum cash-outs of approximately
1,350 deferred-vested participants. Including our 2017
cash-out and other proactive eff orts, the number of
participants in the ATI Pension Plan has declined by more than
20% since 2012. These actions help to substantially limit future
liability growth in the ATI Pension Plan while maintaining
market-competitive pay and benefi ts for our employees.
In addition to our 2017 fi nancial accomplishments, we took
actions to ensure that ATI is well positioned to meet its
long-term growth and strategic objectives. In the second half
of the year, we announced two separate joint ventures (JV),
one each in the HPMC and FRP segments. We believe that
these two JVs represent capital-effi cient opportunities to
leverage our technical prowess, increase our asset utilization,
and position ATI to generate higher revenue and operating
profi ts well into the future. First, in the FRP segment,
we announced an innovative joint venture with an affi liate
company of the Tsingshan Group, the world�s largest stainless
steel producer, to make and sell 60-inch wide stainless steel
sheet products in North America. The Allegheny & Tsingshan
Stainless (A&T Stainless) JV will utilize our previously-idled
Midland, PA direct roll, anneal and pickle (DRAP) facility and
is expected to substantially increase the utilization rates at our
HRPF in Brackenridge, PA. The A&T Stainless JV was formed
in late February 2018 following receipt of applicable
regulatory approvals. We expect it to be fully operational in
the second quarter 2018 and to contribute positively to FRP
segment results in 2018 and, more substantially, in 2019 and
beyond. In the HPMC segment, we created the Next Gen
Alloys joint venture with GE Aviation to further develop and
commercialize a novel meltless titanium alloy powder
manufacturing process. Meltless technology has the potential
to meaningfully change the way that the industry produces

titanium powder materials. We are in the early phases of
developing our Richburg, SC site to commercialize this
process and expect continued progress in 2018. This powderfocused JV is in addition to our all-new nickel super alloy
powder facility opened in 2017, and the announced titanium
powder expansion that is expected to be completed in 2019.
In addition to the JVs, we recently signed several new long-term
customer agreements that we believe will benefi t ATI in
2018 and beyond. These agreements include one with
Pratt & Whitney to supply iso-thermal forgings and powder
materials, and two with General Dynamics Land Systems
to supply titanium plate products for defense applications.
The recently announced JVs and new long-term agreements,
coupled with existing customer relationships, help to
solidify the long-term profi table growth outlook for both
business segments.
I am pleased about our 2017 accomplishments, and I am
confi dent that 2018 will bring continued growth and success
for ATI. This confi dence is based on our demonstrated ability
to execute on aerospace & defense customer agreements,
including expansion of next-generation products; the
anticipated contributions from new JVs, long-term customer
agreements and newly commissioned assets; cost reduction
actions already in place; and our focus on continuous
improvement throughout our operations. In short, we are
intently focused on executing our business plan and on
furthering our strategic initiatives as we move through 2018.
As a result of these eff orts, we believe that 2018 year-overyear revenue growth and operating margin expansion
will continue in both business segments, and that we will
generate higher free cash fl ows, refl ecting in part, lower
capital expenditures and signifi cantly reduced pension
contributions. We believe that the factors that propel us in
2018 will continue to provide a tailwind in 2019. The long-term
outlook for our business is bright!
As we close the books on 2017 and turn our focus toward
2018, I am reminded that ATI�s true North Star lies in our
commitment to our shareholders and in our core values.
These foundational beliefs drive every customer and
employee interaction. They are the constant in an everchanging world. In order to be the world�s best specialty
materials and components company, we must fi rst establish
and build a top-notch reputation in the marketplace and hire,
develop, and retain a world-class workforce. These goals start
with our commitment to Creating Long-Term Value Thru
Relentless Innovation� and are supported by our core values
of integrity, accountability, safety & sustainability; teamwork &
respect; and innovation. Our reputation, built over time and
through the hard work of every ATI employee, is one of our
greatest assets and we work hard each and every day to
ensure that we continue to build upon it through our actions.
I want to personally thank our shareholders, customers,
suppliers, and the communities in which we operate around
the world for their continued support. I want to especially
thank all of ATI�s employees for their hard work, commitment,
and contributions to making 2017 a year of great progress that
positions our Company for sustainable profi table growth in
2018 and beyond.
Finally, I would like to thank our Board of Directors for their
ongoing support in positioning ATI to be the world�s best
specialty materials and components company. Our work
is not complete, but we have a clear vision and welldeveloped plans to make this a reality.
Richard J. Harshman,
Chairman, President and Chief Executive Offi cer