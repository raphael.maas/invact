To Our Shareholders
Praxair performed extremely well in 2005. Our business model and
our employees worldwide again demonstrated extraordinary resilience
in the face of market volatility, increased energy prices, aberrant
weather and an intense competitive environment. The year 2005
presented some mighty challenges for business generally and Praxair
in particular.
Our goal is to generate a good return for our shareholders no matter
what the circumstances. So I am pleased our total shareholder return
increased to 22% for the year, exceeding the performance of many 
key financial market indicators. This can only be achieved while
delivering products, technologies and services of value to our global
customers. So we continued to do just that. Our sales grew 16% and
our pipeline of new business projects expanded substantially. 
The results described in this report were not easy to achieve. 
They required Praxairs people to concentrate intensely on serving 
our customers; to ?nd paths around business obstacles; to think 
out-of-the-box and yet remain grounded in reality. They required
ingenuity in ?nding new, undeveloped markets and astutely applying
new technological developments and applications. Mostly, they came
from hard, smart, relentless work. 
Success also demanded discipline. The discipline to operate safely and
ethically; to zero-in on those markets where we can execute crisply
and be excellent; to insist on good returns on our capital; to ceaselessly
improve productivity and manage costs; and to nurture a long-term
business perspective while maximizing short-term results.
So here is a brief, selective, review of some of the ways we served 
our customers and grew our business during 2005:
Higher energy prices dont have many upsides, but they do make
previously uneconomical gas and oil ?elds more attractive for
drilling. As a result, the energy sector accounted for much of 
Praxairs growth during the year. Demand increased for our nitrogen
and carbon dioxide to pressurize reservoirs of natural gas trapped 
in oil sands, shale or coal beds in the U.S. and Canadian Rockies.
Existing oil fields are also candidates for enhanced oil recovery 
(EOR) techniques. In this sector, we signed a major, 15-year contract 
with the Mexican state oil company, Petroleos Mexicanos (Pemex) 
to supply nitrogen for EOR in the 33-year-old Samaria oil?elds 
in southeastern Mexico. Current sales in the oil and gas production
market are about $260 million a year, and are expected to grow
about 25% per year over the next four or five years.
Praxairs hydrogen pipeline complex on the U.S. Gulf Coast 
continues to deliver high reliability for customers and high 
return on capital for Praxair. Hydrogen enables petroleum re?ners 
to reduce the sulfur content of gasoline and diesel fuel required 
by the U.S. Environmental Protection Agency. During the year, 
we reached agreement to build a new plant to supply two Valero
re?neries in Texas. Demand for Praxairs hydrogen in the region 
is expected to increase by more than 20% per year through 2010.
In fact, it was the strength of our energy-related businesses that
enabled Praxair to compensate for lower oxygen sales to steel 
companies during the year as well as the temporary reduction in
demand from customers hit hard by the two severe hurricanes in 
the Gulf Coast region. Praxairs facilities suffered relatively minor
damage in the storms.
Healthcare sales worldwide were up 19%, re?ecting the increased
demand for medical gases as well as respiratory and other services 
by both hospitals and home care patients. Margins in the U.S. 
have been reduced by lower government reimbursements, but our
business leaders continued to take steps to minimize this impact.
Overall, demand in this substantial market is expected to grow as
populations age and more healthcare is shifted from the hospital 
to homes.
Asia  particularly China and India  continues to enjoy strong growth
rates with the result that many world-class manufacturers are locating
plants there to serve both local and global markets. Praxair is well
positioned in these countries, as well as in South Korea and Thailand.
Currently Praxair has 16 major projects in China, with six scheduled 
to start up over the next two years. We completed construction of a
plant that will supply a giant $4 billion petrochemical complex in
Guangdong province, a joint venture between Shell Petrochemicals
Company and China National Offshore Oil Corporation. Praxair is 
a leading supplier to petrochemicals, electronics and steel companies
in the important industrial corridors around Beijing, Nanjing,
Shanghai and Guangzhou.
Indias upgrading of its civil and industrial infrastructure continues 
to gain momentum and Praxair is already participating strongly 
in this market. Praxair opened a new oxygen plant to supply Tata
Steel in Jamshedpur and signed a new contract with Hospet Steels,
further solidifying our position as a leading supplier to steel, glass,
petrochemicals and other Indian manufacturers. Late in the year,
Praxair also won two major contracts to supply oxygen to steel 
complexes in northeastern India.
European sales for Praxair jumped to more than $1 billion in 2005,
fueled largely by the smooth and rapid integration of our acquisition
of industrial gases assets in Germany late in 2004. With our wellestablished business in Spain, Italy and Benelux, we now have a
stronger and expanded presence in Western Europe.
In South America, our operations continue to support a high-return,
high-growth and strong cash-generating business. Sales increased
across all market sectors, and good pricing strategies helped offset 
cost inflation. In addition, about 40% of our South American 
contracts are priced to protect us from currency exposure. This year 
we expect signi?cant growth in Brazil arising from our decision to
enter the exciting adjacent market of lique?ed natural gas (LNG)
in a joint venture with Brazils state-owned oil company, Petrobras.
Our first LNG plant will start up in the first quarter, and Praxair 
will begin distributing LNG by truck to numerous industries and 
consumers who previously had no access to this clean energy source.
How we conduct our business is just as important as our financial
results. Just after year-end, Praxair was named the most shareholder
friendly among specialty chemical companies in an Institutional
Investor magazine survey. This recognition is based on accessibility  
and forthright communication as well as on stock price appreciation,
?nancial performance and good governance. In addition, we earned a
fourth consecutive top governance rating from GovernanceMetrics
International in 2005  one of only four companies out of more than
3,000 worldwide to achieve the top score in four consecutive cycles.
We were also chosen as a component of the Dow Jones Sustainability
Index for the third consecutive year, recognizing Praxairs leadership
in a number of social, environmental and economic activities.
In safety, Praxair turned in a good performance, although we did not
match last years lowest-ever injury and accident rates. During the
year, a Praxair Distribution facility in St. Louis, Missouri, suffered a
major fire. Following our rigorous emergency procedures, all of our employees evacuated the site without any injuries. We regret this
incident, and have reviewed all of our procedures in order to prevent
such an event in the future.
Praxairs board of directors has provided me and my management
team with excellent support and wise counsel throughout the year
and I thank each one for their dedication to the company. I also 
want to welcome our new board member, Jos Paulo de Oliveira
Alves, former chief executive officer of the U.S. operation of one of
Brazils leading steel producers, Companhia Siderugica Nacional.
His international business experience and extensive knowledge of
South American markets is already providing valuable insight to 
the company.
Effective March 1, I have appointed Steve Angel, formerly executive
vice president, to the position of president and chief operating of?cer
of the corporation. He will also be nominated to stand for election 
to the board. Steve has made an outstanding contribution to Praxairs
success in recent years and possesses exceptional leadership skills.
As I look forward to another year of strong business performance, 
I am well aware that our global businesses will constantly face the
unexpected. Energy prices will continue to bounce; government and
environmental regulations will change; some industries will flounder
while others thrive; competition will intensify everywhere; and
Nature itself can produce random and dramatic surprises. 
But Praxair is resilient. Our business model is robust. Our people 
are fast and ?exible, and able to achieve exceptional results across 
the globe. Operating safely, ethically and with deep integrity, we
will move faster and more boldly in 2006.
