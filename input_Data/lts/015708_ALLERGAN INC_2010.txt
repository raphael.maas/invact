The Value of Specialization
Allergan is a unique company in many ways. We have a high
degree of specialization and focus in the medical specialties
that we serve. Our deep insight into doctors needs  not only
in the United States but worldwide  to improve the care of
their patients, coupled with continuous strong investment
into innovation, certainly paid rich dividends. In fact, 2010
was the best year in our 60-year history in terms of the number
of major product approvals by the U.S. Food and Drug
Administration (FDA) and other regulatory agencies.
Allergan has a unique set of diverse businesses, spanning
biologics, pharmaceuticals, medical devices and over-thecounter
consumer products, with our efforts concentrated on
six distinct specialties, enabling us to preserve our focus while
offering revenue diversity to stakeholders. Compared to most
health care companies, Allergan is unusual in its mix of both
reimbursed treatments and cash-pay products (paid for out-ofpocket
by the patient). In 2010, this was an ideal position, with
recovery in the industrialized economies of North America
and Western Europe benefi ting these cash-pay businesses and
offsetting new cost pressures from U.S. health care reform and containment of government spending in Europe. In
each of our specialty fi elds, we established or retained the No. 1 or No. 2 market share position with the exception of
urology  a business we entered only recently but in which we have already secured the No. 4 position in the U.S.
market. In the coming years, we expect Allergan to be recognized as the emerging company in this specialty, given the
promising strength and breadth of our pipeline.
Allergan is also far advanced compared to its specialized peers in biotech or medical devices in terms of its global
geographical reach, rounding out historically strong market positions in highly industrialized countries. Today, we
have sales and marketing subsidiaries in 36 countries, and in the last two years we have established direct operations in
markets such as the Philippines, Turkey and Poland, with further expansion planned in other emerging markets. This
approach is not new to Allergan, as we have had direct operations in India and Brazil for more than 20 years. In China
and Japan, we reacquired the distribution rights to BOTOX for aesthetic use from GlaxoSmithKline. Supplemented
by distributors, Allergan today is present in more than 100 countries.
Our multi-specialty focus and global presence are enhanced by the advantage of an ideal size  large enough to lead
R&D investment in our specialist fi elds and invest in the creation of markets and product categories, yet small enough to
move quickly and nimbly. We foster agility by assigning accountability to small management teams with a unique blend
of skill sets from the pharmaceutical, medical device and consumer industries. Over and over again, Allergan has led
the creation of new markets such as medical aesthetics with the launch of BOTOX Cosmetic; JUVDERM XC,
our dermal fi ller containing lidocaine; and LATISSE, the fi rst and only FDA-approved product to grow eyelashes.
Another example is our development of the chronic dry eye market with RESTASIS, the fi rst and only prescription
therapy for this condition. Now, with the approval of BOTOX for chronic migraine patients by the FDA and the U.K.
government, we are again in a special position, not only to alleviate the suffering of millions of patients but also to create
a new category as the only focal therapy for the prophylaxis of chronic migraine.
Given our closeness to our customers and our in-depth knowledge of the markets in which we operate, execution is a key
characteristic of our success. Our research knowledge in our areas of expertise allows us to generate a much higher than
average rate of success in moving in-house developed compounds from pre-clinical research through to Phase III registration
studies and eventual regulatory approval. It also enables Allergan to attract new technology for in-licensing from third parties.
Today, the pharmaceutical industry is heavily challenged by expiring patents and the entry of generics. Allergan is uniquely
positioned to handle competition from generics. BOTOX is one of the largest biological pharmaceuticals in both its
molecular weight and complexity. While the European Medicines Evaluations Agency has approved a limited number of
biosimilars and the FDA has the mandate from Congress to establish a related approval pathway, a biosimilar of BOTOX will
require considerable resources and time to develop. In ophthalmic pharmaceuticals, we have applied innovation to optimize
existing drug formulations, where pH and concentration of active ingredients make a substantial difference in the products
risk-benefi t profi le. These efforts have resulted in strong effi cacy and improved tolerability for our glaucoma products, including
ALPHAGAN P 0.1%, LUMIGAN 0.01% and COMBIGAN, welcomed by doctors and their glaucoma patients.
Lastly, I would be remiss if I did not acknowledge the impact of our unique culture, a blend of tradition and bold ,
forward-looking innovation. This past year, we celebrated the 60th anniversary of the founding of Allergan by Gavin
Herbert and his father, and a long tradition of fi rst-in-class products, especially in eye care. Only three CEOs have led
the company, a testament to continuity, yet balanced by a constant search for discovery, new sources of growth and ways
to improve our business processes and operating effi ciency. Also, as the environment in which we operate continues to
evolve, it is imperative we continue to bolster our efforts to ensure our people and our practices are refl ective of the
highest levels of ethics and integrity and are in full compliance with the laws that govern our business. In 2010, we
further enhanced our compliance program in the United States by creating increasingly rigorous processes, supported
by new technology, to help ensure continuing compliance as a top priority for all employees and Allergan stakeholders.
We believe that all these unique elements add up to a sustainable growth strategy for Allergan, buoyed by global megatrends
in the fi eld of health care: the needs of an aging population, the epidemic of obesity worldwide and its linkages to the
burden of diabetes and cardiovascular disease, as well as consumers desires worldwide to look as good as they feel.
Strong Performance in 2010
In 2010, Allergan grew sales by 8.4 percent in U.S. Dollars and 7.5 percent in local currencies and increased adjusted
Diluted Earnings per Share by 13.7 percent benefi ting from cost reduction and operating effi ciency programs we had
instituted in the depths of the recession. [A Reconciliation between Generally Accepted Accounting Principles [GAAP]
and adjusted Diluted EPS is on pages 10-11.] Adjusted cash fl ow from operations was a strong $1,058 million(1) due to
operating performance and strong balance sheet management.
Economic Recovery and Market
Share Gains
Our sales growth was boosted by a recovery
in our cash-pay businesses as highly industrialized
countries recovered from the recesssion
and markets in East Asia, India and
Brazil enjoyed booming conditions. In fact,
all of our markets, with the exception of
obesity intervention, are now at levels
above 2008  in part due to our decision to
increase investment in direct-to-consumer
advertising and educational programs in
the tough economic climate. Allergans
growth was further boosted by market
share gains in almost all businesses. In
2010, JUVDERM captured the No. 1
position worldwide in the dermal fi ller market with the launch of our new formulation containing lidocaine, as well
as JUVDERM VOLUMA, a volumizing fi ller approved in Europe, Canada, Australia, Latin America and many
Asian markets. The obesity intervention market is still suffering declines due to reimbursement restrictions imposed
by U.S. health care plans, as well as the collapse in the cash-pay segment of the market, where patients pay out-of-pocket
for LAP-BAND surgery and many procedures are funded on credit. We believe that the volume of obesity surgeries
is correlated with levels of unemployment. During 2010, we were able to stabilize our U.S. market share in obesity
intervention in excess of 70 percent as Ethicon, which launched a gastric band in 2008, moved its resources to other
surgical intervention products.
Challenges
In 2010, we successfully dealt with several headwinds to sales growth. Most notably, in the United States we faced a
full year of generic competition to our ophthalmic products, including ALPHAGAN and ACULAR; a full year of
competition for BOTOX Cosmetic from Dysport; and the entry of Dysport for the therapeutic indication of cervical
dystonia. In Europe, we saw the launches of aesthetic neuromodulators from Galderma and Merz. Even with these
entries, global market share for BOTOX in the neuromodulator market remained high at 79 percent in 2010.(2)
Outlook for Growth Driven by Innovation
With a record number of regulatory approvals in 2010, prospects for growth in the coming years are strong. In the
pharmaceutical industry it is typical for a newly approved product to require 5-6 years to reach peak global sales.
Almost all of our global markets today are generating double-digit growth rates.
With BOTOX , we have multiple opportunities to further expand its therapeutic potential  which accounted for approximately
51 percent of the products total sales in 2010. With the pending approvals of BOTOX for chronic migraine in
several countries, and our clinical programs for overactive bladder as well as benign prostatic hyperplasia, the therapeutic
franchise for BOTOX where we have limited competition will take on even greater importance in the sales mix.
In our ophthalmology franchise, RESTASIS grew signifi cantly and is expected to become in 2011 the largest single
prescription ophthalmic pharmaceutical in the United States, given the prevalence of chronic dry eye and the growing
acceptance by more and more ophthalmologists of the advantages of early intervention in the treatment of chronic dry eye.
RESTASIS was also launched in Canada at year end. A key task is to secure approval of RESTASIS in Europe. In
the United States and Europe, our latest-generation formulation LUMIGAN 0.01% enjoyed strong uptake due to
its powerful ability to lower intraocular pressure accompanied by lower rates of hyperemia, when compared to the
original LUMIGAN formulation. In Canada, ophthalmologists appreciation of the new formulation has been so
strong that we have announced the discontinuation of the original formula in early 2011.
Today, retina conditions are the leading cause of blindness in highly industrialized countries, and retina is the
second-largest segment in the ophthalmic pharmaceutical market by value. OZURDEX, indicated for retinal vein
occlusion (RVO) and uveitis in the United States and for RVO in Europe, is our entry point into this fast-growing
segment of the market. In order to further boost our No. 2 global position in ophthalmology, we licensed from
Johnson & Johnson the worldwide rights to LASTACAFT , already approved in the United States, which will give
us a highly competitive product in the ocular allergy market.
With our robust strategic position and track record of turning R&D investment into innovation, we will continue
to invest strongly in discovery and clinical development, supplementing our internal efforts with selective licensing
and acquisition of technology. A recent example is our 2010 acquisition of Serica Technologies, which provides a
differentiated silk scaffold technology and a potentially revolutionizing approach to breast reconstruction surgery.
Pursuit of Excellence
Success fuels continuing energy, drives greater motivation and ideas, and in turn delivers continued strong performance.
With attention to detail  from customer service to R&D execution  we are constantly looking for ways to improve
our business model and advance patient care. We have emerged from the recession with an improved structure and
even higher degree of operational focus, and are applying the same disciplines to absorb the costs of health care reform.
This includes saving natural resources, an effort for which we have been recognized as one of the Greenest Companies
in America. With our proven track record of success and commitment to our people, we continue to attract and retain
some of the best employees in our industry.
As individual and team contributions continue to lie at the heart of any companys success, on behalf of our management
team and our Board of Directors, I would like to thank our employees around the world for their many great results and
dedication in 2010. It is a pleasure and honor serving you as Allergans CEO and I look forward to a continued strong 2011.