To My Fellow Shareholders

The demand for high-quality and professionally managed
apartment housing is strong and growing. Fueled by improving
employment markets and increasing new household formation,
we expect these positive trends will be particularly evident in
the high-growth Sunbelt region of the country where we focus
your investment capital.
As I�ve reported to you over the past couple of years, the very
favorable leasing trends that began as a collapse in the singlefamily �for sale� housing market encouraged an increasing
number of households to meet their needs within the rental
market. This influence, coupled with the lowest level of new
apartment construction in the last 40 years, created very
favorable leasing conditions.
The favorable leasing conditions have not gone unnoticed by
developers, investors or lenders, and the development of new
apartment communities is increasing. However, at this point
construction activity is not at a level that is concerning or that
exceeds the growing level of demand for apartments. While
we believe that leasing conditions are likely to moderate some
from 2012, we expect the leasing environment will continue
to support our ability to capture strong and above average
rent growth over the next couple of years.
There has been some discussion by analysts that the recovery
underway in the single-family housing market is a cause for
concern. We don�t share that concern. The high number of
move-outs of residents to buy a house in the 2003�2007
timeframe was, to a large degree, fueled by overly aggressive
mortgage financing products, coupled with the false belief
that a single-family home was almost certain to generate a
positive investment return. The last few years have clearly
shown that is not always the case. The next wave of future
homeowners, those who today are in the age bracket of their
early 20�s to early 30�s, are more likely to stay in the apartment rental market longer. Record levels of student debt, more
disciplined mortgage financing practices, a desire for more
flexibility and mobility, and a general desire for the convenience and lifestyle surrounding apartment living will compel
many rental households to stay in the rental market longer.
At MAA we continue to take a long-term approach to investing. Our approach is centered on deploying investment capital
to generate secure, growing �full cycle� cash flow. As a Real
Estate Investment Trust we believe that generating value for
shareholders through paying a secure and growing cash dividend, over full market and economic cycles, is a key variable
in delivering superior long-term shareholder return. During
the current �up� cycle for apartment real estate our team of
professionals at MAA has been actively working to further
fortify our balance sheet and position the company for favorable access to the capital markets. Additionally, we are
increasingly recycling investment capital into newer and more
profitable apartment properties that will drive higher levels of
shareholder value over the long haul. And, we are continuously strengthening and fine-tuning a number of our operating
processes to deliver superior and more efficient services for
our residents. MAA�s culture and long-standing priority
placed on on-site property operations along with a disciplined
approach to deploying capital are the key cornerstones in our
ability to extract superior performance and value from the
property investments we own.
MAA had a record year of performance in 2012. And with a
favorable outlook for apartment leasing fundamentals over the
next couple of years, we believe MAA is in a solid position to
continue generating record results. We remain excited about
our outlook.
Our record results in 2012 were only possible due to the hard
work and dedication of 1,400 MAA associates working every
day to serve our residents and our shareholders and to support
each other. I want to thank them for their professionalism and
dedication to our team.
I also want to express my deep appreciation and gratitude to
General John Grinalds, who will be retiring from our Board of
Directors effective at our annual shareholder meeting in May.
General Grinalds joined our Board of Directors in 1997 and
is our longest serving independent director. His wise counsel
over the past 16 years has been a key contributor to the
growth and success that we have captured.
Sincerely,
H. Eric Bolton, Jr.
Chairman and CEO