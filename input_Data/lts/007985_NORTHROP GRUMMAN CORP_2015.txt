DEAR FELLOW SHAREHOLDERS
The Northrop Grumman team continued its focus in 2015 on
sustained performance, portfolio alignment and effective
capital deployment. Financial highlights for 2015 include
net earnings of $2.0 billion, or $10.39 per diluted share,
compared to $2.1 billion, or $9.75 per diluted share in 2014.
Diluted earnings per share for 2015 increased 7 percent
and are based on 191.6 million weighted average shares
outstanding, compared with 212.1 million shares in 2014.
During 2015, the company repurchased 19.3 million shares
of its common stock for $3.2 billion. As of Dec. 31, 2015,
$4.3 billion remained on the companys share repurchase
authorization.
Continued strong cash generation also supported our
twelfth consecutive annual dividend increase. We raised
the quarterly dividend by 14 percent and paid shareholders
$603 million in dividends in 2015. Total shareholder return
for the year was slightly more than 30 percent. The first
priority of our capital deployment strategy is to invest in our
businesses. Capital spending in 2015 totaled $471 million,
and we increased our IRAD investment by 25 percent, to
$712 million, or 3 percent of sales, as we foresee significant
opportunities ahead.

In addition to strong financial results, Northrop Grumman
captured several important new business awards, both
within the United States and internationally, as we continued
to focus on our core capabilitiesunmanned, cyber,
C4ISR, logistics and strike. The estimated value of contract awards recorded
during 2015 was $21.3 billion.As we continue to grow as a global security company, we
remain committed to maintaining the highest of ethical
standards, embracing diversity and inclusion, sustaining
the environment, and operating as a responsible corporate
citizen. In 2015, Northrop Grumman, the Northrop Grumman
Foundation and ECHO  our Employees Charity Organization
 contributed a total of $27.7 million in support of science,
technology, engineering and math (STEM) programs;
assistance to veterans, service members and their families;
and help for those with critical needs in our communities.
Please take a few minutes to read our 2015 Corporate
Responsibility report to learn more about our efforts
and accomplishments.
We continue to build on our solid track record, and we look
forward to continued, long-term, sustainable value creation
for our shareholders, customers and employees. Thank you
for your continued investment in Northrop Grumman.

WES buSH
Chairman, CEO and President