dear fellow shareholders
challenges we have faced to the fundamental value of our 
Company, thanks in no small part to the support of our 
stockholders, I plainly told you all it was time for us to reward 
your confidence by demonstrating our ability to enhance 
shareholder value. To this point, insofar as the performance 
of our shares in 2010 reflects, we have not.
The massive gravitational force of relentlessly low natural gas 
prices on independent power producers like NRG has left our 
share price languishing well below levels we find acceptable. 
A flood of cheaply accessed natural gas from unconventional 
shale formations has driven down gas prices, particularly nearterm gas prices. Since the price of our core product, wholesale 
electricity, is fundamentally tied to natural gas, the forward 
price of electricity has dropped and, with it, our share price.
The good news, and indeed it is news that you should take 
significant comfort in, is that although natural gas prices 
have been in decline since mid-2008, that decline has 
not stopped us from posting another year of exceptional 
financial results. Adjusted EBITDA of more than $2 billion for 
the fourth straight year and adjusted free cash flow before 
growth investments of $1.4 billion highlight our continuing 
financial strength and the business logic of both our forward 
hedging program and our unique combination of wholesale 
generation and retail energy services, delivered principally 
through our highly successful subsidiary, Reliant Energy.
Our robust financial performance also underpins our ability 
to seize the extraordinary opportunities that have arisen 
in the rapidly changing power industry landscape. Coming 
out of the Great Recession, an ever increasing number of 
Americans are more aware of their energy usage. They dont 
want to waste energy; they want to promote energy sources 
that are sustainable and do not pollute our air and water 
supply so that they can be equally enjoyed by their children 
and grandchildren. The American people want to make 
energy choices that rely on domestic resources and sustain 
American jobs, rather than increase dependence on foreign 
fuels. The phenomena I am describing tend to be collectively 
characterized as a trend towards a green or sustainable 
lifestyle, but I think of it more as a desire to be smart in our 
energy choices.
The operative word is choice, and thats what has changed 
in our industry. The consumer trend toward sustainability 
has been in progress for at least two decades, but has been 
focused on areas outside of energy, such as organic food, 
recycling and paperless offices. The sustainability trend has 
always been predicated on the simple fact that the well- 
intentioned but supremely pragmatic American consumer 
will make the smart choicethe green choicewhen there is 
a legitimate choice to be made.
Traditionally, Americans have had little or no choice about 
the energy flowing into their homes and the energy pouring 
into their gas tanks. But times are finally changing. Smart 
and sustainable energy choices are increasingly available 
to the American public. Reliant Energy offers e-Sense
smart energy solutions, which allow customers to receive 
detailed information about how they use electricity at home 
and gather timely insights about their power use and cost. 
Green Mountain Energy Company, which sells renewable 
power to consumers, experienced its 10th successive year of 
double-digit growth in green customers who want to make a 
difference for the environment. 
We also want to expand and increase the speed at which 
these smart choices reach consumers, which is why NRG has 
joined GE and ConocoPhillips to form Energy Technology 
Venturesa fund created by the three partners to collectively 
invest an initial $300 million in potentially revolutionary clean 
energy technologies.
While Green Mountain and Reliant are offering American 
consumers the choice to be sustainable in the energy they 
use in the home, an even bigger revolution is in the offing for 
Americans as they take to the roads. In 2011, for the first time, 
American consumers will have a credible choice to reduce 
dramatically or even eliminate their personal dependence on foreign oil by buying any one of a number of plug-in hybrid 
or pure electric vehicles being brought to market by a wide 
range of auto manufacturers. We at NRG expect to play a 
significant role by ensuring that plug-in car owners, as a 
result of their car choice, do not feel constrained in terms of 
their driving range.
If the American public responds positively, the electric 
vehicle will have a transformational impact on both our 
conventional and emerging green businesses. Electric 
vehicles, adopted en masse, will increase electricity demand 
and flatten out daily consumption in a manner that, when 
combined with smart grid technology, will favor the 
baseload generation that is the heart of NRG. Further, the 
emergence of electric vehicles should accelerate the shift 
from conventional power to sustainable power in the home 
as many EV owners will not want to simply trade tailpipe 
emissions for smokestack emissions. Most EV owners, 
we believe, will naturally be drawn to the likes of Green 
Mountain or Reliants smart energy products, creating what 
we call the virtuous green circle of energy usage in the home 
and on the roads. Finally, the emerging EV market presents 
NRG with a tremendous opportunity to develop fueling 
products and services for EV owners and the businesses that 
want to cater to them. 
In November, NRG launched eVgo in Houston, the first 
privately funded, comprehensive electric vehicle charging 
ecosystem in America. The eVgo package, which I like to 
characterize as a miles contract similar to the minutes 
contract on your mobile phone, consists of the purchase 
and installation of the fast charger at the EV owners home, 
free access to the network of fast chargers that we intend 
to install around the Houston area and unlimited electricity 
to fuel the EV itself. The package is designed to bring 
enormous operating convenience to EV owners, substantially 
reduce EV operating costs and create certainty around those 
costs. Most importantly, eVgo will convert the range anxiety 
that is currently associated with electric car ownership to 
range certainty. We are the perfect Company to lead in this 
area because our unique combination of power generation 
facilities and retail energy services in Houston better enable 
us to service EV owners with such comprehensive fueling 
service packages.
The EV revolution is only going to test further an already 
aging U.S. baseload power generation fleet. And probably 
the most critical strategic decision our industry needs to 
make over the next decade, in consultation with federal and 
state public policy makers, is how to replace the baseload 
capacity that will power Americans in their homes and on 
the roads for the better part of the remainder of this century.
Certainly, our industry will build a lot of natural gas-fueled 
plants and that is a good thing. Natural gas is a highly 
flexible, very efficient, reassuringly domestic and relatively 
clean fuel by fossil fuel standards. But natural gas always 
has been, and will likely continue to be, a highly cyclical 
commodity with very significant price volatility over the 
business cycle. An American power industry that relied 
almost exclusively on natural gas for its baseload generation 
would consume an enormous amount of natural gas. Like 
many in the power industry, we believe as a matter of prudent 
public policy and industry practice, the trend towards 
increased reliance on natural gas for power generation 
purposes should not be allowed to become total dependence
on natural gas. 
That presents two alternatives for new baseload powernew 
advanced nuclear power and clean coal. Four years ago, in 
response to the passage of the Energy Policy Act of 2005, we 
started development of two new nuclear units at South Texas 
Project (STP). While the STP project has achieved immense 
progress and is, as I write this, one of very few nuclear 
development projects in contention for the all-critical federal 
loan guarantee, the loan guarantee and the projects combined 
operating license application remain pending. 
While this project remains hugely important and has 
enormous potential for commercial success as the future 
zero-emission cornerstone of our baseload fleet in Texas, 
we remain acutely focused on achieving favorable terms 
and conditions with respect to financing; engineering, 
procurement and construction arrangements; and offtake 
contracts. We will be intensely disciplined about our decision 
whether or not to go forward into construction. The stakes are high, but the potential competitive benefits to us are 
enormous. Nonetheless, we will not bet the bright future of 
our Company on this one project.
Exceptional Financial Performance in a 
Challenging Environment
In my mind, the attractive investment proposition that we 
offer our shareholders is that we have demonstrably proven 
that we can effectively make substantial investments in the 
future of the Company while delivering exemplary financial 
and operating results in the present. Last year was no 
exception. In the face of many external challenges, most 
notably the sharply declining natural gas commodity price 
environment, NRGs 4,964 employees delivered another year 
of exceptional financial and operational performance: 
2010 results
  $2,514 million in adjusted EBITDA, just shy of our record 
EBITDA of $2,618 million in 2009
  Compound annual adjusted EBITDA growth of 4.8% from 
2008 to 2010, compared to a 29.6% compound annual 
decrease in the average per MMBtu Henry Hub natural gas 
price during the same period
  $1,760 million of adjusted cash flow from operations, the 
second highest total in Company history
  $180 million of common stock, or 8.5 million shares 
repurchased
  $758 million debt paid down, including $453 million of 
term loan and $190 million of Common Stock Finance
  Record liquidity of $4,252 million, excluding collateral 
deposits, up 12.1% from 2009
  Safest year in our history with a 0.75 OSHA incident 
rate across the organization, in the top decile of industry 
safety results.
This strong financial and operational performance did 
not come at the expense of investing in NRGs substantial 
growth opportunities as the Company made $309 million 
of equity investments on RepoweringNRG projects and 
STP 3&4 in 2010.
Clearly, while we continue to scan the horizon to best 
position our Company to capitalize on fundamental trends 
molding our industrys future, NRG is executing at the 
highest level today. 
A Banner Year for Value-Enhancing 
Transactions
The Company took advantage during 2010 of opportunities 
to strengthen key elements of both its conventional business 
and its growing sustainable energy business. 
We added the modern Cottonwood combined-cycle plant to 
allow us to more efficiently follow load in the Entergy zone 
from our own portfolio, which, in turn, will better enable us to 
serve our growing customer obligations in the South Central 
region as well as to become a stronger competitor for new 
business. The 1,265 megawatt (MW) natural gas-fueled 
Cottonwood plant is one of the newest and most efficient in 
the region, with a dedicated and highly capable workforce 
that we are pleased to welcome to the NRG family. 
NRG acquired Green Mountain during 2010, which for 
10 years has been the nations leading retail provider of 
clean energy products and services. Green Mountain sells 
renewable power to more than 300,000 commercial and 
residential customers, with the majority in Texas and a small 
but fast-growing number in the Northeast, particularly 
New York City. We intend to focus Green Mountain on 
what it does bestselling green energy and servicesbut our ownership will enable Green Mountain to expand 
more forcefully into attractive markets and into customer 
segments that they previously have not targeted. You also 
can expect to see Green Mountain offering more physical 
green energy products as part of its product menu as we 
anticipate an increasing demand among consumers and 
businesses for distributed green energy generation.
Over time, we will increasingly align Green Mountains retail 
expertise with our fast-growing, large-scale renewable 
generation. We are increasingly leveraging our retail 
businesses, Green Mountain and Reliant, to sell the renewable 
energy credits generated by our 450 MW of wind farms.  
Further west, we struck numerous deals to acquire thermal 
projects and photovoltaic solar projects under development 
in California, Arizona and New Mexico. These projects, 
in our opinion, represent very attractive investments of 
our shareholders capital thanks to long-term offtake 
agreements, minimal construction risk, proven technology 
and very long-term warranties from the key equipment
manufacturers. By the end of 2011, we expect to be on our 
way to being the largest solar generator in the U.S., with 
three of the largest photovoltaic and solar thermal projects 
in the world (Agua Caliente in Arizona, California Valley Solar 
Ranch and Ivanpah in California) well into construction.
We think our Company is uniquely well-suited to realize the 
opportunities associated with solar power, and we like that 
solar power has been consistently encouraged by public policy 
at the federal and state levels and across the political spectrum. 
When you consider the inexhaustibility of the solar resource in 
the U.S., the zero marginal cost of production and the potential 
for continued technological innovation that will bring the total 
cost of solar power down to grid parity, we think the political 
consensus supporting solar power is well-placed.
Taken together, our solar and wind projects and Green 
Mountain will play a major role in helping us achieve our 
target of having roughly a quarter of our EBITDA come from 
cleaner energy sources by the middle part of this decade.
NRG sees solar power as a national development opportunity and 
is building a robust multi-technology portfolio to lead the industry 
in delivering the benefits of this zero-emission renewable power 
source. Through our subsidiary NRG Solar, the Company has made 
great strides in the past year in both expanding and deepening the 
solar portfolio.
NRG began implementing its solar strategy in late 2009 by 
purchasing the Blythe Solar Project, the largest solar photovoltaic 
facility in California. Since then, 2010 has seen a steady stream 
of larger and more sophisticated projects announced across the 
Southwest. With the extension of the federal cash grant program 
for shovel-ready renewable projects through 2011, the coming year 
is sure to be another active one for NRG in solar development.
The Company made a big splash in its first move into solar thermal 
technology, announcing our intention to take a lead investment role 
in BrightSource Energys 392 megawatt (MW) Ivanpah project in 
Californias Mojave Desert. At peak, Ivanpah is expected to generate 
enough renewable power to supply more than 140,000 homes 
when the project comes fully online in 2013.
Also in California, we are a partner in the construction of Avenal, a 
solar facility that will be more than twice the size of Blythe. Through 
a partnership with SunPower Corp., the Company plans to develop 
the 250 MW California Valley Solar Ranch project, expected to be 
one of the largest solar PV projects in the world.
In June, NRG acquired a 720 MW portfolio of projects at sites 
in California and Arizona and broke ground on two smaller PV 
projectsa 25 MW project for Tucson Electric Power and a 20 MW 
project in New Mexico. In December, the Company announced the 
planned acquisition of the 290 MW Agua Caliente Project in Yuma 
County, Arizona, which will be one of the worlds largest PV projects 
when it comes online in 2014.
In addition to large commercial-scale projects, NRG has also started 
developing distributed solar arrays, or solar pavilions, for select 
school districts in Arizona. These solar pavilions and their panels are 
being developed as carports adjacent to school buildings. Future 
installations could also supply shade for playgrounds, sidewalks or 
whatever best fits the needs of the individual school.
Through this busy year, NRG has shown it is committed to taking 
a leadership role in developing this bountiful zero-emission 
energy resource in the Southwest and beyond through a variety 
of innovative technological applications, brightening the future for 
hundreds of thousands of Americans.
Take todays networked TVs, Blu-ray players and smart 
appliances, add in electric vehicle chargers and rooftop 
solar panels, then mix them with a smart meter and an 
electricity provider like Reliant Energywhat do you 
get? The long anticipated home of the future. For the 
2.5 million homes in Texas equipped with smart meters, 
the home of the future is possible today. 
Slicing open a theoretical home complete with 
grid-aware electronic devices like thermostats and 
dishwashers, Reliant showed 2011 Consumer Electronics 
Show attendees how an entire home could be managed 
right from their iPad or smart phone. While the home at 
CES was just a display, this holistic home management 
is possible today in Texas. Whether you forgot to set your 
DVR or you want the dishwasher to run before you get 
home for dinner, consumers with smart devices can now 
manage it all with the press of a few buttons.
And when customers sign up to use Reliant e-Sense
smart energy solutions, they will receive detailed 
information about how they use electricity so that 
they can tell their smart devices to run when electricity 
is most affordable. With that amount of knowledge, 
Reliant customers will have all the tools they need to 
truly manage their electricity bills. Reliant leads Texas 
in bringing the benefits of smart energy technology to 
consumers with more than 175,000 customers already 
using e-Sense.
Finally, in 2010, we expanded our business into Arizona 
through the acquisition of Northwind Phoenix, a very 
highly regarded district cooling business active in and 
around Phoenix. We see a bright future, literally, there as 
NRG Energy Center Phoenix, as it is now named, expands 
its cooling business and as we get more active in tapping 
Arizonas immense solar resources.
Shovels in the Ground
Despite Congress inability to pass comprehensive climate 
and energy legislation in the previous session, public policy 
more generally continues to favor solar and other forms of 
zero-emission power generation across the country. There 
are 35 states with clean alternative energy standards or 
goals. We believe energy will be the focus of considerable 
attention in the new Congress; and we believe the various 
initiatives that our Company has under way put us in a 
unique position to move quickly to capture any opportunity 
that arises as a result of government action or, for that 
matter, inaction.
While the previous year was not kind to our stockholders, 
I believe the evidence is clear that we are well-positioned 
for better years to come. Companies that resist the tide 
of history and societal change tend to get swamped. Your 
Company, on the other hand, has paddled hard during the 
year to get out on the front edge of the wave. The four 
cornerstones of Americas energy future are clean baseload 
power; renewables backed by fast-start, high efficiency gasfueled combined-cycle plants; the smart grid; and electric 
vehicles. NRG holds a leading position as a first mover on 
all of these elements and is poised to capture the immense 
benefits inherent in the opportunities they present.
To my fellow stockholders, thank you for your patience with 
and recognition of the transformation that is now well under 
way at NRG. The preparation is over and, in many respects, 
the shovel is in the ground. But we arent done, and were 
getting more shovels ready. The future is very bright, and 
if you believe as I do that our society is trending green, I 
welcome you to come along for the ride as we continue to 
move clean energy forward.
Sincerely yours,
David Crane
President & Chief Executive Officer
January 31, 2011