To Our Shareholders
STEVE RENDLE BECAME VFS CHIEF EXECUTIVE OFFICER
ON JANUARY 1, 2017.
He succeeds Eric Wiseman who, after 21 years with the company and the past
nine years as CEO, remains with VF as Executive Chairman of the Board
of Directors. Together, they reflect on the companys performance in 2016, as
well as their expectations for the future and the smooth transition of leadership.
How would you describe and assess VFs performance in 2016?
Eric Wiseman: VFs global business model, diverse brand portfolio and operational discipline
helped the company deliver solid results in 2016, with strong performances from our
International and Direct-to-Consumer businesses offset by an inconsistent U.S. marketplace
for apparel and footwear. Revenue was in line with 2015, up 1 percent on a currency-neutral1
basis. 2016 earnings per share (EPS) from continuing operations was down 9 percent to $2.78;
however, adjusted EPS2 was up 2 percent to $3.11, or up 7 percent currency neutral. Were
particularly pleased that our strong gross margin and cash generation performance enabled
us to return a record $1.6 billion to our shareholders. In a mixed global marketplace, our
2016 performance was generally in line with our expectations.
What were the companys most important achievements?
EW: There were many, and they were varied. International revenue continues to grow, up
4 percent, or 6 percent currency neutral. For the year, Direct-to-Consumer revenue was
up 8 percent, or 9 percent currency neutral. Direct-to-Consumer revenue was 28 percent
of VFs total revenue in 2016, compared with 26 percent in 2015. And were pleased to report
that e-commerce continues to grow at a rapid pace  up more than 20 percent in 2016. Gross
margin  a key measure for us  improved by 20 basis points from benefits in pricing, lower
product costs and a mix shift toward higher-margin businesses. The Vans brand continued
its solid growth, up 6 percent worldwide and up 7 percent currency neutral, reaching
$2.3 billion. Its now our largest brand by revenue. And we were named to Corporate
Responsibility Magazines 100 Best Corporate Citizens List, which recognizes U.S. public
companies with standout corporate responsibility performance.
Steve Rendle: Our balance sheet remains strong, giving us ample access to cash to take
significant strategic actions. In addition, our commitment to fostering a diverse and inclusive
workforce achieved a perfect score on the Human Rights Campaigns Corporate Equality
Index, earning the distinction of Best Place to Work for LGBT Equality. We also announced
our partnership with the Paradigm for ParitySM? coalition, as we focus on achieving gender
parity throughout our company.
Will you continue to pursue business growth opportunities around the world?
SR: Absolutely. International growth represents a meaningful opportunity, both in the
near and long term. In 2016, our International revenue reached $4.6 billion and represented
38 percent of total sales. We anticipate that future growth in both mature and emerging
markets around the world will push that percentage even higher. Our international sales
will be driven by an expanding Direct-to-Consumer presence, especially across our
e-commerce platforms.
You began 2016 by indicating that as active portfolio managers you were taking a closer
look at VFs portfolio of brands. What did you accomplish, and what remains to be done?
EW: We were active on that front in 2016, and we continue to be. In March of last year,
we announced that we were exploring strategic alternatives for our Licensed Sports Group
business. And, in August, we completed the sale of our Contemporary Brands businesses.
We constantly assess the composition of our brand portfolio to make sure its aligned with
our strategic objectives and positioned to create growth and strong returns for our shareholders.
SR: Moving forward, well keep a sharp focus on this area. My goal is to make strategic
choices that leverage VFs differentiating business model and winning formula, while
exploring new opportunities and value pools. VF has demonstrated over time an ability
to transform our portfolio to fuel profitable growth. That will continue.
Do you prefer organic growth or growth through acquisition?
SR: Throughout our history, VF has been shaped by our ability to do both, and well continue
that approach. We will continue to drive value-creating, organic growth in our existing
brands by playing to our strengths: connecting with consumers throughout the world with
innovative products and experiences. But M&A takes patience. We wont make a move simply
for the sake of checking a box. Financial discipline will continue to guide our strategic choices.
What is VFs competitive edge?
SR: We have the best people in the industry working at VF. These associates, with their deep
knowledge of our consumers, provide our company with an edge unparalleled in the industry.
Our diverse portfolio of powerful brands and our differentiated products and experiences
also provide a competitive advantage.
EW: It starts with the talent of our people, whose passion and commitment enable VFs
achievements. Our powerful brands are supported by our strong business platforms. They
provide a common focus, enable sharing among brands and functions, and amplify business
solutions across the global organization. For example, our global Supply Chain organization
ensures that we service our brands by responsibly sourcing and efficiently delivering
products around the world. We create value across our company when we share knowledge
with one another. Its our One VF approach to business that creates synergies you wont
find in other competing business models.
Steve, what kind of company will VF be under your leadership?
SR: VF has a long history of evolving its portfolio and business operating model to compete
in an ever-changing consumer landscape. With an eye to the past and keen focus on the
present and future, we will continue to evolve and improve our strategic and business
disciplines to enable us to deliver value to our shareholders and consumers. I believe in
honesty and integrity. VF has operated with these values for the past 117 years. Its in our
DNA. I believe in the power of diverse teams. Diversity of experiences, backgrounds and
insights strengthens business results. I see an opportunity to elevate the power of design
and innovation. We own some of the worlds most beloved and iconic brands, and we have
an opportunity to elevate the impact of design on our products and across brand experiences.
Well be relentless in our pursuit of innovation. Lastly, I believe in the importance of agility.
Agile organizations are simply better prepared to succeed today and in the future.
Eric, your thoughts?
EW: Steve is absolutely the right person to have the honor of being VFs next leader. He has
demonstrated that he can both deliver results, and lead with passion and integrity. With full
credit to our Board of Directors, we have a long-standing, rigorous approach to succession
planning. It's one of the key ingredients in our longevity. When I became CEO in 2008, we
began working to identify and prepare the right person to succeed me when the time came.
That time is now, and Steve is the right person.
Steve, you can have the final word.
SR: I have an overwhelming sense of pride and optimism in the VF of today and tomorrow.
The rate of change within our industry and the broader consumer landscape is happening
at an accelerated pace. The proliferation of technology and innovation across all aspects of
our lives has shifted consumers shopping behaviors and elevated their expectations when
engaging with our brands. Our brands are well-positioned to compete in this new consumer
landscape. As an organization, we are pivoting to become more agile and consumer-centric
to better compete and win in this changing consumer marketplace. Were more than 60,000
people around the world committed to a shared vision and a clear set of business goals.
I speak for all VF associates when I say were grateful for where weve come from, and were
motivated to grow in 2017 and beyond. In doing so, well earn the opportunity to be called an
employer of choice, and well behave as a responsible corporate citizen in everything we do.
March 9, 2017