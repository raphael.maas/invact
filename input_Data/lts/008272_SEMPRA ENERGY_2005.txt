It was a pleasure working with Steve Baum over the
past two decades, and I am honored to follow Steve as
chairman and CEO. I take great pride in the fact that
we have developed and executed a successful strategy
to grow Sempra Energy into a global enterprise. I am
equally proud of the superior financial value we have
been able to generate for you, our shareholders, since
we created Sempra Energy in 1998.
The first phase of Sempra Energys evolutionto
develop robust businesses outside of our two California
utilitiesis complete. Our Sempra Global businesses
collectively contributed more than 70 percent of
Sempra Energys earnings in 2005.
This growth has been led by Sempra Commodities,
which now is the third-largest physical marketer of
natural gas in North America and a major international
broker of other commodities, including electricity,
natural gas, oil-related products and metals. In 2005,
Sempra Commodities achieved a record year, earning
$460 million in net income.
Our next evolutionary phase is to execute our ambitious
capital programcompleting the build-out of our liquefi
ed natural gas (LNG) receipt terminals, natural gas
pipeline and storage facilities, and electric transmission
and generation facilities.
We remain convinced that North America is facing
a severe, long-term shortage of natural gasa conclusion
now shared by most industry and government
forecasters. This is because the United States seriously
underestimated the popularity of natural gas as the
fuel of choice in power generation and manufacturing.
Over the past decade, the majority of U.S. power plants
built have been gas-fired, causing increased strain on
declining domestic natural gas supplies. A primary
solution to the problem is LNG. Other parts of the
worldAsia, the Middle East and Russiahave vast
natural gas resources, but need a market for their gas.
With LNG, that gas can be cooled and condensed into
a liquid, and shipped economically to distant markets.
By the end of this decade, we will be one of the largest
LNG importers in North America. Construction on
our Energa Costa Azul LNG receipt terminal in Baja
California, Mexico, is progressing well. When it
becomes operational in early 2008, Energa Costa
Azul will be the first LNG terminal on the West Coast.
We also have begun construction on our Cameron
LNG receipt terminal in Louisiana, despite a brief
interruption by the Gulf Coast hurricanes last summer.
Additionally, we have laid the groundwork for potential
expansion of both Energa Costa Azul and Cameron
LNG, subject to market interest. Our third LNG receipt
terminal project, Port Arthur LNG in Texas, is in the
final permitting stages and could come online as early
as 2010.
In August 2005, Sempra Pipelines & Storage
announced a keystone partnership with Kinder
Morgan to develop the Rockies Express Pipeline,
a major new transcontinental natural gas pipeline.
The 1,300-mile, 42-inch pipeline, connecting the
Rocky Mountains to gas-hungry markets in the
Midwest and Eastern United States, would be the
largest U.S. pipeline project in more than 20 years.
The LNG business in North America is creating new
opportunities for natural gas facilities to serve the
receipt terminals being built, so Sempra Pipelines &
Storage is developing trunk-line natural gas pipelines
near LNG hubs in Louisiana and Texas, as well as a
large salt-cavern gas storage facility in Calcasieu
Parish, La.
Sempra Generation is concentrating on efficient
operation of its Western power plant fleet of 2,630
megawatts. Due to the high market valuation of
coal-fired generation in Texas, in January 2006, Sempra
Generation decided to sell its 305-megawatt (MW)
Twin Oaks power plant in Texas for $480 million
a plant the company purchased for $120 million
in 2002.
Growth opportunities are not limited to the Sempra
Global businesses. Our California utilitiesSan Diego
Gas & Electric (SDG&E) and Southern California Gas
Co. (SoCalGas)are full-service providers again.
SDG&E will take control of Palomar Energy later this
year. Built by Sempra Generation, the 550-MW natural
gas-fired power plant is the first major new power
plant built in the San Diego region in more than 30
years. SDG&E also has added renewable energy to its
resource mixincluding solar and wind powerand
proposed a major new electric transmission line to
help transport these and other power resources to
the region. As always, SDG&E and SoCalGas remain
focused on improving efficiency, providing safe and
reliable energy service around the clock and controlling
costs for their customers.
Our commitment to the communities in which we
operate has never been greater. In 2005, we expanded
our outreach to support relief efforts for the victims
of the devastating Indian Ocean tsunami in late 2004
and U.S. Gulf Coast hurricanes last summer.
I look forward to the future with great excitement
and a certain amount of anticipationbecause of the
many opportunities before us.

Sincerely,
Donald E. Felsinger
Chairman and Chief Executive Officer