To Our Shareholders:
In fiscal 2016, Emerson made significant strides
to transform the company into a more focused
enterprise poised for stronger, long-term growth.
We completed a number of the vital strategic
initiatives outlined in last years letter to reshape
Emerson for the future, the most significant of
which was the implementation of our One Emerson
approach to realign our businesses and people into
two key business platforms: Automation Solutions
and Commercial & Residential Solutions. By taking
this important first step, we ensured Emerson is
ready and able to quickly adapt to ever-changing
market dynamics and return to growth as we
create broader and deeper solutions for our global
customers most complex challenges.
Our determination to create a new path to longterm
growth and prosperity in the quickly evolving
markets we serve became more evident in 2016.
We signed agreements to sell the Network Power,
Leroy-Somer and Control Techniques businesses at
favorable values; all while working to streamline our
total enterprise cost structure to best operate in a
continued weak, low-growth global environment.
Further, we acquired several strong and innovative
businesses that strengthen the core of our two
realigned business platforms. Much was accomplished
during the year by the entire Emerson organization
and there is much more work to be done entering
2017 as we rebuild sales and earnings momentum
and strive to return our sales level to more than
$20 billion per year.
Given the challenging market conditions and the
length of the industrial investment downturn that
impacted our businesses in 2015 and 2016, we
believe it is important to reiterate the strategic
framework we have designed to drive long-term
growth and shareholder value. We also believe it is
equally important to provide further transparency
into our strategic planning process and projected use
of capital so that all of our stakeholders have a clear
understanding of the path forward and why Emerson
will emerge stronger as a result of the significant
transformation of our portfolio.
Strategic Framework
Emersons strategic framework has been driven
historically by our focus on creating value, delivering
premium sales growth, increasing margins and
cash flow and geographic expansion, particularly
in emerging markets. The framework is set on a
firm foundation: that Emerson is a well-managed,
financially strong and highly focused enterprise with
a history of delivering strong total shareholder return
and 60 consecutive years of increasing dividends.
Our products, technologies, systems and services
hold leading positions in their served markets and
support a broad range of customers that include
some of the worlds most recognizable brands.
Our Automation Solutions business has strong
leadership positions in many critical industries
such as oil & gas, chemical, refining, power and life
sciences and is actively working to help customers
achieve top quartile performance in uncertain
times and rapidly changing market conditions.
Our Commercial & Residential Solutions business
provides global business-to-business and businessto-
consumer technologies that shape the industries
which advance energy efficiency and environmental
conservation, ensure human comfort and serve
and protect food quality and sustainability along
with other temperature-sensitive products such
as medicines.
As we execute our core strategy, we look to increase
our operating cash flow to above $3 billion once
again, for continued investment in both internal
programs and acquisitions while maintaining our
historical focus of returning cash to shareholders. By
focusing our investments in our core served markets
where we hold leading positions, we can tap into new
revenue streams, expand our product and solutions
offerings, drive margin growth and drive earnings
per share back to levels above three dollars. The
acquisition strategy will continue to focus on both
bolt-on and strategic acquisitions of products and
technologies that complement our current offerings
and/or expand our solutions capabilities. By doing so,
we are able to provide our global customers with the
most complete solutions.
Our recent acquisitions are tied closely to this
strategy. The acquisition of Pentairs Valves &
Controls business will allow us to expand the
geographic and product footprint of our already
broad automation portfolio. It also allows our Final
Control business to offer the most complete valve
solution available in the process market supported by
an extensive global service network. In addition, its
broad market exposure provides us the opportunity
to further enhance our position as an industry leader
in key served markets such as chemical, power,
refining and oil and gas.
Our Commercial & Residential Solutions platform
is comprised of market leading businesses and
technologies that are poised for growth in 2017,
supported by the expectation of improved end
markets. We will leverage this growth to fund
increased investments in unique technologies and
solutions as we continue to reshape these businesses
for long-term value creation. We will accomplish
this by growing our core market segments and
investing in innovation around energy efficiency
and sustainability, environmentally friendly
technologies, cold chain management and human
comfort. Initiatives like The Helix Innovation Center,
an investment we made during difficult economic
times, ensure we are well positioned to take a
leading role in shaping critical trends throughout
the industry. We will also work to leverage our vast
product domain knowledge in the air conditioning
and refrigeration markets in order to pivot and offer
more services and solutions to customers around
the world. Bob Sharps letter on the following pages
outlines some examples of our focus in these areas.
We have and will continue to work very closely with
our Board of Directors as we execute this strategy.
The Emerson Board is highly engaged and they bring
tremendous experience, insight and ideas to the
table as we debate the challenges and opportunities
ahead of us. Their support and willingness to actively
engage with management as we reposition Emerson
to enhance long-term value is one of the things
I believe makes our Board unique and gives us a
competitive advantage.
Our People
We will succeed and grow because Emerson has the
smartest, most talented and engaged people  up
and down the organization and around the globe.
Were heavily invested in training and developing our
people and ensuring they are challenged, supported
and positioned to win. I firmly believe that as we
continue to reshape the company and take the
necessary steps to grow our business, we cannot
accomplish this without the best and brightest
talent. We must continue to identify and recruit the
next generation of leaders and provide them with an
environment that encourages innovation, and new
ways of thinking and tackling challenges to ensure
Emersons long-term success.
Responsible Business and Ethics
This year, we enhanced our approach to
sustainability as we undertook a significant update
to our Corporate Social Responsibility report.
This report showcases our efforts to ensure our
operations are efficient and environmentally
responsible, an important issue for our Board,
employees, customers and shareholders. We
have long been committed to conducting our
business with the highest levels of integrity and
responsibility, and we take this charge very seriously.
The customers and industries we serve in process,
industrial, commercial and residential markets
provide key building blocks for critical human
progress. This new report provides background
on our efforts across Environmental Stewardship,
Integrity & Ethics, Corporate Governance, People
& Workplace, Supply Chain and Community
Involvement. I encourage you to read this report
which is available on our website.
2016 has been a successful year of change for
Emerson, and that change could not have happened
without the support, commitment and hard work of
all our employees around the world. I want to extend
a very special thank you to them for everything they
do, day after day, to make Emerson a great company.
I would also like to thank our Board of Directors
for their insight, guidance and active involvement
in the company and their support during this
transition. The Board has been instrumental in the
transformation process, and we could not have
achieved what we have in such a short amount of
time without their help and willingness to take new
and important risks.
On behalf of the Board of Directors and all of
Emersons global employees, we want to thank you,
our shareholders, for your trust and confidence in
us. I know we all share the same vision for a more
focused, profitable, growth-oriented Emerson, and I
look forward to working with you as we continue to
reposition Emerson for the future.
Thank You
David N. Farr
Chairman and Chief Executive Officer