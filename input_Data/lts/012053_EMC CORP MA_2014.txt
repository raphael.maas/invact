Dear Fellow EMC Shareholder,

History has shown that the advent of revolutionary new technologies brings about great disruption to existing

businesses and business models, while at the same time creating new, larger, opportunities in virtually every

industry. We are now in one of those times, and it is being driven by the accelerating adoption of new fundamental

technologiesin mobile devices, in social networking, in cloud computing, and in new ways to harness the power of

very large data sets, or Big Data.

As a result, virtually all businesses need to rethink how they engage their customers and partners, and build new

digital capabilities and business models. Many traditional industries are being redefined by digital upstarts that use

mobile applications backed by sophisticated algorithms made possible by the power of the cloud and Big Data. This

wave of disruption will accelerate as sensors get incorporated into every imaginable product or thing. Responding to

these opportunities will require enterprises to rethink how they do product development and how technology and

business can reinforce each other in more agile ways. This is not just a technology challenge but a cultural mindset

challenge, a major transformational challenge for many businesses.

As inevitable as these forces are, enterprises cannot walk away from their existing systems and applications. These

systems are and will remain critical to running their businesses for many years to come. Business transformation

therefore requires building new capabilities while enhancing, simplifying, and taking cost out of existing systems.

For EMC, 2014 was a year in which we positioned our business to lead on both of these fronts: helping customers in the

twin endeavors of renewing their existing systems and building fundamentally new capabilities. We did this by bringing

new, innovative technologies to market, investing wisely in our future growth, and returning value to shareholders, all

while extending our leadership in the markets we serve.

Last year, once again, we grew revenues faster than peer technology infrastructure providers and gained market share.

EMC consolidated revenue grew to $24.4 billion in 2014, an increase of 5% year over year. By contrast, worldwide IT

spending grew modestly by 2% year over year in 2014, according to industry analysts. On a GAAP basis, earnings per

weighted average diluted share were $1.32, and on a non-GAAP basis1

and in line with our plan. In 2014, EMC generated $6.5 billion in operating cash flow and $5 billion in free cash flow2

We believe our ability to grow faster than the market in 2014 reflects the strength of our strategy and the appeal of our

Federation business model, which includes our Pivotal, VMware and EMC Information Infrastructure businesses. This

structure provides customers with a differentiated choice of best-of-breed capabilities within an open, coordinated

framework, without the vendor lock-in of the past. Each business possesses a formidable competitive position alone,

and each is free and independent to build an open, world-class partner ecosystem. While providing choice to our

customers, our best-in-class capabilities across our companies allow us to do more for customers through integrated

solutions than any one business can do alone. Combined, our Federation entities become more relevant to customers

as their go-to strategic partner for large-scale transformations of data centers and enterprise IT environments. For sure,

our biggest orders in 2014 came from customers who want our businesses to do more for them in more integrated

ways, by working better together.

Pivotal, led by CEO Paul Maritz, has become a trusted partner for IT innovation, enabling enterprises to provide modern

software-driven experiences and business models. Pivotals ability to offer a combination of leading agile development

and data services, an open cloud platform and open suite of Big Data products is resonating strongly with customers

as they seek to transform. Pivotals two key subscription products, Pivotal Cloud Foundry and Big Data Suite, are off

to a strong start, securing an impressive array of customer wins in the first three quarters of general availability. I am

pleased to report that in the fourth quarter of 2014, Pivotals bookings exceeded $100 million.

VMware, led by CEO Pat Gelsinger, grew revenue in 2014 to $6 billion, an increase of 16% year over year. Over the last

two years, VMware has diversified its business significantly, building momentum in new product areas, from its vCloud

Air hybrid cloud service, to cloud management, network virtualization and mobile device management and security. In

the fourth quarter of 2014, a majority of VMwares license bookings came from products beyond its standalone server

virtualization product, vSphere, and these newer products grew nearly 40% year over year.

EMC Information Infrastructure (EMC II), led by CEO David Goulden, is the home of our core storage, data protection,

converged infrastructure, RSA security and enterprise content divisions. EMC II grew revenue to $18.2 billion in 2014,

an increase of 2% year over year, continued to gain market share, and is extremely well positioned to help customers

modernize their existing data centers and bridge them to next-generation data center environments. Within EMC II,

Emerging Storage revenue grew to more than $2.3 billion in 2014, an increase of more than 50% year over year. This

included XtremIO, the industrys number one all flash storage array and the fastest growing product line by far in EMC

history.

We are very pleased that VCE became the newest member of the EMC family of businesses, joining EMC II in December.

VCEs charter is to help customers simplify the deployment and management of hybrid clouds through a full range

of converged infrastructure offerings. VCE is the clear leader in a market segment growing 33% per year, according

to estimates from research firm IDC. In the fourth quarter of 2014, VCE achieved the seventh consecutive quarter of

demand growth above 50% year over year.

In all our businesses, we continue to invest heavily in innovative technologies focused on where we see the market

heading in the coming years. To guide these strategic investments, we have developed a dual innovation strategy

which allocates approximately 12% of annual revenues to research and development and approximately 10% of annual

revenues on average to acquisitions of emerging, leading-edge technology capabilities. Few competitors can match our

track record of success in acquisition integration coupled with organic innovation through R&D.

As a direct result of our dual innovation strategy, we have made significant investments in six new products and/or

business areas, namely: XtremIO all flash storage; rack-scale flash storage from DSSD, a company EMC acquired last

year; heterogeneous, software-defined, cloud-scale storage (including our products Elastic Cloud Storage, ScaleIO and

ViPR); mobile device management and security from AirWatch, a company VMware acquired last year; VMware NSX

network virtualization; and Pivotal, with its new cloud and data fabrics and rapid application development. We expect

combined revenue from these six investments to exceed $2 billion in 2015, an increase of more than 100% year over

year. We firmly anticipate that each of these six areas will contribute more than $1 billion in annual revenue in future

years. The investments we are making in leading technologies, the relationships and respect we have earned with our

customers, our skilled people and leaders, and our successful track record of building billion-dollar businesses, all

make me confident we will achieve these plans.

While making these investments, we are committed to a balanced capital allocation strategy and plan to continue

to return capital to shareholders through a combination of share repurchases and cash dividends. We ended 2014

with consolidated cash and investments of $14.7 billion, after we returned almost $4 billion to shareholders last

year through EMC share repurchases of $3 billion on top of $900 million in shareholder dividends. In January, we

announced our plan to once again return approximately $4 billion to shareholders in 2015.

Our Board of Directors is focused on creating sustained shareholder value and welcomed two new, top-notch members

in January: Jos E. Almeida, former Chairman, President and CEO of Covidien; and Donald J. Carty, retired Chairman

and CEO of AMR Corporation and American Airlines, and retired Vice Chairman and CFO of Dell. Joe and Don will add

valuable perspectives to our strong, active, and highly engaged Board.

I continue to serve as Chairman and CEO at the pleasure of the Board, EMCs employees and shareholders, and

will work closely with the Board to facilitate a succession of my responsibilities in a timeframe the Board feels

is appropriate.

I am very confident that EMC has a bright future ahead of it, not only because of the leading positions we have built

and the bold investments we are making, but also because of the loyalty of our customers, who last year rewarded us

with our highest net promoter score ever recorded. I am also very confident in our future because of EMCs strong bench

of leaders, our truly talented workforce of approximately 70,000 people around the world, and our high performance,

execution-focused culture. Great talent can work almost anywhere, and every day, our people come to work for EMC, a

company named a top 25 global Great Place to Work out of more than 2,800 multinational companies surveyed by the

Great Place to Work Institute last year.

EMC has been on a remarkable journey. I have never been more excited for our companys future as our journey

continues in 2015. Thank you for the confidence you have in EMC and in our future.

Sincerely,

Joseph M. Tucci

Chairman and Chief Executive Officer