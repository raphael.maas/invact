In 2006, we had the best
year in our 104-year history. Were ready for more.
Led by the outstanding performance of our
Crane Group, we achieved
our twelfth consecutive year
of record revenues. Sales
grew more than 30% to $2.9
billion. Net earnings before
special items increased by
139% to $176.5 million, or
$2.81 per share. We created
more than $100 million in
Economic Value-Added
(EVA)  double the previous record set in 1999.
Results like these dont
happen by accident, or
overnight. Our success results
from focusing on six strategic
priorities  all aimed at
creating value  and then
working our plan, year after
year. For 2007, we are adding
a seventh strategic priority 
World-Class Aftermarket
Service and Support  to the
set of imperatives that were
currently using as our roadmap
to the future.
Growth
Our fi rst priority is profi table,
global growth. We are
growing organically and pursuing
strategic acquisitions. In 2006, we
offi cially opened
a new 805,000-
square-foot crane manufacturing plant in China  three
times the size of our previous
facility there  that consolidates our position as the leading crane manufacturer in one
of the worlds fastest-growing
construction markets. The new
crane plant along with a new
ice machine manufacturing facility that we opened in China
a year earlier are the latest in
a series of initiatives that have
transformed our performance
and our prospects. In 1999,
5% of our sales originated
outside the United States.
Today, we not only operate
manufacturing and service facilities in 14 countries, but we
earn 48% of our revenues from
outside the United States.
The 15 acquisitions we have
completed since 1995 have
brought us leading positions
across many of our markets,
enabled us to do more for
customers, and allowed us
to leverage our manufacturing capabilities, distribution channels, and service
networks. In early 2006, we
announced the acquisition
of ExacTech to enhance the
heavy-machining capacity
within our crane business.
The acquisition of McCanns
Engineering, a supplier of
backroom beverage dispensing equipment, will help our
Foodservice Group gain greater control of its production
processes and improve profi t
margins. In January 2007,
we acquired a line of mobile
industrial cranes that complement our existing products
and give us a leading position
in that product category.
Innovation
Our ongoing target is to
generate 80% of our annual
revenues from products introduced within the past fi ve
years. Innovation adds value
to our products and brands,
while increasing both the size
of our markets and our market
shares. In 2006, we introduced
15 new crane products and
announced plans for an entirely new type of crane whose
revolutionary design combines
outstanding mobility with exceptional lifting capacity. Our
foodservice equipment business
introduced 28 new products
in 2006, including new
energy-saving ice machines
that helped us win a larger
share of that market. In addition,
our Marine Group launched the
fi rst of a new class of advanced
ships for the US Navy, the
Littoral Combat Ship, just 15
months after laying its keel.
Customer Focus
As a global company thats
easy to do business with, were
focused on doing more for our
customers. Their voices guide
the development of new products and services; by listening
to their requests at the start
of the product development
process, we greatly increase
the likelihood of our success
in the marketplace. Opening a
modern crane manufacturing
facility in China answered a
customer need for shorter delivery times of products destined
for the rapidly growing Asian
market. New beverage equipment products help convenience store operators capitalize
on the growing demand for
non-carbonated beverages. In
addition, the new ERP system
now being implemented across
our Foodservice operations will
streamline ordering, reduce
lead times, and allow for more
integrated service.
Excellence in Operations
To ensure that we achieve profitable growth, were pursuing
excellence in every aspect of our
operations. In 2006, we continued to widen the application
of Six Sigma quality practices
across our businesses. A new
Design For Six Sigma process
in our crane business helped to
signifi cantly reduce fi rst-90-day
warranty claims. Consolidation
of two walk-in refrigerator
plants is shortening lead times
and increasing production.
Re-engineering and refocusing
our marine business helped it
improve effi ciency and profi tability and win high marks
 and repeat orders  from
both government and commercial customers.
People and Organizational
Development
To be successful, a global
company needs a world-class
workforce. Since 2004, more
than 250 managers have completed one of our three leadership development programs.
In 2006, we implemented new human resources software and
a new employee intranet to
provide the common platform
needed to manage a global
organization. Our enterprise
values  integrity, commitment to stakeholders, and
passion for excellence  were
embedded in a new employee
code of conduct to help assure
the same standards of behavior across an increasingly
multinational, multicultural
workforce. And we havent
lost sight of the basics. A new
safety management system that
draws on the best practices of
all our businesses reduced total
lost-time accidents and cut
workers compensation costs by
more than 50%. Impressively,
our foodservice business had no
lost-time injuries in 2006.
Value Creation
An organization that grows,
innovates, cares about its
customers and employees, and
works productively will create
value. Thats what weve done.
The tremendous operating
leverage of our Crane Group,
combined with a steady contribution from the Foodservice
Group and improved performance at the Marine Group,
have produced record profi ts.
Our commitment to EVA
brings fi nancial discipline to
the company and helps ensure
that we make the best use of
our capital.
By concentrating on these
six priorities, weve achieved
exceptional results. Now, we
are adding a seventh priority:
to provide world-class aftermarket service and support for
all of our customers around
the world. We believe that
formalizing this long-standing commitment will increase
our value to our customers.
World-class service provides
another way to differentiate
our products and businesses
from those of our competitors.
World-class service will help
deliver new growth.
Looking Ahead
Our focus and commitment
will continue to carry the
company forward.
We expect conditions to
remain favorable through
2007 and beyond. With
growing demand in all major
segments and new opportunities in emerging countries,
the outlook for the Crane
Group is bright. Our Foodservice Group will continue to
produce consistent cash fl ows,
while new product introductions and the ongoing implementation of its new ERP
system will improve both sales
and profi tability. Our Marine
Group is booked for commercial orders into 2010 and is
faring well in the competition
for new vessels that support
our nations homeland security
and defense initiatives.
Given all of our success,
we still have more to do. We
will further intensify efforts to
manage rising material costs
and get the most from our
supply chain. Well also seek
to strengthen the protection
of our intellectual property.
Most of all, we plan to
deliver even more for our customers, shareholders, and employees. We will endeavor to
expand our global presence in
India, South America, Eastern
Europe, and the Middle East.
Guided by a proven, disciplined approach, we will continue to pursue acquisitions
in our foodservice and crane
businesses. We will continue
to innovate with products that
offer new features and benefi ts
and fi ll the remaining voids in
our product lines. Well continue to improve every aspect
of our operations, at the same
time that earlier investments
begin to pay off. Our growing
global service networks will
add to our revenue stream.
We also will continue to
develop the best people in our
respective industries.
In 2006, we mourned the
passing of Robert Becker,
who was the fi rst president of
Manitowoc Equipment Works,
(the predecessor of Manitowoc
Ice). Later in the year, we announced the retirement of Tim
Kraus, the president of our
Foodservice Group. At different times in our history, both
men helped to shape Manitowoc Foodservice into the
company it is today. We salute
their achievements.
We have never been more
successful, and we are ready to
do even more. I look forward
to sharing more good news
with you in the months ahead.
Terry D. Growcock
Chairman &
Chief Executive Officer