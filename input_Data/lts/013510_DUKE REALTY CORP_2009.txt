To our Shareholders,


2009 Re-Positioning
During the final quarter of 2008 and the first quarter of 2009, the
U.S. economy and capital markets experienced some of the most
difficult times of our generation. These conditions had a profound
effect on the commercial real estate business including our primary
product types of suburban office and industrial distribution. In
light of these circumstances, we took significant steps in 2009 to
assure that we protected the equity of our existing shareholders
and positioned our company to survive and grow in the future.
In January of 2009, we made the decision to reduce our dividend
in response to lower operating profits resulting from the significant
decline in the economy and also a need to conserve capital. In April
of 2009, we made another difficult decision to issue $575 million
of common equity at a share price well below where our shares had
traded for a significant period before the economic turmoil. While
this equity issuance diluted some of our existing shareholders, we
were certain that this action was necessary to preserve long-term
value and eliminate future capital market risk. Over 75 percent
of the new shares were issued to our existing shareholders.
As a result of our actions and sound business strategy, we are emerging
from the economic downturn as a company fortified, energized and
well-positioned to deliver increased shareholder value and future
profitability. Our confidence is based on the strategic plan we have
in place and the unwavering efforts of our dedicated associates.
Strategic Operating Plan
Our Strategic Operating Plan focuses on three major components:
Asset Strategy, Capital Strategy and Operating Strategy.
Asset Strategy � We strive to maintain the highest quality properties
in each of the markets in which we operate. To achieve this goal,
we constantly review each of our operating properties and look to
identify and dispose of those properties which no longer meet our
quality standards or our longer-term strategy. Today we are focused
on selling some of our Midwest suburban office assets which are
the older properties in our portfolio. We will redeploy the proceeds
of those dispositions into increasing our investment in industrial
distribution in key logistics markets in the United States and
building our medical office portfolio.
Capital Strategy � In 2009, Duke Realty moved aggressively to
strengthen its balance sheet. Following the better part of two years
during which access to capital was extremely limited, we took advantage
of improved conditions that began last spring, generating more than
$1.6 billion through a combination of secured and unsecured debt,
new equity and the sale of land and non-core properties. We further
improved our liquidity by successfully renewing and extending our
operating line of credit at $850 million through early 2013.
As a result of these successful capital-generating activities, we entered 2010
with nearly $150 million in cash and no balance on our line of credit.
We are well-positioned with minimal debt maturities until late 2011.
Operating Strategy � Our Operating Strategy adapts to the world
around us. In 2009, our new development starts were the lowest in
the sixteen years since we have been a public company as a result of
the lack of demand in the markets. Today we are focused on leasing
up the vacant space in our existing buildings to generate additional
cash flow for our shareholders. In our recently developed properties
alone, we can generate an additional $40 to $45 million of net
operating income by leasing them to stabilization.
In 2009, we completed a variety of significant lease transactions.
All told, we leased more than 22 million square feet of space during
the year, including 6.8 million square feet in the fourth quarter.
At the end of 2009, our wholly owned development pipeline
consisted of only four properties, comprising 660,000 square
feet, which were 97 percent pre-leased. 

In spite of the intense pressure on businesses everywhere, we
concluded the year with an occupancy rate of 87.8 percent in our
overall portfolio. Our lease renewal percentage for 2009 was an
outstanding 79.4 percent, a tribute to the quality of our properties
and our operation team�s diligence and service to our customers.
During 2009, we also made the decision to discontinue our operating
business of developing assets to immediately sell, known as the
�merchant building� business. As a result of this decision, we decided
to market for sale about one-third of our current land inventory rather
than hold it for future development. This change in strategy required us
to reduce the carrying value of the land we intend to sell to estimated
current market prices. This adjustment totaled about 30 percent of the
carrying value and is reflected as a charge to our 2009 net income.
2010 and Beyond
While optimistic over the longer term, we are cautious in our outlook
for 2010. We believe the recovery in the U.S. economy will be slow
which will make fundamentals in both the industrial and office
business challenging. We will likely see lower average occupancy levels
and pressure to reduce rental rates on space we lease to our customers.
Duke Realty has honed its long-term strategy, refining the asset classes
and geographic regions in which we will grow. We also have outlined
a plan for further differentiation within our portfolio that is driven by
increasing our investment in medical office, further building upon our
strong industrial base and reducing our suburban office investment.
Our growth efforts will be targeted to those geographic regions
with expanding economies or where demographic trends indicate a
need for development. A key part of our strategy continues to be our
logistics initiative, with a goal of providing industrial distribution
space at key points along the supply chain, including those near
seaside ports and multimodal locations and other hubs.
The medical office segment of our business also is an area of future
growth. An aging population and the shift from inpatient hospitals
to outpatient care facilities for medical treatment will drive a need
for additional medical office space in the years ahead.
In conducting our business, we will continue our long-standing
practices of being honest and forthright with our customers and
a good corporate citizen in the communities in which we operate.
Our ranking by Forbes in 2009 as one of the 100 most trustworthy
companies is reflective of our ethical approach to business. In every
market, our associates show their commitment to their community
by donating their time and expertise, as well as lending financial
support, to worthwhile causes.
A significant element moving forward is the strong leadership team
we have at Duke Realty. Our senior leaders average more than
twenty years in the real estate business, and most have been with
Duke Realty for many years. It is this team that sets the strategic
direction of the company and manages our daily operations. I�d also
like to express my sincere thanks to Bob Chapman who stepped
down as our Chief Operating Officer at the end of the year and was
a key part of our success over the past ten years.
I would also like to thank our Board of Directors for its guidance
and counsel during these challenging times over the past two years.
I would particularly like to thank Ben Lytle who is leaving our board
after fifteen years of service. Ben has been an invaluable mentor and
counselor to me as lead director during my tenure as CEO.
Thank you as shareholders for your confidence in and support
of Duke Realty.
Dennis D. Oklak
Chairman and Chief Executive Officer