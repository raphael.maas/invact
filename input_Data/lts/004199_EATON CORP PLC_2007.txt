To Our Shareholders:
Eatons integrated diversification strategy is working, helping us
to deliver record revenues and earnings for the seventh year in a
row. We are tremendously proud of that accomplishment, especially
given the challenges of a business environment in which
financial and economic volatility have become household words.
We entered 2007 facing a stiff headwind, as North American
heavy-duty truck markets were projected to decline by more than
40 percent due to the changes in the U.S. diesel emissions standards.
That management challenge became even tougher as the
year went on as generally upbeat economic forecasts fell under
the dark cloud of a credit crisis in global markets.
Despite these hurdles, Eaton continued to deliver outstanding
performance, demonstrating the effectiveness of our business
model throughout all phases of the economic cycle. As the world
continues to change, the implications are clear: Businesses must
become more nimble and have a broad base of diversification. At
Eaton, thats exactly the kind of company weve become.
Our Results Speak For Themselves
We continued to significantly outgrow our end markets in 2007,
setting new all-time records in both revenues and profitability. For
the first time in Eatons modern history, Eatons profits increased
in a year in which the North American heavy-duty truck business
declined. During the year, North American heavy-duty truck markets
declined by a whopping 44 percent while our fully diluted
EPS grew by 6 percent.
Among the years financial and operating highlights:
 Our sales surpassed $13 billion for the first time in our history,
growing by 7 percent.
 For the seventh consecutive year, our revenue growth outpaced
the growth in our end marketsby $235 million in 2007.
 We completed or announced 11 acquisitions during the year
primarily in our Electrical and Fluid Power businesses.
 We increased operating earnings per share by 8 percent to
a record $6.90.
 We generated $1.16 billion in cash from operations.
 We increased our dividend by 10 percent, and then again
increased our dividend by 16 percent early in 2008.
 We repurchased $340 million of our stock.
 We delivered a 22 percent return on shareholders equity,
again placing our return near the top of diversified industrial
companies.
 And our all-in shareholder return was 31.5 percent!
This performance is not the result of a one-time event or a simple
turn of good fortune. It is the product of a long-term strategy
and the hard work and dedication of a team of employees who
live and breathe that strategy every day.
Diversification Is Paying Off
Since this decade began, Eaton has been on a mission to achieve
diversification across three key dimensions: business balance,
geographic balance and balance through the three phases of the
economic cycle. That difficult groundwork paid off handsomely
in 2007 as we continued to move closer to our targets:
Business Balance
As a result of our organic growth initiatives and success with targeted
acquisitions, approximately 70 percent of our revenues are
now concentrated in the higher-growth electrical and fluid power
industries. Our announced acquisitions of The Moeller Group and
Phoenixtec, which are expected to close in early 2008, will move
this balance ever closer to 75 percent of total revenues in 2008
Geographic Balance
In 2007, for the first time, approximately 50 percent of Eatons
revenues were driven by economies outside the United States.
Never before has this global balance been more important.
During 2008, when the U.S. economy is expected to underperform
international economies, our acquisitions of The Moeller Group
and Phoenixtec will further expand our base of international
revenues to approximately 55 percent.
Balance Through the Economic Cycle
We have balanced our business portfolio to ensure that we have
large globally capable businesses that afford Eaton the opportunity
for consistent performance throughout the economic cycle. In 2007,
we achieved balance in our portfolio with approximately one-third
of our businesses being early-cycle, one-third mid-cycle and onethird
late-cycle businesses.
In tegrated Power
Diversification is not enough to build an enterprise capable of
sustainable earnings growth on its own. And that is where the
Eaton Business System provides unique value in our overall strategy.
Now in its eighth year of implementation, EBS continues to
provide the disciplined set of processes and tools that ensure
organization-wide alignment and compliance, rapid recognition
and transfer of best practices, the framework for successful
and rapid integration of acquisitions, and the framework and
resources to help our employees focus upon continuing learning
and career development.
In a global environment of higher energy costs, rising emissions
standards and the search for alternative energy sources, Eaton
remains focused upon power managementthe efficient, safe
and sustainable applications of electrical, fluid and mechanical
power. From a technology point of view, Eaton is increasingly
well-positioned in the sweet spot of activity with our customers.
Building on Strengths
We made exciting progress in enriching our technological
strengths and extending our geographic reach during 2007. The
continued environment of high energy prices, the increased
focus upon sustainable business strategies and the pace of global
growth make our progress in each of these dimensions all the
more important for our customers.
Our Electrical Group has emerged as Eatons largest business,
notching a successful year in many dimensions. We completed
or announced eight acquisitions:
 Power Products Ltd., bolstering our service capabilities in the
Czech Republic.
 SMC Electrical Products, Inc.s industrial medium-voltagedrive
business to expand our power distribution and control
assemblies portfolio.
 Aphel and Pulizzi to expand our capabilities in power distribution
units for the power quality market.
 MGEs small systems business to strengthen our low-end
uninterruptible power supply (UPS) products capabilities and
expand our European distribution channels.
 Babco to expand our capabilities serving the fast-growth oil
sands industry in Canada.
 The Moeller Group to expand our International Electrotechnical
Commission (IEC) product offerings and European distribution
channels.
 Phoenixtec Power Company Ltd. to strengthen our global UPS
business and significantly strengthen our channels in Taiwan,
China and Eastern Europe.
Together, the small-business-systems business of MGE, The
Moeller Group and Phoenixtec Power Company represents a
major addition to the strategic capabilities of our Electrical Group.
At the same time, our electrical team continued to capitalize upon
the strong non-residential construction markets in North America,
successfully introducing our FlashGard technologies into motor
control centers and switchgear. The strong demand for more
power-efficient technologies made receptive markets for our
industry-leading energy-efficient new line of UPS equipment.
And our vacuum interrupter-based medium voltage technologies
hit the mark as the green alternative to more traditional arc interruption
technologies employed by many of our competitors.
In Fluid Power, our aerospace business continues to grow briskly,
driven by many introductions of new commercial, business, general
and military aircraft. Eaton is now a major supplier of hydraulics,
pneumatic and fuel systems on commercial and military
aircraft. Our fuel systems capabilities were enriched by the addition
of Argo-Tech, which we acquired early in 2007. Eatons development
and commercialization of hydraulic hybrid technologies
continues to win important new contracts with our customers.
The addition of Arrow Hose and Tubing has broadened our fluid
conveyance capabilities and access to rapidly growing food and
beverage end markets.
Increasing emissions, fuel economy and safety regulations provide
important marketplace demand for our truck and automotive
businesses. Eatons industry-leading diesel-electric powertrains
for commercial vehicles are now in use in Asia, North America
and Europe. Our innovative new-generation superchargers are
redefining the rules in air boosting for both gasoline and diesel
engines. And our acquisition of the fuel components division of
Saturn Electronics & Engineering, Inc. broadens our fuel vapor
emissions-control capabilities.
Raising the Stakes
Volatility opens new opportunities but at the same time can
cause hesitationthe understandable hesitation that emerges
from a lack of certainty in the environment around us. While we
appreciate the increased risk that accompanies volatility, we
remain confident of the fundamental capabilities of our enterprise
and our strategy for success.
Convinced of our planand confident in our peoplewe concluded
that December 2007 was the right time for Eaton to take
the next significant step in our evolution. That step was the bold
simultaneous announcement to commit $2.5 billion to acquire
The Moeller Group and Phoenixtec Power Company. These two
acquisitions fundamentally reposition our electrical business as
a leader in both global distribution and control and power quality,
with a significantly enhanced product portfolio and geographic
reach. The addition of approximately $1.9 billion of incremental
revenues in 2008 and some 14,000 new employees also dramatically
changes the size of our enterprise
Maintaining our Values
Eatons rapid and steady growth brings with it exciting new challenges
and opportunities for our customers, our employees, our
suppliers and the communities in which we do business. While
change is an undeniable partner to growth, we remain fully committed
to our overall philosophy of doing business right.
Every day the 64,000 Eaton employees around the world make
individual decisions that strengthen our reputation and send an
important and consistent message to our partners about doing
business righteverywhere we do business. This demonstration
of our core values is as fundamental to Eaton as our name, and
will continue to be one of the strongest ways we differentiate our
company to our many stakeholders and the world.
As you read this years annual report and reflect on its title, The
World Wont Wait, know that you have our commitment that we
will continue to move forward swiftly, responsibly and in a more
focused manner than ever on delivering superior performance to
our customers and shareholders alike. On behalf of our entire
Eaton team, thank you for your continued support.
Alexander M. Cutler
Chairman and Chief Executive Officer
