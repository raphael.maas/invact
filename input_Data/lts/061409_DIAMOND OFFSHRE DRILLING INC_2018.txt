TO OUR
SHAREHOLDERS

Oil and gas prices remained depressed throughout 2018. After reaching multi-year highs
in early October, U.S. crude prices ended the year down 25% on the back of rising output
from North American shale oil and fears an economic slowdown could weaken oil demand.
Despite significantly improved cash flows, our clients remained largely focused on
short-cycle investments, including shale and other onshore programs. As a result,
offshore rig utilization and dayrates remained at trough levels.
Despite those challenging conditions, in 2018 Diamond Offshore added $647 million
to our backlog, began the reactivation of two rigs, set new company records for operational performance and safety, and added two new innovations to our portfolio of
differentiating technologies.
LEADING INNOVATION
Throughout the year, Diamond Offshore continued our focus on innovation and thought
leadership to improve efficiency, customer service and safety across the offshore industry.
In April 2018, we introduced our Sim-Stack� service, the industry�s first cybernetic
blowout preventer (BOP) service. Using a highly sophisticated and complex virtual replica
of a BOP system, this new service performs advanced digitalization, simulation and
data fusion to improve efficiencies, lower non-productive time and reduce the cost
of deepwater drilling.
With our Sim-Stack service, we can continuously and accurately assess BOP status and
regulatory compliance after a single or multiple component failure has been identified.
When issues arise, Sim-Stack immediately provides critical feedback, without human bias.
With this information, we can determine a proper course of action while providing a thirdparty statement of fact to the operator and regulatory bodies. Whereas the traditional
process can take days or even weeks to reach consensus, with Sim-Stack, this can now
be accomplished in a few hours or less.
An analysis of how our Sim-Stack service would have performed on past unplanned stack
pulls suggests that utilization of Sim-Stack has the potential to reduce our subsea
non-productive time by upwards of 35%.
Within just six months of deployment on our drillships in the Gulf of Mexico, Sim-Stack
enabled us to avoid four unplanned stack pulls, which resulted in the preservation of
an estimated $9 million of lost dayrate.
We are working to implement this service on other rigs in our global fleet over the
coming quarters.
In June 2018, we launched our Blockchain DrillingTM service, the first publicly disclosed
blockchain technology application in upstream oil and gas. In blockchain, �blocks� � or
digital pieces of data � store information about transactions and about the participants
in those transactions. Blockchain is distributed � meaning it creates a shared system of 

record, or ledger, among participants. It is scalable � therefore, massive amounts of data
can be recorded and shared. And blockchain data is immutable � as a result, it can be
recorded and distributed, but not edited.
When applied to offshore drilling, blockchain technology enables all participants in the
entire value chain to plan, track and report across the well construction and drilling
lifecycle. Providing our oil and gas operator clients with 24/7 access to this information
drives efficiencies and enables them to lower the total cost of ownership.
In addition, our Blockchain Drilling service has the potential to eliminate invoicing
waste and automate invoice reconciliation. And it can improve logistics and safety
by optimizing marine traffic and reducing the number of crane lifts onboard the rig.
We began collecting data onto a blockchain platform from each of our drillships in the
fourth quarter of 2018 and plan to have this capability available across the fleet in the
coming year.
OPERATIONAL PERFORMANCE
We constantly look for innovative ways to reduce downtime while enhancing operational
efficiencies for our customers.
I am pleased to report that in 2018 we set a new Company benchmark and delivered
significant improvements in unplanned subsea downtime, where subsea reliability
exceeded surface reliability � making it a first in the history of the Company.
Through ongoing process improvement efforts, we continued to set new drilling efficiency
benchmarks on our drillships, without sacrificing safety. One of our drillships completed
a well in 48 days versus the planned 70 days. Another drilled and completed a well 54 days
ahead of schedule. And yet another reached 28,000 feet after only 38 days.
Diamond Offshore drilled three of the four most cost-effective wells in the Gulf of Mexico
based on time-to-depth, and as a result, helped to deliver a large offshore deepwater
project $1.2 billion under budget and six months ahead of schedule. This type of performance is best-in-class and only further reinforces Diamond Offshore�s already strong
reputation across our client base.
We will continue to pursue ways to make offshore drilling more economic for our clients
while best positioning Diamond Offshore for the eventual market recovery.
SAFETY PERFORMANCE
Operating safely is of paramount importance, and 2018 was our safest year on record.
We delivered the lowest total recordable incident rate (TRIR) and highest number of
zero incident operation (ZIO) days in Company history.
I attribute our success in this area to the talent and commitment of our operational
and technical teams, our culture of safety and our safety-enhancing services such
as Sim-Stack, whose underlying technology received designation as a STAR (Safety,
Technology and Review) initiative from the regulator in the Gulf of Mexico as being one
of the industry�s best available and safest technologies.
I am also pleased to report that for the second consecutive year, Diamond Offshore
was recognized by the International Association of Drilling Contractors for having the
best safety performance in the North Sea in the floating-rig, under-100-million-manhours category.
CONTRACT ACTIVITY
Despite the continued market downturn, our ability to create differentiation in a
traditionally commoditized market enabled us to secure $647 million in net additional
backlog in 2018. 

During the year, we secured an additional five years of backlog for our drillships, the
asset class that is most distressed, with major operators BP and Anadarko � and at
rates significantly above the prevailing market. We are unique among our peers in that
we are the only major offshore player with all its sixth- and seventh-generation assets
under contract.
When we released our first quarter 2018 results, we announced plans to reactivate
the cold-stacked Ocean Endeavor after being awarded a contract that will keep the rig
working in the North Sea for at least two years. When we released our full year 2018
results, we announced plans to upgrade and reactivate a second rig, the Ocean Onyx,
which will work for Beach Energy for a one-year contract offshore Australia. Following
the upgrade, the Ocean Onyx�s operational life will be extended for many years.
We see the reactivation of these two formerly cold-stacked assets as a positive sign
that operators are reinvigorating their drilling programs in anticipation of an eventual oil
and gas industry rebound. We also view this as a vote of confidence in Diamond Offshore
in what is still an oversupplied and highly competitive market.
FINANCIAL RESULTS
For the full year 2018, we reported a net loss of $180 million, or $(1.31) per diluted share,
compared to net income of $18 million, or $0.13 per diluted share, in 2017. Contract drilling
revenue was $1.1 billion compared to $1.5 billion in 2017.
Given the uncertainty around the timing of the recovery, in October 2018, we proactively
improved our liquidity position by establishing a $950 million credit facility, which matures
in fourth quarter 2023. This new facility, combined with our existing credit facility,
provides us with over $1.25 billion of liquidity until 2020.
SUMMARY
2018 was another tough year in the floater drilling market. Nevertheless, we stayed our
strategic course, developing and deploying innovation to lower the total cost of ownership
for operators in offshore drilling while setting new Company records for safety and
operational performance.
We believe the moored market has hit bottom. 2018 saw tightening and modest improvements in both North Sea and non-harsh markets, resulting in incremental dayrate
increases. Clients are looking to lock in capacity for 2020 and beyond.
We believe the dynamically-positioned (DP) floater market has also bottomed, as have
DP dayrates. But we don�t expect these dayrates to recover before the next decade,
as the DP segment is still suffering from significant oversupply.
With a solid backlog, a superior balance sheet and strong liquidity, we are well
positioned for sustainable success in the eventual market recovery.
We will continue to invest our capital conservatively, but have the financial means to act
quickly on strategic opportunities should they present themselves. We are confident
the long-term fundamentals of the oil and gas industry � and particularly the deepwater
drilling sector � remain intact. Oil and gas operators will eventually resume their essential
deepwater campaigns � and we will be ready when they do.
Thank you for your continued confidence in Diamond Offshore.

Marc Edwards
President and Chief Executive Officer