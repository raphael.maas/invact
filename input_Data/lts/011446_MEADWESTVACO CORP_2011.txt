To our shareholders, customers and employees:
At MWV, we continue to change the conversation  about our financial performance,
our progress in targeted markets, and our place in the global packaging industry.
We have been very successful in each of these areas, and were confident that
the best is still yet to come.
Another year of record segment operating results in 2011 came directly from the
strategies weve laid out for profitable growth and value creation. We are gaining share
in growing end markets. We are winning with innovative products. We are extending
our leading positions in emerging markets. And, as a result, we are delivering very
strong financial performance and will continue to be a reliable source of earnings and
cash flow growth in the coming years.
As we executed our plans last year, we were building on a very strong foundation and
measuring ourselves against a high bar of performance. We expected to continue at
the same pace even in a tough economic environment that deteriorated further toward
the end of the year. In 2011, income from continuing operations was $258 million, or

$1.49 per share  nearly the same as our record level from the previous year.
As a result, our stock outperformed the S&P 500 benchmark by a wide margin.
Of course, this strong performance does not mean that we are immune to
broader trends in the global economy, including weak housing and labor
markets in the U.S., and  most recently  the fiscal crisis in Europe. These
acute macroeconomic events resulted in lower consumer confidence, tighter
inventory management, and softer demand in some of our markets  especially
toward the end of 2011. However, our markets have held up fairly well
through a challenging global economic environment, and we have made
smart decisions, taken swift actions, and focused on the right strategic areas
to strengthen our market position and deliver the level of performance our
stakeholders expect.
That includes the announcement that we will spin off our Consumer & Office
Products business, and merge it with ACCO Brands. The combined company
will be a $2 billion leader in the global school and office products market, and
the transaction will create substantial value for MWV shareholders when it
closes in the first half of 2012.
We are confident that executing the plans we can control will continue to
strengthen our market position and buttress our financial performance during
this challenging economic climate.
Heres some evidence:
MWV has deep market expertise in our targeted categories and a growing
number of truly strategic partnerships with our customers, such as Anheuser-
Busch InBev, Coca-Cola, and Johnson & Johnson. This is how we drove a six
percent increase in revenue in 2011, and will continue to generate profitable
growth upwards of five to ten percent over the next several years.
MWV has developed a number of innovative packaging solutions, and continues
to be a partner of choice for brand owners who value our insight-driven and
technology-enabled approach to innovation. This is how we have established
a richer mix of products, preserved our value-based pricing to offset input
cost inflation, and increased our margins compared to the prior year.
MWV has established strong positions in emerging markets with the fastest
growth rates and most promise for new packaging formats and modernized
retail. This is how we increased revenue by ten percent in emerging markets
in 2011. We will continue to make our support of customers growth ambitions
in these markets a larger portion of our overall revenue base.
MWV has also maintained our strong operating discipline, and implemented
an Operational Excellence program across our manufacturing system around
the world. This is how we have generated ever-higher levels of profitability,
and will continue to deliver three to four percent gross operating productivity
each and every year.

These drivers of our performance are true in each of
our businesses. And while our large packaging business
continues to be an earnings engine for MWV, we also
had an especially strong performance in Specialty
Chemicals in 2011. The scientists and marketing experts
who run this business have increasingly shifted our
product mix toward more innovative and value-added
chemical formulations for global automobile, asphalt
paving, adhesive and oilfield drilling markets  and
theyve continued to set earnings records in the process.
Our Specialty Chemicals business is a pacesetter for
MWV in terms of innovation, expansion in emerging
markets, commercial excellence and other areas. And
the business is aligned with market megatrends, including
the global push for lower air emissions and increased
infrastructure investment and energy exploration.
In all of our businesses, we think of ourselves in the
context of the markets we serve  and the range of
solutions we provide. And the source of our strength 
last year, this year, and beyond  is in the increasingly
important conversation we are leading about the global
packaging market. Its a conversation that is vitally
important to brand owners, to consumers, to retailers, to
the supply chain, to the environment, to the modernization
of growing economies. We are in the middle of that
conversation because of our deep market expertise,
strong relationships with consumer goods companies
and brand owners, and the ability to develop innovative
solutions all around the world.
Were listening to what consumers want from product
packaging and what they expect from their favorite
brands. Were opening a new dialogue with our customers
based on these insights, and based on our
ability to translate them into the local needs of emerging
markets such as Brazil and China. And were
responding to what our customers need to win in their
markets by developing innovative solutions and adding
new materials and capabilities to our global platform.
Thats not just the language of packagingit is the
way were delivering profitable growth.
We expect to become the most commercially proficient
company in our industry by establishing strong partnerships
with the worlds largest consumer products
companies, and by demonstrating the commercial
skills we need to earn a larger share of their packaging
spend. A good example of this focus on commercial
excellence is our work with Johnson & Johnson. Weve
leveraged our 30-year history as a supplier to J&J to
become one of only a handful of strategic packaging
partners for their global business.
We are developing insights about the types of packaging
that drive consumer behavior and brand preference,
and then using those insights to accelerate and
augment our pipeline of new solutions. A good example
of our investment in this insight-driven approach to
innovation is the development of Captivate for the

retail food and beverage packaging market. Captivate
can revolutionize grocery categories that are hard-tostock,
hard-to-shop, and hard for brands to stand out on
the store shelf.
We are using our existing leadership positions in many
developing geographies to translate our market and
consumer insights for the growing middle classes in
places like Brazil and China. In fact, were in the center of
the conversation about the right packaging solutions for
these emerging markets, such as the corrugated packaging
solutions we developed that reduce fresh produce
spoilage between the farm and the marketplace in Brazil,
or that reduce the cost of transoceanic beef shipments
from Brazil to China or Europe.
We are responding to what our customers want by adding
new materials and capabilities to our repertoire, and then
leveraging these adjacencies across our global platform.
This expanded participation in the marketplace includes
the addition of Polytops cap and closure technologies to
our existing business in food, beauty and personal care,
and home and garden markets around the world.
These are the things were doing to bring the full impact of
our packaging expertise to each of the markets weve
targeted, and each of the customers we work with. We
think that leading the conversation will help MWV shape
the future of the packaging industry and make our com-

pany an attractive investment for shareholders  allowing
us to achieve up to ten percent real annual growth and
consistent top-quartile financial performance, including
our very strong dividend.
For the first time in more than a decade, MWV will be
pursuing these goals without the counsel of Robert
McCormack, who is retiring from our Board of Directors.
We have also added two new directors, Alan Wilson and
Gracia Martore. Both of these distinguished leaders work
for companies with deep consumer insights  McCormick
and Gannett, respectively  and will bring valuable
backgrounds to help guide the future direction of MWV.
They are joining the best collection of talent in the packaging
industry. MWV will excel because our organization
is strong, our strategies are sound, and  quite simply 
our company is comprised of truly great people. This
talented team is MWVs force for growth  and I am proud
not only to work with them, but to report our success on
their behalf.
Sincerely,
John A. Luke, Jr.
Chairman and Chief Executive Officer
February 27, 2012