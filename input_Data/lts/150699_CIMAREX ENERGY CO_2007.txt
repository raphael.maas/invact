Fellow Stockholders,
For Cimarex, 2007 was a year of progress. Improvements which
were made will move us ahead in 2008. We increased our
exposure to new plays and grew our drilling program as the year
unfolded. An important contributing factor to this growth was
an expansion and refocusing of geo-science personnel.
Some of our new geologists, geophysicists, engineers and landmen are seasoned and others are young, but together they have
enhanced our capacity to exploit our existing properties and
develop new ideas. Combined with some reorganization, these
new people have energized our exploration program. We are
now pursuing several new horizontal drilling projects and
unconventional resource plays.
Cimarex continues to rely on drill-bit driven growth and had
another very profitable year. Net income hit a record $347
million. Cash flow from operating activities increased 13% to
an all-time high of $995 million. We exited the year with a debt
to capitalization of 13%.
Operationally, we also performed well. We invested $983 million
and drilled 452 wells. This program added 300 Bcfe of proved
reserves and replaced 182% of 2007 production. After adjusting
for 123 Bcfe of property sales, total proved reserves increased
11% from year-end 2006 to 1.47 Tcfe.
Going forward, we will continue to balance our higher-risk
seismically-controlled Gulf Coast drilling efforts with our moderate
risk Mid-Continent and Permian programs. There will also be a
sprinkling of new higher-risk, emerging unconventional resource
plays. As always, most of the capital will go towards moderate
risk projects and all will be measured and tracked by our internal
systems. These controls provide early and continuous feedback
allowing for constant adjustment and adaptation.
Refocusing and reorganizing had an important role in our 2007
programs and will strongly influence future years. We split
the old Mid-Continent region into two separately managed
units, Oklahoma and the Texas Panhandle. This was necessary
because of our growing southern Oklahoma, Arkoma Basin
and Anadarko Woodford programs. Just as important was the
increase in our Texas Panhandle Granite Wash activity, which is
now our single largest project.
In the Permian Basin, between relocations and new hires, Midland
has become our central office. This was driven by our ability to
attract and retain critical talent and our growing exploration and
development program, including horizontal drilling for oil in the
Bone Spring and Wolfcamp formations.
In the Gulf of Mexico, we sold our Main Pass Area operated
properties and plan to complete our offshore exit in 2008.
Our onshore Gulf Coast operations remain anchored on our
very active Yegua/Cook Mountain play. We earn our highest
economic returns in this area and plan to continue its expansion.
We changed our Western region groups name and function
to New Ventures. It operates without geographical limits and also functions in support of our other regions lending technical
assistance in unconventional plays.
We expect an active 2008 drilling program. We plan to invest
$1.1 $1.3 billion, funded from cash flow and cash on hand.
Our current 2008 outlook is for total company production to
grow to 465-485 MMcfe per day, which is 5 10% higher than
2007 after adjusting for property sales.
We have to constantly adapt to volatile prices and a changing
industry environment, but we will not waver in our basic
approach. We will continue to execute the strategy set forth
from our beginning more than fifteen years ago: to grow proved
reserves and production through the drill-bit, maintain a low debt
level and generate strong returns on our drilling investment.
We look forward to the challenges and opportunities of 2008.
Thank you for your continued support.
F.H. Merelli
March 4, 2008