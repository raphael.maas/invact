Fiscal 2005 was a transformational year for Mylan Laboratories 
and the generic pharmaceutical industry. We faced challenges on 
a number of fronts. Many of the trends and issues that we predicted would impact the generic industry converged as we expected over the course of the year. Despite these challenges, we are 
proud to report that Mylan remains one of the largest U.S. manufacturers of generic prescription medicines, and one of the 
world's top suppliers -- branded or generic -- of prescription medications. While industry challenges did negatively impact our 
financial results, we were able to achieve the upper end of our 
revised financial guidance and surpassed $1 billion in revenue for 
the fourth consecutive year. 
Mylan has an impressive long-term history of significant revenue growth and stock appreciation, including a 17% compounded annual growth rate for revenues over the past 20 years. 
We've achieved this incredible long-term success in spite of multiple challenging generic 
industry business cycles. As this chart illustrates, however, every time Mylan encountered a 
difficult cycle, we were able to emerge stronger than before. 
We believe we are well positioned to emerge from this latest challenging cycle. Our senior 
management team and Board of Directors have extensive generic industry experience that 
enabled us to recognize and prepare for the latest challenges. We made strategic investments 
in key areas of the company over the past several years, including research and development, 
manufacturing and legal, which have resulted in the largest product pipeline in the history of 

the company. We acted well in advance of the changing environment to minimize the negative 
factors and maximize new opportunities and we believe we are poised to achieve significant 
potential top and bottom line growth in the years ahead. 
Generic Pharmaceuticals 
Mylan remains a market leader in developing, manufacturing and marketing generic pharmaceuticals. We currently manufacture and market over 140 generic products in approximately 360 
product strengths, covering over 40 therapeutic areas. During fiscal 2005, Mylan shipped over 
12.5 billion dosages, and over 200 million prescriptions were dispensed with Mylan products. 
We recognize, however, that the generic pharmaceutical competitive environment is changing, 
and we must adapt to maintain our leading industry position. Branded pharmaceutical companies are increasingly using authorized generics, citizen's petitions and other tactics to 
interfere with the natural balance between generic and branded pharmaceuticals created by 
Hatch-Waxman. There has also been an increase in the number of generic competitors and 
increased competition within Mylan's base business. It would be a mistake for any generic 
company to think that future success can be achieved by relying solely on traditional generic 
business strategies. 
Mylan has always faced new challenges head on. We are fiercely committed to preserving the 
generic industry and the rights of consumers looking to obtain affordable health care. We have 
been and will continue to be an industry leader in aggressively addressing the competitive, legislative and regulatory issues facing our business in an attempt to mitigate the negative 
impact of such issues. For example, Mylan led various initiatives to try to resolve some of 
these challenges by working with the Food and Drug Administration (FDA), the Centers for 
Medicare and Medicaid Services (CMS), the Federal Trade Commission (FTC) and through the 
judicial system on issues such as authorized generics and the abuse of citizen's petitions, as 
well as further clarification on the intent of Hatch-Waxman legislation. We believe we have had 
a positive impact on these organizations and have succeeded in encouraging leading members of the Senate and House of Representatives to take a closer look at the long-term 
implications of authorized generics and other issues that ultimately could harm consumers 
faced with rising health care costs. We have not won every battle, but we are making progress, 
and have, at a minimum, achieved our well-publicized primary objective of simply defining the 
rules of engagement for the generic pharmaceutical industry. Given the rules of engagement 
as presently defined, it is important to note that Mylan intends to participate in authorized 
generics going forward, as appropriate. 
Another important element of our success to date in the generic pharmaceutical industry is 
the careful selection and development of the products that we bring to market. This is not an 
overnight or short-term process. The process of identifying, evaluating and developing products, as well as managing the legal and regulatory hurdles to successfully bring a product to 
market can take three to five years or longer. The product decisions we make today can affect 
the Company's performance in fiscal 2009 and beyond. It is important to note that our senior 
management team and Board of Directors significantly increased our investment in generic 

product development over the past three years. We more than doubled our annual investment 
in generic research and development from approximately $34 million in fiscal 2002 to approximately $69 million in fiscal 2005. There is expected to be a significant number of branded 
products coming off patent industry-wide over the next several years, with approximately $75 
billion of brand pharmaceuticals potentially losing exclusivity between 2005 and 2008. Our 
increased investment in R&D has Mylan well positioned to participate in the industry upturn 
when it occurs. We expect to achieve a disproportionately high share of significant generic 
product launches. Mylan now has the deepest product pipeline in the history of our company, 
and we intend to launch approximately 45 new products over the course of fiscal 2006 and 
fiscal 2007. Looking further ahead we anticipate obtaining approval of over 100 new products, 
representing over $79 billion in 2004 brand sales, over the next five years. 
Branded Pharmaceuticals 
Our proposed acquisition of King Pharmaceuticals, Inc., a branded pharmaceutical company, 
was announced in July 2004 and terminated in February 2005 as a result of material changes 
within King and the inability of Mylan and King to agree on revised merger terms. That merger, 
if consummated, would have provided Mylan with an appropriate brand platform for the launch 
of nebivolol, our highly anticipated beta blocker. Based on the termination of the King merger 
and the lack of other appropriate acquisition alternatives, we recently announced that the next 
best available option was to out license nebivolol to a major brand pharmaceutical company. 
Such company will be responsible for co-developing and commercializing this exciting opportunity. We look forward to sharing additional news related to this product in the coming months. 
In conjunction with the decision to out-license nebivolol, we also spent considerable time 
reviewing our existing operations for further operational efficiencies. Had we consummated 
the King transaction, we would have integrated our Mylan Bertek branded product subsidiary 
and King into a very strong brand franchise. On its own, however, Mylan Bertek was not the 
appropriate brand platform that would enable us to fulfill our vision and objective of becoming 
a more balanced specialty pharmaceutical company. Thus, we decided to close the Mylan 
Bertek division and transfer responsibility for sales and marketing most of Mylan Bertek's 
products, predominantly branded generics, to two of our subsidiaries, Mylan Pharmaceuticals 
and UDL Laboratories. We believe that this restructuring will significantly enhance our earnings per share in fiscal 2006 and beyond. 
These actions do not change our fundamental long-term strategy of building a successful 
brand platform. We continue to make the right decisions to optimize the economic value of the 
nebivolol opportunity for our shareholders and create the most effective operational structure 
for our business as it exists today. We will continue to focus on building a successful brand 
platform, but we now expect to have a different entry into this area.  
Delivering Value to Shareholders 
Mylan has a longstanding commitment to, and history of, delivering value to its shareholders 
through a strong balance between share appreciation and our dividend. We recently made two 
significant announcements to further enhance both of these areas. We have long been one 
of only a small number of generic companies that pays a dividend to its shareholders, and 
we significantly enhanced our dividend policy moving forward by increasing our dividend by 
100% - from $0.12 to $0.24 cents per share on an annual basis. This allows us to return a 
greater portion of our strong cash flows to our shareholders. At the same time we also initiated a major share repurchase initiative to buyback nearly 25% of our shares outstanding. 
The sheer magnitude of this buyback will enhance the impact to earnings per share going for 
ward of any and all new generic and other opportunities due to the significant reduction in our 
shares outstanding. 
We believe both of these measures create the right balance between positioning Mylan for 
both short and long-term success while delivering value to our shareholders. We continue to 
have significant financial flexibility to allow us to pursue new opportunities, as well as the 
ability to invest in key areas of the Company for future success. 
Positioned For Significant Growth 
As a result of the actions we have taken to position Mylan for future growth, fiscal 2006 will 
be a transformational year for Mylan. That said, we still project increased revenues and earnings in fiscal 2006 and are excited that we are well positioned to achieve significant growth 
over the next five years and beyond. On the following pages you'll learn more about exciting 
developments at Mylan and the achievements of our three subsidiaries: Mylan Pharmaceuticals, UDL Laboratories and Mylan Technologies. Each of these organizations is unmatched in 
the industry in their respective specialty areas, and each provides Mylan with unique opportunities for growth. 
The Board of Directors and senior management team have implemented a number of important and sound strategies that we believe will continue to build on Mylan's 44year history of 
delivering strong performance and impressive shareholder value. 
On behalf of the Board of Directors and senior management team, we thank you for your 
continuing loyalty and support. 


