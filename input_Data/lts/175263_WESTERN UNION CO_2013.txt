Dear Fellow
ShareholDerS,
At Western Union, our customers are at the center of
everything we do.
Our customers are people like Aurelia, who works a
demanding job caring for elderly people in Italy so she
can send money to her mother back home in Romania.
They are people like George, who uses wu.com to send
money every payday from the U.S. to multiple family
members in the Philippines. They are people like Gary, a
businessman in California who uses Western Union
Business Solutions to pay invoices in Hungarian forints
and euros.
While these stories are representative of the many
people who use our servicesmillions of consumer
money transfer senders and more than 100,000 business
customers in 2013our definition of a customer
is much broader.
Our customers are travelers who lack options for
accessing their money overseas. They are nongovernmental
organizations that have difficulty getting
funds to the people and places that need it most. They
are people who prefer using cash to pay their bills.
Worldwide, many people have unmet financial service
needs. At Western Union, its our goal to meet those
needsthrough providing new, innovative solutions.
No matter who our customer is, each has a voice. Our
customers are telling us what they want, what they need.
Were listening, and were taking action.
Our vision
Western Union has a strong foundation to deliver crossborder,
cross-currency money transfer services to
customers and businesses around the globe.
We have an extensive network, including more than
500,000 agent locations in more than 200 countries and
territories. We have a strong global brand, tailored solutions
for businesses, and compliance and regulatory
capabilities that allow us to operate in a complex
environment.
We have a growing digital business, and believe we are
well-positioned to connect the digital and physical
worlds for our customers.
Our vision is to be a recognized leader in providing
innovative solutions, high service levels, and omnichannel
integration for cross-border, cross-currency
money transfer.
Last year, we continued to invest to improve our business
for the future.
We focused our strategies on strengthening our consumer
money transfer business, increasing customers
and usage in business-to-business, and generating and
deploying strong cash flow for our shareholders.
Strengthening our consumer-to-consumer business
In 2013, our 10,000 employees and the more than 1 million
front-line associates at our agent locations continued
a relentless pursuit of understanding our customers
needs, implementing a wide variety of changes in
response.
Beginning in late 2012 and continuing in the first half of
last year, we implemented a series of price reductions in
key corridors to regain momentum with our customers.
These actions delivered the transaction growth we
expected, with significant improvement in the second
half of 2013.
worldwide, many people have unmet financial service
needs. at western Union, its our goal to meet those
needsthrough providing new, innovative solutions.

We also continued to expand our options for sending or
receiving money, with more transactional websites,
mobile money transfer services, and capabilities to
move money directly to or from a bank account in certain
countries.
At the end of 2013, we offered our services at more
than 100,000 ATMs across the world. We now have 19
active mobile money transfer connections in 17 countries,
wu.com transactional sites in more than 20 countries,
and customers can use our mobile app in the U.S.
and Australia to send money around the globe. In addition,
we have agreements with more than 75 banks to
offer our account-based money transfer service.
We continued to add strategic new agent relationships
last year, including Banorte in Mexico, Alexandria Bank
in Egypt, Riyad Bank in Saudi Arabia, and Walgreens
in the U.S. We also renewed and expanded our agreement
with La Banque Postale in Europe, one of our
largest agents.
Consumer-to-consumer transactions increased 5% for
the year, with 9% growth in the fourth quarter. Revenue
declined 3%, as expected, primarily due to the pricing
actions we began in late 2012. Electronic channels delivered
strong growth, with revenue increasing 25% for the
year, representing 5% of our overall revenue in 2013.
Increasing customers and usage in
business-to-business
Were also listening to small- and medium-sized enterprises
(SMEs), which have differentbut equally important
needs than consumers.
Last year, we introduced a variety of new products and
services, including a new cash management dashboard
tool for SMEs, new services for non-governmental
organizations and options and forwards products in additional
markets.
We significantly expanded our services in India and
Japan, and we increased offerings for cross-border
tuition payments in several countries, including China,
India and South Korea.
These services contributed to a 7% revenue increase in
our Business Solutions segment last year, and allowed
us to meet some of the real and growing needs of
our customers to better manage their cash flows and
foreign-currency exposures.
Business Solutions now offers cross-border payments
in 32 countries and represented 7% of our overall revenue
in 2013.
Generating and deploying strong cash flow for
shareholders
One of the great qualities of our business model is our
ability to generate strong cash flow. Although 2013
was an investment year, with pricing, compliance and
other strategic investments, the company once again
generated more than $1 billion of cash flow from operating
activities.
In 2013, Western Union returned $671 million to shareholders
through share repurchases and dividends.
Our commitment to customers and society
We are listening to our customers in other ways, too.
We continue to implement compliance and fraudprevention
enhancements around the world to protect
our customers and their hard-earned money. We
believe this commitment will make us stronger over the
long term.
We also are committed to giving back to the communities
where our services are offered. In 2013, the Western
Union Foundation gave approximately $8 million to
causes supporting education and disaster-relief efforts
around the world.
The future
We remain focused on executing our strategic priorities to
return our business to stronger revenue and profit growth.
We will continue to be proactiveanticipating, adapting
to and addressing the ever-changing needs of our customers,
always striving to stay one step ahead.
In 2014, this means continuing to strengthen our consumer
money transfer business, with an emphasis on
digital expansion, to drive strong growth in Business
Solutions, and to generate and deploy strong cash flow
for our shareholders.
We have a clear vision and a solid foundation for growth.
We have millions of loyal customers. And, we have
passionate, hard-working colleagues around the globe.
We are focused on executing our vision and creating
long-term value for our shareholders.
Thank you for your commitment to Western Union and,
most importantly, to our customers!
HIKMET ERSEK
President, Chief Executive Officer and Director
April 2, 2014