TO OUR SHAREHOLDERS
                                                            customers in lower Manhattan had electric, gas, and
A Year of Challenge As New York State's largest
                                                            steam service available. In view of the damage
energy distributor, your company faced many new
                                                            inflicted on the energy delivery systems, this
challenges in 2001 and met them with strength
                                                            achievement is a testament to the initiative, skill,
and success. Con Edison of New York and Orange
                                                            and dedication of Con Edison emergency teams
and Rockland Utilities, our two regulated utility
                                                            and the many others throughout the company who
companies, served 9.5 million New Yorkers, about
                                                            provided support. The Edison Electric Institute,
half the state's population, and delivered 43% of
                                                            an association of United States investor-owned
all electric energy consumed in New York State. Over
                                                            electric utilities with affiliates and associates
the past year, we continued substantial investment
                                                            worldwide, recognized Con Edison's accomplish-
in our electric, gas, and steam energy infrastructure.
                                                            ment with its highest honor, the Edison Award.
Recent events have shown just how vital that infra-
                                                              Field crews continue working on permanent
structure is to the economy of our entire nation.
                                                            repairs. Two substations that were destroyed must
  In tumultuous times for the energy industry and
                                                            be rebuilt. The first is scheduled for completion
our country, Con Edison's focus on its core energy
                                                            by the summer of 2002, the second in 2003.
transmission and delivery business has protected
                                                            Additional rebuilding includes installing more
our shareholders' interests. The price of our common
                                                            than 130 miles of cable and associated support
stock rose 4.8% in 2001. Total return to share-
                                                            structures in the dense, narrow streets of downtown
holders, including reinvestment of dividends, was
                                                            Manhattan. We've made excellent progress on this
11%. This performance is particularly notable by
                                                            work. The energy delivery systems will be ready for
comparison with declines in 2001 in major market
                                                            the summer of 2002.
indices. Despite the difficult economic environment,
we earned a solid $3.22 a share. We maintained
                                                            Meeting New York's Energy Needs Though the
the strength of our balance sheet, along with our
                                                            events of September 11 overshadowed the year's
very good credit ratings.
                                                            developments, other major challenges were success-
  We recognize the importance of the dividend to
                                                            fully met during 2001. In August, the Northeast
our shareholders and increased the dividend in
                                                            was subjected to an intense weeklong heat storm.
2001 for the 27th consecutive year, a record
                                                            On August 9, Con Edison of New York set a new
shared by few in our industry. In January 2002,
                                                            record for electric demand, when peak load reached
we increased the dividend again, to an annualized
                                                            12,207 megawatts. That same day, gas operations
$2.22 a share, reflecting our confidence in the
                                                            set a daily delivery record of 1,077,858 dekatherms.
future of Con Edison.
                                                            Orange and Rockland Utilities also experienced
  On September 11, our company, our city, and
                                                            a historic electric peak of 1,340 megawatts.
our nation were confronted with an unprecedented
                                                            Our systems and the people who operate them
challenge. As a vital participant in the life of New
                                                            performed very well in response to record demand.
York City, Con Edison was on the front lines, active
                                                            Customers received reliable service that kept their
in the emergency response to this crisis. In the
                                                            homes and businesses supplied with power and
days that followed, our hard-working men and
                                                            helped them cope with the heat.
women accomplished a truly extraordinary feat by
                                                               All businesses, from multinational securities
restoring the critical energy services needed to
                                                            firms to the corner grocery, are expanding their use
help the city begin its recovery.
                                                            of digital processing to track their sales, manage
  Within eight days of the attack, virtually all



inventory, execute transactions, and handle day-                             kept pace and is strongly positioned for growth as
to-day operations. Many of the advances being                                the economy recovers.
made today in research and engineering are making
computerized information management systems                                  Investing in Our Future An important factor in
more useful in daily living and business pursuits.                           the ability of our energy delivery systems to meet
A reliable supply of energy is essential for both                            the challenge of sustained growth is our ongoing
homes and commercial facilities, and meeting this                            program of capital investment in electric, gas, and
need represents a source of continuing growth for                            steam infrastructures. More than $700 million
Con Edison.                                                                  was spent on improvements in 2001. Over the
  Although the nation and the region are experi-                             next five years, Con Edison of New York expects
encing an economic slowdown, the New York                                    to invest $4.3 billion on electric, gas, and steam
                                                                             delivery systems. With new technologies being
                                                                             applied to monitoring and control systems, our
                                                                             state-of-the-art equipment provides an in-depth
                                                                             real-time picture of network performance that
                                                                             helps improve operational reliability and efficiency.
                                                                               In 2001, we completed the sale of the Indian
                                                                             Point nuclear plant facilities for more than $500
                                                                             million. To help ensure an adequate energy supply
                                                                             for our customers, we negotiated an arrangement
                                                                             to acquire the electric output of this facility
                                                                             through 2004.
                                                                               As part of our effort to secure adequate electricity
                                                                             supplies for summer peak demand until new gen-
                                                                             eration is built, we are repowering our East River
                                                                             Station using new state-of-the-art turbines. These
                                                                             new units will allow Con Edison to generate steam
                                                                             more efficiently and will provide needed electric
                                                                             energy year-round.
                                                                               Ongoing capital improvements  and the steadfast
                                                                             dedication of our workforce  help us keep Con
Manhattan Electric Operations control room facility incorporates state-of-
the-art system monitoring and management technology.
                                                                             Edison of New York's system reliability the nation's
                                                                             best. This investment is one part of our corporate
metropolitan area retains its core strength. It
                                                                             strategy to grow our core business. As the need
remains the center of major industries, such as
                                                                             for energy climbs, Con Edison will continue to
finance, media, advertising, insurance, real estate,
                                                                             make investments in infrastructure and technology
pharmaceuticals, and medical research. The most
                                                                             to meet customer needs.
recent census confirms that the population of New
York City has increased to more than eight million
                                                                             Providing Industry Leadership In 2001, extraordi-
people, an expansion that drives development
                                                                             nary interest was focused on the energy industry
of new housing, stimulates more business, and
                                                                             nationwide. The year began with events in California
enhances commercial activity and new job crea-
                                                                             that burdened the state's economy and strained
tion, all of which contribute to a sound economic
                                                                             available energy supplies in the West. Sharp
base. For more than 175 years, Con Edison has
                                                                             increases in the cost of electric supply led to the
been a vital part of the metropolitan area. As the
                                                                             bankruptcy of one of the West Coast's largest pub-
need for energy has increased in New York City
                                                                             lic utilities and left another teetering on the brink.
and the rest of our service area, your company has



                                                     
By the end of the year, the country's largest energy                       broadband infrastructure, as businesses seek to
trading firm had also collapsed. These events have                         bolster their communications capability in light of
once again called public attention to the role of                          September 11.
energy policy and business practices in an                                   Con Edison Solutions provides a wide range of
increasingly competitive global economy.                                   energy procurement and management services to
  Utility customers in New York State can purchase                         companies and individuals, helping its customers
electric energy and natural gas from independent                           successfully navigate the new competitive energy
energy services companies. This promotes retail                            landscape.
and wholesale competition among energy suppliers,                            Con Edison Development acquires, develops, owns,
which will ultimately benefit businesses and con-                          and operates electric generation plants in several
sumers. Con Edison has taken a leadership position                         mid-Atlantic and New England states, where
in working with the Federal Energy Regulatory                              demand for electric energy continues to increase.
Commission, the New York State Public Service                                Con Edison Energy has access to the output
Commission, the New York Independent System                                from these and other generating plants. It offers
Operator, and other policymakers and market                                energy, capacity, and risk-management services to
participants to ensure that energy supplies are                            wholesale customers in the Northeast.
adequate to meet our peak summer loads and that                              Our competitive business units leverage the
the transition to fully competitive energy markets                         strengths of Con Edison to build profitable growth
goes forward in an effective and measured way.                             in their respective markets. As the communications
We also work closely with our customers to pro-                            and energy industries evolve, these companies are
vide the information that can help them to make                            well positioned to satisfy customer demand for
informed energy choices.                                                   innovative services and business solutions.

                                                                           Meeting Every Challenge Con Edison excels at our
Building a Presence in Competitive Markets
Con Edison's four unregulated companies are on                             core "wires and pipes" business. With our solid
track to develop and grow in a spectrum of markets.                        credit ratings, strong capital structure, and steady
Con Edison Communications continues to build                               growth in dividends, we are pursuing a corporate
out its fiber-optic network in New York City and                           strategy that combines our financial strength with
has completed service facilities for more than                             our experience in energy markets. We look forward
60 buildings. The company has established long-                            to pursuing opportunities in the regulated sector
term agreements with major facility owners and                             through enhancement of our delivery systems to
property management firms to provide backup                                meet growing energy demand and through con-
                                                                           tinuing improvements in operating efficiency. We
                                                                           continue to apply our expertise in energy supply,
                                                                           energy services, and infrastructure systems to
                                                                           maximize the potential of our non-utility companies
                                                                           in competitive markets.
                                                                             Our distinguished Board members, who contribute
                                                                           their judgment and insights to the management
                                                                           of the company, were joined in September of 2001
                                                                           by Vincent Calarco, Chairman of the Board of
                                                                           Crompton Corporation, a global producer and mar-
                                                                           keter of specialty chemicals, and polymer products
                                                                           and equipment. Mr. Calarco's experience in the
                                                                           business community and his financial expertise
                                                                           make him a valuable addition to our Board.
Today's power-intensive businesses require reliable sources of energy.


