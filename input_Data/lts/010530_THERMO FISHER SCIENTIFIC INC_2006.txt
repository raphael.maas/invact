DEAR SHAREHOLDERS

Its not often a company can claim a truly exceptional year, but 2006 clearly fi ts that description
for us. On May 5, we hosted a life sciences symposium to celebrate the 50th anniversary
of our founding. Three days later, we announced our intent to merge with Fisher Scientifi c
 transforming not only our corporation, but also the way our customers will approach
their work from now on. Through the merger, we created Thermo Fisher Scientifi c, the
world leader in serving science. With that distinction comes great responsibility  to meet
the needs of our customers, employees and shareholders. Leadership also brings great
opportunity  to leverage our unmatched breadth of technologies and services as we enable
our customers to make the world healthier, cleaner and safer.
The merger closed on November 9, 2006, but the important work involved in planning
the integration of Thermo and Fisher began months earlier. I was extremely pleased with
how quickly employees from both companies began working together and, because of their
efforts, the integration is progressing better than I could have imagined. On Day One,
employees were able to share a sense of pride as site managers around the world presented
our new company and the vast opportunities for both individual and business growth that
come with working for a $9 billion organization. Our new name  Thermo Fisher Scientifi c 
became the symbol of two strong businesses joining forces to reshape the life, laboratory
and health sciences industry.
An important trait of a world leader is to consistently deliver on fi nancial commitments.
In that regard, 2006 was no exception, as we continued to meet or exceed our growth goals.
Our reported revenues grew 44% in 2006 to $3.79 billion. That fi gure included $849 million
of Fisher revenues from the merger date to the end of the year. Adjusted earnings per share*
grew 30% in 2006 to $1.91  even better than wed expected. Adjusted operating income

increased 63%, and adjusted operating margin expanded 180 basis points over 2005 to 15.1%,
well above our goal of 100 basis points for the year. In addition, we reported very strong cash
fl ow, generating $407 million from continuing operations for a 49% increase over 2005.
Looking at our year-end results on a pro forma basis, as if the merger had occurred at the
beginning of 2005, revenues grew 10% to $8.87 billion. Pro forma adjusted operating
income in 2006 rose 21% year to year, and pro forma adjusted operating margin expanded
130 basis points over 2005 to 14.7%.
Several key factors drove our excellent performance in 2006: strength in our major end
markets, signifi cant new product introductions and our successful integration of acquired
businesses. Our ongoing efforts to improve effi ciency and productivity on the job every
day, using our Practical Process Improvement (PPI) methodology, also contribute to growth.
Looking ahead, we expect to meet our growth goals by continuing to build the best technology
and service portfolio, through both internal development and acquisitions, as well
as by delivering it more effi ciently through our extensive customer channels.
To be the world leader, its no longer enough to offer the best products and services. We
also have a responsibility to deliver them the way customers expect, in the most convenient
manner possible. Through our new customer channels network serving research, healthcare
and safety markets, Thermo Fisher can now do that much more effectively than any of our
competitors  and thats where the merger brings tremendous value.

We now have two premier brands: Thermo Scientifi c and Fisher Scientifi c. Simply put,
Thermo Scientifi c stands for technology  the integrated workfl ow solutions that address
todays most complex analytical challenges. Fisher Scientifi c stands for convenience  the
most extensive channel network in the industry, consisting of catalogs, e-commerce and
supply-chain services options. It also includes a range of services, such as the specialized
packaging of drugs for clinical trials or the safe management and storage of biospecimens,
which are increasingly outsourced by our big pharma and biotech customers. Together, these
well-known brands offer the best of both worlds, giving customers tremendous access
to the greatest breadth of technologies, consumables, software and services on the market.
Weve made signifi cant investments over the past few years to more effectively reach our
customers in 150 countries around the world, especially those in growing Asian markets.
Our extensive portfolio of offerings is ideally suited to help countries like China and India
meet public demand for broader healthcare, better generic drugs, higher-quality food and
water supplies, and cleaner air, as evidenced by an 11% increase in sales there in 2006.
Ive often said that what excites me most about working for this company is seeing what
our customers are able to do with our technologies. Every day, our innovations spark new
discoveries, enabling researchers, healthcare providers, industrial workers and fi rst responders
to make the world healthier, cleaner and safer. Some great examples are highlighted on
the opening pages of this annual report.
As Thermo Fisher Scientifi c, our focus on technology development will not change. Over
the past fi ve years, we have worked hard to improve our R&D process. We are now very
disciplined  in how we execute R&D projects and how we learn what our customers want
and are willing to pay for. Our discipline has paid off, with our Product Vitality Index nearly
doubling in three years to 29% in 2006. This means that almost one third of our product
revenues came from new introductions in the last two years  products like the LTQ Orbitrap
mass spectrometer, iCAP spectroscopy system, Mercury Freedom air-quality monitor
and NITON portable elemental analyzer, all from our Thermo Scientifi c brand.
As the world leader, we are known for constantly raising the technology bar. But just as
important today is how we put these technologies together to create a total workfl ow solution.
I regularly visit customers, and while they may be impressed by the science, they
are also under pressure to run their labs more effi ciently. We offer the best technologies
and we can integrate them in a way that allows our customers to be more successful  not
only by improving their results, but also their productivity.
For example, researchers can perform an important scientifi c technique, called Electron
Transfer Dissociation (ETD), with greater precision and speed now that weve combined it
with a 2-D linear ion trap system for the fi rst time. Our Thermo Scientifi c LTQ XL mass
spectrometer with ETD allows scientists to better understand how proteins are modifi ed
as they continue to expand their search for biomarkers of disease. By using our Pierce
bioreagents during the sample preparation process, scientists can achieve even better results,
and they can more easily interpret all the data by applying our Thermo Scientifi c SIEVE
software.
As our customers needs evolve to address the emerging complexities of our society, providing
world-class solutions will become more and more important. We anticipated this trend
years ago when we began to better integrate our systems and add more comprehensive
services. The merger has dramatically strengthened our ability to be a total solutions provider,
as well as our ability to deliver those solutions more effi ciently.
At Thermo Fisher Scientifi c, our focus is on putting our customers fi rst. If we do that, employees
and shareholders will be rewarded. Our strong company values  Integrity, Intensity,
Innovation and Involvement  defi ne who we are and how we interact with customers, with
partners and with each other. They are the very foundation of our culture, which has guided
our 30,000 employees through a challenging, but very rewarding, year. I am grateful for
their support, and I believe our shared values will unite and energize us to pursue the many
opportunities we have as the world leader in serving science.
Sincerely,
Marijn E. Dekkers
President and Chief Executive Officer