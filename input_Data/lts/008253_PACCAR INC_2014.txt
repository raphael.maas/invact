T O O U R S H A R E H O L D E R S

PACCAR had an excellent year in 2014, achieving record revenues of $18.99 billion due to a strong
truck market in North America and growth in aftermarket parts sales and financial services assets
worldwide. Net income of $1.36 billion was the second best in company history. The company has
earned an impressive 76 consecutive years of net income. This remarkable achievement was due to our
23,000 employees delivering industry-leading product quality and innovation, and outstanding operating
efficiency. The benefits of global diversification were evident in 2014, with the strength of the North
American truck market more than offsetting lower truck demand in Europe. PACCARs superior
financial strength enabled the company to invest $439 million in capital projects and research and
development in 2014 to enhance its manufacturing capability, expand its range of vehicles and engines
and strengthen its aftermarket capabilities. PACCAR delivered 142,900 trucks to its customers and sold
a record $3.1 billion of aftermarket parts. PACCARs excellent credit rating of A+/A1 supported
PACCAR Financial Services new loans and leases of $4.5 billion. Shareholders equity was a record
$6.75 billion.
Class 8 industry truck sales in North America, including Mexico, were 270,000 vehicles in 2014
compared to 236,000 the prior year. The European 16+ tonne market in 2014 declined to 227,000
vehicles compared to 241,000 in 2013. Customers in North America are generating good profits due to
increased shipments, improved freight rates and higher fleet utilization.
PACCARs excellent financial performance in 2014 benefited from record pre-tax profits for PACCAR
Parts and PACCAR Financial Services. The companys 2014 after-tax return on revenues was 7.2%.
After-tax return on beginning shareholder equity (ROE) was 20.5% in 2014 compared to 20.0% in 2013.
PACCARs long-term financial performance has enabled the company to distribute over $4.8 billion in
dividends during the last 10 years. PACCARs average annual total shareholder return over the last
decade was 9.9%, versus 7.7% for the S&P 500 Index.
INVESTING FOR THE FUTURE  PACCARs consistent profitability, strong balance sheet and intense focus
on quality, technology and productivity have allowed the company to invest $5.8 billion in the last
decade in world-class facilities, innovative products and new technologies. Productivity and efficiency
improvements of 5-7% annually and capacity improvements of nearly 15% in the last five years have
enhanced the capability of the companys manufacturing and parts facilities.
In 2014, capital investments were $223 million and research and development expenses were $216
million. PACCAR launched new truck models and expanded its product range, invested in global
expansion and enhanced its manufacturing efficiency during the year. The new Kenworth T680
Advantage, Peterbilt Model 579 EPIQ and DAF XF and CF Euro 6 Transport Efficiency vehicles are
leaders in fuel efficiency and premium quality. PACCARs Mississippi engine factory produced a record
number of PACCAR MX-13 and MX-11 engines in 2014. PACCAR engines installed in Kenworth and
Peterbilt trucks total over 75,000 since engine production began in 2010. Customers benefit from the
PACCAR MX engines excellent fuel economy, light weight and reliability.

PACCAR expanded its investment in the BRIC countries (Brasil, Russia, India, China) in 2014. The
companys new DAF factory in Ponta Grossa, Brasil completed its first full year of truck production.
PACCAR Parts increased parts sales to DAF and Kenworth dealers in Russia by 31%. The PACCAR
Technical Center in Pune, India provided excellent support to PACCARs global product and technology
initiatives and qualified many world-class Indian automotive suppliers for PACCAR. In China, the
worlds largest truck market, PACCAR expanded its purchasing activities and continued to examine joint
venture opportunities.
SIX SIGMA  Six Sigma is integrated into all business activities at PACCAR and has been adopted at 280
of the companys suppliers and many of the companys dealers and customers. Six Sigmas statistical
methodology is critical in the development of new product designs, customer services and
manufacturing processes. Since 1997, Six Sigma has delivered over $2.6 billion in cumulative savings in
all facets of the company. Over 15,000 employees have been trained in Six Sigma and 26,100 projects
have been implemented. Six Sigma, in conjunction with Supplier Quality, has been vital to improving
logistics performance and component quality from company suppliers.
INFORMATION TECHNOLOGY  PACCARs Information Technology Division and its 740 innovative
employees are an important competitive asset for the company. PACCAR develops and integrates
software and hardware to enhance the quality and efficiency of all products and operations throughout
the company. In 2014, PACCAR was again recognized as a technology leader by InformationWeek
magazines 2014 Elite 100 Companies. Over 29,000 dealers, customers, suppliers and employees have
experienced PACCARs Technology Centers, which highlight electronic work instructions, mobile
computing, an electronic leasing and finance office and an automated service analyst.
TRUCKS  U.S. and Canadian Class 8 industry retail sales in 2014 were 250,000 units and the Mexican
market totaled 20,000. The European Union (EU) industry 16+ tonne sales were 227,000 units.
PACCARs Class 8 retail sales in the U.S. and Canada achieved a market share of 27.9% in 2014. DAF
achieved a 13.8% share in the 16+ tonne truck market in Europe. Industry Class 6 and 7 truck retail
sales in the U.S. and Canada were 73,000 units, up 11% from the previous year. In the EU, the 6 to
16-tonne market was 47,000 units, down 18% compared to 2013. PACCARs market shares in the U.S.
and Canadian and European medium-duty truck segments were 16.7% and 8.8%, respectively, as the
company delivered 22,300 medium-duty vehicles in 2014.
A tremendous team effort by the companys engineering, purchasing, human resources, materials and
production employees enabled the rapid acceleration of truck production rates in our North American
factories to meet growing customer demand.
PACCARs product innovation and manufacturing expertise continued to be recognized as the
industry leader in 2014. PACCARs engine factory in Columbus, Mississippi and Peterbilts truck factory
in Denton, Texas earned Frost and Sullivans Manufacturing Leadership awards. DAF was awarded
LGV Manufacturer of the Year in the United Kingdom by Green Fleet Magazine.

PACCAR Mexico continues its sales leadership, achieving a 41% Class 8 market share. PACCAR truck
deliveries in South America increased 36% in 2014.
PACCAR Australia achieved strong results in 2014 with combined heavy-duty market share of
Kenworth and DAF of 23.5%. Kenworth introduced the PACCAR MX-13 engine for its Kenworth T400
truck models and unveiled a new conventional cab.
AFTERMARKET CUSTOMER SERVICES  PACCAR Parts had record revenues and profits in 2014 as dealers
and customers embraced new innovative e-commerce platforms, national billing services and customized
vehicle maintenance programs. With sales of $3.1 billion, PACCAR Parts is the primary source for
aftermarket parts and services for PACCAR vehicles, as well as supplying its TRP branded parts for all
makes of trucks, trailers and buses. Over six million heavy-duty trucks operate in North America and
Europe. The large vehicle parc and the growing number of PACCAR MX engines installed in Peterbilt
and Kenworth trucks in North America create excellent demand for parts and service and moderate the
cyclicality of truck sales.
PACCAR Parts expanded its facilities to enhance logistics performance to dealers and customers.
PACCAR Parts new Montreal distribution center opened in October 2014 and construction began on a
new 160,000 square-foot distribution center in Renton, Washington. PACCAR Parts continues to lead
the industry with technology that offers competitive advantages at PACCAR dealerships.
FINANCIAL SERVICES  PACCAR Financial Services (PFS) conservative business approach, coupled with
PACCARs superb S&P credit rating of A+ and the strength of the dealer network, enabled PFS to earn
record pre-tax profits in 2014. PACCAR issued $1.6 billion in medium-term notes at attractive rates
during the year. The PACCAR Financial Services group of companies has operations covering four
continents and 22 countries. The global breadth of PFS and its rigorous credit application process
support a portfolio of 166,000 trucks and trailers, with total assets of $11.9 billion. PACCAR Financial
Corp. is the preferred funding source in North America for Peterbilt and Kenworth trucks, financing
21% of dealer Class 8 sales in the U.S. and Canada in 2014. Strategically located used truck centers,
interactive webcasts and targeted marketing enabled PFS to sell over 8,000 used trucks worldwide.
PACCAR Financial Europe (PFE) focuses on the financing of new and used DAF trucks. PFE
provides wholesale and retail financing for DAF dealers and customers in 15 European countries, and in
2014 financed over 26% of DAFs 6+ tonne vehicle sales.
PACCAR Leasing (PacLease) expanded its fleet to a record 38,000 vehicles. PacLease placed 6,700
new PACCAR vehicles in service in 2014. PacLease represents one of the largest full-service truck rental
and leasing operations in North America and Germany and continued to increase its market presence in
2014, expanding its truck rental fleet by 20%.
ENVIRONMENTAL LEADERSHIP  PACCAR is a global environmental leader. All PACCAR manufacturing
facilities have earned ISO 14001 environmental certification. The companys manufacturing facilities
enhanced their Zero Waste to Landfill programs during the year. PACCAR joined the CDP (formerly

known as the Carbon Disclosure Project), which aligns corporate environmental goals with national and
local green initiatives. PACCAR earned an excellent initial score of 94 (out of 100).
A LOOK AHEAD  PACCARs 23,000 employees enable the company to distinguish itself as a global leader
in the technology, capital goods, financial services and aftermarket parts businesses. The outlook for
2015 is good in North America as the economy is expected to generate growth of about 3%. The
European economy is expected to grow about 1%.
The North American truck market is expected to build on the momentum gained during 2014, while
the European truck market is forecast to be similar to 2014 as limited economic growth will affect
heavy-duty truck demand. Current estimates for the 2015 Class 8 truck industry in the U.S. and Canada
indicate that truck sales could range from 250,000-280,000 units. Sales for Class 6-7 trucks are expected
to be between 70,000-80,000 vehicles. The European 16+ tonne truck market in 2015 is estimated to be
in the range of 200,000-240,000 trucks, while demand for medium-duty trucks should range from
45,000-55,000 units.
PACCAR Parts industry-leading services and strong freight demand in North America should deliver
continued growth of the companys aftermarket parts business. PACCAR Financial is expected to
perform well due to a good economy in North America and lower fuel prices.
PACCARs new range of vehicles, modern high technology factories and superb customer service in
parts and financial services provide an excellent foundation for future growth. PACCAR is well
positioned and committed to maintaining the profitable results its shareholders expect.

R O N A L D E . A RMS T R O N G
Chief Executive Officer

