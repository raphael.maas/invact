           BIG FOCUS:
           TO OUR
           SHAREHOLDERS
Halliburton is committed to big        During        , we produced record         Building on Our Advantages
                                       revenue and operating income               Strong customer relationships,
thinking, focused execution and        for the total company, as well as          exceptional service quality and
outperforming our peers in any         revenue records for our North              di erentiating technologies matter
                                       America, Europe/Africa/CIS and             in every environment. We are proud
market environment. During 2014,       Middle East/Asia Pacific regions.          to work with many of the world's
we once again delivered industry-      We also set revenue records in             premiere operators. In addition to
                                       both of our divisions and across
leading growth and returns by             of our product service lines.
                                                                                  holding acreage in the sweet spots
                                                                                  of reservoirs, they leverage technology
executing on our key strategies        With an unwavering focus on best-          to achieve the best economics in
                                       in-class returns, we returned a
around unconventionals, mature                                                    their assets and have the financial
                                       third of our       cash flow from          resources to work through cycles.
fields and deepwater. While 2015       operations to shareholders, raising
is shaping up to be a challenging      our quarterly dividend by       percent
                                       and repurchasing          million of       We look beyond the
year, we have what it takes to         our common stock.                          cycle, capitalize
outperform through the down            Strength Across Cycles                     on market opportuni-
cycle and emerge a stronger            The year clearly represented another       ties and continue
company. Our strategies have           peak in what has always been a cyclical    with our strategic
                                       industry. A decline in commodity           initiatives, emerging
worked, they are working, and          prices late in     led customers to
                                       begin reducing their capital budgets,      as a stronger com-
we intend to stay the course.
                                       which translates into a challenging        pany when the mar-
                                       environment for service companies.         ket comes back.
                                       On the positive side, we have built a
                                       position of strength; we have been
                                                                                  We maintain these relationships by
                                       through these cycles before, and we
                                                                                  partnering with our customers on
                                       know what we need to do.
                                                                                  solutions tailored to their needs and
                                       We always take a two-pronged               the prevailing market conditions.
                                       approach. First, we will control what      Our leadership in e cient execution
                                       we can control, defend our market          reduces non-productive time and
                                       position, and live within our cash flow.   lowers the cost per barrel of oil
                                       We will size our operations to market      equivalent for our customers. In an
                                       demand and take the necessary steps        industry characterized by large, com-
                                       to reduce input costs. Second, we          plex shale wells in North America,
                                       look beyond the cycle, capitalize on       daunting technical challenges o shore
                                       market opportunities and continue          and a growing base of integrated pro-
                                       with our strategic initiatives, which      jects on the international stage, we
                                       position us for even greater success       believe that superior execution and
                                       in the future.                             best-in-class technologies will allow us
           David J. Lesar
           Chairman of the Board and                                              to continue outperforming our peers.
           Chief Executive O cer




           4                                                                       Halliburton 2014 Annual Report // GO BIG
Strong customer                           deepwater and mature fields, and            to predict the timing of the recovery,
                                          create a bellwether global oilfield         we know that it will come. In the
relationships, excep-                     service company.                            meantime, we are taking the neces-
tional service quality                                                                sary steps to maintain our financial
                                          Further, we believe the transaction
and di erentiating                        will result in a broader, more cost-        strength through this period while
technologies matter                       e ective service o ering, and provide       also looking past the cycle at a big
                                          compelling value for our customers.         future for Halliburton.
in every environment.
                                          We remain dedicated to developing
                                          the technology portfolio and inno-
We are particularly well-positioned
                                          vations of the combined company,
in the North American shales where
                                          and delivering best-in-class solutions
we are the clear leader in e ciency
                                          and service quality to our customers.
and the preferred partner for maxi-
mizing the value of customer assets.      We are excited about the benefits this
Our Frac of the FutureTM delivery         transaction will create for stakeholders    David J. Lesar
platform gets the job done with           of both companies who play such a           Chairman of the Board and
                                          vital role in our success. We believe we    Chief Executive O cer
    percent less capital deployed
and o ers maintenance cost savings        will be very well-positioned financially,
of up to percent, while improving         poised to accelerate growth, deliver
completion times by up to percent         outstanding margin improvement
                                          and drive shareholder returns.              Je rey A. Miller
at sites where it is employed.
                                                                                      President and Chief Health, Safety and
The value we have seen from the           We greatly appreciate the contri-           Environment O cer
rollout of our Q pumps has truly          butions of our employees and Board
exceeded our original expectations        of Directors, the confidence our cus-
and created a strong competitive          tomers show by looking to us as a
advantage for Halliburton. We             partner in solutions, and the patience      Mark A. McCollum
continue investing to accelerate the      of our investors through the ups and        Executive Vice President and Chief Integration O   cer
retirement of older, higher-cost fleets   downs of our industry. Rest assured
as a way to extend our leadership         that we will never lose our focus on
position. Frac of the Future currently    delivering best-in-class returns and
represents about      percent of our      value for all of our stakeholders.          Lawrence J. Pope
North American fleet, and that                                                        Executive Vice President of Administration and
                                          The long-term fundamentals for our          Chief Human Resources O cer
number is expected to be closer           industry remain very strong. Global
to    percent by the end of        .      demand for oil is forecast to increase
Big Future                                by approximately million barrels per
                                          day in the coming year, and the steep
We look forward to completing our                                                     Robb L. Voyles
                                          decline curves from aging reservoirs        Executive Vice President and General Counsel
acquisition of Baker Hughes to create
                                          are still at work. Any extended period
a stronger, more diverse company with
                                          of under investment resulting from
an unsurpassed depth and breadth of
                                          low commodity prices will reduce
services. We believe this acquisition
                                          production, help to restore supply-         Christian A. Garcia
will accelerate the execution of our
                                          demand equilibrium, and return the          Senior Vice President, Finance and
key strategies in unconventionals,
                                          industry to growth. While it is di cult     Acting Chief Financial O cer




                                                                                                                               5
