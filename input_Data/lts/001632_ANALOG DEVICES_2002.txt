Fiscal 2002 was a challenging but very successful year for
ADI. After revenue troughed in the first quarter, we
began a gradual sequential recovery in the last three
quarters of the year. Gross margins for the year
remained strong at 53%. Operating expenses declined
by 9% as a result of cost containment measures
throughout the company. Operating profits and cash
flow remained strong. We increased our cash balances
by over $100 million to $2.9 billion during FY2002 after
purchasing approximately $100 million of our stock. We
are very proud that ADI is one of the very few
semiconductor companies that remained consistently
profitable throughout the most severe contraction in the
history of our industry, while continuing to fund the
new product programs that will ensure our growth in
the future.
F U T U R E P E R F O R M A N C E I S O F T E N
D E T E R M I N E D B Y AC T I O N S T A K E N
I N T H E D OW N T U R N
At ADI, we have always believed that long-term
corporate performance is often determined by the
actions taken in response to cyclical downturns. Early in
this economic cycle, we established clear objectives and
policies that guided us throughout the year and will
continue to allow ADI to deliver one of the industrys
most robust, profitable and sustainable business models.
Maintain R&D Investments to Fuel Future Growth
Due to our confidence in the long-term viability of our
business strategy, which focuses on signal processing,
supported by our financial strength, we continued R&D
investments at levels commensurate with higher revenue
than we were achieving in the short term. While hiring
rates of technical professionals slowed from prior years,
we nevertheless continued to hire selectively and
maintained a strong presence on college campuses,
where we were able to hire 135 top graduates. In total,
we now employ approximately 3,100 engineers in 33
locations around the world. For the year, R&D
investments totaled $424 million, which represented
25% of our total revenue.
Streamline Manufacturing Operations to Improve
Gross Margins
We decided that we had both the opportunity and the
imperative to use this downturn to fundamentally and
permanently reduce manufacturing infrastructure costs
worldwide. Our focus in FY2002 was to begin a process
to close older 4-inch wafer fabs and transfer production
to newer, more modern 6- and 8-inch fabs that were
operating well below capacity. These transfers are a very
complex and time-consuming process for ADI, given the
intense fragmentation of our product families and the
fact that many products produced in these older fabs
were first introduced 10 to 20 years ago. By year-end, we
were well on our way to meeting this objective, which
will significantly reduce infrastructure costs and will
provide much better utilization of newer fabs. We are
optimistic that these initiatives will provide good gross
margin leverage as revenue increases.
Double Field Applications Engineering to Leverage
New Products
Recognizing the increasing complexity of our products,
we decided to significantly increase our field
applications engineering (FAE) capacity to help our
customers design in our latest products and enhance
communication on new requirements between our
customers and design groups. During FY2002, we more
than doubled our FAE strength around the world and
we added to our sales channel in the Asia Pacific region,
where ADI has substantial growth opportunities. The
industry downturn offered ADI a unique opportunity to
hire very experienced, technically savvy professionals,
many of whom were users of our products in the past.
Maintain Superior Organizational Climate  Our People
Are Our Most Valuable Asset
Despite the constant pressure to reduce costs, we
continued our commitment to remain one of the best
places to work in our industry. When faced with the
necessity of reducing payroll expenses, which represent
approximately 30% of our operating expenses, we
temporarily reduced the salaries of our highest-paid
professionals and eliminated bonuses in an effort to
preserve as many jobs as possible. In fact, the size of our
non-manufacturing workforce remained essentially
unchanged from FY2001. As a result, we are emerging
from this cycle with an energized population,
committed to our long-term success.
A N A L O G D E V I C E S , I N C .
2 0 0 2 A n n u a l R e p o r t 1
O U R A N A L O G  F R A N C H I S E
S T R E N G T H E N E D I N F Y 2 0 0 2
Analog product revenue comprised 80% of ADIs total
sales in FY2002. Within the very broad category of
analog products, ADI focuses on the high-performance
area, which in 2002 represented approximately one
third of the $24 billion analog market. It has become
increasingly clear that analog products truly power the
digital revolution in virtually every market segment as
voice, video and multimedia capability enable and often
differentiate the fastest-growing applications in
consumer, computer, communications and industrial
products. The increasing penetration of analog signal
processing in electronic equipment has stimulated
growth for high-performance analog products at rates
well above industry averages for the past five years and
will likely continue to do so in the future.
The intense fragmentation of the high-performance
analog market, a limited number of competitors capable
of providing this technology and the significant added
value of analog components provide an environment
where customers are willing to pay for performance
when our components can differentiate our customers
overall product performance. The average life cycle for
our analog products remains extremely long given the
fact that the average usage and the percentage of the
bill of materials is relatively low per application and the
costs and risks of redesign are extremely high. Many of
our analog products introduced 10 to 20 years ago are
still selling well today, and we continue to generate 50%
of our analog revenue from products introduced more
than five years ago.
We generally develop our leading-edge core analog
technology for our broad horizontal base of 60,000
customers. Very often, vertical customers in very large
applications also need this technology and they need it
quickly. It is often too late or too risky to begin to
develop core analog technology to satisfy the time-tomarket
requirements of these customers. Our significant
core technology platforms allow us to help our
customers get to market quickly by rapidly adapting our
technology to new applications at low incremental cost
and low risk.
There are numerous examples over the past five years
where this strategy has worked well in many market
segments. Our core amplifier and converter technology
powers new applications in wireless base stations,
handsets and broadband access, and consumer
electronics, such as digital still cameras, digital
camcorders, flat panel monitors, TVs, DVD players and
computer audio. Many of these application-specific
products were derived from our core converter and
amplifier technology. Starting from scratch was not an
option for these rapidly developing applications.
ADI Is #1 in Converters  By a Wide Margin
Clearly, in a digital world, products that convert realworld
analog signals to digital format and vice versa are
critical components in most real-world signal processing
systems. As a result, for many years, converters have
been our most important product family, now
comprising approximately 35% of our total sales. Our
converter revenue is derived from many thousands of
horizontal converter products sold to tens of thousands
A N A L O G D E V I C E S , I N C .
2 2 0 0 2 A n n u a l R e p o r t
Products introduced within 18 months of current year
Products introduced between 1.5 and 5 years from current year
Products introduced 5 to 10 years from current year
Products introduced more than 10 years from current year
FY99 FY00 FY01 FY02
20% 22% 25% 25%
24% 25% 19% 21%
32% 28% 30% 28%
24% 25% 26% 26%
Percentage of Sales
ADI Analog Products Continue to Have
Long Product Life Cycles
Vintage
Resolution (Bits)
12-bit 14-bit 16-bit 18-bit
105
Msps
80
Msps
Analog Devices
60
Msps
600
Ksps
1000
Ksps
AD6645
800
Ksps
210
Msps
No Existing
Competitor
AD9430 AD7677 AD7674
Nearest Competitor
ADI Converters Introduced in FY2002
Deliver the Highest Speed
at Every Resolution
Msps = Megasamples / second Ksps = Kilosamples / second
of customers around the world and also from many
vertical customers for whom we adapt this technology to
more application-specific products. No single converter
product or converter customer accounts for more than a
few percent of our revenue.
Our market position in converters has been getting
stronger each year, with FY2002 market share reaching
40%, based on Semiconductor Industry Association
statistics. We have achieved this position by continuing
to out-innovate our competition and as a result of
continuing to occupy the technical high ground in both
accuracy and speed. FY2002 produced our richest crop
of new core leadership converters in our history and
represent truly leading-edge performance for our
customers next-generation architectures.
ADI Is #1 in High-Performance Amplifiers
Amplifiers are a very large product category,
representing over $2 billion in industry sales in 2002 or
nearly 10% of industry analog sales. Approximately 40%
of industry amplifier sales are derived from precision or
high-speed amplifiers, which is the area where ADI
focuses. Whereas our market share in overall amplifiers
totals just under 20%, ADI has approximately 40% share
of the high-performance amplifier market  quite similar
to our converter market share. In FY2002 we also had a
rich crop of industry-leading amplifier products, which
represent breakthrough performance at extremely low
cost, low power dissipation and small size. In very highperformance
applications, such as base stations and test
equipment, new amplifiers introduced in FY2002 deliver
the highest speeds and the lowest power consumption
while maintaining very high signal quality.
Adapting Core Analog Technology to High-Growth
Vertical Markets
ADI had a record year for adapting core amplifier,
converter and other analog technology to large
emerging vertical markets. These FY2002 vintage
products, some of which are highlighted in the table
below, are all products where we already enjoy or are
building significant market share.
Analog Is a Great Business
Overall our analog products emerged from FY2002 in
great shape. We enjoy 40% market share in the two
most important and defensible analog product
categories  converters and high-performance
amplifiers  which together comprise 55% of ADIs total
revenue. Our growth rate has exceeded the market for
the past four years. Our new product programs are
producing leadership products for both horizontal and
vertical markets. Our cost structure has vastly improved.
We believe we have the largest and most capable analog
engineering team in the industry. We are well
positioned in our core business to produce superior
results for many years to come.
A N A L O G D E V I C E S , I N C .
2 0 0 2 A n n u a l R e p o r t 3
Selection of Analog Products Introduced in FY2002 for Vertical Markets
Application Part # Description
Cable Modem/ AD9879  Mixed-signal front end for receiving and transmitting broadband communications
Set-top Boxes  Combines high-performance digital-to-analog and analog-to-digital converters with optimized
filters and a direct interface to external cable line drivers
Digital Still Cameras AD800xx  Series of digital imaging solutions
 Provides all the functionality needed to digitize an image from a CCD
(charge-coupled device) sensor
PCs ADM1027  Complete thermal systems sensor with the ability to monitor and
control multiple zones and fans
 First device to integrate a thermal control circuit for monitoring the Pentium 4 CPU
Automotive & AD1954  Integrates converters and DSP to enable rich acoustic experience for cost-conscious systems
Consumer Stereos  First product in the SigmaDSP? family of digital audio processors
Cellular Base Stations AD8362  RF IC measures the power level of complex waveforms transmitted by 2.5G and 3G base stations
 Towers and cell phones can adjust the strength of transmission signals, lowering emissions and
reducing battery consumption
M E M S I S B E C O M I N G A C O R E
T E C H N O L O G Y F O R T H I S D E C A D E
Many industry experts now believe that microelectrical
mechanical systems (MEMS) technology could be one
of the most significant electronic technologies for this
decade, approaching $5 billion in revenue over
the next five years. After many years of investment,
MEMS technology is emerging as a key growth
opportunity for ADI.
ADI pioneered MEMS technology in the 1990s by
producing the worlds first silicon micro-machined
accelerometer. We produce not only the inertial sensor
but also all requisite signal conditioning on the same
chip. ADI accelerometers are used in 35% of all airbag
systems in the world and we have cumulatively shipped
over 100 million devices to stringent manufacturing and
reliability standards. Today we have the only highvolume,
dedicated production facility for integrated
inertial MEMS products.
In FY2002, we continued our industry leadership in
inertial sensors with the introduction of the worlds first
integrated silicon MEMS gyroscope. This product has
generated worldwide attention since it offers a truly
revolutionary improvement in both size and cost for rate
sensors, much as accelerometers revolutionized air bag
sensors in the mid 1990s. Our new gyro is 1/100th the
size of existing products and should significantly reduce
customers costs while providing good margins for ADI.
These breakthroughs in cost and size for gyros will allow
rate-sensing technology to penetrate many new
automotive applications as well as new industrial and
commercial applications such as robotics, industrial
control, flight control and navigation systems. Our gyro
product is already winning awards  it was recently
named Product of the Year by both Electronic Products
magazine and AnalogZONE.com. We believe that MEMS
technology will become another growth engine for ADI
over the next few years.
F Y 2 0 0 2 : T H E B R E A K T H R O U G H Y E A R F O R
O U R D S P P O R T F O L I O
FY2002 was a remarkable year for our DSP products.
After many years of investment, we have now achieved
the technical high ground in digital signal processing.
We are hitting breakthrough speed and power efficiency
levels, well ahead of our DSP competition, and we have
closed the performance gap between programmable
digital signal processing and fixed-function digital signal
processing. In our Blackfin and TigerSHARC
platforms, we have brought to market very highperformance
signal processors with programmable
functionality never before seen. These are
breakthroughs that we believe will expand the market
for ADI DSP technology.
Two New DSP Cores Provide Disruptive Technology
We recognized a few years ago that for DSPs to be fully
accessible to mainstream system designers, processors had
to become much more powerful and easier to use. Our
collaboration with Intel, which combined our decades
of leadership in signal processing with Intels expertise in
microprocessors, has produced a radical new architecture
for high-performance, low-cost, low-power DSPs, which
we branded Blackfin. The Blackfin architecture integrates
a very high-performance DSP, a media processor for
multimedia and a microcontroller for system control  all
in one easy-to-program platform.
We also announced the production release of
TigerSHARC, which builds ADI leadership in 32-bit
DSPs beyond our successful SHARC family.
TigerSHARCs leadership in floating-point processing,
I/O performance and on-chip memory bandwidth
create the fastest floating-point and fixed-point DSP
available in the market.
Each core represents industry-leading performance in
each product category. Together, these two new cores
make ADI the obvious choice for general-purpose
applications of programmable DSPs and also for a wide
range of vertical applications.
Blackfin as the Highest-Performance DSP Engine
Blackfin is today the highest-performance and lowestpower
DSP in its class. It has been designed for speeds
above 500 MHz and is power optimized for portable
applications. Our Blackfin core is today only 2.5
millimeters square. Yet in this small space, we achieve
approximately 20 times the performance of just a few
A N A L O G D E V I C E S , I N C .
4 2 0 0 2 A n n u a l R e p o r t
years ago. The Blackfin DSP was architected to run C
code efficiently, which makes it significantly easier to
program than existing DSPs that use more complex
assembly language. Our new Blackfin development tools
allow developers more flexibility than ever before.
Blackfin Creates a New Class of Media Processor
Music, movies and photographs are some of the most
popular and most technically challenging content in
electronic equipment. Implementing video and image
processing algorithms in software allows our customers
to adapt quickly to evolving standards and new
functional requirements without hardware changes.
The Blackfin architecture builds in support for 8-bit
data, the format common to red-green-blue pixel
processing algorithms, and has specialized video
instructions. The integrated video instructions
eliminate complex and confusing communications
between the main processor and a separate video
processor. These features help lower overall system
cost while improving the time-to-market for the
end application.
Blackfin Leverages Microcontroller Techniques
With Blackfin products, the microcontroller that is
typically found in a system alongside a conventional DSP
will no longer be necessary in many applications.
Software programs written to control system interfaces
use very little memory in Blackfin-based designs. An onchip
memory management unit packs programs more
efficiently than conventional DSPs, eliminating the cost
and complexity associated with external memory.
Our DSP Franchise Is Building
Our goal is to build a DSP franchise that
complements our analog franchise. Like our analog
products, leadership core technology wins in the
horizontal markets for DSPs. And like our analog
products, we can leverage this core DSP technology to
quickly craft winning application-specific solutions for
the vertical markets.
Today, the programmable DSP market is about $4.3
billion, of which approximately 65% is baseband DSPs
for cellular telephone handsets. An additional 20% is
made up of DSPs sold into other vertical applications
such as broadband modems, cellular base stations and
consumer electronics. The remaining 15% is generalpurpose
or catalog DSPs sold to the broadest base of
customers in a wide variety of telecommunication,
imaging, audio and instrumentation applications.
Although we have penetrated many vertical
applications with products such as our Softfone
wireless chipset, Othello Direct Conversion Radio IC
and Eagle broadband modem solution, our highest
market share in DSPs is with the broad customer base
that makes up the general-purpose market. Here we
have a 35% market share that has been growing
steadily. As the new Blackfin and TigerSHARC
architectures make DSPs attractive and accessible to
many more customers, this market should become the
fastest-growing part of the DSP industry, especially as
the cellular handset market begins to mature.
We also expect to capture a share of the market
traditionally served by fixed-function digital signal
processing ICs. This market is nearly three times the
size of the programmable DSP market. By giving
customers the flexibility of a programmable solution
that delivers the very high performance their system
needs, our technology can improve power efficiency,
reduce costs and speed time-to-market. With our
Blackfin and TigerSHARC platforms, there is
opportunity for programmable DSPs to replace the
encoders and decoders, custom ICs and media
processors that would otherwise be used by mainstream
system designers as they struggle to integrate
communications, multimedia and system control into
these emerging applications.
A N A L O G D E V I C E S , I N C .
2 0 0 2 A n n u a l R e p o r t 5
Control Processing
(Traditionally Microcontrollers)
Communications Processing
(Traditionally DSP)
Media Processing
(Traditionally FASICs)
There is considerable growth opportunity ahead for our
DSP portfolio. Our position in general-purpose
applications provides the customer base. The technical
high ground provides the momentum. The Blackfin
and TigerSHARC architectures provide the technology
platform. This should add up to above-market growth
rates and continued share gains for ADI.
L O O K I N G TO T H E F U T U R E
As we emerge from the severe downturn of 2001 and
2002, the central question on most investors minds is
whether there has been a fundamental change in the
outlook for semiconductor products going forward. This
question arises naturally, given the maturation of the
personal computer and cell phone markets, which
together comprise approximately 40% of all
semiconductor industry sales, and given the structural
issues that remain in the telecommunications market.
Therefore, by implication, are semiconductors still a
growth business? To better understand the answer to
this question, one needs to look to the fundamental
market drivers for semiconductor products, rather than
focusing on the short-term issues over the past few years.
First, growth rates for electronic equipment continue on
a solid path, growing at consistent rates of 9% per year.
Over the history of our industry, there have been
stronger years and weaker years, but the long-term
trends have remained consistent for some time.
Second, the penetration of semiconductors in electronic
equipment has been increasing reliably for many years.
This is not a surprise, but rather a trend that is both
understandable and predictable. Increasingly the chip is
A N A L O G D E V I C E S , I N C .
6 2 0 0 2 A n n u a l R e p o r t
becoming the system. It is realistic to assume that this
trend will continue and that the penetration of
semiconductors in electronic equipment could climb to
20% over the next few years (up from 14% today),
producing semiconductor industry growth rates above
electronic equipment growth rates.
Lastly and most importantly, within the semiconductor
industry, there has been a fundamental shift to
applications that require real-world signal processing
technology. Across the industry, people keep asking
what the next killer application will be. We believe the
next killer application is making the Internet and
electronic equipment, in general, more useful and
accessible  that is, providing high-speed content that is
portable, multimedia capable and always connected.
The next growth surge will be powered by wireless
appliances, digital still and video cameras, networked
music and video appliances, portable audio and video
systems, home gateways, automotive navigation and
telematics systems and virtually all systems that stream
audio and video data over wired and wireless networks.
At the heart of these new applications is highperformance
analog and DSP technology.
Processing and transmitting real-world signals at very
high speed using very little power will become the new
driver of industry growth. As a result, signal processing
products could well comprise 20 to 25% of total
semiconductor sales by 2007, up from 15% today. It is
this discontinuity, coupled with our core technology
position in converters, amplifiers and DSPs, that creates
the opportunity for ADI to continue to outgrow the
overall market by a factor of 1.5 to two times.
8%
14%
19%
Semiconductor $ Content as a
% of All Electronic Equipment
Source: Gartner Dataquest and ADI estimates
2007
estimate
1992 2002
Semiconductor Content Wi t h i n
E l e c t ro n i c Equipment I s Growing
8%
15%
Signal Processing Sales as a % 20-25%
of All Semiconductor Sales
Source: ADI estimates
2007
estimate
1992 2002
Signal Processing I s Becoming A Larger
Share of the Semiconductor Industry
The past four years have been the most tumultuous and
exhilarating years in our history. In a relatively short
period of time, we have seen periods where we grew by
80%, or $1 billion in revenue in a single year, to a
period where our revenue declined 50% from the peak
over a few quarters. Throughout these peaks and valleys,
we have always been confident that we are in the best
possible product spaces in the entire semiconductor
industry and ADI has the very best practitioners of signal
processing technology in our industry. We are emerging
from this cycle stronger than ever in our history, with
solid prospects for continuing to increase market share,
further improve operating margins and deliver strong
cash flow. While uncertainty remains in the short term,
with a little help from the market, 2003 should be a
good year for ADI.