                      




                 Fluor accomplished a great deal during the year,
       

with many proud achievements. We delivered solid earnings growth
in 2003, despite a business investment climate that remained
challenging throughout most of the year. Importantly, we were able
to more than offset an anticipated significant decline in earnings
contribution from our power segment, as the strong investment
cycle in power generation moved to completion.
                                                                                                
                                                                                                                         


This year's growth in earnings from continuing operations
was accomplished through our strategy of market diver-
sification, combined with solid execution, and was within
the upper end of our target range. Most importantly, we
delivered excellent safety performance, 50 times better than
the national industry average, demonstrating our steadfast
commitment to this core value.
     We have made significant strides in positioning for
long-term growth. Over the past few years, we've outdis-
tanced our competition in key markets, including clean
fuels, life sciences, power and transportation. During
2003, we completed three acquisitions that support our
goal to grow our Government and Operations & Maintenance
(O&M) businesses. Our focus on expanding our partici-
pation in the federal services market showed substantial
results in 2003, with Fluor's Government business posting
record earnings on an increase of 62 percent.
     While selectively pursuing near-term prospects, we                       
also continued to position the company for longer-term
                                                                   n 2003, we achieved our fourth consecutive year of earn-
growth opportunities, particularly in the transportation and
                                                                 ings growth, with earnings from continuing operations of
global oil and gas markets. Our reputation and capabilities
                                                                 $179 million, or $2.23 per share, an increase of 6 percent
in the large transportation market continue to expand as we
                                                                 over last year. Consolidated operating profit was $406 mil-
target key projects where we bring unique value that differ-
                                                                 lion, down 2 percent from a year ago. An improvement
entiates us from our competition.
                                                                 in the operating margin partially offset the impact of the
     In the oil and gas market, our geographic presence and
                                                                 decline in annual revenues, primarily related to the cycle
experience in challenging international locations, such
                                                                 of project completions in our power business and a shift
as the former Soviet Union, China and the Middle East, is
                                                                 to the early stages of a new cycle of projects in our oil and
providing a critical competitive advantage. For example,
                                                                 gas business.
Fluor's ability to quickly put resources in place in Iraq, has
                                                                      We also posted encouraging growth in new awards and
led to a growing level of participation in the reconstruc-
                                                                 backlog, while remaining fully committed to selectivity and
tion effort. Several significant oil and gas awards were
                                                                 financial discipline that is fundamental to our business
booked during the year, with ongoing development driv-
                                                                 strategy. New awards increased 16 percent to $10 billion,
ing increasing levels of capital investment in these key
                                                                 while consolidated backlog grew 9 percent to $10.6 billion.
geographic markets. Overall, we're very well positioned to
                                                                 In particular, Fluor's Government Group nearly doubled
assist our clients as they direct their capital spending in
                                                                 their new awards for the year, while O&M posted a 23 per-
these expanding global markets.
                         




                                                                                                                             

cent increase in new business. In addition, we booked sev-      which plays to Fluor's strengths, along with a growing list
eral major new oil and gas projects, increasing new awards      of more moderate-sized prospects. In addition, a contin-
by more than 80 percent, reflecting tangible evidence that      uation of the global economic recovery, which began to
the anticipated cycle of substantial capital investment in      strengthen late in the year, should add further momentum
this global market has begun.                                   to our positive outlook for new capital investment across
                                                                a majority of our markets.
                                                                     Complementing our pursuit of organic growth
                                                                within each of our business segments, is our ongoing
                                                                search for niche acquisitions to further penetrate and
                                                                expand our share in targeted markets. During 2003, we
                                                                completed three acquisitions, two focused on expand-
                                                                ing our growth potential in the Government market, and
                                          
                   
                                   
       
                                                                another directed at enhancing our turnaround services
                                                                offering in the O&M market.
                            
                                                                     In last year's report, I outlined our goal to increase the
   luor's market diversity has long been a key strength in      proportion of revenue and operating earnings from these
reducing the impact of cyclicality in individual markets        two less cyclical markets to approximately 40 percent of
and enhancing consistency in long-term performance. Our         Fluor's total business mix. Significant progress was made
broad geographic scope and experience across the breadth        toward this objective in 2003, with the combined revenues
of the markets that we serve, combined with our financial       of these businesses increasing to 32 percent from 19 per-
strength, provides a competitive advantage that is virtually    cent a year ago, while operating earnings grew to 36 percent
unmatched within our industry.                                  of the total from 30 percent last year.
     Over the past few years, we have concentrated our               Additionally, in February 2004, we completed a fourth
attention on executing our diversification strategy to          acquisition, further strengthening our capabilities and
achieve more consistent growth, deliver solid returns           presence in the Government market. Fluor's Del-Jen unit SM



on capital and enhance shareholder value. Our portfolio         acquired Trend Western Technical Corporation, which
management approach allows us to quickly and efficiently        specializes in logistics services and base operations sup-
move resources between markets to take advantage of cycli-      port. Trend Western has previously teamed with Del-Jen,
cal upturns and reduce costs in the downturns. Combined         and will be fully integrated into the company, enabling the
with our actions to increase our most stable businesses in      combined entity to provide a full spectrum of support ser-
proportion to our overall business mix, we have achieved a      vices to the Department of Defense at military installations
good balance across our entire business portfolio and are       worldwide. We intend to continue to pursue well-managed
extremely well positioned in each of our market areas.          and accretive acquisition candidates that complement and
     We have continued our unrelenting focus on market          enhance our long-term growth potential.
selectivity, execution excellence, financial discipline, risk        Overall, we believe significant progress was made
management and meeting our clients' needs. We remain            during the year in executing our business strategy and we
absolutely committed to these principles and believe that       are cautiously optimistic about our prospects in the coming
they provide the strong foundation for our current and          year. We expect that 2004 will be a year of transition, as we
future success.                                                 move from the completion of a cycle of power projects to a
     Looking ahead to 2004, we expect to build on the           new cycle of oil and gas projects.
growth achieved in new awards and backlog during 2003,               This shift from the conclusion of a cycle to the begin-
driven by anticipated increases in capital spending by a        ning of a new one has implications for the company's near-
number of major clients. We remain convinced that the           term earnings outlook. Because projects often take two to
market for our services, particularly in the global oil and     three years or longer to complete, revenues and earnings
gas industry, is in the early stages of a long-term cycle of    recognized in the early stages of project execution reflect
investment that will continue to unfold over the next three     the lower volume of work performed during engineering
to five years. We are actively tracking a number of large,      and project planning. As projects get closer to comple-
complex projects in geographically challenging locations,       tion the volume of work performed is at a peak, resulting
                                                                in higher revenue and profit recognition. This means that
                      




                                                                                                                           

there is a lag time between when projects are booked and              Effective February 3, 2004, Fluor announced the
when they begin to materially contribute to the bottom line.     election of Suzanne H. Woolsey to its board. Dr. Woolsey
For this reason, an important leading indicator of future        served as the chief operating officer and most recently, the
performance will be the trend in new awards and backlog,         chief communications officer, for the National Academies,
which we expect will be positive in 2004.                        the advisors to the nation on science, engineering and
     While the precise timing of major new awards, particu-      medicine. Her expertise in and passion for government
larly in the oil and gas market, remains difficult to predict,   policy, private industry and science will be important
continuing front-end activities, positive ongoing client         enhancement to our board.
discussions and improving global economic conditions all              At year-end, Admiral Bob Inman, a long-time mem-
point toward major new investment. An additional factor          ber of Fluor's board, retired. During his tenure on the
that could influence earnings performance in 2004 is the         board, Bob's depth of political insight, awareness of global
extent of work Fluor performs in Iraq, given the fast-track      changes and understanding of technology provided invalu-
nature of the reconstruction effort. Fluor is engaged in a       able leadership to Fluor. Bob has served with distinction
variety of activities in Iraq, with continuing potential for     on the board since 1985 and we thank him for his many
expansion of existing task orders as well as further new         contributions and long, dedicated service. We also saw the
opportunities that are being actively pursued.                   departure of Paul Anderson from our board, who resigned
     Overall, we are optimistic about our business prospects     to become the chairman and chief executive officer of
in 2004 and beyond, and are confident that our strategy          Duke Energy.
of market diversity will continue to allow us to deliver long-
term earnings growth. However, given the timing issues
and potential variables that could affect earnings in 2004,         would like to thank our board of directors and employees
we have chosen to remain somewhat cautious in our earn-          for their outstanding contributions during the year.
ings guidance at this early stage, until we gain greater vis-    With their hard work and dedication we made significant
ibility as the year progresses.                                  progress during the year on a number of our strategic
                                                                 objectives and delivered a strong return to shareholders.
            
                                                                 Their commitment to our guiding values of safety, integ-
   luor's financial condition remains strong and will con-       rity, teamwork and excellence in everything we do are key
tinue to be a high priority. Our "A" investment grade credit     to achieving our vision to be the premier engineering and
rating and strong balance sheet provides a distinctive com-      construction company in the world.
petitive advantage, and ensures access to letters of credit            Finally, let me express my appreciation to our fellow
and bonding capability, which is critical to executing our       shareholders for their support and confidence in Fluor and
business. At the end of 2003, Fluor had cash and securi-         its future. We are confident in our global market diver-
ties of $497 million and a debt-to-total capital ratio of 20     sification strategy and I believe that we have never been
percent. Our financial strength will continue to provide         better positioned to deliver increasing shareholder value
the financial capability to fund internal growth initiatives,    than we are today. Our financial strength, coupled with the
strategic acquisitions and pay dividends.                        quality of our services and people, and the reliability of
                                                                 our performance, are well aligned with our clients' needs.
         
                                                                 We continue to build the strong foundation for long-term
                                                                 growth and to make our goals for the future a reality. It is
  n October 2003, we were pleased to welcome Admiral
                                                                 with tremendous pride that I serve this great company and
Joseph Wilson Prueher to Fluor's board of directors. He
                                                                 look forward to the exciting future that lies ahead.
brings an international, informed and seasoned set of
perspectives, as well as valuable expertise on China and
the federal government. Following 35 years of distin-
guished service in the U.S. Navy, Admiral Prueher served
as ambassador to the People's Republic of China from 1999
                                                                 Alan L. Boeckmann
to 2001. He currently is a consulting professor at Stanford
                                                                 Chairman and Chief Executive Officer
University's Institute of International Studies and a senior
                                                                 March 3, 2004
advisor on The Preventive Defense Project.
