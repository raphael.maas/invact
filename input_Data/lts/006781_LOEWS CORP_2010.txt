letter to our shareholders and employees

Loews posted good operating results in
2010 and, despite the feeble economic
recovery in the United States, ended the
year in strong financial shape. During
the year, our holding company cash
and investments grew to more than
$4.6 billion from $3.0 billion at the end
of 2009. It might seem unconventional
to begin our shareholder letter with
this statistic rather than earnings
per share or net income, but in our
view, the ability to generate cash is an
excellent measure of financial health
and success.

Maintaining a strong and liquid balance sheet has served us well over
many years by allowing us to move rapidly to take advantage of
opportunities such as acquisitions and share repurchases, as well as
to invest in our existing subsidiaries. We recognize that Loews is not
alone in being awash in liquidity  U.S. corporations are currently
holding a record level of cash and equivalents. What distinguishes
Loews from many other companies, however, is that our cash is
readily accessible  it is not trapped off shore nor is it encumbered.
As much as we value the flexibility that a surplus of cash provides, we
do not consider it to be a permanent holding. We continually evaluate
opportunities but feel no pressure to invest, even if low interest rates
have resulted in uninspiring returns on our cash portfolio. More often
than not, the return on our periodic investments more than adequately
compensates us for the intervening periods of lower returns.
Our restraint served us well this past year, as it was a challenging
environment in which to find investments meeting our financial
criteria. Private equity firms have large sums of capital that they
are eager and motivated to put to work, and historically low interest
rates have adjusted downward the return requirements for most
investors in search of yield. In any given year, Loewss balance of cash
and investments might fl uctuate, but our ongoing goal is to deploy
capital in ways that ultimately create enduring value for shareholders.
In keeping with our long-held strategy of repurchasing our common
stock at prices below our view of its intrinsic value, we repurchased
11 million shares during the year, or nearly 2.6 percent of shares
outstanding, at an aggregate cost of $405 million.
Dividends and interest received by Loews from three of our
subsidiaries  Boardwalk Pipeline, CNA Financial, and Diamond
Offshore  totaled $720 million in 2010. We received another
$1.0 billion from CNA when it redeemed the remaining preferred stock
that we had purchased from CNA in 2008 to provide capital support
during the fi nancial crisis. Additionally, our holding company portfolio
of cash and investments generated a net gain, including interest and
dividends, of $187 million.
For 2010, net income increased to $1,288 million from $564 million in
the prior year, refl ecting growth at Boardwalk Pipeline and steady
performance at CNA. We also experienced some ongoing challenges
in the respective industries of Diamond Off shore, HighMount E&P,
and Loews Hotels. But our results for 2010 and 2009 also included
some signifi cant charges (after tax and noncontrolling interest): in
2010, a charge of $328 million related to CNAs Loss Portfolio Transfer
transaction, and in 2009, a non-cash charge of $660 million at
HighMount related to the carrying value of natural gas and oil
properties as well as other-than-temporary impairment charges of
$791 million at CNA.
diamond offshore
Diamond Offshore had no involvement with the blowout of BP
plcs Macondo well on April 20, 2010, but along with the entire
off shore drilling industry, it has suff ered the consequences of the
ensuing drilling moratorium in the U.S. Gulf of Mexico (GOM).
On May 30, 2010, the U.S. government declared a six-month
moratorium on drilling activity in the GOM at water depths greater
than 500 feet, and subsequent rules effectively prohibited most
off shore drilling. After much legal wrangling, the Department of the
Interior offi cially lifted the moratorium on October 12, 2010, but the
imposition of comprehensive new regulations has been tantamount
to a de facto drilling ban. Diamonds customers must obtain a permit
from the Bureau of Ocean Energy Management, Regulation and
Enforcement (BOEMRE) before drilling operations can commence,
and since the moratorium was fi rst declared, the BOEMRE has been
slow to issue new permits.
Regulatory uncertainty in the GOM has forced drillers, including
Diamond Offshore, to look for opportunities to move rigs to
international markets in order to keep them employed. Since the onset
of the moratorium, Diamond Offshore has mobilized two
semisubmersibles, one to West Africa and one to Egypt, and has idled
a number of other rigs. In the years preceding the Macondo incident,

Diamond had already implemented a significant strategic
repositioning of its semisubmersible fleet from the GOM to
international markets. As a result of these eff orts, and the departure of
additional rigs in 2010, Diamond has only three semisubmersible rigs
and two jack-up rigs operating in U.S. waters. Despite its reduced
exposure to the U.S. market, Diamonds results for the second half
and the full year were negatively impacted by the drilling moratoriums
eff ects on average utilization and dayrates.
Given ongoing demand for oil and the industrys continuing success
in finding significant hydrocarbon reserves in ultra-deep waters,
Diamond remains optimistic about the long-term prospects for
off shore drilling in international deepwater markets. Diamond has
ordered two new-build ultra-deepwater drillships, the Ocean
BlackHawk and Ocean BlackHornet, which are scheduled for delivery
in 2013. These two drillships will be completed at costs signifi cantly
lower than the peak prices seen just a few years ago.
cna financial
In 2010, CNA improved its operating results and strengthened its
balance sheet. A cornerstone of its balance sheet strength is a
disciplined approach to reserving, as evident in CNAs sixteenth
consecutive quarter of favorable prior year reserve development in its
core Property & Casualty Operations.
CNA continues to make progress on its strategic priorities to drive top
and bottom line growth by: (i) developing and deepening expertise in
targeted industry segments, (ii) managing the mix of business to
improve profitability, and (iii) extending its geographic reach. In
support of these goals, CNA has continued to emphasize disciplined
risk selection and pricing, while also building a more eff ective fi eld
organization by opening more branch offices, upgrading field
leadership, and strengthening relationships with agents and brokers.
All of these actions are intended to drive more profi table business to
CNA, and the early returns on these investments are encouraging. In
Commercial Insurance, CNA achieved rate increases and increased
new business in growth areas such as technology and manufacturing.
In Specialty Insurance, CNA continued to perform at a very high level
while also delivering new business in its growth areas.
CNA has made signifi cant progress in improving its fi nancial strength
and stability. In the third quarter, CNA ceded its exposure to legacy
asbestos and environmental pollution liabilities through a Loss
Portfolio Transfer reinsurance transaction with National Indemnity
Company, a subsidiary of Berkshire Hathaway. The transaction
substantially mitigated the risk and volatility from these legacy
liabilities in CNAs runoff operations.
As mentioned earlier, CNA redeemed the remaining $1.0 billion of the
$1.25 billion of senior preferred stock purchased by Loews in
November 2008. The redemption of the senior preferred stock,
combined with CNAs overall fi nancial position and results, enabled
CNAs board of directors to declare a quarterly common dividend of
$0.10 per share in February 2011. This action will deliver cash to CNAs
shareholders, including roughly $24 million to Loews in the first
quarter of 2011.
We feel confi dent in the quality of CNAs balance sheet, reserves, and
capital position. In February 2011, CNA completed a $400 million debt
off ering, with the proceeds used to retire senior notes due in August of
this year. CNA does not have any other signifi cant debt maturing until
December of 2014.
other subsidiaries
Below are some 2010 operational highlights for our other
three subsidiaries.
 HighMount named Steve Hinchman as its new Chief Executive
Offi cer. Steves leadership and experience will be of great value to
HighMount, given the challenging operating environment in the
E&P sector stemming from weak natural gas prices. To better
position itself to succeed in this environment, HighMount sold noncore
assets in Alabama and Michigan and used the net proceeds
of approximately $500 million to reduce its outstanding term loan to
approximately $1.1 billion. Going forward, HighMount will focus on

improving the performance in its core business and growing its
asset base  both organically and through acquisitions.
 Boardwalk Pipeline increased its operating revenues as its pipeline
expansion projects came fully online. Boardwalk received regulatory
approvals to operate all of its major expansion projects at their full
design capacities and also completed a number of compression
projects to further increase pipeline transmission capacity.
 Loews Hotels saw signs of improvement, but recovery in the lodging
industry continues to be slow. Revenue per available room increased
by 10 percent versus 2009, but this represents a 19 percent decline as
compared to 2008. Loews Hotels opened one new property  the
414 room Loews Atlanta Hotel  during 2010.
2011 outlook
Though the U.S. economy has shown some signs of improvement, we
remain cautious about the future. Persistently high unemployment,
government budget crises at all levels, and a glut of unsold housing
inventory represent just a small sampling of potential impediments to
the health of the economy. These concerns, however, are not cause for
worry with respect to Loews. While we are not impervious to the state
of the economy, our companys structural diversification, liquid
balance sheet, and conservative capital structure position Loews to
ride out diffi cult periods  as well as take advantage of opportunities
that may present themselves. Each of our subsidiaries is aff ected
diff erently by factors in the economy and each is able to carve out a
growth strategy in its own industry. What our subsidiaries all share is
our conservative, long-term approach to building shareholder value.
Our patient, value-oriented approach has worked well over the years.
Over the past 50 years, Loews has delivered an average annual total
return of 17.6 percent, versus 9.6 percent for the S&P 500 Index. While
we cannot promise comparable returns in the future, we believe we
can generate value for all shareholders by focusing on the long term.
We would like to thank our employees, directors, and the employees of
our subsidiaries for their contributions over the past year to the
success of Loews. We are grateful for your support as we endeavor to
continue creating value over the long term.

Sincerely,

James S. Tisch Andrew H. Tisch Jonathan M. Tisch
Office of the President
February 23, 2011
