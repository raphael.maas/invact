Dear Fellow Shareholders:

As we review 2003 and begin the new year, we
reflect on the factors that have contributed to
the marked improvement in the coal marketplace
during the year, in contrast to the weak conditions
we described in our annual report one year
ago. High natural gas prices, the improving U.S.
economy, the strong world economy, the weak
U.S. dollar, higher ocean freight rates and a
relatively cold winter have all contributed to
this improvement. Demand for energy is strong
today and will continue to be strong. And the
demand for coal is no exception.
At the same time, there appears to be a
shortage of coal to meet this increasing demand.
In our own Central Appalachian region, for example,
the supply/demand imbalance has been
further impacted by an estimated 15 million ton
decrease in production in 2003 versus 2002. But
the coal shortage is actually a worldwide issue.
The lack of availability from China and other coalproducing
countries may well be more than a
short-term phenomenon.
The current and future coal markets will
present positive opportunities for Massey
Energy in the months and years ahead. We
remain the largest producer of metallurgical coal
in the United States at a time when the shortage
of high-quality metallurgical coal throughout the
world is particularly acute. During most of 2003,
the metallurgical coal market remained sluggish.
The coal we supplied to steel companies
domestically and via export fell to a 10-year low
of 9.6 million tons, with significant metallurgical
coal production being utilized as high-grade utility
coal. But given the market improvement, we have
recently taken a number of positive steps to free
some of this metallurgical tonnage for sale to the
steel industry, both domestically and overseas.
In order to increase our production of steam
coal at lower costs, we have worked to grow our
surface mine capacity. However, the difficulty of
obtaining permits has long restricted our growth
and our ability to develop lower-cost reserves.
For the first time in many years, we have recently
been encouraged by the improving flow of
permits in our region, at both the state and the
federal levels. Permitting problems remain, and
we continue to face ever-changing regulatory
requirements, but we are pleased to have
acquired or received permits in 2003 covering
over 100 million tons of reserves.
During 2003, we were also successful in
putting in place attractively priced financing to
support our operations for the mid-to-long term.
This refinancing allowed us to realign our debt
structure, reducing short-term bank debt and
extending our maturities. We began the process
with the issuance of $132 million in 4.75%
Convertible Senior Notes due May 15, 2023,
and continued with the issuance of $360 million
in 6.625% Senior Notes due November 15, 2010.
At year end, virtually all our debt was long term,

our debt-to-book capitalization was just 51% and
available cash totaled over $88 million. In January
of 2004, we further enhanced our liquidity and
secured letters of credit capacity by entering
into a new asset-based revolving credit facility
which provides for borrowing up to $130 million.
We were disappointed in 2003 with our net
loss and our inability to make more progress in
improving our overall productivity. We are not
where we had hoped and planned to be in terms
of reducing our costs, but we continue to focus
on better and more cost-effective execution at
each of our mines and are confident that we
can achieve far more. We reduced our capital
spending in 2002 and 2003 (exclusive of reserve
purchases and the buyout of operating leases)
to a level that largely represented maintenance
of our current infrastructure. However, in 2004
we are in the process of purchasing significant
new surface mine equipment, which will be
utilized on several of our newly permitted surface
mines, help us increase our capacity and aid us
in improving our efficiency and productivity.
Looking forward to 2004, we are excited
and energized. The marketplace for coal has
surpassed our expectations and we remain
uniquely positioned in the Central Appalachian
region where demand is increasing and supply
has been shrinking. We recognize that we
have much to accomplish in order to improve
our execution and productivity. However, we
are confident that we can successfully leverage
the strength of both the market and our skilled
and dedicated members.
Thank you for your continuing support of
Massey Energy Company.
Don L. Blankenship
Chairman, Chief Executive Officer and President

