
Fellow Shareholders:

Edison International in 2008 largely achieved the       of Edison International's growth potential, which we
performance goals we set for ourselves at the begin-    believe is among the best in our industry.
ning of the year, and net income grew 11 percent to
                                                        1. Investments in renewable energy, grid reliability,
a record $1.22 billion, or $3.69 per share.
                                                           and advanced energy technologies are important
Based solely on these measures, we would consider          public policy objectives at the national, state and
2008 to be a good, solid year of performance.              local levels. Investment to meet these objectives
But of course it was not a normal year, for Edison         forms the core of our growth strategy at both
International or for the economy as a whole. Our           SCE and EMG.
accomplishments must be viewed in the context of
                                                        2. We have the scale and stability of one of the
the dramatic decline in the economy and the stock
                                                           largest electric utilities in the country, coupled
market. Our stock price fell 39.8 percent in 2008,
                                                           with the upside opportunity of our competitive
generally in line with the 38.5 percent decline of
                                                           generation business and its strategic flexibility
the S&P 500.
                                                           to participate in multiple markets.
Our peers, measured by the Philadelphia Utility
                                                        3. We have a strong financial position. Our
Index, performed somewhat better as a group, with
                                                           total liquidity at year-end was in excess of
a decline of 29.9 percent for the year. This index
                                                           $5.5 billion, we have no significant long-term
consists of "pure-play" utilities, where essentially
                                                           debt maturities until 2012, and we generate
the entire business is regulated, and "integrated"
                                                           strong cash flow from operations.
utilities like Edison International. We operate both
a regulated utility, Southern California Edison         4. We have skilled and committed employees, led
(SCE), and a competitive generation business,              by able and seasoned management, focused on
Edison Mission Group (EMG). In 2008 our stock              operating safely and serving our customers and
performed somewhat worse than the pure-play                the communities in which we do business.
utilities, which were viewed as more insulated from
the economic turmoil, but better than the pure                    SUPERIOR EXECUTION OF OUR
competitive generation businesses, which were                         GROWTH PROGRAMS
impacted greatly.
                                                        Our ability to realize Edison International's growth
                                                        potential rests principally on how we execute two
In this, my first letter to shareholders as chairman
                                                        initiatives: a five-year, $21 billion capital investment
and CEO of your company, I want to share what I
                                                        plan at SCE, the largest such program in our history;
believe underlies the value creation opportunity at
                                                        and the development of a large renewable energy
Edison International. We have a strong foundation,
                                                        project pipeline at EMG consisting of approximately
and we are in a good position to achieve our sub-
                                                        5,000 megawatts of potential wind and solar energy
stantial growth objectives  but it will require
                                                        projects. Successful execution of these plans would
superior execution, financial discipline and innova-
                                                        benefit electricity customers and the environment,
tive approaches to our business.
                                                        and dramatically grow the earning assets of the
                                                        company over the next few years.
       THE FOUNDATIONS OF OUR VALUE
Neither the global financial crisis nor the weakened
economy has altered the four most significant drivers
PAGE 4 EDISON INTERNATIONAL 2008 ANNUAL REPORT


REPLACEMENT STEAM GENERATOR EN ROUTE TO SAN ONOFRE




These investment and development programs consti-           Our proposal to launch an innovative
                                                        I
tute a large, diverse and extremely complex volume          solar initiative using the rooftops of large
of work. They demand superior execution on many             commercial buildings.
fronts including planning, permitting, engineering,
                                                        In addition to the challenges associated with com-
procurement and construction management.
                                                        pleting these large capital projects, there is another
                                                        concern: the financial impact on SCE customers.
The SCE capital investment program. SCE has
                                                        The accelerated pace of investment in reliability
increased its infrastructure investment significantly
                                                        and in a smarter, cleaner, electricity system will put
over the last three years, and 2009 will be particu-
                                                        upward pressure on SCE customer rates in 2009
larly important as the company ramps up to an
                                                        and beyond, an issue to which we are acutely sensi-
even greater level. Projects totaling as much as
                                                        tive given the weakened economy. Declining fuel
$3.6 billion are planned in five main areas:
                                                        costs will partially offset rate changes in the near
    Reliability investments in the distribution
I                                                       term, but we are taking action on multiple fronts to
    system;                                             help our customers.
    Transmission investments, particularly to enable
I                                                       As always, we are aggressively promoting our
    the development of new sources of renewable         energy-efficiency and demand-response programs to
    energy;                                             help SCE customers lower their monthly electricity
    Edison SmartConnect , our advanced
                         TM
                                                        bills. We offer extended payment arrangements to
I
    meter program;                                      customers experiencing difficulty paying their bills,
    Replacement of steam generators to extend the       and rate reduction programs to income-qualified
I
    life of our San Onofre Nuclear Generating           customers. For customers struggling most, we offer
    Station; and,                                       direct financial assistance through the Energy
                                                        Assistance Fund.



We are also working with legislative leaders,           and financing required to bring a new wind farm
public officials and consumer groups to address         on-line. This also will demand superior execution,
an inequity in electricity rates that was created       repeated consistently across multiple projects in
unintentionally by the state legislature during the     several states.
California energy crisis. Today, approximately
                                                        The wind development program will for the
50 percent of SCE's residential customers shoulder
                                                        foreseeable future remain the anchor of our
100 percent of most new costs. Reform would
                                                        renewables business at EMG, but in 2008 we also
enable the California Public Utilities Commission
                                                        advanced our solar program significantly. We are
to distribute costs fairly while maintaining existing
                                                        exploring more than 30 sites in six Western states
protections for low-income customers.
                                                        for potential solar projects. These generally are
                                                        large-scale projects of 20 to 100 megawatts per
EMG renewable energy and natural gas-fired
generation development. At EMG, the core of             site. We are processing interconnection requests,
our growth plan is the continued expansion              taking steps to secure land rights, and competing
of our wind portfolio. We made good progress in         for long-term power sale contracts to utilities.
2008, increasing our installed wind capacity by         Significantly, we signed a strategic development
70 percent. At year-end we had projects in nine         agreement with a photovoltaic solar panel manu-
states, with 962 megawatts in service and 223           facturer, First Solar Electric, LLC, to develop large
megawatts under construction.                           solar utility projects in certain markets.
Our wind development pipeline consists of               In addition to EMG's developments in renewable
approximately 5,000 megawatts of potential              energy, we are also exploring opportunities to
projects in 13 states. As we identify projects to       diversify our portfolio with gas-fired generation.
develop that meet our return criteria, our chal-        A major potential advance occurred in 2008
lenge is to drive them to completion through the        when the company was awarded a 10-year,
permitting, grid interconnection, construction
PAGE 6 EDISON INTERNATIONAL 2008 ANNUAL REPORT


                 MOUNTAIN WIND PROJECT, WYOMING




competitively bid power sales agreement with             was well received. We were among only a handful of
                                                         companies able to raise meaningful funds during
SCE for the output of the Walnut Creek project, a
                                                         this period.
500-megawatt gas-fired plant in the Los Angeles
basin. Although uncertainties about adequate
                                                         EMG has been building liquidity and extending
availability of emissions offsets caused us to write
                                                         debt maturities over the last two years with several
off some early stage project costs, we are actively
                                                         highly favorable opportunistic financings. We did
pursuing these offsets and remain committed to
                                                         this to meet our significant growth investments and
this $600 million project.                               other obligations even if credit markets tightened.
                                                         These efforts have provided the significant cash
  MAINTAINING GOOD FINANCIAL DISCIPLINE
                                                         needed through summer 2009 to complete wind
Our growth programs require large amounts of             power projects in development. As long as credit
capital. It is essential that we maintain financial      markets remain difficult, the availability of project
discipline in order to give investors the confidence     financing will be an important factor in any decision
to fund our projects. Financial strength is a            to commit to new construction projects beyond
competitive advantage we want to protect and extend.     those currently planned.
Our short-term response to the financial crisis          For companies that remain strong and stable, the
has been to follow our natural inclination to            present environment may actually create opportuni-
manage financial risks on the conservative side. In      ties. The difficulty and increased expense of obtain-
September, we took the precaution of drawing from        ing investment capital, for example, only intensifies
our lines of credit approximately $2.1 billion, even     the challenge of building new power plants. That
though we had no immediate need for the funds.           may ultimately increase the value of existing capac-
If a long line started to form at the banks, we wanted   ity and make the need for new generation all the
to have already been there.                              more urgent in an economic recovery, rewarding
                                                         those who are in position to move quickly. We aim
We further enhanced our liquidity and tested
                                                         to maintain the financial strength and flexibility to
the bond market in mid-October at SCE with a
                                                         pursue these opportunities.
$500 million, 5.75 percent coupon offering, which
PAGE 7 EDISON INTERNATIONAL 2008 ANNUAL REPORT


                                                 SOLAR ROOFTOP INSTALLATION, FONTANA, CALIFORNIA




                                                       for renewable energy. California's goal for the state's
        INNOVATIVE APPROACHES FOR A
                                                       investor-owned utilities is for 20 percent of our
          LOW-CARBON ECONOMY
                                                       customers' energy needs to come from renewable
Meeting society's demands for a cleaner, more
                                                       sources by 2010, and there is growing support to
robust electricity system will require fundamental
                                                       raise that goal to 33 percent by 2020. SCE currently
changes to the way electricity is generated, trans-
                                                       leads the nation with a renewables portfolio of
ported and distributed to customers. Transitions
                                                       nearly 13 billion kilowatt-hours, representing
of this magnitude require insightful public policy,
                                                       about 16 percent of our customers' energy needs.
precise planning and huge sums of new capital. It
                                                       And yet with load growth, this portfolio will need
also requires sufficient time for customers, workers
                                                       to more than double in size to meet the higher
and companies to digest the significant costs with-
                                                       goal, and significant new transmission will need
out further weakening the economy.
                                                       to be constructed.
The transformation of the electricity industry
                                                       At EMG, we have been transitioning our investment
requires Edison International to change as well. In
                                                       emphasis to the development of wind and solar
many respects, we are well ahead of the curve; in
                                                       generation. The growth of our renewables business
other areas, we have much work to do.
                                                       both furthers, and is supported by, national goals for
At SCE, approximately 40 percent of our generation     increased low-carbon generation.
already comes from low-carbon sources. Innovative
                                                       With respect to our large fleet of merchant
approaches to renewable energy, energy efficiency,
                                                       coal-fired generation at EMG, the future is more
smart grid technology and electric transportation
                                                       complicated and involves more risk. Approximately
have made us an industry leader in these areas.
                                                       three-quarters of EMG's generation capacity is
This expertise will be increasingly important in
                                                       coal-fired. Coal is an abundant domestic resource,
the transition to a low-carbon economy, and could
                                                       valuable for energy security in an uncertain world,
become a strong competitive advantage.
                                                       and has provided low-cost, reliable electricity to
Significant effort and capital will be required for
SCE to achieve potentially higher state-level goals
PAGE 8 EDISON INTERNATIONAL 2008 ANNUAL REPORT


                       

customers for decades. One half of the nation's           selected by the Electric Power Research Institute to
electricity is produced by coal-fired plants.             host research on advanced control technologies for
However, in addition to concerns about the tradi-         capturing carbon emissions at an existing plant.
tional pollutants emitted when coal is burned, coal
                                                          Achieving these environmental objectives while
is also the most carbon intensive of the fuels used
                                                          continuing to invest heavily in the growth of
for generating electricity. It is the center of atten-
                                                          EMG's renewables business will require significant
tion for our industry in the effort to move to a
                                                          capital. Importantly, EMG pays no regular dividends
low-carbon economy.
                                                          to Edison International for the benefit of sharehold-
Over the last decade, EMG has invested hundreds           ers. Instead, it reinvests all profits to fund environ-
of millions of dollars in our coal plants to reduce       mental controls and grow its low-carbon wind and
emissions of mercury, nitrogen oxide, and sulfur          solar generation portfolio. And EMG only makes a
dioxide. Some of these efforts have been leading-         profit if it can first recover its costs through market
edge, such as the controls installed last year at three   prices in competitive electricity markets. EMG's
of our Illinois plants that have dramatically reduced     low-cost coal fleet has generated substantial cash
mercury emissions. To achieve the further emissions       flow over the last few years, which has allowed it to
reductions required under state and federal regula-       make these investments. That cash flow has proven
tion, EMG will need either to invest billions of          to be volatile and is typically weak during periods
dollars in control equipment and emissions credits,       of low commodity prices, such as those experienced
retire the plants, or do some combination of both.        in the early part of this decade and currently. We
                                                          are highly focused on ensuring we recover the
At the same time that EMG has been reducing the           investments already made, and require any new
traditional pollutants from its coal plants, it has       investments to have a reasonable prospect of
been working on innovative approaches to lower its        providing investors with capital recovery and an
carbon intensity. For example, EMG is testing bio-        adequate return on investment.
fuels such as switch grass and miscanthus that
might replace some of its coal burn. Also, EMG's
Powerton plant in Illinois is one of five sites
PAGE 9 EDISON INTERNATIONAL 2008 ANNUAL REPORT





                                                         committed to the complex job of building value for
                   TRANSITIONS
                                                         all those who count on us to perform: our customers,
In 2008 we said farewell to three leaders who made
                                                         our shareholders, our employees and our communities.
significant contributions to the company's success.
John Bryson retired from the company at the
                                                         Over the last few months I have spent a great deal
end of July after serving as Edison International's
                                                         of time in the field touring our operations and talk-
chairman and CEO for 18 years. Chief Financial
                                                         ing with employees. These visits have given me an
Officer and long-time colleague Tom McDaniel
                                                         even greater appreciation for their skill, and pride
also retired at the end of July, as did General
                                                         in their dedication. The men and women of Edison
Counsel Lon Bouknight. They contributed greatly
                                                         International inspire confidence for the future of
to a substantial growth in the value of the company.
                                                         our company and are eager to take on the work that
                                                         lies ahead.
I would also like to thank and recognize Bob
Smith, who retired as a member of the Board of
Directors in April after 20 years of service to the
company. Bob provided leadership and steadiness
during times of great change and challenge.
He will be missed.
                                                                               Theodore F. Craver, Jr.
                                                                               Chairman, President and
               OUR COMMITMENT
                                                                               Chief Executive Officer
Thank you for your support throughout this past
year. Our goal is to provide you with superior                                 March 2, 2009
results regardless of the economic climate. I have
attempted in this letter to outline the opportunities
that excite us and give us optimism, as well as the
challenges we need to overcome. We are deeply
