Fiscal 2005 was an outstanding year for
Meredith. We produced the best earnings
in our 103-year history and, on July 1, 2005,
we completed our most significant Publishing
transactionthe acquisition of Parents, Family Circle,
Fitness, Child, and Ser Padres magazines. We expanded
our consumer reach, extended our marketing services
capabilities and increased audience share at our
television stations.
We delivered record revenues, net income, earnings
per share and earnings before interest, taxes, depreciation
and amortization (EBITDA). Revenues rose 5 percent
to $1.2 billion. Earnings increased 23 percent to $128.1
million and diluted earnings per share grew 25 percent
to $2.50 before the $0.02 cumulative benefit of a change
in accounting related to option expensing. EBITDA
grew 16 percent to $263.4 million. Net profit margin
improved from 8.9 percent to 10.5 percent and return
on equity increased from 17 percent to 19.7 percent.
Both of our operating units continued to improve
their margins in fiscal 2005. Publishings operating
profit margin rose from 18.1 percent to 19.2 percent.
Broadcastings EBITDA margin increased from 31.8
percent to 35.2 percent.
We continued to return capital to shareholders this year
through dividends and share repurchases. In January, the
Board of Directors increased the quarterly dividend 17
percent, which was on top of a 26 percent increase in
the prior year. Fiscal 2005 was the 12th consecutive
year in which the dividend was raised. We repurchased
1.96 million shares compared with approximately
750,000 in fiscal 2004.
For more detailed analysis of our financial performance, we encourage you to read the Form 10-K, which
is included in this report.
While Meredith has produced outstanding earnings
growth in recent yearsincreasing earnings per share
at a 25 percent compound annual growth rate from
fiscal 2003 to fiscal 2005we assure you that your
company is not resting on its laurels. We are pleased
to report five fiscal 2005 accomplishments that enhance
our ability to sustain strong earnings growth in fiscal
2006 and beyond:
 We broadened our magazine portfolio with the
acquisition of Parents, Family Circle, Fitness, Child,
and Ser Padres.
 We furthered our strategy to serve the rapidly
growing Hispanic market.
We extended our marketing services offerings to
capture more corporate marketing dollars.
 We expanded significantly into the childrens
book market.
 We continued to increase the audience share for
our newscasts at our existing television stations.
We acquired the WB affiliate in Chattanooga and
entered into a joint sales agreement with the WB
affiliate in Kansas City.
Broadening the Magazine Portfolio
The acquisition of Parents, Family Circle, Fitness, Child,
and Ser Padres broadens our magazine portfolio, particularly as it relates to younger women. It increases
our reach and scale, adds to our Hispanic operations
and extends our retail presence.
Our titles now have a combined circulation approaching
30 million, making us the second-largest magazine publisher in the United States in terms of circulation. Meredith
is the leading womens magazine publisher, speaking to
more than 75 million unduplicated women readers. We
are in a position to offer advertisers the ability to reach
women at major life stages, from expecting and new
mothers to women whose children are establishing
lives of their own. Our expanded portfolio provides
unparalleled understanding of the power and influence
of women in todays marketplace. This knowledge will
be critical for us as we execute new programs for our
marketing partners.
The new titles strengthen our core advertising
categories and bolster our targeted growth categories
cosmetics, media and entertainment, automotive, business
and apparel. We now have 11 titles with a rate base of
approximately one million or more. As weve stated
previously, a rate base of this magnitude is critical in
attracting non-endemic advertisers to our magazines.
The newly acquired magazines have significant
profit improvement potential. We have a well laid-out
execution plan and are confident that we will increase
the profit of these titles over time. We will leverage our
editorial, circulation, sales and database expertise to
grow these businesses.
In particular, we will employ our long-term direct-topublisher circulation model that emphasizes our editorial
content and commitment to service journalism. Over
time, this model has proven to be effective in generating
highly profitable circulation. We expect our strategy
will have a positive impact on our new titles as well. 
Furthering Our Strategy to Serve the Hispanic Market
Parents Spanish-language title, Ser Padres, is
an established publication with a distribution of
500,000. Combined with American Babys Hispanic
titles (Healthy Kids en Espanol, 12meses and Espera),
our custom publications and Siempre Mujerour new
Spanish-language womens lifestyle magazineMeredith
is now the leading magazine company serving Hispanic
women and advertisers targeting this rapidly growing
market in the United States.
We believe the Hispanic market represents a significant
growth opportunity. According to the Selig Center for
Economic Growth, the Hispanic market currently represents $700 billion in spending power and is expected to
grow to nearly $1 trillion in 2009. From our research, we
know our core editorial expertisehome and family
is highly important to Hispanic women. We believe our
efforts to establish an early and trusted presence with
these consumers will be rewarded in future years.
Extending Marketing Services
In todays world of media fragmentation, consumers
control when, how and through what medium they
receive advertising and promotion. In response to these
demands, we have developed products and services that
reach target audiences more effectively. Our custom
publishing, database marketing and management,
interactive media, integrated marketing, and crossplatform programsalong with traditional national
and local advertisingare an incredibly powerful
combination. Our model distinguishes us from
competitors and diversifies our revenue streams
beyond traditional advertising. Revenues from sources
other than magazine advertising and circulation were
26 percent of total Publishing revenues in fiscal 2005,
up from 20 percent in fiscal 2002.
In fiscal 2005, Meredith Integrated Marketing
extended its service offerings, enhancing our
ability to capture more corporate marketing dollars.
Traditionally, integrated marketing primarily provided
delivery of custom publishing programs designed to
increase brand loyalty. In fiscal 2005, we continued
to expand our offerings to include strategic customer
relationship management services that include the
Internet, direct mail and e-mail, along with loyalty
magazines. We further leveraged our database expertise
and our 80-million name database to provide data
management and program evaluation services. Our
comprehensive custom marketing programs for Hyundai,
Nestl and Carnival Cruise Lines are excellent examples
of our ability to provide broader services.
Expanding to Childrens Books
Meredith Books expanded significantly into the
childrens market. We published more than 60
childrens books in fiscal 2005 compared with less
than 10 in the prior year. Through relationships with
DC Comics, Marvel Entertainment, Warner Brothers
and DreamWorks, we have established a strong
reputation. We look forward to building on this
success and capturing more of the $2 billion retail
childrens book market. 
Increasing Broadcastings Reach
Meredith Broadcasting continued to increase the audience share for its existing television stations newscasts.
We acquired the WB affiliate in Chattanooga and
entered into a joint sales agreement with the WB affiliate
in Kansas City, which complements our CBS station in
that market.
Improving our news ratings and audience share
is vital to the continued growth of Meredith
Broadcasting. In particular, we have improved the
share for our late news considerably in recent years.
For local television, the fastest growing day-part is
morning news. We posted strong audience share for
most of our morning newscasts in the May 2005
book. In Portland and Hartford, Merediths stations
captured approximately one-third of the television
audience. In Nashville, Kansas City and Saginaw,
the Companys stations garnered approximately
one-fourth of the viewers.
Increasing news ratings and share is important, but
only if the improvement can be monetized. We have
done an outstanding job of growing spot advertising,
outpacing the industry in 16 of the last 18 quarters,
according to the Television Bureau of Advertising.
We know we can do better. We need to ensure that
our revenue share is equal to or greater than our
audience share. New Broadcasting President Paul
Karpowicz has made increasing revenue share a top
priority since joining Meredith in mid-February. He
has instituted quarterly revenue entitlement meetings
with each stations sales management to ensure
accountability for sales results. We are confident this
new process will be very effective for us.
We are very proud of the record earnings in fiscal
2005 and we can assure you that our focus is to sustain
our strong earnings growth. We recognize fiscal 2006
presents challenges, but we have well-defined growth
strategies and a proven track record of execution. We
look forward to building on our strong momentum in
fiscal 2006. On behalf of Merediths 3,000 employees,
we thank you for your ongoing trust and support.
Stephen M. Lacy
President and COO
William T. Kerr
Chairman and CEO