In 2010, Offi ceMax had strong performance, despite
continued macroeconomic challenges that persisted much
longer than had been anticipated heading into the year.
During the year, the company continued to manage costs while
investing in the business, and signifi cantly improved gross
margin in both Contract and Retail. I am proud that our team
more than doubled the companys adjusted operating income
on a year-over-year basis.
In November, I joined Offi ceMax as CEO, as Sam Duncan
retired after successfully leading the company for more than
fi ve years. Prior to accepting this position, I did a substantial
amount of due diligence, and have taken a deep dive into the
company during my fi rst several months here. I have enjoyed
meeting with my colleagues throughout the organization as
well as customers and consumers and other partners to gain
an understanding of the opportunities, challenges and issues
in each area of the business.
As I refl ect on my early experience at Offi ceMax, I see an
organization with excellent opportunities for sustainable,
profi table growth. I am gratifi ed that Offi ceMaxs 30,000
associates view our company values as a way of life, and
integrity is part of the DNA of the company. It is clear to me
that the company has achieved fi nancial stability, and the
infrastructure enhancements that the company is making
will play an important role in the next phase of our journey
toward sustainable, profi table growth.
One of my fi rst priorities going forward is to evolve the
organizational mindset to a growth orientation. This is not an
easy task, and it involves a signifi cant paradigm shift and change
management. As such, I have begun to share my fi rst set of
philosophies for the company to facilitate this paradigm shift:
 Peoplecultivating a culture that leverages the strength
and talent of the organization at all levels by raising the
bar continuously.
 Prioritiesevaluating our opportunities to focus on
initiatives that are meaningful to the growth and profi tability
of the company and can move the needle materially.
 Pacesignifi cantly increasing the speed with which we
change to stay ahead of the curve.
 Processesbeing deliberate, disciplined and thoughtful in
our approach to addressing our opportunities and plans.
 Performanceensuring everyone across the organization
has accountability for our results.
Going forward, we will need to leverage our expense control
strength to the next level and fi nd systematic and programmatic
ways to drive signifi cant and ongoing cost savings. These
programmatic cost effi ciencies will enable us to fund our
growth journey. I have established fi ve near-term actionable
priorities for the organization for 2011 which sharpen the focus
on our core business and should accelerate momentum behind
new business:
 First, continuing to focus on gross margin improvements
and driving expense control and prioritization across
the organization. We will work to identify and implement
one or two major programmatic effi ciencies that will result
in signifi cant savings benefi ts, likely towards the end of this
year or in 2012.
 Second, improving customer retention in the Contract
segment, and vigorously defending our key contracts
through enhanced service. We will evolve our sales
culture from personal relationships to performance-based
relationships with clients. We will also work to stem declines
in the base business through new products and service offerings.
 Third, proactively focusing on advancing the
e-commerce channel for all customer segments as a
P&L driver, and continuing to scale our new channel/
store-in-store concept.
 Fourth, critically evaluating and prioritizing our
fi ve-year plan initiatives. We will focus on a few that
can materially and profi tably drive growth and will work
to reallocate resources to the initiatives that can be
differentiated and scaled.
 Fifth, addressing our organizational capabilities, skill
sets, and talent and knowledge gaps to drive growth
through innovation and leveraging customer and
consumer insights.
Overall, I am pleased with what I have seen during my fi rst
few months at Offi ceMax. I believe that we have an organization fi lled with resilient associates with a can-do attitude.
However, I do believe there is a lot of room for improvement to
achieve a world-class organization, and I have challenged our
teams to raise the bar signifi cantly.
I look forward to continuing to work with our leadership team
as we turn our focus to creating a strong, well-differentiated
company that, over time, becomes an engine for sustainable,
profi table growth.
Sincerely,
RAVI SALIGRAM PRESIDENT AND CHIEF EXECUTIVE OFFICER
March 2011