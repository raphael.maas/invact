Welcome to Our Annual Review
At UnitedHealth Group, we work every day to achieve our full potential
to help improve the health of those we serve and health systems serving
society. The business result is consistent, sustainable growth.
UnitedHealth Group, Optum and UnitedHealthcare
have remarkable potential to help improve health
care and the lives of people around the globe —
a humbling responsibility and social pursuit. The
285,000 people of this enterprise are dedicated
to this task. We are intently focused on serving
individuals, one person at a time, working closely
with clients, customers and health systems to
increase the quality of health care and improve the
affordability and experience of care. This approach
is building trust and loyalty among the consumers
and customers we serve which, in turn, drives
consistent, sustainable, market-leading growth.
This review summarizes our solid performance in
2017 and our plans for 2018 and beyond. We fully
appreciate that our shareholders’ investments enable
us to serve more people in more and better ways
each year. UnitedHealth Group remains committed
to long-term earnings growth and distinctive total
shareholder returns.
Our future success will be powered by the hard,
intelligent and caring work of our people to achieve
our mission and the opportunity to serve offered
by the growing and challenging global health care
markets. Thank you for your interest in our company.
2 | UnitedHealth Group | Annual Review
At its core, UnitedHealth Group
is shaped by its people and their
commitment to a culture, based on
integrity, compassion, innovation,
relationships and performance.
Motivated by 
UnitedHealth Group | Annual Review | 3
UnitedHealthcare is a leader in health
coverage and benefits, and Optum is
the leader for health services, broadly.
These two businesses are purposefully
complementary and positioned to
work together as an integrated and
strategically aligned operating portfolio,
as well as to be fully free-standing,
market-facing businesses. Together, they
leverage our core competencies to apply
actionable insights to some of the most
complex challenges facing health care.
We use Net Promoter Score (NPS) to
measure the quality of our products and
services. NPS is based on the answer to
a single question, “How likely are you to
recommend our company to a friend or
family member?” Our NPS rose sharply
in 2017 and we expect this momentum
to continue through 2018 and beyond.
Our commitment to NPS is about more
than just numbers. We are changing our
culture to be much more consumercentric,
listening more intently than ever
to our customers, taking actions based
on what we learn and monitoring our
improvements over time.
As we continue to improve quality
and service, we build increasing trust
and loyalty among the people and
customers we serve. In turn,
we continue to grow.
Our enterprise is focused on five growth
areas where we believe we have the
opportunity to improve health care and
better serve the needs of individuals
and the system overall. These are:
Health Care Delivery: We have a
growing presence in the direct delivery
of care, where our goals are better
quality, lower cost and higher consumer
value and satisfaction, informed by
appropriate site of service for care.
Pharmacy Care Services: Pharmacy
transactions are the most common
point of contact between health care
consumers and the system. We use
the pharmacy interaction to engage
consumers with our whole-person care
model, to encourage smart decisions
and healthy behaviors.
Consumer-Centric Benefits:
Consumers expect a simple,
personalized, dependable care
experience. Our affordable product
designs use incentives to reward
healthy behaviors and lifestyles,
guiding people along a more
engaged and healthier path.
Digital Health Care: Innovation,
new product development and fresh
approaches are critical to improving
simplicity, connectivity, service and
accuracy, so we continue to grow our
digital offerings and technology solutions
for consumers and care providers.
Global Opportunity: Our global
businesses had strong, positive 2017
performance and are carrying that
momentum into 2018. The closing of
the Banmédica acquisition in the first
quarter 2018 adds a major provider of
health care services and health benefits
in Chile, Colombia and Peru.
These five areas of strategic focus
underpin our strategy as we look to
evolve forward with our customers
and markets toward fulfilling existing
and emergent needs in 2025.
UnitedHealth Group is advancing health care quality by serving others, one person and one health system at a time.
4 | UnitedHealth Group | Annual Review
Since 2010, UnitedHealthcare has
produced one of the strongest periods
of growth for any company in health
care, growing organically in the U.S.
by more than 11 million people. Today,
we serve nearly 50 million medical
members, primarily in the U.S. and Brazil.
UnitedHealthcare is comprised of four
operating segments:
UnitedHealthcare Employer & Individual
provides health benefits for over
27 million Americans through fully insured
and self-funded medical plans. We offer
consumer-oriented benefit plans and
services nationwide for large national
employers, public sector employers,
midsized employers, small businesses
and individuals.
UnitedHealthcare Medicare & Retirement
is dedicated to serving the growing health
and well-being needs of individuals over
the age of 50. We help more than 12 million
seniors manage their health through a
comprehensive and diversified array of
products and services.
UnitedHealthcare Community & State is
dedicated to providing health care products
and services to state programs that care
for the economically disadvantaged, the
medically underserved and those without
the benefit of employer-funded health
care coverage in 28 states and the District
of Columbia.
UnitedHealthcare Global operates in
two distinct segments: Global Solutions
and Global Markets. Global Solutions
serves employers and individuals in more
than 130 countries who live and/or work
outside their home nations. The Global
Markets business serves the in-country
health care needs of specific populations
with health benefits and medical care
delivery, primarily in Brazil and Portugal.
Through our 2018 merger with Empresas
Banmédica, a leading private health
benefits and care delivery provider, we
further expanded our services to Chile,
Colombia and Peru.
Creating a Better Health
Care Experience
We are dedicated to supporting better health
and more affordable care.
At UnitedHealthcare, we
serve people with health
benefits through every stage
of their lives, from childhood
and youth through working
life and into retirement.
We are a company with a
vital social role. Effective,
sustainable health insurance
underpins our nation’s
ability to deliver effective,
sustainable health care.
UnitedHealthcare is
dedicated to supporting
better health and creating a
better consumer experience,
all while making health
care more affordable.
This involves putting the
individual at the center
of how we operate and
equipping the people we
serve to be actively engaged
health care consumers.
We also work with care
providers, collaborating
to establish new ground
rules based on improving
patient care through valuebased
payment initiatives
and incentives.
Following are stories that highlight
how UnitedHealthcare is improving the
delivery of value-based care, addressing
the social determinants of health and
helping make health care more accessible
and affordable for more people, while
continuing to drive growth.
• NexusACO
• myConnections
• Serving Seniors
UnitedHealth Group | Annual Review | 5
Accountable care organizations (ACOs) help reduce costs and improve the quality
of the health care system and health outcomes. This health care model can:
• improve prescribing and patient referrals;
• minimize unnecessary emergency room use and reduce hospital admissions
and readmissions;
• better coordinate care transitions from post-acute to rehab to home; and
• identify gaps in care so they can be appropriately closed.
UnitedHealthcare’s NexusACO is the first national health benefits plan to
integrate value-based ACO contracts with incentive-based consumer benefits.
Many of our employer customers have employees living across the country,
yet most ACOs today only serve a single market. NexusACO represents the next
generation of value-based health plans, organizing our high-performing ACOs
and our premium care physicians into a national-tiered network, all supported by
comprehensive digital resources.
Employees in the NexusACO program receive better care coordination.
Primary care physicians help people navigate the health care system, making
sure they and their families receive the right care with the right doctors at the
most appropriate site of care. By using our real-time data analytics, NexusACO
physicians are proactively engaging with their patients, helping lower costs
and improve health outcomes. And this higher level of quality care is delivered
at lower costs by UnitedHealthcare, helping our members save 8 percent to
12 percent on their health plans.
NexusACO launched in 2017 and is already delivering strong results. We expect
to grow to 250,000 people in NexusACO by the end of 2019.
NexusACO is the first national health
benefits plan to integrate value-based
ACO contracts with incentive-based
consumer benefits.
Lower cost
Better health Better care
Value-Based Agreements
NexusACO:
Expanding Access
to Quality Care at
Lower Costs
NexusACO primary care physicians
help people navigate the health care
system, making sure they and their
families receive the right care with
the right doctors.
6 | UnitedHealth Group | Annual Review
Individuals who are homeless average nine times the number of ER visits, six
times the number of hospitalizations and three times the overall health care
costs of individuals who are not homeless. UnitedHealthcare’s myConnections
is helping these vulnerable people, including individuals eligible for state
programs for the economically disadvantaged and medically underserved,
receive essential social, medical and behavioral services. Through the program,
UnitedHealthcare is arranging for consistent and affordable housing, facilitating
transportation and job training, and connecting people with community health
workers who can help provide additional support.
T.J.’s story is a great example of how UnitedHealthcare can help. T.J. was
homeless and unemployed, on the streets for some time, suffering from
depression, diabetes, asthma and chest pain. Since 2015, he made
254 trips to the emergency room and had 32 admissions to the hospital.
UnitedHealthcare’s myConnections helped T.J. move into a supportive housing
community and buy furniture to set up his apartment. The next step was to
initiate a wraparound health plan, including counseling for his depression,
treatment for his diabetic foot ulcer, help applying for Social Security Disability
and education on rental housing.
T.J.’s health is improving, he is staying out of the emergency room and
beginning to establish goals and imagine a future for himself, working toward
achieving self-sufficiency.
myConnections is helping
people receive essential
social, medical and
behavioral services.
UnitedHealthcare’s myConnections
helped T.J. (pictured right) with
affordable housing and essential
medical and behavioral care.
myConnections:
Addressing
the Social
Determinants
of Health Care
UnitedHealth Group | Annual Review | 7
At UnitedHealthcare, we’ve learned seniors value stability in their health care
benefits and experiences. As simple as it sounds, it makes a huge difference for
the people we serve.
UnitedHealthcare Medicare & Retirement has a well-diversified portfolio of
stable products that will carry the AARP brand exclusively through at least 2025.
In 2017, our new low premium Part D plan grew more than any other competitor
nationally, and we introduced a low premium Medicare Supplement product in
43 states. Across Medicare Supplement, we’ve grown to serve 1.3 million more
people in just the past five years.
In group Medicare Advantage, leading employers continue to choose our products
to serve their retirees, adding more than 850,000 people to UnitedHealthcare over
the last five years. And stability in our individual Medicare Advantage products
has contributed to our record retention level and growth of approximately 1 million
people over the past five years.
But our service and growth story is about much more than product design and
stability. It’s also about the ever-advancing positive, simpler and personalized
experience we create.
For example:
• Navigate4Me provides seniors personalized one-stop service for financial,
social, medical, behavioral and product needs.
• HouseCalls, offered in collaboration with Optum, delivers a personal health
assessment in the comfort of seniors’ homes at absolutely no charge to them
and provides seniors information on current and potential health issues.
We completed 1.3 million HouseCalls and closed more than 2 million gaps
in care in 2017.
Our diversified portfolio of stable
products and services combined
with customer service advocates
who view their role as helping to
save lives — not just answering
the phone — will continue to drive
our growth in the expanding
senior market.
1.3 million
HouseCalls
We completed
and closed more than
2 million gaps in care in 2017.
These meaningfully improved
stability of health care benefits
and premiums for seniors.
In five years, UnitedHealthcare has
grown to serve 1.3 million more
people in Medicare Supplement,
850,000 more in group Medicare
Advantage and 1 million more in
individual Medicare Advantage.
Medicare: Serving
Seniors With
Stable Benefits,
Personalized
Care and Quality
Customer Service
8 | UnitedHealth Group | Annual Review
Optum's capabilities are infused with
OptumIQ — our longstanding expertise in
data and analytics — and delivered across
three businesses:
OptumHealth serves broad market
needs through OptumCare and Optum
Consumer Solutions. OptumCare is a
provider-led, patient-centric, ambulatory
care system that is advancing value-based
care at the local level, delivering the right
care at the right time in the right setting
to more than 14 million patients. Optum
Consumer Solutions helps people achieve
better health and helps employers, health
plans, government agencies and care
providers effectively manage health
care trends. Services include digital
tools, wellness coaching and incentives,
behavioral health, care management,
clinical specialty services and financial
services to help people save and pay for
their health care needs.
OptumInsight provides health care
analytics, software, technology,
consulting and process management,
serving hospitals, physicians, health
plans, government agencies, life sciences
companies and other organizations.
OptumInsight helps clients reduce costs,
meet compliance mandates, improve
clinical performance and adapt to change.
OptumRx delivers a full spectrum of
pharmacy care services that improve
health outcomes and reduce total health
care costs. OptumRx continuously
engages with stakeholders across the
health system to help synchronize
individuals’ clinical care, provide a wholeperson
approach to specialty pharmacy
and home infusion services, simplify
the consumer experience, empower
physicians to make better decisions at the
point of care and drive meaningful savings
in health care expenditures.
A leader in health services,
Optum serves virtually all
types of participants in the
health care system, helping
to create better consumer
experiences, achieve higherquality
outcomes, reduce
costs and improve physician
satisfaction. We serve our
diverse clients and consumers
through five capabilities that
drive meaningful change and
measurably move health
care forward:
• Data and analytics
• Pharmacy care services
• Population health
management
• Health care delivery
• Health care operations
Powering Modern Health Care
Across the Health System
We are helping our clients and the people we serve
solve their biggest problems, navigate change, capture
opportunities and achieve their health care goals.
The following stories share a few
examples of how Optum is making the
health system work better for everyone by
helping people find the right care at the
right time in the right setting, managing
pharmacy care services — the point of
care consumers use most — and helping
improve the quality, effectiveness and
efficiency of health care.
• Surgical Care Affiliates
• PreCheck MyScript
• Performance Analytics
UnitedHealth Group | Annual Review | 9
lower cost compared to care provided
in a hospital setting.
SCA provides care at
50% to 75%
Optum improves clinical
outcomes by developing
data driven, risk bearing,
high-value ambulatory care
systems in local communities.
Surgical Care Affiliates’ (SCA) leading ambulatory surgical care centers add
to our comprehensive care platform of primary and specialty care, urgent
care and surgical care services.
A significant portion of the care currently delivered in hospitals or hospital
licensed facilities can be performed at one of more than 200 facilities in
our growing SCA network. Our clinicians and physician partners provide
a broad range of high-quality surgical and procedural care at 50 percent
to 75 percent lower costs than if the same care was provided in a
hospital setting.
In 2017, approximately 1 million surgeries and procedures were performed
in SCA facilities. Total joint replacement surgeries exemplify the quality
and affordability provided by SCA. These procedures, which often cost
$50,000 or more in a hospital setting, can now be performed at half that
cost in an ambulatory surgery center with outstanding clinical outcomes
and patient satisfaction.
In many markets, we have the opportunity to shift appropriate care to
the ambulatory surgery center setting, while driving outstanding clinical
outcomes and receiving a Net Promoter Score above 90.
Surgical Care Affiliates, which joined
Optum in 2017, provides high-value
surgical services through a network
of more than 200 independent
ambulatory surgical centers and
surgical hospitals in the U.S.
Surgical Care
Affiliates:
Outstanding
Clinical Outcomes
and Patient
Satisfaction at
Lower Cost
10 | UnitedHealth Group | Annual Review
PreCheck MyScript is a breakthrough capability that provides real-time visibility
into patients’ drug benefits. Here’s how it works. Imagine you’re a patient in the
exam room with your physician. Your doctor wants to prescribe a medication.
Because PreCheck MyScript is seamlessly integrated into your medical record,
your physician can immediately look up the desired medication. The two of you
can then talk about whether or not the drug is covered by your health benefits
plan, how much it costs you in out-of-pocket expenses, and whether or not
there’s an alternative medication that might save you money. For your physician,
it’s simple and easy to use.
PreCheck MyScript also alerts physicians if a prior authorization is required. It
enables them to take action, saving patients from potential disruption at the
pharmacy counter. Physicians have the option to either prescribe an alternative
drug or file electronically for authorization. They receive instant notification
regarding approval.
With PreCheck MyScript, patients receive the right drug at the best price with
less hassle. That’s important because if they encounter problems when filling
their prescriptions, they might not fill the prescriptions at all.
UnitedHealthcare has already adopted PreCheck MyScript. In the brief time it’s
been in action, tens of thousands of prescribers have used it, accessing it over
a million times. In 20 percent of the cases where an alternative, lower cost drug
is offered, physicians prescribe it. The results are savings for consumers and our
clients, and less frustration for everyone.
Real-time visibility into
patients’ drug benefits
with PreCheck MyScript
simplifies the drug
prescribing experience for
care providers and patients.
With PreCheck MyScript, physicians
and patients can determine the
out-of-pocket costs for medications
and identify lower-cost alternatives
while in the doctor's office.
PreCheck MyScript:
Prescribing the
Right Drug at the
Best Price With
Less Hassle
UnitedHealth Group | Annual Review | 11
Data and analytics form the foundation of Optum’s health services business. In
2017, we introduced OptumIQ, an integrated, modern architecture for our data
and analytics capability.
Our data is deep and dynamic with nearly 190 million lives of claims data and
over 100 million lives of clinical data, including more than 6.5 billion medical
procedures, over 18 billion lab results, and nearly 6 billion diagnoses. Although
the size of our data assets is impressive, it's the enrichment and curation that
makes them unparalleled in the industry.
Through OptumIQ we are actively investing in the future, leveraging artificial
intelligence — neural networks, machine learning, deep learning and linguistic
analysis — in preparation for the next decade and beyond. By applying artificial
intelligence capable of addressing massive data sets, OptumIQ is uncovering
patterns in quality and outcomes, consumer behavior, cost, risk and utilization,
and operational performance. OptumIQ gets smarter every day, transforming
that data into insight for our clients and helping them more easily keep up with
the pace of change in health care.
Positioned at the epicenter of health care, OptumIQ is building a common
language across the health care system by aggregating, cleaning, normalizing
and integrating countless types of data from different sources.
Using our data and the latest technologies made by experts, for experts,
our people are continually innovating with a purpose to solve real world
challenges — transforming data into insight with industry-leading, dynamic
metrics and measures.
OptumIQ claims and
clinical data provide an
unparalleled database of:
lives of claims data, over
190 million
lives of clinical data, including
100 million
medical procedures,
6.5 billion
lab results and
18 billion
diagnoses.
6 billion
OptumIQ converts data into insights
that care providers, health plans,
employers and government agencies
rely on to improve care quality, better
manage cost and utilization and
enhance consumer satisfaction.
OptumIQ:
Transforming
Data Into Insights
and Actionable
Information
12 | UnitedHealth Group | Annual Review
Commitment to Excellence
UnitedHealth Group, Optum and UnitedHealthcare are committed to strong
fundamental execution on behalf of the people and customers we serve,
innovation, community involvement and value creation for both the health
care system and our shareholders. We are honored to be acknowledged for
our performance by the following awards and recognition.
UnitedHealth Group is the top ranking
company in the insurance and managed
care sector on Fortune’s 2018 “World’s
Most Admired Companies” list. This
is the eighth straight year UnitedHealth
Group ranked No. 1 overall in its sector.
UnitedHealth Group was included
among the 2017 Best Employers for
Diversity by Forbes.
UnitedHealth Group is a member of the
Dow Jones Industrial Average, a blue
chip group of 30 companies deemed
industry leaders.
In 2017, The Civic 50, a Points of Light
initiative that highlights companies
that improve the quality of life in the
communities where they do business,
ranked UnitedHealth Group one of
America’s 50 most community-minded
companies.
Project HOPE, a global health and
disaster response organization, honored
UnitedHealth Group with a 2017 Project
HOPE Global Health Award, which
recognizes outstanding contributions
to advance health outcomes in the
developing world.
UnitedHealth Group has been listed in
the Dow Jones Sustainability World
Index and Dow Jones North America
Index annually since 1999.
In 2017, for the seventh consecutive
year, the National Business Group on
Health honored UnitedHealth Group
with a “Best Employers for Healthy
Lifestyles” top-tier Platinum award.
UnitedHealth Group was named a 2017
Military Friendly Employer by Victory
Media, the publisher of G.I. Jobs and
Military Spouse magazines.
In 2017, for the third consecutive year,
Optum ranked No. 1 on the Healthcare
Informatics (HCI) 100, a listing of the
top health care IT companies based on
U.S. revenues.
Optum360, a leading provider of health
care revenue management services, 