Dear Shareholders:
On December 1st, I had the distinct honor and privilege of becoming the third Chief Executive Officer of
Visa. I am returning to my professional roots, having spent twenty-three years in the payment space, and I
am excited about it.
The payments ecosystem is very dynamic, and Visa plays a very important role in this system and in the
enablement of commerce globally. Every year, every month and even every day, more people around the
world are participating in electronic payments and alternatives to cash and check.
Visa�s incredible acceptance network and its powerful global brand are the foundation on which we
will continue to grow our company. The competitive environment is diverse and growing, but we are as
committed as ever to play a lead role in payments. As a board member for the last three years, I have had a
front row seat to observe Visa�s incredible focus on innovation, convenience, security and the brand. I have
seen the management at Visa energize their teams to think ahead with the goal of helping our many clients.
We will continue to invest in the resources required to move the industry and our company forward.
The payments industry is complicated and involves thousands of different players. At Visa, we are also
committed to being a strong and dependable partner for our clients as well as governments around the
world. Our relationships with issuers, acquirers, merchants, and other industry partners are very important to
us and we will continue to work hard every day to deliver tangible value and service excellence.
As a board member, I have also participated in the development of the company�s current strategy and
I continue to endorse it as we look ahead to 2017 and beyond. Additionally, I have worked with all the
members of the executive team and they are a strong and impressive group of professionals, and I am
thrilled to join their ranks.
The architect of this team and Visa�s success over the past four years is Charlie Scharf, who I have succeeded.
Charlie has been a terrific Chief Executive Officer. He has had a hugely positive impact on our company, and
I look forward to building on the strong foundation he has
put in place. I am grateful to Charlie that he has agreed to
remain available for the next several months.
Let me finish with where I started � I am very energized to
join and lead the team at Visa. In the few weeks that I have
been with the company, I am struck by the dedication of
our people and together we will work hard to build our
business for our client partners and you, our shareholders.
Alfred F. Kelly, Jr.
Chief Executive Offi cer
Visa Inc.