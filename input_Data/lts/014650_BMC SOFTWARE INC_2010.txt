VISION.Chairman and CEO Bob Beauchamp
discusses BMCs strategy, what makes it unique and how its
driving value for shareholders.
Can you start by providing a quick summary of
BMCs strategy?
We have a very clear and compelling vision: to be the de facto IT
management platform for enterprises, IT systems integrators
and IT service providers around the world. Customers want and
need to simplify, standardize, integrate and automate their IT
processes and workflows to better support their businesses, increase
efficiency and reduce costs. Thats what we help them do.
How does this differ from BMCs competitors?
IT management is all we do. Its what we are completely
focused on. And because of this, were not tied to any particular
hardware or software solution. Our platform is agnostic and is
designed to function in a heterogeneous environmentone in
which companies use multiple IT vendors.
Why are BMCs unique focus and heterogeneity
so important?
We think they give us a tremendous advantage. Theyre
why we have the broadest and deepest IT management
solutions portfolio. Theyre why those solutions integrate
so well. Theyre what give CIOs real choices and incredible
economic power in terms of selecting their IT providers
and focusing more on bringing value to the business as
opposed to the underlying technology. Theyre also how we can
give customers a bridge between the past, present and future,
between their legacy systems, their current systems and their
future systems.
How did BMC perform in fiscal 2010?
Overall, fiscal 2010 was another good year for BMC. Considering
the operating environment, it was a very good year. We
met or exceeded all of our key financial metrics. We increased
bookings, revenue, operating margin, earnings and cash flow
from operations.
What were BMCs biggest accomplishments in fiscal 2010?
I think that in a challenging environment, we outperformed our
competitors in terms of gaining marketshare, generating superior
financial results and extending our technology leadership.
Im very proud that we continued to invest in our company by
launching new solutions that respond to key technology trends,
such as cloud computing, virtualization and Software-as-a-
Service (SaaS).
We substantially upgraded our Enterprise Service Management
(ESM) sales force and expect that well increase the average
number of productive sales reps in fiscal 2011 by 20 percent
over last years level. Thats one of the reasons were
Of the top 30 ESM deals
were multi-discipline or
platform deals
Increase in the number of
ESM license transactions
between $500k and $1 million  33%
generating an increase in platform wins for our business.
A case in point: in fiscal 2010 more than 75% of our top
30 ESM license deals were multi-discipline or platform
deals. We had 52 multi-discipline ESM license transactions
over $1 million. Thats an increase of 10% over fiscal
2009. And we had a 33% increase in the number of ESM
license transactions between $500,000 and $1 million.
What is BMCs most important strategic priority?
We want to always have a product and distribution
roadmap that stays ahead of the market. We want to be a
leader, not a follower. That doesnt mean we always have
to be first, but it does mean making sure that we have the
right solutions, that we are in front of the right customers
and that we dont miss any inflection points in the market.
Automation is a case in point. We were in the market at
the right time with the right product at the right price and
this has enabled us to really drive our IT management
strategy forward.
How will BMC maintain its competitive edge?
We will do that by investing in our business for the longterm,
and by not thinking and acting for the short-term.
Whether its attracting and keeping the best people,
developing the best technology or building the best distribution
systems, were determined to do whats necessary
to stay ahead of our market.
Last year, for example, our ESM business unit filed 20
patent applications and our MSM business unit filed 14.
The previous year we purchased BladeLogic, a leader in
automation. These are examples of how were investing
internally and externally to maintain our technology
leadership and broaden and deepen our portfolio.
Can you comment on how the IT marketplace
is changing?
Its interesting: back when I started out in my career, much
of the IT world was vertically integrated. You had a few big
players that dominated the whole hardware and software
landscape. Then enterprise applications came along that
could work on any system and the vertical stack began to
disintegrate. Now we are starting to see larger IT players
look for growth by expanding into adjacent markets and
moving back towards a vertically integrated strategy.
Its hard to say how it will turn out. I havent met any
customer who was excited about locking in and committing
to a single vendor. They seem to want multiple
products from multiple providers. We think this can be a
big positive for BMC, because we give them the flexibility
to maintain choice and heterogeneity.
How is BMC adopting new technologies and its own IT
management solutions to drive its business forward?
Let me give you one example. Using virtualization
technology, we have created an internal cloud computing
environment for our software development engineers.
We took development labs around the world and consolidated
them into a Global Computing Laboratory, reducing
the number of physical servers by about 8,000, or 55
percent, and eliminating more than 10,000 square feet of
data center floor space.
These servers were replaced with virtual machines hosted
on a much smaller physical footprint of new servers,
which resulted in significantly higher levels of utilization.
We also improved server to administrator support ratios,
resulting in additional cost savings.
From beginning to end, the evolution to a virtualized environment
was made possible by applying BMC IT management
solutions: our state-of-the-art service request
management system; automated build and recovery
procedures; comprehensive configuration management
database system (CMDB); and capacity monitoring tools.
We think the lessons weve learned along the way could
really benefit other organizations operating large scale
cloud environments to support global business operations.
How do you feel about BMCs momentum
going forward?
I have been with BMC for over 20 years and I cant recall
when we have had more opportunities than we do today.
We have done a good job aligning our strategy with the
needs of the marketplace. We have the clearest vision in
the industry of IT management and how it benefits customers.
We have the people, technology and resources to
execute that vision. We really like where we are, what we
see and how we are positioned.