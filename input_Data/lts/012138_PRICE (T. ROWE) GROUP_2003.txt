Dear Stockholder,
The financial markets and our company made significant strides in 2003 as geopolitical concerns early in the year gave
way to a resurgent economy over the final nine months. Stocks reversed their three-year slide and outperformed bonds,
which also posted gains, primarily because of a good first half.
The strengthening financial market allowed us to improve on our previous years results, with income and earnings registering
solid year-over-year gains. In December, the Board of Directors increased the quarterly dividend on our common
stock, maintaining a record of consecutive annual increases since the company became publicly owned in 1986. We are
pleased to report that the companys overall competitive position remains very strong. Especially gratifying was the performance
of the mutual funds and portfolios that we manage, which continued to perform very well relative to our peers.
More than 65% of all our funds surpassed their Lipper averages for the one-year period ended December 31, 2003, and
75% or more beat their peers for the 3-, 5-, and 10-year periods through the end of the year.
Financial Highlights
The markets robust performance combined with strong inflows to expand the value of the investment assets we manage
for individuals and institutions. At year-end 2003, assets under management totaled $190 billion compared with $141
billion a year earlier. The management fees that we charge on those assets, which provide over three-quarters of our
operating revenues, improved commensurately. Administrative services revenue also rose in line with similar increases
in our costs of providing these services.
Net revenues for 2003 rose to nearly $996 million from $924 million in 2002. Net income of $227.5 million improved
from the previous year, and diluted earnings per share increased 16% to $1.77, up from 2002s $1.52.
Operating expenses increased by approximately 5% in 2003. Compensation and related expenses increased due to rising
salaries, bonuses, and outside labor costs. Advertising and promotional spending was flat compared to 2002, and depreciation
and amortization of property and equipment declined. Other operating expenses increased.
In the last several years, due to major changes in accounting principles and regulations, investors have increased their focus
on a firms ability to generate positive cash flows. As shown in our financial statements, the firm generated strong cash
flows from its operating activities. These cash flows increased from $269 million in 2002 to $297 million in 2003. This
placed the firm in a very strong position to invest in our business, pay off debt, increase our dividend, and repurchase stock.
We paid off the remaining $56 million of debt that was outstanding at the beginning of 2003 and have no short- or
long-term debt outstanding at year-end 2003. We also repurchased 787,000 of our common shares at a cost of $20
million during the first quarter of the year.
For further financial details, please consult Managements Discussion and Analysis starting on page 26 of this report.

Market Environment and Asset Flows
U.S. stocks recorded gains for the first time since 1999, with the Standard & Poors 500 Stock Index rising a powerful 29%.
The technology-heavy Nasdaq Composite Index surged 50% as investors appeared to regain some of the confidence lost
in that sector. In retrospect, the markets bottomed in October 2002 before finishing that year strongly, but concerns about
the looming war in Iraq held the markets in check in 2003s first quarter. A relatively quick resolution to the initial combat
phase of that war, low interest rates, improving corporate earnings, and signs that the economy was at last gathering
momentum helped stocks advance. Although global economic recovery was more subdued, the improving U.S. outlook
boosted non-U.S. equities as well. Domestic bonds also posted modest gains, primarily due to an improving outlook
and lower risk premiums on corporate and other less interest rate-sensitive fixed-income securities. As the year progressed
and the economic news brightened, intermediate and long-term yields rose, hurting U.S. Treasuries. At the same time,
high-yield bonds soared as corporate credit profiles improved and market confidence grew. Foreign bonds were also very
strong as investors became increasingly alarmed at the growing U.S. trade and budget deficits, causing the dollar to fall
against foreign currencies.
The strong market environment and solid relative performance of our funds and portfolios led to positive net cash
inflows of $13.3 billion for the year. Third-party distribution (where we manage assets gathered through intermediaries
such as insurance companies and brokers) was responsible for a significant portion of the net inflows but institutional
investors both within and outside the U.S. and domestic retail investors also contributed positively. For the year 2003,
investors added nearly $7.5 billion to the T. Rowe Price mutual funds offered in the U.S. and $5.8 billion to the other
managed portfolios. About two-thirds of the net mutual fund inflows for the year were spread among four fundsthe
Mid-Cap Growth, Equity Income, High Yield, and Growth Stock Funds. The percent of total assets under management
comprising equities moved higher, from 66% in 2002 to 71%, reflecting the stock markets rally.
Troubling Developments in the Fund Industry
This positive economic and market backdrop was tarnished by some shocking news concerning trading abuses by several
firms in the investment management industry, including allegations that executives at certain fund companies permitted
excessive trading or illegal after-hours trading. In addition, certain intermediaries that process fund transactions allegedly
assisted some investors in executing improper mutual fund trades. The investigations have led to charges of improper
mutual fund trading by some fund company executives and portfolio managers.
I want T. Rowe Price stockholders to know that we emphatically condemn the abuses that have been revealed or alleged
against other firms in the industry. We unequivocally oppose inappropriate trading. We have conducted a thorough review
of our policies, procedures, and practices with respect to late trading and market timing, and we found that they are sound.
T. Rowe Price has not entered into any agreements that authorize market timing by any party. We will use the current
situation as an opportunity to make improvements where possible. We are also actively participating in industry-wide
discussions with the intent of making possible suggestions to the SEC and NASD for more effective regulation of
hedge funds and third-party intermediaries.

Thomas Rowe Price, Jr., who founded our firm in 1937, was known for saying: If you do right by your clients, your
clients will do right by you. We believe that philosophy is engrained in the firms culture. No system is foolproof, but
we have made a serious and sustained effort to live up to this principle.
Building for Growth
Over the past few years, we have pursued a number of initiatives to strengthen our competitive position and prepare for
the changing market and investment environment. Among the major strides in 2003 was the implementation of our expanded
primary provider initiative, which is a comprehensive effort designed to respond to the increasing need among our fund
shareholders and investment clients for guidance and service through the creation of new tools, practices, and processes.
We also focused on our initiative to identify investment-only opportunities in the defined contribution retirement plan market.
These initiatives are only a part of our overall strategy for success, which builds on our principled heritage, disciplined
investment process, diversified business model, world-class service, and long-term orientation. For a more comprehensive
look at our strategy, please see the story behind the elements of our success beginning on page 6.
Management and Board Changes
Before closing, I would like to thank several valued associates who will be leaving the company shortly. First, as part of
a planned succession, Vice Chairman and Chief Investment Officer M. David Testa turned over his responsibilities as
chief investment officer to Brian C. Rogers at the end of 2003 and will retire from the firm and the T. Rowe Price
Group Board of Directors in 2004. Upon his retirement, Davids management responsibility for the firms investment
divisions will transition to me. During his tenure, David was an innovator in the development of international investing
for U.S.-based investors and in 1979 was instrumental in establishing the joint venture that became T. Rowe Price
International. He served for a decade as portfolio manager of the T. Rowe Price Growth Stock Fund and also served as
director of the firms Equity Division. While Davids leadership will be missed, Brians extensive investment experience
managing the Equity Income Fund and institutional portfolios, his strategic insight, and the high regard in which he is held
by portfolio managers, research analysts, and clients make him uniquely qualified to assume the CIO role for the firm.
In addition, William T. Reynolds will turn over responsibilities as director of the Fixed Income Division in the first quarter
of 2004 and retire from the firm and the firms Board in 2004. Bill has enjoyed a remarkably successful 22-year career
at T. Rowe Price, playing a critical role in the development of the firms fixed-income investment capabilities. Mary Miller
will take over the reins of the Fixed Income Division. She has been at the firm for 20 years, working with Bill the entire
time. She became head of the municipal bond department in 1996 and assistant director of Fixed Income in 2001.

There are other changes to the makeup of the Board of Directors. In August, James H. Gilliam, Jr., passed away unexpectedly.
Mr. Gilliam was an attorney, consultant, and philanthropist who had been a member of the Board since 2001. We miss him,
and we express our condolences to his family. Richard L. Menschel, who has been an independent director of the Price Group
since 1995 and chairman of the Executive Compensation Committee, has reached the mandatory retirement age and will
not stand for reelection to the Board in April 2004. He has been a strong contributor to our Board, and we wish him well.
Additionally, Henry H. Hopkins and John H. Laporte will step down from the Board this year as we move to implement
the policy of having a majority of independent directors as mandated by the new Nasdaq listing requirements. They will
continue to function in their current positions at the firm, Henry as director of the Legal Division and chief legal officer, and
Jack as one of our most senior portfolio managers. We thank them for their service to the firm as directors. We welcomed
two new independent directors to the Board in 2003. James T. Brady is a managing director for Ballantrae International
Ltd., a management consulting firm, and Dr. Alfred Sommer serves as dean of the Johns Hopkins Bloomberg School of
Public Health.
Looking Forward
The economy appears to have turned the corner in the second half of 2003, and we are optimistic that the markets will continue
to provide investors reasonable returns, although neither the economy nor stocks are likely to grow at the very strong levels
reached in 2003. The nations rising budget and trade deficits are a concern, as are higher commodity prices. At some point,
interest rates will begin to rise, limiting bond returns. Whatever the economy and markets present us with this year, our
risk-aware investment approach, experience in the markets, diversified range of investment options, world-class service,
sound financial position, and talented and dedicated associates provide a solid foundation for continued growth.
Sincerely,
George A. Roche
Chairman and President
February 6, 2004