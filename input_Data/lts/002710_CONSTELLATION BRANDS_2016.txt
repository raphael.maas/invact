SEIZING GROWTH AT EVERY TURN...
I am extremely pleased to share that fiscal 2016 was yet another incredibly dynamic and
fulfilling year for Constellation Brands  marked by strong financial performance, notable
brand and business milestones, industry accolades and strategic acquisitions.

We accelerated our growth
across beer, wine and spirits
by strictly adhering to our
strategic imperatives to:
build brands, apply rigorous
financial discipline and build
the best organization.
As a result, we made
remarkable progress and
achieved record-breaking
outcomes that confirm
we have the leadership
strength, the right portfolio
and the organizational
discipline to be a dominant
leader in the Total Alcohol
Beverage (TAB) category.
Why the emphasis on
growing our TAB
leadership? Simply put,
it provides the greatest
opportunity to create
long-term shareholder
value. Within Consumer
Packaged Goods (CPG)
retail, TAB was the second
largest dollar sales
category and the second
largest provider of dollar
growth.1 Thus, it is
a highly important
category in which to
win with consumers 
and winning we are!
Today, Constellation is the
largest multi-category
supplier and the third
largest TAB company in
the U.S.1 We achieved this
strong position through
our long-term efforts to
premiumize our overall
portfolio. Approximately
80% of TAB category growth
in IRI channels is driven by
premium products, which
is why our fiscal 2016
strategic acquisitions of
Meiomi Wines and Ballast
Point, two higher-end
brands, were so important.

TAKING OUR INNOVATION CUES
FROM CONSUMERS
To further accelerate our
TAB success, in fiscal 2016,
we solidified and expanded
our Growth organization
around a consumerfocused
framework of
Innovation, Insights and
Process. These efforts
are helping us more
precisely target our retailer
strategies with respect to
expanding TAB shelf space
and developing new ways
to engage consumers and
build loyalty. They are also
more effectively aligning
our brand investments with
proven consumer trends
and preferences.
In fiscal 2016, the Growth organization
added new team members and
made significant advances to enable
Constellation to capitalize on the
distinct premium TAB advantages we
have in the marketplace. Namely, we
are exceptionally well positioned to
maximize the success of holiday crosspromotions,
as consumers drink across
categories and we offer the right mix
of products within our own portfolio
for every occasion. We can also create
powerful collaborations with national
retail accounts to develop strategies
needed to win with consumers. Finally,
we have the tools and insights to look
beyond beer, wine or spirits specific data
in order to fully understand and respond
to consumers total lifestyle and TAB
drinking experiences and preferences.
CULTIVATING LEADERSHIP
EXCELLENCE
At Constellation, our people make us
great, so its important to continually
evolve our leaders to develop the skills
and talents required to maintain our
growth leadership over time.
In fiscal 2016, we made strategic
leadership changes across Constellation
to further bolster performance and quality.
By investing in the next generation of
senior business leaders, we are securing
a strong pipeline of leadership excellence
for years to come.
The following pages provide a closer
look at our fiscal 2016 highlights across
our beer, wine and spirits businesses.
I want to personally thank you for your
support of our company, our leadership,
our employees and our brands. Were
grateful youve chosen to be an invested
partner with us as we fulfill our mission to
build brands that people love.
Rob Sands
President & CEO