Letter to Shareholders
                                                                         I Associate Development: -- Systematically    identify, hire, train, and
WHERE WE ARE
                                                                            continuously develop a broadly diverse group of talented associates
Sales and Earnings. In fiscal 2006 we experienced many of the
                                                                            to support both our new store growth and continued operational
market challenges we had seen in fiscal 2005. Unexpected spikes
                                                                            improvement. During the year, we successfully developed more
in gas prices, rising wholesale vehicle prices, rising interest rates,
                                                                            than 200 new managers to support growth, and we opened
Iraq war anxiety -- all were key factors we noted last year that we
                                                                            nine superstores, which employ more than 800 associates. We
thought might come into play this year, and indeed they did.
                                                                            also enhanced our continuing training and development pro-
Nonetheless, our sales and earnings growth was much stronger                grams for existing managers. Finally, we launched an on-line
this year than last. Net earnings increased 31% on a sales increase         job application and screening system that significantly improved
of 19%, including 4% growth in comparable store used units. Key             the quantity and quality of our applicants.
drivers of our performance included:
                                                                         I Company Culture: -- Maintain         an enthusiastic, down-to-earth,
I We learned how to optimize our appraisal approach in the                  non-hierarchical business culture that treats every associate and
   rising wholesale price environment, offsetting margin pressure           every customer with the respect and personal attention they deserve,
   on our retail business.                                                  and lets us all have fun doing it. CarMax was named to the
                                                                            Fortune "100 Best Companies to Work For" list for the second
I We benefited this year from the summer incentive environment.
                                                                            consecutive year and we were named the nation's "Most
   New car manufacturer employee pricing programs drove traffic
                                                                            Admired" auto retail/service company in a Fortune industry
   into the marketplace, providing the consumer greater pricing
                                                                            survey. We're proud of this recognition of what we've achieved
   clarity -- an environment where the transparent CarMax offer
                                                                            with our culture; we're also aware that there are many opportu-
   produced strong results.
                                                                            nities for further improvement.
I CarMax Auto Finance continued to provide a strong
                                                                            We surveyed all our associates in fiscal 2006 to define the core
   earnings contribution.
                                                                            elements of our culture. We have also begun an annual associate
Additional Accomplishments. Our store growth stayed on target.
                                                                            engagement benchmarking process with the Gallup organization
We added nine stores, increasing our superstores 16%, in line with
                                                                            that helps us understand where we're good and where we need
our plan to grow our store base 15% to 20% per year. We also con-
                                                                            to be better. The process allows us to focus on tangible incre-
tinued to balance our store growth by market size and type of store,        mental improvements in our daily working relationships at the
adding five standard superstores and four satellite superstores.            individual work team level. It also allows us to benchmark our-
                                                                            selves against other world-class organizations to keep our sights
                                            on an ever higher goal.
We continue to focus on the three broad operational goals that           I carmax.com: -- Assure      that carmax.com is both the single best
we've discussed in our last two annual reports, and we've added             place on the internet to shop for a used car, and is a comfortable,
carmax.com to this list because of its increasingly central role in         user-friendly home where the automotive shopper can find all the
our marketing program:                                                      tools and information needed. Over the last five years, carmax.com
                                                                            has become a central feature of our sales and marketing program.
I Quality/Waste Elimination: -- Further     systematize our efforts at
                                                                            There were more than 40 million visits to carmax.com this
   continuous quality improvement, combined with a relentless focus on
                                                                            year, and approximately 70% of in-store customers indicate they
   waste elimination. During fiscal 2005, we launched a systematic
                                                                            visited our site before coming to the store. During fiscal 2006,
   framework for achieving process improvement in our recondi-
                                                                            we undertook extensive consumer research to identify those
   tioning and service operations. During fiscal 2006, we extended
                                                                            improvements that were most important to shoppers and cus-
   it to our cosmetic process. We also launched a ground-zero
                                                                            tomers. We launched a significant rewrite of the site providing
   redesign of our reconditioning process at our newly opened
                                                                            improved "power search" functionality, improved quantity and
   Virginia Beach superstore based on a single-piece flow system
                                                                            quality of vehicle photos, and improved overall graphic quality.
   that eliminates waste, insures quality, and provides superior
                                                                            We also significantly expanded our use of key-word search
   diagnostics at each step of the process. We will continue to
                                                                            advertising. During the coming year, we will continue to shift
   develop and refine this process over the coming year.
                                                                            advertising dollars from newspaper to online search, online
   Waste elimination is a key to the continuous improvement
                                                                            classified advertising, and other innovative ways of using the
   process. By identifying and eliminating wasted motion, materials,
                                                                            web to drive customers to carmax.com. We will also undertake
   and labor we can improve efficiency and quality while reducing
                                                                            additional significant investment in carmax.com usability,
   cost. We plan to extend this waste elimination focus to key
                                                                            functionality, and information resource connections.
   opportunity areas of our store operations over the coming year.



                                                                           market/small trade area store. Charlottesville is a separate televi-
community involvement
                                                                           sion market with approximately 185,000 people, less than half the
We continue to believe it's important for CarMax to contribute to
                                                                           size of our next smallest trade area. We will be opening the market
the communities where we live and work. During fiscal 2006, the
                                                                           with a 14,000 ft.2 satellite that will operate with a different manage-
CarMax Foundation made new grants totaling $1.2 million focusing
                                                                           ment structure designed for efficiency at volumes of 100 to 175 cars
on these primary areas:
                                                                           per month, which, while lower than our norm, still are 2 to 4 times
I At the national level, family automotive safety: We continued
                                                                           that of a typical new car dealer's used operation. This store will
    a focused strategy of investing to promote driver and passen-
                                                                           allow us to understand and refine the operating characteristics
    ger auto safety, providing grants to: Driver's Edge, providing
                                                                           both for smaller markets and smaller fill-in trade areas and will
    safety and collision-avoidance education to young drivers;
                                                                           contribute to our efforts to ultimately increase market share and
    Mother's Against Drunk Driving, funding nine new chapters
                                                                           overall growth potential through denser storing patterns.
    at universities; and The Healthy Mothers, Healthy Babies
    Coalition, for further development of booster seat safety.             We also are opening the CarMax Car Buying Center, our first
                                                                           appraisal-only location. This center is on a one-half acre site where
I In Richmond, education and youth development: Our home
                                                                           we will conduct appraisals and purchase vehicles using the same
    town efforts continue to focus on education and youth develop-
    ment, particularly among economically disadvantaged inner city         processes and systems utilized in our used car superstores. This
    youth. We provided grants to 31 organizations including a seed-        test is part of our long-term program to increase both appraisal
    grant to the National Center for Neighborhood Enterprise to            traffic and our retail vehicle sourcing self sufficiency.
    bring its successful "Violence-Free Zone" initiative to Richmond.
                                                                           Going forward, our growth program will continue to include a mix
I Local, matching gifts, and volunteer grants: Approximately               of stores each year designed to balance the opening load across our
    15% of our funds are reserved to support matching gifts by our         regions and the risks and rewards of new versus established, and
    associates to charitable organizations. We also provide matching       large versus small, markets.
    grants of $10 per hour of volunteer time our associates provide
    to qualified organizations. The enormous tragedy created by
                                                                           THANKS
    Hurricane Katrina brought out the natural generosity of our
                                                                           I want to again thank all 12,000 CarMax associates for the terrific
    associates. Despite the fact we currently have no operations in
                                                                           job they did helping us successfully navigate a very volatile auto-
    Louisiana or Mississippi, our associates provided donations for
                                                                           motive market and still turn in an outstanding performance for
    Katrina relief which, with foundation and corporate matching,
                                                                           both our customers and our shareholders. As I will be retiring this
    totaled nearly a half million dollars. We also make grants to
                                                                           year after 15 years of work on CarMax, I also want to particularly
    local charities as part of each CarMax grand opening.
                                                                           thank the long-serving CarMax associates, the Pam Hill's and
                                                                           Kenny West's, Joe Palermo's and Ruby McNeil's, Shawn Vu's
WHERE WE'RE GOING                                                          and Esther Tedesco's. I continuously receive the credit for what
Growth Program. Our focus since resuming growth in late fiscal             is really the work that they and their counterparts have done to
2002 has been to add fill-in stores in established markets and stan-       create and build CarMax into the wonderful company it is today.
dard stores in new, mid-sized markets. These represent the lowest          My sincerest thanks to each of you.
risk, highest early return opportunities, helping to offset the
expense penalties of our growth program buildup. In fiscal 2006,
we also added two new stores and launched a full advertising
program in Los Angeles. We're quite pleased with the progress of
this market, and will continue to add stores to L.A., with a goal
of moving to 10 or more stores over the next several years.
                                                                              Austin Ligon
In fiscal 2007, we plan to add approximately eleven stores, includ-           President and Chief Executive Officer
                                                                              March 30, 2006
ing a superstore in Charlottesville, Virginia, our first test of a small


                                                                                                                     