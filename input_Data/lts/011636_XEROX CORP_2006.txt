Dear Fellow Shareholders,

I am pleased to report that 2006 was another
year of good progress for Xerox. The strategic
investments we made several years ago
are paying off and we are confident in our
continued ability to give you good returns on
the trust you have placed in us.

Our progress in 2006 provided more
evidence that we are on track and that
we are building momentum.
Our results
In 2006, our financial results included:
 Net income of $1.2 billion or earnings
per share of $1.22, compared to EPS
of 94 cents for full-year 2005.
 Adjusted EPS of $1.05, an increase of
17 percent from full-year 2005 adjusted
EPS of 90 cents.*
 Total revenue of $15.9 billion, an
increase of 1 percent or $194 million
from full-year 2005.
 Operating cash flow of $1.6 billion.
 Year-end cash and short-term investments
balance of $1.5 billion.

Our second strategic platform for growth is what we call the New Business of Printing.
Its a market where Xerox is competitively advantaged. Our customers in this space are
typically commercial printers, marketing and graphic arts companies and large enterprises.
They depend on digital printing to print on demand, produce short runs of books, personalize
documents as they come off the press and leverage the power of our digital technology in a
myriad of other ways. Our digital presses, sophisticated workflow capabilities and expanding
set of solutions and applications help our customers grow their businesses.
New products helped drive a 74 percent increase in installs of Xeroxs production color
publishing systems and printers  thats actual placements in a customers workplace.
We closely track install data as an indicator of whats to come in post-sale revenue. The
more color products we sell, the more pages printed on Xerox systems. More pages
mean more profitable annuity, all adding up to sustainable growth.
One especially satisfying trend: the acceptance of the Xerox iGen3 Digital Production Press,
our flagship and unparalleled product that creates up to 110 full-color impressions per
minute with the ability to personalize each and every one. The iGen3 is a winner with our
customers. In fact, some 180 customers now own more than one. And, about 70 of our
customers iGens produce more than 1 million images per month.
Last year we acquired XMPie for $54 million. XMPie provides software that enables
multimedia campaigns with personalized e-mail, Web sites, catalogs, brochures and other
documents. When applied to direct marketing campaigns, these capabilities significantly
increase standard response rates and open up new ways for marketing professionals to
communicate with their customers.
Youve probably received marketing materials in the mail that are targeted to your personal
buying needs  like postcards from car dealers with an image of the new version of your
car including a license plate with your initials or a travel itinerary from a cruise line featuring
activities that appeal to your individual interests.
These marketing campaigns cut through the clutter. Theyre personalized, colorful and
most likely printed on Xerox color systems using XMPie software. The market for variable
data jobs that capture individual information such as name, address, account information
and photographs is projected to grow from 49 billion pages in 2004 to 138 billion pages
in 2009. Thats an annual compounded growth rate of 23 percent. Xerox is in the forefront
of driving that market opportunity.
Our third strategy for growth is to lead in services in our major accounts  and here,
too, were making good progress. Our services signings last year were up over 15
percent from the previous year. Customers who are entrusting us to manage large
enterprise systems include the likes of Medco and Honeywell, the University of Calgary
and Microsoft, OfficeMax and United Technologies and more.
And we believe its the tip of the iceberg. More and more large customers are turning to us
for help in simplifying their document-intensive work processes; bridging the divide between
paper and digital documents; managing their infrastructure; and creating digital systems
that allow them to easily store, search and retrieve their digital files.
We call this Smarter Document Management. Its a market where Xerox is uniquely
advantaged  drawing upon our heritage in understanding how people work with documents
and adding our rich portfolio of digital applications and technology.
During 2006, we acquired Amici LLC for $175 million. Amici is the market leader in
e-discovery technology, which involves the identification, filtering, production and storage
of relevant data from paper or electronic documents, such as e-mail, text files, memos,
databases, presentations and spreadsheets. Projected to grow at an annual compounded
rate of close to 40 percent, e-discovery services could be a nearly $2.5 billion market
in the United States alone by 2009. Now a part of Xerox Litigation Services, Amici helps
position us to go after this burgeoning market.
The fourth plank in our growth strategy is to aggressively attack the small and medium
business (SMB) market. Its a $44 billion opportunity where our presence has been modest.
Segments of the market, however, are growing  especially in developing countries, in color
and in multifunction devices.
Here, too, we are making excellent progress. Last year we introduced four products
targeted to this segment and earlier this year we launched seven more. At the same time,
we continue to expand our distribution channels. We recently announced our most significant
investment in resellers and independent
agents since launching
these sales channels several years
ago. That means there will be more
feet on the street selling Xerox
systems and services and greater
Xerox brand visibility in the SMB
market. Our strategy is straight-forward:
more technology plus more
distribution equals more growth.
Were attacking a $117 billion
market opportunity through our
focus on these growth strategies.
We can compete for every dollar
of it  and we are.

Our people
Our success is dependent on contributions from Xeroxs 54,000 worldwide employees who
impress me every day with their passion for creating great experiences for our customers.
Theyre led by a deeply committed executive team with a fierce will to win. We have a wealth
of talent across our enterprise and spend significant time developing this talent  spreading
that wealth throughout our operations and preparing our next generation of leadership.
Our recent appointment of Ursula Burns to president of Xerox and a member of the Board of
Directors reflects this commitment. Ursula joined Xerox in 1980 as an engineering intern and
earned her stripes through product development and business division management roles
to her most recent position as head of Business Group Operations with full responsibility for
Xeroxs research and product groups through to manufacturing and distribution. Shes been
instrumental in fine tuning Xeroxs business model so were more cost competitive, and
accelerating our time to market with the launch of more than 100 products in the last three
years. Ursula now brings her deep knowledge of our business to the president role, where
shell work closely with me and our leadership team to aggressively drive our well-defined
growth strategy. With a lot of heavy lifting behind us, our operations are in good shape.
Were now well positioned  with the best people and best products and services  to
build on our momentum in the marketplace both in the near term and for your long-term
investment in Xerox.
Our innovation
Including our partner Fuji Xerox, we invested a combined $1.4 billion last year in research
and development, with a similar amount planned for 2007. Our returns on that investment
are excellent. Last year we launched 14 products that together earned 208 industry
awards. We expect to more than double the number of product announcements this year.
We measure the success of our R&D investments through a metric called Time to Revenue
 how long it takes for new ideas to become new revenue. We believe our progress in this
area is among the best in the industry. About two-thirds of Xeroxs equipment sales come
from products launched in the past two years.
Thanks to our scientists and engineers, the future is bright as well. Last year, the Xerox
innovation community received nearly 560 U.S. utility patents. Thats up 24 percent over the
previous year. With our research partner, Fuji Xerox, the total was over 800. We hold more
than 8,000 active patents, many that are the foundation of breakthrough technology like:
 Self-erasable paper that uses a combination of light and ink to automatically erase marks
on a page about 16-24 hours after the page is printed.
 E-Agent, a special chemical ingredient that reduces the amount of energy needed to make
certain Xerox toners by up to 22 percent.
 Advancements in natural language search technology which uses familiar phrases and
sentences to simplify search results.
Our customers
One of the hallmarks of the new Xerox is deep relationships with our customers.
Increasingly, we are strategic partners. One of them recently referred to Xerox as
a combination painkiller and steroid. Weve been called lots of things, but never
that, so I asked him what he meant. His answer went something like this:
Before Xerox came along, document management was a huge headache. We had no idea
what we were spending. We had lots of vendors but no partners. We kept investing in the
promise that the next new thing would be the right thing. Xerox has made my life simpler.
I talk to one person who saves me money and boosts my productivity. Thats the painkiller.
The steroid part comes from leveraging documents to reach customers more effectively, to
make it easier for customers to do business with us and to grow our business. Thats the
steroid part. Xerox gives me both in ways no one else can.
Thats music to my ears. Its also a novel way to express our value proposition  painkiller
and steroid, streamlining document processes to reduce costs and leveraging documents
to grow revenue. No one does that better than Xerox  no one.
Our operations
We also have a business model that works  in part because its flexible. We react quickly
and decisively to marketplace challenges and opportunities. In todays world, every quarter
seems to present its own unique dynamics. Pricing pressures may put pressures on our
margins. Economic slowdowns may slow our revenue pipeline. Competitive threats may
cause us to bulk up our investments in marketing.
Whatever the circumstances, we adjust. We do it quickly. And we do it in a way that keeps
our business model aligned with our financial objectives. As a result, we are a company that
is known for consistency. We know thats important to you so its critical to us.
Our commitment to citizenship
So, too, is our continued focus on being a good corporate citizen. Its a cherished part
of our heritage and an essential part of our future. I urge you to read the snapshot
description of our activities in this report. Were making progress in reducing greenhouse
gas emissions. We supported more than 500 non-profit organizations; spent more than
$370 million with minority-, women- and veteran-owned businesses; strengthened our
efforts to protect our forests; sent thousands of volunteers into the communities in which
we work and live; and much more.
Last year, we published our first Report on Global Citizenship. It provides a comprehensive
look at how we run our company  from serving our customers and taking care of the
environment to conducting our business with integrity and giving back to our communities.
The report is available in its entirety at www.xerox.com/citizenship.
Our commitment to you
We believe our track record is good, our strategy is sound and our business model is
robust. Were participating in a $117 billion market and were positioned to compete
aggressively for every dollar of it. In 2007 youll see continued activity that strengthens
our leadership in color, accelerates the New Business of Printing, expands our offerings
in services, and broadens our offerings and distribution in the small and medium business
market. All this fuels our profitable annuity stream, which accounts for more than
70 percent of our revenue.
At the same time, you can expect us to remain diligent on reducing costs, generating cash
and prioritizing profitability and earnings growth. We know that you have invested in Xerox
because you have confidence in us. Its an awesome responsibility that we embrace and
take very seriously.
The thing I love the most about this company is the people. They have what I like to call
a steely optimism. They do whatever it takes to make Xerox successful, to help our
customers be successful and to give you a good return on your investment. Because of
them, I have every confidence that 2007 will be another good year for Xerox  and for you.

Anne M. Mulcahy
Chairman and Chief Executive Officer

