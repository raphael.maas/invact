John C. Lechleiter, Ph.D., Lilly's chairman, president, and chief executive officer, and Jan Lundberg, Ph.D., president of Lilly Research
Laboratories, join with employees of the Lilly Cambridge Innovation Center in Cambridge, Massachusetts, at the center's opening.




To Our Lilly Shareholders:
May 10, 2016, marks the 140th anniversary of the founding of
Eli Lilly and Company, a milestone that very few U.S. companies
our size have ever reached. We've done it by staying true to our
values--integrity, excellence, and respect for people--and to
our mission of discovering and developing new medicines that
make life better for people around the world.
In 2015, our commitment to innovation bore fruit in a                       At the same time, as a result of lower expenses and higher
truly extraordinary year for Lilly. Even as we turned                       other income, earnings per share increased 13 percent to
the corner in our business results and began to grow                        $3.43 on a non-GAAP basis, which excludes adjustments
again after a prolonged period of patent expirations, we                    totaling $1.17 per share. Reported earnings per share
achieved unprecedented progress across our research                         were $2.26. (For information on the items that were
and development efforts. Through it all, we honored our                     adjusted for purposes of non-GAAP financial measures,
commitments to those who have a stake in our business--                     please see the 2015 Financial Highlights on the inside
including patients, customers, physicians, the communities                  front cover of the accompanying Financial Report.)
where we operate, our shareholders, and our employees
                                                                            This progress occurred in the face of some serious challenges,
who make it all possible.
                                                                            including a still-sluggish global economy, a significant
2015 Business Results and Pipeline Progress                                 slowdown in China, and continued pricing pressures in the
In 2015, despite unprecedented and substantial currency                     United States and other established markets.
headwinds brought on by the strengthening U.S. dollar, we                   In 2015, Lilly achieved significant advances in our pipeline
returned to revenue growth, led by Cyramza and Trulicity                  of molecules in clinical development. Highlights include:
following their strong launches, with significant contributions             in diabetes, positive cardiovascular outcomes data for
from our enlarged Elanco animal health business. Revenue                    Jardiance; in immunology, four positive Phase III studies
increased 2 percent to $19.96 billion, as six of our products               on baricitinib and strong Phase III data on ixekizumab;
and Elanco exceeded $1 billion in annual sales.                             and in oncology, Breakthrough Therapy Designation for
olaratumab and abemaciclib, several important business         stubborn scourge of multidrug-resistant tuberculosis.
development deals in immuno-oncology, and the                  Elanco continued its important work to address the key
approval of PortrazzaTM for the treatment of metastatic        link between nutrition and health through its partnership
squamous non-small cell lung cancer late in the year.          with Heifer International and through HATCHTM for
                                                               Hunger, a community partnership to provide eggs to
Our strong pipeline portends a lot of good news for
                                                               undernourished people in the Midwest.
patients--the ultimate measure of our success. As of
early 2016, we had nine molecules in Phase III testing         Over the past year, Lilly employees have added to our
or regulatory review, including potential medicines that       strong track record of volunteerism to strengthen
hold the promise of significant advances in the treatment      communities. In the first five years of our Connecting
of immunological disorders, Alzheimer's disease, and           Hearts Abroad program, 1,000 Lilly employees have
various pain conditions.                                       worked a combined 64,000 hours during two-week
                                                               assignments in impoverished communities across Africa,
Investors have taken note of how we've performed
                                                               Asia, Eastern Europe, and Latin America. In addition, our
and how we've kept our promises despite the challenges.
                                                               employees worldwide have volunteered 825,000 hours
Our stock price was up 22 percent for the year, leading
                                                               since 2008 through our annual Global Day of Service. And
to a 25 percent total shareholder return--once again
                                                               in 2015, we built on our legacy of support for United Way
outperforming most of our peers.
                                                               by initiating a partnership approach that includes pairing
The bottom line is pretty simple. We have emerged from         Lilly teams with United Way agencies.
the so-called "YZ" years of patent expirations as a
                                                               Lastly, we continue to demonstrate a firm commitment
better, stronger company. And a very promising future
                                                               to operating responsibly in all areas of our business--
is unfolding by the day!
                                                               from being recognized year after year around the world
Looking Ahead to More Growth in 2016                           as a great place to work, to continually striving to reduce
I could not be more excited about what lies ahead in           our environmental footprint. This commitment extends
2016 as we look forward to additional launches and             to our support for the United Nations Global Compact
some important pipeline milestones.                            and its principles related to human rights, labor, the
                                                               environment, and anti-corruption.
While recognizing the challenging environment ahead of
us, we continue to believe that Lilly's growth opportunities   Faithful to Our Mission, Confident in Our Future
will depend largely on our own performance. This               Our company has been through some real challenges
includes realizing continued strong uptake of Cyramza,         these past few years. But we confronted them head
Trulicity, and Jardiance, and good launches of Portrazza       on, figured out a strategy to handle what we faced, and
and the other products, such as ixekizumab, that we            executed that strategy with grit and determination.
hope will emerge from our pipeline in the months ahead.        We never wavered. And in 2015, we got sure signs that
                                                               it's working.
I'm confident that we've put the necessary investments
behind these recent and upcoming launches. At the              As we continue to honor Colonel Lilly's instruction to
same time, we will continue to depend on strong sales of       his son, to "take what you find here and make it better
Alimta, Forteo, Cialis, and our insulins--despite the       and better," I believe uncertainty will once again give
necessary shift of some resources to the launch side.          way to confidence in what an enterprise such as ours--
                                                               dedicated for 140 years to making lives better for people
Our Ongoing Commitment to Corporate Responsibility
                                                               all over the world--is able to accomplish.
In 2015, we also demonstrated our dedication to corporate
responsibility--a legacy dating back to our founder,           I am honored to be a part of this work and grateful to
Colonel Eli Lilly.                                             you for your support.

Our greatest contribution to society will always be            For the Board of Directors,
making medicines that make life better.

Yet we firmly believe that we have a further role to
play by collaborating with select partners to address
serious health challenges and to enhance access to             John C. Lechleiter, Ph.D.
high-quality care for people around the world. In 2015,        Chairman, President, and Chief Executive Officer
we continued support of our two signature global health
programs--the Lilly NCD Partnership and the Lilly MDR-
TB Partnership--focused on the growing challenge of
non-communicable diseases, such as diabetes, and the
