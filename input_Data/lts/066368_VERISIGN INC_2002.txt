For many companies, 2002 will be remembered as one of the more difficult years the high-tech community has faced. For 
VeriSign, it will be remembered as the year the company's long-term strategy  to be the leading provider of critical infrastructure 
services for all networks  was solidified. With the integration of its telecommunication services group in 2002,VeriSign put in place 
the assets necessary to be the hub for communications and commerce on the Internet and telecommunications networks. 
VeriSign's guiding principles for 2002 were simple: Focus on what we could control, be the best in the world at everything we do, 
achieve world-class customer satisfaction and build a durable company that rewards its employees and stockholders over the long term. 
By making strides in these key areas, VeriSign ended the year in a strong position from both an operating and financial standpoint with 
our organization focused on the largest potential opportunities in security, telecommunications and directory services. 
For the year ended December 31, 2002,VeriSign reported revenues of $1.2 billion. The company ended 2002 with a strong balance 
sheet with more than $400 million in cash, cash equivalents and short-term investments. 
While 2002 was a challenging year by most standards, it also presented an opportunity to focus our efforts on our core businesses 
and reconsider the most effective means to communicate to our diverse customer base. At the end of 2002 we re-launched the 
Network Solutions brand as it has the highest brand name recognition among Internet users of any provider of domain name 
registration services. 
VeriSign's security and payments offerings also continued to be well received in the marketplace. Our alliance with IBM opened up 
a new channel for our managed services, and we ended the year protecting nearly 400,000 web sites with our SSL certificates. In 
addition, the number of active merchants using our payment gateway services climbed to over 83,000 while we helped process 
ecommerce transactions with an aggregate value of more than $14 billion. 
Our registry services business finished the year with more than 26 million names in the .com and .net database and successfully 
transitioned the .org registry to the not-for-profit Internet Society. VeriSign's telecommunication services group introduced 
several new offerings, including Net Discovery, a one-step, turnkey solution that helps telecommunications carriers comply 
the Communications Assistance for Law Enforcement Act (CALEA). 
VeriSign's service offerings continued to deliver an unmatched level of security and reliability to Internet and telecommunications 
customers around the world. In fact, nearly all of the Fortune 500 companies, numerous governmental bodies and other organizations, 
hundreds of thousands of small businesses and hundreds of service providers and telecommunications carriers rely on VeriSign to 
engage in digital commerce and communications. 
As we look at 2003 and beyond, we see an exciting opportunity and unique role for VeriSign in helping every customer communicate 
and transact safely and reliably. We believe that VeriSign has an unequaled set of assets in technology, infrastructure and dedicated 
employees. In 2003 we intend to set a new standard of operational excellence with the rollout of the ATLAS ^TM Platform. The 
ATLAS system  the Advanced Transaction Lookup and Signaling system  combines our trusted Internet and telephony network 
infrastructure to serve as a next generation platform for telecommunications and database lookup services. In many respects, the 
ATLAS technology symbolizes what VeriSign does best  scale technology to seamlessly handle billions of transactions every day 
for our customers. 
The VeriSign team remains passionately committed to scaling technology to new heights  and in doing so realizing our goal of 
creating a transformational company. On behalf of all of VeriSign's 3,200 employees, I want to thank our stockholders, customers 
and partners for the trust and faith they have in us, and we look forward to your continued support. 



