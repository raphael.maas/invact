To Our Shareholders,

Life and Science, the two themes that bookend this annual report, drive what we do at
Edwards Lifesciences. We are a company motivated by our desire to extend and
improve the lives of people suffering from the debilitating effects of advanced
cardiovascular disease, the worlds leading killer. Our vehicle for enhancing patients
lives is science  we work diligently to develop, test and launch innovative technologies
to fulfill unmet patient needs. As we pursue our scientific mission, we are rewarded by
the realization that we are making a positive difference in patients lives, and also with
financial success that results in greater value for our shareholders.
2002: Near-Term Success, Long-Term Investment
I am pleased to report that in our third year as an independent company, we made
significant progress in our continuing efforts to transform Edwards Lifesciences into a
more profitable, faster-growing entity. In 2002, we met our sales growth and bottom-line
goals, while dramatically increasing our investment in research and development
(R&D). Our 2002 sales growth rate increased for the third consecutive year, reaching
7.0 percent underlying growth, and moved us closer to our long-term aspiration of
annual double-digit gains. Net income was up 31.8 percent over 2001, excluding
non-recurring items, and Edwards generated earnings before interest, taxes, depreciation
and amortization (EBITDA) of $164 million. Importantly, while we delivered on our
near-term commitments, we were also aggressive in investing in the companys
long-term viability by advancing key growth initiatives through R&D spending (up
19 percent over 2001) and adding outstanding new talent to the Edwards organization.
The numbers tell only part of the story of our success. As the worldwide leader in
heart valve therapy, Edwards continues to drive the global market shift from mechanical
to tissue valves, and in 2002 we again contributed important innovations in tissue valve

replacement and repair. Our Cardiac Surgery sales returned to historic double-digit
growth levels in the second half of the year, and our strategy for further strengthening
our global franchises through significant R&D investment paid dividends with the
addition of several new state-of-the-art products to our portfolio. Most notably, our
PERIMOUNT Magna pericardial valve, which is designed for optimal hemodynamics,
was introduced in Europe and Canada, while our Carpentier-Edwards S.A.V. porcine
bioprosthesis  a leading porcine valve internationally  received FDA approval and was
introduced in the U.S., providing another option for clinicians seeking a porcine alternative
for their patients. We also launched our Edwards MC3 Tricuspid Annuloplasty
System, the first three-dimensional annuloplasty ring designed specifically for surgical
repair of the hearts tricuspid valve, garnering very favorable responses from the U.S.
and European clinician communities.We believe our robust and unmatched pipeline of
heart valve therapy products will sustain our growth and leadership for many years to
come. There remains ample growth opportunity for tissue valve replacement and repair
therapies as we work to further expand the heart valve market by developing less
invasive repair and replacement approaches for patients who are not currently
candidates for conventional valve procedures.
Other 2002 highlights include the favorable settlement of patent infringement
litigation that we initiated against a major competitor in 2000, resulting in a cash settlement
and ongoing protection of our valuable intellectual property.We also acquired the
Japan business that had previously operated as a joint venture with Baxter
International, further strengthening our position in one of our largest international
markets. This transaction will add approximately $100 million annually to reported sales,
but is not expected to impact the bottom line.
Not everything was ideally executed this year. We experienced more challenges
than anticipated with the European launch of our Lifepath AAA Endovascular Graft
System, a novel, less-invasive treatment for patients with potentially life-threatening
abdominal aortic aneurysms, where adoption has been slower than expected.
We remain optimistic that the adoption rates will increase as more long-term data
demonstrate Lifepath AAAs therapeutic value, and about the products potential in the
U.S., where we completed our Phase II clinical trials and remain on track for a late 2004
commercial launch.

Investing Wisely, Progressing Toward Growth-Oriented Goals
We are strongly committed to continuing Edwards growth through increased R&D
investments, the establishment of new technology partnerships, and by making selective
acquisitions. In 2002, we applied resources to accelerate our internal innovation
process, and used our strong cash flows primarily to reduce debt and repurchase shares.
As a result, Edwards is well positioned to move quickly when we see strategically sound
opportunities to grow.
As we enter 2003, we remain focused on the aspirations we set when we became
a publicly traded company  to leverage our strong base business into double-digit
sales growth. We intend to shape the future treatment of advanced cardiovascular
disease, and our R&D strategy involves exploring multiple treatment approaches. We
are aggressively pursuing near-term strategic opportunities, such as our initiatives in
stent technologies to treat peripheral vascular disease, and the application of
proprietary laser systems to surgically treat atrial fibrillation. We are also exploring
technologies with longer-term clinical potential, among them our recently announced
partnership with Cook Biotech to pursue the creation of tissue-engineered heart valves
as living replacement options for younger patients, and our angiogenesis development
program with Sangamo BioSciences, which aims to use a gene therapy approach to
trigger the growth of new blood vessels in diseased hearts.
While we continue to build our capacity to develop important new products to
ensure our long-term growth, we are also delivering on our near-term financial
objectives. As in previous years, we have set ambitious, growth-oriented financial goals
for 2003. Our goals include underlying sales growth of 7 to 9 percent, net income growth
of 14 to 16 percent, excluding the non-recurring items in 2002, free cash flow of $85 to
$90 million, and continued investment in R&D at or above our sales growth rate. We
also expect to generate more bottom-line growth from new products, among them, our
Magna pericardial heart valve, which is on track to receive U.S. FDA approval in the
second half of 2003. We also are on pace for the global launch of our LifeStent line of
balloon- and self-expanding stents this year, representing an exciting opportunity for
Edwards to gain a strong position in the large and rapidly growing peripheral stent
market. Additionally, we expect to launch a new heart valve repair product for treating
ischemic mitral disease.

A Promising Future
We have great optimism about Edwards future. As a company, we are uniquely
positioned, combining a 40-year track record of stability and leadership with the energy
and determination of a newcomer in the process of transforming into a growth leader.
We are large enough to fund our own growth and launch globally, yet small enough to
be focused and agile to quickly pursue new opportunities. Our global leadership in two
highly dependable core franchises - Cardiac Surgery and Critical Care - enables us to
deliver solid results while pursuing additional growth opportunities. Many of our brands
are recognized as best-in-class, attracting strong customer loyalty and sustaining
leading market share positions. In addition, our global infrastructure enables us to
maximize profitability and establish a presence in developing countries, where
populations are living longer and the focus is shifting toward treating chronic conditions,
including cardiovascular disease.
Above all, we are driven by the awareness that our work can make a difference in
patients lives. Cardiovascular disease is the worlds leading killer, a complex and
devastating condition that must be addressed by a host of players, and we are joined in
this fight with other innovators, clinicians, public health and government regulatory
agencies, medical institutions and patients. We are very proud of the contributions we
have made thus far, but we recognize much more is yet to be accomplished. As we look
to apply scientific breakthroughs to help improve the lives of people fighting cardiovascular
disease, we remain guided by our Credo, which states, Helping patients is our
lifes work, and life is now.We thank you for your support as we continue our journey in
search of improving both life and science.
Michael A. Mussallem
Chairman and Chief Executive Officer