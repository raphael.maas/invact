Over the past three years, DDR has followed a
disciplined strategy of reducing risk while enhancing
virtually every aspect of our business model and
corporate culture to ensure that we are prepared to
thrive in any economic cycle. This strategy has resulted in
consistent performance and focused execution regarding leasing
results, capital markets and financing activities, and portfolio
operations and management. Since 2009, we have spent a
considerable amount of time talking about the lessons learned from
the past, the strategic shift that would successfully carry us forward,
and our commitment to that strategy without compromise. I am
pleased to report that 2011 was another excellent year for DDR,
and I believe it is only the beginning of something special.
Reflecting on 2011
2011 was a volatile year in the financial markets as global
economic indicators repeatedly gave reason to doubt a sustainable
recovery, and many alarming headlines fostered further uncertainty.
As a result, the market showed signs of inconsistency and volatility;
but, to the contrary, DDRs performance was simply steady and
consistent. By year-end 2011 and since the announcement of our
strategic initiatives, we had achieved twelve consecutive quarters
of balance sheet improvement; ten consecutive quarters of leased
rate gains; seven consecutive quarters of same store net operating
income growth; seven consecutive quarters of positive rental
spreads; and, twenty consecutive quarters of primarily non-prime
asset sales, which now total over $2.6 billion since 2007.
We are now in the enviable position of having historically low debt
maturities and historically high liquidity with an extended and
balanced debt maturity profile. At the end of 2011, our weighted
average debt maturity was 4.3 years, a significant improvement
from 2.9 years at the end of 2009. Additionally, at the end of the
first quarter of 2012, we had only $223 million of unsecured debt
maturities over the next three years, a meaningful improvement
from $2.3 billion at the end of 2008, $1.9 billion at the end of
2009, and $600 million at the end of 2010.
From an operational perspective, we experienced historically high
leasing volume in 2011 with over 11.7 million square feet of leases
signed in an environment where supply and demand was shifting
to benefit landlords, as evidenced by a 3% increase in average
base rent per square foot across the portfolio. Our progress in
2011 was recognized by Moodys, Standard & Poors and Fitch
with continued outlook upgrades as we continued to pursue our
aspirational goal of consensus investment grade ratings. While
we are pleased with the continued progress made in 2011, we
acknowledge there is more work to be done and we are confident
in our abilities to deliver going forward and continue to embrace the
challenge with optimism, high morale and enthusiasm.
Think Retail. Create Value.
Owning and operating retail real estate presents its challenges
and opportunities given the ever-changing landscape of retail
concepts, the fact that retail is inextricably tied to the consumers
health and shopping behaviors, and the continuing evolution of
technology. Successful retailers reinvent themselves every season,
never rest on their laurels and are constantly striving to sell the
right merchandise at the right price at the right time. As an owner
of prime power centers populated by todays most successful
retailers, we are in constant communication with our retailers to
stay abreast of their evolving business models and ensure we are
meeting their needs. As retailers continue to utilize new distribution channels to
maximize market share and stay relevant to the consumer, it is
apparent that omnichannel retailing will be the future of the industry.
Brick-and-mortar retail remains the number one distribution channel
for retailers today, with nearly 95% of all retail sales taking place
in physical stores. As technology evolves and consumer behaviors
and preferences are influenced by their lifestyles, retailers will be
forced to meet the needs of the consumer while maximizing
margins through efficient, cost-effective and reliable product
distribution. In-store pick-up, a professional and trained sales
force, product selection and consumer fulfillment will remain
paramount, and we believe that the retailers who make those
investments will ultimately win by expanding market share and
profitability. As retailers refine their strategy and seek ways to
create dynamic shopping experiences in the right environment,
landlords must be willing and able to deliver. And, in a supply
constrained environment where retailers are looking to open new
stores and reconfigure existing stores, landlords who get it and
have the professional staff to execute, will maximize the value of
their real estate and solidify their relationships with the worlds
most successful retailers. At DDR, we embrace the addition of the
Internet as a distribution channel which we think will only
strengthen our most prosperous tenants. We continue to partner
with retailers to improve their presentation to consumers with
coordinated strategic adjustments to the in-store experience, and
strive to further assist the retailer to enhance distribution relevance
and efficiency through physical locations.
Outlook
In regard to our view of growth and financing our business going
forward, we believe a tremendous opportunity exists for those
who continue to enhance their portfolio quality and financial
standing. Quite simply, financial stability and a realistic view of
todays environment will lead to growth tomorrow. We believe
the capital markets will continue to present opportunities to
strategically accelerate re-financings that will enable us to take
advantage of attractive market pricing rather than allowing the
market to take advantage of us by deferring difficult decisions
and risking a negative market turn. We acknowledge the unique
capital opportunity that currently exists and we are preparing
ourselves for the future with balance, flexibility and capacity.
We will continue to pursue and evaluate future growth initiatives
on a risk-adjusted basis with the overall focus of further improving
the quality of our portfolio and enhancing net asset value. Organic
growth through tenant openings with high credit quality retailers;
an ambitious redevelopment platform designed to maximize the
value of what we already own; and, targeted acquisitions of prime
assets that can be enhanced by inclusion within our operating
platform, are all expected to contribute to consistent growth in
both earnings and net asset value. The execution of this strategy
allowed us to increase our dividend three times since the beginning
of 2011, representing a 200% increase, and reflects the strength
of our operating platform, the high credit quality of our cash flows,
our proven access to capital and minimal near-term debt maturities.
We are proud to be in a position to prudently return more cash
to our shareholders on a regular basis, and we look forward to
delivering additional growth going forward.
In Conclusion
Consistent execution of our business plan and the superior results
produced by our operating platform combined with strategic
balance sheet initiatives highlight a steadfast determination to
adhere to our aspirations with discipline and foresight. As I have
said in the past, the markets may be volatile, but DDR is not.
While it is easy to become distracted by many domestic challenges
and the instability of global markets, I assure you that our team of
real estate professionals will remain focused and committed to
achieving our stated goals of becoming a blue chip REIT while
creating exceptional value for you, our shareholders.
I would like to thank my co-workers at DDR who have tirelessly
worked with urgency, professionalism and camaraderie to execute
a strategy that has dramatically advanced our enterprise both
operationally and culturally. Our overall corporate progress
validates our obsession with attracting and retaining the finest
talent and supporting that strategy through empowerment and
career advancement. People can never be overlooked. At the end
of the day, most retail REITs will own quality assets in quality
markets. We firmly believe, however, that it is what you do with
those assets that matters most. Maximizing value at the asset
and corporate level depends on creative and highly-talented
people working together toward a common goal. We will never
forget that.
With sincere appreciation and on behalf of all of us at DDR,
I thank you for your support, investment and confidence in
our Company.
Respectfully yours,
Daniel B. Hurwitz
President & Chief Executive Officer