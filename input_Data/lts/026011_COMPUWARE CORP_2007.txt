Dear Fellow Shareholder,

In last year's Letter to Shareholders, I wrote that Compuware's management team understood the need to
deliver growth in both earnings and revenue in FY '07. With more than 20 percent year-over-year growth in
EPS and Compuware's first revenue growth in seven years, I'm pleased to report that Compuware achieved
those milestones this fiscal year.

With powerful performances by Compuware's key growth-drivers like Vantage, Changepoint and Covisint
leading to overall revenue growth and to Compuware's third consecutive year of increasing EPS, fiscal year
2007 represented an important inflection point for the company. Moving forward, the key focus for Compuware
will be harnessing the company's momentum to further strengthen results.

To ensure Compuware seizes this opportunity in fiscal year 2008, I will continue to lead the company in
implementing its comprehensive plans for driving strong operational results and for aligning costs with market
conditions. Through these efforts, I expect Compuware to again deliver growth in earnings and revenues in
the fiscal year ahead.

Specifically, Compuware will improve its operational results by continuing to market and invest in its
solutions that have the greatest potential for growth. Compuware has this year launched its Test Factory,
IT Portfolio Management, IT Service Management, Application Delivery Management and Enterprise
Legacy Modernization initiatives. I expect these initiatives to further strengthen the already robust
performance of Compuware solutions such as Changepoint, Vantage and the company's application
development offerings by delivering Compuware customers greater visibility into their IT operations and
more control over their IT resources.

Additionally, the company will work to maintain and even increase its extremely valuable mainframe customer
base in FY '08. The mainframe remains a critical technology for large business, and therefore a lucrative and
significant market for Compuware. Accordingly, Compuware will continue to ensure that its solutions deliver
tremendous value for the mainframe and that the company's customers clearly understand that value.

The coming year will also be an important one for Compuware's Professional Services organization. In fiscal
year 2008, I expect the company's plans for Professional Services to yield increases in utilization and billing
rates, making the division more profitable.

Finally, I expect Compuware Covisint to continue adding customers in three major lines of business in the
year ahead: automotive, healthcare and security. By expanding its already thriving automotive operation and
by investing in its growing healthcare and security markets, Covisint should continue its rapid growth in fiscal
year 2008.

Taken together, these sales drivers give me great optimism about the company's capacity to grow the
business. I expect Compuware's sales teams to leverage these drivers to win new customers and to expand
the company's relationships with existing customers.

As the operations team propels these efforts in fiscal year 2008, I will continue to lead the company's plan for
aligning its costs and investments with market conditions. To this end, I have established a team of Compuware
employees dedicated to working full-time on increasing the company's efficiency and on improving its business
processes. Reporting directly to me, this Business Transformation Team will expedite the transformation of
Compuware's business and ensure that the company, its employees and its investors prosper.
As an initial step in this effort, Compuware has carefully evaluated expenses across the organization. The
company has begun implementing a plan that will take a minimum of $50 million in operating expenses out
of the business by the end of the fiscal year. These reductions will be across the company and in the areas
that make the most sense for the business.

With cash and investments totaling nearly $440 million as of March 31, 2007, the company continues to
maintain a strong balance sheet. In FY '07 the company enhanced this balance sheet with an enviable
operating cash flow of more than $200 million. I expect a similar level of operating cash flow in the coming
fiscal year. Finally, Compuware will continue to buy back the company's stock in fiscal year 2008, making
each share you own proportionally more valuable.

I remain optimistic about Compuware's future, and I remain passionate about creating that bright future in
partnership with Compuware employees and customers around the world. By focusing its investments
where the return will be the greatest, Compuware will deliver enhanced operational results. By managing the
company's costs, Compuware will improve its margins. Together, these efforts will increase the value of
Compuware's stock for investors, strengthen the company for its employees and increase the value that
Compuware delivers to its customers.

Sincerely,




Peter Karmanos, Jr.
Chairman and CEO
