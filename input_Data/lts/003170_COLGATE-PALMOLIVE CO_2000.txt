Dear Colgate Shareholder...
A Year of Broad-Based Strong Growth,
Earnings Per Share Increase 16 Percent
                          Q. How has Colgate continued its strong volume growth and
                          profitability gains?
                          A. Reuben Mark: Colgate's strong growth continues. This is due to our focus
                          on market leadership in our global core categories. Unit volume rose 6 percent in
                          2000 and was consistently strong each quarter. Every Colgate division con-
                          tributed, led by North America and Asia/Africa, which grew 8 percent and 7 per-
                          cent, respectively. We are particularly proud of our earnings per share increase of
                          16 percent. Other new records achieved were our highest-ever gross profit mar-
             gin, earnings before interest and taxes, and return on capital.
                                We drive top-line growth by developing innovative new products, bringing
                          them quickly to global markets and supporting them effectively. And we constantly
                          identify and achieve new savings opportunities. Moreover, the Company's broad
                          geographic base -- our global brands are sold in over 200 countries and territories
                          -- gives us a sustainable competitive advantage.


                          Q. How are the Company's new products succeeding?
                          A. Bill Shanahan: Innovation is where Colgate truly shines. A record-setting
                          38 percent of our 2000 sales came from new products introduced just in the past
                          five years. And in the U.S., which is a major source for global new products, it was
           61 percent of sales. We have made our new product process faster and more prof-
                          itable, significantly reducing the time from idea to shelf. For Colgate Fresh Confi-
                                                                                                                   
                          dence toothpaste, this meant launching in 48 countries within 12 months.
                                                                                                               
                               Strong incremental sales from new products added to our leadership. For
                                                                                                                                   
                                                                                                                  
                          instance, our global toothpaste share set a new record for the sixth consecutive                    
                                                                                                                 
                          year, bolstered by strong gains in the U.S. and China. Notable too was our record
                          39.7 percent global share in dishwashing liquid, up almost two points from 1999.
                                                                                                                   
                          Q. Colgate has called its financial strategy both "simple" and                            
                                                                                                                      
                          "powerful." Please explain this strategy and how it contributes to                       
                          the Company's growth.                                                                    
                                                                                                                   
                                                   A. Reuben Mark: Our financial strategy is
                                                                                                                 
                                                                                                                   
                                                          designed to increase gross profit margin and
                                                              reduce overhead on a continuous basis. The sav-
                                                                                                                   
                                                               ings enable us to reinvest in growth-building
                                                                activities, including R&D and advertising, while
                                               also delivering incremental profitability. The strategy is simple
                          and very powerful when pursued relentlessly by 38,000 Colgate people around
     
                          the world. Little by little, it has enabled Colgate to increase our leadership posi-
       
          tions, deliver earnings growth and build shareholder value.


                          Q. Will Colgate's ad budget continue to grow? How is the Company
                          reaching consumers differently?

2
                                           A. Bill Shanahan: Because advertising drives growth, we increased our
                                           investment in advertising in 2000. And we are budgeting an increase in
                                           2001. In addition to traditional TV and print advertising, we are utilizing a
                                           variety of innovative programs to reach consumers in all aspects of their
                               daily lives.

                                           Q. How will Colgate continue the trend of improving its



                                           gross profit margin, from 39.2 percent in 1984 to 54.4


                                           percent today?
                   


                                           A. Reuben Mark: We see considerable opportunity ahead. We
         

                                           have consistently met or exceeded our target to increase gross mar-
  
                                           gin by 50 to 100 basis points every year. We are now ahead of plan

                                           to reach our near-term goal of 55 percent by 2002. Further out,

                                           we are targeting a 60 percent margin by 2008.


                                                Gains are the result of new products that bring greater ben-
                                           efit to consumers and a myriad of cost-reduction programs.
                                           Savings are facilitated by the powerful enterprise-wide SAP
                                           software. SAP now supports 80 percent of Colgate operations
                                                                                                                                      
                                           worldwide, and the benefits accruing from our newly installed
                                                                                                                                    
                                           SAP applications are highly encouraging.


                                           Q. How is Colgate investing its growing cash flow?
                                           A. Reuben Mark: Colgate generated record cash flow of $1.5 billion in 2000,
                                           up 19 percent from last year. It was our fifth consecutive year of growth in cash
                                           generation. Driving these increases are Colgate's increasingly profitable operations
                                           and tight control of working capital. We utilize our strong cash flow to fund capi-
                                           tal expenditures that will drive future growth and cost savings, pay dividends and
                                           repurchase Colgate shares, and maintain our balance sheet ratios in the optimum
                                           range. During 2000, we repurchased 19.1 million common shares at a cost of
                                           $1,040.6 million.


                                           Q. What impact are external factors, such as rising oil prices and
                                           the weak euro, having on Colgate's profitability?
                                           A. Reuben Mark: Many companies, including Colgate, experienced cost
                                           increases in fuel and raw and packaging materials. The euro also had an impact,
                                           although our European division performed well in 2000 with unit volume grow-
                                           ing 4 percent. These external negatives were offset by our strong volume growth
                                           and cost savings.


                                           Q. What are Colgate's prospects for achieving growth in develop-
                                           ing regions?
                                            A. Bill Shanahan: Colgate's strong presence in the developing world, created
                                            over the last 80 years, continues to serve us well. Providing a healthy balance, approx-
                                            imately 45 percent of our sales comes from developing markets.
                                                    Our brands are leaders throughout Latin America and much of Asia, Africa
                                              and Central Europe -- regions with the majority of the world 's population. We
                                               foresee stronger growth trends in these markets as low per capita consump-
                                                tion for core consumer products continues to build. We have positioned
                                                 Colgate to fully participate in this growth by offering consumers a range of
                                                  affordable products.
    
                                                  Q. How does Colgate attract and retain the best people?
       
                                                  A. Reuben Mark: No mission is more important to Colgate's future.
                                                                                              
     



                            That is why "Making Colgate the Best Place to Work" is one of three strategic pri-
                            orities. We have outstanding people. We provide terrific opportunities for global
                            careers, a motivating environment based on our values of managing with respect,
                            a chance to share in our financial success through pay for performance and con-
                            sistent support for personal growth through global training. We believe these key
                            elements will help us continue to grow outstanding leaders -- so critical to our busi-
                            ness success. For more on this vital subject, please see pages 14-17.


                                 Q.   How     is   Colgate    taking    advantage       of   technology      and

                                  e-business opportunities?

                                   A. Bill Shanahan: We have only scratched the surface in achieving
                                   benefits from new technologies. This process began in 1995 when we first
                                   installed SAP real-time enterprise software in North America and reengi-
                                    neered the supply chain. The benefits to both efficiency and customer ser-
                                     vice have been substantial. We now have extended SAP to most parts of
                                      the world. A second generation of SAP applications is enabling Colgate's
                                       suppliers to automatically replenish supplies to our factories as needed,
                                       further reducing working capital devoted to inventory.
                                            We also are moving quickly on internet opportunities -- from direct
                                        receipt of orders on-line by veterinarians and dentists, to purchasing
                                         raw materials in internet auctions. Our first tests of on-line purchasing
                                          achieved significant savings.


                            Q. What is the outlook for Hill's?
        
           A. Bill Shanahan: Hill's Pet Nutrition had a good year. Unit volume increased 5          
        
                                                                                                                     
        percent, with especially strong growth in Europe and Asia. Superior new products
                                                                                                                    
                            continue to build this business. Most recently, Hill's relaunched the entire Science
                                                                                                                     
                            Diet line with a superior antioxidant formula to help protect the immune systems      
                                                                                                                                             
                            of cats and dogs. New innovative products are benefiting both the Science Diet                             
                            and Prescription Diet brands. As the world leader in specialty pet food, Hill's has an
                                                                                                                    
                            enviable franchise and is committed to continued growth, serving veterinarians,
                                                                                                                    
                            pet shops and breeders, the channels where it is a strong Number One.
                                                                                                                    
                                                                                                                     
                            Q. Please comment on the Company's outlook for 2001.                                         
                            A. Reuben Mark: We set many new records this past year and are confident                 
                            that Colgate's strong growth will continue. Our global market shares are strong
                                                                                                                     
                            and growing. Our new product pipeline is full. We are still at the early stages of
                            profiting from all aspects of technology. Collectively we are becoming better and
                            better at what we do, gaining strength from the commitment of 38,000 talented
                            Colgate people. All of this gives us much confidence about our ability to continue
                            delivering strong shareholder value.


                            Thank you.




                            Reuben Mark                                    William S. Shanahan
                            Chairman and Chief Executive Officer           President

