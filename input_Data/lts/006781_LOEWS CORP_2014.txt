To Our Shareholders:

At Loews, our view is long; we are students of history,
of market cycles and volatility. Our approach to value
creation and capital allocation is driven by this
perspective, resulting in a portfolio of diverse businesses
that we believe is well positioned for the long term.




                    Jonathan M. Tisch           James S. Tisch                  Andrew H. Tisch
                    Co-Chairman of the Board    President and Chief Executive   Co-Chairman of the Board of
                    of Loews, and Chairman of   Officer of Loews                Loews, and Chairman of the
                    Loews Hotels                                                Executive Committee of Loews

                                                                                                           -
                                                                                                         001
We rarely measure the signi cance of an event -- or
the return on an investment -- over the short term.
We do, however, take notice of near-term developments,
and we certainly noticed the drop in our share price
in 2014. Loews shares closed at $42.02 on December 31,
2014, a decrease of 13% from year-end 2013, compared
to a gain of about 11% for the S&P 500.
Shares Outstanding Since 1971   The performance of our stock was largely               Share repurchases are just one of the levers that
                                affected by two key factors. First, the cyclical       we employ to grow shareholder value. Other
                                downturn in the offshore drilling market was           levers include investing patiently in each of our

1.3 billion                     compounded by the plunge in oil prices in the
                                final months of 2014, resulting in a significant
                                decline in the stock price of Diamond Offshore
                                                                                       subsidiaries, acquiring businesses at attractive
                                                                                       valuations, and building our capital base. We
                                                                                       are comfortable maintaining a sizable liquidity
in 1971                         Drilling. Second, Boardwalk Pipeline Partners          position if appropriate investment options do
                                cut its cash distribution, which freed up internally   not present themselves -- and with $5 billion of


373 million                     generated capital to be used toward a number
                                of new growth projects -- but this action also
                                led to a steep decline in its share price.
                                                                                       cash and investments at year-end, we have
                                                                                       the freedom, ability and flexibility to deploy our
                                                                                       capital opportunistically.
in 2014
                                Clearly, our stock price performance in 2014           Over the long term, our capital allocation
                                was disappointing. But rather than complain            strategies have enabled us to outperform the
                                about it, we bought our shares. Seeing value,          S&P 500 and create value for shareholders,
                                we repurchased almost 4% of Loews's                    delivering compound annual returns of 17%
                                outstanding shares and 1.9 million shares of           since 1963 and 7% since 1998, when the current
Shares outstanding since 1971
(adjusted for splits)           Diamond Offshore.                                      Office of the President was formed.




2014


Loews Corporation

Annual Report

002
Total Return to Shareholders
of Loews Common Stock                                                                                                                  Loews
Since 1998                                                                                                                             S&P 500


(December 31, 1998
to December 31, 2014)




 250%
 200%                                                                                                                              Loews

  150%
                                                                                                                                   S&P
 100%
    50%
      0%
  -50%
            1998    1999    2000     2001     2002   2003   2004   2005   2006 2007   2008   2009   2010   2011   2012   2013   2014



Average annual compound returns of 7% since
current management took office in 1998 vs. average
annual compound returns of 5% for the S&P 500
over the same period.




                                                                                                                                           -
                                                                                                                                         003
Typically our annual letter to shareholders is       For Boardwalk and Loews Hotels & Resorts,            whether by buying Loews shares, investing
a look back at the year that just closed, but        the start of 2015 may have been quieter,             in our other subsidiaries, or adding another
we would be remiss if we did not mention two         but both of these subsidiaries have continued        business to the holding company.
important developments that occurred in              to make progress on their strategies.
early 2015. The Board of Directors of each           Boardwalk, like Diamond, is being affected by        We are actively seeking to further diversify our
of our two largest subsidiaries, CNA Financial       transformative change in its market, as the U.S.     portfolio, albeit in a disciplined way. We want
and Diamond Offshore, made divergent                 shale revolution continues to alter the flow of      the right deal at the right price -- either a
and independent decisions regarding their            natural gas and gas liquids. The good news is        company with good cash-on-cash returns and
respective companies special dividends.              that this transformation is creating attractive      strong secular long-term growth trends, or
                                                     investment possibilities to utilize and expand       distressed, undervalued assets at an attractive
At Diamond, the negative conditions in the           Boardwalk's existing infrastructure. While these     entry point in the cycle.
offshore drilling industry led its board to decide   opportunities will not bear fruit overnight, we
not to declare a special dividend in February        expect they will ultimately create value over the    Investing in our subsidiaries is another important
2015. As Diamond's majority shareholder, we          long term.                                           lever that we deploy to create value. Since
support its decision to retain cash so that the                                                           2010, Loews, our subsidiaries and our partners
company can maintain financial flexibility and       In 2014, Loews Hotels & Resorts successfully         have allocated more than $9 billion to capital
be ready to act if assets become available to        executed its strategy of building its brand          projects and tuck-in acquisitions across our
purchase at attractive prices.                       and broadening its customer base by                  portfolio of businesses. We have invested more
                                                     adding properties in gateway cities and              than $1 billion of holding company cash to
At CNA, the company's continued improvement          resort destinations, as well as by renovating        help facilitate transactions for Boardwalk
in underwriting performance further bolstered        existing properties. Consequently, the               and Loews Hotels, while CNA and Diamond
its capital position. In fact, CNA's balance sheet   company's financial performance is showing           have funded their capital projects and
has never been stronger, ending the year with        strong improvement.                                  acquisitions internally.
statutory surplus of over $11 billion and GAAP
shareholders' equity of almost $13 billion. In its   The breadth of Loews's portfolio reflects our        In any given period, each of our subsidiaries
most recent write-up on CNA, Standard & Poor's       belief that owning diversified businesses with       can uncover new opportunities or face difficult
stated that CNA has "AAA level capital."             different market prospects and cycles offers         challenges. Our conservatively managed
Consistent with its robust capital position, CNA's   the best way to deliver superior returns over        balance sheet can help our subsidiaries ride
board declared a $2.00 per share special             the long term. That said, we are willing to exit     out the challenges or take advantage of
dividend in February 2015 -- paying almost           a business when the assumptions underlying           changing market dynamics to emerge stronger
$485 million to Loews -- which was in addition       our original investment are no longer valid.         and better positioned.
to CNA's regular $0.25 quarterly dividend.
                                                     Our decision to sell HighMount Exploration           A deeper look at the performance and market
These very different decisions by Diamond            & Production during 2014 exemplified this            dynamics of each of our portfolio companies --
and CNA are not only in line with the interests      willingness. We didn't like the returns that would   and the activities taken by each to create long-
of their respective shareholders, they also          have been generated by investing additional          term value -- is provided in the rest of the letter.
highlight the benefit to Loews of maintaining        capital into HighMount, and determined that
a portfolio of diverse subsidiaries.                 there were better ways to deploy our capital,




2014


Loews Corporation

Annual Report

004
                                                   workers compensation business, and the
CNA                                                2014 sale of Continental Assurance Company
                                                                                                    CNA by the
                                                                                                    Numbers
Financial                                          (CAC), which represented the vast majority
                                                   of CNA's payout annuity business. The sale of    (year ended December 31, 2014,
In 2014, CNA remained sharply focused on           CAC marked another milestone in the quest        $ in millions)
driving underwriting margin improvement            to simplify CNA's operations and create a more
and profitability. The results of these efforts    focused P&C business with the potential to
are evident in CNA's 2014 performance,             deliver consistent performance. These actions,
even though the company was operating in a         in combination with the growth in CNA's
market where premium rate increases were           capital, have transformed CNA's balance
relatively modest and investment income was
constrained by the interest rate environment.

CNA's core Property & Casualty (P&C)
                                                   sheet into a source of strength.

                                                   In recognition of the company's strong capital
                                                   position, CNA's board of directors continued
                                                                                                    $9,692
                                                                                                    Revenue
operations delivered over a billion dollars of     paying a $0.25 per share regular quarterly
operating income for the second year in a          dividend throughout 2014 and, in March 2014,
row. Its underlying combined ratio improved by
more than a full point for the year due to a
better business mix, as the company continued
                                                   paid a $1.00 per share special dividend.
                                                   Additionally, as we mentioned earlier, in
                                                   February 2015, CNA's board declared a special
                                                                                                    6,900
to focus on industry segments where it has                                                          Employees
                                                   dividend of $2.00 per share. Given CNA's
deep expertise, such as technology, financial      limited ability to repurchase its own stock,
services and manufacturing, while forgoing
business that would require trading off
profitability for growth. Additionally, even
                                                   we believe returning capital to shareholders
                                                   through regular and special dividends
                                                   makes sense.
                                                                                                    3,000
though premium rate increases have slowed,                                                          Agents & Brokers
CNA did benefit from such increases in 2014.

CNA is actively managing its individual and
group long-term care blocks of business, which
at year-end had total reserves of $9.8 billion.
                                                                                                    62
                                                                                                    Field offices
Management's goal is to reduce inherent
                                                                                                    worldwide
risk by managing the claims and operations
of this business, while continuing to pursue
rate increases.

CNA's focus on risk reduction over the past
                                                                                                    $46,262
few years is also reflected in such moves as                                                        Invested assets
the loss portfolio transfer of its asbestos and
pollution liabilities, the sale of its Argentine
                                                                                                    118
                                                                                                    Years in business




                                                                                                                                       -
                                                                                                                                     005
                                                                                    experience that cycles eventually turn, and
Diamond by the                   Diamond                                            markets rebalance. When they do, Diamond's
Numbers
                                 Offshore Drilling                                  customers will ultimately refocus on reserve
(year ended December 31, 2014,                                                      replacement and production growth.
$ in millions)                   After many years of favorable market
                                 conditions, 2014 was especially challenging        Despite the stiff headwinds in the second half
                                 for Diamond Offshore. The cyclical downturn        of the year, the team at Diamond was able to
                                 in the offshore drilling market that began         win contracts with Hess Corporation for the
                                 in mid-2013 accelerated in 2014, sharply           remaining two of their four new drillships, the
                                 exacerbated by the plunge in the price of oil,     Ocean BlackRhino and the Ocean BlackLion .
                                 which closed 2014 at half its level six months

$2,815                           earlier. Oil companies have been scaling back
                                 their exploration and development budgets
                                 in light of continued depressed oil prices and
                                                                                    These drillships will be working in the U.S. Gulf
                                                                                    of Mexico, where they will join the company's
                                                                                    other two new drillships -- the Ocean BlackHawk
                                                                                    and the Ocean BlackHornet -- which are working
Revenue
                                 their own cash flow constraints. Combined          under contract for Anadarko. All four units are
                                 with the influx of new offshore drilling units     contracted into 2019 and beyond. The Ocean

$387                             entering the market, the oil price decline has
                                 led to a supply-demand imbalance that has
                                 driven down dayrates, shortened or canceled
                                                                                    GreatWhite, Diamond's ultra-deepwater harsh
                                                                                    environment semisubmersible, is scheduled to
                                                                                    join the fleet in 2016 and has an initial three-year
Net income                       drilling contracts, and idled rigs.                contract with BP.



38                               In view of the pronounced unfavorable state of
                                 the offshore drilling industry, Diamond decided
                                 not to pay a special dividend for the latest
                                                                                    We are confident that Diamond will withstand
                                                                                    this cyclical downturn. We also hope that the
                                                                                    company will emerge having found opportunities
Offshore drilling rigs           quarter, and, we anticipate, for the foreseeable   to acquire very good assets at attractive prices.
                                 future. Instead, the company has elected to        Diamond has a strong balance sheet, sufficient
                                 retain cash in order to maintain Diamond's

5,200                            financial strength and strategic flexibility, as
                                 well as to position the company to be ready
                                 to act if opportunities present themselves.
                                                                                    liquidity, moderate leverage, and a patient
                                                                                    majority shareholder that understands the cyclical
                                                                                    nature of the offshore drilling industry. All of
                                                                                    these factors will substantially support Diamond
Employees
                                                                                    as it weathers this down cycle while focusing on
                                 Diamond has a long-term view toward capital        the future.
                                 allocation that we believe is essential in a
                                 cyclical industry. Since 2009, Diamond has
                                 committed over $5 billion to new assets and
                                 fleet upgrades, while maintaining a strong
                                 and liquid financial position. The potential
                                 return on capital over the long term drives
                                 each investment decision, as does our shared




2014


Loews Corporation

Annual Report

006
                                                      approximately 2.7 billion cubic feet per day
Boardwalk                                             of firm natural gas transportation. They will
                                                                                                          Boardwalk Pipeline by
                                                                                                          the Numbers
Pipeline Partners                                     utilize a combination of existing and newly
                                                      constructed facilities to provide bi-directional    (year ended December 31, 2014,
The natural gas industry remains in a period of       flow on certain of Boardwalk's natural gas          $ in millions)
transition as shale plays continue to transform       pipelines and serve various end users, including
gas flows in the U.S. Throughout this period          two LNG export facilities along the Gulf
of change, the team at Boardwalk has pursued          of Mexico.
its strategy of connecting its natural gas pipeline
and storage assets to new end-use markets,            The $1.5 billion of organic growth projects
as well as optimizing its existing assets to take
advantage of new gas flows. Boardwalk is
focused on developing growth projects backed
                                                      also includes long-term, liquids transportation
                                                      and storage agreements to service a new
                                                      ethane cracker in Louisiana, which will convert
                                                                                                          $1,234
by long-term commitments. Additionally, the           ethane extracted from the natural gas stream        Revenue
company is diversifying into the transportation       to ethylene, one of the building blocks of the
and storage of liquids. These actions are timely,
as gas production is expected to increase
20% by the end of the decade, leading to
higher demand for natural gas transportation.
                                                      petrochemical industry. Boardwalk is able to
                                                      take advantage of projects such as this one
                                                      as a result of its acquisition of Boardwalk
                                                      Louisiana Midstream, a liquids transportation
                                                                                                          6.9           Bcf

                                                                                                          Average daily throughput
                                                      and storage company that it bought in 2012
In 2014, Boardwalk took a series of steps to          as part of its diversification strategy.
support its growth initiatives. Early in the
year, the company made the difficult decision
to reduce L.P. quarterly cash distributions
                                                      With a continued deep commitment to safe
                                                      and reliable operations, Boardwalk made
                                                                                                          1,230
by 81% to $0.10 per unit. This action was taken                                                           Employees
                                                      important and tangible progress in pursuing
to enable Boardwalk to fund more of its growth        new growth opportunities in 2014. While
with internally generated cash flow. One year
later this step appears to have been exactly
the right decision for the company and its long-
                                                      the latest initiatives will take time to generate
                                                      revenue, their potential is very exciting.          14,625
term shareholders. During that time Boardwalk                                                             Total miles
was able to fund the overwhelming majority                                                                of pipeline
of its organic growth projects with internally
generated capital.

Since the beginning of 2014, Boardwalk has
secured an impressive $1.5 billion in organic
                                                                                                          208                 Bcf

                                                                                                          Working gas
growth projects to be funded over the next
                                                                                                          storage capacity
three to four years. These projects include




                                                                                                                                             -
                                                                                                                                           007
                                                                                       The addition of these new properties will give
Loews Hotels & Resorts           Loews Hotels & Resorts                                Loews Hotels more than 12,000 guestrooms
by the Numbers
                                                                                       across 24 hotels by the end of 2016. But that
                                 Loews Hotels & Resorts also recorded a year
(year ended December 31, 2014,                                                         number could grow, as the company is
                                 of important progress in 2014, reaping initial
$ in millions)                                                                         continually on the lookout for new properties
                                 benefits from an extensive renovation program
                                                                                       and development projects that will enhance
                                 mainly completed in 2013, and moving forward
                                                                                       its value.
                                 on its strategy to build its brand and broaden
                                 its customer base by adding new properties in
                                                                                       We are pleased to welcome S. Kirk Kinsell as
                                 gateway cities and resort destinations.
                                                                                       the President & Chief Executive Officer of

$995                             Among the highlights was the opening of the
                                 1,800-guestroom Cabana Bay Beach Resort,
                                 Loews Hotels' fourth hotel in Orlando and
                                                                                       Loews Hotels, and look forward to his leading
                                                                                       the company into its next phase of growth
                                                                                       and operational excellence. Kirk, who will join
Chain-wide revenue                                                                     Loews Hotels in March 2015, has extensive
                                 part of its very successful 15-year partnership
                                                                                       experience in the hospitality industry, having
                                 with Universal Orlando Resorts. Loews Hotels

21                               recently announced another project in Orlando
                                 with Universal -- the 1,000-guestroom Loews
                                 Sapphire Falls Resort -- which is scheduled to open
                                                                                       served for the past 19 years in increasingly
                                                                                       senior positions with InterContinental Hotels
                                                                                       Group. Kirk's track record and broad experience
                                                                                       in development, sales and brand operations
Chain-wide hotels                in the second half of 2016. When completed,
                                                                                       make him an ideal choice to build upon the well-
                                 this new hotel will bring the number of on-site
                                                                                       regarded Loews Hotels luxury brand and

10,893                           guestrooms at Universal Orlando Resorts
                                 to 5,200.

                                 Loews Hotels also made major strides in
                                                                                       the solid foundation of hotels now in place.

                                                                                       Kirk succeeds Paul Whetsell, who led the company
Chain-wide guestrooms
                                                                                       for the past three years, growing the brand
                                 expanding its footprint in the Midwest with the
                                                                                       and improving operations exponentially. Paul
                                 acquisitions in 2014 of the 251-guestroom

8,900                            Loews Minneapolis Hotel and the 556-guestroom
                                 Loews Chicago O'Hare Hotel. The Loews
                                 Chicago Hotel, a new 400-guestroom hotel in
                                                                                       will become Vice Chairman of Loews Hotels
                                                                                       after Kirk signs on in March and, we are pleased
                                                                                       to say, will continue to lend his expertise and
Chain-wide employees                                                                   counsel to the company.
                                 downtown Chicago, will open in March 2015.
                                 Loews Hotels also announced in early 2015 that
                                 it has agreed to acquire a 158-guestroom hotel
                                 in the heart of San Francisco's Financial District,
                                 a major gateway market.




2014


Loews Corporation

Annual Report

008
                                                     We thank our shareholders for their confidence,    Per share value of Loews's stake in
A Portfolio and Strategy                             and our employees and directors -- at              shares of CNA, DO, and BWP based
for the Long Term                                    both Loews and our subsidiaries -- for their
                                                                                                        on New York Stock Exchange closing
                                                                                                        prices on December 31, 2014, and
                                                     commitment to the long-term principles             Loews's net cash and investments (net
As we look to the future, we are reaffirming         of value creation.                                 of debt) at December 31, 2014.
our commitment to pursuing a value-oriented
investment strategy and to creating a diverse        Sincerely,
portfolio of solid businesses. We have great
confidence in the long-term prospects of each
                                                                                                       $25.16 $9.11
                                                                                                       CNA                   Cash & Investments
of our businesses. CNA is strengthening the                                                                                  (net of debt)
underwriting performance of its core P&C
operations while exiting underperforming
segments. Diamond is operating in a challenging
                                                                                                       $7.09                 $5.98
                                                     James S. Tisch            Andrew H. Tisch         DO                    BWP
market but is positioned to withstand this
downturn and hopefully capitalize on today's
troubles to create tomorrow's opportunities.
Boardwalk is repositioning its operations to align
with the evolution of the U.S. natural gas
marketplace. And Loews Hotels is adding to its
presence in key markets with exciting potential.
                                                     Jonathan M. Tisch                                       $47.34
                                                     Office of the President
                                                                                                                        Total
Uniting these diverse businesses is a focused
investment strategy at the parent company            February 23, 2015
level, based on ample "dry powder," multiple
capital allocation levers, and a perspective
gained from investing across many economic
                                                                                                                    Non-public
cycles. Loews will continue to deploy these                                                                       holdings include:
resources with a goal of creating value for our
shareholders well into the future.



                                                                                                                 Loews Hotels
                                                                                                                      &
                                                                                                               Boardwalk Pipeline
                                                                                                                General Partner




                                                                                                                    Compared to



                                                                                                             $42.02   NYSE:L
                                                                                                             New York Stock Exchange
                                                                                                             closing price of Loews
                                                                                                             common stock as
                                                                                                             of December 31, 2014.


                                                                                                                                                    -
                                                                                                                                                  009
