2010 was an extraordinary year for our Company,
highlighted by the closing of the merger of Stanley
Works and Black & Decker on March 12. Undoubtedly
the most significant transaction in both companies
histories, the formation of this enterprise creates the
undisputed tool industry leader with global scale,
strong growth prospects and prodigious cash flow. Our
Company, with its growing market capitalization and
cash flow, combined with attractive growth platforms
and business building capabilities, is continuing its
journey to become a diversified industrial leader.
Financial results were solid in 2010, in
spite of a weak economic underpinning in
the developed countries. Total revenues
were up 125% to $8.4 billion. Earnings
grew 176%, while free cash flow increased
$489 to $935 million, powered by benefits
from the merger integration and the
Stanley Fulfillment System (SFS).(1)
Our strong results are a manifestation of
our Companys commitment to driving
shareholder value through a strategy
aligned with our long-term financial
objectives as described below:
 46% organic revenue growth; 1012%
total revenue growth
 Mid-teens EPS growth
 Free cash flow greater than or equal to
net income
 Return on capital employed (ROCE)
between 12% and 15%
 Continued dividend growth
 Strong investment grade credit rating
These objectives have been intact since
2004, with the exception of our revenue
growth targets, which were increased from
a previous range of 35% organic and
810% total growth following the merger,
due to our continued footprint expansion
into emerging markets and our higher free
cash flow base which, in part, will fuel the
expansion of our growth platforms. Since
these objectives were established, total
average annual revenue growth has been
25%. In the four years preceding the recent
recession, our average organic revenue
growth was 5% and, as a combined
Company in 2010, our organic growth
rate was 5%. Net income has grown at a
9%(1) compounded annual rate, slightly
below our long-term target but strong
nonetheless considering the near-trough
stage in the cycle in which we operated
during 2010. ROCE has averaged 13%
during this seven-year period.(1)
At the same time, free cash flow has
averaged 133% of net income and has
grown at a 10% CAGR.(1) Our dividend
has been increased every year without
interruption and our credit ratings
continue to be in strong investment
grade territory. We are pleased with our
performance versus these goals and are
confident that we will see meaningful
improvements in our net income CAGR
in 2011 and 2012. We are also pleased
with our stock performance which, as of
December 31, 2010, had outperformed
the S&P 500 Index on each of a one-year,
three-year, five-year and ten-year basis.
In fact, while the S&P 500 experienced no
price appreciation at all during the last
decade, our stock more than doubled,
while paying a steady and growing
dividend.
Stanley Black & Decker: The Story
The thesis behind this landmark merger
is compelling: to take two strong
companies, combine them, and create
an even stronger global enterprise,
positioned for success in the years and
decades to come. The merger created the
global leader in hand and power tools
with a stable of iconic brands, worldclass
innovation processes, outstanding
channel access, enormous global reach,
including into high growth emerging
markets, significant scale efficiencies
and benefits from the Stanley Fulfillment
System. At the time of the announcement,
cost synergies were believed to be $350
million over three years which, when
capitalized, represented an extraordinary
36% of the combined companies market
capitalization. The transaction was also
expected to be highly accretive to earnings
per share of the new Company with an
incremental $1.00 per share expected by
year three. The stocks of both companies
performed well, with legacy Black & Decker
and legacy Stanley Works up 31% and 10%,
respectively, in the first ten days after the
announcement. Stanley Black & Deckers
price movement was in the top decile of all
equity performances experienced in U.S.
stock, for stock transactions over $500
million announced during the last decade.
We are keenly aware that this
overwhelmingly positive reaction was
both an endorsement of the strategic and
financial logic of the merger, as well as
a vote of confidence that the integration
would be successful and the announced
targets would be met or exceeded over
time. We take this responsibility very
seriously and have established the
organization, operating mechanisms and
governance processes to ensure success.
The integration got off to an excellent
start and continues to be firmly on track.
We exceeded our original calendar year
2010 announced cost synergy commitment
by $45 million, bringing the total amount
realized for that period up to $135 million.
Further, we recently upgraded our total
cost synergy commitment from $350
million to $425 million and reduced
the expected time to fully realize these
synergies from 36 to 33 months. We
also recently announced that free cash
flow, which was originally expected to
total $1 billion by year three, was likely
to be $1.1 billion in the first full year of
the merger.(1) As a result of this financial
outperformance, we were able to
announce a 21% dividend increase
in February 2011, signaling our high
level of confidence in the Companys
future trajectory of earnings and cash
flow growth.
We also recently provided a first look at
the quantification of revenue synergies,
with the expected revenue benefit
totaling between $300 and $400 million
in addition to our normal organic growth
initiatives over the intermediate term.
These synergies result from a myriad of

opportunities around the globe arising
from the merger, including but not limited
to brand expansion and leveraging
complementary geographic and channel
strengths.
Our experience thus far with melding
the two companies cultures has been
extraordinarily positive. The similarities
are far greater than the differences,
especially in the areas of growth, brand
expansion, end user focus and product
innovation. This is highly positive because
it enables our management teams to
galvanize around attributes that are
most critical to serving our customers
effectively, which some would say is the
most energizing aspect of running
a business.
The first chapter in the story of Stanley
Black & Decker has been a great one and
we expect that this is just the beginning
of a long and fulfilling journey to create
exceptional shareholder value.
Other 2010 Highlights
In a significant step in the establishment
of our Infrastructure Solutions growth
platform, we acquired CRC-Evans Pipeline
International (CRC-Evans), a leading global
supplier of specialized tools, equipment
and services for the construction of oil
and natural gas transmission pipelines.
With 2010 revenues of approximately $250
million, nearly 60% of which were derived
from emerging markets, CRC-Evans is a
scalable, global growth vehicle.
We expanded our $915 million Convergent
Security Solutions electronic security
business with the acquisition of ADTs
French operations (2009 revenues
approximately $175 million), which we
rebranded Stanley Solutions de Scurit,
growing our market share of the $1 billion
French commercial security monitoring
business to approximately 25%.
We took an important strategic step
forward in evolving our H ealthcare growth
platform with the acquisition of InfoLogix
(revenues approximately $55 million), a
leading provider of mobile workstations
and productivity solutions to hospitals.
InfoLogix offers acute care providers with
custom-designed process improvements
to reduce their costs, improve safety levels
and assist in compliance with growing
regulatory requirements.
We also announced a majority-owned
joint venture with Shanghai-based GMT
Hardware Enterprise Corp. (GMT), a
leading manufacturer and distributor of
commercial hardware in China with 2009
revenues of $40 million. This strategic
partnership provides us with instant
access to one of the best commercial
hardware brands in China, as well
as a strong and well-developed local
distribution network.
Where Do We Go from Here?
The strategy remains the same. We will
continue to strengthen our core franchises
through strong brand support, robust
product and business model innovation
and the relentless implementation of SFS.
We will continue to pursue consolidating
acquisitions in tools when the economic
and strategic rationales are strong;
however, the bulk of our acquisition
capital in coming years will be directed
towards expanding our high margin growth
platforms in Security and Engineered
Fastening and continuing to build upon
early successes in H ealthcare and
Infrastructure. Significant emphasis will be
placed on growth in the emerging markets,
both organically and through acquisition.
We view one of our most important roles
as being stewards of capital. Since 2004,
we have been pursuing a strategy of
reinvesting two-thirds of our free cash flow
into accretive acquisitions and returning
the other third to shareholders in the
form of dividends and share repurchases.
We reinvested 52% and returned 48%
to shareholders during this period while
still achieving our growth objectives. Our
intent is to reinvest enough cash to fund
above-average growth while returning
enough cash to be responsive to investors.
The goal is to create an attractive hybrid
value proposition: a growth company that
returns a large percentage of cash to its
shareholders. This approach does not
appeal to everyone inasmuch as we dont
fit into a neat box; however, it has resulted
in above-average returns for investors and
we are quite comfortable with it. It does
require an unrelenting focus on operating
returns and cash flow, which we have
achieved through management rigor and
process improvement, especially as it
relates to SFS.
Stanley Fulfillment System
SFS has evolved over the past decade
and really began to gain traction across
the Company in 2007. At that time, our
working capital turns were 4.5. Through
the implementation of SFS principles over
the succeeding three years, including
operational lean, complexity reduction,
sales and operations planning, global
supply management and order-to-cash
excellence, we were able to improve
overall turns for legacy Stanley to 8.6
in 2010.
This was achieved during a time of
general economic weakness and volume
contraction. The process excellence
required for this type of performance
resulted in significant customer service
level and cycle time improvements for
our customers. H undreds of millions of
dollars of cash were freed up for capital
allocation and our balance sheet strength
was maintained in the face of very difficult
economic conditions.
As we go forward, SFS represents one
of the biggest opportunities to create
additional value from the Stanley Black &
Decker merger, with the potential to unlock
over half a billion dollars if we are able to
return to the 8 turns level within three to
four years, which we believe is feasible.
Summary
2010 was a year of successes for Stanley
Black & Decker with the closing of the
merger, a strong start to the integration,
solid financial results and strategic
progress across the Company. We are
proud of our accomplishments but remain
firmly grounded and focused on execution
and constantly and methodically moving
the Company forward. Our strategy is
aligned with our long-term financial goals
which, in turn, are intended to drive stock
price outperformance. H owever, the most
important part of our success derives
from our people, whose passion for this
Company, its customers and its future is
palpable. With that as a foundation, we
are confident in our continued success
and a bright future.


John F. Lundgren
President & Chief Executive Officer

James M. Loree
Executive Vice President & Chief Operating Officer