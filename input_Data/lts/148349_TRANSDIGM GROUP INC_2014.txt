FELLOW SHAREHOLDERS:


Fiscal 2014 was a busy and profitable year for TransDigm. We took advantage
of favorable credit markets to fund payment of another special dividend of $25
per share and refinance approximately $1.6 billion of 7-3/4% Senior Subordinated
Notes with lower interest rates. We also completed two acquisitions and purchased 909,700 shares of our own stock, all while supporting our existing businesses, maintaining financial flexibility, and growing net sales, EBITDA as Defined
and adjusted net income at double-digit rates. We are pleased that our consistent
performance continues to provide shareholders with exceptional returns.
The acquisitions of actuator manufacturer Elektro-Metall Export (EME) and
parachute manufacturer Airborne Systems for a combined purchase price of
approximate $310 million further expanded our geographic footprint. Germanbased EME is our first standalone European acquisition, and Airborne has two
facilities in the United Kingdom. We now have 10 locations in Europe, six
in Asia and two in Mexico.
Fiscal 2014 net sales rose 23.3% to $2,372.9 million from $1,924.4 million
in fiscal 2013. Organic net sales grew approximately 8% with the favorable
contribution from acquisitions accounting for the balance of the increase.
After a soft 2013, the commercial aftermarket recovered and organic sales
increased 12%. Commercial OEM sales increased 8% driven by a 12% rise
in commercial transport OEM sales.
Adjusted net income, which excludes non-cash stock compensation, acquisitions,
refinancing, expenses related to dividends, and other non-recurring charges,
increased 16.4% to $442.4 million, or $7.76 per share, from $380.1 million, or
$6.90 per share, in fiscal 2013. 

EBITDA As Defined, a meaningful indicator of operating performance, increased
19.2% to $1,073.2 million in 2014 compared with fiscal 2013. Our fiscal 2014
EBITDA As Defined margin remained high at 48% of net sales, excluding dilution
from the five most recent acquisitions. This represents a 1% increase over the
same margin in fiscal 2013.
Amidst record sales and new business orders, we continued to tightly manage
our cost structure. Productivity initiatives included consolidating some acquired
manufacturing operations, moving other operations to lower-cost locations,
and enhancing our strategic sourcing of material from efficient domestic and
offshore sources.
A UNIQUE BUSINESS MODEL
Our strong performance in 2014 reflects the ability of our unique business
model and consistent strategy to create and sustain intrinsic shareholder value
through all phases of the aerospace market cycle. To restate, the strategy has
five elements:
Q Operate a wide range of proprietary niche aerospace businesses with
 significant aftermarket exposure. More than 90% of our net sales are
 generated from proprietary products and around three-quarters are from
 products for which we are the sole source provider. About 55% of net
 sales and a much higher percentage of EBITDA are from the higher margin,
 more stable aftermarket.
Q Consistently pursue a three-part value-based operating strategy focused on
 new business, improvements to our cost structure, and value based pricing.
Q Maintain a decentralized, incentivized organization that keeps us close to the
 customer and enables us to attract and retain entrepreneurial managers who
 think and act like owners.
Q Follow a focused and disciplined process to acquire proprietary aerospace
 businesses with significant aftermarket revenue where we can demonstrate
 a clear path to value creation.
Q Maintain a capital structure appropriate to achieving our long-term goal of
 private equity-like returns with the liquidity of a public market.
FLEXIBLE CAPITAL STRUCTURE
We view our capital structure and the efficient allocation of capital as another
means to create shareholder value. We are willing to increase our debt leverage
when we see good opportunities or view our leverage as suboptimum for value
creation. We strive to return money to our shareholders when and if that seems
an appropriate way to create value. 

As a result of our strong free cash flow, high margins and relatively low capital
requirements, we have been able to de-lever quickly. Last year was the fourth
time since the end of fiscal 2009 that we have returned significant cash value to
shareholders through special dividends funded in large part through debt financing.
After paying the most recent special dividend, buying back about $160 million of
our stock, and making two acquisitions for $310 million, we still closed fiscal year
2014 with $820 million in cash, about $400 million in unrestricted and undrawn
revolver, and additional capacity under our credit agreement. Based on current
capital market conditions, we believe we have adequate capacity to make more
than $2 billion of acquisitions without issuing additional equity.
MANAGEMENT CHANGES
President and Chief Operating Officer Ray Laubenthal decided to retire after
21 years with TransDigm. Ray has been a key part of building TransDigm since
its inception, and we are deeply grateful to him for his service, his contributions
and his friendship. He was appointed to the Board on December 31, 2014, and he
remains a significant investor in the Company.
In keeping with our close attention to succession planning at the senior level and
throughout the organization, we hired Kevin Stein to be a Chief Operating Officer
of our Power segment. This segment makes up about half of our business. Kevin is
a high-caliber senior aerospace executive with extensive operating experience.
He comes from Precision Castparts Corp., a well-managed aerospace company
that has consistently generated substantial value for its shareholders.
Additionally, we promoted Bob Henderson to be the new Chief Operating Officer
of our Airframe segment. Bob has been a significant partner in our growth over
the last 20 years. He has been an Executive Vice President since 2005, responsible
for a broad range of the Company�s businesses and many acquisition integrations.
Bob brings a unique understanding of our culture, history and value creation process.
In summary, 2014 was a very good year for TransDigm. I�m confident that with our
consistent value-focused strategy and strong mix of businesses, we can continue
to create long-term intrinsic value for our shareholders. We thank our shareholders for their investment and for their support of our proxy proposals. We thank
our customers for choosing TransDigm, and we thank our managers and employees for their hard work and dedication. We look forward to reporting to you on
our progress during 2015.
SINCERELY,
W. Nicholas Howley January 9, 2015
Chairman and Chief Executive Officer