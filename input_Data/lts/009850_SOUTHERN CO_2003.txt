Our disciplined approach to the business and focus on the
fundamentals have produced a record of consistent success.
The past year illustrates what I mean. As they have so many
times before, Southern Company people did an outstanding job
in 2003. We met or exceeded our financial, operational, and customer
satisfaction goals. Reported earnings were $1.47 billion, or
$2.03 per share. Thats a record for Southern Company, including
the period before the Mirant spinoff in 2001. Our focus on
the business, customers, and region we know best has proven to
be right for us.
If you want to know what kind of company we are, look at
how we achieve our results. Our profile is clear. We intend to be
a stable, income-producing investment with predictable earnings
growth. We stay focused on meeting our long-term objectives
while the business fads of the moment come and go.
A SOLID CORE
Our core business is generating and delivering electricity in
the Southeast. We do that very well.
The 2003 results from our regulated retail business, our
biggest business, were solid. We faced mild weather most of
the year. That meant retail customers used less electricity. But
the Southeast continues to grow and we gained 68,000 new
customers. That offset some of the negative effects of the weather.
And in an encouraging sign about the economy, we saw sales to
industrial customers increasing late in the year.
Reliably serving our customers requires well-planned and
properly operated generation and power delivery systems.
We have that. Our plants operated at record levels of reliability in
2003. And we continued to invest in transmission and distribution
facilities. The importance of a well-maintained system was unfortunately
demonstrated last summer when the Northeast suffered
a widespread blackout. No utility can guarantee there never will
be a blackout. But our actions speak louder than words about
the importance we place on reliability. This has a direct impact
on customer satisfaction, which is a priority for our people.
Competitive generation is the higher-growth part of our business.
It had an excellent year. In general, we build competitive
generation capacity only after securing long-term contracts with
wholesale customers for the output. This approach fits our lowrisk
profile and has helped protect us from the price volatility in
wholesale markets that has hurt some other players. Last year we
began operations at Plant Stanton A, a new competitive generation
facility serving three utilities in central Florida. Its a good
indicator of the potential for growth in the expansive area we call
the Super Southeast. Our competitve generation business also
benefited from more short-term energy sales opportunities than
had been expected. Mild weather in our service area made more
of our lower-cost coal generation available for the wholesale market.
And higher natural gas prices expanded the opportunities to
sell energy from the coal-fired generation to wholesale customers.
Our goal was to earn more than $200 million from competitive
generation by 2005. We achieved that in 2003, and now weve
set the bar higher with a goal to earn $300 million by 2007.
We have an important role to play in continuing to make
the environment better. We accept this responsibility. We have
continued our progress in reducing emissions of nitrogen oxides
and sulfur dioxide. In Atlanta, the largest metropolitan area we
serve, we have made a major investment in plant controls that
has significantly reduced our impact on smog. Were actively
working, both on our own and with other parties, to find effective
ways to reduce releases of carbon dioxide and mercury. The
challenge to continue and expand environmental improvement
while keeping energy affordable is greatest in regions like the
Southeast that are growing. Weve made real progress and we plan
to continue major investments in environmental controls while
searching for better technological solutions. We are doing our
part to create a healthier environment. After all, we live here too.
THE NEXT PHASE
Late last year, I announced my plans to retire in July. As part of a
succession and management transition plan, David Ratcliffe will
become president in April, and chairman and CEO in July.
Personally, after 34 years with this great company, it is a good
time to make a change. From the companys standpoint, its also
a good time for a change. Weve achieved the goals we set at the
time of the Mirant spinoff. Among other things, we fully implemented
our regional strategy, strengthened the balance sheet,
consistently met our earnings growth objectives, and got back on
track with dividend increases. Now its time to move to the next
phase, focusing on continuing earnings growth over the long
term while preserving the low-risk characteristics of the company.
David is a great choice to lead the company. For more than
20 years, Ive known David and seen him produce excellent
results, most recently as CEO of Georgia Power, our largest
subsidiary. Furthermore, David has been involved in all the key
strategic discussions in recent years that have produced such
good results for shareholders, customers, employees, and the
communities we serve.
This transition speaks to one of the great strengths of
Southern Company  the commitment, dedication, and talent of
our people. We have a history of developing people and promoting
from within. This transition has created stability and confidence
that Southern Company will stay the course. It makes me
feel very confident about the future of the company.
Coming to work for Southern Company was one of the best
decisions Ive ever made. Im fortunate to have been part of a
company that provides the kind of essential service that Southern
Company does, that plays such a positive role in the communities
we serve, and that has been so good to investors over the
years. The company has been flexible enough to meet changes
in the business environment. But the basic values havent
changed  integrity, honesty, being active as a good citizen, and
serving customers well. None of that has changed, nor do I
expect it will. Your company is in good hands.

Allen Franklin
Chairman, President, and CEO