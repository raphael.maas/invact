
Anyone who doubts that industrial real estate is now a 
global industry should visit the Yangshan Deepwater Port 
outside Shanghai. The port is a wonder of modern, world-class
engineering, built on a rock island in the East China Sea and
linked to the mainland by a 30-kilometer bridge.
Yangshan, which opened in 2005, will be the largest
container port in the world when fully developed. It 
is set to become a crucial node in the global supply
chain and will help process the steadily increasing 
flow of goods between China and western markets.
Over the course of the last year, I and other ProLogis
management team members have been to Yangshan
several times to visit the facilities we are developing 
at our exclusive distribution park just off the bridge on
the mainland. On every trip, I am struck by how quickly
global trade and logistics outsourcing have changed
our industry and how important it has been for ProLogis
to build a global platform over the past 10 years.
The same trends are clearly evident at other ProLogis
developments all over the world. In Japan, demand 
for space at our distribution parks in Tokyo, Osaka 
and other key markets has been truly exceptional, as
companies embrace outsourcing of logistics. East of
the Los Angeles/Long Beach port complex, our 25 million square-foot stabilized portfolio in the Inland Empire
was fully occupied at year end. In Central Europe, we
have seen robust demand from third-party logistics
companies looking to serve markets in Western Europe
as well as growing domestic consumption. 

When Walt Rakowich and I first joined ProLogis in
1994, we owned less than $1.0 billion of industrial
properties  all in the United States. By the end of
2006, our assets owned, managed and under development exceeded $26 billion, with more than 40 percent
in Europe and Asia.
Today, the breadth and balance of our operations 
are among our most important strategic advantages
and are key drivers of our financial performance. In 
2006, we recorded 36 percent growth in funds from
operations per share, driven by strong performance 
in all three of our business segments. We achieved
record gains from our global development activity, 
solid increases in property fund income and fees and
continued improvement in property operations. 
Our diverse global platform, strategic land positions
and strong customer relationships support future growth
in our development, or CDFS, business. Our $2.5 billion
of new development in 2006 was well balanced across
North America, Europe and Asia. Based on continued
strength in customer demand, we plan to increase development starts in 2007 by roughly 20 to 30 percent. This
development, led by Ted Antenucci and our talented
teams throughout the world, also will be distributed fairly
evenly across the three continents, thereby minimizing
our exposure to any single national or regional economy. 

During the year, we enhanced our future development
opportunities through several key transactions. These
included an acquisition of land and properties in
Mexico City and Guadalajara that gave us a strong 
market leadership position in Mexico; completion 
of nearly 5.3 million square feet of new distribution
centers to serve growing demand in Japan; and new
land reservation agreements in China that enable
development of up to 32 million square feet in key
coastal and inland logistics markets. 
Building on this momentum, in February 2007, 
we acquired the industrial development business 
of Parkridge, one of our top competitors in Europe. 
As a result, we now have access to strategic land 
positions on which we can develop an additional 
$3 billion of properties in Central Europe and the
United Kingdom. The transaction extends our leadership position as Europes largest provider of distribution facilities and enhances our organization 
through the addition of some of Europes top real 
estate professionals.

Our property fund business continues to be an 
important source of growth for the company. With 
more than $12 billion of properties under management, our funds provide a steady, growing source 
of fee income as well as our share of each funds 
earnings. In 2006, we recognized cumulative incentive fees of $131 million due to strong performance 
within our property funds. Most of this amount was
related to the successful initial public offering of
ProLogis European Properties (PEPR) in September. 
The success of the offering reflects the quality of 
our European portfolio and market leading position 
we have established since we entered Europe 
in 1997.
Additionally, we achieved meaningful improvement 
in our property operations business last year. We
leased over 100 million square feet of space  a
record amount  and this strong leasing activity is 
continuing into 2007. At the end of 2006, growth 
in rental rates and net operating income in our
same-store pool were at their highest levels since 
the second quarter of 2002, driven by occupancies
that have recovered and remained stable at approximately 95 percent.

We remain exceptionally well situated as we 
continue to build on our business platform and 
global franchise. We have a solid financial position and expansive pipeline of opportunities. Strong 
market conditions and the continued vitality of 
global customer demand support our confidence 
in our ability to generate future growth in all our 
business segments. Importantly, our talented global
team has the experience and relationships necessary
to continue to capture an increasing share of the
industrial business in key logistics markets. 
We thank you for your continued support as we 
explore new ways to leverage the strength of our 
business and our colleagues demonstrated ability 
to deliver value for our customers and shareholders. 
Jeffrey H. Schwartz, Chief Executive Officer; Walter C. Rakowich, President and Chief Operating Officer; Ted R. Antenucci, 
President Global Development
Jeffrey H. Schwartz
Chief Executive Officer