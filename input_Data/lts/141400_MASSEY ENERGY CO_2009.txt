Dear Fellow Shareholders:

This year will mark my 20th anniversary as Chairman
and CEO of Massey Energy Company and my 40th
year as a coal miner. Both are important personal
milestones. More significantly, these anniversaries
demonstrate a commitment to coal that is shared by
all Massey stakeholders, including our shareholders,
our members, our communities, and our customers.
As 2009 unfolded, this commitment was challenged as never
before by regulators in Washington and anti-coal activists in
Appalachia and around the country. Coal became the enemy.
Attacks on coal-generated electricity threatened the viability
of this nations manufacturing base. Government-proposed
restrictions on the consumption of coal were matched by
new attempts to limit production as well. The Environmental
Protection Agency delayed the surface mining permits needed
to produce low-cost coal. Emboldened by the governments
treatment of coal, anti-coal activists willfully ignored the law
and the livelihood of Massey members with their protests
against legitimate coal mining operations.
These efforts not only threatened our shareholders, but also
presented a clear and present danger to Appalachia  our
people, our jobs and our way of life. Under political pressure,
even some of Appalachias own elected representatives in
Washington suggested that coals future was limited.
Nothing could be further from the truth. Coal is the solution,
not the problem. Coal fuels economic growth. Coal produces
the low-cost electricity that is critical to economic development,
job creation and an improving quality of life for all. Only when
people have quality jobs and suffificient economic resources to
provide for themselves and their families will they be able to

devote their attention to environmental concerns.
Nothing benefifits the environment like prosperity.
Coal is also critical to ensuring our energy independence
and national security. Anti-coal activists
ignore the critical role coal plays in providing
energy in America and around the world. Reducing
or eliminating coal as a key energy resource would
increase energy costs for all people and all businesses,
limit economic opportunity, and intensify
our dependence on foreign energy sources. These
foreign sources include petroleum from the
Middle East and high cost wind turbines manufactured
overseas.
Despite the misguided efforts to reduce coal
production and consumption in this country, world
coal consumption continues to increase faster than
any other primary energy source. The Department
of Energy estimated in May 2009 that world coal
consumption will increase by 49 percent from
2006 to 2030. Thats good news for Massey and the
people of the world. It means people  especially
those in the poorest nations and in the most
desperate conditions  may enjoy the benefifits
of low-cost electricity.
Sadly, approximately 1.6 billion people in the
world still live without electricity, a lamentable
and altogether preventable condition that too
often means unsafe water supplies, hunger and
disease. The quality of life for people around
the world depends on affordable electricity.
And affordable electricity depends on coal.
Economic growth and the jobs that accompany
it are driven by coal-fifired electricity. China is but
the most recent example. Its economic growth is
averaging 9.9 percent a year, fueling a 12.5 percent
increase in coal use annually. Coal-fifired electricity
drives Chinas manufacturing expansion, industrial
might and, most important, the dramatic
improvement in health and lifestyle of its people.
Coals most outrageous critics have rejected all the
benefifits of coal and claimed the dangers of climate
change outweigh all of coals benefifits. But the
credibility of these climate change proponents has
been questioned in the wake of misinformation
scandals. Some global warming experts have now
admitted that there has been no statistically signifificant
warming for the past 15 years. United Nations
reports that the Himalayan glaciers would melt by
2035 were revealed to be completely unreliable.
In the wake of these scandals, corporate support
for aggressive action on global warming is appropriately
waning. Three corporate members of the
U.S. Climate Action Partnership  BP America,
Caterpillar and ConocoPhillips  have resigned.
One corporate offificial announced it was now
time to address issues that affect shareholders
and consumers.
At Massey, we couldnt agree more. We have
remained focused on doing what is right for all
our stakeholders, including our investors, our
members, our customers, our communities and
our country.

Massey is especially proud of our continued
success in improving the safety of our nearly
6,000 members. Last year was the safest ever at
Massey  as was the year before that and the year
before that. Our success in protecting our miners
has been recognized at both the federal and state
level. Massey has been honored with numerous
awards for our outstanding safety record, including
recognition by state regulators and the Federal
Mine Safety and Health Administration. The
recognition that these awards represent are
welcomed by everyone at Massey.
While we are proud of our safety record, Massey
will not rest on its laurels. We continue to endorse
and implement a culture of safety by developing
innovative programs and technologies, encouraging
worker training and devoting engineering
resources to anticipate and eliminate potential
safety problems. We are continually developing
safety systems, procedures and equipment
that will protect our members. Our latest effort
includes the development of an innovative new
miners helmet that exceeds industry requirements
and is likely to become the standard in the
mining industry. These types of investment and
innovation more clearly defifine our commitment
to safety than does our compliance with state and
federal regulatory requirements.
Massey is also proud of its success in protecting
the environment in Central Appalachia. We
continue to invest large amounts of time and
money to maintain and improve the natural
surroundings of our communities because this is
not only where we work, but it is where we live,
too. Environmental stewardship has become part
of our companys DNA.
As a result, reclamation efforts remain a top
priority. Massey continues to lead the mining
industry in acres of land reclaimed. As part of
this focus on reclamation, we plant more than
1 million trees and restore miles of streams each
year. Through the use of modern technology
and diligent efforts throughout the Company, we
have strengthened our environmental compliance
record and greatly improved the quality of water
discharged from Company mining sites. In fact,
the water that flflows out of Massey operations is
typically cleaner than the natural streams into
which it is discharged.
We remain fully committed to sustaining and
improving our environmental stewardship and
our safety performance relative to regulatory and
industry standards. However, we will continue to
voice our concerns with unwarranted and unnecessary
regulations that damage or offer little in the
way of environmental improvement or worker safety
yet undermine Americas economic prosperity.

I am increasingly concerned with ill-considered
regulations promulgated by federal offificials
with little or no experience in the private sector.
The EPA, for example, increasingly aims to delay
surface mining permits throughout Appalachia.
Environmental extremism leads to unworkable
and unreasonable surface mining regulations
that could eliminate thousands of jobs in Central
Appalachia during a period of economic distress
and harm those citizens most dependent on lowcost
electricity. Approximately 40 percent of the
coal mined in the region comes from surface
mining, which also provides 14,000 high-paying
jobs. These regulatory restrictions on energy
development not only threaten our nations
energy independence, but also our security
and prosperity.
Overzealous regulation restricts economic growth,
impedes the hopes of a recovery in 2010, and
eliminates opportunities to create the jobs that
so many Americans are so desperately seeking.
We are in a government-created recession that is
damaging the American dream.
Ill-considered regulations now reach into every
nook and cranny of our economy, potentially
costing more jobs than government stimulus
spending will ever create. We are told that regulation
 not free enterprise or the market  will
drive innovation. Companies are told to fifind
creative ways to comply with government
strictures
on business. Unfortunately, increased
regulation will only make environmentallyfriendly
and safety-conscious U.S. businesses
less competitive in the world marketplace and
encourage corporations to locate offshore.
Massey speaks out on these issues because we
believe it is possible to alter the political environment
by making common sense arguments that
will benefifit our company and our country. We
will continue to demand honest discussion of
energy, environmental and regulatory issues
because such a debate is critical to the interests
of all Massey stakeholders, all Americans and,
in fact, to all people of the world.
Thank you for your continued support.
Don L. Blankenship
Chairman and Chief Executive Offificer