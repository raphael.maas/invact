To Our Shareholders:
2008 was a watershed year from many perspectives. An economy that began the year promising
robust growth slid precipitously into the deepest recession since the Great Depression during
the fourth quarter. Daily liquidity replaced
growth as the No. 1 priority for businesses. And
business leaders struggled to demonstrate
that their enterprises were adaptive, their balance
sheets strong, their technologies compelling
and their global workforces capable of managing
the turbulent economic environment  all in
a span of 12 short months.
In the face of these challenges, I could not be
more proud of our global team. Eaton people
were ON their game every day around the world.
As a result of their efforts, we were not only
able to adapt quickly to the economic crisis; we
were also able to build for the future by expanding the breadth of our business portfolio. By
acquiring  and successfully integrating  The
Moeller Group and Phoenixtec Technologies,
we have expanded our Electrical business to be
approximately half of our corporation, making
Eaton a global leader in both the power distribution and power quality markets.
Staying ON course in volatile times
Among the years financial and operating
highlights:
	 Our sales surpassed $15 billion for the first
time in Eatons history, growing by 18 percent
to $15.4 billion.
	 For the eighth consecutive year, our revenue
growth outpaced the growth in our end
markets  by $120 million in 2008.
	 We completed or announced six acquisitions
and one joint venture during the year, highlighted by our acquisitions of Moeller and
Phoenixtec and our enhanced strategic
alliance with Nittan Valve Company in Japan.
	 We completed a $1.5 billion equity offering
in April as part of the financing of our acquisitions of Moeller and Phoenixtec, returning
our balance sheet to our targeted financing
ratios by years end.
	 Our operating earnings grew 7 percent to
a record $1.1 billion; operating earnings per
share dipped slightly to $6.83 as a result
of the April equity offering.
	 We generated $1.4 billion in cash from
operations.
	 We increased our dividend by 16 percent.
	 We delivered a 17 percent return on shareholders equity, again placing our return near
the top of diversified industrial companies.
	 Unfortunately, despite these outstanding
achievements, our total shareholder return
was significantly impacted during the year 
declining 47 percent compared to 37 percent
for the S&P 500, after entering 2008 at
near-record stock price levels.
Diversification is ON track
Business balance. Eaton continues to grow in
size, breadth and diversification. Approximately
three-quarters of Eatons sales and profits now
come from our Electrical, Hydraulics and Aerospace businesses, illustrating the dramatic
transformation of our business balance toward
faster-growing industries.
To continue to facilitate this growth, we reorganized our business on February 1, 2009 into
two sectors: Electrical, headed by new Vice
Chairman and Chief Operating Officer Tom Gross;
and Industrial (our Aerospace, Automotive,
Hydraulics and Truck businesses), headed by new
Vice Chairman and Chief Operating Officer
Craig Arnold. In addition, Richard Fearon has been
named to the new position of Vice Chairman
and Chief Financial and Planning Officer for
Eaton. To provide further transparency into the
performance of our businesses, we will begin
to break out our financial results into six segments in 2009: Electrical Americas, Electrical
Rest of World, Hydraulics, Aerospace, Truck
and Automotive.
Geographic balance. Eaton now does business
in more than 150 countries. Furthermore, 2008
was the first year in which 55 percent of our revenues were driven from economies outside of the
U.S.  another significant milestone in the
increasing globalization of our franchise. We are
also pleased that more than 20 percent of our revenues now come from the developing economies
of the world, where the growth in infrastructure
projects is particularly advantageous to Eaton.
Balance through the economic cycle. A year
ago, we noted that our early-, mid- and late-cycle
businesses each generated approximately onethird of our revenues. The benefits of this balance
throughout the entire economic cycle have never
been more important. Our late-cycle businesses
in Aerospace and portions of our Electrical business held up very well during 2008 and we expect
them to be strong in 2009. A number of earlycycle businesses such as our Truck and Automotive groups are currently experiencing
weakness but should see increased demand
for their technologies in the 2010  2011 time
frame. We also believe that as the global economy
emerges from this period of deep recession,
the demand for capital goods will increase,
since the current environment has artificially
depressed sales.
Building ON our strengths
Electrical. It has been a year of integration.
Our successful acquisitions of The Moeller Group
and Phoenixtec, combined with the earlier
acquisition of MGE Office Protection Systems,
make Eaton the worlds second-largest supplier of uninterruptible power systems (UPS)
and a global leader in power distribution equipment. Our industry-leading energy-efficient
products have earned the respect of many
global customers, leading to a number of new
strategic relationships. In October, we opened
a new R&D center in Suzhou, China, adding
to our global product development and support
capabilities in China and throughout the Asia
Pacific region.
Hydraulics. The acquisition of Integrated
Hydraulics Ltd. in the U.K. brings to Eaton
a greatly enhanced set of capabilities in cartridge
valves and hydraulic integrated circuits, one
of the fastest growing product segments in the
hydraulics market. It also expands our product
and applications offerings for European customers. Among significant wins: Transportation
and logistics leader UPS purchased new delivery
vehicles powered by Eatons new clean and
fuel-efficient hybrid hydraulic system.
Aerospace. Our fuel, hydraulics and pneumatic
fluids capabilities are delivering real value and
key customer wins in 2008, including the new
Sikorsky CH-53K military heavy-lift helicopter
and the Rolls-Royce Trent XWB.
 Many of the
new programs awarded in past years have now
completed development and are in the ramp-up
phase of production.
Truck. While continuing our leadership in traditional heavy-duty and medium-duty truck
applications, we notched significant new wins
with our industry-leading hybrid electric powertrain systems in the U.S., China and Europe.
In India, we have been awarded the new Tata
world truck program with a projection of more
than $450 million of volume over the next five
years. Globally, major light transmission customers have come on board with significant new
business commitments using Eatons technology
to serve emerging countries.
Automotive. While fuel prices continue to
oscillate broadly, the long-term trend for higher
energy costs has led global automotive manufacturers to select Eaton for its leadership in
engine air management. Our innovative nextgeneration superchargers and patented valve
actuation technologies continue to be the first
choice of major automotive brands. During
the year, we also enhanced our partnership with
Nittan Valve Company, significantly expanding
our ability to serve leading Japanese and Korean
global customers.
Capitalizing ON our values
Through this period of tumultuous change,
our commitment to being a values-based organization has remained constant. In fact, this
strong foundation has continued to provide
a steady source of strength and confidence that
has so long differentiated Eaton. While the
behavior of several high-profile organizations
has demonstrated the hazards of adhering
to a different set of ethical standards, we know
that Doing Business Right pays dividends
both in the short term and the long term. It is
why so many people have chosen to make
Eaton their chosen employer and supplier. We
have maintained our commitments to our
customers, our suppliers, our employees and
the communities with whom we are fortunate
to do business. To each, we owe our thanks.
We enter 2009 respecting its challenges. We
believe economies in the U.S. and Europe will
continue to decline and it will be a number
of quarters before we are likely to see our end
markets stabilize and begin to resurge. As a
result, we have resized our company  lowering
overall expense and investment levels  and
focused our efforts on activities and opportunities that are likely to create real value in a
business environment that will be remarkably
different than the last five years. Certainly
one of the most painful elements of this resizing
has been the need to reduce our global fulltime employment by nearly 9,000 people. These
Eaton associates made lasting contributions
to our enterprise  and I would once again like
to thank them for their contributions.
As economic conditions deteriorate around
the world, companies must be careful to avoid
becoming inwardly focused and indecisive,
actually contributing to the downward spiral.
At Eaton, we remain focused instead on implementing the important elements of our power
management company strategy. Under virtually
any scenario, we believe the sources of energy
and the increased legislation surrounding carbon
usage will increase the cost of the energy that
powers our homes, businesses and communities.
Only through the effective combination of
the types of power management technologies
that Eaton delivers today  and that we are
designing for tomorrow  can the negative
economic impact of the dual drivers of higher
energy costs and a carbon-constrained world
be contained. In this environment, our opportunities are not reduced; in fact, they are
measurably enhanced.
For all these reasons, Eaton remains always
ON  on our strategy, on in terms of delivering
real value for our customers, on in providing a
compelling and rewarding workplace, on in living
our values-based culture and on in recognizing
our accountability for delivering premium shareholder return throughout the economic cycle.
On behalf of our entire Eaton team, thank you
for your continued support.
Alexander M. Cutler
Chairman and Chief Executive Officer