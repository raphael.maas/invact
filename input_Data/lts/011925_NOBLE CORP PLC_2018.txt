To Our Shareholders

The oshore drilling industry continues to await
robust recovery. However, despite the various
hurdles that the industry may face, at Noble, we
still saw encouraging signs of improvement. 

There is little doubt that weakness in crude oil and natural gas prices and the
recent interruption in sustainable trading patterns of these commodities have led
to our customers demonstrating a disciplined approach to the business, including a
commitment to spend within the limits of a restricted amount of annual cash flow and
allocation of capital that has frequently subordinated oshore programs.
However, recent activities by our customers imply that interest in oshore
prospects is once again emerging. The resource potential found in many of the world�s
oshore basins has continued to expand, while our customers� cost per barrel of crude
oil equivalent to access this growing potential has seldom been lower. At the same
time, our industry is experiencing important and much needed fundamental change,
and we believe the lack of recent meaningful investment in oshore production will
be a catalyst in an improving market for our services. Together, these important
developments should set the stage for a robust business environment, and we certainly
experienced encouraging signs of an improving market in 2018.
Indications of Growing Oshore Interest
Customer interest in emerging oshore basins is rapidly building due, in part, to
improving access and successful exploration results. Guyana, Brazil, Suriname and
Mexico oer excellent recent examples of basins where exploration and production
companies have gained access to expansive oshore acreage while the resource potential
of these areas is increasingly supported by encouraging exploration results. In 2018,
five of the 18 announced deepwater discoveries worldwide were located in these four
basins. Oshore Guyana, where exceptional resource potential is particularly evident,
12 out of 13 exploration probes to date have located hydrocarbons. It is worth noting
that Noble is a leading provider of drilling services in this region with two of our ultradeepwater drillships located there. We expect exploration in these areas, and in others
around the world, to experience gradual improvement as operators identify oshore
prospects with excellent resource potential that can support future production growth
and reserve replacement. 

Growing interest from operators is not limited to the emerging frontier regions.
Activity in mature oshore basins is also expected to expand due to a combination of
changes in ownership of producing and undeveloped acreage, exploration success, and
the presence of established oshore infrastructure that provides a quick and e?cient
path to commercialization. The North Sea has been a prime example of activity
supported by new owners in mature basins.
Finally, exploration and production companies are experiencing increased success
in utilizing technological advances that lower the cost of doing business oshore
while delivering improved results in exploration and field recovery. Enhanced seismic
acquisition and expedited imaging technologies are also creating value for our
customers and should lead to increased oshore activity.
Improvement in Industry Fundamentals
In addition to the mounting evidence of growing oshore interest from operators,
2018 saw a healthy increase in bidding and award activity, including a welcomed
improvement in the floating rig category. Jackup demand has continued to improve,
particularly for high specification units which has led to improving margins for those
assets.
Also, the industry�s total rig supply declined further in 2018 as more rigs were
retired from service. Among the industry�s floating rig fleet, 18 units were retired in
2018, bringing the total number of floating rig retirements since 2014 to 126 units,
with additional retirements likely in the future. Noble has been an active participant
in fleet rationalization, having retired approximately one-third of our fleet since late
2014. These rig retirements, combined with the addition of new, high specification
assets, have resulted in one of the industry�s youngest, most modern fleets. Noble�s
average fleet age is only six years as we begin 2019.
Positioning Noble for Industry Recovery
We believe the early stages of recovery in the oshore drilling industry have begun.
As a result of this outlook, we have taken several actions in 2018 and into 2019 to
strengthen our customer base and further improve our ability to compete in what will
remain a competitive industry environment. These actions were led by our acquisition
of two newbuild jackups. The attractively priced rigs, the Noble Johnny Whitstine and
Noble Joe Knight, have both received multi-year contracts for operations in the Middle
East. These modern, fit-for-purpose drilling solutions are expected to remain active in
the region well into the future. 

Also, in keeping with our improved industry outlook,
we completed reactivation programs for five premium
rigs, including four ultra-deepwater floating units and
a high-specification jackup. Recognizing our customers�
preference for premium rigs with proven performance
records, we moved decisively to position each unit to
compete for an expanding list of contract opportunities in
2018 and beyond. Our intended objective was achieved, as
all of the reactivation projects were executed successfully,
with each of the five units securing contracts. Even better,
new contract opportunities are now visible, increasing the
likelihood that each of these rigs will remain contractually
engaged well into or beyond 2019, as demonstrated by the
recent contract award for the drillship Noble Tom Madden
in Guyana.
I also believe eorts to focus our fleet in key geographies
have advantageously positioned our premium jackup and
floating rigs for a growing number of contract opportunities
as customer demand strengthens. In our jackup fleet, 11 of
our 13 units are, or will soon be, located in the North Sea
and Middle East regions where numerous customer needs
are outstanding, including an estimated 50 rig-years of
customer demand in the Middle East.
Our marketed premium floating fleet is concentrated in the Western Hemisphere
where enormous resource potential is driving heightened ultra-deepwater exploration
interest and multi-year development opportunities. We have already established
ourselves as a leading service provider in some regions, such as Guyana, and we
have an established and successful history in others, such as the U.S. Gulf of Mexico,
Suriname, Mexico and Brazil, attractive locations to which we maintain prompt access. 

In 2018, for the second consecutive year, we established record results in fleet
uptime and safety performance as measured by our total recordable incident rate.
More impressively, these important measures of operational execution were achieved
during a year in which we increased our active fleet by four rigs and expanded our
headcount by 10 percent. Safety performance and operational excellence have long
been hallmarks at Noble and will continue to contribute to our success going forward.
Conclusion
We believe a prolonged period of under-investment by exploration and production
companies over recent years will provide the eventual catalyst for increased oshore
rig demand. Recent actions by our customers are encouraging and should eventually
support heightened oshore activities. As we enter 2019, Noble is better positioned
to capture this expected growth in customer activity, and we remain committed to
maintaining our foundation and culture of safe, environmentally respectful and
e?cient service delivery through the cycle.
Thank you for your continued interest in Noble. I extend my gratitude on behalf
of our Board of Directors to the entire Noble team for their exemplary hard work and
dedication to pursuing excellence at all times. I look forward to success in the New Year
as we extend the achievements of 2018.
Julie J. Robertson
Chairman, President &
Chief Executive O?cer 