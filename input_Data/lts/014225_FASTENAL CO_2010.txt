
                      I started my 2009 president's letter by explaining                                 opportunity to rebound in 2010 and recoup a good


                      that, while we may not have produced great                                         part of the bonus compensation lost due to the eco-

                      numbers, I believed we had done many things that                                   nomic slowdown. As I mentioned earlier, our incentive

                      positioned us for strong results in the future. Our                                pay did recover in 2010. In fact, based on our sales growth,

                      team has proven that in 2010. Although the year                                    expense control, and earnings growth, the commission and

                      began with very low sales growth, we had good                                      bonus formulas calculated in 2010 produced record commis-


                      momentum. By May, our annual daily sales growth                                    sions and bonuses for most areas of the company. Our calculation

                      exceeded 20%, and our daily sales growth aver-                                     for the company's 2010 401(k) profit-sharing contribution, which is

aged 21.7% over the last eight months of 2010 to end the year with                                       based on achieving certain profitability goals, approached our record
$2.27 billion in total revenue, an increase of 17.6% over 2009. We are                                   payout in 2008. I am proud that we were able to keep our promise to all


very focused on carrying this momentum into 2011.                                                        of the hard-working people who stuck with us in 2009 and made 2010
                                                                                                         such a good year in so many ways.
Our net earnings for 2010 were $265.4 million, versus $184.4 million
in 2009, an increase of 43.9%. This improvement was made possi-         As we returned to growth, we were able to make good progress with
ble by a combination of sales growth, expense control, and margin       our `pathway to profit' strategic growth plan. Introduced in 2007, this
improvement. Our gross margin for 2010 was 51.8%, compared to           plan calls for us to continue to add stores, but at a slower pace, and to
50.9% for 2009  a solid gain but one I believe we can improve upon.    use the savings to hire additional sales people in the stores. Through
This will be a high priority for our company in the year ahead.         this balanced approach, we originally projected that we could grow
                                                                        our average monthly store sales from $80,000 to $125,000 over a
Overall, our expenses grew just 8.5% (versus 17.6%                                        five-year period from 2007 to 2012, in turn increas-
sales growth), but that number doesn't tell the complete                                  ing our pre-tax earnings one percentage point each
                                                            "Our net earnings
story. Non-payroll related expenses were down 1.8%                                        year, from 18 to 23% of sales, by 2012. A year ago,
                                                          for 2010 were $265.4 in the midst of the recession, it was estimated that
from 2009, reflecting a lot of smart decision making
across the company which allowed us to hit our goals million, versus $184.4 an additional 2430 months would be required to
while managing expenses. As you read through this                                         achieve our 23% pre-tax earnings goal, which would
                                                            million in 2009, an
letter, you'll come across numerous examples of our                                       have extended the `pathway to profit' timeline from
                                                           increase of 43.9%."
people's ability to accomplish more with less. Although                                   2012 to almost 2015. However, after analyzing our
our payroll and related expenses grew 14.6%, more in                                      performance over the past year, we now believe our
line with our sales growth, this is directly related to the way our     23% pre-tax earnings goal can be achieved with an average monthly
incentive pay is structured. We've worked hard to develop a program     store size of $100,000$110,000, which we anticipate will be achieved
that pays well when we perform well.                                    in 2013.

I'd like to share a little background about our employee incentives.                                     Because of the financial stress many companies were experiencing at
Early in 2009, we spent a great deal of time communicating with our                                      the beginning of the year, I had been concerned about higher bad debt
employees about how tough the year would likely be, and what they                                        risk and lengthened days sales outstanding. I am pleased to say that we
should plan for from a pay and benefits standpoint. We expressed our                                     actually saw lower days sales outstanding and write-offs, thanks in large
commitment to minimize headcount reductions, and shared our belief                                       part to the hard work of our accounts receivable team and their increased
                          
that if we worked hard and made good decisions, we had a realistic                                       utilization of electronic billing, which shortens the payment cycle.              

                                                                                                     EVOLVING TO MEET THE NEEDS OF OUR GLOBAL CUSTOMERS
                                                           In keeping with company tradition, our new manufacturing facility in Malaysia was created to meet a specific customer need 
                                                                                                     in this case, for specialty bolts and studs used in oil and gas industry applications.
                                                                                                                                                                                        
                                                                                                                                                                          D
                    It wasn't the only area of our business that played       high-quality customer into the program, creating new local opportunities
                     out a little differently than I had expected. When the      for our stores. In addition to adding new customers, this team continued
                  year began, I predicted our inventory would grow very          to grow our partnerships with our largest accounts and take our
               slowly throughout 2010. But as the year progressed, our           relationships with customers of all sizes to the next level. I'm not alone
             inventory growth increased, and we ended the year with              in my belief that this is the best national accounts group we've ever had,
          $557.4 million in inventory, an increase of 9.6% over the              and I expect continued success with this important area of our business
        ending number in 2009. Although our inventory grew at a                  moving forward.
      slower rate than sales, I believe we will see greater improvement in
    inventory utilization in the coming year.                                              As part of our initiative to grow our sales to federal,
                                                                                           state, and local government entities, we created a new
                                                              "I believe that 2010
At the beginning of 2010, our focus was on position-                                       position  vice president of government sales. His
                                                              will go down as the
ing the company for renewed sales growth, and yet                                          responsibility is to first learn and understand the
we knew it was important to be cautious with expenses                                      various purchasing systems used at each level of
                                                               year our Fastenal
because of the uncertain economic picture. With this in                                    government, and then formulate and implement a plan
                                                               Automated Supply
mind, we sought to identify investment strategies that                                     to grow this business. Our initial focus has been on
                                                              Technology (FAST)
would produce the best growth at the lowest possible                                       state and local government, and we've had some very
                                                            program really gained good success. Much of the purchasing spend for these
cost. This included developing a plan that would allow
us to start out the year opening stores at a lower rate     traction and took off." government entities is driven through purchasing
and adjust the rate as the year progressed. Using this                                     cooperatives, which work with several states to leverage
plan, we opened 45 stores in the first half of the year                                    their buying power and help smaller entities save
and 82 in the second half, an overall increase of 5.4% over 2009.         money on needed supplies. Our team spent the year meeting with
                                                                          numerous state purchasing agencies and several cooperatives. They've
In an effort to drive growth faster with a smaller investment in          won several contracts, and more are pending as this letter goes to
personnel, we made a decision to hire fewer store-based sales             print. We feel very good about these opportunities because we con-
people and instead add more sales specialists focused on specific         tinue to hear that, if given a choice, state and local governments would
opportunities. After reviewing many different growth and market           prefer to buy locally  and none of our competitors can match our
opportunities, this investment was primarily focused in four basic        local presence.
areas: national accounts sales specialists, government sales specialists,
manufacturing sales specialists, and specialists to demonstrate and sell  I believe 2010 will go down as the year our Fastenal Automated
our Fastenal Automated Supply Technology (vending solutions - see         Supply Technology (FAST) program really gained traction and took
photos below). In all, more than 150 sales specialists were added in      off. Our flagship FAST solution is the FAST 5000 industrial vending
these four areas during 2010, providing our stores and customers with     program. From the customer's perspective, it's like having a 24/7
a new level of expert support in the field. Based on the early results,   Fastenal store within their facility, custom-stocked with needed items
we've decided to increase our investment in sales specialists in all of   and continually replenished by local Fastenal personnel. This solution
these areas in 2011.                                                      provides many benefits for the customer  reduced inventory,
                                                                          automated ordering, improved productivity  but probably the biggest
It was a strong year for our national accounts program, which focuses     advantage is that it makes it easy to track usage and control wasteful
 on providing value and solutions to large corporate customers.           consumption. The program was designed to be extremely cost-effective
   Sales through this program grew at a rate faster than company-wide     for customers, in turn creating new opportunities for our stores; and in
    sales, and it seemed like every time I talked to one of our national  fact we're now seeing exceptional growth at customer sites where FAST
      accounts people during the year, they had just brought another      5000 machines are placed. In my opinion, this will be a big part of the



future of industrial supply, and it will become an increasingly important                Another area that provided excellent growth was our
part of our business in 2011 and beyond.                                                 international business. We are now located in nine
                                                                                         countries outside of the United States, and as a group
We continued to make progress with fastenal.com, which seems to                          our international locations saw their business grow by
improve every day. The web development team has designed the site                        35% over 2009, generating more than $200 million. We
to be very user-friendly and fast, with an intuitive layout that makes                   opened in two countries during 2010, Panama and the United
it easy to drill down and find everything. I encourage you to go                         Kingdom. We're growing nicely in all of the countries we're located
online and try it out for yourself; I think you'll be impressed. Internally,             in, and profit growth is outpacing sales growth. To me, this is a good
we view our web site not as a separate business but rather as                            indication that our people know how to operate in these markets,
another way for customers to place orders with their local Fastenal                      and that future prospects are very promising. The people who lead
stores. We're one of very few companies that can receive a web order                     these businesses spent the year not only producing great results but
from a customer and have it ready for local pick-up or delivery in a                     also traveling to identify other countries that will support the Fastenal
matter of minutes. For the customer, this is like having the best of both                business model in the future. Based on their research, our plan is to
worlds  the convenience of electronic ordering, along with a local sales                open in more countries in 2011.
representative who's right there to help out if something goes wrong
or the order needs to be changed. I believe that as we introduce this        For the people in our distribution centers, 2010 was a year of fine
service to more customers, we will continue to see a steady migration        tuning. We didn't start any major projects or move any facilities, but
to fastenal.com.                                                                                 it was nonetheless a busy year of improve-
                                                                                                 ment and innovation. Much of the focus was
                                                                "We're one of very few
Our manufacturing division had a busy year. At                                                   on developing procedures that would allow our
                                                              companies that can receive
the end of 2009, we completed the purchase                                                       distribution personnel to provide error-free service,
                                                             a web order from a customer even earlier in the day. They found that by stream-
of Holo-Krome from the Danaher Corporation.
                                                               and have it ready for local
Located in West Hartford, Connecticut since                                                      lining processes they were able to provide better
1929, Holo-Krome is a leading producer of socket                                                 service at a lower cost. For the year, our distribution
                                                                pick-up or delivery in a
head products. This acquisition greatly increased                                                expense grew by only 3.7% while supporting sales
                                                                  matter of minutes."
our capacity to produce high-quality domestic                                                    growth of 17.6%.
fasteners, and the transition has gone very well.
During the year, we purchased a new facility for Holo-Krome located in                   Improving service while controlling expenses was also the theme of our
Wallingford, Connecticut, within 20 miles of Hartford. The new building                  transportation team. In 2009, we had to cut back on some routes and
is 187,000 square feet and is set on a 51-acre lot, giving us plenty of                  reduce service in some cases. This year we were able to reintroduce
room to grow our Holo-Krome business in the future.                                      some of this service, but the routing group worked hard to accomplish
                                                                                         this while still reducing expenses. Because of the hard work of
Our entire manufacturing business now consists of eight locations                        our transportation managers and our drivers, we had the lowest
totaling over 532,000 square feet and employing 468 people at year's                     transportation costs as compared to revenue in the company's history.
end. This team grew their business by 29% in 2010, contributing $91
million in revenue for the year. Along the way, they produced more than                  Our purchasing and product development team spent much of the year
1,000 orders per week, many of them manufactured and shipped in less                     working with suppliers to streamline the flow of product. They have
than 24 hours to meet the needs of our customers. With the additional                    completed, and are now testing, a new information system that was
equipment we added in 2010, we can now produce fasteners from #0                         designed to save thousands of hours during the receiving process. This
to 4 inches in diameter, giving us even more opportunities to `make the                  system also provides much more accurate information regarding the
unavailable part available.'                                                             location of inbound product, allowing us to improve service


                                                     
               to our stores and, in turn, our customers. Our FASTCO                                 than 45,000 FSB courses (nearly 40,000 online) during the year  up
             trading company, located in Shanghai, China, continued                                  from 7,343 in 2008, the year prior to the LearnCenter's launch. In 2011,
           to focus on identifying and building strong relationships                                 we anticipate that employees will participate in more than 60,000 FSB
         with manufacturers throughout the world that are capable of                                 courses  through the LearnCenter, but also via WebEx and in traditional
       producing high-quality product at a competitive price. During                                 classroom and shop settings  as they work to improve their product
      2010, FASTCO sourced more than 5,060 containers of product, an                                 knowledge and service skills.
    increase of 81% over 2009.
                                                                       At FSB, they're always striving to identify the most important things to
 Some of the most important leaders in our company are our district    teach to our employees. As part of this effort, they created a customer
sales managers. On average, district managers manage twelve stores,    research group in late 2009. This group interviewed nearly 20,000
although this number varies depending on experience                                    customers over the past twelve months, gaining valu-
and geography. An experienced district manager can                                     able insight into how our customers view our service,
                                                          During 2010, FASTCO our strengths, as well as some areas where we need to
manage up to 20 stores, and the larger districts generate
revenue exceeding $3 million per month. In 2010, our sourced and purchased improve. Based on this research, the team worked with
district managers continued to focus on improving                                      FSB to create a list of customer service standards. They
                                                            more than 5,060
their underperforming stores, using our internal                                       took the initiative to turn this list into an easy-to-read
                                                          containers of product, book, a picture of which is featured on the back of this
benchmarking data to identify areas of opportunity in
                                                           an increase of 81%
every store, then working with store personnel to make                                 report, then they had it printed and distributed a copy in
                                                               over 2009.
necessary changes. This group has done a very good                                     the Christmas goody box we send to every employee.
job of improving the overall performance of our stores,                                To me, this project exemplifies the Fastenal culture of
and it shows in the numbers we report.                                                 entrepreneurship and problem solving.

Because of the wages lost during the business slowdown in 2009, it                                   I want to thank all of our employees for the great year we had
was more important than ever to provide meaningful benefits at an                                    in 2010. I also want to express to all of our stakeholders that I
affordable price to employees. During the year, our benefits team spent                              am fully committed to fostering an entrepreneurial culture that
a great deal of time working with our health providers to control the cost                           encourages our employees to make good decisions that benefit
we pay for health care. Thanks to their efforts, we were able to make                                themselves, our customers, and our shareholders  while creating
improvements to our already high-quality health package, while still                                 greater opportunities for everyone involved. Here's to another year of
keeping it affordable for our people to provide coverage for themselves                              Growth Though Customer Service.
and their families. This same team also led the implementation of our
new hiring system, which is designed to improve the process by bet-
ter screening and qualifying potential candidates, while also automating
the record keeping required by law. This change required a great deal                                Willard D. Oberton
of work up front, but the early information we're seeing with regards to                             President & CEO
employee retention looks very promising.

  The Fastenal School of Business (FSB) had a breakthrough year with
   our new online LearnCenter. In all, employees participated in more

