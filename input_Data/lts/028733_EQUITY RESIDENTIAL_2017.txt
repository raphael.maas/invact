Dear Fellow Shareholders:
A growing economy, resulting in record low unemployment and rising wages, along with
extremely favorable demographics continue to provide strong and steady demand for rental
housing across our markets. Yet we began 2017 knowing that elevated levels of new apartment
supply would impact rental rates and produce revenue and earnings growth below that
delivered in the prior year. Therefore, we were pleased that strong occupancy, record number of
residents choosing to renew their leases with us and favorable renewal pricing allowed us to
achieve our most optimistic expectations for annual growth in same store revenue, net
operating income and Normalized Funds from Operations.
THE COMPANY WE HAVE BUILT
In 2017, we continued our long and successful track record of creating value through focused
and proactive revenue generation, expense management and strategic capital allocation while
maintaining a conservative balance sheet that enhances returns and value creation while
maintaining capacity to take advantage of the next opportunity. We believe that our
stakeholders value stability, liquidity, predictability and accountability and that is the mission to
which we remain unwaveringly committed.
We have focused our portfolio on the coastal, gateway markets of Boston, New York,
Washington, DC, Southern California, San Francisco and Seattle where our country�s knowledgebased
economy will continue to grow and create well-paying jobs that attract large segments of
the population opting for high-quality, well-located rental housing. These markets continue to
see incredible growth in their city centers and in the downtowns of their close-in suburban
submarkets as people of all ages embrace the lifestyle provided by high-density living. And, as
these cities have the highest cost of single family home ownership, there are growing
populations of permanent renters that ensure high occupancy, low turnover and strong
renewals in the markets where we have invested your capital.

THE DRIVERS OF DEMAND IN OUR MARKETS
The apartment business benefits from the continuing growth in demand for rental housing and,
in particular, for the high-quality, well-located assets that we own and manage. Demand for
rental housing is driven primarily by household formations from the Millennial segment of our
population, also known as the Echo Boom Generation. These young adults, born between 1981
and 2000, total approximately 78 million people and comprise the largest segment of the US
population. Millennials are disproportionally renters and the impact they are having and will
continue to have on rental housing cannot be overstated. We will continue to see strong
demand from this group as the largest sub-segment of this cohort is just 27 years of age while
the median age of our resident is 33 years old. And right behind the Millennials is Generation Z,
another cohort of future renters comprised of more than 70 million people born after 2000.
Equity Residential is extremely well positioned to benefit for many years to come as a result of
the huge impact these generations will have on rental housing.

Young adults are attracted to rental housing for the flexibility it offers as they pursue their
careers and life interests and are attracted to Equity Residential because of the quality of our
assets and the locations that we offer. We are pleased that they stay with us because of our
outstanding property staff and the remarkable services they provide.

But Millennials will also stay with us longer as they delay the life choices that most commonly
drive the decision to buy a home. These young adults are marrying later and having children later
in life and are therefore opting to stay in rental housing significantly longer than past generations.

For many households, remaining in rental housing is a lifestyle choice�a desire to live in highdensity,
urban locations in close proximity to work, friends and favorite activities. For others, it is
an economic reality and particularly so in our markets that have median single family housing
costs well above the national average. This makes it far less likely that our residents could afford
a single family home if, in fact, they wished to make that lifestyle decision.

The primary reason people buy a single family home is a lifestyle choice, one most often made
when having children and looking for a place to raise their families. So it comes as no surprise
that the great majority of first time home buyers are couples with children. We see little risk to
our resident base from an increase in demand for a single family home lifestyle because, across
our portfolio, only 8% of our units are occupied by residents who fit this profile. More
importantly, more than 40% of our units are occupied by a single resident, a demographic that
has an extremely low propensity to purchase a home. In fact, we saw less than 6% of our
residents leave us to buy a home in 2017, a level consistent with that experienced in each of the
last five years despite a significantly improving economy.
There is absolutely no denying that Millennials are an important part of today�s renting
demographic, but they are not the whole story. Currently, more than 18% of our units are
occupied by people 50 years and older. Many demographers expect the share of aging Baby
Boomer rental households to grow as they shift away from home ownership and, like
Millennials, seek the simplicity, flexibility and overall lifestyle offered by rental apartments. In
fact, people of all ages and walks of life are participating in the re-urbanization of our nation as
they are attracted to high-density living in the urban core.
As a result, over the last decade we have put great focus on Walk Scores (www.walkscore.com)
when making our investment decisions. These third-party �scores� rank a specific location based
on its proximity to neighborhood restaurants, coffee shops, grocery stores, culture, education,
parks, public transportation and other goods and services that are important to today�s
apartment resident, regardless of age. In every market in which we operate, our average Walk
Score is significantly higher than that of our public market competitors.
The demographic picture across our nation is a powerful generator of current and future
demand for rental housing. And, as much of this demand will be in our high-density, coastal
cities, we believe we are extraordinarily well positioned and have a long run of strong growth
yet ahead of us.

Nevertheless, 2018 will be another year of elevated supply in our markets, particularly in
New York City. Historically, the markets in which we operate annually deliver new apartment
units equal to approximately one percent of existing apartment inventory but we are currently
seeing higher than average deliveries this year. Rental apartments continue to be one of the
most desirable sectors for institutional real estate investors so it�s no surprise that developers
have kept building. However, we believe that 2018 will see peak supplies in most of our markets
and that new supply will decrease in 2019 and considerably so in New York City. The decline in
new deliveries is the inevitable result of high land prices, rising construction costs and slowing
rental growth that shrink returns on development costs to unacceptable levels.
CAPITAL ALLOCATION AND PORTFOLIO TRANSFORMATION
Over the last decade, we have successfully transformed our apartment portfolio from one that
was comprised primarily of garden-style properties in suburban locations across more than 50
US apartment markets. Today, we are highly focused on more urban and close-in suburban
locations in the six gateway coastal markets of Boston, New York, Washington, DC, Southern
California, San Francisco and Seattle. As part of this process, in just the last three years we have
sold more than 32,000 apartment units for approximately $7.7 billion, generating annual returns
of 12% and $4.2 billion in special dividends paid to our shareholders.
Our transformed portfolio is now entirely in markets expected to achieve high occupancy and
produce the best long term rent growth.
The heavy lifting in our transformation is complete and our focus now is on managing the
portfolio to maximize long-term total returns by recycling capital from lower growth assets into
those with higher growth potential. In 2017, we saw few opportunities to add new assets to our
portfolio at attractive prices and acquired only four properties, consisting of 947 apartment
units, for approximately $468 million. In 2018, we expect a similar level of acquisition activity
which, like last year, will be funded with proceeds from the disposition of properties expected to
underperform.
We focus our development activity in the urban core and our close in, highly walkable suburban
sub-markets. Development is an excellent opportunity to create long term value by building
new streams of income where it is either difficult to acquire existing ones or where the costs to
do so represent an unreasonable premium to development returns. We currently have seven
projects under construction or in lease-up, which we are developing at a total cost of nearly
$1 billion and which we expect to yield nearly 6% on costs when fully stabilized. In 2017, we
stabilized five new developments�in Los Angeles, Orange County and San Francisco�at a
weighted average yield of 5.7% on $983 million of development costs. These assets would sell at
yields in the high 3% to low 4% range, reflecting the significant value created by our
development business in this cycle.
However, like acquisitions, over the past several years we have seen few new development
opportunities that make sense due to high land prices and increasing construction costs. As a
result, it has been several years since we added new sites to our land inventory for future
development. In 2018, we do, however, plan to begin construction on one new development
project in Boston that we have been working on for nearly a decade. When completed in 2021,
this $410 million project containing 469 apartment units will be New England�s tallest residential
rental tower.
As we experience great rental demand across our portfolio of well-located assets, we continue
to see unit renovations as an area of significant value creation. This year, we anticipate spending
more than $60 million renovating kitchens and baths in 4,500 apartment units. This activity
delivers low to mid-teen returns on capital and keeps our assets fresh. We also plan to spend
several million dollars on customer facing renovation projects in our common areas � like club
rooms, lobbies and gyms � in order to ensure that our properties remain competitive with newly
delivered assets.
FOCUS ON OUR CUSTOMER
New apartment supply means more choices for our customers so keeping our residents happy
and in place is more important than ever. In 2017, our relentless focus on our customers�
experience led to the highest retention rate in our history and renewal increases averaging
4.6%. This company- wide priority on the customer, led by our incredibly talented team of hard
working property management professionals, produced a remarkable 11% increase in our
Customer Loyalty Scores in 2017. The entire company will work hard again this year, resident by
resident, lease by lease, to deliver on our commitments to our customers.
The business of renting a place to live has been around since the dawn of time but the
technology driving the business today continues to expand at a rapid pace. We continue to be
at the forefront of harnessing advances in technology that allow us to serve our customers
better and deliver them the optimal customer experience.
Across our enterprise, we drive our state of the art, best in class, operating platform to create the
performance that you have come to expect from Equity Residential. We maximize revenue
through the utilization of our customized pricing system that uses proprietary data on current
and projected demand and unit availability to create both current and future pricing every day
for every unit we manage. We use a standardized, centralized purchasing system to control our
operating expenses and a business intelligence platform that allows all our team members to
quickly identify and address trends and opportunities.
The �internet of things� describes the connectivity of all the systems that exist in a place like our
properties�from entry systems to security and lighting systems to the wi-fi in our resident
lounges, roof tops, meeting rooms and other amenity areas. Technology advances at a
tremendously rapid rate and our residents expect us to keep pace. We are committed to making
the investments necessary to enhance our properties and systems to meet our customers�
rapidly changing expectations. In 2017, we upgraded high-band width internet speed and
performance across most of our portfolio and will complete this process during 2018.
We remain constantly focused on �what�s next� because every day new technology is being
created that will impact how we build and operate our apartment properties, communicate
with our residents and help meet their needs. We are committed to staying on top of these
changes and will pursue every opportunity to enhance our customers� experience and improve
in every aspect of our business. This commitment has led us to recently make modest
investments in two venture capital funds focused on early stage real estate tech opportunities.
Through these relationships, we gain valuable insights into numerous emerging real estate
technology platforms, many of which are applicable to our operations and some of which we
are already testing.
FINANCIAL STABILITY
Having a prudent capital structure through operating cycles and transformative events enables
financial flexibility, better access to capital and reduced interest rate risk. We are focused on
having a strong, conservative and flexible balance sheet which preserves shareholder value
during capital market dislocations and provides us with the capacity to take advantage of
opportunities when others are capital constrained.
Real estate is a capital intensive business so we maintain access to multiple sources of capital.
Short term liquidity of $2.0 billion is provided by our unsecured commercial paper program and
unsecured revolving credit agreement. Longer term, we can borrow on a secured basis at
attractive rates from Fannie Mae and Freddie Mac as well as numerous life insurance companies.
We also have excellent access to the public bond market. We were pleased that recent upgrades
brought our senior unsecured debt ratings to A-/A3/A by S&P, Moody�s and Fitch respectively,
among the highest long-term credit ratings in the public real estate sector.
These ratings give us immediate and regular access to the unsecured bond market, which we
have been using to lock in historically low interest rates and extend our weighted average
maturity from six years in 2013 to approximately nine years today. For example, in February
2018 we issued a $500 million, 10-year unsecured bond at 3.5%, representing the lowest credit
spread (80 basis points) of any 10-year REIT benchmark offering in history. We were pleased to
use the proceeds to retire a $550 million, 6.08% mortgage loan and significantly reduce our
interest cost and extend our duration. We are excited to have additional opportunities to
refinance nearly $1.0 billion of above market indebtedness in 2019 and 2020 and further reduce
our interest expense.
ENVIRONMENTAL SOCIAL AND GOVERNANCE ISSUES
We are committed to the incorporation of Environmental, Social and Governance (ESG)
concepts in all aspects of our business. We continue to improve and enhance our policies and
practices on issues from sustainability to employee and community engagement to board
composition and governance policies and we continue to be recognized for our work in these
areas.
As an industry leader, we are committed to creating and maintaining sustainable communities.
For us, sustainability thrives at the intersection of people, planet and profit. We are proud that
our recent developments meet sustainability standards set by the US Green Building Council or
the NAHB Green Building Program.
Our portfolio of over 300 properties gives us frequent opportunities to invest in projects that
improve the long-term sustainability of our assets while generating annual returns of more than
20%. We have completed lighting and water conservation projects across the great majority of
our portfolio and we have installed clean, on-site, power generation at 25 of our existing assets.
Our focus on conservation has driven reductions of over 10% in both energy and water use
across our portfolio over the past five years. The more than 350 LED lighting projects we have
completed at our communities in the last 5 years have reduced our annual electricity use by
35,000,000 kWh.
We were proud to be recognized as the 2017 Global Residential Listed Sector Leader by the
Global Real Estate Sustainability Benchmark (GRESB) survey, a globally recognized analysis of
the sustainability indicators of more than 800 real estate portfolios worldwide. This was the
fourth consecutive year that we were recognized as a sector leader by GRESB. In addition, we
were the recipient of the 2017 Residential Leader in the Light award for sustainability from the
National Association of Real Estate Investment Trusts, our industry association.
We have a dedicated team of professionals across our entire enterprise. Their commitment to
their customers, their colleagues and their communities is unmatched. In addition to providing
remarkable customer service to our residents, our team members spend thousands of hours
volunteering in the communities that they and our customers call home. Mentoring,
participating in fundraising events, cleaning up local parks and running food drives are just
some of the many activities in which they are engaged. Each year a rotating committee of
employees from across our enterprise, our Equity Values Council, drives our initiatives in
community service, employee wellness, diversity and inclusion. The Council has had a
tremendous impact on our ESG efforts and will propel us forward again in 2018.
(Equity Residential employees volunteering at the Greater Chicago Food Depository,
November 2017)
Our Board has implemented many corporate governance measures over the years designed to
serve the interests of our shareholders and further align the interests of trustees and
management with those of our shareholders.
In late 2017, in response to the recommendations of our shareholders, the Board amended the
Company�s Bylaws to add a provision which provides for the right of shareholders to amend the
Bylaws. This right had previously been reserved, by Maryland law, for the Board of Trustees.
We are proud to have been recognized by the Women�s Forum of New York as a Corporate
Champion for accelerating gender parity in the boardroom with female representation of more
than 25% on our Board of Trustees.
Our organization is only as good as it is because of terrific people. One of the best is our long
time General Counsel, Bruce Strohm, who retired at the end of 2017 after 30 years at Equity
Residential and its predecessor company. Bruce has been an integral part of every acquisition,
every merger, every Board meeting and every big decision we�ve made starting with our IPO in
1993. He is a highly valued leader, counselor and friend and we cannot thank him enough for all
that he has done to contribute to Equity Residential�s success over his remarkable career. With
transition comes opportunity and we are pleased that, as part of a well thought out
commitment to succession planning, Scott Fenster, a 15-year veteran of our legal department,
became our new General Counsel on January 1, 2018.
We are also incredibly fortunate to have an exceptional Board of Trustees comprised of
thoughtful and highly-engaged individuals with a variety of backgrounds who are dedicated to
working with our management team to create long term value for our shareholders. One such
trustee is John Alexander who is stepping down from our board having served since our IPO in
1993. John has made tremendous contributions to Equity Residential over the past 25 years for
which we will always remain extraordinarily grateful. But John has been much more than a
wonderful and dedicated trustee. He has also been a friend, mentor and leader. We have all
benefitted greatly from his wisdom, insight and commitment and will surely miss his sense of
humor!
Equity Residential will celebrate 25 years as a publicly traded company this summer. Since our
IPO in August 1993, we are pleased to have returned 12.9% annually to our shareholders
compared to 9.7% for the S&P 500. As we look forward, we expect apartment fundamentals to
continue to be strong and we remain confident that we should see an extended run of strong
growth across our markets notwithstanding short term headwinds as new supply is absorbed.
We expect the re-urbanization of our cities to continue unabated as powerful demographic and
economic forces shape our country�s future. Equity Residential is uniquely positioned to benefit
from this trend and we look forward to many years of growing income, dividends and
shareholder value on your behalf.
We appreciate your continued support.
Regards,
Sam Zell David J. Neithercut
Chairman President and CEO
April 26, 2018