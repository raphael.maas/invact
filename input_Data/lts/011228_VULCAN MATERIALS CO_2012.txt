To our shareholders and friends:
2012 was a year of decisive action
and spirited performance by
our employees, despite a challenging
economic environment.
While construction activity was weaker than in 2011,
resulting in lower volumes for our Company, we nevertheless
delivered sharply improved operating results.
Actions taken by our people throughout the Company
over the past several years have positioned Vulcan
for better performance in any phase of the economic
cycle and especially in a recovering construction
economy. We have achieved significant and sustainable
reductions in our cost structure, and we have
continued to manage our business so as to allow our
operations to participate fully in a recovery, at higher
margins. We have grown aggregates unit profitability
notwithstanding the weak economy. We expect that
any meaningful demand recovery will translate into
additional improvements in unit profitability and
significant earnings growth.
And there are indications that demand is finally
moving in the right direction. Residential construction
momentum is improving, and the consensus is that
the improvement will continue. Private non-residential
construction contract awards continue to grow.
We are also encouraged about the funding of public
infrastructure construction. The new Highway Bill,
MAP-21, was signed into law by President Obama in
July 2012. MAP-21 provides for a continuation of
near-record funding levels through Fiscal Year 2014,
while also significantly enhancing credit assistance
funding for large infrastructure projects through the
TIFIA (Transportation Infrastructure Finance and
Innovation Act) program. This legislation, which also
includes important highway program reforms that
will bring projects to completion sooner and with less

red tape and bureaucratic waste, provides both stability
and predictability for state highway programs.
The last several years have presented us with a challenging
journey, but one we have every confidence
will lead to record earnings well before volumes fully
recover. The hostile takeover attempt by a competitor
is behind us. In spite of the significant distractions
and costs caused by Martin Mariettas unsolicited
exchange offer, our people continued to operate safely
and efficiently, to serve our customers well, and
to build an even stronger Vulcan Materials Company.
Our focus is clear, and our fundamental strengths 
outstanding employees, strong and loyal customers,
the best production locations in the United States,
and superior cash gross profit margin per ton 
remain firmly in place.
OVERVIEW OF RESULTS
Given the continuing weak macroeconomic environment,
our performance in 2012 was strong. We grew
Adjusted EBITDA (earnings before interest, taxes,
depreciation, and amortization) significantly in 2012.
We ended 2012 with our fifth consecutive quarter of
year-over-year higher unit profitability in aggregates
and lower SAG (selling, administrative, and general)
expense. The key to our continued strong performance
in the face of considerable headwinds is to be
found in our people. Our workforce is second to
none  the best in the business. The people of Vulcan
are well trained, highly motivated and focused on
what matters. This includes a keen focus on managing
controllable costs, as well as a commitment to
meeting and exceeding our customers expectations

For 2012, cash gross profit per ton in aggregates was
5 percent higher than that of the prior year. SAG
expense was down 11 percent and Adjusted EBITDA
increased $59.5 million or 17 percent  all while
our aggregates shipments declined 1 percent. These
results demonstrate the benefits of our ongoing focus
on reducing controllable costs, maximizing operating
efficiency across the organization, and providing
effective price leadership in our markets.
We have made concerted efforts to enhance our
profit ability by extending and improving our already
robust procurement practices to capture significant
incremental savings across a broad array of spending
categories. We are also seeing a positive impact from
the critical investments we have made over the past
few years to replace legacy IT systems and processes
with new Enterprise Resource Planning (ERP)
and shared services platforms. We have significantly
streamlined processes and reduced back-office
costs while providing enhanced reporting flexibility
to monitor and control costs. These restructuring
actions taken in 2011 and 2012 resulted in annualized
overhead cost savings of more than $55 million.
OUTLOOK
2013 AND BEYOND
As you can see by the accompanying charts of
leading indicators of construction activity  including
residential, private non-residential, highways
and non-highway infrastructure  the outlook for
our industry and our business is improving. This
is the first time in years that we are starting to see
an emerging recovery in these indicators, especially
in the private sector.

While the economic and construction-related fundamentals
that drive demand for our products
continue to improve from the historically low levels
created by the economic downturn, there is a lag
in the impact on our businesses. Our expectations
for growth in volumes in 2013 are consequently
cautious; but we are confident that improving construction
contract awards, both public and private,
will lead to considerably higher demand for our
products as these projects get underway. However
great the actual improvement may be, our strong
operating leverage will enable us to generate significant
earnings growth as the economy improves.
Leading indicators of private construction activity,
specifically residential housing starts and contract
awards for nonresidential buildings, continue to
improve in our markets. Consequently, aggregates
demand in private construction, particularly residential,
is beginning to grow. We are seeing evidence
of this growth in several key states, including
California, Georgia, Florida, Texas and Arizona.
For public construction, passage of the new federal
highway bill in July of 2012 is providing stability
and predictability to future highway funding. The
large increase in TIFIA funds contained in the
new highway bill provides leveraged funding of up
to $50 billion according to the U.S. Department
of Transportation, and should positively affect
demand in the future. TIFIA program support for
large-scale transportation infrastructure projects
has already been eagerly embraced by several of the
largest states in the U.S.  some of the biggest TIFIA
projects in the nation are in Vulcan-served states
such as California, Texas, Florida and Virginia. The
uncertain timing of larger projects, including TIFIAfunded
projects, continues to make forecasting
quarterly volume growth difficult.

CONTINUING TO
STRENGTHEN OUR
BALANCE SHEET
Our balance sheet continues to improve. To maintain
that momentum, our management team will continue
to focus on prudently divesting non-core assets,
reducing debt and building our aggregates positions
in key markets to strengthen an aggregates asset portfolio
that already delivers the highest unit cash gross
profit in the aggregates industry.
We generated $238.5 million in cash from our operations
in 2012. We raised another $168.6 million
in cash proceeds from various strategic asset sales
and related transactions. We retired an additional
$135 million in debt and made several strategic
investments. This left us with $275.5 million cash on
hand, which is available to pay the $151 million
of debt maturities scheduled in 2013. We expect
future debt maturities will be funded from available
cash and free cash flows, with our $600 million
line of credit still in place for short-term liquidity
needs. We expect net interest expense to decrease
by approximately $14 million from 2012 to 2013.
These moves have greatly enhanced our liquidity
and are providing the financial flexibility to continue
investing in our business as the economy, and our
volumes, recover. Measures such as these, combined
with our efforts to enhance profitability, will also
enable us to achieve an important goal for us: restoring
a meaningful dividend.

ENDURING STRENGTHS
Aggregates are the most essential of basic inputs
to private and public construction, infrastructure
and growth. Vulcan remains the leading supplier
of those aggregates in the United States, with
unmatched, easily accessed, permitted reserves
and superior aggregates operations.
We are well-positioned to capitalize on market
recovery. Recent financial results demonstrate strong
operating leverage, with commensurately strong
conversion of incremental sales to cash flow. Our
de-risked balance sheet provides great potential to
deliver enhanced shareholder value going forward.
We offer proven operational expertise, attractive
unit profitability and industry-leading pricing growth.
All of these factors contribute to our favorable prospects
for long-term growth.
Population growth drives demand for our essential
products, and our footprint is concentrated in
dynamic markets where population continues to grow
at a pace exceeding national averages by a significant
factor. Our aggregates assets are strategically positioned
in attractive markets. We serve more of the
nations high-growth areas than any other aggregates
producer  19 of the 25 highest-growth metropolitan
areas in the United States are served by Vulcan.
We are fully prepared to meet demand as it rises.
Our high-quality reserves  at 15 billion tons, the
countrys largest reported proven and probable
reserve base  are close to these growth markets, and
our distribution systems are unsurpassed. And we
have the right people to meet the challenges ahead.
In a year that brought many challenges, our employees
stayed the course, delivered superior results in
our aggregates business, and proved once again that
they are the very best in the business. Their focus
and efforts have positioned Vulcan for superior performance
and results.
We are optimistic about our future. Vulcan offers
enduring, fundamental strengths and remains
well-positioned for increasing profitability and
significant long-term growth.
Thank you for your continued interest and support.
Donald M. James
Chairman and Chief Executive Officer
March 15, 2013