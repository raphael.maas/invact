DEAR SHAREHOLDERS AND FRIENDS,


For the past 120 years, The J.M. Smucker Company
has grown and flourished thanks to an unwavering
commitment to making quality products that meet
the evolving needs of our consumers. We recognize
consumers� eating habits have fundamentally changed
since our founding�and even over the past few years.
We are developing an increasingly sophisticated
view of the link between food and our consumers�
sense of purpose in their lives. These insights are
helping us develop brands and products that align
with the values of today�s consumers, giving us a
clear roadmap for the future.
As consumer preferences are more influential than ever,
fiscal 2017 was a pivotal year of gearing up for future growth
as we continued to transform our business to reflect this
changing landscape while remaining true to our heritage
and Our Purpose of helping to bring families together to share
memorable meals and moments. We�re investing significantly
in product innovation and strengthening capabilities in areas
such as data analytics, e-commerce, and digital marketing to
drive top-line growth while managing costs to fuel that growth.
While top-line softness persisted throughout the fiscal year,
both for our industry and for our business, we achieved
adjusted earnings per share in line with our projections for
the year by accelerating realized synergies related to the Big
Heart Pet Brands acquisition and managing budgets and costs
efficiently. Specific highlights for fiscal 2017 include:
� Achieved a 7 percent increase in adjusted earnings per
share, excluding a prior year gain and noncash tax benefit;
� Grew key on-trend brands such as Smucker�s�
Uncrustables� sandwiches and Caf� Bustelo� coffee with
both brands experiencing double-digit sales growth;
� Returned more than $775 million to our shareholders
in the form of dividends and share repurchases; and
� Attained $122 million in incremental synergies for the
year, exceeding our original target, and embarked on
the next phase of our cost-reduction initiatives.
CAPITALIZING ON BRAND STRENGTHS AND CONSUMER TRENDS
We have strong brands that participate in growing categories
including coffee, pet food, peanut butter, and snacking. Today,
our brands and products can be found in 93 percent of U.S.
households, and our priority is to ensure that Smucker products
remain the brands of choice for consumers.
We�re fortifying that position by developing products that
reflect the reality of today�s consumers, who expect more from
food than ever before. In addition to providing nourishment,
food must deliver an enjoyable experience, connect individuals,
satisfy cravings, promote health, and convey personal
values. Brands once seen as status symbols now represent
one�s values.
Against this backdrop, we see consumers in the future making
choices that fit their nontraditional, fast-paced schedules;
meet highly specific wellness and functional needs; deliver
convenience and quality in equal measure; and center on
authentic brands with recognizable ingredients. These food
trends diverge sharply from the past, which is why we have
developed a three-year strategic roadmap to capitalize on
new marketplace opportunities and generate sustainable,
profitable growth.
In fiscal 2017, we made great strides in bolstering a new
product pipeline that reflects shifts in consumer preferences
toward convenience, simple ingredients, and snacking. While
more meaningful contributions are expected in the years
ahead, the effectiveness of these initiatives already can be
seen in the near term.
� In U.S. Retail Coffee, Folgers� coffee responded to
consumers� fondness for simple ingredients with
Folgers Simply Gourmet�, a premium line of coffees
made with all-natural flavors. Meanwhile, Dunkin�
Donuts� K-Cup� pods are satisfying demand for
convenient one-cup servings

� In U.S. Retail Consumer Foods, we are leveraging our
Jif� peanut butter brand to build a snacking platform
that caters to consumers� desire for on-the-go options
and protein-packed foods.
� In U.S. Retail Pet Foods, consumers� preferences for simple
ingredients extend to the foods they buy for their pets.
New Milk-Bone� Farmer�s Medley� treats are made with
USA-sourced meat and contain no fillers, corn, or artificial
flavors or preservatives.
A ROADMAP FOR FUTURE GROWTH
At the highest level, our three-year plan is about balancing
a focus on top-line growth with a diligent approach to cost
savings, allowing us to deliver our growth objectives regarding
earnings per share. It includes disproportionate investment
in key growth segments across our portfolio, including our
Jif, Smucker�s Uncrustables, Sahale Snacks�, and Milk-Bone
brands, and all of our coffee brands.
In e-commerce, we are redefining every aspect of our approach,
including organization, capabilities, and investment. With pet
food and pet snacks and coffee leading the way, our target
is to generate 5 percent of net sales in fiscal 2020 through
e-commerce. Already, subscription order models are proving
highly successful in this distribution channel; online sales of
Natural Balance� premium pet food grew roughly 60 percent
in fiscal 2017.
Other facets of the strategic plan include increasing capital
expenditures to add new manufacturing capacity, improving
flexibility and productivity at several existing manufacturing
facilities, and enhancing our information technology. Recently,
we broke ground on a new Smucker�s Uncrustables sandwiches
manufacturing facility in Longmont, Colorado. When complete
in 2020, this plant will provide the capacity to double sales
of what is today a $220 million business. We also plan key
capital improvements at our coffee facilities in New Orleans,
Louisiana, and peanut butter plant in Lexington, Kentucky,
that will improve efficiencies, lower costs, and enhance quality.
Finally, we expect acquisitions to play an ongoing role in our
future growth. As demonstrated by our recently announced
agreement to acquire the Wesson� cooking oil brand, these
transactions provide opportunities to add top- and bottomline growth where we benefit from our existing customers
and channels, broaden participation in existing categories,
and realize synergies in our supply chain.
BALANCING INVESTMENTS WITH COST MANAGEMENT
To succeed, this growth journey needs fuel, which is being
created in the form of ongoing cost management. We delivered
over $120 million in incremental synergies associated with
the Big Heart Pet Brands acquisition in fiscal 2017, exceeding
our $100 million goal. This brings cumulative synergies
achieved to date to approximately $160 million out of the
total $200 million we expect to achieve by fiscal 2018. We
have targeted another $250 million in cost reductions by fiscal
2020 from our companywide cost management program.
This will result in $450 million in total annual synergy and
cost reductions by that year.
Savings under this $250 million cost management program
are expected to come from previously announced plant
consolidations, accelerated operational efficiencies across
the entire manufacturing network, organization optimization,
revenue growth management, SKU rationalization, and a zerobased budgeting program. We believe that this strategy will
capture the growth opportunities at hand and continue our
historical track record of strong value creation, which has enabled
us to return more than $3.1 billion to shareholders over the
past five fiscal years through dividends and share repurchases.
A LONG-TERM VIEW
As important as our new roadmap is for charting our course
for the next three years, we will continue to pay attention to
the factors that have guided us for the past century: a focus
on the consumer, strong relationships with all our constituents,
and adherence to our culture and long-standing Basic Beliefs
as we �Execute for Today, Build for Tomorrow.� This message
encapsulates our plans for the near future as we expect to
deliver on three key financial objectives:
� Achieve earnings per share growth in line with our
stated long-term objective;
� Grow the top line, both organically and through
acquisitions; and
� Achieve significant cost savings that will provide fuel for
investments in growth and support the bottom line.
Although our industry looks very different than it did a
generation ago, we believe companies that embrace and
address today�s accelerating pace of change can and will
thrive. In this environment, our strong portfolio of leading
and emerging brands continues to serve us well, providing
the flexibility required to continuously adapt to changes in
the marketplace. We are thankful for our employees, who bring
these beliefs to life every day, and to you, our shareholders,
for your continued support and confidence in our Company.
Tim Smucker Richard Smucker Mark Smucker
June 19, 2017


