Lets cut to the chase.
For this years letter, instead of comprehensively reviewing 2016, we thought wed start by
addressing the two questions we have heard most often from shareholders lately:
 Why did we purchase relatively few Loews shares in 2016?
 Why hasnt Loews made an acquisition recently?
The answers to these questions illustrate our fundamental approach to creating long-term
shareholder value, and the ways in which we allocate capital to achieve that goal.
In 2016 we purchased 3.4 million shares of Loews stock, or about 1% of our
outstanding shares, at an average price of just under $39 per share. About
2.6 million of those shares were repurchased during the first half of the year.
As the equity markets climbed to record heights during the second half of 2016,
and Loewss common stock moved along with them, we chose to exercise caution.
In hindsight, we could have repurchased more of our shares early in the year, when
Loews stock was trading at lower prices and the S&P 500 was well below its recent
levels. Alas, it must be time to get a new crystal ball!
Unlike some companies, we do not repurchase our stock robotically or set annual
repurchase quotas. Rather, we seek to buy back our stock when its below our
estimation of its intrinsic value and when prevailing market conditions seem
conducive. While we remain positive on our shares and see great potential for our
subsidiaries, we did not see the wisdom of buying back shares in an exuberant equity
market. We are more comfortable buying back our shares when the market is not
hitting new highs. Over the span of time, measured in years and decades, we are
proud of our record and we feel that our share repurchases have created significant
value for our shareholders.
As for the second question, we continue to kick tires on potential acquisitions,
looking for the right acquisition at the right price. Valuations in the merger market
have made our search difficult and frustrating. The merger market today is being
driven by large numbers of corporate buyers, as well as by private equity funds which
are under pressure to put their capital to work. The abundance of private capital
combined with the amount of leverage available at remarkably low rates has enabled
private equity firms to pay generous prices for companies that havent already been
gobbled up by strategic buyers.
Our focus drives us to invest only when all the pieces of a transaction  from
valuation, to potential cash flow, to future industry dynamics  add up to a solid
investment. To date, we have not found a potential acquisition where all the pieces
fit together the way we believe they should. Its a tough market in which to be a
disciplined buyer.
That said, our liquidity gives us tremendous strategic and financial flexibility.
We will never stop using our best judgment to balance risk and reward to build value
for all shareholders. And we are confident that, despite todays market exuberance,
we will find desirable opportunities in the future.
ALLOCATING CAPITAL
AND CREATING
SHAREHOLDER VALUE
As weve often said, we are committed to our
longstanding philosophy of creating value for all
our shareholders. We view our primary job as being
effective capital allocators  finding attractive
investments with the potential to create value over
the short, medium and long-term. We thought it
would be useful to review the four ways we allocate
our capital and manage Loewss financial resources
to drive shareholder value. We can:
 Return capital to shareholders, largely through
share repurchases;
 Use holding company liquidity to invest in, or
alongside, our existing subsidiaries;
 Acquire new businesses, though corporate
acquisitions for us are admittedly rare; and
 Maintain holding company cash and investments.
We are comfortable maintaining substantial
holding company liquidity because it enables us
to take advantage of opportunities as they arise, as
well as to protect ourselves during turbulent times.
We focus on achieving attractive risk-adjusted returns
when allocating our capital, essentially utilizing a
hurdle rate of return for each specific potential use of
parent company liquidity based on our perception
of the risks and rewards of the investment. Given that
share repurchases involve investing in something we
know well  our existing businesses  they require a
relatively low risk-adjusted return. The next rung on
our investment ladder is investing in our subsidiaries.
We know these businesses and their industries very
well. In general, the required risk-adjusted return
for these projects is slightly higher than share
repurchases. Acquisitions at the holding company
level compete for capital with share repurchases
and with investing in our subsidiaries. In view of
the greater risks of investing in a new business, the
risk-adjusted hurdle rate is the highest of our capital
allocation options, and underscores why acquisitions
at the holding company level happen infrequently.
Lets look at each potential use of capital in
more detail.
SHARE BUYBACKS
We have a long history of share repurchases, having
used this tool to create value long before it became
popular. Over each of the last five decades, we
have repurchased more than one-quarter of our
outstanding shares. As we stated earlier, we aim to
buy back our shares when the market is not reaching
new highs. When we evaluate share repurchases,
many factors go into the equation, including the price
of Loewss shares, the price of our subsidiaries shares,
our assessment of our subsidiaries intrinsic value, the
state of the equity markets, and the value we ascribe
to our non-public businesses.
While our share repurchases were relatively modest in
2016, this came on the heels of the repurchase of 9%
of our shares for $1.3 billion in 2015  the highest
number of shares we have ever repurchased in a single
year. We believe our longstanding practice of buying
back our shares has greatly enhanced the long-term
outperformance of Loewss share price relative to the
S&P 500.
INVESTING IN OUR
SUBSIDIARIES TO
SUPPORT GROWTH
The second way we create value is by using holding
company liquidity to help our subsidiaries grow.
Since 2010, our subsidiaries and their partners have
invested or committed more than $10 billion to
capital projects and acquisitions. Such investments
have enabled Boardwalk Pipeline Partners to launch
a series of strategic capacity expansion projects; they
have allowed Diamond Offshore Drilling to upgrade
its fleet; and they have helped Loews Hotels develop,
acquire and renovate numerous properties, extend its
market presence and enhance its offerings to guests.
Over the years, we have deployed holding company
cash to help our subsidiaries execute transactions
when we considered it in the best interest of Loewss
shareholders. Most of our subsidiaries capital projects
and acquisitions have been self-funded, but from time
to time over the past few years, we have provided
bridge financing or other funding, particularly to
Boardwalk and Loews Hotels.
At Boardwalk, when attractive capital markets
funding has not been available or when flexible
forms of financing were required, Loews has stepped
in and provided bridge financing. We work closely
with Boardwalks management team to hone their
financing plans, and our cash position enables us
to utilize parent company capital in projects with
attractive risk-adjusted returns for Loews and
Boardwalk. We were able to do just that in 2012
when we helped Boardwalk purchase Louisiana
Midstream. At the time, advantageous capital
markets funding was not available to Boardwalk, so
Loews jointly acquired the assets with Boardwalk.
Shortly following the acquisition, Boardwalk obtained
third-party financing in order to purchase Loewss
stake in Louisiana Midstream. This acquisition
allowed Boardwalk to enter the natural gas liquids
storage and distribution business. Today, Boardwalk
is able to take advantage of the revival of the
U.S. petrochemical industry due to this strategic
acquisition made in 2012.
We have also provided equity capital to support
Loews Hotels acquisition, development and
renovation activities. Having access to our capital
is a distinct advantage for Loews Hotels, because it
enables the company to move quickly and decisively
to secure properties. Since 2012 Loews Hotels has
grown from a chain of 18 hotels to 24 hotels. And
in that same period, the number of Loews Hotels
rooms has increased 50%, from just over 8,000 to
more than 12,000.
Included in this new group of properties is the
companys fifth hotel in Orlando, the 1,000-room
Loews Sapphire Falls Resort. This project builds
on our very successful 15-year partnership with
Universal Studios.
POTENTIAL ACQUISITIONS:
DISCIPLINED CAPITAL
SEEKING OPPORTUNITY
While corporate level acquisitions by Loews
are rare, that doesnt mean we arent continually
reviewing potential opportunities. Maintaining
a substantial liquidity position allows us to move
swiftly and decisively when considering an
acquisition. Typically, we look to make an initial
equity investment in the range of $500 million
to $1.5 billion, with the ability to invest additional
equity once we have grown even more comfortable
with the company and its risks and opportunities.
We use an extremely disciplined approach when
we explore acquisitions. A potential new leg to the
stool competes with other uses of our capital such
as share repurchases  so we meticulously review
all opportunities. In making an acquisition at the
holding company level, were seeking the right deal
at the right price  either a company with good cashon-
cash returns and strong secular growth trends,
or undervalued assets at an attractive entry point in
the cycle.
As weve said before, we are not seeking to add
another insurance or energy business at the Loews
corporate level. However, we would consider
expanding our presence in those sectors through
bolt-on acquisitions by our subsidiaries.
In simple terms, were looking for businesses that
can stand the test of time and create value for us
over the long term. We avoid businesses that are
especially vulnerable to technological disruption or
management miscalculations of consumer demand,
such as retail, fashion or hi-tech. When we do stake
out a presence in a new sector, we prefer to do so by
establishing a platform upon which we can build, as
we did with Diamond and Boardwalk, rather than
make a large acquisition. We do look for businesses
based in the United States, although its fine if they
have significant global operations.
MAINTAINING LIQUIDITY
After looking at all of these alternatives, if we
think theres nothing to do with our cash, we are
comfortable allowing cash and investments to
accumulate. Weve seen time and again that there
is value in maintaining liquidity during both good
and challenging times.
BALANCE SHEET
STRENGTH
Our ability to execute any of these value-creation
strategies is supported by a conservatively capitalized
and highly liquid balance sheet. One source of cash
flow for Loews is earnings on the holding companys
portfolio of cash and investments. However, the
primary source of Loewss cash flow is dividends
received from our subsidiaries. Most recently, CNA
Financial has been the main source of such dividends,
which totaled $2.1 billion over the past four years. In
February of 2017, CNAs board of directors declared
a special dividend of $2.00 per CNA share, for a
payment of almost $485 million to Loews in the
2017 first quarter. This is on top of the $243 million
we expect to receive from CNA in 2017 through
its $0.25 per share quarterly dividend. Five years
ago, however, it was Diamond and Boardwalk that
contributed the majority of dividends to Loews. This
evolution underscores the benefit to our shareholders
of a portfolio of diverse businesses.
The cornerstone of our capacity to create value for
Loews shareholders is our financial strength. While
we never know when new opportunities will present
themselves, we do know that strong companies
are best positioned to withstand adversity and to
capitalize on opportunities when they arise. For this
reason, one of our basic principles in managing our
capital is to maintain relatively low levels of holding
company debt so that we can service all holding
company obligations in any foreseeable financial
environment without difficulty. In this regard, Loews
had $1.8 billion in debt at the end of 2016, with
the first maturity coming due in 2023, compared
to Loewss market capitalization of approximately
$16 billion.
At the end of 2016, our portfolio of cash and
investments totaled over $5 billion. Most of these
funds are investments in high quality money
market instruments, mainly U.S. Treasury bills.
The remainder of the portfolio is largely made up
of fixed income securities, public equities and a
diverse set of alternative investments. The portfolio is
overseen by our in-house investment department.
THE BENEFITS OF A
DIVERSIFIED PORTFOLIO
Loewss diversified holding company structure is a
key factor in our ability to generate shareholder value.
Our multi-industry structure is unique and does
not fit neatly into todays typical S&P 500 corporate
model, which is dominated by pure play companies.
Yet, as a diversified holding company, we have the
freedom to make investments across a broad spectrum
of industries wherever we perceive opportunity, along
with the ability to balance industry cycles.
We also believe that the holders of Loews shares
benefit from the fact that three of our subsidiaries 
Boardwalk, CNA and Diamond  are publiclytraded
companies. Our shareholders benefit from
the public ownership of our subsidiaries in several
important ways:
 Market Valuation  Third-party investors value
our subsidiaries directly in the public equity
markets, providing Loews shareholders with an
objective measure of the value of our publiclytraded
subsidiaries.
 Transparency  As public companies and SEC
registrants, our subsidiaries provide financial
disclosures that further enhance transparency for
Loews shareholders.
 Self-financing  The subsidiaries can, with greater
ease, directly access the capital markets to finance
their operations and expansion plans, if needed.
 Recruiting top talent  The opportunity to hold
a senior executive position in a publicly-traded
company is very appealing to most candidates.
While we closely monitor the performance of our
subsidiaries, one thing we do not do is micromanage
them. We view the holding companys role as
providing counsel on significant capital and strategic
initiatives, as well as CEO selection. We rely on
the experienced subsidiary management teams to
make fundamental decisions about operating issues,
product and service offerings, and marketing plans.
A PORTFOLIO
POSITIONED FOR
LONG-TERM VALUE
Each of our businesses has significant strengths, along
with talented management teams that are building on
those strengths to create value for the long term.
CNA FINANCIAL
CNA is a property-casualty insurance company that
has been in the Loews portfolio of businesses for
over 40 years. The company operates a solid specialty
business within the commercial insurance industry.
Because of its deep expertise and long-established
relationships with customers and producer partners,
CNA enjoys a competitive edge in key areas of its
specialty portfolio, such as professional services,
healthcare, surety and warranty. CNAs specialty
business continues to produce excellent results. In
addition, the commercial business has improved
meaningfully and the international business is
performing well. CNAs strong balance sheet and
capital position, well-respected brand, dedicated
team, extensive branch footprint and strong local
relationships with agents and brokers provide an
excellent foundation upon which CNA can continue
to build its business.
Under the leadership of CNAs new CEO, Dino
Robusto, who joined in November 2016, the company
will focus on growing underwriting profits, in
particular by expanding in segments where it
has a decided competitive advantage. CNA will
also continue to emphasize cost control in order
to improve its expense ratio, while supporting
investments in advanced technology, and digital
and analytics capabilities to improve productivity and
streamline operations.
DIAMOND OFFSHORE DRILLING
The offshore drilling market has continued to suffer
through a tough, protracted downturn, reflected in
a decline in the number of working rigs and lower
revenues industry-wide. While Diamond has not
been immune to market forces, it remains in a stable
position relative to many others in the industry.
Diamond did not over-expand during the industrys
boom years, instead using its strong balance sheet
to strategically renew and upgrade its fleet. As a
result, Diamonds fleet is younger than the industry
average, with a good backlog. On December 31,
2016, the total contracted backlog was $3.6 billion,
and approximately 94% of Diamonds available
ultra-deepwater rig days for 2017 are contracted with
top-tier customers.
The Diamond team, led by CEO Marc Edwards,
has managed through this turbulent period by
maintaining the companys liquidity and balance
sheet strength, continuing to control costs and
improving rig efficiencies. We have every confidence
that offshore drilling will rebound over the long
term, as offshore oil production is a major source
of the worlds oil supply. When prices recover, and
customers again look offshore for new sources of oil
and gas production, Diamonds attractive fleet and
sound financial footing should provide a foundation
for performance and growth.
BOARDWALK PIPELINE PARTNERS
After nearly a decade of rapid supply growth from
unconventional supply sources, natural gas supply
and demand has begun to come into balance and
natural gas prices have started to recover. In the U.S.,
increased demand for natural gas is being driven by
four market dynamics:
 A rapid increase in exports to Mexico;
 An increase of exports through the liquefied
natural gas (LNG) facilities that have started to
come online along the Gulf Coast;
 An increase in the use of natural gas as a fuel; and
 Higher industrial usage by petrochemical, steel
and agricultural chemicals businesses due to lower
natural gas prices and the abundant long-term
supply of this commodity.
These market dynamics have created both
opportunities and challenges for Boardwalk.
Over the past three years, Boardwalk announced
$1.6 billion of growth projects primarily to meet
natural gas demand by end-users, such as LNG
export, power generation and industrial facilities.
During 2016, Boardwalk placed four of these
projects into service. These projects, which cost
approximately $320 million, were collectively under
budget and on time. The company has been using
internally-generated cash to help fund growth
projects and has decreased its balance sheet leverage.
Under the leadership of CEO Stan Horton, the
Boardwalk team is continuing to strategically build
out its transportation system, pursue new revenue
opportunities, and strengthen its balance sheet.
LOEWS HOTELS
Loews Hotels, with a network of 24 properties,
can move nimbly to capitalize on trends in an
industry that is increasingly dominated by behemoth
chains. The companys focus is on highly profitable,
distinguished hotels in the upper upscale market
that can accommodate group business. Additionally,
Loews Hotels continues to look for opportunities
with unique, built-in demand generators and seeks to
cultivate successful long-term partnerships, as it has
done with Universal Studios in Orlando.
Strategic partnerships allow Loews Hotels to
accelerate growth while improving its return on
invested capital. In 2016, Loews Hotels opened its
fifth hotel in Orlando  the 1,000-room Loews
Sapphire Falls Resort  and announced the
expansion of its extremely successful Cabana Bay
Beach Resort, which will add another 400 rooms
in the summer of 2017. These two additions bring
Loews Hotels room count in Orlando up to 5,600
keys. In the summer of 2018, the joint venture is
scheduled to open its sixth property, the 600-room
Aventura Hotel.
Over the coming years, Loews Hotels will continue
to look to invest in properties and leverage new and
existing partnerships. Additionally, Loews Hotels
will stay focused on commercial and operational
excellence while pursuing smart growth.
COMMITTED
TO BUILDING VALUE
As significant shareholders of Loews, we come into
the office every day with one goal: to create value.
How can we best deploy our capital to create
significant value? is the question that guides our
decision making.
We rely on our subsidiaries and their executive teams
to make wise choices and generate strong financial
results. We seek to ensure that our subsidiaries are led
by executives with extensive industry experience and
that the companies have the financial firepower to
compete effectively and grow their businesses.
As we strive each day to build long-term value for
our shareholders, we know this would not be possible
without the talent, commitment and professionalism
of our employees, both at Loews and our subsidiaries.
We thank our team members and associates for their
hard work, excellence and integrity, as well as our
board of directors for their commitment, guidance
and sage counsel. We also want to thank you, our
shareholders, for entrusting us with your investment.
We will always work to earn your trust by managing
Loews for long-term value creation.
Sincerely,
JAMES S. TISCH
ANDREW H. TISCH
JONATHAN M. TISCH
Office of the President
February 16, 2017