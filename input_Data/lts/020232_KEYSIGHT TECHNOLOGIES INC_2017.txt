To Our Stockholders,
2017 was a pivotal and notable year for Keysight. Transforming Keysight for revenue and earnings growth is a key
element of our plan that we initiated a few years ago to create value for our shareholders. We are pleased to report that
our investments and commitment to transform Keysight are yielding results, which included record revenue and orders
while maintaining our strong operational discipline and financial performance for the year.
Keysight is at the heart of innovation processes in many growing end markets. We are executing on our strategy to
create value for our customers and shareholders. In 2017, we drove growth across multiple avenues of emerging
technology trends such as 5G, automotive and energy, IoT, aerospace and defense and high-speed datacenters, that
yielded total Keysight organic order growth of 7 percent, the highest in the last six years. With sustainable topline
growth, we are well-positioned to deliver earnings growth going forward.
This year we also continued to innovate ahead of emerging technologies to enable our customers to win with end-toend design, test and optimization solutions. Our investments to bring new solutions to market, our relentless focus on
the growth areas of the market and our go-to-market approach are fueling our momentum. Partnering early with
customers and delivering full solutions are key initiatives for Keysight and areas where we are making strong progress.
As a result, we are engaging with customers earlier in the development cycle than ever before, increasing the value
we provide and growing sales of our R&D and software solutions, which is ultimately a catalyst for earnings growth.
In addition to making investments in our core business, we also expanded our technology portfolio and markets
inorganically with several acquisitions in markets where customers are investing in next-generation digital and
electronic technologies. Our largest acquisition, which closed in April, was Ixia, a leader in the network and
application test and visibility markets. The addition of Ixia expanded our reach within the communications
development lifecycle. To deliver the next world-changing innovation, our customers need help with more than just
physical layer test and measurement solutions. With more complicated and integrated functionality, electronic devices
are vulnerable to faults at any point in the technology stack. Customers need to verify the quality, performance,
compliance and security of products at every layer--from Layer 1 to Layer 7--as well as how they integrate into live
networks. Through our acquisition of Ixia, Keysight helps customers gain insight into how products are functioning
at every layer. Our ability to combine Keysight's and Ixia's technologies and bring complete end-to-end solutions to
market enables us to widen the technology gap between Keysight and the competition.
While smaller in scale, I will also note our acquisition of ScienLab, which expanded our Auto and Energy solutions
offerings. ScienLab is based in Germany and serves a Tier-1 customer base. This acquisition strategically expands our
global footprint and solutions portfolio, allowing end-to-end solutions for hybrid and electric vehicles and battery test
solutions. Several trends are driving development activities across multiple dimensions at once in this end market,
including increasing electronic content in vehicles, electric and hybrid powered cars, and radar technologies for
autonomous driving. With this broad development landscape, we believe we will see continued R&D investment in
auto and energy for many years to come, and we are intensifying our focus on this growing market opportunity.
Keysight�s execution and performance this year were even more meaningful when considering the unimaginable
challenges our team faced with the northern California wildfires in Santa Rosa in October. I am proud of how the
Keysight team came together to help the community and each other navigate through this challenging time. I�m
inspired by the resiliency, acts of courage and generosity. Our production facilities in Santa Rosa are now fully
operational and our team is more focused than ever. I�d like to thank each and every member of the Keysight team
around the globe, as well as our partners, customers and investors, for their unwavering support. 

In closing, our clear vision, continued focus on market-leading solutions and commitment to our customers drove our
strong results for the year. We have consistently delivered on our commitments and are very pleased with our progress
to transform Keysight for long-term value creation. While 2017 was a transformative year, we believe we are just
getting started and are poised to continue to drive revenue and earnings growth as the long-term trends in our markets
evolve. We look forward to sharing our progress with you along the way.
Thank you for your continued support.
Ron Nersesian
President and Chief Executive Officer
February 9, 2018