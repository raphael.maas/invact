Dear Shareholders: Great Gathering Places: these are what we create. Special Places and
Experiences are the focus of our vision. A CUSTOMer-built Company giving our Consumers,
Owners, Retailers and Employees (C.O.R.E.) what they want, when they want it and where they
want it is our mission. These three things define and drive us as we strive to create value for
you. To be successful it is imperative for a company to have vision, a mission and goals. It has
been demonstrated and documented that companies that adhere to these traits are better
financial performers over extended periods of time. It is not dumb luck that General Growth
Properties, Inc. (GGP) has produced superior returns, ranking us within the top three among all
Real Estate Investment Trusts (REITs) during periods covering the last three, five and ten years.
We have accomplished this because GGP is anchored solidly to our core foundation of ethics,
integrity, timeless fundamentals and consistent profitability. Good reputations are not made
overnight, nor is recurring profitability; it takes time and it takes hard work. That is what we are
here doing for you. We have made, and we continue to make, a lasting contribution in the
marketplace. Beginning in 1954, this company was Built To Last and we are continuing to Build
To Last as we move forward in the 21st century.
2005 was a year of integration for GGP. Following the 2004 acquisition of The Rouse Company,
which consisted of ownership interests in 37 regional malls, 4 community centers, 6 mixed-use
projects and the master planned communities of Columbia, Maryland, Summerlin, (Las Vegas)
Nevada, The Bridgelands, (Houston) Texas and
a 52.5% interest in The Woodlands, (Houston)
Texas; we had to integrate all of the properties
into our company. With these properties came
approximately an additional 1100 people that
needed to understand and embrace the culturethat we have created at GGP. Are we happy with our decision to acquire the Rouse Company?
Absolutely. Our enthusiasm for this transaction grows with each passing day. Our excitement
builds because we see many opportunities to improve the Rouse portfolio of retail centers,
and we see our existing GGP malls improving as well. We are creating even Greater Gathering
Places in malls such as Water Tower Place, Chicago, Illinois, Fashion Show Mall, Las Vegas,
Nevada, Faneuil Hall, Boston, Massachusetts and Ala Moana Center, Honolulu, Hawaii.
GGP is a strong and unified company. The addition of the Rouse people and properties make us
even better. We are a team at General Growth. We share the same goals, objectives and values to
make this a better company for all concerned. Yes, we have an outstanding collection of assets,
but first and foremost we are a company. The properties comprise a part of the company but they
do not make up the whole. The REIT industry has historically relied on net asset value (NAV) as
being the primary metric for establishing the value of a publicly traded real estate company. Why
are real estate companies not valued on earnings per share growth, cash flow growth or dividend
growth? Is real estate that different from other industries? We believe that as GGP continues to
grow and continues to operate as a consistently stable and
profitable company, living up in every way to our reputation
as a blue-chip real estate company, that the marketplace will
revisit how we should be valued.
2005 was another exceptionally profitable year for our
shareholders. We delivered a 35.2% total return versus
the Morgan Stanley REIT Index, which appreciated 12.1%,
the S&P 500, which increased 4.9%, the NASDAQ which
appreciated 2.2% and the DJIA, which appreciated 1.7%,
all on a total return basis. Over the last 12 years (since its
initial public offering (IPO) in April 1993), GGPs per-share
growth in FFO has increased at a 16% rate compounded annually. Our annual compounded total return including reinvested dividends, has been
approximately 22.5% from the IPO through December 31, 2005, compared to 10.3%
for the S&P 500 Total Return Index and 10.3% for the NASDAQ during this same period. As owners of GGP, you will be pleased to know that in the past year the Company
accomplished the following:
1 Total annual dividend increased to $1.49 per share, an increase of 18.3% over 2004.
1 Total share price increased from $35.30 on January 3, 2005 to $46.99 on December 30, 2005.
1 Fully diluted FFO per share increased to $3.06 for 2005, 10.5% above 2004.
1 Total FFO for 2005 increased to $896 million versus $766 million in 2004, an increase
of 16.9% over 2004.
1 Total sales increased 6.4% and comparable sales increased 3.5% for 2005.
1 Sales per square foot in 2005 were $437 versus $410 in 2004.
1 Total prorata property revenues for 2005 increased to $3.3 billion, an increase of
55% over 2004.
1 Real estate property net operating income (NOI) increased to $2.3 billion for the year
2005, an increase of 57% over 2004. Mall shop occupancy increased to 92.5% in 2005 versus 92.1% in 2004.
1 The average rent per square foot for new/renewal leases signed was $38.69 in 2005,
versus $28.24 for all leases that expired in 2005, a 27% increase.
We often speak about the importance of being a blue chip company. Blue chips are large,
creditworthy companies. Blue chips are companies that are renowned for quality and for
the wide acceptance of their products and services. And most notably, blue chips are
companies that consistently make money and pay increased dividends year after year.
These characteristics define GGP.
We are very proud that our dividend increases during the past five years have averaged
18.5% per year, more than any other REIT in that same period. Importantly, we achieved
this dividend growth despite paying out less than 50% of our funds from operations (FFO)
per share, leaving us with hundreds of millions of dollars to reinvest in our business.
In the 13 years that GGP has been a public company, our annualized dividend has increased
13 times from a split adjusted (3:1) $.493 per share in 1993 to $1.64 per share currently.
A shareholder who invested in GGP at the time of the IPO in 1993 received a $1.48 per share
(pre-split) dividend on their $22 investment, or a 6.7% yield. IPO shareholders that still own
their stock now receive an
approximate 22.4% cash yield
on their initial (pre-split)
$22 per share investment,
not including an approximate
541% return on their principal. The question we ask ourselves daily is where do we go from here? How do we maintain and
grow the terrific business we have? Guidance can oftentimes come from looking back into
history. It was during 500 BC that The Greek Agora at the foot of the Acropolis in ancient
Athens became one of the first urban marketplaces, with a central square surrounded by
buildings. In 100 BC, the Republican Forum at the base of Capitoline Hill was the commercial
and government center in Rome. By 1174 AD, the Market-Place developed in the city of Brussels
with buildings constructed by guilds and crafts corporations along a rectangular square. Similar
market squares would appear during the Middle Ages in other European cities. In 1288, the
Piazza del Campo in Venice, Italy was an open space surrounded by the cathedral and buildings,
with eleven shop-lined streets converging on the center. Sturbridge Fair opened in 1400 near
Cambridge, England and was one of the largest medieval fairs that were market centers for
merchants and in 1500 the London Bridge was lined with shops across the Thames River in
London, England. Markets are as old as history. They have been incessantly reinvented
merchants bring new ideas and innovations which result in new marketplaces. We believe the
single most exciting area, the one which affords us the best opportunity to create enormous
value going forward, is reinventing, remerchandising and reinvesting in our existing properties,
just as investors, owners and developers have been doing for hundreds and hundreds of years.
The department stores at our malls are an example of this opportunity. We frequently meet
with all department store chains during the year as we strive to improve performance for
both our centers and the department stores themselves. As these stores experience continual
change, we are excited about the
opportunities for GGP created by
these changes. GGP continues to
take what was once departmentstore space and create additional mall shop space, big box retail space, restaurant space,
theatres, outdoor retail villages, residential components, office space, hotels or simply replace
one department store with another to enhance the image and productivity of the property.
All of these alternatives benefit GGP and increase the long-term viability of our malls.
Alternative uses with potentially greater density of space are cash flow generators available
to us that will contribute to our future growth.
A good example can be found in the recently announced Federated department store closings.
On average, the announced closings occurring in GGP centers are at properties that have sales
of approximately $500 per square foot  more than 14% higher than our average center and
more than 35% higher than the industry average. The trade area populations in these locations
are estimated to be growing at 8.4% between now and 2009 versus 4.8% nationally, a difference
of 75%. On average, these same trade areas show household incomes approximately 11% higher
than the national average. This is just one of many reasons we remain excited about the future
of our properties.
Touring our properties reinforces the strength and solidity of our business. When we visit our
malls we see first hand how our customers embrace what we are doing. The redevelopments
and village components that we add to our existing centers are desired by our shoppers and
our retailers. The consumers want the restaurants and retail concepts that are located in our
properties. Because of the viability and
strength of our centers, the retailers are
introducing almost 100% of their new
concepts in the mall, creating centers that
are fresh and innovative. They are at the
forefront of retail and consumer desirability.
We believe GGP must give the consumer and
the retailer what they want, when they want
it and where they want it. Conditions for the
consumer and retailer are close to ideal. The
consumer continues to motor along, fueled
by positive job growth and ample disposable
income from home equity increases and a
willingness to acquire more debt. Mall space
supply remains in equilibrium and is not
outpacing retail tenant demand. Occupancy levels are high, and retailers are rolling out unprecedented levels of new branding concepts.
Spend time in our malls. Monitor the pulse of our business. You will see from firsthand
observation that GGPs pulse is strong.
Densification. This will define our properties for many, many years into the future and gives
us the ability to bring more square footage onto our existing properties. Our malls are in the
enviable position of being situated on sites typically ranging in size from 100 to 120 acres
of land. As we witness changing demographics, changing commuting patterns and the desire
of people to live, work, play and learn in convenient environments, we have the ability and the
responsibility to provide this to them. Our properties lend themselves to change. Our centers
are constantly evolving. We have continually brought new ideas and innovations to our land.
As previously mentioned, recaptured department store space can be reincarnated into village
retail environments, entertainment areas, big box retail formats, residential units, hotels
and/or office complexes. The array of choices we have is large. We treat each and every market
individually, giving us alternatives that are completely unique in the real estate industry.
Learning is critically important to improving. We learn new things every day. One area of our
business that is a continual source of new lessons is the master planned community business.
GGP is expanding from being the creator of special places and experiences through retail
alone, to being the creator of special places and experiences for living, working, playing and learning in master planned communities. The special places we
now strive to create include destinations where residential, office,
hospitality, retail and civic uses can find effective expressions. This
allows GGP to benefit from extending the experience we create in retail
to other asset classes as well. Instead of deriving financial returns from
retail alone, we now benefit from the economic potential of effective
development of other uses. We now capture the value of place and
experience over thousands of acres, instead of hundredsand what
we learn from the thousands of acres we apply to the hundreds.
Jim Rouse envisioned Columbia, Maryland as a place to grow people.
His vision for Columbia has succeeded. Because of his success,
Columbia today is a thriving community of nearly 100,000 residents.
But today, Columbia is much different in its needs and its desires than it was 37 years ago as
it was being planned. Today, the town center of Columbia comprises approximately 500
acres and this land is vastly under-utilized. There is a demand for high-rise office and living
spaces, learning centers, hospitality and village retail. People want a more urbane expression
and vitality in their town centers as they densify. The community has the opportunity to become
bright lights, little city.
The Woodlands has been developed as a city in the forest. The Woodlands took a village
approach to living over 32 years ago and today the villages of the Woodlands form a community
of unparalleled living. Much like Columbia, we envision the Woodlands taking on a much more
urbane expression while remaining dedicated and nestled in the splendor and glory of its
natural environment.
Summerlin, (Las Vegas) Nevada is also a community of beautiful neighborhoods in natural
surroundings. Summerlin is fast approaching a population of 100,000 people and is quickly
emerging as an active, vibrant suburban community. As the community has grown, we have
recognized the importance of growing with the community and we are establishing the first phases
of Summerlin Town Center. We are planning and developing more extensive retail, office, multifamily and hospitality uses. We continue to redefine the experience as the scale of the community
becomes more apparent and we are not viewing the mall as an isolated microcosm but as the
heart of the shopping, entertainment, business and residential fabric of the Town Center.
Columbia, The Woodlands, and Summerlin are leaders in advancing the practice of
sustainable development. By designing with nature, they demonstrate that community building can meet market opportunities better when the planning process includes thoughtful
stewardship of our natural resources. These developments have won international acclaim
by achieving our core human aspiration to be in harmony with nature while also enjoying
the rewarding experience of participating in vibrant, special gathering places. We are excited
about the prospect of applying these lessons to our newest community, Bridgelands, as well
as to the communities of merchants at our malls.
In addition, GGP is actively taking steps to adopt high performance, green design building and
operational practices in our existing, redevelopment and new development projects. GGP is
exploring new technologies and practices to help ensure an efficient use of resources, promote
the health and comfort of our building users and reduce negative impact on the environment,
while reducing energy and operating costs. We believe these efforts are both environmentally
responsible and sound business practices that will benefit our shoppers, retailers, employees
and investors.
We hope this letter gives you a better understanding of how we look for ways to change and
improve GGP. These improvements touch all aspects of our businessnot just the obvious
ones. We are improving as a company, and we have the best and most talented people in
the mall industry. We are improving as managers of our employees, and our employees are
in turn improving the assets in each and every community in which we operate. People are
the foundation of GGP.
In 513 BC, Heraclitus of Greece observed, There is nothing permanent except change.
GGP continues to change. We make certain that as we change, we live our vision and our
mission each and every day. People Creating Special Places and Experiencesthis is our
vision, and this is what we are accomplishing. Enjoy our Great Gathering Places.
John Bucksbaum
Chief Executive Officer