This year, Stanley Black & Decker is celebrating its
175th anniversary. Founded in 1843, in the wake
of the First Industrial Revolution, our Company
has prospered through the ebbs and flows and the
triumphs and tragedies of recent world history.
For 175 years we have navigated through the
challenges and opportunities afforded by the
relentless social and technological advances of
the Second and Third Industrial Revolutions and
we are now in the midst of the Fourth. The pace
of technological change that is occurring today is
breathtaking and unprecedented. Moreover, it is
accelerating as the rapidly plummeting cost of data
storage, increases in communications bandwidth
and the impact of Moores law on computing power
all combine to drive the digital revolution.
In fact, technological change is now advancing at an exponentially increasing
rate to a point where societys relatively linear ability to absorb the change is
beginning to become overwhelmed. It is all moving so fast that individuals,
businesses, governments and society are having a difficult time keeping up, and
there are entire institutions and growing numbers of people that are getting left
behind. Its tough to keep pace  to keep individual skills, business models,
social and economic policies, and regulations and laws current and relevant.
To use corporations as a case in point, the average life span for a Fortune 500
company has declined to 15 years today from 67 in the 1920s. And the vast
majority of companies that comprised the Fortune 500 list in 1955 have
disappeared in one form or another. New, emerging companies are leveraging
the power of technology and their new brands and business models to
completely upend legacy businesses that have been successful for generations.
Its an exciting time, full of opportunity albeit one that is also fraught with risk.
So, lets rewind to 1843. When Frederick T. Stanley founded the original Stanley
Bolt Manufactory in New Britain, Connecticut, which was later incorporated
as Stanley Works, Im sure he could not have anticipated that the Company
would grow to a $13 billion revenue global industrial with a $26 billion market
capitalization and approximately 58,000 employees across the globe. But it
did. Its a remarkable feat when you think about it, and even more so when you
consider that approximately 80% of the revenue growth and 90% of the market
Visit 2017yearinreview.
stanleyblackanddecker.com
to view stories and pictures that
bring exciting aspects of the
Stanley Black & Decker story to life,
explore our financials, review our
sustainable practices, and
read about our businesses and
our plans for growth.
VISIT THE 2017 YEAR
IN REVIEW WEBSITE
2
value creation has occurred since the year 2000 in just 10% of the Companys history
as measured in years. The Company has seized the opportunity and enjoyed this
monumental recent growth spurt just as the challenges cited above have increasingly
become more pronounced.
What we discovered when we analyzed the causal factors for our success is that the
foundational attributes that defined our Companys culture all the way back to
inception, have not only stayed with us but have become amplified over the years.
These four cultural traits are (1) bold breaks the mold, (2) we cut through challenges,
(3) reliable to the core and (4) we join forces. Said another way, we strive to be bold
and agile while at the same time thoughtful, disciplined and collaborative.
This special combination of attributes is a powerful blend for tackling the challenges
of the day, just as it has been effective for 175 years. Last year, we launched a project
to define our Companys purpose  For Those Who Make The World, which was
extracted from a comprehensive review of our history. We empower the makers and
creators, those who are doing the work of creating and shaping the world around us.
We produce the hardest working, most innovative tools, products and services for the
worlds hardest working people. Its this deep sense of place, building and creation that
underpins our values and operating model, and we have rededicated ourselves to our
Purpose in all that we do. There is a subtle essence of humility embedded in this as we
dedicate ourselves and our Company to support the true heroes of the world, the
makers and creators.
Purpose has begun to transcend our enterprise and our business strategies. In
Engineered Fastening, we are working to penetrate the electric vehicle marketplace,
and our Security business has a renewed and relentless focus on making the world
a safer place and, of course, tools are at the heart of making the world. We are
approaching innovation with a new, exponential framework that deploys contemporary
organizational techniques to commercialize innovations that are both sustainable and
provide a societal benefit. We have amplified our efforts to advance diversity and
inclusion and have launched five Employee Resource Groups across the company over
the last two years, including our Womens Network, our African Ancestry group and our
most recent one, Pride & Allies, in support of the LGBT+ community. We are also
engaging in new dialogues and activities with our employees and communities. And
we have increased our commitment to sustainability with new strategies and goals,
including pledges to become carbon positive by 2030 and to stand up for human
rights and equality.
As our message for our 175th anniversary says, Times Change. Our Purpose Hasnt.
And we empower our teams to live and drive our purpose for our Company, our
shareholders, our employees and society. It is a fulfilling challenge to meet and one
we are passionate about.
Stanley Black & Decker. For Those Who Make The World. Since 1843.
2017: Strong Financial Performance
The Stanley Black & Decker team delivered above-market organic growth, fueled by
innovation and strong commercial execution, seamless acquisition integrations, and
financial success. This resulted in a strong year of value creation for our shareholders,
where the Company delivered 50% total shareholder return in 2017.
Our 2017 financial results included 12% total revenue growth, with 7% organic
growth and a 7% contribution from acquisitions, record earnings per share* and
operating margin rate,* and strong free cash flow conversion.* In addition, 2017
highlights included:
 Outperforming our peer group and the overall S&P 500, with SWKs share price up
48% for the year versus 18% for our peers and 19% for the S&P 500
 Reshaping our portfolio by completing the divestiture of our Mechanical Security
business in February, and closing the acquisitions of Newell Tools and the Craftsman
brand in March 2017, adding three iconic brands to our portfolio
 Reaching agreement to purchase the industrial business of Nelson Fastener Systems
in December 2017 for $440 million
Growth was once again the engine behind our operating results, with all businesses
delivering organic growth and most of the portfolio experiencing share gains.
Tools & Storage generated an impressive 9% organic growth rate, with every region
and business unit delivering increases, including 9% growth in North America,
Europe and Emerging Markets. Total revenue growth was 19% including
contributions from the Newell Tools and Craftsman brand acquisitions.
Our organic growth was driven by innovation  both a steady stream of core
innovations and our recently commercialized breakthrough innovation, FLEXVOLT.
In addition, we saw continued success with mid-price-point product launches in
Emerging Markets, strong commercial execution around the globe and benefits from
our aggressive, global efforts in the e-commerce channel.
Turning to Industrial organic growth, Engineered Fastening was up 4%, led by strong
automotive systems sales supporting our customers new model launches,
automotive fastener growth that was 430 basis points in excess of light vehicle
production, and industrial vertical growth in all geographies. Oil & Gas (+8%)
experienced higher project and inspection activity within North America, and
Hydraulics (+20%) became the most recent business to demonstrate the power of
applying SFS 2.0 Commercial Excellence principles.
Excluding the impact from the Mechanical Security divestiture, growth within
Security was 4%, which included organic growth of 1%, as continued improvements
in field execution and targeted commercial wins were supplemented by our strategy
to execute small bolt-on acquisitions to bolster the recurring revenue portfolio. The
business remains focused on continuing the growth momentum and returning to
margin expansion in 2018 and beyond.Our overall operating margin rate* rose to a record 14.8%, up
40 basis points versus 2016, supported by record operating margin
levels in Tools & Storage and Industrial. We demonstrated the ability
to deliver meaningful operating leverage through robust productivity
and cost control, while offsetting commodity inflation and continuing
to make targeted investments to support future growth.
Free cash flow conversion was approximately 100%* which
supported our 50th consecutive annual dividend increase and a
series of strategic acquisitions. Our cash flow return on investment
remained strong at 13.8%, which is in line with our long-term targets.
These 2017 results show the power of our strategy, SFS 2.0
operating system and our value creation model  a year where the
teams delivered strong organic growth with margin expansion, and
successful acquisition integrations  which fueled mid-teens
earnings per share* growth.
Continuing to Deliver Top-Quartile Performance
SFS 2.0 differentiates our performance and supports our day-today execution. Digital Excellence, Breakthrough Innovation,
Commercial Excellence, Core SFS and Functional Transformation
work in concert to sustain above-market organic growth, support
margin expansion and deliver strong free cash flow generation.
Our well-established value creation model has produced strong
shareholder returns. It starts with our world class brands, attractive
growth platforms, and scalable and defensible franchises.
Importantly, it leverages the power of SFS 2.0  enabling the
achievement of the Companys long-term financial objectives.
We also employ an investor-friendly capital allocation approach.
Our historical practice, which we intend to continue, has been to
return 50% of our capital to shareholders in the form of dividends
and/or opportunistic share repurchases, with the remaining 50%
earmarked for acquisitions to further strengthen our business
portfolio and fuel growth.
Our strategy, value creation model and operating system have
shown a level of consistency through the past decade plus, but
have also grown and evolved as times have changed.
Growth Catalysts: 2018 and Beyond
Leveraging SFS 2.0, we are executing on a series of growth
catalysts that we believe will sustain our above-market growth
potential for the foreseeable future.
FLEXVOLT, our recent breakthrough innovation initiative,
represents the fastest adoption for a new product launch in
DEWALTs history. It is powered by a flexible battery system that
delivers 60 or 120 volts for high power tools and three times the
runtime when the 60-volt battery is used within our 20-volt power
tool system. This innovative product launch was a high growth
driver in 2017, and is also stimulating incremental demand for our
20-volt cordless offerings. We will continue to expand this system
in the future with the ultimate goal of eliminating the need for cords
on jobsites and thus making a dramatic and positive impact on
worker safety and efficiency.
The Craftsman transaction gives Stanley Black & Decker the rights
to develop, manufacture and sell Craftsman-branded products in non-Sears Holdings channels. In 2017, we successfully pursued retail partnerships
with a major home center, a formidable co-op hardware retailer and the leading
e-commerce player. We focused on developing 2018 commercial plans, designing an
impressive and comprehensive product portfolio, adding capacity and preparing the
supply chain to support our second-half rollout. We are working with passion and
excitement to enable this iconic brand with its proud and beloved history to soon
reclaim its rightful place in American homes, garages, factories and automotive shops.
To support the overall growth in the tools business and the rollout of the Craftsman
brand, we continue to expand our US manufacturing footprint. Stanley Black &
Decker has been a proud US manufacturer for 175 years and in fact has added more
than 1,200 jobs in the US over the past three years. Looking ahead, we expect to
add 1.5 million square feet of new manufacturing and distribution capacity in 2018,
which will support our goal to increase US tools production to 50% of our total US
tools volume over the next three years.
The Newell Tools acquisition integration continues to proceed on or ahead of plan,
which makes us confident that we will achieve our targeted $80$90 million in cost
synergies. We are now turning our attention to capturing the revenue synergies from
the Lenox and Irwin brands by leveraging these products within our global customer
base and bringing new offerings to enhance organic growth within Tools & Storage.
In December, we reached an agreement to purchase the industrial business of
Nelson Fastener Systems. This bolt-on transaction will enhance Engineered
Fastenings presence in general industrial end markets, expand its portfolio of highly
engineered fastening solutions, and deliver cost synergies.
We continue to be encouraged by the prospects for value creation within the M&A
pipeline. Our focus remains on strengthening the core through bolt-on transactions
within Tools & Storage, Industrial and Security, as well as pursuing adjacency
opportunities that possess sound industrial logic and fit with our value creation model.
In addition, we continue to invest in additional opportunities aligned with our SFS
2.0 operating system. We are optimistic that we will be able to commercialize these
Breakthrough Innovation, Commercial Excellence and Digital Excellence programs
to generate additional growth prospects in the future.
Becoming Recognized as One of the Worlds
Most Innovative Companies
We are building a culture in which we strive to become known as one of the worlds
great innovative companies. Our opportunity is to embrace this environment of rapid
innovation and digital transformation to deliver disruptive innovation to the market.
In 2017, we increased our R&D expenditures by $48 million, a 23% increase versus
2016 and a total two-year increase of 34%.
We now have ten breakthrough innovation teams covering all businesses and
multiple worldwide locations focused on developing innovations that each have the
potential to deliver greater than $100 million in annual revenues. These teams are
separated from the day-to-day organization to remain focused on the pulse of what
our customers want and need, and are working with leading universities and venture
companies to advance breakthrough technologies. We are encouraged by the
prototypes that have been generated by the teams and look forward to successfully
commercializing the most promising opportunities in the coming years. See more at
2017yearinreview.stanleyblackanddecker.com
.
Our Digital Accelerator team in Atlanta has now grown to approximately
100 employees. This team of world class technology talent has been successful in
demonstrating the value of their skillsets to the Companys core operations by
infusing digital capabilities into our products, processes and business models. The
collaboration of our Digital Accelerator team across the Company has been so
successful that approximately half of the original employees in the Digital
Accelerator now report directly into our businesses. We expect to continue to grow
our digital team and will add more experts that specialize in applying emerging
technologies such as artificial intelligence, machine learning, robotics and advanced
data science. Our innovation and digital initiatives that impact manufacturing, or Industry 4.0,
are becoming increasingly important in todays operating environment. We
continue to make good progress in our three lighthouse manufacturing
facilities, applying the latest in robotics, manufacturing execution systems (MES),
3-D printing, innovation labs and maker spaces to drive the next wave of
flexibility, cost efficiency and quality improvement. We recently announced the
creation of an Advanced Manufacturing Center of Excellence, or Manufactory
4.0, in Hartford, Connecticut. Our Manufactory 4.0 will be the epicenter for the
latest technologies and processes and accelerate the adoption and scaling of
new technologies across our manufacturing footprint.
Industry 4.0 is a key enabler for our make where we sell strategy. This strategy
makes good business sense as it shortens the supply chain, lowers the
environmental impact, mitigates currency exposure, and is a cost effective
alternative after including the improvements in efficiency and advances in
technology. Additionally, recent expansions of tool production in the US and the
UK have shown that our end users generally prefer to buy products made locally.
Certain of our potentially disruptive breakthrough innovations do not have a
natural commercialization pathway within our existing business models or would
be constrained from successfully scaling with the speed required for success if
incubated within our core organization. We have seen this first-hand with some
of our promising innovations and digital products. To address this, we have
created an Exponential Organization, located in Silicon Valley, with a heightened
priority on innovations that have the potential to provide a significant societal
impact. This organization will work closely with our Chief Technology Officer,
Stanley Ventures, the Digital Accelerator, and our core businesses to incubate
and advance these types of innovations.
We are encouraged by the actions taken in 2017 to enhance our growth culture
and work toward becoming recognized as one of the worlds great innovators.
We are making investments to ensure that we stay abreast of the fast-evolving
digital and technology landscape to position the Company to disrupt ourselves
before others do.
Elevating Our Commitment to Corporate Social Responsibility
At Stanley Black & Decker, we have long been committed to improving the
communities in which our employees live and work, with a keen focus on
environmental health and safety. Last year, as part of our 22/22 Vision and
activating on our broader purpose in society, we began elevating our already
strong commitment to corporate social responsibility. We are encouraged by our
progress as the Company has been recognized on a number of notable lists,
including Forbes Americas Best Employers for Diversity (#67), Barrons 100 Most
Sustainable Companies (#30), Fortunes Most Admired Companies, Dow Jones
Sustainability Index (7th consecutive year) and Moguls Top 100 Innovators in
Diversity & Inclusion (#4) and their Top 100 Companies for Millennial Women (#1).
For us, corporate social responsibility describes our organizations continual
focus on how our business can be a force for good  creating value beyond
profits, including environmental and social value, and generating a positive
impact for shareholders, the environment and greater society.
As part of this effort, we reevaluated our existing sustainability and philanthropic
work with the goal of becoming a leading purpose-driven company. We want
to be recognized for our work to inspire makers and innovators to create
a more sustainable world, in line with the United Nations 2030 Sustainable
Development Goals. In this regard, we recently established a specific
Corporate Social Responsibility Strategy, which is focused in three key areas:
Empower Makers: Empower Makers and Creators to Thrive in a Changing World
Industrial and technological innovations are rapidly changing the nature of work
and jobs. Globally 10 million jobs in manufacturing remain unfilled due to gaps in
skills. Stanley Black & Decker is uniquely positioned to help close this gap. We
recognize that our own workers, as well as those in the communities where welive and work, will require education, learning, upskilling and experience to
ensure they can thrive in this new context. We are committed to helping
employees and people of the world gain the skills and expertise needed to
secure jobs and revitalize communities.
Innovate with Purpose: Innovate Our Products to Improve Lives
Global sustainable development challenges remain profound, with one in five
people living in extreme poverty and 40% of the global population affected by
water scarcity serving as jarring examples. We make products and services that
help create and shape the world, and have the ability to partner even more
broadly to create new solutions to meet global societal needs. Matching social
impact to our business also inspires our employees and presents new and
exciting commercial opportunities. Through our work, we will improve the
positive societal impacts of our products across their lifecycle, including design,
use, and end of life.
Create a More Sustainable World: Positively Impact the Environment
Demand for already constrained resources continues to rise as does the
economic impact of environmental degradation. Climate change will potentially
cost the global economy $12 trillion by 2050. This represents a debilitating threat
to society as well as a business risk for the private sector. Our sustainability
initiative no longer simply seeks to reduce negative impact, but to positively
impact the environment across our footprint. Through our ECOSMART program,
Stanley Black & Decker has made considerable contributions, but to keep pace
with changing dynamics, we are raising our ambitions and working towards
positive environmental impacts across our operations.
We believe our commitment to corporate social responsibility will provide
considerable benefits, including positioning Stanley Black & Decker to continue
to deliver top-quartile financial performance.
In Closing
As I reflect upon 2017, my first full year as Chief Executive Officer and my
19th with Stanley Black & Decker, I am humbled to have the privilege and
responsibility of leading this great Company forward. I am pleased that we have
laid the groundwork for an inspiring and fulfilling future during these exciting
times. We enter 2018 positioned well for another successful year, one in which
we celebrate our 175th anniversary while executing on a myriad of compelling
growth and value creation opportunities.
Our deep and agile leadership team along with our entire employee base remain
committed to delivering above-market organic growth with operating leverage,
continuing to successfully integrate our recent acquisitions, and generating
strong free cash flow. Purpose has energized our people as we pursue our
22/22 vision to become a great human-centered industrial company while
delivering $22 billion of revenue by 2022 with margin expansion. To achieve this
vision, we know that we need to continue to execute, evolve and change. In this
regard, we will focus on becoming known as one of the worlds leading
innovators, continuing to achieve top-quartile performance and elevating our
commitment to corporate social responsibility. We will leverage our strong sense
of history and place, our growing talent base, our Stanley Fulfillment System and
the power of the platform we have built in order to propel the Company
successfully into the future.
2017 GLOBAL PRESENCE
US
James M. Loree
President & Chief Executive Officer