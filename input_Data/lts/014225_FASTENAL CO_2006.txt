  2006                                       President's Letter to Shareholders

In most respects, 2006 was a good year for               continues to improve the store conversion
Fastenal. The year started out strong but as the         process. These improvements have served to
overall economy in North America slowed in the           lower some costs and reduce the existing store
second half of the year, so did Fastenal. This           inventory before we convert them to CSP2.
caused us to have lower sales and earnings
growth than predicted. Even though this was              Another new store project called CSP3 was
not a stellar year for Fastenal, there are several       started in 2006. At this time, this project will be
initiatives in which we made great progress.             limited to 8 to 10 stores. It is much like CSP2,
These initiatives are like pieces of a puzzle we         only larger. The store size will average 12,000
are putting together. Individually they do not do        square feet and stock more than 14,000 different
much, but when linked together correctly they            items. Because of the low number of stores in
help execute our overall strategy; to become the         this project, we will develop a labor model as we
best distributor of industrial and construction          learn more. Our plan is to continue working on
products in each of the markets we serve. I              this project, but not to increase the number of
would like to share some of these initiatives with       conversions in 2007.
you as I believe they are key contributors to our
continued growth.                                        Our sales people did a great job of increasing the
                                                         account activity in 2006. During the fourth
                                                         quarter we increased our active accounts by
Our 2006 sales growth was 18.8%. This gave us
                                                         18.5% over the same period in 2005. This is one
total revenue of $1.8 billion which is an increase
                                                         of the largest increases in our history.
of more than $286 million over 2005. Our
reported net earnings for 2006 was $199 million,
                                                         We saw mixed results in the area of asset
an increase of 19.3% over 2005. We thought we
                                                         management. We continued to lower our days
would be able to produce more earnings leverage
                                                         outstanding for accounts receivable as the people
than we did, but with the slowing of sales growth
                                                         in our call center continue to speed up the
in the second quarter, combined with the CSP2
                                                         collection process. We reduced our days
expansion, this was not possible.
                                                         outstanding by four days since December 2004,
                                                         when we first implemented the central collection
We opened 245 stores in 2006. We opened 222
                                                         process.
stores in 2005. We started the year opening
stores on schedule but fell behind in the second
                                                         Our overall inventory grew faster than we had
quarter and had to work hard to catch up in the
                                                         planned. Most of this growth was due to our
fourth quarter. This is a very inefficient way to
                                                         aggressive plans of opening stores, converting
open our stores, and we lose sales contribution
                                                         CSP2 stores, and expanding the stocking levels
from these stores when they are opened late. I
                                                         in distribution. In 2006 we added $94 million in
believe the plan we put in place to prevent this
                                                         inventory, an increase of 26.1%. We have put
from happening in the future is a good plan. A
                                                         plans in place to slow this growth and I believe
milestone for Fastenal in 2006 was the opening
                                                         we will improve our inventory turns in 2007.
of our 2,000th store in Seattle, Washington. This
is a significant accomplishment for Fastenal and
                                                         In 2006 our Chief Operating Officer, Nick
it was a very proud moment for our team. I
                                                         Lundquist, reorganized the National Accounts
traveled to Seattle for the grand opening and
                                                         group, our National Construction Group and any
enjoyed visiting with some of our local
                                                         other people that were assigned to large account
customers. This opening is also special for me
                                                         business. The key accounts program began in
because I was born in Seattle and have spent a
                                                         1995 and became fragmented from an
lot of time in the Northwest.
                                                         organizational standpoint as it grew. This
                                                         reorganized group is now referred to as our
The people working on the CSP2 store
                                                         Strategic Accounts Group and is lead by Steve
conversions are doing a very good job of staying
                                                         Rucinski. Steve is a 26 year employee with a
on schedule and the project is going well.
                                                         proven track record in key account sales.
However, I believe the number of conversions
completed in 2006 may have contributed to the
delay in new store openings. We converted 163
stores to this format in 2006. This team




This group is charged with signing up large               The biggest project we started in 2006 and the
accounts for the entire company. The early                one that will produce the most to improve our
results show a large increase in the number of            customer service is the inventory expansion
quotes to potential customers, but the real               project we undertook in our Indianapolis
measure will come in the first half of 2007 when          warehouse. We chose Indianapolis for this
we review how this piece of our business is               project because of its central location to our
growing.                                                  customer base and the good interstate highway
                                                          system out of Indianapolis. In mid 2005 we
Over the last several years we have taken many            started studying the feasibility of expanding our
steps to strengthen our name recognition in the           stocking model in Indianapolis to include all of
marketplace. Four of the larger steps are: 1)             the items in our catalog. Up until this time, we
lease better store locations, 2) add better signage       had only stocked the best selling items and the
to increase customer awareness, 3) place decals           stores bought the other items as they needed
on our large fleet of vehicles creating moving            them. We estimated we would save 9% on the
billboards, and 4) develop better packaging.              product and another 13% on freight if we
                                                          purchased it centrally and hauled it with our own
In addition, in 2006 we took a much bigger step           fleet. Based on this information, and our strong
and became the primary sponsor of a NASCAR                commitment to continually improve our
Craftsman Truck Series race truck. After doing            customer service, we made the decision to
research on the demographics of racing, we                expand the number of items we stock in that
determined the truck series provided the best             facility from 28,000 to more than 120,000. We
demographic match to our customers and was a              estimated it would require about $25 million of
good entry point into motor sports marketing.             additional inventory in Indianapolis to
We signed a two year sponsorship deal with                accomplish this. The project started late in 2005
Bobby Hamilton Racing and became the sponsor              and has made steady progress throughout the
of the #18 Dodge driven by Bobby Hamilton,                year. This has been a very difficult project and
Sr., the 2004 Truck Series Champion. We                   has caused many service problems in 2006.
invited some of our larger product suppliers to
help co-sponsor the team. Our marketing goal is           For us to gain the maximum benefit from the
to familiarize people with Fastenal and our               inventory expansion project we need to continue
quality brands like Bosch Power tools, Lenox              to improve our transportation system. We
saw blades, Craftsman hand tools, etc. The                worked hard on this goal in 2006 by improving
season started off well and we received great             our delivery times, providing five-day a week
television coverage as a new sponsor. Then, less          delivery service to more stores and developing
than a month into the season, our driver Bobby            scanning hardware to better track the shipments.
Hamilton, Sr. was diagnosed with cancer. After            We also started our first consolidated air
a courageous battle, Bobby passed away, and on            shipments from Indianapolis to our facility in
that day we lost a great friend.                          Seattle. By contracting with commercial
                                                          airlines, we are able to provide second day
As part of our motor sports marketing initiative          shipments at very reasonable prices. We plan to
our marketing team worked hard to produce and             expand this service in 2007 to all of our
direct our first television commercial. Although          distribution centers in western North America.
I am no marketing expert, I believe they did a            Although I am pleased with the progress in our
great job creating a very high quality commercial         transportation system, there are still many areas
with a clear message about who Fastenal is. The           we are working to improve that will provide
main focus on all of our marketing efforts is two-        better service and make us more efficient.
fold. First, is to tell our story with the goal of
driving traffic to our stores and second, is to           We completed many projects in distribution in
create brand awareness for our sales people when          2006, but the largest project was the opening of
they are making calls on potential customers.             our new distribution center in California. In
We have found it much easier to get an                    November 2005, we purchased a 320,000 square
appointment with a potential customer if they             foot warehouse in Modesto, California. We
have heard of Fastenal in the past.                       currently occupy 140,000 square feet of the
                                                          building and have leased out the remaining
                                                          space.

 

Our distribution team spent most of the year             that will limit the growth of this business will be
designing and installing the most efficient              our ability to find the quality suppliers we need
warehouse system in our network. The project             to produce our product.
was completed in November 2006 and we started
operation in December. This new facility                 We now have a second office       in Kang Shan,
replaced our existing warehouse in Fresno,               Taiwan where we installed a       product testing
California.                                              laboratory. This investment       was made to
                                                         improve our turnaround time in    quality control;
The Information Systems people worked on                 which will speed up shipping      time from our
many projects, but I believe the most exciting           Asian suppliers.
area of development is hand-held sales devices.
They are currently testing hand-held computers           Our manufacturing division had another good
that will give our sales people much better              year. They produced sales growth in 2006 of
information. Armed with this new device, a               26%. They also moved our manufacturing
sales person in the field can price product, check       facilities in both Indianapolis and Fresno.
order status, check product availability and do          Indianapolis moved within our current facility to
almost anything else they can do from their desk.        a much larger space to accommodate the
This is another big step in both customer service        growth. The Fresno facility moved to our new
and labor productivity.                                  building in Modesto. Both of these moves
                                                         provide us with larger and improved
The people in our accounting department have             manufacturing space.
been busy developing and implementing new
systems to increase efficiency in both accounts          I have talked about many areas of Fastenal, and
receivable and payable. Using a new automated            even more projects, but none of this would be
scanning system we are able to collect data from         possible without great people. In 2006 we hit
incoming checks and invoices and shorten the             another milestone by surpassing 10,000
time it takes to process the transaction. This new       employees for the first time. Our dedicated staff
system has not only helped us improve our labor          in the Fastenal School of Business had another
efficiency, but has also allowed us to improve           record training year. They developed new
our ability to take advantage of early pay               training programs for the CSP2 managers,
discounts offered by our suppliers.                      additional product trainings, and more than
                                                         doubled the training that is done in the training
Product development continues to be a big part           centers outside of Winona. Our commitment to
of our growth strategy. Our product managers             training our people continues to be one of the
had a very busy 2006 splitting their time between        main reasons we perform at a high level.
CSP2, CSP3 and the Indianapolis expansion
project. I believe they have done a very good job        We have made great progress in 2006 in our
of determining not only which brands to support,         effort to build the best distribution organization
but also which products we should stock in each          in our industry. But no matter how good our
area and in what quantity. In 2007 they will             systems are, they cannot perform without our
focus on streamlining the process.                       great people. I am very proud of everything our
                                                         dedicated employees accomplished. I strongly
Many of the products we buy in North America             believe if we continue to hire people that share
were originally produced in Southeast Asia and           our values, give them strong support and put
then imported by one of our suppliers. Since the         them in a position to make good decisions, we
early 1990's, we have worked to go-direct to the         will always be successful.
manufacturer when volume justified. The go-
direct strategy took a huge leap forward in 2003         Thank you for your belief in us and we promise
when we established a trading company in                 to work hard for the continued success of
Shanghai, China called FASTCO Trading                    Fastenal.
Company.         FASTCO Trading Company
continues its rapid growth. In 2006 they
imported 4,500 containers, an increase of 32%
                                                         Willard D. Oberton
over 2005. At this point, I believe the only thing
                                                         CEO and President

