DEAR SHAREHOLDERS,
2015 was a busy and rewarding year for Regeneron as we made major strides in advancing our mission of
bringing important new medicines to people with serious diseases, over and over again.
We delivered EYLEA (aflibercept) Injection, our therapy for serious
vision-threatening diseases, to more and more patients, and we launched
PRALUENT (alirocumab) Injection, a first-in-class therapy for uncontrolled
LDL cholesterol in certain patients.
In the next few years, we anticipate additional approvals for a number of new
therapies. In 2016, we look forward to potentially launching sarilumab for
rheumatoid arthritis, pending U.S. Food and Drug Administration (FDA) review
of our application. We also expect a potential U.S. regulatory submission for
dupilumab in atopic dermatitis, a serious form of eczema, for which we have
been granted a Breakthrough Therapy designation by the FDA.
Our pipeline of a dozen clinical-stage antibodies continues to progress, with
important programs in eye disease, cancer, infectious disease, pain,
cardiovascular disease and inflammation. We also continue to invest in
technology and innovation that will position us to bring needed new
medicines to patients for many years into the future. Likewise, we have made
important infrastructure investments to ensure our long-term success,
including adding two new buildings at our headquarters in Tarrytown, New
York, and expanding our Industrial Operations facilities in Rensselaer, New
York, and Raheen, Ireland.
We have always run Regeneron by the principle of doing well by doing
good. In addition to our work to invent new and needed medicines, we focus
on improving our world and operating with the highest standards of integrity. 
This year, for the first time, our Annual Report integrates reporting on our
citizenship priorities and aspirations, in addition to our financial and business
performance.
We invite you to read about our 2015 accomplishments, financial
performance and citizenship efforts below and in our 2015 Annual Report on
Form 10-K, available on the Investor Relations portion of our website.
Unfortunately, there was also some sadness in 2015. Our longtime friend,
mentor, co-founder and Board member, Dr. Alfred G. Gilman, passed away in
December. Dr. Gilman was a Nobel Laureate who made lasting contributions
to science and medicine. On a personal level, we all benefited greatly from
Als counsel and wry wit over the years, and we will miss him greatly.
We look forward to updating you on our progress as we continue building
Regeneron into a leading global biopharmaceutical company.
Sincerely, 