TO OUR
STOCKHOLDERS

Host Hotels & Resorts is the premier lodging real estate investment company with one of
the world�s best hotel portfolios and with the brightest and most dedicated workforce in
the business. Our strategy is to own the most geographically diverse portfolio of iconic and
irreplaceable hotels in the United States, utilizing our scale, integrated investment platform
and investment-grade balance sheet to grow through organic initiatives and disciplined
DFTXLVLWLRQV:HDUHFRQCGHQWWKLVVWUDWHJ\FUHDWHVORQJWHUPYDOXHIRURXUVWRFNKROGHUV
Beyond the quality of our assets, our scale is a key differentiator, as the only lodging
REIT with a dedicated enterprise analytics team. Our in-house expertise spans investPHQWVDVVHWPDQDJHPHQWGHYHORSPHQWGHVLJQDQGFRQVWUXFWLRQFRUSRUDWHCQDQFHOHJDO
tax and accounting creating a fully integrated investment platform.
We are the only lodging REIT with an investment-grade balance sheet, which we are
committed to maintaining as we believe it enables us take advantage of value-creation
opportunities throughout the lodging cycle. While we have historically operated from a
SRVLWLRQRICQDQFLDOVWUHQJWKDQGcH[LELOLW\ZHHQWHUHGZLWKWKHVWURQJHVWEDODQFH
sheet in Host Hotels� history, providing us with substantial optionality to continue executing on our strategic initiatives.
To capitalize on these competitive advantages requires a best-in-class organization. We
KDYHIRVWHUHGDXQLTXHFXOWXUHWKDWHQFRXUDJHVHPSOR\HHVWREHLQQRYDWLYHQLPEOHHICFLHQW
and entrepreneurial with the collective goal of generating superior returns for investors. We
are committed to the highest quality and performance. Our focus has been, and remains,
owning a portfolio of superior, geographically diverse assets, that is supported by an investment grade balance sheet and culture that rewards and demands success � this is, and will
continue to be, what makes Host Hotels & Resorts the lodging REIT of choice for investors.
STRONG PERFORMANCE AND RESULTS
In 2018, we achieved industry-leading margin improvement, advanced our long-term
strategic vision and executed several important initiatives that position our irreplaceable
portfolio to continue outperforming the industry and creating value for our investors.
We had solid revenue per available room (�RevPAR�) growth of 2% at our comparable
hotels (on a constant U.S. Dollar basis) and ended the year with comparable RevPAR of
2XURSHUDWLQJSURCWPDUJLQVGHFUHDVHGEDVLVSRLQWVGXHWRLPSDLUPHQWH[SHQVH
related to four hotels recorded in 2018, while comparable hotel EBITDA margins improved
60 basis points due to continued productivity gains in the rooms and food and beverage
departments, and synergies from the Marriott-Starwood integration. These operating
improvements led to net income of $1.15 billion and Adjusted EBITDAre of $1.56 billion.
Diluted earnings per share was $1.47 and Adjusted FFO per diluted share was $1.77.
ACTIVE PORTFOLIO MANAGEMENT AND STRATEGIC INVESTMENT
In addition to delivering strong operating results, 2018 was a busy year of active portfolio
management and strategic investment for Host Hotels.
We further enhanced our portfolio by adding four world-class assets for $1.6 billion.
We purchased three iconic hotels from Hyatt in March of 2018: the Andaz Maui at Wailea
Resort, the Grand Hyatt San Francisco and the Hyatt Regency Coconut Point Resort and
Spa in Florida. In February 2019, we closed on the acquisition of 1 Hotel South Beach in
Miami Beach for $610 million. These fantastic hotels are in markets with a variety of
VWURQJGHPDQGJHQHUDWRUVDQGUHcHFWRXUVWUDWHJLFIRFXVRQRSSRUWXQLVWLFDOO\DFTXLULQJ
assets that enhance the value of the entire portfolio.
We funded these acquisitions by strategically divesting non-core assets. We have completed over $2.2 billion in asset sales since the beginning of 2018 at very attractive prices
and executed on several initiatives we laid out at the beginning of the year, including:

 Exiting international markets and refocusing our efforts in key markets in the U.S. by
selling the JW Marriott Hotel Mexico City and our interest in the European joint venture,
decreasing our revenues from assets outside the U.S. to 1.5%;
3 5HGXFLQJRXUH[SRVXUHWRSURCWDELOLW\FKDOOHQJHGKRWHOVLQ1HZ<RUNE\VHOOLQJIRXU
assets, including the January 2019 sale of The Westin New York Grand Central; and
3 5HDOL]LQJSURCWVIURPRXUUHDOHVWDWHYDOXHFUHDWLRQHIIRUWVWKURXJKWKHFRPSOHWLRQRIYDOXH
enhancement projects, such as the sale of the retail and signage at the New York Marriott
Marquis and the sale of the Key Bridge Marriott as a mixed-use redevelopment project.
We invested $200 million in return on investment (�ROI�) capital expenditures in 2018
and entered into an agreement with Marriott International to execute a number of transformational capital projects, beginning last year with the San Francisco Marriott Marquis
DQGFDUU\LQJWKURXJK$VSDUWRIWKHDJUHHPHQW0DUULRWWZLOOSURYLGHRSHUDWLQJSURCW
guarantees as protection for the anticipated disruption associated with the projects and
increased priority returns on the incremental investments.
VALUABLE STOCKHOLDER PERSPECTIVES
:HKDYHOLVWHQHGFORVHO\WRWKHLQYHVWPHQWFRPPXQLW\DQGKDYHVLJQLCFDQWO\HQKDQFHG
transparency of our portfolio disclosures to provide greater insight into our business. We
did so by overhauling our investor presentation and providing enhanced supplemental
CQDQFLDOLQIRUPDWLRQZLWKSURSHUW\OHYHOGHWDLOLQWRWKHWRSKRWHOVWKDWGULYHRXURYHUDOO
SHUIRUPDQFH<RXFDQCQGWKHH[SDQGHGLQYHVWRULQIRUPDWLRQDWZZZKRVWKRWHOVFRP
We listened when stockholders told us that they appreciate consistent capital returns, and
care deeply about the integrity and sustainability of our operations. We are proud to have returned
almost $630 million to our stockholders via a total 2018 dividend of $0.85 per common share.
We value the communities in which we operate and strive to be good corporate citizens.
We are committed to achieving environmental targets, monitoring the performance of our
responsible investments and measuring our progress toward improving the environmental
IRRWSULQWRIRXUSURSHUWLHV:HKDYHLQYHVWHGLQWDUJHWHGHQYLURQPHQWDOHICFLHQF\SURMHFWV
modernization of equipment and infrastructure upgrades and work with our hotel managers
to implement best practices across the portfolio. We have been recognized with numerous
awards in 2018 for our corporate responsibility efforts, including membership in the Dow
Jones Sustainability Index, NAREIT�s Leader in the Light Award for excellence among lodging
REITs, and the highest score in the hotel sector by Global Real Estate Sustainability Benchmark.
POISED FOR CONTINUED SUCCESS IN 2019
Looking ahead, our capital allocation efforts are off to a strong start in 2019 with the acquisition
of the 1 Hotel South Beach and the sale of The Westin New York Grand Central. We will continue
to use our scale and diverse portfolio to drive improvements in operating performance, explore
strategic acquisitions and dispositions and mine the portfolio for value-enhancement opportunities as we look to capitalize on the strength of our balance sheet throughout the lodging cycle.
We remain steadfast in our commitment to drive long-term value to our stockholders
DQGZHDUHFRQCGHQW+RVW+RWHOV	5HVRUWVLVZHOOSRVLWLRQHGIRUFRQWLQXHGVXFFHVV
7KDQN\RXIRU\RXULQYHVWPHQWLQ+RVW+RWHOVDQGWKHFRQCGHQFH\RXKDYHSODFHGLQXV

RICHARD E. MARRIOTT
JAMES F. RISOLEO
3UHVLGHQW&KLHI([HFXWLYH2ICFHUDQG'LUHFWRU
March 19, 2019