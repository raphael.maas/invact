To Our Stockholders

Broadridge had a milestone year in fiscal 2017. We reported record revenues and earnings
per share, booked record closed sales, continued to make critical investments in our products
and technology, made major strides towards integrating a significant acquisition, and were
once again recognized as a best place to work for our associates. In turn, our achievements
were rewarded by the market as we generated total shareholder return of 18% in the year,
putting Broadridge squarely in the top quartile for S&P 500 total shareholder return on both
a three- and five-year basis. It sure seems that the harder we work, the luckier we get.
Broadridge celebrated two important milestones over the course of fiscal 2017. The most
significant was our 10-year anniversary of becoming a public company. As we look back over
a decade of extraordinary growth, we can take pride in seeing how our belief in our business
model and our commitment to the Service Profit Chain have been rewarded by our clients
and our stockholders. The second milestone was the successful achievement of the three-year
financial objectives we laid out for the company in December 2014. Our willingness to
establish multi-year objectives sets us apart from most public companies, and our ability to
achieve those targets has contributed to our strong share price performance.
As Broadridge enters fiscal 2018, I am more optimistic than ever about the transformational
opportunities we see ahead of us. This optimism goes beyond a founder�s pride and is based
on the effort and investments we have made to position Broadridge for the future. Thanks
to our ongoing investments in broadening and innovating our product set, enhancing our
technology capabilities, and building out our sales and marketing organization, we have two
great franchise businesses that play an important role in powering corporate governance and
the capital markets, each with attractive growth opportunities. We have targeted plans that
will position us to add even more value to our clients and grow our business. We have a
blue-chip client base, whose confidence we continue to earn every day by delivering real
business value through leading technology-driven solutions that set the standard for the
financial services industry. Most importantly, Broadridge has more than 10,000 engaged
associates around the globe who remain focused on meeting and exceeding our clients�
expectations. Simply put, Broadridge is better positioned than ever to continue to drive
growth and deliver value to our clients and our stockholders. We are Ready for Next.
FISCAL 2017 WAS A MILESTONE YEAR
Last spring, Broadridge marked its 10th anniversary as a public company. In 2007, the year
we were spun off from ADP, Broadridge had revenues of $2.1 billion, adjusted earnings per
share of $1.51 and approximately 4,200 associates. From the beginning of our journey, we
were determined to invest in our business, control our own destiny, and stay ahead of market
dynamics by expanding our capabilities and services and solidifying our position as a
respected fintech market leader. 

To do that we said we would align ourselves around key client demand trends, including
mutualization and digitization. We said we would bring new products to market that addressed
critical client needs and supported the achievement of their goals. We also clearly understood
the power of our low capital intensity, strong free cash flow business model, and said that
we would be strong stewards of our stockholders� capital.
Over the past 10 years, we have delivered on each of those objectives. We have extended
our communications franchise to enable an increasing volume of digital communications. We
have built a capital markets back-office utility that powers global post-trade processing for a
growing number of leading financial institutions. And we have integrated innovative data and
analytics capabilities across our product suite to drive incremental value for our clients.
To do all of this, we have reinvested almost $2 billion of your money over the last 10 years
to broaden our product lineup and enhance our technology offerings. We have added to
our sales and marketing capabilities in order to enhance our relationships with our clients
and deliver these new products to them. We have also brought in new perspectives to our
management team and invested in career development opportunities to enable our
associates to adapt their skills to meet the changing needs of our clients. What�s more, we
have returned more than $7 per share in the form of dividend payments to our stockholders.
Our growth over the past decade is clear proof that these efforts are paying off. The number
of Broadridge associates has grown more than 130% to more than 10,000 today, our revenues
have grown by $2 billion to $4.1 billion and our adjusted earnings per share have more than
doubled. This growth has translated directly into shareholder value creation. Our shares
debuted on the New York Stock Exchange on April 2, 2007, at a price of $19.70. When the
market closed on June 30, 2017, the last day of our 2017 fiscal year, the share price was $75.56.
Coupled with our annual dividends, Broadridge stockholders have seen a total shareholder
return since our trading debut of 383%, well ahead of the 113% return generated by the
S&P 500 over the same time period.
FISCAL 2017: ANOTHER STRONG YEAR
Fiscal 2017 marked another year of strong financial performance for Broadridge, with 43%
total revenue growth, 15% adjusted earnings per share growth and record closed sales.
Recurring fee revenues rose to $2.5 billion, up 29% overall and 6% on an organic basis.
Revenue growth was primarily driven by last year�s acquisition of DST Systems� North
American Customer Communications business (NACC) and by balanced organic revenue
growth across both our Investor Communication Solutions (ICS) and Global Technology
and Operations (GTO) operating segments.
Revenues in ICS increased 54% to $3.4 billion in fiscal 2017. Recurring fee revenues, which
exclude distribution and event-driven revenues, rose 42% to $1.6 billion. The biggest driver
of total and recurring fee revenue growth in ICS was the acquisition of NACC, which
contributed $424 million to our recurring revenue growth. Thanks to a lot of hard work on
the part of our associates, NACC is now an integrated part of our newly-formed Broadridge 


Fiscal 2017 marked another year of strong financial
performance for Broadridge.

Customer Communications (BRCC) business. BRCC is positioned to be a leading technology
platform designed to deliver valuable communications across digital and print channels through
a unique network that connects tens of millions of households and thousands of brands.
On an organic basis, ICS grew recurring fee revenue by 6% for the year. Our growth
benefited from a strong proxy season as stock record growth, which measures the growth
in shareholder positions, rose 8% for the full year, including 10% in the all-important fourth
quarter. That was nearly triple the 3% growth recorded in fiscal 2016. While stock record
growth can vary from year to year, its long-term growth has been driven by the continued
popularity of stock ownership as a means to invest. After the financial crisis, stock record
growth was slow to recover, averaging 1% over the fiscal 2011�2013 period. Since then,
however, the number has been much healthier, averaging 7% over the past four years. By
staying committed to delivering world-class service to our proxy clients, Broadridge
continues to benefit from that trend.
Another driver of our organic growth was mutual fund interim record growth. Just as stock
record growth is a proxy for the number of individual stockholders, interim record growth
captures the number of fund holders who own mutual funds and exchange-traded funds
(ETFs) in beneficial accounts. In general, interim record growth tends to track the overall
level of fund flows into mutual funds and ETFs. Those fund flows dipped into negative
territory at the end of fiscal 2016. Since the fall, fund flows have turned positive again and
that turn has been reflected in our business. After a slow start to the year, mutual fund and
ETF interim record growth picked up in the second half of fiscal 2017, rising to 7% in the
fourth quarter. For the full year, interim record growth was 4%.
ICS revenues also benefited from strong event-driven activity in fiscal 2017, as event-driven
revenues increased 10%. The biggest contributor to the year-to-year changes in event-driven
revenues is the timing of when mutual fund complexes solicit their fund holders for a vote. In
fiscal 2017, the overall level of activity was higher than normal, driven in part by the
activities of a very large mutual fund and ETF complex. Our event-driven revenues also
benefited from an increase in shareholder activism at large capitalization companies.
Because of their accelerated timeline, these large campaigns are complex and require real
precision to execute. Broadridge�s ability to do so is a core skill set of our proxy business.
Our GTO business also performed very well in fiscal 2017, as large financial institutions
continued to look for ways to mutualize costs. Revenues in GTO grew by 9% for the year,
including 6% on an organic basis, driven by continued strong sales performance and 

improved trading volumes. The biggest contributor to GTO revenue growth came from the
implementation of new sales as we onboarded clients onto our industry utility platforms.
The GTO business has been an important contributor to our record closed sales results in
recent years, and these sales are flowing through to revenues and contributing to the pick-up
in growth. GTO revenue growth also benefited modestly from higher trading volumes and
the contributions from our recent tuck-in acquisitions.
Our strong revenue growth during the fiscal year translated into exceptional profit growth,
as adjusted operating income increased 16% in fiscal 2017 to $623 million. Adjusted operating
margin declined to 15.0% compared to 18.5% a year ago, which primarily reflected the
impact of adding the lower-margin NACC business. Without the impact of the NACC
acquisition, adjusted operating margin would have expanded by 40 basis points in fiscal
2017, driven by solid recurring fee revenue growth and cost efficiencies. Adjusted net
earnings increased 14% to $378 million and adjusted earnings per share rose 15% to $3.13.
Strong profits, in turn, contributed to strong cash generation. One of the hallmarks of
Broadridge�s financial profile has always been our strong free cash flow, and this was again
the case in fiscal 2017. Free cash flow was $402 million for the fiscal year, up 11% over our
solid performance in fiscal 2016.
I am proud to report that Broadridge recorded another record closed sales result in fiscal
2017, with closed sales rising 25% to $188 million. This remarkable performance reflects the
breadth of our business, the strong value proposition that we bring to our clients, and the
power of Broadridge�s franchise businesses.
For example, Broadridge signed several major deals that further enhance the scale of our
global post-trade processing platforms during fiscal 2017. These included a large deal with
a major tier-one bank to provide equity post-trade processing services and another with a
top-15 investment bank to manage its global back-office operations. These transactions,
coming on top of previous deals signed in earlier years to convert a major client�s seven fixed
income platforms onto a single platform managed by Broadridge and to provide back-office
processing technology to several major European global banks, further cement Broadridge�s
position as a leading global capital markets infrastructure utility.
We also extended our reach in proxy with the win-back of a notable proxy client and made
progress on extending our sales to our wealth management clients. Further, I was pleased to
see a growing number of sales from our newly-combined BRCC business. Not only have sales
in this business exceeded our expectations for the first year, but the pipeline of potential
deals is full, which bodes well for putting that business on a growth track earlier than we
had anticipated.
Looking ahead, our closed sales pipeline is strong. Even after another year of record sales,
and not counting the incremental opportunities we are seeing from BRCC, our pipeline was
stronger at the end of fiscal 2017 than it was at the beginning. The combination of record 

sales in 2017 and a stronger pipeline to begin fiscal 2018 gives us even more confidence in
our ability to generate growth and achieve our targets.
DISCIPLINED CAPITAL STEWARDSHIP
We continued to allocate capital using our balanced, disciplined approach. We deployed
$539 million in fiscal 2017 towards strategic M&A focused on driving the evolution of our
business. This included the acquisition of NACC and enhancing our blockchain capabilities
with the acquisition of Inveshare�s technology assets. Additionally, we made two smaller,
buy-versus-build acquisitions that strengthened our ability to serve the wealth management
market and extended our capabilities in post-trade processing and controls.
We have always been firm believers that a strong dividend is fundamental to long-term
shareholder value creation, and have committed to a target payout ratio of 45% of net earnings.
After all, what�s the point of owning a company with strong cash flow if you don�t get your
hands on it? In fiscal 2017, we returned $152 million to investors through our dividend, and
in August, the Board announced its decision to increase our annual dividend by 11% in
fiscal 2018 to $1.46 per share. This means we have increased our dividend every year we
have been a public company, and marks the sixth consecutive double-digit increase.
We also continued to repurchase our stock, buying back 4.9 million shares during the fiscal year,
and in the process returned an additional $282 million to stockholders, net of option proceeds.
ACHIEVING OUR THREE-YEAR FINANCIAL OBJECTIVES
At our last Investor Day in December 2014, we set out a series of three-year growth objectives.
I�m pleased to say that Broadridge achieved those targets in fiscal 2017. Our first objective
was to grow our recurring revenues by 7�10% per year. We achieved a compound annual
recurring revenue growth rate of 14%. Next, we targeted adjusted earnings growth of 9�11%.
Our compound annual adjusted earnings growth rate over the past three years was 11%. In
addition, we targeted 50�60 basis points per year of margin expansion and we achieved that
goal as well, excluding the margin dilutive impact of the NACC acquisition.
We set these three-year objectives to help us maintain our focus on creating value over the
mid- to long-term. As I noted in my introduction, our ability to set and meet these goals has
been an important contributor to the strong performance of Broadridge shares in recent
years. We are in the process of establishing new objectives through fiscal 2020 that are
reflective of the continued strength of our model and our commitment to the execution of
our growth strategy, and we look forward to sharing them with you later this year. We�ve
delivered against our commitments in the past and will continue to work hard to do so
going forward.
Management strength has been and will continue to be key to our ability to achieve our goals.
I couldn�t be prouder of our team, a group with deep expertise that effectively combines a
mixture of long-tenured experience with new talent and fresh perspectives. This year, our
Board recognized Tim Gokey�s significant contributions to Broadridge�s success with his 

promotion to President. Tim joined us in 2010 as head of strategy, and in the seven years
since then has been instrumental in our efforts to closely align Broadridge with its clients
and long-term demand trends to drive its growth. In his role as Chief Operating Officer, Tim
has been a valuable partner in managing both our ICS and GTO segments, broadening our
product offering, and been a thought leader for our clients. I look forward to continuing to
work closely with Tim in the years to come.
DELIVERING ON WHAT WE STAND FOR
Broadridge�s ability to successfully grow and evolve our business and continue our journey is
based on our focus on meeting fundamental commitments to all of our stakeholders.
While we always remain true to our roots as an accurate, trusted, and efficient partner�these
are fundamental to Broadridge�s DNA�we are also building on our strong heritage of
innovation to help clients meet the complexities of today�s market challenges. In short,
Broadridge helps clients transform their businesses by enabling them to optimize efficiency,
navigate risk and generate growth. We are bringing to bear our full enterprise value, and
working alongside clients as a strategic partner with the insights and solutions that help
them turn the challenges of digitization, mutualization, and data and analytics into
new opportunities.
Our corporate governance franchise is a great example of this in action. By bringing scale
and a focus on our clients� needs to mission-critical, back-office functions like proxy
processing and delivery, we have enabled our clients to enhance their communications with
their customers, meet their regulatory needs, and realize significant savings. Another example
is post-trade processing. In recent years, we improved our technology platforms and built
out managed services capabilities to the point where we are now running a proven backoffice trade processing utility for more than 30 clients in the financial services industry.
Those solutions are delivering tremendous value to our clients versus the cost of trying to
provide those solutions on their own.
We are also committed to reinvesting in our business. Over the last decade, we have invested
almost $2 billion, both organically and through strategic M&A, to enhance our technology
and broaden our product portfolio. At the same time, we have worked hard to integrate
leading-edge technologies such as cloud-based communications and blockchain capabilities
into our core product offerings. We have also invested in our sales and marketing efforts to
further enhance our clients� understanding of our value proposition, strengthen our position
as a thought leader on relevant industry issues, and ensure that we have the right people in
the right spots to engage on the right opportunities. We have expanded our infrastructure
globally, building out our presence in both Europe and Asia.
With the acquisition of NACC, we have strengthened our ability to more fully leverage our
capabilities and solutions to lay a foundation for a powerful �network effect,� particularly as it
relates to our digital and cloud-based solutions, by linking more than 80% of North American
households, thousands of brands and multiple cloud channels through the distribution of 

five billion pieces of important content that people need to live their lives. The scale of this
network is unmatched.
We kept our foot on the gas on the investment front in fiscal 2017, expanding our digital
communications and post-trade processing capabilities while also investing in new product
development areas like tax reporting and blockchain and opening a new office in Singapore.
All of these investments have further strengthened our reputation among our clients as both a
trusted and efficient vendor and a strategic partner and thought leader capable of helping
them adapt to new technologies and transform their business models. The result is that
Broadridge is in a much stronger position to pursue the large industry-transforming
opportunities we see in the markets today.
To our stockholders, our commitment is simple: to endeavor to deliver top quartile total
shareholder return over any multi-year period through the execution of a strategy that
generates consistent revenue and earnings growth and a capital allocation strategy that
prioritizes consistent investment in our business while returning capital to our stockholders.
Our financial performance over the past decade and in fiscal 2017 validates our effectiveness
in delivering on this commitment.
As you think about our performance, there are a few dynamics to bear in mind, all of which
have become more relevant over the course of the past 10 years and whose importance will
only continue to grow going forward. First, the mission-critical nature of our services, our
strategic client relationships, and the structure of our client agreements are such that we
believe we have solid revenue and earnings visibility for multiple years. Client revenue
retention has averaged an enviable 98% over the past 10 years. Furthermore, not only does
our business provide strong visibility, but it also generates significant cash flow, which we have
used to consistently reward our stockholders through a robust dividend policy, consistent
share repurchases, and by continually investing in our business.
As important as our ability to deliver for our clients and stockholders is, we would never be
able to do so if we didn�t also meet our commitments to our 10,000-plus associates. At
Broadridge, we are strong believers in the Service Profit Chain: the concept that there is a
direct connection between employee engagement, client satisfaction and the creation of
shareholder value. Our associates recognize that they are responsible for winning the service
battle, delivering day-in and day-out for our clients, and in evolving their skills to meet our
clients� changing needs. In order to help them succeed, we dedicate significant time and
resources to ensuring that our associates are engaged and motivated. Those that meet our 

high expectations are rewarded and those that do not are moved to roles where they can
be more effective or exited from the company.
We run Broadridge as an effective meritocracy where every associate needs to be relevant
both today and tomorrow. Technology continues to make us more efficient, eliminating the
need for some roles and creating new opportunities in other areas, and we are constantly
focused on making sure our business is structured to efficiently achieve our performance
goals. By embracing this continued evolution, our best associates are able to adapt and thrive.
We have also always believed in the idea that doing the right thing is good business. From
the top down, we encourage all associates to first identify what is the right thing to do in
every customer, associate, or industry interaction. Focusing on that simple principle, and not
trying to engineer the most desired outcome, is what has enabled Broadridge to become a
trusted partner to all our constituents.
Whether by providing associates with time off to serve their communities, matching their
donations to causes they feel strongly about, or pursuing worthwhile causes through the
Broadridge Foundation, such as supporting organizations in India focused on getting young
girls out of manual work camps and into school, or the promotion of financial literacy for
young adults in underserved communities, we recognize that the stronger our commitment
to our associates and our communities, the more value we can provide to the marketplace.
Our Corporate Social Responsibility efforts are a win-win, resulting in a stronger company,
deepened relationships and a better world to live in.
You only need to look at the recognition we�ve received to see the proof of our dedication
and the seriousness of our commitment, including recognition from Fortune Magazine as one
of the �World�s Most Admired Companies.� For the tenth year in a row, we have been
recognized as a �Best Company to Work for in New York State� by The New York State
Society of Human Resource Management, and have also been named one of the �Best Places
to Work for LGBT Equality� for five years by the Human Rights Campaign Foundation. Doing
the right thing is a core part of our approach to creating long-term value.
READY FOR NEXT
At Broadridge, we want to drive the innovation roadmap and develop solutions that allow
our clients to get ahead of today�s challenges and capitalize on tomorrow�s opportunities.
That is what we mean by Ready for Next. 

Looking forward, I am more excited than ever about the opportunities we see for Broadridge
to expand its important role in delivering mission-critical infrastructure for the financial services
industry, helping our clients harness the large transformational trends that are impacting
their businesses and continuing to deliver strong returns for our stockholders. Thanks to the
investments we have made in our business over the past decade and the entrepreneurial
spirit of our associates, we are increasingly viewed by our clients as a partner who can help
them adopt new technologies and transform their business models. That puts Broadridge in
a much stronger position to pursue the large transformational market opportunities created
by mutualization, digitization and data and analytics. This will be the next leg in our journey.
Let me share a few examples of how we plan to help our clients transform their businesses.
As financial services institutions look to transform and mutualize their mission-critical but
non-differentiating back-office functions, Broadridge is the only player with the proven
technology, scale, innovation, experience and, most importantly, the clients to achieve this
and meet their needs. This puts us in position to offer additional layers of value by driving
network benefits to our clients, which in turn could lead to new opportunities around faster
settlement time and even the ability to create greater liquidity in fixed income markets. We
also see significant opportunities to continue to extend our capital markets franchise outside
of North America.
Our asset management clients face significant challenges adapting their business models
to meet the challenges posed by the trend towards passive investing and by the need to
strengthen their relationships with fund holders. We are working with mutual funds and
regulators to continue to drive increased engagement and cost efficiencies into their
communications, leveraging our digital capabilities and knowledge of investor behavior. We
also see additional opportunities to help them streamline their operations for the benefit
of all.
We see a significant long-term opportunity to help our clients navigate the evolution of
transactional communications from email and envelopes into a more dynamic and cloud-based
future. In this future state, Broadridge will provide the platform and connectivity that will
enable clients to deliver relevant content to their customers across a range of channels,
including print and postal, email, text and multiple cloud portals through a single interface,
and gain immediate access as new channels emerge. They can also engage more meaningfully
with their customers, creating personalized, contextual experiences that make it easy for
customers to take action by using composition, marketing and personalization tools. The
unique scale of our network, with its billions of pieces of content, relationships with thousands
of brands and access to multiple cloud channels, is attracting significant interest from the
market. The more immediate step in realizing this objective will be to win existing print
business from key, large in-house players, and, with the pipeline we have built over the past
year, I am more optimistic than ever about our ability to do that.
We continue to believe that blockchain distributed ledger technology has the potential to
transform everything from the proxy process to stock exchanges to the trading of foreign 


currencies and commodities. We are investing to ensure that Broadridge will be ready with
the solutions clients need through organic initiatives, the acquisition of Inveshare�s technology
assets, investments in start-up companies, and co-innovating with our clients and other
market players. While this opportunity remains longer-term, we are making substantial
progress. The most notable evidence of this in fiscal 2017 was the completion of a pilot
program, conducted in partnership with J.P. Morgan and Northern Trust, that successfully
used blockchain technology to enhance proxy vote transparency and analytics for a major
European corporate issuer.
Being Ready for Next means that we need to remain focused on driving innovation so that
we can enable our clients to capitalize on the opportunities presented by a constantly
evolving marketplace. The examples above are a few instances of how we are working and
investing to ensure that we and our clients are always ready for their next step. There are
many others across cloud computing and artificial intelligence, among other areas, that we
are working on every day.
One of the most consistent themes in the financial services industry is that success depends
on the readiness to adapt to changing market conditions. This idea has perhaps never been
more relevant than in today�s markets. As we look out over the next 10 years, we are ideally
positioned to continue to build on our more than five decades as a respected fintech industry
leader. We are ready to capitalize on the next big, disruptive trends, and in the process
continue to generate value for our clients, stockholders and associates. Today, we have the
breadth of products, the depth of management talent and the standing in the marketplace
to credibly pursue these exciting opportunities. It�s truly a great time to be at Broadridge.
As always, but perhaps most especially on our 10th anniversary as a public company, I would
like to thank our associates for their efforts. Their commitment to meeting or exceeding our
clients� expectations and their ability to adapt their skill sets to stay relevant has been, and
will continue to be, a critical component of our long-term success.
I would also like to thank our Board of Directors. They are true partners whose insights,
guidance and contributions are essential to our success. Lastly, I want to express my gratitude
to our stockholders for the confidence you have shown in me and in our management team
over the course of our 10 years as a public company. That confidence is critical to enabling
us to focus on creating medium- to long-term value, and we are committed to continuing to
work every day to maintain your trust as we take the next steps on our journey.
Sincerely,
Rich Daly
Chief Executive Officer 