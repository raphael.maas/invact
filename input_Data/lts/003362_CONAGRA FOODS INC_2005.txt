Fellow Shareholders,
ConAgra Foods gave investors a lotto comment on in fiscal
2005, both positive and negative. The year's accomplishments
included solid operating results during the first half ofthe
fiscal year, reducing debt by nearly $1.2 billion, paying $550
million in dividends, investing $450 million for the future,
repurchasing $180 million of our shares, liquidating a portion
of our poultry investments for a gain of more than $180 million,
significant team additions, record results for our trade
group, and reaching important milestones with our business
initiatives. There were also several glaring weaknesses
which overshadowed the accomplishments; for example.
poor packaged meats results along with manufacturing
shortages created a significant earnings shortfall in the
third and fourth quarters, and we discovered historical tax
errors spread over several years so we restated earnings.
In this letter, l will detail more about the year's progress, as
well as its weakness.
To state the obvious, this was a year of ups and downs.
Our stock price topped $30 shortly after we finished a solid
first half; it now trades around $23, mostly because of the
significant negative impact from weak packaged meats
results in our second-half earnings. The formula for a higher
stock price is very clearthe stock price should follow

consistent earnings growth, and we simply didn't
achieve that in the second half of this fiscal year. We're
addressing that shortfall by accelerating progress with
several actions which are underway and summarized in
this letter as well.
Even though we made progress on many fronts, and several
areas of our business posted strong profits for the fiscal
year, fiscal 2005 diluted EPS was only $1.23 because ofthe
shortfall in the second half, and because of $0.12 of net
expense from items that should not be with us on a regular
basis. This was much lower than we planned, and most
definitely beneath what we believe to be the company's future
earnings potential. The $0.12 net expense includes items such
as impairment charges on several assets, severance costs to
facilitate significant headcount reductions, costs related to
early retirement of debt, loss on operations we divested, and
costs resulting from fire damage, all of which were partially
offset by a gain from selling a portion of the Pilgrim's Pride
stock we obtained when we divested our poultry operations.
All of these items are specifically detailed in the archived
quarterly earnings releases and in the fiscal yearend Question
and-Answer document which is posted on our Web site at
www.conagrafoods.com/investors.


Despite overall EPS results that were well below our
plans, fiscal 2005 showed significant accomplishments in
several areas as well as a solid approach to allocating the
company's strong cash resources. Here are some highlights:
Fiscal zoo; Highlights
- We reduced debt by nearly $1.2 billion.
- We paid $550 million of dividends.
- We invested $450 million in capital expenditures
for the future.
- We repurchased more than $180 million of our stock.
- Several of our key brands got stronger and posted year
over-year sales growth on a comparable basis, including
ACT ll, Banquet, Chef Boyardee, Egg Beaters, Hunt's,
Kid Cuisine, Manwich, Marie Callender's, PAM, Parkay,
Peter Pan, Snack Pack, Swiss Miss, Wesson, and others.
Many of these have now grown for several successive
quarters, demonstrating success with the sales and
marketing initiatives we've been implementing over the
past couple of years.
- Our Food Ingredients segment had an outstanding year,
with operating profits up more than 30 percent. This was
due in part to seizing the opportunities brought about by
favorable market conditions for our commodity trading
and merchandising operations.
- ln our Foodservice Products segment, specialty potato
operations continued a string of many solid years;
this was the result of effectively combining high-quality
products, very efficient manufacturing, very effective
quality control, along with strong marketing and
customer service.
- Several new team members joined us over the past year,
and several existing team members assumed new
responsibilities in response to new challenges and
opportunities. Just to name a few, Frank Sklarskyioined
us as Chief Financial Officer; Jim Hardyjoined us as
Seniorl/ice President, enterprise manufacturing;
Dean Hollis, Executive Vice President for the Retail
Products channel and a long-time ConAgra Foods veteran,
assumed responsibility for both retail refrigerated and

frozen operations; Kevin Adams, another ConAgra Foods
veteran, assumed responsibility for all Operational
Support; and Randy Harveyjoined us to lead our tax
group. Because of these changes, and many others which
complement them, we now have deeper bench strength
and an improved focus on key areas to foster better
execution in FY06.
During the year, we implemented the Order-to-Cash
portion of Project Nucleus, the platform for our business
process improvement initiatives. By utilizing new
information systems and implementing new business
processes throughout the company, Order-to-Cash is giving
us better visibility as well as greater efficiency and
effectiveness for transactional activities such as order
fulfillment, invoicing, and collections.
We embarked on reducing lowervolume, lower-margin
SKUs (stock-keeping units, i.e., individual products), and
developed aggressive but realistic goals for continued
SKU reduction over the next 36 months. SKU reduction
greatly simplifies the entire supply chain, from sourcing
raw materials to manufacturing, storing, and distributing
finished products, which translates into lower complexity
and lower cost.
Plans are being developed and refined to improve our
manufacturing efficiency and effectiveness. Our manufac
turing initiatives are being designed to create significant
savings by systematically decreasing fixed overhead and
better utilizing capacity. This involves standardizing and

implementing best operating practices across our entire
production network and optimizing capacity utilization.
Some of the efficiency initiatives already underway gave
rise to impairment charges in fiscal 2005 related to
various manufacturing assets that were sold, closed, or
consolidated to improve efficiency and strengthen the
manufacturing network. You can expect to see more such
charges incurred in the future as strategic manufacturing
opportunities are identified, finalized, and implemented
as part of our plan to pursue significant earnings
improvement opportunities in our supply chain network.
Three new strategically located temperature-controlled
inventory mixing centers were completed during the year,
raising the total to 13 new centers over the last three
years. We will soon finish one more center for a total of
14. 0ur mixing centers serve as the backbone for our
recently overhauled transportation and warehousing
network, They are key to serving customers, reducing
total logistics expenses, and reducing our investment in
inventory over time.
~ Headcount and administrative expense reduction
programs were implemented in the fourth quarter of the
fiscal year. Once completed, these programs should
benefit our cost structure by a run-rate reduction in the
range of $100 million on an annualized basis. There were
severance costs of $0.05 per share in fiscal 2005 to
facilitate the headcount reduction program.
- We sold our equity interest in Swift Foods for more than
$190 million.
~ We recovered all of the $300+ million of cattle-feeding
financing that had been part of the original Swift Foods
deal in fiscal 2003.
~ We liquidated a little more than onethird of our holdings
in Pilgrim's Pride for more than $280 million, $180+
million of which represented a gain. After fiscal Z005
ended, we sold our remaining Pilgrim's Pride stock for
more than $480 million, $330+ million of which
represented a gain.
As this list demonstrates, the organization is actively
engaged in upgrading and integrating key marketing, operating,
and business processes that provide the foundation for
earnings growth, as well as adhering to sound and consistent
capital allocation policies.

Fiscal 2005 Challenges
Along with the accomplishments were significant operating
challenges that interrupted earnings growth. The biggest
challenge was inflation, which is not new to us or the industry;
however, the duration and extent of it presented a greater
challenge than we anticipated. We've seen significant inflation
recently; we incurred inflation in excess of $350 million in
fiscal 2004, and during fiscal 2005 inflation increased our
operating costs by another $400+ million. We were able to
recover some of the inflation through increased pricing and
operational savings, but not enough to prevent margin erosion
in fiscal 2005, particularly in our packaged meats business.
Inflation was a large part of the reason ourfiscal 2005 earnings
were lower than expected, but that's not the whole story.
Production and Capacity. Our weak results also reflect
problems we experienced with basic execution, primarily
during the second half of our fiscal year. As part of a need
to replace some manufacturing equipment and increase
productivity, we also took actions to consolidate production
into fewer plants. The transition was not as smooth as it
could have been, resulting in temporary product shortages.
For these and other reasons, strong demand for some popular
items grew past our capability to manufacture them for a
period of time. Strong demand is a good problem to have;
however, when you can't fill customer orders, you lose sales.
These production and capacity shortages temporarily affected
popular brands like Banquet Crock-Pot Classics, Egg Beaters,
Hebrew National, Marie Callender's, Reddi-wip, Slim Jim,
and Snack Pack. These issues were most pronounced in our
fiscal third and fourth quarters, but for the most part we
believe these operating issues are now behind us.
Packaged Meats Pricing. The single most economically
challenging issue was the performance of our packaged
meats operations across all customer channels. Packaged
meats, which include such items as hot dogs, lunchmeats,
sausage, bacon, and processed turkeys sold under the
Armour, Brown N Serve, Eckrich, Healthy Choice, Hebrew
National, and Butterball brands, account for nearly $2.5 billion
of our sales. In most years these operations are an excellent
business for us, but 2005 was definitely not one of them.
Our packaged meats businesses experienced significantly
higher input, packaging, and distribution costs which we
did not adequately pass along. We would have liked to have
realized higher prices, and thus recovered our increased costs.
but we did not accomplish that rapidly or effectively. In some
situations, we failed to take our prices up because of strained
trade relationships stemming from the production problems
l described above; in other situations, we tookthe prices up,
only to negate the gains through trade promotional deals.
For the most part, though, consumers experienced the pricing
increases at the shelf, even though we did not reap the benefits
due to trade promotional deals with customers.
We hope never to repeat those mistakes. We have since
made many significant changes to how we measure and
operate our packaged meats businesses, and with time we
expect them to return to more normal levels of profitability.
To provide some perspective, these businesses were about
$150 million less profitable in fiscal 2005 than they were
in fiscal 2UO4that accounts for $0.18 per share of year-
over-year EPS decline. lttakes time to unwind these problems,
so the challenges in our packaged meats operations are still
with us, meaning we will most likely not see improvement in
those operations until after the first quarter of fiscal 2006.
That is primarily why we said in our fourth-quarter earnings
release that we do not expect our fiscal Z006 first quarter
to show overall year-over-year EPS growth, and that any
EPS growth we expect in fiscal Z006 would most likely be
concentrated in the second half of the year.
Building a Strong Future:
M ulti-year Pro?t-enhancing Initiatives
As you know, ConAgra Foods has been very focused on
developing a richer business model characterized by better profit
margins and returns on capital. This focus was generated in
response to the risk/reward profile of some of the businesses
previously in our portfolio, and the inherent cost duplication
resulting from the fact that the company had not integrated its
many acquisitions over the years. As we measured ourselves
against competitors we saw significant potential for growth.
In addition, accelerated consolidation in the food industry
has been a driving force, causing us to make many cultural,
operating, and marketplace changes in order to compete



effectively and serve sophisticated customers. For all of these
reasons, a great deal of heavy lifting has been accomplished
over the last few years as we have upgraded our assets,
rebuilt our operating platform, recruited new talent, and
reshaped our portfolio through acquisitions, divestitures,
and consolidation of like businesses and functions.
Operationally, we are now a couple of years into the market-
ing, operating, and business process improvement initiatives
designed to attract consumers, serve trade customers effective-
ly, and manage more efficiently. Because of the scope of the
initiatives and the size of our company, the marketing, operating,
and business process change initiatives will likely require two
to three more years to gain the traction we've targeted.
Generating Top-line Growth
Growing volumes and improving both product mix and margins
are keys to long-term earnings growth. To do this right, we
have to serve existing and new customers consistently,
increase our channel presence, and go to market with products
that drive consumer preference. In-depth knowledge of
consumer trends in health, wellness, demographics,
convenience, and other major platforms is essential for
generating long-term brand growth. We are now using
fact-based analytical methods for targeting growth potential
for our brands and products; this brings powerful tools to our
marketing teams across retail, foodservice, and ingredients
operations and favorably impacts the task of reaching and
keeping consumers. The scope ofthe work strengthens the
fundamentals for the products we sell, i.ee, taste, packaging,
product quality, and nutritional content. This also focuses on
the specifics of our communication with consumers through
advertising and promotional campaigns, store displays, and
price points. Every bit as important is our ability to evaluate
the payoff from marketing investments so that we put
our marketing dollars to their highest and best use; we are
convinced there is significant opportunity to get more
for our money as we more tightly manage our consumer and
trade investment programs.
In fiscal 2005, we invested more than $340 million in
consumer advertising and promotion, most of which was
centered on our retail brands. Our view is that amount of
investment will increase over time on a targeted, brand-
specific basis as part of a strategy to strengthen selected
brand equities; but more important than the amount of
spend is the quality of spend, i.e., that the dollars are spent
the right way for the right level of return. Some of the
marketing methods that have been the most expensive
historically are no longer the most effective, and that opens
up new opportunities for us. In addition, the same discipline
that drives the allocation of consumer marketing dollars
also applies to our $2+ billion a year of customer trade
spending. Again, historical spending practices may no
longer be the most effective.
Earlier I listed several brands that had strong sales
performance in fiscal 2005. The reason some large, high-
margin brands have posted several successive quarters of
growth is because of progress with our sales and marketing
initiatives, through product modification, better consumer
communication, more consumer-friendly packaging, and
prudent use of marketing dollars. There is still much more to
do as we continue to apply these disciplines to all of our
major brands and product lines, but we are encouraged by
the brands response to the actions taken so far
The Essential Rule of Operating Excellence
We are convinced that there is significant earnings growth
opportunity by generating cost savings as we integrate,
simplify, and upgrade our entire supply chain. The plan is for
some of these savings to be reinvested in our businesses
to fuel profitable top-line growth. Because cost savings are
directly connected to future investment, operating excellence
is at the top of our agenda.
Purchasing. As l mentioned last year, we are increasingly
consolidating our purchasing activity for the major inputs we
use, so that we utilize experienced and specialized purchasing
expertise to obtain the best prices and terms. Each year
we are expanding this approach to a greater portion of our
total inputs, so that over time this can be a major contributor
to increased profitability.



Manufacturing. Overthe last few years, we have been
adjusting and consolidating our vast manufacturing network,
but with more than 150 manufacturing facilities still in our
portfolio, much more opportunity exists. There are significant
efficiencies to be gained by consolidating and strengthening
several manufacturing facilities, which means over time we
will close or sell some less efficient operations. These plans
are being carefully developed, and their implementation will
be led by Jim Hardy, who heads enterprise manufacturing.
The improvement will come not only from optimizing plants,
but also from implementing best practices and measurements
relating to overall efficiency throughout all of our plants. Plant
rationalization usually involves restructuring charges and
headcount reduction, which unfortunately are necessary for
continuous productivity improvement in a very competitive
marketplace. When these actions occur, we will call them
out so you can track the company's earnings performance
separate from these charges.
SKU Rationalization. Simplicity is a good thing. We can
become much more efficient over time as we reduce complexity
throughout our entire supply chain; part of the complexity
exists because, over the years, we have allowed too many
SKUs to remain in production. As new and more popular products
overtake older or less popular products, the number of product
variations should be reduced. For this reason, we have begun
to eliminate lower-volume, lower-margin SKUs, and we will
continue to do this very aggressively overthe next 36 months.
Overall we are targeting a sizeable double-digit percentage
reduction in SKUs; this will dramatically improve our ability to
save money across our entire supply chain, and it will also
result in an increased focus on the SKUs that have higher
profit potential. SKU rationalization is a significant enabler for
achieving the broader operating efficiencies being targeted.
Logistics. More efficiently storing and shipping products
can also provide a great deal of cost savings and improve
customer service. We've been transitioning from a network
of hundreds of warehouses across the country to a network
defined by 14 inventory mixing locations; these support
retail and foodservice products in strategic locations that
best serve our customers. An inventory mixing center is a
large facility where multiple brands and products of a similar
temperature state lie. shelf-stable, refrigerated orfrozenl
can be sized, consolidated, and shipped out together. This
benefits us by better utilizing truckload capacities and ware-
house space, while also reducing working capital as excess
inventory quantities are eliminated. This benefits customers
through better efficiencies in ordering, receiving and storing
our shipments.
As of the end of fiscal 2005, we had completed 13 of the
l4 inventory mixing center locations, and the remaining one
will be completed soon. We are phasing in the use of the
mixing centers, and expect a much more efficient logistical
cost base once the centers are being fully utilized and many
of our buffer warehouses are minimized.
General and Administrative Expense and Headcount
Reduction. l\/lost of our opportunity to generate cost savings
is in our supply chain, but there are also other types of savings
well worth pursuing. As such, in fiscal 2005, we developed
specific plans for reductions in our anticipated G&A (general
and administrative) cost structure, and those plans included
salaried headcount reduction. This type of program is never
easy given the disruption it causes employees and their
families, but it is necessary if we are to optimize our expense
structure. Overall we are reducing several hundred salaried
personnel by the end of our first quarter in fiscal 2006, and
those headcount reductions, along with other G&A expense
reduction programs, are expected to save in the range of
$100 million annually once the programs are complete.
Better Informatian, Processes, Visibility
Because of the sophistication of today's business environment.
it is very important that our manufacturing, marketing, sales,
logistics, customer service and accounting functions connect
well together so that we interface well with our customers
and vendors. Project Nucleus is the platform by which we
are upgrading information systems and business processes
to better manage high-dollar areas of our business, Key
areas we expect Project Nucleus to favorably impact include
trade merchandising programs, order management, inventory
management, customer payments, and logistics. This year
we passed an important milestone with Project Nucleus as



we implemented Order-to-Cash. As a result of this process,
we learned a great deal about what to expect going forward
as we implement additional aspects of Project Nucleus. The
improved information and visibility we have already obtained
from Order-to-Cash is well worth the growing pains we
endured, and we are confident that all aspects of Project
Nucleus will provide a significant payoff for the time and
financial investment we have made in the project. While we
wish we had implemented Project Nucleus years ago and
realized the benefits earlier, the good news is we are now
well on our way with the project, and have line of sightto
the benefits that drive operating improvement.
All aspects of the operating efficiency initiatives are very
important, and over the next two years we will be able to give
a greater amount of attention and focus to the manufacturing,
SkU rationalization, and G&A expense opportunities within
our supply chain because of the improved visibility we have on
these opportunities and their sizeable savings potential.
Our Team's Commitment to Strong Values
Our team is committed to the business goals of improving
profit margins and returns on capital through the specific
initiatives I've described in this letter; of equal importance is
that our team pursues these business goals while displaying
the values that we all expect Our company's commitment to
its code of ethics, governance policies, sound environmental
practices, and charitable causes like fighting childhood
hunger are items that collectively represent core values.
Straightforward CulrureSEC and Taxes
While we are committed to these strong values, we have
not been perfect, as reflected in the pending SEC matter
that we have talked about for several years. We have also
made mistakes, as reflected in the recent restatement
resulting from historical tax errors. ln both cases, our team
has proactively worked to address the underlying causes of
these events. When we discovered the tax errors, ourteam
took immediate steps to dig deeper, correct and communicate.
We are committed to a corporate culture that fosters integrity,
accountability and excellence.
SEC Matters
In May of 2001, we announced a restatement of historical
financial results in connection with accounting and conduct
matters at UAP, a business we have since sold. For several
years we have been providing information to the SEC and
working to resolve matters with the SEC. As ofthe third
quarter of fiscal 2005, we had accrued $46 million as part of
the process of settling and resolving these SEC matters.
Although a settlement has not been finalized, we hope that it
will be shortly, and lookforward to putting this behind us.
Additionally, in May of 2005, we resolved the shareholder
class action litigation matters related to this situation as well.
Tax Restatement
In connection with work by our financial team, early in the
fourth quarter we discovered that some historical tax amounts
were incorrect. Tax amounts were understated in some years
and overstated in others. The cumulative net impact of the
resulting restatement adjustments was a $48 million reduction
of stockholders equity. The most significant impact arose
from errors in the area of capital loss carryforwards, and in
large part were connected to some ofthe businesses we
acquired in the 80's and 90's, and then divested over the last
few years. We disclosed the use of the tax carryforwards in
fiscal year 2004 and the lower tax rate they produced. When
we subsequently discovered the carryforwards were incorrect.
we corrected the errors in our recent restatement. Some
of the errors appear to be the result of good faith mistakes
involving historical information, the source documents for
which go back many years in some cases. Regardless, they
were still errors so we closely scrutinized them.
Discovering and dealing with these issues has not been
an easy task, butl commend our finance leadership on the
straightforward way they handled and followed up on a very
difficult and complicated matter. Upon discovering the
errors, our finance team promptly notified the IRS and the
SEC of the situation and informed them that appropriate
corrections would be made in due course. Regardless of
the cause, the errors were totally unacceptable. We have
therefore made personnel and financial control improvements
in the tax department.



As you have no doubt seen in the press, there were class
action lawsuits filed against our company afterwe restated
our historical results. These types of lawsuits are common
when restatements occur. As a matter of policy, we do not
comment beyond the basics on legal matters, other than to
say they will be vigorously defended.
Corporate Governance. The company's board of directors
has long been committed to solid governance practices.
Several factors combined make for balanced and solid
governance at ConAgra Foods, including a board with a
majority of independent directors, appropriate independent
committees that support board focus, board discussions
that occur with and without management being present, the
strong commitment of our directors and senior executives
toward stock ownership, and other aspects you can read
about on page 78.
l would like to point out some recent changes regarding
our corporate governance policies. One such change is that
in this year's proxy, we are recommending the declassification
of our board structure; if our board is declassified, each
director will eventually stand for reelection annually instead
of serving a three-year term. As is the case with many
companies, this change will be phased in over time. Another
change we are recommending in this year's proxy is the
repeal of the current supermajority voting provision requirement
in the case of a business combination or large transaction
with a significant shareholder; if changed, only a simple
majority of shareholder votes, instead of a 75+ percent
supermajority of shareholder votes, would be required to
approve such a transaction. Our recommendations for both
of these items demonstrate our practice to continually
update and strengthen our governance practices.
Another governance change is also taking place. In 2002
we were pioneers of sorts with our insidertrading policy.
The rule put in place at that time was that our directors and
executive officers could not sell company stock, regardless
of how it was acquired, for any reason until they had left
their executive position or the board for at least six months.
While this received a lot of notice, few companies followed
our example, and regulators have not suggested or required
the adoption of anything similar. As a result, we are overly
restrictive as compared with most public companies. We
have since adopted stock ownership guidelines for officers,
and we now have a policy permitting executive officers to
sell company stock once a year if certain minimum ownership
guidelines are met. Even with this change to our previous
rule, our senior managers will still have a very strong
commitment to the company's stock, as we still require those
individuals to own ConAgra Foods stock worth a multiple
of their annual salaries.
CEO Succession Plan
As most of you know, during the fiscal year l announced that
I had asked the board of directors to initiate a search for
my successor. Fiscal 2006 marks the beginning of my tenth
year with the company. Formal succession planning and
communication are simply good business and good governance.
I asked the board to do this because my focus has been
strategically repositioning the company, and that agenda is
largely complete. ConAgra Foods is wellpoised for a new
and significant phase of execution and growth centered on
marketing and operational excellence.
Regarding the tactical aspects of the search, the search
is being headed by board member Steven Goldstone and
he is utilizing Heidrick and Struggles, a premier international
search firm that is frequently involved in senior executive
searches for many large companies.
It has been a privilege to be associated with ConAgra Foods,
and I thank all of our constituents for the opportunity to be
part of this fine team. As I have said before, l know the people,
the brands, the assets, and the potential, so l am convinced
the best is ahead of us. The opportunities stemming from
the important initiatives underway should make ConAgra Foods
a much stronger and better company in the years ahead.
lam sure that the person who succeeds me will find this
company to be one of extraordinary opportunity, and will
have the privilege of working with a talented team that is
taking the initiative to realize this company's potential.

Bruce Rohde
Chairman and Chief Executive Officer