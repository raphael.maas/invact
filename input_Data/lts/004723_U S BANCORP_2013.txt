Fellow Shareholders:
U.S. Bancorp remains true to its core strengths  serving
our customers and supporting our communities, engaging
our employees and helping our country. We reward our
shareholders by ensuring that the Company remains strong,
prudently managed and profitable.
Another record year
We achieved record net income of $5.8 billion for the year
2013 or $3.00 per diluted common share, representing a
5.6 percent increase over 2012. Our profitability measures were,
once again, industry-leading, including a return on average
assets of 1.65 percent, a return on average common equity
of 15.8 percent and an efficiency ratio of 52.4 percent, placing
us at the top of our peer group. Importantly, we returned
$4 billion, or 71%, of earnings to you, our shareholders,
through dividends and share buybacks  well within our goal
of returning between 60 to 80 percent of earnings each year.
I am particularly proud to have achieved these results during
a year that was marked by continued slow economic growth,
a significant pullback in mortgage activity and ongoing
regulatory and legislative change and uncertainty. Our results
clearly demonstrate the benefits we derive from our diverse
mix of businesses and conservative risk profile.
Credit quality continues to be strong. Total net charge-offs
declined by 30.1 percent from 2012, while total nonperforming
assets decreased year-over-year by 23.7 percent (13.2 percent
excluding covered assets). The improvement in both net
charge-offs and nonperforming assets, as well as the overall
quality of our loan portfolio, allowed us to release $125 million
of reserves for credit losses in 2013. Our Company, as well as
the industry, is expected to continue to benefit from a relatively
stable credit environment as the economy steadily improves.
We continue to generate significant capital each quarter
through our earnings. Total U.S. Bancorp shareholders
equity was $41.1 billion at December 31, 2013, compared
with $39.0 billion at December 31, 2012. Our capital ratios
exceeded both regulatory requirements and our own target
levels. We submitted our 2014 Comprehensive Capital Plan
to the Federal Reserve in early January of this year, and are
awaiting regulatory approval to, once again, raise our dividend
and continue our stock buyback program.

Heightened focus on regulation and compliance
2013 was another year of increased federal banking regulation
 with more expected to come in 2014.
As new regulations are finalized and become effective, we
respond quickly and seek to understand any impacts beyond
the initial rulings. Compliance has become a top priority for
our industry and, as a result, for every leader and employee
at U.S. Bancorp. Compliance is a foundation for trust  and
banking is a business of trust. Every bank must have a wellrun
compliance function  and we have one. Our systems,
people and policies are in place to protect the Company, our
customers and, consequently, our shareholders. We have
chosen to be active, vocal and visible within the industry and
play a leading role in coordinating with bank regulators about
the possible outcomes and, importantly, the unintended
consequences of regulatory over-reach.
We continue our focus on protecting our customers accounts
from fraud and cyber attacks. Threats to our Banks data
and customer information are persistent and increasing, and
we utilize a wide range of sophisticated fraud detection and
prevention tools to keep our Company and our customers
safe. U.S. Bank has also taken the lead to develop a
comprehensive and collaborative approach to defending
against cyber attacks on the financial services industry.
The banking industry will face more challenges in the coming
year and beyond, but banking remains crucial to the recovery
and, ultimately, the soundness of the nations economy.
Dealing directly and effectively with regulation is something all
banks must do for the sake of their customers, employees,
shareholders and the country. We take that responsibility very
seriously. We manage this company, not just for the benefit of
our reputation and the value we can return to shareholders,
but for the financial well-being of all of our constituents.
To further strengthen our focus on risk and compliance,
we recently promoted P.W. Bill Parker to the position of
Vice Chairman and Chief Risk Officer, overseeing all risk and

compliance functions at U.S. Bancorp. Bill was previously
Chief Credit Officer for U.S. Bancorp, an area he will continue
to oversee in his new role. We already enjoy a well-deserved
reputation as a leader among banks for our operating, credit
and risk profile, and Bills leadership in his expanded role
will serve to enhance that standing. Richard J. Hidy, who
previously held the role of Chief Risk Officer, retires from
U.S. Bancorp in March after 20 years of significant contributions
to U.S. Bancorp and as a leader in building our risk
management reputation. We wish Rich all the best as he
begins this new chapter in his life.
Whats ahead?
U.S. Bancorp is in an enviable position. We are in the
businesses we want to be in, and we do not face the need
to divest of any businesses due to regulatory or profitability

constraints. We continue to enjoy the benefits of the flight
to quality as customers recognize our exceptional products
and services, our superior financial performance and industryleading
debt ratings  all signs of an outstanding banking
franchise. Our prudent management culture, a disciplined
attention to measuring every aspect of our business and a
commitment to aligning expenses with revenue have resulted
in a balance sheet that is strong and growing, along with
a mix of business that is well diversified and a franchise
performance that is consistent, predictable and repeatable.
We resist the temptation to enter businesses we dont
understand; we dont follow irrational players in the market
and we remain steadfastly diligent in evaluating potential
acquisitions that could only extend our success.
2014 is beginning much like 2013 with corporations and
consumers husbanding cash and foregoing discretionary
spending and investments. Soon we will see consumers
begin to spend rather than accumulate. We will also see
corporations begin to invest, rather than stockpile. Eventually,
we will expect to see spending and credit line utilization
increase and deposits decrease as the recovery takes hold.
However, for this current stage, deposits are still growing, an
indication that a robust recovery has yet to begin. Were not
at the inflection point yet, but dialogue with our customers
leads us to believe that the recovery will accelerate in the
second half of this year. As I have said before, we are wellpositioned
to capitalize on the upturn. Until then, we will
continue to prudently manage our Company  watching our
expenses and keeping them aligned with revenue, maintaining
and growing our market share and investing for the future.
Opportunity to grow
We are asked regularly about our interest in acquisitions 
and our answer is always the same. We are interested in
deepening our market share where we already have a branch
network. Our recent agreement to acquire branches in
Chicago, which doubles our presence in that great city, is
a perfect example. We expect to be a net branch grower
in the coming years. We like branches. Some of them might
not be traditional branches but, rather, in-store or on-site
branches in partnership with supermarkets, corporations,
hospitals, colleges or other high-traffic locations. We are
not interested in leap-frogging across states or acquiring a
few branches in a state where we have no critical mass or
presence. Additionally, we would like to continue to acquire
corporate trust and payments-related portfolios and companies;
acquisitions that increase competitive and operational scale in
these high value businesses. I have referred to our acquisition
focus as one-offs. By that I mean discreet, strategic acquisitions
which are smaller, rather than transformational, that are
priced correctly and enhance our franchise, capabilities and
product set and, ultimately, make sense for our shareholders.
A strong team and a strong future
I am very proud of our record full year 2013 earnings and
results. Our Companys results are directly tied to the hard
work and dedication of our 67,000 employees, and I want to
take this opportunity to thank them for their contribution to
our success.

As we look forward to the coming year, we are mindful of
the strength of our company and how we, as a bank, remain
an integral part of the growth and vibrancy of the nations
economy, our communities and the customers we serve and
support. We are focused on the future and confident in our
ability to deliver outstanding products, service and results for
the benefit of our customers, communities, employees and,
ultimately, for you, our shareholders.

Sincerely,
Richard K. Davis
Chairman, President and Chief Executive Officer
February 21, 2014