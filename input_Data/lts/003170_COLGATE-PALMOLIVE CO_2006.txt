                          Dear Colgate Shareholder...




                                                 Q. Please comment on the Company's 2006 global business results.
                                                 A. Reuben Mark: We are delighted that Colgate's strong performance continued in
                                                 2006, our 200th year. All of the Company's fundamentals are strong and getting stronger.
                                                 Global sales rose 7.5% to an all-time record level and global unit volume from continuing
                                                 businesses grew 7.0% with every operating division contributing to the strong volume
                                                 gains. Advertising spending supporting the Company's brands rose 11% to an all-time
                                                 record level on top of double-digit growth in 2005, contributing to widespread market

                                                 share gains in all of our core categories.
"We are pleased to have ended 2006, our
                                                        Profits in 2006 include $286.3 million of aftertax charges associated with our
 200th year, so strongly. Colgate people,
                                                 previously disclosed 2004 Restructuring Program and the net aftertax impact of certain
 linked globally by our common values of
 Caring, Continuous Improvement and              Other Items that reduced net income by $9.9 million. Excluding these items, net income,
 Global Teamwork, are at the foundation of       operating profit and diluted earnings per share were at record-high levels for the year. A
 this success and remain committed to fur-
                                                 complete reconciliation between reported results and results excluding the 2004 Restruc-
 ther strengthening our growth worldwide."
                                                 turing Program and Other Items, including a description of such Other Items, is available
 Ian Cook (above right)                          on Colgate's web site.
"We made significant progress on our
                                                        Cash flow was also strong with operating cash flow reaching a record $1,821.5 mil-
 strategic initiatives in 2006 as evidenced by
                                                 lion in 2006. The Company's strong cash generation and positive growth momentum led
 our strengthening market shares world-
 wide, strong sales and volume growth, and       the Board of Directors to authorize a 10% increase in the quarterly dividend rate effective
 growing profitability. As we look to 2007,      in the second quarter of 2006.
 Colgate people remain sharply focused on
 our global business priorities and I am op-
                                                 Q. Colgate's market shares continue to increase in key categories around the
 timistic that our excellent top and bottom
                                                 world. What is driving these increases and do you expect the positive momen-
 line trends will continue."
                                                 tum to continue into 2007?
                                                 A. Ian Cook: Indeed, our market shares are improving steadily. These increases are
                          driven by our tight focus on the consumer, increasing our marketing spending, using new types of communi-
                          cation vehicles and conducting consumption-building activities in emerging markets around the world.
                               At the core of our market share growth is a steady stream of innovative new products. For example, the
                          global rollout of Colgate 360 manual toothbrush during 2006 drove significant market share gains and new
                          leadership positions in this category around the world. Our tight focus on our core categories and global
                          equities, on innovation and on understanding the way we relate to consumers is keeping our market shares
                          on an upward trend that we expect to continue throughout 2007 and beyond.
                                                         Q. What is the Company's innovation strategy?
                                                         A. Ian Cook: Our innovation strategy encompasses initiatives touching all of
                                                         the Company's business functions and processes. We have established a Global
                                                        Innovation Fund that allocates seed money to employees who recommend in-
                                                        novative ideas in any area of the business. At the same time, we are continuing
                                                       to establish partnerships with outside experts, suppliers that are technical leaders,
                                                       the academic community and leading research firms, to develop ideas for new
                                                       products and processes.
                                                            We have also established a new Global Big Hits process that identifies new
                                                      product opportunities that are of the highest strategic value for Colgate based on
                                                                                            a combination of criteria including: size of
                                                                                             opportunity, which projects have the most in-
                                                                                             cremental potential to reach new consumers,
                                                                                             which are truly new to the market and which
                                                                                             have potential for global expansion. Once
                                                                                             identified, these projects are provided ad-
                                                                                              ditional resources, including ongoing senior
                                                                                                                                          3
                                       management involvement, to get them to market faster. The Ajax Professional line of cleaning products
                                       in Europe is just one example of several successful introductions already nurtured by this process.
                                       Q. How is the 2004 Restructuring Program progressing?
                                       A. Reuben Mark: We are very pleased with what we have accomplished thus far. As you know, the
                                       four-year restructuring and business-building program that was announced in December 2004 was
                                       designed to accelerate growth and generate additional savings throughout the income statement. Now
                                       at the halfway mark, the Program is already generating sizable savings and remains on track to meet
                                       projections. The savings are being used to increase marketing spending, accelerate innovation and
                                       increase profitability. In 2006, we saw significant gross profit margin improvement, up 110 basis points,
                                       excluding restructuring charges, which is at the high end of the Company's targeted range of 75-125
                                       basis points. Including these charges, gross profit margin was up 40 basis points.
                                       Q. Driving greater efficiency has been key to funding Colgate's strong growth. Is there a
                                       limit to the cost-saving opportunities going forward?
                                       A. Reuben Mark: The more savings we achieve, the more committed we become to finding even
                                       more opportunities to further improve the way we do business and generate additional savings. With
                                       Continuous Improvement as one of our global values, we are implementing projects as resources permit
                                       from a lengthy list. Among the initiatives under way promising significant savings are globalizing pro-
                                       curement, moving toward a truly global supply chain and a new global process that will result in more
                                       efficient promotional spending, which we call Colgate Business Planning.
                                             We are also centralizing business support functions into regional and global shared-service centers,
                                       a move that is expected to generate significant savings and increase operational effectiveness for a
                                       range of support services.
                                              All this makes us confident that we will achieve our gross profit margin goal of 60% by 2010.
                                       Q. Please elaborate on the Colgate Business Planning initiative.
                                       A. Ian Cook: Colgate Business Planning is a planning and execution process designed to grow both
                                       market share and margins through more efficient and effective consumer and trade spending. Support-
                                       ed by customized SAP software, this process ensures a consistent approach to commercial investment
                                       worldwide and a global focus on maximizing the return on that investment. It allows us to set goals 18
                                       months out by customer and by brand, then measure our performance against those goals and make
                                       adjustments where indicated.
                                            While it is still in its early days, we anticipate Colgate Business Planning will generate sizable savings
                                       in 2007 and even more in 2008, helping to accelerate our margin and top-line growth and ultimately
                                       contribute to increasing market shares worldwide for both Colgate and our trade partners.
                                       Q. Recently, Colgate has stressed the importance of its professional and trade relationships.
                                       What is the Company doing to strengthen these relationships and how are they contribut-
                                       ing to growth?
                                       A. Ian Cook: Strong relationships with the profession and the trade are important components of our
                                       growth strategy. With dental professionals, for example, we have developed a 10-point plan to encour-
                                       age more usage and recommendations of Colgate brands because such endorsements drive trial of
                                       products as well as long-term loyalty. Country-by-country, we are implementing the plan, which sets
                                       guidelines and objectives for professional sales, sampling, obtaining dental seals of approval, attending




                                    dental conventions and participating in Oral Health Month among others. Today, we have plans in place in
                                    countries representing more than 80% of Colgate's toothpaste sales.
                                         Equally important are Colgate's multifunctional selling teams that support all of our customers world-
                                    wide. These customer-focused, cross-functional teams collaborate to develop more efficient ways of growing
                                    the categories where we compete, using resources better and sharing their expertise. As a result, the right
                                    products get to the right place with the right price and promotional plan, faster and more efficiently for a
                                    win-win experience for both Colgate and our customers. In Mexico, for example, these efforts contributed to
                                    Wal-Mart naming Colgate its 2006 Logistics Supplier of the Year out of over 11,000 suppliers.
                                    Q. What is Colgate doing to develop its next generation of leaders around the world?
                                    A. Reuben Mark: We recognize that the long-term success of Colgate is dependent upon the depth and
                                    breadth of management. The Company uses a formal process to identify next-generation leaders early in
                                    their careers. Their career paths are laid out to build an array of required skills and prepare them for leader-
                                    ship roles, ranging from global assignments to short-term projects to recommending courses from Colgate's
                                    global curriculum of 150 training programs that teach leadership and functional skills. As their careers pro-
                                    gress, they are given broad in-market experience across geographies and functions plus training that instills
                                    Colgate's values and culture.
                                    Q. Please comment on the Company's outlook.
                                    A. Ian Cook: Colgate ended 2006 with excellent business momentum. We have clearly defined strategies
                                    in place with a tight focus on our higher margin priority businesses of oral care, personal care and pet nutri-
                                    tion. We are firmly committed to getting closer to consumers everywhere, to strengthening our partnerships
                                    with the profession and our customers, to fostering innovation in all areas of our business and to getting
                                    more effective and efficient in everything we do.
                                         As we look to the future, we are confident that our positive business momentum will continue and en-
                                    able Colgate to deliver excellent quality, double-digit earnings per share growth in 2007, excluding restruc-
                                    turing charges.
                                    Thank You,




                                     Reuben Mark                                                                    Ian Cook
                                     Chairman and Chief Executive Officer                                           President and Chief Operating Officer
