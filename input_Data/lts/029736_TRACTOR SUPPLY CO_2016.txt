To Our Stakeholders:
Over our 79-year history, Tractor Supply has grown to be the largest and most
dependable supplier of everyday basic products to the rural lifestyle customer.
Our customers depend on us for everything from fencing equipment to animal
feed, outdoor power tools, truck supplies and lawn & garden materials. We know
the rural lifestyle customer and their unique needs because many of our team
members are rural lifestyle customers themselves with these same needs. Our
stores all across America service a unique customer base who values working
in the outdoors, repairing things themselves, caring for livestock and pets and
tending to their land. We call this the Out Here lifestyle, and we take great pride
in being part of this lifestyle and serving this customer.
LOOKING BACK
While it was a challenging year from a sales perspective, there was much to be
proud of in 2016. Continued challenges in energy markets, as well as a lack of
customer demand for several categories of weather-related products, presented
significant hurdles. I am proud of how our team rolled up their sleeves, provided
legendary service to our customers and executed on our mission to be the
most dependable supplier of basic maintenance products to farm, ranch and
rural customers despite the headwinds. In the broader macro environment, our
customers continue to depend upon Tractor Supply for their everyday basic
maintenance products for their farm, ranch or business. We think this is best
reflected in our same store sales transaction count, which increased 2.6% in
2016 (representing over a decade of same store transaction count increases). 
MOVING FORWARD
As we look forward to 2017, we have many opportunities to grow and
strengthen our business. We will invest in team member development,
further enhance our merchandise assortments and expand our information
technology capabilities to better meet the evolving needs of our customers. At
the same time, we will remain focused on our goal of returning value to our
shareholders through sustainable growth strategies.
Our strategic priorities in 2017 include:
 Continuing the growth of our store base in both Tractor Supply and Petsense
to bring our unique product offerings closer to where our customers live
 Providing our customers with new products by utilizing our test and learn
platform for product innovation, quality and value in both our exclusive
brands and national brands
 Growing customer engagement by leveraging new digital technology tools
to ensure the customer experience is the same at every touch point within
Tractor Supply, giving the customer the choice to interact with us anytime,
anywhere and anyway they prefer
 Enhancing Customer Relationship Management (CRM) capabilities with our
new Neighbors Club Program to ensure targeted, relevant communication
across all channels of our business
 Expanding our supply chain capabilities to provide greater service and
convenience to our customer through improved inventory in-stocks,
convenient fulfillment options and faster store replenishment turnaround time
 Connecting all of these capabilities to achieve a ONETractor perspective to
support our long-term growth strategy
WE ARE CONFIDENT
We are committed to being the most dependable supplier of everyday basic
products to the Out Here lifestyle customer and driving value for our
shareholders. We will do this by taking a balanced and disciplined approach
by reinvesting into the business for long-term growth, returning excess capital
to shareholders through regular quarterly dividends and share repurchase. At
a time when customers want the ability to choose how and when they shop,
we are committed to opening new stores and expanding our digital footprint
with new capabilities.
We remain committed to our corporate culture, our Mission and Values, our
unique customers and our trusted stakeholders. We have a differentiated
business model that has grown and delivered strong results over the past
79 years. I am personally excited about our future and believe our success
is directly related to our extraordinary 26,000 plus team members, our loyal
customers, our strong vendor relationships and our commitment to our
stakeholders to manage the business for the long term. We move forward with
our future in mind, and we believe we have the right strategy and strategic
initiatives in place to drive long-term growth for many years to come.
Greg Sandfort
Chief Executive Officer