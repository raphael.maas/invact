Letter to Shareholders


                                    Fellow Shareholders:

                                    Compuware remained profitable and debt-free during fiscal 2003, enhancing our ability
                                    to produce competitive offerings that deliver the productivity and value customers
                                    demand. In our 30th year of continuous operation, we have excellent cash flow, more
                                    than $500 million in cash and investments, and exciting new initiatives we believe will
                                    help us grow the company.

                                    We achieved our financial status in spite of a sluggish economy, excess capacity and
                                    increased price pressure from competitors and customers. For the third consecutive year,
                                    spending on Information Technology (IT) was flat, making sales cycles longer and deals
                                    more difficult to complete.

                                    While it is difficult to predict when market conditions will improve, we are confident
                                    the market is going to change before long, and we are well positioned to take advantage
                                    of any upturns. We are equally confident in the four strategic initiatives we're
                                    launching in fiscal 2004. These initiatives complement and extend our existing
                                    products and services, helping customers quickly and reliably put business applications
                                    into action. Compuware's four strategic initiatives for this fiscal year are the
                                    Compuware Application Reliability Solution (CARS), OptimalJ, Vantage and our
                                    NearShore Development Center.

                                    CARS--Software quality, reliability and time-to-market are some of the biggest
                                    challenges facing IT organizations today. CARS helps IT organizations meet these
                                    challenges through the use of highly skilled people, a solid testing methodology and
                                    industry-leading products. By linking development, quality assurance and operations,
                                    CARS is the only solution on the market that offers customers the people, processes
                                    and products necessary to ensure the quality and reliability of their most important
                                    business applications.

                                    OptimalJ--Developing enterprise applications using Java technologies is complex and
                                    difficult, slowing down advanced Java developers and causing difficulty for mainstream
                                    developers. OptimalJ is an advanced development environment that enables developers
                                    of all experience levels to rapidly design, develop and deploy reliable, industrial-strength
                                    enterprise applications. Increasing productivity by as much as 40 percent, OptimalJ
                                    generates complete, maintainable working applications directly from a visual model,
                                    using sophisticated patterns to implement accepted best practices for J2EE coding.

                                    Vantage--The primary measure of a Chief Information Officer's success is the delivery of
      "I can assure you that our
                                    reliable, high-performing software applications that support the core business needs of
                                    the organization. The focus of Compuware's Vantage suite is to provide unsurpassed
         management team and
                                    visibility and control over software application deployment, monitoring and problem
                                    resolution in complex data processing environments.
 employees are giving you their
                                    NearShore Development Center--More than ever before, companies today are
very best efforts toward meeting    initiating cost-saving efforts by outsourcing application development and maintenance
                                    to external service providers. Alongside our traditional services business--which offers
 Compuware's goals for the new      the services of skilled IT professionals at a reasonable cost--we also offer customers our
                                    NearShore Development Center (NSDC) in Montreal. Compuware's NSDC is a low-
fiscal year. As our shareholders,   cost vehicle that delivers value to our North American customers who desire compre-
                                    hensive outsourcing services for the support of their distributed, e-business or
           you deserve no less."    enterprise software.
We are currently engaged in substantial launch, marketing and public relations efforts
that are focused specifically on these four key initiatives. Our ability to conduct these
activities is supported by our almost 30-year affiliation with mainframe software--the
bedrock upon which our business was built. It has afforded us a substantial maintenance
base, high profit margins, quality business prospects and longstanding relationships with
practically all Fortune 1000 companies. Compuware mainframe software sales also give us
a steady source for financing the research and development necessary to be successful in
the distributed software sector.

Maintenance renewal rates for our mainframe products continued to be very high during
fiscal 2003, solid evidence that our software continues to deliver value to customers every
day. To further demonstrate the payback our mainframe applications provide over the
competition, we've developed Return-On-Investment calculators for our mainframe
products. These tools are already being used successfully by our sales force to demonstrate
to customers the large cost savings our mainframe software products deliver.

I can assure you that our management team and employees are giving you their very best
efforts toward meeting Compuware's goals for the new fiscal year. As our shareholders,
you deserve no less.

Thank you for your investment in Compuware Corporation.

Sincerely,




Peter Karmanos, Jr.
Chairman and Chief Executive Officer
Compuware Corporation
