Annual Report 2016     
Citrix Systems, Inc.
Looking back on 2016, I am incredibly proud of what Citrix accomplished. It was a year of 
significant transformation for our company. We sharpened our vision, renewed our focus 
on our core strategy, and built a strong foundation that will propel our success in the future.
We set forth on our journey to the cloud, sharing our vision that will shape the way 
we move forward. We announced our new mission to power a world where people, 
organizations, and things are securely connected and easily accessible, so our customers 
can make the extraordinary possible. And we laid out our strategy to achieve that mission: 
We will build the worlds best-integrated technology services for the secure delivery of 
apps and data, anytime, anywhere, on any network  all as a service from the Citrix Cloud. 
At Citrix Synergy 2016  our annual customer conference  we announced our renewed 
strategic partnership with Microsoft  an effort that has already seen mutual customer 
wins, deepened strategic alignment across our product and sales organizations, and 
integrations that position Citrix as a leader for the secure delivery of Microsoft apps and 
desktops. As the virtualization leader that can support the largest number of unique 
scenarios within the Microsoft platform, we see Citrix as a strong beneficiary moving 
forward of an expected increase in the pace of adoption of Windows 10 in the enterprise.  
In 2016, we rapidly increased the pace of our innovation. We focused on end-user 
productivity, unmatched performance and scalability of our products and solutions, 
and the overall end-to-end customer experience. And we pushed toward a cloud-first 
development mindset that will allow us to continue this trend.
We have also made significant progress in advancing Citrix culture. We looked to our 
people and empowered them to drive change. We unveiled the new Citrix core enduring 
values, keeping those values of Integrity and Respect that have always made Citrix great, 
but adding the values of Curiosity, Courage and Unity to remind ourselves to never stop 
learning, to be bold in the pursuit of our dreams, and to know that we can only win when 
we work together.
Demonstrating the value of Unity and truly living it, we transitioned Citrix to a functional 
business structure, unifying our business units into one cohesive organization. These 
changes enable us to move faster, act as a single, powerful force and execute as one 
company. In the same spirit, in July 2016 we announced the now-completed separation 
of our GoTo Business and its merger with LogMeIn. While this transaction and the 
reorganization of our teams required significant effort in 2016, it sets us up to execute in 
2017 as an agile and focused company. 
2016 Business Performance
Our results for the year demonstrated that our efforts were resonating in the market. We 
grew our revenue to $3.42 billion, up 4% year over year. As a result of global leadership 
changes, we had positive momentum in all geographies. And we saw renewed momentum 
in Workspace Services, with the last three quarters of 2016 showing continuous growth. 
To our shareholders, 
customers, and partners:
Kirill Tatarinov
President & CEO 
  Overall, we delivered 
strong results in 2016, 
and created a foundation 
that will enable us 
to deliver sustained 
profitable growth in
2017 and beyond.
Annual Report 2016     
Citrix Systems, Inc.
Overall, we saw a record 257 customer transactions of more than $1 million each, which 
reflects the trust that our customers have in our technology. We also saw solid results 
in our cloud transformation, ending the year with nearly 50 customers running their 
enterprise workspaces in Citrix Cloud. 
We continued to improve our operational rigor, increasing our non-GAAP operating 
margin to 30.8%, up from 25.7% in 2015. We delivered a solid non-GAAP earnings per share 
of $5.32 per share, up from $4.34 per share in 2015. Later, in the annual report section, you 
can find a full reconciliation between our non-GAAP and GAAP performance.  
At the close of 2016, we had $2.66 billion in cash and investments, a sharp increase from 
the previous year, and we saw a record cash flow in 2016 of $1.12 billion.
Overall, we delivered strong results in 2016, and created a foundation that will enable us to 
deliver sustained profitable growth in 2017 and beyond.
Looking ahead to 2017, we have a strong roadmap across our product portfolio  a 
roadmap that will allow us to deliver the workspaces of the future, all from the cloud, 
and all as a service. The momentum of Citrix Cloud is accelerating, a trend we expect to 
continue into 2017 because of how much easier and faster the cloud makes it for our 
customers to deliver our solutions to all of their employees. The cloud also enables us to 
reach customers in the mid-market through the simplicity and ease of use that comes with 
this model.
It is an exciting time of digital transformation in the world, as enterprises begin to embrace 
smarter technologies and newer ways of computing to address a variety of growing 
business challenges  lagging productivity, aging infrastructure, and most importantly, 
the ever-growing threats around cyber security. More and more customers recognize 
that Citrix technology enables them to build secure IT architecture, and that our solutions 
provide one of the surest ways to defend their enterprises against cyber-attacks, which are 
currently top-of-mind for all organizations. We are positioned to deliver on these growing 
needs for information security and smarter technology now, more than ever. 
Driven by our improved operational rigor and the strength of the leadership that joined 
our global organization in 2016, we foresee strong product momentum and innovation 
in 2017. By living our values, we can move swiftly and firmly along our journey of 
transformation as a unified force. 
On behalf of the Board and our employees, I thank you for your support and confidence. 
It is that support that fuels our determination to deliver technology that truly makes the 
extraordinary possible.
Sincerely,
Kirill Tatarinov