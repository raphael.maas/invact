                          A LETTER TO OUR SHAREHOLDERS,
                          CLIENTS AND ASSOCIATES
                          Cerner had a great start to the new millennium, delivering strong financial and operating results and making meaningful progress
                          in our efforts to transform healthcare.

                          SUMMARY OF THE YEAR 2000
                          Impressive Financial Results: In 2000, we set record levels of bookings, revenues and cash flow. For the year ended December 30, 2000,
                     revenues increased 19 percent to $404.5 million from $340.2 million for 1999, demonstrating a strong rebound of growth within our busi-
                     ness. Our record new bookings, included our highest quarter ever when we recorded $113.4 million in the fourth quarter. Net earnings
                     before non-recurring and extraordinary items increased 197 percent to $20.4 million compared with $6.9 million in the prior year. Diluted
                     earnings per share before non-recurring and extraordinary items were $0.57 per share compared with $0.20 per share for the 1999 year,
                     demonstrating our ability to grow earnings in a difficult healthcare environment. Operating margins expanded, excluding non-recurring
                     items, for the year to 9.2 percent, more than double the prior year's 4.3 percent and demonstrating the overall strength of our business model.
                     Cash flow increased which led to improving an already strong balance sheet.
                           Increased our market presence: During the last half of the `90s, we completely rebuilt our Health Network Architecture (HNA), creat-
                     ing the MillenniumTM version and expanding our product line from 11 healthcare information products to more than 37 at the end of 2000.
                     Over the last 18 months, we more than doubled our direct sales force. Also, during 2000, we created a strategic, direct Cerner presence in
                     Europe. Between the new client relationships created through our direct sales efforts and the acquisitions we completed during the year, we
                     grew our client base by 40 percent. These 430 new client relationships substantially increase the potential cross-selling opportunities for our
                     technology. We created more than 330 new HNA Millennium footprints during the year. Cerner's total client base now numbers more
                     than 1,500. We also signed licensing agreements with more than 80 other healthcare information technology (HIT) companies to use HNA
                     components, either software or content, as key elements of their architecture under our Power By CernerTM program.
                           Expanded our platforms, market reach, products and services: Our hosting services, Cerner OnlineTM, where we provide our clients the
                     technical infrastructure and services for Cerner solutions in our world-class data center, became the delivery platform of choice for more than
                     $50 million of new bookings in its first year. Many of these clients are community hospitals and represent a significantly expanded market
                     for our products. The expanded sales force is addressing these and other increased opportunities. PowerChart OfficeTM, Cerner's physician
                     clinical practice solution, is having a significant impact on the way many of our clients practice medicine. And ProFitTM, Cerner's new patient
                     accounting solution, completed alpha testing, opening up a market estimated at $3 billion.
                           Improved our business model: Historically, our revenue visibility has been as low as 60 percent. In 2000, we were able to start each peri-
                     od with close to 85 percent of that period's revenue already in our backlog or available from highly predictable sources. Our consulting
                     organization experienced record levels of profitability, increasing their margins substantially and contributing positively to our corporate prof-
                     itability. Based on a healthy backlog of profitable work, we expect the consulting profitability to continue to increase over the next several
                     years.
                          Delivered Impressive Operating Results: We now have more than 800 applications in production on our HNA Millennium product set
              in more than 125 client organizations worldwide. Over the last several years, we have reduced implementation times and resources by 50

percent. Today, over 85 percent of Cerner Consulting associates have experience with our new architecture, up from 40 percent two years
                     ago. HNA Millennium has scaled to support varied environments, with more than 6,000 active users at our largest client. Our overall sys-
                     tem reliability has increased to more than 99.9 percent, as measured by reported unscheduled downtime. And it is still improving. Our sup-
                     port calls have not increased even though we have increased the number of converted applications by nearly 25 percent in the past year.
                           Created Winona Health: We formed a unique partnership with a Midwest community with a population of approximately 25,000,
                     Winona, Minn., where we are in the process of conducting a five-year study to investigate the impact greater connectivity will have on the
                     health status of a community. We are working with the local medical community to create a personal health system for all of the commu-
                     nity's citizens. Today, 48 of the 50 Winona physicians have agreed to "wire" their practices to allow electronic communications with their
                     patients for scheduling requests, prescription refills, and communicating results and treatment plans, creating the basis of a new medium of
                     health care. We also are "wiring" all four of the locally-owned pharmacies, the local clinical laboratory, the emergency room, the acute care
                     hospital and two nursing homes. (See "Connecting the Person," below.)




                                                                                                                        
                                                                                                                                                             YEAR 2000 BROUGHT COMPETITIVE LANDSCAPE CHANGES
         Fortune 100 Best Employer Award: In 2000, Cerner earned its second Fortune magazine "100 Best Companies To Work For" award in
                                                                                                                                                             Our competitive landscape changed as much in 2000 as almost any of the previous five years. What a year!
    three years. We were the 56th best company to work for in the country, according to this national business publication.
          Cerner Stock Price: The Cerner shareholder was rewarded in 2000 with a 135 percent increase in stock price (comparing the closing
                                                                                                                                                              2000 started with a consensus belief that Internet-based companies would revolutionize our industry, and these "new age" companies
    price on December 31, 1999 to the closing price on December 29, 2000), even more impressive when compared to the 41 percent loss for
                                                                                                                                                               would consolidate the traditional companies along the way. Before the dot-com bubble burst, there was a rapid consolidation of
    the Nasdaq during the same period. In the Wall Street Journal's Shareholder Scoreboard published February 26, 2001, Cerner ranked as the
                                                                                                                                                               these companies, merging about as fast as they were created. Next came the well-publicized implosion of the dot-com companies in
    31st best performer over the last 10 years out of the 1,000 companies ranked in the study; and produced the 3rd best 10-year performance
                                                                                                                                                               March of 2000, including those focused on healthcare. The realization was that the Internet is a revolution in connectivity, not a
    of all software companies, finishing ahead of companies such as Microsoft.
                                                                                                                                                               revolution in the way of doing business. The second realization was that healthcare is complex and difficult. The conclusion: Cerner
                                                                                                                                                               has a bright future.
         CERNER EMERGES AS THE CLEAR LEADER IN AN IMPROVING
         HEALTHCARE INFORMATION TECHNOLOGY MARKET, SETTING THE STAGE
                                                                                                                                                              Along the way, our two large competitors disappeared into even larger companies. The first one did so in early 1999, the second
         FOR AN EXCITING FUTURE
                                                                                                                                                               one during 2000. Exit the largest HIT companies; enter much larger equipment and drug companies. We do not expect this to
          In 2000, Cerner emerged as the clear leader in the healthcare information industry, even as healthcare provider organizations struggled
                                                                                                                                                               be the last time a large capitalization company enters this market. Cerner has now become the largest independent company in
    with low operating margins, access to capital, labor shortages and public scrutiny of fundamental systemic flaws in healthcare delivery. The
                                                                                                                                                               this business.
    recently published Institute of Medicine report, Crossing the Quality Chasm, clearly articulated the central role of information technology
    in reinventing our healthcare capabilities: "If we want safer, higher-quality care, we will need to have redesigned systems of care, including
                                                                                                                                                              Our industry has a history of a large number of niche companies. The industry consultants coined the term, "best of breed"
    the use of information technology to support clinical and administrative processes."
                                                                                                                                                               to describe these niche, stand-alone products. The year 2000 saw the market turn on the "best of breed." Cerner acquired the assets
          Relative to 1999, the healthcare provider market for information technology clearly improved in 2000. The impact of the Balanced
                                                                                                                                                               and the operations of six smaller HIT companies, which had literally run out of steam, and on average paid less than one times
    Budget Act (BBA) was assimilated by healthcare executives. Healthcare organizations made the budgetary adjustments to the changes in
                                                                                                                                                               revenues. The cost of competing in this industry is getting larger, mainly due to the high cost of development, which is caused
    reimbursement that came from Medicare, at the same time Congress passed some BBA relief, putting some funds back into the system. Our
                                                                                                                                                               by the skyrocketing cost of programmers and the complexity of contemporary information system architectures. These factors and
    clients still have thin operating margins and their access to capital is constrained, but healthcare executives are beginning to focus on creat-
                                                                                                                                                               our investment in HNA Millennium are significant barriers to entry and represent a strong opportunity for Cerner.
    ing better organizations and more effective processes.
          As we projected in last year's annual report, spending on new projects by our clients in 2000 expanded in the wake of the Y2K situa-
                                                                                                                                                             OUR 2001 PLAN
    tion. With the passing of the Y2K issue, funds that had been set aside to handle remediation were freed for new projects.
                                                                                                                                                              The first four of our five business imperatives for 2001 have been basically the same since 1998. We believe it is essential to grow our
          Going forward, we believe this is one of the best environments we have seen in the last ten years. We anticipate a relatively stable,
                                                                                                                                                        top line, expand our operating margins, set the standard for operational excellence in our industry and connect the person to their health-
    provider-friendly set of policies and practices coming out of Washington, which impacts as much as 50 percent of the revenue of our acute
                                                                                                                                                        care system. Our new imperative added this year is to advance our leadership of the market, with the ultimate goal of transforming health-
    care clients. The managed care organizations, which have a cyclical business, have been getting rate increases, which in part are being passed
                                                                                                                                                        care delivery  making it safer, more efficient and better able to meet the needs of healthcare consumers.
    along to the provider organizations.
          The largest internal pressure point for providers is that almost all healthcare organizations' core method of operations are paper-based,
                                                                                                                                                             GROWING OUR REVENUES OVER THE NEXT DECADE
    manual systems which depend heavily on the memories of doctors and nurses to make the appropriate decision at the appropriate time. In
                                                                                                                                                              From 1990 to 2000, we maintained a 10-year compounded average revenue growth rate that exceeded 20 percent. We believe that we
    the increasingly complex medical environment, these processes are inefficient, and result in a large number of avoidable medical errors, and
                                                                                                                                                        are well positioned to continue good growth rates into the future. Here are our thoughts:
    produce a huge variance in how physicians diagnose and treat the same medical condition. These variances of course lead to large variances
                                                                                                                                                              Over this last decade, our environment has been extremely tough. Healthcare, and particularly healthcare providers, struggled as an
    in the outcomes we receive as patients in the system.
                                                                                                                                                        industry. Managed care became a major factor in the early `90s, extracting margins and forcing operational and clinical improvements.
          The most significant external pressure on healthcare organizations is increasing dramatically as the public becomes more informed about
                                                                                                                                                        Strategies to cope were abundant, but they all failed. The provider industry consolidated both horizontally and vertically, creating unwieldy
    the waste, errors and variance produced by the current healthcare system. The USA Today, March 2, 2001, front page headline following
                                                                                                                                                        organizations with few operating benefits. A number of for-profit organizations were created focusing on physician care or acute care sys-
    the release of the IOM document was: "Report: Health System Broken". The article also cited the 1999 IOM report, which document-
                                                                                                                                                        tems with better management approaches. After a brief period of excitement, these techniques lost their luster, and management abandoned
    ed that there is evidence that up to 98,000 lives are lost to avoidable medical errors in the nations hospitals in the United States each year.
                                                                                                                                                        their strategies, leaving dysfunctional pieces of healthcare in many communities. And in the last four years, healthcare has had a massive
    There is no data to estimate the number of deaths which occur outside the hospitals.
                                                                                                                                                        margin crunch caused by the 1997 Balanced Budget Act, and all of the information systems world -- especially healthcare -- was dramat-
          This environment is creating what we call "wild cards" that could increase the rate of adoption of information technology significantly
                                                                                                                                                        ically impacted by Y2K. On top of all the environmental issues, Cerner completely rebuilt its HNA architecture and all of the application
    beyond our current plan. An example of such a wild card is the Leapfrog Group, a subgroup of large employers from the Business
                                                                                                                                                        product sets.
    Roundtable, that is becoming a forceful proponent for change. Leapfrog recommends that employers select health plans with hospitals that,
                                                                                                                                                              We grew Cerner well throughout this period by focusing on a clear strategy of organic growth based on aggressive software develop-
    among other recommendations, use a computerized, physician-order-entry system. Also, legislators are increasingly logging in on the safe-
                                                                                                                                                        ment, using a common architecture focusing on the person. This strategy has created two decades of growth. From 1990 to 2000, our
    ty issues in healthcare. California recently passed a bill requiring every hospital in the state to prepare and file a plan on safety improvement
                                                                                                                                                        annual new business bookings increased from $9.6 million to $399 million. We believe we have laid the foundation to continue our growth
    within their organizations by January 1, 2002. Additionally, some form of patient safety legislation is proposed in 40 different states.
                                                                                                                                                        into this decade.
    Congress is considering including patient safety in a Patient Bill of Rights in the current session.
                                                                                                                                                              Our organic growth strategy and our attitude toward future growth is illustrated with PathNet, our original application and market-
          Another potential wild card is the new privacy provisions of the Health Insurance Portability and Accountability Act (HIPAA), which
                                                                                                                                                        leading, clinical laboratory system. PathNet was first converted in 1982 at St. John Medical Center, in Tulsa Okla. In 1985, PathNet under-
    strengthened the case for Cerner's integrated architecture strategy. In its coverage of both electronic and paper medical records, the new
                                                                                                                                                        standably constituted 100 percent of our new contracts. In 1990, it was still close to 90 percent of our bookings revenue. In 2000, even
    HIPAA provisions point toward a centralized and systematized method of access control that we think is best met with a single integrated
                                                                                                                                                        after recording the highest level of laboratory system bookings in the Company's history, PathNet contributed only three percent of our total
    architecture.
                                                                                                                                                        new contract bookings. Products introduced after PathNet contributed the remaining 97 percent. The unlocking of the human genome
                                                                                                                                                        will explode the role of laboratory medicine, creating a new role for predictive medicine, in addition to diagnostic medicine. Exciting. Plus
                                                                                                                                                        we have virtually no market share in laboratory systems outside of North America. We expect to be able to continue to grow our PathNet
                                                                                                                                                        revenues well into this decade.



                                                                                                                                                          EXTEND CERNER'S LEADERSHIP POSITION
         However, the most impressive part of our outlook for growth has to do with our clients' environment. Unlike the `90s, we expect the
                                                                                                                                                            Healthcare has very large, systemic issues. We believe that Cerner must continue to create innovative solutions and services to
    issues facing healthcare will strongly encourage the industry to completely endorse and adopt information technology as it has never done
                                                                                                                                                      make fundamental changes to healthcare and be an advocate for these necessary changes to make healthcare much safer, more efficient and
    before. In our opinion, major factors such as cost, efficiency, safety, quality and sensitivity to the consumer will have to be dealt with over
                                                                                                                                                      capable of consistently producing higher quality results.
    the next 10 years, creating a very good environment for growth of Cerner. This, in conjunction with how well we are prepared internally,
                                                                                                                                                            One of the keys to extending our leadership is capitalizing on our ability to deliver real knowledge-based medicine as we develop our
    creates true optimism and confidence for future revenue growth.
                                                                                                                                                      products. Through the power of Cerner's single, integrated architecture, we believe that healthcare will make a dramatic leap forward in the
         EXPAND OPERATING MARGINS                                                                                                                     quality and safety of the care it delivers. We see a transformed healthcare system in the future, with healthcare organizations that: make no
                                                                                                                                                      avoidable errors, efficiently operate in a paperless environment, consistently deliver the highest quality of care based on the most current
          We believe that operating margins of 20 percent are possible and desirable for our type of business. Cerner operated for three years,
                                                                                                                                                      knowledge available, and are highly sensitive to the needs of the persons and communities which they serve.
    1993-95, at operating margins greater than 20 percent. These margins make it possible to invest in the information technology and
                                                                                                                                                            To make that transformation possible will require strong leadership and serious commitment, not just from Cerner's top executives,
    architecture that healthcare needs to address the issues illuminated by the IOM report. As indicated above, we doubled our operating
                                                                                                                                                      but from our managers and all associates. This is an important focus for us, as illustrated by a recent episode in which our CEO, Neal
    margins in 2000, from 4.3 percent to 9.2 percent. We intend to continue to work to increase our operating margins.
                                                                                                                                                      Patterson, challenged our managers to ensure the full productivity of our associates. He also asked for and has received heightened focus and
          The improvements in our business model, growing revenues faster than spending, and general gains in quality, productivity and
                                                                                                                                                      renewed energy. This focus and energy is needed to meet our clients' immediate needs, solve their future challenges and move healthcare
    efficiencies are the keys to making these fundamental improvements. Growing our revenues while expanding our operating margins will
                                                                                                                                                      forward dramatically.
    make for a financially vibrant Cerner.
                                                                                                                                                            Cerner has an exciting, promising future. We have momentum. We also have what we believe to be the most important ingredients
         OPERATIONAL EXCELLENCE                                                                                                                       for the future: a strong vision and a very talented, committed organization. It was these two ingredients that made 2000 a success. It will
                                                                                                                                                      be those two ingredients which will create our future.
         We plan to set the standard for quality in all of healthcare. Our business is complex, it demands excellence. Our vision is large,
    it demands excellence. We need to improve everything we do. We must improve how we develop software and knowledge content,
    improving its quality, reliability and our efficiency in its manufacture. We must improve our professional services, improving the advice and
    counsel we provide in how to transform healthcare organizations. We need to become more efficient in order to reduce our cost to our clients
    while we improve the value of our services. We must improve client service, the way we support the large number of clients who have our
    technology running their clinical, managerial and financial processes. A large job, a large challenge. If we continue to make the progress we
                                                                                                                                                         
    have over the last several years, we will establish the operational standards in our industry.

         CONNECTING THE PERSON
         Connecting the person (the consumer) to their physician and the rest of the healthcare community is an imperative for Cerner and
    a fundamental part of the value information technology can bring to healthcare (see "Created Winona Health," above). We believe it will
    create profound changes in healthcare, moving the center of healthcare closer to the person, empowering them to become more responsible
                                                                                                                                                           
    for management of their health. As the Institute of Medicine noted in Crossing the Quality Chasm:

         ...systems must be designed to serve the needs of patients, and to ensure that they are fully informed, retain control and
         participate in care delivery whenever possible, and receive care that is respectful of their values and preferences... Patients should
         have unfettered access to their own medical information and to clinical knowledge. Clinicians and patients should communicate
         effectively and share information.
                                                                                                                                                           
         Our Winona Health project will help Cerner and this industry understand the value of this connection. We believe it will be very
    valuable to the shareholders, as well as to the communities in which we live.



                                                                                                                                                           
