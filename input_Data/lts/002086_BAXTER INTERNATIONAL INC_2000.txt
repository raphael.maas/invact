
Dear Shareholders:                         [A Successful Year: 2000] In Baxter's 1999 annual report, I discussed
A year ago, I wrote to you after           our focus on three key goals: building the Best Team in health care; being
completing my first 12 months as           the Best Partner to customers and patients; and being the Best Investment
Baxter's chief executive officer, and      for you, our shareholders. I am very pleased with our progress on these
shared with you my vision for Baxter.      three goals. I also discussed our Shared Values of respect, responsiveness,
This year, having completed my first       and results as the foundation of everything we do. Our passion to achieve
year as chairman of Baxter's board         these goals and live our values makes it possible for us to achieve our
of directors, I am even more excited,      business objectives.
based on the results of a successful          As a shareholder, your interests, of course, are focused on the success
year 2000, and more importantly, what      of your investment. We are very proud of our investment results. The best
I believe are even better prospects        barometer of our performance is Baxter's total return (the sum of stock
for a dynamic decade of accelerating       appreciation plus dividends) compared to other indices. In the year 2000,
growth for your company.                   the combination of the increase in Baxter's stock price and the dividend
   First, let me remind you that Baxter    from the spin-off of Edwards Lifesciences gave you a return of more than
serves an extremely important role. The    49 percent. Baxter's performance surpassed that of our peer group, the S&P
products and services that we provide      Healthcare Composite Index, and was far higher than the S&P 500 and
help people with hemophilia, kidney        Dow Jones Industrial Average, both of which yielded negative returns in
disease, cancer, immune deficiencies       2000. Our compound annual return since 1993 is 29 percent.
and other disorders. Our mission is           Also last year, I outlined for you specific financial commitments for the
to provide critical therapies for people   year 2000. They included increasing sales approximately 10 percent, growing
with life-threatening conditions. We       earnings in the mid-teens, and generating at least $500 million in opera-
continue to enhance these therapies        tional cash flow after investing more than $1 billion in capital expenditures
and make quality health care available     and research and development. I am happy to report that we met all of
to more and more people around the         our commitments, and I am confident that the momentum we have generated
world. All of us on the Baxter team are    will continue into 2001 and beyond.
very proud of our mission.
                                           [Growth and Innovation] We have made tremendous progress in recent
                                           years in sharpening our operational focus, improving our financial position,
                                           and positioning Baxter for the future. We are now entering a phase in
                                           which our focus is to significantly increase the sales and earnings growth
                                           of the company while continuing to introduce medical breakthroughs that
                                           will make significant inroads in the treatment and prevention of disease.
                                              Nearly 80 percent of our sales are in markets in which we hold leading
                                           positions. Yet, even in these markets, there are tremendous growth oppor-
                                           tunities. As noted on the opening page of this report, many people in the




world with life-threatening diseases currently go untreated or are undertreated     products and services that will contribute
because their countries have not yet reached a stage of economic develop-           to the accelerated growth of the com-
ment to provide broad access to quality health care. The aging population           pany; E-business, giving us the ability
is creating additional needs, as people require a disproportionate amount           to significantly increase productivity and
of health care in their later years. This creates a tremendous opportunity          get closer to our customers, business
for Baxter.                                                                         partners and patients; and speed in
    Given our global presence, Baxter is uniquely positioned to meet these          decision-making, reducing bureaucracy
health-care needs around the world. Currently, more than 50 percent of our          and making us more agile in under-
sales are generated outside the United States. Another advantage is our             standing and meeting customer and
manufacturing strength. Today, we manufacture more than 85 percent of               patient needs.
what we sell, and we are the highest-quality, best-cost manufacturer in                 I believe we are strongly positioned
virtually everything we produce.                                                    for a great 2001, and equally important,
    Baxter also is uniquely positioned because of our focus on critical therapies   for a great decade ahead. Thank you
for life-threatening conditions. We are increasing our investments in research      for your support. I can promise you that
and development to provide the best and most cost-effective therapies for           the dedicated Baxter team of 45,000
these conditions in the years ahead.                                                members around the world will continue
                                                                                    to focus their energy and attention on
[The Future] So, when we look to Baxter's future, there are several things          making Baxter even better in the future
you can expect. First, we will continue to develop our leadership positions         than it has been in the past. Given our
in the critical therapies we're involved in today. For example, in hemophilia,      great heritage, this is no small task. But
we will continue to advance technologies used to produce clotting factor,           we are up to it. As you read the pages
while increasing our production capacity to meet a tremendous global need.          that follow, I'm sure you will agree that
In renal therapy, we will continue to develop new solutions and technologies        this is a special company, and the best
for peritoneal dialysis while expanding our capabilities in hemodialysis and        is yet to come.
opening renal treatment centers.                                                        As shareholders, this is your company.
    You also will see us move into new areas that build off of or expand our        I am very interested in hearing your
core capabilities. One recent example of this is our growing vaccines busi-         views, comments and questions. Please
ness, where we continue to expand our expertise in recombinant technology.          do not hesitate to leave me an e-mail
Another is anesthesia, the fastest-growing area of our Medication Delivery          at onebaxter@baxter.com.
business, which builds on relationships we have with hospital pharmacists
and others involved in drug delivery. We expect sales in each of these areas
to exceed $1 billion by the end of the decade.
    We also will continue to increase shareholder value by focusing on talent
management, allowing us to attract, retain and develop the best talent              Harry M. Jansen Kraemer, Jr.
in all functional areas; innovation, enabling us to develop new and better          Chairman and Chief Executive Officer
