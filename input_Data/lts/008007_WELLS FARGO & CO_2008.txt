To our owners
In 2008 Wells Fargo earned a profit of $2.7 billion, or 70 cents
per share. Revenue increased 6 percent to $41.90 billion.
Average earning assets rose 17 percent. Average loans rose
16 percent. Average core deposits were up 7 percent. Since
mid-2007, even as credit markets contracted and many of
our competitors retrenched, we were an engine of growth
for the U.S. economy. We made more than a half a trillion
dollars in new loan commitments and mortgage originations
 $540 billion. We achieved the highest total shareholder
return among all our peers  and remained one of the worlds
strongest financial institutions. Wells Fargo Bank, N.A.
has the highest credit rating currently given to U.S. banks
by Moodys Investors Service, Aa1, and Standard & Poors
Ratings Services, AA+. Our Board increased Wells Fargos
dividend by 10 percent  the 21st consecutive year our
Company increased its dividend, exceeding $4.3 billion 
and maintained our dividend in first quarter 2009.
This performance is all the more remarkable because our
talented team achieved it during the sharpest, most volatile,
painful economic downturn our country has experienced in
more than a quarter century. We made some mistakes but kept
our credit discipline.
We took several actions to further strengthen our balance
sheet as we completed the merger of Wells Fargo and Wachovia
 to lay the foundation for the future growth of our combined
company. One was to take a charge against earnings in the
fourth quarter to add $5.6 billion to our credit reserves 
money we set aside to cover future estimated loan losses. This
includes a $3.9 billion provision to conform reserving policies
to the most conservative practices of both companies. This
is in addition to building our reserves earlier in 2008 by $2.5
billion for possible future credit losses. We ended the year with
an allowance for credit losses of $21.7 billion, or $3.20 for every
dollar of our loans that were not accruing interest.*
We further strengthened our balance sheet by taking a
$37.2 billion write-down on $93.9 billion of higher-risk loans
from Wachovia. We also reduced the risk of future charges to
earnings from Wachovias securities portfolio by $9.6 billion.
These write-downs of loans and securities and other actions
related to the Wachovia acquisition reduced our reported Tier
1 capital by about 230 basis points. Tier 1 capital is a core
regulatory measure of a banks financial strength  the ratio
of a banks core equity capital to its total risk-weighted assets.
At year end our Tier 1 ratio was 7.84 percent. Thats well above
regulatory minimums for a well-capitalized bank.
Emerging even stronger
We believe the true test of a financial services company  its
vision, business model, culture and people  is not when times
are good. Its when times are not so good. When the tide goes
out. We emerged from 2008 even stronger, ready for even more
market share growth. Thats because we stayed faithful to our
vision of satisfying all our credit-worthy customers financial
needs and helping them succeed financially  a vision weve
been making steady progress toward for more than 20 years,
through virtually every economic cycle. This vision does not
require any complex mathematical models. All we do is try to
create the best customer experience and then keep count: How
many financial products do our customers have with us, and
how can we earn all their business?
Combining Wells Fargo and Wachovia
Firmly grounded in our vision, we earned even more business
from our more than 40 million customers in 2008. Because
of our financial performance, capital strength, liquidity,
credit discipline and earnings, we also were able to seize an
unprecedented opportunity to satisfy all the financial needs
of at least 30 million more customers. Our merger with
Wachovia Corporation, completed December 31, 2008, has
created the United States premier coast-to-coast community
banking presence, the most extensive distribution system of
any financial services company across North America  11,000
stores, 12,300 ATMs, wellsfargo.com and Wells Fargo Phone
Bank. Our combined company is #1 in mortgage lending,
agricultural lending, small business lending, middle market
commercial banking, asset-based lending, commercial real
estate lending, commercial real estate brokerage and bankowned
insurance brokerage. Were #2 in deposits, mortgage
servicing, and debit card.
We believed this merger was a compelling value the day
we announced the agreement. We like it even more now. The
integration is on schedule and proceeding as planned. In late
2008 loan officers from both Wells Fargo and Wachovia again
carefully reexamined Wachovias entire loan portfolio. Were
comfortable overall with the credit assumptions we made
when we agreed to acquire Wachovia October 3, 2008, without
government assistance. Were confident we can reduce the
combined companys expenses by 10 percent, or $5 billion,
by the end of 2010 and achieve an internal rate of return of
20 percent.
Building our house
We did not, however, merge with Wachovia just to get bigger.
Size alone means nothing to us. You may have noticed:
Nowhere above do I mention how big our combined company
is in assets. Where we rank in asset size alone is meaningless
to us. Thats because we did this merger to get better. In fact,
to our customers, bigness can be a barrier. Ive yet to hear of
a customer walking into one of our banks and saying, I want
to bank here because youre so  big! Safe, secure and stable,
yes. Friendly, hometown and approachable, yes. Smart, savvy,
ethical, yes. But not just because were big. As we like to say:
You dont get better by getting bigger, you get bigger by
getting better.
So, were approaching this merger not as if were building
an empire but as if were building a home  into which we can
welcome our customers so we can have conversations they
find of value about their hopes and dreams and financial goals.
It took only 90 days from definitive agreement to official merger
at the end of 2008. It will take much more time to fully blend
the cultures of our two companies, combine businesses and
products and systems, and name-change where needed.
Just as we did with the very successful Norwest-Wells Fargo
merger a decade ago, were taking our time to do it right. Were
putting our customers first, not the calendar. Were beginning
every discussion with whats best for our customers and our
team members  focusing first not on the bottom line but the
top line. This may sound odd to some people, but we dont
believe our first job is to make a lot of money. Our first job is
to understand our customers financial objectives, their true
financial needs, then off er them products and services to
help satisfy those needs so they can be financially successful.
If we do that right, then all sorts of good things happen for
all our stakeholders, and well be able to produce industryleading
returns for our shareholders. Based on this vision, the
Wells Fargo-Wachovia team is building something together
that we couldnt have built separately. Were building what we
believe can become the premier financial services company not
just in America but in the world. Were building not just for the
next quarter but for generations to come.
Keeping talent
In this merger, as always in our company, people come first.
A major test of our success in this merger is how much talent
we can keep because the talent of our people is our product.
Were a service company, and our team members are the
biggest influence on our customers. Enthusiastic, loyal, satisfied
team members create enthusiastic, loyal, satisfied customers
who want to stay with us for a lifetime. That is why we call them
team members (an asset in which to invest), not employees
(an expense to be managed).
Many people think they have to change companies or careers
every five years to be successful. Were contrarians  we value
loyalty. We believe that the longer talented team members
stay with us, the more they can use their skill, knowledge and
experience to benefit our customers. In a merger this big and
complex (it makes us one of Americas top 10-largest private
employers, with 281,000 team members), some positions have
to be eliminated because theyre duplicated or because they
no longer fit the new structure as we combine businesses and
functions. Inevitably, this causes disruption and uncertainty for
those team members whose positions are eliminated due
to the merger.
Just because a position is eliminated, however, doesnt
necessarily mean the team member in that position has to leave
the company. To keep as much talent as possible, weve paused
in most of our external recruiting and external hiring so we can
help give team members displaced by the merger the chance
to search for other exciting opportunities in our company.
Were a growth company. We have thousands of open jobs in
our company. Were adding them every day  in our banks, our
mortgage company, in investments and insurance and many of
our other businesses.
More alike than diff erent
The past several months Ive had the privilege of meeting
and getting to know many thousands of Wachovia team
members in my visits to Charlotte, Winston-Salem, Columbia
(South Carolina), Richmond, Atlanta, Miami, Jacksonville,
Birmingham, Houston, Dallas, Summit (New Jersey),
Washington, D.C., Philadelphia, Boston, New York City,
St. Louis, Los Angeles and the San Francisco Bay Area. Ive
found them to be just like our Wells Fargo team members
 friendly, caring, sincere, very much committed to their
customers and communities. Weve had diff erent systems,
processes and ways of doing things, but from the customer
point of view I believe were much more alike than were
diff erent. This is one of our great advantages as we integrate
our two companies. Each company began at opposite ends of
the country  Wells Fargo tracing its legacy to 1852, Wachovia
to 1781  but in the last few months weve discovered how much
we have in common and how much we can learn from each
other. We both believe community banking is a noble calling
 the opportunity to help people be financially successful, be
a catalyst for economic growth in our communities and still
earn a fair profit. By instinct and habit we both begin every
discussion with whats best for our customers and communities.
We both want every customer to say, Wow, I didnt know I
could get service like that. She made a diff erence. He solved my
problem and helped me understand how I can achieve my goals.
I learned something new about my financial health.
Were very proud of the communities in which we live and
work, and we want our companies to be known as leaders in
making them even better places to live and work. In our Annual
Report this year, beginning on page 10, we introduce you to
13 team members from Wells Fargo and 13 team members
from Wachovia, each working in the same business or similar
geography to satisfy all our customers financial needs and help
them succeed financially. These great team members are living
proof: Were much more alike than were diff erent.
Our national scope
Our name remains Wells Fargo, our vision and values
remain unchanged, our corporate headquarters remains in
San Francisco, but the talent and resources of our company
are more broadly dispersed across North America than ever
before. Several metro areas across the country have significant
concentrations of businesses, functions and employment
for our company. Des Moines is national headquarters for
Wells Fargo Home Mortgage and global headquarters for
Wells Fargo Financial. Our credit card business has significant
concentrations in Des Moines, Concord (California), Beaverton
(Oregon), and Sioux Falls (South Dakota). Minneapolis-St. Paul
is headquarters for 18 national businesses including Wells Fargo
Insurance (consumer), Institutional Trust, Equipment Finance,
Municipal Finance and SBA Lending. Chicago is our national
headquarters for Wells Fargo Insurance Services (commercial).
Los Angeles is headquarters for 15 national businesses
including U.S. Corporate Banking, Commercial Real Estate
Lending, Community Real Estate Lending, International
Financial Services, Wells Capital Management and Foothill
Capital. Three important centers of employment and expertise
from Wachovia now join this national group of hubquarters
for Wells Fargo. Charlotte, North Carolina  Wachovias former
corporate headquarters  is now Wells Fargos headquarters
for Eastern Community Banking. Its also home to more than
a dozen of our companys senior business heads in Wealth
Management and Retirement Services, Commercial Banking,
Investment Banking, Mortgage, Card Services, and Technology
and Operations. St. Louis is headquarters for Wachovia
Securities, retail broker. Boston is headquarters for Wachovias
Evergreen Investments, asset management.
Our 2008 performance
In addition to the achievements I mentioned at the beginning,
we also built the foundation for future growth of our combined
company with these achievements in 2008:
 Our revenue grew 6 percent, and our expenses declined
one percent  the best such revenue/expense ratio among
our large peers, and the one we consider the best long-term
measure of a companys efficiency.
 Our return on equity (after-tax profit for every shareholder
dollar) was 4.79 cents for every dollar of our shareholders
equity, best among our large peers.
 To help finance the Wachovia acquisition, we issued
422.7 million shares of common stock valued at $12.5 billion
at year-end. Excluding initial public off erings, this was the
largest single issue of common stock in U.S. history. The
enthusiastic response to this off ering in a very difficult
market again showed broad investor confidence in our
long-term growth potential, time-tested vision and
diversified business model.
 Our average Retail Banking household had a record
5.73 products with us (5.5 in 2007; 3.8 in 2001), and one
of every four has eight or more products with us.
 We added 6.2 percent net new checking accounts  a core
product that if our customers have with us makes them
much more inclined to come to us for their next financial
services product.
 More than half our new Business Banking checking
customers bought a package of products from us when they
opened their accounts  such as a business debit card, credit
card, or a business loan or line of credit.
 Our average Wholesale Banking customer had a record
6.4 products per relationship with us; a record 7.8 products
per Commercial Banking customer relationship.
 Average core deposits from our Wealth Management
customers rose $7 billion, up 40 percent; Private Banking
average core deposits rose 38 percent.
 We originated $230 billion in mortgages, and our
owned-mortgage servicing portfolio was $2.1 trillion
(including Wachovia), up 39 percent from last year.
 Our most-used channel, wellsfargo.com, had a blockbuster
year  product solutions sold up 29 percent, total active
online customers surpassed 11 million, up 15 percent, active
consumer Bill Pay customers up 16 percent to two million,
and we introduced the vSafeSM personal online safe for
protecting and storing important documents.
 Besides Wachovia, we acquired the banking operations
of United Bancorporation of Wyoming, Flatiron Credit
Company (insurance premium finance), Farmers State
Bank (Morgan, Colorado), Century Bancshares (Dallas-Fort
Worth; Texarkana, Texas and Arkansas) and EMAR Group
(commercial insurance).
 Our team members as a whole are happier in their work
than ever before. In Community Banking, home to about
one of every three of them, the ratio of engaged to actively
disengaged team members was 8.7 to 1 (2.5 to 1 five years
ago) versus a national average of 1.5 to 1. Our most senior
bankers  market presidents, district managers and
regional presidents  are more engaged than about nine
of every 10 work groups Gallup surveys.
 The $25 billion investment by the U.S. Treasury in
Wells Fargo preferred stock in fourth quarter 2008 gives our
company more resources and greater confidence to make
more loans to credit-worthy customers. Our new lending
in the fourth quarter 2008 alone was almost three times
the government investment. In February 2009 we paid a
quarterly dividend of $371.5 million to the U.S. Treasury
on its Wells Fargo investment.
Lending discipline
Weve said it for years and well say it again because its
never been more important: Our #1 financial goal is to have a
conservative financial structure as measured by asset quality,
capital levels, diversity of revenue sources and dispersing risk
by geography, loan size and industry.
True to that and unlike many of our competitors we actually
built capital and shrunk our balance sheet in 2005 and 2006,
well before credit markets began to contract in 2008. We
understood our customers financial needs. As a result, our
company is one of the worlds strongest financial institutions.
Even as credit markets contracted in 2008, we still led the
industry in lending to credit-worthy customers. We made more
loans at better price spreads while many of our competitors had
to cut back. In 2008 we provided $41 billion in new lending to
businesses and individuals. Our average loans rose 16 percent.
Our average commercial and commercial real estate loans rose
25 percent.
Wells Fargo + Wachovia: growth opportunities
Building on our strength and performance, this merger presents
us with significant growth opportunities in each of our major
business groups. Heres just a sample:
Community Banking: beyond deposits
In addition to our 24 Wells Fargo Community Banking states
in the Midwest, the Southwest, the Rockies and the West,
Wachovia introduces us to 15 more states: Alabama, Connecticut,
Delaware, Florida, Georgia, Kansas, Maryland, Mississippi,
New Jersey, New York, North Carolina, Pennsylvania, South
Carolina, Tennessee, Virginia and Washington, D.C. Our 2008
acquisition of Century Bancshares established our Community
Banking presence in Arkansas. At year-end 2008, our combined
company was #1 in deposit market share in 18 of our 39
Community Banking states and the District of Columbia.
Ninety-three percent of our deposits were in states in which
we rank #1, 2 or 3. We ranked #1 in 10 of the nations 20 largest
metro areas in deposit market share (excluding deposits
$500+ million in a single banking store).
Theres more to financial services, however, than just
deposits. Theres significant potential for more growth in all
these states because banking deposits are an important but
relatively small portion of average household assets. In fact, our
share of total household assets in any of these states is probably
no more than 5 percent. We want to go beyond deposits and
satisfy all the financial needs of Wachovias retail banking and
brokerage households  not just deposits, loans and lines of
credit, but their credit cards, debit cards, insurance, brokerage,
401(k)s, mortgage, home equity, and on and on.
Take a quick inventory of all your own households financial
products. Youll probably come up with about 16, which you
bought from several providers. We believe you can save more
time and money if you have all your products with one trusted
provider that can off er you, and deliver on, a compelling
value proposition. The same goes for our business banking
customers. We want to satisfy all their financial needs  not
just their deposits, loans and lines of credit but their private
banking, mortgage, treasury management, payroll processing,
merchant processing, insurance, and on and on. Were not
satisfied with just being #1 in deposit market share. We want
to be known as the best in every product in every market in
which we do business, in retail banking, business banking,
investments and mortgage  and be the first provider our
customers think of when they need their next financial product.
Both our companies are learning from each other. Wachovia will
help Wells Fargo make the customer experience in our banking
stores even better. Wells Fargo will help further strengthen
the Wachovia sales process. For example, only one of every
10 Wachovia banking customers has a Wachovia credit card;
four of every 10 Wells Fargo customers have a Wells Fargo
credit card.The mortgage and banking opportunity: huge!
With checking, investments and insurance, mortgage is one
of our four core products  one that our customers value so
much that if they have it with us, then theyre more likely to
buy more products from us. This makes it an indispensable
part of satisfying all their financial needs. Weve had a
significant retail mortgage presence in the Eastern U.S. for
a long time; we now have a significant Community Banking
presence there as well. This is one of the biggest cross-sell
opportunities of the Wachovia merger  the opportunity in
15 more states to earn all the banking business of our mortgage
customers and all the mortgage business of Wachovias banking
households. This opportunity alone could generate millions of
dollars of added revenue because we have about three million
Wells Fargo Home Mortgage customers in the 15 states we enter
with Community Banking through the Wachovia merger.
Irrational lenders come and go  mostly they go! We will
stay  as we have for 157 years through virtually every
economic cycle  by lending responsibly, making high-quality
loans to good customers, helping them succeed financially.
Thats why weve avoided most of the recent problems of the
mortgage industry. Its why 93 of every 100 of our mortgage
customers were current with their payments at year-end 2008,
performance consistently better than the industry average.
We still make mortgage loans the old-fashioned way. We do
not underwrite a mortgage loan unless we believe the borrower
can repay it according to its terms. The loan must have a clear
benefit for the customer such as reducing the interest rate or
monthly payment on debt, obtaining significant new money,
paying off delinquent real estate-secured debt, converting an
adjustable rate to a fixed rate, or buying a home. We want to
give customers all the information they need so they know
what theyre buying, what it will cost and that its right for them.
If our customers have trouble making payments, we try to
contact them early and often so we can help them make their
payments on time. Wells Fargo-Wachovia has 8,000 team
members (double more than a year ago) working just to help
these customers keep their homes. In 2008 we helped more than
a half million of our mortgage customers with loan solutions.
We were able to contact 94 of every 100 of our mortgage
customers who were two payments late. We helped more
than half of those 94 avoid foreclosure through refinancing,
reducing payments, agreeing to new repayment plans, short
sales (the proceeds are less than what the owner still owes on
the mortgage), term extensions up to 40 years, reduced interest
rates, no interest charge on part of the principal for some period
of time and, in some markets, reduced principal  and making
sure these modified loans are right for investors.
To help sustain neighborhoods, were selling vacant and
foreclosed properties at significant discounts to new owners,
including first-time homebuyers, by working closely with real
estate agents, housing nonprofits and city officials. Weve made
$33 million in grants to nonprofit housing organizations the last
two years.
The past several years Wachovia off ered a Pick-a-Payment
mortgage loan that allowed customers to choose from among
four payment options including a monthly payment that
may not cover interest charges, perhaps causing the unpaid
principal to increase. Wells Fargo did not off er so-called
negative amortizing loans. Were managing the Pick-a-Payment
portfolio as a separate business unit distinct from the rest of
Wells Fargo Home Mortgage. At the end of 2008, as part of the
accounting for the merger, we charged off $22.2 billion on this
portfolio. In January and February 2009 we gave an extension
to Wachovia mortgage customers being referred to, or in,
foreclosure to give them time to contact us so we could work
with them to help them keep their homes.
Wealth Management: just the beginning
The Wells Fargo-Wachovia merger combines extraordinary
talent and resources in wealth management. We now have
16,000 financial advisors managing $1 trillion in client assets
through bank, private client and independent channels. That
includes our coast-to-coast wealth management organization
with $150 billion in client assets. We provide private banking,
trust and family office services to affluent investors, from those
just beginning to accumulate wealth to those who have many
financial services needs. Were one of the nations top-seven
retirement record keepers for 3.6 million participants. Were
also one of the nations largest retirement administrators and a
leading distributor of IRA and annuity products. Impressive, but
its just a start. We want all our wealth management, brokerage
and retirement services customers to bank with Wells Fargo.
We want all our banking customers to think of us first for all
their wealth management needs. Our goal is to be the most
respected provider of wealth, brokerage and retirement services
in the United States, number one, second to none.
Wholesale Banking: the full relationship
The Wells Fargo-Wachovia merger creates what we believe
is the most comprehensive off ering of products and services
for commercial and corporate customers in the United
States. Nationwide, were now the #1 lender to middle market
companies ($25 million to $500 million in annual revenue),
#1 commercial real estate lender, #1 commercial real estate
broker and the #1 bank-owned insurance broker. Despite
the economic downturn, many of our best corporate and
commercial customers are receiving loans that are wellstructured
with pricing appropriate for the risk. Lending is just
one part of a full-service relationship. Its not uncommon for
many of our commercial customers to have dozens of products
with us covering the full range of lending and operating
services such as Treasury Management, the Commercial
Electronic Office, and Desktop Deposit.
Our Asset Management group now has $500 billion in
assets under management. In the last five years our mutual
funds have grown from the 27th-largest fund complex in 2003
($78 billion) to the 14th-largest in 2008 ($172 billion). Combined
with Wachovias Evergreen Investments, our mutual fund assets
under management had a balance of $250 billion, among the 15
largest in our industry.
We welcome to our Board
With the merger, four members of the Wachovia Board have
joined the Wells Fargo Board, and were benefiting from
their experience and knowledge of Wachovias markets and
customers. We welcome:
 John Baker, president and CEO of Patriot Transportation
Holding, Inc., Jacksonville, Florida, Wachovia Board member
since 2001.
 Don James, chairman and CEO of Vulcan Materials
Company, Birmingham, Alabama, Wachovia Board member
since 2004.
 Mackey McDonald, retired chairman and CEO of
VF Corporation, an apparel manufacturer in Greensboro,
North Carolina, Wachovia Board member since 1997.
 Bob Steel, former president and CEO of Wachovia
Corporation, Wachovia Board member since 2008.
We also thank:
 Dick Kovacevich, who, at the Boards invitation, agreed
to continue as chairman for an interim period to help us
successfully integrate Wachovia into Wells Fargo. His
experience and counsel are needed now more than ever
because he shaped our companys vision and values and
led what is known as the most successful merger of two
large financial services companies in U.S. history  the
Norwest-Wells Fargo merger 10 years ago.
 Phil Quigley, who our Board elected to the new position
of lead director to work with Dick and me to approve
Board meeting agendas, chair meetings of independent
directors, call executive sessions of the Board, help ensure
coordinated coverage of Board responsibilities, and facilitate
communication between the Board and senior management.
 Mike Wright, retired chairman and CEO of
SUPERVALU INC., who will retire from the Board in April
2009 after 18 years of service to our company. Mike joined
our Board in 1991 and has served on the Credit, Human
Resources, and Governance and Nominating Committees.
We thank him for his outstanding contributions to the
governance of our company.
The year ahead: we believe in America
If youre a pessimist, theres a lot for you to like about 2009.
It will be a rough year for our economy and our industry.
Consumer loans will continue under stress with falling home
prices, a nine-month inventory of unsold homes nationally,
higher unemployment, less disposable income and discretionary
spending, more bankruptcies. Credit quality will not improve
until the housing market stabilizes. Charge-off s (uncollectible
debt) probably will continue to rise. Loan losses in consumer
credit businesses including home equity will continue higher
than normal until home values stop falling.
Regardless of what the economy and the markets do in
2009, we focus on our vision and on doing whats right for our
customers. Now, more than ever before in our lifetimes, people
need a safe, trustworthy, capable financial advisor who can
partner with them to help plan and achieve their financial goals
for a home, education, building a business and retirement.
Despite the pain of 2008, we continue to believe in the spirit,
ingenuity, work ethic, creativity and adaptability of American
workers. Were capitalists and proud of it. We believe in free
enterprise. Were Americans first, bankers second. Were
long-term optimists. We believe in the economic potential of
every one of our communities, from Atlanta to Anchorage,
from Galveston to Great Falls. We believe in the United States
of America as a beacon for freedom, liberty and economic
opportunity worldwide. We believe our country can learn from
its mistakes and survive downturns in business cycles better
than any country in the world. Its still the global leader in
technology, manufacturing and services, still the global leader
by far in gross domestic product, still the leader in global
buying power, still the best place in the world in which to invest
and do business.
Our country and our ancestors have faced far more difficult
challenges than those we face today. Just ask your grandparents
or read about their lives. My parents were children of the Great
Depression. They raised 11 children on a family farm in central
Minnesota, worked hard and sacrificed, and taught us what is
really important in life. I see no reason why with discipline and
determination our great country cannot persevere and prosper
through these times as well. Now more than ever before, our
company wants to stand by our customers and do whats right
for them just as Wells Fargo has done for almost 16 decades and
seven generations.
We thank all our 281,000 team members from Wells Fargo
and Wachovia for working together with our customers to earn
more of their business and for collaborating to help ensure
a smooth merger integration. We thank our customers for
entrusting us with more of their business and for returning
to us for their next financial services product. We thank our
communities for allowing us to serve them. And we thank you,
our owners, for your confidence in Wells Fargo as we begin
our 158th year.
Together well go far!

John G. Stumpf, President and Chief Executive Officer