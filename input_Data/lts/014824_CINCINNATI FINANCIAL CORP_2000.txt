TO OUR SHAREHOLDERS:
                                 The year 2000 came in gently. Y2K preparations       environment and have emerged a stronger company
                             succeeded and highly anticipated computer glitches       in many respects. The approach was to forge ahead,
                             had little or no effect on operations of your            doing what it takes to put the problems behind us.
                             Company, our agents and policyholders. By the end           As we begin 2001, we are nimbler, more keenly
                             of six months, net underwriting profits of our           interested in maximizing advantages inherent in our
                             property casualty companies stood at $7.6 million.       unique field structure and underwriting expertise,
                             Increased cash flow from these profits and from          and more committed to the conservative path in
                                                          double-digit premium        accounting and reserves.
                                                          growth contributed to
                                                                                      CHALLENGE: COURT DECISIONS
                                                          excellent growth of
                                                                                         In a 1999 ruling that affected all insurers writing
                                                          investment income.
                                                                                      commercial auto business in Ohio, that state's
                                                             Yet, before the year
                                                                                      Supreme Court granted coverage under an employer's
                                                          was over, sweeping
                                                                                      business auto policy to employees or their family
                                                          changes would occur in
                                                                                      members injured by an uninsured or underinsured
                                                          the larger economic,
                                                                                      motorist while on personal business and even while
                                                          legal and technological
                                                                                      driving a personal car. While we took prompt action
                                                          environments in which
                                                                                      to amend policy language so employers would not be

                                                          we operate. Events and

                                                                                      financially responsible under their policies for such
                             conditions outside of our direct control would test
                                                                                      future losses, plaintiffs' attorneys continue to file
                             our mettle, calling for prompt, bold responses and
                                                                                      claims for past losses. Since late 1999, we have
                             aggressive management of factors within our control.
                                                                                      incurred $40 million in
                             We had a rough year.
                                                                                      losses related to this          Net Operating Income*
                                 These challenges resulted in full-year 2000 net                                      Per Common Share
                                                                                      decision.                       (in dollars)
                             operating income, on a comparable basis excluding a
                                                                                                                                               1.52
                                                                                                                               1.49




                                                                                         Then, in the last
                             one-time charge, of $145.5 million, or 90 cents per
                                                                                                                                       1.16




                                                                                      week of 2000, the same
                                                                                                                       1.11




                             share, versus $255.1 million, or $1.52 per share, last
                                                                                                                                                       .90


                                                                                      court ruled that forms
                             year. Net income, excluding the charge, was
                                                                                      policyholders signed to
                             $143.8 million, or 73 cents per share, versus
                                                                                      reject uninsured and
                             $254.7 million, or $1.52 per share, in 1999. Results
                                                                                      underinsured motorist           96      97      98       99      00
                             were brighter on the balance sheet side, where assets
                                                                                      coverage on their auto          *Adjusted to reflect 5% stock
                                                                                                                      dividends paid in April 1996 and a
                             and shareholders' equity reached all-time highs of                                       3-for-1 stock split paid in May 1998.
                                                                                      and umbrella liability          Excludes 16 cents per share
                                                                                                                      one-time charge
                             $13.287 billion and $5.995 billion, respectively.
                                                                                      policies were insufficient.     Net operating earnings for
                             Book value rose 11.4% to $37.26 compared with                                            2000 include $60.2 million
                                                                                      This meant that                 from parent company
                                                                                                                      investment operations;
                             $33.46 at year-end 1999.
                                                                                                                      $32.3 million from life
                                                                                      uninsured motorist
                                                                                                                      operations; $24.8 million from
                                 While earnings were less than satisfactory, we                                       property casualty operations;
                                                                                      claims previously denied        and $2.7 million from
                             believe we responded effectively to the changed                                          non-insurance subsidiaries.
                                                                                      or not reported now
2
                                                    Back to table of contents
                                                           Commitment to
must be evaluated, even though the policyholder had
not selected or purchased the coverage. Again, we
                                                               
                                                                     
took prompt action to rewrite our forms to meet
extensive new requirements prescribed in the court's       Steadily increasing dividends
                                                              In August 1950, investors had a chance to be part of a new venture, an Ohio fire
decision. Going forward, these revised forms should
                                                           insurance company sponsored by and for local independent agents. The prospectus
relieve us of the obligation to cover new losses in
                                                           for The Cincinnati Insurance Company declared our commitment: "The Company
cases where the policyholder specifically rejected the     shall operate in an extremely conservative
coverage. However, as Ohio's largest commercial auto       manner...every intelligent and sound method of
                                                           transacting business shall be employed...expenses shall
insurer with an 8.4 % market share, we expect
                                                           be kept at a minimum...agents will be offered the
substantial claims due to past losses. These claims
                                                           opportunity to purchase stock...this will enable the
will include losses not yet reported, as well as
                                                           Company to have a better selection of business." Seven
approximately $32 million previously reported, but         of the eight original directors were agents, including

not paid, because the policyholder had rejected in         founders John J. Schiff, Harry M. Turner, Chester T.
                                                           Field and Robert C. Schiff.
writing the option to purchase coverage.
                                                              Directors and officers other than the general manager served without pay for

RESPONSE: POLICYHOLDER                                     three years, putting the Company on a firm financial footing. By 1959, policyholder
SERVICE AND SAFETY                                        surplus exceeded $1 million and, for each of the next 40 consecutive years, the
                                                           Company paid out increasing cash dividends.
   Our first course of action was to protect
policyholders from court-mandated responsibility for
losses they never intended to fund. With remarkable
                                                           short-term pain, it fulfills our obligations to
speed, we placed revised forms in the hands of our
                                                           policyholders, protects our reserve integrity, and
agents, providing guidance and support for delivery
                                                           reduces uncertainty by allowing for a true picture of
to policyholders. While a few insurers have responded
                                                           profitability in future periods.
by abandoning the Ohio commercial auto market,
our commitment is to maintain a market for our
                                                           CHALLENGE: TECHNOLOGY DELAY
agents and their quality accounts even during difficult
                                                              Faced with delays of key deliverables of a large,
times. With revised forms, frontline underwriting
                                                           next-generation software initiative to streamline
and increased monitoring of account quality over the
                                                           policy rating and issuance, management reviewed the
policy term and at renewal, we can be more flexible.
                                                           project and requested an independent assessment.
   Second, in the fourth quarter we added
                                                           We determined that the majority of the investment
$110 million pre-tax (44 cents per share after tax) to
                                                           over the past several years would not be of value as
reserves. This is our best estimate of past losses to be
                                                           the project continued. In the meantime, commercial
reported or paid in or after 2001 as a result of the
                                                           software vendors now were introducing alternatives,
two state Supreme Court decisions. Managing this
                                                           including package solutions compatible with our
liability through an addition to reserves reduced
                                                           corporate technical architecture and further along in
current income instead of letting the losses flow into
                                                           their development.
future quarters. While our approach involves
                                                                                                                                                 3
                                               
                                                                                                normal ranges. Improved performance during the
                               
                               SOLUTIONS                                                        fourth quarter was not sufficient to return to our
                                  First, we recognized the impairment of the                    historic level of profitability, as measured by our
                               software development asset by taking a one-time,                 average combined loss and expense ratio of 101.3%
                               third-quarter net charge of $25.4 million, or 16 cents           over the previous five years. While we're dissatisfied
                               per share. Starting over with a clean slate, we                  to fall short of that mark, we note that our 2000
                               appointed a new project management team to carry                 ratio was in line with A. M. Best's estimate of
                               out this strategic priority.                                     110.3% for the industry. Even including 6.0 points
                                  Second, we renewed our commitment to developing               for the uninsured motorist reserve addition, our
                                                                          software to help      2000 ratio came in at 110.7%, excluding the
                                                                          our agents serve      one-time charge.
                                                                          policyholders with       We analyzed second-half losses, discovering no
                                                                          new efficiency and    geographic or policy age concentration, aside from
                                                                          speed. The project    Ohio uninsured motorist claims. Losses occurred
                                                                          team evaluated        across a broad range of business lines, with a
                                                                          development           concentration in commercial auto and homeowners.
                                                                          options and, in
                                                                                                RESPONSE: UNLOCK INHERENT
                                                                          December, began
                                                                                                
                                                                          plans to customize
                                                                                                   First, improved commercial pricing provided part
                                                                          the selected vendor
                                                                                                of the answer. Our pricing of good renewal and new
                                                                          software for our

                                                                                                business accounts is up 10-15%, with commercial

                                                                          personal lines

                                                                                                auto increases at the high end of that range. Median
                                                    business in three
                                                                                                umbrella liability prices
                                                                                                                               Investment Income
                               pilot states. By year-end 2001, the customized software
                                                                                                are up 18%, and we can         Less Expenses
                                                                                                                               (in millions of dollars)
                               will be tested in the first state. The system features
                                                                                                compete for good                                                415.3
                               direct-bill and agency bill capabilities that will
                                                                                                                                                        386.8
                                                                                                                                                368.0




                                                                                                workers' compensation
                                                                                                                                        348.6
                                                                                                                                327.3




                               complement its online rating and issue functions.
                                                                                                business while
                                                                                                decreasing credits and
                               CHALLENGE: LOSS SEVERITY
                                                                                                moving accounts to
                                  Over the year, insurers reported firmer pricing of
                                                                                                non-dividend paying
                               commercial lines. This is good news, but not good
                                                                                                                                96      97      98      99      00
                                                                                                plans. We have instituted
                               enough to offset many years of inadequate pricing                                               Pre-tax investment income
                                                                                                                               for 2000 rose 7.4% to an
                                                                                                more conservative
                               accompanied by rising loss costs. Higher building                                               all-time high. Investment
                                                                                                                               income is the primary source
                                                                                                underwriting for workers'
                               costs, vehicle repair costs, medical costs and court-                                           of the Company's profits.
                                                                                                                               Dividends from equity
                                                                                                compensation, commer-          holdings contributed
                               directed verdicts now prevail.
                                                                                                                               $186.2 million and interest
                                                                                                cial auto and other            from fixed-rate holdings
                                  During the third quarter, the Company's property                                             contributed $222.0 million,
                                                                                                classes of risk.               pre-tax.
                               casualty losses moved above or to the high end of our
4
                                                           
                                                          Commitment to
   Second, we have intensified efforts to carefully
underwrite each account and to tap the wealth of
                                                             
                                                                   
local knowledge created by our uniquely strong local
field presence. Our no-branch-office structure gives      Choosing carefully, committing fully
                                                             When 60 agents attended the first Florida Sales Conference in 1960, they had
agencies a local team of marketing, claims, engineering
                                                          produced more than $200,000 of premium in the first full year of operation in the
and loss control representatives who work out of
                                                          state. Sales meetings on the agents' home turf already were a Cincinnati tradition,
their homes, spending most of their time actually in
                                                          an opportunity for top officers to
agencies and on their clients' premises. They are         cultivate personal relationships
ideally positioned to take the lead in managing           and loyalties with the
                                                          cream-of-the-crop agents selected
factors we can control. The field representative
                                                          to represent the Company.
profitability initiatives, from increased on-site
                                                             While other insurers appointed
inspections to team-centered renewal discussions,
                                                          large numbers of agents,
are outlined on Page 11.                                  Cincinnati hand-picked a few good agencies and did what was necessary to earn a

                                                          larger portion of their business, thereby reducing Company and agency expense.
CONTINUING OUR TRADITION
                                                          The 1962 Annual Report explained: "Your Company now has 373 agencies. We do
OF COMMITMENT
                                                          not consider numbers alone to be of great importance...but rather the quality and
   The year behind us is a testament to the commitment    desirable volume potential."
your Company has placed on value. It is a legacy
built by people such as Director Emeritus David R.
Huhn, who passed away in February 2000. The former        2000. As our Claims Department manager and a key
president of The McAlpin Company and chief                executive, Jim's perspective will help our Board keep
executive of Mercantile Stores, Inc., had served on       the highest standards of customer service  to our
the Cincinnati Financial Board from 1990 through          agents, policyholders and claimants  in the
1996. It is a legacy built by people like Jackson H.      foreground of every decision.
Randolph, retired chairman of CINergy                       Your Company's outlook is good as price
Corporation, who retired from our Board on                competition abates and agents have more
November 17, 2000, after serving since 1986.              opportunity to sell policies based on service and
   It is a legacy that will be guarded and preserved      value. The commitments we make, and have made
                                                          for 50 years, differentiate your Company, positioning
by people like Senior Vice Presidents Kenneth W.
                                                          it to continue building the strength that creates
Stecher and James E. Benoski. Ken was appointed
                                                          shareholder value, even in an atypical year like 2000.
chief financial officer of Cincinnati Financial on
February 3, 2001. His professionalism and
management skills in leading our accounting and
                                                             /s/ John J. Schiff, Jr.
financial reporting areas make him an integral part of      John J. Schiff, Jr., CPCU
                                                            Chairman, President and Chief Executive Officer
the executive team. Jim, who is vice chairman and
                                                            February 6, 2001
chief insurance officer of The Cincinnati Insurance
Companies, joined our Board on November 17,
                                                