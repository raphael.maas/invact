My fellow stockholders,
2018 was an important year as we took significant steps
in strengthening our leadership in our core business and
advancing our pipeline.
However, before I comment on our 2018
performance, I would like to acknowledge
the discontinuation of the Phase 3
ENGAGE and EMERGE studies of aducanumab in
Alzheimer's disease (AD)
that we announced in
March 2019.
This disappointing news
reaffirmed the complexity
of treating AD. We are
grateful to the patients,
their families and investigators who participated
in the studies. We know
that the sobering reality
of drug discovery is that
many studies will fail before
one succeeds. We are committed to
learning from these clinical studies and
furthering the scientific understanding of this
terrible disease.
We believe our foundation and future remain strong.
Neurological diseases are the #1 cause of disability and
the #2 cause of death worldwide1. The prevalence and
societal burden of these diseases are massive and are
Building for sustainable leadership
CEO LETTER
increasing as the aging population continues to grow.
Few other areas of medicine hold as much promise for
scientific breakthroughs.
In 2018 we maintained our market leadership in our core
franchise of multiple sclerosis (MS) and made progress toward building a neuromuscular disease
franchise with the expansion of SPINRAZA,
the first approved treatment for
spinal muscular atrophy (SMA),
a rare neurological disease.
Our view is that neurological diseases are deeply
connected. Because the
pathways of these diseases are interrelated, we
believe the potential approaches for treating them
are as well. Our success
in MS gives us insight into
many other disease areas.
For example, research in
remyelination and repair,
neuroprotection and axonal
health could have applications in
AD, amyotrophic lateral sclerosis (ALS),
Parkinson�s disease, stroke and pain.
We are working to build a multi-franchise therapeutic
portfolio and to create new sources of value by
diversifying our pipeline. In 2018 we made progress in
movement disorders such as Parkinson�s disease and in
neuromuscular disorders such as ALS.

2018 core business performance
In 2018 we generated an all-time
high of $13.5 billion in total revenues
for the year, a 10% increase over
the prior year, and we generated net
cash flows from operations of $6.2 billion. GAAP diluted earnings per share for 2018
were $21.58, an increase of 81% over 2017, and
Non-GAAP diluted earnings per share increased 20%
over the prior year to $26.20.
These results reflect the resilience of our core MS business, the continued strong global launch of SPINRAZA
and the ongoing growth of our biosimilar business.
With approximately 35% of MS patients treated with
our medicines globally, in 2018 we continued to be
the leader in MS, and we remain firmly committed
to the MS community. Our MS portfolio ranges from
symptomatic treatments to disease modifying therapies,
which enables us to address diverse patient needs
across disease stages, and we have continued to push
forward. In December Alkermes, with whom we have
a license and collaboration agreement, submitted a
New Drug Application (NDA) to the U.S. Food and Drug
Administration (FDA) for diroximel fumarate (BIIB098), a
potential treatment for relapsing forms of MS (RMS). If
approved, we will be responsible for marketing diroximel
fumarate, which we plan to do under the brand name
VUMERITY, a name conditionally accepted by the FDA.
In SMA, SPINRAZA revenues for 2018 nearly doubled
year over year with total global revenues of $1.7 billion,
which was driven by strong revenue growth in both the
U.S. and international markets. Since
its launch two years ago, SPINRAZA,
our first product based on the antisense oligonucleotide (ASO) platform,
has become the standard of care in
SMA. By the end of 2018 SPINRAZA had
been approved in over 40 countries and had
received formal reimbursement in 30 countries.
Including clinical trials and our Expanded Access
Program, as of year-end, more than 6,600 patients
have benefited from this remarkable therapy.
Additionally, in November, SPINRAZA won the prestigious
International Prix Galien for Best Biotechnology
Product � its seventh Prix Galien, following six individual
country awards.
The efficacy and safety profile of SPINRAZA are evidenced by our long-term data and mounting real-world
evidence. This includes the unprecedented efficacy data
in pre-symptomatic infants as demonstrated by the new
interim results from the NURTURE clinical study evaluating efficacy and safety that we announced in October.
Our 2018 revenues also grew as a result of the
continued expansion of our biosimilar business. Our
biosimilars revenues increased 44% over the prior
year. This growth was primarily driven by the success
of BENEPALI (an etanercept biosimilar referencing
ENBREL), as well as the continued growth of FLIXABI
(an infliximab biosimilar referencing REMICADE) and the
October launch of IMRALDI (an adalimumab biosimilar
referencing HUMIRA) in several European markets.
We believe biosimilar products benefit patients and 

are strategically important as we work with payers and
health systems globally with the goal of creating room in
healthcare budgets for innovative therapies. Based on
our internal estimates, BENEPALI has created savings of
up to �800 million annually across Europe.
The launch of SPINRAZA and the growth of our biosimilar
business have led to meaningful revenue growth outside
of the U.S. and the expansion of our global footprint.
At the end of 2018 ex-U.S. product revenues were 37%
of our product revenues, up from 32% of our product
revenues in 2017.
Overall, our business and cash generation remained
strong and continued to provide us with flexibility to allocate capital. In 2018 we spent a total of approximately
$1.8 billion to develop and expand our pipeline as we
signed six business development deals and increased
our ownership in Samsung Bioepis Co., Ltd.
(Samsung Bioepis), our joint venture with
Samsung BioLogics Co. Ltd. (Samsung
BioLogics). We also made share
repurchases totaling approximately
$4.4 billion. We plan to continue
evaluating new opportunities for
business development as we aim to
create long-term stockholder value
and in March 2019 we announced a
new program for the repurchase of up
to $5 billion of our common stock.
2018 pipeline progress
2018 was also a productive year with great strides
in innovation and achievements of clinical milestones.
We continued to build momentum by adding depth in
our core growth areas (MS and neuroimmunology, AD
and dementia, movement disorders and neuromuscular
disorders) and by advancing our emerging growth areas
(acute neurology, neurocognitive disorders and pain).
We expanded and advanced our pipeline by adding six
new clinical programs and completed enrollment of
late-stage studies, including the PASSPORT study for
progressive supranuclear palsy (PSP) and the AFFINITY
study for MS.
We believe our expansion into movement disorders
holds great promise. We initiated the Phase 2 SPARK
study of BIIB054 (anti-a-synuclein) in Parkinson�s
disease and completed enrollment of our Phase 2
PASSPORT study of BIIB092 (gosuranemab) in PSP, a
disease leading to loss of balance, slowing of movement,
difficulty moving the eyes and dementia.
We also made progress in neuromuscular disorders by
acquiring BIIB110 (ActRIIA/B), a muscle enhancement
program for investigation in ALS and SMA, and advancing programs based on the ASO platform. We believe
ASOs hold great promise to directly target the genetic
and pathological origin of disease. We initiated a
Phase 1 study of BIIB078 (IONIS-C9Rx), an
ASO drug candidate, for ALS targeting
C9ORF72, which is the most commonly inherited cause of ALS.
In the fourth quarter of 2018 we
and our collaboration partner
Ionis Pharmaceuticals, Inc. (Ionis)
announced results from a positive interim analysis of the ongoing Phase 1
study of BIIB067 (tofersen)
in ALS with superoxide dismutase
1 (SOD1) mutations, a genetic form of
ALS representing approximately 2% of all ALS
Our goal is to deliver
innovative therapies for
patients and invest in
the areas of highest return
to deliver long-term value
to our stockholders.
patients. This interim analysis demonstrated both
proof-of-biology and proof-of-concept with a concordance
across multiple clinical and biomarker endpoints. Based
on these results, we initiated and dosed the first patient in
the anticipated Phase 3 VALOR study, a continuation of
the Phase 1/2 single- and multiple-ascending dose study.
We remain committed to research in AD and to supporting the millions of patients affected by this devastating 

disease. We will continue to advance our tau-related
programs, including BIIB092 (gosuranemab) and
BIIB080 (IONIS-MAPTRx), and plan to refocus on early
research to understand the biology and genetic origins of
disease for our remaining AD portfolio.
In our emerging growth areas, we initiated four new
studies, including the Phase 3 CHARM study for BIIB093
(glibenclamide IV) in large hemispheric infarction (LHI),
a severe form of ischemic stroke. We also expanded our
pipeline with the addition of BIIB104 (AMPA) for cognitive
impairment associated with schizophrenia (CIAS) and our
option for TMS-007 for acute ischemic stroke.
Looking ahead
Our solid financial performance in 2018 underpinned
our continued investment in research and development
and global business expansion. We will continue to be
financially disciplined and focused on implementing a
lean and simple operating model. We are committed to
continuous improvement across all of our operations,
something that demands a thoughtful approach toward
all our investments over both the short and long term.
We aim to continue our momentum in 2019. To deliver
on our aspirations, we will remain focused on executing
on our strategic priorities in our core business of MS and
SMA and developing and expanding our portfolio. Across
the portfolio, we are prioritizing resources based on
what we believe are the most promising programs, and
we plan to continue to significantly invest in research
and development.
This is a remarkable time for Biogen and all our stakeholders. We believe we are well-positioned to address
the significant unmet medical need in neuroscience, as
well as in adjacent therapeutic areas where we currently
have assets or expertise. Our goal is to deliver innovative
therapies for patients and invest in the areas of highest
return to deliver long-term value to our stockholders.
I would like to thank all of our employees for their continuing dedication to the business and their unwavering
commitment to the patients we serve. Pioneering
means learning along the way and altering the course
when it becomes evident that there is a better path.
We are on an exciting journey, fueled by our passion
for innovation and groundbreaking science. We care
deeply about making a difference. We work fearlessly.
We do not give up even when challenged, pursuing
innovation in all that we do. We are humbled by the
opportunity to change lives.
Michel Vounatsos
Chief Executive Officer