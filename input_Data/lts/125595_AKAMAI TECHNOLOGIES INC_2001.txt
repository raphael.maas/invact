In the rapidly changing world of information technology, Akamai remains dedicated to the revolutionary idea upon
which the company was founded: to pioneer and be the leader in the global distribution, management, processing,
storage, and delivery of information and applications over Internet Protocol (IP) networks, often referred to as
the content delivery market. Akamais unique technology platform enables businesses and government to process
and deliver information and applications over the public Internet and corporate extranets and intranets in a manner
that is more efficient, reliable, and scalable than the computing infrastructure solutions they used before.

This past year has been one of challenge and continued improvement
for Akamaifrom overcoming the personal loss of our co-founder
and working through the deleterious effects of the bursting of the
so-called Internet bubble to strengthening our industry-leading
technology and core competencies to create ever greater value for
our customers.
During the year we significantly cut spending, including
implementing workforce reductions to better align our resources
with economic realities. All the while we made substantial progress
toward achieving the significant opportunities we see for our
services and products in traditional business enterprises and
governmental agencies.
And despite the difficult economic environmentespecially in the
high-tech industrywe made steady improvement in pursuit of our
goal to reach positive free cash flow before year-end 2003, while
preserving our fully funded business plan and strengthening our
competitive position.
Now in our third year as a public company, Akamai has extended
its leadership in content and application delivery with the highly
successful launch of our EdgeSuiteSM offering, a robust new platform
that enables organizations to more effectively reach and interact
with their customers, employees, and suppliers while improving
the return on investment in their Web-based initiatives.
Our EdgeSuite service is a revolutionary next step in the development
of information technology because it allows customers to dynamically
create and deliver a complete Web site from the edge of the
network, close to their end-users, thereby improving performance,
reliability, scalability, and security. EdgeSuite builds upon our initial
content delivery capabilities by adding customer-valued benefits
such as dynamic content assembly, denial-of-service mitigation,
network-attached distributed storage, business intelligence services,
global traffic management, integrated marketing support, and
secure transaction processing. We expect EdgeSuite to contribute
significantly to our top and bottom line performance in 2002
and beyond.

The entire Akamai family mourns the loss of our co-founder and Chief Technology Officer, Danny Lewin,
who was a victim of the terrible events of September 11th. Not only was Danny a valuable member of
the executive team, he helped found Akamai in September 1998, along with Tom Leighton and a group
of Massachusetts Institute of Technology scientists and business professionals. As our Chief Technology
Officer, he was responsible for Akamais fundamental strategy of creating innovative Internet
infrastructure services that would produce an entirely new industry segment and forever change the
way people and companies distribute content, data, and applications worldwide. Dannys spirit and
enthusiasm are deeply and profoundly missed by all who knew him.

2001: Meeting Our Growth Objectives
Our revenue grew to nearly $165 million in 2001 from $90 million
in 2000, an 80% increase, despite the significant downturn in
overall technology spending during the year. Important to our
growth was rapid market acceptance of our EdgeSuite service,
which went from a standing start to more than 20% of our
revenue in the fourth quarter.
In addition to growing the top line, Akamai is strongly
committed to driving bottom-line profitability. Over the past year,
we made a concerted effort to reduce our operating costs and cut
operating losses by close to 70%. Likewise, we dramatically lowered
our capital expenditures by 80%, without sacrificing underlying
network growth. These significant reductions in operating and
capital expenses and our strong cash position at the end of 2001
support our confidence that our business plan is fully funded as
we continue to execute on our growth objectives.
We ended the year with more than 1,000 customers under
recurring revenue contracts of one or more years, including
152 EdgeSuite customers, the majority of which are corporate
enterprises. During 2001, we realigned our direct sales force to
better penetrate enterprise and government opportunities and
added several seasoned executives and a number of sales
professionals with extensive enterprise experience to lead the way.
We achieved higher revenues per customer while simultaneously
reducing customer churn. We also strengthened relationships within
our ecosystem of more than 100 resellers, systems integrators,
and technology partners who utilize Akamai as a key part of their
enterprise go-to-market plans. In 2001, resellers accounted for
20% of Akamais revenues.
Customers
Our customers are primarily leading enterprises, government
agencies, and other businesses from among the top 250 Web
services providersall of which fundamentally depend on the
Internet to help them meet mission-critical business objectives.
Using Akamai services and products, our customers expect to gain
a competitive advantage by providing a rich online experience for
their end-users without having to manage and deploy a complex
and costly infrastructure. We have built a blue-chip customer list
that includes such enterprises as Amtrak, BestBuy.com, FedEx,
Whirlpool, and Xerox; key government agencies including the
Centers for Disease Control and Prevention, the FBI, and U.S. Army
Recruitment; top Web-centric businesses such as Monster.com,
Terra Lycos, Ticketmaster Online-Citysearch, and Yahoo!; and
leading technology companies, including Apple Computer,
Cognos, IBM, McAfee.com, and Microsoft.
To build upon the value we brought to customers in 2001 with
our EdgeSuite platform, this year we intend to extend our offerings
behind the corporate firewall, into the enterprise-computing
infrastructure. The idea is a uniform enterprise-wide platform
that leverages Akamais proprietary technology to better connect
businesses with their constituencies wherever they may be, within
the enterprise and around the world, using both public and private
IP networks. We expect to update you on this exciting initiative
later in the year.

Network
2001 also saw a dramatic increase in the global deployment of
the Akamai network, which grew to more than 13,500 servers in
over 1,000 networks in 66 countries from over 8,000 servers in
460 networks in 55 countries at the end of 2000. The massive reach
of our network, which we built by utilizing low-cost hardware and
partnering with telecommunication providers instead of building our
own capital-intensive facilities, is a crucial reason our customers want
to work with us in distributing their business content and applications.
Akamai continued to significantly strengthen these network
relationships over the course of 2001, adding over 500 new network
partners. Our global scale and strong network ties help us dramatically
lower our operating costs, generating savings that we are able to pass
along to our customers. Our more than 1,000 network partners are
leading Tier 1 carriers, cable and other broadband providers, ISPs,
and wireless carriers, who are key to our competitive differentiation.
Technology
At our core, Akamai is a leading technology development company
comprised of the best engineers who add significant features and
functionality to our offerings on a continuous basis. Last year was
no exception with the introduction of EdgeSuite.
Akamai also worked closely with other leading technology providers
to develop and standardize innovative technologies such as Edge
Side Includes (ESI), which facilitates dynamic assembly of enterprise
information and applications at the edge of the network.
In addition to our enterprise computing infrastructure initiative
I mentioned above, Akamai also plans to introduce new offerings
that distribute application logic processing to the edge of the
network to further improve application performance and scalability.
Our strategy is to continue building upon the technologies, and
leveraging the core competencies, that make Akamais unique
distributed computing capabilities ever more valuable to enterprise
and government customers.
Employees
Akamai continues to attract and employ some of the best and the
brightest minds in technology, and we work to create an environment
that rewards innovation. At the end of 2001, we had 841 employees,
of whom 206 had advanced degrees, including 57 Ph.D.s. Our
employees are highly committed to making Akamai a success and
an exciting place to work. The strength and commitment of our
employee base is what differentiates Akamai as a leader in distributed
computing, and I want to thank every employee for their valuable
contribution to our success.
As a shareholder and employee, Im proud of Akamais commitment
to providing unique and innovative technology to best support
enterprise computing needs, extending the reach and scale of our
global network, sustaining our dedication to customer service,
and employing the most talented people.
Thank you for your continued support of our employees and
management as we strive to achieve our goals.