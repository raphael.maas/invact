In 2009, Schlumberger revenue fell by 16% to $22.7 billion as world economic conditions worsened and customer
spending dropped with lower commodity prices. As the year progressed, however, oil prices recovered gradually from
levels under $40 per barrel in January to over $70 per barrel in December.
Within this market, Schlumberger Oilfield Services revenue in 2009 also declined 16% compared to 2008, falling to
$20.52 billion. Lower natural gas prices and unfavorable market fundamentals led to a 37% decline in North America
revenue, primarily in the US Land and Canada GeoMarket* regions. Europe/CIS/Africa Area revenue fell 13% mainly
owing to the weakening of local currencies against the US dollar and reduced activity in the Russia, North Sea, West &
South Africa, and Caspian GeoMarkets, as well as at Framo Engineering. Middle East & Asia Area revenue dropped
9% primarily as a result of decreases in the East Asia, East Mediterranean, Arabian and Australia/Papua New Guinea
GeoMarkets. Latin America Area revenue, however, was only marginally lower than in 2008 as the impact of weaker
local currencies against the US dollar and much lower activity in the Venezuela/Trinidad & Tobago and Peru/Colombia/
Ecuador GeoMarkets was partly offset by stronger activity in the Mexico/Central America and Brazil GeoMarkets.
WesternGeco revenue in 2009, at $2.12 billion, was 25% lower than 2008. Revenue fell across all product lines,
with the largest declines seen in Marine and Multiclient. Marine revenue fell on lower activity and reduced pricing
as a result of weaker market conditions while Multiclient revenue decreased as customers reduced discretionary
spendingprimarily in North America. Land revenue fell on lower crew utilization and Data Processing revenue
decreased reflecting lower activity primarily in Europe and Africa and in North America.
While customers spent less on new technology in 2009 versus the previous year, a number of recently introduced
Schlumberger services made further market progress. PowerDrive* family rotary-steerable drilling systems extended
their reach, entering new markets in North and Latin America. Scanner Family* services for advanced formation
evaluation saw success in Mexico and Canada, while Well Services recorded market penetration in Saudi Arabia,
Abu Dhabi, and Malaysia with the range of ACTive* real-time coiled tubing services that improve operating efficiency
and well performance through the innovative use of fiber-optic cable to convey downhole information to the surface.
New seismic technology also made progress with WesternGeco announcing the deployment of the UniQ* integrated
point-receiver land seismic acquisition and processing system in Kuwait for the Kuwait Oil Company. The new system,
designed for greater operational efficiency and flexibility, offers up to four times the capability of existing Q-Land*
systems and has already acquired data from 53,000 live point-receiver channels in conjunction with WesternGeco
DX-80* land seismic vibrator trucks equipped with MD Sweep* low-frequency technology introduced the previous year.
Our investment in infrastructure to support future growth continued throughout the year. Significant events included
the opening of a new reservoir completions manufacturing center in Dammam, Saudi Arabia. Representing an investment
of $25 million, the center houses design and manufacturing engineers specializing in the production of downhole
reservoir completions equipment. The center also provides a collaborative environment in which joint oil
company and Schlumberger teams can develop completions solutions for application across Saudi Arabia and the
Middle East. In Brazil, we announced the signing of a joint cooperation agreement with the Universidade Federal
do Rio de Janeiro to build a key international research center on the university campus. This center will focus on
research and development activities in complex deepwater environments.
We also completed a number of technology agreements and acquisitions to enable product development or expand
geographical reach. In May, Schlumberger acquired Techsia SA, a supplier of petrophysical software based in
Montpellier, France. Techsia will become the Schlumberger Petrophysics Software Center of Excellence for the
development of state-of-the-art solutions for the oil and gas exploration and production industry. In September, we
announced the creation of a joint venture with National Oilwell Varco to provide high-speed drillstring telemetry
systems to improve the efficiency and safety of oil and gas operations, and in November, we acquired Lonkar
Servicesa company providing slickline, cased-hole wireline and surface well testing services in Western Canada
and Northern USA. In November we also signed a joint cooperation agreement with Technip to develop subsea
integrity and surveillance solutions for the flexible pipe used in deep offshore oil and gas production. This agreement
will initially focus on surveillance systems activities for new and challenging flexible pipe applications such as those
required in deepwater operations.
Last year I commented with disappointment on the poor safety results of 2008. This year, I am pleased to note some
improvement, particularly in our own driving performance that resulted in no fatalities among our employees for the
first time since 2005. We regretfully still recorded fatalities in accidents suffered by our contractors, and here we
must further strengthen our efforts to raise awareness and enforce the procedures that will ultimately save lives.
Our improved driving safety performance was not, however, mirrored in our operations, and 2009 saw the worst
year for work process fatalities since 2000. We must not relax our efforts in any area of safety and I expect all of the
management team to make safety their highest priority in the coming year.
The outlook for 2010 remains largely dependent on the prospects for the general economy. At the end of the third
quarter of 2009, we indicated that we were encouraged that signs were emerging that demand for oil and gas would
begin to increase. Consensus forecasts now predict that oil demand in 2010 will increase, particularly in the developing
world, for the first time since 2007.
As a result, we feel that oil prices are likely to be sustained at current levels and that as our customers confidence
grows, their exploration and production budgets will increase. We believe that considerable leverage exists to
increase investment in offshore markets, in Russia, as well as in certain emerging investment opportunities such
as Iraq but that this will be dependent on continued economic growth in the second half of the year beyond current
government stimulus packages.
For natural gas activity we remain a great deal more cautious. Despite signs of some recovery in industrial demand as
well as colder winter weather, we consider that markets remain generally oversupplied. Increased LNG flows together
with further capacity being added in 2010, as well as the general uncertainty over the decline rates of unconventional
gas production, have the potential to limit the current increase in the North American gas drilling rig count.
In conclusion, I would like to thank our customers for their confidence and support as well as our employees for the
dedication and commitment that they have shown in a difficult year. Longer term, we remain confident that considerably
increased spending will be necessary to maintain sufficient reserves and production of hydrocarbons to meet the
worlds needs. Our technology portfolio and worldwide infrastructure mean we are strongly positioned to capture
growth opportunities as our customers begin to increase their investment.

Andrew Gould
Chairman and Chief Executive Officer