To Our Stakeholders: I am proud to lead the new
Tyco, and honored to introduce this pure-play fire
and security company. In my nearly seven years
with Tyco, I have had the opportunity to lead a
number of the businesses that now form our new
company. Ive also had the distinct privilege to work
closely with, and benefit from the experience of,
our former CEO Ed Breen. Ed, who remains as
Chairman of our Board of Directors, built Tyco over
the past decade into an industry leader through
strong financial leadership, a firm commitment to
customer and shareholder value, and an unwavering
focus on integrity and transparency.
It is upon that solid foundation that we now begin a new chapter in Tycos
history. Our company is well positioned for growth, with over 70,000
employees around the world who have an unmatched passion and
commitment to safety and security. Our ability to focus our attention and
resources solely on what we do best provides us with an opportunity
to enhance our market-leading position in the fire and security industry.
// We Delivered Strong Performance in 2012
As I look back on 2012, I am pleased to report that it was an exciting
and memorable year for Tyco. Our key achievements over the past
year are highlighted by the successful separation of Tyco International
into three independent public companies that are all leaders in their
respective industries. The newly formed companies are Flow Control,
which simultaneously merged with Pentair, Inc; ADT, the North
America residential security business; and the new Tyco, the largest
pure-play fire and security provider in the world.
Even as we completed one of the more complex separations of
a public company in recent history, we delivered strong operating
performance in 2012:
 organic revenue grew 2%, including 3% growth in service revenue
and 10% growth in product revenue
 segment operating margin before special items improved 70 basis
points to 12.7%
 income from continuing operations before special items increased
13% to $635 million
 earnings per share before special items increased 15% to $1.35
 free cash flow generation continued to be strong, enabling us
to fund our organic growth investments, including a 12% increase
in research and development (R&D). In addition, we completed
seven strategic bolt-on acquisitions and returned nearly $1 billion
to shareholders through share repurchases and dividend payments.
// An Exciting Time for the New Tyco
There is a lot of energy and excitement in the new Tyco with the
opportunities we have as the leader in a fragmented $100 billion
fire and security industry. Being a pure-play operating company
means we can focus our efforts more closely on this industry
than ever before.
As an operating company it is important that our employees and
our businesses are aligned towards common goals and objectives
centered on the needs of our customers. To that end, we are currently
developing a Tyco Operating System that leverages the combined
global capabilities of our fire and security businesses.
The breadth and depth of our company is significant, as we offer
comprehensive fire and security solutions globally. Our capabilities
include fire detection and suppression, intrusion prevention, video
surveillance, access control, retail security solutions and personal
breathing apparatus, as well as monitoring, maintenance and service.
We have a broad portfolio of innovative products and services that
allow us to partner with customers to help them protect people and
assets everywhere they do business.
Every day, we fulfill our promise to our customers to design,
manufacture, install and service fire protection and security solutions,
and we remain focused on finding smarter ways to save lives,
improve businesses and protect where people live and work.
// Executing on our Three-Year Growth Strategy
The new Tyco remains dedicated to meeting its commitments to
shareholders, with a clear and concise strategy for increased growth
and improved operating margins over the next three years. The
strategy features three main priorities:
1. ACCELERATE ORGANIC GROWTH A key to accelerating our organic
growth is expanding our technology leadership through R&D
investments in our global products businesses. Our total annual R&Drelated
spend has grown by over 45% since 2009 to $210 million,
and we plan to continue with double-digit annual increases going
forward. The increased R&D spend will go towards the development
of new products and platforms, targeted solutions for the high-growth
markets, and differentiated technology to enhance our services and
deliver total lifecycle cost reduction.

Over the past year we have introduced several new products that
have contributed to our organic growth. In the North America
fire services market we unveiled a new lighter and more compact
high-pressure Air-Pak for firefighters. We also introduced a new
interactive intrusion solution that leveraged the technology from
our recent Visonic acquisition. In addition, we developed and
introduced an automated fire detection and fire suppression solution
for unmanned oil platforms, integrating our infrared imaging flame
detection with an extinguishing monitor.
Our installation and services businesses are focused on
accelerating service growth. Services today represent about 45%
of our revenues. In 2012, services grew 3% and we expect to reach
5% over the next three years. We will achieve that target by focusing
on the right installation projects that lead to services, leveraging the
combined customer base of security and fire to bring expanded service
offerings, differentiating our offerings through innovation and improving
our customer service via technology and process enhancements.
Today, about 12% of our revenues are generated in our highgrowth
markets where we have continued to deliver strong doubledigit
growth. We plan to accelerate investment in these high-growth
markets to assure that we have the right products, as well as the right
design, manufacturing and services footprint to be able to take full
advantage of the potential in these markets.
We are also developing differentiated vertical market solutions
that leverage our combined capabilities and will enable us to increase
our market share in key verticals.
2. EXECUTE DISCIPLINED BOLT-ON ACQUISITIONS We expect our
disciplined acquisition strategy to provide a key growth platform over
the next few years. Our acquisitions have been, and will continue to
be, aligned with our organic growth initiatives focused on technology,
product expansion and services. In 2012, we added nearly $300
million of revenue on an annualized basis from acquisitions, with solid
returns on invested capital. These acquisitions offer a good example
of the type and size that we intend to pursue moving forward.
3. DRIVE OPERATIONAL IMPROVEMENTS Combining our commercial
security businesses with our fire protection businesses provides
a significant opportunity for us to synergize our field offices and
business processes and to capitalize on our scale to deliver significant
cost savings. We currently have productivity initiatives underway
across the company that will support the increased investments
required to accelerate growth, while delivering approximately 300 basis
points of margin expansion over the next three years. Our strategic
sourcing, cost rationalization and branch-in-a-box initiatives provide
us with significant cost-saving opportunities that are under our control
and independent of the global economic environment.
The three priorities that form the foundation for our growth strategy
are enabled by a highly experienced leadership team which has
defined a company culture focused on alignment and collaboration 
and built on our core values of integrity, excellence, accountability and
teamwork. Within the new Tyco, we measure our success based on
our ability to act in a boundaryless, collaborative fashion to provide the
best fire and security solutions for our customers and drive continued
growth for our shareholders.
We expect these initiatives to accelerate our organic revenue
growth rate and expand our segment operating margin to 15% to
16% in 2015 while generating the strong cash flow necessary to fund
continued growth investment.
// Building a Bright Future for Tyco
Coming off a strong performance in 2012, the new Tyco is well
positioned to accelerate growth over the next three years. The
passion of our people, our innovative products and services, and
our loyal customers combine to give us an opportunity for increased
market share in a large, global and fragmented industry. As we look
ahead, I am confident that we will continue to grow our leadership in
the fire and security industry.
I would like to acknowledge the valuable contributions from our
people around the world. We are fortunate to have the most
dedicated and passionate work force in the industry, led by a
seasoned management team supported by an engaged and highly
experienced Board of Directors.
The new Tyco builds on a strong and proud legacy of growth,
innovation and integrity. We are excited and confident about our future
and the opportunities we have to leverage our market-leading position
and continue to create shareholder value. To all Tyco shareholders, I
offer my sincere thanks for your continued confidence and investment
in the new Tyco.
George R. Oliver
Chief Executive Officer