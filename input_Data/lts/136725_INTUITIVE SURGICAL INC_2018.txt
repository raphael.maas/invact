Dear Owner,
The healthcare industry today stands at an inflection point. Globally, the burden of disease in aging
populations is large and growing. Specifically, while surgery and acute interventions have improved
significantly in the past decades, there remains a significant need for better outcomes and decreased
variability of these outcomes across care teams. The current environment is exerting a large and
increasing burden on critical resources, including the professionals who staff care teams; surgeons,
anesthesiologists, nurses, and other staff. At the same time, governments are straining to cover the
healthcare needs of their populations and are demanding lower total cost to treat disease per patient.
In the face of these challenges, scientific, process, and technology advances in biology, computing,
imaging, algorithms, and robotics offer the promise of new methods to solve old and difficult problems.
At Intuitive, we address these needs by focusing on what hospitals have termed the quadruple aim.
First, we focus on products and services that can improve outcomes and decrease variability in the
hands of care teams. Second, we seek to improve the patient experience by minimizing disruption to
their lives and creating greater predictability for the treatment experience. Third, we seek to improve
care team satisfaction by creating products and services that are dependable, smart, and optimized for
the care environment in which they are used. Finally, we seek to lower the total cost to treat per patient
episode when compared with existing treatment alternatives, providing a return on investment for
hospitals and healthcare systems and value for payers.
Much has been said and written in the past couple of years about the promise and perils of technology
in healthcare; there is well justified excitement and skepticism about the introduction of computing
technologies into the healthcare ecosystem. As shareholders of Intuitive, it is important to recognize
that while surgery can benefit from the potential of increased computational capabilities, this is not
inherently a consumer industry. An apt analogy for technology in surgery is the use of technology in
commercial aviation. 

In commercial flight, a highly trained network of professionals (pilots, mechanics, gate agents, air traffic
control, flight attendants) uses sophisticated technologies (modern aircraft, mobile technologies, cloud
computing, simulation) to provide a safe, repeatable, and economical service to passengers who are not
expected to be expert in any of these fields.
Likewise, surgery is performed by a highly trained network of professionals (surgeons, anesthesiologists,
nurses, assistants, sterilization departments) using sophisticated technologies (the da Vinci� system,
advanced instruments, anesthesia, cloud computing, simulation) to provide a safe, repeatable, and
economically sustainable service to patients who are not, and are not expected to be, expert in any
of these fields.
For Intuitive to provide solutions that create value in the acute care environment, we need to possess:
(1) a deep understanding of the human interactions among the care team as well as the patient;
(2) outstanding, smart and cloud-enabled robotic systems, imaging systems, and instrumentation;
and (3) sensing, computing, and analytics. Alone, each of these elements is not enough � they are
necessary but not sufficient. By integrating all three � human understanding, smart systems, imaging
and instruments, and validated computational insights � we can build solutions capable of advancing the
quadruple aim. To us, this makes surgery more than minimally invasive and more than digital � it makes
it intelligent.

2018 was a strong year for Intuitive in pursuit of our mission. Use of da Vinci systems increased 18
percent from 2017 and our installed base of da Vinci systems in clinical use grew 13 percent in the year.
We serve multiple surgical disciplines, including general surgery, urology, gynecology, thoracic, and
trans-oral surgery. This year, use of our products grew significantly in general surgery, up 32 percent in
the US compared to 2017.
The opportunity for improvement in acute intervention is global, and we have been building deep
capability in our priority countries. For example, a dozen new procedures received reimbursement in
Japan in Q2 2018 after several years of clinical assessment and interaction with surgical societies
and the Japan Ministry of Health, Labor and Welfare. In the back half of 2018, da Vinci system use in
Japan grew more than 40 percent.
In 2018, we strengthened our capabilities in China with our joint venture partner, Fosun Pharma.
Regulatory authorities in China granted our da Vinci Xi clearance for sale in Q4 2018, along with
releasing a new quota for new system placements. We also integrated the distribution business of
our partner, Chindex, into our joint venture in Q1 2019. We began direct operations in India
and Taiwan in 2018. We also continued to build our clinical, economics, and commercial capability in
Europe in 2018, with the addition of talented staff to better support these markets. As with Japan, we
believe these are important investments that will return value over time.
Turning to our innovation pipeline, our teams are making outstanding progress. In pursuit of reducing
trauma by decreasing the number and size of incisions, we added a single port option to our fourthgeneration product line with the da Vinci SP� system. The FDA granted clearance for the SP system for
some urology applications, and we installed the first systems in the back half of the year. In Q3 of 2018, 

we submitted to the FDA our 510(k) application for transoral robotic surgery (TORS), our second SP
system indication, and we are collecting data for future indications beyond TORS.
To optimize multi-quadrant general surgery procedures using our fourth-generation systems, we released
our next generation stapler, the SureForm� 60mm stapler, as well as our next generation cut-and-seal
instrument, Vessel Sealer Extend. Both have received very positive customer feedback. We also released
our hernia grasper in 2018. We received FDA clearance for our SureForm 45mm stapler in Q1 2019.
Together, we expect these instruments to ease adoption of da Vinci in general surgery specialties like
hernia repair, bariatric surgery, and colorectal surgery.
In this first quarter of 2019, the FDA cleared our flexible robotics platform, Ion�, the goal of which is to
increase the accuracy and decrease the time to definitive diagnosis of lung cancer, a disease with global
needs. We believe the Ion system brings class-leading precision and control to interventional
pulmonology, and we anticipate a phased roll-out this year, focused on strengthening our clinical
evidence base and enabling outstanding programs.
Lastly, we continue to build our cloud computing and analytics capabilities, through deeper collaboration
with InTouch Health, a leading provider of telepresence health. This collaboration enables high reliability,
low latency connections to Intuitive systems, which supports remote proctoring and strengthens our
remote diagnostics capabilities. Finally, we are advancing our augmented reality capabilities including
the integration of 3D pre-operative images naturally displayed in the surgeon�s console for use in realtime during surgery, our Iris� product, with the aim of improved anatomical structure identification and
greater efficiency during surgery. Iris imaging received its first 510(k) approval in Q1 2019.
While we have outstanding technologies, our greatest asset is our world-class team of highly talented,
mission-driven people from around the globe bringing with them diverse experiences. We now have more
than 5,500 employees globally collaborating with our customers, our suppliers, and research institutions.
Collectively, we are laser-focused on the total performance of our offerings and seek to invent and deliver
effective solutions that advance the quadruple aim.
I believe there is an enormous and durable opportunity to improve acute care through our work at
Intuitive. As the pioneer in computer-assisted and robotic surgery, we are investing with an eye toward
building the market and the company for the long term. Toward this end, we prioritize our investments
as follows: First, we are assertive in developing organic opportunities to improve outcomes and our 

capabilities � we believe this delivers the greatest long-term value; Second, we invest in operational
capabilities that generate economies of scale, which allow us to lower our costs and share savings with
our customers; Third, we look for third parties that can accelerate our capabilities through partnership,
collaboration, or acquisition; and Fourth, we seek to return cash to shareholders in tax efficient ways
with an eye towards long-term value.
For 2019, we are focused on (1) making significant progress in the continued adoption of our da Vinci
systems in general surgery and in other key procedures outside the United States; (2) launching da Vinci
SP and Ion platforms; (3) driving intelligent surgery innovation; and (4) supporting clinical and economic
validation by global region.
In closing, Intuitive has a substantial opportunity to enhance the capabilities of care teams to
fundamentally improve surgery and acute intervention. We appreciate your support on this journey.
I assure you that we are focused on working on things that truly make a difference.
Gary Guthart, CEO