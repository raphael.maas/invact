be Intels sixth CEO. In my first letter to you, I would like to share how we are building on
Intels strengths to accelerate innovation and drive growth. But first, lets look at the results for fiscal year 2013.
Intel delivered revenue of $52.7 billion, net income of $9.6 billion, and earnings per share of $1.89. Client
computing products generated $33 billion in revenue and approximately $12 billion in operating profits. Our
datacenter business revenue grew to more than $11 billion, driven by rising demand for cloud services, highperformance
computing, storage, and networking. We generated almost $21 billion in cash from operations
and returned $6.6 billion to our stockholders in the form of dividends and share repurchases.
These are solid results in a time of rapid industry transformation, but we must do better. We are refocusing our efforts to reignite
growth by driving Intel innovation to market faster.
Leading in the pursuit of Moores Law
The relentless pursuit of Moores Law is Intels foundation and continues to be our driving force. We lead the industry as the only
semiconductor manufacturer in the world offering Tri-gate transistors and 22-nanometer (nm) technology-based products. The benefits
of Moores Law can be seen across our product lines in the form of higher performance, lower energy requirements, and lower cost per
transistor. In 2013, we introduced our 4th generation Intel Core processors, which deliver the largest generation-over-generation
gain in battery life in Intels history, and a new family of low-power Intel Atom processor-based System-on-Chips (SoCs) designed
for high-performance mobile devices. We will continue this manufacturing leadership as we begin production of our 5th generation
Intel Core processors (code-named Broadwell) on our next-generation 14nm process technology this year.
Accelerating mobility
The world of mobile client computing is expanding to include not only notebook computers and tablets, but also 2 in 1 devices that
combine the best features of notebooks and tablets. We are taking aggressive steps to move swiftly in all of these market segments
and to grow with them. Last year, we established a footprint in the tablet market with the shipment of more than 10 million microprocessors,
and for 2014 we have set an aggressive goal to increase that number to 40 million. We also made significant advances
in our 3G and 4G (LTE) communications capabilities in 2013. This year we are driving faster integration of these communications
technologies into our SoCs.
Reinvigorating innovation
We are reinvigorating innovation at Intel and bringing it to market faster. For example, in September 2013 we introduced Intel Quark
technology. This technology is designed for applications in fast-growing markets such as wearables and the Internet of Things,
where the priorities are extremely small size and very low power consumption. We also announced innovative products such as
the Intel Galileo development board, the first Intel architecture-based Arduino*-compatible board to support the maker and
education communities; and the Intel Edison development platform, a general-purpose computer housed in a tiny form factor designed
to enable rapid innovation by inventors, entrepreneurs, and product designers. Neither of these products was on our roadmaps
before mid-2013. You will continue to see a faster pace of innovation moving forward.
Our commitment to corporate responsibility
Corporate responsibility is an enduring Intel value that delivers returns for our company, stockholders, and society. In 2013, we
continued to expand education opportunities for millions of students around the world, we were again the largest voluntary purchaser
of green power in the U.S., and our employees donated more than 1 million volunteer hours to their communities. I am especially
proud of our leadership in the area of conflict minerals. We have worked for five years to ensure that our products do not contain
tantalum, tin, tungsten, or gold that finances or benefits armed groups in the Democratic Republic of the Congo (DRC) and adjoining
countries. In 2013, we accomplished our goal to manufacture microprocessors that are DRC conflict-free.
Prepared for the opportunities ahead
I believe we have great opportunities ahead of us. Intel has leading products and technologies for growing markets, a strong workforce,
and a heritage of leadership in driving Moores Law. We have sharpened our product focus and are accelerating the pace of
our innovation. I look forward to growing stockholder return by delivering industry-leading products for a dynamic and changing
marketplace and ensuring that If it computes, it does it best with Intel.
Letter From Your CEO

Brian M. Krzanich, Chief Executive Officer