2003 was another great year for the company, bringing
Aetna a major step closer to our goal of industry leadership.
In January 2003, we set out some ambitious objectives.
Entering the second half of the companys turnaround, our
priorities were to further expand margins by continuing to
lower our medical cost trend and reduce administrative expenses,
while forging ahead with the implementation of our new
strategic plan. At the same time, we committed to enhancing
our health care quality initiatives and strengthening our
working relationships with doctors and hospitals.
It was a lot of ground to cover, but over the course of 12 months,
we produced outstanding results. Aetna employees proved once
again that they know how to deliver on our promises.
ACCOMPLISHING OUR FINANCIAL GOALS
In 2003, we made further significant progress on Aetnas key
financial, operational and strategic goals. Last year, we reported
full-year operating earnings of $5.18 per share, more than
double the $2.50 per share reported in 2002, excluding favorable
reserve development, realized capital gains and losses and other
items. At year-end, Aetnas pretax operating margin was
7.9 percent. At press time, our stock was trading at its highest
levels since becoming an independent health care company
in December 2000.
We achieved these results by managing medical costs
effectively  Aetnas commercial risk medical cost increases
slowed to 8 percent in 2003, one of the lowest rates in the
managed care industry. In addition, we became more costeffective
as an organization, reducing expenses by $194 million,
while continuing to improve customer satisfaction levels.
We also ended our long decline in membership that began in
2001 and steadied our membership at approximately 13 million.
While we experienced some membership reductions in existing
accounts due to the economy  as did other insurers  which
affected our ability to grow in 2003, those reductions were offset
by solid new sales, and we now are on track to achieve profitable
growth this year. For 2004, we are projecting net membership
growth in the range of 500,000 to 700,000.
These excellent results are not a one-time event. Clearly,
Aetna is back.
INNOVATION, INTEGRATION AND INFORMATION
In 2003, we also set our sights firmly on enhancing Aetnas value
in the marketplace. A steady stream of new products and services
came out of our pipeline to support our strategy to innovate in
every aspect of our business, as well as integrate products and
information in order to facilitate customer decision making,
help improve health care quality and lower costs.
Last year, we became the first full-service national competitor to
integrate a consumer-directed health plan with pharmacy, dental
and long-term care benefits. We continue to be pleased by the
favorable response to our industry-leading Aetna HealthFund
consumer-directed product, which early experience indicates is
effectively controlling costs, while increasing utilization of
preventive care. We also introduced a new dental discount
program called Vital Savings by AetnaSM for employees and
retirees without access to dental insurance through their
employers; and rolled out a new integrated medical, disability
and behavioral health management model. With Aetna
Integrated Health and Disability, we can consider member
needs from both a health- and work-related perspective.
This holistic approach can lead to more coordinated care
and reduced absenteeism.
In 2003, we also enhanced our online plan sponsor
administration tools and employee self-service Web sites.
Employers and consumers need innovative tools and information
to help them spend their health care dollars more wisely,
and Aetna is one of the few companies with the scale and
infrastructure to help them. For example, with our awardwinning
Aetna Navigator, now with more than 3 million users,
members can price prescription drugs, determine their portion
of the cost, order refills from Aetna Rx Home Delivery and
check order status  all through the same online portal.


ENHANCING OUR QUALITY-OF-CARE INITIATIVES
Using information to improve access to high-quality and safe
medical care for members was also foremost in our minds. In
2003, we began piloting an innovative network of specialist
physicians, called AexcelSM, based on evidence of quality health
care outcomes and cost-effectiveness of care delivery in general
surgery, obstetrics-gynecology, orthopedics, cardiology,
cardiothoracic surgery and gastroenterology.
Another effort was our innovative MedQuerySM program. This
program helps doctors identify opportunities to avert potential
medical errors, gaps and omissions in members health care by
providing physicians additional information they may not be
aware of and may want to consider in a patients treatment. To
date, we have identified more than 100,000 opportunities. We
also launched Aetna Rx CheckSM, a suite of six drug utilization
review programs designed to improve member safety by flagging
for physicians potentially harmful interactions, as well as identify
opportunities to reduce pharmacy costs for members.
Our disease management programs and new medical case
management initiatives are helping Aetna reduce claim costs
while facilitating members access to quality care. We were
very pleased when last October Aetna won the Best Disease
Management Program in Managed Care award from the
Disease Management Association of America for our chronic
heart failure program.
USHERING IN A NEW ERA OF COOPERATION
Finally, last year we ushered in a new era of cooperation with
the medical community. When we first embarked on our effort
to rebuild Aetna, one goal we laid out was to strengthen our
relationships with doctors. On May 22, 2003, we announced
an agreement with nearly 1 million physicians to settle a
national class-action lawsuit. In August, we reached a similar
agreement with the American Dental Association on behalf
of approximately 150,000 dentists.

RESULTS
 Aetnas commercial risk
medical cost increases
slowed to 8 percent
in 2003.
 We reduced expenses
by $194 million,
while continuing to
improve customer
satisfaction levels.
 A steady stream of new
products and services
came out of our pipeline
to support our strategy.

These landmark agreements  and related business practice
changes designed to make it easier to do business with Aetna 
have set a precedent in the industry. We already are seeing
improvement in satisfaction among these constituents, which is
giving us a competitive advantage in the marketplace, as well as
a better opportunity to work with them to improve health care
costs, access and quality. We will continue to build on Aetnas
leadership position in this area.
COMPLETING AETNAS TRANSFORMATION
In 2004, we will build on our success and work toward
completing Aetnas transformation. Our most important task
will be to demonstrate profitable growth. This will be fueled by
continuing our industry leadership in medical cost control and
providing value-added innovations that serve customers needs.
This growth will be aided by strengthening the Aetna brand so
that it clearly differentiates us in the marketplace.
Our new We Want You To KnowSM advertising and tagline
signals that a new leader is emerging in the industry, one
that goes beyond simply paying claims for customers. Going
forward, we will focus all of our energies on putting our
knowledge to work for customers to help them make betterinformed
decisions about their health and financial well-being.
We will continue to take advantage of our strengths 
information, innovation and integration  to provide
end-to-end service excellence in every interaction and deliver
a unique value proposition to our targeted customers.
Our goal will be to generate profitable business from new
customers, as well as expand existing accounts with additional
products from our comprehensive portfolio, including benefits
such as dental, pharmacy, behavioral health, long-term care,
disability and life insurance. In 2004, Aetna also is building
on existing relationships with doctors and hospitals to launch
a workers compensation network access product.
In addition to growth in our existing product lines and markets,
our capital deployment strategy calls for targeted acquisitions
that are relevant to our strategy, consistent with our brand and
grounded in the needs of customers. These acquisitions will be

manageable in size and must enhance our existing capabilities or
growth opportunities in the future. An excellent example is our
recent acquisition of The Chickering Group, a recognized leader
in the highly specialized student health insurance market.
We also will continue to look for ways to operate more
efficiently, and achieve and maintain a competitive cost structure.
Through our disciplined approach to expense management, we
believe we can achieve a $150 million reduction in general and
administrative costs in 2004, which will be partially offset by
a $100 million increase in selling costs.
TAKING A LEAD IN IMPROVING THE SYSTEM
Beyond our business priorities, this year we also will continue
to lead our industry in helping to shape public policy on broad
and complex issues facing our nations health care system. The
Medicare bill, for example, was a landmark piece of legislation
we supported because key provisions not only provided
increased funding for the program, it introduced new health
plan choices to seniors, a new prescription drug discount
program and new tax-deductible health savings accounts
(HSAs). We will take this opportunity to provide Medicare
beneficiaries expanded products and services, and in 2004 we
will offer HSAs as part of our portfolio of consumer-directed
product solutions.
We also will continue to raise awareness about other important
societal issues such as a still-growing population of uninsured
Americans. In the wealthiest country in the world, we now have
43.6 million uninsured individuals. We are learning more about
many subsets of this group  such as college students  and
working hard to offer affordable solutions.
As part of Aetnas comprehensive efforts to help address racial
and ethnic disparities in health care  composed of research,
education, data collection and supporting initiatives 
we are developing programs targeted to the needs of specific
populations such as a maternity management program for
African American women and a cervical cancer prevention

In 2003, the Aetna
Foundation provided
approximately
$3 million in funding
to programs focused
primarily on addressing
disparities in health
and disease prevention.
COMMITTED
to shaping public policy
on broad and complex
issues facing our nations
health care system

program for Vietnamese women. These important initiatives in
our business are complemented by strong efforts in this area by
the Aetna Foundation. In 2003, the Aetna Foundation provided
approximately $3 million in funding to programs focused
primarily on addressing disparities in health and disease
prevention. In the future, well continue to align our customerfocused
business efforts and our corporate philanthropic efforts.
You can read more about these issues  and other issues such as
childhood obesity, malpractice reform, consumer-driven health
care, health care quality and safety, infectious diseases and
Medicaid  in the essays written by outside experts that are
included in our annual report. As in past years, Aetna remains
deeply committed to bringing together divergent views and acting
as a catalyst for change in our nations health care system.
WHERE AETNA STANDS TODAY
We are very pleased with where Aetna stands today. We have
completed the first stage of our turnaround, our financial results
have improved, and we are gaining momentum on our quest to
be the leader in our industry.
Our accomplishments in 2003 were a perfect way to celebrate
Aetnas 150th anniversary. Through hard work and determination,
Aetna is emerging as a major force in the marketplace and
delivering business results our shareholders expect. Drawing
inspiration from our companys history, we are bringing forth
a stronger, new Aetna, one that is ready to embark on its future.
Thank you for your continued confidence in us. We believe there
is greater success to come.

2003
First Stage of
Turnaround Completed.
Financial Results
Improved.
Gaining Momentum.

Aetna is emerging
as a major force in
the marketplace
and delivering
business results our
shareholders expect.