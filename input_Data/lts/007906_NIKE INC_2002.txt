	



	



To Our Shareholders:

I can picture them still. That wave of yellow sweeping across the great, green lawn called a pitch. As the world stood witness, Brazil recaptured the World Cup. But it was something more than victory. It was poetry.

Why does it matter?

It matters because it is important to consumers.

Fans in Korea snatched up 150,000 Nike national team jerseys, a demand that outpaced predictions by a factor of 10. In leading retail shops across Europe, a $175 boot we call the Mercurial Vapor led all other boot sales; this in the heart of the games old guard. We have an operating principle at Nike that states, The Consumer Decides. Lets just say our faith in their decisions has never been stronger.

It matters because of what Brazil provedand proved wrong. Regarded by some to be yesterdays hero, distracted by a welter of political noise, Brazil showed its true color as leader of the football world.

We came back, too.

We entered FY 02 with a 1% decline in U.S. futures orders. We had our work cut out for us. Then came September 11, and with it a bow wave of uncertainty. Restaurants, stadiums, theme parks, malls, all thinner of crowd, showed the wan face of anxiety. We were at a threshold, one of those defining moments that pop up out of nowhere in every life, both individual and corporate. You can cross that threshold with courage or turn away in fear. Either way, you change forever.

We decided to cross the threshold of September 11. Eight months later we delivered a 14% increase in earnings, and beat the S&P by 45 points (+31% vs. 14%)a defining momenta Nike moment.

The World Cup mattered in other ways, too.

For eight years we were a distant number-two in the worlds biggest sport, football. This year we came away from the sports premier event with the best team, the best player, the best shoe and the best communications. Were still number-two, but we are now on the front lines. We love a good fight. And were packing for Berlin.

Charlie Denson, who along with Mark Parker is doing such a great job as co-president of the Nike brand, says this years football effort marked that moment when we became a real global company.

Could he be right?
Was this a Nike moment?

To be a global company in our vocabulary means far more than to merely conduct business internationally. I have used this space before to discuss our evolutionary tale, our road to becoming global. Once a story set firmly in the future, we can now claim it to be at least a part of our present.

Nike is global when we are competently linked with all our offices, markets, and sources around the world. We are global when the brand stands for the same things regardless of the geography involved. Global is knowing when to make decisions on the ground in Shanghai or London or Rio de Janeiro, when to coordinate against approved strategy, and when to strike on instinct. It is applying our greatest strength, delivering the best products for the best athletes, while keeping a close eye on expenses and gross margins.

Many companies in our industry compete in most countries around the world, but none is really global in our definition of the word, as informed by the books of former director Ken Ohmae. Moreover, we believe that the rewards to the first company in our industry to achieve that status will be considerable.

Certainly, we have been unwavering in our goal to be that global company and have made a great deal of progress. Our management is growing into our sales.

We have for years said that growth for Nike will come from beyond America. This year, for the first time in our history, futures orders are greater outside the U.S. than insidea Nike momentand, we will be very disappointed if the final sales tally this time next year doesnt reflect that.

But wherever we are on that global spectrum, Ill tell you one thing thats truly global by almost any definition, and is so despite almost universally inept management: sports.

The number-one draft pick in the NBA is a 75 Chinese hero.

A Japanese-American speed skater was the biggest star coming out of the Salt Lake City Olympics.

An American of African-Asian descent dominates golf.

Japan won its first-ever game in the World Cup (on a goal by a Nike athlete) and Korea (wearing Nike kits) also recorded its first victory, and went on to the semifinals.

No one can doubt the passion seen in the flags and riotous flames of countries upsetMoscow, Mexico City, Buenos Aires come immediately to mindbalanced by the joy of victory in Korea and Senegal.

There are more than 100 golf courses in the Shenzhen area of China.

Sports is one of our worlds mother tongues. It is our business. It is a good business. And I love being a part of it.

Nike is 30 years old this summer. We have come a long way from the days of selling shoes out of the trunk of my car. But Nike refuses to be defined by where we once stood. I consider every day of the past 30 years as preparation for this moment this Nike momentand all that follows. 


Philip H. Knight
Chairman of the Board,
Chief Executive Officer
and President 