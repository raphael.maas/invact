                          FELLOW SHAREHOLDERS:

                                                                               The worldwide financial crisis has presented a
                                                                               significant challenge for businesses throughout
                                                                               America. Contractions in liquidity, volatility in
                                                                               commodity prices, and a general downturn in
                                                                               business activity have affected our customers, the
                                                                               communities we serve, and Integrys Energy Group.
                                                                               It has been, and continues to be, a difficult
                                                                               environment for all.

                                                                               Our financial strength and conservative business
                                                                               model have aided us in dealing with the challenges.
                                                                               However, the global financial crisis has negatively
                                                                               impacted our shareholders, and we are modifying
                                                                               our business model and risk profile to provide
                                                                               more protection for our company, customers, and
                                                                               shareholders. I will address this later in the letter.
                                                                               First, let's review our actions in 2008.




                                                                            A WORKABLE REGULATORY
Throughout the past year, our employees have kept their focus
and used their energy to produce results that we expect will
                                                                            ENVIRONMENT
build value for our stakeholders for years to come. We have
made progress on several initiatives.                                       During 2008, we were very active in several regulatory arenas.
                                                                            We successfully completed rate cases in Wisconsin, Illinois,
AN ENVIRONMENT OF                                                           and Michigan. We also received an interim rate increase in
                                                                            Minnesota. These rate cases were critical to our continuing
"GETTING IT DONE RIGHT"                                                     success. They are the first authorized rate increases following our
We completed major construction projects, including the                     significant merger activity in 2006 and 2007, and the successful
500-megawatt Weston 4 generating facility. Weston 4 was                     completion of the rate cases acknowledges that the business
placed into service on July 1, 2008, and garnered two national              systems and processes we are using are accepted by regulators.
recognition awards--POWER magazine's 2008 Plant of the Year                 The conclusion of these cases also improves our return on
award, and Power Engineering magazine's 2008 Best Coal-Fired                investment by correcting for the regulatory lag that typically
Project award. The project was completed on schedule and                    follows a merger or acquisition. The rate orders received in
under budget, at a cost well below comparable projects.                     Illinois and Wisconsin have also approved the use of an energy-
                                                                            efficiency and conservation tool known as "decoupling." This will
We completed construction of the natural gas laterals for the
                                                                            enhance our ability to help customers conserve energy and use
Guardian II pipeline project on schedule and slightly under
                                                                            energy more efficiently. The decoupling mechanism also reduces
budget, at $79 million versus a revised estimate of $85 million.
                                                                            our company's exposure to the risk of fluctuating demand by
The Guardian II trunkline was completed on February 26, 2009.
                                                                            helping to stabilize our margins and net income. We have
As a result, our customers now enjoy the benefits of competitive
                                                                            approximately 2.2 million regulated utility customers, and about
natural gas pipeline service to northeastern Wisconsin.
                                                                            80 percent are now served through decoupling mechanisms.
American Transmission Company, of which Integrys is a 34 percent
                                                                            STRATEGY CHANGES FOR THE
owner, completed major components of its $2.8 billion building
plans. Our investment in American Transmission Company is
                                                                            NEW ENVIRONMENT
adding significant value for shareholders today and will continue
                                                                            Our accomplishments during the past year have been many
doing so for many years.
                                                                            and varied, but the economic environment and financial crisis
Completing these projects provides benefits for customers,                  throughout the world continues. In light of this, and with a
reduces our future capital expenditures, and reduces our cash               determination to remain a strong company, our management
requirements. This represents a significant improvement in our              team has taken steps to re-evaluate our strategy and business
overall risk profile.                                                       model for the near and far term. Success during this period of
                                                                            illiquidity requires timely and swift action.
We have made good progress integrating the Michigan and
Minnesota natural gas distribution operations we acquired in                In the short-term, we have taken steps to reduce our cash
2006 and the Illinois natural gas distribution companies we                 requirements, improve the liquidity in our business model, and
acquired in 2007 with the Peoples Energy merger. We captured                thereby lower our risk profile. We increased our credit facilities
$83 million of annual synergy savings in 2008, surpassing our               for the 2008-2009 winter heating season by $400 million in
target by $10 million. We expect the savings to increase to                 November 2008. We reduced our capital expenditures by
$114 million annually by 2011. In addition, we reduced the                  28 percent, or $186 million, for 2009 and another 41 percent,
costs to achieve the integration by $39 million, to $147 million            or $213 million, for 2010. We are continuing to scrutinize
in total costs to achieve.                                                  our capital expenditure programs and our working capital
                                                                            requirements with the intent of improving cash flow. We have
We have successfully established Integrys Business Support as a
                                                                            reduced wage increases for 2009, and the senior management
separate business unit within Integrys Energy Group to provide
                                                                            team and the Board of Directors have accepted zero wage
centralized services to all subsidiaries and the parent company.
                                                                            increases for this year. We have also initiated a hiring freeze
This business unit has helped us lower costs for services provided,
                                                                            and announced a general reduction in the use of contractors
increase service quality, and provide transparency to regulators.
                                                                            throughout our organization.
Demonstrating leadership in protecting the environment and
                                                                            These initiatives will help improve our liquidity and risk profile
using energy efficiently, we have also been involved in the
                                                                            in the near term, but a strategic shift in our business model is
design and construction of four new buildings that are LEED
                                                                            required for the long term.
(Leadership in Energy and Environmental Design) certified. The
new office buildings for Minnesota Energy Resources Corporation             As a result, in February 2009, we announced a strategic shift
in Rochester, Minnesota; Wisconsin Public Service Corporation in            affecting our nonregulated subsidiary, Integrys Energy Services.
Rhinelander, Wisconsin; Integrys Energy Services in De Pere,                This subsidiary has been very successful and has enhanced
Wisconsin; and the American Transmission Company in Pewaukee                shareholder value for over a decade, but its success has
are all being designed and/or constructed in accordance with                outgrown our ability to adequately support its growth. This
these new environmental and efficiency standards.


                                                                           substantial growth opportunities and help us reach our financial
situation is exacerbated by the uncertain financial markets. The
                                                                           goal of creating long-term value for investors. We expect our
risk would be less of an issue for an owner larger than Integrys.
                                                                           strategic modifications will improve our earnings quality. Capital
We are, therefore, pursuing a full divestiture of this business
                                                                           investments combined with timely rate relief should provide
segment, with alternatives including divestiture of portions of
                                                                           earnings per share growth of 4 to 6 percent on an average
this business or scaling back by further modifying the scope of
                                                                           annualized basis, subject to some fluctuations depending on
the products offered and/or the markets we serve. The goal is to
                                                                           the economic environment. From a financial perspective, our
reduce the demands on our balance sheet and capital support
                                                                           goal is to provide solid returns to investors, manage our risk
obligations that are driven by commodity prices, which, at this
                                                                           profile to acceptable levels, and continue providing consistent
time, have demonstrated unprecedented volatility. We are
                                                                           and dependable dividends to our shareholders.
seeking to deploy our capital to areas with more desirable
risk-adjusted rates of return. We expect to significantly reduce
corporate guarantees and invested capital that have been required
                                                                                             
by our nonregulated energy services segment. If we do not
divest of this business segment entirely, our ultimate objective                                                                                             $2.50
is a size and scope for Integrys Energy Services that will reduce
the liquidity, capital, and credit support requirements for our
nonregulated energy services segment to an insignificant level                                                                                               $2.00

and will give us greater control of our own destiny.
                                                                                                                                                             $1.50
Certain business activities within the nonregulated subsidiary
may remain. These will be identified through our strategic
planning process but could revolve around our expertise in                                                                                                   $1.00
renewable energy and energy efficiency.

Though we are seeking to divest of or reduce the size of our                                                                                                 
nonregulated business, we are planning to expand the regulated
side of our business. The regulated utilities within Integrys Energy                                                                                         $0.00
                                                                           
Group have significant opportunities for investment, which will
enhance shareholder value. These investments will focus on
energy efficiency, conservation, renewable energy sources,
                                                                           AN ENVIRONMENT OF LEADERSHIP
environmental improvements, and infrastructure improvements.
                                                                           Dealing effectively with financial challenges and meeting our
Over the long-term, our utilities have investment opportunities
                                                                           obligations to shareholders will require effective leadership
exceeding $3 billion. Much of this investment will be
                                                                           for years to come. Charles (Charlie) Schrock, the new president
required for infrastructure improvements at Peoples Gas.
                                                                           and chief executive officer of Integrys Energy Group, effective
Wisconsin Public Service and Upper Peninsula Power will also
                                                                           January 1, 2009, is well qualified to provide that leadership.
require investments in additional renewable projects and
                                                                           He brings over 30 years of utility industry experience and a
environmental improvements.
                                                                           strong record of accomplishment to the job.
During 2009, we committed to developing a 99-megawatt wind
                                                                           The board of directors also elected Charlie a director effective in
farm project known as Crane Creek. The project, located in Iowa,
                                                                           February 2009.
will be completed in 2009 at a projected cost of $251 million, and
it will provide renewable energy for our customers.                        Charlie will be supported by a very capable and talented group
                                                                           of leaders throughout our company. There have been numerous
These projects, combined with our investments in infrastructure
                                                                           changes in our leadership team during the past year, and I will
improvements and environmental equipment, will provide
                                                                           only mention a few of those changes here. Phillip (Phil) Mikulsky,
                                                                           executive vice president  corporate development and
                                                                           shared services of Integrys Energy Group, is now also president
  LONG-TERM FINANCIAL GOALS
                                                                           of Integrys Business Support. William (Bill) Laakso is our new
    Provide investors with a solid return on their investments.
                                                                           vice president  human resources. Willard (Will) Evans, Jr.,
    Grow our earnings per share from continuing operations                 is our new president of Peoples Gas and North Shore Gas.
    by 4 to 6 percent on an average annualized basis.                      Charles (Chuck) Cloninger is now president of Michigan Gas
                                                                           Utilities as well as Minnesota Energy Resources, and Jodi Caro
    Divest or reduce our nonregulated business segment
                                                                           joined us as vice president  legal services.
    such that its demands on liquidity and capital are not
    significant by the end of calendar year 2010.
                                                                           Membership on the board of directors also changed, and I
    Manage the risk profile of our business portfolio.                     want to express our gratitude to departing members Jim Boris,
                                                                           Diana Ferguson, and Jack Meng. We thank each of them for
    Continue quarterly dividend payments.
                                                                           the contributions they have made to our success.


We have also made progress in creating a single culture
throughout Integrys Energy Group. Over 674 of our leaders have
graduated from our Leadership Development classes, which are
based on Integrys Energy Group's values and expectations.

We are delivering on our core values and expectations daily.
And we continue to gain recognition for our efforts, as was
recently evidenced when we were recognized by FORTUNE
magazine when it designated Integrys Energy Group as the
World's Most Admired Energy Company on its prestigious 2009
list of "Most Admired Companies" (in its March 16, 2009, issue).
In fact, Integrys rated well against the 363 companies that
appear on FORTUNE's most admired list, where we were the
8th most-admired company in the Use of Corporate Assets
category, the 9th most-admired company in the Innovation
category, the 10th most-admired company in the Long-Term
Investment category, and the 11th highest-rated company
overall by total score.

AN ENVIRONMENT OF ENERGY
I can assure you that the Integrys team is dedicated to
operational excellence and has the expertise and energy
needed to enhance shareholder value for our investors.

We thank you for your investment in Integrys Energy Group
(TEG), and we assure you that we will protect your investment
as our own. Thank you for the faith you have placed in us.

Sincerely,




Larry L. Weyers
Executive Chairman

March 6, 2009