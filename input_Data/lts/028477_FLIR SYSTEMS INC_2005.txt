

To Our Shareholders:

I am pleased to report that 2005 was another         electrical contracting. We expect 2006 to be a        Inc. for integration into the AutoCamTM night
successful year for FLIR Systems. While our          very good year for Thermography and we see            vision system for the automotive market. Santa
revenue growth slowed from the rapid rate            a bright future for our strategy of growing the       Barbara also introduced several new products in
experienced in 2004, we again achieved record        market for thermal imaging technology through         the security and surveillance market in 2005, and
revenue, operating income and profitability.         lower cost products.                                  introduced ThermoVision Mariner, our new
Perhaps more importantly, we made excellent                                                                low cost product targeted at the marine market in
                                                     Our Imaging Division generated revenue of
progress in the strategies we believe are crucial                                                          early 2006. On top of all these accomplishments,
                                                     $325 million, up 2% from 2004. This was
for our future success.                                                                                    this factory significantly improved its profitability
                                                     slower growth than we expected coming into
                                                                                                           in 2005 as well.
For the year, revenues increased 5% to $509          the year, and we expect to do significantly better
million, and earnings from operations grew 15%       in 2006. However, Imaging operating margins           Looking to 2006, we are very optimistic about
to $126 million. We improved our operating           improved to 27% and we saw the beneficial             the business. We have introduced many new
margin to a record 24.8% and earnings per share      impact of our new, more efficient manufacturing       products in both of our divisions. Our cash flow
on a fully diluted basis were also a record at       plants in Boston and Portland, as well as the         is very good, offering us flexibility to continue
$1.16, an increase of 23% over 2004. We reduced      savings derived from using infrared detectors         to make strategic acquisitions or to return
manufacturing costs and continued to invest          made at our Santa Barbara facility (formerly          capital to our shareholders through common
heavily in both our Imaging and Thermography         Indigo Systems) in our Imaging products. Our          stock repurchases. We remain focused on our
businesses. For the year, we invested a total of     Portland facility had a very good year and in the     long term strategies: reducing production costs
$62 million on R&D, consisting of $51 million        fourth quarter set a record by producing 70 large     to exploit price sensitive markets, growing
in internal funds and an additional $11 million      gimbaled systems. Portland introduced Star            distribution and actively developing new products
as part of several government funded research        SAFIRE HD, our newest high performance               and markets.
and development projects. This enabled us to         large gimbal and UltraMedia HD, our first
                                                                                                           I am proud of our performance and of the
introduce a number of new products and enhance       high definition product for the broadcast market.
                                                                                                           dedication and hard work of our 1300 employees,
our existing product lines.                          While revenue in our Boston factory was down,
                                                                                                           who made it happen. I believe the commercial
                                                     margins remained healthy and we introduced
Our Thermography Division grew revenue                                                                     infrared business has a very bright future and
                                                     several new products, including new hand-held
by 13% to a record $184 million, maintained                                                                FLIR is committed to maintaining its market
                                                     and small gimbaled systems.
excellent profitability, and completed several                                                             leadership. With the exciting opportunities
critical projects. We expanded our production        We were delighted by the performance of our           ahead of us, I can say yet again for FLIR, the best
facilities in Stockholm to support continued         Santa Barbara operations in 2005. The former          is yet to come.
volume growth and improve manufacturing              Indigo Systems, acquired in early 2004, has
                                                                                                           Thank you for your continued interest and
efficiencies. In June, we introduced                 proved to be a remarkable success. During
                                                                                                           support.
GasFindIRTM, a new product that provides cost        2005, we saw significant cost savings from the
saving, environmental and worker safety benefits     integration of Indigo detectors into our Imaging
for the petrochemical industry and its regulators.   and Thermography products. The introduction
This product was well received and we expect         of InfraCAM, at a groundbreaking price and
good demand in 2006. We test marketed and            with excellent margins, is directly attributable to
completed development of InfraCAM, our              the lower cost of our in-house infrared detector
                                                                                                           Earl R. Lewis
new low cost, full featured thermal imager.          manufacturing capability. Our Santa Barbara
                                                                                                           Chairman of the Board, President
InfraCAM was formally launched on February           facility has also established itself as the leading
                                                                                                           and Chief Executive Officer
15, 2006, with a US suggested retail price of        supplier of infrared camera cores to original
$6,750  over 30% lower than our least expensive     equipment manufacturers such as Mine Safety
ThermoCam E-Series cameras, and a level we          Appliances, Inc., which uses our ThermoVision
believe will further stimulate demand in such        Photon core in its industry leading Evolution
price elastic markets as building inspection and     firefighting thermal imager series, and to Autoliv
