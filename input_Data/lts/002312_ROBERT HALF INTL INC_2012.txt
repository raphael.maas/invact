To Our Stockholders

Robert Half performed well in 2012. Full-year revenues of
$4.1 billion grew 9 percent from the prior year, and diluted
earnings per share of $1.50 increased much faster, at 44 percent.
Our business last year was characterized by generally
healthy U.S. demand for our temporary staffing and permanent
placement services and Protivitis offerings. By contrast, our non-
U.S. operations were challenged by weaker economies in select
countries, particularly in parts of Europe.
U.S. real gross domestic product grew during 2012 at a
2.2 percent rate, modestly ahead of the prior years 1.8 percent
increase. Labor markets also showed moderate improvement,
with monthly U.S. nonfarm job growth averaging 182,750 in
2012. Those employment increases, along with a shrinking labor
participation rate, caused the unemployment rate to decline to
7.8 percent at the end of 2012, down from 8.3 percent at the
start of the year. In February 2013, the U.S. jobless rate declined
further, to 7.7 percent.
Stepped-up secular demand for flexible staffing provided a
tailwind for our business, particularly in the United States. Companies
are using temporary and project-based workers to gain
access to skills when and for as long as they need them, which
keeps their labor costs variable. The appeal of flexible staffing
is magnified during periods of increased economicand political
uncertainty, and this was true in 2012. The penetration of temporary
employees into the overall labor force has continued to rise.
Temporary workers today represent 1.9 percent of U.S. nonfarm
employment, a penetration level that approaches the record high
of slightly more than 2 percent in 2000.
By the middle of last year, Robert Halfs quarterly revenues
had returned to 2006 levels. This is notable when considering
the U.S. unemployment rate averaged 4.6 percent in 2006,
compared to a much-higher average unemployment rate of
8.2 percent in the first half of 2012. The ability to generate
comparable revenues in a more challenging labor market
underscores the higher secular demand for flexible staffing
and is testament to the hard work and effectiveness of our
experienced field staff and corporate services leaders.

International staffing activities accounted for 27 percent of our 2012 overall staffing revenues
and declined 3 percent from the prior year. The majority of
the total (16 percent) came from Europe, where we operate in 10 countries.
Our best-performing European business was in Germany, where demand
drivers are similar to those in the United States. We see a considerable
market opportunity in Germany and continue to make staffing investments
there. In the United Kingdom, results were affected by difficult economic
conditions and persistent high unemployment. To combat the weaker
demand environment, we reduced spending, limited geographic expansion
and focused on growing profitability in established markets. 

Financial Condition
Once again, our business demonstrated the ability to produce generous
amounts of cash. Last year, cash provided by operating activities totaled
$289 million; the past five-year total was 1.4 billion. We used nearly all of
the cash generated in 2012 to: 1) invest in the business; 2) purchase our
shares in the open market; 3) pay cash dividends; and 4) complete two
small acquisitions. Our $288 million cash balance at year-end was up
$8 million from the prior year.
Capital expenditures in 2012 were $50.1 million. Those outlays were
equivalent to 1.2 percent of revenues, which is a bit below the prior five-year 
average of 1.5 percent. Both percentages are relatively low by typical U.S.
corporate standards. We are not a brick-and-mortar-intensive business.
Much of our spending in recent years has been for updating computer
hardware and software used throughout our organization. The Internet
has had a significant effect on the job search and recruitment processes.
More recently, the proliferation of social networking vehicles has introduced
new tools for our recruiters to employ. Early on, we recognized the
considerable benefits the digital world would bring to the staffing industry.
We are committed to continuing to provide our people with state-of-the-art
technologies.

After netting investing outlays, free cash flow totaled $216 million and
was $1.1 billion for the past five years. We have long been dedicated to
returning cash to our shareholders. Last year was the 16th consecutive
year in which we purchased our common shares in the open market.
During 2012, we spent $133 million to acquire 4.7 million shares in the open
market. An additional 11.4 million shares remain authorized for purchase
through 2013 and beyond. We have also returned cash to shareholders
through the payment of cash dividends since 2004. Last year, our board
declared a $0.15 per share quarterly payout, which consumed $84 million
of cash. Early in 2013, the board raised the quarterly cash dividend to $0.16
per share. The dividend amount has been increased yearly since it was
initiated while compounding at a 12 percent average annual rate.

Our preference has been to grow the business internally rather than to
expand by making acquisitions. We believe strongly that there is ample
market opportunity for our industry and company to grow organically. We
also believe that relying on acquisitions as a primary growth strategy carries
higher risk, especially to our unique corporate culture. However, that does
not mean we will not look at acquisition opportunities when the businesses
involved represent and excellent fit both culturally and strategically. Last year,
we completed two relatively small transactions. In June, Protiviti, our internal
audit and consulting unit, purchased Arsenal Security Group, Inc., a
Virginia-based business that provides IT security and privacy consulting
services. Then, in December, Protiviti purchased the assets of SusQtech
Inc. It will complement Protivitis Microsoft technology-based consulting
services. It will also have a role in serving the client base of Robert Halfs
staffing operations

Division Highlights

Our three finance and accounting staffing divisions represent the majority of our staffing business domestically and internationally. Combined revenues
in 2012 were $2.4 billion and represented 57 percent of consolidated
revenues. Revenues from these three mainstay businesses combined
grew 9 percent last year.
Accountemps provides accounting personnel on a temporary basis. It was
our initial temporary staffing offering and is our largest single business unit.
Its 2012 revenues were $1.5 billion, or 37 percent of the corporate total.
Its global full-year revenues increased 9 percent, with domestic business
showing better gains than those in international markets. All four quarters
produced year-to-year improvement, but quarterly increases moderated as
year-ago comparisons became more demanding.
Robert Half Management Resources provides senior-level accounting
and finance professionals on a project basis, often for longer duration
assignments. This business was launched in 1997. Last years revenues
of $506 million were 10 percent higher than the prior year and were equivalent
to 12 percent of the companywide total. All four quarters showed solid
gains , with the first six months stronger than the second half of the year.
Robert Half Management Resources, as well as our other staffing lines of
business, provide us with an important competitive advantage: They enable
us to offer a unique delivery model by combining highly experienced interim
professionals with Protivitis consultants and its business solutions to serve
a broad range of client needs at attractive prices.

Robert Half Finance & Accounting provides specialized permanent
placement services and was our original business. Its 2012 revenues of
$334 million increased 11 percent from the prior year and represented
8 percent of consolidated revenues. The permanent placement business
enjoyed solid gains throughout the year, with stronger increases in the first
two quarters of 2012.

Office Team is our specialty administrative staffing unit. Last year's revenues 
of $815 million increased 8 percent over the prior year. Office TEam revenues
represented 20 percent of the consolidated total. This business began
in 1991 and was our first expansion effort outside of the accounting and finance specialties.
Over those more than two decades, OfficeTeam has  differentiated itself by focusing on high-end administrative staffing.
Robert Half Technology revenues of $476 million were 12 percent of
the tital and 11 percent ahead of last year. We have defined IT staffing
as a significant growth vehicle and have supported it domestically and in 
select international markets with increased staffing investment. The digital
world has become integral to our small to midmarket client base. However,
many of these smaller businesses cannot justify adding full-time employees
and turn to staffing firms for technical sipport and assistance with software
customization and development. We aim to leverage our well-established
relationships with accounting and finance managers to achieve a leadership
position in the growing IT business sector.
Robert Half staffing divisions operate in 19 countries outside the United
States.Revenues for international staffing operations were $983 million in
2012, down just under 3 percent from the prior years total. Non-U.S.
temporary staffing revenues accounted for 86 percent of that total and 
declined 3 percent, while foreign permanent placement produced the other
14 percent of revenues, down less than 1 percent. Soft economic conditions
in parts of Europe and elsewhere overseas negatively affected our
non-US staffing business, but the effect was moderated by our limited
presence in the hardest-hit countries. Our business in Germany held up
well, and, in North America, Canada remained strong.
Protiviti, our global business consulting unit, had its best year since 2008.
Revenues in 2012 were $453 million, or 11 percent of the corporate total,
and were 7 percent ahead of the prior year. Domestic revenues accounted
for 77 percent of the total and grew 11 percent. By contrast, non-U.S. Protiviti
billings declined 5 percent.

Protiviti celebrated its 10th anniversary in 2012. IN the first half of that
decade, surging demand for the units compliance services was fueled by
the 2002 enactment of the Sarbanes-Oxley Act. But then, large companies,
and later smaller businesses, affected by the act became compliant and
needed less assistance, and Protiviti was challenged to replace a significant
part of those lost revenues. In recent years, it has done an exceptional job
of achieving that objective. Protiviti continues to be a leader in providing
internal audit and internal controls-related activities and now also provides
consulting services in areas such as finance, technology, operations,
governance and risk.
Protivitis non-U.S. operations were pressured last year by unsettled
European economic conditions, stemming in part from the lingering debt
crisis on the Continent. Economic weakness led to uneven demand,
which, in turn, was reflected in price compression in select markets. In the 
Asia-Pacific region, consulting billings were limited by persistent economic
uncertainty in Japan and slower growth in China.
Protiviti operates in 73 locations in 23 countries. This total includes
company-owned offices and independently owned member firms in Europe, The Middle East, Asia-Pacific and the Americas.

Our Service and Our Brands
Over the years, we have strived to develop a reputation for providing
unparalleled service to our clients and job candidates. In 2012, we made it
faster and easier for job candidates to register with us online. We also introduced
remote skills evaluation and increased our use of video interviews of
prospective job candidates to make the process more convenient. Clients
benefitted from these efforts because candidates with the right skills were
presented to them more rapidly.

We believe our brands are among the most visible in the staffing industry.
Last year, we continued to make investments in marketing and public
relations to further strengthen our brand presence. The popularity of our
Salary Guides has increased, and we now publish them around the globe in
six languages. Each of our staffing divisions makes Salary Calculators avail-
able as mobile applications. Our various brand names have been featured
tens of thousands of times in media outlets that reached millions of readers,
viewers, listeners and website visitors. And the number of followers of our
multilanguage corporate social media sites doubled in 2012.
Blogs, online networks and other popular social media vehicles have
fundamentally changed how people communicate and share information.
Last year, we continued to invest in ways to use online channels and social
media networks to build awareness of our brands and to improve service

levels. Social networking tools have become increasingly important to
help us do what we do. But we are equally committed to providing the
personal service and know-how that are the hallmarks of our skilled staffing
and recruitment professionals.

Clients who believe they can use the internet alone to meet their staffing
needs often become open to using our services once they recognize that
web searches are simply a starting point. Robert Half professionals have
the expertise it takes to complete the multiple steps in the recruiting
process: personal interviews, technical skills evaluations, reference checks,
soft skills assessment and, critically, assessing the fit with an employer's
work environment.

Looking Ahead
Robert Half is positioned to grow. Our optimism about the future is based
on underlying economic and business trends, as well as the unique
strengths of our company. Global staffing is a huge industry: Annual world-
wide revenues are estimated at $400 billion. Moreover, the industry is
growing. In the United States, the Bureau of Labor Statistics reports that
employment services will be among the 20 industries adding the most jobs
between 2010 and 2020.
The industry growth we are already seeing in this cycle suggests that
at least some employers are using professional temporary workers for the
first time. As the benefits of using flexible staffing become visible to the
newly initiated, we expect that temporary help could have a bigger role
in companies' future staffing strategies.

We occupy an enviable place within the industry. Shortages of highly skilled
professional workers persist despite lingering high general unemployment
rates. Many of the shortages are in fields in which we are the leader in 
sourcing and placing candidates. Our global networks provide us with a
competitive advantage. We enjoy a well-established reputation for helping
clients locate talent that is proving so elusive this cycle.
Our target market of small and midsize companies is hiring. We believe
skilled temporary professionals are an enduring part of the human resources
mix for an increasing number of companies. Employing variable-cost labor
gives companies greater control over their personnel budgets and provides
them with access to skilled talent on demand. We believe there is opportunity
for the U.S. temporary worker penetration rate to increase further based on
recurring demand for staffing flexibility.

we are optimistic about the promise of the technology staffing market and
its implications for Robert Half. Companies, including small and midsize
businesses, are under pressure to keep pace with technology innovation in
order to stay competitive. Flexible staffing can be an economical solution in these situations. 
in these situations.

Ongoing regulatory changes in the financial services industry and the 
implementation of new healthcare legislation in the United States could be
a source of demand for our staffing services and for Protiviti. The Patient
Protection and Affordable Care Act may provide Robert Half with business
opportunities as employers evaluate staffing decisions in the new healthcare
environment. A flexible staffing approach may help reduce the cost of
compliance for certain businesses.

Robert Half differentiates itself in the staffing industry by our ability to
combine the services of our staffing divisions with the consulting capabili-
ties of Protiviti. Offering blended solutions means we can provide high-value
services more cost-effectively than our competitors.
Our accomplishments in 2012 are the result of the hard work and dedication
of our single greatest asset: our people. We believe we have the best
professionals and the most experienced management teams in our industry.
We thank our employees for their contributions last year and for the passion
they bring to their work every day.
We also would like to convey our gratitude to the members of our board
of directors for the counsel, encouragement and guidance they provide.
And, of course, our appreciation extends to you, our stockholders, for your
continued confidence and support
Respectfully submitted,

Harold M. Messmer, Jr.
Chairman and
Chief Executive Officer

M. Keith Waddell
Vice Chairman, President
and Chief Financial Officer



