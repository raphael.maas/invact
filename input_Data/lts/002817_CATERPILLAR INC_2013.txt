Chairmans Message
In uncertain times, it is the shared strengths and resolve of a community that determine the course of history. This is true
whether it be a family, a country or a company. And for us, the people of Caterpillar, the strengths we share  our common
ground  remain Our Values in Action.
Certainly, well all remember 2013 as a challenging year. But it was a
time of solid accomplishments as well, and right at the top of the list is
the performance by our people resulting in the strongest balance sheet
weve had in more than 25 years. Despite a sales and revenues decline
of about $10 billion, we set a record for Machinery and Power Systems
(M&PS) operating cash flow, strengthened our balance sheet and
improved our overall market position for machines, including strong
gains for excavators in China  all outstanding successes. But getting
here wasnt fun. And it wasnt easy, especially for our employees who
endured an incredibly tough year.
The good news is our performance in 2013 demonstrated the balance in
our business that helped us withstand divergent economic cycles. In a
year overshadowed by a substantial decline in sales of relatively high
margin mining products, our largest segment, Power Systems, delivered
profit near its 2012 record despite lower sales. In addition, our Financial
Products segment achieved a record profit. So while we saw a top
line sales decline in 2013  and nobody likes that, especially me  the
performance of our people and the balance in our business helped us
reduce the profit impact of lower sales.
So what does our 2013 performance mean? Well, a strong balance
sheet means we can weather a storm and grow the company, and of
course it affords returns to shareholders. We recently announced a new
$10 billion stock repurchase program  an indicator of our confidence in
the long-term future of Caterpillar. Our increased market share around
the world results in thousands more machines in the field, affording our
dealer network the opportunity to sell more parts and service. Thats
a tremendous accomplishment.

From an operational perspective, we have continued to make
improvements in safety and quality. This is something Im particularly
proud of. Weve been on a safety journey for nearly 15 years now. Since
2001, weve moved from the bottom in the rankings of safe places to
work, to being a global leader in safety. We remain vigilant about safety
and our ultimate goal is zero injuries. Likewise, quality is absolutely
essential to living our brand promise. Our products are built to last, and
through our disciplined execution of Lean and leveraging our Caterpillar
Production System (CPS), we are holding ourselves accountable to that
standard. Its working. Nearly every day, I hear from dealers and customers
who say we have the best product line weve had in decades.
Of course, with these successes comes responsibility. Im very
pleased to announce that in early 2014, we added Sustainability
to Our Values in Action, clearly stating our responsibility to produce
sustainable solutions. Raising sustainability to a stand-alone value
acknowledges both what we have done in the past and will do in
the future.
For instance, our new Tier 4 designs promise customers better fuel
efficiency and productivity, allowing them to leave a smaller environmental
footprint. Its important to the world, its important to our customers
and its important to us. We think about it every day. Another
example is our Reman  remanufactured parts  business, where we
return end-of-life components to like-new condition. This business
reduces waste in landfills and minimizes the need for raw materials to
produce new parts by keeping nonrenewable resources in circulation
longer. At the same time, remanufacturing allows us to provide costeffective
options for our customers.
Our commitment to sustainability extends beyond helping protect the
environment. We also recognize an obligation to our people and the
communities where we live and work. This isnt something new. The
Caterpillar Foundation has been around since 1952, and has invested
more than $550 million to provide support for not only environmental
sustainability, but also access to education, basic human needs and
emergency relief. Its a legacy we can all be proud of.
What will 2014 hold for Caterpillar? We see some signs of improvement in
the world economy and anticipate 2014 sales and revenues will be
similar to 2013. Although theres a lot of uncertainty in the industries
we serve, especially mining, I feel optimistic. Weve made good progress
over the past few years in our operational performance  safety, quality,
market share and costs  and Im confident that we are well-positioned
for much better financial results when economic conditions and the key
industries we serve improve. Despite the challenges that may lay ahead,
the successes we achieved in 2013 and the commitment of our people
will serve us well in 2014 and beyond.
Doug Oberhelman
Caterpillar Inc. Chairman & CEO