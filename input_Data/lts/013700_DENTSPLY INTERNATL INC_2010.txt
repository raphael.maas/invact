            


dear fellow
     
shareholders
DENTSPLY set a record for sales in 2010, reflecting what we
believe is our advantageous position in an attractive market.
With a diversified business model and strong balance sheet,
DENTSPLY stands prepared to take advantage of renewed
growth as the global dental industry
rebounds from the difficult recession
of 2008 and 2009.



While it is clear that the economy
                 and our industry have begun to
                 recover and are showing signs of
                 growth, the pace of recovery for
dentistry will vary across the different segments
of oral health care and across geographies.
Our breadth of solutions and strong global
presence should serve to moderate volatility
and allow us to return to consistent growth in
sales and earnings. As such, we believe we are
in an outstanding position to continue creating
solutions, opportunities and value, as presented
in this annual report.
  DENTSPLY's revenue grew 2.6 percent (constant
currency) in 2010, slightly faster than the growth we
estimated for the global dental consumable market.
Our above-market growth rate was driven by global
share gains in key product categories, most notably in
implants, endodontics and restoratives.





                                                                                                                          improved solutions for oral health drives us to introduce a
               We exited the recession in a very strong financial position,
                                                                                                                          stream of 25 to 30 new products and solutions each year,
            with $540 million in cash and short-term investments
                                                                                                                          building upon our reputation as a leading innovator in the
            today  even after completing numerous acquisitions in
                                                                                                                          global dental market.
            2010 and repurchasing 6.7 million shares of our stock for
                                                                                                                             Because of the investments made over the past few
            $224 million. Our priorities for cash deployment remain
                                                                                                                          years, we currently have an unusually robust new-
            focused on growing the Company, organically and through
                                                                                                                          product pipeline that we expect to deliver an even higher
            acquisitions, while also driving additional returns to
                                                                                                                          level of innovation than our historical norm. The pace
            shareholders by reinvesting in additional share repurchases
                                                                                                                          will be especially brisk in the first half of 2011, with a
            and increasing our dividend in line with earnings growth.
                                                                                                                          number of launches coinciding with our participation
               The market turnaround for dental consumables
                                                                                                                          in major industry events such as the Midwinter Dental
            has been slow compared with many other industries,
                                                                                                                          Meeting in Chicago, Illinois, and the 34th International
            reaffirming our belief that dentistry serves as a lagging
                                                                                                                          Dental Show in Cologne, Germany.
            indicator of the general economy but should accelerate
                                                                                                                             Several innovative products have already been launched,
            past the growth rate of the overall economy as the
                                                                                                                          including WaveOneTM, a new endodontic reciprocating
            recovery continues. On that basis, we anticipate a
                                                                                                                          system that simulates hand-operated file motion, introduced
            gradual improvement in our markets and expect
                                                                                                                          in February. Our endodontics franchise also recently
            continued above-market growth driven by our strong
                                                                                                                          launched GuttaCoreTM, a new obturator that extends our
            commitment to research and development that was
                                                                                                                          strong Thermafil line, and QmixTM, a novel dual-action
            maintained throughout the 2008-09 recession. The
            substantial pipeline of innovative product solutions we                                                       irrigant. In the restorative area, the patent-pending
            are launching in early 2011 is a direct result of our                                                         ChemFilTM Rock, an innovative zinc ion-reinforced glass
            continued investment in research and development                                                              ionomer featuring superior fracture and wear resistance,
            targeting better dental techniques.                                                                           will provide a proprietary entry into this restorative category.
               As DENTSPLY strives to exceed the needs of clinicians                                                      Among important new products in other franchises are the
            and patients alike, our Global Innovation Process,                                                            SlimLINE1000 ultrasonic inserts (preventive) and the
            described later in this report, fundamentally shapes the                                                      True Expressions tooth line (prosthetics).
            successful development of product and service solutions                                                          Our ability to be a trusted resource for dental
            from discovery to launch. Our commitment to creating                                                          professionals is also supported by our 2,800-member




                            



sales organization, which regularly comes face-to-                      Creating opportunity for development and career
face with those who use our solutions. Despite the                    growth for our Associates also contributes significantly
pressures of the past several years, we maintained our                to DENTSPLY's longstanding success. Our positive
global sales resources with an emphasis on specialty                  performance in 2010 was a direct result of the hard
products and emerging markets that offer the greatest                 work of our more than 9,700 Associates throughout
opportunities. We are committed to our continued                      the world. DENTSPLY offers great potential for team
investment in incremental sales representation of our                 members interested
products, as this has proven to be an effective growth                in advancing their
tactic for the Company.                                               careers. More than 60                            
   Developing business opportunities remains an                       percent of available                            

important component of DENTSPLY's strategy and a                      business leadership
priority for cash deployment. Recent additions such                   positions in 2010
as Italian-based Zhermack and Belgian-based ES                        were filled by internal
Healthcare have filled important strategic niches.                    candidates. Our global
The approximately 20 acquisitions made since 2007                     leadership team
added about $200 million, or 10 percent, to our 2010                  continues to work
revenue. We successfully completed six acquisitions                   together to identify
and other business development transactions during                    new ways to share
the past year and continue to analyze opportunities to                our collective talent
further leverage DENTSPLY's strengths by investing                    across the entire
in promising companies with strong management as                      organization and help
well as high-quality solutions and technologies that our              individuals reach their full potential. We commend our
customers value.                                                      team members' dedication to our mission of improving
   We actively explore unique and innovative partnership              oral health throughout the world and thank them for
opportunities. In December, for example, we invested in               contributing to DENTSPLY's many achievements.
a strategic partnership with South Korean-based dental                  DENTSPLY's Board of Directors continues to provide
implant manufacturer DIO Corporation. We are very                     important guidance, strong governance and the relevant
excited about the potential growth opportunities this                 expertise to strengthen and support the strategy we
investment provides both DENTSPLY and DIO in a number                 are pursuing. During the past year we were pleased to
of countries, particularly in emerging markets. In January            welcome a new member to the Board, Mr. John Miclot,
2011, we entered into an agreement with GlaxoSmithKline               who brings important perspective from his executive, sales,
(GSK) to co-brand our NUPRO products with GSK's                      and marketing background in the health care industry.
                                                                      Most recently, Mr. Willie Deese was elected to our Board
Sensodyne brand, providing a continuum of care for tooth
                                                                      in February. We look forward to learning from his deep
sensitivity from the dental office to everyday use at home
                                                                      knowledge of global manufacturing and operations.
by patients. We believe that these opportunities, along with
                                                                        Finally, we thank you, our investors, for your
our substantial pipeline of new products, provide a solid
                                                                      continued support of the solutions, opportunities
platform to gain market share in 2011.
                                                                      and value we are creating at DENTSPLY. We remain
   One major unforeseen challenge we are facing in 2011 is
                                                                      confident in our ability to deliver on our strategy by
the negative impact of the major earthquake and tsunami
                                                                      taking full advantage of the global dental industry's
in Japan, and the aftermath of the destruction created by
                                                                      recovery in 2011 and the coming years.
these events. Fortunately, our team members are safe and
our DENTSPLY facilities sustained only minor damage.
However, certain key suppliers are facing considerable
challenges in the aftermath of this tragedy. The full impact
of these events on our supply chain and the Japanese dental
market remains unclear at the date of this letter; it is likely       Bret W. Wise                                     Christopher T. Clark
                                                                      Chairman and                                             President and
that there will be negative effects on our sales and earnings.
                                                                      Chief Executive Officer                          Chief Operating Officer
We believe that DENTSPLY's broad geographic footprint
and large product portfolio work to our advantage to soften
the financial effect of these types of unforeseen events. This
                                                                      April 21, 2011
is a time where the dedication and creativity of our team
are particularly valuable.

