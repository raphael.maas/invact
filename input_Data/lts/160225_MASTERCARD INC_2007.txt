Dear Fellow Shareholders:

Whenever I ask customers what they
look for in choosing a business partner,
the answer has little to do with size and
much to do with value. At MasterCard,
we deliver that value through our people.
Its about the knowledge and insights we
bring to the table, our unique and powerful
consulting capabilities and our dedicated
account teams delivering customized
solutions  all of this together.
Our success is driven by the roles we play at The Heart of
Commerce  as visionaries, advisors, partners, advocates,
ambassadors, trailblazers and, above all, as catalysts for
change and growth. With the world accelerating toward
electronic payments, our employees deliver more value
than ever: driving sales and efficiencies for merchants,
convenience and satisfaction for consumers, better control
and compliance for businesses and, of course, growth
opportunities for our customer financial institutions.
Clearly, our employees drove our performance in 2007.
The Value We Deliver Shows in Our Results
In 2007, MasterCard revenue was $4.1 billion, a
22.3 percent increase from $3.3 billion in the previous
year. We generated net income of $1.0 billion for the year,
or $7.58 per share, on a diluted basis, compared with
$457 million, or $3.37 per share, in 20061. Our 2007
results include after-tax gains of $254 million, or $1.87
per share on a diluted basis, from sales of the companys
investment in Redecard.
In terms of our operating performance, we generated
worldwide gross dollar volume (GDV) of $2.3 trillion, up
14.4 percent in local currency terms over 2006. Also in
2007, worldwide purchase volume rose 15.7 percent in
local currency terms, to $1.7 trillion, driven by increased
cardholder spending on a growing number of our cards.
At the end of the year, our customer financial institutions
had issued 916 million MasterCard cards, a 12.6 percent
increase over cards that had been issued at the end of
2006. In addition, the Maestro brand mark appeared
on approximately 653 million cards worldwide at the end
of 2007. Meanwhile, transactions processed across the
MasterCard network reached 18.7 billion during 2007
an increase of 16.2 percent from the previous year.
Of particular note was the extent to which overall growth
continued to be driven by rapidly expanding acceptance and
usage outside the United States. For the first time, just over
half of our revenue came from non-U.S. markets as we
accelerated the global expansion of electronic payments.
Our Strong Relationships Drive Business
At the heart of our strategy is our commitment to build
deep relationships with our customers. MasterCard account
teams are comprised of subject-matter experts in consulting,
marketing, processing, product and technology. The
composition of each team is tailored to the specific needs
of each customer, with a goal to drive profitable growth
for customers and, in turn, for MasterCard.
At the same time, MasterCard is fostering close relationships
with merchants, whether by increasing efficiency, devising
rewards programs or developing co-branded alliances.

Of course, most of our value to merchant partners is
the simple fact that MasterCard connects them to millions
of cardholders worldwide who use our cards wherever
they go and for nearly anything they buy. Consider our
sponsorship of the Great Singapore Sale, one of Asias
most popular destination shopping events. As the official
card, MasterCard adds energy and impetus by promoting
the event to cardholders across the region and around
the world. The result in 2007: a nearly 50 percent jump
in spending by MasterCard cardholders.
The vast majority of payments are still made with cash
and checks, representing a tremendous opportunity for
MasterCard and our customers. To embrace this opportunity,
we continue to extend our presence throughout the
world, whether by increasing acceptance in fast-growing
markets, opening new acceptance channels such as taxi
cabs and vending machines, or pioneering next-generation
payment methods.
In Brazil, for example, our 100% Municpios program,
designed to expand acceptance in remote areas, has
enabled merchants in nearly 450 additional cities and towns
to accept payments with credit or debit MasterCard cards,
making our acceptance footprint unrivaled in the country.
Around the world, MasterCard cardholders shop, travel
and make payments with the confidence that theyll find
acceptance at more than 25 million locations.
MasterCard also is a driving force in protecting the integrity
of the global payments environment. Our efforts include
promoting wide adoption of data protection standards,
delivering advanced fraud management solutions and
educating stakeholders through forums. One example is
the fifth annual MasterCard Worldwide Global Risk
Management Symposium, which drew industry thought
leaders and risk professionals from banks, security vendors
and law enforcement.
Our Global Structure Enables Local Solutions
At MasterCard, were proud of our unified, global
organization, which makes us a valuable ally to customers
wherever they do business. This structure enhances our
ability to provide insights on regional markets, use global
assets to capture local opportunities and transfer best
practices and proven programs from one region to another.
Importantly, our unique global structure makes it easier
to leverage powerful assets such as our technology
and brands.
Through our advanced global processing network,
we process transactions in approximately 210 countries
and territories and in more than 160 currencies. Our
authorization network is designed to automatically adapt
to the individual needs of each transaction, blending the
speed and redundancy of peer-to-peer networking with
the real-time availability of value-added services provided
from a central site. We believe our system is unique,
offering distinct advantages over networks deployed by
our competitors and enabling us to provide our customers
around the world with quality, scalable processing that is
consistently reliable and secure.
Equally important, our global structure also allows us to
best meet the demands of growing cross-border commerce.
In this regard, MasterCard has been particularly active in

Brand Power
MasterCard brands help drive growth for customers
and merchant partners worldwide, whether through
co-branding opportunities, targeted rewards or
sponsorship of global destination events. In 2007,
our Priceless campaign continued to power brand
recognition everywhere. By the end of the year, it
had run in 51 languages and 110 countries, bolstering
its reputation as one of the most successful  and
influential  campaigns in marketing history. In fact,
it was awarded the 2007 Gold Effie Award for
Sustained Success.
Europe, where countries are creating a uniform cardholder
experience across SEPA, the Single Euro Payments Area.
MasterCard created a solution that combines the best
features of national schemes with the most advanced global
payments technology, and it continues to be well received
by customers as they strive to achieve SEPA compliance.
At this stage, customers throughout Europe are in the
process of migrating to Maestro, our global online debit
program. In addition, domestic transactions on all cards
issued with the Maestro logo can now be routed through
our network should an acquirer or merchant choose to do
so. As a result, we currently process some of the domestic
transactions on more than 140 million cards bearing both
the domestic and Maestro brands across the region.
At the same time, our global structure makes it easier to
capitalize on the power of our brands and sponsorship
properties. To this end, we work with our business partners
to develop programs that drive preference and usage in
markets around the world. In 2007, we marked the tenth
anniversary of the MasterCard Priceless advertising
platform, one of the largest and most successful brandbuilding
programs of all time. Having appeared in
51 languages and 110 countries, Priceless continues
to resonate with consumers while delivering value for
customers and merchant partners alike.

Our Innovations Empower Savvy
Consumers and Businesses Everywhere
As we expand geographically, we capture transactions
through innovative new payment forms, chief among
them MasterCard PayPass, the worlds leading Tap & Go
quick-pay solution. Whether in the form of a card, key fob,
bracelet, wristwatch or enabled in a mobile phone, PayPass
is being embraced in markets worldwide.
PayPass is a strong example of how we are leading the
payments revolution. Our organizations unified global
structure enables us to swiftly adapt successes in one region
to meet the emerging needs of others. As an illustration, in
2007 we launched Europes first co-branded PayPass program
in France with Banque Accord and prominent retailer Auchan.
During the year we also advanced mobile phone payment
technology to enable consumers to make purchases, pay
bills online and manage their accounts. Working with
telecom companies on six continents, we continue to
build new payments platforms for consumers on the go.
More than ever, were using our insights to help issuers
develop customized programs tailored to appeal to specific
geographic, demographic and economic markets.
Cardholders around the world are enjoying the benefits of
our premium card programs. In total, our issuers launched
more than 125 affluent card programs in 2007  each
crafted to appeal to a specific segment or region. For
example, in Indonesia, luxury comes in the form of the ANZ
Black World MasterCard card, tailored to appeal to the
countrys entrepreneurial elite.
In the United States, we launched the Unique Experiences
program for World MasterCard and World Elite cardholders,
delivering access to the best shops, spas and destinations
and the best seats in the house through an exclusive,
easy-to-use online reservation system. In addition, art lovers
can now follow their passion with the Sothebys World and
World Elite MasterCard cards, which offer exclusive access
to events, exhibitions and artists studios, and the ability to
apply rewards points to support their favorite museums.
The year 2007 also saw the introduction of Product
Graduation, a true account for life solution. This patentpending
innovation conveniently enables cardholders to
retain their 16-digit account numbers as they migrate within
certain consumer and commercial card programs.
Our innovative solutions also extend to business
cardholders  from small business owners to executives
of major corporations. In 2007, we launched the World
MasterCard for Business program in both the United States
and Singapore, offering the exclusive service and premium

rewards that entrepreneurs and executives seek. In the
United States, we also launched the MasterCard Easy
Savings program for small business, delivering automatic
savings at car rental agencies, restaurants, airlines and hotels,
as well as on printing, shipping and business services. Our
research shows that MasterCard Easy Savings is exactly what
small business owners want  a simple way to save on their
business expenditures. For customers, its another way to
build activation, loyalty and usage, which is why more than
30 major financial institutions have signed on. At the same
time, the program offers valuable benefits for merchants by
attracting new and sought-after small business customers.
Increasingly, companies conduct business globally and
need solutions that provide a comprehensive view of their
business expenses. The MasterCard Multinational Corporate
Program leverages our industry-leading, global data network
for this purpose, allowing companies to manage and
optimize expenses while increasing control and compliance.
With MasterCard smartdata.gen2, we are also helping
businesses of all sizes control costs, drive profits and
improve compliance. This proprietary, Web-based expense
and information management solution provides businesses
with integrated, global expense spending analysis,
unmatched flexibility and multiple robust reporting
options so they can better manage their expenditures.
Another important development was the introduction of
MasterCard inControl. This platform empowers issuers to
offer a wider set of customized products  emphasizing
advanced transaction controls  for consumers, businesses,
corporations and government agencies. In this way,
MasterCard inControl expands the value proposition that
MasterCard provides to issuers.
Throughout 2007, we moved forward with many other
initiatives, including programs to address significant
opportunities in healthcare, consumer bill payment and
expanded use of prepaid cards by governments, businesses
and consumers. By replacing paper-based forms of payment
with more efficient and secure solutions, our innovations
continue to revolutionize the way commerce is conducted
around the world.
Our Knowledge and Insights
Spotlight Trends and Opportunities
We believe that in the last decade we have worked
harder  and smarter  than any competitor to leverage
a wealth of intellectual capital gained through the billions
of transactions processed. Our ability to apply market
intelligence on a customers behalf  or deliver insights
through widely followed studies, publications and reports 
is a clear differentiator for our organization.

Last year, for instance, we proudly launched a groundbreaking
research project, the MasterCard Worldwide Centers of
Commerce program. This unique initiative brought
together an international panel of experts who delivered
insights into the growing importance of urban centers and
their influence on the global economy. An important
component of the program is the Worldwide Centers of
Commerce Index, which ranks and tracks the top 50 cities
driving global commerce. This was accompanied by the
Dynamics of Global Cities and Global Commerce report,
which provides in-depth commentary on the forces that link
and shape commerce around the world. Our research and
insights are available to all of our customers and are helping
many of them shape their growing businesses.
Beyond our reports and research initiatives, our wealth
of knowledge and expertise is also delivered through
MasterCard Advisors, the only global professional services
organization in the payments industry. For MasterCard,
this group has become a strong and growing differentiator,
helping customers worldwide build increasingly profitable
payment programs. Clients include some of the most
important financial institutions in the world, from Citi
and JPMorgan Chase to The Royal Bank of Scotland and
the Bank of China. This market acceptance validates our
strategy of building deeper relationships with customers by
combining knowledge and insights with our most
innovative, profitable payments solutions.
In addition to offering consulting and information services,
MasterCard Advisors provides industry-leading rewards
services and card benefits and handles more than 3.5 million
service calls annually on behalf of customers.
MasterCard Advisors also continued to publish
SpendingPulse, 1 a subscription-based U.S. consumer
spending report that reflects aggregate sales activity in
the MasterCard payments network coupled with estimates
for all other payment forms such as cash and checks. In
2007, SpendingPulse was expanded to include reporting
of U.S. gasoline demand and U.K. retail spending. This data
provides key intelligence on consumer spending  ahead of
the release of government figures  and is closely followed
by issuers, merchants, analysts, the media and others.
Finally, in a major effort to provide our customers with
direct actionable insights, MasterCard Advisors rolled out
Comparative Cardholder Dynamics. Drawing on opinions
from more than 50,000 U.S. consumers, this study provides
our customers with unique perspectives on consumer
attitudes to better understand payment trends and areas
of opportunity. These insights enable our customers to
benchmark against competitors and develop successful
marketplace strategies.
Our Dynamic People Lead at All Levels
Underlying our accomplishments is the talent and drive of
our people, all of whom contributed to our success in 2007.
In an age of global commerce, having a diverse and inclusive
workforce is a key business driver and continues to be
among our greatest priorities. As a company, we can lead
the way only if our people lead at all levels.
Thats why we continue to invest in programs that foster
leadership, teamwork, diversity and innovation across the
organization. In fact, MasterCard was again named to
the Training Top 125 list by Training magazine, which
recognizes the best global companies for employersponsored
training and development.

In 2007, we rotated a number of our top executives to
new positions, with the aim of broadening their expertise
by increasing cross-functional and international exposure.
In addition, we are committed to having a positive impact
on a world that is increasingly interconnected and driven by
global economic forces. This means, for example, creating
consumer education programs that increase financial
management skills to enable current and future cardholders
to use debit and credit wisely.
It also means standing squarely behind a range of broader
educational and philanthropic initiatives, whether strongly
supporting academic scholarship funds for minority
students in the United States or working with Junior
Achievement to fund the JA Global Marketplace program,
to help students in 22 countries understand the impact of
international trade and the global economy. Employees
around the world regularly volunteer their time and efforts
for a broad range of causes, giving back to the communities
in which we do business.
Guiding our organization is a dynamic Board of Directors
with broad functional experience. I thank Norman C.
McLuskie and Manoel Luiz Ferro de Amorim, both of
whom left the Board in 2007. We are deeply appreciative
of their service. We also welcome two new Board members,
Silvio Barzi, Executive Vice President of UniCredit Group
in Italy, and Jos Octavio Reyes Lagunes, President,
Latin America Group, The Coca-Cola Company. These
individuals not only bring strong expertise but also add
further dimension to a Board that is already rich in
international representation.
We Are Committed to
Advancing Commerce Globally
The last few years have been nothing short of
transformational for our company. Weve expanded
worldwide, grown our brands and consulting capabilities,
strengthened our global processing platform and developed
more innovative products and solutions. Notably, our 2006
initial public offering was a defining event for the payments
industry. In the course of these achievements, weve made it
clear that MasterCard is a unique player in the market and a
valuable ally to customers and merchant partners worldwide.
We intend to capitalize on the advantages that set us
apart, enabling us to combine the power of our brands,
technology and insights to make commerce faster, more
secure and more valuable for everyone.
In addition, we will find new ways to broaden and
deepen relationships, helping customers optimize portfolios
and enter new markets while delivering greater value
to merchants.
Increasingly, customers and merchants look to us to drive
opportunities for growth, while consumers seek simpler,
safer, more convenient ways to pay. As a true catalyst at
the heart of commerce, MasterCard is leading the way.
Robert W. Selander
President and Chief Executive Officer
Purchase, New York, U.S.A.
April 2008