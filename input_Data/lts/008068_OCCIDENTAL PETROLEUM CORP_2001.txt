Report to
Stockholders
2001 was an outstanding year for Occidental, in terms of
both financial performance and positioning the Company
for greater profitability. Occidental achieved near-record
earnings and the lowest total debt level since 1985.
Earnings after domestic and foreign taxes, and before
special items, were $1.328 billion, or $3.57 per share,
just 3 cents per share less than our record earnings in
2000. Net income was $1.2 billion ($3.10 per share), the
second highest in Occidentals history. Sales increased
to $14.0 billion in 2001 from $13.6 billion in 2000.
We reduced our total debt by nearly $1.5 billion to $4.9
billion  a 23 percent reduction below the year-end 2000
level. This is more than 46 percent below the $9.2 billion in
total pro-forma debt following the $3.6 billion Altura Energy
acquisition in April 2000. Of the $1.5 billion reduction
in 2001, approximately $750 million came from after-tax
proceeds from the sale of non-strategic assets and
the remainder from free cash flow.We also increased
stockholders equity by $860 million, or 18 percent. The
combination of lower debt and increased equity reduced
our debt-to-capitalization ratio from 57 percent at the
end of 2000 to 46 percent at year-end 2001, the lowest
level in nearly two decades. These improvements in our
balance sheet led Moodys Investors Service and Standard
and Poors to upgrade Occidentals senior unsecured
debt ratings.
Our interest expense was reduced to $423 million in 2001.
Based on our total debt at the end of 2001, we expect
our interest expense to be approximately $350 million in
2002. That represents savings of $209 million compared
to interest expense of $554 million in 2000 and equates
to an annual improvement of 36 cents per share in our
bottom line.
Capital expenditures in 2001 were $1.4 billion  47 percent
higher than our 2000 level of $952 million. Our investment
emphasis clearly lies with our oil and gas business, which
received the vast majority of the funding  $1.2 billion.
We expect our 2002 capital expenditures to be at or
below $1.1 billion, with $1 billion allocated to oil and
gas operations.
Since 1997, we spent more than $7 billion on oil and
gas acquisitions, funded a strong capital program, paid
an annual dividend of $1 per share and were able to
reduce our total debt by almost $1.5 billion.
In 2001, oil and gas earnings before special items were
$2.44 billion  the highest in Company history  exceeding
2000 earnings of $2.40 billion. In other key oil and gas
highlights, Occidental:
 Increased worldwide oil and gas production to 174 million
barrels of oil equivalent (BOE), or 3 percent above 2000
production levels
 Replaced 138 percent of worldwide oil and gas production,
excluding purchases, at a cost of $4.54 per BOE
 Ranked first among industry competitors in operating
profits per barrel for the third consecutive year
 Sold our interest in the Indonesian LNG project for an
excellent price
 Was selected to participate in Saudi Arabias historic
Natural Gas Initiative.
Production
Worldwide oil and gas production in 2001 averaged
476,000 BOE per day net to Occidental, up 3 percent from
461,000 BOE in 2000. In 2001, production from our U.S.
operations comprised nearly 60 percent of our worldwide
oil production and 92 percent of our worldwide natural
gas production.
Dr. Ray R. Irani
Chairman and Chief Executive Officer
Proved Reserves
At year-end 2001, our proved reserves were 2.24 billion
BOE, an increase of 3 percent over the year-end 2000
level of 2.17 billion barrels and a 70 percent increase
above the 1997 level when our restructuring program
was initiated. Of the 2.24 billion BOE of reserves, 85
percent is oil and condensate and 15 percent is natural
gas. Our U.S. assets comprise 95 percent of our natural
gas reserves and 72 percent of our worldwide oil reserves.
Reserve Additions
Proved oil and gas reserve additions, excluding acquisitions,
replaced 138 percent of our 2001 combined
worldwide oil and gas production. Our finding and
development costs were $4.54 per BOE in 2001, excluding
acquisitions. Including acquisitions, the replacement rate
was 141 percent at an average finding, development and
acquisition cost of $4.80 per BOE.
Approximately 48 percent of Occidentals 2001 worldwide
reserve additions came from U.S. operations, replacing
103 percent of U.S. production of nearly 115 million BOE.
In addition, we replaced 215 percent of our international
production with operations in Qatar, Oman and Ecuador
accounting for the majority of the new reserve additions.
For the three-year period, 1999-2001, Occidentals average
annual reserve replacement, excluding acquisitions, was
115 percent at an average finding and development cost
of $3.84 per BOE. Including acquisitions, the replacement
rate for the same period was 316 percent at an average
finding, development and acquisition cost of $3.90 per BOE.
Profitability
Based on our strong 2001 performance, Oxy has retained
its top industry ranking in operating profits per barrel for
the third straight year. This accomplishment demonstrates
the strength of our asset portfolio and the overall
efficiency of our operations and represents quantifiable
success in achieving our goal of being one of the most
profitable exploration and production companies on a
unit-of-production basis.
Prices
Prices for oil and natural gas started the year strong,
but both dropped considerably by year-end.West Texas
Intermediate (WTI) prices fell from around $30 per barrel
in January to less than $20 per barrel in December 
the weakest oil prices since the third quarter of 1999.
NYMEX domestic natural gas prices, which were as high
as $7 per thousand cubic feet (MCF) in the first quarter,
declined significantly to less than $3 per MCF by yearend.
The California gas premium  the price we received
for California production above the NYMEX price  added
more than $400 million to oil and gas earnings in the first
half, but disappeared by the fourth quarter.
United States
California
At Elk Hills, we accelerated natural gas production to
capture the benefits inherent in Californias high-price
environment in 2001. As a result, we achieved unprecedented
earnings during the first half of the year. Four
years after the Elk Hills acquisition, we have recouped
$2.4 billion or 69 percent of the $3.5 billion purchase
price. Operationally, we increased net production in
the field to an average of 99,000 BOE per day, up 3
percent from 2000 levels, through added compression
and other aggressive reservoir exploitation programs.
We are currently drilling deep-gas plays to build our
California base.
Permian Basin
Oxy Permian is another example of Occidentals success
in enhancing production from large, mature assets. 2001
was the first year Occidental derived a full years earnings
from the Altura Energy acquisition, completed in April
2000. In the first 20 months, we recouped $1.2 billion,
or one third of the $3.6 billion purchase price. Net
production of 162,000 BOE per day makes Occidental
the largest oil producer and second largest natural gas
producer in Texas.
Gulf of Mexico
Development of Occidentals deepwater Horn Mountain
project in the Gulf of Mexico is proceeding on schedule
and on budget. Gross proved reserves are more than 100
million BOE. First production is scheduled for November
2002 and peak production is expected by year-end 2003,
with Oxys share anticipated at approximately 21,000
BOE per day.
International
MIDDLE EAST
Continued growth in the Middle East is the focal point of
Occidentals growth strategy and 2001 marked a milestone
for our expansion in the region with our selection by the
Saudi Arabian government as a partner in the Kingdoms
historic Natural Gas Initiative. Operationally, Occidental
continues to maximize production from its operations
and is pursuing exploration prospects in Qatar, Oman
and Yemen.
Saudi Arabia
In Saudi Arabia, Occidental has a 20 percent interest in
the Core Venture Two consortium, which will be investing
in the Red Sea region to develop the Kingdoms natural
gas reserves for the rapidly growing domestic market.
Together with its partners, Occidental will develop gas
from existing discoveries in the northwest region of Saudi
Arabia and build related gas processing and pipeline
facilities. The consortium expects to build at least one
power plant and a water desalination unit, as well as
to evaluate the potential for a petrochemical plant. The
project also calls for onshore and offshore exploration
in 10 blocks located along the Red Sea coast. Saudi
officials have emphasized that participation in the Natural
Gas Initiative will open the door for other projects in the
Kingdom. Occidental expects that participation in the Red
Sea project will mean an attractive return on investment,
a stronger presence in one of our core areas and the
potential for new projects.
Qatar
In Qatar, Occidental operates the North and South Dome
fields where Oxys share of combined production averaged
approximately 43,000 barrels of oil per day in 2001. In the
North Dome, Occidental successfully implemented a
waterflood project that boosted oil reserves in the Shuaiba
Reservoir.We are currently pursuing Phase II of an
enhanced recovery project targeting recovery of 150
million barrels that is expected to add net incremental
production of 25,000 barrels per day. Occidental also is evaluating new exploration prospects in Qatar and we
are currently awaiting response on our bid submitted
for Block 10, which is adjacent to the billion-barrel Al
Shaheen field.
Yemen
In Yemen, Occidental holds a 38 percent interest in the
billion-barrel Masila field in Block 14. Masilas recoverable
reserves and production have increased steadily each
year since the fields discovery in 1993.We also hold a
29 percent interest in the East Shabwa field in Block 10.
The two fields have combined gross production of nearly
260,000 barrels of oil per day.We will be drilling a series
of step-out wells in Masila and East Shabwa to add
additional reserves.
Occidental holds one of the largest acreage positions in
Yemen, where we are pursuing an aggressive exploration
program.We have acquired interests in seven exploration
blocks, encompassing 15 million acres, which gives us
access to nearly 11 percent of the countrys land mass.
We are the operator of Block 44, which is on trend with
the prolific East Shabwa and Masila fields, as well as
Block 20 in central Yemen in the midst of a hydrocarbon province where more than a billion barrels of oil and
three trillion cubic feet of gas have been discovered. In
addition, we have acquired a 40 percent interest in five
blocks in a promising under-explored region along the
Saudi Arabian border.
Growth Opportunities
Occidental has established a business development office
in Dubai to pursue additional strategic opportunities in the
Middle East. In addition to our new development project
in Saudi Arabia, Occidental is one of five companies
competing for an interest in the Dolphin project in the
United Arab Emirates (UAE). The Dolphin project involves
delivering 3 billion cubic feet per day of natural gas from
Qatars massive North Field to markets in the UAE through
a sub-sea pipeline. This $3.5 billion project is scheduled
for start-up by 2005. Occidental was selected by the
governments of Kuwait and Syria to participate in
upcoming bid rounds for Enhanced Oil Recovery
(EOR) projects.
LATIN AMERICA
Ecuador
The most significant development for our Latin American
operations is the construction of the Oleoducto de Crudos
Pesados (OCP) trans-Ecuadorian pipeline. For years, our growth potential in Ecuador was hampered by pipeline
capacity restrictions. This new pipeline is allowing
Occidental to develop the 150 million barrel Eden-Yuturi
oil field discovered in 1997. Start-up of Eden Yuturi is
scheduled to coincide with the completion of the pipeline
in 2003. Development of this field is expected to add
30,000 barrels per day of net incremental production to
Oxy. In addition, Occidental is expanding its exploration
program in Block 15 and we are hopeful that an aggressive
3-D seismic program will lead to continued exploration
success in Ecuador.
Colombia
Sabotage of the Cao Limn pipeline by leftist guerillas
significantly disrupted our Colombian production last
year. When in operation, Colombia is one of our most
profitable operations on a unit-of-production basis and
costs are kept at an absolute minimum when the field is
down. Colombia accounts for less than 3 percent of our
worldwide proved reserves and less than 1 percent of
our total assets.
Chemicals
? OPERATIONS HIGHLIGHTS ?
Occidental Chemical Corporation (OxyChem) had a
difficult year with depressed markets and low commodity
chemical prices. Chemical earnings before special items
in 2001 were $41 million, compared to $293 million in
2000. Our continuing strategy is to harvest cash from the
chemicals division.
In order to reduce the volatility of our chemical earnings,
in January 2002 Occidental agreed in principle to sell
our 29.5 percent equity interest in Equistar to Lyondell
Chemical Company. Occidental will become the owner
of approximately 21 percent of Lyondell.We also will
obtain warrants to purchase an additional 5 million shares
at $25 per share and be entitled to a percentage of
Equistars cash distribution for 2002 and 2003, up to a
total of $35 million to be paid in cash or stock. These
transactions are expected to close in the second quarter
of 2002. Owning an interest in Lyondell will preserve
Occidentals economic benefit when the petrochemicals
market recovers while providing more stable returns
from Lyondells other chemical businesses.
Chemical volumes and pricing remain low.We have
taken aggressive actions to reduce manufacturing costs,
and selling and administrative expenses through idling
high cost or underutilized facilities, shifting production
to our lowest-cost plants and restructuring overhead
expenses. These actions have resulted in $55 million in
cost savings in 2001.
The commodity chemical business is especially sensitive
to fluctuations in the global economy. Conditions are the
worst we have ever seen. An improved economy will
trigger the increased demand that will lead to improved
chemicals performance, but while we wait out the
challenging market conditions, we will continue to focus
on asset rationalization and cost reduction throughout
our chemical operations.
Social Responsibility
? HEALTH, ENVIRONMENT & SAFETY ?
Occidental continues to demonstrate strong performance
in health, environment and safety (HES) programs. In
2001, we again achieved substantial success by reducing
the number of worker-related accidents and reportable
environmental occurrences making Occidental among
the best performers in its industry sectors. The 2001
recordable injury and illness incidence rate (IIR) of 0.69
injuries per 100 workers represents an improvement of
more than 77 percent over the past decade. Also, this is
the sixth consecutive year in which Occidentals IIR has
been less than one, placing the Company among the top
performers in all industries.
Consistent with our endorsement of the Global Sullivan
Principles, a recognized framework for corporate social
responsibility, we strive to achieve the long-term goals
of sustainable growth and development in our core
businesses. Strong programs for health, safety and environmental
protection demonstrate Occidentals commitment
to responsible management of these disciplines. In this
regard, Environmental Resources Management (ERM), a
well-respected environmental consulting firm, completed
a three-year review of the HES Audit and Assessment
programs of Occidental Petroleum and its operating
subsidiaries. Following the implementation of some
improvements suggested by the review, ERM expressed
the opinion that, Occidental has implemented an
effective Health, Environment, Safety and Process Risk
audit program that clearly ranks in the upper quartile of
companies in the chemical manufacturing and oil and
gas industries.
During the year, Occidental has received numerous awards
and achieved important milestones demonstrating the
success of our health, environment and safety programs.
A few of the highlights are: In 2001, Occidental of Oman
received two internationally recognized certifications 
International Organization for Standardization (ISO)
14001 for environmental management systems and
Occupational Health and Safety Accreditation System
(OHSAS) 18001 for health and safety management
systems. Worldwide, few companies have achieved
certification of both their environmental and safety
management systems, and very few have received
them concurrently. Dr. Mohammed bin Hamad Al Rumhy,
Omans Minister of Oil and Gas, congratulated Oxy
Oman on this accomplishment. We in the Sultanate are
proud that there are international companies such as
Occidental that are winning awards for the protection
of the environment. OHSAS 18001 was developed by
the British Standards Institute and is expected to be
convertible to ISO 18001 if that standard is finalized.
Additionally, our Ecuador operation received ISO 14001
recertification in 2001.
The Wildlife Habitat Council (WHC) renewed certification of
Occidentals conservation management program at its Elk
Hills operation in central Californias San Joaquin Valley.
The WHC is a highly respected non-profit organization
created in 1988 to protect and increase wildlife habitats
worldwide by developing its own environmental programs
and certifying other qualified programs. Occidentals
Elk Hills employees have remained committed to a
long-term program that provides and enhances wildlife
habitats, wrote WHC President William W. Howard in
announcing the certification.
OxyChems strong safety programs contribute to ongoing
recognition under the federal governments prestigious
OSHA Voluntary Protection Program (VPP). OxyChem
had eight facilities newly certified in 2001 and now has
19 OSHA Star work sites. Three plant sites also received
confirmation of recertification in 2001.
Summary
Occidental has made significant progress over the last
five years. As a result of our substantive restructuring,
we have a stronger, more geographically focused and
higher-quality asset base. By maintaining strict financial
discipline, we have reduced debt and strengthened the
balance sheet. While these changes contributed to our
strong performance in 2001, in the bigger picture they
are integral improvements that will provide for greater
profitability and less risk in the future. Consequently, we
are better positioned to capitalize on strategic opportunities
that will provide earnings growth and to prosper even
during low-price environments.
These changes were driven by our steadfast goal of
increasing shareholder value. We have made good
progress to date and we remain committed to earnings
growth, debt reduction and strategic new initiatives while
maintaining the highest standards of safety, environmental
and ethical performance.
Ray R. Irani
Chairman and Chief Executive Officer
Dale R. Laurance
President