TO OUR STOCKHOLDERS, MEMBERS, GOVERNMENT AND BUSINESS PARTNERS, AND ASSOCIATES:
By the end of 2019, our company will almost double its revenue in just four short years. This growth is made possible because of our more than 12,000 associates, who never lose sight of our mission, vision and values. WellCare is a rare find � one where we take pride in our meaningful work, place great emphasis on teamwork and produce excellent results.

By comparison to many FORTUNE 500 companies, we are a very young organization. Established in 1985, WellCare grew both organically and through acquisitions. However, our hallmark remains our unwavering focus on our members and our ability to help them live better, healthier lives.

Big Wins, Big Growth
This past year proved to be one of the most successful in WellCare�s history. We remained diversified across three lines of business including Medicaid, Medicare Advantage and Medicare Prescription Drug Plans (PDP), and our 2018 growth reflects our strategy to focus on government-sponsored health plans.

Last year, we were awarded a new five-year contract in Florida to provide managed care to Medicaid-eligible beneficiaries and provide long-term services and supports in 10 of 11 regions � an increase of two regions. Leveraging WellCare�s expertise in complex care, Florida also awarded the company a statewide contract to serve Serious Mental Illness specialty plan recipients. Separately, we were exclusively selected to provide managed care services covering approximately 60,000 children with complex physical and behavioral needs.

Also, last year, we welcomed Meridian to our WellCare family of companies. This acquisition grew our Medicaid membership by more than 40 percent, expanded our Medicare Advantage presence into new markets and added a proprietary pharmacy benefit management platform. Like WellCare, Meridian associates share a strong commitment to quality � and we�ve leveraged the best talent across our collective companies to improve quality for all members.

We also executed an agreement to acquire Aetna�s standalone Medicare Part D Prescription Drug plan business. We�ve already begun work to ensure a thoughtful transition of up to 2.4 million members at the beginning of 2020. Lastly, WellCare expanded our footprint in Arizona by opening a regional hub in Phoenix in conjunction with a new contract to provide physical and behavioral health services to Medicaid members in eight Arizona counties.

Our Business by the Numbers
WellCare�s commitment to double our revenue from $14 billion to $28 billion by 2021 is well within reach. At the end of 2018, we reported over $20 billion in total revenue and issued 2019 guidance of $26.3 billion at the midpoint.

In Medicaid, WellCare now holds the fourth largest position in the United States, serving nearly 4 million members across 13 states, while holding the number one market share in six of those states. WellCare�s Medicare Advantage plan is the sixth largest in the country with 545,000 members in 21 states. In addition, WellCare�s Medicare PDP is the fifth largest U.S. plan with 1.1 million members nationwide.

Our Opportunities and Differentiators
For Medicare Advantage, the opportunity continues to expand based on the aging population in the U.S. and the increasing popularity of the Medicare Advantage program. Spending in this segment is expected to nearly double from $240 billion in 2017 to approximately $460 billion in 2022. Medicaid spending is expected to follow a similar course from $480 billion in 2017 to nearly $800 billion in 2022, due to states opting to move more of their complex populations into managed Medicaid programs.

WellCare is uniquely positioned to serve these growing market segments. Our 2019 Medicaid win in North Carolina and expansion into other states underscore the value of our unique local approach to managed care combined with our high-touch, integrated care model. WellCare goes beyond healthcare to incorporate physical, behavioral, pharmaceutical and social needs of our members. For more than a decade, WellCare has integrated social determinants into our care model through our program called Community Connections. In 2018, we connected more than 51,000 people to approximately 194,000 social service resources providing more cost-effective, community-based solutions.

For the past several years, WellCare has made significant investments in process improvements, quality programs and our people � all to ensure our members have access to high-quality, affordable healthcare. Our focus on quality is beginning to yield results. Approximately 42 percent of our Medicare Advantage members are now in a plan with 4.0 stars or higher. Among this year�s highlights, Houston attained a 4.5-star rating; Florida, New York/Maine and California achieved an overall 4.0-star rating; and five plans achieved an overall 3.5-star rating.

In Medicaid, seven of the nine WellCare markets that were rated by the National Committee for Quality Assurance (NCQA) received quality scores that ranked us first or second. While we�ve made significant progress, we remain focused on continued improvement in our quality performance.

A Mission-driven Company
For the second consecutive year, WellCare earned a spot in the Civic 50, an initiative by Points of Light, which recognizes the 50 most community-minded companies in the nation.

Year after year, our associates demonstrate a deep commitment to giving back to our communities and members. In 2018, 76 percent of our associates volunteered time through the WellCare Associate Volunteer Efforts (WAVE) and Day of Service (DOS) programs � this participation rate is more than double the national average. Our commitment to our communities and our passion for serving our members are cornerstones.

Lastly, we continued our pledge to diversity and inclusion where we engaged all leaders with in-person Conscious Inclusivity training. This training supports our intent to build a culture where our associates are empowered to bring their full selves to work each day. We also participated in the CEO Action for Diversity and Inclusions� Day of Understanding with 150 other U.S. companies, the nation�s largest conversation to advance diversity and inclusion in the workplace. As chairman of our Diversity Council, we�ve strengthened oversight of diversity activities including expansion of our associate resource groups.

While we are proud of our diverse workforce and actions, we know that we must continue our focus and place our diversity and inclusion strategy at the center of our growth and innovation in the years ahead.

On behalf of the board of directors and the WellCare leadership team, I�m genuinely grateful for our associates, providers and community partners, who work with WellCare to serve our 5.5 million members. Also, we want to thank our stockholders for their continued support and investments. We are pleased with our performance in 2018; however, we are even more enthusiastic about our future and what lies ahead.

Signature

Kenneth A. Burdick
Chief Executive Officer
WellCare Health Plans, Inc.