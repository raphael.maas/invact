Dear Fellow Shareholders,
Customers & Employees

2010 was a defining year for International Flavors
& Fragrances, with growth in both divisions, across
all categories and on every continent. At every level
of the organization, we attained unity of purpose in
seeing  and seizing  the exciting future before us.
Never before have consumers been so dynamic,
and diverse, in choosing what is meaningful to
them. Now, because of the choices we have made
in gathering consumer insights and aligning them
with our world-class research and development
practices, IFF is uniquely positioned to satisfy
consumers ever-shifting desires in developed and
emerging markets, today and in the future. For
this reason, we have chosen Focus on the Future
as the theme for our 2010 annual report. We
hope you will see inside these pages, that at IFF,
winning in the future is not just an aspiration but
the essence of who we are.
In reviewing our 2010 results, I am pleased
to report IFF increased sales approximately $300
million, or 13 percent to $2.6 billion. While
collaborating with our customers  leading global
and regional consumer products companies
around the world  we created unique scent
and taste experiences for over 34,000 products
worldwide. Our performance was strong and
broad-based with nearly three-quarters of our
categories closing the year up double - digits.
We also achieved considerable progress in
improving customer intimacy, culminating in our
designation as a leading supplier by Procter
& Gamble. In addition, we made excellent
progress in our commitment to sustainable
growth by aligning our resources to improve
our corporate sustainability platform.
From a regional perspective, our emerging
markets grew 16 percent and now represent 44
percent of our total business. To put this in context,
the incremental sales generated in these markets
represented the equivalent of adding another
China  our fifth-largest market  to our business.
Now, for the first time in IFFs history, Greater Asia
has surpassed North America to become our
second largest region behind our Europe, Africa
and Middle East business.

Our success did not stop there. In 2010, IFF
significantly increased overall profitability. Strong
operational leverage, combined with favorable
input costs and greater efficiency, drove a 17
percent increase in adjusted operating profit. This
substantial growth, in turn, gave us the flexibility
we needed to make key strategic investments while
delivering a 60 bps improvement in margin to
16.3 percent  a level not seen since 2004. The
consequences for the bottom-line were profound.
For the full year, adjusted EPS soared 25 percent
to an all-time company high of $3.37.
Our formidable operational performance,
along with improvements in working capital
efficiency, also helped drive higher cash flow
year-over-year. As a result, we increased our
quarterly cash dividend eight percent to $0.27
 our fifth increase in six years  while enhancing
our investments in capital expenditures to bolster
key regions and technologies.
In our Flavors business unit, local currency
sales improved by 10 percent, due in part to
volume gains and new business wins across all
regions. Especially encouraging were the results
from our emerging markets, which outpaced the
rest of the world, with countries like Brazil, Russia,
India and China registering combined growth of
over 15 percent. In the developed markets of
North America and Europe, innovative tools,
including sweetness enhancement and sodium
reduction, helped transform our new win
performance. This excellent top-line growth  as
well as lower input costs and margin improvement
initiatives  led to a second straight year of 90
bps improvement in operating profit margin for
a record 20.2 percent.
Focusing in on Fragrances, local currency sales
increased 16 percent, with all regions and nearly
all categories yielding double - digit growth. In
Fine Fragrance, we bested our 2007 levels
 the previous company record  with doubledigit
growth driven by new business wins and
elements of restocking. New product launches in
key world markets also helped fuel double-digit
growth in Beauty Care, including Hair Care and
Toiletries. The highlight of the year was our Latin
America business, where our Fine Fragrance and
Beauty Care segment expanded at an impressive
40 percent growth rate. At the same time, our
long-standing presence and consumer insight
investments in emerging markets helped spur
growth in Functional Fragrances, especially in
our Fabric Care business. In addition, Home
Care reversed its 2009 trend with double-digit
growth, while Fragrance Ingredients, the building
blocks of our Fragrance Business, expanded by
double-digits thanks to underlying improvements in
demand and elements of restocking. Profitability
also improved significantly, with adjusted operating
profit jumping 30 percent and adjusted operating
margin accelerating by 220 bps to 17.3 percent.
This is the first time we have seen such growth
and margin improvement since 2006, when we
divided the company into two business units.
The vital question every growing business must
face following such a banner year is: whats next?
At IFF, the future is always urgent, and it is always
now. With this in mind, we have adopted three
guiding principles to develop and implement our
strategy for making IFFs long-term goals actionable
in the present:
Leverage our geographic footprint  consumer
spending in emerging markets will continue rivaling
those of developed countries. IFF has worked
hard to build and sustain an unmatched footprint
in these emerging markets, resulting in extensive
knowledge of local consumers and a proven
ability to deliver consumer-preferred flavors and
fragrances. In nurturing populations still growing
in size, wealth, hope and aspiration, we will
broaden the advantages we already enjoy as a
leading global supplier to many leading consumer
products companies.
Strengthen our innovation platform  consumers
will continue embracing new and better ways
of living, including searching for healthier, more
natural foods and scents. Starting from a position
of strength, IFF has already devoted substantial
resources to discovering and developing new
molecules, natural ingredients and delivery systems.
Innovation is rooted in everything we do, and we
are committed to deepening our R&D investments
to further enliven IFFs portfolio for our customers
and the consumers they serve.
Maximize our portfolio  successful businesses
will continue refining their product portfolios to meet
and exceed consumers changing expectations.
Self-reflection is not only the hallmark of
IFFs operating culture; it is a value we share at all
levels of the organization. In 2010, we worked
hard to refine our strategies and business model.
Moving forward, we will continue focusing on
areas that offer the best returns while improving
those that are challenged. Maximizing our product
portfolio will put us in a stronger position to grow
our customers businesses and gain greater value
for shareholders who choose to invest in the
future with us.
We believe the next few years will be critical
to defining the long-term future for IFF. In seizing
the exciting opportunities and challenges before
us, we will be strengthened by the successes we
have achieved in the past year. Not only does
2010 represent another landmark chapter in
IFFs ongoing growth story; it has reinforced our
confidence in driving improved performance. As
we Focus on the Future, let us also reaffirm our
commitment to winning it with our shareholders,
customers and employees.
On a personal note, I would like to thank my
colleagues on the Board, especially Burt Tansky
for his invaluable contributions to IFF over the many
years. I would also like to welcome his successor,
Roger Ferguson, a well-respected business leader
within the financial community. Finally, I would
like to give special thanks to every one of our
employees around the world for their incredible
talent, drive and passion.
Douglas D. Tough
Chairman of the Board and Chief Executive Officer
International Flavors & Fragrances Inc.