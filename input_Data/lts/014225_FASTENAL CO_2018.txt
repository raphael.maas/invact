LETTER TO SHAREHOLDERS

Thank you for being a shareholder of Fastenal.


We operate a simple business model at Fastenal, and we limit our
priorities to just four things: our customers, our employees, our
suppliers, and our shareholders. The situation determines the ranking
of these four priorities. In most situations, customers and employees
occupy the first two spots and suppliers and shareholders fill in the
last two. We believe this prioritization benefits you, our shareholder,
because we are at our best when we focus on our customers and
those who serve them. We also take every opportunity to thank each
constituent � thank you for being a shareholder of Fastenal.
We approach these four priorities with a long-term perspective
and a �short-term edge.� The long-term perspective builds a more
durable business, and the short-term edge maintains our sense of
urgency. That said, we sincerely hope you view Fastenal as a longterm investment, not a short-term rental � this ensures our goals
are aligned.
It is early January 2019 as I sit down to write this letter. Since stepping
into my current role late in 2015, I�ve followed a similar preparation:
talk to some Fastenal employees, talk to some customers, talk to
some suppliers, and reach out to a few shareholders. It�s also a good
time to re-visit the last several letters. This step helps me avoid
getting caught up in repetition and rhetoric. It also illuminates some
truths about the role. First, the longer a person is in this position,
the more opportunities exist to just be wrong. Second, if you are
fortunate enough to surround yourself with great people, we can
get through the bad days and enjoy the good, always mindful of the
beauty and wisdom of humility. Humility pulls the best out of people,
and it also challenges us to periodically �step it up.� (Note to self: In
my case, �periodically� should mean daily.)
My first three letters weren�t especially profound, but they did
capture the spirit of the year. To set the tone for 2018, here are the
first three paragraphs of last year�s letter.
Excerpt from our 2017 Letter to Shareholders
(dated February 2018):
Our 50th year was a good year for Fastenal, a year for our
customers and employees to experience improving success, a
year for our suppliers to participate in this success, and a year for
our shareholders to enjoy a return on their investment.
It was also a year to remind us about the nature of Fastenal.
We believe in people. We believe we can accomplish anything
if everyone in the organization pursues a common goal. And we
believe we can accomplish our goals faster if we unleash the vast
human potential within the organization.
We have a common goal; it�s Growth Through Customer Service.
We also have a means to unleash our potential; it�s called
challenging each other, providing great training, and operating
with a decentralized decision-making mindset.
Our business continued to improve in 2018.
It improved not just in the financial results, but also in the ability of our
team to execute on our growth drivers, including national accounts,
Onsite, vending, international, construction, and e-commerce (scan
ahead to the end of the letter for a more in-depth definition). A piece
of this improvement can be explained by a good economy; however,
we outperformed the economy and we continued to separate
ourselves in the marketplace in terms of sales growth.
This separation is noteworthy because our growth comparisons
became progressively more difficult as we moved through 2018.
For background perspective, our daily sales grew 6.2% in the first
quarter of 2017. This growth expanded more than four percentage
points, to 10.6%, in the second quarter, then expanded to 13.6% and
14.8% in the third and fourth quarters, respectively.
In the interest of full disclosure, a piece of the improvement in the
second quarter of 2017 (a little over one percent of our growth)
came from a March 2017 acquisition; however, the real drivers run
much deeper: a willingness of our customers (new and old) to trust
us with their supply chain needs; a willingness of our employees to
approach their business and customers with a �Think Big!� mindset;
a willingness of our suppliers to improve and change with us;
and finally, a willingness of our shareholders to believe in us. (As
mentioned in last year�s letter, �Think Big!� is a challenge to everyone
on the Blue Team to have a plan, to stretch ourselves, and to vet our
plans with those around us: our teams, our peers, our mentors and
leaders.)
These human elements � trust, collaboration, ambition �
underpinned strong financial results in 2018. Here�s a quarter-byquarter breakdown of how the year played out:
First quarter: Sales grew $138.1 million, or 13.2%, operating earnings
grew $22.0 million, or 10.4%, and incremental operating margins
(operating earnings growth versus sales growth) were 16.0%.
Second quarter: Sales grew $146.4 million, or 13.1%, operating
earnings grew $31.5 million, or 13.3%, and incremental operating
margins were 21.5%.
Third quarter: Sales grew $147.0 million, or 13.0%, operating
earnings grew $33.9 million, or 14.8%, and incremental operating
margins were 23.1%.
Fourth quarter: Sales grew $143.2 million, or 13.2%, operating
earnings grew $30.1 million, or 14.8%, and incremental operating
margins were 21.0%.
Annual: Sales grew $574.6 million, or 13.1%, operating earnings
grew $117.5 million, or 13.3%, and incremental operating margins
were 20.5%.
By contrast, in 2017 and 2016 our annual sales grew $428.4 million 

(10.8%) and $92.8 million (2.4%), respectively; our operating
earnings grew $85.9 million (10.8%) and contracted $32.9 million
(-4.0%), respectively; and our incremental operating margins were
20.1% in 2017 and negative in 2016.
Several things should stand out in the summary above, including
some changes and trends you started to see in last year�s letter.
First, after summarizing our sales growth, the discussion�s focus
moves from our pre-tax earnings and net earnings performance
to just a discussion of our operating earnings performance. The
reasons for this change are simple: (1) Over the last several years
we repurchased stock and changed our debt/equity structure. This
structurally altered our net interest income/expense amounts,
making pre-tax earnings performance a less precise comparison
aspect. (2) The United States government passed income tax reform
in December 2017. Since over 85% of our earnings occur in the
United States, this structurally lowered our income tax liabilities
(providing a benefit of about $127 million and $24 million in 2018
and 2017, respectively). This income tax savings amplified our
13.3% and 10.8% operating earnings growth to net earnings growth
of 29.9% and 15.8%, respectively, and really muddied the water
for comparability. We prefer straightforward comparisons, so we just
focused on the operating earnings performance comparisons.
The second item that should stand out is our incremental operating
margins in the last several years. Beginning in late 2015, we began an
increased investment to reinvigorate our ability to grow the business.
This investment centered on three things. The first was inventory to
serve our customers, particularly our construction customers (more
on this in a few paragraphs). The latter two centered on people
resources to support our growth drivers and our technology needs.
We have big plans to expand our relationships with national account
customers and within channels such as vending and Onsite. We also
have plans to utilize technology to deepen our relationships with all
customers. These plans require a focused support infrastructure, so
we expanded our headcount between late 2015 and late 2016 to
start building it.
Looking at our earnings performance in 2016, you can see this
investment was painful; however, it breathed new life into Fastenal,
and we realized double-digit sales and earnings growth in 2017
and 2018. Of equal importance, we achieved 20%-plus incremental
operating margin gains in each of the last two years. I�m proud of our
Blue Team, and hope you are too.
At Fastenal, we work to inspire greatness in each other. We
challenge each other, we learn, and we change. We also work on
our shortcomings every day. Even in a good year like 2018, we had
a few:
We could have produced a great fourth quarter. We didn�t, and our
operating margin leverage barely broke 20%.
Our growth drivers are fundamentally changing the business. The
success of our growth drivers naturally lowers our gross margin
percentage (it�s no secret that these areas of our business carry
an inherently lower gross margin). But this success also leverages
our existing distribution network, naturally lowering our operating
expense percentage. These trends are expected. Our shortcoming
lies in the amount of degradation in the gross margin percentage. We
have been actively battling inflation, particularly fastener (i.e., steel)
inflation. We are moving at a fast pace, but the inflation is moving
even faster, and we lost 20 to 40 basis points of gross margin in
2018 as a result. I believe we can recapture this, but only time and
execution will determine if that belief can be made into a reality. In
our February 2017 Letter to Shareholders, we indicated a plan to
better manage expenses. This is where the Blue Team has really
stepped up, enabling us to overcome our gross margin weakness
through better expense management. Our strong operating earnings
performance is the result.
Our business generates great cash flow, but like all distribution
businesses, it weakens a bit with strong sales growth. We accept
this trade-off as a good problem to have. The weakening has been
more pronounced than normal because the source of our strong
growth leans toward large national account customers, particularly
in international markets. Unfortunately, these typically aren�t our
fastest-paying customers. Some of the weakness is an outcome of
our national account success, but some is within our control. We
need to alter this trend in 2019.
Inventory growth is a different matter. Several years ago, we made
a decision to increase our inventory breadth and depth, particularly
at the branch level. This required a sizable investment, but we have
seen a great improvement in sales performance, particularly with
construction customers, re-establishing Fastenal as a construction
supplier. We also invested in inventory to support our strong
growth with national account customers, particularly Onsite and
international locations. As president, I have been willing to accept
high single-digit inventory growth to support double-digit sales
growth with our growth drivers. We are generating attractive returns
with this investment.
That said, in 2018 we didn�t grow our inventory in the high single
digits; we grew it about 17%. Several factors changed the math
in the latter half of the year. The inflation mentioned earlier added
to our inventory, as did the extra layer of import tariffs that began
building in September with the implementation of the Section 301/
List 3 tariffs. A related factor was a decision we made to accelerate
some container shipments in the fourth quarter in light of additional
pending tariffs. Please note, we decided not to accelerate our
purchasing patterns, as the situation was just too fluid; however,
we did instruct our suppliers to ship everything they had produced
to beat the January 1st proposed tariff increase. These three items
added about 4% to our inventory growth. Absent this, we added
about 13%, and as mentioned earlier, we really should have added
less than 10%.
These shortcomings are on me, and in no way should they
overshadow the real Fastenal story of 2018: great execution by a
great team. That said, we will work hard to improve in these areas
in the year ahead.

We are wired for Growth Through Customer Service.
In 2018, we achieved sales and operating earnings of about
$5 billion and $1 billion, respectively. Our internal discussions
are focused on preparing Fastenal�s Blue Team to become the
$10-billion organization we see in our future. This future requires
a plan and an organizational willingness to change. We continually
ask ourselves: What capabilities must our organization develop to
serve our customers? What skill sets must our employees possess
to serve our customers and each other within the Blue Team? What
capabilities must our suppliers bring to be part of our customers�
supply chains? What financial performance must we produce to
attract long-term investors to Fastenal?
An example on the employee front involves developing leaders.
Frankly, our organization has been good at this for the last 50 years.
Our willingness to trust and challenge each other, our request of
each person to be willing to learn and change � these aspects of
the Fastenal DNA produce great leaders. However, like everything
organic, sometimes we need to till the soil. Beginning in 2019,
our Fastenal School of Business has a new course inspired by our
leaders throughout the organization. The technical name is LDR450:
Leadership Enhancement Program. It includes some self and peer
assessments, but it really explores individual leadership stories,
leadership competencies, methods to understand mindsets, and
the ability to understand influence style and emotional intelligence.
Given the willingness of the Blue Team to learn and change, we think
it will give us the ability to develop great leaders even faster.
The list of questions continues: How should we best convey our
business to all four constituencies? What new partners should
we consider to improve our customer service capabilities, our
employee skills, our supplier relationships, and ultimately our
shareholder value? An example on the partner front is a relatively
small investment we made in July 2018 with a technology firm
based in the United Kingdom. They specialize in mobility (not one of
our strengths), and we believe they share a similar set of priorities
and culture with Fastenal. We look forward to the potential of other
similar opportunities in the future.
Thank you for the patience you demonstrate with our long-term
perspective (combined with a short-term edge). Our belief in people
can make for a less controlled and sometimes bumpy ride, but
we believe the trip is well worth it. Thank you again for being a
shareholder of Fastenal.

DANIEL L. FLORNESS
President and Chief Executive Officer
