TO OUR SHAREHOLDERS:

                   Equifax is winning in the marketplace with game-changing
                   initiatives. As a result, 2006 was one of the most successful
                   years in Equifax's 108-year history. Our Company developed
                   a powerful strategic plan for future growth. We created a new
                   vision and a new set of values to guide that growth. We
                   realigned the organization to better execute our strategic
                   plan and to make our culture more customer-centric.
                   Throughout this transformation, our team generated
                   outstanding financial results. We asked a lot from our
                   associates, and they delivered. I am very proud of their
                   accomplishments and extremely excited about the level of
                   momentum we have created together. In fact, based on 2006
                   performance, Equifax was named to the FORTUNE 2007
                   list of America's Most Admired Companies in the
                   Financial Data Services category.


                   A Record Year
                   As the Company's financial results demonstrate, 2006 once again reflects
                   this management team's commitment to deliver performance for Equifax
                   shareholders. Revenue rose 7 percent to $1.55 billion, reflecting growth
                   across all businesses. Net income was $275 million, up 11 percent.
                       Earnings per share, excluding certain items, were $2.01 in 2006
                          compared to $1.86 in 2005, up 8%.
                                  Approximately 27 percent of our U.S. online transactions
                                were processed by year-end through one of our enabling
                                  technologies, up from 23 percent in 2005 and working
                                      toward our goal of 50 percent by 2010. Commercial
                                         Solutions grew 66 percent in the U.S. and was
                                            strengthened through the acquisition of Austin-
                                                Tetra, a leading provider of business-to-
                                                  business data management solutions
                                                    for FORTUNE 1000 companies and
                                                     government agencies. Our Personal
                                                       Solutions direct-to-consumer business
                                                       grew revenue 10 percent over last
                                                       year and increased its subscription-
                                                       revenue business, which will result
                                                      in faster, more profitable growth in
                                                   that segment. Latin America contin-
                                                 ued to drive 21 percent revenue growth
                                               and margin improvement, and Europe
                                           achieved 8 percent revenue growth while
                                         maintaining its margin.




A New Game Plan                                        
Throughout its history, Equifax has transformed                      
itself whenever new challenges and opportunities
were presented. The most recent transformation                

has been driven by the ever-changing market reali-               $2,000                                 $2.20
ties, and, even more importantly, by the untapped
potential of our organization. Our goal is not sim-                                                       2.10
ply to sustain growth, but also to take Equifax to                  1,500
an entirely new level of performance. To do that,
                                                                                                          2.00
we realigned our organization to better focus busi-
ness unit resources on the customer and leverage
                                                                   1,000
our support expertise globally.
                                                                                                          1.90
     At the foundation of the plan is our new vision
statement that positions Equifax "to be the trusted
provider of information solutions empowering our                      500                                 1.80
customers to make critical decisions with greater
confidence." It reflects our ongoing belief that data,
                                                                         0                                1.70
particularly unique data, is core to what we do. We                              
create the most value for our customers when our
                                                                      
people combine analytics and enabling technologies                        
into empowering solutions. Trust is fundamental.
We take this responsibility very seriously on behalf of        Supporting three of four smartGROWTH
consumers, customers, shareholders and employees.         initiatives, on February 14, 2007, we entered into
     The process of putting together a game-chang-        an agreement and plan of merger with TALX
ing strategic plan was almost as beneficial as the        Corporation, a leading provider of workplace
result. Roughly 150 employees from around the             verification data and employment services, in a
world were involved in making decisions, galvaniz-        transaction valued on that date at approximately
ing into one team to own our strategy and commit          $1.4 billion, including the assumption of debt.
to its execution. Working over six months, this team      The pending transaction, which is structured to
developed a powerful framework for growth. We call        consist of 75 percent Equifax common stock and
it smartGROWTH. This strategy sets goals that are         25 percent cash, is subject to approval of TALX's
ambitious yet achievable, balances risk with return,      shareholders, regulatory review and other custom-
and analyzes and exercises discipline at every turn.      ary closing conditions.

smart GROWTH Initiatives
Our smartGROWTH planning took place from the
                                                                                      
corporate level down through each individual
                                                                                      
business unit. We ultimately distilled the work into                                 
four areas that will change the game and drive our
                                                                                      The mission of the Marketing Center of
growth through 2010:
                                                                                      Excellence is to lead the innovation and rev-
    Capture a greater share-of-wallet from
                                                                                      enue growth initiatives for Equifax. During
     our customers through enhanced customer                                          2006, Marketing worked closely with the
     segmentation and targeting, aggressive new              business and support units to create the smartGROWTH strategic
     product development and a focus on multi-               plan. A global New Product Innovation (NPI) process was also
     product solutions                                       implemented. NPI focused corporate resources on develop-
                                                             ing and launching new products within each business unit and
    Focus on enabling technologies and predic-
                                                             market. The new products launched, or that are in development,
     tive sciences, which deliver faster growth
                                                             have the potential to generate up to $50 million in revenue in 2007.
     and increased retention rates when integrated
                                                             Marketing also works closely with the businesses to acquire new
     into customer business processes and                    data sources, enter new geographic markets, expand into new
     technology platforms                                    industry verticals, develop customer segmentation strategies and
    Invest in unique data assets that can further           create initiatives to expand current market share. In addition, this
     differentiate Equifax in the marketplace                Center of Excellence includes branding, marketing communications
                                                             and public relations.
    Expand into adjacent markets, new geographies
     and new industries where we can leverage our
     intellectual capital and proprietary capabilities




                                              enabling technologies, corporate development and
                                             human resources.

                                                   A Culture of Game Changers
                            $375
                                                                       Part of moving to a more customer-centric culture
                                                                       means acting as one team, speaking with one voice
                                                                       and driving toward one vision. Behind this entire
                                                                       transformation are the people of Equifax. They
                                                                       exhibit a tremendous amount of energy and com-
                              325
                                                                       mitment to changing the game.
                                                                            We are moving to a high-performance organiza-
                                                                       tion focused on meritocracy, where outstanding work
                                                                       is rewarded. I commend the almost 5,000 Equifax
                              275
                                                                       associates for their work as Game Changers. They
                                                                       are making this transformation a reality. More
                                                                       than 92 percent participated in our employee
                                                                       survey that helped shape faster decision making,
                              225                                      as well as helped create new ideas to fuel growth.
                                          
                                                                       This is a transformation that has been driven by
                                                                       market needs, but built on the voice of the people
                                                                       who conduct our business every day.
             Organized for Growth
             To ensure the success of our smartGROWTH                  I n Pos ition to Chan ge the G am e
             initiatives, our strategy will be supported by a          It is personally thrilling to see an organization of our
             structure that can facilitate faster growth. Therefore,   size, with a heritage of more than 100 years, transform
             our organizational realignment was another critical       itself and its future. Equifax emerged from 2006 bet-
             accomplishment in 2006. Our game-changing struc-          ter positioned than ever before to create value for
             ture, which was effective January 1, 2007, is built       customers and shareholders, and to be a place where
             around four distinct market-facing business units:        smart, hard-working people can flourish professionally.
             U.S. Consumer Information Solutions, International,       As 2007 unfolds, our key priorities will drive perfor-
             North America Commercial Solutions and North              mance: executing the strategy put in place during
             America Personal Solutions. These four businesses         2006, instilling the values of a high-performance
             are supported by global Centers of Excellence, includ-    culture; exploring strategic opportunities which
             ing technology, operations, marketing, finance, legal,    may include acquisitions; developing innovative
                                                                       products; and incorporating more efficiency into
                                                                       our operations.
                                                                       To all of the Equifax Game Changers who contrib-
                        
                                                                       uted to an outstanding 2006 and positioned us for
                          
                                                                       a strong 2007, you have my personal thanks. To all
                           
                                                                       of our consumer and business customers, as well as
                      The Human Resources Center of Excellence         shareholders, who will benefit from our efforts, you
                      is responsible for attracting, acquiring,        have my promise that Equifax will work diligently to
                      rewarding and developing the necessary
                                                                       fully realize its potential.
                      talent to ensure the successful execution
of the business strategy. The human resources organization
led the alignment of the Company to the new growth strategy.
                                                                       Sincerely,
The alignment included a rigorous assessment of the culture,
work, structure, capabilities and talent required to deliver the
growth initiatives. This Center of Excellence will be instrumental
in the transition of our culture to a more performance-driven
organization with greater challenges, accountabilities,
opportunities for development, and rewards for employees.
The focus in 2007 is to launch new performance and total               Richard F. Smith
reward programs which will enable Equifax to acquire,                  Chairman and Chief Executive Officer
develop and retain the best talent in our industry.


