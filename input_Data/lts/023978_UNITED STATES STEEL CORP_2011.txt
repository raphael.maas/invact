A Message from Our Chairman

In 2011, United States Steel Corporation reached two significant milestones. We
observed our 110th anniversary in business, but more importantly, 2011 was
the safest year in our companys history. Continued active engagement of our
entire workforce resulted in improved performance in
our key safety measurements. Our 2011 Global OSHA
Recordable Incidence Rate was our lowest on record. We
also outperformed our target for Global Days Away from
Work Incidence Rate, marking another record for our
performance in this category. In addition, our companys
injury rate in the U.S. Bureau of Labor Statistics most
severe category  Days Away from Work Cases Greater
Than or Equal to 31 Days  continues to reflect the strides
we are making when compared to average rates for our
industry and for manufacturing in general. While we are
pleased with our progress in recent years and the results we achieved in 2011,
we recognize that there is still room for improvement. We will remain focused on
achieving our ultimate goal of zero injuries across our entire company because
we owe it to ourselves, our co-workers, our families and you, our shareholders, to
settle for nothing less.
Last year we also made progress on a number of significant, strategically important
capital investments. One area of focus centers on investments in coke and coke
substitute production capabilities to improve both our long-term self-sufficiency
in this key thinly traded steelmaking raw material as well as our environmental
performance. Construction of a technologically and environmentally advanced byproducts
recovery coke battery, with a projected capacity of approximately one
million tons per year, along with two low emissions quench towers is under way at
Mon Valley Works Clairton Plant with coke production scheduled to begin later this
year. At Gary Works, we expect to finish construction later this year on a Carbonyx
carbon alloy synthesis process facility. The facility, which has two modules with a
total projected capacity of 500,000 tons per year, will employ an environmentally
compliant, energy efficient and flexible production technology to produce a coke
substitute. In 2011, we also completed an expansion of our blast furnace coal
injection facilities, allowing all of our blast furnaces in Slovakia to use pulverized
coal, historically a lower cost source of carbon than coke.

In accordance with our commercial strategy of manufacturing high value-added
steel products to meet our customers evolving needs, work continued on an
important investment at one of our flat-rolled joint ventures and we completed a
significant facility expansion project in our tubular segment. At PRO-TEC Coating
Company, our flat-rolled joint venture with partner Kobe Steel, construction
continues on a state-of-the-art continuous annealing line that is expected to
reach full production by the end of 2013. Once completed, this facility will enable
production of advanced and ultra high-strength steels that can be utilized by the
automotive industry to meet increasingly stringent vehicle emissions and safety
requirements. In the third quarter of 2011, we completed the construction of a
quench and temper line as well as the installation of an API (American Petroleum
Institute) and premium connection finishing facility at our Lorain Tubular Operations,
all of which will help our company more efficiently serve our tubular products
customers increased focus on North American shale resources.
The increased and ongoing development of shale resources not only presents our
company with opportunities to increase our tubular product sales, but it has also

created a reliable, competitively priced supply of natural gas that we expect will
continue well into the future. This reality has led our company to begin implementing
certain initiatives as well as consider other longer-term business strategies to
effectively increase the use of natural gas in our operations given the significant
cost savings and environmental advantages of what we believe is an extraordinary
fuel. Among the options we are exploring are ways to leverage our significant iron
ore position in the United States as well as use natural gas as an alternative to coke
in the iron reduction process. We continue to consider plans for an expansion of our
iron ore pelletizing operations at Minnesota Ore Operations Keetac facility, and we
completed the necessary permitting process for the proposed project in December
2011. We also are examining alternative iron and steelmaking technologies such
as gas-based, direct-reduced iron and electric arc furnace steelmaking, although
we expect that the blast furnace and basic oxygen furnace processes we currently
employ will remain our primary technology for the long term. While there are
practical limits to how much natural gas we can use, we do plan to keep pushing
the envelope and our future capital spending may reflect these and other related
strategies because the benefits  both economic and environmental  are worth it.
Looking back on our 2011 financial performance, we continued to manage through
uncertainties brought about by the slow and uneven economic recoveries in North
America and the European Union (EU). Demand in some of the markets we serve,
such as automotive and energy, improved from the historic lows experienced during
the global economic downturn, but other markets, such as construction, continued
to be challenging. For our part, we continued to focus on the things that are within
our control: improving our safety and environmental performances, reducing
costs where possible, operating our facilities efficiently, producing the highest
quality value-added products, and providing our customers with the world-class
customer service they expect and deserve. Those efforts, coupled with the market
improvements mentioned above, resulted in significant financial progress for our

company in 2011; however, the EU sovereign debt crisis and the resulting economic
uncertainties adversely affected markets in the region, particularly Southern Europe,
where we had been dealing with significant cost and commercial challenges at
U. S. Steel Serbia. We pursued all options to improve our situation there and ultimately
the option that proved to be in the best interest of our shareholders was to sell the
operations to the Republic of Serbia effective Jan. 31, 2012. The sale allowed our
company to exit the operations quickly and focus our management attention and our
capital on other areas of our company.
As we look ahead, we expect market conditions for all three of our operating
segments to improve. As has been the case since the economic downturn began,
we will continue to operate our facilities in line with our customers orders and we
believe our operating configuration gives us the flexibility to effectively and efficiently
meet our customers needs.
Despite the challenges we have faced in recent years and the uncertainty that
remains, our company can proudly say that our commitment to our core values
of safety, diversity and inclusion, focus on cost, quality and customer, results and
accountability, and environmental stewardship is  and always has been  as strong
as the remarkable material our dedicated employees play a role in manufacturing
every day. Together, our management team and employees across the company
have drawn strength from those values, from 110 years of history, and from modern
societys continuing need for steel as we work to position United States Steel
Corporation to take advantage of the opportunities the market has to offer now and
in the future. We thank you for your continued support of our company.
Sincerely,
John P. Surma
Chairman of the Board of Directors and Chief Executive Officer