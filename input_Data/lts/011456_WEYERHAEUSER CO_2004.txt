Dear Shareholders:
Each day, the people of Weyerhaeuser are putting in place the behaviors, practices, assets
and skills to help us become the best forest products company in the world.
In 2004, we saw that work result in record earnings of $1.3 billion, or $5.43 per diluted
share. But there are other signs that their hard work is paying off. We make prudent and
effective use of resources to create shareholder value. Our performance as measured by
return on net assets is improving. We operate with greater safety. Our workforce is increasingly
diverse. The highest standards of social responsibility, ethical conduct and environmental
responsibility guide our actions.
Such results have not come easily. To achieve them, we made tough decisions over the
past five years. We closed facilities, sold non-strategic timberlands and streamlined operations.
We grew through three acquisitions and then merged systems, cultures and business
styles into a new Weyerhaeuser. We absorbed the costs associated with these changes in
anticipation of the benefits we knew lay ahead.
In the process, we created sustainable changes in how we operate.
Were proud of our progress, but we cannot rest. We operate in a global market, competing
internationally and selling products used in nearly every nation. Our planning must take
into account world economics, and our businesses must be world class. So, during 2004,
we continued to improve.
One focus involved reducing our debt to approximately $10.6 billion at the end of 2004.
When we acquired Willamette Industries, we committed to regaining our historical financial
fitness. Weve kept our word. In the process, we've preserved our access to major financial
markets and protected your underlying interests.
We also strengthened our balance sheet by shedding non-strategic assets. During the year,
we sold or closed 11 facilities and sold non-strategic timberlands, including approximately
270,000 acres in Georgia. We know these decisions affect valued workers and the communities
where we operate. But they build a necessary base from which we can compete and
thrive in the future.
To help us make the most of our new company, were creating tomorrows leaders from an
increasingly diverse workforce. At the highest level of the company, three women 
Ernesta Ballard, Patty Bedient and Susan Mersereau  report directly to me. Never before
have women held so many important leadership roles at Weyerhaeuser. We also made
progress in other areas of the company. Our professional workforce is now 34 percent
women and 11 percent minorities. Women hold nearly 15 percent of our management jobs;
minorities account for over 10 percent. But our job is far from finished. I am personally
committed to making further progress before I retire in 2007.
You also have my personal commitment to improving our safety and financial performance.
Safety is our top priority. Through focus, dedication and caring, weve reduced our recordable
incident rate from 7.3 in 1994 to 2.04 in 2004. But people are still getting hurt. Unsafe
behavior occurs in plain sight. Ive challenged our entire management team to improve
our performance in 2005. We will start with a re-dedication to safety that will go beyond
the courage to care and demand intervention to prevent injury.
We also must continue to improve our financial performance. This year, our return on
shareholders interest increased to nearly 16 percent. But as investors, you have a right to
expect better performance and were committed to meeting your expectations.
We know we can because we have the necessary assets, people and determination.
 Thanks to our silvicultural and forest management expertise, Weyerhaeuser timberlands
are among the most productive in the world.
 Our Wood Products business produced more feet of lumber in 2004 than it did in 2002
with five fewer facilities.
 Our Fine Paper manufacturing facilities boast some of the most modern paper machines in
the industry, allowing us to produce more paper on a daily basis at a lower cost per ton.
 In Containerboard, because of production improvements and the productivity of our
employees, weve been able to eliminate older, less-efficient machines and facilities and
still meet increased demands from our customers.
 Operating in select markets and maintaining a local focus, Weyerhaeuser Real Estate
Company has posted seven consecutive years of increased earnings.
These assets, combined with our peoples focus on frugality, will help us produce the returns
you expect.
After we further reduce our debt, well be well-positioned to capitalize on opportunities to
enhance shareholder value. As we explore our options, including growth opportunities in
North America and the Southern Hemisphere, our focus will always be on increasing
shareholder value.
This past year, we demonstrated our potential. In the coming years we intend to achieve
the strong planned results for our performance-driven company. Our commitment to speed,
simplicity and decisiveness will guide us. We will view the world as our market and people
worldwide as our stakeholders.
Enjoy your tour of our operations. As you look through the rest of this report, youll see the
faces of our global workforce. In the future, expect to see more faces from around the
world. As we expand our presence, well continue to focus on being a strong global citizen.
We invite you to watch our world grow, and with it, the value of your investment in
Weyerhaeuser.

Sincerely,
Steven R. Rogel
Chairman, President + CEO
