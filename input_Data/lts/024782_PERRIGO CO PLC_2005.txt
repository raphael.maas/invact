Dear Fellow Shareholders,
During fiscal 2005 Perrigo
made progress on a number
of strategic initiatives.
The acquisition of Agis
Industries, Israels number two
pharmaceutical manufacturer,
with a global generic Rx
drug presence, has effectively
created a new Perrigo: a
global company with broader
capabilities to grow in the
generic pharmaceutical, active
pharmaceutical ingredients
(API) and consumer health care
markets. The acquisition brings
together talent and technology,
and combines Perrigos
leadership in store brand
over-the-counter (OTC)
pharmaceutical and nutritional
products with Agis growing
position in topical (creams,
ointments and gels) generic Rx
drugs and APIs. The new
Perrigo has outstanding R & D
capabilities and an experienced
global management team.
We are pleased with our
integration progress and
are enthusiastic about the
opportunities we see to
grow, and to enhance value
for our shareholders. The Agis
acquisition will provide us with
an expanding product line,
global presence, and synergies
that will contribute to our
generic Rx strategy.
During fiscal 2005 we also
continued to focus on expanding
the scope and reach of our
store brand OTC products
business. We did this through the
introduction of new products that
have switched from prescription
to over-the-counter status
(Rx-to-OTC switches), a strategy
that is key to the growth of our
Consumer Healthcare business.
In addition, we continued our
ongoing efforts to innovate in
manufacturing and distribution to
maximize profitability in a price-competitive marketplace. These
efforts contributed positively to
top and bottom line results in
fiscal 2005.
Importantly, following the
acquisition of Agis, we realigned the
company to reflect Perrigos more
diversified portfolio of products.
The new Perrigo is now comprised
of the following business segments:
Consumer Healthcare:
Store brand OTC pharmaceutical
and nutritional business, including
operations in the U.S., U.K.,
and Mexico
Rx Pharmaceuticals:
Topical and solid-dose
oral generic Rx drug products
API:
Chemicals sold to others for
use in generic pharmaceutical
manufacturing, plus those
used in Perrigo pharmaceutical
products
Other:
Cosmetics, toiletries and detergents;
branded pharmaceutical drugs,
diagnostics and other medical
products for the Israeli marketResults
It is important to note that fiscal
year-end 2005 results include one
quarter of contribution from the
Agis acquisition. During fiscal
2005, Perrigo sales surpassed
$1 billion for the first time, with
fiscal 2005 net sales reaching
$1.02 billion, an increase of
$126 million, or 14 percent over
fiscal 2004.
However, net income, including
a $386.8 million write-off of
in-process R & D, resulted in a
loss of $353 million, or $4.57 per
share. Adjusted for nonrecurring
items, excluding the
charges associated with the
acquisition, and a class action
lawsuit settlement, net income
was $63 million, or $0.81 per
share. The non-adjusted results
compared with net income of
$81 million, or $1.11 per share
last year, which included an
income tax benefit of $13
million, or $0.18 per share, and
an after-tax charge of $3.4
million, or $0.05 per share.
Agis Acquisition
As already noted, the years most
important event occurred when,
following shareholder approval
on March 15, 2005, we completed
the acquisition of Agis Industries
(1983) Ltd. on March 17, 2005.
Based in Israel, Agis is a
developer and manufacturer
of specialized generic
pharmaceuticals and APIs with
2004 sales of $405 million.
Concurrently, Perrigo shares
began trading on the Tel Aviv
Stock Exchange (TASE) under the
symbol PRGO and since that
time, the company has been
included in the TA-25 Index,
which includes the top 25
companies traded on the TASE.
The combination of Perrigo
and Agis has created a global
company with a track record
of sales and earnings growth
and broader capabilities to
grow in the global generic
pharmaceutical, API, and
consumer health care markets.
The combination of
Perrigo and Agis has
created a global
company with a track
record of sales and
earnings growth...
The new Perrigo has numerous
strategic and financial strengths,
including:
 strong market leadership
in the store brand consumer
health care sector as a primary
supplier to virtually every major
retailer in the U.S., Canada,
Mexico and the U.K;
 a platform for growth in
generic pharmaceuticals
through the position Agis
enjoys in topical generic
Rx drugs;
 an established position in API,
which has become increasingly
important to generic
pharmaceutical manufacturers
as a means to control costs;
 an enhanced store brand
OTC topical products portfolio,
with the opportunity to
leverage Agis complementary
semi-solid OTC products with
Perrigos sales, marketing, and
distribution; and
 expanded manufacturing and
R & D capabilities, combining
Perrigos strength in product
development, manufacturing,
and customized packaging
of solid dose and liquids
with Agis topical products
development expertise. To date, Perrigo has brought
more than three dozen
generic prescription products
to market, with more than
$100 million in sales. It has also
received U.S. Food and Drug
Administration (FDA) approval
for nearly four dozen
Abbreviated New Drug
Applications (ANDAs) for
prescription and OTC products,
and three approved European
Union (EU) registrations, in the
past three years.
Because there is limited
overlap between Perrigo and
Agis products, complementary
products will benefit from
increased access to retail
channels through our
extensive supply chain and
logistics management. In
addition, products resulting
from the Agis acquisition
should benefit from our
broad customer base.
The acquisition will also
expand our product pipeline.
Perrigo now has 17 prescription
and OTC ANDAs pending
with the FDA and 65 new
prescription and OTC products
under development.
Share Dividend Increase
and Repurchase Program
On October 29, 2004, the
Perrigo board of directors
declared a quarterly dividend of
$0.04 per share, a 14 percent
increase over $0.035 per share
paid in the last four quarters.
This move was in recognition
of the companys financial
strength and future prospects.
On April 26, 2005, the Perrigo
board of directors authorized
the repurchase of up to $30
million of Perrigo common
stock over the next 12 months.
This repurchase program
followed our previous $20
million repurchase program
that expired on April 28, 2005.
Perrigo has repurchased 6.2
million shares at a cost of
approximately $70 million
since November 2000.
Management and
Board Change
Mr. Moshe Arkin, former
chairman and president of
Agis Industries, became vice
chairman of Perrigo and
joined the board of directors
following the acquisition.
Mr. Arkin has more than three
decades of experience in the
pharmaceutical field, and
brings valuable international
pharmaceutical, as well as
specific generic Rx drug
expertise to Perrigo by also
serving as manager of Perrigos
global generic Rx drug and
API businesses.
Pseudoephedrine Restrictions
and Product Conversion
Recently, a number of major
retailers announced plans to
move certain OTC products
containing the active ingredient
pseudoephedrine to behind
the pharmacy counter. This
decision was in response to
new or proposed legislation in many states intended to slow
the misuse of pseudoephedrinebased
products in the manufacturing
of methamphetamine
for illegal drug use. We believe
these actions will have a
substantial negative impact on
our cough/cold product sales in
fiscal year 2006.
We are developing phenylephrine-
based alternatives
for many popular products
and expect to phase-in the
reformulated products over
the next 12-15 months.
GROWTH STRATEGY
Our growth strategy for our
Consumer Healthcare business
continues to be focused on the
introduction of new products
that are equivalent to
leading national brand OTC
pharmaceutical and nutritional
products, including products
that have switched, or are about
to switch, from prescription to
OTC status. Our FDA approval
and 2005 launch of nicotine
gum for the store brand market
resulted from this strategy and
enabled us to enter the smoking
cessation category.
We expect to continue to
grow our generic Rx drug
business. We believe that the
synergies between the OTC
pharmaceutical and generic
Rx drug businesses will enable
us to grow by leveraging
our R & D capabilities,
manufacturing expertise,
customer relationships, and
supply chain infrastructure.
Our strategy is to grow our
generic Rx drug presence
through the internal
development of generic drugs,
the acquisition and/or licensing
of generic drugs through
partner companies, and via
acquisitions of generic drug
capabilities and products. To
facilitate that development,
we invested $10 million in
fiscal 2005 in generic Rx drug
R & D. We anticipate that
investment to eclipse $20
million in fiscal 2006.
The Agis acquisition supports
our growth strategy by
providing Perrigo with a solid
generic Rx pipeline, along
with an established generic Rx
topicals business.
DELIVERING HEALTH
CARE MORE WAYS
THAN EVER
Our strategic progress during
fiscal 2005 has reinforced our
commitment to make Perrigo
the global leader in delivering
affordable, high-quality
health care.
Given worldwide aging
demographics, an increasingly
global economy, and the
pressing need for economically
sound health care solutions,
we believe Perrigos long-term
future is strategically solid. We
have the financial strength,
the management expertise,
and the growing pipeline of OTC
pharmaceutical and nutritional
products, combined with a
growing presence in generic
Rx to deliver even greater
health care value in the years
to come.
No company can effectively
compete in todays global
economy without highly
talented, committed people.
We continue to recruit and hire
the best-in-class in each of our
businesses. And we remain
committed to promoting our
values and integrity through
each and every Perrigo
employee. They have been,
and will continue to be, at the
very core of everything we do.
Sincerely,
David T. Gibbons
Chairman, President and
Chief Executive Officer
September 26, 2005