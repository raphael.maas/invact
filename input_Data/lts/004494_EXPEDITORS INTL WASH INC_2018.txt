TO OUR SHAREHOLDERS

2018 turned out to be another fantastic year in the history
of Expeditors. We set a new record for gross revenue, net
revenue, operating income, and earnings per share. We
moved more kilos of airfreight and ocean containers, and
processed more customs declarations than we had in any
prior year. Moreover, every product  not just air, ocean and
customs  performed better in 2018 than it had in any prior
year. We accomplished this through the teamwork of our
more than 17,000 amazing employees who are aligned on
our strategy, dedicated to outstanding customer service, and
focused on superior execution across our global network.
2018 also marked the 25th year in a row that we paid an
increased dividend to our shareholders.
2018 was also a year when the talk about disruption of our
industry reached an all-time high. Many continue to believe
that our industry will be disrupted by technology. Interestingly
enough, we thought the same thing in 1979 when the company
was founded, and we still believe that today. Consequently,
each year we invest significant resources to enhance our core
logistics platform and to develop new systems, it is part of
our DNA. Our continuing commitment to logistics technology
built by experienced logistics professionals was as integral
to the success our employees achieved in 2018, as it has
been throughout our history.
In addition, customers, shareholders, and employees have
increasingly shown interest in the various aspects of our
Environment, Sustainability, and Governance (ESG) efforts.
We are thankful that the vision set out by our founders
included a holistic approach that requires our employees
to be deeply engaged in these important areas, both locally
and globally.

From an environmental standpoint, in this Annual Report you
will see details on our new Amsterdam facility. What is not
said is that achieving the various environmental ratings for
the site was not a corporate mandate. Instead, the facility was
largely designed by our European management team, done
so in a way that met budget, and at the same time achieved
the high environmental standards that are important to our
employees and our company.
Regarding our social initiatives, we note the activities of our
employees in India for the program that they created there
called Avasar. This is an example of how our Company culture,
coupled with the interest and initiative of our employees,
allows us to be a better corporate citizen and engage in our
local communities.
We will continue to do more from an ESG standpoint, but we
also believe that the most successful efforts will be those that
are employee-driven and supported at the corporate level.
Again, the vision set out by our founders is alive and well
today and aligns perfectly with this approach.
As always, there are many to thank for our 2018 results. We
thank our employees for their commitment, dedication, and
execution. We thank our customers for putting trust in us and
allowing us to handle their complex supply chains. We thank
our service providers with the understanding that without
world-class service providers, we would not be the company
that we are today. Finally, we thank you, our shareholders, for
your continued trust in our company.

Jeffrey S. Musser
President & Chief Executive Officer, Director