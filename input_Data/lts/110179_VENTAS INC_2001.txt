Dear Stockholders,
2001 was a year of remarkable achievement for Ventas and our stockholders, producing total stockholder return of 123 percent.
This performance made Ventas the fourth best performing equity REIT in the country. It also placed us in the top four
percent of all NYSE-traded companies and in the top seven percent of companies in the Russell 2000 Index.With a reliable
and growing cash flow, high quality real estate assets, a creditworthy tenant, an experienced, cohesive management team,
increasing dividend paying ability and opportunities for growth and diversification, we expect to generate superior results
for our stockholders again this year. Ventas is now well positioned for the future.
2001  A breakthrough year
2001 was a breakthrough year for Ventas. In April,
our principal tenant, Kindred Healthcare, Inc.,
emerged successfully from bankruptcy and we
completed an excellent settlement of all our
Medicare disputes with the Department of Justice.
During the year we made exceptional additions to
our Board of Directors, electing Jay Gellert, Gary
Loveman and Sheli Rosenberg  all executives of
superior intelligence, integrity, experience and
independence. And we used our consistent excess
cash flow to benefit our stockholders through
the reinstatement of normal quarterly dividends
totaling $0.92 per share in 2001 and the repayment
of almost $40 million in debt.
We finished 2001 with the refinancing of $225 million
of debt through a commercial mortgage-backed securities
(CMBS) transaction, which saved the Company about $3.5 million per year by reducing our cost of debt. Proceeds of the
CMBS deal paid off all our debt maturities until December 2005, putting the Company in a very secure balance sheet position.
2002  Positioned for growth and prosperity
We expect to have the highest internal FFO (Funds From Operations) growth rate in the healthcare REIT sector, derived from
the built-in rent increases in our leases. Our long-term acute care hospitals (LTACs) and skilled nursing facilities (SNFs)
provide balance and diversification within our portfolio, and Kindred is operating them professionally and profitably.Our rental revenue streams from our master leases are highly reliable because of the significant profit our facilities generate
for our tenant. As an example, we estimate that Kindred earns over $0.70 in profit for each dollar of rent it pays to Ventas.
This strong Kindred profit margin at our facilities incentivizes continuing rental payments to us.
Our revenue streams are also highly reliable because nursing home operators now have lower debt burdens, smaller lease
payments and more efficient operating structures than those that existed in 1998 when the Prospective Payment System began
to wreak havoc on the nursing home industry. Today, Kindreds financial and operating stability will permit it to successfully
weather any contemplated changes in Medicare reimbursement reductions.We believe that Congress will act responsibly to keep
Medicare reimbursement levels relatively stable despite the current budget debate and political wrangling. But even if some
reimbursement cuts occur, our Company, as the owners of the real estate assets and the landlord, will be insulated from any
direct impact on earnings or cash flow.
On top of our internal and reliable FFO growth, we have the opportunity to further boost FFO through the combination
of paying down debt with proceeds of our Kindred equity stake and refinancing our current expensive debt with less costly
capital.We have a great platform to achieve future refinancings and we are actively pursuing opportunities that will drive
down our cost of debt and increase the value of Ventas equity.
Shaping the future
Ventas has produced repeated, exceptional returns for our stockholders of 80 percent
in 2000 and 123 percent in 2001, and has emerged as a leading healthcare REIT.
We believe we can continue to reward investors as we shift from a restructuring
mode into a growth and diversification phase. As we move forward, our goal is
to preserve the qualities that make us attractive and valuable  excellent assets,
a creditworthy and improving tenant, above-market cash flow-to-rent coverages
at our leased properties, high year-over-year FFO growth with real and immediate
opportunities for an FFO pop from refinancing and debt pay-down, and a
management team focused strictly on stockholder value.
At the same time, we will reduce our risk profile. Building a safer, more reliable
Company will be achieved as we (1) diversify our revenue sources and the sectors
represented in our asset portfolio to lessen our concentration risk, (2) continue
to use cash flow to pay down debt, (3) manage corporate and interest rate risk,
(4) monitor the physical and financial performance of our assets and (5) build
stronger and deeper organizational capabilities.
Our intention to implement a disciplined diversification program, probably through acquisitions, merits further discussion.
Stockholders who have doubled, tripled, or even quadrupled their investment in Ventas readily admit that we have done a
good job for them. But both old and new stockholders want to know how we intend to deliver superior returns now that
the Company has different opportunities and challenges.
First, if you add our built-in earnings growth of 7 to 8 percent per year to a dividend yield of between 7 and 8 percent, our
total return would approximate 15 percent (assuming a constant multiple). If we achieve additional refinancing successes,
our earnings growth could increase substantially above that level. There simply arent many competing investments that
offer such an attractive profile.
Second, while our challenges and corporate objectives are fresh, the methodology we will use to achieve success is not.
The processes we are using at Ventas as we reorient ourselves to a more traditional operating and acquisition environment
are identical to those tried and true methods I employed when I arrived at Ventas three years ago. The replication of these
steps will make us successful again. They include: gathering and absorbing information; making discerning judgments;
crafting a strategic plan to improve our Company; continuously challenging and refining that plan; hiring the best people
to execute reliably; developing and enforcing processes that will result in consistent, good decisions; and working with focus
and energy to achieve our corporate objectives.
The second phase of Ventass life as a REIT is designed to create additional wealth for our stockholders by accomplishing
the following objectives during the coming years:
1. Drive FFO growth
2. Diversify with discipline
3. Lower our cost of debt
4. Reduce our debt levels as a percentage
of total market capitalization
5. Develop infrastructure
6. Increase our dividend
As we define and implement our corporate strategy, the Company
will benefit greatly from our Board of Directors, which will continue
to play an integral, interactive role.We have deliberately chosen an
independent, experienced Board that is characterized by individuals
with strong ideas and opinions, a willingness to engage in healthy
debate and a strict awareness of their fiduciary role. You can be confident
that our Company will thrive with their guidance and oversight.
You will also benefit from the comprehensive experience and skill of
our management team, T. Richard Riney, John C. Thompson, Brian
K.Wood and Mary L. Smith. Their extensive healthcare and real estate backgrounds were broadened and tempered by our
complex and difficult challenges of the past few years. Each of my colleagues is highly talented and personally committed
to the Companys goals and our duties to our stockholders. These individuals, supported by our professional Louisvillebased
staff, form the right nucleus to execute Ventass future successes.
In the coming year, we intend to create an ever-improving, high-performing Company for our investors.We will continue
to act with transparency, consistency, integrity and credibility so that all of our constituents  stockholders, lenders,
prospective business partners, rating agencies, auditors and employees  can count on us. Our reputation remains our
most precious asset.
We encourage you to be a part of our efforts to deliver, once again, great returns and measurable progress toward our goals.
All of us at Ventas value your trust and will continue to earn it in the coming year.

Sincerely,
Debra A. Cafaro
President and CEO