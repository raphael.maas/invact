To Our Shareholders
2008 was a year of substantial investment in Pattersons
future as we worked to further expand the market presence
and value-added capabilities of our three businesses and
increase the returns to our shareholders.
 We strengthened Patterson Dentals competitive position
in the northeastern U.S. with the acquisition of a highly
respected regional dental distributor.
 Patterson Medical continued establishing a branch
office structure in selected markets through acquisitions
and internal start-ups.
 We acquired an industry-leading line of practice
management software for physical therapists to further
strengthen Patterson Medicals value-added platform.
 Webster Veterinary continued to expand its equipment
offerings, in addition to providing more product choices
for its customers.
 Webster bolstered its presence in the growing
southwestern U.S. with the acquisition of an Arizonabased
veterinary distributor.
 We repurchased approximately 18 million Patterson
shares as part of our initiative to realign Pattersons
capital structure.
These and other initiatives are components of our drive
to strengthen Pattersons long-term operating results. As
part of this ongoing commitment, each of our businesses
is implementing additional strategies aimed at enhancing
their future growth and profitability. We expect to realize the
initial positive impact from these new strategies in fiscal
2009, which should be a year of improved performance for
Patterson.
Financial Review
Consolidated sales increased 7% in fiscal 2008 to $3.0
billion from $2.8 billion in fiscal 2007. Net income of
$224.9 million or $1.69 per diluted share was up 8% from
$208.3 million or $1.51 in fiscal 2007.
During the second half of fiscal 2008, we repurchased
approximately 18 million shares under our 25 millionshare
repurchase authorization. In the fourth quarter, we
also closed on $525 million of long-term debt financing,
part of which was used to acquire shares under an
accelerated share repurchase (ASR) program. The positive
impact of the accretion on Pattersons earnings from our
share repurchases was largely offset by increased interest
expense and lower interest income on cash reserves in
fiscal 2008. However, the accretion from this buyback
activity is estimated at an incremental $0.06 per share in
fiscal 2009.
Investing in our three businesses remains our first priority
when it comes to capital deployment, but last years stock
repurchasing and debt financing transactions accomplished
several objectives:
 We returned $630 million to our shareholders.
 By taking advantage of excellent pricing in the debt
markets, we lowered our weighted average cost of
capital by about 100 basis points.
 We positioned Patterson to increase the returns to our
shareholders.
 We maintained our financial flexibility to take
advantage of acquisition opportunities.
Patterson Dental
Sales of Patterson Dental, our largest business, increased
6% in fiscal 2008 to $2.2 billion. Sales of consumable
dental supplies were solid throughout the year, reflecting
the continued strength of the North American dental
market. We are particularly encouraged by the strong
rebound in sales of the CEREC 3D dental restorative
system during last years second half. This growing sales
momentum reflected the positive impact of the new
software and crown milling chamber introduced late in
fiscal 2007. These enhancements have taken CERECs
performance to a new level, which is drawing considerable
attention in the marketplace. We believe the sales progress
of our CEREC line should continue in fiscal 2009.
Sales of basic dental equipment, including digital x-ray
systems and related software, fluctuated significantly
between quarters during fiscal 2008, resulting in belowforecasted
levels for the full year. As the industrys largest
distributor of dental equipment, Patterson Dental is
implementing new sales and marketing initiatives designed
to strengthen the performance of its entire equipment
offerings, including digital technology.
In April 2008, we acquired A. Leventhal & Sons, Inc.,
a full-service regional distributor of dental supplies and
equipment with annual sales of $18 million. The addition
of Leventhal, which serves customers in Pennsylvania,
New Jersey, New York, Delaware and Maryland, has
strengthened Pattersons existing market position in the
northeastern U.S.
Webster Veterinary
Fiscal 2008 marked another year of strong results for
Webster Veterinary, one of the nations largest distributors
of companion-pet veterinary supplies. Websters sales
grew 12% to $446 million, fueled in part by the strong
performance of its consumable supply business. Our
strategic decision to provide more choices for our customers
in several key product categories, including vaccines and
flea/tick and heartworm medications, is working as intended.
Websters fiscal 2008 performance also benefited from its
strategic emphasis on equipment and practice management
software, which represents a major component of this units
drive to further strengthen its value-added platform. Webster
is now selling such equipment as digital x-ray systems,
tables, kennels and cabinetry in its markets. This initiative
is supported by Websters steadily expanding capabilities in
local technical support and financing. During fiscal 2009,
Webster will focus a significant portion of its attention on
refining and further expanding its relatively new equipment
business.
Associated Medical Supply, Inc., a veterinary distributor
serving companion-pet veterinarians in Arizona and
Utah, was acquired late in fiscal 2008. This transaction
was of strategic importance, since it strengthened
Websters competitive position in the southwestern U.S.
The acquisition of New England X-Ray, Inc., a specialty
equipment and service business in the greater Boston
market, provided a platform for expanding Websters
equipment business in this important region.
Patterson Medical
Patterson Medical, the worlds leading distributor of
rehabilitation supplies and equipment, reported sales
growth of 11% to $371 million. This business, which is
starting to realize its potential in the large and growing
rehabilitation market, is responding to strategies aimed
at implementing a more extensive value-added business
model. Patterson Medical has greatly expanded its
field sales force, which currently totals more than 200
representatives. In addition, it continued to establish a
full-service branch office structure. By the end of fiscal
2008, Patterson Medical had acquired or internally
started 12 branch offices around the country. Also during
fiscal 2008, Patterson Medical became the only national
distributor of the industry-leading line of Chattanooga
Medical Supply rehabilitation equipment.
In November 2007, we acquired PTOS software, a
leading line of practice management software for physical
therapists. Endorsed by the largest provider networks
in the rehabilitation market, PTOS software is enabling
Patterson Medical to garner new customers and deepen
its relationships with existing customers.
Fiscal 2008 was a year of investment and progress, and
more work remains ahead of us. We are confident about
getting this job done due to the quality and commitment
of our many outstanding employees. We thank our people
for their hard work, and we also appreciate the continued
support of our shareholders, valued customers and
business partners.
Sincerely,
James W. Wiltz
President and Chief Executive Officer